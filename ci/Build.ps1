param(
	[Switch] $Rebuild = $false
)
$ErrorActionPreference = "Stop"

$ENV:PATH +=" ;C:\\Python27;C:\\HighTec\\toolchains\\tricore\\v4.9.3.0\\bin;C:\\HighTec\\licensemanager"
$ENV:RLM_LICENSE = "esvl-licsvr05.itc.global.mahle@5053"
$ENV:HTC_DEVELOPMENT = "C:\\HighTec"
$ENV:RLM_LICENSES = "C:\\HighTec\\licenses"

$ENV:PATH += ";$((gl).path)\CODE\Source\mak"

pushd CODE\Source
try {
	if ($Rebuild) {
		echo "REBUILD selected"
		Run-Legacy .\build d
		Run-Legacy .\build b
	} else {
		Run-Legacy .\build a
	}	
} finally {
	popd
}