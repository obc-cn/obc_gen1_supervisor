/* This file contains stub implementations of the AUTOSAR RTE functions.
   The stub implementations can be used for testing the generated code in
   Simulink, for example, in SIL/PIL simulations of the component under
   test. Note that this file should be replaced with an appropriate RTE
   file when deploying the generated code outside of Simulink.

   This file is generated for:
   Atomic software component:  "CtApAEM"
   ARXML schema: "4.0"
   File generated on: "22-Oct-2019 11:27:30"  */

#include "Rte_CtApAEM.h"

/* Parameters */
boolean Rte_CData_UCE_bInhPtlWkuX10_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX10_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX10_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX11_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX11_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX11_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX12_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX12_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX12_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX13_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX13_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX13_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX14_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX14_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX14_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX15_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX15_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX15_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX16_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX16_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX16_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX1_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX1_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX1_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX2_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX2_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX2_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX3_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX3_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX3_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX4_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX4_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX4_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX5_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX5_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX5_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX6_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX6_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX6_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX7_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX7_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX7_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX8_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX8_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX8_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuX9_C_data;
boolean Rte_CData_UCE_bInhPtlWkuX9_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuX9_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuY1_C_data;
boolean Rte_CData_UCE_bInhPtlWkuY1_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuY1_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuY2_C_data;
boolean Rte_CData_UCE_bInhPtlWkuY2_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuY2_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuY3_C_data;
boolean Rte_CData_UCE_bInhPtlWkuY3_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuY3_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuY4_C_data;
boolean Rte_CData_UCE_bInhPtlWkuY4_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuY4_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuY5_C_data;
boolean Rte_CData_UCE_bInhPtlWkuY5_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuY5_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuY6_C_data;
boolean Rte_CData_UCE_bInhPtlWkuY6_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuY6_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuY7_C_data;
boolean Rte_CData_UCE_bInhPtlWkuY7_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuY7_C_data;
}

boolean Rte_CData_UCE_bInhPtlWkuY8_C_data;
boolean Rte_CData_UCE_bInhPtlWkuY8_C(void)
{
  return Rte_CData_UCE_bInhPtlWkuY8_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX10AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX10AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX10AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX11AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX11AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX11AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX12AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX12AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX12AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX13AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX13AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX13AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX14AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX14AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX14AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX15AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX15AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX15AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX16AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX16AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX16AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX1AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX1AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX1AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX2AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX2AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX2AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX3AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX3AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX3AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX4AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX4AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX4AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX5AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX5AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX5AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX6AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX6AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX6AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX7AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX7AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX7AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX8AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX8AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX8AcvMod_C_data;
}

boolean Rte_CData_UCE_bSlavePtlWkuX9AcvMod_C_data;
boolean Rte_CData_UCE_bSlavePtlWkuX9AcvMod_C(void)
{
  return Rte_CData_UCE_bSlavePtlWkuX9AcvMod_C_data;
}

boolean Rte_CData_UCE_noUCETyp_C_data;
boolean Rte_CData_UCE_noUCETyp_C(void)
{
  return Rte_CData_UCE_noUCETyp_C_data;
}

IdtVehicleSpeedThreshold Rte_CData_UCE_spdThdDegDeac_C_data;
IdtVehicleSpeedThreshold Rte_CData_UCE_spdThdDegDeac_C(void)
{
  return Rte_CData_UCE_spdThdDegDeac_C_data;
}

IdtVehicleSpeedDegThreshold Rte_CData_UCE_spdThdNomDeac_C_data;
IdtVehicleSpeedDegThreshold Rte_CData_UCE_spdThdNomDeac_C(void)
{
  return Rte_CData_UCE_spdThdNomDeac_C_data;
}

IdtDegMainWkuExtinctionTime Rte_CData_UCE_tiDegMainWkuDeac_C_data;
IdtDegMainWkuExtinctionTime Rte_CData_UCE_tiDegMainWkuDeac_C(void)
{
  return Rte_CData_UCE_tiDegMainWkuDeac_C_data;
}

IdtTimeNomMainWkuDisord Rte_CData_UCE_tiMainDisrdDet_C_data;
IdtTimeNomMainWkuDisord Rte_CData_UCE_tiMainDisrdDet_C(void)
{
  return Rte_CData_UCE_tiMainDisrdDet_C_data;
}

IdtTimeNomMainWkuIncst Rte_CData_UCE_tiMainIncstDet_C_data;
IdtTimeNomMainWkuIncst Rte_CData_UCE_tiMainIncstDet_C(void)
{
  return Rte_CData_UCE_tiMainIncstDet_C_data;
}

IdtEnforcedMainWkuTime Rte_CData_UCE_tiMainTransForc_C_data;
IdtEnforcedMainWkuTime Rte_CData_UCE_tiMainTransForc_C(void)
{
  return Rte_CData_UCE_tiMainTransForc_C_data;
}

IdtMainWkuValidTime Rte_CData_UCE_tiMainWkuAcv_C_data;
IdtMainWkuValidTime Rte_CData_UCE_tiMainWkuAcv_C(void)
{
  return Rte_CData_UCE_tiMainWkuAcv_C_data;
}

IdtTimeNomMainWkuRehabilit Rte_CData_UCE_tiMainWkuReh_C_data;
IdtTimeNomMainWkuRehabilit Rte_CData_UCE_tiMainWkuReh_C(void)
{
  return Rte_CData_UCE_tiMainWkuReh_C_data;
}

IdtInternalPartialWkuMaxDuration Rte_CData_UCE_tiMaxTiIntPtlWku_C_data;
IdtInternalPartialWkuMaxDuration Rte_CData_UCE_tiMaxTiIntPtlWku_C(void)
{
  return Rte_CData_UCE_tiMaxTiIntPtlWku_C_data;
}

IdtMasterPartialWkuY1MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY1_C_data;
IdtMasterPartialWkuY1MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY1_C(void)
{
  return Rte_CData_UCE_tiMaxTiMstPtlWkuY1_C_data;
}

IdtMasterPartialWkuY2MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY2_C_data;
IdtMasterPartialWkuY2MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY2_C(void)
{
  return Rte_CData_UCE_tiMaxTiMstPtlWkuY2_C_data;
}

IdtMasterPartialWkuY3MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY3_C_data;
IdtMasterPartialWkuY3MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY3_C(void)
{
  return Rte_CData_UCE_tiMaxTiMstPtlWkuY3_C_data;
}

IdtMasterPartialWkuY4MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY4_C_data;
IdtMasterPartialWkuY4MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY4_C(void)
{
  return Rte_CData_UCE_tiMaxTiMstPtlWkuY4_C_data;
}

IdtMasterPartialWkuY5MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY5_C_data;
IdtMasterPartialWkuY5MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY5_C(void)
{
  return Rte_CData_UCE_tiMaxTiMstPtlWkuY5_C_data;
}

IdtMasterPartialWkuY6MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY6_C_data;
IdtMasterPartialWkuY6MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY6_C(void)
{
  return Rte_CData_UCE_tiMaxTiMstPtlWkuY6_C_data;
}

IdtMasterPartialWkuY7MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY7_C_data;
IdtMasterPartialWkuY7MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY7_C(void)
{
  return Rte_CData_UCE_tiMaxTiMstPtlWkuY7_C_data;
}

IdtMasterPartialWkuY8MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY8_C_data;
IdtMasterPartialWkuY8MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY8_C(void)
{
  return Rte_CData_UCE_tiMaxTiMstPtlWkuY8_C_data;
}

IdtShutdownPreparationMaxTime Rte_CData_UCE_tiMaxTiShutDownPrep_C_data;
IdtShutdownPreparationMaxTime Rte_CData_UCE_tiMaxTiShutDownPrep_C(void)
{
  return Rte_CData_UCE_tiMaxTiShutDownPrep_C_data;
}

IdtMasterPartialWkuY1MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY1_C_data;
IdtMasterPartialWkuY1MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY1_C(void)
{
  return Rte_CData_UCE_tiMinTiMstPtlWkuY1_C_data;
}

IdtMasterPartialWkuY2MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY2_C_data;
IdtMasterPartialWkuY2MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY2_C(void)
{
  return Rte_CData_UCE_tiMinTiMstPtlWkuY2_C_data;
}

IdtMasterPartialWkuY3MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY3_C_data;
IdtMasterPartialWkuY3MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY3_C(void)
{
  return Rte_CData_UCE_tiMinTiMstPtlWkuY3_C_data;
}

IdtMasterPartialWkuY4MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY4_C_data;
IdtMasterPartialWkuY4MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY4_C(void)
{
  return Rte_CData_UCE_tiMinTiMstPtlWkuY4_C_data;
}

IdtMasterPartialWkuY5MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY5_C_data;
IdtMasterPartialWkuY5MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY5_C(void)
{
  return Rte_CData_UCE_tiMinTiMstPtlWkuY5_C_data;
}

IdtMasterPartialWkuY6MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY6_C_data;
IdtMasterPartialWkuY6MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY6_C(void)
{
  return Rte_CData_UCE_tiMinTiMstPtlWkuY6_C_data;
}

IdtMasterPartialWkuY7MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY7_C_data;
IdtMasterPartialWkuY7MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY7_C(void)
{
  return Rte_CData_UCE_tiMinTiMstPtlWkuY7_C_data;
}

IdtMasterPartialWkuY8MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY8_C_data;
IdtMasterPartialWkuY8MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY8_C(void)
{
  return Rte_CData_UCE_tiMinTiMstPtlWkuY8_C_data;
}

IdtShutdownPreparationMinTime Rte_CData_UCE_tiMinTiShutDownPrep_C_data;
IdtShutdownPreparationMinTime Rte_CData_UCE_tiMinTiShutDownPrep_C(void)
{
  return Rte_CData_UCE_tiMinTiShutDownPrep_C_data;
}

IdtMainWkuDevalidTime Rte_CData_UCE_tiNomMainWkuDeac_C_data;
IdtMainWkuDevalidTime Rte_CData_UCE_tiNomMainWkuDeac_C(void)
{
  return Rte_CData_UCE_tiNomMainWkuDeac_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX10Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX10Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX10Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX10Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX10Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX10Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX10Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX10Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX10Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX11Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX11Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX11Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX11Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX11Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX11Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX11Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX11Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX11Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX12Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX12Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX12Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX12Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX12Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX12Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX12Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX12Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX12Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX13Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX13Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX13Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX13Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX13Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX13Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX13Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX13Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX13Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX14Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX14Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX14Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX14Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX14Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX14Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX14Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX14Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX14Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX15Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX15Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX15Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX15Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX15Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX15Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX15Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX15Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX15Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX16Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX16Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX16Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX16Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX16Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX16Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX16Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX16Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX16Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX1Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX1Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX1Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX1Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX1Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX1Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX1Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX1Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX1Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX2Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX2Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX2Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX2Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX2Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX2Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX2Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX2Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX2Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX3Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX3Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX3Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX3Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX3Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX3Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX3Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX3Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX3Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX4Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX4Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX4Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX4Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX4Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX4Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX4Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX4Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX4Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX5Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX5Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX5Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX5Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX5Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX5Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX5Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX5Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX5Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX6Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX6Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX6Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX6Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX6Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX6Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX6Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX6Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX6Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX7Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX7Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX7Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX7Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX7Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX7Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX7Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX7Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX7Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX8Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX8Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX8Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX8Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX8Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX8Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX8Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX8Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX8Lock_C_data;
}

IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX9Acv_C_data;
IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX9Acv_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX9Acv_C_data;
}

IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX9Deac_C_data;
IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX9Deac_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX9Deac_C_data;
}

IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX9Lock_C_data;
IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX9Lock_C(void)
{
  return Rte_CData_UCE_tiPtlWkuX9Lock_C_data;
}

IdtTimeRCDPulse Rte_CData_UCE_tiRCDLineCmdAcv_C_data;
IdtTimeRCDPulse Rte_CData_UCE_tiRCDLineCmdAcv_C(void)
{
  return Rte_CData_UCE_tiRCDLineCmdAcv_C_data;
}

IdtRCDLineGndSCConfirmTime Rte_CData_UCE_tiRCDLineScgDet_C_data;
IdtRCDLineGndSCConfirmTime Rte_CData_UCE_tiRCDLineScgDet_C(void)
{
  return Rte_CData_UCE_tiRCDLineScgDet_C_data;
}

IdtRCDLineGndSCRehabilitTime Rte_CData_UCE_tiRCDLineScgReh_C_data;
IdtRCDLineGndSCRehabilitTime Rte_CData_UCE_tiRCDLineScgReh_C(void)
{
  return Rte_CData_UCE_tiRCDLineScgReh_C_data;
}

IdtExtinctionTime Rte_CData_UCE_tiTransitoryDeac_C_data;
IdtExtinctionTime Rte_CData_UCE_tiTransitoryDeac_C(void)
{
  return Rte_CData_UCE_tiTransitoryDeac_C_data;
}

IdtCOMLatchMaxDuration Rte_CData_UCE_tiMaxTiComLatch_C_data;
IdtCOMLatchMaxDuration Rte_CData_UCE_tiMaxTiComLatch_C(void)
{
  return Rte_CData_UCE_tiMaxTiComLatch_C_data;
}
