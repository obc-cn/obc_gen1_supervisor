/* This file contains stub implementations of the AUTOSAR RTE functions.
   The stub implementations can be used for testing the generated code in
   Simulink, for example, in SIL/PIL simulations of the component under
   test. Note that this file should be replaced with an appropriate RTE
   file when deploying the generated code outside of Simulink.

   This file is generated for:
   Atomic software component:  "CtApAEM"
   ARXML schema: "4.0"
   File generated on: "22-Oct-2019 11:27:30"  */

#ifndef Rte_Type_h
#define Rte_Type_h
#include "Std_Types.h"

/* AUTOSAR Implementation data types, specific to software component */
/* AUTOSAR Primitive Types */
typedef uint16 ABS_VehSpd;
typedef uint16 IdtCOMLatchMaxDuration;
typedef uint16 IdtDegMainWkuExtinctionTime;
typedef uint16 IdtEnforcedMainWkuTime;
typedef uint16 IdtExtinctionTime;
typedef uint16 IdtInternalPartialWkuMaxDuration;
typedef uint8 IdtMainWkuDevalidTime;
typedef uint8 IdtMainWkuValidTime;
typedef uint16 IdtMasterPartialWkuY1MaxDuration;
typedef uint16 IdtMasterPartialWkuY1MinDuration;
typedef uint16 IdtMasterPartialWkuY2MaxDuration;
typedef uint16 IdtMasterPartialWkuY2MinDuration;
typedef uint16 IdtMasterPartialWkuY3MaxDuration;
typedef uint16 IdtMasterPartialWkuY3MinDuration;
typedef uint16 IdtMasterPartialWkuY4MaxDuration;
typedef uint16 IdtMasterPartialWkuY4MinDuration;
typedef uint16 IdtMasterPartialWkuY5MaxDuration;
typedef uint16 IdtMasterPartialWkuY5MinDuration;
typedef uint16 IdtMasterPartialWkuY6MaxDuration;
typedef uint16 IdtMasterPartialWkuY6MinDuration;
typedef uint16 IdtMasterPartialWkuY7MaxDuration;
typedef uint16 IdtMasterPartialWkuY7MinDuration;
typedef uint16 IdtMasterPartialWkuY8MaxDuration;
typedef uint16 IdtMasterPartialWkuY8MinDuration;
typedef uint16 IdtRCDLineGndSCConfirmTime;
typedef uint16 IdtRCDLineGndSCRehabilitTime;
typedef uint16 IdtShutdownPreparationMaxTime;
typedef uint16 IdtShutdownPreparationMinTime;
typedef uint8 IdtSlavePartialWkuXDevalidTime;
typedef uint16 IdtSlavePartialWkuXLockTime;
typedef uint8 IdtSlavePartialWkuXValidTime;
typedef uint16 IdtTimeNomMainWkuDisord;
typedef uint16 IdtTimeNomMainWkuIncst;
typedef uint16 IdtTimeNomMainWkuRehabilit;
typedef uint16 IdtTimeRCDPulse;
typedef uint8 IdtVehicleSpeedDegThreshold;
typedef uint8 IdtVehicleSpeedThreshold;
typedef boolean BSI_PostDriveWakeup;
typedef boolean BSI_PreDriveWakeup;
typedef boolean OBC_CoolingWakeup;
typedef boolean OBC_HVBattRechargeWakeup;
typedef boolean OBC_HoldDiscontactorWakeup;
typedef boolean OBC_PIStateInfoWakeup;
typedef boolean SUPV_PostDriveWupState;
typedef boolean SUPV_PreDriveWupState;
typedef boolean SUPV_PrecondElecWupState;
typedef boolean SUPV_RCDLineState;
typedef boolean SUPV_StopDelayedHMIWupState;
typedef boolean VCU_PrecondElecWakeup;
typedef boolean VCU_StopDelayedHMIWakeup;

/* AUTOSAR Enumeration Types */
typedef uint8 BSI_MainWakeup;

#ifndef Cx0_Invalid_RCD
#define Cx0_Invalid_RCD                (0)
#endif

#ifndef Cx1_No_main_wake_up_request
#define Cx1_No_main_wake_up_request    (1)
#endif

#ifndef Cx2_Main_wake_up_request
#define Cx2_Main_wake_up_request       (2)
#endif

#ifndef Cx3_Not_valid
#define Cx3_Not_valid                  (3)
#endif

typedef uint8 IdtAppRCDECUState;

#ifndef APP_STATE_0
#define APP_STATE_0                    (0)
#endif

#ifndef APP_STATE_1
#define APP_STATE_1                    (1)
#endif

#ifndef APP_STATE_2
#define APP_STATE_2                    (2)
#endif

#ifndef APP_STATE_3
#define APP_STATE_3                    (3)
#endif

#ifndef APP_STATE_4
#define APP_STATE_4                    (4)
#endif

#ifndef APP_STATE_5
#define APP_STATE_5                    (5)
#endif

typedef uint8 SUPV_ECUElecStateRCD;

#ifndef Cx0_Partial_wakeup
#define Cx0_Partial_wakeup             (0)
#endif

#ifndef Cx1_Internal_wakeup
#define Cx1_Internal_wakeup            (1)
#endif

#ifndef Cx2_Transitory
#define Cx2_Transitory                 (2)
#endif

#ifndef Cx3_Nominal_main_wakeup
#define Cx3_Nominal_main_wakeup        (3)
#endif

#ifndef Cx4_Degraded_main_wakeup
#define Cx4_Degraded_main_wakeup       (4)
#endif

#ifndef Cx5_Shutdown_preparation
#define Cx5_Shutdown_preparation       (5)
#endif

#ifndef Cx6_Reserved
#define Cx6_Reserved                   (6)
#endif

#ifndef Cx7_Reserved
#define Cx7_Reserved                   (7)
#endif

typedef void* Rte_Instance;

#endif
