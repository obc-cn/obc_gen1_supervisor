/*
 * File: CtApAEM_data.c
 *
 * Code generated for Simulink model 'CtApAEM'.
 *
 * Model version                  : 1.2
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Tue Oct 22 11:27:15 2019
 *
 * Target selection: autosar.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "CtApAEM.h"
#include "CtApAEM_private.h"

/* Constant parameters (auto storage) */
const ConstP_CtApAEM_T CtApAEM_ConstP = {
  /* Pooled Parameter (Expression: [0 1;1 0;0 1;0 1;1 0;1 0;1 0;1 0])
   * Referenced by:
   *   '<S512>/Logic'
   *   '<S517>/Logic'
   *   '<S551>/Logic'
   *   '<S555>/Logic'
   *   '<S590>/Logic'
   *   '<S594>/Logic'
   *   '<S599>/Logic'
   *   '<S613>/Logic'
   *   '<S617>/Logic'
   *   '<S28>/Logic'
   *   '<S32>/Logic'
   *   '<S39>/Logic'
   *   '<S43>/Logic'
   *   '<S47>/Logic'
   *   '<S52>/Logic'
   *   '<S57>/Logic'
   *   '<S62>/Logic'
   *   '<S570>/Logic'
   *   '<S574>/Logic'
   *   '<S579>/Logic'
   *   '<S584>/Logic'
   *   '<S71>/Logic'
   *   '<S75>/Logic'
   *   '<S86>/Logic'
   *   '<S90>/Logic'
   *   '<S94>/Logic'
   *   '<S533>/Logic'
   *   '<S537>/Logic'
   *   '<S226>/Logic'
   *   '<S235>/Logic'
   *   '<S244>/Logic'
   *   '<S253>/Logic'
   *   '<S262>/Logic'
   *   '<S271>/Logic'
   *   '<S280>/Logic'
   *   '<S289>/Logic'
   *   '<S298>/Logic'
   *   '<S307>/Logic'
   *   '<S316>/Logic'
   *   '<S325>/Logic'
   *   '<S334>/Logic'
   *   '<S343>/Logic'
   *   '<S352>/Logic'
   *   '<S361>/Logic'
   *   '<S370>/Logic'
   *   '<S379>/Logic'
   *   '<S388>/Logic'
   *   '<S397>/Logic'
   *   '<S406>/Logic'
   *   '<S415>/Logic'
   *   '<S424>/Logic'
   *   '<S433>/Logic'
   *   '<S442>/Logic'
   *   '<S451>/Logic'
   *   '<S460>/Logic'
   *   '<S469>/Logic'
   *   '<S478>/Logic'
   *   '<S487>/Logic'
   *   '<S496>/Logic'
   *   '<S505>/Logic'
   */
  { 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0 }
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
