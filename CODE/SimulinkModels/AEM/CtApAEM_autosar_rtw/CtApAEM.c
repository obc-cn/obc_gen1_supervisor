/*
 * File: CtApAEM.c
 *
 * Code generated for Simulink model 'CtApAEM'.
 *
 * Model version                  : 1.2
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Tue Oct 22 11:27:15 2019
 *
 * Target selection: autosar.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "CtApAEM.h"
#include "CtApAEM_private.h"

/* Named constants for Chart: '<S66>/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD' */
#define CtApAEM_IN_Defaut_actif        ((uint8_T)1U)
#define CtApAEM_IN_Defaut_inactif      ((uint8_T)2U)
#define CtApAEM_IN_NO_ACTIVE_CHILD     ((uint8_T)0U)
#define CtApAEM_active                 (true)
#define CtApAEM_inactive               (false)

/* Named constants for Chart: '<S14>/F01_05_01_Machine_etats_RCD' */
#define C_IN_Preparation_Mise_en_veille ((uint8_T)1U)
#define CtA_IN_Reveil_principal_degrade ((uint8_T)4U)
#define CtA_IN_Reveil_principal_nominal ((uint8_T)5U)
#define CtApAEM_DEGRADED_MAIN_WAKEUP   (5.0)
#define CtApAEM_IN_Reveil_partiel      ((uint8_T)3U)
#define CtApAEM_IN_Transitoire         ((uint8_T)6U)
#define CtApAEM_NOMINAL_MAIN_WAKEUP    (4.0)
#define CtApAEM_PARTIAL_WAKEUP         (1.0)
#define CtApAEM_SHUTDOWN_PREPARATION   (7.0)
#define CtApAEM_TRANSITORY_STATE       (3.0)
#define CtApA_IN_Reveil_Partiel_interne ((uint8_T)2U)

/* Block signals (auto storage) */
B_CtApAEM_T CtApAEM_B;

/* Block states (auto storage) */
DW_CtApAEM_T CtApAEM_DW;

/*
 * Output and update for atomic system:
 *    '<S25>/DetectSat'
 *    '<S26>/DetectSat'
 *    '<S35>/DetectSat'
 *    '<S36>/DetectSat'
 *    '<S37>/DetectSat'
 *    '<S50>/DetectSat'
 *    '<S55>/DetectSat'
 *    '<S60>/DetectSat'
 *    '<S68>/DetectSat'
 *    '<S69>/DetectSat'
 *    ...
 */
void CtApAEM_DetectSat(real_T rtu_SatMax, real_T rtu_In, real_T rtu_SatMin,
  B_DetectSat_CtApAEM_T *localB)
{
  /* MinMax: '<S27>/MinMax2' incorporates:
   *  MinMax: '<S27>/MinMax1'
   */
  localB->MinMax2 = fmin(rtu_SatMax, fmax(rtu_In, rtu_SatMin));
}

/*
 * System reset for atomic system:
 *    '<S25>/rising_edge'
 *    '<S25>/rising_edge1'
 *    '<S26>/rising_edge'
 *    '<S26>/rising_edge1'
 *    '<S35>/rising_edge'
 *    '<S35>/rising_edge1'
 *    '<S36>/rising_edge'
 *    '<S36>/rising_edge1'
 *    '<S37>/rising_edge'
 *    '<S37>/rising_edge1'
 *    ...
 */
void CtApAEM_rising_edge_Reset(DW_rising_edge_CtApAEM_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<S29>/UnitDelay' */
  localDW->UnitDelay_DSTATE = false;
}

/*
 * Output and update for atomic system:
 *    '<S25>/rising_edge'
 *    '<S25>/rising_edge1'
 *    '<S26>/rising_edge'
 *    '<S26>/rising_edge1'
 *    '<S35>/rising_edge'
 *    '<S35>/rising_edge1'
 *    '<S36>/rising_edge'
 *    '<S36>/rising_edge1'
 *    '<S37>/rising_edge'
 *    '<S37>/rising_edge1'
 *    ...
 */
void CtApAEM_rising_edge(boolean_T rtu_Signal, B_rising_edge_CtApAEM_T *localB,
  DW_rising_edge_CtApAEM_T *localDW)
{
  /* Logic: '<S29>/Logical Operator' incorporates:
   *  Logic: '<S29>/Logical Operator1'
   *  UnitDelay: '<S29>/UnitDelay'
   */
  localB->LogicalOperator = (rtu_Signal && (!localDW->UnitDelay_DSTATE));

  /* Update for UnitDelay: '<S29>/UnitDelay' */
  localDW->UnitDelay_DSTATE = rtu_Signal;
}

/*
 * System initialize for atomic system:
 *    '<S18>/TurnOnDelay1'
 *    '<S18>/TurnOnDelay2'
 *    '<S19>/TurnOnDelay1'
 *    '<S19>/TurnOnDelay2'
 *    '<S19>/TurnOnDelay3'
 *    '<S20>/TurnOnDelay'
 *    '<S21>/TurnOnDelay'
 *    '<S22>/TurnOnDelay'
 *    '<S65>/TurnOnDelay1'
 *    '<S79>/TurnOnDelay1'
 *    ...
 */
void CtApAEM_TurnOnDelay1_Init(DW_TurnOnDelay1_CtApAEM_T *localDW)
{
  /* InitializeConditions for Memory: '<S28>/Memory' */
  localDW->Memory_PreviousInput = true;
}

/*
 * System reset for atomic system:
 *    '<S18>/TurnOnDelay1'
 *    '<S18>/TurnOnDelay2'
 *    '<S19>/TurnOnDelay1'
 *    '<S19>/TurnOnDelay2'
 *    '<S19>/TurnOnDelay3'
 *    '<S20>/TurnOnDelay'
 *    '<S21>/TurnOnDelay'
 *    '<S22>/TurnOnDelay'
 *    '<S65>/TurnOnDelay1'
 *    '<S79>/TurnOnDelay1'
 *    ...
 */
void CtApAEM_TurnOnDelay1_Reset(DW_TurnOnDelay1_CtApAEM_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<S25>/UnitDelay2' */
  localDW->UnitDelay2_DSTATE = 0.0;

  /* InitializeConditions for UnitDelay: '<S25>/UnitDelay3' */
  localDW->UnitDelay3_DSTATE = false;

  /* InitializeConditions for Memory: '<S28>/Memory' */
  localDW->Memory_PreviousInput = true;

  /* SystemReset for Atomic SubSystem: '<S25>/rising_edge' */
  CtApAEM_rising_edge_Reset(&localDW->rising_edge);

  /* End of SystemReset for SubSystem: '<S25>/rising_edge' */

  /* SystemReset for Atomic SubSystem: '<S25>/rising_edge1' */
  CtApAEM_rising_edge_Reset(&localDW->rising_edge1);

  /* End of SystemReset for SubSystem: '<S25>/rising_edge1' */
}

/*
 * Output and update for atomic system:
 *    '<S18>/TurnOnDelay1'
 *    '<S18>/TurnOnDelay2'
 *    '<S19>/TurnOnDelay1'
 *    '<S19>/TurnOnDelay2'
 *    '<S19>/TurnOnDelay3'
 *    '<S20>/TurnOnDelay'
 *    '<S21>/TurnOnDelay'
 *    '<S22>/TurnOnDelay'
 *    '<S65>/TurnOnDelay1'
 *    '<S79>/TurnOnDelay1'
 *    ...
 */
void CtApAEM_TurnOnDelay1(boolean_T rtu_In, real_T rtu_Tdly, real_T rtu_Te,
  B_TurnOnDelay1_CtApAEM_T *localB, DW_TurnOnDelay1_CtApAEM_T *localDW)
{
  int32_T rowIdx;
  real_T rtb_MinMax2;
  boolean_T rtb_RelationalOperator3_m;
  real_T rtb_Switch_l;

  /* MinMax: '<S25>/MinMax2' incorporates:
   *  Constant: '<S25>/Constant8'
   */
  rtb_MinMax2 = fmax(rtu_Tdly, 0.0);

  /* RelationalOperator: '<S25>/Relational Operator3' */
  rtb_RelationalOperator3_m = (rtb_MinMax2 < rtu_Te);

  /* Outputs for Atomic SubSystem: '<S25>/rising_edge' */
  CtApAEM_rising_edge(rtu_In, &localB->rising_edge, &localDW->rising_edge);

  /* End of Outputs for SubSystem: '<S25>/rising_edge' */

  /* Switch: '<S25>/Switch' incorporates:
   *  Constant: '<S25>/Constant10'
   *  Constant: '<S25>/Constant11'
   *  Constant: '<S25>/Constant13'
   *  Constant: '<S25>/Constant14'
   *  Logic: '<S25>/Logical Operator4'
   *  Product: '<S25>/Divide1'
   *  Sum: '<S25>/Sum3'
   *  Sum: '<S25>/Sum4'
   *  UnitDelay: '<S25>/UnitDelay2'
   *  UnitDelay: '<S25>/UnitDelay3'
   */
  if (localDW->UnitDelay3_DSTATE || localB->rising_edge.LogicalOperator) {
    rtb_Switch_l = 0.0;
  } else {
    /* Outputs for Atomic SubSystem: '<S25>/DetectSat' */
    CtApAEM_DetectSat(rtu_Te * 10.0 + rtb_MinMax2, localDW->UnitDelay2_DSTATE,
                      0.0, &localB->DetectSat);

    /* End of Outputs for SubSystem: '<S25>/DetectSat' */
    rtb_Switch_l = (2.2204460492503131E-16 + rtu_Te) + localB->DetectSat.MinMax2;
  }

  /* End of Switch: '<S25>/Switch' */

  /* Outputs for Atomic SubSystem: '<S25>/rising_edge1' */

  /* RelationalOperator: '<S25>/Relational Operator4' */
  CtApAEM_rising_edge(rtb_Switch_l >= rtb_MinMax2, &localB->rising_edge1,
                      &localDW->rising_edge1);

  /* End of Outputs for SubSystem: '<S25>/rising_edge1' */

  /* CombinatorialLogic: '<S28>/Logic' incorporates:
   *  Logic: '<S25>/Logical Operator3'
   *  Logic: '<S25>/Logical Operator5'
   *  Memory: '<S28>/Memory'
   *  Switch: '<S25>/Switch2'
   */
  rowIdx = (int32_T)((((((!rtb_RelationalOperator3_m) &&
    localB->rising_edge1.LogicalOperator) || (!!rtb_RelationalOperator3_m)) +
                       ((uint32_T)!rtu_In << 1)) << 1) +
                     localDW->Memory_PreviousInput);
  localB->Logic[0U] = CtApAEM_ConstP.pooled39[(uint32_T)rowIdx];
  localB->Logic[1U] = CtApAEM_ConstP.pooled39[rowIdx + 8U];

  /* Update for UnitDelay: '<S25>/UnitDelay2' */
  localDW->UnitDelay2_DSTATE = rtb_Switch_l;

  /* Update for UnitDelay: '<S25>/UnitDelay3' */
  localDW->UnitDelay3_DSTATE = rtb_RelationalOperator3_m;

  /* Update for Memory: '<S28>/Memory' */
  localDW->Memory_PreviousInput = localB->Logic[0];
}

/*
 * System reset for atomic system:
 *    '<S68>/falling_edge1'
 *    '<S9>/falling_edge'
 */
void CtApAEM_falling_edge1_Reset(DW_falling_edge1_CtApAEM_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<S72>/UnitDelay' */
  localDW->UnitDelay_DSTATE = false;
}

/*
 * Output and update for atomic system:
 *    '<S68>/falling_edge1'
 *    '<S9>/falling_edge'
 */
void CtApAEM_falling_edge1(boolean_T rtu_Signal, B_falling_edge1_CtApAEM_T
  *localB, DW_falling_edge1_CtApAEM_T *localDW)
{
  /* Logic: '<S72>/Logical Operator' incorporates:
   *  Logic: '<S72>/Logical Operator1'
   *  UnitDelay: '<S72>/UnitDelay'
   */
  localB->LogicalOperator = ((!rtu_Signal) && localDW->UnitDelay_DSTATE);

  /* Update for UnitDelay: '<S72>/UnitDelay' */
  localDW->UnitDelay_DSTATE = rtu_Signal;
}

/*
 * Output and update for atomic system:
 *    '<S101>/8Bit Decoder'
 *    '<S101>/8Bit Decoder1'
 *    '<S102>/8Bit Decoder1'
 *    '<S102>/8Bit Decoder2'
 */
void CtApAEM_uBitDecoder(int32_T rtu_U8in, B_uBitDecoder_CtApAEM_T *localB)
{
  real_T rtb_Switch8_ca;

  /* Assertion: '<S103>/Assertion' incorporates:
   *  RelationalOperator: '<S103>/Relational Operator15'
   */
  utAssert(rtu_U8in < 256);

  /* RelationalOperator: '<S103>/Relational Operator8' */
  localB->RelationalOperator8 = (rtu_U8in >= 128);

  /* Switch: '<S103>/Switch8' incorporates:
   *  Constant: '<S103>/Constant8'
   *  Sum: '<S103>/Sum8'
   */
  if (localB->RelationalOperator8) {
    rtb_Switch8_ca = (real_T)rtu_U8in - 128.0;
  } else {
    rtb_Switch8_ca = rtu_U8in;
  }

  /* End of Switch: '<S103>/Switch8' */

  /* RelationalOperator: '<S103>/Relational Operator9' incorporates:
   *  Constant: '<S103>/Constant9'
   */
  localB->RelationalOperator9 = (rtb_Switch8_ca >= 64.0);

  /* Switch: '<S103>/Switch9' incorporates:
   *  Constant: '<S103>/Constant9'
   *  Sum: '<S103>/Sum9'
   */
  if (localB->RelationalOperator9) {
    rtb_Switch8_ca -= 64.0;
  }

  /* End of Switch: '<S103>/Switch9' */

  /* RelationalOperator: '<S103>/Relational Operator10' incorporates:
   *  Constant: '<S103>/Constant10'
   */
  localB->RelationalOperator10 = (rtb_Switch8_ca >= 32.0);

  /* Switch: '<S103>/Switch10' incorporates:
   *  Constant: '<S103>/Constant10'
   *  Sum: '<S103>/Sum10'
   */
  if (localB->RelationalOperator10) {
    rtb_Switch8_ca -= 32.0;
  }

  /* End of Switch: '<S103>/Switch10' */

  /* RelationalOperator: '<S103>/Relational Operator11' incorporates:
   *  Constant: '<S103>/Constant11'
   */
  localB->RelationalOperator11 = (rtb_Switch8_ca >= 16.0);

  /* Switch: '<S103>/Switch11' incorporates:
   *  Constant: '<S103>/Constant11'
   *  Sum: '<S103>/Sum11'
   */
  if (localB->RelationalOperator11) {
    rtb_Switch8_ca -= 16.0;
  }

  /* End of Switch: '<S103>/Switch11' */

  /* RelationalOperator: '<S103>/Relational Operator12' incorporates:
   *  Constant: '<S103>/Constant12'
   */
  localB->RelationalOperator12 = (rtb_Switch8_ca >= 8.0);

  /* Switch: '<S103>/Switch12' incorporates:
   *  Constant: '<S103>/Constant12'
   *  Sum: '<S103>/Sum7'
   */
  if (localB->RelationalOperator12) {
    rtb_Switch8_ca -= 8.0;
  }

  /* End of Switch: '<S103>/Switch12' */

  /* RelationalOperator: '<S103>/Relational Operator13' incorporates:
   *  Constant: '<S103>/Constant13'
   */
  localB->RelationalOperator13 = (rtb_Switch8_ca >= 4.0);

  /* Switch: '<S103>/Switch13' incorporates:
   *  Constant: '<S103>/Constant13'
   *  Sum: '<S103>/Sum13'
   */
  if (localB->RelationalOperator13) {
    rtb_Switch8_ca -= 4.0;
  }

  /* End of Switch: '<S103>/Switch13' */

  /* RelationalOperator: '<S103>/Relational Operator14' incorporates:
   *  Constant: '<S103>/Constant14'
   */
  localB->RelationalOperator14 = (rtb_Switch8_ca >= 2.0);

  /* Switch: '<S103>/Switch14' incorporates:
   *  Constant: '<S103>/Constant14'
   *  Sum: '<S103>/Sum12'
   */
  if (localB->RelationalOperator14) {
    rtb_Switch8_ca -= 2.0;
  }

  /* End of Switch: '<S103>/Switch14' */

  /* DataTypeConversion: '<S103>/Data Type Conversion' */
  localB->DataTypeConversion = (rtb_Switch8_ca != 0.0);
}

/*
 * Output and update for atomic system:
 *    '<S101>/8Bit Encoder'
 *    '<S101>/8Bit Encoder1'
 */
void CtApAEM_uBitEncoder(boolean_T rtu_Bit7, boolean_T rtu_Bit6, boolean_T
  rtu_Bit5, boolean_T rtu_Bit4, boolean_T rtu_Bit3, boolean_T rtu_Bit2,
  boolean_T rtu_Bit1, boolean_T rtu_Bit0, B_uBitEncoder_CtApAEM_T *localB)
{
  /* Sum: '<S105>/Sum' incorporates:
   *  DataTypeConversion: '<S105>/Data Type Conversion10'
   *  DataTypeConversion: '<S105>/Data Type Conversion11'
   *  DataTypeConversion: '<S105>/Data Type Conversion12'
   *  DataTypeConversion: '<S105>/Data Type Conversion13'
   *  DataTypeConversion: '<S105>/Data Type Conversion14'
   *  DataTypeConversion: '<S105>/Data Type Conversion15'
   *  DataTypeConversion: '<S105>/Data Type Conversion8'
   *  DataTypeConversion: '<S105>/Data Type Conversion9'
   *  Gain: '<S105>/Gain10'
   *  Gain: '<S105>/Gain11'
   *  Gain: '<S105>/Gain12'
   *  Gain: '<S105>/Gain13'
   *  Gain: '<S105>/Gain14'
   *  Gain: '<S105>/Gain8'
   *  Gain: '<S105>/Gain9'
   */
  localB->Sum = (((((((rtu_Bit7 << 7) + (rtu_Bit6 << 6)) + (rtu_Bit5 << 5)) +
                    (rtu_Bit4 << 4)) + (rtu_Bit3 << 3)) + (rtu_Bit2 << 2)) +
                 (rtu_Bit1 << 1)) + rtu_Bit0;
}

/*
 * Output and update for atomic system:
 *    '<S108>/Gerer_etat_Reveil_partiel_maitre_Yj'
 *    '<S109>/Gerer_etat_Reveil_partiel_maitre_Yj'
 *    '<S110>/Gerer_etat_Reveil_partiel_maitre_Yj'
 *    '<S111>/Gerer_etat_Reveil_partiel_maitre_Yj'
 *    '<S112>/Gerer_etat_Reveil_partiel_maitre_Yj'
 *    '<S113>/Gerer_etat_Reveil_partiel_maitre_Yj'
 *    '<S114>/Gerer_etat_Reveil_partiel_maitre_Yj'
 *    '<S115>/Gerer_etat_Reveil_partiel_maitre_Yj'
 */
void Gerer_etat_Reveil_partiel_maitr(boolean_T rtu_UCE_bMstPtlWkuYjNd, boolean_T
  rtu_UCE_bMstPtlWkuYjHldReq, boolean_T rtu_UCE_bNomMainWkuAcv, int32_T
  rtu_UCE_stRCDSt, real_T rtu_UCE_tiMinTiMstPtlWkuYj, real_T
  rtu_UCE_tiMaxTiMstPtlWkuYj, B_Gerer_etat_Reveil_partiel_m_T *localB,
  DW_Gerer_etat_Reveil_partiel__T *localDW)
{
  boolean_T rtb_LogicalOperator4_j5;
  real_T rtb_Switch_k;
  real_T tmp;

  /* Outputs for Atomic SubSystem: '<S118>/DetectSat' */

  /* Constant: '<S118>/Constant5' incorporates:
   *  Constant: '<S118>/Constant7'
   */
  CtApAEM_DetectSat(10000.0, rtu_UCE_tiMinTiMstPtlWkuYj, 0.0, &localB->DetectSat);

  /* End of Outputs for SubSystem: '<S118>/DetectSat' */

  /* Logic: '<S116>/Logical Operator4' incorporates:
   *  Constant: '<S116>/PARTIAL_WAKEUP'
   *  RelationalOperator: '<S116>/Relational Operator7'
   *  UnitDelay: '<S116>/UnitDelay1'
   */
  rtb_LogicalOperator4_j5 = (localDW->UnitDelay_e && (rtu_UCE_stRCDSt == 1));

  /* Outputs for Atomic SubSystem: '<S118>/rising_edge' */
  CtApAEM_rising_edge(rtb_LogicalOperator4_j5, &localB->rising_edge,
                      &localDW->rising_edge);

  /* End of Outputs for SubSystem: '<S118>/rising_edge' */

  /* Switch: '<S118>/Switch' incorporates:
   *  Constant: '<S116>/UCE_Sldxxms_PdV1'
   *  Logic: '<S118>/Logical Operator'
   *  Sum: '<S118>/Sum1'
   *  Switch: '<S118>/Switch1'
   *  UnitDelay: '<S118>/UnitDelay1'
   */
  if (localB->rising_edge.LogicalOperator) {
    rtb_Switch_k = 0.04 + localB->DetectSat.MinMax2;
    tmp = 0.04 + localB->DetectSat.MinMax2;
  } else {
    rtb_Switch_k = localDW->UnitDelay1_DSTATE;
    tmp = localDW->UnitDelay1_DSTATE;
  }

  /* End of Switch: '<S118>/Switch' */

  /* Outputs for Atomic SubSystem: '<S117>/DetectSat' */

  /* Constant: '<S117>/Constant5' incorporates:
   *  Constant: '<S117>/Constant7'
   */
  CtApAEM_DetectSat(10000.0, rtu_UCE_tiMaxTiMstPtlWkuYj, 0.0,
                    &localB->DetectSat_h);

  /* End of Outputs for SubSystem: '<S117>/DetectSat' */

  /* Outputs for Atomic SubSystem: '<S117>/rising_edge' */
  CtApAEM_rising_edge(rtb_LogicalOperator4_j5, &localB->rising_edge_i,
                      &localDW->rising_edge_i);

  /* End of Outputs for SubSystem: '<S117>/rising_edge' */

  /* Switch: '<S117>/Switch1' incorporates:
   *  Constant: '<S116>/UCE_Sldxxms_PdV1'
   *  Constant: '<S117>/Constant3'
   *  Constant: '<S118>/Constant1'
   *  Constant: '<S118>/Constant6'
   *  Logic: '<S116>/Logical Operator1'
   *  Logic: '<S116>/Logical Operator2'
   *  Logic: '<S116>/Logical Operator7'
   *  Logic: '<S116>/Logical Operator8'
   *  Logic: '<S117>/Logical Operator2'
   *  RelationalOperator: '<S118>/Relational Operator'
   *  Sum: '<S118>/Sum2'
   *  UnitDelay: '<S116>/UnitDelay2'
   */
  if (!(rtu_UCE_bNomMainWkuAcv || (localDW->UnitDelay && (!((rtb_Switch_k -
           2.2204460492503131E-16) - 0.04 > 0.0)) &&
        (!rtu_UCE_bMstPtlWkuYjHldReq)))) {
    /* Switch: '<S117>/Switch' incorporates:
     *  Constant: '<S116>/UCE_Sldxxms_PdV'
     *  Logic: '<S117>/Logical Operator'
     *  Sum: '<S117>/Sum1'
     *  UnitDelay: '<S117>/UnitDelay1'
     */
    if (localB->rising_edge_i.LogicalOperator) {
      rtb_Switch_k = 0.04 + localB->DetectSat_h.MinMax2;
    } else {
      rtb_Switch_k = localDW->UnitDelay1_DSTATE_a;
    }

    /* End of Switch: '<S117>/Switch' */
  } else {
    rtb_Switch_k = 0.0;
  }

  /* End of Switch: '<S117>/Switch1' */

  /* Outputs for Atomic SubSystem: '<S117>/DetectSat1' */

  /* Sum: '<S117>/Sum2' incorporates:
   *  Constant: '<S116>/UCE_Sldxxms_PdV'
   *  Constant: '<S117>/Constant6'
   *  Constant: '<S117>/Constant8'
   */
  CtApAEM_DetectSat(localB->DetectSat_h.MinMax2, (rtb_Switch_k -
    2.2204460492503131E-16) - 0.04, 0.0, &localB->DetectSat1);

  /* End of Outputs for SubSystem: '<S117>/DetectSat1' */

  /* RelationalOperator: '<S117>/Relational Operator' incorporates:
   *  Constant: '<S116>/UCE_Sldxxms_PdV'
   *  Constant: '<S117>/Constant1'
   *  Constant: '<S117>/Constant6'
   *  Sum: '<S117>/Sum2'
   */
  localB->RelationalOperator = ((rtb_Switch_k - 2.2204460492503131E-16) - 0.04 >
    0.0);

  /* Outputs for Atomic SubSystem: '<S118>/DetectSat1' */

  /* Sum: '<S118>/Sum2' incorporates:
   *  Constant: '<S116>/UCE_Sldxxms_PdV1'
   *  Constant: '<S118>/Constant6'
   *  Constant: '<S118>/Constant8'
   */
  CtApAEM_DetectSat(localB->DetectSat.MinMax2, (tmp - 2.2204460492503131E-16) -
                    0.04, 0.0, &localB->DetectSat1_f);

  /* End of Outputs for SubSystem: '<S118>/DetectSat1' */

  /* Outputs for Atomic SubSystem: '<S116>/rising_edge' */

  /* Logic: '<S116>/Logical Operator6' incorporates:
   *  Logic: '<S116>/Logical Operator5'
   */
  CtApAEM_rising_edge(rtu_UCE_bMstPtlWkuYjNd && (!rtu_UCE_bNomMainWkuAcv),
                      &localB->rising_edge_d, &localDW->rising_edge_d);

  /* End of Outputs for SubSystem: '<S116>/rising_edge' */

  /* Update for UnitDelay: '<S116>/UnitDelay2' */
  localDW->UnitDelay = localB->RelationalOperator;

  /* Update for UnitDelay: '<S116>/UnitDelay1' */
  localDW->UnitDelay_e = localB->rising_edge_d.LogicalOperator;

  /* Update for UnitDelay: '<S118>/UnitDelay1' */
  localDW->UnitDelay1_DSTATE = localB->DetectSat1_f.MinMax2;

  /* Update for UnitDelay: '<S117>/UnitDelay1' */
  localDW->UnitDelay1_DSTATE_a = localB->DetectSat1.MinMax2;
}

/*
 * Output and update for atomic system:
 *    '<S198>/16Bit Decoder'
 *    '<S199>/16Bit Decoder'
 *    '<S199>/16Bit Decoder1'
 */
void CtApAEM_u6BitDecoder(int32_T rtu_U16in, B_u6BitDecoder_CtApAEM_T *localB)
{
  real_T rtb_Switch_i1;

  /* Assertion: '<S200>/Assertion' incorporates:
   *  RelationalOperator: '<S200>/Relational Operator15'
   */
  utAssert(rtu_U16in < 65536);

  /* RelationalOperator: '<S200>/Relational Operator' */
  localB->RelationalOperator = (rtu_U16in >= 32768);

  /* Switch: '<S200>/Switch' incorporates:
   *  Constant: '<S200>/Constant'
   *  Sum: '<S200>/Sum'
   */
  if (localB->RelationalOperator) {
    rtb_Switch_i1 = (real_T)rtu_U16in - 32768.0;
  } else {
    rtb_Switch_i1 = rtu_U16in;
  }

  /* End of Switch: '<S200>/Switch' */

  /* RelationalOperator: '<S200>/Relational Operator1' incorporates:
   *  Constant: '<S200>/Constant1'
   */
  localB->RelationalOperator1 = (rtb_Switch_i1 >= 16384.0);

  /* Switch: '<S200>/Switch1' incorporates:
   *  Constant: '<S200>/Constant1'
   *  Sum: '<S200>/Sum1'
   */
  if (localB->RelationalOperator1) {
    rtb_Switch_i1 -= 16384.0;
  }

  /* End of Switch: '<S200>/Switch1' */

  /* RelationalOperator: '<S200>/Relational Operator2' incorporates:
   *  Constant: '<S200>/Constant2'
   */
  localB->RelationalOperator2 = (rtb_Switch_i1 >= 8192.0);

  /* Switch: '<S200>/Switch2' incorporates:
   *  Constant: '<S200>/Constant2'
   *  Sum: '<S200>/Sum2'
   */
  if (localB->RelationalOperator2) {
    rtb_Switch_i1 -= 8192.0;
  }

  /* End of Switch: '<S200>/Switch2' */

  /* RelationalOperator: '<S200>/Relational Operator3' incorporates:
   *  Constant: '<S200>/Constant3'
   */
  localB->RelationalOperator3 = (rtb_Switch_i1 >= 4096.0);

  /* Switch: '<S200>/Switch3' incorporates:
   *  Constant: '<S200>/Constant3'
   *  Sum: '<S200>/Sum3'
   */
  if (localB->RelationalOperator3) {
    rtb_Switch_i1 -= 4096.0;
  }

  /* End of Switch: '<S200>/Switch3' */

  /* RelationalOperator: '<S200>/Relational Operator4' incorporates:
   *  Constant: '<S200>/Constant4'
   */
  localB->RelationalOperator4 = (rtb_Switch_i1 >= 2048.0);

  /* Switch: '<S200>/Switch4' incorporates:
   *  Constant: '<S200>/Constant4'
   *  Sum: '<S200>/Sum4'
   */
  if (localB->RelationalOperator4) {
    rtb_Switch_i1 -= 2048.0;
  }

  /* End of Switch: '<S200>/Switch4' */

  /* RelationalOperator: '<S200>/Relational Operator5' incorporates:
   *  Constant: '<S200>/Constant5'
   */
  localB->RelationalOperator5 = (rtb_Switch_i1 >= 1024.0);

  /* Switch: '<S200>/Switch5' incorporates:
   *  Constant: '<S200>/Constant5'
   *  Sum: '<S200>/Sum5'
   */
  if (localB->RelationalOperator5) {
    rtb_Switch_i1 -= 1024.0;
  }

  /* End of Switch: '<S200>/Switch5' */

  /* RelationalOperator: '<S200>/Relational Operator6' incorporates:
   *  Constant: '<S200>/Constant6'
   */
  localB->RelationalOperator6 = (rtb_Switch_i1 >= 512.0);

  /* Switch: '<S200>/Switch6' incorporates:
   *  Constant: '<S200>/Constant6'
   *  Sum: '<S200>/Sum6'
   */
  if (localB->RelationalOperator6) {
    rtb_Switch_i1 -= 512.0;
  }

  /* End of Switch: '<S200>/Switch6' */

  /* RelationalOperator: '<S200>/Relational Operator7' incorporates:
   *  Constant: '<S200>/Constant7'
   */
  localB->RelationalOperator7 = (rtb_Switch_i1 >= 256.0);

  /* Switch: '<S200>/Switch7' incorporates:
   *  Constant: '<S200>/Constant7'
   *  Sum: '<S200>/Sum14'
   */
  if (localB->RelationalOperator7) {
    rtb_Switch_i1 -= 256.0;
  }

  /* End of Switch: '<S200>/Switch7' */

  /* RelationalOperator: '<S200>/Relational Operator8' incorporates:
   *  Constant: '<S200>/Constant8'
   */
  localB->RelationalOperator8 = (rtb_Switch_i1 >= 128.0);

  /* Switch: '<S200>/Switch8' incorporates:
   *  Constant: '<S200>/Constant8'
   *  Sum: '<S200>/Sum8'
   */
  if (localB->RelationalOperator8) {
    rtb_Switch_i1 -= 128.0;
  }

  /* End of Switch: '<S200>/Switch8' */

  /* RelationalOperator: '<S200>/Relational Operator9' incorporates:
   *  Constant: '<S200>/Constant9'
   */
  localB->RelationalOperator9 = (rtb_Switch_i1 >= 64.0);

  /* Switch: '<S200>/Switch9' incorporates:
   *  Constant: '<S200>/Constant9'
   *  Sum: '<S200>/Sum9'
   */
  if (localB->RelationalOperator9) {
    rtb_Switch_i1 -= 64.0;
  }

  /* End of Switch: '<S200>/Switch9' */

  /* RelationalOperator: '<S200>/Relational Operator10' incorporates:
   *  Constant: '<S200>/Constant10'
   */
  localB->RelationalOperator10 = (rtb_Switch_i1 >= 32.0);

  /* Switch: '<S200>/Switch10' incorporates:
   *  Constant: '<S200>/Constant10'
   *  Sum: '<S200>/Sum10'
   */
  if (localB->RelationalOperator10) {
    rtb_Switch_i1 -= 32.0;
  }

  /* End of Switch: '<S200>/Switch10' */

  /* RelationalOperator: '<S200>/Relational Operator11' incorporates:
   *  Constant: '<S200>/Constant11'
   */
  localB->RelationalOperator11 = (rtb_Switch_i1 >= 16.0);

  /* Switch: '<S200>/Switch11' incorporates:
   *  Constant: '<S200>/Constant11'
   *  Sum: '<S200>/Sum11'
   */
  if (localB->RelationalOperator11) {
    rtb_Switch_i1 -= 16.0;
  }

  /* End of Switch: '<S200>/Switch11' */

  /* RelationalOperator: '<S200>/Relational Operator12' incorporates:
   *  Constant: '<S200>/Constant12'
   */
  localB->RelationalOperator12 = (rtb_Switch_i1 >= 8.0);

  /* Switch: '<S200>/Switch12' incorporates:
   *  Constant: '<S200>/Constant12'
   *  Sum: '<S200>/Sum7'
   */
  if (localB->RelationalOperator12) {
    rtb_Switch_i1 -= 8.0;
  }

  /* End of Switch: '<S200>/Switch12' */

  /* RelationalOperator: '<S200>/Relational Operator13' incorporates:
   *  Constant: '<S200>/Constant13'
   */
  localB->RelationalOperator13 = (rtb_Switch_i1 >= 4.0);

  /* Switch: '<S200>/Switch13' incorporates:
   *  Constant: '<S200>/Constant13'
   *  Sum: '<S200>/Sum13'
   */
  if (localB->RelationalOperator13) {
    rtb_Switch_i1 -= 4.0;
  }

  /* End of Switch: '<S200>/Switch13' */

  /* RelationalOperator: '<S200>/Relational Operator14' incorporates:
   *  Constant: '<S200>/Constant14'
   */
  localB->RelationalOperator14 = (rtb_Switch_i1 >= 2.0);

  /* Switch: '<S200>/Switch14' incorporates:
   *  Constant: '<S200>/Constant14'
   *  Sum: '<S200>/Sum12'
   */
  if (localB->RelationalOperator14) {
    rtb_Switch_i1 -= 2.0;
  }

  /* End of Switch: '<S200>/Switch14' */

  /* DataTypeConversion: '<S200>/Data Type Conversion' */
  localB->DataTypeConversion = (rtb_Switch_i1 != 0.0);
}

/*
 * Output and update for atomic system:
 *    '<S198>/16Bit Encoder'
 *    '<S198>/16Bit Encoder1'
 */
void CtApAEM_u6BitEncoder(boolean_T rtu_Bit15, boolean_T rtu_Bit14, boolean_T
  rtu_Bit13, boolean_T rtu_Bit12, boolean_T rtu_Bit11, boolean_T rtu_Bit10,
  boolean_T rtu_Bit9, boolean_T rtu_Bit8, boolean_T rtu_Bit7, boolean_T rtu_Bit6,
  boolean_T rtu_Bit5, boolean_T rtu_Bit4, boolean_T rtu_Bit3, boolean_T rtu_Bit2,
  boolean_T rtu_Bit1, boolean_T rtu_Bit0, B_u6BitEncoder_CtApAEM_T *localB)
{
  /* Sum: '<S201>/Sum' incorporates:
   *  DataTypeConversion: '<S201>/Data Type Conversion'
   *  DataTypeConversion: '<S201>/Data Type Conversion1'
   *  DataTypeConversion: '<S201>/Data Type Conversion10'
   *  DataTypeConversion: '<S201>/Data Type Conversion11'
   *  DataTypeConversion: '<S201>/Data Type Conversion12'
   *  DataTypeConversion: '<S201>/Data Type Conversion13'
   *  DataTypeConversion: '<S201>/Data Type Conversion14'
   *  DataTypeConversion: '<S201>/Data Type Conversion15'
   *  DataTypeConversion: '<S201>/Data Type Conversion2'
   *  DataTypeConversion: '<S201>/Data Type Conversion3'
   *  DataTypeConversion: '<S201>/Data Type Conversion4'
   *  DataTypeConversion: '<S201>/Data Type Conversion5'
   *  DataTypeConversion: '<S201>/Data Type Conversion6'
   *  DataTypeConversion: '<S201>/Data Type Conversion7'
   *  DataTypeConversion: '<S201>/Data Type Conversion8'
   *  DataTypeConversion: '<S201>/Data Type Conversion9'
   *  Gain: '<S201>/Gain'
   *  Gain: '<S201>/Gain1'
   *  Gain: '<S201>/Gain10'
   *  Gain: '<S201>/Gain11'
   *  Gain: '<S201>/Gain12'
   *  Gain: '<S201>/Gain13'
   *  Gain: '<S201>/Gain14'
   *  Gain: '<S201>/Gain2'
   *  Gain: '<S201>/Gain3'
   *  Gain: '<S201>/Gain4'
   *  Gain: '<S201>/Gain5'
   *  Gain: '<S201>/Gain6'
   *  Gain: '<S201>/Gain7'
   *  Gain: '<S201>/Gain8'
   *  Gain: '<S201>/Gain9'
   */
  localB->Sum = (((((((((((((((rtu_Bit15 << 15) + (rtu_Bit14 << 14)) +
    (rtu_Bit13 << 13)) + (rtu_Bit12 << 12)) + (rtu_Bit11 << 11)) + (rtu_Bit10 <<
    10)) + (rtu_Bit9 << 9)) + (rtu_Bit8 << 8)) + (rtu_Bit7 << 7)) + (rtu_Bit6 <<
    6)) + (rtu_Bit5 << 5)) + (rtu_Bit4 << 4)) + (rtu_Bit3 << 3)) + (rtu_Bit2 <<
    2)) + (rtu_Bit1 << 1)) + rtu_Bit0;
}

/*
 * System initialize for atomic system:
 *    '<S204>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S205>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S206>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S207>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S208>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S209>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S210>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S211>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S212>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S213>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    ...
 */
void Gerer_etat_Reveil_partie_b_Init(DW_Gerer_etat_Reveil_partie_i_T *localDW)
{
  /* SystemInitialize for Atomic SubSystem: '<S221>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&localDW->TurnOnDelay);

  /* End of SystemInitialize for SubSystem: '<S221>/TurnOnDelay' */

  /* SystemInitialize for Atomic SubSystem: '<S223>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&localDW->TurnOnDelay_k);

  /* End of SystemInitialize for SubSystem: '<S223>/TurnOnDelay' */
}

/*
 * Output and update for atomic system:
 *    '<S204>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S205>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S206>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S207>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S208>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S209>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S210>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S211>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S212>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S213>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    ...
 */
void Gerer_etat_Reveil_partiel_escla(boolean_T rtu_UCE_bSlavePtlWkuXiReq,
  boolean_T rtu_Ext_bRCDLine, int32_T rtu_UCE_stRCDSt, boolean_T
  rtu_UCE_bNomMainWkuAcv, boolean_T rtu_UCE_bSlavePtlWkuXiAcvMod, real_T
  rtu_UCE_tiPtlWkuXiAcv, real_T rtu_UCE_tiPtlWkuXiDeac, real_T
  rtu_UCE_tiPtlWkuXiLock, B_Gerer_etat_Reveil_partiel_e_T *localB,
  DW_Gerer_etat_Reveil_partie_i_T *localDW)
{
  real_T tmp;

  /* Outputs for Atomic SubSystem: '<S221>/TurnOnDelay' */

  /* Logic: '<S221>/Logical Operator12' incorporates:
   *  Constant: '<S221>/PARTIAL_WAKEUP'
   *  Constant: '<S221>/TRANSITORY_STATE'
   *  Constant: '<S221>/UCE_Sldxxms_PdV'
   *  Logic: '<S221>/Logical Operator1'
   *  Logic: '<S221>/Logical Operator2'
   *  Logic: '<S221>/Logical Operator3'
   *  Logic: '<S221>/Logical Operator4'
   *  RelationalOperator: '<S221>/Relational Operator1'
   *  RelationalOperator: '<S221>/Relational Operator2'
   *  RelationalOperator: '<S221>/Relational Operator3'
   *  RelationalOperator: '<S221>/Relational Operator7'
   */
  CtApAEM_TurnOnDelay1((rtu_Ext_bRCDLine || rtu_UCE_bSlavePtlWkuXiAcvMod) &&
                       rtu_UCE_bSlavePtlWkuXiReq && (((rtu_UCE_stRCDSt == 1) ||
    (rtu_UCE_stRCDSt == 3)) && (!rtu_UCE_bNomMainWkuAcv)), rtu_UCE_tiPtlWkuXiAcv,
                       0.04, &localB->TurnOnDelay, &localDW->TurnOnDelay);

  /* End of Outputs for SubSystem: '<S221>/TurnOnDelay' */

  /* Outputs for Atomic SubSystem: '<S223>/TurnOnDelay' */

  /* Logic: '<S223>/Logical Operator5' incorporates:
   *  Constant: '<S223>/PARTIAL_WAKEUP'
   *  Constant: '<S223>/UCE_Sldxxms_PdV'
   *  RelationalOperator: '<S223>/Relational Operator4'
   *  RelationalOperator: '<S223>/Relational Operator5'
   */
  CtApAEM_TurnOnDelay1((!rtu_UCE_bSlavePtlWkuXiReq) && (rtu_UCE_stRCDSt == 1),
                       rtu_UCE_tiPtlWkuXiDeac, 0.04, &localB->TurnOnDelay_k,
                       &localDW->TurnOnDelay_k);

  /* End of Outputs for SubSystem: '<S223>/TurnOnDelay' */

  /* Outputs for Atomic SubSystem: '<S229>/DetectSat' */

  /* Constant: '<S229>/Constant5' incorporates:
   *  Constant: '<S229>/Constant7'
   */
  CtApAEM_DetectSat(10000.0, rtu_UCE_tiPtlWkuXiLock, 0.0, &localB->DetectSat);

  /* End of Outputs for SubSystem: '<S229>/DetectSat' */

  /* Outputs for Atomic SubSystem: '<S229>/rising_edge' */

  /* Logic: '<S222>/Logical Operator2' incorporates:
   *  UnitDelay: '<S222>/UnitDelay'
   */
  CtApAEM_rising_edge(localB->TurnOnDelay.Logic[1] && (!localDW->UnitDelay),
                      &localB->rising_edge, &localDW->rising_edge);

  /* End of Outputs for SubSystem: '<S229>/rising_edge' */

  /* Switch: '<S229>/Switch1' incorporates:
   *  Constant: '<S229>/Constant3'
   *  Logic: '<S222>/Logical Operator1'
   *  Logic: '<S229>/Logical Operator2'
   */
  if (!(localB->TurnOnDelay_k.Logic[1] || rtu_UCE_bNomMainWkuAcv)) {
    /* Switch: '<S229>/Switch' incorporates:
     *  Constant: '<S222>/UCE_Sldxxms_PdV'
     *  Logic: '<S229>/Logical Operator'
     *  Sum: '<S229>/Sum1'
     *  UnitDelay: '<S229>/UnitDelay1'
     */
    if (localB->rising_edge.LogicalOperator) {
      tmp = 0.04 + localB->DetectSat.MinMax2;
    } else {
      tmp = localDW->UnitDelay1_DSTATE;
    }

    /* End of Switch: '<S229>/Switch' */
  } else {
    tmp = 0.0;
  }

  /* End of Switch: '<S229>/Switch1' */

  /* Outputs for Atomic SubSystem: '<S229>/DetectSat1' */

  /* Sum: '<S229>/Sum2' incorporates:
   *  Constant: '<S222>/UCE_Sldxxms_PdV'
   *  Constant: '<S229>/Constant6'
   *  Constant: '<S229>/Constant8'
   */
  CtApAEM_DetectSat(localB->DetectSat.MinMax2, (tmp - 2.2204460492503131E-16) -
                    0.04, 0.0, &localB->DetectSat1);

  /* End of Outputs for SubSystem: '<S229>/DetectSat1' */

  /* RelationalOperator: '<S229>/Relational Operator' incorporates:
   *  Constant: '<S222>/UCE_Sldxxms_PdV'
   *  Constant: '<S229>/Constant1'
   *  Constant: '<S229>/Constant6'
   *  Sum: '<S229>/Sum2'
   */
  localB->RelationalOperator = ((tmp - 2.2204460492503131E-16) - 0.04 > 0.0);

  /* Update for UnitDelay: '<S222>/UnitDelay' */
  localDW->UnitDelay = localB->RelationalOperator;

  /* Update for UnitDelay: '<S229>/UnitDelay1' */
  localDW->UnitDelay1_DSTATE = localB->DetectSat1.MinMax2;
}

/* Model step function for TID1 */
void RCtApAEM_task10msA(void)          /* Sample time: [0.01s, 0.0s] */
{
  /* RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msA_at_outport_1' incorporates:
   *  SubSystem: '<Root>/RCtApAEM_task10msA_sys'
   */
  /* SignalConversion: '<S1>/OutportBufferForPpAppRCDECUState_DeAppRCDECUState_write' */
  CtApAEM_B.OutportBufferForPpAppRCDECUStat = APP_STATE_0;

  /* End of Outputs for RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msA_at_outport_1' */

  /* Outport: '<Root>/PpAppRCDECUState_DeAppRCDECUState' */
  Rte_Write_PpAppRCDECUState_DeAppRCDECUState
    (CtApAEM_B.OutportBufferForPpAppRCDECUStat);
}

/* Model step function for TID2 */
void RCtApAEM_task10msB(void)          /* Sample time: [0.01s, 0.0s] */
{
  int32_T rowIdx;
  boolean_T tmp;
  boolean_T tmp_0;
  boolean_T tmp_1;
  ABS_VehSpd tmp_2;
  BSI_MainWakeup tmp_3;
  BSI_PostDriveWakeup tmp_4;
  BSI_PreDriveWakeup tmp_5;
  VCU_PrecondElecWakeup tmp_6;
  VCU_StopDelayedHMIWakeup tmp_7;
  int32_T rtb_DataTypeConversion;
  boolean_T rtb_LogicalOperator1;
  boolean_T rtb_Switch2_ab;
  boolean_T rtb_LogicalOperator12_jr;
  boolean_T rtb_LogicalOperator5_mj;
  boolean_T rtb_LogicalOperator;
  boolean_T rtb_LogicalOperator1_f;
  boolean_T rtb_LogicalOperator4;
  boolean_T rtb_LogicalOperator12_l;
  boolean_T rtb_RelationalOperator1_e0;
  real_T rtb_Switch2;

  /* Inport: '<Root>/PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP' */
  Rte_Read_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP(&rtb_LogicalOperator1_f);

  /* Inport: '<Root>/PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup' */
  Rte_Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(&tmp_7);

  /* Inport: '<Root>/PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup' */
  Rte_Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(&tmp_6);

  /* Inport: '<Root>/PpInt_SUPV_RCDLineState_SUPV_RCDLineState' */
  Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(&rtb_LogicalOperator1);

  /* Inport: '<Root>/PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup' */
  Rte_Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(&tmp_5);

  /* Inport: '<Root>/PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup' */
  Rte_Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(&tmp_4);

  /* Inport: '<Root>/PpInt_BSI_MainWakeup_BSI_MainWakeup' */
  Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(&tmp_3);

  /* Inport: '<Root>/PpInt_ABS_VehSpd_ABS_VehSpd' */
  Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(&tmp_2);

  /* Inport: '<Root>/PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP' */
  Rte_Read_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP(&tmp_1);

  /* Inport: '<Root>/PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP' */
  Rte_Read_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP(&tmp_0);

  /* Inport: '<Root>/PpCOOLING_WAKEUP_DeCOOLING_WAKEUP' */
  Rte_Read_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP(&tmp);

  /* RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' incorporates:
   *  SubSystem: '<Root>/RCtApAEM_task10msB_sys'
   */
  /* DataTypeConversion: '<S4>/Data Type Conversion' */
  rtb_DataTypeConversion = tmp_3;

  /* End of Outputs for RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' */

  /* Inport: '<Root>/PpElectronicIntegrationRequest_DeElectronicIntegrationRequest' */
  Rte_Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest
    (&rtb_LogicalOperator12_l);

  /* Inport: '<Root>/PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC' */
  Rte_Read_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC(&rtb_LogicalOperator4);

  /* RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' incorporates:
   *  SubSystem: '<Root>/RCtApAEM_task10msB_sys'
   */
  /* Outputs for Enabled SubSystem: '<S2>/F0_Gerer_PdV_EE_generique' incorporates:
   *  EnablePort: '<S3>/Enable'
   */
  if (!CtApAEM_DW.F0_Gerer_PdV_EE_generique_MODE) {
    /* SystemReset for Atomic SubSystem: '<S9>/falling_edge' */
    CtApAEM_falling_edge1_Reset(&CtApAEM_DW.falling_edge);

    /* End of SystemReset for SubSystem: '<S9>/falling_edge' */

    /* SystemReset for Atomic SubSystem: '<S9>/BasculeRS' */
    /* InitializeConditions for UnitDelay: '<S620>/UnitDelay' */
    CtApAEM_DW.UnitDelay_DSTATE = false;

    /* End of SystemReset for SubSystem: '<S9>/BasculeRS' */
    CtApAEM_DW.F0_Gerer_PdV_EE_generique_MODE = true;
  }

  /* Outputs for Enabled SubSystem: '<S3>/F01_Gerer_PdV_RCD' incorporates:
   *  EnablePort: '<S6>/Enable'
   */
  /* Constant: '<S8>/UCE_noUCETyp_C' */
  if (Rte_CData_UCE_noUCETyp_C()) {
    if (!CtApAEM_DW.F01_Gerer_PdV_RCD_MODE) {
      /* InitializeConditions for UnitDelay: '<S6>/Unit Delay1' */
      CtApAEM_DW.UnitDelay1_DSTATE_o = 0;

      /* SystemReset for Atomic SubSystem: '<S18>/TurnOnDelay1' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay1);

      /* End of SystemReset for SubSystem: '<S18>/TurnOnDelay1' */

      /* SystemReset for Atomic SubSystem: '<S18>/TurnOnDelay2' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay2);

      /* End of SystemReset for SubSystem: '<S18>/TurnOnDelay2' */

      /* SystemReset for Atomic SubSystem: '<S19>/TurnOnDelay1' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay1_c);

      /* End of SystemReset for SubSystem: '<S19>/TurnOnDelay1' */

      /* SystemReset for Atomic SubSystem: '<S19>/TurnOnDelay2' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay2_k);

      /* End of SystemReset for SubSystem: '<S19>/TurnOnDelay2' */

      /* SystemReset for Atomic SubSystem: '<S19>/TurnOnDelay3' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay3);

      /* End of SystemReset for SubSystem: '<S19>/TurnOnDelay3' */

      /* SystemReset for Atomic SubSystem: '<S20>/TurnOnDelay' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay);

      /* End of SystemReset for SubSystem: '<S20>/TurnOnDelay' */

      /* SystemReset for Atomic SubSystem: '<S21>/TurnOnDelay' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay_g);

      /* End of SystemReset for SubSystem: '<S21>/TurnOnDelay' */

      /* SystemReset for Atomic SubSystem: '<S22>/TurnOnDelay' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay_a);

      /* End of SystemReset for SubSystem: '<S22>/TurnOnDelay' */

      /* SystemReset for Atomic SubSystem: '<S65>/TurnOffDelay' */
      /* InitializeConditions for UnitDelay: '<S68>/UnitDelay1' */
      CtApAEM_DW.UnitDelay1_DSTATE_i = 0.0;

      /* InitializeConditions for UnitDelay: '<S68>/UnitDelay2' */
      CtApAEM_DW.UnitDelay2_DSTATE_o = false;

      /* InitializeConditions for Memory: '<S71>/Memory' */
      CtApAEM_DW.Memory_PreviousInput = false;

      /* SystemReset for Atomic SubSystem: '<S68>/falling_edge1' */
      CtApAEM_falling_edge1_Reset(&CtApAEM_DW.falling_edge1);

      /* End of SystemReset for SubSystem: '<S68>/falling_edge1' */

      /* SystemReset for Atomic SubSystem: '<S68>/rising_edge' */
      CtApAEM_rising_edge_Reset(&CtApAEM_DW.rising_edge);

      /* End of SystemReset for SubSystem: '<S68>/rising_edge' */

      /* End of SystemReset for SubSystem: '<S65>/TurnOffDelay' */

      /* SystemReset for Atomic SubSystem: '<S65>/TurnOnDelay1' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay1_h);

      /* End of SystemReset for SubSystem: '<S65>/TurnOnDelay1' */

      /* SystemReset for Chart: '<S66>/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD' */
      CtApAEM_DW.is_active_c1_CtApAEM = 0U;
      CtApAEM_DW.is_c1_CtApAEM = CtApAEM_IN_NO_ACTIVE_CHILD;
      CtApAEM_B.UCE_bDgoMainWkuDisrd = false;

      /* SystemReset for Atomic SubSystem: '<S79>/TurnOnDelay1' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay1_m);

      /* End of SystemReset for SubSystem: '<S79>/TurnOnDelay1' */

      /* SystemReset for Atomic SubSystem: '<S79>/TurnOnDelay2' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay2_e);

      /* End of SystemReset for SubSystem: '<S79>/TurnOnDelay2' */

      /* SystemReset for Atomic SubSystem: '<S79>/TurnOnDelay3' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay3_o);

      /* End of SystemReset for SubSystem: '<S79>/TurnOnDelay3' */

      /* SystemReset for Chart: '<S80>/F01_01_07_02_01_Automate_Incoherence_Reveil_principal_pour_GD' */
      CtApAEM_DW.is_active_c2_CtApAEM = 0U;
      CtApAEM_DW.is_c2_CtApAEM = CtApAEM_IN_NO_ACTIVE_CHILD;
      CtApAEM_B.UCE_bDgoMainWkuIncst = false;

      /* SystemReset for Atomic SubSystem: '<S12>/TurnOnDelay' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay_i);

      /* End of SystemReset for SubSystem: '<S12>/TurnOnDelay' */

      /* SystemReset for Atomic SubSystem: '<S13>/TurnOnDelay' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay_j);

      /* End of SystemReset for SubSystem: '<S13>/TurnOnDelay' */

      /* SystemReset for Chart: '<S14>/F01_05_01_Machine_etats_RCD' */
      CtApAEM_DW.is_active_c15_CtApAEM = 0U;
      CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_NO_ACTIVE_CHILD;
      CtApAEM_B.UCE_stRCDSt = 3;

      /* SystemReset for Atomic SubSystem: '<S17>/TurnOnDelay1' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay1_d);

      /* End of SystemReset for SubSystem: '<S17>/TurnOnDelay1' */

      /* SystemReset for Atomic SubSystem: '<S17>/TurnOnDelay2' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay2_g);

      /* End of SystemReset for SubSystem: '<S17>/TurnOnDelay2' */
      CtApAEM_DW.F01_Gerer_PdV_RCD_MODE = true;
    }

    /* Inport: '<Root>/PpDiagToolsRequest_DeDiagToolsRequest' */
    Rte_Read_PpDiagToolsRequest_DeDiagToolsRequest(&rtb_LogicalOperator5_mj);

    /* Logic: '<S18>/Logical Operator1' incorporates:
     *  Constant: '<S18>/PARTIAL_WAKEUP'
     *  Constant: '<S18>/TRANSITORY_STATE'
     *  RelationalOperator: '<S18>/Relational Operator2'
     *  RelationalOperator: '<S18>/Relational Operator5'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    rtb_Switch2_ab = ((CtApAEM_DW.UnitDelay1_DSTATE_o == 1) ||
                      (CtApAEM_DW.UnitDelay1_DSTATE_o == 3));

    /* Logic: '<S18>/Logical Operator12' incorporates:
     *  Constant: '<S18>/INAVLID_MAIN_WKU_REQ'
     *  RelationalOperator: '<S18>/Relational Operator3'
     *  RelationalOperator: '<S627>/Relational Operator'
     */
    rtb_LogicalOperator12_jr = ((rtb_DataTypeConversion == 0) &&
      rtb_LogicalOperator1 && rtb_Switch2_ab);

    /* Outputs for Atomic SubSystem: '<S18>/TurnOnDelay1' */

    /* Logic: '<S18>/Logical Operator2' incorporates:
     *  Constant: '<S18>/ACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S18>/UCE_Sldxxms_PdV'
     *  Constant: '<S18>/UCE_tiMainWkuAcv_C'
     *  RelationalOperator: '<S18>/Relational Operator4'
     */
    CtApAEM_TurnOnDelay1((rtb_DataTypeConversion == 2) && rtb_Switch2_ab,
                         (real_T)Rte_CData_UCE_tiMainWkuAcv_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay1, &CtApAEM_DW.TurnOnDelay1);

    /* End of Outputs for SubSystem: '<S18>/TurnOnDelay1' */

    /* Outputs for Atomic SubSystem: '<S18>/TurnOnDelay2' */

    /* Constant: '<S18>/UCE_tiMainTransForc_C' incorporates:
     *  Constant: '<S18>/UCE_Sldxxms_PdV'
     */
    CtApAEM_TurnOnDelay1(rtb_LogicalOperator12_jr, (real_T)
                         Rte_CData_UCE_tiMainTransForc_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay2, &CtApAEM_DW.TurnOnDelay2);

    /* End of Outputs for SubSystem: '<S18>/TurnOnDelay2' */

    /* Logic: '<S18>/Logical Operator5' incorporates:
     *  Logic: '<S18>/Logical Operator4'
     */
    rtb_LogicalOperator5_mj = (CtApAEM_B.TurnOnDelay1.Logic[1] ||
      CtApAEM_B.TurnOnDelay2.Logic[1] || (rtb_LogicalOperator12_jr &&
      rtb_LogicalOperator5_mj));

    /* RelationalOperator: '<S19>/Relational Operator2' incorporates:
     *  Constant: '<S19>/NOMINAL_MAIN_WAKEUP'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    rtb_Switch2_ab = (CtApAEM_DW.UnitDelay1_DSTATE_o == 4);

    /* Outputs for Atomic SubSystem: '<S19>/TurnOnDelay1' */

    /* Logic: '<S19>/Logical Operator4' incorporates:
     *  Constant: '<S19>/INVALID_MAIN_WKU_REQ'
     *  Constant: '<S19>/UCE_Sldxxms_PdV'
     *  Constant: '<S19>/UCE_tiMainDisrdDet_C'
     *  RelationalOperator: '<S19>/Relational Operator4'
     */
    CtApAEM_TurnOnDelay1((rtb_DataTypeConversion == 0) && rtb_Switch2_ab,
                         (real_T)Rte_CData_UCE_tiMainDisrdDet_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay1_c, &CtApAEM_DW.TurnOnDelay1_c);

    /* End of Outputs for SubSystem: '<S19>/TurnOnDelay1' */

    /* Outputs for Atomic SubSystem: '<S19>/TurnOnDelay2' */

    /* Logic: '<S19>/Logical Operator12' incorporates:
     *  Constant: '<S19>/ACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S19>/UCE_Sldxxms_PdV'
     *  Constant: '<S19>/UCE_tiMainIncstDet_C'
     *  RelationalOperator: '<S19>/Relational Operator3'
     *  RelationalOperator: '<S627>/Relational Operator'
     */
    CtApAEM_TurnOnDelay1((rtb_DataTypeConversion == 2) && (!rtb_LogicalOperator1)
                         && rtb_Switch2_ab, (real_T)
                         Rte_CData_UCE_tiMainIncstDet_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay2_k, &CtApAEM_DW.TurnOnDelay2_k);

    /* End of Outputs for SubSystem: '<S19>/TurnOnDelay2' */

    /* Outputs for Atomic SubSystem: '<S19>/TurnOnDelay3' */

    /* Logic: '<S19>/Logical Operator2' incorporates:
     *  Constant: '<S19>/INACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S19>/UCE_Sldxxms_PdV'
     *  Constant: '<S19>/UCE_spdThdNomDeac_C'
     *  Constant: '<S19>/UCE_tiMainIncstDet_C'
     *  DataTypeConversion: '<S4>/Data Type Conversion1'
     *  RelationalOperator: '<S19>/Relational Operator5'
     *  RelationalOperator: '<S19>/Relational Operator6'
     */
    CtApAEM_TurnOnDelay1(rtb_Switch2_ab && (rtb_DataTypeConversion == 1) &&
                         (tmp_2 >= Rte_CData_UCE_spdThdNomDeac_C()), (real_T)
                         Rte_CData_UCE_tiMainIncstDet_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay3, &CtApAEM_DW.TurnOnDelay3);

    /* End of Outputs for SubSystem: '<S19>/TurnOnDelay3' */

    /* Logic: '<S19>/Logical Operator3' */
    rtb_Switch2_ab = (CtApAEM_B.TurnOnDelay1_c.Logic[1] ||
                      CtApAEM_B.TurnOnDelay2_k.Logic[1] ||
                      CtApAEM_B.TurnOnDelay3.Logic[1]);

    /* Outputs for Atomic SubSystem: '<S20>/TurnOnDelay' */

    /* Logic: '<S20>/Logical Operator12' incorporates:
     *  Constant: '<S20>/ACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S20>/DEGRADED_MAIN_WAKEUP'
     *  Constant: '<S20>/UCE_Sldxxms_PdV'
     *  Constant: '<S20>/UCE_tiMainWkuReh_C'
     *  RelationalOperator: '<S20>/Relational Operator2'
     *  RelationalOperator: '<S20>/Relational Operator3'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    CtApAEM_TurnOnDelay1((rtb_DataTypeConversion == 2) &&
                         (CtApAEM_DW.UnitDelay1_DSTATE_o == 5) &&
                         rtb_LogicalOperator1, (real_T)
                         Rte_CData_UCE_tiMainWkuReh_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay, &CtApAEM_DW.TurnOnDelay);

    /* End of Outputs for SubSystem: '<S20>/TurnOnDelay' */

    /* Outputs for Atomic SubSystem: '<S21>/TurnOnDelay' */

    /* Logic: '<S21>/Logical Operator2' incorporates:
     *  Constant: '<S21>/INACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S21>/INVALID_SPEED'
     *  Constant: '<S21>/NOMINAL_MAIN_WAKEUP'
     *  Constant: '<S21>/UCE_Sldxxms_PdV'
     *  Constant: '<S21>/UCE_spdThdNomDeac_C'
     *  Constant: '<S21>/UCE_tiNomMainWkuDeac_C'
     *  DataTypeConversion: '<S4>/Data Type Conversion1'
     *  Logic: '<S21>/Logical Operator1'
     *  RelationalOperator: '<S21>/Relational Operator2'
     *  RelationalOperator: '<S21>/Relational Operator3'
     *  RelationalOperator: '<S21>/Relational Operator4'
     *  RelationalOperator: '<S21>/Relational Operator5'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    CtApAEM_TurnOnDelay1((rtb_DataTypeConversion == 1) && ((tmp_2 <
      Rte_CData_UCE_spdThdNomDeac_C()) || (tmp_2 == 255)) &&
                         (CtApAEM_DW.UnitDelay1_DSTATE_o == 4), (real_T)
                         Rte_CData_UCE_tiNomMainWkuDeac_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay_g, &CtApAEM_DW.TurnOnDelay_g);

    /* End of Outputs for SubSystem: '<S21>/TurnOnDelay' */

    /* Outputs for Atomic SubSystem: '<S22>/TurnOnDelay' */

    /* Logic: '<S22>/Logical Operator2' incorporates:
     *  Constant: '<S22>/DEGRADED_MAIN_WAKEUP'
     *  Constant: '<S22>/INACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S22>/INVALID_MAIN_WKU_REQ'
     *  Constant: '<S22>/INVALID_SPEED'
     *  Constant: '<S22>/UCE_Sldxxms_PdV'
     *  Constant: '<S22>/UCE_spdThdDegDeac_C'
     *  Constant: '<S22>/UCE_tiDegMainWkuDeac_C'
     *  DataTypeConversion: '<S4>/Data Type Conversion1'
     *  Logic: '<S22>/Logical Operator1'
     *  Logic: '<S22>/Logical Operator12'
     *  RelationalOperator: '<S22>/Relational Operator1'
     *  RelationalOperator: '<S22>/Relational Operator2'
     *  RelationalOperator: '<S22>/Relational Operator3'
     *  RelationalOperator: '<S22>/Relational Operator4'
     *  RelationalOperator: '<S22>/Relational Operator5'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    CtApAEM_TurnOnDelay1(((rtb_DataTypeConversion == 0) ||
                          (rtb_DataTypeConversion == 1)) &&
                         (!rtb_LogicalOperator1) && ((tmp_2 <
      Rte_CData_UCE_spdThdDegDeac_C()) || (tmp_2 == 255)) &&
                         (CtApAEM_DW.UnitDelay1_DSTATE_o == 5), (real_T)
                         Rte_CData_UCE_tiDegMainWkuDeac_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay_a, &CtApAEM_DW.TurnOnDelay_a);

    /* End of Outputs for SubSystem: '<S22>/TurnOnDelay' */

    /* RelationalOperator: '<S65>/Relational Operator4' incorporates:
     *  Constant: '<S65>/INVALID_MAIN_WKU_REQ'
     */
    rtb_LogicalOperator12_jr = (rtb_DataTypeConversion == 0);

    /* Logic: '<S65>/Logical Operator' */
    rtb_LogicalOperator = !rtb_LogicalOperator12_jr;

    /* Outputs for Atomic SubSystem: '<S65>/TurnOffDelay' */
    /* RelationalOperator: '<S68>/Relational Operator1' incorporates:
     *  Constant: '<S65>/UCE_Sldxxms_PdV'
     *  Constant: '<S65>/UCE_tiMainWkuReh_C'
     */
    rtb_RelationalOperator1_e0 = ((real_T)Rte_CData_UCE_tiMainWkuReh_C() < 0.04);

    /* Outputs for Atomic SubSystem: '<S68>/falling_edge1' */
    CtApAEM_falling_edge1(rtb_LogicalOperator, &CtApAEM_B.falling_edge1,
                          &CtApAEM_DW.falling_edge1);

    /* End of Outputs for SubSystem: '<S68>/falling_edge1' */

    /* Switch: '<S68>/Switch2' incorporates:
     *  Constant: '<S65>/UCE_Sldxxms_PdV'
     *  Constant: '<S65>/UCE_tiMainWkuReh_C'
     *  Constant: '<S68>/Constant3'
     *  Constant: '<S68>/Constant4'
     *  Constant: '<S68>/Constant6'
     *  Constant: '<S68>/Constant7'
     *  Logic: '<S68>/Logical Operator2'
     *  Product: '<S68>/Divide2'
     *  Sum: '<S68>/Sum1'
     *  Sum: '<S68>/Sum3'
     *  UnitDelay: '<S68>/UnitDelay1'
     *  UnitDelay: '<S68>/UnitDelay2'
     */
    if (CtApAEM_DW.UnitDelay2_DSTATE_o ||
        CtApAEM_B.falling_edge1.LogicalOperator) {
      rtb_Switch2 = 0.0;
    } else {
      /* Outputs for Atomic SubSystem: '<S68>/DetectSat' */
      CtApAEM_DetectSat(0.4 + (real_T)Rte_CData_UCE_tiMainWkuReh_C(),
                        CtApAEM_DW.UnitDelay1_DSTATE_i, 0.0,
                        &CtApAEM_B.DetectSat);

      /* End of Outputs for SubSystem: '<S68>/DetectSat' */
      rtb_Switch2 = 0.040000000000000223 + CtApAEM_B.DetectSat.MinMax2;
    }

    /* End of Switch: '<S68>/Switch2' */

    /* Outputs for Atomic SubSystem: '<S68>/rising_edge' */

    /* RelationalOperator: '<S68>/Relational Operator2' incorporates:
     *  Constant: '<S65>/UCE_tiMainWkuReh_C'
     */
    CtApAEM_rising_edge(rtb_Switch2 >= (real_T)Rte_CData_UCE_tiMainWkuReh_C(),
                        &CtApAEM_B.rising_edge, &CtApAEM_DW.rising_edge);

    /* End of Outputs for SubSystem: '<S68>/rising_edge' */

    /* CombinatorialLogic: '<S71>/Logic' incorporates:
     *  Logic: '<S68>/Logical Operator1'
     *  Memory: '<S71>/Memory'
     *  Switch: '<S68>/Switch'
     */
    rowIdx = (int32_T)((((((!rtb_RelationalOperator1_e0) &&
      CtApAEM_B.rising_edge.LogicalOperator) || (!!rtb_RelationalOperator1_e0))
                         + ((uint32_T)rtb_LogicalOperator << 1)) << 1) +
                       CtApAEM_DW.Memory_PreviousInput);

    /* Update for UnitDelay: '<S68>/UnitDelay1' */
    CtApAEM_DW.UnitDelay1_DSTATE_i = rtb_Switch2;

    /* Update for UnitDelay: '<S68>/UnitDelay2' */
    CtApAEM_DW.UnitDelay2_DSTATE_o = rtb_RelationalOperator1_e0;

    /* Update for Memory: '<S71>/Memory' incorporates:
     *  CombinatorialLogic: '<S71>/Logic'
     */
    CtApAEM_DW.Memory_PreviousInput = CtApAEM_ConstP.pooled39[(uint32_T)rowIdx];

    /* End of Outputs for SubSystem: '<S65>/TurnOffDelay' */

    /* Outputs for Atomic SubSystem: '<S65>/TurnOnDelay1' */

    /* Constant: '<S65>/UCE_tiMainDisrdDet_C' incorporates:
     *  Constant: '<S65>/UCE_Sldxxms_PdV'
     */
    CtApAEM_TurnOnDelay1(rtb_LogicalOperator12_jr, (real_T)
                         Rte_CData_UCE_tiMainDisrdDet_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay1_h, &CtApAEM_DW.TurnOnDelay1_h);

    /* End of Outputs for SubSystem: '<S65>/TurnOnDelay1' */

    /* Chart: '<S66>/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD' incorporates:
     *  CombinatorialLogic: '<S71>/Logic'
     *  Constant: '<S65>/NOMINAL_MAIN_WAKEUP'
     *  Logic: '<S65>/Logical Operator1'
     *  RelationalOperator: '<S65>/Relational Operator1'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    /* Gateway: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_06_Traiter_Anomalie_Reveil_principal/F01_01_06_02_Renseigner_Anomalie_Reveil_principal_pour_GD/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD */
    /* During: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_06_Traiter_Anomalie_Reveil_principal/F01_01_06_02_Renseigner_Anomalie_Reveil_principal_pour_GD/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD */
    if (CtApAEM_DW.is_active_c1_CtApAEM == 0U) {
      /* Entry: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_06_Traiter_Anomalie_Reveil_principal/F01_01_06_02_Renseigner_Anomalie_Reveil_principal_pour_GD/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD */
      CtApAEM_DW.is_active_c1_CtApAEM = 1U;

      /* Entry Internal: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_06_Traiter_Anomalie_Reveil_principal/F01_01_06_02_Renseigner_Anomalie_Reveil_principal_pour_GD/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD */
      /* Transition: '<S78>:4' */
      CtApAEM_DW.is_c1_CtApAEM = CtApAEM_IN_Defaut_inactif;

      /* Entry 'Defaut_inactif': '<S78>:2' */
      CtApAEM_B.UCE_bDgoMainWkuDisrd = CtApAEM_inactive;
    } else if (CtApAEM_DW.is_c1_CtApAEM == CtApAEM_IN_Defaut_actif) {
      /* Outputs for Atomic SubSystem: '<S65>/TurnOffDelay' */
      /* During 'Defaut_actif': '<S78>:1' */
      if (CtApAEM_ConstP.pooled39[(uint32_T)rowIdx] &&
          (CtApAEM_DW.UnitDelay1_DSTATE_o == 4)) {
        /* Transition: '<S78>:5' */
        CtApAEM_DW.is_c1_CtApAEM = CtApAEM_IN_Defaut_inactif;

        /* Entry 'Defaut_inactif': '<S78>:2' */
        CtApAEM_B.UCE_bDgoMainWkuDisrd = CtApAEM_inactive;
      }

      /* End of Outputs for SubSystem: '<S65>/TurnOffDelay' */
    } else {
      /* During 'Defaut_inactif': '<S78>:2' */
      if (CtApAEM_B.TurnOnDelay1_h.Logic[1]) {
        /* Transition: '<S78>:3' */
        CtApAEM_DW.is_c1_CtApAEM = CtApAEM_IN_Defaut_actif;

        /* Entry 'Defaut_actif': '<S78>:1' */
        CtApAEM_B.UCE_bDgoMainWkuDisrd = CtApAEM_active;
      }
    }

    /* End of Chart: '<S66>/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD' */

    /* Logic: '<S67>/Logical Operator1' incorporates:
     *  Constant: '<S67>/DEGRADED_MAIN_WAKEUP'
     *  Constant: '<S67>/NOMINAL_MAIN_WAKEUP'
     *  RelationalOperator: '<S67>/Relational Operator1'
     *  RelationalOperator: '<S67>/Relational Operator2'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    CtApAEM_B.LogicalOperator1_g = ((CtApAEM_DW.UnitDelay1_DSTATE_o == 4) ||
      (CtApAEM_DW.UnitDelay1_DSTATE_o == 5));

    /* Outputs for Atomic SubSystem: '<S79>/TurnOnDelay1' */

    /* Logic: '<S79>/Logical Operator12' incorporates:
     *  Constant: '<S79>/ACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S79>/UCE_Sldxxms_PdV'
     *  Constant: '<S79>/UCE_tiMainIncstDet_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  RelationalOperator: '<S79>/Relational Operator3'
     */
    CtApAEM_TurnOnDelay1((rtb_DataTypeConversion == 2) && (!rtb_LogicalOperator1),
                         (real_T)Rte_CData_UCE_tiMainIncstDet_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay1_m, &CtApAEM_DW.TurnOnDelay1_m);

    /* End of Outputs for SubSystem: '<S79>/TurnOnDelay1' */

    /* Outputs for Atomic SubSystem: '<S79>/TurnOnDelay2' */

    /* Logic: '<S79>/Logical Operator2' incorporates:
     *  Constant: '<S79>/INACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S79>/UCE_Sldxxms_PdV'
     *  Constant: '<S79>/UCE_spdThdNomDeac_C'
     *  Constant: '<S79>/UCE_tiMainIncstDet_C'
     *  DataTypeConversion: '<S4>/Data Type Conversion1'
     *  RelationalOperator: '<S79>/Relational Operator5'
     *  RelationalOperator: '<S79>/Relational Operator6'
     */
    CtApAEM_TurnOnDelay1((rtb_DataTypeConversion == 1) && (tmp_2 >=
      Rte_CData_UCE_spdThdNomDeac_C()), (real_T)Rte_CData_UCE_tiMainIncstDet_C(),
                         0.04, &CtApAEM_B.TurnOnDelay2_e,
                         &CtApAEM_DW.TurnOnDelay2_e);

    /* End of Outputs for SubSystem: '<S79>/TurnOnDelay2' */

    /* Logic: '<S79>/Logical Operator1' */
    rtb_LogicalOperator12_jr = (CtApAEM_B.TurnOnDelay1_m.Logic[1] ||
      CtApAEM_B.TurnOnDelay2_e.Logic[1]);

    /* Outputs for Atomic SubSystem: '<S79>/TurnOnDelay3' */

    /* RelationalOperator: '<S627>/Relational Operator' incorporates:
     *  Constant: '<S79>/UCE_Sldxxms_PdV'
     *  Constant: '<S79>/UCE_tiMainWkuReh_C'
     */
    CtApAEM_TurnOnDelay1(rtb_LogicalOperator1, (real_T)
                         Rte_CData_UCE_tiMainWkuReh_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay3_o, &CtApAEM_DW.TurnOnDelay3_o);

    /* End of Outputs for SubSystem: '<S79>/TurnOnDelay3' */

    /* Chart: '<S80>/F01_01_07_02_01_Automate_Incoherence_Reveil_principal_pour_GD' incorporates:
     *  Constant: '<S79>/NOMINAL_MAIN_WAKEUP'
     *  Logic: '<S79>/Logical Operator3'
     *  RelationalOperator: '<S79>/Relational Operator2'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    /* Gateway: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_07_Traiter_Incoherence_Reveil_principal/F01_01_07_02_Renseigner_Incoherence_Reveil_principal_pour_GD/F01_01_07_02_01_Automate_Incoherence_Reveil_principal_pour_GD */
    /* During: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_07_Traiter_Incoherence_Reveil_principal/F01_01_07_02_Renseigner_Incoherence_Reveil_principal_pour_GD/F01_01_07_02_01_Automate_Incoherence_Reveil_principal_pour_GD */
    if (CtApAEM_DW.is_active_c2_CtApAEM == 0U) {
      /* Entry: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_07_Traiter_Incoherence_Reveil_principal/F01_01_07_02_Renseigner_Incoherence_Reveil_principal_pour_GD/F01_01_07_02_01_Automate_Incoherence_Reveil_principal_pour_GD */
      CtApAEM_DW.is_active_c2_CtApAEM = 1U;

      /* Entry Internal: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_07_Traiter_Incoherence_Reveil_principal/F01_01_07_02_Renseigner_Incoherence_Reveil_principal_pour_GD/F01_01_07_02_01_Automate_Incoherence_Reveil_principal_pour_GD */
      /* Transition: '<S97>:4' */
      CtApAEM_DW.is_c2_CtApAEM = CtApAEM_IN_Defaut_inactif;

      /* Entry 'Defaut_inactif': '<S97>:2' */
      CtApAEM_B.UCE_bDgoMainWkuIncst = CtApAEM_inactive;
    } else if (CtApAEM_DW.is_c2_CtApAEM == CtApAEM_IN_Defaut_actif) {
      /* During 'Defaut_actif': '<S97>:1' */
      if (CtApAEM_B.TurnOnDelay3_o.Logic[1] && (CtApAEM_DW.UnitDelay1_DSTATE_o ==
           4)) {
        /* Transition: '<S97>:5' */
        CtApAEM_DW.is_c2_CtApAEM = CtApAEM_IN_Defaut_inactif;

        /* Entry 'Defaut_inactif': '<S97>:2' */
        CtApAEM_B.UCE_bDgoMainWkuIncst = CtApAEM_inactive;
      }
    } else {
      /* During 'Defaut_inactif': '<S97>:2' */
      if (rtb_LogicalOperator12_jr) {
        /* Transition: '<S97>:3' */
        CtApAEM_DW.is_c2_CtApAEM = CtApAEM_IN_Defaut_actif;

        /* Entry 'Defaut_actif': '<S97>:1' */
        CtApAEM_B.UCE_bDgoMainWkuIncst = CtApAEM_active;
      }
    }

    /* End of Chart: '<S80>/F01_01_07_02_01_Automate_Incoherence_Reveil_principal_pour_GD' */

    /* Logic: '<S81>/Logical Operator1' incorporates:
     *  Constant: '<S81>/DEGRADED_MAIN_WAKEUP'
     *  Constant: '<S81>/NOMINAL_MAIN_WAKEUP'
     *  RelationalOperator: '<S81>/Relational Operator1'
     *  RelationalOperator: '<S81>/Relational Operator2'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    CtApAEM_B.LogicalOperator1_h = ((CtApAEM_DW.UnitDelay1_DSTATE_o == 4) ||
      (CtApAEM_DW.UnitDelay1_DSTATE_o == 5));

    /* Outputs for Enabled SubSystem: '<S11>/F01_02_01_Gerer_Reveils_partiels_maitres' incorporates:
     *  EnablePort: '<S98>/Enable'
     */
    if (Rte_CData_UCE_noUCETyp_C()) {
      if (!CtApAEM_DW.F01_02_01_Gerer_Reveils_partiel) {
        CtApAEM_DW.F01_02_01_Gerer_Reveils_partiel = true;
      }

      /* Outputs for Atomic SubSystem: '<S101>/8Bit Decoder' */

      /* S-Function (sfix_bitop): '<S628>/Bitwise AND' incorporates:
       *  ArithShift: '<S628>/Shift Arithmetic'
       *  ArithShift: '<S628>/Shift Arithmetic1'
       *  ArithShift: '<S628>/Shift Arithmetic2'
       *  DataTypeConversion: '<S628>/Data Type Conversion1'
       *  DataTypeConversion: '<S628>/Data Type Conversion2'
       *  DataTypeConversion: '<S628>/Data Type Conversion3'
       *  DataTypeConversion: '<S628>/Data Type Conversion4'
       */
      CtApAEM_uBitDecoder(tmp_1 | rtb_LogicalOperator1_f << 1 | tmp_0 << 2 | tmp
                          << 3, &CtApAEM_B.uBitDecoder);

      /* End of Outputs for SubSystem: '<S101>/8Bit Decoder' */

      /* Outputs for Atomic SubSystem: '<S101>/8Bit Decoder1' */

      /* S-Function (sfix_bitop): '<S629>/Bitwise AND' incorporates:
       *  ArithShift: '<S629>/Shift Arithmetic'
       *  ArithShift: '<S629>/Shift Arithmetic1'
       *  ArithShift: '<S629>/Shift Arithmetic2'
       *  DataTypeConversion: '<S629>/Data Type Conversion'
       *  DataTypeConversion: '<S629>/Data Type Conversion1'
       *  DataTypeConversion: '<S629>/Data Type Conversion2'
       *  DataTypeConversion: '<S629>/Data Type Conversion3'
       */
      CtApAEM_uBitDecoder(tmp_1 | rtb_LogicalOperator1_f << 1 | tmp_0 << 2 | tmp
                          << 3, &CtApAEM_B.uBitDecoder1);

      /* End of Outputs for SubSystem: '<S101>/8Bit Decoder1' */

      /* Outputs for Enabled SubSystem: '<S101>/F01_02_01_01_08_Gerer_etat_Reveil_partiel_maitre_Y8' incorporates:
       *  EnablePort: '<S115>/Enable'
       */
      /* Logic: '<S107>/Logical Operator8' incorporates:
       *  Constant: '<S107>/UCE_bInhPtlWkuY5_C1'
       *  Constant: '<S115>/UCE_tiMaxTiMstPtlWkuY5_C'
       *  Constant: '<S115>/UCE_tiMinTiMstPtlWkuY5_C'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY8_C()) {
        /* Outputs for Atomic SubSystem: '<S115>/Gerer_etat_Reveil_partiel_maitre_Yj' */
        Gerer_etat_Reveil_partiel_maitr
          (CtApAEM_B.uBitDecoder1.RelationalOperator8,
           CtApAEM_B.uBitDecoder.RelationalOperator8, rtb_LogicalOperator5_mj,
           CtApAEM_DW.UnitDelay1_DSTATE_o, (real_T)
           Rte_CData_UCE_tiMinTiMstPtlWkuY8_C(), (real_T)
           Rte_CData_UCE_tiMaxTiMstPtlWkuY8_C(),
           &CtApAEM_B.Gerer_etat_Reveil_partiel_ma_ju,
           &CtApAEM_DW.Gerer_etat_Reveil_partiel_ma_ju);

        /* End of Outputs for SubSystem: '<S115>/Gerer_etat_Reveil_partiel_maitre_Yj' */
      }

      /* End of Logic: '<S107>/Logical Operator8' */
      /* End of Outputs for SubSystem: '<S101>/F01_02_01_01_08_Gerer_etat_Reveil_partiel_maitre_Y8' */

      /* Outputs for Enabled SubSystem: '<S101>/F01_02_01_01_07_Gerer_etat_Reveil_partiel_maitre_Y7' incorporates:
       *  EnablePort: '<S114>/Enable'
       */
      /* Logic: '<S107>/Logical Operator7' incorporates:
       *  Constant: '<S107>/UCE_bInhPtlWkuY4_C1'
       *  Constant: '<S114>/UCE_tiMaxTiMstPtlWkuY5_C'
       *  Constant: '<S114>/UCE_tiMinTiMstPtlWkuY5_C'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY7_C()) {
        /* Outputs for Atomic SubSystem: '<S114>/Gerer_etat_Reveil_partiel_maitre_Yj' */
        Gerer_etat_Reveil_partiel_maitr
          (CtApAEM_B.uBitDecoder1.RelationalOperator9,
           CtApAEM_B.uBitDecoder.RelationalOperator9, rtb_LogicalOperator5_mj,
           CtApAEM_DW.UnitDelay1_DSTATE_o, (real_T)
           Rte_CData_UCE_tiMinTiMstPtlWkuY7_C(), (real_T)
           Rte_CData_UCE_tiMaxTiMstPtlWkuY7_C(),
           &CtApAEM_B.Gerer_etat_Reveil_partiel_mai_l,
           &CtApAEM_DW.Gerer_etat_Reveil_partiel_mai_l);

        /* End of Outputs for SubSystem: '<S114>/Gerer_etat_Reveil_partiel_maitre_Yj' */
      }

      /* End of Logic: '<S107>/Logical Operator7' */
      /* End of Outputs for SubSystem: '<S101>/F01_02_01_01_07_Gerer_etat_Reveil_partiel_maitre_Y7' */

      /* Outputs for Enabled SubSystem: '<S101>/F01_02_01_01_06_Gerer_etat_Reveil_partiel_maitre_Y6' incorporates:
       *  EnablePort: '<S113>/Enable'
       */
      /* Logic: '<S107>/Logical Operator6' incorporates:
       *  Constant: '<S107>/UCE_bInhPtlWkuY3_C1'
       *  Constant: '<S113>/UCE_tiMaxTiMstPtlWkuY5_C'
       *  Constant: '<S113>/UCE_tiMinTiMstPtlWkuY5_C'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY6_C()) {
        /* Outputs for Atomic SubSystem: '<S113>/Gerer_etat_Reveil_partiel_maitre_Yj' */
        Gerer_etat_Reveil_partiel_maitr
          (CtApAEM_B.uBitDecoder1.RelationalOperator10,
           CtApAEM_B.uBitDecoder.RelationalOperator10, rtb_LogicalOperator5_mj,
           CtApAEM_DW.UnitDelay1_DSTATE_o, (real_T)
           Rte_CData_UCE_tiMinTiMstPtlWkuY6_C(), (real_T)
           Rte_CData_UCE_tiMaxTiMstPtlWkuY6_C(),
           &CtApAEM_B.Gerer_etat_Reveil_partiel_mai_j,
           &CtApAEM_DW.Gerer_etat_Reveil_partiel_mai_j);

        /* End of Outputs for SubSystem: '<S113>/Gerer_etat_Reveil_partiel_maitre_Yj' */
      }

      /* End of Logic: '<S107>/Logical Operator6' */
      /* End of Outputs for SubSystem: '<S101>/F01_02_01_01_06_Gerer_etat_Reveil_partiel_maitre_Y6' */

      /* Outputs for Enabled SubSystem: '<S101>/F01_02_01_01_05_Gerer_etat_Reveil_partiel_maitre_Y5' incorporates:
       *  EnablePort: '<S112>/Enable'
       */
      /* Logic: '<S107>/Logical Operator5' incorporates:
       *  Constant: '<S107>/UCE_bInhPtlWkuY5_C'
       *  Constant: '<S112>/UCE_tiMaxTiMstPtlWkuY5_C'
       *  Constant: '<S112>/UCE_tiMinTiMstPtlWkuY5_C'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY5_C()) {
        /* Outputs for Atomic SubSystem: '<S112>/Gerer_etat_Reveil_partiel_maitre_Yj' */
        Gerer_etat_Reveil_partiel_maitr
          (CtApAEM_B.uBitDecoder1.RelationalOperator11,
           CtApAEM_B.uBitDecoder.RelationalOperator11, rtb_LogicalOperator5_mj,
           CtApAEM_DW.UnitDelay1_DSTATE_o, (real_T)
           Rte_CData_UCE_tiMinTiMstPtlWkuY5_C(), (real_T)
           Rte_CData_UCE_tiMaxTiMstPtlWkuY5_C(),
           &CtApAEM_B.Gerer_etat_Reveil_partiel_mai_g,
           &CtApAEM_DW.Gerer_etat_Reveil_partiel_mai_g);

        /* End of Outputs for SubSystem: '<S112>/Gerer_etat_Reveil_partiel_maitre_Yj' */
      }

      /* End of Logic: '<S107>/Logical Operator5' */
      /* End of Outputs for SubSystem: '<S101>/F01_02_01_01_05_Gerer_etat_Reveil_partiel_maitre_Y5' */

      /* Outputs for Enabled SubSystem: '<S101>/F01_02_01_01_04_Gerer_etat_Reveil_partiel_maitre_Y4' incorporates:
       *  EnablePort: '<S111>/Enable'
       */
      /* Logic: '<S107>/Logical Operator4' incorporates:
       *  Constant: '<S107>/UCE_bInhPtlWkuY4_C'
       *  Constant: '<S111>/UCE_tiMaxTiMstPtlWkuY4_C'
       *  Constant: '<S111>/UCE_tiMinTiMstPtlWkuY4_C'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY4_C()) {
        /* Outputs for Atomic SubSystem: '<S111>/Gerer_etat_Reveil_partiel_maitre_Yj' */
        Gerer_etat_Reveil_partiel_maitr
          (CtApAEM_B.uBitDecoder1.RelationalOperator12,
           CtApAEM_B.uBitDecoder.RelationalOperator12, rtb_LogicalOperator5_mj,
           CtApAEM_DW.UnitDelay1_DSTATE_o, (real_T)
           Rte_CData_UCE_tiMinTiMstPtlWkuY4_C(), (real_T)
           Rte_CData_UCE_tiMaxTiMstPtlWkuY4_C(),
           &CtApAEM_B.Gerer_etat_Reveil_partiel_mai_c,
           &CtApAEM_DW.Gerer_etat_Reveil_partiel_mai_c);

        /* End of Outputs for SubSystem: '<S111>/Gerer_etat_Reveil_partiel_maitre_Yj' */
      }

      /* End of Logic: '<S107>/Logical Operator4' */
      /* End of Outputs for SubSystem: '<S101>/F01_02_01_01_04_Gerer_etat_Reveil_partiel_maitre_Y4' */

      /* Outputs for Enabled SubSystem: '<S101>/F01_02_01_01_03_Gerer_etat_Reveil_partiel_maitre_Y3' incorporates:
       *  EnablePort: '<S110>/Enable'
       */
      /* Logic: '<S107>/Logical Operator3' incorporates:
       *  Constant: '<S107>/UCE_bInhPtlWkuY3_C'
       *  Constant: '<S110>/UCE_tiMaxTiMstPtlWkuY3_C'
       *  Constant: '<S110>/UCE_tiMinTiMstPtlWkuY3_C'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY3_C()) {
        /* Outputs for Atomic SubSystem: '<S110>/Gerer_etat_Reveil_partiel_maitre_Yj' */
        Gerer_etat_Reveil_partiel_maitr
          (CtApAEM_B.uBitDecoder1.RelationalOperator13,
           CtApAEM_B.uBitDecoder.RelationalOperator13, rtb_LogicalOperator5_mj,
           CtApAEM_DW.UnitDelay1_DSTATE_o, (real_T)
           Rte_CData_UCE_tiMinTiMstPtlWkuY3_C(), (real_T)
           Rte_CData_UCE_tiMaxTiMstPtlWkuY3_C(),
           &CtApAEM_B.Gerer_etat_Reveil_partiel_ma_pm,
           &CtApAEM_DW.Gerer_etat_Reveil_partiel_ma_pm);

        /* End of Outputs for SubSystem: '<S110>/Gerer_etat_Reveil_partiel_maitre_Yj' */
      }

      /* End of Logic: '<S107>/Logical Operator3' */
      /* End of Outputs for SubSystem: '<S101>/F01_02_01_01_03_Gerer_etat_Reveil_partiel_maitre_Y3' */

      /* Outputs for Enabled SubSystem: '<S101>/F01_02_01_01_02_Gerer_etat_Reveil_partiel_maitre_Y2' incorporates:
       *  EnablePort: '<S109>/Enable'
       */
      /* Logic: '<S107>/Logical Operator2' incorporates:
       *  Constant: '<S107>/UCE_bInhPtlWkuY2_C'
       *  Constant: '<S109>/UCE_tiMaxTiMstPtlWkuY2_C'
       *  Constant: '<S109>/UCE_tiMinTiMstPtlWkuY2_C'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY2_C()) {
        /* Outputs for Atomic SubSystem: '<S109>/Gerer_etat_Reveil_partiel_maitre_Yj' */
        Gerer_etat_Reveil_partiel_maitr
          (CtApAEM_B.uBitDecoder1.RelationalOperator14,
           CtApAEM_B.uBitDecoder.RelationalOperator14, rtb_LogicalOperator5_mj,
           CtApAEM_DW.UnitDelay1_DSTATE_o, (real_T)
           Rte_CData_UCE_tiMinTiMstPtlWkuY2_C(), (real_T)
           Rte_CData_UCE_tiMaxTiMstPtlWkuY2_C(),
           &CtApAEM_B.Gerer_etat_Reveil_partiel_mai_k,
           &CtApAEM_DW.Gerer_etat_Reveil_partiel_mai_k);

        /* End of Outputs for SubSystem: '<S109>/Gerer_etat_Reveil_partiel_maitre_Yj' */
      }

      /* End of Logic: '<S107>/Logical Operator2' */
      /* End of Outputs for SubSystem: '<S101>/F01_02_01_01_02_Gerer_etat_Reveil_partiel_maitre_Y2' */

      /* Outputs for Enabled SubSystem: '<S101>/F01_02_01_01_01_Gerer_etat_Reveil_partiel_maitre_Y1' incorporates:
       *  EnablePort: '<S108>/Enable'
       */
      /* Logic: '<S107>/Logical Operator1' incorporates:
       *  Constant: '<S107>/UCE_bInhPtlWkuY1_C'
       *  Constant: '<S108>/UCE_tiMaxTiMstPtlWkuY1_C'
       *  Constant: '<S108>/UCE_tiMinTiMstPtlWkuY1_C'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY1_C()) {
        /* Outputs for Atomic SubSystem: '<S108>/Gerer_etat_Reveil_partiel_maitre_Yj' */
        Gerer_etat_Reveil_partiel_maitr
          (CtApAEM_B.uBitDecoder1.DataTypeConversion,
           CtApAEM_B.uBitDecoder.DataTypeConversion, rtb_LogicalOperator5_mj,
           CtApAEM_DW.UnitDelay1_DSTATE_o, (real_T)
           Rte_CData_UCE_tiMinTiMstPtlWkuY1_C(), (real_T)
           Rte_CData_UCE_tiMaxTiMstPtlWkuY1_C(),
           &CtApAEM_B.Gerer_etat_Reveil_partiel_mai_p,
           &CtApAEM_DW.Gerer_etat_Reveil_partiel_mai_p);

        /* End of Outputs for SubSystem: '<S108>/Gerer_etat_Reveil_partiel_maitre_Yj' */
      }

      /* End of Logic: '<S107>/Logical Operator1' */
      /* End of Outputs for SubSystem: '<S101>/F01_02_01_01_01_Gerer_etat_Reveil_partiel_maitre_Y1' */

      /* Outputs for Atomic SubSystem: '<S101>/8Bit Encoder' */
      CtApAEM_uBitEncoder
        (CtApAEM_B.Gerer_etat_Reveil_partiel_ma_ju.rising_edge_d.LogicalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_l.rising_edge_d.LogicalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_j.rising_edge_d.LogicalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_g.rising_edge_d.LogicalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_c.rising_edge_d.LogicalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_ma_pm.rising_edge_d.LogicalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_k.rising_edge_d.LogicalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_p.rising_edge_d.LogicalOperator,
         &CtApAEM_B.uBitEncoder);

      /* End of Outputs for SubSystem: '<S101>/8Bit Encoder' */

      /* Outputs for Atomic SubSystem: '<S101>/8Bit Encoder1' */
      CtApAEM_uBitEncoder
        (CtApAEM_B.Gerer_etat_Reveil_partiel_ma_ju.RelationalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_l.RelationalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_j.RelationalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_g.RelationalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_c.RelationalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_ma_pm.RelationalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_k.RelationalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_p.RelationalOperator,
         &CtApAEM_B.uBitEncoder1);

      /* End of Outputs for SubSystem: '<S101>/8Bit Encoder1' */

      /* Outputs for Atomic SubSystem: '<S102>/8Bit Decoder1' */
      CtApAEM_uBitDecoder(CtApAEM_B.uBitEncoder.Sum, &CtApAEM_B.uBitDecoder1_k);

      /* End of Outputs for SubSystem: '<S102>/8Bit Decoder1' */

      /* Logic: '<S102>/Logical Operator4' */
      CtApAEM_B.LogicalOperator4_b =
        (CtApAEM_B.uBitDecoder1_k.RelationalOperator8 ||
         CtApAEM_B.uBitDecoder1_k.RelationalOperator9 ||
         CtApAEM_B.uBitDecoder1_k.RelationalOperator10 ||
         CtApAEM_B.uBitDecoder1_k.RelationalOperator11 ||
         CtApAEM_B.uBitDecoder1_k.RelationalOperator12 ||
         CtApAEM_B.uBitDecoder1_k.RelationalOperator13 ||
         CtApAEM_B.uBitDecoder1_k.RelationalOperator14 ||
         CtApAEM_B.uBitDecoder1_k.DataTypeConversion);

      /* Outputs for Atomic SubSystem: '<S102>/8Bit Decoder2' */
      CtApAEM_uBitDecoder(CtApAEM_B.uBitEncoder1.Sum, &CtApAEM_B.uBitDecoder2);

      /* End of Outputs for SubSystem: '<S102>/8Bit Decoder2' */

      /* Logic: '<S102>/Logical Operator2' incorporates:
       *  Constant: '<S102>/PARTIAL_WAKEUP'
       *  Logic: '<S102>/Logical Operator1'
       *  RelationalOperator: '<S102>/Relational Operator2'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      CtApAEM_B.LogicalOperator2 =
        ((!(CtApAEM_B.uBitDecoder2.RelationalOperator8 ||
            CtApAEM_B.uBitDecoder2.RelationalOperator9 ||
            CtApAEM_B.uBitDecoder2.RelationalOperator10 ||
            CtApAEM_B.uBitDecoder2.RelationalOperator11 ||
            CtApAEM_B.uBitDecoder2.RelationalOperator12 ||
            CtApAEM_B.uBitDecoder2.RelationalOperator13 ||
            CtApAEM_B.uBitDecoder2.RelationalOperator14 ||
            CtApAEM_B.uBitDecoder2.DataTypeConversion)) &&
         (CtApAEM_DW.UnitDelay1_DSTATE_o == 1));
    } else {
      if (CtApAEM_DW.F01_02_01_Gerer_Reveils_partiel) {
        /* Disable for Outport: '<S98>/UCE_bfMstPtlWkuReq' */
        CtApAEM_B.uBitEncoder1.Sum = 0;
        CtApAEM_DW.F01_02_01_Gerer_Reveils_partiel = false;
      }
    }

    /* End of Outputs for SubSystem: '<S11>/F01_02_01_Gerer_Reveils_partiels_maitres' */

    /* Outputs for Atomic SubSystem: '<S198>/16Bit Decoder' */

    /* S-Function (sfix_bitop): '<S630>/Bitwise AND' incorporates:
     *  ArithShift: '<S630>/Shift Arithmetic'
     *  ArithShift: '<S630>/Shift Arithmetic1'
     *  ArithShift: '<S630>/Shift Arithmetic2'
     *  DataTypeConversion: '<S630>/Data Type Conversion1'
     *  DataTypeConversion: '<S630>/Data Type Conversion2'
     *  DataTypeConversion: '<S630>/Data Type Conversion3'
     *  DataTypeConversion: '<S630>/Data Type Conversion4'
     */
    CtApAEM_u6BitDecoder(tmp_7 | tmp_4 << 1 | tmp_5 << 2 | tmp_6 << 3,
                         &CtApAEM_B.u6BitDecoder);

    /* End of Outputs for SubSystem: '<S198>/16Bit Decoder' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_16_Gerer_etat_Reveil_partiel_esclave_X16' incorporates:
     *  EnablePort: '<S219>/Enable'
     */
    /* Logic: '<S203>/Logical Operator16' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX8_C8'
     *  Constant: '<S219>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S219>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S219>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S219>/UCE_tiPtlWkuX8Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX16_C()) {
      /* Outputs for Atomic SubSystem: '<S219>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX16AcvMod_C(),
        (real_T)Rte_CData_UCE_tiPtlWkuX16Acv_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX16Deac_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX16Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_c,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_c);

      /* End of Outputs for SubSystem: '<S219>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator16' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_16_Gerer_etat_Reveil_partiel_esclave_X16' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_15_Gerer_etat_Reveil_partiel_esclave_X15' incorporates:
     *  EnablePort: '<S218>/Enable'
     */
    /* Logic: '<S203>/Logical Operator15' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX8_C7'
     *  Constant: '<S218>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S218>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S218>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S218>/UCE_tiPtlWkuX8Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX15_C()) {
      /* Outputs for Atomic SubSystem: '<S218>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator1,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX15AcvMod_C(),
        (real_T)Rte_CData_UCE_tiPtlWkuX15Acv_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX15Deac_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX15Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_es_m4,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_es_m4);

      /* End of Outputs for SubSystem: '<S218>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator15' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_15_Gerer_etat_Reveil_partiel_esclave_X15' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_14_Gerer_etat_Reveil_partiel_esclave_X14' incorporates:
     *  EnablePort: '<S217>/Enable'
     */
    /* Logic: '<S203>/Logical Operator14' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX8_C6'
     *  Constant: '<S217>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S217>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S217>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S217>/UCE_tiPtlWkuX8Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX14_C()) {
      /* Outputs for Atomic SubSystem: '<S217>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator2,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX14AcvMod_C(),
        (real_T)Rte_CData_UCE_tiPtlWkuX14Acv_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX14Deac_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX14Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_es_gv,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_es_gv);

      /* End of Outputs for SubSystem: '<S217>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator14' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_14_Gerer_etat_Reveil_partiel_esclave_X14' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_13_Gerer_etat_Reveil_partiel_esclave_X13' incorporates:
     *  EnablePort: '<S216>/Enable'
     */
    /* Logic: '<S203>/Logical Operator13' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX8_C5'
     *  Constant: '<S216>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S216>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S216>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S216>/UCE_tiPtlWkuX8Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX13_C()) {
      /* Outputs for Atomic SubSystem: '<S216>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator3,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX13AcvMod_C(),
        (real_T)Rte_CData_UCE_tiPtlWkuX13Acv_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX13Deac_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX13Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_es_mj,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_es_mj);

      /* End of Outputs for SubSystem: '<S216>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator13' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_13_Gerer_etat_Reveil_partiel_esclave_X13' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_12_Gerer_etat_Reveil_partiel_esclave_X12' incorporates:
     *  EnablePort: '<S215>/Enable'
     */
    /* Logic: '<S203>/Logical Operator12' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX8_C4'
     *  Constant: '<S215>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S215>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S215>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S215>/UCE_tiPtlWkuX8Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX12_C()) {
      /* Outputs for Atomic SubSystem: '<S215>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator4,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX12AcvMod_C(),
        (real_T)Rte_CData_UCE_tiPtlWkuX12Acv_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX12Deac_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX12Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_es_n2,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_es_n2);

      /* End of Outputs for SubSystem: '<S215>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator12' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_12_Gerer_etat_Reveil_partiel_esclave_X12' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_11_Gerer_etat_Reveil_partiel_esclave_X11' incorporates:
     *  EnablePort: '<S214>/Enable'
     */
    /* Logic: '<S203>/Logical Operator11' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX8_C3'
     *  Constant: '<S214>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S214>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S214>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S214>/UCE_tiPtlWkuX8Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX11_C()) {
      /* Outputs for Atomic SubSystem: '<S214>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator5,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX11AcvMod_C(),
        (real_T)Rte_CData_UCE_tiPtlWkuX11Acv_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX11Deac_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX11Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_d,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_d);

      /* End of Outputs for SubSystem: '<S214>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator11' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_11_Gerer_etat_Reveil_partiel_esclave_X11' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_10_Gerer_etat_Reveil_partiel_esclave_X10' incorporates:
     *  EnablePort: '<S213>/Enable'
     */
    /* Logic: '<S203>/Logical Operator10' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX8_C2'
     *  Constant: '<S213>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S213>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S213>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S213>/UCE_tiPtlWkuX8Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX10_C()) {
      /* Outputs for Atomic SubSystem: '<S213>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator6,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX10AcvMod_C(),
        (real_T)Rte_CData_UCE_tiPtlWkuX10Acv_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX10Deac_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX10Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_e,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_e);

      /* End of Outputs for SubSystem: '<S213>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator10' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_10_Gerer_etat_Reveil_partiel_esclave_X10' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_09_Gerer_etat_Reveil_partiel_esclave_X9' incorporates:
     *  EnablePort: '<S212>/Enable'
     */
    /* Logic: '<S203>/Logical Operator9' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX8_C1'
     *  Constant: '<S212>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S212>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S212>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S212>/UCE_tiPtlWkuX8Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX9_C()) {
      /* Outputs for Atomic SubSystem: '<S212>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator7,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX9AcvMod_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX9Acv_C(), (real_T)Rte_CData_UCE_tiPtlWkuX9Deac_C(),
        (real_T)Rte_CData_UCE_tiPtlWkuX9Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_es_hk,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_es_hk);

      /* End of Outputs for SubSystem: '<S212>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator9' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_09_Gerer_etat_Reveil_partiel_esclave_X9' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_08_Gerer_etat_Reveil_partiel_esclave_X8' incorporates:
     *  EnablePort: '<S211>/Enable'
     */
    /* Logic: '<S203>/Logical Operator7' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX8_C'
     *  Constant: '<S211>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S211>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S211>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S211>/UCE_tiPtlWkuX8Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX8_C()) {
      /* Outputs for Atomic SubSystem: '<S211>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator8,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX8AcvMod_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX8Acv_C(), (real_T)Rte_CData_UCE_tiPtlWkuX8Deac_C(),
        (real_T)Rte_CData_UCE_tiPtlWkuX8Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_k,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_k);

      /* End of Outputs for SubSystem: '<S211>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator7' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_08_Gerer_etat_Reveil_partiel_esclave_X8' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_07_Gerer_etat_Reveil_partiel_esclave_X7' incorporates:
     *  EnablePort: '<S210>/Enable'
     */
    /* Logic: '<S203>/Logical Operator8' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX7_C'
     *  Constant: '<S210>/UCE_bSlavePtlWkuX7AcvMod_C'
     *  Constant: '<S210>/UCE_tiPtlWkuX7Acv_C'
     *  Constant: '<S210>/UCE_tiPtlWkuX7Deac_C'
     *  Constant: '<S210>/UCE_tiPtlWkuX7Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX7_C()) {
      /* Outputs for Atomic SubSystem: '<S210>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator9,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX7AcvMod_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX7Acv_C(), (real_T)Rte_CData_UCE_tiPtlWkuX7Deac_C(),
        (real_T)Rte_CData_UCE_tiPtlWkuX7Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_h,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_h);

      /* End of Outputs for SubSystem: '<S210>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator8' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_07_Gerer_etat_Reveil_partiel_esclave_X7' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_06_Gerer_etat_Reveil_partiel_esclave_X6' incorporates:
     *  EnablePort: '<S209>/Enable'
     */
    /* Logic: '<S203>/Logical Operator5' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX6_C'
     *  Constant: '<S209>/UCE_bSlavePtlWkuX6AcvMod_C'
     *  Constant: '<S209>/UCE_tiPtlWkuX6Acv_C'
     *  Constant: '<S209>/UCE_tiPtlWkuX6Deac_C'
     *  Constant: '<S209>/UCE_tiPtlWkuX6Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX6_C()) {
      /* Outputs for Atomic SubSystem: '<S209>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla
        (CtApAEM_B.u6BitDecoder.RelationalOperator10, rtb_LogicalOperator1,
         CtApAEM_DW.UnitDelay1_DSTATE_o, rtb_LogicalOperator5_mj,
         Rte_CData_UCE_bSlavePtlWkuX6AcvMod_C(), (real_T)
         Rte_CData_UCE_tiPtlWkuX6Acv_C(), (real_T)Rte_CData_UCE_tiPtlWkuX6Deac_C
         (), (real_T)Rte_CData_UCE_tiPtlWkuX6Lock_C(),
         &CtApAEM_B.Gerer_etat_Reveil_partiel_es_mk,
         &CtApAEM_DW.Gerer_etat_Reveil_partiel_es_mk);

      /* End of Outputs for SubSystem: '<S209>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator5' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_06_Gerer_etat_Reveil_partiel_esclave_X6' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_05_Gerer_etat_Reveil_partiel_esclave_X5' incorporates:
     *  EnablePort: '<S208>/Enable'
     */
    /* Logic: '<S203>/Logical Operator6' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX5_C'
     *  Constant: '<S208>/UCE_bSlavePtlWkuX5AcvMod_C'
     *  Constant: '<S208>/UCE_tiPtlWkuX5Acv_C'
     *  Constant: '<S208>/UCE_tiPtlWkuX5Deac_C'
     *  Constant: '<S208>/UCE_tiPtlWkuX5Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX5_C()) {
      /* Outputs for Atomic SubSystem: '<S208>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla
        (CtApAEM_B.u6BitDecoder.RelationalOperator11, rtb_LogicalOperator1,
         CtApAEM_DW.UnitDelay1_DSTATE_o, rtb_LogicalOperator5_mj,
         Rte_CData_UCE_bSlavePtlWkuX5AcvMod_C(), (real_T)
         Rte_CData_UCE_tiPtlWkuX5Acv_C(), (real_T)Rte_CData_UCE_tiPtlWkuX5Deac_C
         (), (real_T)Rte_CData_UCE_tiPtlWkuX5Lock_C(),
         &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_a,
         &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_a);

      /* End of Outputs for SubSystem: '<S208>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator6' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_05_Gerer_etat_Reveil_partiel_esclave_X5' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_04_Gerer_etat_Reveil_partiel_esclave_X4' incorporates:
     *  EnablePort: '<S207>/Enable'
     */
    /* Logic: '<S203>/Logical Operator3' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX4_C'
     *  Constant: '<S207>/UCE_bSlavePtlWkuX4AcvMod_C'
     *  Constant: '<S207>/UCE_tiPtlWkuX4Acv_C'
     *  Constant: '<S207>/UCE_tiPtlWkuX4Deac_C'
     *  Constant: '<S207>/UCE_tiPtlWkuX4Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX4_C()) {
      /* Outputs for Atomic SubSystem: '<S207>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla
        (CtApAEM_B.u6BitDecoder.RelationalOperator12, rtb_LogicalOperator1,
         CtApAEM_DW.UnitDelay1_DSTATE_o, rtb_LogicalOperator5_mj,
         Rte_CData_UCE_bSlavePtlWkuX4AcvMod_C(), (real_T)
         Rte_CData_UCE_tiPtlWkuX4Acv_C(), (real_T)Rte_CData_UCE_tiPtlWkuX4Deac_C
         (), (real_T)Rte_CData_UCE_tiPtlWkuX4Lock_C(),
         &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_j,
         &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_j);

      /* End of Outputs for SubSystem: '<S207>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator3' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_04_Gerer_etat_Reveil_partiel_esclave_X4' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_03_Gerer_etat_Reveil_partiel_esclave_X3' incorporates:
     *  EnablePort: '<S206>/Enable'
     */
    /* Logic: '<S203>/Logical Operator4' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX3_C'
     *  Constant: '<S206>/UCE_bSlavePtlWkuX3AcvMod_C'
     *  Constant: '<S206>/UCE_tiPtlWkuX3Acv_C'
     *  Constant: '<S206>/UCE_tiPtlWkuX3Deac_C'
     *  Constant: '<S206>/UCE_tiPtlWkuX3Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX3_C()) {
      /* Outputs for Atomic SubSystem: '<S206>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla
        (CtApAEM_B.u6BitDecoder.RelationalOperator13, rtb_LogicalOperator1,
         CtApAEM_DW.UnitDelay1_DSTATE_o, rtb_LogicalOperator5_mj,
         Rte_CData_UCE_bSlavePtlWkuX3AcvMod_C(), (real_T)
         Rte_CData_UCE_tiPtlWkuX3Acv_C(), (real_T)Rte_CData_UCE_tiPtlWkuX3Deac_C
         (), (real_T)Rte_CData_UCE_tiPtlWkuX3Lock_C(),
         &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_m,
         &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_m);

      /* End of Outputs for SubSystem: '<S206>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator4' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_03_Gerer_etat_Reveil_partiel_esclave_X3' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_02_Gerer_etat_Reveil_partiel_esclave_X2' incorporates:
     *  EnablePort: '<S205>/Enable'
     */
    /* Logic: '<S203>/Logical Operator1' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX2_C'
     *  Constant: '<S205>/UCE_bSlavePtlWkuX2AcvMod_C'
     *  Constant: '<S205>/UCE_tiPtlWkuX2Acv_C'
     *  Constant: '<S205>/UCE_tiPtlWkuX2Deac_C'
     *  Constant: '<S205>/UCE_tiPtlWkuX2Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX2_C()) {
      /* Outputs for Atomic SubSystem: '<S205>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla
        (CtApAEM_B.u6BitDecoder.RelationalOperator14, rtb_LogicalOperator1,
         CtApAEM_DW.UnitDelay1_DSTATE_o, rtb_LogicalOperator5_mj,
         Rte_CData_UCE_bSlavePtlWkuX2AcvMod_C(), (real_T)
         Rte_CData_UCE_tiPtlWkuX2Acv_C(), (real_T)Rte_CData_UCE_tiPtlWkuX2Deac_C
         (), (real_T)Rte_CData_UCE_tiPtlWkuX2Lock_C(),
         &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_n,
         &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_n);

      /* End of Outputs for SubSystem: '<S205>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator1' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_02_Gerer_etat_Reveil_partiel_esclave_X2' */

    /* Outputs for Enabled SubSystem: '<S198>/F01_02_02_01_01_Gerer_etat_Reveil_partiel_esclave_X1' incorporates:
     *  EnablePort: '<S204>/Enable'
     */
    /* Logic: '<S203>/Logical Operator2' incorporates:
     *  Constant: '<S203>/UCE_bInhPtlWkuX1_C'
     *  Constant: '<S204>/UCE_bSlavePtlWkuX1AcvMod_C'
     *  Constant: '<S204>/UCE_tiPtlWkuX1Acv_C'
     *  Constant: '<S204>/UCE_tiPtlWkuX1Deac_C'
     *  Constant: '<S204>/UCE_tiPtlWkuX1Lock_C'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX1_C()) {
      /* Outputs for Atomic SubSystem: '<S204>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.DataTypeConversion,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX1AcvMod_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX1Acv_C(), (real_T)Rte_CData_UCE_tiPtlWkuX1Deac_C(),
        (real_T)Rte_CData_UCE_tiPtlWkuX1Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_g,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_g);

      /* End of Outputs for SubSystem: '<S204>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S203>/Logical Operator2' */
    /* End of Outputs for SubSystem: '<S198>/F01_02_02_01_01_Gerer_etat_Reveil_partiel_esclave_X1' */

    /* Outputs for Atomic SubSystem: '<S198>/16Bit Encoder' */
    CtApAEM_u6BitEncoder
      (CtApAEM_B.Gerer_etat_Reveil_partiel_esc_c.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_m4.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_gv.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_mj.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_n2.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_d.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_e.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_hk.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_k.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_h.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_mk.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_a.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_j.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_m.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_n.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_g.TurnOnDelay.Logic[1],
       &CtApAEM_B.u6BitEncoder);

    /* End of Outputs for SubSystem: '<S198>/16Bit Encoder' */

    /* Outputs for Atomic SubSystem: '<S198>/16Bit Encoder1' */
    CtApAEM_u6BitEncoder
      (CtApAEM_B.Gerer_etat_Reveil_partiel_esc_c.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_m4.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_gv.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_mj.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_n2.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_d.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_e.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_hk.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_k.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_h.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_mk.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_a.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_j.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_m.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_n.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_g.RelationalOperator,
       &CtApAEM_B.u6BitEncoder1);

    /* End of Outputs for SubSystem: '<S198>/16Bit Encoder1' */

    /* Outputs for Atomic SubSystem: '<S199>/16Bit Decoder' */
    CtApAEM_u6BitDecoder(CtApAEM_B.u6BitEncoder.Sum, &CtApAEM_B.u6BitDecoder_f);

    /* End of Outputs for SubSystem: '<S199>/16Bit Decoder' */

    /* Outputs for Atomic SubSystem: '<S199>/16Bit Decoder1' */
    CtApAEM_u6BitDecoder(CtApAEM_B.u6BitEncoder1.Sum, &CtApAEM_B.u6BitDecoder1);

    /* End of Outputs for SubSystem: '<S199>/16Bit Decoder1' */

    /* Logic: '<S100>/Logical Operator1' incorporates:
     *  Constant: '<S199>/TRANSITORY_STATE'
     *  Logic: '<S199>/Logical Operator2'
     *  Logic: '<S199>/Logical Operator3'
     *  RelationalOperator: '<S199>/Relational Operator7'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    rtb_LogicalOperator1_f = (CtApAEM_B.LogicalOperator4_b ||
      ((CtApAEM_DW.UnitDelay1_DSTATE_o == 3) &&
       (CtApAEM_B.u6BitDecoder_f.RelationalOperator ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator1 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator2 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator3 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator4 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator5 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator6 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator7 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator8 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator9 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator10 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator11 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator12 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator13 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator14 ||
        CtApAEM_B.u6BitDecoder_f.DataTypeConversion)));

    /* Outputs for Atomic SubSystem: '<S12>/TurnOnDelay' */

    /* Logic: '<S12>/Logical Operator12' incorporates:
     *  Constant: '<S12>/TRANSITORY_STATE'
     *  Constant: '<S12>/UCE_Sldxxms_PdV'
     *  Constant: '<S12>/UCE_tiTransitoryDeac_C'
     *  Logic: '<S12>/Logical Operator1'
     *  Logic: '<S12>/Logical Operator2'
     *  RelationalOperator: '<S12>/Relational Operator7'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    CtApAEM_TurnOnDelay1((!rtb_LogicalOperator1) && (!rtb_LogicalOperator5_mj) &&
                         (!rtb_LogicalOperator1_f) &&
                         (CtApAEM_DW.UnitDelay1_DSTATE_o == 3), (real_T)
                         Rte_CData_UCE_tiTransitoryDeac_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay_i, &CtApAEM_DW.TurnOnDelay_i);

    /* End of Outputs for SubSystem: '<S12>/TurnOnDelay' */

    /* Outputs for Atomic SubSystem: '<S13>/TurnOnDelay' */

    /* RelationalOperator: '<S13>/Relational Operator7' incorporates:
     *  Constant: '<S13>/NTERNAL_PARTIAL_WAKEUP'
     *  Constant: '<S13>/UCE_Sldxxms_PdV'
     *  Constant: '<S13>/UCE_tiMaxTiIntPtlWku_C'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    CtApAEM_TurnOnDelay1(CtApAEM_DW.UnitDelay1_DSTATE_o == 2, (real_T)
                         Rte_CData_UCE_tiMaxTiIntPtlWku_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay_j, &CtApAEM_DW.TurnOnDelay_j);

    /* End of Outputs for SubSystem: '<S13>/TurnOnDelay' */

    /* Chart: '<S14>/F01_05_01_Machine_etats_RCD' incorporates:
     *  Constant: '<S199>/PARTIAL_WAKEUP'
     *  Logic: '<S100>/Logical Operator2'
     *  Logic: '<S100>/Logical Operator3'
     *  Logic: '<S13>/Logical Operator12'
     *  Logic: '<S13>/Logical Operator2'
     *  Logic: '<S13>/Logical Operator3'
     *  Logic: '<S199>/Logical Operator4'
     *  Logic: '<S199>/Logical Operator9'
     *  RelationalOperator: '<S199>/Relational Operator1'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  Switch: '<S100>/Switch2'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    /* Gateway: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_05_Calculer_ETAT_APPLICATIF_UCE_RCD/F01_05_01_Machine_etats_RCD */
    /* During: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_05_Calculer_ETAT_APPLICATIF_UCE_RCD/F01_05_01_Machine_etats_RCD */
    if (CtApAEM_DW.is_active_c15_CtApAEM == 0U) {
      /* Entry: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_05_Calculer_ETAT_APPLICATIF_UCE_RCD/F01_05_01_Machine_etats_RCD */
      CtApAEM_DW.is_active_c15_CtApAEM = 1U;

      /* Entry Internal: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_05_Calculer_ETAT_APPLICATIF_UCE_RCD/F01_05_01_Machine_etats_RCD */
      /* Transition: '<S520>:7' */
      CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Transitoire;

      /* Entry 'Transitoire': '<S520>:2' */
      CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_TRANSITORY_STATE;
    } else {
      switch (CtApAEM_DW.is_c15_CtApAEM) {
       case C_IN_Preparation_Mise_en_veille:
        /* During 'Preparation_Mise_en_veille': '<S520>:1' */
        if ((!rtb_LogicalOperator1_f) && rtb_LogicalOperator1) {
          /* Transition: '<S520>:22' */
          CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Transitoire;

          /* Entry 'Transitoire': '<S520>:2' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_TRANSITORY_STATE;
        } else {
          if (rtb_LogicalOperator1_f) {
            /* Transition: '<S520>:21' */
            CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Reveil_partiel;

            /* Entry 'Reveil_partiel': '<S520>:4' */
            CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_PARTIAL_WAKEUP;
          }
        }
        break;

       case CtApA_IN_Reveil_Partiel_interne:
        /* During 'Reveil_Partiel_interne': '<S520>:3' */
        if (((!rtb_LogicalOperator1) && (!rtb_LogicalOperator1_f)) ||
            CtApAEM_B.TurnOnDelay_j.Logic[1]) {
          /* Transition: '<S520>:13' */
          CtApAEM_DW.is_c15_CtApAEM = C_IN_Preparation_Mise_en_veille;

          /* Entry 'Preparation_Mise_en_veille': '<S520>:1' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_SHUTDOWN_PREPARATION;
        } else if (rtb_LogicalOperator1_f) {
          /* Transition: '<S520>:19' */
          CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Reveil_partiel;

          /* Entry 'Reveil_partiel': '<S520>:4' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_PARTIAL_WAKEUP;
        } else {
          if (rtb_LogicalOperator1) {
            /* Transition: '<S520>:14' */
            CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Transitoire;

            /* Entry 'Transitoire': '<S520>:2' */
            CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_TRANSITORY_STATE;
          }
        }
        break;

       case CtApAEM_IN_Reveil_partiel:
        /* During 'Reveil_partiel': '<S520>:4' */
        if (((!Rte_CData_UCE_noUCETyp_C()) || CtApAEM_B.LogicalOperator2) &&
            ((CtApAEM_DW.UnitDelay1_DSTATE_o == 1) &&
             (!(CtApAEM_B.u6BitDecoder1.RelationalOperator ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator1 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator2 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator3 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator4 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator5 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator6 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator7 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator8 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator9 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator10 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator11 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator12 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator13 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator14 ||
                CtApAEM_B.u6BitDecoder1.DataTypeConversion))) &&
            (!rtb_LogicalOperator5_mj)) {
          /* Transition: '<S520>:18' */
          CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Transitoire;

          /* Entry 'Transitoire': '<S520>:2' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_TRANSITORY_STATE;
        } else {
          if (rtb_LogicalOperator5_mj) {
            /* Transition: '<S520>:10' */
            CtApAEM_DW.is_c15_CtApAEM = CtA_IN_Reveil_principal_nominal;

            /* Entry 'Reveil_principal_nominal': '<S520>:5' */
            CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_NOMINAL_MAIN_WAKEUP;
          }
        }
        break;

       case CtA_IN_Reveil_principal_degrade:
        /* During 'Reveil_principal_degrade': '<S520>:6' */
        if (CtApAEM_B.TurnOnDelay.Logic[1]) {
          /* Transition: '<S520>:8' */
          CtApAEM_DW.is_c15_CtApAEM = CtA_IN_Reveil_principal_nominal;

          /* Entry 'Reveil_principal_nominal': '<S520>:5' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_NOMINAL_MAIN_WAKEUP;
        } else {
          if (CtApAEM_B.TurnOnDelay_a.Logic[1]) {
            /* Transition: '<S520>:17' */
            CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Transitoire;

            /* Entry 'Transitoire': '<S520>:2' */
            CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_TRANSITORY_STATE;
          }
        }
        break;

       case CtA_IN_Reveil_principal_nominal:
        /* During 'Reveil_principal_nominal': '<S520>:5' */
        if (CtApAEM_B.TurnOnDelay_g.Logic[1]) {
          /* Transition: '<S520>:20' */
          CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Transitoire;

          /* Entry 'Transitoire': '<S520>:2' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_TRANSITORY_STATE;
        } else {
          if (rtb_Switch2_ab) {
            /* Transition: '<S520>:9' */
            CtApAEM_DW.is_c15_CtApAEM = CtA_IN_Reveil_principal_degrade;

            /* Entry 'Reveil_principal_degrade': '<S520>:6' */
            CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_DEGRADED_MAIN_WAKEUP;
          }
        }
        break;

       default:
        /* During 'Transitoire': '<S520>:2' */
        if (CtApAEM_B.TurnOnDelay_i.Logic[1]) {
          /* Transition: '<S520>:12' */
          CtApAEM_DW.is_c15_CtApAEM = C_IN_Preparation_Mise_en_veille;

          /* Entry 'Preparation_Mise_en_veille': '<S520>:1' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_SHUTDOWN_PREPARATION;
        } else if (rtb_LogicalOperator5_mj) {
          /* Transition: '<S520>:11' */
          CtApAEM_DW.is_c15_CtApAEM = CtA_IN_Reveil_principal_nominal;

          /* Entry 'Reveil_principal_nominal': '<S520>:5' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_NOMINAL_MAIN_WAKEUP;
        } else {
          if (rtb_LogicalOperator1_f) {
            /* Transition: '<S520>:15' */
            CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Reveil_partiel;

            /* Entry 'Reveil_partiel': '<S520>:4' */
            CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_PARTIAL_WAKEUP;
          }
        }
        break;
      }
    }

    /* End of Chart: '<S14>/F01_05_01_Machine_etats_RCD' */

    /* Outputs for Enabled SubSystem: '<S6>/F01_06_Gerer_CMD_LIGNE_RCD_par_type1' incorporates:
     *  EnablePort: '<S15>/Enable'
     */
    if (Rte_CData_UCE_noUCETyp_C()) {
      if (!CtApAEM_DW.F01_06_Gerer_CMD_LIGNE_RCD_par_) {
        /* InitializeConditions for UnitDelay: '<S15>/Unit Delay7' */
        CtApAEM_DW.UnitDelay7_DSTATE = false;

        /* InitializeConditions for UnitDelay: '<S521>/Unit Delay2' */
        CtApAEM_DW.UnitDelay2_DSTATE = false;

        /* InitializeConditions for UnitDelay: '<S523>/UnitDelay1' */
        CtApAEM_DW.UnitDelay1_DSTATE = 0.0;

        /* SystemReset for Atomic SubSystem: '<S527>/TurnOnDelay1' */
        CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay1_df);

        /* End of SystemReset for SubSystem: '<S527>/TurnOnDelay1' */

        /* SystemReset for Atomic SubSystem: '<S527>/TurnOnDelay2' */
        CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay2_n);

        /* End of SystemReset for SubSystem: '<S527>/TurnOnDelay2' */

        /* SystemReset for Chart: '<S528>/F01_05_02_01_Automate_LIGNE_RCD_CC_masse_pour_GD' */
        CtApAEM_DW.is_active_c5_CtApAEM = 0U;
        CtApAEM_DW.is_c5_CtApAEM = CtApAEM_IN_NO_ACTIVE_CHILD;
        CtApAEM_B.UCE_bDgoRCDLineScg = false;

        /* SystemReset for Atomic SubSystem: '<S523>/rising_edge' */
        CtApAEM_rising_edge_Reset(&CtApAEM_DW.rising_edge_e);

        /* End of SystemReset for SubSystem: '<S523>/rising_edge' */
        CtApAEM_DW.F01_06_Gerer_CMD_LIGNE_RCD_par_ = true;
      }

      /* Outputs for Atomic SubSystem: '<S527>/TurnOnDelay1' */

      /* Logic: '<S527>/Logical Operator4' incorporates:
       *  Constant: '<S527>/UCE_Sldxxms_PdV'
       *  Constant: '<S527>/UCE_tiRCDLineScgDet_C'
       *  RelationalOperator: '<S627>/Relational Operator'
       *  UnitDelay: '<S15>/Unit Delay7'
       */
      CtApAEM_TurnOnDelay1((!rtb_LogicalOperator1) &&
                           CtApAEM_DW.UnitDelay7_DSTATE, (real_T)
                           Rte_CData_UCE_tiRCDLineScgDet_C(), 0.04,
                           &CtApAEM_B.TurnOnDelay1_df,
                           &CtApAEM_DW.TurnOnDelay1_df);

      /* End of Outputs for SubSystem: '<S527>/TurnOnDelay1' */

      /* Outputs for Atomic SubSystem: '<S527>/TurnOnDelay2' */

      /* RelationalOperator: '<S627>/Relational Operator' incorporates:
       *  Constant: '<S527>/UCE_Sldxxms_PdV'
       *  Constant: '<S527>/UCE_tiRCDLineScgReh_C'
       */
      CtApAEM_TurnOnDelay1(rtb_LogicalOperator1, (real_T)
                           Rte_CData_UCE_tiRCDLineScgReh_C(), 0.04,
                           &CtApAEM_B.TurnOnDelay2_n, &CtApAEM_DW.TurnOnDelay2_n);

      /* End of Outputs for SubSystem: '<S527>/TurnOnDelay2' */

      /* Chart: '<S528>/F01_05_02_01_Automate_LIGNE_RCD_CC_masse_pour_GD' */
      /* Gateway: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_06_Gerer_CMD_LIGNE_RCD_par_type1/F01_06_02_Traiter_LIGNE_RCD_CC_masse/F01_06_02_02_Renseigner_LIGNE_RCD_CC_masse_pour_GD/F01_05_02_01_Automate_LIGNE_RCD_CC_masse_pour_GD */
      /* During: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_06_Gerer_CMD_LIGNE_RCD_par_type1/F01_06_02_Traiter_LIGNE_RCD_CC_masse/F01_06_02_02_Renseigner_LIGNE_RCD_CC_masse_pour_GD/F01_05_02_01_Automate_LIGNE_RCD_CC_masse_pour_GD */
      if (CtApAEM_DW.is_active_c5_CtApAEM == 0U) {
        /* Entry: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_06_Gerer_CMD_LIGNE_RCD_par_type1/F01_06_02_Traiter_LIGNE_RCD_CC_masse/F01_06_02_02_Renseigner_LIGNE_RCD_CC_masse_pour_GD/F01_05_02_01_Automate_LIGNE_RCD_CC_masse_pour_GD */
        CtApAEM_DW.is_active_c5_CtApAEM = 1U;

        /* Entry Internal: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_06_Gerer_CMD_LIGNE_RCD_par_type1/F01_06_02_Traiter_LIGNE_RCD_CC_masse/F01_06_02_02_Renseigner_LIGNE_RCD_CC_masse_pour_GD/F01_05_02_01_Automate_LIGNE_RCD_CC_masse_pour_GD */
        /* Transition: '<S540>:4' */
        CtApAEM_DW.is_c5_CtApAEM = CtApAEM_IN_Defaut_inactif;

        /* Entry 'Defaut_inactif': '<S540>:2' */
        CtApAEM_B.UCE_bDgoRCDLineScg = CtApAEM_inactive;
      } else if (CtApAEM_DW.is_c5_CtApAEM == CtApAEM_IN_Defaut_actif) {
        /* During 'Defaut_actif': '<S540>:1' */
        if (CtApAEM_B.TurnOnDelay2_n.Logic[1]) {
          /* Transition: '<S540>:5' */
          CtApAEM_DW.is_c5_CtApAEM = CtApAEM_IN_Defaut_inactif;

          /* Entry 'Defaut_inactif': '<S540>:2' */
          CtApAEM_B.UCE_bDgoRCDLineScg = CtApAEM_inactive;
        }
      } else {
        /* During 'Defaut_inactif': '<S540>:2' */
        if (CtApAEM_B.TurnOnDelay1_df.Logic[1]) {
          /* Transition: '<S540>:3' */
          CtApAEM_DW.is_c5_CtApAEM = CtApAEM_IN_Defaut_actif;

          /* Entry 'Defaut_actif': '<S540>:1' */
          CtApAEM_B.UCE_bDgoRCDLineScg = CtApAEM_active;
        }
      }

      /* End of Chart: '<S528>/F01_05_02_01_Automate_LIGNE_RCD_CC_masse_pour_GD' */

      /* Outputs for Atomic SubSystem: '<S523>/DetectSat' */

      /* Constant: '<S523>/Constant5' incorporates:
       *  Constant: '<S521>/UCE_tiRCDLineCmdAcv_C'
       *  Constant: '<S523>/Constant7'
       */
      CtApAEM_DetectSat(10000.0, (real_T)Rte_CData_UCE_tiRCDLineCmdAcv_C(), 0.0,
                        &CtApAEM_B.DetectSat_o);

      /* End of Outputs for SubSystem: '<S523>/DetectSat' */

      /* Outputs for Atomic SubSystem: '<S523>/rising_edge' */

      /* Logic: '<S521>/Logical Operator4' incorporates:
       *  Constant: '<S521>/PARTIAL_WAKEUP'
       *  RelationalOperator: '<S521>/Relational Operator7'
       *  UnitDelay: '<S521>/Unit Delay2'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      CtApAEM_rising_edge(CtApAEM_DW.UnitDelay2_DSTATE &&
                          (CtApAEM_DW.UnitDelay1_DSTATE_o == 1),
                          &CtApAEM_B.rising_edge_e, &CtApAEM_DW.rising_edge_e);

      /* End of Outputs for SubSystem: '<S523>/rising_edge' */

      /* Switch: '<S523>/Switch' incorporates:
       *  Constant: '<S521>/UCE_Sldxxms_PdV'
       *  Logic: '<S523>/Logical Operator'
       *  Sum: '<S523>/Sum1'
       *  Switch: '<S523>/Switch1'
       *  UnitDelay: '<S523>/UnitDelay1'
       */
      if (CtApAEM_B.rising_edge_e.LogicalOperator) {
        rtb_Switch2 = 0.04 + CtApAEM_B.DetectSat_o.MinMax2;
      } else {
        rtb_Switch2 = CtApAEM_DW.UnitDelay1_DSTATE;
      }

      /* End of Switch: '<S523>/Switch' */

      /* Logic: '<S521>/Logical Operator6' incorporates:
       *  Constant: '<S521>/UCE_Sldxxms_PdV'
       *  Constant: '<S523>/Constant1'
       *  Constant: '<S523>/Constant6'
       *  Logic: '<S521>/Logical Operator1'
       *  RelationalOperator: '<S523>/Relational Operator'
       *  Sum: '<S523>/Sum2'
       */
      CtApAEM_B.LogicalOperator6 = ((!CtApAEM_B.UCE_bDgoRCDLineScg) &&
        ((rtb_Switch2 - 2.2204460492503131E-16) - 0.04 > 0.0));

      /* Outputs for Atomic SubSystem: '<S523>/DetectSat1' */

      /* Sum: '<S523>/Sum2' incorporates:
       *  Constant: '<S521>/UCE_Sldxxms_PdV'
       *  Constant: '<S523>/Constant6'
       *  Constant: '<S523>/Constant8'
       */
      CtApAEM_DetectSat(CtApAEM_B.DetectSat_o.MinMax2, (rtb_Switch2 -
        2.2204460492503131E-16) - 0.04, 0.0, &CtApAEM_B.DetectSat1);

      /* End of Outputs for SubSystem: '<S523>/DetectSat1' */

      /* Logic: '<S529>/Logical Operator4' incorporates:
       *  Constant: '<S529>/DEGRADED_MAIN_WAKEUP'
       *  Constant: '<S529>/INTERNAL_PARTIAL_WAKEUP'
       *  Constant: '<S529>/NOMINAL_MAIN_WAKEUP'
       *  Constant: '<S529>/PARTIAL_WAKEUP'
       *  Constant: '<S529>/TRANSITORY_STATE'
       *  RelationalOperator: '<S529>/Relational Operator1'
       *  RelationalOperator: '<S529>/Relational Operator2'
       *  RelationalOperator: '<S529>/Relational Operator3'
       *  RelationalOperator: '<S529>/Relational Operator4'
       *  RelationalOperator: '<S529>/Relational Operator5'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      CtApAEM_B.LogicalOperator4 = ((CtApAEM_DW.UnitDelay1_DSTATE_o == 1) ||
        (CtApAEM_DW.UnitDelay1_DSTATE_o == 2) || (CtApAEM_DW.UnitDelay1_DSTATE_o
        == 3) || (CtApAEM_DW.UnitDelay1_DSTATE_o == 4) ||
        (CtApAEM_DW.UnitDelay1_DSTATE_o == 5));

      /* SignalConversion: '<S15>/OutportBufferForUCE_bDgoRCDLineScg' */
      CtApAEM_B.OutportBufferForUCE_bDgoRCDL_gt = CtApAEM_B.UCE_bDgoRCDLineScg;

      /* Update for UnitDelay: '<S15>/Unit Delay7' */
      CtApAEM_DW.UnitDelay7_DSTATE = CtApAEM_B.LogicalOperator6;

      /* Update for UnitDelay: '<S521>/Unit Delay2' */
      CtApAEM_DW.UnitDelay2_DSTATE = CtApAEM_B.LogicalOperator4_b;

      /* Update for UnitDelay: '<S523>/UnitDelay1' */
      CtApAEM_DW.UnitDelay1_DSTATE = CtApAEM_B.DetectSat1.MinMax2;
    } else {
      if (CtApAEM_DW.F01_06_Gerer_CMD_LIGNE_RCD_par_) {
        /* Disable for Outport: '<S15>/UCE_bRCDLineCmd' */
        CtApAEM_B.LogicalOperator6 = false;

        /* Disable for Outport: '<S15>/UCE_bDgoRCDLineScg' */
        CtApAEM_B.OutportBufferForUCE_bDgoRCDL_gt = false;

        /* Disable for Outport: '<S15>/UCE_bMonRunRCDLineScg' */
        CtApAEM_B.LogicalOperator4 = false;
        CtApAEM_DW.F01_06_Gerer_CMD_LIGNE_RCD_par_ = false;
      }
    }

    /* End of Outputs for SubSystem: '<S6>/F01_06_Gerer_CMD_LIGNE_RCD_par_type1' */

    /* Logic: '<S541>/Logical Operator1' incorporates:
     *  Constant: '<S541>/DEGRADED_MAIN_WAKEUP'
     *  Constant: '<S541>/NOMINAL_MAIN_WAKEUP'
     *  RelationalOperator: '<S541>/Relational Operator1'
     *  RelationalOperator: '<S541>/Relational Operator3'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    rtb_LogicalOperator1_f = ((CtApAEM_DW.UnitDelay1_DSTATE_o == 4) ||
      (CtApAEM_DW.UnitDelay1_DSTATE_o == 5));

    /* Switch: '<S541>/Switch1' incorporates:
     *  Constant: '<S541>/Electronic_Integration_Mode'
     *  Constant: '<S541>/PARTIAL_WAKEUP'
     *  Constant: '<S541>/TRANSITORY_STATE'
     *  Logic: '<S541>/Logical Operator10'
     *  Logic: '<S541>/Logical Operator11'
     *  Logic: '<S541>/Logical Operator12'
     *  Logic: '<S541>/Logical Operator3'
     *  RelationalOperator: '<S541>/Relational Operator2'
     *  RelationalOperator: '<S541>/Relational Operator4'
     *  RelationalOperator: '<S541>/Relational Operator6'
     *  RelationalOperator: '<S627>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (rtb_LogicalOperator1 && rtb_LogicalOperator1_f &&
        rtb_LogicalOperator12_l) {
      CtApAEM_B.Switch1_k = 2;
    } else {
      CtApAEM_B.Switch1_k = (rtb_LogicalOperator1_f ||
        (((CtApAEM_DW.UnitDelay1_DSTATE_o == 3) ||
          (CtApAEM_DW.UnitDelay1_DSTATE_o == 1)) && rtb_LogicalOperator4));
    }

    /* End of Switch: '<S541>/Switch1' */

    /* RelationalOperator: '<S17>/Relational Operator5' incorporates:
     *  Constant: '<S17>/SHUTDOWN_PREPARATION'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    rtb_LogicalOperator12_l = (CtApAEM_DW.UnitDelay1_DSTATE_o == 7);

    /* Outputs for Atomic SubSystem: '<S17>/TurnOnDelay1' */

    /* Constant: '<S17>/UCE_tiMinTiShutDownPrep_C' incorporates:
     *  Constant: '<S17>/UCE_Sldxxms_PdV'
     */
    CtApAEM_TurnOnDelay1(rtb_LogicalOperator12_l, (real_T)
                         Rte_CData_UCE_tiMinTiShutDownPrep_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay1_d, &CtApAEM_DW.TurnOnDelay1_d);

    /* End of Outputs for SubSystem: '<S17>/TurnOnDelay1' */

    /* Logic: '<S17>/Logical Operator2' */
    rtb_LogicalOperator1_f = CtApAEM_B.TurnOnDelay1_d.Logic[1];

    /* Outputs for Atomic SubSystem: '<S17>/TurnOnDelay2' */

    /* Constant: '<S17>/UCE_tiMaxTiShutDownPrep_C' incorporates:
     *  Constant: '<S17>/UCE_Sldxxms_PdV'
     */
    CtApAEM_TurnOnDelay1(rtb_LogicalOperator12_l, (real_T)
                         Rte_CData_UCE_tiMaxTiShutDownPrep_C(), 0.04,
                         &CtApAEM_B.TurnOnDelay2_g, &CtApAEM_DW.TurnOnDelay2_g);

    /* End of Outputs for SubSystem: '<S17>/TurnOnDelay2' */

    /* Logic: '<S17>/Logical Operator1' */
    CtApAEM_B.LogicalOperator1_m = (rtb_LogicalOperator1_f ||
      CtApAEM_B.TurnOnDelay2_g.Logic[1]);

    /* SignalConversion: '<S6>/OutportBufferForUCE_bDgoMainWkuDisrd' */
    CtApAEM_B.OutportBufferForUCE_bDgoMainW_m = CtApAEM_B.UCE_bDgoMainWkuDisrd;

    /* SignalConversion: '<S6>/OutportBufferForUCE_bDgoMainWkuIncst' */
    CtApAEM_B.OutportBufferForUCE_bDgoMain_kx = CtApAEM_B.UCE_bDgoMainWkuIncst;

    /* SignalConversion: '<S6>/OutportBufferForUCE_bDgoRCDLineScg' */
    CtApAEM_B.OutportBufferForUCE_bDgoRCDLi_g =
      CtApAEM_B.OutportBufferForUCE_bDgoRCDL_gt;

    /* SignalConversion: '<S6>/OutportBufferForUCE_bMonRunRCDLineScg' */
    CtApAEM_B.OutportBufferForUCE_bMonRunRC_e = CtApAEM_B.LogicalOperator4;

    /* SignalConversion: '<S6>/OutportBufferForUCE_bRCDLineCmd' */
    CtApAEM_B.OutportBufferForUCE_bRCDLineC_i = CtApAEM_B.LogicalOperator6;

    /* SignalConversion: '<S6>/OutportBufferForUCE_bfMstPtlWkuReq' */
    CtApAEM_B.OutportBufferForUCE_bfMstPtlW_c = CtApAEM_B.uBitEncoder1.Sum;

    /* SignalConversion: '<S6>/OutportBufferForUCE_stRCDSt' */
    CtApAEM_B.OutportBufferForUCE_stRCDSt = CtApAEM_B.UCE_stRCDSt;

    /* Update for UnitDelay: '<S6>/Unit Delay1' */
    CtApAEM_DW.UnitDelay1_DSTATE_o = CtApAEM_B.UCE_stRCDSt;
  } else {
    if (CtApAEM_DW.F01_Gerer_PdV_RCD_MODE) {
      /* Disable for Enabled SubSystem: '<S11>/F01_02_01_Gerer_Reveils_partiels_maitres' */
      if (CtApAEM_DW.F01_02_01_Gerer_Reveils_partiel) {
        /* Disable for Outport: '<S98>/UCE_bfMstPtlWkuReq' */
        CtApAEM_B.uBitEncoder1.Sum = 0;
        CtApAEM_DW.F01_02_01_Gerer_Reveils_partiel = false;
      }

      /* End of Disable for SubSystem: '<S11>/F01_02_01_Gerer_Reveils_partiels_maitres' */

      /* Disable for Enabled SubSystem: '<S6>/F01_06_Gerer_CMD_LIGNE_RCD_par_type1' */
      if (CtApAEM_DW.F01_06_Gerer_CMD_LIGNE_RCD_par_) {
        /* Disable for Outport: '<S15>/UCE_bRCDLineCmd' */
        CtApAEM_B.LogicalOperator6 = false;

        /* Disable for Outport: '<S15>/UCE_bDgoRCDLineScg' */
        CtApAEM_B.OutportBufferForUCE_bDgoRCDL_gt = false;

        /* Disable for Outport: '<S15>/UCE_bMonRunRCDLineScg' */
        CtApAEM_B.LogicalOperator4 = false;
        CtApAEM_DW.F01_06_Gerer_CMD_LIGNE_RCD_par_ = false;
      }

      /* End of Disable for SubSystem: '<S6>/F01_06_Gerer_CMD_LIGNE_RCD_par_type1' */

      /* Disable for Outport: '<S6>/UCE_stRCDSt' */
      CtApAEM_B.OutportBufferForUCE_stRCDSt = 3;

      /* Disable for Outport: '<S6>/UCE_bRCDLineCmd' */
      CtApAEM_B.OutportBufferForUCE_bRCDLineC_i = false;

      /* Disable for Outport: '<S6>/UCE_bfMstPtlWkuReq' */
      CtApAEM_B.OutportBufferForUCE_bfMstPtlW_c = 0;

      /* Disable for Outport: '<S6>/UCE_bfSlavePtlWkuSt' */
      CtApAEM_B.u6BitEncoder1.Sum = 0;

      /* Disable for Outport: '<S6>/UCE_stRCDCAN1ComReq' */
      CtApAEM_B.Switch1_k = 0;

      /* Disable for Outport: '<S6>/UCE_bRCDShutDownAuth' */
      CtApAEM_B.LogicalOperator1_m = false;

      /* Disable for Outport: '<S6>/UCE_bDgoMainWkuDisrd' */
      CtApAEM_B.OutportBufferForUCE_bDgoMainW_m = false;

      /* Disable for Outport: '<S6>/UCE_bMonRunMainWkuDisrd' */
      CtApAEM_B.LogicalOperator1_g = false;

      /* Disable for Outport: '<S6>/UCE_bDgoMainWkuIncst' */
      CtApAEM_B.OutportBufferForUCE_bDgoMain_kx = false;

      /* Disable for Outport: '<S6>/UCE_bMonRunMainWkuIncst' */
      CtApAEM_B.LogicalOperator1_h = false;

      /* Disable for Outport: '<S6>/UCE_bDgoRCDLineScg' */
      CtApAEM_B.OutportBufferForUCE_bDgoRCDLi_g = false;

      /* Disable for Outport: '<S6>/UCE_bMonRunRCDLineScg' */
      CtApAEM_B.OutportBufferForUCE_bMonRunRC_e = false;
      CtApAEM_DW.F01_Gerer_PdV_RCD_MODE = false;
    }
  }

  /* End of Outputs for SubSystem: '<S3>/F01_Gerer_PdV_RCD' */

  /* Outputs for Enabled SubSystem: '<S3>/F02_Gerer_PdV_APC' incorporates:
   *  EnablePort: '<S7>/Enable'
   */
  if (CtApAEM_DW.F02_Gerer_PdV_APC_MODE) {
    /* Disable for Outport: '<S7>/UCE_stAPCSt' */
    CtApAEM_B.OutportBufferForUCE_stAPCSt = 12;
    CtApAEM_DW.F02_Gerer_PdV_APC_MODE = false;
  }

  /* End of Outputs for SubSystem: '<S3>/F02_Gerer_PdV_APC' */

  /* Switch: '<S9>/Switch1' incorporates:
   *  Constant: '<S8>/UCE_noUCETyp_C'
   */
  if (Rte_CData_UCE_noUCETyp_C()) {
    rtb_DataTypeConversion = CtApAEM_B.OutportBufferForUCE_stRCDSt;
  } else {
    rtb_DataTypeConversion = CtApAEM_B.OutportBufferForUCE_stAPCSt;
  }

  /* End of Switch: '<S9>/Switch1' */

  /* Logic: '<S9>/Logical Operator12' incorporates:
   *  Constant: '<S9>/DEGRADED_MAIN_WAKEUP'
   *  Constant: '<S9>/MAIN_WAKEUP_APC'
   *  Constant: '<S9>/NOMINAL_MAIN_WAKEUP'
   *  RelationalOperator: '<S9>/Relational Operator1'
   *  RelationalOperator: '<S9>/Relational Operator2'
   *  RelationalOperator: '<S9>/Relational Operator4'
   */
  CtApAEM_B.LogicalOperator12 = ((rtb_DataTypeConversion == 4) ||
    (rtb_DataTypeConversion == 5) || (rtb_DataTypeConversion == 12));

  /* Outputs for Atomic SubSystem: '<S9>/falling_edge' */
  CtApAEM_falling_edge1(CtApAEM_B.LogicalOperator12, &CtApAEM_B.falling_edge,
                        &CtApAEM_DW.falling_edge);

  /* End of Outputs for SubSystem: '<S9>/falling_edge' */

  /* Outputs for Atomic SubSystem: '<S9>/BasculeRS' */
  /* Logic: '<S620>/Logical Operator1' incorporates:
   *  Logic: '<S620>/Logical Operator3'
   *  Logic: '<S620>/Logical Operator7'
   *  UnitDelay: '<S620>/UnitDelay'
   */
  rtb_LogicalOperator12_l = ((CtApAEM_DW.UnitDelay_DSTATE ||
    CtApAEM_B.falling_edge.LogicalOperator) && (!CtApAEM_B.LogicalOperator12));

  /* Update for UnitDelay: '<S620>/UnitDelay' */
  CtApAEM_DW.UnitDelay_DSTATE = rtb_LogicalOperator12_l;

  /* End of Outputs for SubSystem: '<S9>/BasculeRS' */

  /* Switch: '<S9>/Switch2' incorporates:
   *  Constant: '<S8>/UCE_noUCETyp_C'
   */
  if (Rte_CData_UCE_noUCETyp_C()) {
    rowIdx = CtApAEM_B.Switch1_k;
  } else {
    rowIdx = CtApAEM_B.Switch1_a;
  }

  /* Outport: '<Root>/PpCANComRequest_DeCANComRequest' incorporates:
   *  Switch: '<S9>/Switch2'
   */
  Rte_Write_PpCANComRequest_DeCANComRequest(rowIdx);

  /* Switch: '<S9>/Switch3' incorporates:
   *  Constant: '<S8>/UCE_noUCETyp_C'
   */
  if (Rte_CData_UCE_noUCETyp_C()) {
    rtb_LogicalOperator1_f = CtApAEM_B.LogicalOperator1_m;
  } else {
    rtb_LogicalOperator1_f = CtApAEM_B.LogicalOperator1;
  }

  /* Outport: '<Root>/PpShutdownAuthorization_DeShutdownAuthorization' incorporates:
   *  Switch: '<S9>/Switch3'
   */
  Rte_Write_PpShutdownAuthorization_DeShutdownAuthorization
    (rtb_LogicalOperator1_f);

  /* Switch: '<S9>/Switch7' incorporates:
   *  Logic: '<S9>/Logical Operator1'
   *  Logic: '<S9>/Logical Operator2'
   *  Logic: '<S9>/Logical Operator3'
   *  RelationalOperator: '<S9>/Relational Operator10'
   *  RelationalOperator: '<S9>/Relational Operator3'
   *  RelationalOperator: '<S9>/Relational Operator6'
   *  RelationalOperator: '<S9>/Relational Operator7'
   *  RelationalOperator: '<S9>/Relational Operator8'
   *  RelationalOperator: '<S9>/Relational Operator9'
   *  Switch: '<S9>/Switch8'
   *  Switch: '<S9>/Switch9'
   */
  if ((!CtApAEM_B.LogicalOperator12) && (!rtb_LogicalOperator12_l)) {
    /* Outport: '<Root>/PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup' incorporates:
     *  Constant: '<S9>/PRE_MAIN_WAKEUP'
     */
    Rte_Write_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup(0);
  } else {
    if (CtApAEM_B.LogicalOperator12 && (!rtb_LogicalOperator12_l)) {
      /* Switch: '<S9>/Switch8' incorporates:
       *  Constant: '<S9>/MAIN_WAKEUP'
       */
      rowIdx = 1;
    } else if ((!CtApAEM_B.LogicalOperator12) && rtb_LogicalOperator12_l) {
      /* Switch: '<S9>/Switch9' incorporates:
       *  Constant: '<S9>/POST_MAIN_WAKEUP'
       *  Switch: '<S9>/Switch8'
       */
      rowIdx = 2;
    } else {
      /* Switch: '<S9>/Switch8' incorporates:
       *  Constant: '<S9>/INAVLIID_VALUE'
       *  Switch: '<S9>/Switch9'
       */
      rowIdx = 3;
    }

    /* Outport: '<Root>/PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup' incorporates:
     *  Logic: '<S9>/Logical Operator3'
     *  RelationalOperator: '<S9>/Relational Operator10'
     *  RelationalOperator: '<S9>/Relational Operator9'
     *  Switch: '<S9>/Switch8'
     *  Switch: '<S9>/Switch9'
     */
    Rte_Write_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup(rowIdx);
  }

  /* End of Switch: '<S9>/Switch7' */

  /* Outport: '<Root>/PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd' incorporates:
   *  SignalConversion: '<S3>/OutportBufferForUCE_bDgoMainWkuDisrd'
   */
  Rte_Write_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd
    (CtApAEM_B.OutportBufferForUCE_bDgoMainW_m);

  /* Outport: '<Root>/PpFaultMainWakeupIncst_DeFaultMainWakeupIncst' incorporates:
   *  SignalConversion: '<S3>/OutportBufferForUCE_bDgoMainWkuIncst'
   */
  Rte_Write_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst
    (CtApAEM_B.OutportBufferForUCE_bDgoMain_kx);

  /* Outport: '<Root>/PpFaultRCDLineSC_DeFaultRCDLineSC' incorporates:
   *  SignalConversion: '<S3>/OutportBufferForUCE_bDgoRCDLineScg'
   */
  Rte_Write_PpFaultRCDLineSC_DeFaultRCDLineSC
    (CtApAEM_B.OutportBufferForUCE_bDgoRCDLi_g);

  /* Outport: '<Root>/PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd' incorporates:
   *  SignalConversion: '<S3>/OutportBufferForUCE_bMonRunMainWkuDisrd'
   */
  Rte_Write_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd
    (CtApAEM_B.LogicalOperator1_g);

  /* Outport: '<Root>/PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst' incorporates:
   *  SignalConversion: '<S3>/OutportBufferForUCE_bMonRunMainWkuIncst'
   */
  Rte_Write_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst
    (CtApAEM_B.LogicalOperator1_h);

  /* Outport: '<Root>/PpMonitoringRCDLineSC_DeMonitoringRCDLineSC' incorporates:
   *  SignalConversion: '<S3>/OutportBufferForUCE_bMonRunRCDLineScg'
   */
  Rte_Write_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC
    (CtApAEM_B.OutportBufferForUCE_bMonRunRC_e);

  /* Outport: '<Root>/PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue' incorporates:
   *  RelationalOperator: '<S631>/Compare'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bRCDLineCmd'
   */
  Rte_Write_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue
    (CtApAEM_B.OutportBufferForUCE_bRCDLineC_i);

  /* Outport: '<Root>/PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup' incorporates:
   *  Constant: '<S634>/Constant'
   *  DataTypeConversion: '<S632>/Data Type Conversion'
   *  RelationalOperator: '<S634>/Compare'
   *  S-Function (sfix_bitop): '<S632>/Bitwise AND'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfMstPtlWkuReq'
   */
  Rte_Write_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup
    ((CtApAEM_B.OutportBufferForUCE_bfMstPtlW_c & 1) == 1);

  /* Outport: '<Root>/PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup' incorporates:
   *  Constant: '<S635>/Constant'
   *  DataTypeConversion: '<S632>/Data Type Conversion1'
   *  RelationalOperator: '<S635>/Compare'
   *  S-Function (sfix_bitop): '<S632>/Bitwise AND1'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfMstPtlWkuReq'
   */
  Rte_Write_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup
    ((CtApAEM_B.OutportBufferForUCE_bfMstPtlW_c & 2) == 2);

  /* Outport: '<Root>/PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup' incorporates:
   *  Constant: '<S636>/Constant'
   *  DataTypeConversion: '<S632>/Data Type Conversion2'
   *  RelationalOperator: '<S636>/Compare'
   *  S-Function (sfix_bitop): '<S632>/Bitwise AND2'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfMstPtlWkuReq'
   */
  Rte_Write_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup
    ((CtApAEM_B.OutportBufferForUCE_bfMstPtlW_c & 4) == 4);

  /* Outport: '<Root>/PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup' incorporates:
   *  Constant: '<S637>/Constant'
   *  DataTypeConversion: '<S632>/Data Type Conversion3'
   *  RelationalOperator: '<S637>/Compare'
   *  S-Function (sfix_bitop): '<S632>/Bitwise AND3'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfMstPtlWkuReq'
   */
  Rte_Write_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup
    ((CtApAEM_B.OutportBufferForUCE_bfMstPtlW_c & 8) == 8);

  /* Outport: '<Root>/PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState' incorporates:
   *  Constant: '<S638>/Constant'
   *  DataTypeConversion: '<S633>/Data Type Conversion'
   *  RelationalOperator: '<S638>/Compare'
   *  S-Function (sfix_bitop): '<S633>/Bitwise AND'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfSlavePtlWkuSt'
   */
  Rte_Write_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState
    ((CtApAEM_B.u6BitEncoder1.Sum & 1) == 1);

  /* Outport: '<Root>/PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState' incorporates:
   *  Constant: '<S639>/Constant'
   *  DataTypeConversion: '<S633>/Data Type Conversion1'
   *  RelationalOperator: '<S639>/Compare'
   *  S-Function (sfix_bitop): '<S633>/Bitwise AND1'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfSlavePtlWkuSt'
   */
  Rte_Write_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState
    ((CtApAEM_B.u6BitEncoder1.Sum & 2) == 2);

  /* Outport: '<Root>/PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState' incorporates:
   *  Constant: '<S640>/Constant'
   *  DataTypeConversion: '<S633>/Data Type Conversion2'
   *  RelationalOperator: '<S640>/Compare'
   *  S-Function (sfix_bitop): '<S633>/Bitwise AND2'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfSlavePtlWkuSt'
   */
  Rte_Write_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState
    ((CtApAEM_B.u6BitEncoder1.Sum & 4) == 4);

  /* Outport: '<Root>/PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState' incorporates:
   *  Constant: '<S641>/Constant'
   *  DataTypeConversion: '<S633>/Data Type Conversion3'
   *  RelationalOperator: '<S641>/Compare'
   *  S-Function (sfix_bitop): '<S633>/Bitwise AND3'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfSlavePtlWkuSt'
   */
  Rte_Write_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState
    ((CtApAEM_B.u6BitEncoder1.Sum & 8) == 8);

  /* End of Outputs for SubSystem: '<S2>/F0_Gerer_PdV_EE_generique' */
  /* End of Outputs for RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' */

  /* Outport: '<Root>/PpECU_WakeupMain_DeECU_WakeupMain' */
  Rte_Write_PpECU_WakeupMain_DeECU_WakeupMain(CtApAEM_B.LogicalOperator12);

  /* RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' incorporates:
   *  SubSystem: '<Root>/RCtApAEM_task10msB_sys'
   */
  /* Outport: '<Root>/PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD' incorporates:
   *  DataTypeConversion: '<S5>/Data Type Conversion'
   */
  Rte_Write_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD
    ((SUPV_ECUElecStateRCD)rtb_DataTypeConversion);

  /* End of Outputs for RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' */
}

/* Model initialize function */
void CtApAEM_Init(void)
{
  /* SystemInitialize for RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msA_at_outport_1' incorporates:
   *  SystemInitialize for SubSystem: '<Root>/RCtApAEM_task10msA_sys'
   */
  /* SystemInitialize for SignalConversion: '<S1>/OutportBufferForPpAppRCDECUState_DeAppRCDECUState_write' */
  CtApAEM_B.OutportBufferForPpAppRCDECUStat = APP_STATE_0;

  /* End of SystemInitialize for RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msA_at_outport_1' */

  /* SystemInitialize for RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' incorporates:
   *  SystemInitialize for SubSystem: '<Root>/RCtApAEM_task10msB_sys'
   */
  /* SystemInitialize for Enabled SubSystem: '<S2>/F0_Gerer_PdV_EE_generique' */
  /* SystemInitialize for Enabled SubSystem: '<S3>/F01_Gerer_PdV_RCD' */

  /* SystemInitialize for Atomic SubSystem: '<S18>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1);

  /* End of SystemInitialize for SubSystem: '<S18>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S18>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2);

  /* End of SystemInitialize for SubSystem: '<S18>/TurnOnDelay2' */

  /* SystemInitialize for Atomic SubSystem: '<S19>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_c);

  /* End of SystemInitialize for SubSystem: '<S19>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S19>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2_k);

  /* End of SystemInitialize for SubSystem: '<S19>/TurnOnDelay2' */

  /* SystemInitialize for Atomic SubSystem: '<S19>/TurnOnDelay3' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay3);

  /* End of SystemInitialize for SubSystem: '<S19>/TurnOnDelay3' */

  /* SystemInitialize for Atomic SubSystem: '<S20>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay);

  /* End of SystemInitialize for SubSystem: '<S20>/TurnOnDelay' */

  /* SystemInitialize for Atomic SubSystem: '<S21>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay_g);

  /* End of SystemInitialize for SubSystem: '<S21>/TurnOnDelay' */

  /* SystemInitialize for Atomic SubSystem: '<S22>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay_a);

  /* End of SystemInitialize for SubSystem: '<S22>/TurnOnDelay' */

  /* SystemInitialize for Atomic SubSystem: '<S65>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_h);

  /* End of SystemInitialize for SubSystem: '<S65>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S79>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_m);

  /* End of SystemInitialize for SubSystem: '<S79>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S79>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2_e);

  /* End of SystemInitialize for SubSystem: '<S79>/TurnOnDelay2' */

  /* SystemInitialize for Atomic SubSystem: '<S79>/TurnOnDelay3' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay3_o);

  /* End of SystemInitialize for SubSystem: '<S79>/TurnOnDelay3' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_16_Gerer_etat_Reveil_partiel_esclave_X16' */

  /* SystemInitialize for Atomic SubSystem: '<S219>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_c);

  /* End of SystemInitialize for SubSystem: '<S219>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_16_Gerer_etat_Reveil_partiel_esclave_X16' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_15_Gerer_etat_Reveil_partiel_esclave_X15' */

  /* SystemInitialize for Atomic SubSystem: '<S218>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_es_m4);

  /* End of SystemInitialize for SubSystem: '<S218>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_15_Gerer_etat_Reveil_partiel_esclave_X15' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_14_Gerer_etat_Reveil_partiel_esclave_X14' */

  /* SystemInitialize for Atomic SubSystem: '<S217>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_es_gv);

  /* End of SystemInitialize for SubSystem: '<S217>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_14_Gerer_etat_Reveil_partiel_esclave_X14' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_13_Gerer_etat_Reveil_partiel_esclave_X13' */

  /* SystemInitialize for Atomic SubSystem: '<S216>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_es_mj);

  /* End of SystemInitialize for SubSystem: '<S216>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_13_Gerer_etat_Reveil_partiel_esclave_X13' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_12_Gerer_etat_Reveil_partiel_esclave_X12' */

  /* SystemInitialize for Atomic SubSystem: '<S215>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_es_n2);

  /* End of SystemInitialize for SubSystem: '<S215>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_12_Gerer_etat_Reveil_partiel_esclave_X12' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_11_Gerer_etat_Reveil_partiel_esclave_X11' */

  /* SystemInitialize for Atomic SubSystem: '<S214>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_d);

  /* End of SystemInitialize for SubSystem: '<S214>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_11_Gerer_etat_Reveil_partiel_esclave_X11' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_10_Gerer_etat_Reveil_partiel_esclave_X10' */

  /* SystemInitialize for Atomic SubSystem: '<S213>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_e);

  /* End of SystemInitialize for SubSystem: '<S213>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_10_Gerer_etat_Reveil_partiel_esclave_X10' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_09_Gerer_etat_Reveil_partiel_esclave_X9' */

  /* SystemInitialize for Atomic SubSystem: '<S212>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_es_hk);

  /* End of SystemInitialize for SubSystem: '<S212>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_09_Gerer_etat_Reveil_partiel_esclave_X9' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_08_Gerer_etat_Reveil_partiel_esclave_X8' */

  /* SystemInitialize for Atomic SubSystem: '<S211>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_k);

  /* End of SystemInitialize for SubSystem: '<S211>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_08_Gerer_etat_Reveil_partiel_esclave_X8' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_07_Gerer_etat_Reveil_partiel_esclave_X7' */

  /* SystemInitialize for Atomic SubSystem: '<S210>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_h);

  /* End of SystemInitialize for SubSystem: '<S210>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_07_Gerer_etat_Reveil_partiel_esclave_X7' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_06_Gerer_etat_Reveil_partiel_esclave_X6' */

  /* SystemInitialize for Atomic SubSystem: '<S209>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_es_mk);

  /* End of SystemInitialize for SubSystem: '<S209>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_06_Gerer_etat_Reveil_partiel_esclave_X6' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_05_Gerer_etat_Reveil_partiel_esclave_X5' */

  /* SystemInitialize for Atomic SubSystem: '<S208>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_a);

  /* End of SystemInitialize for SubSystem: '<S208>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_05_Gerer_etat_Reveil_partiel_esclave_X5' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_04_Gerer_etat_Reveil_partiel_esclave_X4' */

  /* SystemInitialize for Atomic SubSystem: '<S207>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_j);

  /* End of SystemInitialize for SubSystem: '<S207>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_04_Gerer_etat_Reveil_partiel_esclave_X4' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_03_Gerer_etat_Reveil_partiel_esclave_X3' */

  /* SystemInitialize for Atomic SubSystem: '<S206>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_m);

  /* End of SystemInitialize for SubSystem: '<S206>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_03_Gerer_etat_Reveil_partiel_esclave_X3' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_02_Gerer_etat_Reveil_partiel_esclave_X2' */

  /* SystemInitialize for Atomic SubSystem: '<S205>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_n);

  /* End of SystemInitialize for SubSystem: '<S205>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_02_Gerer_etat_Reveil_partiel_esclave_X2' */

  /* SystemInitialize for Enabled SubSystem: '<S198>/F01_02_02_01_01_Gerer_etat_Reveil_partiel_esclave_X1' */

  /* SystemInitialize for Atomic SubSystem: '<S204>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_g);

  /* End of SystemInitialize for SubSystem: '<S204>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S198>/F01_02_02_01_01_Gerer_etat_Reveil_partiel_esclave_X1' */

  /* SystemInitialize for Atomic SubSystem: '<S12>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay_i);

  /* End of SystemInitialize for SubSystem: '<S12>/TurnOnDelay' */

  /* SystemInitialize for Atomic SubSystem: '<S13>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay_j);

  /* End of SystemInitialize for SubSystem: '<S13>/TurnOnDelay' */

  /* SystemInitialize for Chart: '<S14>/F01_05_01_Machine_etats_RCD' */
  CtApAEM_B.UCE_stRCDSt = 3;

  /* SystemInitialize for Enabled SubSystem: '<S6>/F01_06_Gerer_CMD_LIGNE_RCD_par_type1' */

  /* SystemInitialize for Atomic SubSystem: '<S527>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_df);

  /* End of SystemInitialize for SubSystem: '<S527>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S527>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2_n);

  /* End of SystemInitialize for SubSystem: '<S527>/TurnOnDelay2' */

  /* End of SystemInitialize for SubSystem: '<S6>/F01_06_Gerer_CMD_LIGNE_RCD_par_type1' */

  /* SystemInitialize for Atomic SubSystem: '<S17>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_d);

  /* End of SystemInitialize for SubSystem: '<S17>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S17>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2_g);

  /* End of SystemInitialize for SubSystem: '<S17>/TurnOnDelay2' */

  /* SystemInitialize for Outport: '<S6>/UCE_stRCDSt' */
  CtApAEM_B.OutportBufferForUCE_stRCDSt = 3;

  /* End of SystemInitialize for SubSystem: '<S3>/F01_Gerer_PdV_RCD' */

  /* SystemInitialize for Enabled SubSystem: '<S3>/F02_Gerer_PdV_APC' */

  /* SystemInitialize for Atomic SubSystem: '<S564>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_l);

  /* End of SystemInitialize for SubSystem: '<S564>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S564>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2_d);

  /* End of SystemInitialize for SubSystem: '<S564>/TurnOnDelay2' */

  /* SystemInitialize for Atomic SubSystem: '<S565>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay_j0);

  /* End of SystemInitialize for SubSystem: '<S565>/TurnOnDelay' */

  /* SystemInitialize for Atomic SubSystem: '<S566>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay_h);

  /* End of SystemInitialize for SubSystem: '<S566>/TurnOnDelay' */

  /* SystemInitialize for Atomic SubSystem: '<S559>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_n);

  /* End of SystemInitialize for SubSystem: '<S559>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S559>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2_kj);

  /* End of SystemInitialize for SubSystem: '<S559>/TurnOnDelay2' */

  /* SystemInitialize for Atomic SubSystem: '<S560>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay_m);

  /* End of SystemInitialize for SubSystem: '<S560>/TurnOnDelay' */

  /* SystemInitialize for Chart: '<S561>/F02_04_01_ECU_APC_Applicative_state_machine' */
  CtApAEM_B.UCE_stAPCSt = 12;

  /* SystemInitialize for Atomic SubSystem: '<S563>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_md);

  /* End of SystemInitialize for SubSystem: '<S563>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S563>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2_h);

  /* End of SystemInitialize for SubSystem: '<S563>/TurnOnDelay2' */

  /* SystemInitialize for Outport: '<S7>/UCE_stAPCSt' */
  CtApAEM_B.OutportBufferForUCE_stAPCSt = 12;

  /* End of SystemInitialize for SubSystem: '<S3>/F02_Gerer_PdV_APC' */
  /* End of SystemInitialize for SubSystem: '<S2>/F0_Gerer_PdV_EE_generique' */
  /* End of SystemInitialize for RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' */

  /* SystemInitialize for Outport: '<Root>/PpAppRCDECUState_DeAppRCDECUState' */
  Rte_Write_PpAppRCDECUState_DeAppRCDECUState
    (CtApAEM_B.OutportBufferForPpAppRCDECUStat);

  /* SystemInitialize for Outport: '<Root>/PpECU_WakeupMain_DeECU_WakeupMain' */
  Rte_Write_PpECU_WakeupMain_DeECU_WakeupMain(CtApAEM_B.LogicalOperator12);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
