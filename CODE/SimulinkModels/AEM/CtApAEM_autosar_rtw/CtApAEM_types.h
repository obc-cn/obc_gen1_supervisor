/*
 * File: CtApAEM_types.h
 *
 * Code generated for Simulink model 'CtApAEM'.
 *
 * Model version                  : 1.2
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Tue Oct 22 11:27:15 2019
 *
 * Target selection: autosar.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_CtApAEM_types_h_
#define RTW_HEADER_CtApAEM_types_h_
#endif                                 /* RTW_HEADER_CtApAEM_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
