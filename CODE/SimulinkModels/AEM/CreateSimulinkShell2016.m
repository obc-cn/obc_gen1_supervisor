function CreateSimulinkShell2016(swcArxmlFile)

if nargin == 0
    [filename, pathname] = uigetfile('*.arxml', 'Select the components arxml file');
    swcArxmlFile = fullfile(pathname, filename);
end
    generalArxmls = {'C:\obcp11\CODE\Utils\davinci_workspace\Config\AUTOSAR\PlatformTypes_AR4.arxml', ...
        'C:\obcp11\CODE\Utils\davinci_workspace\Config\Developer\Constants.arxml', ...
        'C:\obcp11\CODE\Utils\davinci_workspace\Config\Developer\DataTypes.arxml', ...
        'C:\obcp11\CODE\Utils\davinci_workspace\Config\Developer\Packages.arxml', ...
        'C:\obcp11\CODE\Utils\davinci_workspace\Config\Developer\PortInterfaces.arxml'};

    %obj = arxml.importer([swcArxmlFile, generalArxmls]);
    obj = arxml.importer(swcArxmlFile);
    
    applicationSWC = obj.getApplicationComponentNames{1};
    [~,modelname] = fileparts(applicationSWC);
    modelname = strrep(modelname,'/','');
    createComponentAsModel(obj,applicationSWC,...
        'CreateInternalBehavior', true, ...
        'DataDictionary',[modelname '.sldd']);

    save_system(modelname);
    
end

