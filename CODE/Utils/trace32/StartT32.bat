@echo off


echo *********************************************************
echo *************** Start TRACE32 for OBCP11 ****************
echo *********************************************************
echo *


rem *                                                        *
rem * Execute actions                                        *
rem *                                                        *
echo * Searching for a valid TRACE32 tool installed...
echo *
rem Checking devtools OK
call ..\devtools_chkr\trace32_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_T32

echo * Update configuration tree as per user needs...
echo *
rem Read trace32 allocation path
for /f "tokens=2 delims==" %%i in ('%WINDIR%\system32\findstr.exe /b /c:"DEVTOOLS_TRACE32_PATH" %~dp0\..\devtools.path.defines') do (
    set trace32_path=%%i
)
rem Update configurationtree with the proper trace32 application path
call support\cmd\fart .\support\configurationtree\T32Start.ts2 _T32PATH %trace32_path% >nul

echo * Checking compatible FBL version to be flashed...
rem Read HW_VERSION compiled
for /f "tokens=3 delims=	,	" %%i in ('%WINDIR%\system32\findstr.exe /b /c:"#define		APP_HW_VERSION" %~dp0\..\..\Source\AppSW\AppExternals.h') do (
    set hw_version=%%i
)
echo *    - HW_VERSION: %hw_version%
echo * 
rem Prepare compatible FBL version to be flashed
rem !!! Please, update this script for any new HW VERSION defined
rem NOTES:
rem     - FBL V1_1 and V1_2 are fully compatibles
set fbl_version=UNKNOWN
if %hw_version% == APP_SAMPLES_D2_1_NO_XCP set fbl_version=V1_2
if %hw_version% == APP_SAMPLES_D2_1 set fbl_version=V1_2
if %hw_version% == APP_SAMPLES_D1_2 set fbl_version=V1
if %hw_version% == APP_SAMPLES_D1_1 set fbl_version=V0
if %hw_version% == APP_SAMPLES_D1_1_NO_LED set fbl_version=V0
if %hw_version% == APP_SAMPLES_D1_NO_PLC set fbl_version=V0
if %hw_version% == APP_SAMPLES_D1 set fbl_version=V0
if %hw_version% == APP_SAMPLES_C1 set fbl_version=V0
if %fbl_version% == UNKNOWN goto :END_ERR_HWVERSION

copy %~dp0\..\bootloader\bin\OBCP11_FlashBootloader_%fbl_version%.hex %~dp0\..\bootloader\bin\FBL2FLASH.hex  >nul 2>&1

echo * Start a new TRACE32 debug session...
echo *
rem Launch trace32 debug session
call %trace32_path%\bin\windows64\t32start.exe -RUNCFGFILE .\support\configurationtree\T32Start.ts2 -RUNITEM "//Configuration" -QUIT
if not %ERRORLEVEL% == 0 goto END_NOK
goto END_OK


rem *                                                        *
rem * End batch script                                       *
rem *                                                        *
:END_OK
echo * OK: TRACE32 debug session started successully! :)
echo *
exit /b %ERRORLEVEL%

:END_NOK
echo * ERR: TRACE32 debug session failed
echo *
echo * Press any key for finishing...
set /p dummy=
echo *
exit /b %ERRORLEVEL%

:END_ERR_T32
echo * ERR: TRACE32 tool required not found
echo *
echo * Press any key for finishing...
set /p dummy=
echo *
exit /b %ERRORLEVEL%
:END_ERR_HWVERSION
echo * ERR: HW Version setted not found
echo *      Hint: please, update StartT32.bat for defining the HW Version needed
echo *
echo * Press any key for finishing...
set /p dummy=
echo *
exit /b 

