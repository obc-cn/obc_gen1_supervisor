; --------------------------------------------------------------------------------
; @Title: Trace and Performance Menu File for OSEK/ORTI
; @Description:
;   This file contains additional menu entries for trace
;   and performance measurement.
;   It is automatically loaded with the TASK.ORTI command.
; @Keywords: AUTOSAR OSEK ORTI
; @Author: DIE
; @Copyright: (C) 1989-2014 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Id: ortiperf.men 13493 2018-11-14 08:52:56Z rweiss $

add
menu
(
  popup "Trace"
  (
    popup "List"
    (
      separator
      menuitem "&Task Switches and Services"  "Trace.List List.TASK"
      menuitem "&Default and Tasks"           "Trace.List List.TASK DEFault"
    )
  )
  popup "Perf"
  (
    separator
    popup "&Task Runtime"
    (
      menuitem "&Prepare"
      (
        if t.method.analyzer()
        (
          Analyzer.AutoInit on
        )
        if ice()&&!trace.flow()
        (
          Analyzer.ReProgram
          (
            Sample.Enable if AlphaBreak&&Write
          )
          Break.Delete /Alpha
          Break.Set task.config(magic)++(task.config(magicsize)-1) /Alpha
        )
        if fire()&&!trace.flow()
        (
          Analyzer.ReProgram
          (
            Sample.Enable if AlphaBreak
          )
          Break.Delete /Alpha
          Break.Set task.config(magic)++(task.config(magicsize)-1) /Alpha /Write
        )
        if trace.flow()
        (
          Break.Delete /TraceEnable
          Break.Set task.config(magic) /Write /TraceEnable
          if cpufamily()=="TRICORE"
            PRINT %WARNING "Please ensure proper MCDS settings for data trace!"
        )
        Trace.Init
      )
      menuitem "[:perf]Show &Numerical"   "Trace.STATistic.TASK"
      menuitem "[:achart]Show as &Timing" "Trace.CHART.TASK"
      menuitem "[:achart]Tracking with Trace &List"
      (
        Trace.List List.TASK DEFault /Track
        Trace.CHART.TASK /Track
      )
    )
    popup "Task &Services"
    (
      menuitem "&Prepare"
      (
        if ice()&&!trace.flow()
        (
          Analyzer.ReProgram
          (
            Sample.Enable if AlphaBreak&&Write
          )
          Break.Delete /Alpha
          Break.Set task.config(magic_service) /Alpha
        )
       if fire()&&!trace.flow()
        (
          Analyzer.ReProgram
          (
            Sample.Enable if AlphaBreak
          )
          Break.Delete /Alpha
          Break.Set task.config(magic_service) /Alpha /Write
        )
        if trace.flow()
        (
          Break.Delete /TraceEnable
          Break.Set task.config(magic_service) /Write /TraceEnable
          if cpufamily()=="TRICORE"
            PRINT %WARNING "Please ensure proper MCDS settings for data trace!"
        )
        Trace.Init
      )
      menuitem "[:perf]Show &Numerical"   "Trace.STATistic.TASKSRV"
      menuitem "[:achart]Show as &Timing" "Trace.CHART.TASKSRV"
      menuitem "[:achart]Tracking with Trace &List"
      (
        Trace.List List.TASK DEFault /Track
        Trace.CHART.TASKSRV /Track
      )
    )
    popup "Task ISR&2s"
    (
      menuitem "&Prepare"
      (
        if ice()&&!trace.flow()
        (
          Analyzer.ReProgram
          (
            Sample.Enable if AlphaBreak&&Write
          )
          Break.Delete /Alpha
          Break.Set task.config(magic) /Alpha
          Break.Set task.config(magic_isr2) /Alpha
        )
       if fire()&&!trace.flow()
        (
          Analyzer.ReProgram
          (
            Sample.Enable if AlphaBreak
          )
          Break.Delete /Alpha
          Break.Set task.config(magic) /Alpha /Write
          Break.Set task.config(magic_isr2) /Alpha /Write
        )
        if trace.flow()
        (
          Break.Delete /TraceEnable
          Break.Set task.config(magic) /Write /TraceEnable
          Break.Set task.config(magic_isr2) /Write /TraceEnable
          if cpufamily()=="TRICORE"
            PRINT %WARNING "Please ensure proper MCDS settings for data trace!"
        )
        Trace.Init
      )
      menuitem "[:perf]Show &Numerical" "Trace.STATistic.TASKINTR"
      menuitem "[:perf]Show N&umerical (task related)" "Trace.STATistic.TASKVSINTR"
      menuitem "[:achart]Show as &Timing" "Trace.CHART.TASKINTR"
      menuitem "[:achart]Show as &Timing (task related)" "Trace.CHART.TASKVSINTR"
      menuitem "[:achart]Tracking with Trace &List"
      (
        Trace.List List.TASK DEFault /Track
        Trace.CHART.TASKINTR /Track
      )
    )
    popup "Task &Function Runtime"
    (
      menuitem "&Prepare"
      (
        if t.method.analyzer()
        (
          Analyzer.AutoInit on
          Analyzer.STATistic.PreFetch on
        )

        if ice()&&!trace.flow()
        (
          if a.config.hac()
          (
             Analyzer.ReProgram
             (
               Sample.Enable if AlphaBreak
               Sample.Enable if BetaBreak
               Mark.A        if AlphaBreak
               Mark.B        if BetaBreak
             )
          )
          else
          (
             Analyzer.ReProgram
             (
               Sample.Enable if AlphaBreak||BetaBreak||CharlyBreak
               Mark.A if AlphaBreak
               Mark.B if BetaBreak
               Mark.C if CharlyBreak
             )
          )
          Break.Delete /Alpha /Beta /Charly
          Break.SetFunc
          Break.Set task.config(magic)++(task.config(magicsize)-1) /Alpha
        )
        if fire()&&!trace.flow()
        (
              Analyzer.ReProgram
             (
               Sample.Enable if AlphaBreak||BetaBreak||CharlyBreak
               Mark.A if AlphaBreak
               Mark.B if BetaBreak
               Mark.C if CharlyBreak
             )
          Break.Delete /Alpha /Beta /Charly
          Break.SetFunc
          Break.Set task.config(magic)++(task.config(magicsize)-1) /Alpha /Write
        )
        if trace.flow()
        (
          Break.Delete /TraceData
          Break.Set task.config(magic) /Write /TraceData
          if cpufamily()=="TRICORE"
            PRINT %WARNING "Please ensure proper MCDS settings for data trace!"
        )
        Trace.Init
      )
      menuitem "[:perf]Show &Numerical"     "Trace.STATistic.TASKFUNC"
      menuitem "[:perf]Show as &Tree"       "Trace.STATistic.TASKTREE"
      menuitem "[:perf]Show &Detailed Tree" "Trace.STATistic.TASKTREE ALL"
      menuitem "[:achart]Show as &Timing"   "Trace.CHART.TASKFUNC"
      menuitem "[:alist]Show N&esting"      "Trace.List List.TASK FUNC TI.FUNC"
    )
    popup "Task &Status"
    (
      menuitem "&Prepare"
      (
        if t.method.analyzer()
        (
          Analyzer.AutoInit on
        )
        if ice()&&!trace.flow()
        (
          Analyzer.ReProgram
          (
            Sample.Enable if AlphaBreak&&Write
          )
          Break.Delete /Alpha
          Break.Set task.config(magic)++(task.config(magicsize)-1) /Alpha
          TASK.TASKState
        )
        if fire()&&!trace.flow()
        (
          Analyzer.ReProgram
          (
            Sample.Enable if AlphaBreak
          )
          Break.Delete /Alpha
          Break.Set task.config(magic)++(task.config(magicsize)-1) /Alpha /Write
          TASK.TASKState
          if cpufamily()=="TRICORE"
            PRINT %WARNING "Please ensure proper MCDS settings for data trace!"
        )
      )
      menuitem "[:perf]Show &Numerical"   "Trace.STATistic.TASKSTATE"
      menuitem "[:achart]Show as &Timing" "Trace.CHART.TASKSTATE"
      menuitem "[:achart]Tracking with Trace &List"
      (
        Trace.List List.TASK DEFault /Track
        Trace.CHART.TASKSTATE /Track
      )
    )
    replace
    menuitem "&Reset"
    (
      perf.reset
      Break.Delete /Alpha /Beta /Charly
      if ice()||fire()
        Analyzer.rp
      if trace.flow()
      (
        Break.Delete task.config(magic)
        Break.Delete task.config(magic_isr2)
        Break.Delete task.config(magic_service)
      )
    )
  )
  popup "&Help"
  (
    menuitem "[:manual]ORTI Awareness Manual" "HELP __RTOS_ORTI_"
  )
)

