; --------------------------------------------------------------------------------
; @Title: Manufacturing date flash operation for OBCP11 project
; @Description:
;   Launch flash manufacturing date (system current Day/Month/Year) operation
;   on FBL memory region
; @Keywords: AURIX, Infineon, TriCore
; @Author: Enrique Bueno
; @Board: TriBoard-TC2x4
; @Chip: TC234L
; @Copyright: MAHLE ELECTRONICS (C) 2020
; --------------------------------------------------------------------------------


; --------------------------------------------------------------------------------
; Read current system date
LOCAL &Day &Month &Year
&Day=date.day()
&Month=date.month()
&Year=date.year()-0x7D0


; --------------------------------------------------------------------------------
; Execute flashing operations
LOCAL &progFlash

print "flash operation started (Manufacturing Date -&Day/&Month/&Year-)"

DIALOG.YESNO "Program OBCP11(Manufacturing Date -&Day/&Month/&Year-) into flash memory?"
ENTRY &progFlash
IF (&progFlash)
(
  ; prepare flash programming (declarations)
  DO ./batchfiles/support/flashing/tc23x.cmm CPU=TC234L PREPAREONLY

  ; enable flash programming (only affected addresses)
  FLASH.ReProgram 0xA0019006--0xA0019008

  ; write Manufacturing Data
  PER.Set.simple D:0xA0019006 %Byte &Day  ; Day (1-31)
  PER.Set.simple D:0xA0019007 %Byte &Month  ; Month (1-12)
  PER.Set.simple D:0xA0019008 %Byte &Year-2000  ; Year (00-99)

  ; finally program flash memory
  FLASH.ReProgram off

  ; force break just after programming
  break
)


print "flash operation completed succesfully!"
ENDDO
