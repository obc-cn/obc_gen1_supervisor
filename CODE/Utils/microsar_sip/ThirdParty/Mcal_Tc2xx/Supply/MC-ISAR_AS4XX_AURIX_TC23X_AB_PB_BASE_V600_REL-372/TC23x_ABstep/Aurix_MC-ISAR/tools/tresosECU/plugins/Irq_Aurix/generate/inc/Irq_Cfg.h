[!/*****************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2018)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**   $FILENAME   : Irq_Cfg.h $                                                **
**                                                                            **
**   $CC VERSION : \main\dev_tc23x\14 $                                       **
**                                                                            **
**   $DATE       : 2018-06-11 $                                               **
**                                                                            **
**   AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                            **
**   VENDOR      : Infineon Technologies                                      **
**                                                                            **
**   DESCRIPTION : Irq configuration generated out of ECU configuration file  **
**                                                                            **
**   MAY BE CHANGED BY USER [yes/no]: No                                      **
**                                                                            **
*************************************************************************/!][!//
[!// 
/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2018)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**   $FILENAME   : Irq_Cfg.h $                                                **
**                                                                            **
**   $CC VERSION : \main\dev_tc23x\14 $                                       **
**                                                                            **
**   DATE, TIME: [!"$date"!], [!"$time"!]                                         **
**                                                                            **
**   GENERATOR : Build [!"$buildnr"!]                                           **
**                                                                            **
**   AUTHOR    : DL-AUTOSAR-Engineering                                       **
**                                                                            **
**   VENDOR    : Infineon Technologies                                        **
**                                                                            **
**   DESCRIPTION  : Irq configuration generated out of ECU configuration      **
**                 file                                                       **
**                                                                            **
**   MAY BE CHANGED BY USER [yes/no]: No                                     **
**                                                                            **
*******************************************************************************/
#ifndef IRQ_CFG_H 
#define IRQ_CFG_H 
[!//
[!/* Select MODULE-CONFIGURATION as context-node */!][!//
[!SELECT "as:modconf('Irq')[1]"!][!//
[!//
[!//
[!VAR "Readbit" = "num:i(0)"!][!//
[!VAR "ErrorFlag" = "num:i(0)"!][!//
[!VAR "Variable0" = "num:i(0)"!][!//
[!VAR "Variable1" = "num:i(0)"!][!//
[!VAR "Variable2" = "num:i(0)"!][!//
[!VAR "Variable3" = "num:i(0)"!][!//
[!VAR "Variable4" = "num:i(0)"!][!//
[!VAR "Variable5" = "num:i(0)"!][!//
[!VAR "Variable6" = "num:i(0)"!][!//
[!VAR "Variable7" = "num:i(0)"!][!//
[!VAR "Variable8" = "num:i(0)"!][!//
[!VAR "Variable9" = "num:i(0)"!][!//
[!VAR "Variable10" = "num:i(0)"!][!//
[!VAR "Variable11" = "num:i(0)"!][!//
[!VAR "Variable12" = "num:i(0)"!][!//
[!VAR "Variable13" = "num:i(0)"!][!//
[!VAR "Variable14" = "num:i(0)"!][!//
[!VAR "Variable15" = "num:i(0)"!][!//
[!VAR "DmaMaxPrio" = "num:i(ecu:get('Irq.DmaMaxPrio'))"!][!//

[!//
[!/*****************************************************************************
  MACRO: CG_PriorityCheck 
  
  Macro to determine whether the configured priorities are different or not
  
  Input Parameters:
  Givenno - Value of the node (SrnId: Priority Number)
  cont - Module Name
*****************************************************************************/!]
[!MACRO "CG_PriorityCheck", "Givenno" = "","cont" = ""!][!//
[!IF "($Givenno != num:i(0))"!][!//
[!VAR "Givenno1" = "num:i($Givenno div num:i(32))"!][!//
[!VAR "Givenno2" = "num:i($Givenno mod num:i(32))"!][!//
[!IF "($Givenno1 = num:i(0))"!][!//
[!IF "bit:getbit($Variable0,$Givenno2) != 'true'"!][!//
[!VAR "Variable0" = "bit:bitset($Variable0,$Givenno2)"!][!//
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!]
[!ENDIF!][!//			
[!ELSEIF "($Givenno1 = num:i(1))"!][!//
[!IF "bit:getbit($Variable1,$Givenno2) != 'true'"!][!//
[!VAR "Variable1" = "bit:bitset($Variable1, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ELSEIF "($Givenno1 = num:i(2))"!][!//
[!IF "bit:getbit($Variable2,$Givenno2) != 'true'"!][!//
[!VAR "Variable2" = "bit:bitset($Variable2, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ELSEIF "($Givenno1 = num:i(3))"!][!//
[!IF "bit:getbit($Variable3,$Givenno2) != 'true'"!][!//
[!VAR "Variable3" = "bit:bitset($Variable3, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ELSEIF "($Givenno1 = num:i(4))"!][!//
[!IF "bit:getbit($Variable4,$Givenno2) != 'true'"!][!//
[!VAR "Variable4" = "bit:bitset($Variable4, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ELSEIF "($Givenno1 = num:i(5))"!][!//
[!IF "bit:getbit($Variable5,$Givenno2) != 'true'"!][!//
[!VAR "Variable5" = "bit:bitset($Variable5, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ELSEIF "($Givenno1 = num:i(6))"!][!//
[!IF "bit:getbit($Variable6,$Givenno2) != 'true'"!][!//
[!VAR "Variable6" = "bit:bitset($Variable6, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ELSEIF "($Givenno1 = num:i(7))"!][!//
[!IF "bit:getbit($Variable7,$Givenno2) != 'true'"!][!//
[!VAR "Variable7" = "bit:bitset($Variable7, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ELSEIF "($Givenno1 = num:i(8))"!][!//
[!IF "bit:getbit($Variable8,$Givenno2) != 'true'"!][!//
[!VAR "Variable8" = "bit:bitset($Variable8, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ELSEIF "($Givenno1 = num:i(9))"!][!//
[!IF "bit:getbit($Variable9,$Givenno2) != 'true'"!][!//
[!VAR "Variable9" = "bit:bitset($Variable9, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ELSEIF "($Givenno1 = num:i(10))"!][!//
[!IF "bit:getbit($Variable10,$Givenno2) != 'true'"!][!//
[!VAR "Variable10" = "bit:bitset($Variable10, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ELSEIF "($Givenno1 = num:i(11))"!][!//
[!IF "bit:getbit($Variable11,$Givenno2) != 'true'"!][!//
[!VAR "Variable11" = "bit:bitset($Variable11, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ELSEIF "($Givenno1 = num:i(12))"!][!//
[!IF "bit:getbit($Variable12,$Givenno2) != 'true'"!][!//
[!VAR "Variable12" = "bit:bitset($Variable12, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ELSEIF "($Givenno1 = num:i(13))"!][!//
[!IF "bit:getbit($Variable13,$Givenno2) != 'true'"!][!//
[!VAR "Variable13" = "bit:bitset($Variable13, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ELSEIF "($Givenno1 = num:i(14))"!][!//
[!IF "bit:getbit($Variable14,$Givenno2) != 'true'"!][!//
[!VAR "Variable14" = "bit:bitset($Variable14, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ELSEIF "($Givenno1 = num:i(15))"!][!//
[!IF "bit:getbit($Variable15,$Givenno2) != 'true'"!][!//
[!VAR "Variable15" = "bit:bitset($Variable15, $Givenno2)"!][!//	
[!ELSE!][!//
[!VAR "ErrorFlag" = "num:i(1)"!][!//
[!ENDIF!][!//
[!ENDIF!][!//
[!IF "($ErrorFlag != num:i(0))"!][!//
[!ERROR!][!//
Same Priority is configured in two places, check [!"$cont"!] and some other container.
[!ENDERROR!][!//
[!ENDIF!][!//
[!ENDIF!][!//
[!ENDMACRO!][!//
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/

/************************* interrupt CATEGORY *********************************/
#define IRQ_CAT1                    (0)
#define IRQ_CAT23                   (1)
/************************* interrupt Type of Service **************************/
#define IRQ_TOS_CPU0                (0x0000)
#define IRQ_TOS_DMA                 (0x0800)
/*
                     Container : IrqGeneralConfiguration
*/

/*
Configuration: IRQ_OSEK_ENABLE 
- if TRUE, OSEK RTOS is used, 
  Both CAT1 and CAT23 interrupt categories can be configured 
  Any CAT23 interrupt priority should be greater than CAT1 interrupt priority
- if FALSE,OSEK RTOS is not used
  Only CAT1 interrupt category can be configured 
*/

#define IRQ_OSEK_ENABLE  [!IF "IrqGeneral/IrqOsekEnable = 'true'"!][!//
           (STD_ON)[!//
[!ELSE!][!//
           (STD_OFF)[!//
[!ENDIF!]

[!NOCODE!][!//
[!VAR "MIN_CAT1_LEVEL" = "num:i(511)"!][!//
[!VAR "MAX_CAT2_LEVEL" = "num:i(0)"!][!//
[!VAR "PREVIOUS"       = "num:i(0)"!][!// 
[!ENDNOCODE!][!//

[!VAR "IrqAscLinExist" = "'STD_OFF'"!][!//
[!IF "num:i(count(IrqAscLinConfig/*)) = 1"!][!//
/*
                     Container : IrqAscLin interruptConfiguration
*/
[!VAR "IrqAscLinExist" = "'STD_ON'"!][!//
/* ASC Lin Tx interrupt Category Setting*/
[!LOOP "IrqAscLinConfig/*[1]/IrqAscLinCatConfig/IrqAscLinTxCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ASCLIN', (substring-before(substring-after($NodeName, "IrqAscLin"), "TxCat")), '_TX_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//


/* ASC Lin Tx interrupt Priority Setting*/
[!LOOP "IrqAscLinConfig/*[1]/IrqAscLinPrioConfig/IrqAscLinTxPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAscLin"), "TxPrio"))"!][!//
[!SELECT "../../../IrqAscLinTosConfig/IrqAscLinTxTosConfig"!][!//
[!VAR "AscLinTxTosParam" = "concat('IrqAscLin',$SrnId,'TxTos')"!][!//
[!VAR "TypeOfService" = "node:value($AscLinTxTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../../IrqAscLinCatConfig/IrqAscLinTxCatConfig"!][!//
[!VAR "AscLinTxCatParam" = "concat('IrqAscLin',$SrnId,'TxCat')"!][!//
[!VAR "AscLinTxCatVal" = "node:value($AscLinTxCatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ASCLINTX'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ASCLIN', (substring-before(substring-after($NodeName, "IrqAscLin"), "TxPrio")), '_TX_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($AscLinTxCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ASCLINTX interrupt Priority Setting for loop */!][!//

/* ASC Lin Tx interrupt type of service Setting*/
[!LOOP "IrqAscLinConfig/*[1]/IrqAscLinTosConfig/IrqAscLinTxTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ASCLIN', (substring-before(substring-after($NodeName, "IrqAscLin"), "TxTos")), '_TX_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAscLin"),"TxTos"))"!]
[!SELECT "../../../IrqAscLinPrioConfig/IrqAscLinTxPrioConfig"!][!//
[!VAR "AscLinTxPrioParam" = "concat('IrqAscLin', $SrnId, 'TxPrio')"!]
[!VAR "PrioVal" = "num:i(node:value($AscLinTxPrioParam))"!][!//
[!ENDSELECT!]
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ASCLINTX interrupt Priority Setting for loop */!][!//

/* ASC Lin Rx interrupt Category Setting*/

[!LOOP "IrqAscLinConfig/*[1]/IrqAscLinCatConfig/IrqAscLinRxCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ASCLIN', (substring-before(substring-after($NodeName, "IrqAscLin"), "RxCat")), '_RX_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//


/* ASC Lin Rx interrupt Priority Setting*/
[!LOOP "IrqAscLinConfig/*[1]/IrqAscLinPrioConfig/IrqAscLinRxPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAscLin"), "RxPrio"))"!][!//
[!SELECT "../../../IrqAscLinTosConfig/IrqAscLinRxTosConfig"!][!//
[!VAR "AscLinRxTosParam" = "concat('IrqAscLin',$SrnId,'RxTos')"!][!//
[!VAR "TypeOfService" = "node:value($AscLinRxTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../../IrqAscLinCatConfig/IrqAscLinRxCatConfig"!][!//
[!VAR "AscLinRxCatParam" = "concat('IrqAscLin',$SrnId,'RxCat')"!][!//
[!VAR "AscLinRxCatVal" = "node:value($AscLinRxCatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ASCLINRX'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ASCLIN', (substring-before(substring-after($NodeName, "IrqAscLin"), "RxPrio")), '_RX_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($AscLinRxCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ASCLINRX interrupt Priority Setting for loop */!][!//


/* ASC Lin Rx interrupt type of service Setting*/
[!LOOP "IrqAscLinConfig/*[1]/IrqAscLinTosConfig/IrqAscLinRxTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ASCLIN', (substring-before(substring-after($NodeName, "IrqAscLin"), "RxTos")), '_RX_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAscLin"),"RxTos"))"!]
[!SELECT "../../../IrqAscLinPrioConfig/IrqAscLinRxPrioConfig"!][!//
[!VAR "AscLinRxPrioParam" = "concat('IrqAscLin', $SrnId, 'RxPrio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($AscLinRxPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//

/* ASC Lin Err interrupt Category Setting*/
[!LOOP "IrqAscLinConfig/*[1]/IrqAscLinCatConfig/IrqAscLinErrCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ASCLIN', (substring-before(substring-after($NodeName, "IrqAscLin"), "ErrorCat")), '_ERR_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/* ASC Lin Err interrupt Priority Setting*/
[!LOOP "IrqAscLinConfig/*[1]/IrqAscLinPrioConfig/IrqAscLinErrPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAscLin"), "ErrorPrio"))"!][!//
[!SELECT "../../../IrqAscLinTosConfig/IrqAscLinErrTosConfig"!][!//
[!VAR "AscLinErrTosParam" = "concat('IrqAscLin',$SrnId,'ErrTos')"!][!//
[!VAR "TypeOfService" = "node:value($AscLinErrTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../../IrqAscLinCatConfig/IrqAscLinErrCatConfig"!][!//
[!VAR "AscLinErrCatParam" = "concat('IrqAscLin',$SrnId,'ErrorCat')"!][!//
[!VAR "AscLinErrCatVal" = "node:value($AscLinErrCatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ASCLINERR'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ASCLIN', (substring-before(substring-after($NodeName, "IrqAscLin"), "ErrorPrio")), '_ERR_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($AscLinErrCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ASCLINERR interrupt Priority Setting for loop */!][!//

/* ASC Lin Err interrupt type of service Setting*/
[!LOOP "IrqAscLinConfig/*[1]/IrqAscLinTosConfig/IrqAscLinErrTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ASCLIN', (substring-before(substring-after($NodeName, "IrqAscLin"), "ErrTos")), '_ERR_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAscLin"),"ErrTos"))"!][!//
[!SELECT "../../../IrqAscLinPrioConfig/IrqAscLinErrPrioConfig"!][!//
[!VAR "AscLinErrPrioParam" = "concat('IrqAscLin', $SrnId, 'ErrorPrio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($AscLinErrPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!VAR "IrqQspiExist" = "'STD_OFF'"!][!//
[!IF "num:i(count(IrqQspiConfig/*)) = 1"!][!//
/*
                     Container : IrqQspi interruptConfiguration
*/
[!VAR "IrqQspiExist" = "'STD_ON'"!][!//
[!//
/* Qspi Tx interrupt Category Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiCatConfig/IrqQspiTxCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "TxCat")), '_TX_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/* Qspi Tx interrupt Priority Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiPrioConfig/IrqQspiTxPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqQspi"), "TxPrio"))"!][!//
[!SELECT "../../../../*[1]/IrqQspiTosConfig/IrqQspiTxTosConfig"!][!//
[!VAR "QspiTxTosParam" = "concat('IrqQspi',$SrnId,'TxTos')"!][!//
[!VAR "TypeOfService" = "node:value($QspiTxTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../../../*[1]/IrqQspiCatConfig/IrqQspiTxCatConfig"!][!//
[!VAR "QspiTxCatParam" = "concat('IrqQspi',$SrnId,'TxCat')"!][!//
[!VAR "QspiTxCatVal" = "node:value($QspiTxCatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'QSPITX'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "TxPrio")), '_TX_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($QspiTxCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of QSPITX interrupt Priority Setting for loop */!][!//


/* Qspi Tx interrupt type of service Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiTosConfig/IrqQspiTxTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "TxTos")), '_TX_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqQspi"),"TxTos"))"!][!//
[!SELECT "../../../../*[1]/IrqQspiPrioConfig/IrqQspiTxPrioConfig"!][!//
[!VAR "QspiTxPrioParam" = "concat('IrqQspi', $SrnId, 'TxPrio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($QspiTxPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//

/* Qspi Rx interrupt Category Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiCatConfig/IrqQspiRxCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "RxCat")), '_RX_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/* Qspi Rx interrupt Priority Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiPrioConfig/IrqQspiRxPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqQspi"), "RxPrio"))"!][!//
[!SELECT "../../../../*[1]/IrqQspiTosConfig/IrqQspiRxTosConfig"!][!//
[!VAR "QspiRxTosParam" = "concat('IrqQspi',$SrnId,'RxTos')"!][!//
[!VAR "TypeOfService" = "node:value($QspiRxTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../../../*[1]/IrqQspiCatConfig/IrqQspiRxCatConfig"!][!//
[!VAR "QspiRxCatParam" = "concat('IrqQspi',$SrnId,'RxCat')"!][!//
[!VAR "QspiRxCatVal" = "node:value($QspiRxCatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'QSPIRX'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "RxPrio")), '_RX_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($QspiRxCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of QSPIRX interrupt Priority Setting for loop */!][!//


/* Qspi Rx interrupt type of service Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiTosConfig/IrqQspiRxTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "RxTos")), '_RX_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqQspi"),"RxTos"))"!][!//
[!SELECT "../../../../*[1]/IrqQspiPrioConfig/IrqQspiRxPrioConfig"!][!//
[!VAR "QspiRxPrioParam" = "concat('IrqQspi', $SrnId, 'RxPrio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($QspiRxPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//

/* Qspi Err interrupt Category Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiCatConfig/IrqQspiErrCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "ErrCat")), '_ERR_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/* Qspi Err interrupt Priority Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiPrioConfig/IrqQspiErrPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqQspi"), "ErrPrio"))"!][!//
[!SELECT "../../../../*[1]/IrqQspiTosConfig/IrqQspiErrTosConfig"!][!//
[!VAR "QspiErrTosParam" = "concat('IrqQspi',$SrnId,'ErrTos')"!][!//
[!VAR "TypeOfService" = "node:value($QspiErrTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../../../*[1]/IrqQspiCatConfig/IrqQspiErrCatConfig"!][!//
[!VAR "QspiErrCatParam" = "concat('IrqQspi',$SrnId,'ErrCat')"!][!//
[!VAR "QspiErrCatVal" = "node:value($QspiErrCatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'QSPIERR'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "ErrPrio")), '_ERR_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($QspiErrCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of QSPIERR interrupt Priority Setting for loop */!][!//
/* Qspi Err interrupt type of service Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiTosConfig/IrqQspiErrTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "ErrTos")), '_ERR_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqQspi"),"ErrTos"))"!][!//
[!SELECT "../../../../*[1]/IrqQspiPrioConfig/IrqQspiErrPrioConfig"!][!//
[!VAR "QspiErrPrioParam" = "concat('IrqQspi', $SrnId, 'ErrPrio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($QspiErrPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//


/* Qspi PT interrupt Category Setting*/

[!LOOP "IrqQspiConfig/*[1]/IrqQspiCatConfig/IrqQspiPTCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "PTCat")), '_PT_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/* Qspi PT interrupt Priority Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiPrioConfig/IrqQspiPTPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqQspi"), "PTPrio"))"!][!//
[!SELECT "../../../../*[1]/IrqQspiTosConfig/IrqQspiPTTosConfig"!][!//
[!VAR "QspiPTTosParam" = "concat('IrqQspi',$SrnId,'PTTos')"!][!//
[!VAR "TypeOfService" = "node:value($QspiPTTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../../../*[1]/IrqQspiCatConfig/IrqQspiPTCatConfig"!][!//
[!VAR "QspiPTCatParam" = "concat('IrqQspi',$SrnId,'PTCat')"!][!//
[!VAR "QspiPTCatVal" = "node:value($QspiPTCatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'QSPIPT'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "PTPrio")), '_PT_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($QspiPTCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of QSPIPT interrupt Priority Setting for loop */!][!//

/* Qspi PT interrupt type of service Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiTosConfig/IrqQspiPTTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "PTTos")), '_PT_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqQspi"),"PTTos"))"!][!//
[!SELECT "../../../../*[1]/IrqQspiPrioConfig/IrqQspiPTPrioConfig"!][!//
[!VAR "QspiPTPrioParam" = "concat('IrqQspi', $SrnId, 'PTPrio')"!]
[!VAR "PrioVal" = "num:i(node:value($QspiPTPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//


/* Qspi HC interrupt Category Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiCatConfig/IrqQspiHCCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "HCCat")), '_HC_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/* Qspi HC interrupt Priority Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiPrioConfig/IrqQspiHCPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqQspi"), "HCPrio"))"!][!//
[!SELECT "../../../../*[1]/IrqQspiTosConfig/IrqQspiHCTosConfig"!][!//
[!VAR "QspiHCTosParam" = "concat('IrqQspi',$SrnId,'HCTos')"!][!//
[!VAR "TypeOfService" = "node:value($QspiHCTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../../../*[1]/IrqQspiCatConfig/IrqQspiHCCatConfig"!][!//
[!VAR "QspiHCCatParam" = "concat('IrqQspi',$SrnId,'HCCat')"!][!//
[!VAR "QspiHCCatVal" = "node:value($QspiHCCatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'QSPIHC'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "HCPrio")), '_HC_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($QspiHCCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of QSPIHC interrupt Priority Setting for loop */!][!//

/* Qspi HC interrupt type of service Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiTosConfig/IrqQspiHCTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "HCTos")), '_HC_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqQspi"),"HCTos"))"!][!//
[!SELECT "../../../../*[1]/IrqQspiPrioConfig/IrqQspiHCPrioConfig"!][!//
[!VAR "QspiHCPrioParam" = "concat('IrqQspi', $SrnId, 'HCPrio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($QspiHCPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//


/* Qspi UD interrupt Category Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiCatConfig/IrqQspiUDCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "UDCat")), '_UD_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/* Qspi UD interrupt Priority Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiPrioConfig/IrqQspiUDPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqQspi"), "UDPrio"))"!][!//
[!SELECT "../../../../*[1]/IrqQspiTosConfig/IrqQspiUDTosConfig"!][!//
[!VAR "QspiUDTosParam" = "concat('IrqQspi',$SrnId,'UDTos')"!][!//
[!VAR "TypeOfService" = "node:value($QspiUDTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../../../*[1]/IrqQspiCatConfig/IrqQspiUDCatConfig"!][!//
[!VAR "QspiUDCatParam" = "concat('IrqQspi',$SrnId,'UDCat')"!][!//
[!VAR "QspiUDCatVal" = "node:value($QspiUDCatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'QSPIUD'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "UDPrio")), '_UD_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($QspiUDCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of QSPIUD interrupt Priority Setting for loop */!][!//

/* Qspi UD interrupt type of service Setting*/
[!LOOP "IrqQspiConfig/*[1]/IrqQspiTosConfig/IrqQspiUDTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_QSPI', (substring-before(substring-after($NodeName, "IrqQspi"), "UDTos")), '_UD_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqQspi"),"UDTos"))"!][!//
[!SELECT "../../../../*[1]/IrqQspiPrioConfig/IrqQspiUDPrioConfig"!][!//
[!VAR "QspiUDPrioParam" = "concat('IrqQspi', $SrnId, 'UDPrio')"!]
[!VAR "PrioVal" = "num:i(node:value($QspiUDPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!VAR "IrqCCU6Exist" = "'STD_OFF'"!][!//
[!IF "num:i(count(IrqCCU6Config/*)) = 1"!][!//
/*
                     Container : IrqCCU6 interruptConfiguration
*/
[!VAR "IrqCCU6Exist" = "'STD_ON'"!][!//
[!IF "contains( ecu:get('Irq.CCU6Available'), 'IrqCCU60' )"!][!//
/*CCU60 interrupt Category setting */
[!LOOP "IrqCCU6Config/*[1]/IrqCCU60Config/IrqCCU60CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_CCU60_SR', (substring-before(substring-after($NodeName, "IrqCCU60SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//
 
/*CCU60 interrupt Priority setting */ 
[!LOOP "IrqCCU6Config/*[1]/IrqCCU60Config/IrqCCU60PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqCCU60SR"), "Prio"))"!][!//
[!SELECT "../../IrqCCU60TosConfig"!][!//
[!VAR "CCU60TosParam" = "concat('IrqCCU60SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($CCU60TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqCCU60CatConfig"!][!//
[!VAR "CCU60CatParam" = "concat('IrqCCU60SR',$SrnId,'Cat')"!][!//
[!VAR "CCU60CatVal" = "node:value($CCU60CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'CCU60'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_CCU60_SR', (substring-before(substring-after($NodeName, "IrqCCU60SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($CCU60CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of CCU60 interrupt Priority Setting for loop */!][!//

/*CCU60 interrupt type of service setting */
[!LOOP "IrqCCU6Config/*[1]/IrqCCU60Config/IrqCCU60TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_CCU60_SR', (substring-before(substring-after($NodeName, "IrqCCU60SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqCCU60SR"),"Tos"))"!][!//
[!SELECT "../../IrqCCU60PrioConfig"!][!//
[!VAR "CCU60PrioParam" = "concat('IrqCCU60SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($CCU60PrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.CCU6Available'), 'IrqCCU61' )"!][!//
/*CCU61 interrupt Category setting */
[!LOOP "IrqCCU6Config/*[1]/IrqCCU61Config/IrqCCU61CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_CCU61_SR', (substring-before(substring-after($NodeName, "IrqCCU61SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//
	   
/*CCU61 interrupt Priority setting */ 
[!LOOP "IrqCCU6Config/*[1]/IrqCCU61Config/IrqCCU61PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqCCU61SR"), "Prio"))"!][!//
[!SELECT "../../IrqCCU61TosConfig"!][!//
[!VAR "CCU61TosParam" = "concat('IrqCCU61SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($CCU61TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqCCU61CatConfig"!][!//
[!VAR "CCU61CatParam" = "concat('IrqCCU61SR',$SrnId,'Cat')"!][!//
[!VAR "CCU61CatVal" = "node:value($CCU61CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'CCU61'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_CCU61_SR', (substring-before(substring-after($NodeName, "IrqCCU61SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($CCU61CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of CCU61 interrupt Priority Setting for loop */!][!//


/*CCU61 interrupt type of service setting */

[!LOOP "IrqCCU6Config/*[1]/IrqCCU61Config/IrqCCU61TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_CCU61_SR', (substring-before(substring-after($NodeName, "IrqCCU61SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqCCU61SR"),"Tos"))"!][!//
[!SELECT "../../IrqCCU61PrioConfig"!][!//
[!VAR "CCU61PrioParam" = "concat('IrqCCU61SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($CCU61PrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//
[!ENDIF!][!//


[!VAR "IrqGptExist" = "'STD_OFF'"!][!//
[!IF "num:i(count(IrqGptConfig/*)) = 1"!][!//
/*
                     Container : IrqGPTinterruptConfiguration
*/
[!VAR "IrqGptExist" = "'STD_ON'"!][!//
[!IF "contains( ecu:get('Irq.GPT12Available'), 'IrqGPT120' )"!][!//
/* GPT120 interrupt Category setting */
[!LOOP "IrqGptConfig/*[1]/IrqGpt120Config/IrqGpt120CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!IF "contains($NodeName, 'IrqGpt120CARPEL') = 'true'"!][!//
[!CODE!][!//
#define IRQ_GPT120_CARPEL_CAT  ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!IF "contains($NodeName, 'IrqGpt120T') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_GPT120_T', (substring-before(substring-after($NodeName, "IrqGpt120T"), "Cat")), '_CAT')"!]      ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!ENDLOOP!][!//

/*  GPT120 interrupt Priority setting  */

[!LOOP "IrqGptConfig/*[1]/IrqGpt120Config/IrqGpt120PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "cont" = "'GPT120'"!][!//
[!IF "contains($NodeName, 'IrqGpt120CARPEL') = 'true'"!][!//
[!VAR "TypeOfService" = "node:value(../../IrqGpt120TosConfig/IrqGpt120CARPELTos)"!][!//
[!VAR "Gpt120CatVal" = "node:value(../../IrqGpt120CatConfig/IrqGpt120CARPELCat)"!][!//
[!VAR "SrnId" = "num:i(0)"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define IRQ_GPT120_CARPEL_PRIO  [!"num:inttohex($Givenno)"!]
[!ELSE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGpt120T"), "Prio"))"!][!//
[!SELECT "../../IrqGpt120TosConfig"!][!//
[!VAR "Gpt120TosParam" = "concat('IrqGpt120T',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Gpt120TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqGpt120CatConfig"!][!//
[!VAR "Gpt120CatParam" = "concat('IrqGpt120T',$SrnId,'Cat')"!][!//
[!VAR "Gpt120CatVal" = "node:value($Gpt120CatParam)"!][!//
[!ENDSELECT!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_GPT120_T', (substring-before(substring-after($NodeName, "IrqGpt120T"), "Prio")), '_PRIO')"!]      [!"num:inttohex($Givenno)"!]
[!ENDIF!][!//
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Gpt120CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of GPT120 interrupt Priority Setting for loop */!][!//

/* GPT120 interrupt type of service setting */
[!LOOP "IrqGptConfig/*[1]/IrqGpt120Config/IrqGpt120TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!IF "contains($NodeName, 'IrqGpt120CARPEL') = 'true'"!][!//
[!CODE!][!//
#define IRQ_GPT120_CARPEL_TOS  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGpt120"),"Tos"))"!][!//
[!VAR "PrioVal" = "node:value(../../IrqGpt120PrioConfig/IrqGpt120CARPELPrio)"!][!//
[!ENDNOCODE!][!//
[!ELSE!][!//
[!CODE!][!//
#define [!" concat ('IRQ_GPT120_T', (substring-before(substring-after($NodeName, "IrqGpt120T"), "Tos")), '_TOS')"!]      ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGpt120T"),"Tos"))"!][!//
[!SELECT "../../IrqGpt120PrioConfig"!][!//
[!VAR "Gpt120TParam" = "concat('IrqGpt120T', $SrnId, 'Prio')"!]
[!VAR "PrioVal" = "num:i(node:value($Gpt120TParam))"!][!//
[!ENDSELECT!][!//
[!ENDNOCODE!][!//
[!ENDIF!][!//
[!NOCODE!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
 [!IF "$SrnId = 'CARPEL'"!][!//
 [!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i(0)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
 [!ENDASSERT!][!//
 [!ELSE!][!//
 [!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)-num:i(1)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
 [!ENDASSERT!][!//
 [!ENDIF!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//
[!ENDIF!][!//[!/* End of GPT interrupt */!]  


[!VAR "IrqStmExist" = "'STD_OFF'"!][!//
[!IF "num:i(count(IrqStmConfig/*)) = 1"!][!//
/*
                     Container : IrqSTMinterruptConfiguration
*/
[!VAR "IrqStmExist" = "'STD_ON'"!][!//
[!IF "contains( ecu:get('Irq.STMAvailable'), 'IrqSTM0' )"!][!//
/* STM0 interrupt Category Setting */ 
[!LOOP "IrqStmConfig/*[1]/IrqStm0Config/IrqStm0CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_STM0_SR', (substring-before(substring-after($NodeName, "IrqStm0SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//
	  
/* STM0 interrupt Priority Setting */  

[!LOOP "IrqStmConfig/*[1]/IrqStm0Config/IrqStm0PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqStm0SR"), "Prio"))"!][!//
[!SELECT "../../IrqStm0TosConfig"!][!//
[!VAR "Stm0TosParam" = "concat('IrqStm0SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Stm0TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqStm0CatConfig"!][!//
[!VAR "Stm0CatParam" = "concat('IrqStm0SR',$SrnId,'Cat')"!][!//
[!VAR "Stm0CatVal" = "node:value($Stm0CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'STM0'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_STM0_SR', (substring-before(substring-after($NodeName, "IrqStm0SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Stm0CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of STM0 interrupt Priority Setting for loop */!][!//


/* STM0 interrupt type of service Setting */ 
[!LOOP "IrqStmConfig/*[1]/IrqStm0Config/IrqStm0TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_STM0_SR', (substring-before(substring-after($NodeName, "IrqStm0SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqStm0SR"),"Tos"))"!][!//
[!SELECT "../../IrqStm0PrioConfig"!][!//
[!VAR "Stm0SRPrioParam" = "concat('IrqStm0SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($Stm0SRPrioParam))"!][!//
[!ENDSELECT!]
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.STMAvailable'), 'IrqSTM1' )"!][!//
/* STM1 interrupt Category Setting */ 
[!LOOP "IrqStmConfig/*[1]/IrqStm1Config/IrqStm1CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_STM1_SR', (substring-before(substring-after($NodeName, "IrqStm1SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//
  
/* STM1 interrupt Priority Setting */  
[!LOOP "IrqStmConfig/*[1]/IrqStm1Config/IrqStm1PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqStm1SR"), "Prio"))"!][!//
[!SELECT "../../IrqStm1TosConfig"!][!//
[!VAR "Stm1TosParam" = "concat('IrqStm1SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Stm1TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqStm1CatConfig"!][!//
[!VAR "Stm1CatParam" = "concat('IrqStm1SR',$SrnId,'Cat')"!][!//
[!VAR "Stm1CatVal" = "node:value($Stm1CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'STM1'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_STM1_SR', (substring-before(substring-after($NodeName, "IrqStm1SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Stm1CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of STM1 interrupt Priority Setting for loop */!][!//

/* STM1 interrupt type of service Setting */ 
[!LOOP "IrqStmConfig/*[1]/IrqStm1Config/IrqStm1TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_STM1_SR', (substring-before(substring-after($NodeName, "IrqStm1SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqStm1SR"),"Tos"))"!][!//
[!SELECT "../../IrqStm1PrioConfig"!][!//
[!VAR "Stm1SRPrioParam" = "concat('IrqStm1SR', $SrnId, 'Tos')"!][!//
[!VAR "PrioVal" = "num:i(node:value($Stm1SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//


[!IF "contains( ecu:get('Irq.STMAvailable'), 'IrqSTM2' )"!][!//
/* STM2 interrupt Category Setting */ 
[!LOOP "IrqStmConfig/*[1]/IrqStm2Config/IrqStm2CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_STM2_SR', (substring-before(substring-after($NodeName, "IrqStm2SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

   
/* STM2 interrupt Priority Setting */  
[!LOOP "IrqStmConfig/*[1]/IrqStm2Config/IrqStm2PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqStm2SR"), "Prio"))"!][!//
[!SELECT "../../IrqStm2TosConfig"!][!//
[!VAR "Stm2TosParam" = "concat('IrqStm2SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Stm2TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqStm2CatConfig"!][!//
[!VAR "Stm2CatParam" = "concat('IrqStm2SR',$SrnId,'Cat')"!][!//
[!VAR "Stm2CatVal" = "node:value($Stm2CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'STM2'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_STM2_SR', (substring-before(substring-after($NodeName, "IrqStm2SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Stm2CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of STM2 interrupt Priority Setting for loop */!][!//


/* STM2 interrupt type of service Setting */ 
[!LOOP "IrqStmConfig/*[1]/IrqStm2Config/IrqStm2TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_STM2_SR', (substring-before(substring-after($NodeName, "IrqStm2SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqStm2SR"),"Tos"))"!][!//
[!SELECT "../../IrqStm2PrioConfig"!][!//
[!VAR "Stm2SRPrioParam" = "concat('IrqStm2SR', $SrnId, 'Tos')"!][!//
[!VAR "PrioVal" = "num:i(node:value($Stm2SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//
[!ENDIF!][!//

[!VAR "IrqDmaExist" = "'STD_OFF'"!][!//
[!IF "num:i(count(IrqDmaConfig/*)) = 1"!][!//
/*
                     Container : IrqDMAinterruptConfiguration
*/
[!VAR "IrqDmaExist" = "'STD_ON'"!][!//
/* DMA interrupt Category settings */
[!LOOP "IrqDmaConfig/*[1]/IrqDmaCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!IF "contains($NodeName, 'Err') = 'true'"!][!//
[!CODE!][!//
#define IRQ_DMA_ERR_SR_CAT       ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!IF "contains($NodeName, 'Ch') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_DMA_CHANNEL', (substring-before(substring-after($NodeName, "IrqDmaCh"), "SRCat")), '_SR_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!ENDLOOP!][!//

/* DMA interrupt Priority settings */
[!LOOP "IrqDmaConfig/*[1]/IrqDmaPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "cont" = "'DMA'"!][!//
[!IF "contains($NodeName, 'Err') = 'true'"!][!//
[!VAR "TypeOfService" = "node:value(../../../*[1]/IrqDmaTosConfig/IrqDmaErrSRTos)"!][!//
[!VAR "DmaCatVal" = "node:value(../../../*[1]/IrqDmaCatConfig/IrqDmaErrSRCat)"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define IRQ_DMA_ERR_SR_PRIO       [!"num:inttohex($Givenno)"!]
[!ELSE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqDmaCh"), "SRPrio"))"!][!//
[!SELECT "../../../*[1]/IrqDmaTosConfig"!][!//
[!VAR "DmaTosParam" = "concat('IrqDmaCh',$SrnId,'SRTos')"!][!//
[!VAR "TypeOfService" = "node:value($DmaTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../../*[1]/IrqDmaCatConfig"!][!//
[!VAR "DmaCatParam" = "concat('IrqDmaCh',$SrnId,'SRCat')"!][!//
[!VAR "DmaCatVal" = "node:value($DmaCatParam)"!][!//
[!ENDSELECT!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_DMA_CHANNEL', (substring-before(substring-after($NodeName, "IrqDmaCh"), "SRPrio")), '_SR_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!ENDIF!][!//
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($DmaCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of DMA interrupt Priority Setting for loop */!][!//

/* DMA interrupt type of service settings */
[!LOOP "IrqDmaConfig/*[1]/IrqDmaTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!IF "contains($NodeName, 'Err') = 'true'"!][!//
[!CODE!][!//
#define IRQ_DMA_ERR_SR_TOS       ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!ELSE!][!//
[!CODE!][!//
#define [!" concat ('IRQ_DMA_CHANNEL', (substring-before(substring-after($NodeName, "IrqDmaCh"), "SRTos")), '_SR_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!NOCODE!][!//
[!IF "contains($NodeName, 'Err') = 'true'"!][!//
[!VAR "SrnId" = "num:i(16)"!][!//
[!VAR "PrioVal" = "node:value(../../../*[1]/IrqDmaPrioConfig/IrqDmaErrSRPrio)"!][!//
[!ELSE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqDmaCh"),"SRTos"))"!][!//
[!SELECT "../../../*[1]/IrqDmaPrioConfig"!][!//
[!VAR "DmaPrioParam" = "concat('IrqDmaCh', $SrnId, 'SRPrio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($DmaPrioParam))"!][!//
[!ENDSELECT!][!//
[!ENDIF!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
 [!IF "$DmaPrioParam = 'IrqDmaErrSRPrio'"!][!//
 [!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"$SrnId"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
 [!ENDASSERT!][!//
 [!ELSE!][!//
 [!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
 [!ENDASSERT!][!//
 [!ENDIF!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//

[!/* Macro for Dma.Irq.c */!][!//
[!VAR "AdcCheck" = "'false'"!][!//
[!VAR "SpiCheck" = "'false'"!][!//
[!SELECT "as:modconf('Adc')[1]"!][!//
[!IF "AdcGeneral/AdcUseEmux = 'true'"!][!//
[!VAR "AdcCheck" = "'true'"!][!//
[!ENDIF!][!//
[!ENDSELECT!][!//
[!SELECT "as:modconf('Spi')[1]"!][!//
[!IF "SpiGeneral/SpiLevelDelivered = 1 or SpiGeneral/SpiLevelDelivered = 2"!][!//
[!VAR "SpiCheck" = "'true'"!][!//
[!ENDIF!][!//
[!IF "SpiGeneral/SpiLevelDelivered = 0"!][!//
[!IF "SpiSafety/SpiSafetyEnable = 'true'"!][!//
[!IF "SpiGeneral/SpiSlaveSelectRef != 'SPI_SLAVE_UNUSED'"!]
[!VAR "SpiCheck" = "'true'"!]
[!ENDIF!][!//
[!ENDIF!][!//
[!ENDIF!][!//

[!ENDSELECT!][!//

[!ENDIF!][!/* End of DMA interrupt Setting */!][!//


/* Macro for Dma_Irq.c*/
[!FOR "SrnId" ="num:i(1)" TO "num:i(count(IrqDmaConfig/*[1]/IrqDmaCatConfig/*))"!][!//
[!VAR "ParamName" = "node:name(IrqDmaConfig/*[1]/IrqDmaCatConfig/*[num:i($SrnId)])"!][!//
[!IF "not(contains($ParamName,'Ch'))"!][!//
[!ELSE!][!//
[!NOCODE!][!//
[!VAR "ChNum" = "substring-after($ParamName,'IrqDmaCh')"!][!//
[!VAR "ChNum" = "substring-before($ChNum,'SRCat')"!][!//
[!VAR "UsedResource" = "'IRQ_DMA_AVAILABLE'"!][!//
[!VAR "UsedParam" = "'255'"!][!//
[!VAR "DMAChannelId" = "'255'"!][!//
[!// Check whether the given DMA channel is used in ADC 
[!IF "$AdcCheck = 'true'"!][!//
[!SELECT "as:modconf('Adc')[1]"!][!//
[!VAR "TotalAdcConfig" = "num:i(count(AdcConfigSet/*))"!][!//
	[!FOR "CfgCount" ="num:i(1)" TO "num:i($TotalAdcConfig)"!][!//
	[!SELECT "AdcConfigSet/*[num:i($CfgCount)]"!][!//
	[!VAR "TotalAdcUnit" = "num:i(count(AdcHwUnit/*))"!][!//
	[!FOR "AdcId" ="num:i(1)" TO "num:i($TotalAdcUnit)"!][!//
		[!SELECT "AdcHwUnit/*[num:i($AdcId)]"!][!//
		[!VAR "TotalAdcGroups"= "num:i(count(AdcGroup/*))"!][!//
		[!FOR "GroupId" ="num:i(0)" TO "num:i($TotalAdcGroups)-1"!][!//
		[!VAR "AdcEmuxScan" = "AdcGroup/*[@index = $GroupId]/AdcEmuxScanEnable"!][!//
		[!IF "($AdcEmuxScan = 'true')"!][!//
		[!ASSERT "node:refexists(./AdcDmaChannelRef) = 'true'", "DMA Channel reference not provided"!][!//
		[!VAR "DMAChannelId" = "(node:ref(./AdcDmaChannelRef)/ChannelId)"!][!//
		[!ENDIF!][!//
		[!IF "$DMAChannelId = $ChNum"!][!//
		[!VAR "UsedResource" = "'IRQ_DMA_USED_MCALADC'"!][!//
	  [!VAR "UsedParam1" = "./AdcHwUnitId"!][!//
		[!VAR "UsedParam" = "substring-after($UsedParam1,'ADC')"!]
		[!BREAK!][!//
		[!ENDIF!][!//		
		[!ENDFOR!][!//
		[!ENDSELECT!][!//
		[!IF "$DMAChannelId = $ChNum"!][!//
		[!BREAK!][!//
		[!ENDIF!][!//		
    [!ENDFOR!][!//
	[!ENDSELECT!][!//
	[!ENDFOR!][!//
[!ENDSELECT!][!//
[!ENDIF!][!//
[!IF "$SpiCheck = 'true'"!][!//
	[!SELECT "as:modconf('Spi')[1]"!][!//  
	[!FOR "HwNumber" = "num:i(0)" TO "num:i(5)"!][!//
	[!VAR "SpiDmaRef" = "concat('SpiHwConfiguration/SpiHwConfigurationQspi',$HwNumber)"!][!//  
	[!VAR "DmaTxNode" = "concat($SpiDmaRef,'/SpiHwDmaConfiguration/SpiHwDmaChannelTransmissionRef')"!][!//
	[!VAR "DmaRxNode" = "concat($SpiDmaRef,'/SpiHwDmaConfiguration/SpiHwDmaChannelReceptionRef')"!][!//
    [!IF "node:exists($DmaTxNode) = 'true'"!][!//
    [!IF "node:refexists(node:ref($DmaTxNode)) = 'true'"!][!//
	[!VAR "DmaChTxNum" = "node:ref(node:ref($DmaTxNode))/ChannelId"!][!//
	 [!IF "$DmaChTxNum = $ChNum"!][!//
	  [!VAR "UsedResource" = "'IRQ_DMA_USED_MCALSPI_TX'"!][!//
	  [!VAR "UsedParam" = "num:i($HwNumber)"!][!//
	  [!ENDIF!][!//
	[!ENDIF!]
	[!ENDIF!]
    [!IF "node:exists($DmaRxNode) = 'true'"!][!//
	[!IF "node:refexists(node:ref($DmaRxNode)) = 'true'"!][!//
	[!VAR "DmaChRxNum" = "node:ref(node:ref($DmaRxNode))/ChannelId"!][!//
	  [!IF "$DmaChRxNum = $ChNum"!][!//
	  [!VAR "UsedResource" = "'IRQ_DMA_USED_MCALSPI_RX'"!][!//
	  [!VAR "UsedParam" = "num:i($HwNumber)"!][!//
	  [!ENDIF!][!//
	[!ENDIF!][!//
	[!ENDIF!][!//	
	[!ENDFOR!][!//
	[!ENDSELECT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!IF "($ChNum <= 9)"!][!//
#define IRQ_DMA_CHANNEL[!"num:i($ChNum)"!]_USED        ([!"$UsedResource"!])[!CR!][!//
#define IRQ_DMA_CHANNEL[!"num:i($ChNum)"!]_PARAM       ([!"$UsedParam"!]U)[!CR!][!//
[!ELSE!][!//
#define IRQ_DMA_CHANNEL[!"num:i($ChNum)"!]_USED       ([!"$UsedResource"!])[!CR!][!//
#define IRQ_DMA_CHANNEL[!"num:i($ChNum)"!]_PARAM      ([!"$UsedParam"!]U)[!CR!][!//
[!ENDIF!][!//
[!ENDIF!][!//
[!ENDFOR!][!//

/* Macro for AscLin_Irq.c*/
[!FOR "SrnId" ="num:i(1)" TO "num:i(count(IrqAscLinConfig/*[1]/IrqAscLinCatConfig/IrqAscLinErrCatConfig/*))"!][!//
[!VAR "ParamName" = "node:name(IrqAscLinConfig/*[1]/IrqAscLinCatConfig/IrqAscLinErrCatConfig/*[num:i($SrnId)])"!][!//
[!NOCODE!][!//
[!VAR "ChNum" = "substring-after($ParamName,'IrqAscLin')"!][!//
[!VAR "ChNum" = "substring-before($ChNum,'ErrorCat')"!][!//
[!VAR "UsedResource" = "'IRQ_ASCLIN_AVAILABLE'"!][!//
[!// Check whether the given ASCLIN channel is used in LIN 
[!SELECT "as:modconf('Lin')[1]"!][!//
[!VAR "TotalLinConfig" = "num:i(count(LinGlobalConfig/*))"!][!//
	[!FOR "CfgCount" ="num:i(1)" TO "num:i($TotalLinConfig)"!][!//
	[!SELECT "LinGlobalConfig/*[num:i($CfgCount)]"!][!//
	[!VAR "TotalLinUnit" = "num:i(count(LinChannel/*))"!][!//
	[!FOR "LinId" ="num:i(1)" TO "num:i($TotalLinUnit)"!][!//
		[!SELECT "LinChannel/*[num:i($LinId)]"!][!//
		[!VAR "LinHwChan"= "./LinChanAssignedHw"!][!//
    [!VAR "CurChNum" = "substring-after($LinHwChan,'ASCLIN')"!][!//
		[!IF "$CurChNum = $ChNum"!][!//
		[!VAR "UsedResource" = "'IRQ_ASCLIN_USED_MCALLIN'"!][!//
		[!BREAK!][!//
		[!ENDIF!][!//		
		[!ENDSELECT!][!//
    [!ENDFOR!][!//
	[!ENDSELECT!][!//
	[!ENDFOR!][!//
[!ENDSELECT!][!//
[!// Check whether the given ASCLIN channel is used in UART 
[!SELECT "as:modconf('Uart')[1]"!][!//
[!VAR "TotalUartConfig" = "num:i(count(UartConfigSet/*))"!][!//
	[!FOR "CfgCount" ="num:i(1)" TO "num:i($TotalUartConfig)"!][!//
	[!SELECT "UartConfigSet/*[num:i($CfgCount)]"!][!//
	[!VAR "TotalUartUnit" = "num:i(count(UartChannel/*))"!][!//
	[!FOR "UartId" ="num:i(1)" TO "num:i($TotalUartUnit)"!][!//
		[!SELECT "UartChannel/*[num:i($UartId)]"!][!//
		[!VAR "UartHwChan"= "./UartHwUnit"!][!//
    [!VAR "CurChNum" = "substring-after($UartHwChan,'ASCLIN')"!][!//
		[!IF "$CurChNum = $ChNum"!][!//
		[!VAR "UsedResource" = "'IRQ_ASCLIN_USED_MCALUART'"!][!//
		[!BREAK!][!//
		[!ENDIF!][!//		
		[!ENDSELECT!][!//
    [!ENDFOR!][!//
	[!ENDSELECT!][!//
	[!ENDFOR!][!//
[!ENDSELECT!][!//
[!// Check whether the given ASCLIN channel is used in STDLIN 
[!SELECT "as:modconf('StdLin')[1]"!][!//
[!VAR "TotalStdLinConfig" = "num:i(count(StdLinConfigSet/*))"!][!//
	[!FOR "CfgCount" ="num:i(1)" TO "num:i($TotalStdLinConfig)"!][!//
	[!SELECT "StdLinConfigSet/*[num:i($CfgCount)]"!][!//
	[!VAR "TotalStdLinUnit" = "num:i(count(StdLinChannel/*))"!][!//
	[!FOR "StdLinId" ="num:i(1)" TO "num:i($TotalStdLinUnit)"!][!//
		[!SELECT "StdLinChannel/*[num:i($StdLinId)]"!][!//
		[!VAR "StdLinHwChan"= "./StdLinHwUnit"!][!//
    [!VAR "CurChNum" = "substring-after($StdLinHwChan,'ASCLIN')"!][!//
		[!IF "$CurChNum = $ChNum"!][!//
		[!ASSERT "$UsedResource = 'IRQ_ASCLIN_AVAILABLE'", "CONFIG ERROR: ASCLIN channel is used by  STDLIN and LIN/UART"!][!//
		[!VAR "UsedResource" = "'IRQ_ASCLIN_USED_MCALSTDLIN'"!][!//
		[!BREAK!][!//
		[!ENDIF!][!//		
		[!ENDSELECT!][!//
    [!ENDFOR!][!//
	[!ENDSELECT!][!//
	[!ENDFOR!][!//
[!ENDSELECT!][!//
[!ENDNOCODE!][!//
#define IRQ_ASCLIN_CHANNEL[!"num:i($ChNum)"!]_USED      ([!"$UsedResource"!])[!CR!][!//
[!ENDFOR!][!//



[!VAR "IrqETHExist" = "'STD_OFF'"!][!//
[!IF "num:i(count(IrqETHConfig/*)) = 1"!][!//
/*
                     Container : Ethernet interrupt Configuration
*/
[!NOCODE!][!//
[!VAR "IrqETHExist" = "'STD_ON'"!][!//	
[!ENDNOCODE!][!//
/* Ethernet interrupt Category Setting*/
[!LOOP "IrqETHConfig/*[1]/IrqETHCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define IRQ_ETH_SR_CAT  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//
/* Ethernet interrupt priority Setting*/
[!LOOP "IrqETHConfig/*[1]/IrqETHPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "TypeOfService" = "node:value(../../../../IrqETHConfig/*[1]/IrqETHTosConfig/IrqETHSRTos)"!][!//
[!VAR "ETHCatVal" = "node:value(../../../../IrqETHConfig/*[1]/IrqETHCatConfig/IrqETHSRCat)"!][!//
[!VAR "cont" = "'ETH'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define IRQ_ETH_SR_PRIO [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($ETHCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of Ethernet interrupt Priority Setting for loop */!][!//
/* Ethernet interrupt type of service Setting*/
[!LOOP "IrqETHConfig/*[1]/IrqETHTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define IRQ_ETH_SR_TOS  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!SELECT "../../../*[1]/IrqETHPrioConfig"!][!//
[!VAR "PrioVal" = "num:i(node:value(IrqETHSRPrio))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i(0)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "num:i(count(IrqCanConfig/*)) = 1"!][!//
/*
                     Container : IrqCaninterruptConfiguration
*/
/* CAN interrupt Category setting */
[!LOOP "IrqCanConfig/*[1]/IrqCanCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_CAN_SR', (substring-before(substring-after($NodeName, "IrqCanSR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*  CAN interrupt Priority setting  */
[!LOOP "IrqCanConfig/*[1]/IrqCanPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqCanSR"), "Prio"))"!][!//
[!SELECT "../../../../IrqCanConfig/*[1]/IrqCanTosConfig"!][!//
[!VAR "CanTosParam" = "concat('IrqCanSR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($CanTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../../../IrqCanConfig/*[1]/IrqCanCatConfig"!][!//
[!VAR "CanCatParam" = "concat('IrqCanSR',$SrnId,'Cat')"!][!//
[!VAR "CanCatVal" = "node:value($CanCatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'CAN'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_CAN_SR', (substring-before(substring-after($NodeName, "IrqCanSR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($CanCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of CAN interrupt Priority Setting for loop */!][!//


/* CAN interrupt type of service setting */
[!LOOP "IrqCanConfig/*[1]/IrqCanTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_CAN_SR', (substring-before(substring-after($NodeName, "IrqCanSR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqCanSR"),"Tos"))"!][!//
[!SELECT "../../../*[1]/IrqCanPrioConfig"!][!//
[!VAR "CanSRPrioParam" = "concat('IrqCanSR',$SrnId,'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($CanSRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//


[!IF "num:i(count(IrqHSMConfig/*)) = 1"!][!//
/*
                     Container : IrqHSMinterruptConfiguration
*/
/* HSM interrupt Category setting */
[!LOOP "IrqHSMConfig/*[1]/IrqHSMCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_HSM_SR', (substring-before(substring-after($NodeName, "IrqHSMSR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*  HSM interrupt Priority setting  */
[!LOOP "IrqHSMConfig/*[1]/IrqHSMPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqHSMSR"), "Prio"))"!][!//
[!SELECT "../../../*[1]/IrqHSMTosConfig"!][!//
[!VAR "HSMTosParam" = "concat('IrqHSMSR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($HSMTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../../*[1]/IrqHSMCatConfig"!][!//
[!VAR "HSMCatParam" = "concat('IrqHSMSR',$SrnId,'Cat')"!][!//
[!VAR "HSMCatVal" = "node:value($HSMCatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'HSM'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_HSM_SR', (substring-before(substring-after($NodeName, "IrqHSMSR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($HSMCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of HSM interrupt Priority Setting for loop */!][!//

/* HSM interrupt type of service setting */
[!LOOP "IrqHSMConfig/*[1]/IrqHSMTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_HSM_SR', (substring-before(substring-after($NodeName, "IrqHSMSR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqHSMSR"),"Tos"))"!][!//
[!SELECT "../../../*[1]/IrqHSMPrioConfig"!][!//
[!VAR "HSMPrioParam" = "concat('IrqHSMSR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($HSMPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!//  ADC configuration
[!VAR "IrqAdcExist" = "'STD_OFF'"!][!//
[!IF "num:i(count(IrqAdcConfig/*)) = 1"!][!//
/*
                     Container : Irq ADC configuration
*/
[!VAR "IrqAdcExist" = "'STD_ON'"!][!//
[!IF "contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC0' )"!][!//
/*ADC0 interrupt Category setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc0Config/IrqAdc0CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC0_SR', (substring-before(substring-after($NodeName, "IrqAdc0SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*ADC0 interrupt Priority setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc0Config/IrqAdc0PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc0SR"), "Prio"))"!][!//
[!SELECT "../../IrqAdc0TosConfig"!][!//
[!VAR "Adc0TosParam" = "concat('IrqAdc0SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Adc0TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqAdc0CatConfig"!][!//
[!VAR "Adc0CatParam" = "concat('IrqAdc0SR',$SrnId,'Cat')"!][!//
[!VAR "Adc0CatVal" = "node:value($Adc0CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ADC0'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ADC0_SR', (substring-before(substring-after($NodeName, "IrqAdc0SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Adc0CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ADC0 interrupt Priority Setting for loop */!][!//

/*ADC0 interrupt type of service setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc0Config/IrqAdc0TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC0_SR', (substring-before(substring-after($NodeName, "IrqAdc0SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc0SR"),"Tos"))"!][!//
[!SELECT "../../IrqAdc0PrioConfig"!][!//
[!VAR "Adc0SRPrioParam" = "concat('IrqAdc0SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($Adc0SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC1' )"!][!//
/*ADC1 interrupt Category setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc1Config/IrqAdc1CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC1_SR', (substring-before(substring-after($NodeName, "IrqAdc1SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*ADC1 interrupt Priority setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc1Config/IrqAdc1PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc1SR"), "Prio"))"!][!//
[!SELECT "../../IrqAdc1TosConfig"!][!//
[!VAR "Adc1TosParam" = "concat('IrqAdc1SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Adc1TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqAdc1CatConfig"!][!//
[!VAR "Adc1CatParam" = "concat('IrqAdc1SR',$SrnId,'Cat')"!][!//
[!VAR "Adc1CatVal" = "node:value($Adc1CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ADC1'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ADC1_SR', (substring-before(substring-after($NodeName, "IrqAdc1SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Adc1CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ADC1 interrupt Priority Setting for loop */!][!//

/*ADC1 interrupt type of service setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc1Config/IrqAdc1TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC1_SR', (substring-before(substring-after($NodeName, "IrqAdc1SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc1SR"),"Tos"))"!][!//
[!SELECT "../../IrqAdc1PrioConfig"!][!//
[!VAR "Adc1SRPrioParam" = "concat('IrqAdc1SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($Adc1SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC2' )"!][!//
/*ADC2 interrupt Category setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc2Config/IrqAdc2CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC2_SR', (substring-before(substring-after($NodeName, "IrqAdc2SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//


/*ADC2 interrupt Priority setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc2Config/IrqAdc2PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc2SR"), "Prio"))"!][!//
[!SELECT "../../IrqAdc2TosConfig"!][!//
[!VAR "Adc2TosParam" = "concat('IrqAdc2SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Adc2TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqAdc2CatConfig"!][!//
[!VAR "Adc2CatParam" = "concat('IrqAdc2SR',$SrnId,'Cat')"!][!//
[!VAR "Adc2CatVal" = "node:value($Adc2CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ADC2'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ADC2_SR', (substring-before(substring-after($NodeName, "IrqAdc2SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Adc2CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ADC2 interrupt Priority Setting for loop */!][!//

/*ADC2 interrupt type of service setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc2Config/IrqAdc2TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC2_SR', (substring-before(substring-after($NodeName, "IrqAdc2SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc2SR"),"Tos"))"!][!//
[!SELECT "../../IrqAdc2PrioConfig"!][!//
[!VAR "Adc2SRPrioParam" = "concat('IrqAdc2SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($Adc2SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC3' )"!][!//
/*ADC3 interrupt Category setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc3Config/IrqAdc3CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC3_SR', (substring-before(substring-after($NodeName, "IrqAdc3SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*ADC3 interrupt Priority setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc3Config/IrqAdc3PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc3SR"), "Prio"))"!][!//
[!SELECT "../../IrqAdc3TosConfig"!][!//
[!VAR "Adc3TosParam" = "concat('IrqAdc3SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Adc3TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqAdc3CatConfig"!][!//
[!VAR "Adc3CatParam" = "concat('IrqAdc3SR',$SrnId,'Cat')"!][!//
[!VAR "Adc3CatVal" = "node:value($Adc3CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ADC3'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ADC3_SR', (substring-before(substring-after($NodeName, "IrqAdc3SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Adc3CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ADC3 interrupt Priority Setting for loop */!][!//

/*ADC3 interrupt type of service setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc3Config/IrqAdc3TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC3_SR', (substring-before(substring-after($NodeName, "IrqAdc3SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc3SR"),"Tos"))"!][!//
[!SELECT "../../../IrqAdc3PrioConfig"!][!//
[!VAR "Adc3SRPrioParam" = "concat('IrqAdc3SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($Adc3SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC4' )"!][!//
/*ADC4 interrupt Category setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc4Config/IrqAdc4CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC4_SR', (substring-before(substring-after($NodeName, "IrqAdc4SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*ADC4 interrupt Priority setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc4Config/IrqAdc4PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc4SR"), "Prio"))"!][!//
[!SELECT "../../IrqAdc4TosConfig"!][!//
[!VAR "Adc4TosParam" = "concat('IrqAdc4SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Adc4TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqAdc4CatConfig"!][!//
[!VAR "Adc4CatParam" = "concat('IrqAdc4SR',$SrnId,'Cat')"!][!//
[!VAR "Adc4CatVal" = "node:value($Adc4CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ADC4'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ADC4_SR', (substring-before(substring-after($NodeName, "IrqAdc4SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Adc4CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ADC4 interrupt Priority Setting for loop */!][!//

/*ADC4 interrupt type of service setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc4Config/IrqAdc4TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC4_SR', (substring-before(substring-after($NodeName, "IrqAdc4SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc4SR"),"Tos"))"!][!//
[!SELECT "../../../IrqAdc4PrioConfig"!][!//
[!VAR "Adc4SRPrioParam" = "concat('IrqAdc4SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($Adc4SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC5' )"!][!//
/*ADC5 interrupt Category setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc5Config/IrqAdc5CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC5_SR', (substring-before(substring-after($NodeName, "IrqAdc5SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*ADC5 interrupt Priority setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc5Config/IrqAdc5PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc5SR"), "Prio"))"!][!//
[!SELECT "../../IrqAdc5TosConfig"!][!//
[!VAR "Adc5TosParam" = "concat('IrqAdc5SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Adc5TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqAdc5CatConfig"!][!//
[!VAR "Adc5CatParam" = "concat('IrqAdc5SR',$SrnId,'Cat')"!][!//
[!VAR "Adc5CatVal" = "node:value($Adc5CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ADC5'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ADC5_SR', (substring-before(substring-after($NodeName, "IrqAdc5SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Adc5CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ADC5 interrupt Priority Setting for loop */!][!//

/*ADC5 interrupt type of service setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc5Config/IrqAdc5TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC5_SR', (substring-before(substring-after($NodeName, "IrqAdc5SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc5SR"),"Tos"))"!][!//
[!SELECT "../../IrqAdc5PrioConfig"!][!//
[!VAR "Adc5SRPrioParam" = "concat('IrqAdc5SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($Adc5SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC6' )"!][!//
/*ADC6 interrupt Category setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc6Config/IrqAdc6CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC6_SR', (substring-before(substring-after($NodeName, "IrqAdc6SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*ADC6 interrupt Priority setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc6Config/IrqAdc6PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc6SR"), "Prio"))"!][!//
[!SELECT "../../IrqAdc6TosConfig"!][!//
[!VAR "Adc6TosParam" = "concat('IrqAdc6SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Adc6TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqAdc6CatConfig"!][!//
[!VAR "Adc6CatParam" = "concat('IrqAdc6SR',$SrnId,'Cat')"!][!//
[!VAR "Adc6CatVal" = "node:value($Adc6CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ADC6'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ADC6_SR', (substring-before(substring-after($NodeName, "IrqAdc6SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Adc6CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ADC6 interrupt Priority Setting for loop */!][!//

/*ADC6 interrupt type of service setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc6Config/IrqAdc6TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC6_SR', (substring-before(substring-after($NodeName, "IrqAdc6SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc6SR"),"Tos"))"!][!//
[!SELECT "../../IrqAdc6PrioConfig"!][!//
[!VAR "Adc6SRPrioParam" = "concat('IrqAdc6SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($Adc6SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC7' )"!][!//
/*ADC7 interrupt Category setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc7Config/IrqAdc7CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC7_SR', (substring-before(substring-after($NodeName, "IrqAdc7SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*ADC7 interrupt Priority setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc7Config/IrqAdc7PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc7SR"), "Prio"))"!][!//
[!SELECT "../../IrqAdc7TosConfig"!][!//
[!VAR "Adc7TosParam" = "concat('IrqAdc7SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Adc7TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqAdc7CatConfig"!][!//
[!VAR "Adc7CatParam" = "concat('IrqAdc7SR',$SrnId,'Cat')"!][!//
[!VAR "Adc7CatVal" = "node:value($Adc7CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ADC7'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ADC7_SR', (substring-before(substring-after($NodeName, "IrqAdc7SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Adc7CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ADC7 interrupt Priority Setting for loop */!][!//

/*ADC7 interrupt type of service setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc7Config/IrqAdc7TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC7_SR', (substring-before(substring-after($NodeName, "IrqAdc7SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc7SR"),"Tos"))"!][!//
[!SELECT "../../IrqAdc7PrioConfig"!][!//
[!VAR "Adc7SRPrioParam" = "concat('IrqAdc7SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($Adc7SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC8' )"!][!//
/*ADC8 interrupt Category setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc8Config/IrqAdc8CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC8_SR', (substring-before(substring-after($NodeName, "IrqAdc8SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*ADC8 interrupt Priority setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc8Config/IrqAdc8PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc8SR"), "Prio"))"!]
[!SELECT "../../IrqAdc8TosConfig"!][!//
[!VAR "Adc8TosParam" = "concat('IrqAdc8SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Adc8TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqAdc8CatConfig"!][!//
[!VAR "Adc8CatParam" = "concat('IrqAdc8SR',$SrnId,'Cat')"!][!//
[!VAR "Adc8CatVal" = "node:value($Adc8CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ADC8'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ADC8_SR', (substring-before(substring-after($NodeName, "IrqAdc8SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Adc8CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ADC8 interrupt Priority Setting for loop */!][!//

/*ADC8 interrupt type of service setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc8Config/IrqAdc8TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC8_SR', (substring-before(substring-after($NodeName, "IrqAdc8SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc8SR"),"Tos"))"!][!//
[!SELECT "../../IrqAdc8PrioConfig"!][!//
[!VAR "Adc8SRPrioParam" = "concat('IrqAdc8SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($Adc8SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC9' )"!][!//
/*ADC9 interrupt Category setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc9Config/IrqAdc9CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC9_SR', (substring-before(substring-after($NodeName, "IrqAdc9SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*ADC9 interrupt Priority setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc9Config/IrqAdc9PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc9SR"), "Prio"))"!][!//
[!SELECT "../../IrqAdc9TosConfig"!][!//
[!VAR "Adc9TosParam" = "concat('IrqAdc9SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Adc9TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqAdc9CatConfig"!][!//
[!VAR "Adc9CatParam" = "concat('IrqAdc9SR',$SrnId,'Cat')"!][!//
[!VAR "Adc9CatVal" = "node:value($Adc9CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ADC9'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ADC9_SR', (substring-before(substring-after($NodeName, "IrqAdc9SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Adc9CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ADC9 interrupt Priority Setting for loop */!][!//

/*ADC9 interrupt type of service setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc9Config/IrqAdc9TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC9_SR', (substring-before(substring-after($NodeName, "IrqAdc9SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc9SR"),"Tos"))"!][!//
[!SELECT "../../IrqAdc9PrioConfig"!][!//
[!VAR "Adc9SRPrioParam" = "concat('IrqAdc9SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($Adc9SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC10' )"!][!//
/*ADC10 interrupt Category setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc10Config/IrqAdc10CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC10_SR', (substring-before(substring-after($NodeName, "IrqAdc10SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*ADC10 interrupt Priority setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc10Config/IrqAdc10PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc10SR"), "Prio"))"!][!//
[!SELECT "../../IrqAdc10TosConfig"!][!//
[!VAR "Adc10TosParam" = "concat('IrqAdc10SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($Adc10TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqAdc10CatConfig"!][!//
[!VAR "Adc10CatParam" = "concat('IrqAdc10SR',$SrnId,'Cat')"!][!//
[!VAR "Adc10CatVal" = "node:value($Adc10CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ADC10'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ADC10_SR', (substring-before(substring-after($NodeName, "IrqAdc10SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($Adc10CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ADC10 interrupt Priority Setting for loop */!][!//

/*ADC10 interrupt type of service setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdc10Config/IrqAdc10TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADC10_SR', (substring-before(substring-after($NodeName, "IrqAdc10SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdc10SR"),"Tos"))"!][!//
[!SELECT "../../IrqAdc10PrioConfig"!][!//
[!VAR "Adc10SRPrioParam" = "concat('IrqAdc10SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($Adc10SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADCCG0' )"!][!//
/*ADC CG0 interrupt Category setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdcCG0Config/IrqAdcCG0CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADCCG0_SR', (substring-before(substring-after($NodeName, "IrqAdcCG0SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*ADC CG0 interrupt Priority setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdcCG0Config/IrqAdcCG0PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdcCG0SR"), "Prio"))"!][!//
[!SELECT "../../IrqAdcCG0TosConfig"!][!//
[!VAR "AdcCG0TosParam" = "concat('IrqAdcCG0SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($AdcCG0TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqAdcCG0CatConfig"!][!//
[!VAR "AdcCG0CatParam" = "concat('IrqAdcCG0SR',$SrnId,'Cat')"!][!//
[!VAR "AdcCG0CatVal" = "node:value($AdcCG0CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ADCCG0'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ADCCG0_SR', (substring-before(substring-after($NodeName, "IrqAdcCG0SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($AdcCG0CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ADC CG0 interrupt Priority Setting for loop */!][!//


/*ADC CG0 interrupt type of service setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdcCG0Config/IrqAdcCG0TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADCCG0_SR', (substring-before(substring-after($NodeName, "IrqAdcCG0SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdcCG0SR"),"Tos"))"!][!//
[!SELECT "../../IrqAdcCG0PrioConfig"!][!//
[!VAR "AdcCG0SRParam" = "concat('IrqAdcCG0SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($AdcCG0SRParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//


[!IF "contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADCCG1' )"!][!//
/*ADC CG1 interrupt Category setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdcCG1Config/IrqAdcCG1CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADCCG1_SR', (substring-before(substring-after($NodeName, "IrqAdcCG1SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*ADC CG1 interrupt Priority setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdcCG1Config/IrqAdcCG1PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdcCG1SR"), "Prio"))"!][!//
[!SELECT "../../IrqAdcCG1TosConfig"!][!//
[!VAR "AdcCG1TosParam" = "concat('IrqAdcCG1SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($AdcCG1TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqAdcCG1CatConfig"!][!//
[!VAR "AdcCG1CatParam" = "concat('IrqAdcCG1SR',$SrnId,'Cat')"!][!//
[!VAR "AdcCG1CatVal" = "node:value($AdcCG1CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'ADCCG1'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_ADCCG1_SR', (substring-before(substring-after($NodeName, "IrqAdcCG1SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($AdcCG1CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of ADC CG1 interrupt Priority Setting for loop */!][!//

/*ADC CG1 interrupt type of service setting */
[!LOOP "IrqAdcConfig/*[1]/IrqAdcCG1Config/IrqAdcCG1TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_ADCCG1_SR', (substring-before(substring-after($NodeName, "IrqAdcCG1SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqAdcCG1SR"),"Tos"))"!][!//
[!SELECT "../../IrqAdcCG1PrioConfig"!][!//
[!VAR "AdcCG1SRParam" = "concat('IrqAdcCG1SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($AdcCG1SRParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//
[!ENDIF!][!//


[!VAR "IrqFlexRayExist" = "'STD_OFF'"!][!//
[!IF "num:i(count(IrqFlexRayConfig/*)) = 1"!][!//
/*
                     Container : IrqFLEXRAYinterruptConfiguration
*/
[!VAR "IrqFlexRayExist" = "'STD_ON'"!][!//
[!IF "contains( ecu:get('Irq.FlexRayAvailable'), 'IrqFlexRay0' )"!][!//
/* FLEXRAY0 interrupt Category Setting */
[!LOOP "IrqFlexRayConfig/*[1]/IrqFlexRay0Config/IrqFlexRay0CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!IF "contains($NodeName, 'SR') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY0_SR', (substring-before(substring-after($NodeName, "IrqFlexRay0SR"), "Cat")), '_CAT')"!]         ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!IF "contains($NodeName, 'TimerInt') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY0_TIMER_INT', (substring-before(substring-after($NodeName, "IrqFlexRay0TimerInt"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!IF "contains($NodeName, 'NewData') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY0_NEW_DATA', (substring-before(substring-after($NodeName, "IrqFlexRay0NewData"), "Cat")), '_CAT')"!]   ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!IF "contains($NodeName, 'MBSC') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY0_MBSC', (substring-before(substring-after($NodeName, "IrqFlexRay0MBSC"), "Cat")), '_CAT')"!]       ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!IF "contains($NodeName, 'OBBusy') = 'true'"!][!//
[!CODE!][!//
#define IRQ_FLEXRAY0_OB_BUSY_CAT     ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!IF "contains($NodeName, 'IBBusy') = 'true'"!][!//
[!CODE!][!//
#define IRQ_FLEXRAY0_IB_BUSY_CAT     ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!ENDLOOP!][!//
/* FLEXRAY0 interrupt Priority Setting */
[!LOOP "IrqFlexRayConfig/*[1]/IrqFlexRay0Config/IrqFlexRay0PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "cont" = "'FLEXRAY0'"!][!//
[!IF "contains($NodeName, 'SR') = 'true'"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay0SR"), "Prio"))"!][!//
[!SELECT "../../IrqFlexRay0TosConfig"!][!//
[!VAR "FlexRay0SRTosParam" = "concat('IrqFlexRay0SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($FlexRay0SRTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqFlexRay0CatConfig"!][!//
[!VAR "FlexRay0SRCatParam" = "concat('IrqFlexRay0SR',$SrnId,'Cat')"!][!//
[!VAR "FlexRay0CatVal" = "node:value($FlexRay0SRCatParam)"!][!//
[!ENDSELECT!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_FLEXRAY0_SR', (substring-before(substring-after($NodeName, "IrqFlexRay0SR"), "Prio")), '_PRIO')"!]         [!"num:inttohex($Givenno)"!]
[!ELSEIF "contains($NodeName, 'TimerInt') = 'true'"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay0TimerInt"), "Prio"))"!][!//
[!SELECT "../../IrqFlexRay0TosConfig"!][!//
[!VAR "FlexRay0TimerIntTosParam" = "concat('IrqFlexRay0TimerInt',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($FlexRay0TimerIntTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqFlexRay0CatConfig"!][!//
[!VAR "FlexRay0TimerIntCatParam" = "concat('IrqFlexRay0TimerInt',$SrnId,'Cat')"!][!//
[!VAR "FlexRay0CatVal" = "node:value($FlexRay0TimerIntCatParam)"!][!//
[!ENDSELECT!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_FLEXRAY0_TIMER_INT', (substring-before(substring-after($NodeName, "IrqFlexRay0TimerInt"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!ELSEIF "contains($NodeName, 'NewData') = 'true'"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay0NewData"), "Prio"))"!][!//
[!SELECT "../../IrqFlexRay0TosConfig"!][!//
[!VAR "FlexRay0NewDataTosParam" = "concat('IrqFlexRay0NewData',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($FlexRay0NewDataTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqFlexRay0CatConfig"!][!//
[!VAR "FlexRay0NewDataCatParam" = "concat('IrqFlexRay0NewData',$SrnId,'Cat')"!][!//
[!VAR "FlexRay0CatVal" = "node:value($FlexRay0NewDataCatParam)"!][!//
[!ENDSELECT!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_FLEXRAY0_NEW_DATA', (substring-before(substring-after($NodeName, "IrqFlexRay0NewData"), "Prio")), '_PRIO')"!]   [!"num:inttohex($Givenno)"!]
[!ELSEIF "contains($NodeName, 'MBSC') = 'true'"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay0MBSC"), "Prio"))"!][!//
[!SELECT "../../IrqFlexRay0TosConfig"!][!//
[!VAR "FlexRay0MBSCTosParam" = "concat('IrqFlexRay0MBSC',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($FlexRay0MBSCTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqFlexRay0CatConfig"!][!//
[!VAR "FlexRay0MBSCCatParam" = "concat('IrqFlexRay0MBSC',$SrnId,'Cat')"!][!//
[!VAR "FlexRay0CatVal" = "node:value($FlexRay0MBSCCatParam)"!][!//
[!ENDSELECT!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_FLEXRAY0_MBSC', (substring-before(substring-after($NodeName, "IrqFlexRay0MBSC"), "Prio")), '_PRIO')"!]       [!"num:inttohex($Givenno)"!]
[!ELSEIF "contains($NodeName, 'OBBusy') = 'true'"!][!//
[!VAR "TypeOfService" = "node:value(../../IrqFlexRay0TosConfig/IrqFlexRay0OBBusyTos)"!][!//
[!VAR "FlexRay0CatVal" = "node:value(../../IrqFlexRay0CatConfig/IrqFlexRay0OBBusyCat)"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define IRQ_FLEXRAY0_OB_BUSY_PRIO     [!"num:inttohex($Givenno)"!]
[!ELSEIF "contains($NodeName, 'IBBusy') = 'true'"!][!//
[!VAR "TypeOfService" = "node:value(../../IrqFlexRay0TosConfig/IrqFlexRay0IBBusyTos)"!][!//
[!VAR "FlexRay0CatVal" = "node:value(../../IrqFlexRay0CatConfig/IrqFlexRay0IBBusyCat)"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define IRQ_FLEXRAY0_IB_BUSY_PRIO     [!"num:inttohex($Givenno)"!]
[!ENDIF!][!//
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($FlexRay0CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of FlexRay0 interrupt Priority Setting for loop */!][!//


/* FLEXRAY0 interrupt type of service Setting */

[!LOOP "IrqFlexRayConfig/*[1]/IrqFlexRay0Config/IrqFlexRay0TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!IF "contains($NodeName, 'SR') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY0_SR', (substring-before(substring-after($NodeName, "IrqFlexRay0SR"), "Tos")), '_TOS')"!]         ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay0SR"),"Tos"))"!][!//
[!SELECT "../../IrqFlexRay0PrioConfig"!][!//
[!VAR "FlexRay0SRPrioParam" = "concat('IrqFlexRay0SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($FlexRay0SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "SrnId"="num:i($SrnId)"!][!//
[!ENDNOCODE!][!//
[!ELSEIF "contains($NodeName, 'TimerInt') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY0_TIMER_INT', (substring-before(substring-after($NodeName, "IrqFlexRay0TimerInt"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay0TimerInt"),"Tos"))"!][!//
[!SELECT "../../IrqFlexRay0PrioConfig"!][!//
[!VAR "FlexRay0TimerIntPrioParam" = "concat('IrqFlexRay0TimerInt', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($FlexRay0TimerIntPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "SrnId"="num:i($SrnId)+num:i(2)"!][!//
[!ENDNOCODE!][!//
[!ELSEIF "contains($NodeName, 'NewData') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY0_NEW_DATA', (substring-before(substring-after($NodeName, "IrqFlexRay0NewData"), "Tos")), '_TOS')"!]   ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay0NewData"),"Tos"))"!][!//
[!SELECT "../../IrqFlexRay0PrioConfig"!][!//
[!VAR "FlexRay0NewDataPrioParam" = "concat('IrqFlexRay0NewData', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($FlexRay0NewDataPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "SrnId"="num:i($SrnId)+num:i(4)"!][!//
[!ENDNOCODE!][!//
[!ELSEIF "contains($NodeName, 'MBSC') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY0_MBSC', (substring-before(substring-after($NodeName, "IrqFlexRay0MBSC"), "Tos")), '_TOS')"!]       ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay0MBSC"),"Tos"))"!][!//
[!SELECT "../../IrqFlexRay0PrioConfig"!][!//
[!VAR "FlexRay0MBSCPrioParam" = "concat('IrqFlexRay0MBSC', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($FlexRay0MBSCPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "SrnId"="num:i($SrnId)+num:i(6)"!][!//
[!ENDNOCODE!][!//
[!ELSEIF "contains($NodeName, 'OBBusy') = 'true'"!][!//
[!CODE!][!//
#define IRQ_FLEXRAY0_OB_BUSY_TOS     ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "PrioVal" = "node:value(../../IrqFlexRay0PrioConfig/IrqFlexRay0OBBusyPrio)"!][!//
[!VAR "SrnId" = "num:i(8)"!][!//
[!ENDNOCODE!][!//
[!ELSEIF "contains($NodeName, 'IBBusy') = 'true'"!][!//
[!CODE!][!//
#define IRQ_FLEXRAY0_IB_BUSY_TOS     ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "PrioVal" = "node:value(../../IrqFlexRay0PrioConfig/IrqFlexRay0IBBusyPrio)"!][!//
[!VAR "SrnId" = "num:i(9)"!][!//
[!ENDNOCODE!][!//
[!ENDIF!][!//
[!NOCODE!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
 [!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//



[!IF "contains( ecu:get('Irq.FlexRayAvailable'), 'IrqFlexRay1' )"!][!//
/* FLEXRAY1 interrupt Category Setting */
[!LOOP "IrqFlexRayConfig/*[1]/IrqFlexRay1Config/IrqFlexRay1CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!IF "contains($NodeName, 'SR') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY1_SR', (substring-before(substring-after($NodeName, "IrqFlexRay1SR"), "Cat")), '_CAT')"!]         ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!IF "contains($NodeName, 'TimerInt') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY1_TIMER_INT', (substring-before(substring-after($NodeName, "IrqFlexRay1TimerInt"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!IF "contains($NodeName, 'NewData') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY1_NEW_DATA', (substring-before(substring-after($NodeName, "IrqFlexRay1NewData"), "Cat")), '_CAT')"!]   ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!IF "contains($NodeName, 'MBSC') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY1_MBSC', (substring-before(substring-after($NodeName, "IrqFlexRay1MBSC"), "Cat")), '_CAT')"!]       ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!IF "contains($NodeName, 'OBBusy') = 'true'"!][!//
[!CODE!][!//
#define IRQ_FLEXRAY1_OB_BUSY_CAT     ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!IF "contains($NodeName, 'IBBusy') = 'true'"!][!//
[!CODE!][!//
#define IRQ_FLEXRAY1_IB_BUSY_CAT     ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!ENDLOOP!][!//

/* FLEXRAY1 interrupt Priority Setting */
[!LOOP "IrqFlexRayConfig/*[1]/IrqFlexRay1Config/IrqFlexRay1PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "cont" = "'FLEXRAY1'"!][!//
[!IF "contains($NodeName, 'SR') = 'true'"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay1SR"), "Prio"))"!]
[!SELECT "../../IrqFlexRay1TosConfig"!][!//
[!VAR "FlexRay1SRTosParam" = "concat('IrqFlexRay1SR',$SrnId,'Tos')"!]
[!VAR "TypeOfService" = "node:value($FlexRay1SRTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqFlexRay1CatConfig"!][!//
[!VAR "FlexRay1SRCatParam" = "concat('IrqFlexRay1SR',$SrnId,'Cat')"!]
[!VAR "FlexRay1CatVal" = "node:value($FlexRay1SRCatParam)"!][!//
[!ENDSELECT!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_FLEXRAY1_SR', (substring-before(substring-after($NodeName, "IrqFlexRay1SR"), "Prio")), '_PRIO')"!]         [!"num:inttohex($Givenno)"!]
[!ELSEIF "contains($NodeName, 'TimerInt') = 'true'"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay1TimerInt"), "Prio"))"!]
[!SELECT "../../IrqFlexRay1TosConfig"!][!//
[!VAR "FlexRay1TimerIntTosParam" = "concat('IrqFlexRay1TimerInt',$SrnId,'Tos')"!]
[!VAR "TypeOfService" = "node:value($FlexRay1TimerIntTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqFlexRay1CatConfig"!][!//
[!VAR "FlexRay1TimerIntCatParam" = "concat('IrqFlexRay1TimerInt',$SrnId,'Cat')"!]
[!VAR "FlexRay1CatVal" = "node:value($FlexRay1TimerIntCatParam)"!][!//
[!ENDSELECT!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_FLEXRAY1_TIMER_INT', (substring-before(substring-after($NodeName, "IrqFlexRay1TimerInt"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!ELSEIF "contains($NodeName, 'NewData') = 'true'"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay1NewData"), "Prio"))"!]
[!SELECT "../../IrqFlexRay1TosConfig"!][!//
[!VAR "FlexRay1NewDataTosParam" = "concat('IrqFlexRay1NewData',$SrnId,'Tos')"!]
[!VAR "TypeOfService" = "node:value($FlexRay1NewDataTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqFlexRay1CatConfig"!][!//
[!VAR "FlexRay1NewDataCatParam" = "concat('IrqFlexRay1NewData',$SrnId,'Cat')"!]
[!VAR "FlexRay1CatVal" = "node:value($FlexRay1NewDataCatParam)"!][!//
[!ENDSELECT!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_FLEXRAY1_NEW_DATA', (substring-before(substring-after($NodeName, "IrqFlexRay1NewData"), "Prio")), '_PRIO')"!]   [!"num:inttohex($Givenno)"!]
[!ELSEIF "contains($NodeName, 'MBSC') = 'true'"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay1MBSC"), "Prio"))"!]
[!SELECT "../../IrqFlexRay1TosConfig"!][!//
[!VAR "FlexRay1MBSCTosParam" = "concat('IrqFlexRay1MBSC',$SrnId,'Tos')"!]
[!VAR "TypeOfService" = "node:value($FlexRay1MBSCTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqFlexRay1CatConfig"!][!//
[!VAR "FlexRay1MBSCCatParam" = "concat('IrqFlexRay1MBSC',$SrnId,'Cat')"!]
[!VAR "FlexRay1CatVal" = "node:value($FlexRay1MBSCCatParam)"!][!//
[!ENDSELECT!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_FLEXRAY1_MBSC', (substring-before(substring-after($NodeName, "IrqFlexRay1MBSC"), "Prio")), '_PRIO')"!]       [!"num:inttohex($Givenno)"!]
[!ELSEIF "contains($NodeName, 'OBBusy') = 'true'"!][!//
[!VAR "TypeOfService" = "node:value(../../IrqFlexRay1TosConfig/IrqFlexRay1OBBusyTos)"!][!//
[!VAR "FlexRay1CatVal" = "node:value(../../IrqFlexRay1CatConfig/IrqFlexRay1OBBusyCat)"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define IRQ_FLEXRAY1_OB_BUSY_PRIO     [!"num:inttohex($Givenno)"!]
[!ELSEIF "contains($NodeName, 'IBBusy') = 'true'"!][!//
[!VAR "TypeOfService" = "node:value(../../IrqFlexRay1TosConfig/IrqFlexRay1IBBusyTos)"!][!//
[!VAR "FlexRay1CatVal" = "node:value(../../IrqFlexRay1CatConfig/IrqFlexRay1IBBusyCat)"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define IRQ_FLEXRAY1_IB_BUSY_PRIO     [!"num:inttohex($Givenno)"!]
[!ENDIF!][!//
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($FlexRay1CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of FlexRay1 interrupt Priority Setting for loop */!][!//

/* FLEXRAY1 interrupt type of service Setting */
[!LOOP "IrqFlexRayConfig/*[1]/IrqFlexRay1Config/IrqFlexRay1TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!IF "contains($NodeName, 'SR') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY1_SR', (substring-before(substring-after($NodeName, "IrqFlexRay1SR"), "Tos")), '_TOS')"!]         ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay1SR"),"Tos"))"!]
[!SELECT "../../IrqFlexRay1PrioConfig"!][!//
[!VAR "FlexRay1SRPrioParam" = "concat('IrqFlexRay1SR', $SrnId, 'Prio')"!]
[!VAR "PrioVal" = "num:i(node:value($FlexRay1SRPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "SrnId"="num:i($SrnId)"!]
[!ENDNOCODE!][!//
[!ELSEIF "contains($NodeName, 'TimerInt') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY1_TIMER_INT', (substring-before(substring-after($NodeName, "IrqFlexRay1TimerInt"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay1TimerInt"),"Tos"))"!]
[!SELECT "../../IrqFlexRay1PrioConfig"!][!//
[!VAR "FlexRay1TimerIntPrioParam" = "concat('IrqFlexRay1TimerInt', $SrnId, 'Prio')"!]
[!VAR "PrioVal" = "num:i(node:value($FlexRay1TimerIntPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "SrnId"="num:i($SrnId)+num:i(2)"!]
[!ENDNOCODE!][!//
[!ELSEIF "contains($NodeName, 'NewData') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY1_NEW_DATA', (substring-before(substring-after($NodeName, "IrqFlexRay1NewData"), "Tos")), '_TOS')"!]   ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay1NewData"),"Tos"))"!]
[!SELECT "../../IrqFlexRay1PrioConfig"!][!//
[!VAR "FlexRay1NewDataPrioParam" = "concat('IrqFlexRay1NewData', $SrnId, 'Prio')"!]
[!VAR "PrioVal" = "num:i(node:value($FlexRay1NewDataPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "SrnId"="num:i($SrnId)+num:i(4)"!]
[!ENDNOCODE!][!//
[!ELSEIF "contains($NodeName, 'MBSC') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_FLEXRAY1_MBSC', (substring-before(substring-after($NodeName, "IrqFlexRay0MBSC"), "Tos")), '_TOS')"!]       ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqFlexRay0MBSC"),"Tos"))"!]
[!SELECT "../../IrqFlexRay0PrioConfig"!][!//
[!VAR "FlexRay0MBSCPrioParam" = "concat('IrqFlexRay0MBSC', $SrnId, 'Prio')"!]
[!VAR "PrioVal" = "num:i(node:value($FlexRay0MBSCPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "SrnId"="num:i($SrnId)+num:i(6)"!]
[!ENDNOCODE!][!//
[!ELSEIF "contains($NodeName, 'OBBusy') = 'true'"!][!//
[!CODE!][!//
#define IRQ_FLEXRAY1_OB_BUSY_TOS     ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "PrioVal" = "node:value(../../IrqFlexRay1PrioConfig/IrqFlexRay1OBBusyPrio)"!][!//
[!VAR "SrnId" = "num:i(8)"!][!//
[!ENDNOCODE!][!//
[!ELSEIF "contains($NodeName, 'IBBusy') = 'true'"!][!//
[!CODE!][!//
#define IRQ_FLEXRAY1_IB_BUSY_TOS     ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "PrioVal" = "node:value(../../IrqFlexRay1PrioConfig/IrqFlexRay1IBBusyPrio)"!][!//
[!VAR "SrnId" = "num:i(9)"!][!//
[!ENDNOCODE!][!//
[!ENDIF!][!//
[!NOCODE!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
 [!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//
[!ENDIF!][!//



/*
                     Container : PMU0 interrupt Configuration
*/
/* PMU0 interrupt Category Setting*/

[!LOOP "IrqPMU0Config/*[1]/IrqPMU0CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_PMU0_SR', (substring-before(substring-after($NodeName, "IrqPMU0SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/* PMU0 interrupt Category Setting*/
[!LOOP "IrqPMU0Config/*[1]/IrqPMU0PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqPMU0SR"), "Prio"))"!][!//
[!SELECT "../../../*[1]/IrqPMU0TosConfig"!][!//
[!VAR "IrqPMU0TosParam" = "concat('IrqPMU0SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($IrqPMU0TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../../*[1]/IrqPMU0CatConfig"!][!//
[!VAR "IrqPMU0CatParam" = "concat('IrqPMU0SR',$SrnId,'Cat')"!][!//
[!VAR "IrqPMU0CatVal" = "node:value($IrqPMU0CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'PMU0'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_PMU0_SR', (substring-before(substring-after($NodeName, "IrqPMU0SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($IrqPMU0CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of PMU0 interrupt Priority Setting for loop */!][!//


/* PMU0 interrupt type of service Setting*/
[!LOOP "IrqPMU0Config/*[1]/IrqPMU0TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_PMU0_SR', (substring-before(substring-after($NodeName, "IrqPMU0SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqPMU0SR"), "Tos"))"!]
[!SELECT "../../../*[1]/IrqPMU0PrioConfig"!][!//
[!VAR "PMU0SRParam" = "concat('IrqPMU0SR', $SrnId, 'Prio')"!]
[!VAR "PrioVal" = "num:i(node:value($PMU0SRParam))"!][!//
[!ENDSELECT!]
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//

[!NOCODE!][!//
[!VAR "IrqScuExist" = "'STD_OFF'"!][!//
[!ENDNOCODE!][!//
[!IF "num:i(count(IrqScuConfig/*)) = 1"!][!//
/*
                     Container : Scu interrupt Configuration
*/
[!NOCODE!][!//
[!VAR "IrqScuExist" = "'STD_ON'"!][!//	
[!ENDNOCODE!][!//
/* Scu interrupt Category Setting*/
[!LOOP "IrqScuConfig/*[1]/IrqScuCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!IF "contains($NodeName, 'Dts') = 'true'"!][!//
[!CODE!][!//
#define IRQ_SCU_DTS_BUSY_SR_CAT  ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!IF "contains($NodeName, 'Eru') = 'true'"!][!//
[!CODE!][!//
#define [!" concat ('IRQ_SCU_ERU_SR', (substring-before(substring-after($NodeName, "IrqScuEruSR"), "Cat")), '_CAT')"!]      ([!"concat('IRQ_', node:value(.))"!])
[!ENDCODE!][!//
[!ENDIF!][!//
[!ENDLOOP!][!//



/* Scu interrupt Category Setting*/
[!LOOP "IrqScuConfig/*[1]/IrqScuPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "cont" = "'SCU'"!][!//
[!IF "contains($NodeName, 'Dts') = 'true'"!][!//
[!VAR "TypeOfService" = "node:value(../../../*[1]/IrqScuTosConfig/IrqScuDtsSRTos)"!][!//
[!VAR "ScuCatVal" = "node:value(../../../*[1]/IrqScuCatConfig/IrqScuDtsSRCat)"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define IRQ_SCU_DTS_BUSY_SR_PRIO  [!"num:inttohex($Givenno)"!]
[!ELSE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqScuEruSR"), "Prio"))"!][!//
[!SELECT "../../../*[1]/IrqScuTosConfig"!][!//
[!VAR "ScuTosParam" = "concat('IrqScuEruSR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($ScuTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../../*[1]/IrqScuCatConfig"!][!//
[!VAR "ScuCatParam" = "concat('IrqScuEruSR',$SrnId,'Cat')"!][!//
[!VAR "ScuCatVal" = "node:value($ScuCatParam)"!][!//
[!ENDSELECT!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//`
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_SCU_ERU_SR', (substring-before(substring-after($NodeName, "IrqScuEruSR"), "Prio")), '_PRIO')"!]      [!"num:inttohex($Givenno)"!]
[!ENDIF!][!//
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($ScuCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of SCU interrupt Priority Setting for loop */!][!//



/* Scu interrupt type of service Setting*/
[!LOOP "IrqScuConfig/*[1]/IrqScuTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!IF "contains($NodeName, 'Dts') = 'true'"!][!//
[!CODE!][!//
#define IRQ_SCU_DTS_BUSY_SR_TOS  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqScu"),"SRTos"))"!][!//
[!VAR "PrioVal" = "num:i(node:value(../../../*[1]/IrqScuPrioConfig/IrqScuDtsSRPrio))"!][!//
[!ELSE!][!//
[!CODE!][!//
#define [!" concat ('IRQ_SCU_ERU_SR', (substring-before(substring-after($NodeName, "IrqScuEruSR"), "Tos")), '_TOS')"!]      ([!"concat('IRQ_TOS_', node:value(.))"!])
[!ENDCODE!][!//
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqScuEruSR"),"Tos"))"!][!//
[!SELECT "../../../*[1]/IrqScuPrioConfig"!][!//
[!VAR "ScuEruParam" = "concat('IrqScuEruSR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($ScuEruParam))"!][!//
[!ENDSELECT!][!//
[!ENDNOCODE!][!//
[!ENDIF!][!//
[!NOCODE!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
 [!IF "$SrnId = 'Dts'"!][!//
 [!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i(0)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
 [!ENDASSERT!][!//
 [!ELSE!][!//
 [!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)+num:i(1)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
 [!ENDASSERT!][!//
 [!ENDIF!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//



[!VAR "IrqGPSRGroupExist" = "'STD_OFF'"!][!//
[!IF "num:i(count(IrqGPSRGroupConfig/*)) = 1"!][!//
/*
                     Container : IrqGPSRGroupinterruptConfiguration
*/
[!VAR "IrqGPSRGroupExist" = "'STD_ON'"!][!//
[!IF "contains( ecu:get('Irq.GpsrAvailable'), 'IrqGpsr0' )"!][!//
/* GPSRGROUP0 interrupt Category Setting */ 
[!LOOP "IrqGPSRGroupConfig/*[1]/IrqGPSRGroup0Config/IrqGPSRGroup0CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_GPSRGROUP0_SR', (substring-before(substring-after($NodeName, "IrqGPSRGroup0SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

   
/* GPSRGROUP0 interrupt Priority Setting */  
[!LOOP "IrqGPSRGroupConfig/*[1]/IrqGPSRGroup0Config/IrqGPSRGroup0PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGPSRGroup0SR"), "Prio"))"!][!//
[!SELECT "../../IrqGPSRGroup0TosConfig"!][!//
[!VAR "GPSRGroup0TosParam" = "concat('IrqGPSRGroup0SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($GPSRGroup0TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqGPSRGroup0CatConfig"!][!//
[!VAR "GPSRGroup0CatParam" = "concat('IrqGPSRGroup0SR',$SrnId,'Cat')"!][!//
[!VAR "GPSRGroup0CatVal" = "node:value($GPSRGroup0CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'GPSRGROUP0'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_GPSRGROUP0_SR', (substring-before(substring-after($NodeName, "IrqGPSRGroup0SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($GPSRGroup0CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of GPSRGROUP0 interrupt Priority Setting for loop */!][!//

/* GPSRGROUP0 interrupt type of service Setting */ 
[!LOOP "IrqGPSRGroupConfig/*[1]/IrqGPSRGroup0Config/IrqGPSRGroup0TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_GPSRGROUP0_SR', (substring-before(substring-after($NodeName, "IrqGPSRGroup0SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGPSRGroup0SR"), "Tos"))"!][!//
[!SELECT "../../IrqGPSRGroup0PrioConfig"!][!//
[!VAR "GPSRGroup0SRParam" = "concat('IrqGPSRGroup0SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($GPSRGroup0SRParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.GpsrAvailable'), 'IrqGpsr1' )"!][!//
/* GPSRGROUP1 interrupt Category Setting */   
[!LOOP "IrqGPSRGroupConfig/*[1]/IrqGPSRGroup1Config/IrqGPSRGroup1CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_GPSRGROUP1_SR', (substring-before(substring-after($NodeName, "IrqGPSRGroup1SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/* GPSRGROUP1 interrupt Priority Setting */  
[!LOOP "IrqGPSRGroupConfig/*[1]/IrqGPSRGroup1Config/IrqGPSRGroup1PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGPSRGroup1SR"), "Prio"))"!][!//
[!SELECT "../../IrqGPSRGroup1TosConfig"!][!//
[!VAR "GPSRGroup1TosParam" = "concat('IrqGPSRGroup1SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($GPSRGroup1TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqGPSRGroup1CatConfig"!][!//
[!VAR "GPSRGroup1CatParam" = "concat('IrqGPSRGroup1SR',$SrnId,'Cat')"!][!//
[!VAR "GPSRGroup1CatVal" = "node:value($GPSRGroup1CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'GPSRGROUP1'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_GPSRGROUP1_SR', (substring-before(substring-after($NodeName, "IrqGPSRGroup1SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($GPSRGroup1CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of GPSRGROUP1 interrupt Priority Setting for loop */!][!//

/* GPSRGROUP1 interrupt type of service Setting */ 
[!LOOP "IrqGPSRGroupConfig/*[1]/IrqGPSRGroup1Config/IrqGPSRGroup1TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_GPSRGROUP1_SR', (substring-before(substring-after($NodeName, "IrqGPSRGroup1SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGPSRGroup1SR"), "Tos"))"!][!//
[!SELECT "../../IrqGPSRGroup1PrioConfig"!][!//
[!VAR "GPSRGroup1SRParam" = "concat('IrqGPSRGroup1SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($GPSRGroup1SRParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.GpsrAvailable'), 'IrqGpsr2' )"!][!//
/* GPSRGROUP2 interrupt Category Setting */ 
[!LOOP "IrqGPSRGroupConfig/*[1]/IrqGPSRGroup2Config/IrqGPSRGroup2CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_GPSRGROUP2_SR', (substring-before(substring-after($NodeName, "IrqGPSRGroup2SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//
	
/* GPSRGROUP2 interrupt Priority Setting */  
[!LOOP "IrqGPSRGroupConfig/*[1]/IrqGPSRGroup2Config/IrqGPSRGroup2PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGPSRGroup2SR"), "Prio"))"!][!//
[!SELECT "../../IrqGPSRGroup2TosConfig"!][!//
[!VAR "GPSRGroup2TosParam" = "concat('IrqGPSRGroup2SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($GPSRGroup2TosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqGPSRGroup2CatConfig"!][!//
[!VAR "GPSRGroup2CatParam" = "concat('IrqGPSRGroup2SR',$SrnId,'Cat')"!][!//
[!VAR "GPSRGroup2CatVal" = "node:value($GPSRGroup2CatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'GPSRGROUP2'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_GPSRGROUP2_SR', (substring-before(substring-after($NodeName, "IrqGPSRGroup2SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($GPSRGroup2CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of GPSRGROUP2 interrupt Priority Setting for loop */!][!//

/* GPSRGROUP2 interrupt type of service Setting */ 
[!LOOP "IrqGPSRGroupConfig/*[1]/IrqGPSRGroup2Config/IrqGPSRGroup2TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_GPSRGROUP2_SR', (substring-before(substring-after($NodeName, "IrqGPSRGroup2SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGPSRGroup2SR"), "Tos"))"!][!//
[!SELECT "../../IrqGPSRGroup2PrioConfig"!][!//
[!VAR "GPSRGroup2SRParam" = "concat('IrqGPSRGroup2SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($GPSRGroup2SRParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//
[!ENDIF!][!//
[!//

[!VAR "IrqGtmExist" = "'STD_OFF'"!][!//
[!IF "num:i(count(IrqGtmConfig/*)) = 1"!][!//
/*
                     Container : Irq GTM interruptConfiguration
*/
[!VAR "IrqGtmExist" = "'STD_ON'"!][!//
[!IF "contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmAei' )"!][!//
/* GTM AEI interrupt Category Setting */ 
 
[!LOOP "IrqGtmConfig/*[1]/IrqGtmAEIConfig/IrqGtmAEICatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define IRQ_GTM_AEI_CAT   ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//
	 
/* GTM AEI interrupt Priority Setting */  
[!LOOP "IrqGtmConfig/*[1]/IrqGtmAEIConfig/IrqGtmAEIPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!SELECT "../../IrqGtmAEITosConfig"!][!//
[!VAR "TypeOfService" = "node:value(IrqGtmAEITos)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqGtmAEICatConfig"!][!//
[!VAR "GtmAEICatVal" = "node:value(IrqGtmAEICat)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'GTMAEI'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define IRQ_GTM_AEI_PRIO       [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($GtmAEICatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of GTM AEI interrupt Priority Setting for loop */!][!//

/* GTM AEI interrupt type of service Setting */ 
[!LOOP "IrqGtmConfig/*[1]/IrqGtmAEIConfig/IrqGtmAEITosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define IRQ_GTM_AEI_TOS  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!SELECT "../../IrqGtmAEIPrioConfig"!][!//
[!VAR "PrioVal" = "num:i(node:value(IrqGtmAEIPrio))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i(0)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmErr' )"!][!//
/* GTM ERR interrupt Category Setting */ 
  
[!LOOP "IrqGtmConfig/*[1]/IrqGtmERRConfig/IrqGtmERRCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define IRQ_GTM_ERR_SR_CAT  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/* GTM ERR interrupt Priority Setting */  

[!LOOP "IrqGtmConfig/*[1]/IrqGtmERRConfig/IrqGtmERRPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!SELECT "../../IrqGtmERRTosConfig"!][!//
[!VAR "TypeOfService" = "node:value(IrqGtmERRSRTos)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqGtmERRCatConfig"!][!//
[!VAR "GtmERRCatVal" = "node:value(IrqGtmERRSRCat)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'GTMERR'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define IRQ_GTM_ERR_SR_PRIO    [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($GtmERRCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of GTM ERR interrupt Priority Setting for loop */!][!//

/* GTM ERR interrupt type of service Setting */ 

[!LOOP "IrqGtmConfig/*[1]/IrqGtmERRConfig/IrqGtmERRTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define IRQ_GTM_ERR_SR_TOS  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!SELECT "../../IrqGtmERRPrioConfig"!][!//
[!VAR "PrioVal" = "num:i(node:value(IrqGtmERRSRPrio))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i(0)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmTim0' )"!][!//
/* GTM TIM0 interrupt Category Setting */ 
[!LOOP "IrqGtmConfig/*[1]/IrqGtmTIM0Config/IrqGtmTIM0CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_GTM_TIM0_SR', (substring-before(substring-after($NodeName, "IrqGtmTIM0SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//
	 
/* GTM TIM0 interrupt Priority Setting */  
[!LOOP "IrqGtmConfig/*[1]/IrqGtmTIM0Config/IrqGtmTIM0PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGtmTIM0SR"), "Prio"))"!][!//
[!SELECT "../../IrqGtmTIM0TosConfig"!][!//
[!VAR "GtmTIM0SRParam" = "concat('IrqGtmTIM0SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($GtmTIM0SRParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqGtmTIM0CatConfig"!][!//
[!VAR "GtmTIM0Param" = "concat('IrqGtmTIM0SR',$SrnId,'Cat')"!][!//
[!VAR "GtmTIM0CatVal" = "node:value($GtmTIM0Param)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'GTMTIM0'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_GTM_TIM0_SR', (substring-before(substring-after($NodeName, "IrqGtmTIM0SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($GtmTIM0CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of GTM TIM0 interrupt Priority Setting for loop */!][!//

/* GTM TIM0 interrupt type of service Setting */ 
[!LOOP "IrqGtmConfig/*[1]/IrqGtmTIM0Config/IrqGtmTIM0TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_GTM_TIM0_SR', (substring-before(substring-after($NodeName, "IrqGtmTIM0SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGtmTIM0SR"), "Tos"))"!][!//
[!SELECT "../../../IrqGtmTIM0PrioConfig"!][!//
[!VAR "GtmTIM0PrioParam" = "concat('IrqGtmTIM0SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($GtmTIM0PrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmTom0' )"!][!//
/* GTM TOM0 interrupt Category Setting */ 
[!LOOP "IrqGtmConfig/*[1]/IrqGtmTOM0Config/IrqGtmTOM0CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_GTM_TOM0_SR', (substring-before(substring-after($NodeName, "IrqGtmTOM0SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//
						
/* GTM TOM0 interrupt Priority Setting */  

[!LOOP "IrqGtmConfig/*[1]/IrqGtmTOM0Config/IrqGtmTOM0PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGtmTOM0SR"), "Prio"))"!][!//
[!SELECT "../../IrqGtmTOM0TosConfig"!][!//
[!VAR "GtmTOM0SRParam" = "concat('IrqGtmTOM0SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($GtmTOM0SRParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqGtmTOM0CatConfig"!][!//
[!VAR "GtmTOM0Param" = "concat('IrqGtmTOM0SR',$SrnId,'Cat')"!][!//
[!VAR "GtmTOM0CatVal" = "node:value($GtmTOM0Param)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'GTMTOM0'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_GTM_TOM0_SR', (substring-before(substring-after($NodeName, "IrqGtmTOM0SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($GtmTOM0CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of GTM TOM0 interrupt Priority Setting for loop */!][!//

/* GTM TOM0 interrupt type of service Setting */ 
[!LOOP "IrqGtmConfig/*[1]/IrqGtmTOM0Config/IrqGtmTOM0TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_GTM_TOM0_SR', (substring-before(substring-after($NodeName, "IrqGtmTOM0SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGtmTOM0SR"), "Tos"))"!][!//
[!SELECT "../../../IrqGtmTOM0PrioConfig"!][!//
[!VAR "GtmTOM0PrioParam" = "concat('IrqGtmTOM0SR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($GtmTOM0PrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmTom1' )"!][!//
/* GTM TOM1 interrupt Category Setting */  
[!LOOP "IrqGtmConfig/*[1]/IrqGtmTOM1Config/IrqGtmTOM1CatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_GTM_TOM1_SR', (substring-before(substring-after($NodeName, "IrqGtmTOM1SR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

	
/* GTM TOM1 interrupt Priority Setting */  
[!LOOP "IrqGtmConfig/*[1]/IrqGtmTOM1Config/IrqGtmTOM1PrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGtmTOM1SR"), "Prio"))"!][!//
[!SELECT "../../IrqGtmTOM1TosConfig"!][!//
[!VAR "GtmTOM1SRParam" = "concat('IrqGtmTOM1SR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($GtmTOM1SRParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqGtmTOM1CatConfig"!][!//
[!VAR "GtmTOM1Param" = "concat('IrqGtmTOM1SR',$SrnId,'Cat')"!][!//
[!VAR "GtmTOM1CatVal" = "node:value($GtmTOM1Param)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'GTMTOM1'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_GTM_TOM1_SR', (substring-before(substring-after($NodeName, "IrqGtmTOM1SR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($GtmTOM1CatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!/* End of GTM TOM1 interrupt Priority Setting for loop */!][!//

/* GTM TOM1 interrupt type of service Setting */ 
[!LOOP "IrqGtmConfig/*[1]/IrqGtmTOM1Config/IrqGtmTOM1TosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_GTM_TOM1_SR', (substring-before(substring-after($NodeName, "IrqGtmTOM1SR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqGtmTOM1SR"), "Tos"))"!][!//
[!SELECT "../../../IrqGtmTOM1PrioConfig"!][!//
[!VAR "GtmTOM1PrioParam" = "concat('IrqSentSR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($GtmTOM1PrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//
[!ENDIF!][!//

[!IF "num:i(count(IrqSentConfig/*)) = 1"!][!//
/*
                     Container : IrqSentInterruptConfiguration
*/
/* SENT Interrupt Category setting */
[!LOOP "IrqSentConfig/*[1]/IrqSentCatConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_SENT_SR', (substring-before(substring-after($NodeName, "IrqSentSR"), "Cat")), '_CAT')"!]  ([!"concat('IRQ_', node:value(.))"!])
[!ENDLOOP!][!//

/*  SENT Interrupt Priority setting  */
[!LOOP "IrqSentConfig/*[1]/IrqSentPrioConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
[!VAR "Givenno" = "num:i(.)"!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqSentSR"), "Prio"))"!][!//
[!SELECT "../../IrqSentTosConfig"!][!//
[!VAR "SentSRTosParam" = "concat('IrqSentSR',$SrnId,'Tos')"!][!//
[!VAR "TypeOfService" = "node:value($SentSRTosParam)"!][!//
[!ENDSELECT!][!//
[!SELECT "../../IrqSentCatConfig"!][!//
[!VAR "SentCatParam" = "concat('IrqSentSR',$SrnId,'Cat')"!][!//
[!VAR "SentCatVal" = "node:value($SentCatParam)"!][!//
[!ENDSELECT!][!//
[!VAR "cont" = "'SENT'"!][!//
[!NOCODE!][!//
[!IF "$TypeOfService != 'DMA'"!][!//
[!CALL "CG_PriorityCheck", "Givenno" = "$Givenno","cont" = "$cont"!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
#define [!" concat ('IRQ_SENT_SR', (substring-before(substring-after($NodeName, "IrqSentSR"), "Prio")), '_PRIO')"!]  [!"num:inttohex($Givenno)"!]
[!NOCODE!][!//
[!/* Following lines of code is used to test if Priority of Category 2 Interrupts 
     is Less than the lowest priority of Catagory 1 Interrupts  */!][!//
[!/* Check only if IRQ_OSEK_ENABLE is ON */!][!//
[!IF "(as:modconf('Irq')[1]/IrqGeneral/IrqOsekEnable = 'true')"!][!//
[!/* Assign current priority to variable "PRIORITY" */!][!//
[!VAR "PRIORITY" = "num:i($Givenno)"!][!//    
 [!/* Assign current category to variable "CATEGORY" */!][!//
 [!VAR "CATEGORY" = "($SentCatVal)"!][!//
   [!/* Check if Priority value 1 and 2 are not used if IRQ_OSEK_ENABLE is ON */!][!//
   [!IF "((num:i($PRIORITY) = num:i(1))or (num:i($PRIORITY)= num:i(2)))"!][!//
     [!WARNING "Priority 1 and 2 of CPU Interrupts are used by OSEK for Task Switching!"!][!//
   [!ENDIF!][!//
         [!/* If TypeOfService is DMA then relax the checking of CAT1 and CAT23 */!][!//   
   [!IF "$TypeOfService != 'DMA'"!][!//   
[!/* Check if current category is "CAT1" */!][!//
   [!IF "($CATEGORY) = 'CAT1'"!][!// 
    [!/* Check if priority is less than MIN_CAT1_LEVEL */!][!// 
    [!IF "((num:i($PRIORITY) < num:i($MIN_CAT1_LEVEL))and(num:i($PRIORITY)!= 0))"!][!//
      [!/* Reassign MIN_CAT1_LEVEL with current priority  */!][!// 
      [!VAR "MIN_CAT1_LEVEL" = "$PRIORITY"!][!//
    [!ENDIF!][!//
   [!/* Check if current category is "CAT23" */!][!//
   [!ELSEIF "($CATEGORY) ='CAT23'"!][!//
    [!/* Check if priority is greater than MAX_CAT2_LEVEL reassign MAX_CAT2_LEVEL with current priority  */!][!//
    [!IF "((num:i($PRIORITY) > num:i($MAX_CAT2_LEVEL))and(num:i($PRIORITY)!= num:i(0)))"!][!//
      [!/* Reassign MAX_CAT2_LEVEL with current priority  */!][!//
      [!VAR "MAX_CAT2_LEVEL" = "$PRIORITY "!][!//
    [!ENDIF!][!//
   [!ENDIF!][!//    
   [!ASSERT "num:i($MIN_CAT1_LEVEL)> num:i($MAX_CAT2_LEVEL)"!][!//
     Priority of Category 2 Interrupts should be Less than lowest priority of Catagory 1 Interrupts!
   [!ENDASSERT!][!//
    [!ENDIF!][!/* End of "TypeOfService is DMA then relax the checking of CAT1 and CAT23" */!][!// 
[!ENDIF!][!/* End of "Check only if IRQ_OSEK_ENABLE is ON " */!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//

/* SENT Interrupt type of service setting */
[!LOOP "IrqSentConfig/*[1]/IrqSentTosConfig/*"!][!//
[!VAR "NodeName" = "node:name(.)"!][!//
#define [!" concat ('IRQ_SENT_SR', (substring-before(substring-after($NodeName, "IrqSentSR"), "Tos")), '_TOS')"!]  ([!"concat('IRQ_TOS_', node:value(.))"!])
[!NOCODE!][!//
[!VAR "SrnId" = "(substring-before(substring-after($NodeName, "IrqSentSR"), "Tos"))"!][!//
[!SELECT "../../IrqSentPrioConfig"!][!//
[!VAR "SentPrioParam" = "concat('IrqSentSR', $SrnId, 'Prio')"!][!//
[!VAR "PrioVal" = "num:i(node:value($SentPrioParam))"!][!//
[!ENDSELECT!][!//
[!VAR "TypeOfService" = "."!][!//
[!IF "$TypeOfService = 'DMA'"!][!//
[!ASSERT "num:i($PrioVal) <= num:i($DmaMaxPrio)"!][!//
	Type of service is selected as DMA for [!"$cont"!]_SR[!"num:i($SrnId)"!] and the priority is configured as [!"num:i($PrioVal)"!]. Priority should not be more than [!"num:i($DmaMaxPrio)"!]!
[!ENDASSERT!][!//
[!ENDIF!][!//
[!ENDNOCODE!][!//
[!ENDLOOP!][!//
[!ENDIF!][!//

/* Global macros that determines whether a module / SRN is in use or not
*/
[!VAR "LinAdded" = "'false'"!][!//
[!SELECT "as:modconf('Lin')[1]"!][!//
[!VAR "LinAdded" = "'true'"!][!//
[!ENDSELECT!][!//
[!VAR "UartAdded" = "'false'"!][!//
[!SELECT "as:modconf('Uart')[1]"!][!//
[!VAR "UartAdded" = "'true'"!][!//
[!ENDSELECT!][!//
[!VAR "StdLinAdded" = "'false'"!][!//
[!SELECT "as:modconf('StdLin')[1]"!][!//
[!VAR "StdLinAdded" = "'true'"!][!//
[!ENDSELECT!][!//
[!//
[!VAR "ModAdded" = "'false'"!][!//
[!IF "($LinAdded = 'true') or ($UartAdded = 'true') or ($StdLinAdded = 'true')"!][!//
[!VAR "ModAdded" = "'true'"!][!//
[!ENDIF!][!//
[!//
[!IF "($ModAdded = 'true')"!][!//
#define IRQ_ASCLIN_EXIST            (STD_ON)
[!ELSE!][!//
#define IRQ_ASCLIN_EXIST            (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.AscLinSrnAvailable'), 'IrqAscLin0' )) and ($ModAdded = 'true')"!][!//
#define IRQ_ASCLIN0_EXIST           (STD_ON)
[!ELSE!][!//
#define IRQ_ASCLIN0_EXIST           (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.AscLinSrnAvailable'), 'IrqAscLin1' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ASCLIN1_EXIST           (STD_ON)
[!ELSE!][!//
#define IRQ_ASCLIN1_EXIST           (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.AscLinSrnAvailable'), 'IrqAscLin2' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ASCLIN2_EXIST           (STD_ON)
[!ELSE!][!//
#define IRQ_ASCLIN2_EXIST           (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.AscLinSrnAvailable'), 'IrqAscLin3' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ASCLIN3_EXIST           (STD_ON)
[!ELSE!][!//
#define IRQ_ASCLIN3_EXIST           (STD_OFF)
[!ENDIF!][!//

[!VAR "ModAdded" = "'false'"!][!//
[!SELECT "as:modconf('Spi')[1]"!][!//
[!VAR "ModAdded" = "'true'"!][!//
[!ENDSELECT!][!//
[!//
[!IF "($ModAdded = 'true')"!][!//
#define IRQ_QSPI_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_QSPI_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.QspiSrnAvailable'), 'IrqQspi0' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_QSPI0_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_QSPI0_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.QspiSrnAvailable'), 'IrqQspi1' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_QSPI1_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_QSPI1_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.QspiSrnAvailable'), 'IrqQspi2' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_QSPI2_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_QSPI2_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.QspiSrnAvailable'), 'IrqQspi3' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_QSPI3_EXIST             (STD_ON)
[!ELSE!][!//													 
#define IRQ_QSPI3_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.QspiSrnAvailable'), 'IrqQspi4' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_QSPI4_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_QSPI4_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.QspiSrnAvailable'), 'IrqQspi5' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_QSPI5_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_QSPI5_EXIST             (STD_OFF)
[!ENDIF!][!//



[!VAR "ModAdded" = "'false'"!][!//
[!SELECT "as:modconf('Icu')[1]"!][!//
[!VAR "ModAdded" = "'true'"!][!//
[!ENDSELECT!][!//
[!//
[!IF "($ModAdded = 'true')"!][!//
#define IRQ_CCU6_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_CCU6_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CCU6Available'), 'IrqCCU60' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CCU60_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CCU60_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CCU6Available'), 'IrqCCU61' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CCU61_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CCU61_EXIST             (STD_OFF)
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.ModAvailable'), 'IrqGpt' )"!][!//
#define IRQ_GPT_EXIST               (STD_ON)
[!ELSE!][!//
#define IRQ_GPT_EXIST               (STD_OFF)
[!ENDIF!][!//
[!IF "contains( ecu:get('Irq.GPT12Available'), 'IrqGPT120' )"!][!//
#define IRQ_GPT120_EXIST            (STD_ON)
[!ELSE!][!//
#define IRQ_GPT120_EXIST            (STD_OFF)
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.ModAvailable'), 'IrqStm' )"!][!//
#define IRQ_STM_EXIST               (STD_ON)
[!ELSE!][!//
#define IRQ_STM_EXIST               (STD_OFF)
[!ENDIF!][!//
[!IF "contains( ecu:get('Irq.STMAvailable'), 'IrqSTM0' )"!][!//
#define IRQ_STM0_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_STM0_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "contains( ecu:get('Irq.STMAvailable'), 'IrqSTM1' )"!][!//
#define IRQ_STM1_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_STM1_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "contains( ecu:get('Irq.STMAvailable'), 'IrqSTM2' )"!][!//
#define IRQ_STM2_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_STM2_EXIST              (STD_OFF)
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.ModAvailable'), 'IrqDma' )"!][!//
#define IRQ_DMA_EXIST               (STD_ON)
[!ELSE!][!//
#define IRQ_DMA_EXIST               (STD_OFF)
[!ENDIF!][!//
[!IF "contains( ecu:get('Irq.NoOfDmaChannels'), 'IrqDmaCh0To15' )"!][!//
#define IRQ_DMA_CH0TO15_EXIST       (STD_ON)
[!ELSE!][!//
#define IRQ_DMA_CH0TO15_EXIST       (STD_OFF)
[!ENDIF!][!//

[!VAR "ModAdded" = "'false'"!][!//
[!SELECT "as:modconf('Eth')[1]"!][!//
[!VAR "ModAdded" = "'true'"!][!//
[!ENDSELECT!][!//
[!//
[!IF "($ModAdded = 'true')"!][!//
#define IRQ_ETH_EXIST               (STD_ON)
[!ELSE!][!//
#define IRQ_ETH_EXIST               (STD_OFF)
[!ENDIF!][!//

[!VAR "ModAdded" = "'false'"!][!//
[!SELECT "as:modconf('Can')[1]"!][!//
[!VAR "ModAdded" = "'true'"!][!//
[!ENDSELECT!][!//
[!//
[!IF "($ModAdded = 'true')"!][!//
#define IRQ_CAN_EXIST               (STD_ON)
[!ELSE!][!//
#define IRQ_CAN_EXIST               (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn0' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN0_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_CAN0_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn1' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN1_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_CAN1_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn2' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN2_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_CAN2_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn3' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN3_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_CAN3_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn4' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN4_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_CAN4_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn5' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN5_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_CAN5_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn6' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN6_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_CAN6_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn7' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN7_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_CAN7_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn8' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN8_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_CAN8_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn9' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN9_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_CAN9_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn10' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN10_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CAN10_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn11' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN11_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CAN11_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn12' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN12_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CAN12_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn13' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN13_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CAN13_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn14' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN14_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CAN14_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn15' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN15_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CAN15_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn16' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN16_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CAN16_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn17' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN17_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CAN17_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn18' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN18_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CAN18_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn19' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN19_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CAN19_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn20' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN20_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CAN20_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn21' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN21_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CAN21_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn22' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN22_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CAN22_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.CanSrnAvailable'), 'IrqCanSrn23' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_CAN23_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_CAN23_EXIST             (STD_OFF)
[!ENDIF!][!//


[!IF "(contains( ecu:get('Irq.HsmSrnAvailable'), 'IrqHsmSrn0' ))"!][!//
#define IRQ_HSM0_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_HSM0_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.HsmSrnAvailable'), 'IrqHsmSrn1' ))"!][!//
#define IRQ_HSM1_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_HSM1_EXIST              (STD_OFF)
[!ENDIF!][!//

[!VAR "ModAdded" = "'false'"!][!//
[!SELECT "as:modconf('Adc')[1]"!][!//
[!VAR "ModAdded" = "'true'"!][!//
[!ENDSELECT!][!//
[!//
[!IF "($ModAdded = 'true')"!][!//
#define IRQ_ADC_EXIST               (STD_ON)
[!ELSE!][!//
#define IRQ_ADC_EXIST               (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC0' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ADC0_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_ADC0_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC1' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ADC1_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_ADC1_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC2' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ADC2_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_ADC2_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC3' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ADC3_EXIST              (STD_ON)
[!ELSE!][!//													 
#define IRQ_ADC3_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC4' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ADC4_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_ADC4_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC5' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ADC5_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_ADC5_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC6' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ADC6_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_ADC6_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC7' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ADC7_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_ADC7_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC8' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ADC8_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_ADC8_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC9' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ADC9_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_ADC9_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADC10' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ADC10_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_ADC10_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADCCG0' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ADCCG0_EXIST            (STD_ON)
[!ELSE!][!//
#define IRQ_ADCCG0_EXIST            (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.VAdcAvailable'), 'IrqVADCCG0' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_ADCCG1_EXIST            (STD_ON)
[!ELSE!][!//
#define IRQ_ADCCG1_EXIST            (STD_OFF)
[!ENDIF!][!//



[!VAR "ModAdded" = "'false'"!][!//
[!SELECT "as:modconf('Fr_AURIX')[1]"!][!//
[!VAR "ModAdded" = "'true'"!][!//
[!ENDSELECT!][!//
[!//
[!IF "($ModAdded = 'true')"!][!//
#define IRQ_FLEXRAY_EXIST           (STD_ON)
[!ELSE!][!//
#define IRQ_FLEXRAY_EXIST           (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.FlexRayAvailable'), 'IrqFlexRay0' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_FLEXRAY0_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_FLEXRAY0_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.FlexRayAvailable'), 'IrqFlexRay1' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_FLEXRAY1_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_FLEXRAY1_EXIST          (STD_OFF)
[!ENDIF!][!//

[!VAR "ModAdded" = "'false'"!][!//
[!SELECT "as:modconf('Fls')[1]"!][!//
[!VAR "ModAdded" = "'true'"!][!//
[!ENDSELECT!][!//
[!//
[!IF "($ModAdded = 'true')"!][!//
#define IRQ_PMU0_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_PMU0_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.PmuSrnAvailable'), 'IrqPmu0Sr0' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_PMU0_SR0_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_PMU0_SR0_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.PmuSrnAvailable'), 'IrqPmu0Sr1' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_PMU0_SR1_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_PMU0_SR1_EXIST          (STD_OFF)
[!ENDIF!][!//

[!VAR "ModAdded" = "'false'"!][!//
[!SELECT "as:modconf('Icu')[1]"!][!//
[!VAR "ModAdded" = "'true'"!][!//
[!ENDSELECT!][!//
[!//
[!IF "($ModAdded = 'true')"!][!//
#define IRQ_SCU_EXIST               (STD_ON)
[!ELSE!][!//
#define IRQ_SCU_EXIST               (STD_OFF)
[!ENDIF!][!//

[!IF "contains( ecu:get('Irq.ModAvailable'), 'IrqGpsr' )"!][!//
#define IRQ_GPSRGROUP_EXIST         (STD_ON)
[!ELSE!][!//
#define IRQ_GPSRGROUP_EXIST         (STD_OFF)
[!ENDIF!][!//
[!IF "contains( ecu:get('Irq.GpsrAvailable'), 'IrqGpsr0' )"!][!//
#define IRQ_GPSRGROUP0_EXIST        (STD_ON)
[!ELSE!][!//
#define IRQ_GPSRGROUP0_EXIST        (STD_OFF)
[!ENDIF!][!//
[!IF "contains( ecu:get('Irq.GpsrAvailable'), 'IrqGpsr1' )"!][!//
#define IRQ_GPSRGROUP1_EXIST        (STD_ON)
[!ELSE!][!//
#define IRQ_GPSRGROUP1_EXIST        (STD_OFF)
[!ENDIF!][!//
[!IF "contains( ecu:get('Irq.GpsrAvailable'), 'IrqGpsr2' )"!][!//
#define IRQ_GPSRGROUP2_EXIST        (STD_ON)
[!ELSE!][!//
#define IRQ_GPSRGROUP2_EXIST        (STD_OFF)
[!ENDIF!][!//

[!VAR "ModAdded" = "'false'"!][!//
[!NOCODE!]
[!SELECT "as:modconf('Mcu')[1]"!]
[!VAR "McuSelect" = "'McuModuleConfiguration'"!]
[!FOR "ModuleIndex" = "0" TO "(num:i(count(node:ref($McuSelect)/*))- 1)"!]
[!VAR "McuSelectIndex" = "concat('McuModuleConfiguration/*[',$ModuleIndex + 1,']')"!]
[!VAR "Count" = "num:i(count(node:ref($McuSelectIndex)/GtmConfiguration/*))"!]
[!IF "$Count > num:i(0)"!]		
[!VAR "ModAdded" = "'true'"!][!//
[!ENDIF!][!//
[!ENDFOR!]
[!ENDSELECT!][!//
[!ENDNOCODE!]
[!//
[!IF "($ModAdded = 'true')"!][!//
#define IRQ_GTM_EXIST               (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_EXIST               (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmAei' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_AEI_EXIST           (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_AEI_EXIST           (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmAru' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_ARU_EXIST           (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_ARU_EXIST           (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmBrc' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_BRC_EXIST           (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_BRC_EXIST           (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmCmp' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_CMP_EXIST           (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_CMP_EXIST           (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmSpe' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_SPE_EXIST           (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_SPE_EXIST           (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmPsm0' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_PSM0_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_PSM0_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmPsm1' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_PSM1_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_PSM1_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmDpll' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_DPLL_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_DPLL_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmErr' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_ERR_EXIST           (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_ERR_EXIST           (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmTim0' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_TIM0_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_TIM0_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmTim1' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_TIM1_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_TIM1_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmTim2' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_TIM2_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_TIM2_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmTim3' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_TIM3_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_TIM3_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmTim4' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_TIM4_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_TIM4_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmTim5' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_TIM5_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_TIM5_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmMcs0' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_MCS0_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_MCS0_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmMcs1' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_MCS1_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_MCS1_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmMcs2' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_MCS2_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_MCS2_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmMcs3' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_MCS3_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_MCS3_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmMcs4' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_MCS4_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_MCS4_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmMcs5' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_MCS5_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_MCS5_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmTom0' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_TOM0_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_TOM0_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmTom1' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_TOM1_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_TOM1_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmTom2' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_TOM2_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_TOM2_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmTom3' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_TOM3_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_TOM3_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmTom4' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_TOM4_EXIST          (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_TOM4_EXIST          (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmAtom0' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_ATOM0_EXIST         (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_ATOM0_EXIST         (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmAtom1' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_ATOM1_EXIST         (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_ATOM1_EXIST         (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmAtom2' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_ATOM2_EXIST         (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_ATOM2_EXIST         (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmAtom3' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_ATOM3_EXIST         (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_ATOM3_EXIST         (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmAtom4' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_ATOM4_EXIST         (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_ATOM4_EXIST         (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmAtom5' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_ATOM5_EXIST         (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_ATOM5_EXIST         (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmAtom6' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_ATOM6_EXIST         (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_ATOM6_EXIST         (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmAtom7' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_ATOM7_EXIST         (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_ATOM7_EXIST         (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.GTMAvailable'), 'IrqGtmAtom8' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_GTM_ATOM8_EXIST         (STD_ON)
[!ELSE!][!//
#define IRQ_GTM_ATOM8_EXIST         (STD_OFF)
[!ENDIF!][!//

[!VAR "ModAdded" = "'false'"!][!//
[!SELECT "as:modconf('Sent')[1]"!][!//
[!VAR "ModAdded" = "'true'"!][!//
[!ENDSELECT!][!//
[!//
[!IF "($ModAdded = 'true')"!][!//
#define IRQ_SENT_EXIST              (STD_ON)
[!ELSE!][!//
#define IRQ_SENT_EXIST              (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn0' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT0_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_SENT0_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn1' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT1_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_SENT1_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn2' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT2_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_SENT2_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn3' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT3_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_SENT3_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn4' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT4_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_SENT4_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn5' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT5_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_SENT5_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn6' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT6_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_SENT6_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn7' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT7_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_SENT7_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn8' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT8_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_SENT8_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn9' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT9_EXIST             (STD_ON)
[!ELSE!][!//
#define IRQ_SENT9_EXIST             (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn10' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT10_EXIST            (STD_ON)
[!ELSE!][!//
#define IRQ_SENT10_EXIST            (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn11' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT11_EXIST            (STD_ON)
[!ELSE!][!//
#define IRQ_SENT11_EXIST            (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn12' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT12_EXIST            (STD_ON)
[!ELSE!][!//
#define IRQ_SENT12_EXIST            (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn13' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT13_EXIST            (STD_ON)
[!ELSE!][!//
#define IRQ_SENT13_EXIST            (STD_OFF)
[!ENDIF!][!//
[!IF "(contains( ecu:get('Irq.SentSrnAvailable'), 'IrqSentSrn14' ))  and ($ModAdded = 'true')"!][!//
#define IRQ_SENT14_EXIST            (STD_ON)
[!ELSE!][!//
#define IRQ_SENT14_EXIST            (STD_OFF)
[!ENDIF!][!//

/*******************************************************************************
**                      Global Type Definitions                               **
*******************************************************************************/
/*******************************************************************************
**                      Global Constant Declarations                          **
*******************************************************************************/
/*******************************************************************************
**                      Global Variable Declarations                          **
*******************************************************************************/
/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/
/*******************************************************************************
**                      Global Inline Function Definitions                    **
*******************************************************************************/
[!ENDSELECT!][!//

/* IRQ_CFG_H */
#endif
