<?xml version='1.0'?>
<AUTOSAR xmlns="http://autosar.org/schema/r4.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://autosar.org/schema/r4.0 AUTOSAR_4-0-3.xsd">
  <AR-PACKAGES>
    <AR-PACKAGE UUID="13d9a111-9d4c-47b6-98cb-fe5aec24c8b0">
      <SHORT-NAME>AURIX</SHORT-NAME>
      <ELEMENTS>
        <ECUC-MODULE-DEF UUID="03eaa8c5-137e-402e-823d-8d7776c1de4d">
          <SHORT-NAME>SafeWdgQspi</SHORT-NAME>
          <SUPPORTED-CONFIG-VARIANTS>
            <SUPPORTED-CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</SUPPORTED-CONFIG-VARIANT>
            <SUPPORTED-CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</SUPPORTED-CONFIG-VARIANT>
          </SUPPORTED-CONFIG-VARIANTS>
          <CONTAINERS>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ccda81dc-42cf-4171-858b-7c947884702f">
              <SHORT-NAME>SafeWdgQspiConfigSet</SHORT-NAME>
              <DESC>
                <L-2 L="FOR-ALL">This is the base container that contains the post-build selectable configuration parameters for Qspi modules.</L-2>
              </DESC>
              <PARAMETERS>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce928e-6ed1-5968-93b2-34d30b1aa49f">
                  <SHORT-NAME>SafeWdgQspiModule</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter represents the QSPI module to which TLF is assigned.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>3</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce9285-6ed1-5978-93b2-34d30b1aa49f">
                  <SHORT-NAME>SafeWdgQspiRxInpSelection</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter selects one of the seven options available for MRST line for the QSPI communication,
for the selected QSPI module. 0 implies MRSTa is selected and 7 implies MRSTh.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>7</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-BOOLEAN-PARAM-DEF UUID="ccda81dc-42cf-4171-858b-7c947884704g">
                  <SHORT-NAME>SafeWdgQspiDmaUsed</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Specifies whether DMA shall be used for FIFO read/write.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>true</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce928e-6edb-5978-93b2-34d30b1aa5bf">
                  <SHORT-NAME>SafeWdgQspiDmaTxChannel</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Specifies the DMA channel to be used for data tramsfer between the drivers Tx buffer and Qspi TxFIFO.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>14</DEFAULT-VALUE>
                  <MAX>15</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce928e-6edb-59ab-93b2-34d30b1aa5bf">
                  <SHORT-NAME>SafeWdgQspiDmaRxChannel</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Specifies the DMA channel to be used for data tramsfer between Qspi RxFIFO and drivers Rx buffer.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>15</DEFAULT-VALUE>
                  <MAX>15</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce828e-6eda-5978-93b3-44d30b1aa49f">
                  <SHORT-NAME>SafeWdgQspiModuleTQLen</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter sets global time quantum length common n-divider scaling of the baud rates of the QSPI module.
0 implies, QSPI clock will be divided by 1, 1 implies division by 2 etc...</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>255</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-FUNCTION-NAME-DEF UUID="999ccf11-8b4f-4719-abde-2f2b65e86142">
                  <SHORT-NAME>SafeWdgQspiCustCallback</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">This parameter represents the function, which has to be called from QSPI receive interrup service routine (SafeWdgQspi_IsrRx() ) to notify the application software upon data transfer, initiated by customer specific API SafeWdgQspi_CustTxRx ,is completed by SafeWdgQspi driver.Application software can call SafeWdgQspi_RxDone API to know the status of data transfer.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <ECUC-FUNCTION-NAME-DEF-VARIANTS>
                    <ECUC-FUNCTION-NAME-DEF-CONDITIONAL>
                      <DEFAULT-VALUE>NULL_PTR</DEFAULT-VALUE>
                    </ECUC-FUNCTION-NAME-DEF-CONDITIONAL>
                  </ECUC-FUNCTION-NAME-DEF-VARIANTS>
                </ECUC-FUNCTION-NAME-DEF>
              </PARAMETERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ccda81dc-42cf-4171-858b-7c947884703h">
              <SHORT-NAME>SafeWdgQspiChannelConfigSet</SHORT-NAME>
              <DESC>
                <L-2 L="FOR-ALL">This is the base container that contains the post-build selectable configuration parameters for individual                    channels.</L-2>
              </DESC>
              <MULTIPLE-CONFIGURATION-CONTAINER>true</MULTIPLE-CONFIGURATION-CONTAINER>
              <PARAMETERS>
                <ECUC-INTEGER-PARAM-DEF UUID="983e928e-6ed1-5968-93b2-34d30b1aa49f">
                  <SHORT-NAME>SafeWdgQspiChannel</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter represents the QSPI, slave select signal (SLSO) to which TLF is connected.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>8</DEFAULT-VALUE>
                  <MAX>15</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98c4928e-6ed1-5978-93b2-34d30b1aa49f">
                  <SHORT-NAME>SafeWdgQspiSlaveSlctActvLvl</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter decides the active output level of slave select signal for the channel used by TLF.
0 implies low and 1 implies high active level.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>1</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-597d-93b3-44d30b1aa49f">
                  <SHORT-NAME>SafeWdgQspiChannelTQLen</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter sets time quantum length used by SafeWdgQspiECONzBitSeg1, SafeWdgQspiECONzBitSeg2 and
SafeWdgQspiECONzBitSeg3 to get desired baud rate.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>63</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5978-93b3-44d30b1aa49a">
                  <SHORT-NAME>SafeWdgQspiECONzBitSeg1</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter is expressed as Tq time units (SafeWdgQspiChannelTQLen) and sets the value of ECONz A  in ECON
register.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>2</DEFAULT-VALUE>
                  <MAX>3</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5973-93b3-44d30b1aa49f">
                  <SHORT-NAME>SafeWdgQspiECONzBitSeg2</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter is expressed as Tq time units (SafeWdgQspiChannelTQLen) and sets the value of ECONz B  in ECON
register</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>2</DEFAULT-VALUE>
                  <MAX>3</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5978-93bf-44d30b1aa49f">
                  <SHORT-NAME>SafeWdgQspiECONzBitSeg3</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter is expressed as Tq time units (SafeWdgQspiChannelTQLen) and sets the value of ECONz C  in ECON
register.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>1</DEFAULT-VALUE>
                  <MAX>3</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5978-93b3-44d30b1aa4af">
                  <SHORT-NAME>SafeWdgQspiClkPhase</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter enables, delay of one half SCLK clock cycle at the start of data transfer..</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>1</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5978-93b3-45d30b1aa49f">
                  <SHORT-NAME>SafeWdgQspiClkPolarity</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter set the Idle level of the clock signal at the SCLK pin.0 implies Idle level is set to low and 1 implies high</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>1</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5978-93b3-4fd30b1aa49f">
                  <SHORT-NAME>SafeWdgQspiParityEnable</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter sets both, parity generation in transmit and parity check in receive direction.
0 implies parity is disabled and 1 implies parity is enabled</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>1</DEFAULT-VALUE>
                  <MAX>1</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5978-93b3-44d30b1ba49f">
                  <SHORT-NAME>SafeWdgQspiParityType</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter sets the type of parity. 0 Implies even parity and 1 implies odd parity is set.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>1</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-fed1-5918-93b3-44d30b1aa49f">
                  <SHORT-NAME>SafeWdgQspiMSBFirst</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter decides, if MSB or LSB of the byte has to be shifted first.0 implies LSB will be
shifted first and 1 implies MSB will be shifted first.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>1</DEFAULT-VALUE>
                  <MAX>1</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-6778-93b3-44d3011aa49f">
                  <SHORT-NAME>SafeWdgQspiDataLen</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter defines the data length in bits for one data block. 1 implies a data block contains 2 bits and 31 imply 32 bits.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>14</DEFAULT-VALUE>
                  <MAX>31</MAX>
                  <MIN>1</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5978-93b3-44d30b1af49f">
                  <SHORT-NAME>SafeWdgQspiIdlePrescaler</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter sets the prescaler for the idle delay. Prescaler will be set to 1 if entered as 1,
4 if entered as 1 and to 16 if entered as 2 etc..</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>3</DEFAULT-VALUE>
                  <MAX>7</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5978-93b3-44d30b1ca49f">
                  <SHORT-NAME>SafeWdgQspiIdleTime</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter defines the length of both idle delays, IDLEA and IDLEB,pre scaled with SafeWdgQspiIdlePrescaler.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>7</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5978-93b3-44d30b1ad49f">
                  <SHORT-NAME>SafeWdgQspiLeadPrescaler</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter sets the prescaler for the leading delay. Prescaler will be set to 1 if entered as 1, 
4 if entered as 1 and to 16 if entered as 2 etc?</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>2</DEFAULT-VALUE>
                  <MAX>7</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5978-93b3-44d30b1ae49f">
                  <SHORT-NAME>SafeWdgQspiLeadTime</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter defines leading delay, pre scaled with SafeWdgQspiLeadPrescaler.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>3</DEFAULT-VALUE>
                  <MAX>7</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5978-93b3-f4d30b1aa49f">
                  <SHORT-NAME>SafeWdgQspiTrailPrescaler</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter sets the prescaler for the trailing delay. Prescaler will be
set to 1 if entered as 1, 4 if entered as 1 and to 16 if entered as 2 etc?</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>2</DEFAULT-VALUE>
                  <MAX>7</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5978-93b3-e4d30b1aa49f">
                  <SHORT-NAME>SafeWdgQspiTrailTime</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter defines the trailing delay, pre scaled with SafeWdgQspiTrailPrescaler.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>7</DEFAULT-VALUE>
                  <MAX>7</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
              </PARAMETERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
          </CONTAINERS>
        </ECUC-MODULE-DEF>
      </ELEMENTS>
    </AR-PACKAGE>
  </AR-PACKAGES>
</AUTOSAR>
