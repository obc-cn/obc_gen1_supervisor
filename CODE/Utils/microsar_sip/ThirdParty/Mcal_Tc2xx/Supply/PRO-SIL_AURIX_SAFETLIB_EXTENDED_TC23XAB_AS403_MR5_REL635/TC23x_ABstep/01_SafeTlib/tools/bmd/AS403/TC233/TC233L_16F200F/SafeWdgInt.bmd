<?xml version='1.0'?>
<AUTOSAR xmlns="http://autosar.org/schema/r4.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://autosar.org/schema/r4.0 AUTOSAR_4-0-3.xsd">
  <AR-PACKAGES>
    <AR-PACKAGE UUID="93d9a111-9d4c-47b6-98cb-fe5aec24c8b0">
      <SHORT-NAME>AURIX</SHORT-NAME>
      <ELEMENTS>
        <ECUC-MODULE-DEF UUID="03eaa8c5-137e-402e-823d-8d7776c1de4d">
          <SHORT-NAME>SafeWdgInt</SHORT-NAME>
          <SUPPORTED-CONFIG-VARIANTS>
            <SUPPORTED-CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</SUPPORTED-CONFIG-VARIANT>
            <SUPPORTED-CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</SUPPORTED-CONFIG-VARIANT>
          </SUPPORTED-CONFIG-VARIANTS>
          <CONTAINERS>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ccda81dc-46cf-4171-858b-7c947884702f">
              <SHORT-NAME>SafeWdgIntConfigSet</SHORT-NAME>
              <DESC>
                <L-2 L="FOR-ALL">This is the base container that contains the post-build selectable configuration parameters</L-2>
              </DESC>
              <MULTIPLE-CONFIGURATION-CONTAINER>true</MULTIPLE-CONFIGURATION-CONTAINER>
              <PARAMETERS>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce928e-6ed1-4968-93b2-21d90b1fa49e">
                  <SHORT-NAME>SafeWdgIntFreq</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter represents the frequency selection option for the internal safety watchdog.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>2</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce928e-6ed1-4968-93b2-21d90b1fa49f">
                  <SHORT-NAME>SafeWdgIntTimerReload</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This represents the reload value for the internal safety watchdog that needs to be setup after every successful servicing of the safety watchdog.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>1000</DEFAULT-VALUE>
                  <MAX>65535</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
              </PARAMETERS>
              <SUB-CONTAINERS>
                <ECUC-PARAM-CONF-CONTAINER-DEF UUID="97e6fd09-f3da-4944-b2e1-edc4dc70e5a3">
                  <SHORT-NAME>SafeWdgIntSigTable</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This is the base container that contains the expected test signature values parameters from SafeWdgIntExptdTstSig0 to SafeWdgIntExptdTstSig15</L-2>
                  </DESC>
                  <PARAMETERS>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d30b1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig0</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d31b1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig1</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d32b1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig2</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d33b1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig3</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d34b1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig4</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d35b1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig5</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d36b1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig6</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d37b1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig7</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d38b1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig8</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d39b1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig9</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d3ab1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig10</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d3bb1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig11</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d3cb1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig12</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d3db1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig13</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d3eb1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig14</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d3fb1fa49e">
                      <SHORT-NAME>SafeWdgIntExptdTstSig15</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This represents the expected consolidated signature based on the execution of the test(s). The incoming consolidated signature received from the upper layer is compared against this value and only incase of a match, the correct base value is used for obtaining the password for servicing the watchdog.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>4294839324</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                  </PARAMETERS>
                </ECUC-PARAM-CONF-CONTAINER-DEF>
              </SUB-CONTAINERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
          </CONTAINERS>
        </ECUC-MODULE-DEF>
      </ELEMENTS>
    </AR-PACKAGE>
  </AR-PACKAGES>
</AUTOSAR>
