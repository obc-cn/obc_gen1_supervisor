#/******************************************************************************
#** Copyright (C) Infineon Technologies (2013)                                **
#**                                                                           **
#** All rights reserved.                                                      **
#**                                                                           **
#** This document contains proprietary information belonging to Infineon      **
#** Technologies. Passing on and copying of this document, and communication  **
#** of its contents is not permitted without prior written authorization.     **
#**                                                                           **
#*******************************************************************************
#**  FILENAME  : Compiler_Options_gnu.mak                                     **
#**                                                                           **
#**  VERSION   : 1.0.0                                                        **
#**                                                                           **
#**  DATE      : 2013-10-25                                                   **
#**                                                                           **
#**  VARIANT   : VariantPB                                                    **
#**                                                                           **
#**  PLATFORM  : Infineon AURIX                                               **
#**                                                                           **
#**  COMPILER  : Tasking                                                      **
#**                                                                           **
#**  AUTHOR    : SafeTlib Team                                                **
#**                                                                           **
#**  VENDOR    : Infineon Technologies                                        **
#**                                                                           **
#**  TRACEABILITY:                                                            **
#**                                                                           **
#**  DESCRIPTION  : This file compiler make options                           **
#**                                                                           **
#**  SPECIFICATION(S) :                                                       **
#**                                                                           **
#**  MAY BE CHANGED BY USER [Yes/No]: No                                      **
#**                                                                           **
#******************************************************************************/
#------------------------------------------------------------
# Following variables are used only for system test 
#------------------------------------------------------------
ifeq ($(CODE_COVERAGE),1)
include $(CTC_BASE_PATH)/mak/infineon_ctc_defs.mak
CODE_COVERAGE_DEFINED=-D_CODE_COVERAGE_
endif

ifeq ($(UVP_TEST_AUTO),1)
UVP_TEST_AUTO_DEFINED=-DUVP_TEST_AUTO
endif

ifeq ($(RA_SMU_SET_ALARM),1)
RA_SMU_SET_ALARM_DEFINED=-D_RA_SMU_SET_ALARM_
endif
#------------------------------------------------------------

### SMU Directory
SMU_DIR = $(SAFETLIBCD_BASE_DIR)/SMU

### Test Handler Directory
TEST_HANDLER_DIR = $(SAFETLIBCD_BASE_DIR)/TestHandler

### Micro Test Lib Directory
MTL_DIR = $(SAFETLIBCD_BASE_DIR)/MicroTestLib

### Safe Watchdog Interface Directory
SAFEWDG_IF_DIR = $(SAFEWDG_BASE_DIR)/SafeWdgIf

### Internal Safe Watchdog Directory
SAFEWDG_INT_DIR = $(SAFEWDG_BASE_DIR)/SafeWdgInt

### External CiC Safe Watchdog Directory
SAFEWDG_EXTCIC_DIR = $(SAFEWDG_BASE_DIR)/SafeWdgExtCic

### External TLF Safe Watchdog Directory
SAFEWDG_EXTTLF_DIR = $(SAFEWDG_BASE_DIR)/SafeWdgExtTlf

### External Alive Watchdog Directory
SAFEWDG_EXTALIVE_DIR = $(SAFEWDG_BASE_DIR)/SafeWdgExtAlive

### ASCLIN SPI driver Directory
SAFEWDG_ASCLIN_DIR = $(SAFEWDG_BASE_DIR)/SafeWdgAscLin

### QSPI driver Directory
SAFEWDG_QSPI_DIR = $(SAFEWDG_BASE_DIR)/SafeWdgQspi

MCAL_Common_Headers =
IRQ_CFG_INCLUDE= 
ifeq ($(SAFETLIB_STANDALONE), FALSE)
MCAL_Common_Headers = -I"$(BASE_PATH)/../Aurix_MC-ISAR/integration_general/ssc/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/tricore_general/ssc/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/ecum_infineon_tricore/ssc/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/ecum_infineon_tricore/cfg1/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/spi_infineon_tricore/ssc/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/spi_infineon_tricore/cfg1/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/adc_infineon_tricore/ssc/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/adc_infineon_tricore/cfg1/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/lin_17_asclin_infineon_tricore/ssc/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/lin_17_asclin_infineon_tricore/cfg1/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/integration_general/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/uart_infineon_tricore/ssc/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/uart_infineon_tricore/cfg1/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/demoapp/tricore/compiler"
IRQ_CFG_INCLUDE=-I"$(BASE_PATH)/../Aurix_MC-ISAR/irq_infineon_tricore/cfg1/inc"
endif
ifeq ($(SAFETLIB_STANDALONE), TRUE)
IRQ_CFG_INCLUDE=-I$(APPL_BASE_DIR)/cfg
endif

###################################
ifeq ($(STL_TARGET),27)
Common_Headers = -I"$(COMMON)/" -I"$(COMMON)/TC$(STL_TARGET)xC/" -I"$(CTC_BASE_PATH)/inc" -I"$(CONFIG_DIR)/inc" $(MCAL_Common_Headers)
else
Common_Headers = -I"$(COMMON)/" -I"$(COMMON)/TC$(STL_TARGET)x/" -I"$(CTC_BASE_PATH)/inc" -I"$(CONFIG_DIR)/inc" $(MCAL_Common_Headers)
endif
SafeTlibCD_Headers = -I"$(SMU_DIR)/inc/" -I"$(TEST_HANDLER_DIR)/inc" -I"$(MTL_DIR)/inc/"  

SafeWdgCD_Headers = -I"$(SAFEWDG_IF_DIR)/inc/" -I"$(SAFEWDG_INT_DIR)/inc/" -I"$(SAFEWDG_EXTCIC_DIR)/inc/" -I"$(SAFEWDG_EXTTLF_DIR)/inc/" -I"$(SAFEWDG_EXTALIVE_DIR)/inc/" -I"$(SAFEWDG_QSPI_DIR)/inc/" -I"$(SAFEWDG_ASCLIN_DIR)/inc/" $(IRQ_CFG_INCLUDE) 

UpperLayer_Headers = -I"$(SWDGM_BASE_DIR)/inc/" -I"$(TSTM_BASE_DIR)/inc/"

Application_Headers = -I"$(APPL_BASE_DIR)/inc/" -I"$(APPL_BASE_DIR)/gnu_wks/RefApp/"  -I"$(BASE_PATH)/../../TestApp/inc/" -I"$(BASE_PATH)../inc/" $(IRQ_CFG_INCLUDE)

###################################

STL_CD_HEADERS = $(Common_Headers) $(SafeTlibCD_Headers) $(Application_Headers)

SWDG_CD_HEADERS = $(Common_Headers) $(SafeWdgCD_Headers) $(Application_Headers)

APPL_HEADERS = $(Common_Headers) $(SafeWdgCD_Headers) $(Application_Headers) $(UpperLayer_Headers) $(SafeTlibCD_Headers)

### Compiler options

ifeq ($(C_STANDARD),C90)
CSTANDARD_FLAG =-std=iso9899:1990
endif

ifeq ($(C_STANDARD),C99)
CSTANDARD_FLAG =-std=iso9899:1999
endif

MANDATORY_COMPILER_OPTIONS = $(CSTANDARD_FLAG) -ansi -ffreestanding -fno-short-enums -fpeel-loops -falign-functions=4 -funsigned-bitfields -ffunction-sections -fno-ivopts -fno-peephole2 -O3 -mtc161 -D_GNU_C_TRICORE_=1 -c -I "$(COMPILER_PATH)/tricore/include/machine"

OPTIONAL_COMPILER_OPTIONS = -save-temps=obj -Wundef -Wp,$(CSTANDARD_FLAG) -frecord-gcc-switches -fsection-anchors -g3 -W -fno-asm -nostartfiles -Wall -Wuninitialized 

STD_GNU_COMPILER_OPTIONS = $(MANDATORY_COMPILER_OPTIONS) $(OPTIONAL_COMPILER_OPTIONS)

###################################
STD_OPT_STL = $(STL_CD_HEADERS)  $(UVP_TEST_AUTO_DEFINED) $(STD_GNU_COMPILER_OPTIONS)

STD_OPT_STL_2 = $(STD_OPT_STL)

STD_OPT_SWDG = $(SWDG_CD_HEADERS) $(STD_GNU_COMPILER_OPTIONS)

ifeq ($(STL_TARGET),27)
STD_OPT_SWDG_TLF = $(SWDG_CD_HEADERS) -save-temps=obj $(CSTANDARD_FLAG) -ansi -fno-asm -ffreestanding -Wp,$(CSTANDARD_FLAG) -fno-short-enums -fpeel-loops -falign-functions=4 -frecord-gcc-switches -fsection-anchors -funsigned-bitfields -ffunction-sections -nostartfiles -O1 -g3 -Wall -w -mcpu=tc27xx -D_GNU_C_TRICORE_=1 -DSTL_TARGET=$(STL_TARGET)u -I "$(COMPILER_PATH)/tricore/include/machine" -c
endif

ifeq ($(STL_TARGET),26)
STD_OPT_SWDG_TLF = $(SWDG_CD_HEADERS) -save-temps=obj $(CSTANDARD_FLAG) -ansi -fno-asm -ffreestanding -Wp,$(CSTANDARD_FLAG) -fno-short-enums -fpeel-loops -falign-functions=4 -frecord-gcc-switches -fsection-anchors -funsigned-bitfields -ffunction-sections -nostartfiles -O0 -g3 -Wall -w -mcpu=tc27xx -D_GNU_C_TRICORE_=1 -DSTL_TARGET=$(STL_TARGET)u -I "$(COMPILER_PATH)/tricore/include/machine" -c
endif

ifeq ($(STL_TARGET),22)
STD_OPT_SWDG_TLF = $(SWDG_CD_HEADERS) -save-temps=obj $(CSTANDARD_FLAG) -ansi -fno-asm -ffreestanding -Wp,$(CSTANDARD_FLAG) -fno-short-enums -fpeel-loops -falign-functions=4 -frecord-gcc-switches -fsection-anchors -funsigned-bitfields -ffunction-sections -nostartfiles -Os -g3 -Wall -w -mcpu=tc27xx -D_GNU_C_TRICORE_=1 -DSTL_TARGET=$(STL_TARGET)u -I "$(COMPILER_PATH)/tricore/include/machine" -c
endif

ifeq ($(STL_TARGET),21)
STD_OPT_SWDG_TLF = $(SWDG_CD_HEADERS) -save-temps=obj $(CSTANDARD_FLAG) -ansi -fno-asm -ffreestanding -Wp,$(CSTANDARD_FLAG) -fno-short-enums -fpeel-loops -falign-functions=4 -frecord-gcc-switches -fsection-anchors -funsigned-bitfields -ffunction-sections -nostartfiles -O0 -g3 -Wall -w -mcpu=tc27xx -D_GNU_C_TRICORE_=1 -DSTL_TARGET=$(STL_TARGET)u -I "$(COMPILER_PATH)/tricore/include/machine" -c
endif

ifeq ($(STL_TARGET),23)
STD_OPT_SWDG_TLF = $(SWDG_CD_HEADERS) -save-temps=obj $(CSTANDARD_FLAG) -ansi -fno-asm -ffreestanding -Wp,$(CSTANDARD_FLAG) -fno-short-enums -fpeel-loops -falign-functions=4 -frecord-gcc-switches -fsection-anchors -funsigned-bitfields -ffunction-sections -nostartfiles -O0 -g3 -Wall -w -mcpu=tc27xx -D_GNU_C_TRICORE_=1 -DSTL_TARGET=$(STL_TARGET)u -I "$(COMPILER_PATH)/tricore/include/machine" -c
endif

ifeq ($(STL_TARGET),29)
STD_OPT_SWDG_TLF = $(SWDG_CD_HEADERS) -save-temps=obj $(CSTANDARD_FLAG) -ansi -fno-asm -ffreestanding -Wp,$(CSTANDARD_FLAG) -fno-short-enums -fpeel-loops -falign-functions=4 -frecord-gcc-switches -fsection-anchors -funsigned-bitfields -ffunction-sections -nostartfiles -Os -g3 -Wall -w -mcpu=tc27xx -D_GNU_C_TRICORE_=1 -DSTL_TARGET=$(STL_TARGET)u -I "$(COMPILER_PATH)/tricore/include/machine" -c
endif

STD_OPT_APPL = $(APPL_HEADERS) $(STD_GNU_COMPILER_OPTIONS) $(CODE_COVERAGE_DEFINED) $(RA_SMU_SET_ALARM_DEFINED)

### Below compiler options for Test scripts
STD_OPT_CTC_NOP = $(UVP_TEST_AUTO_DEFINED) -save-temps=obj $(CSTANDARD_FLAG) -ansi -fno-asm -ffreestanding -Wp,$(CSTANDARD_FLAG) -I"$(CTC_BASE_PATH)/inc" -fno-short-enums -fpeel-loops -falign-functions=4 -frecord-gcc-switches -fsection-anchors -funsigned-bitfields -ffunction-sections -nostartfiles -O0 -g3 -Wall -w -Wuninitialized  -mcpu=tc27xx -D_GNU_C_TRICORE_=1 -DSTL_TARGET=$(STL_TARGET)u -I "$(COMPILER_PATH)/tricore/include/machine" -c


###################################
CCTC = "$(COMPILER_PATH)\bin\tricore-gcc.exe"
CCTC_CTC=$(CCTC)
LINKER = "$(COMPILER_PATH)\bin\tricore-gcc.exe"
HEX_GENERATE_COMMAND = "$(COMPILER_PATH)\bin\tricore-objcopy.exe"
###################################
ifeq ($(CODE_COVERAGE),1)
CODE_COV_CMD = $(CODE_COV_PATH)/ctc.exe
CODE_COV_OPT_GENERAL = -i m -k
CODE_COV_OPT_INI = -c $(BASE_PATH)..\..\ctc\ctc_aurix_$(COMPILER).ini
CODE_COV_OPT_SYM = -n $(BASE_PATH)..\obj\$(MODULE_NAME)_ctc.sym
CCTC_CTC = "$(CODE_COV_CMD)" $(CODE_COV_OPT_GENERAL) $(CODE_COV_OPT_INI) $(CODE_COV_OPT_SYM) "tricore-gcc"
endif

MANDATORY_LINKER_OPTIONS = -Wl,--oformat=elf32-tricore -Wl,--mem-holes -nostartfiles -mcpu=tc27xx

OPTIONAL_LINKER_OPTIONS = -o "$(APPL_OUTDIR)/$(APPL_TARGET).elf" -T"$(APPL_BASE_DIR)/src/_mcal_pjt.ld" -Wl,-Map="$(APPL_OUTDIR)/$(APPL_TARGET).map" -Wl,--cref -Wl,--extmap="a" -Wl,--allow-multiple-definition

OPT_LKLC = $(MANDATORY_LINKER_OPTIONS) $(OPTIONAL_LINKER_OPTIONS)

ARTC = "$(COMPILER_PATH)/bin/tricore-ar.exe"

OPT_ARTC = r

