#@ECHO ON
#/******************************************************************************
#** Copyright (C) Infineon Technologies (2014)                                **
#**                                                                           **
#** All rights reserved.                                                      **
#**                                                                           **
#** This document contains proprietary information belonging to Infineon      **
#** Technologies. Passing on and copying of this document, and communication  **
#** of its contents is not permitted without prior written authorization.     **
#**                                                                           **
#*******************************************************************************
#**  $FILENAME   : SafeTlibCD.mak $                                           **
#**                                                                           **
#**  $CC VERSION : \main\64 $                                                 **
#**                                                                           **
#**  $DATE       : 2015-07-29 $                                               **
#**                                                                           **
#**  PLATFORM  : Infineon AURIX                                               **
#**                                                                           **
#**  COMPILER  : GNU                                                          **
#**                                                                           **
#**  AUTHOR    : SafeTlib Team                                                **
#**                                                                           **
#**  VENDOR    : Infineon Technologies                                        **
#**                                                                           **
#**  DESCRIPTION  : Makefile for SafeTlibCD                                   **
#**                                                                           **
#**  MAY BE CHANGED BY USER [Yes/No]: Yes                                     **
#**                                                                           **
#******************************************************************************/

### SafeTlibCD Objects Directory
SAFETLIBCD_OBJDIR = $(APPL_BASE_DIR)/debug/TC$(STL_TARGET)x/gnu
ifeq ($(UVP_TEST_AUTO),1)
SAFETLIBCD_OBJDIR = $(BASE_PATH)../obj
endif

#-------------------------------------------------------------------------------
# One or more of below vairables may be defined by batch file that invokes the 
# build for system test. The variable definition might be done to do 
# instrumentation for code coverage test
#-------------------------------------------------------------------------------
ifeq ($(COMPILE_SMU), 1)
COMPILE_SMU = $(CCTC_CTC)
else
COMPILE_SMU = $(CCTC)
endif
ifeq ($(COMPILE_TSTHANDLER), 1)
COMPILE_TSTHANDLER = $(CCTC_CTC)
else
COMPILE_TSTHANDLER = $(CCTC)
endif
ifeq ($(COMPILE_CPUMPUTST), 1)
COMPILE_CPUMPUTST = $(CCTC_CTC)
else
COMPILE_CPUMPUTST = $(CCTC)
endif
ifeq ($(COMPILE_SFRCMPTST), 1)
COMPILE_SFRCMPTST = $(CCTC_CTC)
else
COMPILE_SFRCMPTST = $(CCTC)
endif
ifeq ($(COMPILE_SFRCRCTST), 1)
COMPILE_SFRCRCTST = $(CCTC_CTC)
else
COMPILE_SFRCRCTST = $(CCTC)
endif
ifeq ($(COMPILE_CPUBUSMPULFMTST), 1)
COMPILE_CPUBUSMPULFMTST = $(CCTC_CTC)
else
COMPILE_CPUBUSMPULFMTST = $(CCTC)
endif
ifeq ($(COMPILE_CLKMTST), 1)
COMPILE_CLKMTST = $(CCTC_CTC)
else
COMPILE_CLKMTST = $(CCTC)
endif
ifeq ($(COMPILE_PMUECCEDCTST), 1)
COMPILE_PMUECCEDCTST = $(CCTC_CTC)
else
COMPILE_PMUECCEDCTST = $(CCTC)
endif
ifeq ($(COMPILE_REGACCPROTTST), 1)
COMPILE_REGACCPROTTST = $(CCTC_CTC)
else
COMPILE_REGACCPROTTST = $(CCTC)
endif
ifeq ($(COMPILE_SRITST), 1)
COMPILE_SRITST = $(CCTC_CTC)
else
COMPILE_SRITST = $(CCTC)
endif
ifeq ($(COMPILE_TRAPTST), 1)
COMPILE_TRAPTST = $(CCTC_CTC)
else
COMPILE_TRAPTST = $(CCTC)
endif
ifeq ($(COMPILE_VLTMTST), 1)
COMPILE_VLTMTST = $(CCTC_CTC)
else
COMPILE_VLTMTST = $(CCTC)
endif
ifeq ($(COMPILE_SFFTST), 1)
COMPILE_SFFTST = $(CCTC_CTC)
else
COMPILE_SFFTST = $(CCTC)
endif
ifeq ($(COMPILE_WDGTST), 1)
COMPILE_WDGTST = $(CCTC_CTC)
else
COMPILE_WDGTST = $(CCTC)
endif
ifeq ($(COMPILE_SRAMECCTST), 1)
COMPILE_SRAMECCTST = $(CCTC_CTC)
else
COMPILE_SRAMECCTST = $(CCTC)
endif
ifeq ($(COMPILE_CPUSBSTTST), 1)
COMPILE_CPUSBSTTST = $(CCTC_CTC)
else
COMPILE_CPUSBSTTST = $(CCTC)
endif
ifeq ($(COMPILE_LOCKSTEPTST), 1)
COMPILE_LOCKSTEPTST = $(CCTC_CTC)
else
COMPILE_LOCKSTEPTST = $(CCTC)
endif
ifeq ($(COMPILE_SMUTST), 1)
COMPILE_SMUTST = $(CCTC_CTC)
else
COMPILE_SMUTST = $(CCTC)
endif
ifeq ($(COMPILE_SPBTST), 1)
COMPILE_SPBTST = $(CCTC_CTC)
else
COMPILE_SPBTST = $(CCTC)
endif
ifeq ($(COMPILE_IRTST), 1)
COMPILE_IRTST = $(CCTC_CTC)
else
COMPILE_IRTST = $(CCTC)
endif
ifeq ($(COMPILE_IOMTST), 1)
COMPILE_IOMTST = $(CCTC_CTC)
else
COMPILE_IOMTST = $(CCTC)
endif
ifeq ($(COMPILE_PHLSRAMTST), 1)
COMPILE_PHLSRAMTST = $(CCTC_CTC)
else
COMPILE_PHLSRAMTST = $(CCTC)
endif
ifeq ($(COMPILE_DMATST), 1)
COMPILE_DMATST = $(CCTC_CTC)
else
COMPILE_DMATST = $(CCTC)
endif
ifeq ($(COMPILE_LMUREGACCPROTTST), 1)
COMPILE_LMUREGACCPROTTST = $(CCTC_CTC)
else
COMPILE_LMUREGACCPROTTST = $(CCTC)
endif
ifeq ($(COMPILE_LMUBUSMPULFMTST), 1)
COMPILE_LMUBUSMPULFMTST = $(CCTC_CTC)
else
COMPILE_LMUBUSMPULFMTST = $(CCTC)
endif
ifeq ($(COMPILE_FCETST), 1)
COMPILE_FCETST = $(CCTC_CTC)
else
COMPILE_FCETST = $(CCTC)
endif
#-------------------------------------------------------------------------------
include $(SAFETLIBCD_BASE_DIR)/SafeTlibCD_Cfg.mak
include $(COMMON)/Compiler_Options_gnu.mak

ifeq ($(TEST_DELIVERY),ON)
$(MODULE_DELIVERY_NAME)_DELIVERY = ON
endif

### SafeTlibCD object files
SAFETLIBCD_OBJ = 
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/Mtl_TrapTab.o
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/Smu.o $(SAFETLIBCD_OBJDIR)/Smu_PBCfg.o
ifeq ($(TSTHANDLER_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/TstHandler.o $(SAFETLIBCD_OBJDIR)/TstHandler_PBCfg.o 
endif
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/Mtl_Trap.o 
ifeq ($(SFRTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/SfrCmpTst.o $(SAFETLIBCD_OBJDIR)/SfrCrcTst.o $(SAFETLIBCD_OBJDIR)/SfrTst_LCfg.o 
endif
ifeq ($(CPUBUSMPULFMTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/CpuBusMpuLfmTst.o $(SAFETLIBCD_OBJDIR)/CpuBusMpuLfmTst_LCfg.o 
endif
ifeq ($(CPUMPUTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/CpuMpuTst.o $(SAFETLIBCD_OBJDIR)/CpuMpuTst_LCfg.o
endif
ifeq ($(CLKMTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/ClkmTst.o
endif
ifeq ($(VLTMTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/VltmTst.o
endif
ifeq ($(SFFTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/SffTst.o
endif
ifeq ($(PMUECCEDCTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/PmuEccEdcTst.o $(SAFETLIBCD_OBJDIR)/PmuEccEdcTst_RefPtrnDef.o $(SAFETLIBCD_OBJDIR)/PflashMonTst.o $(SAFETLIBCD_OBJDIR)/PflashMonTst_LCfg.o
endif
ifeq ($(TRAPTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/TrapTst.o
endif
ifeq ($(SRITST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/SriTst.o $(SAFETLIBCD_OBJDIR)/SriTst_LCfg.o
endif
ifeq ($(REGACCPROTTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/RegAccProtTst.o
endif
ifeq ($(IRTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/IRTst_IRTab.o
endif
ifeq ($(WDGTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/WdgTst.o $(SAFETLIBCD_OBJDIR)/WdgTst_LCfg.o 
endif
ifeq ($(CPUSBSTTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/CpuSbstTst.o
endif
ifeq ($(SRAMECCTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/SramEccTst.o $(SAFETLIBCD_OBJDIR)/SramEccTst_LCfg.o $(SAFETLIBCD_OBJDIR)/SramEccTst_MemDef.o
endif
ifeq ($(PHLSRAMTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/PhlSramTst.o $(SAFETLIBCD_OBJDIR)/PhlSramTst_LCfg.o $(SAFETLIBCD_OBJDIR)/PhlSramTst_MemDef.o
endif
ifeq ($(LOCKSTEPTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/LockStepTst.o
endif
ifeq ($(SMUTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/SmuTst.o
endif
ifeq ($(SPBTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/SpbTst.o $(SAFETLIBCD_OBJDIR)/SpbTst_LCfg.o 
endif
ifeq ($(IRTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/IRTst.o $(SAFETLIBCD_OBJDIR)/IRTst_LCfg.o $(SAFETLIBCD_OBJDIR)/IRTst_IRTab.o
endif
ifeq ($(IOMTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/IomTst.o $(SAFETLIBCD_OBJDIR)/IomTst_LCfg.o
endif
ifeq ($(DMATST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/DmaTst.o $(SAFETLIBCD_OBJDIR)/DmaTst_LCfg.o
endif
ifeq ($(LMUREGACCPROTTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/LmuRegAccProtTst.o
endif
ifeq ($(LMUBUSMPULFMTST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/LmuBusMpuLfmTst.o $(SAFETLIBCD_OBJDIR)/LmuBusMpuLfmTst_LCfg.o 
endif
ifeq ($(FCETST_DELIVERY),ON)
SAFETLIBCD_OBJ += $(SAFETLIBCD_OBJDIR)/FceTst.o $(SAFETLIBCD_OBJDIR)/FceTst_LCfg.o
endif



#---------------STL_TARGET = SafeTlibCD
all: $(SAFETLIBCD_LIB)/SafeTlibCD.a

PHONY: clean
clean:
	@-rm -f $(SAFETLIBCD_OBJ)
	@-rm -f $(SAFETLIBCD_OBJDIR)/*.src
	@-rm -f $(SAFETLIBCD_OBJDIR)/*.sym

$(SAFETLIBCD_LIB)/SafeTlibCD.a : $(SAFETLIBCD_OBJ)
	@echo Creating archive SafeTlibCD.a
	@$(ARTC) $(OPT_ARTC) $(SAFETLIBCD_LIB)/SafeTlibCD.a $(SAFETLIBCD_OBJ)

### SMU
$(SAFETLIBCD_OBJDIR)/Smu.o: $(SMU_DIR)/src/Smu.c 
	@echo Compiling $(SMU_DIR)/src/Smu.c
	@$(COMPILE_SMU) -o $(SAFETLIBCD_OBJDIR)/Smu.o $(STD_OPT_STL) $(SMU_DIR)/src/Smu.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/Smu.ers

$(SAFETLIBCD_OBJDIR)/Smu_PBCfg.o: $(CONFIG_DIR)/src/Smu_PBCfg.c
	@echo Compiling $(CONFIG_DIR)/src/Smu_PBCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/Smu_PBCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/Smu_PBCfg.c  	
	@-rm -f $(SAFETLIBCD_OBJDIR)/Smu_PBCfg.ers
	
### TEST HANDLER
$(SAFETLIBCD_OBJDIR)/TstHandler.o: $(TEST_HANDLER_DIR)/src/TstHandler.c
	@echo Compiling $(TEST_HANDLER_DIR)/src/TstHandler.c
	@$(COMPILE_TSTHANDLER) -o $(SAFETLIBCD_OBJDIR)/TstHandler.o $(STD_OPT_STL) $(TEST_HANDLER_DIR)/src/TstHandler.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/TstHandler.ers
	
$(SAFETLIBCD_OBJDIR)/TstHandler_PBCfg.o: $(CONFIG_DIR)/src/TstHandler_PBCfg.c
	@echo Compiling $(CONFIG_DIR)/src/TstHandler_PBCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/TstHandler_PBCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/TstHandler_PBCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/TstHandler_PBCfg.ers	
	
### MICRO TEST LIB - CPU MPU TEST
$(SAFETLIBCD_OBJDIR)/CpuMpuTst.o: $(MTL_DIR)/src/CpuMpuTst.c
	@echo Compiling $(MTL_DIR)/src/CpuMpuTst.c
	@$(COMPILE_CPUMPUTST) -o $(SAFETLIBCD_OBJDIR)/CpuMpuTst.o $(STD_OPT_STL_2) $(MTL_DIR)/src/CpuMpuTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/CpuMpuTst.ers
	
$(SAFETLIBCD_OBJDIR)/CpuMpuTst_LCfg.o: $(CONFIG_DIR)/src/CpuMpuTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/CpuMpuTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/CpuMpuTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/CpuMpuTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/CpuMpuTst_LCfg.ers
	

### MICRO TEST LIB - TRAP TEST
$(SAFETLIBCD_OBJDIR)/Mtl_Trap.o: $(MTL_DIR)/src/Mtl_Trap.c
	@echo Compiling $(MTL_DIR)/src/Mtl_Trap.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/Mtl_Trap.o $(STD_OPT_STL) $(MTL_DIR)/src/Mtl_Trap.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/Mtl_Trap.ers
	
$(SAFETLIBCD_OBJDIR)/Mtl_TrapTab.o: $(MTL_DIR)/src/Mtl_TrapTab.c
	@echo Compiling $(MTL_DIR)/src/Mtl_TrapTab.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/Mtl_TrapTab.o $(STD_OPT_STL) $(MTL_DIR)/src/Mtl_TrapTab.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/Mtl_TrapTab.ers 
	
### MICRO TEST LIB - SFR TESTS
$(SAFETLIBCD_OBJDIR)/SfrCmpTst.o: $(MTL_DIR)/src/SfrCmpTst.c
	@echo Compiling $(MTL_DIR)/src/SfrCmpTst.c
	@$(COMPILE_SFRCMPTST) -o $(SAFETLIBCD_OBJDIR)/SfrCmpTst.o $(STD_OPT_STL) $(MTL_DIR)/src/SfrCmpTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/SfrCmpTst.ers
	
$(SAFETLIBCD_OBJDIR)/SfrCrcTst.o: $(MTL_DIR)/src/SfrCrcTst.c
	@echo Compiling $(MTL_DIR)/src/SfrCrcTst.c
	@$(COMPILE_SFRCRCTST) -o $(SAFETLIBCD_OBJDIR)/SfrCrcTst.o $(STD_OPT_STL) $(MTL_DIR)/src/SfrCrcTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/SfrCrcTst.ers
	
$(SAFETLIBCD_OBJDIR)/SfrTst_LCfg.o: $(CONFIG_DIR)/src/SfrTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/SfrTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/SfrTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/SfrTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/SfrTst_LCfg.ers 

$(SAFETLIBCD_OBJDIR)/CpuBusMpuLfmTst.o: $(MTL_DIR)/src/CpuBusMpuLfmTst.c
	@echo Compiling $(MTL_DIR)/src/CpuBusMpuLfmTst.c
	@$(COMPILE_CPUBUSMPULFMTST) -o $(SAFETLIBCD_OBJDIR)/CpuBusMpuLfmTst.o $(STD_OPT_STL) $(MTL_DIR)/src/CpuBusMpuLfmTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/CpuBusMpuLfmTst.ers
	
$(SAFETLIBCD_OBJDIR)/CpuBusMpuLfmTst_LCfg.o: $(CONFIG_DIR)/src/CpuBusMpuLfmTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/CpuBusMpuLfmTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/CpuBusMpuLfmTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/CpuBusMpuLfmTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/CpuBusMpuLfmTst_LCfg.ers 

$(SAFETLIBCD_OBJDIR)/ClkmTst.o: $(MTL_DIR)/src/ClkmTst.c
	@echo Compiling $(MTL_DIR)/src/ClkmTst.c
	@$(COMPILE_CLKMTST) -o $(SAFETLIBCD_OBJDIR)/ClkmTst.o $(STD_OPT_STL) $(MTL_DIR)/src/ClkmTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/ClkmTst.ers
	
$(SAFETLIBCD_OBJDIR)/VltmTst.o: $(MTL_DIR)/src/VltmTst.c
	@echo Compiling $(MTL_DIR)/src/VltmTst.c
	@$(COMPILE_VLTMTST) -o $(SAFETLIBCD_OBJDIR)/VltmTst.o $(STD_OPT_STL) $(MTL_DIR)/src/VltmTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/VltmTst.ers
	
$(SAFETLIBCD_OBJDIR)/SffTst.o: $(MTL_DIR)/src/SffTst.c
	@echo Compiling $(MTL_DIR)/src/SffTst.c
	@$(COMPILE_SFFTST) -o $(SAFETLIBCD_OBJDIR)/SffTst.o $(STD_OPT_STL) $(MTL_DIR)/src/SffTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/SffTst.ers
$(SAFETLIBCD_OBJDIR)/TrapTst.o: $(MTL_DIR)/src/TrapTst.c
	@echo Compiling $(MTL_DIR)/src/TrapTst.c
	@$(COMPILE_TRAPTST) -o $(SAFETLIBCD_OBJDIR)/TrapTst.o $(STD_OPT_STL) $(MTL_DIR)/src/TrapTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/TrapTst.ers
	
$(SAFETLIBCD_OBJDIR)/TrapTst_LCfg.o: $(CONFIG_DIR)/src/TrapTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/TrapTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/TrapTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/TrapTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/TrapTst_LCfg.ers 
	
$(SAFETLIBCD_OBJDIR)/SriTst.o: $(MTL_DIR)/src/SriTst.c
	@echo Compiling $(MTL_DIR)/src/SriTst.c
	@$(COMPILE_SRITST) -o $(SAFETLIBCD_OBJDIR)/SriTst.o $(STD_OPT_STL_2) $(MTL_DIR)/src/SriTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/SriTst.ers
	
$(SAFETLIBCD_OBJDIR)/SriTst_LCfg.o: $(CONFIG_DIR)/src/SriTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/SriTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/SriTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/SriTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/SriTst_LCfg.ers 

$(SAFETLIBCD_OBJDIR)/RegAccProtTst.o: $(MTL_DIR)/src/RegAccProtTst.c
	@echo Compiling $(MTL_DIR)/src/RegAccProtTst.c
	@$(COMPILE_REGACCPROTTST) -o $(SAFETLIBCD_OBJDIR)/RegAccProtTst.o $(STD_OPT_STL) $(MTL_DIR)/src/RegAccProtTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/RegAccProtTst.ers
	
$(SAFETLIBCD_OBJDIR)/RegAccProtTst_LCfg.o: $(CONFIG_DIR)/src/RegAccProtTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/RegAccProtTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/RegAccProtTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/RegAccProtTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/RegAccProtTst_LCfg.ers 
	
$(SAFETLIBCD_OBJDIR)/PmuEccEdcTst.o: $(MTL_DIR)/src/PmuEccEdcTst.c
	@echo Compiling $(MTL_DIR)/src/PmuEccEdcTst.c
	@$(COMPILE_PMUECCEDCTST) -o $(SAFETLIBCD_OBJDIR)/PmuEccEdcTst.o $(STD_OPT_STL) $(MTL_DIR)/src/PmuEccEdcTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/PmuEccEdcTst.ers

$(SAFETLIBCD_OBJDIR)/PmuEccEdcTst_RefPtrnDef.o: $(MTL_DIR)/src/PmuEccEdcTst_RefPtrnDef.c
	@echo Compiling $(MTL_DIR)/src/PmuEccEdcTst_RefPtrnDef.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/PmuEccEdcTst_RefPtrnDef.o $(STD_OPT_STL) $(MTL_DIR)/src/PmuEccEdcTst_RefPtrnDef.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/PmuEccEdcTst_RefPtrnDef.ers
	

$(SAFETLIBCD_OBJDIR)/PflashMonTst.o: $(MTL_DIR)/src/PflashMonTst.c
	@echo Compiling $(MTL_DIR)/src/PflashMonTst.c
	@$(COMPILE_PMUECCEDCTST) -o $(SAFETLIBCD_OBJDIR)/PflashMonTst.o $(STD_OPT_STL) $(MTL_DIR)/src/PflashMonTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/PflashMonTst.ers

$(SAFETLIBCD_OBJDIR)/PflashMonTst_LCfg.o: $(CONFIG_DIR)/src/PflashMonTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/PflashMonTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/PflashMonTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/PflashMonTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/PflashMonTst_LCfg.ers 

$(SAFETLIBCD_OBJDIR)/WdgTst.o: $(MTL_DIR)/src/WdgTst.c
	@echo Compiling $(MTL_DIR)/src/WdgTst.c
	@$(COMPILE_WDGTST) -o $(SAFETLIBCD_OBJDIR)/WdgTst.o $(STD_OPT_STL) $(MTL_DIR)/src/WdgTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/WdgTst.ers
	
$(SAFETLIBCD_OBJDIR)/WdgTst_LCfg.o: $(CONFIG_DIR)/src/WdgTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/WdgTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/WdgTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/WdgTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/WdgTst_LCfg.ers 

$(SAFETLIBCD_OBJDIR)/SramEccTst.o: $(MTL_DIR)/src/SramEccTst.c
	@echo Compiling $(MTL_DIR)/src/SramEccTst.c
	@$(COMPILE_SRAMECCTST) -o $(SAFETLIBCD_OBJDIR)/SramEccTst.o $(STD_OPT_STL) $(MTL_DIR)/src/SramEccTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/SramEccTst.ers
	
$(SAFETLIBCD_OBJDIR)/SramEccTst_LCfg.o: $(CONFIG_DIR)/src/SramEccTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/SramEccTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/SramEccTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/SramEccTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/SramEccTst_LCfg.ers 

$(SAFETLIBCD_OBJDIR)/SramEccTst_MemDef.o: $(MTL_DIR)/src/SramEccTst_MemDef.c
	@echo Compiling $(MTL_DIR)/src/SramEccTst_MemDef.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/SramEccTst_MemDef.o $(STD_OPT_STL) $(MTL_DIR)/src/SramEccTst_MemDef.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/SramEccTst_MemDef.ers 

$(SAFETLIBCD_OBJDIR)/PhlSramTst.o: $(MTL_DIR)/src/PhlSramTst.c
	@echo Compiling $(MTL_DIR)/src/PhlSramTst.c
	@$(COMPILE_PHLSRAMTST) -o $(SAFETLIBCD_OBJDIR)/PhlSramTst.o $(STD_OPT_STL) $(MTL_DIR)/src/PhlSramTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/PhlSramTst.ers
	
$(SAFETLIBCD_OBJDIR)/PhlSramTst_LCfg.o: $(CONFIG_DIR)/src/PhlSramTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/PhlSramTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/PhlSramTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/PhlSramTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/PhlSramTst_LCfg.ers 

$(SAFETLIBCD_OBJDIR)/PhlSramTst_MemDef.o: $(MTL_DIR)/src/PhlSramTst_MemDef.c
	@echo Compiling $(MTL_DIR)/src/PhlSramTst_MemDef.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/PhlSramTst_MemDef.o $(STD_OPT_STL) $(MTL_DIR)/src/PhlSramTst_MemDef.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/PhlSramTst_MemDef.ers 
$(SAFETLIBCD_OBJDIR)/CpuSbstTst.o: $(MTL_DIR)/src/CpuSbstTst.c
	@echo Compiling $(MTL_DIR)/src/CpuSbstTst.c
	@$(COMPILE_CPUSBSTTST) -o $(SAFETLIBCD_OBJDIR)/CpuSbstTst.o $(STD_OPT_STL) $(MTL_DIR)/src/CpuSbstTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/CpuSbstTst.ers	

### MICRO TEST LIB - LOCKSTEP TEST
$(SAFETLIBCD_OBJDIR)/LockStepTst.o: $(MTL_DIR)/src/LockStepTst.c
	@echo Compiling $(MTL_DIR)/src/LockStepTst.c
	@$(COMPILE_LOCKSTEPTST) -o $(SAFETLIBCD_OBJDIR)/LockStepTst.o $(STD_OPT_STL) $(MTL_DIR)/src/LockStepTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/LockStepTst.ers
  
$(SAFETLIBCD_OBJDIR)/SmuTst.o: $(MTL_DIR)/src/SmuTst.c
	@echo Compiling $(MTL_DIR)/src/SmuTst.c
	@$(COMPILE_SMUTST) -o $(SAFETLIBCD_OBJDIR)/SmuTst.o $(STD_OPT_STL) $(MTL_DIR)/src/SmuTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/SmuTst.ers
	
$(SAFETLIBCD_OBJDIR)/SpbTst.o: $(MTL_DIR)/src/SpbTst.c
	@echo Compiling $(MTL_DIR)/src/SpbTst.c
	@$(COMPILE_SPBTST) -o $(SAFETLIBCD_OBJDIR)/SpbTst.o $(STD_OPT_STL) $(MTL_DIR)/src/SpbTst.c  
	
$(SAFETLIBCD_OBJDIR)/SpbTst_LCfg.o: $(CONFIG_DIR)/src/SpbTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/SpbTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/SpbTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/SpbTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/SpbTst_LCfg.ers
	

$(SAFETLIBCD_OBJDIR)/DmaTst.o: $(MTL_DIR)/src/DmaTst.c
	@echo Compiling $(MTL_DIR)/src/DmaTst.c
	@$(COMPILE_DMATST) -o $(SAFETLIBCD_OBJDIR)/DmaTst.o $(STD_OPT_STL) $(MTL_DIR)/src/DmaTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/DmaTst.ers
	
$(SAFETLIBCD_OBJDIR)/DmaTst_LCfg.o: $(CONFIG_DIR)/src/DmaTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/DmaTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/DmaTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/DmaTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/DmaTst_LCfg.ers		
	

$(SAFETLIBCD_OBJDIR)/IRTst_LCfg.o: $(CONFIG_DIR)/src/IRTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/IRTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/IRTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/IRTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/IRTst_LCfg.ers
	
$(SAFETLIBCD_OBJDIR)/IRTst_IRTab.o: $(MTL_DIR)/src/IRTst_IRTab.c
	@echo Compiling $(MTL_DIR)/src/IRTst_IRTab.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/IRTst_IRTab.o $(STD_OPT_STL) $(MTL_DIR)/src/IRTst_IRTab.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/IRTst_IRTab.ers
	
$(SAFETLIBCD_OBJDIR)/IRTst.o: $(MTL_DIR)/src/IRTst.c
	@echo Compiling $(MTL_DIR)/src/IRTst.c
	@$(COMPILE_IRTST) -o $(SAFETLIBCD_OBJDIR)/IRTst.o $(STD_OPT_STL) $(MTL_DIR)/src/IRTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/IRTst.ers
  
$(SAFETLIBCD_OBJDIR)/IomTst_LCfg.o: $(CONFIG_DIR)/src/IomTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/IomTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/IomTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/IomTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/IomTst_LCfg.ers
	
$(SAFETLIBCD_OBJDIR)/IomTst.o: $(MTL_DIR)/src/IomTst.c
	@echo Compiling $(MTL_DIR)/src/IomTst.c
	@$(COMPILE_IOMTST) -o $(SAFETLIBCD_OBJDIR)/IomTst.o $(STD_OPT_STL) $(MTL_DIR)/src/IomTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/IomTst.ers

### MICRO TEST LIB - LMU REGISTER ACCESS PROTECTION TEST
$(SAFETLIBCD_OBJDIR)/LmuRegAccProtTst.o: $(MTL_DIR)/src/LmuRegAccProtTst.c
	@echo Compiling $(MTL_DIR)/src/LmuRegAccProtTst.c
	@$(COMPILE_LMUREGACCPROTTST) -o $(SAFETLIBCD_OBJDIR)/LmuRegAccProtTst.o $(STD_OPT_STL) $(MTL_DIR)/src/LmuRegAccProtTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/LmuRegAccProtTst.ers
	
	
### MICRO TEST LIB - LMU BUS MPU LFM TEST
$(SAFETLIBCD_OBJDIR)/LmuBusMpuLfmTst.o: $(MTL_DIR)/src/LmuBusMpuLfmTst.c
	@echo Compiling $(MTL_DIR)/src/LmuBusMpuLfmTst.c
	@$(COMPILE_LMUBUSMPULFMTST) -o $(SAFETLIBCD_OBJDIR)/LmuBusMpuLfmTst.o $(STD_OPT_STL) $(MTL_DIR)/src/LmuBusMpuLfmTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/LmuBusMpuLfmTst.ers
	
	
$(SAFETLIBCD_OBJDIR)/LmuBusMpuLfmTst_LCfg.o: $(CONFIG_DIR)/src/LmuBusMpuLfmTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/LmuBusMpuLfmTst_LCfg.c
	@$(COMPILE_LMUBUSMPULFMTST) -o $(SAFETLIBCD_OBJDIR)/LmuBusMpuLfmTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/LmuBusMpuLfmTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/LmuBusMpuLfmTst_LCfg.ers
	
### MICRO TEST LIB - FCE KERNEL TEST
$(SAFETLIBCD_OBJDIR)/FceTst.o: $(MTL_DIR)/src/FceTst.c
	@echo Compiling $(MTL_DIR)/src/FceTst.c
	@$(COMPILE_FCETST) -o $(SAFETLIBCD_OBJDIR)/FceTst.o $(STD_OPT_STL) $(MTL_DIR)/src/FceTst.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/FceTst.ers
	
$(SAFETLIBCD_OBJDIR)/FceTst_LCfg.o: $(CONFIG_DIR)/src/FceTst_LCfg.c
	@echo Compiling $(CONFIG_DIR)/src/FceTst_LCfg.c
	@$(CCTC) -o $(SAFETLIBCD_OBJDIR)/FceTst_LCfg.o $(STD_OPT_STL) $(CONFIG_DIR)/src/FceTst_LCfg.c  
	@-rm -f $(SAFETLIBCD_OBJDIR)/FceTst_LCfg.ers		
