
/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2013)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME  :  SafeWdgExtCic_PBCfg.c                                        **
**                                                                            **
**  VERSION   : 0.0.1                                                         **
**                                                                            **
**  DATE, TIME: 2016-09-14, 17:39:59                                          **
**                                                                            **
**  GENERATOR : Build b141014-0350                                            **
**                                                                            **
**  BSW MODULE DECRIPTION : SafeWdgExtCic.bmd/xdm                             **
**                                                                            **
**  VARIANT   : VariantPB                                                     **
**                                                                            **
**  PLATFORM  : Infineon Aurix                                                **
**                                                                            **
**  COMPILER  : Tasking / GNU / Diab                                          **
**                                                                            **
**  AUTHOR    : DL-BLR-ATV-STC                                                **
**                                                                            **                                                                      
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : SafeWdgExtCic configuration generated file                 **
**                                                                            **
**  SPECIFICATION(S) :Aurix-HE_SafeTlib_DS_SafeWdgExtCic.docm, Ver 0.1        **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: no                                       **
**                                                                            **
*******************************************************************************/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "SafeWdgIf.h"

/*******************************************************************************
**                      Private Macro definition                              **
*******************************************************************************/


/*******************************************************************************
**                      Configuration Options                                 **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/


/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
#define IFX_SWDG_START_SEC_POSTBUILDCFG_ASIL_B
#include "Ifx_MemMap.h"


/* external CiC watchdog configuration */
const SafeWdgExtCic_ConfigType SafeWdgExtCic_ConfigRoot[] =
{
 
    {
      /* configuration for safety path check */
       &SafeWdgAscLin_ConfigRoot[0],   /* pointer to config for ASCLIN */       
       {
     
        {                 
                 
           /* Cic expected values */
           {0xc79697e7u, 0x3f9b7e93u},      /* F80DE974  0*/
           {0xe44b1f14u, 0x5309b92fu},      /* B742A63B  1*/
           {0x88675a85u, 0x667ca5e7u},      /* EE1BFF62  2*/
           {0xabbad276u, 0xaee625bu},      /* A154B02D  3*/
           {0xcdef171bu, 0xbf6874e5u},      /* 728763FE  4*/
           {0xee329fe8u, 0xd3fab359u},      /* 3DC82CB1  5*/   
           {0x821eda79u, 0xe68faf91u},      /* 649175E8  6*/    
           {0xa1c3528au, 0x8a1d682du},      /* 2BDE3AA7  7*/
           {0x9a2ee746u, 0x4f0e231fu},      /* D520C459  8*/
           {0xb9f36fb5u, 0x239ce4a3u},      /* 9A6F8B16  9*/
           {0xd5df2a24u, 0x16e9f86bu},    /* C336D24F  10*/
           {0xf602a2d7u, 0x7a7b3fd7u},    /* 8C799D00  11*/
           {0x905767bau, 0xcffd2969u},    /* 5FAA4ED3  12*/
           {0xb38aef49u, 0xa36feed5u},    /* 10E5019C  13*/
           {0xdfa6aad8u, 0x961af21du},    /* 49BC58C5  14*/
           {0xfc7b222bu, 0xfa8835a1u}     /* 06F3178A  15*/
        
      }  
    }    
  }  
};

#define IFX_SWDG_STOP_SEC_POSTBUILDCFG_ASIL_B
#include "Ifx_MemMap.h"

