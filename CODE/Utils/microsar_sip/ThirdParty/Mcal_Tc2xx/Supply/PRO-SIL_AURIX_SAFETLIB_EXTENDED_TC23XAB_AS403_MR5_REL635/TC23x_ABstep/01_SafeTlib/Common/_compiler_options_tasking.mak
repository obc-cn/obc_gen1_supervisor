#/******************************************************************************
#** Copyright (C) Infineon Technologies (2014)                                **
#**                                                                           **
#** All rights reserved.                                                      **
#**                                                                           **
#** This document contains proprietary information belonging to Infineon      **
#** Technologies. Passing on and copying of this document, and communication  **
#** of its contents is not permitted without prior written authorization.     **
#**                                                                           **
#*******************************************************************************
#**   $FILENAME   : compiler_options_tasking.mak $                             **
#**                                                                           **
#**   $CC VERSION : \main\dev_tc2xx_ca\33 $                                    **
#**                                                                           **
#**   $DATE       : 2017-11-28 $                                               **
#**                                                                           **
#**  VARIANT   : VariantPB                                                    **
#**                                                                           **
#**  PLATFORM  : Infineon AURIX                                               **
#**                                                                           **
#**  COMPILER  : Tasking                                                      **
#**                                                                           **
#**  AUTHOR    : SafeTlib Team                                                **
#**                                                                           **
#**  VENDOR    : Infineon Technologies                                        **
#**                                                                           **
#**  TRACEABILITY:                                                            **
#**                                                                           **
#**  DESCRIPTION  : This file compiler make options                           **
#**                                                                           **
#**  SPECIFICATION(S) :                                                       **
#**                                                                           **
#**  MAY BE CHANGED BY USER [Yes/No]: No                                      **
#**                                                                           **
#******************************************************************************/
#------------------------------------------------------------
# Following variables are used only for system test 
#------------------------------------------------------------
ifeq ($(CODE_COVERAGE),1)
include $(CTC_BASE_PATH)/mak/infineon_ctc_defs.mak
CODE_COVERAGE_DEFINED=-D_CODE_COVERAGE_
endif

ifeq ($(UVP_TEST_AUTO),1)
UVP_TEST_AUTO_DEFINED=-DUVP_TEST_AUTO
endif

ifeq ($(RA_SMU_SET_ALARM),1)
RA_SMU_SET_ALARM_DEFINED=-D_RA_SMU_SET_ALARM_
endif
#------------------------------------------------------------
### SMU Directory
SMU_DIR = $(SAFETLIBCD_BASE_DIR)/SMU

### Test Handler Directory
TEST_HANDLER_DIR = $(SAFETLIBCD_BASE_DIR)/TestHandler

### Micro Test Lib Directory
MTL_DIR = $(SAFETLIBCD_BASE_DIR)/MicroTestLib

### Safe Watchdog Interface Directory
SAFEWDG_IF_DIR = $(SAFEWDG_BASE_DIR)/SafeWdgIf

### Internal Safe Watchdog Directory
SAFEWDG_INT_DIR = $(SAFEWDG_BASE_DIR)/SafeWdgInt

### External CiC Safe Watchdog Directory
SAFEWDG_EXTCIC_DIR = $(SAFEWDG_BASE_DIR)/SafeWdgExtCic

### External TLF Safe Watchdog Directory
SAFEWDG_EXTTLF_DIR = $(SAFEWDG_BASE_DIR)/SafeWdgExtTlf

### External Alive Watchdog Directory
SAFEWDG_EXTALIVE_DIR = $(SAFEWDG_BASE_DIR)/SafeWdgExtAlive

### ASCLIN SPI driver Directory
SAFEWDG_ASCLIN_DIR = $(SAFEWDG_BASE_DIR)/SafeWdgAscLin

### QSPI driver Directory
SAFEWDG_QSPI_DIR = $(SAFEWDG_BASE_DIR)/SafeWdgQspi

MCAL_Common_Headers =
IRQ_CFG_INCLUDE=

ifeq ($(SAFETLIB_STANDALONE), FALSE)
MCAL_Common_Headers = -I"$(BASE_PATH)/../Aurix_MC-ISAR/integration_general/ssc/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/tricore_general/ssc/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/ecum_infineon_tricore/ssc/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/ecum_infineon_tricore/cfg1/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/spi_infineon_tricore/ssc/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/spi_infineon_tricore/cfg1/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/adc_infineon_tricore/ssc/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/adc_infineon_tricore/cfg1/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/lin_17_asclin_infineon_tricore/ssc/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/lin_17_asclin_infineon_tricore/cfg1/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/integration_general/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/uart_infineon_tricore/ssc/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/uart_infineon_tricore/cfg1/inc" -I"$(BASE_PATH)/../Aurix_MC-ISAR/demoapp/tricore/compiler"

IRQ_CFG_INCLUDE=-I"$(BASE_PATH)/../Aurix_MC-ISAR/irq_infineon_tricore/cfg1/inc"
endif

ifeq ($(SAFETLIB_STANDALONE), TRUE)
IRQ_CFG_INCLUDE=-I$(APPL_BASE_DIR)/cfg
endif

STL_TARGET_EP=FALSE

ifeq ($(STL_TARGET),23)
STL_TARGET_EP=TRUE
endif

ifeq ($(STL_TARGET),22)
STL_TARGET_EP=TRUE
endif

ifeq ($(STL_TARGET),21)
STL_TARGET_EP=TRUE
endif


### Header file inclusions
ifeq ($(STL_TARGET),27)
Common_Headers = -I"$(COMMON)/" -I"$(COMMON)/TC$(STL_TARGET)xC/" -I"$(CTC_BASE_PATH)/inc" -I"$(CONFIG_DIR)/inc" $(MCAL_Common_Headers)
else
Common_Headers = -I"$(COMMON)/" -I"$(COMMON)/TC$(STL_TARGET)x/" -I"$(CTC_BASE_PATH)/inc" -I"$(CONFIG_DIR)/inc" $(MCAL_Common_Headers)
endif

SafeTlibCD_Headers = -I"$(SMU_DIR)/inc/" -I"$(TEST_HANDLER_DIR)/inc" -I"$(MTL_DIR)/inc/"  

SafeWdgCD_Headers = -I"$(SAFEWDG_IF_DIR)/inc/" -I"$(SAFEWDG_INT_DIR)/inc/" -I"$(SAFEWDG_EXTCIC_DIR)/inc/" -I"$(SAFEWDG_EXTTLF_DIR)/inc/" -I"$(SAFEWDG_EXTALIVE_DIR)/inc/" -I"$(SAFEWDG_ASCLIN_DIR)/inc/" -I"$(SAFEWDG_QSPI_DIR)/inc/" -I"$(APPL_BASE_DIR)/inc/" $(IRQ_CFG_INCLUDE)

UpperLayer_Headers = -I"$(SWDGM_BASE_DIR)/inc/" -I"$(TSTM_BASE_DIR)/inc/"

Application_Headers = -I"$(APPL_BASE_DIR)/inc/" -I"$(APPL_BASE_DIR)/tasking_wks/RefApp/"  $(IRQ_CFG_INCLUDE)


###  SafeTLibCD Headers
STL_CD_HEADERS = $(Common_Headers) $(SafeTlibCD_Headers) $(SafeWdgCD_Headers) -I"$(BASE_PATH)/../../TestApp/inc/"

###  SafeWdgCD Header
SWDG_CD_HEADERS = $(Common_Headers) $(SafeWdgCD_Headers) 

###  Applications Header
APPL_HEADERS = $(Common_Headers) $(Application_Headers) $(UpperLayer_Headers) $(SafeTlibCD_Headers) $(SafeWdgCD_Headers) -I"$(BASE_PATH)../inc/"

### Compiler Options
### This is for Mcal and SafeTlib integration purpose

ifeq ($(C_STANDARD),C90)
CSTANDARD_FLAG=--iso=90
endif

ifeq ($(C_STANDARD),C99)
CSTANDARD_FLAG=--iso=99
endif

ifeq ($(COMPILER_CHOICE_TP),tasking)
MANDATORY_CFLAGS = --core=tc1.6.x  $(CSTANDARD_FLAG) --eabi-compliant --integer-enumeration --language=-comments,-gcc,+volatile,-strings --switch=auto --align=0 --default-near-size=0 --default-a0-size=0 --default-a1-size=0 -O2ROPYG --tradeoff=4 -D_TASKING_C_TRICORE_=1 -c
endif

ifeq ($(COMPILER_CHOICE_TP),tasking50r2)
MANDATORY_CFLAGS = --core=tc1.6.x  $(CSTANDARD_FLAG) --eabi-compliant --integer-enumeration --language=-comments,-gcc,+volatile,-strings --switch=auto --align=0 --default-near-size=0 --default-a0-size=0 --default-a1-size=0 -O2RO --tradeoff=4 -D_TASKING_C_TRICORE_=1 -c
endif

ifeq ($(COMPILER_CHOICE_TP),tasking50r2p3)
MANDATORY_CFLAGS = --core=tc1.6.x  $(CSTANDARD_FLAG) --eabi-compliant --integer-enumeration --language=-comments,-gcc,+volatile,-strings --switch=auto --align=0 --default-near-size=0 --default-a0-size=0 --default-a1-size=0 -O2RO --tradeoff=4 -D_TASKING_C_TRICORE_=1 -c
endif

ifeq ($(COMPILER_CHOICE_TP),tasking62r1p3)
MANDATORY_CFLAGS = --core=tc1.6.x  $(CSTANDARD_FLAG) --eabi-compliant --integer-enumeration --language=-comments,-gcc,+volatile,-strings --switch=auto --align=0 --default-near-size=0 --default-a0-size=0 --default-a1-size=0 -O2RO --tradeoff=4 -D_TASKING_C_TRICORE_=1 -c
endif
ifeq ($(COMPILER_CHOICE_TP),tasking)
OPTIONAL_CFLAGS = -g --source -OKLF-predict -I"$(COMPILER_PATH)/include"
endif

ifeq ($(COMPILER_CHOICE_TP),tasking50r2)
OPTIONAL_CFLAGS = -g --source -OPFGKY --loop=F -I"$(COMPILER_PATH)/include"
endif

ifeq ($(COMPILER_CHOICE_TP),tasking50r2p3)
OPTIONAL_CFLAGS = -g --source -OPFGKY --loop=F -I"$(COMPILER_PATH)/include"
endif

ifeq ($(COMPILER_CHOICE_TP),tasking62r1p3)
OPTIONAL_CFLAGS = -g --source -OPFGKY --loop=F -I"$(COMPILER_PATH)/include"
endif
ifeq ($(COMPILER_TASKING_VER),V4)
MANDATORY_CFLAGS = --core=tc1.6.x  $(CSTANDARD_FLAG) --eabi-compliant --integer-enumeration --language=-comments,-gcc,+volatile,-strings --switch=auto --align=0 --default-near-size=0 --default-a0-size=0 --default-a1-size=0 -O2ROPYG --tradeoff=4 -D_TASKING_C_TRICORE_=1 -c
endif

ifeq ($(COMPILER_TASKING_VER),V5)
MANDATORY_CFLAGS = --core=tc1.6.x  $(CSTANDARD_FLAG) --eabi-compliant --integer-enumeration --language=-comments,-gcc,+volatile,-strings --switch=auto --align=0 --default-near-size=0 --default-a0-size=0 --default-a1-size=0 -O2RO --tradeoff=4 -D_TASKING_C_TRICORE_=1 -c
endif
ifeq ($(COMPILER_TASKING_VER),V6)
MANDATORY_CFLAGS = --core=tc1.6.x  $(CSTANDARD_FLAG) --eabi-compliant --integer-enumeration --language=-comments,-gcc,+volatile,-strings --switch=auto --align=0 --default-near-size=0 --default-a0-size=0 --default-a1-size=0 -O2RO --tradeoff=4 -D_TASKING_C_TRICORE_=1 -c
endif

ifeq ($(COMPILER_TASKING_VER),V4)
OPTIONAL_CFLAGS = -g --source -OK -I"$(COMPILER_PATH)/include"
endif

ifeq ($(COMPILER_TASKING_VER),V5)
OPTIONAL_CFLAGS = -g --source -OPFGK --loop=F -I"$(COMPILER_PATH)/include"
endif

ifeq ($(COMPILER_TASKING_VER),V6)
OPTIONAL_CFLAGS = -g --source -OPFGK --loop=F -I"$(COMPILER_PATH)/include"
endif

### Assembler options
MANDATORY_ASM_FLAGS = -Wa-Ogs

OPTIONAL_ASM_FLAGS = -Wa-gAHLs --emit-locals=-equ,-symbols -Wa--error-limit=42

ifeq ($(OPTIONAL_OPTIMIZATION),Y)

STD_OPT_STL = $(STL_CD_HEADERS) $(MANDATORY_CFLAGS) $(OPTIONAL_CFLAGS) $(MANDATORY_ASM_FLAGS) $(OPTIONAL_ASM_FLAGS) $(UVP_TEST_AUTO_DEFINED)

STD_OPT_SWDG = $(SWDG_CD_HEADERS) $(MANDATORY_CFLAGS) $(OPTIONAL_CFLAGS) $(MANDATORY_ASM_FLAGS) $(OPTIONAL_ASM_FLAGS)

STD_OPT_APPL = $(APPL_HEADERS) $(CODE_COVERAGE_DEFINED) $(RA_SMU_SET_ALARM_DEFINED) $(MANDATORY_CFLAGS) $(OPTIONAL_CFLAGS) $(MANDATORY_ASM_FLAGS) $(OPTIONAL_ASM_FLAGS)

endif

ifeq ($(OPTIONAL_OPTIMIZATION),N)

STD_OPT_STL = $(STL_CD_HEADERS) $(MANDATORY_CFLAGS) $(MANDATORY_ASM_FLAGS) $(OPTIONAL_ASM_FLAGS) $(UVP_TEST_AUTO_DEFINED)

STD_OPT_SWDG = $(SWDG_CD_HEADERS) $(MANDATORY_CFLAGS) $(MANDATORY_ASM_FLAGS) $(OPTIONAL_ASM_FLAGS)

STD_OPT_APPL = $(APPL_HEADERS) $(CODE_COVERAGE_DEFINED) $(RA_SMU_SET_ALARM_DEFINED) $(MANDATORY_CFLAGS) $(MANDATORY_ASM_FLAGS) $(OPTIONAL_ASM_FLAGS)

endif
### Below are Compiler options For Test scripts
ifeq ($(COMPILER_CHOICE_TP),tasking)
STD_OPT_APPL_INT = $(APPL_HEADERS) $(CODE_COVERAGE_DEFINED) $(RA_SMU_SET_ALARM_DEFINED) --core=tc1.6.x -t -Wa-gAHLs --emit-locals=-equ,-symbols -Wa-Ogs -Wa--error-limit=42 --iso=90 --integer-enumeration --language=-comments,-gcc,+volatile,-strings --switch=auto --align=0 --default-near-size=0 --default-a0-size=0 --default-a1-size=0 -O2ROK --tradeoff=4 -g --source -D_TASKING_C_TRICORE_=1 -I"$(COMPILER_PATH)/include" -c
endif

ifeq ($(COMPILER_CHOICE_TP),tasking50r2)
STD_OPT_APPL_INT = $(APPL_HEADERS) $(CODE_COVERAGE_DEFINED) $(RA_SMU_SET_ALARM_DEFINED) --core=tc1.6.x -t -Wa-gAHLs --emit-locals=-equ,-symbols -Wa-Ogs -Wa--error-limit=42 --iso=90 --integer-enumeration --language=-comments,-gcc,+volatile,-strings --switch=auto --align=0 --default-near-size=0 --default-a0-size=0 --default-a1-size=0 -O2ROK --loop=F --tradeoff=4 -g --source -D_TASKING_C_TRICORE_=1 -I"$(COMPILER_PATH)/include" -c
endif

ifeq ($(COMPILER_CHOICE_TP),tasking50r2p3)
STD_OPT_APPL_INT = $(APPL_HEADERS) $(CODE_COVERAGE_DEFINED) $(RA_SMU_SET_ALARM_DEFINED) --core=tc1.6.x -t -Wa-gAHLs --emit-locals=-equ,-symbols -Wa-Ogs -Wa--error-limit=42 --iso=90 --integer-enumeration --language=-comments,-gcc,+volatile,-strings --switch=auto --align=0 --default-near-size=0 --default-a0-size=0 --default-a1-size=0 -O2ROK --loop=F --tradeoff=4 -g --source -D_TASKING_C_TRICORE_=1 -I"$(COMPILER_PATH)/include" -c
endif

ifeq ($(COMPILER_CHOICE_TP),tasking62r1p3)
STD_OPT_APPL_INT = $(APPL_HEADERS) $(CODE_COVERAGE_DEFINED) $(RA_SMU_SET_ALARM_DEFINED) --core=tc1.6.x -t -Wa-gAHLs --emit-locals=-equ,-symbols -Wa-Ogs -Wa--error-limit=42 --iso=90 --integer-enumeration --language=-comments,-gcc,+volatile,-strings --switch=auto --align=0 --default-near-size=0 --default-a0-size=0 --default-a1-size=0 -O2ROK --loop=F --tradeoff=4 -g --source -D_TASKING_C_TRICORE_=1 -I"$(COMPILER_PATH)/include" -c
endif
CCTC = "$(COMPILER_PATH)/bin/cctc.exe"
CCTC_CTC=$(CCTC)

ifeq ($(CODE_COVERAGE),1)
CODE_COV_CMD = $(CODE_COV_PATH)/ctc.exe
CODE_COV_OPT_GENERAL = -i m -k
CODE_COV_OPT_INI = -c $(BASE_PATH)..\..\ctc\ctc_aurix_$(COMPILER).ini
CODE_COV_OPT_SYM = -n $(BASE_PATH)..\obj\$(MODULE_NAME)_ctc.sym
CCTC_CTC = "$(CODE_COV_CMD)" $(CODE_COV_OPT_GENERAL) $(CODE_COV_OPT_INI) $(CODE_COV_OPT_SYM) "cctc"
endif

### Linker Options 
LINKER = "$(COMPILER_PATH)/bin/ltc.exe"

ifeq ($(STL_TARGET_EP),TRUE)
LINKER_OPTIONS = -Cmpe:tc0 -Otxycl -D__CPU__=userdef16x_tc0
else
LINKER_OPTIONS = -Cmpe:vtc -Otxycl -D__CPU__=tc$(STL_TARGET)x
endif

OPTIONAL_LINKER_OPTIONS = -o "$(APPL_OUTDIR)/$(APPL_TARGET).elf" -o "$(APPL_OUTDIR)/$(APPL_TARGET).hex":IHEX --hex-format=s -d"$(APPL_BASE_DIR)/src/_mcal_pjt$(RA_LSL_FILE_SUFFIX).lsl" -L"$(COMPILER_PATH)/include.lsl" --error-limit=42 -M -mcrfiklsmnoduq -L"$(COMPILER_PATH)/lib"  -lc -lfp -lrt

OPT_LKLC = $(LINKER_OPTIONS) $(OPTIONAL_LINKER_OPTIONS)

ARTC = "$(COMPILER_PATH)/bin/artc.exe"

OPT_ARTC = -cr

