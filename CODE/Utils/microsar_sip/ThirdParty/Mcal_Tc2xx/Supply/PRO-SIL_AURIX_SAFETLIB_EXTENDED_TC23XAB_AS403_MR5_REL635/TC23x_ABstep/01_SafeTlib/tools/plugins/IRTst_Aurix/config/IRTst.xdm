<?xml version='1.0'?>
<!--
/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2014)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**   $FILENAME   : IRTst.xdm $                                                **
**                                                                            **
**   $CC VERSION : \main\12 $                                                 **
**                                                                            **
**   $DATE       : 2015-07-20 $                                               **
**                                                                            **
**  PLATFORM  : Infineon AURIX                                                **
**                                                                            **
**  VARIANT   : VariantLinkTime                                               **
**                                                                            **
**  AUTHOR    : DL-AUTOSAR-Engineering                                        **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  TRACEABILITY:[cover parentID=]                                            **
**                                                                            **
**  DESCRIPTION  : XML Data Model for IRTst driver                            **
**                                                                            **
**  [/cover]                                                                  **
**                                                                            **
**  SPECIFICATION(S) : Aurix-HE_SafeTlib_DS_IRTest.docm, Ver 1.1              **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: No                                       **
**                                                                            **
*******************************************************************************/
-->
<datamodel version="3.0" 
           xmlns="http://www.tresos.de/_projects/DataModel2/08/root.xsd" 
           xmlns:a="http://www.tresos.de/_projects/DataModel2/08/attribute.xsd" 
           xmlns:v="http://www.tresos.de/_projects/DataModel2/06/schema.xsd" 
           xmlns:d="http://www.tresos.de/_projects/DataModel2/06/data.xsd">

  <d:ctr type="AUTOSAR" factory="autosar"  
         xmlns:ad="http://www.tresos.de/_projects/DataModel2/08/admindata.xsd" 
         xmlns:icc="http://www.tresos.de/_projects/DataModel2/08/implconfigclass.xsd">
    <d:lst type="TOP-LEVEL-PACKAGES">
      <d:ctr name="AURIX" type="AR-PACKAGE">        
        <a:a name="UUID" value="93d9a111-9d4c-47b6-98cb-fe5aec24c8b0"/>
        <d:lst type="ELEMENTS">
          <d:chc name="IRTst" type="AR-ELEMENT" value="MODULE-DEF">
            <v:ctr type="MODULE-DEF">
              <a:a name="RELEASE" value="asc:4.0"/>        
              <a:a name="UUID" value="03eaa8c5-137e-402e-823d-8d7776c1de4d"/>
              <v:var name="IMPLEMENTATION_CONFIG_VARIANT" type="ENUMERATION">
                <a:a name="LABEL" value="Config Variant"/>
                <a:da name="DEFAULT" value="VariantLinkTime"/>
                <a:da name="RANGE" value="VariantLinkTime"/>                
              </v:var>
              <v:lst name="IRTstConfigSet" type="MULTIPLE-CONFIGURATION-CONTAINER">
                <a:da name="MIN" value="1"/>
                <a:da name="MAX" value="1"/>                
                <v:ctr name="IRTstConfigSet" type="MULTIPLE-CONFIGURATION-CONTAINER">
                  <a:a name="DESC" 
                       value="This is the base container that contains the link time configuration parameters"/>
                  <a:a name="UUID" value="ccda81dc-46cf-4171-858b-7c947884702f"/>
                  <v:var name="DMASelect" type="BOOLEAN">
                    <a:da name="TOOLTIP" value="This boolean parameter used to Enable/Disable DMA as type of service provider in Interrupt Router test."/>
                    <a:a name="DESC" 
                         value="EN: This boolean parameter used to Enable/Disable DMA as type of service provider in Interrupt Router test."/>
                    <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                         type="IMPLEMENTATIONCONFIGCLASS">
                       <icc:v class="Link">VariantLinkTime</icc:v>
                    </a:a>
                    <a:a name="ORIGIN" value="INFINEON_AURIX"/>
                    <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                    <a:a name="UUID" value="ef9ce730-c48a-499a-8eff-8b03a15032cc"/>
                    <a:da name="DEFAULT" value="true"/>
                  </v:var>
                  
                  <v:var name="DmaChannel" type="INTEGER">
                    <a:da name="TOOLTIP" value="This parameter defines the Dma channel number"/>
                    <a:a name="DESC">
                      <a:v>This parameter defines the Dma channel number.
                      </a:v>
                    </a:a>
                    <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                         type="IMPLEMENTATIONCONFIGCLASS">
                           <icc:v class="Link">VariantLinkTime</icc:v>
                    </a:a>                          
                    <a:a name="ORIGIN" value="INFINEON_AURIX"/>
                    <a:a name="UUID" 
                         value="98ce928e-7ed1-4368-95b2-21d30b1fa49e"/>
                    <a:da name="DEFAULT" value="0"/>
                    <a:da name="INVALID" type="Range">
                      <a:tst expr="&lt;ecu:get('SafeTlib.DmaMaxChannels')"/>
                      <a:tst expr="&gt;=0"/>
                    </a:da>
                      <a:da name="EDITABLE" type="XPath">
                      <a:tst expr="../DMASelect  = 'true'"/>
                    </a:da> 
                  </v:var> 
                 
                </v:ctr>                  
              </v:lst>
              <v:ctr name="IRTst" type="IDENTIFIABLE">
                <a:a name="DESC" 
                     value="EN: This test checks for stuck at fault and IR ECC EDC mechanism on the configured SRNs. Two complementary SRNs are selected. 
                            To check IR ECC mechanism interrupt is raised by with or without corrupt ECC of the SRN. SMU alarm checked for detection of corrupt ECC."/>
                <a:a name="UUID" value="3ea7abca-6ba4-4d14-befd-8bb9350fe86f"/>
              </v:ctr>
              </v:ctr>         
          </d:chc>
        </d:lst>
      </d:ctr>
    </d:lst>
  </d:ctr>
</datamodel>
