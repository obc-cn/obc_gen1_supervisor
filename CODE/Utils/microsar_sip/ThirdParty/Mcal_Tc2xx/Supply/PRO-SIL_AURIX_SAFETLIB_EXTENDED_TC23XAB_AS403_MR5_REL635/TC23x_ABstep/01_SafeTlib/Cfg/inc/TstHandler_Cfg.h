/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2014)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME  :  TstHandler_Cfg.h                                             **
**                                                                            **
**  DATE, TIME: 2017-08-29, 11:38:11                                          **
**                                                                            **
**  GENERATOR : Build b141014-0350                                            **
**                                                                            **
**  BSW MODULE DECRIPTION : TstHandler.bmd                                    **
**                                                                            **
**  VARIANT   : VariantPB                                                     **
**                                                                            **
**  PLATFORM  : Infineon Aurix                                                **
**                                                                            **
**  COMPILER  : Tasking / GNU / Diab                                          **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : TstHandler configuration header file                       **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: no                                       **
**                                                                            **
*******************************************************************************/
#ifndef TSTHANDLER_CFG_H
#define TSTHANDLER_CFG_H
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

#include "ClkmTst.h"
#include "VltmTst.h"
#include "TrapTst.h"
#include "SriTst.h"
#include "PmuEccEdcTst.h"
#include "PflashMonTst.h"
#include "WdgTst.h"

#include "CpuSbstTst.h"
#include "LockStepTst.h"
#include "CpuBusMpuLfmTst.h"
#include "CpuMpuTst.h"
#include "SpbTst.h"
#include "SfrCmpTst.h"
#include "SfrCrcTst.h"
#include "RegAccProtTst.h"
#include "SffTst.h"
#include "SramEccTst.h"
#include "DmaTst.h"
#include "IomTst.h"
#include "IRTst.h"
#include "PhlSramTst.h"
#include "SmuTst.h"


/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/

/* Master core Id */
#define SL_MASTER_CORE_ID (0U)


/* Maximum tests configured per group */
#define SL_MAX_TESTS_EARLY_PRE_RUN_GRP  (5U)
#define SL_MAX_TESTS_PRE_RUN_GRP        (10U)
#define SL_MAX_TESTS_RUN_TIME_GRP       (5U)
#define SL_MAX_TESTS_POST_RUN_GRP       (5U)


#define SL_CFG_COUNT (1U)

/* Maximum number of tests among arrays Th_MTLFuncList_Core<x> {x: Core id} */
#define TH_TOTAL_TESTS (29U)


/* Test index macros used to represent tests in array Th_MTLFuncList_Core0 */
#define  Core0_ClkmTst_ConfigSet0  (0U)
#define  Core0_VltmTst_ConfigSet0  (1U)
#define  Core0_TrapTst_ConfigSet0  (2U)
#define  Core0_SriTst_ConfigSet0  (3U)
#define  Core0_PmuEccEdcTst_ConfigSet0  (4U)
#define  Core0_CpuWdgTst_ConfigSet0  (5U)
#define  Core0_SafeWdgTst_ConfigSet0  (6U)
#define  Core0_CpuSbstTst_E_Core_ConfigSet0  (7U)
#define  Core0_LockStepTst_ConfigSet0  (10U)
#define  Core0_LockStepTst1_ConfigSet0  (11U)
#define  Core0_PflsahMonTst_ConfigSet0  (12U)
#define  Core0_CpuBusMpuLfmTst_ConfigSet0  (13U)
#define  Core0_CpuMpuTst_ConfigSet0  (14U)
#define  Core0_SpbTstPeripheral_ConfigSet0  (15U)
#define  Core0_SfrCmpTst_ConfigSet0  (16U)
#define  Core0_SfrCrcTst_ConfigSet0  (17U)
#define  Core0_RegAccProTst_ConfigSet0  (18U)
#define  Core0_SpbTstTimeout_ConfigSet0  (8U)
#define  Core0_SffTst_ConfigSet0  (9U)
#define  Core0_SramEccTst_ConfigSet0  (19U)
#define  Core0_DmaTst_CRCTst_ConfigSet0  (20U)
#define  Core0_DmaTst_TimestampTst_ConfigSet0  (21U)
#define  Core0_DmaTst_SafeLinkedListTst_ConfigSet0  (22U)
#define  Core0_IomTst_ConfigSet0  (23U)
#define  Core0_IRTst_ConfigSet0  (24U)
#define  Core0_PhlSramTst_ConfigSet0  (25U)
#define  Core0_SmuTst_IrqTst_ConfigSet0  (26U)
#define  Core0_SmuTst_RtTst_ConfigSet0  (27U)
#define  Core0_SmuTst_NmiTst_ConfigSet0  (28U)



#endif  /* TSTHANDLER_CFG_H */
