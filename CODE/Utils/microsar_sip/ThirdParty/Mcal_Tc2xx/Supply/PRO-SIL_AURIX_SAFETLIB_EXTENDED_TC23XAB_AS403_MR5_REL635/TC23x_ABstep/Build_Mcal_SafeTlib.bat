@ECHO OFF
REM ****************************************************************************
REM *                                                                         **
REM * Copyright (C) Infineon Technologies (2013)                              **
REM *                                                                         **
REM * All rights reserved.                                                    **
REM *                                                                         **
REM * This document contains proprietary information belonging to Infineon    **
REM * Technologies. Passing on and copying of this document, and communication**
REM * of its contents is not permitted without prior written authorization.   **
REM *                                                                         **
REM ****************************************************************************
REM *                                                                         **
REM *  FILENAME  : Build_Mcal_SafeTlib.bat                                    **
REM *                                                                         **
REM *                                                                         **
REM *  DATE      : 2013.--.--                                                 **
REM *                                                                         **
REM *  AUTHOR    : DL-AUTOSAR-Engineering                                     **
REM *                                                                         **
REM *  VENDOR    : Infineon Technologies                                      **
REM *                                                                         **
REM *  DESCRIPTION  : This file creates the DemoApplication                   **
REM *                                                                         **
REM *                                                                         **
REM *  MAY BE CHANGED BY USER [yes/no]: YES                                   **
REM *                                                                         **
REM ***************************************************************************/

@ECHO OFF

SET SAFETLIB_USED=STD_ON

REM ****************************************************************************
REM  Get MCAL Root Path 
REM ****************************************************************************
SET SSC_ROOT=%~dp0
SET SSC_ELK_ROOT=%~dp0
SET SSC_ROOT=%SSC_ROOT:~0,-1%
SET SSC_ROOT=%SSC_ROOT: =" "%
SET SSC_ROOT=%SSC_ROOT:\=/%
SET SSC_ROOT=%SSC_ROOT%/Aurix_MC-ISAR
SET SSC_ELK_ROOT=%SSC_ROOT%/demoapp/Selectable_bin

REM ****************************************************************************
REM  Get SAFETLIB Root Path 
REM ****************************************************************************
SET SAFETLIB_ROOT=%~dp0
SET SAFETLIB_ROOT=%SAFETLIB_ROOT:~0,-1%
SET SAFETLIB_ROOT=%SAFETLIB_ROOT: =" "%
SET SAFETLIB_ROOT=%SAFETLIB_ROOT:\=/%
SET SAFETLIB_ROOT=%SAFETLIB_ROOT%/01_SafeTlib


rem goto end
REM ****************************************************************************
REM  Get GNU Root Path 
REM ****************************************************************************
SET GNU_MAKE_PATH=%~dp0
SET GNU_MAKE_PATH=%GNU_MAKE_PATH:~0,-1%
SET GNU_MAKE_PATH=%GNU_MAKE_PATH%\GNU

REM ****************************************************************************
REM Choose between Selectable and loadable
REM POSTBUILD_SELECTABLE or POSTBUILD_LOADABLE
REM ***************************************************************************

SET APP_CONFIG=POSTBUILD_SELECTABLE

REM ****************************************************************************
REM  Set variables for integration
REM ****************************************************************************
SET DELIVERY_WITH_TEST=0
SET CODE_COVERAGE=0
SET CLEARCASE_RUN=0

REM ******Set controller to specific derivate
REM CONTROLLER can be set in the ModuleDelivery.bat
IF "%1"=="" goto param_error
IF "%2"=="" goto param_error
IF "%3"=="" goto param_error

SET SECOND_PARAM=%2
SET CONTROLLER_PLUGIN=%2
SET STL_TARGET=%SECOND_PARAM:~2,2%

SET CONTROLLER_PLUGIN=Aurix
REM ****************************************************************************
REM Second Parameter is Variant with step info
REM ****************************************************************************
IF %SECOND_PARAM% == TC27xBC SET CONTROLLER_PLUGIN=Aurix
IF %SECOND_PARAM% == TC27xCA SET CONTROLLER_PLUGIN=Aurix
IF %SECOND_PARAM% == TC27xDB SET CONTROLLER_PLUGIN=Aurix
IF %SECOND_PARAM% == TC26xBB SET CONTROLLER_PLUGIN=Aurix
IF %SECOND_PARAM% == TC29xBB SET CONTROLLER_PLUGIN=Aurix
IF %SECOND_PARAM% == TC23xAB SET CONTROLLER_PLUGIN=Aurix
IF %SECOND_PARAM% == TC22xAB SET CONTROLLER_PLUGIN=Aurix
IF %SECOND_PARAM% == TC21xAB SET CONTROLLER_PLUGIN=Aurix


SET DEVICE_STEP=%2

SET COMPILER_CHOICE=TASKING
SET COMPILER_TASKING_VER=V4
REM SET SET COMPILER=_tasking
IF NOT "%3" == "" SET COMPILER_CHOICE=%3
IF "%3" == "TASKING_V4" SET COMPILER_CHOICE=TASKING
IF "%3" == "TASKING_V4" SET COMPILER_CHOICE_TP=tasking
IF "%3" == "TASKING_V4" SET COMPILER=_tasking
IF "%3" == "TASKING_V4" SET COMPILER_PATH=C:/Program Files/TASKING/TriCore v4.2r2/ctc

IF "%3" == "TASKING_V5" SET COMPILER_CHOICE=TASKING
IF "%3" == "TASKING_V5" SET COMPILER_CHOICE_TP=tasking50r2
IF "%3" == "TASKING_V5" SET COMPILER_TASKING_VER=V5
IF "%3" == "TASKING_V5" SET COMPILER=_tasking
IF "%3" == "TASKING_V5" SET COMPILER_PATH=C:/Program Files/TASKING/TriCore v5.0r2/ctc

IF "%3" == "TASKING_V5P3" SET COMPILER_CHOICE=TASKING
IF "%3" == "TASKING_V5P3" SET COMPILER_CHOICE_TP=tasking50r2p3
IF "%3" == "TASKING_V5P3" SET COMPILER_TASKING_VER=V5
IF "%3" == "TASKING_V5P3" SET COMPILER=_tasking
IF "%3" == "TASKING_V5P3" SET COMPILER_PATH=C:/Program Files/TASKING/TriCore v5.0r2p3/ctc

IF "%3" == "GNU" SET COMPILER_CHOICE=GNU
IF "%3" == "GNU" SET COMPILER_CHOICE_TP=gnu
IF "%3" == "GNU" SET COMPILER=

IF "%3" == "WINDRIVER" SET COMPILER=_windrive
IF "%3" == "WINDRIVER" SET COMPILER_CHOICE_TP=windriver
IF "%3" == "WINDRIVER" SET COMPILER_CHOICE=WINDRIVER


REM ****************************************************************************
REM 4th parameter is C99/C90. Default C Standard is C90
REM ****************************************************************************
SET C_STANDARD=C90
IF "%4" == "C99" SET C_STANDARD=C99

SET OPTIONAL_OPTIMIZATION=N
IF "%5"=="opt" SET OPTIONAL_OPTIMIZATION=Y

REM ****************************************************************************
REM Copying of common files from Safetlib to Mcal on a conditional basis
REM ****************************************************************************

for /f %%i in ('%GNU_MAKE_PATH%\cksum %SAFETLIB_ROOT%\Common\Mcal_TcLib.c') do set VAR1=%%i

for /f %%i in ('%GNU_MAKE_PATH%\cksum %SSC_ROOT%\tricore_general\ssc\src\Mcal_TcLib.c') do set VAR2=%%i

IF %VAR1%==%VAR2% goto no

@echo "Do you want to copy differed Common files from SafeTlib to Mcal ?(y/n)"

SET INPUT1=
SET /P  INPUT1=Type input: %=%
IF %INPUT1% == y goto yes
IF %INPUT1% == n goto no
IF %INPUT1% == Y goto yes
IF %INPUT1% == N goto no

:yes

COPY .\01_SafeTlib\Application\src\Irq.c .\Aurix_MC-ISAR\irq_infineon_tricore\ssc\src
COPY .\01_SafeTlib\Application\src\Mcal.c .\Aurix_MC-ISAR\tricore_general\ssc\src
COPY .\01_SafeTlib\Application\src\Mcal_Trap.c .\Aurix_MC-ISAR\irq_infineon_tricore\ssc\src
COPY .\01_SafeTlib\Application\src\Stm_Irq.c .\Aurix_MC-ISAR\irq_infineon_tricore\ssc\src
COPY .\01_SafeTlib\Application\src\Test_Print.c .\Aurix_MC-ISAR\integration_general\src
COPY .\01_SafeTlib\Application\src\Test_Time.c .\Aurix_MC-ISAR\integration_general\src
COPY .\01_SafeTlib\Application\src\_mcal_pjt.dld .\Aurix_MC-ISAR\demoapp\Selectable_bin
COPY .\01_SafeTlib\Application\src\_mcal_pjt.ld .\Aurix_MC-ISAR\demoapp\Selectable_bin
COPY .\01_SafeTlib\Application\src\_mcal_pjt.lsl .\Aurix_MC-ISAR\demoapp\Selectable_bin
COPY .\01_SafeTlib\Application\inc\Irq.h .\Aurix_MC-ISAR\irq_infineon_tricore\ssc\inc
COPY .\01_SafeTlib\Application\inc\Test_Print.h .\Aurix_MC-ISAR\integration_general\inc
COPY .\01_SafeTlib\Application\inc\Test_Time.h .\Aurix_MC-ISAR\integration_general\inc

COPY .\01_SafeTlib\UpperLayer\SafeWdgM\src\Spi_Irq.c .\Aurix_MC-ISAR\irq_infineon_tricore\ssc\src

COPY .\01_SafeTlib\Common\Mcal.h .\Aurix_MC-ISAR\tricore_general\ssc\inc
COPY .\01_SafeTlib\Common\Compiler.h .\Aurix_MC-ISAR\general\tricore\compiler
COPY .\01_SafeTlib\Common\Compiler_Cfg.h .\Aurix_MC-ISAR\general\tricore\compiler
COPY .\01_SafeTlib\Common\Mcal_Compiler.h .\Aurix_MC-ISAR\tricore_general\ssc\inc
COPY .\01_SafeTlib\Common\Mcal_TcLib.h .\Aurix_MC-ISAR\tricore_general\ssc\inc
COPY .\01_SafeTlib\Common\Mcal_WdgLib.h .\Aurix_MC-ISAR\tricore_general\ssc\inc
COPY .\01_SafeTlib\Common\Mcal_TcLib.c .\Aurix_MC-ISAR\tricore_general\ssc\src
COPY .\01_SafeTlib\Common\Mcal_WdgLib.c .\Aurix_MC-ISAR\tricore_general\ssc\src
COPY .\01_SafeTlib\Common\Platform_Types.h .\Aurix_MC-ISAR\general\tricore\inc
COPY .\01_SafeTlib\Common\Std_Types.h .\Aurix_MC-ISAR\general\inc



REM *********************************END of common file copying***********************************************

:no
REM ****************************************************************************
REM Update Mcal_Options.h respectively to STD_ON for IFX_MCAL_USED/IFX_SAFETLIB_USED
REM ****************************************************************************
echo.%SSC_ROOT%|findstr /C:"aimmc_sw_aurix" >nul 2>&1
if not errorlevel 1 (
   SET CLEARCASE_RUN=1
)

set INTEXTFILE="%SAFETLIB_ROOT%\Common\Mcal_Options.h"
IF "%CLEARCASE_RUN%" == "1" (
  "cleartool.exe" co -unr -c "For Integration" %INTEXTFILE%
 )
IF "%CLEARCASE_RUN%" == "0" (
  attrib -r %INTEXTFILE%
 ) 
set OUTTEXTFILE="%SAFETLIB_ROOT%\Common\Mcal_Options_Temp.h"
set SEARCHTEXT=STD_OFF
set REPLACETEXT=STD_ON
set OUTPUTLINE=

for /f "tokens=1,* delims=" %%A in ( '"type %INTEXTFILE%"' ) do (
SET "string=%%A"
setlocal enabledelayedexpansion
SET modified=!string:%SEARCHTEXT%=%REPLACETEXT%!
  If NOT "!modified!"=="!modified:IFX_MCAL_USED=!" (
      echo !modified!>>%OUTTEXTFILE%
  ) else (
   SET modified=!string:%SEARCHTEXT%=%SEARCHTEXT%!
   echo !modified!>>%OUTTEXTFILE%
  )
endlocal
)




move /Y "%OUTTEXTFILE%" "%INTEXTFILE%"

set INTEXTFILE="%SSC_ROOT%\tricore_general\ssc\inc\Mcal_Options.h"
IF "%CLEARCASE_RUN%" == "1" (
  cleartool co -unr -c "For Integration" %INTEXTFILE%
 )
IF "%CLEARCASE_RUN%" == "0" (
  attrib -r %INTEXTFILE%
 ) 
set OUTTEXTFILE="%SSC_ROOT%\tricore_general\ssc\inc\Mcal_Options_Temp.h"
for /f "tokens=1,* delims=" %%A in ( '"type %INTEXTFILE%"' ) do (
SET "string=%%A"
setlocal enabledelayedexpansion
  SET modified=!string:%SEARCHTEXT%=%REPLACETEXT%!
   If NOT "!modified!"=="!modified:IFX_SAFETLIB_USED=!" (
      echo !modified!>>%OUTTEXTFILE%
  ) else (
   SET modified=!string:%SEARCHTEXT%=%SEARCHTEXT%!
  echo !modified!>>%OUTTEXTFILE%
  )  
endlocal
)
move /Y "%OUTTEXTFILE%" "%INTEXTFILE%"


REM ****************************************************************************
REM  Demo option with/without config package 
REM ****************************************************************************

SET TRESOS_PATH=%1

IF NOT EXIST %SSC_ROOT%\demoapp\cfg goto error_no_cfg_pkg_installed
IF NOT EXIST %TRESOS_PATH% goto error_no_tresos_folder

pushd .
cd %SSC_ROOT%\demoapp\Selectable_bin
@echo | CALL %SSC_ROOT%\demoapp\Selectable_bin\DemoAppWipe.bat

REM ****************************************************************************
REM  Generate Module Configuration Files by calling the Code Generator 
REM ****************************************************************************
ECHO STEP1: ................Generate Configuration Files............................

CALL %TRESOS_PATH%\tresos_cmd.bat -data %SSC_ROOT%\demoapp\cfg\tresos_workspace\Selectable_Cfg generate DemoApp
CALL %SSC_ROOT%\demoapp\Selectable_bin\MCAL_OpFileUpdate.bat
popd
REM Safetlib standalone pre_processor switch
SET SAFETLIB_STANDALONE=FALSE

REM Makefile path 
SET MAKEFILE_PATH=./gnumake

REM Common directory path
SET COMMON=./Common

REM Application Directory
SET APPL_BASE_DIR=./Application

REM SafeTlibCD Directory
SET SAFETLIBCD_BASE_DIR=./SafeTlibCD

REM SafeWdgCD Directory
SET SAFEWDG_BASE_DIR=./SafeWdgCD

REM Folder containing configuration files
SET CONFIG_DIR=./Cfg

SET REFAPP_CORE=0

SET REFAPP_WDG=_REFAPP_INTALIVEWDG_
IF "%5"=="EXT" SET REFAPP_WDG=_REFAPP_EXTCICWDG_

IF "%3" == "GNU" goto gnu_safetbuild
IF "%3" == "WINDRIVER" goto windriver_safetbuild
REM *****************************************************************************************************
REM Clean  build of SafeTlib
REM *****************************************************************************************************

REM Compiler Path
REM SafeTlibCD Library Directory
IF "%3" == "TASKING_V4" SET SAFETLIBCD_LIB=%APPL_BASE_DIR%/debug/TC%STL_TARGET%x/tasking
IF "%3" == "TASKING_V5" SET SAFETLIBCD_LIB=%APPL_BASE_DIR%/debug/TC%STL_TARGET%x/tasking50r2
IF "%3" == "TASKING_V5P3" SET SAFETLIBCD_LIB=%APPL_BASE_DIR%/debug/TC%STL_TARGET%x/tasking50r2p3

REM SafeWdgCD Library Directory
IF "%3" == "TASKING_V4" SET SAFEWDG_LIB=%APPL_BASE_DIR%/debug/TC%STL_TARGET%x/tasking
IF "%3" == "TASKING_V5" SET SAFEWDG_LIB=%APPL_BASE_DIR%/debug/TC%STL_TARGET%x/tasking50r2
IF "%3" == "TASKING_V5P3" SET SAFEWDG_LIB=%APPL_BASE_DIR%/debug/TC%STL_TARGET%x/tasking50r2p3

pushd .
cd %SAFETLIB_ROOT%
SET BASE_PATH=%CD%
REM *********** Clean SafeTlib Tasking*****************************
@echo "***********  Clean: SafeTlibCD library  ************"
IF "%3" == "TASKING_V4" IF EXIST %SAFETLIBCD_BASE_DIR%\SafeTlibCD_Cfg.mak "%MAKEFILE_PATH%/gnumake" -f "./SafeTlibCD/SafeTlibCD_tasking.mak" clean
IF "%3" == "TASKING_V4" IF NOT EXIST %SAFETLIBCD_BASE_DIR%\SafeTlibCD_Cfg.mak DEL /Q /F /S .\Application\debug\TC%STL_TARGET%x\tasking\*.o .\Application\debug\TC%STL_TARGET%x\tasking\*.err .\Application\debug\TC%STL_TARGET%x\tasking\*.src .\Application\debug\TC%STL_TARGET%x\tasking\*.ers .\Application\debug\TC%STL_TARGET%x\tasking\*.s .\Application\debug\TC%STL_TARGET%x\tasking\*.i .\Application\debug\TC%STL_TARGET%x\tasking\*.ifx .\Application\debug\TC%STL_TARGET%x\tasking\*.elk .\Application\debug\TC%STL_TARGET%x\tasking\*.elf .\Application\debug\TC%STL_TARGET%x\tasking\*.hex .\Application\debug\TC%STL_TARGET%x\tasking\*.map > nul

IF "%3" == "TASKING_V5" IF EXIST %SAFETLIBCD_BASE_DIR%\SafeTlibCD_Cfg.mak "%MAKEFILE_PATH%/gnumake" -f "./SafeTlibCD/SafeTlibCD_tasking.mak" clean
IF "%3" == "TASKING_V5" IF NOT EXIST %SAFETLIBCD_BASE_DIR%\SafeTlibCD_Cfg.mak DEL /Q /F /S .\Application\debug\TC%STL_TARGET%x\tasking50r2\*.o .\Application\debug\TC%STL_TARGET%x\tasking50r2\*.err .\Application\debug\TC%STL_TARGET%x\tasking50r2\*.src .\Application\debug\TC%STL_TARGET%x\tasking50r2\*.ers .\Application\debug\TC%STL_TARGET%x\tasking50r2\*.s .\Application\debug\TC%STL_TARGET%x\tasking50r2\*.i .\Application\debug\TC%STL_TARGET%x\tasking50r2\*.ifx .\Application\debug\TC%STL_TARGET%x\tasking50r2\*.elk .\Application\debug\TC%STL_TARGET%x\tasking50r2\*.elf .\Application\debug\TC%STL_TARGET%x\tasking50r2\*.hex .\Application\debug\TC%STL_TARGET%x\tasking50r2\*.map > nul

IF "%3" == "TASKING_V5P3" IF EXIST %SAFETLIBCD_BASE_DIR%\SafeTlibCD_Cfg.mak "%MAKEFILE_PATH%/gnumake" -f "./SafeTlibCD/SafeTlibCD_tasking.mak" clean
IF "%3" == "TASKING_V5P3" IF NOT EXIST %SAFETLIBCD_BASE_DIR%\SafeTlibCD_Cfg.mak DEL /Q /F /S .\Application\debug\TC%STL_TARGET%x\tasking50r2p3\*.o .\Application\debug\TC%STL_TARGET%x\tasking50r2p3\*.err .\Application\debug\TC%STL_TARGET%x\tasking50r2p3\*.src .\Application\debug\TC%STL_TARGET%x\tasking50r2p3\*.ers .\Application\debug\TC%STL_TARGET%x\tasking50r2p3\*.s .\Application\debug\TC%STL_TARGET%x\tasking50r2p3\*.i .\Application\debug\TC%STL_TARGET%x\tasking50r2p3\*.ifx .\Application\debug\TC%STL_TARGET%x\tasking50r2p3\*.elk .\Application\debug\TC%STL_TARGET%x\tasking50r2p3\*.elf .\Application\debug\TC%STL_TARGET%x\tasking50r2p3\*.hex .\Application\debug\TC%STL_TARGET%x\tasking50r2p3\*.map > nul


@echo "***********  Clean: SafeWdgCD library  ************"
IF EXIST %SAFEWDG_BASE_DIR%/SafeWdgCD_Cfg.mak "%MAKEFILE_PATH%/gnumake" -f "./SafeWdgCD/SafeWdgCD_tasking.mak" clean

@echo "***********  Clean: Reference Application  ************"
"%MAKEFILE_PATH%/gnumake" -f "./Application/Application_tasking.mak" clean

REM *********** Build SafeTlib Tasking*****************************
@echo "***********  Build: SafeTlibCD library  ************"
"%MAKEFILE_PATH%/gnumake" -f "./SafeTlibCD/SafeTlibCD_tasking.mak"

@echo "***********  Build: SafeWdgCD library  ************"
"%MAKEFILE_PATH%/gnumake" -f "./SafeWdgCD/SafeWdgCD_tasking.mak"

@echo "***********  Build: Reference Application  ************"
"%MAKEFILE_PATH%/gnumake" -f "./Application/Application_tasking.mak"

popd
if NOT %errorlevel% == 0 Goto demo_build_failed

goto demoapp_build

:gnu_safetbuild

REM GNU compiler path
SET COMPILER_PATH=C:/HIGHTEC/toolchains/tricore/v4.6.3.0

REM SafeTlibCD Library Directory
SET SAFETLIBCD_LIB=%APPL_BASE_DIR%/debug/TC%STL_TARGET%x/gnu

REM SafeWdgCD Library Directory
SET SAFEWDG_LIB=%APPL_BASE_DIR%/debug/TC%STL_TARGET%x/gnu

SET IMPLEMENTATION=.
pushd .

cd %SAFETLIB_ROOT%
SET BASE_PATH=%CD%

REM ************Clean SafeTlib GNU******************************
@echo "***********  Clean: SafeTlibCD library  ************"
IF EXIST %SAFETLIBCD_BASE_DIR%\SafeTlibCD_Cfg.mak "%MAKEFILE_PATH%/gnumake" -f "./SafeTlibCD/SafeTlibCD.mak" clean
IF NOT EXIST %SAFETLIBCD_BASE_DIR%\SafeTlibCD_Cfg.mak DEL /Q /F /S .\Application\debug\TC%STL_TARGET%x\gnu\*.o .\Application\debug\TC%STL_TARGET%x\gnu\*.err .\Application\debug\TC%STL_TARGET%x\gnu\*.src .\Application\debug\TC%STL_TARGET%x\gnu\*.ers .\Application\debug\TC%STL_TARGET%x\gnu\*.s .\Application\debug\TC%STL_TARGET%x\gnu\*.i .\Application\debug\TC%STL_TARGET%x\gnu\*.ifx .\Application\debug\TC%STL_TARGET%x\gnu\*.elk .\Application\debug\TC%STL_TARGET%x\gnu\*.elf .\Application\debug\TC%STL_TARGET%x\gnu\*.hex .\Application\debug\TC%STL_TARGET%x\gnu\*.map > nul

@echo "***********  Clean: SafeWdgCD library  ************"
IF EXIST %SAFEWDG_BASE_DIR%/SafeWdgCD_Cfg.mak "%MAKEFILE_PATH%/gnumake" -f "./SafeWdgCD/SafeWdgCD.mak" clean

@echo "***********  Clean: Reference Application  ************"
"%MAKEFILE_PATH%/gnumake" -f "./Application/Application.mak" clean

REM *********** Build SafeTlib GNU*****************************
@echo "***********  Build: SafeTlibCD library  ************"
"%MAKEFILE_PATH%/gnumake" -f "./SafeTlibCD/SafeTlibCD.mak" -k

@echo "***********  Build: SafeWdgCD library  ************"
"%MAKEFILE_PATH%/gnumake" -f "./SafeWdgCD/SafeWdgCD.mak" -k

@echo "***********  Build: Reference Application  ************"
"%MAKEFILE_PATH%/gnumake" -f "./Application/Application.mak"
popd
if NOT %errorlevel% == 0 Goto demo_build_failed

goto demoapp_build

:windriver_safetbuild

REM WINDRIVER compiler path
SET COMPILER_PATH=C:\WindRiver\diab\5.9.4.8

REM SafeTlibCD Library Directory
SET SAFETLIBCD_LIB=%APPL_BASE_DIR%/debug/TC%STL_TARGET%x/windriver

REM SafeWdgCD Library Directory
SET SAFEWDG_LIB=%APPL_BASE_DIR%/debug/TC%STL_TARGET%x/windriver

pushd .

cd %SAFETLIB_ROOT%
SET BASE_PATH=%CD%

REM ************Clean SafeTlib WINDRIVER******************************
@echo "***********  Clean: SafeTlibCD library  ************"
"%MAKEFILE_PATH%/gnumake" -f "./SafeTlibCD/SafeTlibCD_windriver.mak" clean

@echo "***********  Clean: SafeWdgCD library  ************"
"%MAKEFILE_PATH%/gnumake" -f "./SafeWdgCD/SafeWdgCD_windriver.mak" clean

@echo "***********  Clean: Reference Application  ************"
"%MAKEFILE_PATH%/gnumake" -f "./Application/Application_windriver.mak" clean

REM *********** Build SafeTlib WINDRIVER*****************************
@echo "***********  Build: SafeTlibCD library  ************"
"%MAKEFILE_PATH%/gnumake" -f "./SafeTlibCD/SafeTlibCD_windriver.mak" -k

@echo "***********  Build: SafeWdgCD library  ************"
"%MAKEFILE_PATH%/gnumake" -f "./SafeWdgCD/SafeWdgCD_windriver.mak" -k

@echo "***********  Build: Reference Application  ************"
"%MAKEFILE_PATH%/gnumake" -f "./Application/Application_windriver.mak"

popd
if NOT %errorlevel% == 0 Goto demo_build_failed

REM ***************************************************************************
REM Mcal DemoApp build starts here
REM ***************************************************************************

:demoapp_build
REM *Set the environment variables as per module delivery 
CALL %SSC_ROOT%\demoapp\Selectable_bin\ModuleDelivery.bat

REM ****************************************************************************
REM  Build Demo Application
REM ****************************************************************************
pushd .
cd %SSC_ROOT%\demoapp\Selectable_bin

ECHO STEP2: Build Demo Application

IF %FR_DELIVERY%==STD_ON goto build_flexray 
@"%GNU_MAKE_PATH%\gnumake.exe" -f %SSC_ROOT%\mak\demoapp\ssc\mak\DemoApp.mak

popd
if NOT %errorlevel% == 0 Goto demo_build_failed
if %errorlevel% == 0 Goto demo_build_complete
 
:build_flexray
REM Run FlexTrain 
%SSC_ROOT%\fr_17_eray_infineon_tricore\tool\FlexTrain.exe %SSC_ROOT%\fr_17_eray_infineon_tricore\cfg1\src\Fr_17_Eray_PBCfg.c
if NOT %errorlevel% == 0 Goto FrExitCodeIsOne
if %errorlevel% == 0 Goto FrExitCodeIsZero
:FrExitCodeIsZero
ECHO FlexTrain executed successfully.
REM Build demo application for FlexRay node 0
SET FR_17_ERAY_DELIVERY_SWITCH=0
IF EXIST %SSC_ROOT%\fr_17_eray_infineon_tricore\demo\obj\Fr_17_Eray_Demo.o del /F "%SSC_ROOT%"\fr_17_eray_infineon_tricore\demo\obj\Fr_17_Eray_Demo.o
IF EXIST %SSC_ROOT%\demoapp\obj\DemoApp.o del /F "%SSC_ROOT%"\demoapp\obj\DemoApp.o

@"%GNU_MAKE_PATH%\gnumake.exe" -f %SSC_ROOT%\mak\demoapp\ssc\mak\DemoApp.mak
move /Y ..\out\DemoAppSelectable.elf ..\out\DemoAppSelectable_node0.elf
if NOT %errorlevel% == 0 Goto demo_build_failed

REM Build demo application for FlexRay node 1
SET FR_17_ERAY_DELIVERY_SWITCH=1

del /F "%SSC_ROOT%"\fr_17_eray_infineon_tricore\demo\obj\Fr_17_Eray_Demo.o
del /F "%SSC_ROOT%"\demoapp\obj\DemoApp.o

@"%GNU_MAKE_PATH%\gnumake.exe" -f %SSC_ROOT%\mak\demoapp\ssc\mak\DemoApp.mak
move /Y ..\out\DemoAppSelectable.elf ..\out\DemoAppSelectable_node1.elf
if NOT %errorlevel% == 0 Goto demo_build_failed
if %errorlevel% == 0 Goto demo_build_complete
 
 :demo_build_failed
REM ****************************************************************************
REM  Demo Application failed
REM ****************************************************************************
ECHO Error: Demo build is Failed.
goto end

:demo_build_complete

REM ****************************************************************************
REM  Restoring the PATH variable
REM ****************************************************************************

ECHO Demo build Successfully.
goto end

REM ****************************************************************************
:error_no_cfg_pkg_installed
ECHO Error: Configuration package is not installed. You can use NOCFG option only.
goto end

:build_with_no_cfg_pkg

IF NOT EXIST %SSC_ROOT%\mcu_infineon_tricore goto error_no_basic_pkg_installed
IF NOT %DRIVER_NAME%==BASE IF NOT EXIST %SSC_ROOT%\%DRIVER_NAME%_infineon_tricore goto error_no_driver_pkg_installed
IF %DRIVER_NAME%==FLS goto error_no_fls_driver_demo

CALL DemoAppNoCfgSelectable.bat
goto build_common_stuff

:error_no_basic_pkg_installed
ECHO Error: Basic package is not installed. 
goto end

:error_no_driver_pkg_installed
ECHO Error: %DRIVER_NAME% driver package is not installed. 
goto end

:error_no_fls_driver_demo
echo "No FLS driver demo supported."
goto end

REM -yg :ExitCodeIsOne
REM -ygECHO mTrain Not Executed Successfully
REM -yg goto end

:FrExitCodeIsOne
ECHO FlexTrain execution failed.
goto end

:param_error
ECHO "Usage: Build_Mcal_SafetLib.bat <Tresos installation path> <Target Derivative Compiler> <Safety Internal or External watchdog>"
ECHO Target Derivative supported : TC27x TC26x TC29x TC23x
ECHO Compiler supported : TASKING GNU WINDRIVER
ECHO Optional option for Safety Internal or External watchdog supported : INT  EXT
REM ****************************************************************************
:end
PAUSE