#******************************************************************************* 
#                                                                             ** 
#* Copyright (C) Infineon Technologies (2014)                                 **
#*                                                                            ** 
#* All rights reserved.                                                       ** 
#*                                                                            **
#* This document contains proprietary information belonging to Infineon       ** 
#* Technologies. Passing on and copying of this document, and communication   ** 
#* of its contents is not permitted without prior written authorization.      **
#*                                                                            ** 
#******************************************************************************* 
#*                                                                            ** 
#*  $FILENAME   : SafeTlibCD_Cfg.mak $                                       **
#*                                                                            **
#*  $CC VERSION : \main\15 $                                                 **
#*                                                                            **
#*  $DATE       : 2015-07-22 $                                               **
#*                                                                            ** 
#*  AUTHOR    : SafeTlib Team                                                 **
#*                                                                            ** 
#*  VENDOR    : Infineon Technologies                                         ** 
#*                                                                            ** 
#*  DESCRIPTION  : This file defines safety lib CD modules delivery ON/OFF    ** 
#*         - Based on this delivery type values modules will be compiled      **
#*                                                                            ** 
#*  MAY BE CHANGED BY USER [yes/no]: yes                                      ** 
#*                                                                            ** 
#******************************************************************************/ 

TSTHANDLER_DELIVERY = ON


CLKMTST_DELIVERY = ON
VLTMTST_DELIVERY = ON
TRAPTST_DELIVERY = ON
SRITST_DELIVERY = ON
PMUECCEDCTST_DELIVERY = ON
WDGTST_DELIVERY = ON
CPUSBSTTST_DELIVERY = ON
LOCKSTEPTST_DELIVERY = ON
CPUBUSMPULFMTST_DELIVERY = ON
CPUMPUTST_DELIVERY = ON
SPBTST_DELIVERY = ON
SFRTST_DELIVERY = ON
REGACCPROTTST_DELIVERY = ON
SFFTST_DELIVERY = ON
SRAMECCTST_DELIVERY = ON
DMATST_DELIVERY = ON
IOMTST_DELIVERY = ON
IRTST_DELIVERY = ON
PHLSRAMTST_DELIVERY = ON
SMUTST_DELIVERY = ON