<?xml version='1.0'?>
<AUTOSAR xmlns="http://autosar.org/schema/r4.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://autosar.org/schema/r4.0 AUTOSAR_4-0-3.xsd">
  <AR-PACKAGES>
    <AR-PACKAGE UUID="93d9a111-9d4c-47b6-98cb-fe5aec24c8b0">
      <SHORT-NAME>AURIX</SHORT-NAME>
      <ELEMENTS>
        <ECUC-MODULE-DEF UUID="03eaa8c5-137e-402e-823d-8d7776c1de4d">
          <SHORT-NAME>SafeWdgAscLin</SHORT-NAME>
          <SUPPORTED-CONFIG-VARIANTS>
            <SUPPORTED-CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</SUPPORTED-CONFIG-VARIANT>
            <SUPPORTED-CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</SUPPORTED-CONFIG-VARIANT>
          </SUPPORTED-CONFIG-VARIANTS>
          <CONTAINERS>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ccda81dc-46cf-4171-858b-7c947884702f">
              <SHORT-NAME>SafeWdgAscLinConfigSet</SHORT-NAME>
              <DESC>
                <L-2 L="FOR-ALL">This is the base container that contains the link time configuration parameters</L-2>
              </DESC>
              <MULTIPLE-CONFIGURATION-CONTAINER>true</MULTIPLE-CONFIGURATION-CONTAINER>
              <PARAMETERS>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce928e-6ed1-5968-93b2-34d30b1aa49f">
                  <SHORT-NAME>SafeWdgAscLinAscLinModule</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Specifies the used ASCLIN module for this driver</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>1</DEFAULT-VALUE>
                  <MAX>1</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce928e-6ed1-5978-93b2-34d30b1aa50f">
                  <SHORT-NAME>SafeWdgAscLinDmaTxChannel</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Specifies the DMA channel, DM channel to be used for transmit</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>14</DEFAULT-VALUE>
                  <MAX>15</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce828e-6ed1-5978-93b3-34d30b1aa51f">
                  <SHORT-NAME>SafeWdgAscLinBConOversample</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Oversampling factor for baudrate</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>7</DEFAULT-VALUE>
                  <MAX>16</MAX>
                  <MIN>4</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce828e-6ed1-5978-93b3-44d30b1aa52f">
                  <SHORT-NAME>SafeWdgAscLinBConSamplePoint</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Bit sampling point within oversampling range</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>4</DEFAULT-VALUE>
                  <MAX>15</MAX>
                  <MIN>1</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5978-93b3-44d30b1aa53f">
                  <SHORT-NAME>SafeWdgAscLinBConPreScaler</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Baudrate PreScaler</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>1</DEFAULT-VALUE>
                  <MAX>4096</MAX>
                  <MIN>1</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce827e-6ed1-5958-93b3-44d30b1aa54f">
                  <SHORT-NAME>SafeWdgAscLinBrgDenominator</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Baudrate Denominator</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>49</DEFAULT-VALUE>
                  <MAX>4095</MAX>
                  <MIN>1</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="92ce827e-6ed1-5978-93b3-44d30b1aa55f">
                  <SHORT-NAME>SafeWdgAscLinBrgNumerator</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Baudrate Numerator</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>8</DEFAULT-VALUE>
                  <MAX>4095</MAX>
                  <MIN>1</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="42ce827e-6ed1-5978-93b3-44d30b1aa56f">
                  <SHORT-NAME>SafeWdgAscLinFConIdleCnt</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Idle cycles between two transmissions</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>7</DEFAULT-VALUE>
                  <MAX>7</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="40ce827e-6ed1-5978-93b3-44d30b1aa57f">
                  <SHORT-NAME>SafeWdgAscLinFConTrailCnt</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Trail cycles after transmissions</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>1</DEFAULT-VALUE>
                  <MAX>7</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="42ce827e-6ed1-5878-93b3-44d30b1aa58f">
                  <SHORT-NAME>SafeWdgAscLinFConLeadCnt</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Defines the leading delay before transmission</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>3</DEFAULT-VALUE>
                  <MAX>7</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="42ce827e-6ed0-5978-93b3-44d30b1aa59f">
                  <SHORT-NAME>SafeWdgAscLinFConMsb</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">MSB first - 0: no, 1: yes</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>1</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="42ce827e-6ec0-5978-93b3-44d30b1aa60f">
                  <SHORT-NAME>SafeWdgAscLinCsPolarity</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">slave CS polarity - 0: high active, 1: low active</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>1</DEFAULT-VALUE>
                  <MAX>1</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="42ce827e-6ec0-5978-93b2-44d30b1aa61f">
                  <SHORT-NAME>SafeWdgAscLinRxInputSelect</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">receiver port input selector</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>4</DEFAULT-VALUE>
                  <MAX>7</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-FUNCTION-NAME-DEF UUID="98ce928e-7ed1-4368-95b2-21d30b1fa62f">
                  <SHORT-NAME>SafeWdgAscLinEnIfmDlayTmrFunc</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter represents the function, which can be used to enable configured GTM TOM channel. It is used by this driver to enable data transfer to external CIC watchdog</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <ECUC-FUNCTION-NAME-DEF-VARIANTS>
                    <ECUC-FUNCTION-NAME-DEF-CONDITIONAL>
                      <DEFAULT-VALUE>SafeWdgExtCic_Helper_EnableTOMChOSM</DEFAULT-VALUE>
                    </ECUC-FUNCTION-NAME-DEF-CONDITIONAL>
                  </ECUC-FUNCTION-NAME-DEF-VARIANTS>
                </ECUC-FUNCTION-NAME-DEF>
                <ECUC-FUNCTION-NAME-DEF UUID="98ce928e-7ed1-4368-95b2-21d30b1fa63f">
                  <SHORT-NAME>SafeWdgAscLinDisIfmDlayTmrFunc</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter represents the function, which can be used by this driver to disable configured GTM TOM channel. It is used when data transfer to external CIC watchdog is complete.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <ECUC-FUNCTION-NAME-DEF-VARIANTS>
                    <ECUC-FUNCTION-NAME-DEF-CONDITIONAL>
                      <DEFAULT-VALUE>SafeWdgExtCic_Helper_DisableTOMChOSM</DEFAULT-VALUE>
                    </ECUC-FUNCTION-NAME-DEF-CONDITIONAL>
                  </ECUC-FUNCTION-NAME-DEF-VARIANTS>
                </ECUC-FUNCTION-NAME-DEF>
              </PARAMETERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
          </CONTAINERS>
        </ECUC-MODULE-DEF>
      </ELEMENTS>
    </AR-PACKAGE>
  </AR-PACKAGES>
</AUTOSAR>
