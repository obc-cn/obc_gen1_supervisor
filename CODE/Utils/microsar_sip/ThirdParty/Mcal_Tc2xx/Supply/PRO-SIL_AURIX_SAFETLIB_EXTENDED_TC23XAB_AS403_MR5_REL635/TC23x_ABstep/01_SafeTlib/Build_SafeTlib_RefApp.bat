@ ECHO OFF
REM /**
REM \verbatim
REM ***************************************************************************
REM * Copyright (C) Infineon Technologies (2013), All rights reserved. 
REM ***************************************************************************
REM * This document contains proprietary information belonging to Infineon
REM * Technologies. Passing on and copying of this document, and communication
REM * of its contents is not permitted without prior written authorization.
REM ***************************************************************************
REM ***************************************************************************
REM * FILENAME    : Build_SafeTlib_RefApp.bat
REM * VERSION     : 1.2.1
REM * DATE        : 2015-11-27
REM * VARIANT     : Aurix HE and EP all 
REM * PLATFORM    : Infineon AURIX
REM * COMPILER    : Tasking, HighTec GNU, WindRiver 
REM * AUTHOR      : SafeTlib Team
REM * VENDOR      : Infineon Technologies 
REM * TRACEABILITY: 
REM * DESCRIPTION : DOS Batch file to build SafeTlib reference application
REM * 
REM * MAY BE CHANGED BY USER [Yes/No]: No 
REM *                                     
REM ***************************************************************************
REM * 
REM ***************************************************************************
REM                     Revision Control History  
REM ***************************************************************************
REM 
REM V1.2.1: 2014-01-22: Tasking, HighTec GNU, WindRiver build with paramter 3.
REM                     Tasking 5.0r2 support for safetlib package. 
REM V1.2.0: 2014-01-22: Redefined parameter 3 to be tresos workspace name
REM V1.1.0: 2014-01-16: Added step for configuration generation
REM V1.0.0: 2014-01-15: Added parameters 5, 6 and 7
REM V0.0.4: 2013-10-11: Added support for different Aurix variants
REM V0.0.3: 2013-07-24: Added conditional compile sets for diff. watchdog types 
REM V0.0.2: 2013-05-28: Added support for core 1
REM V0.0.3: 2017-11-07: Windriver 5.9.6.3 support added
REM V0.0.4: 2017-11-28: Tasking v6.2r1p3 support added
REM ***************************************************************************
REM * 
REM * 
REM ====================== User configuration Start ===========================

%~d0
CD %~dp0
SET BASE_PATH=%~dp0

SET COMPILER_CHOICE_TP=tasking
IF NOT "%3" == "" SET COMPILER_CHOICE_TP=%3

REM Tasking Compiler choice and Compiler path
IF "%3" == "tasking" SET COMPILER_CHOICE_TP=tasking
IF "%3" == "tasking" SET COMPILER=_tasking
IF "%3" == "tasking" SET COMPILER_PATH=C:/Program Files/TASKING/TriCore v4.2r2/ctc

IF "%3" == "tasking50r2" SET COMPILER_CHOICE_TP=tasking50r2
IF "%3" == "tasking50r2" SET COMPILER=_tasking
IF "%3" == "tasking50r2" SET COMPILER_PATH=C:/Program Files/TASKING/TriCore v5.0r2/ctc

IF "%3" == "tasking50r2p3" SET COMPILER_CHOICE_TP=tasking50r2p3
IF "%3" == "tasking50r2p3" SET COMPILER=_tasking
IF "%3" == "tasking50r2p3" SET COMPILER_PATH=C:/Program Files/TASKING/TriCore v5.0r2p3/ctc
IF "%3" == "tasking62r1p3" SET COMPILER_CHOICE_TP=tasking62r1p3
IF "%3" == "tasking62r1p3" SET COMPILER=_tasking
IF "%3" == "tasking62r1p3" SET COMPILER_PATH=C:/Program Files/TASKING/Tricore v6.2r1p3/ctc

REM HighTec GNU Compiler choice and Compiler path
IF "%3" == "gnu" SET COMPILER_CHOICE_TP=gnu
IF "%3" == "gnu" SET COMPILER=
IF "%3" == "gnu" SET COMPILER_PATH=C:/HIGHTEC/toolchains/tricore/v4.6.3.0

REM WindRiver Compiler choice and Compiler path
IF "%3" == "windriver" SET COMPILER_CHOICE_TP=windriver
IF "%3" == "windriver" SET COMPILER=_windriver
IF "%3" == "windriver" SET COMPILER_PATH=C:\WindRiver\diab\5.9.4.8

REM WindRiver 5.9.6.1+patch5931 Compiler choice and Compiler path
IF "%3" == "windriver5961p" SET COMPILER_CHOICE_TP=windriver5961p
IF "%3" == "windriver5961p" SET COMPILER=_windriver
IF "%3" == "windriver5961p" SET COMPILER_PATH=C:\WindRiver_5961\compilers\diab-5.9.6.1

REM WindRiver 5.9.6.2 Compiler choice and Compiler path
IF "%3" == "windriver5962" SET COMPILER_CHOICE_TP=windriver5962
IF "%3" == "windriver5962" SET COMPILER=_windriver
IF "%3" == "windriver5962" SET COMPILER_PATH=C:\WindRiver_5962\compilers\diab-5.9.6.2

REM WindRiver 5.9.6.3 Compiler choice and Compiler path
IF "%3" == "windriver5963" SET COMPILER_CHOICE_TP=windriver5963
IF "%3" == "windriver5963" SET COMPILER=_windriver
IF "%3" == "windriver5963" SET COMPILER_PATH=C:\WindRiver_5963\compilers\diab-5.9.6.3
REM tasking / tasking50r2 / HighTec GNU Compiler options standard choice
SET C_STANDARD=C90
IF "%4"=="C99" SET C_STANDARD=C99

SET OPTIONAL_OPTIMIZATION=Y
IF "%4"=="noopt" SET OPTIONAL_OPTIMIZATION=N

REM Flag to run Stanalone_SafeTlib
SET SAFETLIB_STANDALONE=TRUE

REM Makefile path 
SET MAKEFILE_PATH=./gnumake

REM Common files path
SET COMMON=./Common

REM Application Directory
SET APPL_BASE_DIR=./Application

REM SafeTlibCD Directory
SET SAFETLIBCD_BASE_DIR=./SafeTlibCD

REM SafeTlibCD Library Directory
SET SAFETLIBCD_LIB=$(APPL_BASE_DIR)/debug/TC$(STL_TARGET)x/$(COMPILER_CHOICE_TP)

REM SafeWdgCD Directory
SET SAFEWDG_BASE_DIR=./SafeWdgCD

REM SafeWdgCD Library Directory
SET SAFEWDG_LIB=$(APPL_BASE_DIR)/debug/TC$(STL_TARGET)x/$(COMPILER_CHOICE_TP)

REM Folder containing configuration files
SET CONFIG_DIR=./Cfg

IF NOT EXIST "%COMPILER_PATH%" goto no_compiler_path
IF NOT EXIST "%MAKEFILE_PATH%" goto no_makefile_path

REM -----------------------------------
REM Command prompt: Parameter 2
REM Target code: 27/26/29/23
SET STL_TARGET=27
IF NOT "%2"=="" SET STL_TARGET=%2

REM -----------------------------------
REM Command prompt: Parameter 3
REM Default Tresos Config Workspace name: RefApp_Cfg_<3DigitTargetNumber>
IF "%STL_TARGET%"=="27"  SET CONFIG_WKS=RefApp_Cfg_275
IF "%STL_TARGET%"=="26"  SET CONFIG_WKS=RefApp_Cfg_264
IF "%STL_TARGET%"=="29"  SET CONFIG_WKS=RefApp_Cfg_297
IF "%STL_TARGET%"=="23"  SET CONFIG_WKS=RefApp_Cfg_234
IF "%STL_TARGET%"=="22"  SET CONFIG_WKS=RefApp_Cfg_224
IF "%STL_TARGET%"=="21"  SET CONFIG_WKS=RefApp_Cfg_214
IF NOT "%5"=="" SET CONFIG_WKS=%5

REM -----------------------------------
REM Command prompt: Parameter 1
IF "%1"=="" goto just_build
IF "%1"=="build" goto just_build
IF "%1"=="cleanbuild" goto clean
IF "%1"=="clean" goto clean

echo "Wrong parameter option"
goto end

:clean
@echo "***********  Clean: SafeTlibCD library for %COMPILER_CHOICE_TP% ************"
IF EXIST %SAFETLIBCD_BASE_DIR%\SafeTlibCD_Cfg.mak "%MAKEFILE_PATH%/gnumake" -f "./SafeTlibCD/SafeTlibCD%COMPILER%.mak" clean
IF NOT EXIST %SAFETLIBCD_BASE_DIR%\SafeTlibCD_Cfg.mak DEL /Q /F /S .\Application\debug\TC%STL_TARGET%x\$(COMPILER_CHOICE_TP)\*.o .\Application\debug\TC%STL_TARGET%x\$(COMPILER_CHOICE_TP)\*.err .\Application\debug\TC%STL_TARGET%x\$(COMPILER_CHOICE_TP)\*.src .\Application\debug\TC%STL_TARGET%x\$(COMPILER_CHOICE_TP)\*.ers .\Application\debug\TC%STL_TARGET%x\$(COMPILER_CHOICE_TP)\*.s .\Application\debug\TC%STL_TARGET%x\$(COMPILER_CHOICE_TP)\*.i .\Application\debug\TC%STL_TARGET%x\$(COMPILER_CHOICE_TP)\*.ifx .\Application\debug\TC%STL_TARGET%x\$(COMPILER_CHOICE_TP)\*.elk .\Application\debug\TC%STL_TARGET%x\$(COMPILER_CHOICE_TP)\*.elf .\Application\debug\TC%STL_TARGET%x\$(COMPILER_CHOICE_TP)\*.hex .\Application\debug\TC%STL_TARGET%x\$(COMPILER_CHOICE_TP)\*.map > nul

@echo "***********  Clean: SafeWdgCD library for %COMPILER_CHOICE_TP% ************"
IF EXIST %SAFEWDG_BASE_DIR%/SafeWdgCD_Cfg.mak "%MAKEFILE_PATH%/gnumake" -f "./SafeWdgCD/SafeWdgCD%COMPILER%.mak" clean


@echo "***********  Clean: Reference Application for %COMPILER_CHOICE_TP% ************"
"%MAKEFILE_PATH%/gnumake" -f "./Application/Application%COMPILER%.mak" clean
IF "%1"=="clean" goto end

:generate_cfg_files_and_build
@echo "***********  Configuration Generation  ************"
SET TRESOS_PATH=c:\EB\tresos\bin
IF NOT EXIST %TRESOS_PATH% goto no_tresos_path
IF NOT EXIST Application\cfg\%CONFIG_WKS% goto no_tresos_workspace
CALL %TRESOS_PATH%\tresos_cmd.bat -data  Application\cfg\%CONFIG_WKS% generate RefApp_Cfg
copy/y Application\cfg\%CONFIG_WKS%\RefApp_Cfg\output\inc\*.* Cfg\inc
copy/y Application\cfg\%CONFIG_WKS%\RefApp_Cfg\output\src\*.* Cfg\src
copy/y Application\cfg\%CONFIG_WKS%\RefApp_Cfg\output\SafeTlibCD_Cfg.mak SafeTlibCD
copy/y Application\cfg\%CONFIG_WKS%\RefApp_Cfg\output\SafeWdgCD_cfg.mak SafeWdgCD
goto just_build

:no_tresos_path
@echo --------------------------- WARNING --------------------------------------
@echo Tresos Configuration tool path %TRESOS_PATH% does not exist. 
goto info_skip_config_gen

:no_tresos_workspace
@echo --------------------------- WARNING --------------------------------------
@echo Warning: Tresos workspace Application\cfg\%CONFIG_WKS% does not exist. 

:info_skip_config_gen
@echo Skipping configuration generation and continuing build with already 
@echo available configuration files...
@echo --------------------------------------------------------------------------

:just_build
@echo "***********  Build: SafeTlibCD library for %COMPILER_CHOICE_TP% ************"
"%MAKEFILE_PATH%/gnumake" -f "./SafeTlibCD/SafeTlibCD%COMPILER%.mak" -k

@echo "***********  Build: SafeWdgCD library for %COMPILER_CHOICE_TP% ************"
"%MAKEFILE_PATH%/gnumake" -f "./SafeWdgCD/SafeWdgCD%COMPILER%.mak" -k

@echo "***********  Build: Reference Application for %COMPILER_CHOICE_TP% ************"
"%MAKEFILE_PATH%/gnumake" -f "./Application/Application%COMPILER%.mak"

goto end

:no_compiler_path
@echo ERROR: Compiler path "%COMPILER_PATH%" does not exist
goto end

:no_makefile_path
@echo ERROR: Makefile path "%MAKEFILE_PATH%" does not exist

:end
