<?xml version='1.0'?>
<AUTOSAR xmlns="http://autosar.org/schema/r4.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://autosar.org/schema/r4.0 AUTOSAR_4-0-3.xsd">
  <AR-PACKAGES>
    <AR-PACKAGE UUID="93d9a111-9d4c-47b6-98cb-fe5aec24c8b0">
      <SHORT-NAME>AURIX</SHORT-NAME>
      <ELEMENTS>
        <ECUC-MODULE-DEF UUID="03eaa8c5-137e-402e-823d-8d7776c1de4d">
          <SHORT-NAME>SriTst</SHORT-NAME>
          <SUPPORTED-CONFIG-VARIANTS>
            <SUPPORTED-CONFIG-VARIANT>VARIANT-LINK-TIME</SUPPORTED-CONFIG-VARIANT>
          </SUPPORTED-CONFIG-VARIANTS>
          <CONTAINERS>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="3ea7abca-6ba4-4d14-befd-8bb9350fe86f">
              <SHORT-NAME>SriTstConfigSet</SHORT-NAME>
              <DESC>
                <L-2 L="EN">This container is the base of an Configuration Set which contains the configured Sri Configuration.</L-2>
              </DESC>
              <MULTIPLE-CONFIGURATION-CONTAINER>true</MULTIPLE-CONFIGURATION-CONTAINER>
              <PARAMETERS>
                <ECUC-BOOLEAN-PARAM-DEF UUID="c4db2e42-a9d6-41dd-a33d-4ae4fc723972">
                  <SHORT-NAME>PMUDFlashTestEnCpu0</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter defines whether the SRI EDC will include the PMUDFlash module.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>LINK</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-LINK-TIME</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>false</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
                <ECUC-BOOLEAN-PARAM-DEF UUID="b39c3bfa-edbc-48e2-8d67-a3164e076311">
                  <SHORT-NAME>PMUPFlashTestEnCpu0</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter defines whether the SRI EDC will include the PMU PFlash address decoder. Data decoder is not tested -&gt; refer to Global Design Decisions.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>LINK</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-LINK-TIME</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>false</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
                <ECUC-BOOLEAN-PARAM-DEF UUID="ebdc5a68-8502-411f-9c6c-a3586ed4b42d">
                  <SHORT-NAME>DMATestEnCpu0</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter defines whether the SRI EDC will include the DMA module.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>LINK</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-LINK-TIME</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>false</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
                <ECUC-BOOLEAN-PARAM-DEF UUID="ebdc5a68-8502-411f-9c6c-a358aed4b42d">
                  <SHORT-NAME>XBARTestEnCpu0</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter defines whether the SRI EDC will include the XBAR module.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>LINK</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-LINK-TIME</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>false</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="d0831385-935c-4d80-9135-3a24a0f05a9a">
                  <SHORT-NAME>SriTstPSPRTstAdrCpu0</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter defines the address where the accesses (read and write) into the PSPR memory are done during the SRI-CPUx.PMI test. Actually nothing shall ever be written to that address because due to the injected error.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>LINK</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-LINK-TIME</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>1880096768</DEFAULT-VALUE>
                  <MAX>1880104959</MAX>
                  <MIN>1880096768</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="72177e02-4687-492e-9c73-95124f699152">
                  <SHORT-NAME>SriTstDSPRTstAdrCpu0</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter defines the address where the accesses (read and write) into the DSPR memory are done during the SRI-CPUx.PMI test.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>LINK</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-LINK-TIME</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>1879048192</DEFAULT-VALUE>
                  <MAX>1879236607</MAX>
                  <MIN>1879048192</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="5608a8b8-ab01-4db0-80d5-0a026a046342">
                  <SHORT-NAME>SriTstDFlashTstAdrCpu0</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter defines the address where the accesses (read and write) into the PSPR memory are done during the SRI-CPUx.PMI test. Actually nothing shall ever be written to that address because due to the injected error.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>LINK</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-LINK-TIME</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>2936012800</DEFAULT-VALUE>
                  <MAX>2936143871</MAX>
                  <MIN>2936012800</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="64105e52-0dfb-43f9-9b2a-1ae3096f3188">
                  <SHORT-NAME>SriTstPFlashTstAdrCpu0</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter defines the address where the accesses (read and write) into the PSPR memory are done during the SRI-CPUx.PMI test. Actually nothing shall ever be written to that address because due to the injected error.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>LINK</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-LINK-TIME</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>2684354560</DEFAULT-VALUE>
                  <MAX>2686451711</MAX>
                  <MIN>2684354560</MIN>
                </ECUC-INTEGER-PARAM-DEF>
              </PARAMETERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="a868aef7-06f0-5e10-9482-3b9f6ca77cb2">
              <SHORT-NAME>SriTstGeneral</SHORT-NAME>
              <DESC>
                <L-2 L="EN">General configuration (parameters) of the SriTst driver software module.</L-2>
              </DESC>
              <PARAMETERS>
                <ECUC-INTEGER-PARAM-DEF UUID="e03d91fc-1eed-435f-a52a-c33f687c3a58">
                  <SHORT-NAME>SriTstDMAChannelNum</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter defines which DMA channel shall be used for the SRI test. The Maximum channel numbers varies between device to device</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-LINK-TIME</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>15</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
              </PARAMETERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
          </CONTAINERS>
        </ECUC-MODULE-DEF>
      </ELEMENTS>
    </AR-PACKAGE>
  </AR-PACKAGES>
</AUTOSAR>
