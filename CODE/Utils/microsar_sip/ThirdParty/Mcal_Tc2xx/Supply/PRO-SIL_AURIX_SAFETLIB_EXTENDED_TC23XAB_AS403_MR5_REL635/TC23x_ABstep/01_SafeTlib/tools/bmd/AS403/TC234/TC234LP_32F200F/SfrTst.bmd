<?xml version='1.0'?>
<AUTOSAR xmlns="http://autosar.org/schema/r4.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://autosar.org/schema/r4.0 AUTOSAR_4-0-3.xsd">
  <AR-PACKAGES>
    <AR-PACKAGE UUID="93d9a111-9d4c-47b6-98cb-fe5aec24c8b0">
      <SHORT-NAME>AURIX</SHORT-NAME>
      <ELEMENTS>
        <ECUC-MODULE-DEF UUID="03eaa8c5-137e-402e-823d-8d7776c1de4d">
          <SHORT-NAME>SfrTst</SHORT-NAME>
          <SUPPORTED-CONFIG-VARIANTS>
            <SUPPORTED-CONFIG-VARIANT>VARIANT-LINK-TIME</SUPPORTED-CONFIG-VARIANT>
          </SUPPORTED-CONFIG-VARIANTS>
          <CONTAINERS>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ccda81dc-46cf-4171-858b-7c947884702f">
              <SHORT-NAME>SfrTstConfigSet</SHORT-NAME>
              <DESC>
                <L-2 L="FOR-ALL">This is the base container that contains the link time configuration parameters</L-2>
              </DESC>
              <MULTIPLE-CONFIGURATION-CONTAINER>true</MULTIPLE-CONFIGURATION-CONTAINER>
              <PARAMETERS>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce928e-7ed1-4368-95b2-21d30b1fa49e">
                  <SHORT-NAME>SfrTstExpectedCrcResult</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This parameter defines the CRC value over all registers configured in container SfrTstRegister.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>LINK</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-LINK-TIME</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>4294967295</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
              </PARAMETERS>
              <SUB-CONTAINERS>
                <ECUC-PARAM-CONF-CONTAINER-DEF UUID="97e6fd09-f2da-4944-b2e0-edc4dc70e5a3">
                  <SHORT-NAME>SfrTstRegister</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">This is the base container having parameter to be configured SfrTstRegister Name, bitfield, Mask, Expected crcresult </L-2>
                  </DESC>
                  <LOWER-MULTIPLICITY>1</LOWER-MULTIPLICITY>
                  <UPPER-MULTIPLICITY-INFINITE>1</UPPER-MULTIPLICITY-INFINITE>
                  <PARAMETERS>
                    <ECUC-STRING-PARAM-DEF UUID="ECUC:d3117de4-d573-e62d-12aa-c0febc0b1cc6">
                      <SHORT-NAME>SfrTstRegisterName</SHORT-NAME>
                      <DESC>
                        <L-2 L="EN">Name of the register to be edited, e.g. LMU_CLC, SMU_FSP, SMU_AG etc.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>LINK</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-LINK-TIME</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                      <ECUC-STRING-PARAM-DEF-VARIANTS>
                        <ECUC-STRING-PARAM-DEF-CONDITIONAL>
                          <DEFAULT-VALUE>LMU_CLC</DEFAULT-VALUE>
                        </ECUC-STRING-PARAM-DEF-CONDITIONAL>
                      </ECUC-STRING-PARAM-DEF-VARIANTS>
                    </ECUC-STRING-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce928e-8ed1-2968-93b2-24d30b1fa49e">
                      <SHORT-NAME>SfrTstRegisterValue</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This parameter defines the expected value of the register to be tested.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>LINK</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-LINK-TIME</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>0</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="98ce928e-7ed1-9968-93b2-24d30b1fa49e">
                      <SHORT-NAME>SfrTstRegisterMask</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This parameter defines a mask to omit certain bits of the register from the SFR test.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>LINK</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-LINK-TIME</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>0</DEFAULT-VALUE>
                      <MAX>4294967295</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-ENUMERATION-PARAM-DEF UUID="ECUC:57dcff50-89c4-444b-b345-2c797a2881f7">
                      <SHORT-NAME>SfrTstRegisterBitWidth</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">Select 16-bit/32-bit based on configured SFR</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>LINK</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-LINK-TIME</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                      <DEFAULT-VALUE>STL_BIT_32</DEFAULT-VALUE>
                      <LITERALS>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>STL_BIT_16</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>STL_BIT_32</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                      </LITERALS>
                    </ECUC-ENUMERATION-PARAM-DEF>
                  </PARAMETERS>
                </ECUC-PARAM-CONF-CONTAINER-DEF>
              </SUB-CONTAINERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
          </CONTAINERS>
        </ECUC-MODULE-DEF>
      </ELEMENTS>
    </AR-PACKAGE>
  </AR-PACKAGES>
</AUTOSAR>
