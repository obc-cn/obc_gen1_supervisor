
/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2013)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME  :  SafeWdgExtTlf_PBCfg.c                                        **
**                                                                            **
**  VERSION   : 0.0.1                                                         **
**                                                                            **
**  DATE, TIME: 2016-09-14, 17:37:06                                      **
**                                                                            **
**  GENERATOR : Build b141014-0350                                          **
**                                                                            **
**  BSW MODULE DECRIPTION : SafeWdgExtTlf.bmd/xdm                             **
**                                                                            **
**  VARIANT   : VariantPB                                                     **
**                                                                            **
**  PLATFORM  : Infineon Aurix                                                **
**                                                                            **
**  COMPILER  : Tasking / GNU / Diab                                          **
**                                                                            **
**  AUTHOR    : DL-BLR-ATV-STC                                                **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : SafeWdgExtTlf configuration generated file                 **
**                                                                            **
**  SPECIFICATION(S) :Aurix-HE_SafeTlib_DS_SafeWdgExtTlf.docm, Ver 0.1        **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: no                                       **
**                                                                            **
*******************************************************************************/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "SafeWdgIf.h"

/*******************************************************************************
**                      Private Macro definition                              **
*******************************************************************************/


/*******************************************************************************
**                      Configuration Options                                 **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/


/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
#define IFX_SWDG_START_SEC_POSTBUILDCFG_ASIL_B
#include "Ifx_MemMap.h"


/* external TLF watchdog configuration */
const SafeWdgExtTlf_ConfigType SafeWdgExtTlf_ConfigRoot[] =
{
	{
		&SafeWdgQspi_ChannelConfigRoot[0],
    TLF_WM_FWD_WWD_SPI,/*Type of Watchdog */
        0U, /*Watchdog cycle time.This value will set "WDCYC" bit in WDCFG0*/
        1U, /*open window time*/
    2U, /*Close window time*/
    4U, /*Functional watchdog time*/
 		9U,				/*Window Watchdog error threshold*/
		9U,				/*Functional Watchdog error threshold*/
 
		&MODULE_P20,		/*DUMMY PORT ,Will not be used since TLF_WM_WWD_WDI ia not selected.*/
		9U,				/*DUMMY PIN ,Will not be used since TLF_WM_WWD_WDI ia not selected.*/
		/* Signature XOR table: */
		{
      /*XOR Mask for sig;     Actual TLF Sig*/
			0xC0948E93U,						  /* FF0FF000*/
			0xE3490660U,						/* B040BF4F*/
			0x8F6543F1U,						/* E919E616*/
			0xACB8CB02U,						/* A656A959*/
			0xCAED0E6FU,						/* 75857A8A*/
			0xE930869CU,						/* 3ACA35C5*/
			0x851CC30DU,						/* 63936C9C*/
			0xA6C14BFEU,						/* 2CDC23D3*/
			0x9D2CFE32U,						/* D222DD2D*/
			0xBEF176C1U,						/* 9D6D9262*/
			0xD2DD3350U,						/* C434CB3B*/
			0xF100BBA3U,						/* 8B7B8474*/
			0x97557ECEU,						/* 58A857A7*/
			0xB488F63DU,						/* 17E718E8*/
			0xD8A4B3ACU,						/* 4EBE41B1*/
			0xFB793B5FU							/* 01F10EFE*/
		}
  }
};

#define IFX_SWDG_STOP_SEC_POSTBUILDCFG_ASIL_B
#include "Ifx_MemMap.h"
