/*******************************************************************************
** Copyright (C) Infineon Technologies (2014)                                  ** 
**                                                                            **
** All rights reserved.                                                       ** 
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**   $FILENAME   : CpuSbstTst.c $                                             **
**                                                                            **
**   $CC VERSION : \main\24 $                                                 **
**                                                                            **
**   $DATE       : 2016-09-30 $                                               **
**                                                                            **
**   VARIANT      : VariantLT                                                 **
**                                                                            **
**   PLATFORM     : Infineon AURIX                                            **
**                                                                            **
**   AUTHOR       : DL-BLR-ATV-STC                                            **
**                                                                            **
**   VENDOR       : Infineon Technologies                                     **
**                                                                            **
**   TRACEABILITY :                                                           **
**                                                                            **
**   DESCRIPTION  : This file contains                                        **
**                 - functionality of SBST                                    **
**                                                                            **
**   SPECIFICATION(S) : Aurix_SafeTlib_DS_CpuSbst_Test.docm                   **
**                                                                            **
**   MAY BE CHANGED BY USER [Yes/No]: No                                      **
**                                                                            **
*******************************************************************************/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "Mcal.h"
#include "IfxScu_reg.h"
#include "CpuSbstTst.h"
#include "Sl_Timeout.h"
#include "Sl_Ipc.h"
#include "Smu.h"

/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/
extern uint32 __SBST_CRC_RESULT;


/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/
extern void SBST_mini_Slice0(void);
extern void SBST_mini_Slice1(void);
extern void SBST_mini_Slice2(void);
extern void SBST_mini_Slice3(void);
extern void SBST_mini_Slice4(void);
extern void SBST_mini_Slice5(void);
#if (CHIP_ID == 26U) || (CHIP_ID == 27U) || (CHIP_ID == 29U)
extern void SBST_TC16P_Slice6(void);
extern void SBST_TC16P_Slice7(void);
extern void SBST_TC16P_Slice8(void);
extern void SBST_TC16P_Slice9(void);
#endif /* (CHIP_ID == 26U) || (CHIP_ID == 27U) || (CHIP_ID == 29U) */

/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/

#define IFX_SBST_CALLBACK_START_SEC_CODE_ASIL_B
#include "Ifx_MemMap.h"

/*******************************************************************************
**                       Private Function Definitions                         **
*******************************************************************************/

/*******************************************************************************
** Traceability     : [cover parentID=]        [/cover]                       **
**                                                                            **
** Syntax           : void SchM_Enter_SafeTcore(void)                         **
**                                                                            **
** Service ID       : none                                                    **
**                                                                            **
** Sync/Async       : Synchronous                                             **
**                                                                            **
** Reentrancy       : Non Reentrant                                           **
**                                                                            **
** Parameters(in)   : None                                                    **
**                                                                            **
** Parameters (out) : None                                                    **
**                                                                            **
** Return value     : None                                                    **
**                                                                            **
** Description      : API provided for the SBST E/P-core test.                **
**                                                                            **
*******************************************************************************/ 
void SchM_Enter_SafeTcore(void)
{
  /* Call user configured Enter SafeTcore */
  #ifdef CPUSBSTTST_NOTIFICATION
  ((CpuSbstTst_FunPtrType)CPUSBSTTST_ENTER_NOTIF)();
  #endif /* CPUSBSTTST_NOTIFICATION */
}

/* *****************************************************************************
** Traceability    : [cover parentID=]        [/cover]                        **
**                                                                            **
** Syntax          : void SchM_Exit_SafeTcore(void)                           **
**                                                                            **
** Service ID      : none                                                     **
**                                                                            **
** Sync/Async      : Synchronous                                              **
**                                                                            **
** Reentrancy       : Non Reentrant                                           **
**                                                                            **
** Parameters(in)   : None                                                    **
**                                                                            **
** Parameters (out) : None                                                    **
**                                                                            **
** Return value     : None                                                    **
**                                                                            **
** Description      : API provided for the SBST E/P-core test.                **
**                                                                            **
*******************************************************************************/
void SchM_Exit_SafeTcore(void)
{
  /* Call user configured Exit SafeTcore */
  #ifdef CPUSBSTTST_NOTIFICATION  
  ((CpuSbstTst_FunPtrType)CPUSBSTTST_EXIT_NOTIF)();
  #endif /* CPUSBSTTST_NOTIFICATION */
}

#define IFX_SBST_CALLBACK_STOP_SEC_CODE_ASIL_B
#include "Ifx_MemMap.h"

#define IFX_SBST_START_SEC_CODE_ASIL_B
#include "Ifx_MemMap.h"
/*******************************************************************************
** Traceability     : [cover parentID=]        [/cover]                       **
**                                                                            **
** Syntax           : Sl_TstRsltType CpuTst_CpuSbstETst                       **
**                                      (const Sl_ParamSetType ParamSetIndex, **
**                                       const uint8 TstSeed,                 **
**                                       uint32* const TstSignature);         **
**                                                                            **
** Service ID       : none                                                    **
**                                                                            **
** Sync/Async       : Synchronous                                             **
**                                                                            **
** Reentrancy       : Non Reentrant                                           **
**                                                                            **
** Parameters(in)   : ParamSetIndex : Identifies the parameter set to be      **
**                                    used for test execution                 **
**                    TstSeed : Seed to be used for generating the signature  **
**                                                                            **
** Parameters (out) : TstSignature : Test signature generated by the test     **
**                                                                            **
** Return value     : SBST_SUCCESS - Test succeeded                           **
**                    <Error ID> - Type of error during test                  **
**                                                                            **
** Description      : API provided for the SBST E-core test.                  **
**                                                                            **
*******************************************************************************/
Sl_TstRsltType CpuTst_CpuSbstETst
(
 const Sl_ParamSetType ParamSetIndex,
 const uint8 TstSeed,
 uint32* const TstSignature
)
{
  uint32 *sbst_crc_result;
  Sl_TstRsltType Result = SBST_E_FAILURE;

  UNUSED_PARAMETER(ParamSetIndex)
  
  sbst_crc_result = &(__SBST_CRC_RESULT);
  *sbst_crc_result = 0xFFFFFFFFU;
  
  /* Initialize Test Signature: */
  *TstSignature = (uint32)CRC32((uint32)TEST_ID_CPUSBST_E_TST,(uint32)TstSeed);
  
  SBST_mini_Slice0();
  SBST_mini_Slice1();
  SBST_mini_Slice2();
  SBST_mini_Slice3();  
  SBST_mini_Slice4();
  SBST_mini_Slice5();
  *TstSignature = (uint32)(CRC32(*TstSignature, *sbst_crc_result));
  /* Checking result against expected result for TC16E */
  if (*sbst_crc_result == SBST_EXPECTED_CRC_VALUE)
  {
    Result = SBST_E_SUCCESS;
  }

  *TstSignature = (uint32)(CRC32(*TstSignature, Result));
  
  return(Result);
}
#if (CHIP_ID == 26U) || (CHIP_ID == 27U) || (CHIP_ID == 29U)
/*******************************************************************************
** Traceability     : [cover parentID=]        [/cover]                       **
**                                                                            **
** Syntax           : Sl_TstRsltType CpuTst_CpuSbstPTst                       **
**                                      (const Sl_ParamSetType ParamSetIndex, **
**                                       const uint8 TstSeed,                 **
**                                       uint32* const TstSignature);         **
**                                                                            **
** Service ID       : none                                                    **
**                                                                            **
** Sync/Async       : Synchronous                                             **
**                                                                            **
** Reentrancy       : Non Reentrant                                           **
**                                                                            **
** Parameters(in)   : ParamSetIndex : Identifies the parameter set to be      **
**                                    used for test execution                 **
**                    TstSeed : Seed to be used for generating the signature  **
**                                                                            **
** Parameters (out) : TstSignature : Test signature generated by the test     **
**                                                                            **
** Return value     : SBST_SUCCESS - Test succeeded                           **
**                    <Error ID> - Type of error during test                  **
**                                                                            **
** Description      : API provided for the SBST P-core test.                  **
**                                                                            **
*******************************************************************************/
Sl_TstRsltType CpuTst_CpuSbstPTst
(
 const Sl_ParamSetType ParamSetIndex,
 const uint8 TstSeed,
 uint32* const TstSignature
)
{
  uint32 *sbst_crc_result;
  Sl_TstRsltType Result = SBST_P_FAILURE;
  
  UNUSED_PARAMETER(ParamSetIndex)
  
  sbst_crc_result = &(__SBST_CRC_RESULT);  

  *sbst_crc_result = 0xFFFFFFFFU;
  
  /* Initialize Test Signature: */
  *TstSignature = (uint32)CRC32((uint32)TEST_ID_CPUSBST_P_TST, (uint32)TstSeed);
  
  SBST_mini_Slice0();
  SBST_mini_Slice1();
  SBST_mini_Slice2();
  SBST_mini_Slice3();  
  SBST_mini_Slice4();
  SBST_mini_Slice5();
  SBST_TC16P_Slice6();
  SBST_TC16P_Slice7();
  SBST_TC16P_Slice8();
  SBST_TC16P_Slice9();
  *TstSignature = (uint32)(CRC32(*TstSignature, *sbst_crc_result));
  /* Checking result against expected result for TC16P */
  #ifdef SBST_PTST_EXPECTED_CRC_VALUE
  if (*sbst_crc_result == SBST_PTST_EXPECTED_CRC_VALUE)
  #else
  if (*sbst_crc_result == SBST_EXPECTED_CRC_VALUE)
  #endif
  {
    Result = SBST_P_SUCCESS;
  }
  
  *TstSignature = (uint32)(CRC32(*TstSignature, Result));
  
  return(Result);
}
#endif /* (CHIP_ID == 26U) || (CHIP_ID == 27U) || (CHIP_ID == 29U) */

#define IFX_SBST_STOP_SEC_CODE_ASIL_B
#include "Ifx_MemMap.h"
