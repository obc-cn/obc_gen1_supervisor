@ECHO OFF
REM ****************************************************************************
REM *                                                                         **
REM * Copyright (C) Infineon Technologies (2014)                              **
REM *                                                                         **
REM * All rights reserved.                                                    **
REM *                                                                         **
REM * This document contains proprietary information belonging to Infineon    **
REM * Technologies. Passing on and copying of this document, and communication**
REM * of its contents is not permitted without prior written authorization.   **
REM *                                                                         **
REM ****************************************************************************
REM *                                                                         **
REM *  FILENAME  : CopyTresosPlugins.bat                                      **
REM *                                                                         **
REM *  VERSION   : 0.0.1                                                      **
REM *                                                                         **
REM *  DATE      : 2014.09.15                                                 **
REM *                                                                         **
REM *                                                                         **
REM *  VENDOR    : Infineon Technologies                                      **
REM *                                                                         **
REM *  DESCRIPTION  : This file copies driver plugins for Tresos Studio       **
REM *                                                                         **
REM *  MAY BE CHANGED BY USER [yes/no]: no                                    **
REM *                                                                         **
REM ***************************************************************************/

REM ****************************************************************************
REM *                      Author(s) Identity                                 **
REM ****************************************************************************
REM *                                                                         **
REM * Initials     Name                                                       **
REM * ------------------------------------------------------------------------**
REM * Basheer     Basheer Mohaideen                                           **
REM *                                                                         **
REM ***************************************************************************/

REM ****************************************************************************
REM *                      Revision Control History                           **
REM ***************************************************************************/
REM
REM * V0.0.1: 2014.09.15, Basheer  : Initial version 
REM *
REM ***************************************************************************/

REM ****************************************************************************

REM  Check if destination folder is mentioned on command line
SET BASE_PATH=%~dp0
SET BASE_PLUGINS_PATH=%BASE_PATH%\tools\plugins

IF "%1" == "" GOTO NO-DST-DIR

REM  Check if destination folder exists
IF NOT EXIST %1 GOTO DST-DIR-DOESNOT-EXIST

REM  Copy plugin files
REM ----------------------------------------------------------------------------
REM /S - Copies directories and subdirectories except empty ones.
REM /R - Overwrites read-only files.
REM /K - Copies attributes. Normal Xcopy will reset read-only attributes.
REM /Y - Suppresses prompting to confirm you want to overwrite an
REM       existing destination file.
REM /F - Display full file path
REM ----------------------------------------------------------------------------
xcopy /S/R/K/Y/F %BASE_PLUGINS_PATH% %1
IF "%2" == "YES" GOTO LICENSE_COPY
IF "%2" == "Yes" GOTO LICENSE_COPY
GOTO END

:LICENSE_COPY
xcopy ..\..\tools\license\tresos_Partner_Infineon.lic %1\..\configuration
GOTO END

:NO-DST-DIR
ECHO Error: Destination (plugins) directory not mentioned.
ECHO Usage: copy_plugins.bat destination-folder
GOTO END

:DST-DIR-DOESNOT-EXIST
ECHO Error: Destination folder doesn't exist

:END
