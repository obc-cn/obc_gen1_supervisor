#ifndef CPUSBSTTST_H
#define CPUSBSTTST_H

/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2014)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**   $FILENAME   : CpuSbstTst.h $                                             **
**                                                                            **
**   $CC VERSION : \main\16 $                                                 **
**                                                                            **
**   $DATE       : 2016-10-04 $                                               **
**                                                                            **
**  VARIANT   : VariantPB                                                     **
**                                                                            **
**  PLATFORM  : Infineon Aurix                                                **
**                                                                            **
**  AUTHOR    : DL-BLR-ATV-STC                                                **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : Header file for SBST test                                  **
**                                                                            **
**  REFERENCE(S) :Aurix_HE_DS_SBST_Opcode_Check.docx, V1.4                    **
**                                                                            **
**  MAY BE CHANGED BY USER : no                                               **
**                                                                            **
*******************************************************************************/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "Platform_Types.h"
#include "Sl_Timeout.h"
#include "Compiler_Cfg.h"
#include "Sl_ErrorCodes.h"
#include "CpuSbstTst_Cfg.h"
#include "ChipId.h"
/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/

/*******************************************************************************
**                      Typedefs                                              **
*******************************************************************************/
/* Type:Sbst_FunPtrType
Function pointer for Enter and Exit Notification */
typedef void (*CpuSbstTst_FunPtrType)(void);

/*******************************************************************************
**                      Global Constant Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Declarations                          **
*******************************************************************************/

#define IFX_SBST_CALLBACK_START_SEC_CODE_ASIL_B
#include "Ifx_MemMap.h"
/*******************************************************************************
**                      Global Function Prototypes                            **
*******************************************************************************/
extern void SchM_Enter_SafeTcore(void);
extern void SchM_Exit_SafeTcore(void);

#define IFX_SBST_CALLBACK_STOP_SEC_CODE_ASIL_B
#include "Ifx_MemMap.h"

#define IFX_SBST_START_SEC_CODE_ASIL_B
#include "Ifx_MemMap.h"
/*******************************************************************************
** Traceability      : [cover parentID=]        [/cover]                      **
**                                                                            **
** Syntax           : Sl_TstRsltType CpuTst_CpuSbstETst                       **
**                                      (const Sl_ParamSetType ParamSetIndex, **
**                                       const uint8 TstSeed,                 **
**                                       uint32* const TstSignature);         **
**                                                                            **
** Service ID       : none                                                    **
**                                                                            **
** Sync/Async       : Synchronous                                             **
**                                                                            **
** Reentrancy       : Non Reentrant                                           **
**                                                                            **
** Parameters(in)   : ParamSetIndex : Identifies the parameter set to be      **
**                                    used for test execution                 **
**                    TstSeed : Seed to be used for generating the signature  **
**                                                                            **
** Parameters (out) : TstSignature : Test signature generated by the test     **
**                                                                            **
** Return value     : SBST_SUCCESS - Test successed                           **
**                    <Error ID> - Type of error during test                  **
**                                                                            **
** Description      : API provided for the clock monitor test.                **
**                    It is responsible for test preparation, test execution  **
**                    and final test restoration.                             **
**                                                                            **
*******************************************************************************/
extern Sl_TstRsltType CpuTst_CpuSbstETst
(
    const Sl_ParamSetType ParamSetIndex,
    const uint8 TstSeed,
    uint32* const TstSignature
);
#if (CHIP_ID == 26U) || (CHIP_ID == 27U) || (CHIP_ID == 29U)
/*******************************************************************************
** Traceability      : [cover parentID=]        [/cover]                      **
**                                                                            **
** Syntax           : Sl_TstRsltType CpuTst_CpuSbstPTst                       **
**                                      (const Sl_ParamSetType ParamSetIndex, **
**                                       const uint8 TstSeed,                 **
**                                       uint32* const TstSignature);         **
**                                                                            **
** Service ID       : none                                                    **
**                                                                            **
** Sync/Async       : Synchronous                                             **
**                                                                            **
** Reentrancy       : Non Reentrant                                           **
**                                                                            **
** Parameters(in)   : ParamSetIndex : Identifies the parameter set to be      **
**                                    used for test execution                 **
**                    TstSeed : Seed to be used for generating the signature  **
**                                                                            **
** Parameters (out) : TstSignature : Test signature generated by the test     **
**                                                                            **
** Return value     : SBST_SUCCESS - Test successed                           **
**                    <Error ID> - Type of error during test                  **
**                                                                            **
** Description      : API provided for the clock monitor test.                **
**                    It is responsible for test preparation, test execution  **
**                    and final test restoration.                             **
**                                                                            **
*******************************************************************************/
extern Sl_TstRsltType CpuTst_CpuSbstPTst
(
    const Sl_ParamSetType ParamSetIndex,
    const uint8 TstSeed,
    uint32* const TstSignature
);
#endif /* (CHIP_ID == 26U) || (CHIP_ID == 27U) || (CHIP_ID == 29U) */
/*******************************************************************************
**                Global Function like macro Definitions                      **
*******************************************************************************/


#endif /* CPUSBSTTST_H */

#define IFX_SBST_STOP_SEC_CODE_ASIL_B
#include "Ifx_MemMap.h"
