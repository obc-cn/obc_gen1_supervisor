#/******************************************************************************
#** Copyright (C) Infineon Technologies (2013)                                **
#**                                                                           **
#** All rights reserved.                                                      **
#**                                                                           **
#** This document contains proprietary information belonging to Infineon      **
#** Technologies. Passing on and copying of this document, and communication  **
#** of its contents is not permitted without prior written authorization.     **
#**                                                                           **
#*******************************************************************************
#**  FILENAME  : Application_tasking.mak                                      **
#**                                                                           **
#**  VERSION   : 1.0.5                                                        **
#**                                                                           **
#**  DATE      : 2015-11-27                                                   **
#**                                                                           **
#**  VARIANT   : VariantPB                                                    **
#**                                                                           **
#**  PLATFORM  : Infineon AURIX                                               **
#**                                                                           **
#**  COMPILER  : Tasking                                                      **
#**                                                                           **
#**  AUTHOR    : SafeTlib Team                                                **
#**                                                                           **
#**  VENDOR    : Infineon Technologies                                        **
#**                                                                           **
#**  TRACEABILITY:                                                            **
#**                                                                           **
#**  DESCRIPTION  : This file contains build instruntions of Application      **
#**                                                                           **
#**  SPECIFICATION(S) :                                                       **
#**                                                                           **
#**  MAY BE CHANGED BY USER [Yes/No]: No                                      **
#**                                                                           **
#******************************************************************************/
#
#
#/******************************************************************************
#**                      Revision Control History                             **
#******************************************************************************/
# * V1.0.5: 2015-11-27: Tasking 5.0r2 support for safetlib package.
# * V1.0.4: 2014-02-13: sync_on_halt removed
# * V1.0.3: 2014-01-31: Added the SBST file(SBST_RelocTable)
#/* V1.0.2: 2014-01-23: Removed Deadcode Os.c
# * V1.0.1: 2014-01-14: Removed Os.c
# * V1.0.0: 2013-10-25: Integration with SafeTlib Added Os.c, Stm_Irq.c
#                       Removed Mcal_Lib.c, Added Mcal_WdgLib.c
# * V0.0.6: 2013.10.11: Added support for different Aurix variants
# * V0.0.5: 2013-07-24: Added conditional compile sets for diff. watchdog types 
# * V0.0.4: 2013-07-19: Added the CIC hookup file, removed commented lines 
# * V0.0.3: 2013-07-02: Added the SBST module files. 
# * V0.0.2: 2013-03-01: Formal corrections 
# * V0.0.1: 2013-02-28: Initial Version. 
# *                     Makefile for Application and upper layer
# */

### SBST OBJ Directories
SBSTDIR = .\SBST
OBJ_SBST_DIR = $(SBSTDIR)

### Upper Layer Directories
SWDGM_BASE_DIR = ./UpperLayer/SafeWdgM
TSTM_BASE_DIR = ./UpperLayer/TstM

 
### Output Directory
APPL_OUTDIR = $(APPL_BASE_DIR)/debug/TC$(STL_TARGET)x/$(COMPILER_CHOICE_TP)

### Application and Upper Layer Objects Directory
APPL_OBJDIR = $(APPL_BASE_DIR)/debug/TC$(STL_TARGET)x/$(COMPILER_CHOICE_TP)

### TARGET Name
APPL_TARGET = RefApp_TC$(STL_TARGET)x_$(COMPILER_CHOICE_TP)

SAFETLIB_USED="-DSAFETLIB_USED=STD_ON"
ifeq ($(SAFETLIB_STANDALONE), TRUE)
SAFETLIB_STANDALONE_USED="-DSAFETLIB_STANDALONE=TRUE"
else
SAFETLIB_STANDALONE_USED="-DSAFETLIB_STANDALONE=FALSE"
endif

### Compiler Options Makefile
include $(COMMON)/Compiler_Options_tasking.mak
include $(SAFEWDG_BASE_DIR)/SafeWdgCD_Cfg.mak 
include $(SAFETLIBCD_BASE_DIR)/SafeTlibCD_Cfg.mak

### Application and Upper Layer object files
APPL_OBJ = $(APPL_OBJDIR)/AppCbk.o $(APPL_OBJDIR)/RefApp.o
APPL_OBJ += $(APPL_OBJDIR)/SmuIntHandler.o

ifeq ($(SAFETLIB_STANDALONE), TRUE)
SBST_OBJ =
APPL_OBJ += $(APPL_OBJDIR)/Test_Print.o $(APPL_OBJDIR)/Test_Time.o
APPL_OBJ += $(APPL_OBJDIR)/Mcal_WdgLib.o
APPL_OBJ += $(APPL_OBJDIR)/Mcal_TcLib.o
APPL_OBJ += $(APPL_OBJDIR)/Stm_Irq.o
APPL_OBJ += $(APPL_OBJDIR)/Mcal.o $(APPL_OBJDIR)/Mcal_Trap.o $(APPL_OBJDIR)/Irq.o

ifeq ($(CPUSBSTTST_DELIVERY),ON)
ifeq ($(STL_TARGET_EP),FALSE)
SBST_OBJ += "$(OBJ_SBST_DIR)\SBST_Kernel_CoreTest.o" "$(OBJ_SBST_DIR)\SBST_Kernel_ISG.o" "$(OBJ_SBST_DIR)\SBST_Kernel_TestCode.o" "$(OBJ_SBST_DIR)\SBST_TC16E_TestCode.o" "$(OBJ_SBST_DIR)\SBST_TC16P_CoreTest.o" "$(OBJ_SBST_DIR)\SBST_TC16P_ISG.o" "$(OBJ_SBST_DIR)\SBST_TC16P_TestCode.o" "$(OBJ_SBST_DIR)\SBST_RelocTable.o"
else
SBST_OBJ += "$(OBJ_SBST_DIR)\SBST_Kernel_CoreTest.o" "$(OBJ_SBST_DIR)\SBST_Kernel_ISG.o" "$(OBJ_SBST_DIR)\SBST_Kernel_TestCode.o" "$(OBJ_SBST_DIR)\SBST_TC16E_TestCode.o" "$(OBJ_SBST_DIR)\SBST_RelocTable.o"
endif
endif

endif

APPL_OBJ += $(APPL_OBJDIR)/Sl_Ipc.o

APPL_OBJ += $(APPL_OBJDIR)/TstM.o 

ifeq ($(SAFEWDGIF_DELIVERY),ON)
APPL_OBJ += $(APPL_OBJDIR)/SafeWdgM.o  
endif

APPL_OBJ += $(APPL_OBJDIR)/Sl_FlsErrPtrn.o

ifeq ($(SAFEWDGEXTTLF_DELIVERY),ON)
APPL_OBJ += $(APPL_OBJDIR)/SafeWdgExt_Config.o $(APPL_OBJDIR)/SafeWdgExtTlf_UcHandler.o $(APPL_OBJDIR)/Spi_Irq.o
endif

ifeq ($(SAFEWDGEXTCIC_DELIVERY),ON)
APPL_OBJ += $(APPL_OBJDIR)/SafeWdgExt_Config.o $(APPL_OBJDIR)/SafeWdgExtCic_Helper.o $(APPL_OBJDIR)/SafeWdgExtCic_UcHandler.o 
endif

ifeq ($(PMUECCEDCTST_DELIVERY),ON)
APPL_OBJ += $(APPL_OBJDIR)/PmuEccEdcTst_PtrnDef.o
endif


ifeq ($(SAFETLIB_STANDALONE), TRUE)
all: $(APPL_OUTDIR)/$(APPL_TARGET).elf
else
all: $(APPL_OBJ) $(SAFETLIBCD_LIB)/SafeTlibCD.a $(SAFEWDG_LIB)/SafeWdgCD.a
endif

PHONY: clean
clean:
	@-rm -f $(APPL_OBJ)
	@-rm -f $(APPL_OBJDIR)/*.src
	@-rm -f $(APPL_OUTDIR)/*.elf
	@-rm -f $(APPL_OUTDIR)/*.hex
	@-rm -f $(APPL_OUTDIR)/*.map
	@-rm -f $(APPL_OUTDIR)/*.mdf
	@-rm -f $(SAFETLIBCD_LIB)/*.a
	@-rm -f $(SAFEWDG_LIB)/*.a


### Build instructions
$(APPL_OUTDIR)/$(APPL_TARGET).elf : $(APPL_OBJ) $(SAFETLIBCD_LIB)/SafeTlibCD.a $(SAFEWDG_LIB)/SafeWdgCD.a
	@echo Linking $(APPL_OUTDIR)/$(APPL_TARGET).elf for TC$(STL_TARGET)x
	@$(LINKER) $(APPL_OBJ) $(SAFETLIBCD_LIB)/SafeTlibCD.a $(SAFEWDG_LIB)/SafeWdgCD.a $(OPT_LKLC) $(SBST_OBJ)

### SafeWdgExt_Config
$(APPL_OBJDIR)/SafeWdgExt_Config.o: $(APPL_BASE_DIR)/src/SafeWdgExt_Config.c
	@echo Compiling $(APPL_BASE_DIR)/src/SafeWdgExt_Config.c
	@$(CCTC) -o $(APPL_OBJDIR)/SafeWdgExt_Config.o -I$(APPL_BASE_DIR)/cfg $(STD_OPT_APPL) $(APPL_BASE_DIR)/src/SafeWdgExt_Config.c  
	@-rm -f $(APPL_OBJDIR)/SafeWdgExt_Config.ers	
	
### SafeWdgExtCic_Helper
$(APPL_OBJDIR)/SafeWdgExtCic_Helper.o: $(APPL_BASE_DIR)/src/SafeWdgExtCic_Helper.c
	@echo Compiling $(APPL_BASE_DIR)/src/SafeWdgExtCic_Helper.c
	@$(CCTC) -o $(APPL_OBJDIR)/SafeWdgExtCic_Helper.o -I$(APPL_BASE_DIR)/cfg $(STD_OPT_APPL) $(APPL_BASE_DIR)/src/SafeWdgExtCic_Helper.c  
	@-rm -f $(APPL_OBJDIR)/SafeWdgExtCic_Helper.ers	

### SafeWdgExtCic_UcHandler	
$(APPL_OBJDIR)/SafeWdgExtCic_UcHandler.o: $(APPL_BASE_DIR)/src/SafeWdgExtCic_UcHandler.c
	@echo Compiling $(APPL_BASE_DIR)/src/SafeWdgExtCic_UcHandler.c
	@$(CCTC) -o $(APPL_OBJDIR)/SafeWdgExtCic_UcHandler.o -I$(APPL_BASE_DIR)/cfg $(STD_OPT_APPL) $(APPL_BASE_DIR)/src/SafeWdgExtCic_UcHandler.c  
	@-rm -f $(APPL_OBJDIR)/SafeWdgExtCic_UcHandler.ers	
	
### SafeWdgExtTlf_UcHandler	
$(APPL_OBJDIR)/SafeWdgExtTlf_UcHandler.o: $(APPL_BASE_DIR)/src/SafeWdgExtTlf_UcHandler.c
	@echo Compiling $(APPL_BASE_DIR)/src/SafeWdgExtTlf_UcHandler.c
	@$(CCTC) -o $(APPL_OBJDIR)/SafeWdgExtTlf_UcHandler.o -I$(APPL_BASE_DIR)/cfg $(STD_OPT_APPL) $(APPL_BASE_DIR)/src/SafeWdgExtTlf_UcHandler.c  
	@-rm -f $(APPL_OBJDIR)/SafeWdgExtTlf_UcHandler.ers	
	
### Application Callback
$(APPL_OBJDIR)/AppCbk.o: $(APPL_BASE_DIR)/src/AppCbk.c
	@echo Compiling $(APPL_BASE_DIR)/src/AppCbk.c
	@$(CCTC) -o $(APPL_OBJDIR)/AppCbk.o $(STD_OPT_APPL) $(APPL_BASE_DIR)/src/AppCbk.c  
	@-rm -f $(APPL_OBJDIR)/AppCbk.ers

### Reference Application	
$(APPL_OBJDIR)/RefApp.o: $(APPL_BASE_DIR)/src/RefApp.c
	@echo Compiling $(APPL_BASE_DIR)/src/RefApp.c
	@$(CCTC) -o $(APPL_OBJDIR)/RefApp.o $(SAFETLIB_STANDALONE_USED) $(STD_OPT_APPL) $(APPL_BASE_DIR)/src/RefApp.c  
	@-rm -f $(APPL_OBJDIR)/RefApp.ers

	
### SMU Interrupt Handler
$(APPL_OBJDIR)/SmuIntHandler.o: $(APPL_BASE_DIR)/src/SmuIntHandler.c
	@echo Compiling $(APPL_BASE_DIR)/src/SmuIntHandler.c
	@$(CCTC) -o $(APPL_OBJDIR)/SmuIntHandler.o $(STD_OPT_APPL) $(APPL_BASE_DIR)/src/SmuIntHandler.c  
	@-rm -f $(APPL_OBJDIR)/SmuIntHandler.ers

### Test Print	
$(APPL_OBJDIR)/Test_Print.o: $(APPL_BASE_DIR)/src/Test_Print.c
	@echo Compiling $(APPL_BASE_DIR)/src/Test_Print.c
	@$(CCTC) -o $(APPL_OBJDIR)/Test_Print.o $(STD_OPT_APPL) $(APPL_BASE_DIR)/src/Test_Print.c  
	@-rm -f $(APPL_OBJDIR)/Test_Print.ers

### Test Time
$(APPL_OBJDIR)/Test_Time.o: $(APPL_BASE_DIR)/src/Test_Time.c
	@echo Compiling $(APPL_BASE_DIR)/src/Test_Time.c
	@$(CCTC) -o $(APPL_OBJDIR)/Test_Time.o $(STD_OPT_APPL) $(APPL_BASE_DIR)/src/Test_Time.c  
	@-rm -f $(APPL_OBJDIR)/Test_Time.ers

$(APPL_OBJDIR)/Stm_Irq.o: $(APPL_BASE_DIR)/src/Stm_Irq.c
	@echo Compiling $(APPL_BASE_DIR)/src/Stm_Irq.c
	@$(CCTC) -o $(APPL_OBJDIR)/Stm_Irq.o $(STD_OPT_APPL) $(APPL_BASE_DIR)/src/Stm_Irq.c  
	@-rm -f $(APPL_OBJDIR)/Stm_Irq.ers

### MCAL Library	
$(APPL_OBJDIR)/Mcal_WdgLib.o: $(COMMON)/Mcal_WdgLib.c
	@echo Compiling $(COMMON)/Mcal_WdgLib.c
	@$(CCTC) -o $(APPL_OBJDIR)/Mcal_WdgLib.o $(STD_OPT_APPL) $(COMMON)/Mcal_WdgLib.c  
	@-rm -f $(APPL_OBJDIR)/Mcal_WdgLib.ers
### MCAL TC Library	
$(APPL_OBJDIR)/Mcal_TcLib.o: $(COMMON)/Mcal_TcLib.c
	@echo Compiling $(COMMON)/Mcal_TcLib.c
	@$(CCTC) -o $(APPL_OBJDIR)/Mcal_TcLib.o $(STD_OPT_APPL) $(COMMON)/Mcal_TcLib.c  
	@-rm -f $(APPL_OBJDIR)/Mcal_TcLib.ers

### MCAL Library	
$(APPL_OBJDIR)/Sl_Ipc.o: $(COMMON)/Sl_Ipc.c
	@echo Compiling $(COMMON)/Sl_Ipc.c
	@$(CCTC) -o $(APPL_OBJDIR)/Sl_Ipc.o $(STD_OPT_APPL) $(COMMON)/Sl_Ipc.c  
	@-rm -f $(APPL_OBJDIR)/Sl_Ipc.ers
	
### Test Manager	
$(APPL_OBJDIR)/TstM.o: $(TSTM_BASE_DIR)/src/TstM.c
	@echo Compiling $(TSTM_BASE_DIR)/src/TstM.c
	@$(CCTC) -o $(APPL_OBJDIR)/TstM.o $(STD_OPT_APPL) $(TSTM_BASE_DIR)/src/TstM.c  
	@-rm -f $(APPL_OBJDIR)/TstM.ers
	
### Safe Watchdog Manager	
$(APPL_OBJDIR)/SafeWdgM.o: $(SWDGM_BASE_DIR)/src/SafeWdgM.c
	@echo Compiling $(SWDGM_BASE_DIR)/src/SafeWdgM.c
	@$(CCTC) -o $(APPL_OBJDIR)/SafeWdgM.o $(STD_OPT_APPL) $(SWDGM_BASE_DIR)/src/SafeWdgM.c  
	@-rm -f $(APPL_OBJDIR)/SafeWdgM.ers


### ExtCic_Handler	
$(APPL_OBJDIR)/ExtCic_Handler.o: $(SWDGM_BASE_DIR)/src/ExtCic_Handler.c
	@echo Compiling $(SWDGM_BASE_DIR)/src/ExtCic_Handler.c
	@$(CCTC) -o $(APPL_OBJDIR)/ExtCic_Handler.o $(STD_OPT_APPL) $(SWDGM_BASE_DIR)/src/ExtCic_Handler.c  
	@-rm -f $(APPL_OBJDIR)/ExtCic_Handler.ers	

### Spi_Irq	
$(APPL_OBJDIR)/Spi_Irq.o: $(SWDGM_BASE_DIR)/src/Spi_Irq.c
	@echo Compiling $(SWDGM_BASE_DIR)/src/Spi_Irq.c
	@$(CCTC) -o $(APPL_OBJDIR)/Spi_Irq.o $(STD_OPT_APPL) $(SWDGM_BASE_DIR)/src/Spi_Irq.c  
	@-rm -f $(APPL_OBJDIR)/Spi_Irq.ers		
	
### IRQ	
$(APPL_OBJDIR)/Irq.o: $(APPL_BASE_DIR)/src/Irq.c
	@echo Compiling $(APPL_BASE_DIR)/src/Irq.c
	@$(CCTC) -o $(APPL_OBJDIR)/Irq.o $(STD_OPT_APPL) $(APPL_BASE_DIR)/src/Irq.c
	@-rm -f $(APPL_OBJDIR)/Irq.ers
  
### MCAL	
$(APPL_OBJDIR)/Mcal.o: $(APPL_BASE_DIR)/src/Mcal.c
	@echo Compiling $(APPL_BASE_DIR)/src/Mcal.c
	@$(CCTC) -o $(APPL_OBJDIR)/Mcal.o $(SAFETLIB_USED) $(STD_OPT_APPL) $(APPL_BASE_DIR)/src/Mcal.c  
	@-rm -f $(APPL_OBJDIR)/Mcal.ers
  
### MCAL	
$(APPL_OBJDIR)/Mcal_Trap.o: $(APPL_BASE_DIR)/src/Mcal_Trap.c
	@echo Compiling $(APPL_BASE_DIR)/src/Mcal_Trap.c
	@$(CCTC) -o $(APPL_OBJDIR)/Mcal_Trap.o $(STD_OPT_APPL) $(APPL_BASE_DIR)/src/Mcal_Trap.c  
	@-rm -f $(APPL_OBJDIR)/Mcal_Trap.ers
	
### FLS Error Pattern	
$(APPL_OBJDIR)/Sl_FlsErrPtrn.o: $(COMMON)/Sl_FlsErrPtrn.c
	@echo Compiling $(COMMON)/Sl_FlsErrPtrn.c
	@$(CCTC) -o $(APPL_OBJDIR)/Sl_FlsErrPtrn.o $(STD_OPT_APPL) $(COMMON)/Sl_FlsErrPtrn.c  
	@-rm -f $(APPL_OBJDIR)/Sl_FlsErrPtrn.ers

### Pattern file for PFlash tests		
$(APPL_OBJDIR)/PmuEccEdcTst_PtrnDef.o: $(MTL_DIR)/src/PmuEccEdcTst_PtrnDef.c
	@echo Compiling $(MTL_DIR)/src/PmuEccEdcTst_PtrnDef.c
	@$(CCTC) -o $(APPL_OBJDIR)/PmuEccEdcTst_PtrnDef.o $(STD_OPT_STL) $(MTL_DIR)/src/PmuEccEdcTst_PtrnDef.c  
	@-rm -f $(APPL_OBJDIR)/PmuEccEdcTst_PtrnDef.ers