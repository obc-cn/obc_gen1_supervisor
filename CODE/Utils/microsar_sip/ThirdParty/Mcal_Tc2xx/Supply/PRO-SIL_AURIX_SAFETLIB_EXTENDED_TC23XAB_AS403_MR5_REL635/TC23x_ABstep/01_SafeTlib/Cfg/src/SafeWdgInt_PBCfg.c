
/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2013)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME  :  SafeWdgInt_PBCfg.c                                           **
**                                                                            **
**  DATE, TIME: 2017-08-29, 11:38:13                                          **
**                                                                            **
**  GENERATOR : Build b141014-0350                                            **
**                                                                            **
**  BSW MODULE DECRIPTION : SafeWdgInt.bmd/xdm                                **
**                                                                            **
**  VARIANT   : VariantPB                                                     **
**                                                                            **
**  PLATFORM  : Infineon Aurix                                                **
**                                                                            **
**  COMPILER  : Tasking / GNU / Diab                                          **
**                                                                            **
**  AUTHOR    : DL-BLR-ATV-STC                                                **
**                                                                            **                                                                      
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : SafeWdgInt configuration file                              **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: no                                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "SafeWdgInt.h"

/*******************************************************************************
**                      Private Macro definition                              **
*******************************************************************************/


/*******************************************************************************
**                      Configuration Options                                 **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/


/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
#define IFX_SWDG_START_SEC_POSTBUILDCFG_ASIL_B
#include "Ifx_MemMap.h"

/* internal watchdog configuration */
const SafeWdgInt_ConfigType SafeWdgInt_ConfigRoot[] =
{
  {
    0u,  45u, /* timeout settings */

    /* Signature table */
    {
      {
        {MCAL_WDT_PW_FIXPART_READ, 0x3f9b7e93u},
        {MCAL_WDT_PW_FIXPART_READ+0x300u, 0x5309b92fu},
        {MCAL_WDT_PW_FIXPART_READ+0x500u, 0x667ca5e7u},
        {MCAL_WDT_PW_FIXPART_READ+0x700u, 0xaee625bu},
        {MCAL_WDT_PW_FIXPART_READ+0x900u, 0xbf6874e5u},
        {MCAL_WDT_PW_FIXPART_READ+0xb00u, 0xd3fab359u},
        {MCAL_WDT_PW_FIXPART_READ+0xd00u, 0xe68faf91u},
        {MCAL_WDT_PW_FIXPART_READ+0xf00u, 0x8a1d682du},
        {MCAL_WDT_PW_FIXPART_READ+0xe00u, 0x4f0e231fu},
        {MCAL_WDT_PW_FIXPART_READ+0xc00u, 0x239ce4a3u},
        {MCAL_WDT_PW_FIXPART_READ+0xa00u, 0x16e9f86bu},
        {MCAL_WDT_PW_FIXPART_READ+0x800u, 0x7a7b3fd7u},
        {MCAL_WDT_PW_FIXPART_READ+0x600u, 0xcffd2969u},
        {MCAL_WDT_PW_FIXPART_READ+0x400u, 0xa36feed5u},
        {MCAL_WDT_PW_FIXPART_READ+0x200u, 0x961af21du},
        {MCAL_WDT_PW_FIXPART_READ+0x100u, 0xfa8835a1u}
      }
    }
  }    
};

#define IFX_SWDG_STOP_SEC_POSTBUILDCFG_ASIL_B
#include "Ifx_MemMap.h"

/*******************************************************************************
**                      Global Funtion Declarations                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
