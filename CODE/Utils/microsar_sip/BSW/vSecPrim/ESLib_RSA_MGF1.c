/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file ESLib_RSA_MGF1.c
 *        \brief Crypto library - RSA Mask Generation Function
 *
 *      \details Helper functions for RSA Mask generation according to PKCS #1 v2.2
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/
#define ESLIB_RSA_MGF1_SOURCE

/***********************************************************************************************************************
 *  INCLUDES
 **********************************************************************************************************************/

#include "ESLib_Helper.h"
#include "ESLib_types.h"

#include "actUtilities.h"

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

#if ((VSECPRIM_RSA_OAEP_ENABLED == STD_ON) || (VSECPRIM_RSA_PSS_ENABLED == STD_ON)) /* COV_VSECPRIM_NO_SAFE_CONFIG XF xf xf */

#if (VSECPRIM_RSA_HASHALGORITHM_SHA1_ENABLED == STD_ON)
# include "actISHA.h"
#endif /* (VSECPRIM_RSA_HASHALGORITHM_SHA1_ENABLED == STD_ON) */

#if (VSECPRIM_RSA_HASHALGORITHM_SHA2_256_ENABLED == STD_ON)
# include "actISHA2_32.h"
#endif /* (VSECPRIM_RSA_HASHALGORITHM_SHA2_256_ENABLED == STD_ON) */

#if (VSECPRIM_RSA_HASHALGORITHM_RIPEMD160_ENABLED == STD_ON)
# include "actIRMD160.h"
#endif /* (VSECPRIM_RSA_HASHALGORITHM_RIPEMD160_ENABLED == STD_ON) */


/***********************************************************************************************************************
 *  DEFINES
 **********************************************************************************************************************/
#define ESL_RSA_MGF1_SHA1_INIT_HASH_FCT(workspace)                            actSHAInit(&((workspace)->wsSHA1))
#define ESL_RSA_MGF1_SHA1_UPDATE_HASH_FCT(workspace, inputSize, input)        actSHAUpdate(&((workspace)->wsSHA1), (VSECPRIM_P2CONST_PARA(actU8))(input), (actLengthType)(inputSize), (workspace)->header.watchdog)
#define ESL_RSA_MGF1_SHA1_FINALIZE_HASH_FCT(workspace, messageDigest)         actSHAFinalize(&((workspace)->wsSHA1), (VSECPRIM_P2VAR_PARA(actU8))(messageDigest),  (workspace)->header.watchdog)

#define ESL_RSA_MGF1_RMD160_INIT_HASH_FCT(workspace)                          actRMD160Init((VSECPRIM_P2VAR_PARA(actRMD160STRUCT))((workspace)->wsRIPEMD160))
#define ESL_RSA_MGF1_RMD160_UPDATE_HASH_FCT(workspace, inputSize, input)      actRMD160Update((VSECPRIM_P2VAR_PARA(actRMD160STRUCT))((workspace)->wsRIPEMD160), (VSECPRIM_P2CONST_PARA(actU8))(input), (int)(inputSize),  (workspace)->header.watchdog)
#define ESL_RSA_MGF1_RMD160_FINALIZE_HASH_FCT(workspace, messageDigest)       actRMD160Finalize((VSECPRIM_P2VAR_PARA(actRMD160STRUCT))((workspace)->wsRIPEMD160), (VSECPRIM_P2VAR_PARA(actU8))(messageDigest), (workspace)->header.watchdog)

#define ESL_RSA_MGF1_SHA2_256_INIT_HASH_FCT(workspace)                        actSHA256Init(&((workspace)->wsSHA256))
#define ESL_RSA_MGF1_SHA2_256_UPDATE_HASH_FCT(workspace, inputSize, input)    actSHA256Update(&((workspace)->wsSHA256), (VSECPRIM_P2CONST_PARA(actU8))(input), (actLengthType)(inputSize), (workspace)->header.watchdog)
#define ESL_RSA_MGF1_SHA2_256_FINALIZE_HASH_FCT(workspace, messageDigest)     actSHA256Finalize(&((workspace)->wsSHA256), (VSECPRIM_P2VAR_PARA(actU8))(messageDigest),  (workspace)->header.watchdog)

 /***********************************************************************************************************************
 *  LOCAL DATA
 **********************************************************************************************************************/
#define VSECPRIM_START_SEC_CONST_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

 /** Masks for setting leftmost bits of octet to zero */
VSECPRIM_ROM(VSECPRIM_NONE, eslt_Byte) zeroMasks[8] = {
  0xFFu, 0x01u, 0x03u, 0x07u, 0x0Fu, 0x1Fu, 0x3Fu, 0x7Fu
};

#define VSECPRIM_STOP_SEC_CONST_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/***********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/
#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/***********************************************************************************************************************
 *  esl_callInitHashFunction
 **********************************************************************************************************************/
/*! \brief     Calls the init function of the selected hash algorithm.
 *  \details    This calls the init function of the underlying library based on the given hash algorithm.
 *  \param[in]  hashAlgorithm Algorithm which shall be called.
 *  \param[in]  wsHash  Pointer to hash workspace
 *  \return     Result of operation.
 *  \retval     ESL_ERC_NO_ERROR  Operation successful
 *  \retval     ESL_ERC_PARAMETER_INVALID  Hash algorithm is invalid.
 **********************************************************************************************************************/
VSECPRIM_LOCAL_INLINE_FUNC(eslt_ErrorCode) esl_callInitHashFunction(eslt_HashAlgorithm hashAlgorithm,
                                                                    VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRefType) wsHash);

/***********************************************************************************************************************
 *  esl_callUpdateHashFunction
 **********************************************************************************************************************/
/*! \brief     Calls the update function of the selected hash algorithm.
 *  \details    This calls the update function of the underlying library based on the given hash algorithm.
 *  \param[in]  hashAlgorithm Algorithm which shall be called.
 *  \param[in]  wsHash  Pointer to hash workspace
 *  \param[in]  length Length of the buffer which is pointed to by data.
 *  \param[in]  data Pointer to the data, which shall be hashed.
 *  \return     Result of operation.
 *  \retval     ESL_ERC_NO_ERROR  Operation successful
 *  \retval     ESL_ERC_PARAMETER_INVALID  Hash algorithm is invalid.
 **********************************************************************************************************************/
VSECPRIM_LOCAL_INLINE_FUNC(eslt_ErrorCode) esl_callUpdateHashFunction(eslt_HashAlgorithm hashAlgorithm,
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRefType) wsHash,
  eslt_Length length, VSECPRIM_P2CONST_PARA(eslt_Byte) data);

/***********************************************************************************************************************
 *  esl_callFinishHashFunction
 **********************************************************************************************************************/
/*! \brief     Calls the finish function of the selected hash algorithm.
 *  \details    This calls the finish function of the underlying library based on the given hash algorithm.
 *  \param[in]  hashAlgorithm Algorithm which shall be called.
 *  \param[in]  wsHash  Pointer to hash workspace
 *  \param[out] output  Buffer for intermediate hash, has to be large enough to hold complete hash value
 *  \return     Result of operation.
 *  \retval     ESL_ERC_NO_ERROR  Operation successful
 *  \retval     ESL_ERC_PARAMETER_INVALID  Hash algorithm is invalid.
 **********************************************************************************************************************/
VSECPRIM_LOCAL_INLINE_FUNC(eslt_ErrorCode) esl_callFinishHashFunction(eslt_HashAlgorithm hashAlgorithm,
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRefType) wsHash,
  VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/***********************************************************************************************************************
 *  esl_getDigestLengthOfAlgorithm
 **********************************************************************************************************************/
/*! \brief     Returns the Digest Length of the given algorithm.
 *  \details    Based on the hash Algorithm, the function returns the digest length of the hash algorithm.
 *  \param[in]  hashAlgorithm Algorithm of which the digest length shall be returned
 *  \return     Digest length of the selected algorithm
 **********************************************************************************************************************/
VSECPRIM_LOCAL_INLINE_FUNC(eslt_Length) esl_getDigestLengthOfAlgorithm(eslt_HashAlgorithm hashAlgorithm);

/***********************************************************************************************************************
 *  esl_generateMaskMGF1
 **********************************************************************************************************************/
/*! \brief     Mask generation function (MGF1)
 *  \details    MGF1 implementation as described in appendix B.2.1 of PKCS#1 2.2
 *              Based on selected hash function, this function generates a pseudorandom output string of arbitrary length
 *              depending on an input string (seed) of variable length
 *  \param[in]  hashAlgorithm Algorithm to generate Mask
 *  \param[in,out] wsHash  Pointer to hash workspace
 *  \param[in,out] tempHash  Temporary buffer for intermediate hash, has to be large enough to hold complete hash value
 *  \param[in]  seedLength  Length input seed
 *  \param[in]  seed  Input seed from which mask is generated
 *  \param[in]  maskLength  Length of mask to be generated
 *  \param[out] mask  Output buffer for generated mask, has to be large enough to hold requested length
 *  \return     Result of operation
 *  \retval     ESL_ERC_NO_ERROR  Operation successful
 *  \retval     ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or hash algorithm is invalid.
 *  \retval     ESL_ERC_WS_STATE_INVALID  Invalid state
 *  \retval     ESL_ERC_WS_TOO_SMALL  Work space too small
 **********************************************************************************************************************/
VSECPRIM_LOCAL_FUNC(eslt_ErrorCode) esl_generateMaskMGF1(eslt_HashAlgorithm hashAlgorithm,
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRefType) wsHash,
  VSECPRIM_P2VAR_PARA(eslt_Byte) tempHash,
  eslt_Length seedLength, VSECPRIM_P2CONST_PARA(eslt_Byte) seed, eslt_Length maskLength, VSECPRIM_P2VAR_PARA(eslt_Byte) mask);


/***********************************************************************************************************************
 *  LOCAL FUNCTION DEFINITIONS
 **********************************************************************************************************************/

/**********************************************************************************************************************
 *  esl_getDigestLengthOfAlgorithm()
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSECPRIM_LOCAL_INLINE_FUNC(eslt_Length) esl_getDigestLengthOfAlgorithm(eslt_HashAlgorithm hashAlgorithm)
{
  eslt_Length digestLength;
  if (hashAlgorithm == ESL_HA_SHA2_256)
  {
    digestLength = ESL_SIZEOF_SHA256_DIGEST;
  }
  else
  {
    /* SHA1 and RIPEMD160 have the same length of digest */
    digestLength = ESL_SIZEOF_SHA1_DIGEST;
  }

  return digestLength;
}


/**********************************************************************************************************************
 *  esl_callInitHashFunction()
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSECPRIM_LOCAL_INLINE_FUNC(eslt_ErrorCode) esl_callInitHashFunction(eslt_HashAlgorithm hashAlgorithm,
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRefType) wsHash)
{
  eslt_ErrorCode rVal = ESL_ERC_NO_ERROR;
  switch (hashAlgorithm)
  {
#if (VSECPRIM_RSA_HASHALGORITHM_SHA1_ENABLED == STD_ON)
  case ESL_HA_SHA1:
  {
    (void)ESL_RSA_MGF1_SHA1_INIT_HASH_FCT((VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA1))wsHash);
    break;
  }
#endif /* (VSECPRIM_RSA_HASHALGORITHM_SHA1_ENABLED == STD_ON) */
#if (VSECPRIM_RSA_HASHALGORITHM_SHA2_256_ENABLED == STD_ON)
  case ESL_HA_SHA2_256:
  {
    (void)ESL_RSA_MGF1_SHA2_256_INIT_HASH_FCT((VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA256))wsHash);
    break;
  }
#endif /* (VSECPRIM_RSA_HASHALGORITHM_SHA2_256_ENABLED == STD_ON) */
#if (VSECPRIM_RSA_HASHALGORITHM_RIPEMD160_ENABLED == STD_ON)
  case ESL_HA_RMD160:
  {
    (void)ESL_RSA_MGF1_RMD160_INIT_HASH_FCT((VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160))wsHash);
    break;
  }
#endif /* (VSECPRIM_RSA_HASHALGORITHM_RIPEMD160_ENABLED == STD_ON) */
  default:
    rVal = ESL_ERC_INPUT_INVALID;
    break;
  }

  return rVal;
}


/**********************************************************************************************************************
 *  esl_callUpdateHashFunction()
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSECPRIM_LOCAL_INLINE_FUNC(eslt_ErrorCode) esl_callUpdateHashFunction(eslt_HashAlgorithm hashAlgorithm,
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRefType) wsHash,
  eslt_Length length, VSECPRIM_P2CONST_PARA(eslt_Byte) data)
{
  eslt_ErrorCode rVal = ESL_ERC_NO_ERROR;
  switch (hashAlgorithm)
  {
#if (VSECPRIM_RSA_HASHALGORITHM_SHA1_ENABLED == STD_ON)
  case ESL_HA_SHA1:
  {
    (void)ESL_RSA_MGF1_SHA1_UPDATE_HASH_FCT((VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA1))wsHash, length, data);
    break;
  }
#endif /* (VSECPRIM_RSA_HASHALGORITHM_SHA1_ENABLED == STD_ON) */
#if (VSECPRIM_RSA_HASHALGORITHM_SHA2_256_ENABLED == STD_ON)
  case ESL_HA_SHA2_256:
  {
    (void)ESL_RSA_MGF1_SHA2_256_UPDATE_HASH_FCT((VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA256))wsHash, length, data);
    break;
  }
#endif /* (VSECPRIM_RSA_HASHALGORITHM_SHA2_256_ENABLED == STD_ON) */
#if (VSECPRIM_RSA_HASHALGORITHM_RIPEMD160_ENABLED == STD_ON)
  case ESL_HA_RMD160:
  {
    (void)ESL_RSA_MGF1_RMD160_UPDATE_HASH_FCT((VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160))wsHash, length, data);
    break;
  }
#endif /* (VSECPRIM_RSA_HASHALGORITHM_RIPEMD160_ENABLED == STD_ON) */
  default:
  {
    rVal = ESL_ERC_INPUT_INVALID;
    break;
  }
  }

  return rVal;
}

/**********************************************************************************************************************
 *  esl_callFinishHashFunction()
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
VSECPRIM_LOCAL_INLINE_FUNC(eslt_ErrorCode) esl_callFinishHashFunction(eslt_HashAlgorithm hashAlgorithm,
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRefType) wsHash,
  VSECPRIM_P2VAR_PARA(eslt_Byte) output)
{
  eslt_ErrorCode rVal = ESL_ERC_NO_ERROR;
  switch (hashAlgorithm)
  {
#if (VSECPRIM_RSA_HASHALGORITHM_SHA1_ENABLED == STD_ON)
  case ESL_HA_SHA1:
  {
    (void)ESL_RSA_MGF1_SHA1_FINALIZE_HASH_FCT((VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA1))wsHash, output);
    break;
  }
#endif /* (VSECPRIM_RSA_HASHALGORITHM_SHA1_ENABLED == STD_ON) */
#if (VSECPRIM_RSA_HASHALGORITHM_SHA2_256_ENABLED == STD_ON)
  case ESL_HA_SHA2_256:
  {
    (void)ESL_RSA_MGF1_SHA2_256_FINALIZE_HASH_FCT((VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA256))wsHash, output);
    break;
  }
#endif /* (VSECPRIM_RSA_HASHALGORITHM_SHA2_256_ENABLED == STD_ON) */
#if (VSECPRIM_RSA_HASHALGORITHM_RIPEMD160_ENABLED == STD_ON)
  case ESL_HA_RMD160:
  {
    (void)ESL_RSA_MGF1_RMD160_FINALIZE_HASH_FCT((VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160))wsHash, output);
    break;
  }
#endif /* (VSECPRIM_RSA_HASHALGORITHM_RIPEMD160_ENABLED == STD_ON) */
  default:
  {
    rVal = ESL_ERC_INPUT_INVALID;
    break;
  }
  }

  return rVal;
}

/**********************************************************************************************************************
 *  esl_generateMaskMGF1()
 **********************************************************************************************************************/
/*!
 *Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
VSECPRIM_LOCAL_FUNC(eslt_ErrorCode) esl_generateMaskMGF1(eslt_HashAlgorithm hashAlgorithm,
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRefType) wsHash,
  VSECPRIM_P2VAR_PARA(eslt_Byte) tempHash,
  eslt_Length seedLength, VSECPRIM_P2CONST_PARA(eslt_Byte) seed, eslt_Length maskLength, VSECPRIM_P2VAR_PARA(eslt_Byte) mask)
{
  eslt_ErrorCode returnValue;
  eslt_Size32 counter;
  eslt_Byte octetCounter[4u];
  eslt_Length remainder;
  eslt_Length offset;
  eslt_Length passLength;

  /* B.2.1, Step 3: Initialize counter value */
  counter = 0u;
  offset = 0u;
  remainder = maskLength;

  returnValue = ESL_ERC_NO_ERROR;

  /* B.2.1, Step 3: Generate requested length */
  while (remainder > 0u)
  {
    if (ESL_ERC_NO_ERROR == returnValue)
    {
      /* Initialize hash calculation */
      returnValue = esl_callInitHashFunction(hashAlgorithm, wsHash);
    }

    if (ESL_ERC_NO_ERROR == returnValue)
    {
      /* B.2.1, Step 3b: Hash given seed value */
      returnValue = esl_callUpdateHashFunction(hashAlgorithm, wsHash, seedLength, seed);
    }

    if (ESL_ERC_NO_ERROR == returnValue)
    {
      /* B.2.1, Step 3a: Convert counter value to big-endian octet string */
      actU32toBE(octetCounter, counter);

      /* B.2.1, Step 3b: Hash octet string of counter */
      returnValue = esl_callUpdateHashFunction(hashAlgorithm, wsHash, sizeof(octetCounter), octetCounter);
    }

    if (ESL_ERC_NO_ERROR == returnValue)
    {
      /* Finalize hash calculation */
      returnValue = esl_callFinishHashFunction(hashAlgorithm, wsHash, tempHash);
    }

    if (ESL_ERC_NO_ERROR == returnValue)
    {
      /* B.2.1, Step 4: Limit length in this pass, when end of buffer is reached */
      passLength = esl_getDigestLengthOfAlgorithm(hashAlgorithm);
      if (passLength > remainder)
      {
        passLength = remainder;
      }

      /* B.2.1, Steps 3-4: Concatenate (truncated) hash to output */
      actMemcpy(&mask[offset], tempHash, passLength); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */

      /* B.2.1, Step 3: Update counter */
      counter++;
      offset += passLength;
      remainder -= passLength;
    }

    /* Abort loop in case of error */
    if (ESL_ERC_NO_ERROR != returnValue)
    {
      break;
    }
  }

  return returnValue;
}

/***********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 **********************************************************************************************************************/
#if (VSECPRIM_RSA_HASHALGORITHM_RIPEMD160_ENABLED == STD_ON)
/***********************************************************************************************************************
 *  esl_generateMaskMGF1RSARIPEMD160_PSS
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_generateMaskMGF1RSARIPEMD160_PSS(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160) wsHash,
  VSECPRIM_P2VAR_PARA(eslt_Byte) tempHash,
  eslt_Length seedLength, VSECPRIM_P2CONST_PARA(eslt_Byte) seed, eslt_Length maskLength, VSECPRIM_P2VAR_PARA(eslt_Byte) mask)
{
  return esl_generateMaskMGF1(ESL_HA_RMD160, (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRefType)) wsHash, tempHash, seedLength, seed, maskLength, mask);
}
#endif /* (VSECPRIM_RSA_HASHALGORITHM_RIPEMD160_ENABLED == STD_ON) */

#if (VSECPRIM_RSA_HASHALGORITHM_SHA1_ENABLED == STD_ON)
/***********************************************************************************************************************
 *  esl_generateMaskMGF1RSASHA1_PSS
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_generateMaskMGF1RSASHA1_PSS(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA1) wsHash,
  VSECPRIM_P2VAR_PARA(eslt_Byte) tempHash,
  eslt_Length seedLength, VSECPRIM_P2CONST_PARA(eslt_Byte) seed, eslt_Length maskLength, VSECPRIM_P2VAR_PARA(eslt_Byte) mask)
{
  return esl_generateMaskMGF1(ESL_HA_SHA1, (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRefType)) wsHash, tempHash, seedLength, seed, maskLength, mask);
}
#endif /* (VSECPRIM_RSA_HASHALGORITHM_SHA1_ENABLED == STD_ON) */

#if (VSECPRIM_RSA_HASHALGORITHM_SHA2_256_ENABLED == STD_ON)
/***********************************************************************************************************************
 *  esl_generateMaskMGF1RSASHA256_PSS
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_generateMaskMGF1RSASHA256_PSS(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA256) wsHash,
  VSECPRIM_P2VAR_PARA(eslt_Byte) tempHash,
  eslt_Length seedLength, VSECPRIM_P2CONST_PARA(eslt_Byte) seed, eslt_Length maskLength, VSECPRIM_P2VAR_PARA(eslt_Byte) mask)
{
  return esl_generateMaskMGF1(ESL_HA_SHA2_256, (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRefType)) wsHash, tempHash, seedLength, seed, maskLength, mask);
}
#endif /* (VSECPRIM_RSA_HASHALGORITHM_SHA2_256_ENABLED == STD_ON) */

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* ((VSECPRIM_RSA_OAEP_ENABLED == STD_ON) || (VSECPRIM_RSA_PSS_ENABLED == STD_ON)) */

/********************************************************************************************************************** 
 *  END OF FILE: ESLib_RSA_MGF1.c 
 *********************************************************************************************************************/
