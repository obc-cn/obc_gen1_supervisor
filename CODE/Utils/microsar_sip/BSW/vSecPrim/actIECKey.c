/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actIECKey.c
 *        \brief  Implementation file for actIECKey.h
 *
 *      \details This file is part of the embedded systems library cvActLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

/****************************************************************************
 **
 ** This file contains: Implementation file for actIECKey.h
 **
 ** constants:
 **    actEC_PRIVATE_KEY_LENGTH_[curve_id]
 **    actEC_PUBLIC_KEY_LENGTH_[curve_id]
 **
 ** types:
 **
 ** macros:
 **
 ** functions:
 **   actECGetPrivateKeyLength
 **   actECGetPublicKeyLength
 **   actECGenerateKeyPair
 **
 ***************************************************************************/

#include "actIECKey.h"
#include "actECTools.h"
#include "actECKey.h"
#include "actECPoint.h"
#include "actECLengthInfo.h"
#include "actBigNum.h"
#include "actUtilities.h"
#include "actConfig.h"

#if (VSECPRIM_ACTIECKEY_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTIONs:
 **   actECGetPrivateKeyLength
 **   actECGetPublicKeyCompLength
 **
 **  These functions return the key length in bytes
 **  for the desired curve.
 **
 ** input:
 ** - domain:      domain parameter
 **
 ** output:
 ** - returns:    length of key in bytes
 **                 (0, if domain not valid)
 **
 ** assumes:
 **
 ** uses:
 **
 ***************************************************************************/
VSECPRIM_FUNC(int) actECGetPrivateKeyLength(actPROMU8 domain)
{
   int p_bytes, n_bytes;
   actRETURNCODE returncode = actECGetPrimeAndOrderBytes(domain, &p_bytes, &n_bytes);
   return (returncode != actOK) ? 0 : n_bytes;
}

VSECPRIM_FUNC(int) actECGetPublicKeyCompLength(actPROMU8 domain)
{
   int p_bytes, n_bytes;
   actRETURNCODE returncode = actECGetPrimeAndOrderBytes(domain, &p_bytes, &n_bytes);
   return (returncode != actOK) ? 0 : p_bytes;
}

/****************************************************************************
 **
 ** FUNCTION:
 **  actECInitGenerateKeyPair
 **
 **  This function initializes the ECC workspace.
 **
 ** input:
 ** - domain:      domain parameter
 ** - domain_ext:  domain parameter extensions (Montgomery constants etc.)
 ** - speedup_ext: (optional) precomputations (for ECDSA-Sign, -GenKey)
 ** - wksp:        workspace
 ** - wksp_len:    length of workspace in bytes
 **
 ** output:
 ** - wksp:       initialized workspace
 ** - returns:    actEXCEPTION_NULL         if an input parameter is NULL
 **               actEXCEPTION_DOMAIN       domain decoding error
 **               actEXCEPTION_DOMAIN_EXT   domain_ext decoding error
 **               actEXCEPTION_SPEEDUP_EXT  speedup_ext decoding error
 **               actEXCEPTION_MEMORY       wksp_len to small
 **               actOK                     else
 **
 ** assumes:
 **
 ** uses:
 **
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECInitGenerateKeyPair(
   actPROMU8 domain, actPROMU8 domain_ext, actPROMU8 speedup_ext,
   VSECPRIM_P2VAR_PARA(actU8) wksp, int wksp_len)
{
   return actECInit(domain, domain_ext, speedup_ext, actEC_ALGO_FLAG_KG, wksp, wksp_len);
}


/****************************************************************************
 **
 ** FUNCTION:
 **   actECGenerateKeyPair
 **
 **  This function generate an ECC key pair (d, Q), where Q = d * G.
 **
 ** input:
 ** - generate_privatekey:   0, privatekey is passed by the user
 **                          1, privatekey will be generated at random
 ** - privatekey:  buffer to store the private key d
 ** - publickey_x: buffer to store x-coordinate of the public key Q
 ** - publickey_x: buffer to store y-coordinate of the public key Q
 ** - wksp:        workspace
 **
 ** output:
 ** - privatekey:  private key d
 ** - publickey_x: x-coordinate of the public key Q
 ** - publickey_x: y-coordinate of the public key Q
 ** - returns:    actEXCEPTION_NULL       if an input parameter is NULL
 **               actEXCEPTION_PRIVKEY    if d is passed and (d==0||d>=n)
 **               actEXCEPTION_UNKNOWN    internal error (result point
 **                                       not on curve)
 **               actOK                   else
 **
 ** assumes:
 ** - wksp is initialized with actECInitGenerateKeyPair
 **
 ** uses:
 **
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECGenerateKeyPair(
   int generate_privatekey, VSECPRIM_P2VAR_PARA(actU8) privatekey,
   VSECPRIM_P2VAR_PARA(actU8) publickey_x, VSECPRIM_P2VAR_PARA(actU8) publickey_y,
   VSECPRIM_P2VAR_PARA(actU8) wksp)
{
   actRETURNCODE returncode;
   VSECPRIM_P2VAR_PARA(actECCURVE) Curve =
     (VSECPRIM_P2VAR_PARA(actECCURVE))wksp;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) wksp_ptr =
     (VSECPRIM_P2VAR_PARA(actBNDIGIT))(wksp + sizeof(actECCURVE));
   int p_length = Curve->p_field.m_length;
   int p_bytes = Curve->p_field.m_byte_length;
   int n_length = Curve->n_field.m_length;
   int n_bytes = Curve->n_field.m_byte_length;
   actECPOINT Q;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) d;

   int rng_callback_error; /* rng_error is an error code of the actPRNG_GET callback function */

   /* any null pointers ? */
   if ((!privatekey) || (!publickey_x) || (!publickey_y))
   {
      return actEXCEPTION_NULL;
   }

   /* assign temporary bignum workspace */
   wksp_ptr += actECBasicWksp(p_length, n_length); /* this workspace has been used by actECInit() */
   /* d, Q */                           /* adjust workspace pointer */
   d = wksp_ptr;                        wksp_ptr += n_length;
   Q.x = wksp_ptr;                      wksp_ptr += p_length;
   Q.y = wksp_ptr;                      wksp_ptr += p_length;
   Q.z = wksp_ptr;                      wksp_ptr += p_length;

   /* initialization of d */
   if (generate_privatekey)
   {
      /* generate d at random */
      rng_callback_error = actBNModRandomize(d, &(Curve->n_field), Curve->t);
      if (rng_callback_error != actOK) { return rng_callback_error; }
      /* privatekey = d */
      actBNOctetString(privatekey, n_bytes, d, n_length);
   }
   else
   {
      /* d = privatekey */
      actBNSetOctetString(d, n_length, privatekey, n_bytes);
      /* check 0 < d < n before continue */
      if (actBNIsZero(d, n_length) == TRUE)
      {
         return actEXCEPTION_PRIVKEY;
      }
      if (actBNCompare(d, Curve->n_field.m, n_length) >= actCOMPARE_EQUAL)
      {
         return actEXCEPTION_PRIVKEY;
      }
   }

   /*
      call ECkgp and extract affine public key
   */

   /* calculate Q = d * G */
   returncode = actECkgp(d, &Q, Curve, wksp_ptr);

   /* write coordinates to publickey */
   if (returncode == actOK)
   {
      actBNOctetString(publickey_x, p_bytes, Q.x, p_length);
      actBNOctetString(publickey_y, p_bytes, Q.y, p_length);
   }

   /* wipe local d */
   actBNSetZero(d, n_length);

   return returncode;
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTIECKEY_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actIECKey.c
 *********************************************************************************************************************/
