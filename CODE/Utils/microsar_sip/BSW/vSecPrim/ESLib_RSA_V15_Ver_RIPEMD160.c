/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  ESLib_RSA_V15_Ver_RIPEMD160.c
 *        \brief  RSA V1.5 (Sign_RIPEMD160/Verify_RIPEMD160) implementation.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/
#define ESLIB_RSA_V15_VER_RIPEMD160_SOURCE

#include "ESLib.h"
#include "ESLib_types.h"
#include "ESLib_ASN_1.h"

#include "actIRSA.h"
#include "actUtilities.h"
#include "actIRMD160.h"

#if (VSECPRIM_RSA_V15_RIPEMD160_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/***********************************************************************************************************************
 *  DEFINES
 **********************************************************************************************************************/
/** Hash initialization function */
# define ESL_RSA_V15_INIT_HASH_FCT(workspace)                         actRMD160Init((VSECPRIM_P2VAR_PARA(actRMD160STRUCT))((workspace)->wsRIPEMD160))
/** Hash update function */
# define ESL_RSA_V15_UPDATE_HASH_FCT(workspace, inputSize, input)     actRMD160Update((VSECPRIM_P2VAR_PARA(actRMD160STRUCT))((workspace)->wsRIPEMD160), (VSECPRIM_P2CONST_PARA(actU8))(input), (int)(inputSize),  (workspace)->header.watchdog)
/** Hash finalization function */
# define ESL_RSA_V15_FINALIZE_HASH_FCT(workspace, messageDigest)      actRMD160Finalize((VSECPRIM_P2VAR_PARA(actRMD160STRUCT))((workspace)->wsRIPEMD160), (VSECPRIM_P2VAR_PARA(actU8))(messageDigest), (workspace)->header.watchdog)

/****************************************************************************
 ** Types and constants
 ***************************************************************************/
#define VSECPRIM_START_SEC_CONST_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

VSECPRIM_ROM(VSECPRIM_LOCAL, eslt_Byte) eslt_VERIFY_RSA_RIPEMD160_V15_ASN1_DIGESTINFO[] = ASN1_DIGESTINFO_RIPEMD160;

#define VSECPRIM_STOP_SEC_CONST_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 ** Local Functions
 ***************************************************************************/
# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTION:
 **  eslt_ErrorCode
 **  esl_getKeyPairModuleSizeV15SignRIPEMD160(eslt_WorkSpaceRSAver *workSpace,
 **                            eslt_Length          *keyPairModuleSize)
 ** input:
 ** - workSpace
 **
 ** output:
 ** - keyPairModuleSize
 ** - errorCode:
 **      ESL_ERC_NO_ERROR
 **      ESL_ERC_WS_STATE_INVALID
 **      ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 **
 ** assumes:
 ** - publicKeyExponent  < keyPairModule
 **
 ***************************************************************************/
     VSECPRIM_LOCAL_FUNC(eslt_ErrorCode) esl_getKeyPairModuleSizeV15SignRIPEMD160(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace, VSECPRIM_P2VAR_PARA(eslt_Length) keyPairModuleSize)
{

  eslt_ErrorCode returnValue;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader) wsHeaderV15;

  returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* get underlying workspace pointer */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte)) workSpace;
    wsHeaderV15 = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader)) wsRAW;
    *keyPairModuleSize = (eslt_Length) (((eslt_Length) wsRAW[ESL_WS_RSA_V15_SV_RIPEMD160_BUFFERLENGTH] << 8) + (wsRAW[ESL_WS_RSA_V15_SV_RIPEMD160_BUFFERLENGTH + 1]));

    if ((ESL_WST_ALGO_RSA | ESL_WST_M_RUNNING) != (wsHeaderV15->status & (ESL_WST_M_ALGO | ESL_WST_M_RUNNING)))
    {
      returnValue = ESL_ERC_WS_STATE_INVALID;
    }
  }

  return returnValue;
}

/****************************************************************************
 **
 ** FUNCTION:
 **  eslt_ErrorCode
 **  esl_getWorkspaceRIPEMD160(eslt_WorkSpaceRSAver *workSpace,
 **                        eslt_WorkSpaceRIPEMD160  **wsRIPEMD160)
 ** input:
 ** - workSpace
 **
 ** output:
 ** - wsRIPEMD160
 ** - errorCode:
 **      ESL_ERC_NO_ERROR
 **      ESL_ERC_WS_STATE_INVALID
 **      ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 **
 ** assumes:
 ** - publicKeyExponent  < keyPairModule
 **
 ***************************************************************************/
VSECPRIM_LOCAL_FUNC(eslt_ErrorCode) esl_getWorkspaceRIPEMD160(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace, VSECPRIM_P2VAR_PARA(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160)) wsRIPEMD160)
{

  eslt_ErrorCode returnValue;
  eslt_Length keyPairModuleSize;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;

  /* Evaluate workspace status, value of key pair module size not used */
  returnValue = esl_getKeyPairModuleSizeV15SignRIPEMD160(workSpace, &keyPairModuleSize);

  if (ESL_ERC_NO_ERROR == returnValue)
  {
    /* get underlying workspace pointer */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte)) workSpace;
    *wsRIPEMD160 = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160)) (wsRAW + ESL_WS_RSA_V15_SV_RIPEMD160_WS_RIPEMD160(keyPairModuleSize));
  }

  return returnValue;
}

/****************************************************************************
 ** Global Functions
 ***************************************************************************/

/****************************************************************************
 * esl_initVerifyRSARIPEMD160_V15
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_initVerifyRSARIPEMD160_V15(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace,
                                                             eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule, eslt_Length publicKeyExponentSize, eslt_pRomByte publicKeyExponent)
{
  eslt_ErrorCode returnValue;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader) wsHeaderV15;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160) wsRIPEMD160;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver_prim) wsPRIM;

  returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* get underlying workspace pointer */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte)) workSpace;
    wsHeaderV15 = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader)) wsRAW;
    wsRIPEMD160 = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160)) (wsRAW + ESL_WS_RSA_V15_SV_RIPEMD160_WS_RIPEMD160(keyPairModuleSize));
    wsPRIM = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver_prim)) (wsRAW + ESL_WS_RSA_V15_SV_RIPEMD160_WS_RSA_PRIM(keyPairModuleSize));

    if (wsHeaderV15->size < (ESL_SIZEOF_WS_RSA_V15_SV_RIPEMD160_OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)))
    {
      returnValue = ESL_ERC_WS_TOO_SMALL;
    }
    else
    {
      wsRIPEMD160->header.size = (eslt_Length) (sizeof(eslt_WorkSpaceRIPEMD160) - sizeof(eslt_WorkSpaceHeader));
      wsRIPEMD160->header.watchdog = wsHeaderV15->watchdog;
      ESL_RSA_V15_INIT_HASH_FCT(wsRIPEMD160);
      wsPRIM->header.size = (eslt_Length) (wsHeaderV15->size - (ESL_SIZEOF_WS_RSA_V15_SV_RIPEMD160_OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)));
      wsPRIM->header.watchdog = wsHeaderV15->watchdog;
      returnValue = esl_initVerifyRSA_prim(wsPRIM, keyPairModuleSize, keyPairModule, publicKeyExponentSize, publicKeyExponent);
    }
    if (ESL_ERC_NO_ERROR == returnValue)
    {
      wsHeaderV15->status = (ESL_WST_ALGO_RSA | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);
      /* store message buffer length in workspace */
      wsRAW[ESL_WS_RSA_V15_SV_RIPEMD160_BUFFERLENGTH] = (eslt_Byte) (keyPairModuleSize >> 8);
      wsRAW[ESL_WS_RSA_V15_SV_RIPEMD160_BUFFERLENGTH + 1] = (eslt_Byte) (keyPairModuleSize);
    }
  }
  return returnValue;
}

/****************************************************************************
 * esl_updateVerifyRSARIPEMD160_V15
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_updateVerifyRSARIPEMD160_V15(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace, eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input)
{
  eslt_ErrorCode returnValue;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160) wsRIPEMD160;

  returnValue = esl_getWorkspaceRIPEMD160(workSpace, &wsRIPEMD160);

  if (ESL_ERC_NO_ERROR == returnValue)
  {
    if (actOK != ESL_RSA_V15_UPDATE_HASH_FCT(wsRIPEMD160, inputSize, input))
    {
      returnValue = ESL_ERC_RIPEMD160_TOTAL_LENGTH_OVERFLOW;
    }
  }

  return returnValue;
}

/****************************************************************************
 * esl_finalizeHashVerifyRSARIPEMD160_V15
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashVerifyRSARIPEMD160_V15(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest)
{
  eslt_ErrorCode returnValue;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160) wsRIPEMD160;

  returnValue = esl_getWorkspaceRIPEMD160(workSpace, &wsRIPEMD160);

  if (ESL_ERC_NO_ERROR == returnValue)
  {
    ESL_RSA_V15_FINALIZE_HASH_FCT(wsRIPEMD160, messageDigest);
  }

  return returnValue;
}

/****************************************************************************
 * esl_verifySigVerifyRSARIPEMD160_V15
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSARIPEMD160_V15(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace,
                                                                  VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest, eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature)
{
  eslt_ErrorCode returnValue;
  eslt_Length keyPairModuleSize, i;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver_prim) wsPRIM;
  VSECPRIM_P2VAR_PARA(eslt_Byte) messageBuf;

  returnValue = ESL_ERC_NO_ERROR;

  returnValue = esl_getKeyPairModuleSizeV15SignRIPEMD160(workSpace, &keyPairModuleSize);

  if (ESL_ERC_NO_ERROR == returnValue)
  {
    /* get underlying workspace pointer */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte)) workSpace;
    wsPRIM = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver_prim)) (wsRAW + ESL_WS_RSA_V15_SV_RIPEMD160_WS_RSA_PRIM(keyPairModuleSize));
    messageBuf = wsRAW + ESL_WS_RSA_V15_SV_RIPEMD160_BUFFER;

    returnValue = esl_verifyRSA_prim(wsPRIM, signatureSize, signature, &keyPairModuleSize, messageBuf);
    if (ESL_ERC_NO_ERROR == returnValue)
    {
      i = keyPairModuleSize - ESL_SIZEOF_RIPEMD160_DIGEST - sizeof(eslt_VERIFY_RSA_RIPEMD160_V15_ASN1_DIGESTINFO);

      if ((signatureSize != keyPairModuleSize) || (ESL_ERC_NO_ERROR != esl_verifyPaddingRSAEM_V15(messageBuf, &i, ASN1_PADDING_BLOCK_TYPE_PRIVATE)))
      {
        returnValue = ESL_ERC_RSA_SIGNATURE_INVALID;
      }
      else
      {
        if (FALSE == actMemcmp_ROMRAM((VSECPRIM_P2CONST_PARA(actU8)) &messageBuf[i], eslt_VERIFY_RSA_RIPEMD160_V15_ASN1_DIGESTINFO, sizeof(eslt_VERIFY_RSA_RIPEMD160_V15_ASN1_DIGESTINFO))) /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
        {
          returnValue = ESL_ERC_RSA_SIGNATURE_INVALID;
        }
        else
        {
          i += sizeof(eslt_VERIFY_RSA_RIPEMD160_V15_ASN1_DIGESTINFO);

          if (FALSE == actMemcmp((VSECPRIM_P2CONST_PARA(actU8)) messageDigest, (VSECPRIM_P2CONST_PARA(actU8))&messageBuf[i], ESL_SIZEOF_RIPEMD160_DIGEST)) /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
          {
            returnValue = ESL_ERC_RSA_SIGNATURE_INVALID;
          }
        }
      }
    }
  }

  return returnValue;
}

/****************************************************************************
 * esl_finalizeVerifyRSARIPEMD160_V15
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSARIPEMD160_V15(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace, eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature)
{
  eslt_ErrorCode returnValue;
  eslt_Byte hash[ESL_SIZEOF_RIPEMD160_DIGEST];

  returnValue = esl_finalizeHashVerifyRSARIPEMD160_V15(workSpace, hash);
  if (ESL_ERC_NO_ERROR == returnValue)
  {
    returnValue = esl_verifySigVerifyRSARIPEMD160_V15(workSpace, hash, signatureSize, signature);
  }

  return returnValue;
}

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_RSA_V15_RIPEMD160_ENABLED == STD_ON) */
/********************************************************************************************************************** 
 *  END OF FILE: ESLib_RSA_V15_Ver_RIPEMD160.c 
 *********************************************************************************************************************/
