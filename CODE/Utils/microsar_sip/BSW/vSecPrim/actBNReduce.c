/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actBNReduce.c
 *        \brief  A basic (unsigned) integer and module arithmetic used for elliptic curve point arithmetic.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library actCLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#define ACTBNREDUCE_SOURCE

#include "actBigNum.h"
#include "actWatchdog.h"

#if (VSECPRIM_ACTBNREDUCE_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 ** Local Functions
 ***************************************************************************/

/****************************************************************************
 **
 ** FUNCTION:
 **  static actBNDIGIT actBNMulDigit(const actBNDIGIT* a,
 **                                  const actLengthType a_length,
 **                                  const actBNDIGIT b,
 **                                  actBNDIGIT* c)
 **
 **   BigNum multiply digit (c = a*b).
 **
 ** input:                                              (length in digits)
 ** - a:       first number                                  a_length
 ** - b:       digit                                           1
 ** - c:       the result buffer                             a_length
 **
 ** output:
 ** - c:       the result a * b                            a_length
 ** - returns: the last carry digit                            1
 **
 ** assumes:
 **
 ** remark:
 **
 ***************************************************************************/
VSECPRIM_LOCAL_FUNC(actBNDIGIT) actBNMulDigit(
   VSECPRIM_P2CONST_PARA(actBNDIGIT) a, const actLengthType a_length,
   const actBNDIGIT b, VSECPRIM_P2VAR_PARA(actBNDIGIT) c)
{
  actLengthType i;
  actBNDDIGIT tmp = 0;

  /* school method */
  for (i = 0; i < a_length; ++i)
  {
    tmp = ((actBNDDIGIT) a[i]) * b + actBNHiWord(tmp);
    c[i] = actBNLoWord(tmp);
  }
  return actBNHiWord(tmp);
}

/****************************************************************************
 **
 ** FUNCTION:
 **  static void actBNNormalize(actBNDIGIT* a,
 **                             actLengthType* a_length,
 **                             actBNDIGIT* b,
 **                             actLengthType b_length,
 **                             int shift)
 **
 **   Normalization for actBNReduce function.
 **
 ** input:                                              (length in digits)
 ** - a:          number to be shifted                        *a_length+1
 ** - *a_length:  length of a
 ** - b:          number to be shifted                         b_length
 ** - b_length:   length of b
 ** - shift:      bits to be shifted:
 **               0 < shift < actBN_BITS_PER_DIGIT
 **
 ** output:
 ** - a:          shifted number                           *a_length(+1)
 ** - b:          shifted number                             b_length
 ** - *a_length:  length of shifted number
 **
 ** assumes:
 **
 ** remark:
 **
 ***************************************************************************/
VSECPRIM_LOCAL_FUNC(void) actBNNormalize(
   VSECPRIM_P2VAR_PARA(actBNDIGIT) a, actLengthType* a_length,
   VSECPRIM_P2VAR_PARA(actBNDIGIT) b, actLengthType b_length, actLengthType shift)
{
  actBNDIGIT tmp = (actBNDIGIT) (((actBNDIGIT) 1) << shift);  /* tmp = 2^shift */
  if ((a[*a_length] = actBNMulDigit(a, *a_length, tmp, a)) != 0)
  {
    ++(*a_length);
  }                             /* a *= tmp */
  (void)actBNMulDigit(b, b_length, tmp, b); /* b *= tmp */
}

/****************************************************************************
 **
 ** FUNCTION:
 **  static void actBNReNormalize(actBNDIGIT* r,
 **                               actBNDIGIT* b,
 **                               actLengthType b_length,
 **                               int shift)
 **
 **   Undo normalization for actBNReduce function.
 **
 ** input:                                              (length in digits)
 ** - r:          number to be shifted                         b_length+1
 ** - b:          number to be shifted                         b_length
 ** - b_length:   length of b and r
 ** - shift:      bits to be shifted:
 **               0 < shift < actBN_BITS_PER_DIGIT
 **
 ** output:
 ** - r:          shifted number                             b_length
 ** - b:          shifted number                             b_length
 **
 ** assumes:
 **
 ** remark:
 **
 ***************************************************************************/
VSECPRIM_LOCAL_FUNC(void) actBNReNormalize(
   VSECPRIM_P2VAR_PARA(actBNDIGIT) r,
   VSECPRIM_P2VAR_PARA(actBNDIGIT) b, actLengthType b_length, int shift)
{
  actBNDIGIT tmp = (actBNDIGIT) (((actBNDIGIT) 1) << (actBN_BITS_PER_DIGIT - shift)); /* tmp = 2^(bits_per_digit-shift) */
  r[b_length] = actBNMulDigit(r, b_length, tmp, r); /* r *= tmp */
  actBNCopy(r, r + 1, b_length);  /* shift r one digit to the right */
  tmp = actBNMulDigit(b, b_length, tmp, b); /* b *= tmp, write last carry to tmp */
  actBNCopy(b, b + 1, b_length - 1);  /* shift b one digit to the right */
  b[b_length - 1] = tmp;
}



/****************************************************************************
 ** Global Functions
 ***************************************************************************/


/****************************************************************************
 **
 ** FUNCTION:
 **  int actBNReduce(actBNDIGIT* a,
 **                  actLengthType a_length,
 **                  actBNDIGIT* b,
 **                  actLengthType b_length,
 **                  actBNDIGIT* r,
 **                  void (*watchdog) (void));
 **
 **   BigNum reduce (r = a mod b).
 **
 ** input:                                              (length in digits)
 ** - a:       first number (will be distroyed)              a_length+1
 ** - b:       second number (will be returned const         b_length
 **            but touched by the normalization)
 ** - r:       result buffer                                 b_length+1
 **
 ** output:
 ** - a:       the normalized result a mod b                 b_length
 ** - b:       second number                                 b_length
 ** - r:       the result a mod b                            b_length
 ** - returns: -1    if exact b_length <= 1 (error)
 **             0    else (success)
 **
 ** assumes:
 **
 ** remark:
 ** - a, b and r cannot overlap
 **
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actBNReduce(
   VSECPRIM_P2VAR_PARA(actBNDIGIT) a, actLengthType a_length,
   VSECPRIM_P2VAR_PARA(actBNDIGIT) b, actLengthType b_length,
   VSECPRIM_P2VAR_PARA(actBNDIGIT) r, VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void))
{
  actL1trigger(watchdog); /* PRQA S 1338, 2983, 3112  */ /* MD_MSR_DummyStmt */ /*lint -e438 */  /* SBSW_VSECPRIM_FUNCTION_CALL_WATCHDOG */

  /* init r, correct length of a and b */
  actBNSetZero(r, b_length);
  while((a_length > 1) && (a[a_length - 1] == 0))
  {
    --a_length;
  }
  while((b_length > 1) && (b[b_length - 1] == 0))
  {
    --b_length;
  }

  /* reference: MOV'97, Algo. 14.20 */
  if (b_length <= 1)
  {
    return actEXCEPTION_UNKNOWN;                  /* error: b too short */
  }
  else
  {
    actLengthType i, n, t, shift;
    actBNDIGIT qDig, Dig0, Dig1, Dig2;
    actBNDDIGIT tmp;

    /* abort, if a < b ? */
    if (a_length == b_length)
    {
      if (actBNCompare(a, b, a_length) == actCOMPARE_SMALLER)
      {
        actBNCopy(r, a, a_length);  /* r = a */
        return actOK;               /* done  */
      }
    }
    if (a_length < b_length)
    {
      actBNCopy(r, a, a_length);  /* r = a */
      return actOK;                 /* done  */
    }

    /* [step 1] normalization */
    shift = 0;
    Dig0 = b[b_length - 1];
    while(Dig0 < actBN_MAX_BIT)
    {
      Dig0 <<= 1;
      ++shift;
    }
    if (shift > 0)
    {
      actBNNormalize(a, &a_length, b, b_length, shift);
    }

    /* init n, t */
    n = a_length - 1;
    t = b_length - 1;

    /* [step 2] subtract b from b_length most significant digits of a */
    a += (n - t);
    if (actBNCompare(a, b, b_length) >= actCOMPARE_EQUAL)
    {
      actBNSub(a, b, a, b_length);
    }
    a -= (n - t);

    /* [step 3] main division loop */
    for (i = n; i > t; --i)
    {
      actL2trigger(watchdog); /* PRQA S 1338, 2983, 3112  */ /* MD_MSR_DummyStmt */ /*lint -e438 */ /* SBSW_VSECPRIM_FUNCTION_CALL_WATCHDOG */

      /* [3.1] estimation of quotient digit qDig[i-t-1] */
      if (a[i] == b[b_length - 1])
      {
        qDig = 0;
      }
      else
      {
        tmp = ((((actBNDDIGIT) a[i]) << actBN_BITS_PER_DIGIT) + a[i - 1]) / b[b_length - 1];
        qDig = (actBNDIGIT) (((actBNDIGIT) tmp) + 1); /* hi word is zero */
      }

      /* [3.2] check: qDig[i-t-1]*(b[b_length-1]*base + b[b_length-2]) > a[i]*base^2 + a[i-1]*base + a[i-2] */
      do
      {
        --qDig;
        tmp = ((actBNDDIGIT) qDig) * b[b_length - 2];
        Dig0 = actBNLoWord(tmp);
        tmp = ((actBNDDIGIT) qDig) * b[b_length - 1] + actBNHiWord(tmp);
        Dig1 = actBNLoWord(tmp);
        Dig2 = actBNHiWord(tmp);

        actL3trigger(watchdog);
      }
      while((Dig2 > a[i]) || ((Dig2 == a[i]) && (Dig1 > a[i - 1])) || ((Dig2 == a[i]) && (Dig1 == a[i - 1]) && (Dig0 > a[i - 2])));

      /* [3.3 + 3.4] subtract r = qDig[i-t-1]*b from b_length+1 most significant digits of a */
      r[b_length] = actBNMulDigit(b, b_length, qDig, r);
      a += (i - t - 1);

      actL3trigger(watchdog);

      if (actBNCompare(a, r, b_length + 1) == actCOMPARE_SMALLER)
      {
        actBNSub(r, b, r, b_length + 1);  /* qDig is still too large (by 1) */
      }

      actL3trigger(watchdog);

      actBNSub(a, r, a, b_length + 1);
      a -= (i - t - 1);
    }

    actL3trigger(watchdog);

    /* [step 4] copy a to r */
    actBNCopy(r, a, b_length);

    actL3trigger(watchdog);

    /* renormalize r and b */
    if (shift > 0)
    {
      actBNReNormalize(r, b, b_length, shift);
    }
  }

  actL1trigger(watchdog); /* PRQA S 1338, 2983, 3112  */ /* MD_MSR_DummyStmt */ /*lint -e438 */  /* SBSW_VSECPRIM_FUNCTION_CALL_WATCHDOG */

  return actOK;
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTBNREDUCE_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actBNReduce.c
 *********************************************************************************************************************/
