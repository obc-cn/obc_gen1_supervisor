/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  ESLib_RSA_V15_DecCRT.c
 *        \brief  RSA V1.5 (Decryption) implementation.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/
#define ESLIB_RSA_V15_DEC_CRT_SOURCE

#include "ESLib.h"
#include "ESLib_types.h"
#include "ESLib_ASN_1.h"

#include "actIRSA.h"
#include "actUtilities.h"

#if (VSECPRIM_RSA_V15_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/
# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 * esl_initDecryptRSACRT_V15
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptRSACRT_V15(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTdec) workSpace,
                                                        eslt_Length keyPairPrimePSize, eslt_pRomByte keyPairPrimeP,
                                                        eslt_Length keyPairPrimeQSize, eslt_pRomByte keyPairPrimeQ,
                                                        eslt_Length privateKeyExponentDPSize, eslt_pRomByte privateKeyExponentDP,
                                                        eslt_Length privateKeyExponentDQSize, eslt_pRomByte privateKeyExponentDQ,
                                                        eslt_Length privateKeyInverseQISize, eslt_pRomByte privateKeyInverseQI)
{
  eslt_ErrorCode returnValue;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader) wsHeaderV15;
  eslt_Length keyPairModuleSize;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTdec_prim) wsPRIM;

  returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* get underlying workspace pointer */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte)) workSpace;
    wsHeaderV15 = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader)) wsRAW;
    keyPairModuleSize = (eslt_Length) (keyPairPrimePSize + keyPairPrimeQSize);
    wsPRIM = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTdec_prim)) (wsRAW + ESL_WS_RSA_V15_ED_OFFSET_WS_RSA_PRIM(keyPairModuleSize));

    if (wsHeaderV15->size < (ESL_SIZEOF_WS_RSA_V15_ED_V15OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)))
    {
      returnValue = ESL_ERC_WS_TOO_SMALL;
    }
    else
    {
      wsPRIM->header.size = (eslt_Length) (wsHeaderV15->size - (ESL_SIZEOF_WS_RSA_V15_ED_V15OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)));
      wsPRIM->header.watchdog = wsHeaderV15->watchdog;
      returnValue = esl_initDecryptRSACRT_prim(wsPRIM,
                                               keyPairPrimePSize, keyPairPrimeP,
                                               keyPairPrimeQSize, keyPairPrimeQ,
                                               privateKeyExponentDPSize, privateKeyExponentDP, privateKeyExponentDQSize, privateKeyExponentDQ, privateKeyInverseQISize, privateKeyInverseQI);
    }
    if (ESL_ERC_NO_ERROR == returnValue)
    {
      wsHeaderV15->status = (ESL_WST_ALGO_RSA | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);
      /* store message buffer length in workspace */
      wsRAW[ESL_WS_RSA_V15_ED_OFFSET_BUFFERLENGTH] = (eslt_Byte) (keyPairModuleSize >> 8);
      wsRAW[ESL_WS_RSA_V15_ED_OFFSET_BUFFERLENGTH + 1] = (eslt_Byte) (keyPairModuleSize);
    }
  }
  return returnValue;
}

/****************************************************************************
 * esl_decryptRSACRT_V15
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSACRT_V15(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTdec) workSpace,
                                                    eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
                                                    VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message)
{

  eslt_ErrorCode returnValue;
  eslt_Length keyPairModuleSize;
  eslt_Length i;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader) wsHeaderV15;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTdec_prim) wsPRIM;
  VSECPRIM_P2VAR_PARA(eslt_Byte) messageBuf;

  returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* get underlying workspace pointer */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte)) workSpace;
    wsHeaderV15 = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader)) wsRAW;
    keyPairModuleSize = (eslt_Length) (((eslt_Length) wsRAW[ESL_WS_RSA_V15_ED_OFFSET_BUFFERLENGTH] << 8) + (wsRAW[ESL_WS_RSA_V15_ED_OFFSET_BUFFERLENGTH + 1]));
    wsPRIM = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTdec_prim)) (wsRAW + ESL_WS_RSA_V15_ED_OFFSET_WS_RSA_PRIM(keyPairModuleSize));
    messageBuf = wsRAW + ESL_WS_RSA_V15_ED_OFFSET_BUFFER;
    keyPairModuleSize = (eslt_Length) (((VSECPRIM_P2VAR_PARA(actRSACRTSTRUCT)) wsPRIM->wsRSA)->n_bytes);

    if (ESL_WST_ALGO_RSA != (wsHeaderV15->status & ESL_WST_M_ALGO))
    {
      returnValue = ESL_ERC_WS_STATE_INVALID;
    }
    else if (0 == (wsHeaderV15->status & ESL_WST_M_RUNNING))
    {
      returnValue = ESL_ERC_WS_STATE_INVALID;
    }
    else
    {
      i = keyPairModuleSize;
      returnValue = esl_decryptRSACRT_prim(wsPRIM, cipherSize, cipher, &i, messageBuf);
      if (ESL_ERC_NO_ERROR == returnValue)
      {
        if ((i != keyPairModuleSize) || (0x00 != messageBuf[0]) || (0x02 != messageBuf[1]))
        {
          returnValue = ESL_ERC_RSA_ENCODING_INVALID;
        }
        else
        {
          i = 2;
          while((0x00 != messageBuf[i]) && (i < keyPairModuleSize))
          {
            i++;
          }
          if (0x00 == messageBuf[i])
          {
            i++;
            if (ASN1_SIZEOF_MINIMAL_PADDING <= i)
            {
              *messageSize = (eslt_Length) (keyPairModuleSize - i);
              actMemcpy(message, messageBuf + (keyPairModuleSize - *messageSize), (unsigned int)*messageSize); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
            }
            else
            {
              returnValue = ESL_ERC_RSA_ENCODING_INVALID;
            }
          }
          else
          {
            returnValue = ESL_ERC_RSA_ENCODING_INVALID;
          }
        }
      }
    }
  }
  return returnValue;
}

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_RSA_V15_ENABLED == STD_ON) */
/********************************************************************************************************************** 
 *  END OF FILE: ESLib_RSA_V15_DecCRT.c 
 *********************************************************************************************************************/
