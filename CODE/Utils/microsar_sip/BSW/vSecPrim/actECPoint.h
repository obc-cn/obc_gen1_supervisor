/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actECPoint.h
 *        \brief  Interface for Elliptic curve point arithmetic functions.
 *
 *      \details This file is part of the embedded systems library cvActLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

/****************************************************************************
 **
 ** This file contains: Elliptic curve point arithmetic functions.
 **
 ** constants:
 **
 ** types:
 **   actECPOINT, actECCURVE
 **
 ** macros:
 **   actECPIsInfinity
 **
 ** functions:
 **   actECPToMont
 **   actECPFromMont
 **   actECPGetAffineXYFromMont
 **   actECPDouble
 **   actECPAdd
 **   actECPSimMult
 **
 ***************************************************************************/


#ifndef ACTECPOINT_H
# define ACTECPOINT_H


# include "actUtilities.h"
# include "actBigNum.h"

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Constants
 ***************************************************************************/

/*
   As workspace for operations in GF(p) and GF(n) like point addition,
   doubling, point (de)compression and field inversions we need temporary
   BigNum variables, each of word (digit) length:
         max_length + 1, where max_length = max(p_length, n_length)
 */
#if (VSECPRIM_ACTBNEUCLID_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/* 6 variables at minimum (4 for GCD + 2) */
#  define actEC_BIGNUM_TMP                                            actMax((actEC_MOD_EXP_TMP+2u), 6u)
# else
/* 4 variables at minimum (for actPoint Addition) */
#  define actEC_BIGNUM_TMP                                            actMax((actEC_MOD_EXP_TMP+2u), 4u)
# endif

/****************************************************************************
 ** Types
 ***************************************************************************/

/*
 actPoint type

 The internal point representation is projective.
 affine -> proj:  (X, Y) -> (X, Y, 1)
 proj -> affine:  (X, Y, Z) -> (X/(Z^2), Y/(Z^3))
 */
typedef struct
{
  VSECPRIM_P2VAR_PARA(actBNDIGIT) x; /* X-coordinate */
  VSECPRIM_P2VAR_PARA(actBNDIGIT) y; /* Y-coordinate */
  VSECPRIM_P2VAR_PARA(actBNDIGIT) z; /* Z-coordinate */
  int is_affine; /* internal flag for affine points (Z==1) */
  int is_infinity; /* internal flag for the infinity point */
} actECPOINT;

/*
 Curve type

 Contains pointers to:
 - the underlying field GF(p) and the field generated
 by the basepoint order GF(n)
 - the coefficient b and the basepoint G = (0x04, Gx, Gy
 - the precomputation information for scalar multiplications of the basepoint
 - temporary BigNum variables (see above)
 */
typedef actBNDIGIT actEcTempType[10];

typedef struct
{
  actBNRING p_field; /* underlying field GF(p) */
  actBNRING n_field; /* field GF(n), where n is the basepoint order */
  actPROMU8 a_R; /* a in Montgomery Representation */
  actPROMU8 b_R; /* b in Montgomery Representation */
  actPROMU8 G_R; /* G in Montgomery Representation */
  int a_equals_p_minus_3;
  VSECPRIM_P2VAR_PARA(actBNDIGIT) t[actEC_BIGNUM_TMP]; /* temporary variables */ /* PRQA S 3494 */ /* MD_VSECPRIM_CONST_BOOL_OPERAND */ /*lint !e506 */
  int groups;
  actPROMU8 prec_first; /* pointer to first pre-computated point */
} actECCURVE;


/****************************************************************************
 ** Macros
 ***************************************************************************/


/* Q == 0 */
/* PRQA S 3453 1 */ /* MD_MSR_FctLikeMacro */
# define actECPIsInfinity(Q, Curve)                                   (actBNIsZero((Q)->z, (Curve)->p_field->m_length) == TRUE)


/****************************************************************************
 ** Function Prototypes
 ***************************************************************************/
# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
extern "C" {
# endif


/****************************************************************************
 **
 ** FUNCTION:
 **   actECPAssign
 **
 **   Assign point Q2 to point Q1 (Q1 = Q2).
 **   The resulting point (Q1) will be in projective Montgomery representation.
 **
 ** input:                                              (length in digits)
 ** - Q1:       destination point                           3*p_length
 ** - Q2:       source point                            (3/2)*p_length
 ** - Curve: the underlying curve
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->RR:       constant for Montgomery     p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 **
 ** output:
 ** - Q1:       Q1 = Q2                                     3*p_length
 **
 ** assumes:
 ** - Q1 is projective (!Q1->is_affine)
 **
 ** remark:
 **
 ***************************************************************************/
VSECPRIM_FUNC( void ) actECPAssign (VSECPRIM_P2VAR_PARA( actECPOINT ) Q1,
                    VSECPRIM_P2CONST_PARA( actECPOINT ) Q2,
                    VSECPRIM_P2VAR_PARA( actECCURVE ) Curve);


/****************************************************************************
 **
 ** FUNCTION:
 **  void actECPToMont(actECPOINT *Q,
 **                    actECCURVE *Curve)
 **
 **  This function converts a point from normal to Montgomery
 **  Representation:   (X, Y, Z) -> (XR, YR, ZR)
 **
 ** input:                                              (length in digits)
 ** - Q:     the point (X, Y, Z) to be converted            3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->RR:       constant for Montgomery     p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 **
 ** output:
 ** - Q:     the converted point (XR, YR, ZR)               3*p_length
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition; an
 **   example for a complete initialization is the actECDSAVerify(..)
 **   implementation in actIECDSA.c.
 **
 ** remark:
 ** - Q->is_affine is not touched by this routine
 ** - Q->is_infinity is not touched by this routine
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actECPToMont(VSECPRIM_P2VAR_PARA(actECPOINT) Q, VSECPRIM_P2VAR_PARA(actECCURVE) Curve);

/****************************************************************************
 **
 ** FUNCTION:
 **  actECPToAffineFromMont
 **
 **  Calculates the affine representation of the actPoint Q, where Q is in
 **  Montgomery Representation (XR, YR, ZR)
 **
 ** input:                                              (length in digits)
 ** - Q:     the projective point (XR, YR, ZR) in           3*p_length
 **          Montgomery representation
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->RR:       constant for Montgomery     p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** -        p_field->window_size for modular exponentiation
 ** - Curve->t[0..(i-1)]:  i temporary BigNum variables,   i*(p_length+1)
 **                        where i = actEC_BIGNUM_TMP
 ** - flag:  Output control flag; if zero the point will be checked
 **          to be on curve and transformed from Montgomery to normal
 **          representation (used in protocol functions, e.g. ECDSASign).
 **
 ** output:
 ** - Q:  flag==0:   the validated affine point (X, Y)      2*p_length
 **                  [validation means ]
 **       flag!=0:   the affine point (XR, YR) in Montgomery
 **                  Representation                         2*p_length
 ** - returns: 0  if !flag and Q is not on curve (internal error/attack)
 **            1  else
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for a complete initialization is the
 **   actECDSAVerify(..) implementation in actIECDSA.c.
 ** - Q != Infinity is in Montgomery Representation (XR, YR, ZR)
 **
 ***************************************************************************/
VSECPRIM_FUNC(int) actECPToAffineFromMont(
   VSECPRIM_P2VAR_PARA(actECPOINT) Q, VSECPRIM_P2VAR_PARA(actECCURVE) Curve, int flag);

/****************************************************************************
 **
 ** FUNCTION:
 **   actECPointIsOnCurve
 **
 **  This function checks if a point QR is on curve, where QR is in affine
 **  Montgomery Representation (XR, YR, R)
 **
 ** input:
 ** - QR:     the (affine) point to be checked              3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_byte_length: length of p in byte
 ** -        p_field->RR:       constant for Montgomery     p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 ** - Curve->t[1]:   temporary BigNum variable              p_length+1
 ** - Curve->t[2]:   temporary BigNum variable              p_length+1
 ** - Curve->t[3]:   temporary BigNum variable              p_length+1
 ** - Curve->a_R:    coefficient a of curve equation        p_bytes
 ** - Curve->b_R:    coefficient b of curve equation        p_bytes
 **
 ** output:
 ** - returns:    0     if the point is not on curve
 **               1     else
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for the usage of this function is the
 **   actECDSAVerify(.) implementation in actIECDSA.c, which is basically
 **   a call of actECDSAvp(.) with all previous initializations.
 ** - QR != Infinity is in affine Montgomery Representation (XR, YR, R)
 ** - curve equation coefficient a is equal to p-3 (is the case for
 **   supported curves of the ECC module)
 **
 ** uses:
 **
 ***************************************************************************/
VSECPRIM_FUNC(int) actECPointIsOnCurve(
   VSECPRIM_P2VAR_PARA(actECPOINT) Q, VSECPRIM_P2VAR_PARA(actECCURVE) Curve);

/****************************************************************************
 **
 ** FUNCTION:
 **  void actECPDouble(actECPOINT *Q,
 **                    actECCURVE *Curve)
 **
 **  This routine doubles the projective point Q (Q *= 2).
 **  The in- and output point(s) is(are) in Montgomery Representation.
 **
 ** input:                                              (length in digits)
 ** - Q:     the point (X_1*R, Y_1*R, Z_1*R)                3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 ** - Curve->t[1]:   temporary BigNum variable              p_length+1
 ** - Curve->t[2]:   temporary BigNum variable              p_length+1
 **
 ** output:
 ** - Q:     the point 2Q = (X_2*R, Y_2*R, Z_2*R)           3*p_length
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition; an
 **   example for a complete initialization is the actECDSAVerify(..)
 **   implementation in actIECDSA.c.
 ** - Q is in Montgomery Representation (XR, YR, ZR)
 ** - curve equation coefficient a is equal to p-3 (is the case for
 **   supported curves of the ECC module)
 **
 ** remark:
 ** - Q->is_affine is not touched by this routine
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actECPDouble(VSECPRIM_P2VAR_PARA(actECPOINT) Q, VSECPRIM_P2VAR_PARA(actECCURVE) Curve);

/****************************************************************************
 **
 ** FUNCTION:
 **  void actECPAdd(actECPOINT *Q1,
 **                 const actECPOINT *Q2,
 **                 actECCURVE *Curve)
 **
 **  This routine adds the affine point Q2 to Q1 (Q1 += Q2).
 **  The in- and output point(s) is(are) in Montgomery Representation.
 **  Implementation Reference: IEEE 1363 / D 13, Algorithm A.10.4
 **
 **  Projective Addition:    (X_0, Y_0, Z_0) + (X_1, Y_1, Z_1) = (X_2, Y_2, Z_2),
 **  where                   U_0 = X_0 * (Z_1^2)
 **                          S_0 = Y_0 * (Z_1^3)
 **                          U_1 = X_1 * (Z_0^2)
 **                          S_1 = Y_1 * (Z_0^3)
 **                          W   = U_0 - U_1
 **                          R   = S_0 - S_1
 **                          T   = U_0 + U_1
 **                          M   = S_0 + S_1
 **                          Z_2 = Z_0 * Z_1 * W
 **                          X_2 = (R^2) - T * (W^2)
 **                          V   = T * (W^2) - 2*X_2
 **                        2*Y_2 = V * R - M * (W^3)
 **
 ** input:                                              (length in digits)
 ** - Q1:    the point (X_0*R, Y_0*R, Z_0*R)                3*p_length
 ** - Q2:    the point (X_1*R, Y_1*R, Z_1*R)                3*p_length
 ** - Q2->is_affine has to be 0 if Q2 is not in affine representation
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 ** - Curve->t[1]:   temporary BigNum variable              p_length+1
 ** - Curve->t[2]:   temporary BigNum variable              p_length+1
 ** - Curve->t[3]:   temporary BigNum variable              p_length+1
 **
 ** output:
 ** - Q1:    the point Q1+Q2 = (X_2*R, Y_2*R, Z_2*R)        3*p_length
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for a complete initialization is the
 **   actECDSAVerify(..) implementation in actIECDSA.c.
 ** - Q1 is in Montgomery Representation (XR, YR, ZR)
 ** - Q2 != 0 is in Montgomery Representation (XR, YR, ZR)
 **
 ** remark:
 ** - Q1->is_affine is not touched by this routine
 ** - Q2->is_affine is not touched by this routine
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actECPAdd(
   VSECPRIM_P2VAR_PARA(actECPOINT) Q1, 
   VSECPRIM_P2CONST_PARA(actECPOINT) Q2,
   VSECPRIM_P2VAR_PARA(actECCURVE) Curve);

/****************************************************************************
 **
 ** FUNCTION:
 **  void actECPSub(actECPOINT *Q1,
 **                 const actECPOINT *Q2,
 **                 actECCURVE *Curve)
 **
 **  This routine subtracts the point Q2 from Q1 (Q1 -= Q2).
 **  The in- and output points are in Montgomery Representation.
 **
 **  Subtraction is achieved by< negating the Y coordinate (Q2.y) and then adding the points.
 **  Q2.y is restored after addition (just negated once more).
 **
 ** input:                                              (length in digits)
 ** - Q1:    the point (X_0*R, Y_0*R, Z_0*R)                3*p_length
 ** - Q2:    the point (X_1*R, Y_1*R, Z_1*R)                3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - more info under -> actECPAdd
 **
 ** output:
 ** - Q1:    the point Q1-Q2 = (X_2*R, Y_2*R, Z_2*R)        3*p_length
 **
 ** assumes:
 ** - see actECPAdd
 **
 ** remark:
 ** - Q1->is_affine is not touched by this routine
 ** - Q2->is_affine is not touched by this routine
 **
 ***************************************************************************/
VSECPRIM_FUNC( void ) actECPSub (VSECPRIM_P2VAR_PARA( actECPOINT ) Q1,
                                  VSECPRIM_P2CONST_PARA( actECPOINT ) Q2,
                                  VSECPRIM_P2VAR_PARA( actECCURVE ) Curve);

/****************************************************************************
 **
 ** FUNCTIONs:
 **  actECPMult
 **
 **  This routine calculates R = k*Q (scalar multiplication)
 **  The in- and output point(s) is(are) in Montgomery Representation.
 **
 ** input:                                              (length in digits)
 ** - Q:     the point to be multiplied with k              3*p_length
 ** - k:     scalar                                         n_length
 ** - R:     the point to store the result                  3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->n_field: the field GF(n):
 ** -        n_field->m_length: equals to n_length
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 ** - Curve->t[1]:   temporary BigNum variable              p_length+1
 ** - Curve->t[2]:   temporary BigNum variable              p_length+1
 ** - Curve->t[3]:   temporary BigNum variable              p_length+1
 **
 ** output:
 ** - R:    the point k*Q in Montgomery                     3*p_length
 **          Representation (XR, YR, ZR)
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for a complete initialization is the
 **   actECDSAVerify(..) implementation in actIECDSA.c.
 ** - Q is in affine Montgomery Representation (XR, YR, R)
 **
 ***************************************************************************/
VSECPRIM_FUNC( void ) actECPMult(VSECPRIM_P2CONST_PARA( actECPOINT ) Q,
                                 VSECPRIM_P2CONST_PARA( actBNDIGIT ) k,
                                 VSECPRIM_P2VAR_PARA( actECPOINT ) R,
                                 VSECPRIM_P2VAR_PARA( actECCURVE ) Curve );

/****************************************************************************
**
** FUNCTIONs:
**  actECPMultShort
**
**  This routine calculates R = k*Q (scalar multiplication)
**  The in- and output point(s) is(are) in Montgomery Representation.
**
** input:                                              (length in digits)
** - Q:     the point to be multiplied with k              3*p_length
** - k:     scalar                                         n_length
** - R:     the point to store the result                  3*p_length
** - Curve: the underlying curve (see assumptions)
** - Curve->p_field: the underlying field GF(p):
** -        p_field->m:        the modulus p               p_length
** -        p_field->m_length: equals to p_length
** -        p_field->m_bar:    constant for Montgomery     1
** -        p_field->prime_structure==curveid
** - Curve->n_field: the field GF(n):
** -        n_field->m_length: equals to n_length
** - Curve->t[0]:   temporary BigNum variable              p_length+1
** - Curve->t[1]:   temporary BigNum variable              p_length+1
** - Curve->t[2]:   temporary BigNum variable              p_length+1
** - Curve->t[3]:   temporary BigNum variable              p_length+1
**
** output:
** - R:    the point k*Q in Montgomery                     3*p_length
**          Representation (XR, YR, ZR)
**
** assumes:
** - The actECCURVE structure parameter 'Curve' holds all necessary
**   information and the workspace. It has to be initialized as far as
**   listed above. Please have a look at the actECCURVE definition in
**   actECPoint.h; an example for a complete initialization is the
**   actECDSAVerify(..) implementation in actIECDSA.c.
** - Q is in affine Montgomery Representation (XR, YR, R)
**
***************************************************************************/
VSECPRIM_FUNC( void ) actECPMultShort(VSECPRIM_P2CONST_PARA( actECPOINT ) Q,
                                      const actU32      k,
                                      VSECPRIM_P2VAR_PARA( actECPOINT ) R,
                                      VSECPRIM_P2VAR_PARA( actECCURVE ) Curve);


/****************************************************************************
 **
 ** FUNCTION:
 **  void actECPMultG(const actBNDIGIT *k,
 **                   actECPOINT *Prec,
 **                   actECPOINT *R,
 **                   actECCURVE *Curve)
 **
 **  This routine calculates R = k*G (scalar multiplication of basepoint).
 **  The in- and output point(s) is(are) in Montgomery Representation.
 **
 ** input:                                              (length in digits)
 ** - k:     scalar                                         n_length
 ** - Prec:  temporary point to store a precomputed point   3*p_length
 ** - R:     the point to store the result                  3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_byte_length: length of p in byte
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->n_field: the field GF(n):
 ** -        n_field->m_length: equals to n_length
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 ** - Curve->t[1]:   temporary BigNum variable              p_length+1
 ** - Curve->t[2]:   temporary BigNum variable              p_length+1
 ** - Curve->t[3]:   temporary BigNum variable              p_length+1
 ** - Curve->prec_info:   precomputation information
 **
 ** output:
 ** - R:    the point k*G in Montgomery                     3*p_length
 **          Representation (XR, YR, ZR)
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for a complete initialization is the
 **   actECDSAVerify(..) implementation in actIECDSA.c.
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actECPMultG(
   VSECPRIM_P2CONST_PARA(actBNDIGIT) k, VSECPRIM_P2VAR_PARA(actECPOINT) Prec,
   VSECPRIM_P2VAR_PARA(actECPOINT) R, VSECPRIM_P2VAR_PARA(actECCURVE) Curve);

/****************************************************************************
 **
 ** FUNCTION:
 **  void actECPSimMult(const actECPOINT *Q1,
 **                     const actECPOINT *Q2,
 **                     const actBNDIGIT *k1,
 **                     const actBNDIGIT *k2,
 **                     actECPOINT *Q12,
 **                     actECPOINT *R,
 **                     actECCURVE *Curve)
 **
 **  This routine calculates R = k1*Q1 + k2*Q2 (two scalar multipli-
 **  cations incl. addition).
 **  The in- and output point(s) is(are) in Montgomery Representation.
 **  Implementation Reference: MOV'97, algo 14.88
 **
 ** input:                                              (length in digits)
 ** - Q1:    the point to be multiplied with k1             3*p_length
 ** - Q2:    the point to be multiplied with k2             3*p_length
 ** - k1:    first scalar                                   n_length
 ** - k1:    second scalar                                  n_length
 ** - Q12:   the point to store Q1 + Q2                     3*p_length
 ** - R:     the point to store the result                  3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->n_field: the field GF(n):
 ** -        n_field->m_length: equals to n_length
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 ** - Curve->t[1]:   temporary BigNum variable              p_length+1
 ** - Curve->t[2]:   temporary BigNum variable              p_length+1
 ** - Curve->t[3]:   temporary BigNum variable              p_length+1
 **
 ** output:
 ** - R:     the point k1*Q1 + k2*Q2 in Montgomery          3*p_length
 **          Representation (XR, YR, ZR)
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for a complete initialization is the
 **   actECDSAVerify(..) implementation in actIECDSA.c.
 ** - Q1 is in affine Montgomery Representation (XR, YR, R)
 ** - Q2 is in affine Montgomery Representation (XR, YR, R)
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actECPSimMult(
   VSECPRIM_P2CONST_PARA(actECPOINT) Q1, VSECPRIM_P2CONST_PARA(actECPOINT) Q2,
   VSECPRIM_P2CONST_PARA(actBNDIGIT) k1, VSECPRIM_P2CONST_PARA(actBNDIGIT) k2,
   VSECPRIM_P2VAR_PARA(actECPOINT) Q12, VSECPRIM_P2VAR_PARA(actECPOINT) R,
   VSECPRIM_P2VAR_PARA(actECCURVE) Curve);

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
} /* extern "C" */
# endif
# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* ACTECPOINT_H */

/**********************************************************************************************************************
 *  END OF FILE: actECPoint.h
 *********************************************************************************************************************/
