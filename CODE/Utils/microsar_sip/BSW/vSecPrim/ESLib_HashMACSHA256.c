 /**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
 /**        \file  ESLib_HashMACSHA256.c
 *        \brief  Hash MAC SHA-256 implementation.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

 /**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#define ESLIB_HASHMACSHA256_SOURCE

#include "ESLib.h"
#include "ESLib_types.h"
#include "ESLib_Helper.h"

/* actCLib includes */
#include "actIHashMACSHA256.h"
#include "actUtilities.h"

#if (VSECPRIM_HMAC_SHA2_256_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 * esl_initHashMACSHA256
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_initHashMACSHA256(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA256) workSpace, const eslt_Length keyLength, VSECPRIM_P2CONST_PARA(eslt_Byte) key)
{
  eslt_ErrorCode retVal = ESL_ERC_NO_ERROR;

  /* Any NULLs ? */
  if ((!workSpace) || (!key))
  {
    retVal = ESL_ERC_PARAMETER_INVALID;
  }
  /* Check workSpace */
  else if (workSpace->header.size < ESL_MAXSIZEOF_WS_HMACSHA256)
  {
    retVal = ESL_ERC_WS_TOO_SMALL;
  }
  /* Check keyLength */
  else if (keyLength == 0)
  {
    retVal = ESL_ERC_HMAC_KEY_LENGTH_OUT_OF_RANGE;
  }
  else
  {
    /* init actCLib HMAC-SHA-256 */
    if (actOK == actHashMACSHA256Init((VSECPRIM_P2VAR_PARA(actHASHMACSHA256STRUCT)) workSpace->wsHMACSHA256, key, keyLength, workSpace->header.watchdog)) /* PRQA S 0310 */ /* MD_VSECPRIM_11.4 */
    {
      /* Set workSpace state */
      esl_SetWorkspaceStatus(&workSpace->header, ESL_WST_ALGO_HMACSHA256); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
    }
    else
    {
      retVal = ESL_ERC_ERROR;
    }
  }

  return retVal;
}

/****************************************************************************
 * esl_updateHashMACSHA256
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_updateHashMACSHA256(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA256) workSpace, const eslt_Length inputLength, VSECPRIM_P2CONST_PARA(eslt_Byte) input)
{
  eslt_ErrorCode retVal = ESL_ERC_NO_ERROR;

  /* Any NULLs ? */
  if ((!workSpace) || (!input))
  {
    retVal = ESL_ERC_PARAMETER_INVALID;
  }

  /* Check workSpace */
  else if ((workSpace->header.status & ESL_WST_M_ALGO) != ESL_WST_ALGO_HMACSHA256)
  {
    retVal = ESL_ERC_WS_STATE_INVALID;
  }
  else if (!(workSpace->header.status & ESL_WST_M_RUNNING))
  {
    retVal = ESL_ERC_WS_STATE_INVALID;
  }
  if (workSpace->header.size < ESL_MAXSIZEOF_WS_HMACSHA256)
  {
    retVal = ESL_ERC_WS_TOO_SMALL;
  }
  else
  {
    /* update actCLib HMAC-SHA-256 */
    if (actEXCEPTION_LENGTH == actHashMACSHA256Update((VSECPRIM_P2VAR_PARA(actHASHMACSHA256STRUCT)) workSpace->wsHMACSHA256, input, (int)inputLength, workSpace->header.watchdog)) /* PRQA S 0310 */ /* MD_VSECPRIM_11.4 */
    {
      retVal = ESL_ERC_SHA256_TOTAL_LENGTH_OVERFLOW;
    }
  }

  return retVal;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */

/****************************************************************************
 * esl_finalizeHashMACSHA256
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashMACSHA256(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA256) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageHashMAC)
{
  eslt_ErrorCode retVal = ESL_ERC_NO_ERROR;

  /* Any NULLs ? */
  if ((!workSpace) || (!messageHashMAC))
  {
    retVal = ESL_ERC_PARAMETER_INVALID;
  }

  /* Check workSpace */
  else if ((workSpace->header.status & ESL_WST_M_ALGO) != ESL_WST_ALGO_HMACSHA256)
  {
    retVal = ESL_ERC_WS_STATE_INVALID;
  }
  else if (!(workSpace->header.status & ESL_WST_M_RUNNING))
  {
    retVal = ESL_ERC_WS_STATE_INVALID;
  }
  else if (workSpace->header.size < ESL_MAXSIZEOF_WS_HMACSHA256)
  {
    retVal = ESL_ERC_WS_TOO_SMALL;
  }
  else
  {
    /* finalize actCLib HMAC-SHA-256 */
    if (actOK != actHashMACSHA256Finalize((VSECPRIM_P2VAR_PARA(actHASHMACSHA256STRUCT)) workSpace->wsHMACSHA256, messageHashMAC, workSpace->header.watchdog)) /* PRQA S 0310 */ /* MD_VSECPRIM_11.4 */
    {
      retVal = ESL_ERC_ERROR;
    }

    /* Reset and clear workspace */
    esl_ResetAndClearWorkspace(&workSpace->header, workSpace->wsHMACSHA256); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
  }

  return retVal;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */

/****************************************************************************
 * esl_verifyHashMACSHA256
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_verifyHashMACSHA256(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA256) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) messageHashMAC)
{
  eslt_Byte tmpMAC[ESL_SIZEOF_SHA256_DIGEST];
  eslt_ErrorCode retVal = ESL_ERC_NO_ERROR;

  /* Any NULLs ? */
  if ((!workSpace) || (!messageHashMAC))
  {
    retVal = ESL_ERC_PARAMETER_INVALID;
  }

  /* Check workSpace */
  else if ((workSpace->header.status & ESL_WST_M_ALGO) != ESL_WST_ALGO_HMACSHA256)
  {
    retVal = ESL_ERC_WS_STATE_INVALID;
  }
  else if (!(workSpace->header.status & ESL_WST_M_RUNNING))
  {
    retVal = ESL_ERC_WS_STATE_INVALID;
  }
  else if (workSpace->header.size < ESL_MAXSIZEOF_WS_HMACSHA256)
  {
    retVal = ESL_ERC_WS_TOO_SMALL;
  }
  else
  {
    /* Verify MAC */
    if (actOK == esl_finalizeHashMACSHA256(workSpace, tmpMAC))
    {
      if (actMemcmp(tmpMAC, messageHashMAC, ESL_SIZEOF_SHA256_DIGEST) != TRUE) /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
      {
        retVal = ESL_ERC_HMAC_INCORRECT_MAC;
      }
    }
    else
    {
      retVal = ESL_ERC_ERROR;
    }
  }

  return retVal;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_HMAC_SHA2_256_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: ESLib_HashMACSHA256.c
 *********************************************************************************************************************/
