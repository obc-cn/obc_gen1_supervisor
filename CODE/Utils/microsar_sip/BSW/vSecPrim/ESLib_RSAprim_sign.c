/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file ESLib_RSAprim_sign.c
 *        \brief RSA sign primitive
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#define ESLIB_RSA_PRIM_SIGN_SOURCE

#include "ESLib.h"
#include "ESLib_types.h"

/* actCLib includes */
#include "actIRSA.h"

#if (VSECPRIM_RSA_SIGNATURE_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/
# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 * esl_initSignRSA_prim
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignRSA_prim(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig_prim) workSpace,
                                                   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule, eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent)
{
  /* Init actCLib RSA private key initialization */
  {
    actRETURNCODE result = actRSAInitPrivateKeyOperation(keyPairModule, (int)keyPairModuleSize, privateKeyExponent,
                                                         (int)privateKeyExponentSize, workSpace->wsRSA,
                                                         (int)(workSpace->header.size), workSpace->header.watchdog);
    if (result != actOK)
    {
      if (result == actEXCEPTION_NULL)
      {
        return ESL_ERC_PARAMETER_INVALID;
      }
      else if (result == actEXCEPTION_MEMORY)
      {
        return ESL_ERC_WS_TOO_SMALL;
      }
      else if (result == actRSA_PARAM_OUT_OF_RANGE)
      {
        return ESL_ERC_PARAMETER_INVALID;
      }
      else if (result == actEXCEPTION_PRIVKEY)
      {
        return ESL_ERC_RSA_PRIVKEY_INVALID;
      }
    }
  }

  /* Set workSpace state */
  workSpace->header.status = (ESL_WST_ALGO_RSA | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);

  return ESL_ERC_NO_ERROR;
}

/****************************************************************************
 * esl_signRSA_prim
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_signRSA_prim(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig_prim) workSpace,
                                               eslt_Length messageSize, VSECPRIM_P2CONST_PARA(eslt_Byte) message,
                                               VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte) signature)
{
  /* Check workSpace */
  if ((workSpace->header.status & ESL_WST_M_ALGO) != ESL_WST_ALGO_RSA)
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  if (!(workSpace->header.status & ESL_WST_M_RUNNING))
  {
    return ESL_ERC_WS_STATE_INVALID;
  }

  /* Call actCLib RSA private key operation */
  {
    int signatureLen = *signatureSize;
    actRETURNCODE result = actRSAPrivateKeyOperation(message, (int)messageSize, signature, &signatureLen,
                                                     workSpace->wsRSA, workSpace->header.watchdog);
    if (result != actOK)
    {
      if (result == actEXCEPTION_NULL)
      {
        return ESL_ERC_PARAMETER_INVALID;
      }
      else if (result == actEXCEPTION_MEMORY)
      {
        return ESL_ERC_OUTPUT_SIZE_TOO_SHORT;
      }
      else if (result == actRSA_PARAM_OUT_OF_RANGE)
      {
        return ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE;
      }
    }
    *signatureSize = (eslt_Length) (signatureLen & 0xFFFFu);
  }

  return ESL_ERC_NO_ERROR;
}

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_RSA_SIGNATURE_ENABLED == STD_ON) */
/********************************************************************************************************************** 
 *  END OF FILE: ESLib_RSAprim_sign.c 
 *********************************************************************************************************************/
