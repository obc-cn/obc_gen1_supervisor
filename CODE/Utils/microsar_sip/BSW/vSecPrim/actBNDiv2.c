/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actBNDiv2.c
 *        \brief  A basic (unsigned) integer and module arithmetic used for elliptic curve point arithmetic.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library actCLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/
#define ACTBNDIV2_SOURCE

#include "actBigNum.h"
#include "actUtilities.h"

#if (VSECPRIM_ACTBNDIV2_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTION:
 ** void actBNDiv2(actBNDIGIT* a,
 **                actU8 first_carry,
 **                const actLengthType length)
 **
 **  Division by 2 (a /= 2 ).
 **
 ** input:                                              (length in digits)
 ** - a:            number to be divided                   length (+ 1 bit)
 ** - first_carry:  the bit_length of 'a' is
 **                     digits * bits_per_digit + 1,
 **                 and the leading bit passed as 'first_carry'
 ** - length:       length of a in digits
 **
 ** output:
 ** - a:      the result a / 2                              m_length
 **
 ** assumes:
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actBNDiv2(VSECPRIM_P2VAR_PARA(actBNDIGIT) a, actU8 first_carry, actLengthType length)
{
  actLengthType tmpLength;
  actU8 carry1 = first_carry, carry2;
  actBNDIGIT tmp;

  /* shift a to the right by 1 bit */
  for (; length > 0; --length)
  {
    tmpLength = length - 1;
    tmp = a[tmpLength];
    carry2 = (actU8) (tmp & 1);
    tmp >>= 1;
    tmp |= (((actBNDIGIT) carry1) << (actBN_BITS_PER_DIGIT - 1));
    carry1 = carry2;
    a[tmpLength] = tmp;
  }
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTBNDIV2_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actBNDiv2.c
 *********************************************************************************************************************/
