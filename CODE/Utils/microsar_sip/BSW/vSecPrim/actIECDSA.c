/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actIECDSA.c
 *        \brief  Implementation file for actIECDSA.h
 *
 *      \details This file is part of the embedded systems library cvActLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

/****************************************************************************
 **
 ** This file contains: Implementation file for actIECDSA.h
 **
 ** constants:
 **    actECDSA_SIGNATURE_LENGTH_[curve_id]
 **
 ** types:
 **
 ** global variables:
 **
 ** macros:
 **
 ** functions:
 **   actECDSAGetSignatureLength
 **   actECDSAVerify
 **   actECDSASign
 **
 ***************************************************************************/

#include "actIECDSA.h"
#include "actIECKey.h"
#include "actECTools.h"
#include "actECDSA.h"
#include "actECPoint.h"
#include "actECLengthInfo.h"
#include "actUtilities.h"
#include "actConfig.h"

#if (VSECPRIM_ACTIECDSA_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/
#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTION:
 **   actECDSAGetSignatureCompLength
 **
 **  This function returns the signature component length in bytes
 **  for the desired curve.
 **
 ** input:
 ** - domain:      domain parameter
 **
 ** output:
 ** - returns:    length of each signature component (r, s) in bytes
 **                 (0, if domain not valid)
 **
 ** assumes:
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(int) actECDSAGetSignatureCompLength(actPROMU8 domain)
{
  return actECGetPrivateKeyLength(domain);
}

/****************************************************************************
 **
 ** FUNCTION:
 **  actECDSAInitVerify
 **
 **  This function initializes the ECC workspace.
 **
 ** input:
 ** - domain:      domain parameter
 ** - domain_ext:  domain parameter extensions (Montgomery constants etc.)
 ** - wksp:        workspace
 ** - wksp_len:    length of workspace in bytes
 **
 ** output:
 ** - wksp:       initialized workspace
 ** - returns:    actEXCEPTION_NULL         if an input parameter is NULL
 **               actEXCEPTION_DOMAIN       domain decoding error
 **               actEXCEPTION_DOMAIN_EXT   domain_ext decoding error
 **               actEXCEPTION_MEMORY       wksp_len to small
 **               actOK                     else
 **
 ** assumes:
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDSAInitVerify(actPROMU8 domain, actPROMU8 domain_ext, VSECPRIM_P2VAR_PARA(actU8) wksp, int wksp_len)
{
  return actECInit(domain, domain_ext, (VSECPRIM_P2CONST_PARA(actU8)) NULL_PTR, actEC_ALGO_FLAG_VP, wksp, wksp_len);
}

/****************************************************************************
 **
 ** FUNCTION:
 **  actECDSAVerify
 **
 **  This function verifies a signature of a message value.
 **
 ** input:
 ** - message:     pointer to message that has been signed
 ** - message_len: length of message in bytes
 ** - signature_r: pointer to the signature component r of (r, s)
 ** - signature_r_len: length of signature_r
 ** - signature_s: pointer to the signature component s of (r, s)
 ** - signature_s_len: length of signature_s
 ** - publickey_x: x-coordinate of the public key Q of the key pair (d, Q)
 ** - publickey_y: y-coordinate of the public key Q of the key pair (d, Q)
 ** - wksp:        workspace
 **
 ** output:
 ** - returns:    actEXCEPTION_NULL          if an input parameter is NULL
 **               actEXCEPTION_LENGTH        wrong message_len (in bits)
 **               actEXCEPTION_PUBKEY        if Q is invalid
 **               actSIGNATURE_OUT_OF_RANGE  if (r==0||s==0||r>=n||s>=n)
 **               actSIGNATURE_INVALID       if the signature is invalid
 **               actEXCEPTION_UNKNOWN       internal error (result point
 **                                          not on curve)
 **               actOK                      else
 **
 ** assumes:
 ** - wksp is initialized with actECDSAInitVerify
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDSAVerify(VSECPRIM_P2CONST_PARA(actU8) message, int message_len,
                                            VSECPRIM_P2CONST_PARA(actU8) signature_r, int signature_r_len,
                                            VSECPRIM_P2CONST_PARA(actU8) signature_s, int signature_s_len,
                                            VSECPRIM_P2CONST_PARA(actU8) publickey_x, VSECPRIM_P2CONST_PARA(actU8) publickey_y, VSECPRIM_P2VAR_PARA(actU8) wksp)
{
  VSECPRIM_P2VAR_PARA(actECCURVE) Curve = (VSECPRIM_P2VAR_PARA(actECCURVE)) wksp;
  VSECPRIM_P2VAR_PARA(actBNDIGIT) wksp_ptr = (VSECPRIM_P2VAR_PARA(actBNDIGIT)) (wksp + sizeof(actECCURVE));
  int p_length = Curve->p_field.m_length;
  int p_bytes = Curve->p_field.m_byte_length;
  int n_length = Curve->n_field.m_length;
  int n_bytes = Curve->n_field.m_byte_length;
  actECPOINT Q;
  VSECPRIM_P2VAR_PARA(actBNDIGIT) e;
  VSECPRIM_P2VAR_PARA(actBNDIGIT) r;
  VSECPRIM_P2VAR_PARA(actBNDIGIT) s;

  /* any null pointers ? */
  if ((!message) || (!signature_r) || (!signature_s) || (!publickey_x) || (!publickey_y))
  {
    return actEXCEPTION_NULL;
  }

  /* assign temporary bignum workspace */
  wksp_ptr += actECBasicWksp(p_length, n_length); /* this workspace has been used by actECInit() */
  /* e, r, s, Q */ /* adjust workspace pointer */
  e = wksp_ptr;
  wksp_ptr += n_length;
  r = wksp_ptr;
  wksp_ptr += n_length;
  s = wksp_ptr;
  wksp_ptr += n_length;
  Q.x = wksp_ptr;
  wksp_ptr += p_length;
  Q.y = wksp_ptr;
  wksp_ptr += p_length;

  /* initialization of e */
  if ((message_len <= 0) || (message_len > n_bytes))
  {
    return actEXCEPTION_LENGTH;
  }
  actBNSetOctetString(e, n_length, message, message_len);
  if (message_len == n_bytes)
  {
    if (actBNGetBitLength(e, n_length) > actBNGetBitLength(Curve->n_field.m, n_length))
    {
      return actEXCEPTION_LENGTH;
    }
  }

  /* initialization of r, s */
  if ((signature_r_len <= 0) || (signature_r_len > n_bytes) || (signature_s_len <= 0) || (signature_s_len > n_bytes))
  {
    return actSIGNATURE_OUT_OF_RANGE;
  }
  actBNSetOctetString(r, n_length, signature_r, signature_r_len);
  actBNSetOctetString(s, n_length, signature_s, signature_s_len);
  if (actBNIsZero(r, n_length) == TRUE)
  {
    return actSIGNATURE_OUT_OF_RANGE;
  }
  if (actBNCompare(r, Curve->n_field.m, n_length) >= actCOMPARE_EQUAL)
  {
    return actSIGNATURE_OUT_OF_RANGE;
  }
  if (actBNIsZero(s, n_length) == TRUE)
  {
    return actSIGNATURE_OUT_OF_RANGE;
  }
  if (actBNCompare(s, Curve->n_field.m, n_length) >= actCOMPARE_EQUAL)
  {
    return actSIGNATURE_OUT_OF_RANGE;
  }

  /* initialization of Q */
  actBNSetOctetString(Q.x, p_length, publickey_x, p_bytes);
  actBNSetOctetString(Q.y, p_length, publickey_y, p_bytes);
  Q.is_affine = 1;
  Q.is_infinity = 0;

  /*
     that's it: finally call ECDSA vp
   */
  return actECDSAvp(e, r, s, &Q, Curve, wksp_ptr);
}

/****************************************************************************
 **
 ** FUNCTION:
 **  actECDSAInitSign
 **
 **  This function initializes the ECC workspace.
 **
 ** input:
 ** - domain:      domain parameter
 ** - domain_ext:  domain parameter extensions (Montgomery constants etc.)
 ** - speedup_ext: (optional) precomputations (for ECDSA-Sign, -GenKey)
 ** - wksp:        workspace
 ** - wksp_len:    length of workspace in bytes
 **
 ** output:
 ** - wksp:       initialized workspace
 ** - returns:    actEXCEPTION_NULL         if an input parameter is NULL
 **               actEXCEPTION_DOMAIN       domain decoding error
 **               actEXCEPTION_DOMAIN_EXT   domain_ext decoding error
 **               actEXCEPTION_SPEEDUP_EXT  speedup_ext decoding error
 **               actEXCEPTION_MEMORY       wksp_len to small
 **               actOK                     else
 **
 ** assumes:
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDSAInitSign(actPROMU8 domain, actPROMU8 domain_ext, actPROMU8 speedup_ext, VSECPRIM_P2VAR_PARA(actU8) wksp, int wksp_len)
{
  return actECInit(domain, domain_ext, speedup_ext, actEC_ALGO_FLAG_SP, wksp, wksp_len);
}

/****************************************************************************
 **
 ** FUNCTION:
 **  actECDSASign
 **
 **  This function signs the message value under usage of the private key.
 **
 ** input:
 ** - message:     pointer to message to be signed
 ** - message_len: length of message in bytes
 ** - privatekey:  the private key d of the key pair (d, Q)
 ** - signature_r: buffer to store the signature component r of (r, s)
 ** - signature_r_len: length of signature_r buffer
 ** - signature_s: buffer to store the signature component s of (r, s)
 ** - signature_s_len: length of signature_s buffer
 ** - wksp:        workspace
 **
 ** output:
 ** - signature_r: signature component r of (r, s)
 ** - signature_r_len: length of signature_r
 ** - signature_s: signature component s of (r, s)
 ** - signature_s_len: length of signature_s
 ** - returns:    actEXCEPTION_NULL       if an input parameter is NULL
 **               actEXCEPTION_MEMORY     signature buffer to small
 **               actEXCEPTION_LENGTH     wrong message_len (in bits)
 **               actEXCEPTION_PRIVKEY    if (d==0||d>=n)
 **               actEXCEPTION_UNKNOWN    internal error (result point
 **                                       not on curve)
 **               actOK                   else
 **
 ** assumes:
 ** - wksp is initialized with actECDSAInitSign
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDSASign(VSECPRIM_P2CONST_PARA(actU8) message, int message_len,
                                          VSECPRIM_P2CONST_PARA(actU8) privatekey, VSECPRIM_P2VAR_PARA(actU8) signature_r,
                                          VSECPRIM_P2VAR_PARA(int) signature_r_len, VSECPRIM_P2VAR_PARA(actU8) signature_s, VSECPRIM_P2VAR_PARA(int) signature_s_len, VSECPRIM_P2VAR_PARA(actU8) wksp)
{
  actRETURNCODE returncode;
  VSECPRIM_P2VAR_PARA(actECCURVE) Curve = (VSECPRIM_P2VAR_PARA(actECCURVE)) wksp;
  VSECPRIM_P2VAR_PARA(actBNDIGIT) wksp_ptr = (VSECPRIM_P2VAR_PARA(actBNDIGIT)) (wksp + sizeof(actECCURVE));
  int p_length = Curve->p_field.m_length;
  int n_length = Curve->n_field.m_length;
  int n_bytes = Curve->n_field.m_byte_length;
  VSECPRIM_P2VAR_PARA(actBNDIGIT) e;
  VSECPRIM_P2VAR_PARA(actBNDIGIT) d;
  VSECPRIM_P2VAR_PARA(actBNDIGIT) r;
  VSECPRIM_P2VAR_PARA(actBNDIGIT) s;

  /* any null pointers ? */
  if ((!message) || (!signature_r) || (!signature_s) || (!privatekey))
  {
    return actEXCEPTION_NULL;
  }

  /* check workspace and signature buffer length */
  if ((*signature_r_len < n_bytes) || (*signature_s_len < n_bytes))
  {
    return actEXCEPTION_MEMORY;
  }

  /* assign temporary bignum workspace */
  wksp_ptr += actECBasicWksp(p_length, n_length); /* this workspace has been used by actECInit() */
  /* e, d, r, s */ /* adjust workspace pointer */
  e = wksp_ptr;
  wksp_ptr += n_length;
  d = wksp_ptr;
  wksp_ptr += n_length;
  r = wksp_ptr;
  wksp_ptr += n_length;
  s = wksp_ptr;
  wksp_ptr += n_length;

  /* initialization of d */
  actBNSetOctetString(d, n_length, privatekey, n_bytes);

  /* check 0 < d < n before continue */
  if (actBNIsZero(d, n_length) == TRUE)
  {
    return actEXCEPTION_PRIVKEY;
  }
  if (actBNCompare(d, Curve->n_field.m, n_length) >= actCOMPARE_EQUAL)
  {
    return actEXCEPTION_PRIVKEY;
  }

  /* initialization of e */
  if ((message_len <= 0) || (message_len > n_bytes))
  {
    return actEXCEPTION_LENGTH;
  }
  actBNSetOctetString(e, n_length, message, message_len);
  if (message_len == n_bytes)
  {
    if (actBNGetBitLength(e, n_length) > actBNGetBitLength(Curve->n_field.m, n_length))
    {
      return actEXCEPTION_LENGTH;
    }
  }

  /*
     call ECDSA sp and calculate signature (r, s)
   */

  returncode = actECDSAsp(e, d, r, s, Curve, wksp_ptr);

  /* if ok, write (r, s) to output */
  if (returncode == actOK)
  {
    actBNOctetString(signature_r, n_bytes, r, n_length);
    actBNOctetString(signature_s, n_bytes, s, n_length);
    *signature_r_len = *signature_s_len = n_bytes;
  }

  /* wipe local d */
  actBNSetZero(d, n_length);

  return returncode;
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTIECDSA_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actIECDSA.c
 *********************************************************************************************************************/
