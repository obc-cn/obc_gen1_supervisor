/****************************************************************************
 ** Main author: Ubr                     Creation date: 03/21/06
 ** Author: mns                          JustDate: 03/21/06
 ** Workfile: actIRSAPublic.c               Revision: 1435
 ** NoKeywords:
 **
 **
 ** \copyright(cv cryptovision GmbH, 1999 - 2006                         )
 **
 ** \version(1.0 (beta)                                                 )
 ***************************************************************************/

/****************************************************************************
 **
 **     Part of the actCLibrary
 **
 **     Layer: User Module
 **
 ***************************************************************************/

/****************************************************************************
 **
 ** This file contains: Implementation file for actIRSA.h
 **
 ** constants:
 **
 ** types:
 **
 ** global variables:
 **
 ** macros:
 **
 ** functions:
 **   actRSAInitPublicKeyOperation
 **   actRSAPublicKeyOperation
 **
 ***************************************************************************/

#include "actIRSA.h"
#include "actIRSAExp.h"
#include "actBigNum.h"
#include "actUtilities.h"

#if (VSECPRIM_ACTIRSAPUBLIC_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTION:
 **  actRSAInitPublicKeyOperation
 **
 **  This function initializes the RSA workspace.
 **
 ** input:
 ** - modulus:                pointer to the public modulus
 ** - modulus_len:            length of modulus
 ** - public_exponent:        pointer to the public exponent e
 ** - public_exponent_len:    length of public_exponent
 ** - wksp:                   workspace
 ** - wksp_len:               length of workspace in bytes
 **
 ** output:
 ** - wksp:       initialized workspace
 ** - returns:    actEXCEPTION_NULL       if an input parameter is NULL
 **               actEXCEPTION_MEMORY     wksp_len to small
 **               actRSA_PARAM_OUT_OF_RANGE   n has leading zero digits
 **               actEXCEPTION_PUBKEY     e==0 || e>=n
 **               actOK                   else
 **
 ** assumes:
 **
 ** uses:
 ** trace:  CREQ-132100, CREQ-132101, CREQ-140951, CREQ-140952, CREQ-132102, CREQ-132103, CREQ-132104, CREQ-132105, CREQ-132106, CREQ-132107
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actRSAInitPublicKeyOperation(
   actPKey modulus, int modulus_len,
   actPKey public_exponent, int public_exponent_len,
   VSECPRIM_P2VAR_PARA(actU8) wksp, int wksp_len,
   VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void))
{
   return actRSAInitExponentiation(modulus, modulus_len, public_exponent, public_exponent_len, wksp, wksp_len, actRSA_PUBLIC_KEY_OPERATION, watchdog);
}


/****************************************************************************
 **
 ** FUNCTION:
 **  actRSAPublicKeyOperation
 **
 **  This function calculates c = m^e mod n.
 **
 ** input:
 ** - message:       pointer to the message m to be encrypted
 ** - message_len:   length of message
 ** - cipher:        buffer to store the cipher text cipher c
 ** - *cipher_len:   length of cipher buffer
 ** - wksp:          workspace
 **
 ** output:
 ** - cipher:        the encrypted message
 ** - *cipher_len:   length of cipher buffer used (n_bytes)
 ** - returns:    actEXCEPTION_NULL          if an input parameter is NULL
 **               actEXCEPTION_MEMORY        *cipher_len < n_bytes
 **               actRSA_PARAM_OUT_OF_RANGE  message >= n
 **               actOK                      else
 **
 ** assumes:
 ** - wksp is initialized by actRSAInitPublicKeyOperation() function
 **
 ** uses:
 ** trace:  CREQ-132100, CREQ-132101, CREQ-140951, CREQ-140952, CREQ-132102, CREQ-132103, CREQ-132104, CREQ-132105, CREQ-132106, CREQ-132107
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actRSAPublicKeyOperation(
   VSECPRIM_P2CONST_PARA(actU8) message, int message_len,
   VSECPRIM_P2VAR_PARA(actU8) cipher, VSECPRIM_P2VAR_PARA(int) cipher_len,
   VSECPRIM_P2VAR_PARA(actU8) wksp, VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void))
{
   return actRSAExponentiation(message, message_len, cipher, cipher_len, wksp, watchdog);
}

/****************************************************************************
 **
 ** FUNCTION:
 **  actRSAPublicKeyGetBitLength
 **
 **  This function returns the exact length in bits of the modulus.
 **
 ** input:
 ** - wksp:          workspace
 **
 ** output:
 ** - returns:    Exact length in bits of the modulus
 **
 ** assumes:
 ** - wksp is initialized by actRSAInitPublicKeyOperation() function
 **
 ** uses:
 ** trace:  CREQ-132100, CREQ-132101, CREQ-140951, CREQ-140952, CREQ-132102, CREQ-132103, CREQ-132104, CREQ-132105, CREQ-132106, CREQ-132107
 ***************************************************************************/
VSECPRIM_FUNC(int) actRSAPublicKeyGetBitLength(VSECPRIM_P2CONST_PARA(actU8) wksp)
{
   VSECPRIM_P2CONST_PARA(actBNRING)n_ring =
      (VSECPRIM_P2CONST_PARA(actBNRING))wksp;

   return actBNGetBitLength(n_ring->m, n_ring->m_length);
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTIRSAPUBLIC_ENABLED == STD_ON) */
