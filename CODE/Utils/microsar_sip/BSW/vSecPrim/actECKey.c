/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actECKey.c
 *        \brief  Implementation file for actECKey.h.
 *
 *      \details This file is part of the embedded systems library cvActLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

/****************************************************************************
 **
 ** This file contains: Implementation file for actECKey.h.
 **
 ** constants:
 **
 ** types:
 **
 ** macros:
 **
 ** functions:
 **   actECkg
 **
 ***************************************************************************/

#include "actECKey.h"
#include "actECLengthInfo.h"

#if (VSECPRIM_ACTECKEY_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/****************************************************************************
 ** Function Prototypes
 ***************************************************************************/
#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTION:
 ** actECkgp
 **
 **  This function calculates Q = d * G.
 **
 ** input:                                              (length in digits)
 ** - d:     the private key                                n_length
 ** - Q:     to store the public key                        3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_byte_length: length of p in byte
 ** -        p_field->RR:       constant for Montgomery     p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->one:      1-element of the field      p_length
 ** -        p_field->prime_structure==curveid
 ** -        p_field->window_size for modular exponentiation
 ** - Curve->n_field: the field GF(n):
 ** -        n_field->m:        the modulus n               n_length
 ** -        n_field->m_length: equals to n_length
 ** - Curve->Gx_R:   basepoint coordinate x                 p_bytes
 ** - Curve->Gy_R:   basepoint coordinate y                 p_bytes
 ** - Curve->t[0..(i-1)]:  i temporary BigNum variables,   i*(max_length+1)
 **                        where i = actEC_BIGNUM_TMP
 **        [ Notation: max_length = max(p_length, n_length) ]
 ** - wksp_ptr:  temporary BigNum workspace for G           3*p_length
 **
 ** output:
 ** - Q:     the public key (d*G)                           3*p_length
 ** - returns:    actEXCEPTION_UNKNOWN    internal error (result point
 **                                       not on curve)
 **               actOK                   else
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for the usage of this function is the
 **   actECDSAVerify(.) implementation in actIECDSA.c, which is basically
 **   a call of actECDSAvp(.) with all previous initializations.
 **
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECkgp(
   VSECPRIM_P2CONST_PARA(actBNDIGIT) d, VSECPRIM_P2VAR_PARA(actECPOINT) Q,
   VSECPRIM_P2VAR_PARA(actECCURVE) Curve, VSECPRIM_P2VAR_PARA(actBNDIGIT) wksp_ptr)
{
   actECPOINT Prec;
   actLengthType p_length = Curve->p_field.m_length;

   /* assign workspace for Prec */    /* adjust workspace pointer */
   Prec.x = wksp_ptr;                 wksp_ptr += p_length;
   Prec.y = wksp_ptr;

   /* Compute Q = d*G */
   actECPMultG(d, &Prec, Q, Curve);
   /* Transform to affine */
   if (!actECPToAffineFromMont(Q, Curve, 0))
   {
     return actEXCEPTION_UNKNOWN;
   }

   return actOK;
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTECKEY_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actECKey.c
 *********************************************************************************************************************/
