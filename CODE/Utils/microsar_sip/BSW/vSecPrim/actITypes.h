/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actITypes.h
 *        \brief  Basic type definitions.
 *
 *      \details Currently the actClib version is used.
 *               This file is part of the embedded systems library actCLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#ifndef ACTITYPES_H
# define ACTITYPES_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "actPlatformTypes.h"
# include "actConfig.h"

/****************************************************************************
 ** Types and constants
 ***************************************************************************/

/* basic unsigned int types */
typedef actPlatformUint8 actU8;
typedef actPlatformUint16 actU16;
typedef actPlatformUint32 actU32;
# ifdef ACT_PLATFORM_UINT64_AVAILABLE
typedef actPlatformUint64 actU64;
# endif

# ifndef TRUE /* COV_VSECPRIM_STD_VALUES */
#  define TRUE                                                        (1u)
# endif

# ifndef FALSE /* COV_VSECPRIM_STD_VALUES */
#  define FALSE                                                       (0u)
# endif

typedef actU32 actLengthType;
typedef actLengthType actBNLENGTH; 
typedef unsigned char actBoolean; /*        TRUE .. FALSE           */

/* Pointer to key data usually located in ROM but moved to RAM for special use-case,
   (e.g. RSA keys) */
VSECPRIM_P2CONST_TYPE(VSECPRIM_NONE, actU8) actPKey;

/* Pointer to byte(s) located in ROM, e.g. digest information */
VSECPRIM_P2ROMCONST_TYPE(VSECPRIM_NONE, actU8) actPROMU8;


/****************************************************************************
 ** Main arithmetic data types:    actLengthType, actBNDIGIT, actBNDDIGIT
 **
 **  A long number is represented as a little endian array of actBNDIGITs,
 **  the length of this array will be represented as actLengthType.
 **
 ** CAUTION:
 **  These types are strongly dependend of the used hardware platform!
 **  The arithmetic module is designed to work on 8-Bit, 16-Bit and 32-Bit
 **  platforms, which means actBNDIGIT should be an unsigned type of this
 **  size. Basically your choise have to fulfill these conditions:
 **
 ** MAJOR CONDITIONS:
 **  - actLengthType is a unsigned type
 **  - actBNDIGIT and actBNDDIGIT are unsigned types
 **  - an actBNDIGIT has 8, 16 or 32 bits
 **  - an actBNDDIGIT is exactly twice as long as an actBNDIGIT
 **  - for efficiency the processor should be able to multiply (add) two
 **    actDIGITs into one actBNDDIGIT
 ****************************************************************************
 **
 ** !!! NOTE !!! NOTE !!! NOTE !!!
 **
 **  The arithmetic modules were successfully tested on various 16-Bit and
 **  32-Bit platforms, which are the recommended platforms. On these
 **  platforms, switches to 8-Bit digit size were also tested, but not on
 **  a 'real' 8-Bit platform!
 **  If an 'int' has only 8 bit, there may be some 'length problems' left
 **  for numbers with more than 127 bytes (if actLengthType is an 'unit32').
 **  Also, functions of type 'int' may have incorrect return values (>127).
 ***************************************************************************/

/*
The BigNum digit and double digit types:
 */
# define actBN_BITS_PER_DIGIT                                         (ACT_PLATFORM_BITS_PER_DIGIT)
# define actBN_BYTES_PER_DIGIT                                        ((actBN_BITS_PER_DIGIT)>>3)


# if (actBN_BITS_PER_DIGIT == 8) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
#  define actBN_BITS_PER_DIGIT_IS_VALID
/*  8-Bit digits, 16-Bit digits */
typedef actU8 actBNDIGIT;
typedef actU16 actBNDDIGIT;
# endif
# if (actBN_BITS_PER_DIGIT == 16)
#  define actBN_BITS_PER_DIGIT_IS_VALID
/* 16-Bit digits, 32-Bit digits */
typedef actU16 actBNDIGIT;
typedef actU32 actBNDDIGIT;
# endif
# if (actBN_BITS_PER_DIGIT == 32)
#  define actBN_BITS_PER_DIGIT_IS_VALID
/* 32-Bit digits, 64-Bit digits */
#  if defined(ACT_PLATFORM_UINT64_AVAILABLE)
typedef actU32 actBNDIGIT;
typedef actU64 actBNDDIGIT;
#  else
#   error UNABLE TO DEFINE 64 BIT DOUBLE DIGIT TYPE (NOT AVAILABLE)
#  endif
# endif
# ifndef actBN_BITS_PER_DIGIT_IS_VALID
#  error actBN_BITS_PER_DIGIT value unsupported
# endif

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* Our returncodes are actBYTEs */
typedef actU32 actRETURNCODE;

/* no failures */
# define actOK                                                        (0x00u)
/* internal error during computation */
# define actEXCEPTION_UNKNOWN                                         (0x01u)

# define actCOMPARE_SMALLER                                           (0x10u)
# define actCOMPARE_EQUAL                                             (0x11u)
# define actCOMPARE_LARGER                                            (0x12u)


/* null pointer exception */
# define actEXCEPTION_NULL                                            (0xFFu)
/* invalid curve domain or extension data (decoding error) */
# define actEXCEPTION_DOMAIN                                          (0xFEu)
# define actEXCEPTION_DOMAIN_EXT                                      (0xFDu)
# define actEXCEPTION_SPEEDUP_EXT                                     (0xFCu)
/* invalid private key d (d==0 || d>=n) */
# define actEXCEPTION_PRIVKEY                                         (0xFBu)
/* invalid point (invalid TAG or not on curve) */
# define actEXCEPTION_POINT                                           (0xFAu)
/* invalid public key exception */
# define actEXCEPTION_PUBKEY                                          (0xF9u)
/* invalid length */
# define actEXCEPTION_LENGTH                                          (0xF8u)
/* invalid algorithm identifier */
# define actEXCEPTION_ALGOID                                          (0xF7u)
/* unpad error */
# define actEXCEPTION_PADDING                                         (0xF6u)
/* bad input length */
# define actEXCEPTION_INPUT_LENGTH                                    (0xF5u)
/* workspace or buffer too small */
# define actEXCEPTION_MEMORY                                          (0xF4u)
/* signature does NOT verify */
# define actVERIFICATION_FAILED                                       (0xF3u)
/* invalid operating mode */
# define actEXCEPTION_MODE                                            (0xF2u)
/* random generator failed */
# define actEXCEPTION_RANDOM                                          (0xF1u)

/* Padding mode */
# define actPADDING_PM_OFF                                            (0x00u)
# define actPADDING_PM_ONEWITHZEROES                                  (0x10u)
/* PKCS #7 */
# define actPADDING_PM_PKCS7                                          (0x20u)

# define actPADDING_PM_MASK                                           (0xF0u)

/****************************************
 * AES
 ****************************************/
# define actAES_BLOCK_SIZE                                            (16u)

/* Block mode */
/* ECB */
# define actAES_BM_ECB                                                (0x00u)
/* CBC */
# define actAES_BM_CBC                                                (0x01u)

# define actAES_BM_MASK                                               (0x0Fu)

/* Check block mode mask against padding mode mask (== 0) */
# if ((actAES_BM_MASK & actPADDING_PM_MASK) != 0x00u)
#  error Bits of actAES_BM_MASK and actPADDING_PM_MASK overlap
# endif

/* Compatibility modes */
/* ECB with PKCS #7 */
# define actAES_ECB                                                   (actAES_BM_ECB | actPADDING_PM_PKCS7)
/* CBC with PKCS #7 */
# define actAES_CBC                                                   (actAES_BM_CBC | actPADDING_PM_PKCS7)

/****************************************
 * SHA
 ****************************************/
# define actHASH_SIZE_SHA                                             (20u)
# define actHASH_BLOCK_SIZE_SHA                                       (64u)

# define actHASH_SIZE_SHA224                                          (28u)
# define actHASH_SIZE_SHA256                                          (32u)
# define actHASH_BLOCK_SIZE_SHA256                                    (64u)

# define actHASH_SIZE_SHA512_224                                      (28u)
# define actHASH_SIZE_SHA512_256                                      (32u)
# define actHASH_SIZE_SHA384                                          (48u)
# define actHASH_SIZE_SHA512                                          (64u)
# define actHASH_BLOCK_SIZE_SHA512                                    (128u)

/****************************************
 * FIPS-186.2
 ****************************************/
/* Byte_length of base 2^b, b = 160  */
# define actFIPS186_BASE_LENGTH                                       (20u)
# define actFIPS186_SEED_SIZE                                         actFIPS186_BASE_LENGTH

/****************************************
 * Ed25519/X25519
 ****************************************/
# define actEd25519_KEY_SIZE                                          (32u)
# define actEd25519pure                                               (0u)
# define actEd25519ctx                                                (1u)
# define actEd25519ph                                                 (2u)
# define actX25519_KEY_SIZE                                           (32u)

# define BNDIGITS_256                                                 (256u /8u /sizeof (actBNDIGIT))
# define BNDIGITS_512                                                 (512u /8u /sizeof (actBNDIGIT))
# define BNBYTES_256                                                  (256u /8u /sizeof (actU8))
# define BNBYTES_512                                                  (512u /8u /sizeof (actU8))
# define MASK_MSBit                                                   (0x7FFFFFFFuL >> (32u - actBN_BITS_PER_DIGIT))


/****************************************
 * SipHash
 ****************************************/
/* Configure SipHash-2-4 */
#  define ACT_SIPHASH_CROUNDS                                         (2u)
#  define ACT_SIPHASH_DROUNDS                                         (4u)

# define actSIPHASH_BLOCK_SIZE                                        (8u)

/****************************************
 * GHash
 ****************************************/
/*
###########################################################################
##
##   LIMIT SPEED UP SETTING!
##   Please read the "library/es Porting Guide"
##   on how to further speed up GHash!
##
###########################################################################
 */
# if (actGHASH_SPEED_UP > 1)  /* COV_VSECPRIM_GHASH_SPEED_UP XF */
#  define actGHASH_SPEED_UP                                           1 /*  limit speedup  */
# endif

# define actGHASH_BLOCK_SIZE                                          (16u)
# define actGHASH_WORD_SIZE                                           (4u)
# define actGHASH_WORDS_PER_BLOCK                                     (actGHASH_BLOCK_SIZE/actGHASH_WORD_SIZE)

/****************************************
 * GHash
 ****************************************/
# define actGCM_BLOCK_SIZE                                            16u
# define actGCM_BYTE_SIZE                                             8u
# define actGCM_IV_DEFAULT_SIZE                                       12u
# define actGCM_TAG_LENGTH                                            16u


/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/* expand a 32-bit value into a set of actBNDIGITs depending upon the actBNDIGIT size */
/* PRQA S 3453 11 */ /* MD_MSR_FctLikeMacro */
# if (8 == actBN_BITS_PER_DIGIT) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
#  define DIGITs(value)                                               ((actBNDIGIT) ((value) >>  0)), \
  ((actBNDIGIT)((value) >> 8)), \
  ((actBNDIGIT)((value) >> 16)), \
  ((actBNDIGIT)((value) >> 24))
# elif (16 == actBN_BITS_PER_DIGIT)
#  define DIGITs(value)                                               ((actBNDIGIT) ((value) >>  0)), \
  ((actBNDIGIT)((value) >> 16))
# elif (32 == actBN_BITS_PER_DIGIT)
#  define DIGITs(value)                                               (value)
# else
#  error actBN_BITS_PER_DIGIT value unsupported
# endif


/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/* Context structure of the AES algorithm */
typedef struct
{
  actU32 e_key[64];
  actLengthType key_dword_len;
  actLengthType buffer_used;
  actU8 prev_block[actAES_BLOCK_SIZE];
  actU8 buffer[actAES_BLOCK_SIZE];
  actU8 mode;
} actAESSTRUCT;

/* Workspace structure for AES CMAC */
typedef actAESSTRUCT actCMACAESSTRUCT;

/* Workspace structure for SHA-1 */
typedef struct
{
  actU32 H[5];                  /* message digest state buffer */
  actU32 low_count, hi_count;   /* 64 bit input count */
  actLengthType buffer_used;              /* number of bytes saved in buffer */
  actU8 buffer[actHASH_BLOCK_SIZE_SHA]; /* remaining data buffer */
} actSHASTRUCT;

/* Workspace structure for SHA-224 & SHA-256 */
typedef struct
{
  actU32 H[8]; /* message digest state buffer */
  actU32 low_count, hi_count; /* 64 bit input count */
  actLengthType buffer_used; /* number of bytes saved in buffer */
  actU16 result_length; /* number of bytes to return as hash-value */
  actU8 buffer[actHASH_BLOCK_SIZE_SHA256]; /* remaining data buffer */
} actSHA224STRUCT, actSHA256STRUCT;

/* Workspace structure for SHA-384 & SHA-512 */
typedef struct
{
# ifdef ACT_PLATFORM_UINT64_AVAILABLE
  actU64 H[8]; /* message digest state buffer */
  actU64 count_L, count_H; /* 128 bit input count */
# else
  actU32 H[16]; /* message digest state buffer */
  actU32 count_L, count_2, count_3, count_H; /* 128 bit input count */
# endif
  actLengthType buffer_used; /* number of bytes saved in buffer */
  actU16 result_length; /* number of bytes to return as hash-value */
  actU8 buffer[actHASH_BLOCK_SIZE_SHA512]; /* remaining data buffer */
} actSHA512_224STRUCT, actSHA512_256STRUCT, actSHA384STRUCT, actSHA512STRUCT;

/* Workspace structure for FIPS186 */
typedef struct
{
  actLengthType bytes_remain;
  actU8 X_KEY[actFIPS186_BASE_LENGTH];
  actSHASTRUCT sha1;
} actFIPS186STRUCT;

/* Workspace structure for KDFX963 */
typedef struct
{
  actSHASTRUCT actwsHash;
  actU8 hashBuf[actHASH_SIZE_SHA];
} actKDFX963STRUCT;


/* Workspace structure for KDF X9.63 w/ SHA-256 */
typedef struct
{
  actSHA256STRUCT actwsHash;
  actU8           hashBuf[actHASH_SIZE_SHA256];
} actKDFX963SHA256;

/* Big Number Types */
/* Curve point represented in extended homogeneous coordinates. */
typedef struct
{
  actBNDIGIT x[BNDIGITS_256];
  actBNDIGIT y[BNDIGITS_256];
  actBNDIGIT z[BNDIGITS_256];
  actBNDIGIT t[BNDIGITS_256];
} actPoint;

/*
BigNum modular ring type

Contains:
- the modulus (MUST BE ODD for Montgomery !!!)
- length information of the modulus
- additional constants for Montgomery
- optimization switch for Montgomery
 */
typedef struct
{
  VSECPRIM_P2VAR_PARA(actBNDIGIT) m;  /* ring modulus */
  actLengthType m_length;         /* length of m in digit */
  actLengthType m_byte_length;    /* length of m in byte */
  VSECPRIM_P2VAR_PARA(actBNDIGIT) RR; /* RR = R^2, R = base^m_length mod m */
  actBNDIGIT m_bar;             /* mbar = -(m^-1) mod base */
# if (actBN_MONT_MUL_VERSION==1)
  actU32 prime_structure;          /* flag for optimization in MontMul */
# endif
# if (actBN_MOD_EXP_WINDOW_SIZE==0) /* variable k-bit window algorithm */ /* COV_VSECPRIM_NOT_CONFIGURABLE XF */
  actU32 window_size;              /* is used for modular exponentiation */
# endif
} actBNRING;


typedef struct
{
  actBNDIGIT tempA[BNDIGITS_256]; /* used in actEd25519* functions */
  actBNDIGIT tempB[BNDIGITS_256];
  actBNDIGIT tempC[BNDIGITS_256];
  actBNDIGIT tempD[BNDIGITS_256];
  actBNDIGIT tempE[BNDIGITS_256]; /* used in actCurve25519* functions */
  actBNDIGIT tempF[BNDIGITS_256];
  actBNDIGIT tempG[BNDIGITS_256];
  actBNDIGIT tempH[BNDIGITS_256];
  actBNDIGIT temp1[BNDIGITS_512]; /* 512 bit temp variable (used for multiplication) */
  actBNDIGIT tempQ[BNDIGITS_256]; /* 256 bit temp variable (used for reduction) */
} actTempType;

/* Curve structure */
typedef struct
{
  actBNDIGIT prime[BNDIGITS_256];
  actBNDIGIT order[BNDIGITS_256];
  actBNDIGIT orderB[BNDIGITS_256 + 1u];  /* 2^512 / q (order) */

  actBNRING pRing;
  actBNRING qRing;
  actTempType auxVar;
} actCurve25519ws;            /* size <= 520 bytes (64bit pointers) */


/* Workspace structure for Ed25519 */
typedef struct
{
  actU32 instance;                        /* algorithm instance  (plain, context, pre-hash) */
  union {                                 /* PRQA S 0750 */ /* MD_MSR_Union */
    actSHA512STRUCT sha512;               /* SHA-512         - used for pre-hash and Ed25519 */
    actPoint point_A;                     /* A:  need 128 bytes - actSHA512Struct is 212 bytes */
    actPoint point_rB;                    /* rB: need 128 bytes - actSHA512Struct is 212 bytes */
    actPoint point_sB;                    /* sB: sB / A are NEVER used concurrently! */
  } shaWs;
  union {                                 /* PRQA S 0750 */ /* MD_MSR_Union */
    actU8 hashValue[actHASH_SIZE_SHA512]; /* hashed message  - used for pre-hash */
    actBNDIGIT bnDigit_s;                 /* s: need  32 bytes - hashValue is 64 bytes */
    actBNDIGIT bnDigit_k;                 /* k: k / s  are NEVER used concurrently! */
  } hashBuf;
  actU8 privateKey[actHASH_SIZE_SHA512];  /* private scalar & prefix */
  union {                                 /* PRQA S 0750 */ /* MD_MSR_Union */
    actU8 publicKey[actEd25519_KEY_SIZE]; /* public key  */
    actBNDIGIT bnDigit_a;                 /* a: need  32 bytes - publicKey is 32 bytes */
  } pubKeyBuf;
  VSECPRIM_P2CONST_PARA(actU8) message;   /* input to signature (probably hashValue) */
  actU32 msgLen;                          /* length of message */
  VSECPRIM_P2CONST_PARA(actU8) context;   /* context */
  actU32 ctxLen;                          /* length of context */
  actCurve25519ws coreWS;                 /* workspace for calculation core (layout depends on core used) */
} actEd25519STRUCT;

/* Workspace structure for X25519 */
typedef struct
{
  actU8 privateKey[actX25519_KEY_SIZE];   /* private key */
  union {                                 /* PRQA S 0750 */ /* MD_MSR_Union */
    actU8 uCoord[actX25519_KEY_SIZE];     /* u (x) coordinate */
    actBNDIGIT bnDigit_x_1[BNDIGITS_256]; /* x_1: x_1 / u  are NEVER used concurrently! */
  } basePointBuf;
  actCurve25519ws coreWS;                 /* workspace for calculation core */
} actX25519STRUCT;

/* Compatibility */
typedef actX25519STRUCT actX25519Struct;
typedef actEd25519STRUCT actEd25519Struct;

/* Workspace structure for SipHash */
typedef struct
{
# if defined ACT_PLATFORM_UINT64_AVAILABLE
  actU64 v0;
  actU64 v1;
  actU64 v2;
  actU64 v3;
# endif
  actU8 buffer[actSIPHASH_BLOCK_SIZE];
  actU32 length;
  actU8 buffer_used;
} actSipHashSTRUCT;

/* Workspace structure for GHash */
typedef struct
{
  actLengthType buffer_used;          /* number of bytes saved in buffer */
  actU8 buffer[actGHASH_BLOCK_SIZE];  /* the input buffer */
  actU32 H[actGHASH_WORDS_PER_BLOCK]; /* the hash subkey */
  actU8 Y[actGHASH_BLOCK_SIZE];       /* the hash state buffer */
# if (actGHASH_SPEED_UP >= 4)                                           /* COV_VSECPRIM_GHASH_SPEED_UP XF */
  actU32 M[256][4];                   /* the precomputation table */
# elif (actGHASH_SPEED_UP >= 1)                                         /* COV_VSECPRIM_GHASH_SPEED_UP TX */
  actU32 M[16][4];                    /* the precomputation table */
# endif
} actGHASHSTRUCT;

/* Workspace structure for GCM */
typedef struct
{
  actAESSTRUCT aes;                  /* the aes context structure */
  actGHASHSTRUCT ghash;              /* the ghash context structure */
  actU8 J0[actGCM_BLOCK_SIZE];       /* the first counter block depending on the iv for the tag computation */
  actU8 J[actGCM_BLOCK_SIZE];        /* the counter block that is iterated */
  actLengthType buffer_used;         /* number of bytes saved in buffer */
  actU8 buffer[actGCM_BLOCK_SIZE];   /* the input buffer */
  uint8 state;                       /* the current internal state (authdata or ciphertext) */
  actU32 count[4];                   /* BE 64 bit AAD count and data count */
} actGCMSTRUCT;

#endif /* ACTITYPES_H */

/**********************************************************************************************************************
 *  END OF FILE: actITypes.h
 *********************************************************************************************************************/
