/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actECTools.h
 *        \brief  Interface EC domain parameter initialization.
 *
 *      \details This file is part of the embedded systems library cvActLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#ifndef ACTECTOOLS_H
# define ACTECTOOLS_H


# include "actITypes.h"

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Constants
 ***************************************************************************/

# define actEC_ALGO_FLAG_KG                                           (1)
# define actEC_ALGO_FLAG_SP                                           (2)
# define actEC_ALGO_FLAG_VP                                           (3)
# define actEC_ALGO_FLAG_DH_PRIM                                      (4)
# define actEC_ALGO_FLAG_DH                                           (5)
# define actEC_ALGO_FLAG_BDKA                                         (6)

# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
extern "C" {
# endif


/****************************************************************************
 **
 ** FUNCTION:
 **  actECParseDomain
 **
 **  Function to parse domain TLV object.
 **
 ** input:
 ** - domain:     domain parameter
 ** - p_bytes:    |
 ** - p_addr:     |
 ** - a_addr:     |  buffers to return extracted addresses or length values
 ** - n_bytes:    |
 ** - n_addr:     |
 **
 ** output:
 ** - p_bytes:    |
 ** - p_addr:     |
 ** - a_addr:     |  the extracted addresses or length values
 ** - n_bytes:    |
 ** - n_addr:     |
 ** - returns:    actEXCEPTION_DOMAIN       domain decoding error
 **               actOK                     else
 **
 ** assumes:
 **
 ** uses:
 **
 ***************************************************************************/
VSECPRIM_FUNC( actRETURNCODE ) actECParseDomain(
   actPROMU8 domain,
   VSECPRIM_P2VAR_PARA( int ) p_bytes,
   VSECPRIM_P2VAR_PARA( actPROMU8 ) p_addr,
   VSECPRIM_P2VAR_PARA( actPROMU8 ) a_addr,
   VSECPRIM_P2VAR_PARA( int ) n_bytes,
   VSECPRIM_P2VAR_PARA( actPROMU8 ) n_addr
);


/****************************************************************************
 **
 ** FUNCTION:
 **  actECInit
 **
 **  This function initializes the ECC workspace.
 **
 ** input:
 ** - domain:      domain parameter
 ** - domain_ext:  domain parameter extensions (Montgomery constants etc.)
 ** - speedup_ext: (optional) precomputations (for ECDSA-Sign, -GenKey)
 ** - algo_flag:   actEC_ALGO_FLAG_*
 ** - wksp:        workspace
 ** - wksp_len:    length of workspace in bytes
 **
 ** output:
 ** - wksp:       initialized workspace
 ** - returns:    actEXCEPTION_NULL         if an input parameter is NULL
 **               actEXCEPTION_DOMAIN       domain decoding error
 **               actEXCEPTION_DOMAIN_EXT   domain_ext decoding error
 **               actEXCEPTION_SPEEDUP_EXT  speedup_ext decoding error
 **               actEXCEPTION_MEMORY       wksp_len to small
 **               actOK                     else
 **
 ** assumes:
 **
 ** uses:
 **
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECInit(
   actPROMU8 domain, actPROMU8 domain_ext, actPROMU8 speedup_ext, int algo_flag,
   VSECPRIM_P2VAR_PARA(actU8) wksp, int wksp_len);


# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
} /* extern "C" */
# endif
# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* ACTECTOOLS_H */

/**********************************************************************************************************************
 *  END OF FILE: actECTools.h
 *********************************************************************************************************************/
