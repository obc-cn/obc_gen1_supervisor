/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actKDFX963_SHA256.c
 *        \brief  KDFX963 key derivation function according to ANSI X9.63 using SHA-256 as underlying hash function.
 *
 *      \details Currently the actClib version is used.
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#define ACTKDFX963_SHA256_SOURCE

#include "actIKDFX963_SHA256.h"
#include "actUtilities.h"

#if (VSECPRIM_ACTKDFX963SHA256_ENABLED == STD_ON)

/****************************************************************************
 ** Global Prototypes
 ***************************************************************************/

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

 /***********************************************************************************************************************
 *  actKDFX963_SHA256_Derive
 **********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(void) actKDFX963_SHA256_Derive(
  VSECPRIM_P2VAR_PARA(actKDFX963SHA256) info,
  VSECPRIM_P2CONST_PARA(actU8) passwd, actLengthType passwd_length,
  VSECPRIM_P2CONST_PARA(actU8) salt, actLengthType salt_length,
  VSECPRIM_P2VAR_PARA(actU8) key, actLengthType key_length,
  VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void))
{
  VSECPRIM_P2VAR_PARA(actSHA256STRUCT) sha = &info->actwsHash;

  actU8   counter[4];
  actLengthType     blocks, remain, b;

  blocks = (key_length + actHASH_SIZE_SHA256 - 1u) / actHASH_SIZE_SHA256;
  remain = key_length % actHASH_SIZE_SHA256;
  /* derive key */
  for (b = 1u; b <= blocks; ++b) {
    /* hash (passwd|counter|salt) */
    actU32toBE(counter, (actU32)b); /* SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER */

    (void)actSHA256Init(sha); /* SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER */
    (void)actSHA256Update(sha, passwd, passwd_length, watchdog); /* SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER */
    (void)actSHA256Update(sha, counter, sizeof(counter), watchdog); /* SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER */
    (void)actSHA256Update(sha, salt, salt_length, watchdog); /* SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER */

    if ((b != blocks)       /* last block? */
      || (0u == remain)) {    /* complete last block required? */
          /* complete (last) block */
      (void)actSHA256Finalize(sha, &key[actHASH_SIZE_SHA256 * (b - 1u)], watchdog); /* SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER */
    }
    else
    {
      /* partial last block */
      (void)actSHA256Finalize(sha, info->hashBuf, watchdog); /* SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER */
      actMemcpy(&key[actHASH_SIZE_SHA256 * (b - 1u)], info->hashBuf, remain);  /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */ /* SBSW_VSECPRIM_CALL_FUNCTION */
    }
  }

} /* PRQA S 6060 */ /* MD_MSR_STPAR */

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTKDFX963SHA256_ENABLED == STD_ON) */
