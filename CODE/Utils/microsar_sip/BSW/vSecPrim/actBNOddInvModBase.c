/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actBNOddInvModBase.c
 *        \brief  A basic (unsigned) integer and module arithmetic used for elliptic curve point arithmetic.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library actCLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#define ACTBNODDINVMODBASE_SOURCE

#include "actBigNum.h"

#if (VSECPRIM_ACTBNODDINVMODBASE_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/

/****************************************************************************
 **
 ** FUNCTION:
 **  void actBNOddInvModBase(actBNDIGIT *inverse, actBNDIGIT value)
 **
 **   Inversion of an odd digit mod base.
 **
 ** input:                                              (length in digits)
 ** - value:      odd digit to be inverted                    1
 ** - inverse     result digit                                1
 **
 ** output:
 ** - inverse     value^-1 mod 2^bits_per_digit               1
 **
 ** assumes:
 **
 ** remark:
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actBNOddInvModBase(VSECPRIM_P2VAR_PARA(actBNDIGIT) inverse, actBNDIGIT value)
{
  actBNDDIGIT d = 1;
  actBNDDIGIT pow = 1;
  actU8 i;

  *inverse = 0;
  for (i = 0; i < actBN_BITS_PER_DIGIT; ++i)
  {
    pow <<= 1;
    if (d % pow != 0)
    {
      d = (actBNDDIGIT) (d + value);
      (*inverse) = (actBNDIGIT) ((*inverse) + (actBNDIGIT) (pow >> 1));
    }
    value <<= 1;
  }
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTBNODDINVMODBASE_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actBNOddInvModBase.c
 *********************************************************************************************************************/
