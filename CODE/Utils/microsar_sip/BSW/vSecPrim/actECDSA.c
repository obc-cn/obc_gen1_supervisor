/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actECDSA.c
 *        \brief  Implementation of Elliptic Curve Digital Signature Algorithm
 *
 *      \details This file is part of the embedded systems library cvActLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

/****************************************************************************
 **
 ** This file contains: Implementation file for actECDSA.h
 **
 ** constants:
 **
 ** types:
 **
 ** macros:
 **
 ** functions:
 **   actECDSAvp
 **
 ***************************************************************************/

#include "actECDSA.h"
#include "actECLengthInfo.h"

#if (VSECPRIM_ACTECDSA_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Local Functions
 ***************************************************************************/
#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* help function to reduce the x-coordinate of a point modulo order n */
VSECPRIM_LOCAL_FUNC(void) actECReduceXModN(
  VSECPRIM_P2CONST_PARA(actBNDIGIT) n, actLengthType n_length,
  VSECPRIM_P2VAR_PARA(actBNDIGIT) x, actLengthType p_length,
  VSECPRIM_P2VAR_PARA(actBNDIGIT) Nval) /* to store n of length p_length */
{
  /* for n_length > p_length is nothing to do */
  if (p_length >= n_length)
  {
    /* copy n to a workspace variable of length >= p_length */
    actBNSetZero(Nval, p_length);
    actBNCopy(Nval, n, n_length);
    /* Reduce per subtraction; this is sufficient, because
    n is close to p in general. (The Hasse bound for
    cofactor 1 is:   |n-p| <= 2*sqroot(p) + 1 .)  */
    while (actBNCompare(x, Nval, p_length) >= actCOMPARE_EQUAL)
    {
       actBNSub(x, Nval, x, p_length);
    }
  }
}


/****************************************************************************
 ** Global Functions
 ***************************************************************************/


/****************************************************************************
 **
 ** FUNCTION:
 **  actECDSAvp
 **
 **  This function implements the ECDSA verification protocol arithmetic.
 **
 ** input:                                              (length in digits)
 ** - e:     the signed hash value                          n_length
 ** - r:     signature r                                    n_length
 ** - s:     signature s                                    n_length
 ** - Q:     (affine) public key (Q = d*G, where           3*p_length
 **          d is the private key)
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_byte_length: length of p in byte
 ** -        p_field->RR:       constant for Montgomery     p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** -        p_field->window_size for modular exponentiation
 ** - Curve->n_field: the field GF(n):
 ** -        n_field->m:        the modulus n               n_length
 ** -        n_field->m_length: equals to n_length
 ** -        n_field->RR:       constant for Montgomery     n_length
 ** -        n_field->m_bar:    constant for Montgomery     1
 ** -        n_field->prime_structure==0
 ** -        n_field->window_size for modular exponentiation
 ** - Curve->b_R:    coefficient b of curve equation        p_bytes
 ** - Curve->Gx_R:   basepoint coordinate x                 p_bytes
 ** - Curve->Gy_R:   basepoint coordinate y                 p_bytes
 ** - Curve->t[0..(i-1)]:  i temporary BigNum variables,   i*(max_length+1)
 **                        where i = actEC_BIGNUM_TMP
 **        [ Notation: max_length = max(p_length, n_length) ]
 ** - wksp:  temporary BigNum workspace for u1 and u2     2*(max_length+1)
 **          and the points G, G+Q, u1*G+u2*Q              3*(3*p_length)
 **
 ** output:
 ** - returns:    actEXCEPTION_PUBKEY     if Q is not on curve
 **               actSIGNATURE_INVALID    if signature (r, s) is invalid
 **               actEXCEPTION_UNKNOWN    internal error (result point
 **                                       not on curve)
 **               actOK                   else
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for the usage of this function is the
 **   actECDSAVerify(..) implementation in actIECDSA.c, which is basically
 **   a call of actECDSAvp(..) with all previous initializations.
 ** - Q is in affine representation (X, Y, 1)
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDSAvp(
   VSECPRIM_P2CONST_PARA(actBNDIGIT) e, VSECPRIM_P2CONST_PARA(actBNDIGIT) r, VSECPRIM_P2CONST_PARA(actBNDIGIT) s,
   VSECPRIM_P2VAR_PARA(actECPOINT) Q, VSECPRIM_P2VAR_PARA(actECCURVE) Curve,
   VSECPRIM_P2VAR_PARA(actBNDIGIT) wksp_ptr)
{
   actECPOINT G, QG, point;
   VSECPRIM_P2VAR_PARA(actBNRING) n_field = &Curve->n_field;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) x1     = Curve->t[0];
   VSECPRIM_P2VAR_PARA(actBNDIGIT) s_inv  = Curve->t[actEC_BIGNUM_TMP-1];
   actLengthType p_length = Curve->p_field.m_length;
   actLengthType p_bytes  = Curve->p_field.m_byte_length;
   actLengthType n_length = Curve->n_field.m_length;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) u1;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) u2;

   /* u1, u1, G, QG, actPoint */       /* adjust workspace pointer */
   u1 = wksp_ptr;                   wksp_ptr += (n_length+1);
   u2 = wksp_ptr;                   wksp_ptr += (n_length+1);
   G.x = wksp_ptr;                  wksp_ptr += p_length;
   G.y = wksp_ptr;                  wksp_ptr += p_length;
   QG.x = wksp_ptr;                 wksp_ptr += p_length;
   QG.y = wksp_ptr;                 wksp_ptr += p_length;
   QG.z = wksp_ptr;                 wksp_ptr += p_length;
   point.x = wksp_ptr;              wksp_ptr += p_length;
   point.y = wksp_ptr;              wksp_ptr += p_length;
   point.z = wksp_ptr;

   /* initialization of basepoint coordinates Gx, Gy, Gz */
   actBNSetOctetStringROM(G.x, p_length, Curve->G_R, p_bytes);
   actBNSetOctetStringROM(G.y, p_length, Curve->G_R+p_bytes, p_bytes);
   G.is_affine   = 1;
   G.is_infinity = 0;

   /* Convert Q to Montgomery Representation */
   actECPToMont(Q, Curve);

   /* check if Q is on curve Q before continue */
   if (!actECPointIsOnCurve(Q, Curve))
   {
      return actEXCEPTION_PUBKEY;
   }

   /* Compute u1 and u2 */

   /* u1 = sR mod n (s in Montgomery Representation) */
   actBNMontMul(s, n_field->RR, u1, n_field, VSECPRIM_FUNC_NULL_PTR);
   /* s_inv = (s^-1)*R mod n */
   actBNFieldInversion(u1, s_inv, n_field, Curve->t);
   /* u1 = (e * s^(-1)) mod n */
   actBNMontMul(e, s_inv, u1, n_field, VSECPRIM_FUNC_NULL_PTR);
   /* u2 = (r * s^(-1)) mod n */
   actBNMontMul(r, s_inv, u2, n_field, VSECPRIM_FUNC_NULL_PTR);

   /* Compute x-Coordinate of u1*Basepoint + u2*pubkey */

   /* Compute point = u1*Basepoint + u2*pubkey */
   actECPSimMult(&G, Q, u1, u2, &QG, &point, Curve);
   /* Compute x-Coordinate x1 of point */
   if (!actECPToAffineFromMont(&point, Curve, 0))
   {
     return actEXCEPTION_UNKNOWN;
   }
   actBNCopy(x1, point.x, p_length);

   /* Reduce the x-Coordinate x1 mod n */
   actECReduceXModN(n_field->m, n_length, x1, p_length, Curve->t[1]);

   /* finally compare x1 and r (should be equal) */
   if (actBNCompare(x1, r, n_length) != actCOMPARE_EQUAL)
   {
      return actSIGNATURE_INVALID;
   }

   return actOK;
}


/****************************************************************************
 **
 ** FUNCTION:
 **  actECDSAsp
 **
 **  This function implements the ECDSA signature protocol arithmetic.
 **
 ** input:                                              (length in digits)
 ** - e:     the hash value to be signed                    n_length
 ** - d:     the private key of the key pair (d, Q)         n_length
 ** - r:     to store signature r                           n_length
 ** - s:     to store signature s                           n_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_byte_length: length of p in byte
 ** -        p_field->RR:       constant for Montgomery     p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** -        p_field->window_size for modular exponentiation
 ** - Curve->n_field: the field GF(n):
 ** -        n_field->m:        the modulus n               n_length
 ** -        n_field->m_length: equals to n_length
 ** -        n_field->RR:       constant for Montgomery     n_length
 ** -        n_field->m_bar:    constant for Montgomery     1
 ** -        n_field->prime_structure==0
 ** -        n_field->window_size for modular exponentiation
 ** - Curve->Gx_R:   basepoint coordinate x                 p_bytes
 ** - Curve->Gy_R:   basepoint coordinate y                 p_bytes
 ** - Curve->t[0..(i-1)]:  i temporary BigNum variables,   i*(max_length+1)
 **                        where i = actEC_BIGNUM_TMP
 **        [ Notation: max_length = max(p_length, n_length) ]
 ** - wksp:  temporary BigNum workspace for k               n_length
 **          and 2 points to calculate k*G                 2*(3*p_length)
 **
 ** output:
 ** - r:     signature r                                    n_length
 ** - s:     signature s                                    n_length
 ** - returns:    actEXCEPTION_UNKNOWN    internal error (result point
 **                                       not on curve)
 **               actOK                   else
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for the usage of this function is the
 **   actECDSAVerify(..) implementation in actIECDSA.c, which is basically
 **   a call of actECDSAvp(..) with all previous initializations.
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDSAsp(
   VSECPRIM_P2CONST_PARA(actBNDIGIT) e, VSECPRIM_P2CONST_PARA(actBNDIGIT) d, VSECPRIM_P2VAR_PARA(actBNDIGIT) r,
   VSECPRIM_P2VAR_PARA(actBNDIGIT) s, VSECPRIM_P2VAR_PARA(actECCURVE) Curve,
   VSECPRIM_P2VAR_PARA(actBNDIGIT) wksp_ptr)
{
   actECPOINT Prec, Q;
   VSECPRIM_P2VAR_PARA(actBNRING) n_field = &Curve->n_field;
   actLengthType n_length = n_field->m_length;
   actLengthType p_length = Curve->p_field.m_length;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) RR     = n_field->RR;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) x1     = Curve->t[0];
   VSECPRIM_P2VAR_PARA(actBNDIGIT) k_inv  = Curve->t[actEC_BIGNUM_TMP-1];
   VSECPRIM_P2VAR_PARA(actBNDIGIT) t0     = Curve->t[0];
   VSECPRIM_P2VAR_PARA(actBNDIGIT) t1     = Curve->t[1];
   VSECPRIM_P2VAR_PARA(actBNDIGIT) k;

   int found = 0, rng_callback_error; /* rng_error is an error code of the actPRNG_GET callback function */

   /* assign workspace */    /* adjust workspace pointer */
   k = wksp_ptr;             wksp_ptr += n_length;
   Q.x = wksp_ptr;           wksp_ptr += p_length;
   Q.y = wksp_ptr;           wksp_ptr += p_length;
   Q.z = wksp_ptr;           wksp_ptr += p_length;
   Prec.x = wksp_ptr;        wksp_ptr += p_length;
   Prec.y = wksp_ptr;

   /*
      from a formal point of view, signature generation is an endless loop, but
      the probability of termination even in the first round is very high
   */
   while (!found)
   {
      /*
         1.) generate a one time key pair (k, Q)
      */

      /* generate k at random */
      rng_callback_error = actBNModRandomize(k, n_field, Curve->t);
      if (rng_callback_error != actOK) { return rng_callback_error; }

      /* compute Q = k*G */
      actECPMultG(k, &Prec, &Q, Curve);
      /* compute x-Coordinate x1 of point */
      if (!actECPToAffineFromMont(&Q, Curve, 0))
      {
        return actEXCEPTION_UNKNOWN;
      }
      actBNCopy(x1, Q.x, p_length);

      /*
         2.)  r = (X-coordinate of Q) mod n; if r==0, goto step 1.
      */

      /* Reduce the x-Coordinate x1 mod n (assumption: n_length >= p_length,
      which is the case for all supported curves) */
      actECReduceXModN(n_field->m, n_length, x1, p_length, Curve->t[1]);

      /* r = x1 */
      actBNSetZero(r, n_length);
      actBNCopy(r, x1, n_length);
      if (actBNIsZero(r, n_length) == FALSE)
      {
         /*
            3.)  s = k^(-1) * (e + d*r) mod n; if s==0 then go to step 1.
         */

         /* k = k*R mod n (k in Montgomery Representation) */
         actBNMontMulCopy(k, RR, t0, n_field, VSECPRIM_FUNC_NULL_PTR);
         /* k_inv = (k^-1)*R mod n */
         actBNFieldInversion(k, k_inv, n_field, Curve->t);
         /* t0 = d*r*(R^-1) mod n */
         actBNMontMul(d, r, t0, n_field, VSECPRIM_FUNC_NULL_PTR);
         /* t1 = d*r mod n */
         actBNMontMul(t0, RR, t1, n_field, VSECPRIM_FUNC_NULL_PTR);
         /* t0 = e + d*r mod n */
         actBNModAdd(e, t1, t0, n_field, VSECPRIM_FUNC_NULL_PTR);
         /* t1 = k^(-1) * (e + d*r) mod n */
         actBNMontMul(k_inv, t0, t1, n_field, VSECPRIM_FUNC_NULL_PTR);

         /* s = t1 */
         actBNCopy(s, t1, n_length);

         found = !actBNIsZero(s, n_length);
      }
   }

   return actOK;
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTECDSA_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actECDSA.c
 *********************************************************************************************************************/
