/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actIECKey.h
 *        \brief  An ECC key generation interface.
 *
 *      \details This file is part of the embedded systems library cvActLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#ifndef ACTIECKEY_H
# define ACTIECKEY_H


# include "actITypes.h"

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Function Prototypes
 ***************************************************************************/
# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
extern "C" {
# endif

/****************************************************************************
 **
 ** FUNCTIONs:
 **   actECGetPrivateKeyLength
 **   actECGetPublicKeyCompLength
 **
 **  These functions return the key length in bytes
 **  for the desired curve.
 **
 ** input:
 ** - domain:      domain parameter
 **
 ** output:
 ** - returns:    length of key in bytes
 **                 (0, if domain not valid)
 **
 ** assumes:
 **
 ** uses:
 **
 ***************************************************************************/
VSECPRIM_FUNC(int) actECGetPrivateKeyLength(actPROMU8 domain);
VSECPRIM_FUNC(int) actECGetPublicKeyCompLength(actPROMU8 domain);

/****************************************************************************
 **
 ** FUNCTION:
 **  actECInitGenerateKeyPair
 **
 **  This function initializes the ECC workspace.
 **
 ** input:
 ** - domain:      domain parameter
 ** - domain_ext:  domain parameter extensions (Montgomery constants etc.)
 ** - speedup_ext: (optional) precomputations (for ECDSA-Sign, -GenKey)
 ** - wksp:        workspace
 ** - wksp_len:    length of workspace in bytes
 **
 ** output:
 ** - wksp:       initialized workspace
 ** - returns:    actEXCEPTION_NULL         if an input parameter is NULL
 **               actEXCEPTION_DOMAIN       domain decoding error
 **               actEXCEPTION_DOMAIN_EXT   domain_ext decoding error
 **               actEXCEPTION_SPEEDUP_EXT  speedup_ext decoding error
 **               actEXCEPTION_MEMORY       wksp_len to small
 **               actOK                     else
 **
 ** assumes:
 **
 ** uses:
 **
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECInitGenerateKeyPair(
   actPROMU8 domain, actPROMU8 domain_ext, actPROMU8 speedup_ext,
   VSECPRIM_P2VAR_PARA(actU8) wksp, int wksp_len);

/****************************************************************************
 **
 ** FUNCTION:
 **   actECGenerateKeyPair
 **
 **  This function generate an ECC key pair (d, Q), where Q = d * G.
 **
 ** input:
 ** - generate_privatekey:   0, privatekey is passed by the user
 **                          1, privatekey will be generated at random
 ** - privatekey:  buffer to store the private key d
 ** - publickey_x: buffer to store x-coordinate of the public key Q
 ** - publickey_x: buffer to store y-coordinate of the public key Q
 ** - wksp:        workspace
 **
 ** output:
 ** - privatekey:  private key d
 ** - publickey_x: x-coordinate of the public key Q
 ** - publickey_x: y-coordinate of the public key Q
 ** - returns:    actEXCEPTION_NULL       if an input parameter is NULL
 **               actEXCEPTION_PRIVKEY    if d is passed and (d==0||d>=n)
 **               actEXCEPTION_UNKNOWN    internal error (result point
 **                                       not on curve)
 **               actOK                   else
 **
 ** assumes:
 ** - wksp is initialized with actECInitGenerateKeyPair
 **
 ** uses:
 **
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECGenerateKeyPair(
   int generate_privatekey, VSECPRIM_P2VAR_PARA(actU8) privatekey,
   VSECPRIM_P2VAR_PARA(actU8) publickey_x, VSECPRIM_P2VAR_PARA(actU8) publickey_y,
   VSECPRIM_P2VAR_PARA(actU8) wksp);

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
} /* extern "C" */
# endif
# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* ACTIECKEY_H */

/**********************************************************************************************************************
 *  END OF FILE: actIECKey.h
 *********************************************************************************************************************/
