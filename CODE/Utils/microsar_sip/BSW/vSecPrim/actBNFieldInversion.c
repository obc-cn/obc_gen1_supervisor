/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actBNFieldInversion.c
 *        \brief  A basic (unsigned) integer and module arithmetic used for elliptic curve point arithmetic.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library actCLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#define ACTBNFIELDINVERSION_SOURCE

#include "actBigNum.h"
#if (VSECPRIM_ACTBNEUCLID_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
# include "actBigNumGCD.h"
#endif
#include "actUtilities.h"

#if (VSECPRIM_ACTBNFIELDINVERSION_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTION:
 **  void actBNFieldInversion(const actBNDIGIT* gR,
 **                           actBNDIGIT* g_invR,
 **                           const actBNRING* Field,
 **                           actBNDIGIT* t[]))
 **
 **  Field inversion (g_invR = gR^-1 mod p, p=Ring->m must be prime).
 **  The in- and output is in Montgomery Representation.
 **
 ** input:                                              (length in digits)
 ** - gR:     field element to be inverted (gR != 0)        p_length
 ** - g_invR: field element to store the result             p_length+1
 ** - Field:  the underlying field GF(p)  (see assumptions)
 ** - Field->m:        the prime p                          p_length
 ** - Field->m_length: equals to p_length
 ** - Field->RR:       constant for Montgomery              p_length
 ** - Field->m_bar:    constant for Montgomery              1
 ** - Field->prime_structure==curveid if p is the curve prime
 ** - Field->prime_structure==0       else
 ** if 'actEUCLID' is defined:
 ** - t[0..4]:  5 temporary BigNum variables                5*(p_length+1)
 ** else:
 ** - Field->window_size (=:k)  for modular exponentiation
 ** - t[0..i]:  i+1 temporary BigNum variables,         (i+1)*(p_length+1)
 **                 where i = 2^(k-1)
 **
 ** output:
 ** - g_invR: the result gR^-1 mod m                        p_length
 **
 ** assumes:
 ** - The actBNRING structure parameter 'Field' holds all necessary
 **   information. It has to be initialized as far as listed above.
 **   Please have a look at the actBNRING definition; an example for
 **   a complete initialization is the actECDSAVerify(..) implementation
 **   in actIECDSA.c.
 ** - Field->m must be prime
 ** - gR != 0
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actBNFieldInversion(
   VSECPRIM_P2CONST_PARA(actBNDIGIT) gR, VSECPRIM_P2VAR_PARA(actBNDIGIT) g_invR,
   VSECPRIM_P2CONST_PARA(actBNRING) Field, VSECPRIM_P2VAR_PARA(actBNDIGIT) t[])
{
#if (VSECPRIM_ACTBNEUCLID_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
  /*
     To invert g, we use the extended Euclidian algorithm.
     'actBNGCDix' calculates x, such that
     gcd(g, p) = 1 = x*g + y*p   (p prime)
     <=>      1 = x*g mod p
     <=>   g^-1 = x
   */

  VSECPRIM_P2VAR_PARA(actBNDIGIT) tmp = t[4];

  /* tmp = g */
  actBNSetZero(t[3], Field->m_length);
  (t[3])[0] = (actBNDIGIT) 1;   /* t[3] = 1 */
  actBNMontMul(gR, t[3], tmp, Field, VSECPRIM_FUNC_NULL_PTR);
  /* g_invR = g^-1 */
  actBNGCDix(tmp, g_invR, Field, t);
  /* g_invR = g^-1 * R */
  actBNMontMulCopy(g_invR, Field->RR, tmp, Field, VSECPRIM_FUNC_NULL_PTR);

#else

  /*
     To invert g, we use Fermat:
     g^(p-1) = 1 (p prime)
     <=>    g^-1 = g^(p-2)
   */

  actLengthType p_length = Field->m_length;
  VSECPRIM_P2VAR_PARA(actBNDIGIT) e;
# if (actBN_MOD_EXP_WINDOW_SIZE == 0)
  e = t[1 << (Field->window_size - 1)];
# else
  e = t[1 << (actBN_MOD_EXP_WINDOW_SIZE - 1)];
# endif

  /* e = p - 2 */
  actBNSetZero(e, p_length);
  e[0] = 2;
  actBNSub(Field->m, e, e, p_length);

  /* g_invR = gR^e = gR^-1 */
  actBNModExp(gR, e, g_invR, Field, t, (VSECPRIM_P2FUNC(VSECPRIM_NONE, void, VSECPRIM_NONE) (void))NULL_PTR);
#endif
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTBNFIELDINVERSION_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actBNFieldInversion.c
 *********************************************************************************************************************/
