/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actECDSA.h
 *        \brief  An ECDSA verification arithmetic interface.
 *
 *      \details This file is part of the embedded systems library cvActLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

/****************************************************************************
 **
 ** This file contains: An ECDSA verification arithmetic interface.
 **
 ** constants:
 **
 ** types:
 **
 ** macros:
 **
 ** functions:
 **   actECDSAvp
 **
 ***************************************************************************/


#ifndef ACTECDSA_H
# define ACTECDSA_H


# include "actIECDSA.h"
# include "actECPoint.h"


/****************************************************************************
 ** Function Prototypes
 ***************************************************************************/
# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
extern "C" {
# endif

/****************************************************************************
 **
 ** FUNCTION:
 **  actECDSAvp
 **
 **  This function implements the ECDSA verification protocol arithmetic.
 **
 ** input:                                              (length in digits)
 ** - e:     the signed hash value                          n_length
 ** - r:     signature r                                    n_length
 ** - s:     signature s                                    n_length
 ** - Q:     (affine) public key (Q = d*G, where           3*p_length
 **          d is the private key)
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_byte_length: length of p in byte
 ** -        p_field->RR:       constant for Montgomery     p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** -        p_field->window_size for modular exponentiation
 ** - Curve->n_field: the field GF(n):
 ** -        n_field->m:        the modulus n               n_length
 ** -        n_field->m_length: equals to n_length
 ** -        n_field->RR:       constant for Montgomery     n_length
 ** -        n_field->m_bar:    constant for Montgomery     1
 ** -        n_field->prime_structure==0
 ** -        n_field->window_size for modular exponentiation
 ** - Curve->b_R:    coefficient b of curve equation        p_bytes
 ** - Curve->Gx_R:   basepoint coordinate x                 p_bytes
 ** - Curve->Gy_R:   basepoint coordinate y                 p_bytes
 ** - Curve->t[0..(i-1)]:  i temporary BigNum variables,   i*(max_length+1)
 **                        where i = actEC_BIGNUM_TMP
 **        [ Notation: max_length = max(p_length, n_length) ]
 ** - wksp:  temporary BigNum workspace for u1 and u2     2*(max_length+1)
 **          and the points G, G+Q, u1*G+u2*Q              3*(3*p_length)
 **
 ** output:
 ** - returns:    actEXCEPTION_PUBKEY     if Q is not on curve
 **               actSIGNATURE_INVALID    if signature (r, s) is invalid
 **               actEXCEPTION_UNKNOWN    internal error (result point
 **                                       not on curve)
 **               actOK                   else
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for the usage of this function is the
 **   actECDSAVerify(..) implementation in actIECDSA.c, which is basically
 **   a call of actECDSAvp(..) with all previous initializations.
 ** - Q is in affine representation (X, Y, 1)
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDSAvp(VSECPRIM_P2CONST_PARA(actBNDIGIT) e,
   VSECPRIM_P2CONST_PARA(actBNDIGIT) r, VSECPRIM_P2CONST_PARA(actBNDIGIT) s,
   VSECPRIM_P2VAR_PARA(actECPOINT) Q, VSECPRIM_P2VAR_PARA(actECCURVE) Curve,
   VSECPRIM_P2VAR_PARA(actBNDIGIT) wksp_ptr);

/****************************************************************************
 **
 ** FUNCTION:
 **  actECDSAsp
 **
 **  This function implements the ECDSA signature protocol arithmetic.
 **
 ** input:                                              (length in digits)
 ** - e:     the hash value to be signed                    n_length
 ** - d:     the private key of the key pair (d, Q)         n_length
 ** - r:     to store signature r                           n_length
 ** - s:     to store signature s                           n_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_byte_length: length of p in byte
 ** -        p_field->RR:       constant for Montgomery     p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** -        p_field->window_size for modular exponentiation
 ** - Curve->n_field: the field GF(n):
 ** -        n_field->m:        the modulus n               n_length
 ** -        n_field->m_length: equals to n_length
 ** -        n_field->RR:       constant for Montgomery     n_length
 ** -        n_field->m_bar:    constant for Montgomery     1
 ** -        n_field->prime_structure==0
 ** -        n_field->window_size for modular exponentiation
 ** - Curve->Gx_R:   basepoint coordinate x                 p_bytes
 ** - Curve->Gy_R:   basepoint coordinate y                 p_bytes
 ** - Curve->t[0..(i-1)]:  i temporary BigNum variables,   i*(max_length+1)
 **                        where i = actEC_BIGNUM_TMP
 **        [ Notation: max_length = max(p_length, n_length) ]
 ** - wksp:  temporary BigNum workspace for k               n_length
 **          and 2 points to calculate k*G                 2*(3*p_length)
 **
 ** output:
 ** - r:     signature r                                    n_length
 ** - s:     signature s                                    n_length
 ** - returns:    actEXCEPTION_UNKNOWN    internal error (result point
 **                                       not on curve)
 **               actOK                   else
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for the usage of this function is the
 **   actECDSAVerify(..) implementation in actIECDSA.c, which is basically
 **   a call of actECDSAvp(..) with all previous initializations.
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDSAsp(VSECPRIM_P2CONST_PARA(actBNDIGIT) e,
   VSECPRIM_P2CONST_PARA(actBNDIGIT) d, VSECPRIM_P2VAR_PARA(actBNDIGIT) r,
   VSECPRIM_P2VAR_PARA(actBNDIGIT) s, VSECPRIM_P2VAR_PARA(actECCURVE) Curve,
   VSECPRIM_P2VAR_PARA(actBNDIGIT) wksp_ptr);

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
} /* extern "C" */
# endif

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* ACTECDSA_H */

/**********************************************************************************************************************
 *  END OF FILE: actECDSA.h
 *********************************************************************************************************************/
