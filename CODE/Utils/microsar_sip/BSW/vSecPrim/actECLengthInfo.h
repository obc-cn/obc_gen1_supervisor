/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actECLengthInfo.h
 *        \brief  Interface for Length information for internal use.
 *
 *      \details This file is part of the embedded systems library cvActLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

/****************************************************************************
 **
 ** This file contains: Length information for internal use.
 **
 ** constants:
 **
 ** types:
 **
 ** macros:
 **   actECDomainWksp
 **   actECPointWksp
 **   actECPointDecompWksp
 **   actECTmpWksp
 **   actECKgWksp
 **   actECDHWksp
 **   actECDSASpWksp
 **   actECDSAVpWksp
 **
 ** functions:
 **
 ***************************************************************************/

#ifndef ACTECLENGTHINFO_H
# define ACTECLENGTHINFO_H

# include "actITypes.h"

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
extern "C"
{
# endif

/* ECDecompPoint */
VSECPRIM_FUNC(int) actECPointDecompWksp(int p_length, int n_length);

/* EC basic workspace used by all EC primitives except ECDecompPoint */
VSECPRIM_FUNC(int) actECBasicWksp(int p_length, int n_length);

/* ECKeyGen */
VSECPRIM_FUNC(int) actECKgWksp(int p_length, int n_length);

/* ECDHGetSecret */

VSECPRIM_FUNC(int) actECDHpWksp(int p_length, int n_length);
/* ECDSAVerify */

VSECPRIM_FUNC(int) actECDHWksp(int p_length, int n_length);

/* ECDSASign */
VSECPRIM_FUNC(int) actECDSASpWksp(int p_length, int n_length);
/* ECDSAVerify */
VSECPRIM_FUNC( int ) actECDSAVpWksp( int p_length, int n_length );

/* EC-B/D key agreement */
VSECPRIM_FUNC( int ) actECBDWksp( int p_length, int n_length );

/****************************************************************************
 **
 ** FUNCTIONs:
 **  actECGetPrimeAndOrderBytes
 **
 **  These functions extracts the length in bytes of the prime p and
 **  the basepoint order n for the given domain parameters.
 **
 ** input:
 ** - domain:     pointer to curve's domain parameter
 ** - p_bytes:    where length of p is returned
 ** - n_bytes:    where length of n is returned
 **
 ** output:
 ** - p_bytes:    length of p
 ** - n_bytes:    length of n
 ** - returns:    actEXCEPTION_DOMAIN    domain decoding error
 **               actOK                  else
 **
 ** assumes:
 **
 ** uses:
 **
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECGetPrimeAndOrderBytes(actPROMU8 domain, VSECPRIM_P2VAR_PARA(int) p_bytes, VSECPRIM_P2VAR_PARA(int) n_bytes);

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
} /* extern "C" */
# endif
# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* ACTECLENGTHINFO_H */

/**********************************************************************************************************************
 *  END OF FILE: actECLengthInfo.h
 *********************************************************************************************************************/
