/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actBNModRandomize.c
 *        \brief  A basic (unsigned) integer and module arithmetic used for elliptic curve point arithmetic.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library actCLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#define ACTBNMODRANDOMIZE_SOURCE

#include "actBigNum.h"
#include "actUtilities.h"
#include "actExternRandom.h"

#if (VSECPRIM_ACTBNMODRANDOMIZE_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTION:
 **  int actBNModRandomize(actBNDIGIT* x,
 **                        const actBNRING* Ring,
 **                        actBNDIGIT* t[])
 **
 **  Modular Randomization (x will randomized with 0 < x < m, m=Ring->m).
 **
 ** input:                                              (length in digits)
 ** - x:      ring element to store the result              m_length
 ** - Ring:   the underlying ring (see assumptions)
 ** - Ring->m:        the odd(!) modulus m                  m_length
 ** - Ring->m_length: equals to m_length
 ** - Ring->RR:       constant for Montgomery               m_length
 ** - Ring->m_bar:    constant for Montgomery               1
 ** - Ring->prime_structure==curveid if m is the curve prime
 ** - Ring->prime_structure==0       else
 ** - t[0, 1]:  2 temporary BigNum variables,                m_length+1
 **
 ** output:
 ** - x:     random x with 0 < x < m                        m_length
 ** - returns: 0 or errorcode of the get random callback function
 **
 ** assumes:
 ** - The actBNRING structure parameter 'Ring' holds all necessary
 **   information. It has to be initialized as far as listed above.
 **   Please have a look at the actBNRING definition; an example for
 **   a complete initialization is the actECDSAVerify(..) implementation
 **   in actIECDSA.c.
 ** - Ring->m must be odd
 **
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actBNModRandomize(VSECPRIM_P2VAR_PARA(actBNDIGIT) x, VSECPRIM_P2CONST_PARA(actBNRING) Ring, VSECPRIM_P2VAR_PARA(actBNDIGIT) t[])
{
  actLengthType m_length = Ring->m_length;
  actRETURNCODE returncode = actOK;

  do
  {
    /* 1.  randomize x      (0 <= x < 2^(n_digits*bits_per_digit)) */
    returncode = actGetExternRandom(((VSECPRIM_P2VAR_PARA(actU8)) x), m_length * actBN_BYTES_PER_DIGIT);
    if (returncode != actOK)
    {
      return returncode;
    }
    /* 2.  randomize t[0]   (0 <= t[0] < 2^(n_digits*bits_per_digit)) */
    returncode = actGetExternRandom(((VSECPRIM_P2VAR_PARA(actU8)) t[0]), m_length * actBN_BYTES_PER_DIGIT);
    if (returncode != actOK)
    {
      return returncode;
    }
    /* 3.  x = Mont(x, t[0]) = x*t[0]*R mod 2^(n_digits*bits_per_digit) */
    actBNMontMulCopy(x, t[0], t[1], Ring, VSECPRIM_FUNC_NULL_PTR);
    /* 4.  x = Mont(x, R^2) = x*t[0] mod n */
    actBNMontMulCopy(x, Ring->RR, t[0], Ring, VSECPRIM_FUNC_NULL_PTR);
  }
  while(actBNIsZero(x, m_length) == TRUE);  /* while x==0 */

  return returncode;
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTBNMODRANDOMIZE_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actBNModRandomize.c
 *********************************************************************************************************************/
