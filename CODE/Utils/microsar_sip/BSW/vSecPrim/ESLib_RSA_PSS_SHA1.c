/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file ESLib_RSA_PSS_SHA1.c
 *        \brief Crypto library - PKCS #1 RSA PSS signature verification / generation (SHA-1)
 *
 *      \details Helper functions for RSA signature generation / generation using RSA PSS encoding scheme
 *               according to PKCS #1 v2.2
 *               Currently the actClib version is used.
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/
#define ESLIB_RSA_PSS_SHA1_SOURCE

/***********************************************************************************************************************
 *  INCLUDES
 **********************************************************************************************************************/

#include "ESLib.h"
#include "ESLib_types.h"

#include "actUtilities.h"
#include "actISHA.h"

#if (VSECPRIM_RSA_PSS_SHA1_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/***********************************************************************************************************************
 *  DEFINES
 **********************************************************************************************************************/

# define ESL_RSA_PSS_INIT_HASH_FCT(workspace)                         (void)actSHAInit(&((workspace)->wsSHA1))
# define ESL_RSA_PSS_UPDATE_HASH_FCT(workspace, inputSize, input)     actSHAUpdate(&((workspace)->wsSHA1), (VSECPRIM_P2CONST_PARA(actU8))(input), (actLengthType)(inputSize), (workspace)->header.watchdog)
# define ESL_RSA_PSS_FINALIZE_HASH_FCT(workspace, messageDigest)      (void)actSHAFinalize(&((workspace)->wsSHA1), (VSECPRIM_P2VAR_PARA(actU8))(messageDigest),  (workspace)->header.watchdog)

# define ESL_RSA_PSS_SIZEOF_DIGEST                                    ESL_SIZEOF_SHA1_DIGEST

/***********************************************************************************************************************
 *  TYPEDEFS
 **********************************************************************************************************************/

typedef eslt_WorkSpaceSHA1 eslt_WorkSpaceHash;

/***********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 **********************************************************************************************************************/
# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/***********************************************************************************************************************
 *  esl_calcSaltedHashRSASHA1_PSS
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_calcSaltedHashRSASHA1_PSS(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash) wsHash,
                                                            eslt_Length saltSize, VSECPRIM_P2CONST_PARA(eslt_Byte) salt,
                                                            VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest, VSECPRIM_P2VAR_PARA(eslt_Byte) saltedHash)
{
  eslt_ErrorCode returnValue = ESL_ERC_NO_ERROR;
  actRETURNCODE actReturnValue;
  eslt_Byte padding[8u];

  /* Initialize hash calculation */
  ESL_RSA_PSS_INIT_HASH_FCT(wsHash);

  /* 9.1.2, Steps 5-6: Hash leftmost zero octets */
  actMemset(padding, 0x00u, sizeof(padding)); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
  actReturnValue = ESL_RSA_PSS_UPDATE_HASH_FCT(wsHash, sizeof(padding), padding);


  /* 9.1.2, Steps 5-6: Hash given message digest */
  if (actOK == actReturnValue)
  {
    actReturnValue = ESL_RSA_PSS_UPDATE_HASH_FCT(wsHash, ESL_RSA_PSS_SIZEOF_DIGEST, messageDigest);
  }

  if (actOK == actReturnValue)
  {
    /* 9.1.2, Steps 5-6: Hash salt when given */
    if (saltSize > 0u)
    {
      actReturnValue = ESL_RSA_PSS_UPDATE_HASH_FCT(wsHash, saltSize, salt);
    }
  }

  if (actOK == actReturnValue)
  {
    /* 9.1.2, Steps 6: Finalize hash calculation and return in provided buffer */
    ESL_RSA_PSS_FINALIZE_HASH_FCT(wsHash, saltedHash);
  }
  else
  {
    returnValue = ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW;
  }

  return returnValue;
}

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_RSA_PSS_SHA1_ENABLED == STD_ON) */
/********************************************************************************************************************** 
 *  END OF FILE: ESLib_RSA_PSS_SHA1.c 
 *********************************************************************************************************************/
