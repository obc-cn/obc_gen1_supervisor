 /**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
 /**        \file ESLib_version.h
 *        \brief  Deprecated header file that contains last API version of ESLib
 *
 *      \details  This file is part of the embedded systems library cvActLib/ES
 *                This file is deprecated and only exist for compatibility reasons. Do not use anymore.
 *                This is an additional header used to document changes implemented by Vector Informatik GmbH.
 *********************************************************************************************************************/

 /**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/


#ifndef ESLIB_VERSION_H
#define ESLIB_VERSION_H

/**********************************************************************************************************************
 *  VERSION
 *********************************************************************************************************************/

#define SYSSERVICE_CRYPTOCV_ESLIB_VERSION             (0x0302u)
#define SYSSERVICE_CRYPTOCV_ESLIB_RELEASE_VERSION     (0x01u)

#endif /* ESLIB_VERSION_H */


/**********************************************************************************************************************
 *  END OF FILE: ESLib_version.h
 *********************************************************************************************************************/
