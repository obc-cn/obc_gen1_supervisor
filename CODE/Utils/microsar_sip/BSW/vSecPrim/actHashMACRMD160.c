/****************************************************************************
 ** Main author: Ubr                     Creation date: 03/18/05
 ** Author: mns                          JustDate: 03/18/05
 ** Workfile: actHashMACRMD160.c         Revision: 1435
 ** NoKeywords:
 **
 **
 ** \copyright(cv cryptovision GmbH, 1999 - 2005                         )
 **
 ** \version(1.0 (beta)                                                 )
 ***************************************************************************/

/****************************************************************************
 **
 **     Part of the actCLibrary
 **
 **     Layer: User Module - Interface
 **
 ***************************************************************************/

/****************************************************************************
 **
 ** This file contains: The interface for RIPEMD-160 based Hash MAC algorithm.
 **
 ** constants:
 **
 ** types:
 **
 ** macros:
 **
 ** functions:
 **   actHashMACInit
 **   actHashMACUpdate
 **   actHashMACFinalize
 **
 ***************************************************************************/

#include "actIHashMACRMD160.h"
#include "actUtilities.h"

#if (VSECPRIM_ACTHASHMACRMD160_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/
#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTION:
 ** actRETURNCODE actHashMACRMD160Init(actHASHMACRMD160STRUCT* info),
 **                                    const actU8* key,
 **                                    int key_length,
 **                                    void (*watchdog) (void))
 **
 **  This function initializes the HashMAC algorithm.
 **
 ** input:
 ** - info:       pointer to context structure
 ** - key:        MAC key
 ** - key_length: length of key in bytes
 ** - watchdog:   pointer to watchdog reset function
 **
 ** output:
 ** - info:       initialized context structure
 ** - returns:    actEXCEPTION_LENGTH    if key_len < 1
 **               actOK                  else
 **
 ** assumes:
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actHashMACRMD160Init(
   VSECPRIM_P2VAR_PARA(actHASHMACRMD160STRUCT) info,
   VSECPRIM_P2CONST_PARA(actU8) key, int key_length,
   VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void))
{
   VSECPRIM_P2VAR_PARA(actRMD160STRUCT) rmd160 = &info->rmd160;
   VSECPRIM_P2VAR_PARA(actU8) tmp_block = info->rmd160.buffer;
   int i;

   /* store key */
   if (key_length < 1)
   {
      return actEXCEPTION_LENGTH;
   }
   else if (key_length <= actHASH_BLOCK_SIZE_RMD160)
   {
      /* copy key to key_buf */
      actMemcpy(info->key_buf, key, (unsigned int)key_length); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
      info->key_length = key_length;
   }
   else
   {
      /* use RMD-160(key) */
      actRMD160Init(rmd160);
      actRMD160Update(rmd160, key, key_length, watchdog);
      actRMD160Finalize(rmd160, info->key_buf, watchdog);
      info->key_length = actHASH_SIZE_RMD160;
   }

   /* ipad */
   actMemcpy(tmp_block, info->key_buf, (unsigned int)(info->key_length)); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
   actMemset(tmp_block + info->key_length, 0, (unsigned int)(actHASH_BLOCK_SIZE_RMD160 - info->key_length)); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
   for (i=0; i < actHASH_BLOCK_SIZE_RMD160; ++i)
   {
      tmp_block[i] ^= (actU8)(0x36);
   }

   actRMD160Init(rmd160);
   actRMD160Update(rmd160, tmp_block, actHASH_BLOCK_SIZE_RMD160, watchdog);

   return actOK;
}


/****************************************************************************
 **
 ** FUNCTION:
 ** actRETURNCODE actHashMACRMD160Update(actHASHMACRMD160STRUCT* info,
 **                                      const actU8* dataIN,
 **                                      int length,
 **                                      void (*watchdog) (void))
 **
 **  This function hashes the given data and can be called arbitrary
 **  often between an initialize and finalize of the HashMAC algorithm.
 **  Uses any data already in the actRMD160STRUCT structure and leaves
 **  any partial data block there.
 **
 ** input:
 ** - info:       pointer to context structure
 ** - dataIN:     pointer to data to be hashed
 ** - length:     length of data in bytes
 ** - watchdog:   pointer to watchdog reset function
 **
 ** output:
 ** - info:       actualized context structure
 ** - returns:    actEXCEPTION_LENGTH   total input more than 2^64 - 1 bit
 **               actOK                 else
 **
 ** assumes:
 ** - actHashMACRMD160Init() is called once before calling this function
 ** - dataIN != NULL is a valid pointer
 ** - length >= 0
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actHashMACRMD160Update(
   VSECPRIM_P2VAR_PARA(actHASHMACRMD160STRUCT) info,
   VSECPRIM_P2CONST_PARA(actU8) dataIN, int length,
   VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void))
{
   return actRMD160Update(&info->rmd160, dataIN, length, watchdog);
}


/****************************************************************************
 **
 ** FUNCTION:
 ** actRETURNCODE actHashMACRMD160Finalize(actHASHMACSTRUCT* info,
 **                     actU8 hash[actHASH_SIZE_RMD160],
 **                     void (*watchdog) (void))
 **
 **  This function finalizes the HashMAC algorithm and
 **  delivers the hash value.
 **
 ** input:
 ** - info:       pointer to context structure
 ** - hash:       byte array to contain the hash value
 ** - watchdog:   pointer to watchdog reset function
 **
 ** output:
 ** - info:       finalized context structure
 ** - hash:       the final hash value,
 **                  (big endian of length actHASH_SIZE_RMD160)
 ** - returns:    actOK allways
 **
 ** assumes:
 ** - actHashMACRMD160Init() is called once before calling this function
 **
 ** uses:
 ** - actHASH_SIZE_RMD160
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actHashMACRMD160Finalize(
   VSECPRIM_P2VAR_PARA(actHASHMACRMD160STRUCT) info, VSECPRIM_P2VAR_PARA(actU8) hash,
   VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void))
{
   VSECPRIM_P2VAR_PARA(actRMD160STRUCT) rmd160 = &info->rmd160;
   VSECPRIM_P2VAR_PARA(actU8) tmp_block = info->rmd160.buffer;
   int i;

   actRMD160Finalize(rmd160, hash, watchdog);

   /* opad */
   actMemcpy(tmp_block, info->key_buf, (unsigned int)(info->key_length)); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
   actMemset(tmp_block + info->key_length, 0, (unsigned int)(actHASH_BLOCK_SIZE_RMD160 - info->key_length)); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
   for (i=0; i < actHASH_BLOCK_SIZE_RMD160; ++i)
   {
      tmp_block[i] ^= (actU8)(0x5c);
   }

   actRMD160Init(rmd160);
   actRMD160Update(rmd160, tmp_block, actHASH_BLOCK_SIZE_RMD160, watchdog);
   actRMD160Update(rmd160, hash, actHASH_SIZE_RMD160, watchdog);
   actRMD160Finalize(rmd160, hash, watchdog);

   return actOK;
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTHASHMACRMD160_ENABLED == STD_ON) */
