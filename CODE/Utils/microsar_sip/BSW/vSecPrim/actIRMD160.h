/****************************************************************************
 ** Main author: Ubr                     Creation date: 10/17/03
 ** Author: mns                          JustDate: 03/18/05
 ** Workfile: actIRMD160.h               Revision: 1435
 ** NoKeywords:
 **
 **
 ** \copyright(cv cryptovision GmbH, 1999 - 2005                         )
 **
 ** \version(1.0 (beta)                                                 )
 ***************************************************************************/

/****************************************************************************
 **
 **     Part of the actCLibrary
 **
 **     Layer: User Module - Interface
 **
 ***************************************************************************/

/****************************************************************************
 **
 ** This file contains: The interface for RMD160 hash algorithm.
 **
 ** constants:
 **  actHASH_SIZE_RMD160
 **
 ** types:
 **
 ** macros:
 **
 ** functions:
 **   actRMD160Init
 **   actRMD160Update
 **   actRMD160Finalize
 **
 ***************************************************************************/


#ifndef ACTIRMD160_H
# define ACTIRMD160_H


# include "actITypes.h"

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Types and constants
 ***************************************************************************/


# define actHASH_SIZE_RMD160                                          (20)
# define actHASH_BLOCK_SIZE_RMD160                                    (64)

/* Workspace structure for RMD160 */
typedef struct
{
   actU32 H[5];                  /* message digest state buffer */
   actU32 low_count, hi_count;   /* 64 bit input count */
   int buffer_used;              /* number of bytes saved in buffer */
   actU8 buffer[actHASH_BLOCK_SIZE_RMD160];     /* remaining data buffer */
} actRMD160STRUCT;


/****************************************************************************
 ** Function Prototypes
 ***************************************************************************/

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
extern "C" {
# endif

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTION:
 ** actRETURNCODE actRMD160Init(actRMD160STRUCT* info)
 **
 **  This function initializes the RMD160 algorithm.
 **
 ** input:
 ** - info:       pointer to hash context structure
 **
 ** output:
 ** - info:       initialized hash context structure
 ** - returns:    actOK allways
 **
 ** assumes:
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actRMD160Init(VSECPRIM_P2VAR_PARA(actRMD160STRUCT) info);

/****************************************************************************
 **
 ** FUNCTION:
 ** actRETURNCODE actRMD160Update(actRMD160STRUCT* info,
 **                               const actU8* dataIN,
 **                               int length,
 **                               void (*watchdog) (void));
 **
 **  This function hashes the given data and can be called arbitrary
 **  often between an initialize and finalize of the RMD160 algorithm.
 **  Uses any data already in the actRMD160STRUCT structure and leaves
 **  any partial data block there.
 **
 ** input:
 ** - info:       pointer to hash context structure
 ** - dataIN:     pointer to data to be hashed
 ** - length:     length of data in bytes
 ** - watchdog:   pointer to watchdog reset function
 **
 ** output:
 ** - info:       actualized hash context structure
 ** - returns:    actEXCEPTION_LENGTH   total input more than 2^64 - 1 bit
 **               actOK                 else
 **
 ** assumes:
 ** - actRMD160Init() is called once before calling this function
 ** - dataIN != NULL is a valid pointer
 ** - length >= 0
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actRMD160Update(
   VSECPRIM_P2VAR_PARA(actRMD160STRUCT) info,
   VSECPRIM_P2CONST_PARA(actU8) dataIN, int length,
   VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void));

/****************************************************************************
 **
 ** FUNCTION:
 ** actRETURNCODE actRMD160Finalize(actRMD160STRUCT* info,
 **                                 actU8 hash[actHASH_SIZE_RMD160],
 **                                 void (*watchdog) (void));
 **
 **  This function finalizes the RMD160 algorithm and delivers the hash value.
 **
 ** input:
 ** - info:       pointer to hash context structure
 ** - hash:       byte array to contain the hash value
 ** - watchdog:   pointer to watchdog reset function
 **
 ** output:
 ** - info:       finalized hash context structure
 ** - hash:       the final hash value,
 **                  (big endian of length actHASH_SIZE_RMD160)
 ** - returns:    actOK allways
 **
 ** assumes:
 ** - actRMD160Init() is called once before calling this function
 **
 ** uses:
 ** - actHASH_SIZE_RMD160
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actRMD160Finalize(
   VSECPRIM_P2VAR_PARA(actRMD160STRUCT) info, VSECPRIM_P2VAR_PARA(actU8) hash,
   VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void));

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
} /* extern "C" */
# endif

#endif /* ACTIRMD160_H */

