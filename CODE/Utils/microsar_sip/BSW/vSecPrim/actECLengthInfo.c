/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actECLengthInfo.c
 *        \brief  The EC length info functions.
 *
 *      \details This file is part of the embedded systems library cvActLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

/****************************************************************************
 **
 ** This file contains: The EC length info functions.
 **
 ** constants:
 **
 ** globals:
 **
 ** functions:
 **   actECInit
 **
 ***************************************************************************/

#include "actECLengthInfo.h"
#include "actITypes.h"
#include "actUtilities.h"
#include "actBigNum.h"
#include "actECPoint.h"
#include "actECTools.h"
#include "actECLengthMacros.h"

#if (VSECPRIM_ACTECLENGTHINFO_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Macros
 ***************************************************************************/

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/

/* ----------------------------------------------------------------------
 |  the long number scratch required (in actBNDIGIT units)
 |  is calculated from the context input vars:
 |    p_length, n_length
 --------------------------------------------------------------------- */


/* ECDecompPoint: ECDomain + temporary bignums */
VSECPRIM_FUNC(int) actECPointDecompWksp(int p_length, int n_length)
{
   return ( actEC_DOMAIN_WKSP(p_length, n_length) + (actEC_MOD_EXP_TMP+3)*(p_length+1) );
}

/* ECBasic: ECDomain + PointAddDouble */
VSECPRIM_FUNC(int) actECBasicWksp(int p_length, int n_length)
{
   return ( actEC_BASIC_WKSP(p_length, n_length) );
}

/* ECKeyGen: ECBasic + d + (d*Q) + PointAffine */
VSECPRIM_FUNC(int) actECKgWksp(int p_length, int n_length)
{
   return ( actEC_KG_WKSP(p_length, n_length) );
}

/* ECDHGetSecret: ECBasic + Q + (Q*d) + d */
VSECPRIM_FUNC(int) actECDHpWksp(int p_length, int n_length)
{
   return ( actEC_DHp_WKSP(p_length, n_length) );
}

/* ECDHKeyDerive: ECBasic + Q + (Q*d) + d + secret (p_length) */
VSECPRIM_FUNC(int) actECDHWksp(int p_length, int n_length)
{
   return ( actEC_DH_WKSP(p_length, n_length) );
}

/* ECDSASign: ECBasic + (k*Q) + PointAffine + e + d + r + s + k */
VSECPRIM_FUNC(int) actECDSASpWksp(int p_length, int n_length)
{
   return ( actEC_DSASp_WKSP(p_length, n_length) );
}

/* ECDSAVerify: ECBasic + G + Q + (G+Q) + (u1*G+u2*Q) + u1 + u2 + e + r + s */
VSECPRIM_FUNC( int ) actECDSAVpWksp( int p_length, int n_length )
{
   return ( actEC_DSAVp_WKSP(p_length, n_length) );
}

/* EC-B/D: ECBasic + d + (d*Q) + PointAffine */
VSECPRIM_FUNC( int ) actECBDWksp( int p_length, int n_length )
{
   return ( actEC_BDKA_WKSP(p_length, n_length) );
}
/****************************************************************************
 **
 ** FUNCTIONs:
 **  actECGetPrimeAndOrderBytes
 **
 **  These functions extracts the length in bytes of the prime p and
 **  the basepoint order n for the given domain parameters.
 **
 ** input:
 ** - domain:     pointer to curve's domain parameter
 ** - p_bytes:    where length of p is returned
 ** - n_bytes:    where length of n is returned
 **
 ** output:
 ** - p_bytes:    length of p
 ** - n_bytes:    length of n
 ** - returns:    actEXCEPTION_DOMAIN    domain decoding error
 **               actOK                  else
 **
 ** assumes:
 **
 ** uses:
 **
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECGetPrimeAndOrderBytes(
   actPROMU8 domain, VSECPRIM_P2VAR_PARA(int) p_bytes, VSECPRIM_P2VAR_PARA(int) n_bytes)
{
   return actECParseDomain(domain,
                            p_bytes,
                            (VSECPRIM_P2VAR_PARA(actPROMU8))NULL_PTR,
                            (VSECPRIM_P2VAR_PARA(actPROMU8))NULL_PTR,
                            n_bytes,
                            (VSECPRIM_P2VAR_PARA(actPROMU8))NULL_PTR);
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTECLENGTHINFO_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actECLengthInfo.c
 *********************************************************************************************************************/
