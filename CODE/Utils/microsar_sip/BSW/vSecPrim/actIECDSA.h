/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actIECDSA.h
 *        \brief  Interface for ECDSA verification.
 *
 *      \details This file is part of the embedded systems library cvActLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#ifndef ACTIECDSA_H
# define ACTIECDSA_H

# include "actITypes.h"

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Return and Errorcodes
 ***************************************************************************/
/* signature not validated */
# define actSIGNATURE_INVALID                                         (0x80)
/* signature (r, s) does not satisfiy 0 < r, s < n  (n = basepoint order) */
# define actSIGNATURE_OUT_OF_RANGE                                    (0x81)

/****************************************************************************
 ** Function Prototypes
 ***************************************************************************/
# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
extern "C"
{
# endif

/****************************************************************************
 **
 ** FUNCTION:
 **   actECDSAGetSignatureCompLength
 **
 **  This function returns the signature component length in bytes
 **  for the desired curve.
 **
 ** input:
 ** - domain:      domain parameter
 **
 ** output:
 ** - returns:    length of each signature component (r, s) in bytes
 **                 (0, if domain not valid)
 **
 ** assumes:
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(int) actECDSAGetSignatureCompLength(actPROMU8 domain);

/****************************************************************************
 **
 ** FUNCTION:
 **  actECDSAInitVerify
 **
 **  This function initializes the ECC workspace.
 **
 ** input:
 ** - domain:      domain parameter
 ** - domain_ext:  domain parameter extensions (Montgomery constants etc.)
 ** - wksp:        workspace
 ** - wksp_len:    length of workspace in bytes
 **
 ** output:
 ** - wksp:       initialized workspace
 ** - returns:    actEXCEPTION_NULL         if an input parameter is NULL
 **               actEXCEPTION_DOMAIN       domain decoding error
 **               actEXCEPTION_DOMAIN_EXT   domain_ext decoding error
 **               actEXCEPTION_MEMORY       wksp_len to small
 **               actOK                     else
 **
 ** assumes:
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDSAInitVerify(actPROMU8 domain, actPROMU8 domain_ext, VSECPRIM_P2VAR_PARA(actU8) wksp, int wksp_len);

/****************************************************************************
 **
 ** FUNCTION:
 **  actECDSAVerify
 **
 **  This function verifies a signature of a message value.
 **
 ** input:
 ** - message:     pointer to message that has been signed
 ** - message_len: length of message in bytes
 ** - signature_r: pointer to the signature component r of (r, s)
 ** - signature_r_len: length of signature_r
 ** - signature_s: pointer to the signature component s of (r, s)
 ** - signature_s_len: length of signature_s
 ** - publickey_x: x-coordinate of the public key Q of the key pair (d, Q)
 ** - publickey_y: y-coordinate of the public key Q of the key pair (d, Q)
 ** - wksp:        workspace
 **
 ** output:
 ** - returns:    actEXCEPTION_NULL          if an input parameter is NULL
 **               actEXCEPTION_LENGTH        wrong message_len (in bits)
 **               actEXCEPTION_PUBKEY        if Q is invalid
 **               actSIGNATURE_OUT_OF_RANGE  if (r==0||s==0||r>=n||s>=n)
 **               actSIGNATURE_INVALID       if the signature is invalid
 **               actEXCEPTION_UNKNOWN       internal error (result point
 **                                          not on curve)
 **               actOK                      else
 **
 ** assumes:
 ** - wksp is initialized with actECDSAInitVerify
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDSAVerify(VSECPRIM_P2CONST_PARA(actU8) message, int message_len,
                                            VSECPRIM_P2CONST_PARA(actU8) signature_r, int signature_r_len,
                                            VSECPRIM_P2CONST_PARA(actU8) signature_s, int signature_s_len,
                                            VSECPRIM_P2CONST_PARA(actU8) publickey_x, VSECPRIM_P2CONST_PARA(actU8) publickey_y, VSECPRIM_P2VAR_PARA(actU8) wksp);

/****************************************************************************
 **
 ** FUNCTION:
 **  actECDSAInitSign
 **
 **  This function initializes the ECC workspace.
 **
 ** input:
 ** - domain:      domain parameter
 ** - domain_ext:  domain parameter extensions (Montgomery constants etc.)
 ** - speedup_ext: (optional) precomputations (for ECDSA-Sign, -GenKey)
 ** - wksp:        workspace
 ** - wksp_len:    length of workspace in bytes
 **
 ** output:
 ** - wksp:       initialized workspace
 ** - returns:    actEXCEPTION_NULL         if an input parameter is NULL
 **               actEXCEPTION_DOMAIN       domain decoding error
 **               actEXCEPTION_DOMAIN_EXT   domain_ext decoding error
 **               actEXCEPTION_SPEEDUP_EXT  speedup_ext decoding error
 **               actEXCEPTION_MEMORY       wksp_len to small
 **               actOK                     else
 **
 ** assumes:
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDSAInitSign(actPROMU8 domain, actPROMU8 domain_ext, actPROMU8 speedup_ext, VSECPRIM_P2VAR_PARA(actU8) wksp, int wksp_len);

/****************************************************************************
 **
 ** FUNCTION:
 **  actECDSASign
 **
 **  This function signs the message value under usage of the private key.
 **
 ** input:
 ** - message:     pointer to message to be signed
 ** - message_len: length of message in bytes
 ** - privatekey:  the private key d of the key pair (d, Q)
 ** - signature_r: buffer to store the signature component r of (r, s)
 ** - signature_r_len: length of signature_r buffer
 ** - signature_s: buffer to store the signature component s of (r, s)
 ** - signature_s_len: length of signature_s buffer
 ** - wksp:        workspace
 **
 ** output:
 ** - signature_r: signature component r of (r, s)
 ** - signature_r_len: length of signature_r
 ** - signature_s: signature component s of (r, s)
 ** - signature_s_len: length of signature_s
 ** - returns:    actEXCEPTION_NULL       if an input parameter is NULL
 **               actEXCEPTION_MEMORY     signature buffer to small
 **               actEXCEPTION_LENGTH     wrong message_len (in bits)
 **               actEXCEPTION_PRIVKEY    if (d==0||d>=n)
 **               actEXCEPTION_UNKNOWN    internal error (result point
 **                                       not on curve)
 **               actOK                   else
 **
 ** assumes:
 ** - wksp is initialized with actECDSAInitSign
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDSASign(VSECPRIM_P2CONST_PARA(actU8) message, int message_len,
                                          VSECPRIM_P2CONST_PARA(actU8) privatekey, VSECPRIM_P2VAR_PARA(actU8) signature_r,
                                          VSECPRIM_P2VAR_PARA(int) signature_r_len, VSECPRIM_P2VAR_PARA(actU8) signature_s,
                                          VSECPRIM_P2VAR_PARA(int) signature_s_len, VSECPRIM_P2VAR_PARA(actU8) wksp);

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
}                               /* extern "C" */
# endif

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* ACTIECDSA_H */

/**********************************************************************************************************************
 *  END OF FILE: actIECDSA.h
 *********************************************************************************************************************/
