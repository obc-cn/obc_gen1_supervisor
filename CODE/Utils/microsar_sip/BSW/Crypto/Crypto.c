/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2018 by Vector Informatik GmbH.                                                  All rights reserved.
 * 
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  Crypto.c
 *      Project:  MICROSAR IP
 *       Module:  MICROSAR Crypto
 *    Generator:  -
 *
 *  Description:  implementation
 *  
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Crypto module. >> Crypto.h
 *********************************************************************************************************************/

#define CRYPTO_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0857 MACRO_LIMIT */ /* Macros improve code readability */
#include "Crypto.h"
#include "Crypto_Priv.h"


#include "ESLib_types.h"  /* renamed by Vector */
#include "ESLib_ASN_1.h"  /* renamed by Vector */

#include "actIRSA.h"
#include "actUtilities.h"

#include "ESLib_RNG.h"

#if( (defined CRYPTO_ECC_SUPPORT_ENABLED) && (STD_ON == CRYPTO_ECC_SUPPORT_ENABLED) )
#include "vstdlib.h"
#endif

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/
/* Check consistency of source and header file. */
#if ( (CRYPTO_SW_MAJOR_VERSION != 4u) || (CRYPTO_SW_MINOR_VERSION != 0u) || (CRYPTO_SW_PATCH_VERSION != 1u) )
  #error "Crypto.c: Source and Header file are inconsistent!"
#endif

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

#if defined (STATIC)
#else
# define STATIC static
#endif

#define CRYPTO_VENABLE_CANOE_WRITE_STRING                STD_OFF

#define CRYPTO_CODE_HMAC_IPAD                            ((uint8) 0x36u)
#define CRYPTO_CODE_HMAC_OPAD                            ((uint8) 0x5Cu)

#define CRYPTO_RAND_SSED_LEN                             42
#define CRYPTO_RSA_KEY_PAIR_MODULE_SIZE_MIN              46
#define CRYPTO_ASN1_DIGESTINFO_MD5_LEN                   18
#define CRYPTO_ASN1_DIGESTINFO_SHA1_LEN                  15
#define CRYPTO_ASN1_DIGESTINFO_SHA256_LEN                19

/****************************************************************************
 ***************************************************************************/

#define ESL_WS_RSA_V15_SV_MD5_BUFFERLENGTH                      (ACT_ALIGN (sizeof(eslt_WorkSpaceHeader)))
#define ESL_WS_RSA_V15_SV_MD5_BUFFER                            (ACT_ALIGN (ESL_WS_RSA_V15_SV_MD5_BUFFERLENGTH + sizeof(eslt_Length)))
#define ESL_WS_RSA_V15_SV_MD5_WS_MD5(ByteSizeOfBuffer)          (ACT_ALIGN (ESL_WS_RSA_V15_SV_MD5_BUFFER + (ByteSizeOfBuffer)))
#define ESL_WS_RSA_V15_SV_MD5_WS_RSA_PRIM(ByteSizeOfBuffer)     (ACT_ALIGN (ESL_WS_RSA_V15_SV_MD5_WS_MD5(ByteSizeOfBuffer) + sizeof(eslt_WorkSpaceMD5)))
#define ESL_SIZEOF_WS_RSA_V15_SV_MD5_OVERHEAD(ByteSizeOfBuffer) (ESL_WS_RSA_V15_SV_MD5_WS_RSA_PRIM(ByteSizeOfBuffer))

/* hash_len + tlvHeader_len + sizeof(byte) + digestInfo_len */
#define ASN1_SIZEOF_PARAMETERS_WITH_FIXED_LENGTH_MD5 (16 + 3 + 18)
#define ASN1_SIZEOF_PARAMETERS_WITH_FIXED_LENGTH_NULL (0 + 3 + 0)

#define ASN1_MINIMAL_SIGVER_KEY_LENGTH_MD5           (ASN1_SIZEOF_PARAMETERS_WITH_FIXED_LENGTH_MD5 + ASN1_MINIMAL_PADDING_LENGTH)
#define ASN1_MINIMAL_SIGVER_KEY_LENGTH_NULL          (ASN1_SIZEOF_PARAMETERS_WITH_FIXED_LENGTH_NULL + ASN1_MINIMAL_PADDING_LENGTH)

/* DigestInfo algorithmIdentifier */
#define ASN1_DIGESTINFO_RSA      {0x30, 0x35, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x01, 0x05, 0x00, 0x04, 0x24}
#define ASN1_DIGESTINFO_MD5      {0x30, 0x20, 0x30, 0x0c, 0x06, 0x08, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x02, 0x05, 0x05, 0x00, 0x04, 0x10}
#define ASN1_DIGESTINFO_SHA1     {0x30, 0x21, 0x30, 0x09, 0x06, 0x05, 0x2b, 0x0e, 0x03, 0x02, 0x1a, 0x05, 0x00, 0x04, 0x14}
#define ASN1_DIGESTINFO_SHA256   {0x30, 0x31, 0x30, 0x0d, 0x06, 0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x01, 0x05, 0x00, 0x04, 0x20}
/* MD5 (18byte), SHA1 (15byte), SHA256 (19byte) */
/*
MD5        30 20 30 0c 06 08 2a 86 48 86 f7 0d 02 05 05 00 04 10
SHA-1      30 21 30 09 06 05 2b 0e 03 02 1a 05 00 04 14
SHA-256    30 31 30 0d 06 09 60 86 48 01 65 03 04 02 01 05 00 04 20
SHA-384    30 41 30 0d 06 09 60 86 48 01 65 03 04 02 02 05 00 04 30
SHA-512    30 51 30 0d 06 09 60 86 48 01 65 03 04 02 03 05 00 04 40
*/

/* HashAlgorithms and SignatureAlgorithms as defined in RFC5246 (TLS 1.2), section 7.4.1.4.1. Signature Algorithms */
/* HashAlgorithm     : none(0), md5(1), sha1(2), sha224(3), sha256(4), sha384(5), sha512(6), (255) */
/* SignatureAlgorithm: anonymous(0), rsa(1), dsa(2), ecdsa(3), (255) */
#define CRYPTO_HASH_ALGORITHM_NONE               0x00u
#define CRYPTO_HASH_ALGORITHM_MD5                0x01u
#define CRYPTO_HASH_ALGORITHM_SHA1               0x02u
#define CRYPTO_HASH_ALGORITHM_SHA224             0x03u
#define CRYPTO_HASH_ALGORITHM_SHA256             0x04u
#define CRYPTO_HASH_ALGORITHM_SHA384             0x05u
#define CRYPTO_HASH_ALGORITHM_SHA512             0x06u

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3453,3458 FCT_LIKE_MACROS */ /* MISRA 19.4,19.7: Macros improve code readability */
#if ( CRYPTO_VENABLE_CANOE_WRITE_STRING == STD_ON )
  #include "stdio.h"
  #define CANOE_WRITE_STRING(Txt)                 CANoeAPI_WriteString((Txt));
  #define CANOE_WRITE_STRING_1(Txt, P1)           \
    _snprintf(Crypto_CanoeWriteStr, 256, Txt, P1); CANoeAPI_WriteString((Crypto_CanoeWriteStr));
  #define CANOE_WRITE_STRING_2(Txt, P1, P2)       \
    _snprintf(Crypto_CanoeWriteStr, 256, Txt, P1, P2); \
    CANoeAPI_WriteString((Crypto_CanoeWriteStr));
  #define CANOE_WRITE_STRING_3(Txt, P1, P2, P3)   \
    _snprintf(Crypto_CanoeWriteStr, 256, Txt, P1, P2, P3); \
    CANoeAPI_WriteString((Crypto_CanoeWriteStr));
#else
  #define CANOE_WRITE_STRING(txt)
  #define CANOE_WRITE_STRING_1(txt, p1)
  #define CANOE_WRITE_STRING_2(txt, p1, p2)
  #define CANOE_WRITE_STRING_3(txt, p1, p2, p3)
#endif
/* PRQA L:FCT_LIKE_MACROS */
/* PRQA L:MACRO_LIMIT */

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/
/* PRQA S 5087,3614 ASR_MEMMAP */ /* MISRA 19.1: AUTOSAR Memory Mapping */
#define CRYPTO_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h"
#define CRYPTO_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h"

#define CRYPTO_START_SEC_CONST_32BIT
#include "MemMap.h"

/* The curve params in shorthand notation:

    Name: ANSIp256r1
    p:    0xffffffff00000001000000000000000000000000ffffffffffffffffffffffff
    a:    0xffffffff00000001000000000000000000000000fffffffffffffffffffffffc
    b:    0x5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b
    Gx:   0x6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296
    Gy:   0x4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5
    n:    0xffffffff00000000ffffffffffffffffbce6faada7179e84f3b9cac2fc632551
    h:    0x1
*/

#if( (defined CRYPTO_ECC_SUPPORT_ENABLED) && (STD_ON == CRYPTO_ECC_SUPPORT_ENABLED) )
#if (CRYPTO_CV_BYTE_PER_DIGIT == 2)
/* 2 byte per digit */

/* ***************************************************************************
 curve domain parameters that are used in all ECC computations are put into
 esl_EccDomain
*************************************************************************** */
/*
 The curve data ASN1 structure:
 
    TLV(SEQUENCE) {
      TLV(INTEGER(version)),
      TLV(SEQUENCE) {
        TLV(OBJECT_IDENTIFIER(prime OID)),
        TLV(INTEGER(p))
      },
      TLV(SEQUENCE) {
        TLV(OCTETSTRING(a)),
        TLV(OCTETSTRING(b))
      },
      TLV(OCTETSTRING(G)),
      TLV(INTEGER(n)),
      TLV(INTEGER(h))
    }
*/
const uint8 Crypto_EccCurveANSIp256r1Domain [] = { /* 227 byte */
  /* SEQUENCE */
  0x30,0x81,0xe0,
  /*   version */
  0x02,0x01,0x01,
  /*   SEQUENCE */
  0x30,0x2c,
  /*     prime OID */
  0x06,0x07,0x2a,0x86,0x48,0xce,0x3d,0x01,0x01,
  /*     p   (field prime) */
  0x02,0x21,0x00,
  0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
  /*   SEQUENCE */
  0x30,0x44,
  /*     a   (coefficient a of the curve equation) */
  0x04,0x20,
  0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,
  /*     b   (coefficient b of the curve equation) */
  0x04,0x20,
  0x5a,0xc6,0x35,0xd8,0xaa,0x3a,0x93,0xe7,0xb3,0xeb,0xbd,0x55,0x76,0x98,0x86,0xbc,
  0x65,0x1d,0x06,0xb0,0xcc,0x53,0xb0,0xf6,0x3b,0xce,0x3c,0x3e,0x27,0xd2,0x60,0x4b,
  /*   G   (basepoint of the curve) */
  0x04,0x41,0x04,
  0x6b,0x17,0xd1,0xf2,0xe1,0x2c,0x42,0x47,0xf8,0xbc,0xe6,0xe5,0x63,0xa4,0x40,0xf2,
  0x77,0x03,0x7d,0x81,0x2d,0xeb,0x33,0xa0,0xf4,0xa1,0x39,0x45,0xd8,0x98,0xc2,0x96,
  0x4f,0xe3,0x42,0xe2,0xfe,0x1a,0x7f,0x9b,0x8e,0xe7,0xeb,0x4a,0x7c,0x0f,0x9e,0x16,
  0x2b,0xce,0x33,0x57,0x6b,0x31,0x5e,0xce,0xcb,0xb6,0x40,0x68,0x37,0xbf,0x51,0xf5,
  /*   n   (order of G) */
  0x02,0x21,0x00,
  0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
  0xbc,0xe6,0xfa,0xad,0xa7,0x17,0x9e,0x84,0xf3,0xb9,0xca,0xc2,0xfc,0x63,0x25,0x51,
  /*   h   (cofactor) */
  0x02,0x01,0x01,
};

/* ***************************************************************************
 curve domain parameters extensions are put into
 esl_EccDomainExt
*************************************************************************** */
/*
 The curve extensions ASN1 structure:
 
    TLV(SEQUENCE) {
      TLV(SEQUENCE) {
        TLV(OCTETSTRING(p_RR)),
        TLV(OCTETSTRING(p_bar)),
        TLV(SEQUENCE) {
          TLV(OCTETSTRING(a_R)),
          TLV(OCTETSTRING(b_R))
        },
        TLV(OCTETSTRING(G_R)),
      }
      TLV(SEQUENCE) {
        TLV(OCTETSTRING(n_RR)),
        TLV(OCTETSTRING(n_bar)),
      }
    }
*/
const uint8 Crypto_EccCurveANSIp256r1DomainExt [] = { /* 221 byte */
  /* SEQUENCE */
  0x30,0x81,0xda,
  /*   SEQUENCE */
  0x30,0x81,0xaf,
  /*     p_RR  (R^2 in prime field) */
  0x04,0x20,
  0x00,0x00,0x00,0x04,0xff,0xff,0xff,0xfd,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfe,
  0xff,0xff,0xff,0xfb,0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,
  /*     p_bar   (low digit of -(p^-1)) */
  0x04,0x02,0x00,0x01,
  /*     SEQUENCE */
  0x30,0x44,
  /*       a_R   (a in Montgomery representation) */
  0x04,0x20,
  0xff,0xff,0xff,0xfc,0x00,0x00,0x00,0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x03,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,
  /*       b_R   (b in Montgomery representation) */
  0x04,0x20,
  0xdc,0x30,0x06,0x1d,0x04,0x87,0x48,0x34,0xe5,0xa2,0x20,0xab,0xf7,0x21,0x2e,0xd6,
  0xac,0xf0,0x05,0xcd,0x78,0x84,0x30,0x90,0xd8,0x9c,0xdf,0x62,0x29,0xc4,0xbd,0xdf,
  /*     G_R   (G in Montgomery representation) */
  0x04,0x41,0x04,
  0x18,0x90,0x5f,0x76,0xa5,0x37,0x55,0xc6,0x79,0xfb,0x73,0x2b,0x77,0x62,0x25,0x10,
  0x75,0xba,0x95,0xfc,0x5f,0xed,0xb6,0x01,0x79,0xe7,0x30,0xd4,0x18,0xa9,0x14,0x3c,
  0x85,0x71,0xff,0x18,0x25,0x88,0x5d,0x85,0xd2,0xe8,0x86,0x88,0xdd,0x21,0xf3,0x25,
  0x8b,0x4a,0xb8,0xe4,0xba,0x19,0xe4,0x5c,0xdd,0xf2,0x53,0x57,0xce,0x95,0x56,0x0a,
  /*   SEQUENCE */
  0x30,0x26,
  /*     n_RR   (R^2 in order field) */
  0x04,0x20,
  0x66,0xe1,0x2d,0x94,0xf3,0xd9,0x56,0x20,0x28,0x45,0xb2,0x39,0x2b,0x6b,0xec,0x59,
  0x46,0x99,0x79,0x9c,0x49,0xbd,0x6f,0xa6,0x83,0x24,0x4c,0x95,0xbe,0x79,0xee,0xa2,
  /*     n_bar   (low digit of -(n^-1)) */
  0x04,0x02,0xbc,0x4f,
};

/* ***************************************************************************
 some additional precomputed points for key generation and signature
 they are not needed for diffie hellman and verification
*************************************************************************** */
/*
 The curve precomputations ASN1 structure:
 
    TLV(SEQUENCE) {
      TLV(OCTETSTRING(groups)),
      TLV(SEQUENCE) {
        TLV(OCTETSTRING(2*G_R)),
        ...
        [2^groups - 1 precomputed points and correction point D_R]
        ...
        TLV(OCTETSTRING(D_R))
      },
    }
*/
const uint8 Crypto_EccCurveANSIp256r1SpeedUpExt [] = { /* 547 byte */
  /* SEQUENCE */
  0x30,0x82,0x02,0x1f,
  /*   groups parameter */
  0x04,0x01,0x03,
  /*   SEQUENCE */
  0x30,0x82,0x02,0x18,
  /*     2 * G in Montgomery representation */
  0x04,0x41,0x04,
  0xf6,0xbb,0x32,0xe4,0x3d,0xcf,0x3a,0x3b,0x73,0x22,0x05,0x03,0x8d,0x14,0x90,0xd9,
  0xaa,0x6a,0xe3,0xc1,0xa4,0x33,0x82,0x7d,0x85,0x00,0x46,0xd4,0x10,0xdd,0xd6,0x4d,
  0x78,0xc5,0x77,0x51,0x0a,0x5b,0x8a,0x3b,0x19,0xa8,0xfb,0x0e,0x92,0x04,0x2d,0xbe,
  0x15,0x2c,0xd7,0xcb,0xeb,0x23,0x6f,0xf8,0x2f,0x36,0x48,0xd3,0x61,0xbe,0xe1,0xa5,
  /*     (2^86 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0xda,0x59,0xad,0x0d,0xc7,0x0d,0xed,0xbc,0x33,0x6b,0xcc,0x89,0x6f,0x1f,0xca,0x13,
  0x1f,0x08,0x80,0xb0,0xe1,0xf4,0xe3,0x02,0x46,0x15,0xd9,0x12,0xc1,0xd8,0x5f,0x12,
  0x2a,0xef,0xd9,0x5a,0xdd,0xc8,0x4f,0x79,0xa3,0xb1,0xc2,0xf2,0x60,0x32,0x1b,0xbb,
  0xba,0xed,0x81,0xcd,0xf4,0x99,0x0c,0xfd,0x38,0x97,0xef,0xae,0xb0,0xf6,0x2e,0xce,
  /*     (2^86 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x15,0xfd,0x84,0x55,0x0f,0x1c,0x86,0x84,0x57,0x5f,0xcc,0x49,0x01,0x57,0xf2,0xad,
  0x6c,0x12,0xfa,0xa9,0xfa,0xf7,0x90,0xe8,0x63,0x36,0xff,0x57,0xba,0xf5,0x98,0x07,
  0x70,0xaa,0x92,0xc6,0xda,0x2b,0x23,0x7c,0xd0,0xb6,0x16,0xca,0x6f,0x5e,0x49,0x06,
  0xd4,0x73,0xff,0x4e,0x67,0x6c,0x6c,0x11,0x94,0x4f,0x97,0x01,0xe2,0x2c,0xd1,0x70,
  /*     (2^172 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x73,0xa6,0x37,0x95,0x2f,0x83,0x7f,0x32,0x36,0x06,0x82,0xc0,0xe4,0x0e,0x81,0x25,
  0xec,0xfd,0xb3,0xa2,0x98,0x9a,0x01,0x82,0x21,0xe0,0x7f,0x9a,0xbc,0x0a,0x70,0xc0,
  0xb7,0x1e,0xf4,0xef,0x34,0xe2,0x2a,0xb1,0xf9,0x35,0x21,0x23,0xaf,0x3d,0x5d,0x7e,
  0xef,0xb9,0x7f,0xec,0xeb,0xf4,0xc7,0xa5,0xf4,0xeb,0x8c,0xef,0x9c,0x0d,0x32,0x6b,
  /*     (2^172 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x04,0x51,0x20,0x9d,0x10,0x44,0xf3,0xfe,0x9b,0x45,0x74,0xb0,0x53,0x96,0x9b,0xcc,
  0x6f,0xb2,0x38,0xc2,0xd5,0xe1,0xe9,0x0e,0x60,0x2d,0x9b,0x54,0x9d,0x20,0xb7,0x33,
  0x8b,0x61,0x9b,0xe0,0x6c,0xb0,0xf9,0x64,0x96,0xa4,0x69,0xf4,0x99,0xe2,0x5f,0xfe,
  0x52,0x2c,0xb3,0x5c,0x96,0x7b,0x2b,0x08,0x7e,0x95,0x17,0xbb,0x66,0xdd,0x6b,0xcf,
  /*     (2^172 + 2^86 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x19,0x29,0xcf,0x60,0x6d,0x8e,0x17,0x6f,0x2e,0xd8,0x08,0x5a,0x69,0x55,0xc2,0x90,
  0x00,0x0f,0xc8,0xd4,0x84,0x1b,0xe9,0xed,0x8f,0x2e,0xac,0xfe,0x49,0x38,0x89,0x95,
  0x68,0xfe,0x61,0x07,0x8f,0x61,0x83,0x2b,0x13,0xa8,0x1b,0x95,0xb2,0x6c,0x6e,0x05,
  0x58,0xd7,0x67,0xad,0x6c,0xb6,0x26,0xcd,0x2e,0xfd,0x26,0xa5,0xfd,0x1a,0x09,0xdb,
  /*     (2^172 + 2^86 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0xa2,0x13,0xc8,0x07,0xfd,0x95,0x8b,0xe4,0xbc,0x98,0x2c,0x36,0xe9,0x4d,0xd3,0xc1,
  0xae,0x8b,0x32,0x58,0x92,0x91,0x14,0xc8,0xe4,0x5a,0x10,0xbd,0xc1,0xe0,0x67,0x8b,
  0xea,0x8d,0x82,0x90,0xba,0xab,0x01,0x1b,0x40,0xdf,0x78,0xe0,0x60,0xf5,0xa0,0x6b,
  0x9e,0x99,0xe5,0x43,0x5e,0xde,0x80,0xae,0xc2,0x68,0x3d,0xff,0xda,0xa2,0xcc,0xa7,
  /*     correction point D = - ((2^86 - 1) * G) in Montgomery representation */
  0x04,0x41,0x04,
  0x7a,0x22,0xeb,0xe8,0x23,0x5b,0xde,0x3b,0x6d,0xfe,0x83,0xfd,0x1b,0x62,0xe3,0xcd,
  0x01,0x94,0x9a,0x18,0xba,0x87,0x5f,0x3c,0x0a,0x36,0xc1,0xc7,0xbf,0x34,0x15,0x09,
  0x23,0xc3,0xa9,0xc1,0x49,0x0d,0x76,0xe8,0x0a,0x8e,0x84,0x2a,0x66,0xa6,0x30,0x19,
  0x0f,0x48,0x18,0xb6,0x72,0xca,0xfb,0x8a,0x69,0x7d,0x55,0xe5,0x85,0x60,0x37,0x65,
};
#endif

#if (CRYPTO_CV_BYTE_PER_DIGIT == 4)
/* 4 byte per digit */

/* ***************************************************************************
 curve domain parameters that are used in all ECC computations are put into
 esl_EccDomain
*************************************************************************** */
/*
 The curve data ASN1 structure:
 
    TLV(SEQUENCE) {
      TLV(INTEGER(version)),
      TLV(SEQUENCE) {
        TLV(OBJECT_IDENTIFIER(prime OID)),
        TLV(INTEGER(p))
      },
      TLV(SEQUENCE) {
        TLV(OCTETSTRING(a)),
        TLV(OCTETSTRING(b))
      },
      TLV(OCTETSTRING(G)),
      TLV(INTEGER(n)),
      TLV(INTEGER(h))
    }
*/
const uint8 Crypto_EccCurveANSIp256r1Domain [] = { /* 227 byte */
  /* SEQUENCE */
  0x30,0x81,0xe0,
  /*   version */
  0x02,0x01,0x01,
  /*   SEQUENCE */
  0x30,0x2c,
  /*     prime OID */
  0x06,0x07,0x2a,0x86,0x48,0xce,0x3d,0x01,0x01,
  /*     p   (field prime) */
  0x02,0x21,0x00,
  0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
  /*   SEQUENCE */
  0x30,0x44,
  /*     a   (coefficient a of the curve equation) */
  0x04,0x20,
  0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,
  /*     b   (coefficient b of the curve equation) */
  0x04,0x20,
  0x5a,0xc6,0x35,0xd8,0xaa,0x3a,0x93,0xe7,0xb3,0xeb,0xbd,0x55,0x76,0x98,0x86,0xbc,
  0x65,0x1d,0x06,0xb0,0xcc,0x53,0xb0,0xf6,0x3b,0xce,0x3c,0x3e,0x27,0xd2,0x60,0x4b,
  /*   G   (basepoint of the curve) */
  0x04,0x41,0x04,
  0x6b,0x17,0xd1,0xf2,0xe1,0x2c,0x42,0x47,0xf8,0xbc,0xe6,0xe5,0x63,0xa4,0x40,0xf2,
  0x77,0x03,0x7d,0x81,0x2d,0xeb,0x33,0xa0,0xf4,0xa1,0x39,0x45,0xd8,0x98,0xc2,0x96,
  0x4f,0xe3,0x42,0xe2,0xfe,0x1a,0x7f,0x9b,0x8e,0xe7,0xeb,0x4a,0x7c,0x0f,0x9e,0x16,
  0x2b,0xce,0x33,0x57,0x6b,0x31,0x5e,0xce,0xcb,0xb6,0x40,0x68,0x37,0xbf,0x51,0xf5,
  /*   n   (order of G) */
  0x02,0x21,0x00,
  0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
  0xbc,0xe6,0xfa,0xad,0xa7,0x17,0x9e,0x84,0xf3,0xb9,0xca,0xc2,0xfc,0x63,0x25,0x51,
  /*   h   (cofactor) */
  0x02,0x01,0x01,
};

/* ***************************************************************************
 curve domain parameters extensions are put into
 esl_EccDomainExt
*************************************************************************** */
/*
 The curve extensions ASN1 structure:
 
    TLV(SEQUENCE) {
      TLV(SEQUENCE) {
        TLV(OCTETSTRING(p_RR)),
        TLV(OCTETSTRING(p_bar)),
        TLV(SEQUENCE) {
          TLV(OCTETSTRING(a_R)),
          TLV(OCTETSTRING(b_R))
        },
        TLV(OCTETSTRING(G_R)),
      }
      TLV(SEQUENCE) {
        TLV(OCTETSTRING(n_RR)),
        TLV(OCTETSTRING(n_bar)),
      }
    }
*/
const uint8 Crypto_EccCurveANSIp256r1DomainExt [] = { /* 225 byte */
  /* SEQUENCE */
  0x30,0x81,0xde,
  /*   SEQUENCE */
  0x30,0x81,0xb1,
  /*     p_RR  (R^2 in prime field) */
  0x04,0x20,
  0x00,0x00,0x00,0x04,0xff,0xff,0xff,0xfd,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfe,
  0xff,0xff,0xff,0xfb,0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,
  /*     p_bar   (low digit of -(p^-1)) */
  0x04,0x04,0x00,0x00,0x00,0x01,
  /*     SEQUENCE */
  0x30,0x44,
  /*       a_R   (a in Montgomery representation) */
  0x04,0x20,
  0xff,0xff,0xff,0xfc,0x00,0x00,0x00,0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x03,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,
  /*       b_R   (b in Montgomery representation) */
  0x04,0x20,
  0xdc,0x30,0x06,0x1d,0x04,0x87,0x48,0x34,0xe5,0xa2,0x20,0xab,0xf7,0x21,0x2e,0xd6,
  0xac,0xf0,0x05,0xcd,0x78,0x84,0x30,0x90,0xd8,0x9c,0xdf,0x62,0x29,0xc4,0xbd,0xdf,
  /*     G_R   (G in Montgomery representation) */
  0x04,0x41,0x04,
  0x18,0x90,0x5f,0x76,0xa5,0x37,0x55,0xc6,0x79,0xfb,0x73,0x2b,0x77,0x62,0x25,0x10,
  0x75,0xba,0x95,0xfc,0x5f,0xed,0xb6,0x01,0x79,0xe7,0x30,0xd4,0x18,0xa9,0x14,0x3c,
  0x85,0x71,0xff,0x18,0x25,0x88,0x5d,0x85,0xd2,0xe8,0x86,0x88,0xdd,0x21,0xf3,0x25,
  0x8b,0x4a,0xb8,0xe4,0xba,0x19,0xe4,0x5c,0xdd,0xf2,0x53,0x57,0xce,0x95,0x56,0x0a,
  /*   SEQUENCE */
  0x30,0x28,
  /*     n_RR   (R^2 in order field) */
  0x04,0x20,
  0x66,0xe1,0x2d,0x94,0xf3,0xd9,0x56,0x20,0x28,0x45,0xb2,0x39,0x2b,0x6b,0xec,0x59,
  0x46,0x99,0x79,0x9c,0x49,0xbd,0x6f,0xa6,0x83,0x24,0x4c,0x95,0xbe,0x79,0xee,0xa2,
  /*     n_bar   (low digit of -(n^-1)) */
  0x04,0x04,0xee,0x00,0xbc,0x4f,
};

/* ***************************************************************************
 some additional precomputed points for key generation and signature
 they are not needed for diffie hellman and verification
*************************************************************************** */
/*
 The curve precomputations ASN1 structure:
 
    TLV(SEQUENCE) {
      TLV(OCTETSTRING(groups)),
      TLV(SEQUENCE) {
        TLV(OCTETSTRING(2*G_R)),
        ...
        [2^groups - 1 precomputed points and correction point D_R]
        ...
        TLV(OCTETSTRING(D_R))
      },
    }
*/
const uint8 Crypto_EccCurveANSIp256r1SpeedUpExt[] = { /* 2155 byte */
  /* SEQUENCE */
  0x30,0x82,0x08,0x67,
  /*   groups parameter */
  0x04,0x01,0x05,
  /*   SEQUENCE */
  0x30,0x82,0x08,0x60,
  /*     2 * G in Montgomery representation */
  0x04,0x41,0x04,
  0xf6,0xbb,0x32,0xe4,0x3d,0xcf,0x3a,0x3b,0x73,0x22,0x05,0x03,0x8d,0x14,0x90,0xd9,
  0xaa,0x6a,0xe3,0xc1,0xa4,0x33,0x82,0x7d,0x85,0x00,0x46,0xd4,0x10,0xdd,0xd6,0x4d,
  0x78,0xc5,0x77,0x51,0x0a,0x5b,0x8a,0x3b,0x19,0xa8,0xfb,0x0e,0x92,0x04,0x2d,0xbe,
  0x15,0x2c,0xd7,0xcb,0xeb,0x23,0x6f,0xf8,0x2f,0x36,0x48,0xd3,0x61,0xbe,0xe1,0xa5,
  /*     (2^52 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x4f,0x32,0x66,0x43,0xd9,0xa6,0xf5,0x48,0xbc,0x2c,0x5f,0xf2,0xa9,0xb7,0x5b,0x8c,
  0x4b,0x52,0x4d,0x25,0x4d,0x1b,0xb0,0x68,0xf2,0x67,0x55,0x62,0xa0,0xbe,0x5d,0x0e,
  0x60,0xdf,0xae,0x28,0xb3,0xce,0xc3,0xb0,0xb0,0xb6,0x2c,0x65,0xf4,0xa1,0x7b,0x42
  ,0x7d,0x21,0xbe,0xee,0x67,0x60,0x90,0xe0,0x50,0xdd,0x68,0x44,0x12,0x58,0x83,0x5e,
  /*     (2^52 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0xfc,0x58,0x51,0xae,0x64,0xdd,0x6b,0xfc,0xd4,0xc1,0x19,0x18,0x79,0x89,0x17,0x61,
  0xfd,0xbd,0xda,0xa9,0xb8,0xad,0xb8,0x1e,0xff,0x29,0xcd,0x62,0xcb,0x07,0x11,0xf8,
  0xcf,0x37,0xb5,0x99,0xb6,0x07,0xcb,0x63,0x2f,0xda,0xc0,0x6d,0x4a,0x25,0x08,0x7d,
  0x84,0x4b,0x84,0x7e,0xff,0xae,0x5e,0xf5,0x97,0xe3,0x7e,0x66,0x71,0x6a,0x83,0x0c,
  /*     (2^104 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0xbc,0x7c,0x95,0x42,0xbb,0x7c,0x37,0x5b,0x04,0xe5,0x2f,0x8f,0x24,0xc3,0x70,0x44,
  0x26,0xb5,0xd7,0x0a,0x92,0x0a,0x68,0x1d,0x53,0x1e,0x7b,0x64,0xb1,0x13,0xf9,0x18,
  0xfe,0x27,0xd2,0xc2,0x49,0xac,0x78,0x32,0x9e,0xed,0x2e,0xca,0xa9,0x29,0x2d,0x57,
  0xd8,0x42,0xa3,0x42,0xe9,0x22,0xa3,0xd0,0xb6,0x3a,0x04,0x4b,0xf2,0xe2,0x63,0x75,
  /*     (2^104 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x27,0x93,0x3c,0x73,0x5d,0xfa,0xd6,0x08,0xf8,0x06,0x4e,0xdf,0x2c,0xac,0xe3,0xe9,
  0xc2,0x25,0x19,0x97,0xea,0xb6,0xe7,0xb5,0x0a,0x97,0x76,0x99,0x2b,0xd7,0x74,0xdd,
  0x34,0xc0,0x94,0xb9,0x9e,0xca,0xe3,0xd2,0xd0,0xa6,0xac,0x74,0x25,0x8b,0xf6,0x2b,
  0x58,0x4a,0x78,0xae,0xd2,0xc7,0xfe,0x18,0x29,0xe0,0xa9,0x9f,0x49,0x12,0x8b,0xee,
  /*     (2^104 + 2^52 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0xef,0x25,0x5c,0x54,0xa7,0x5e,0xef,0x24,0xc5,0x35,0xf8,0xb4,0x1d,0x04,0x0a,0xbc,
  0xa6,0xe1,0x2f,0x57,0x19,0x37,0x96,0x64,0xb0,0xab,0x54,0x01,0x0b,0x63,0x99,0x42,
  0x0a,0x18,0xba,0xd4,0xf6,0x24,0xdf,0x06,0x67,0x4d,0x8f,0xdc,0x18,0x0c,0xac,0xab,
  0x38,0xfc,0xc8,0xc1,0x9d,0x87,0x9e,0x2f,0xb2,0x36,0xf7,0x34,0xae,0xce,0xb0,0xea,
  /*     (2^104 + 2^52 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x27,0xd6,0xf1,0x7b,0xb5,0x92,0x2c,0x29,0xf5,0x09,0xb2,0x55,0x93,0xe6,0xd4,0x4e,
  0x14,0x31,0x5e,0x85,0xda,0x1f,0x77,0x18,0x2f,0xb5,0xe0,0x1a,0x53,0x20,0x91,0x67,
  0x0e,0x09,0x65,0xf0,0xaf,0x9f,0xdf,0xd7,0x54,0xf3,0xf7,0x95,0x0f,0x37,0xca,0x5a,
  0xa8,0xa9,0x75,0xf0,0x95,0x20,0x62,0xec,0x34,0xc0,0x08,0xf5,0xd8,0xba,0xa4,0x0b,
  /*     (2^156 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0xb2,0xe2,0xa3,0x74,0xb1,0x0e,0x51,0x1e,0xec,0xde,0x2f,0x1f,0xa6,0x53,0xaf,0x5a,
  0x58,0xc2,0x4f,0x52,0x7b,0xc2,0x3d,0xc8,0x83,0xfc,0x80,0x91,0x60,0x53,0x0d,0x0a,
  0x1e,0x51,0x3c,0xa2,0xed,0x17,0xef,0xd3,0xd8,0x66,0xf5,0x5e,0x9f,0x22,0xb4,0x33,
  0x23,0x9c,0x25,0xdf,0xad,0xe4,0x22,0x70,0xf0,0xc5,0x4b,0x32,0x9b,0xeb,0xe1,0xe4,
  /*     (2^156 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0xfe,0x97,0xb9,0xd7,0x51,0x59,0x36,0x39,0x56,0x7e,0xed,0x51,0xa4,0x79,0x0f,0x53,
  0xd1,0x12,0x2d,0x0c,0x15,0x96,0xa5,0x56,0xe0,0x4a,0xd2,0x0d,0xdc,0x49,0x65,0x1c,
  0xad,0x4e,0x79,0xbf,0x3b,0x01,0xa2,0xac,0xe3,0x98,0xd2,0x9f,0xa5,0x42,0xf3,0x28,
  0x11,0xb2,0xe3,0x2f,0x0f,0xa6,0x8d,0xb5,0x69,0x96,0xad,0x01,0x2c,0x19,0x8d,0x9c,
  /*     (2^156 + 2^52 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x25,0x2c,0xdb,0x93,0x23,0x9e,0xc2,0x3d,0x40,0xae,0x27,0x9f,0x70,0xbd,0x5f,0x07,
  0x43,0x1f,0x75,0xd4,0x4b,0x36,0xf4,0x98,0x4e,0x06,0x67,0x13,0xf5,0xa3,0x26,0x32,
  0xab,0xef,0x64,0x51,0xa5,0x80,0xcf,0xc5,0x02,0x0f,0x09,0xc3,0x17,0x15,0xfe,0xde,
  0x5b,0x77,0x67,0x3c,0x23,0xa9,0xe5,0x61,0xc1,0x8d,0xdd,0xf8,0x73,0x12,0xa2,0x46,
  /*     (2^156 + 2^52 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0xb7,0xfd,0xe8,0x13,0x40,0x69,0xdb,0x82,0xc8,0x76,0x95,0x31,0x04,0x4e,0x7c,0x74,
  0xd5,0xc2,0xa1,0xf7,0x7c,0x5f,0x91,0x1d,0x71,0x15,0x5f,0x88,0xfe,0x85,0x7d,0x21,
  0x8f,0xaa,0x7d,0x0d,0x51,0x85,0x4d,0x37,0x55,0x3c,0x2c,0x4c,0x24,0xa5,0x91,0x38,
  0x7d,0xef,0x3f,0x35,0x5d,0xba,0x96,0x39,0x41,0x3f,0xe7,0xe6,0xfe,0xc3,0x49,0x78,
  /*     (2^156 + 2^104 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x42,0xeb,0x9d,0xe0,0x91,0x10,0xc6,0x16,0x54,0x79,0xe1,0xbc,0x08,0xd0,0x25,0x5b,
  0x2c,0x6a,0x80,0xd6,0x08,0xac,0x26,0x85,0x34,0xdf,0xbf,0xc4,0xc7,0xf6,0x87,0x82,
  0x1a,0xc7,0xe5,0x28,0xe6,0x8b,0x42,0x43,0xd0,0x5a,0xd7,0x8b,0x69,0xdd,0xc0,0x36,
  0xf3,0x6a,0xcc,0x8f,0x94,0xd9,0x97,0xc7,0x97,0x99,0x1d,0xd8,0x10,0xb4,0xac,0xba,
  /*     (2^156 + 2^104 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0xcd,0x01,0xd5,0xd9,0xdb,0x95,0x20,0x39,0xb1,0xb3,0xe9,0xad,0x9a,0x5e,0x5f,0x54,
  0x71,0x67,0x46,0xd6,0xf6,0x54,0x04,0x59,0x91,0x0a,0x61,0xf1,0x15,0x2b,0x08,0x21,
  0xb2,0xae,0x5a,0x6e,0x15,0x3c,0xd1,0xbf,0xdc,0x86,0xdd,0x92,0x71,0x54,0xa4,0xf5,
  0xa2,0xd8,0x42,0xd9,0x94,0x2d,0xe7,0xc9,0x30,0xd0,0x95,0x88,0xc0,0x16,0x84,0xef,
  /*     (2^156 + 2^104 + 2^52 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x77,0x35,0x79,0xfc,0xe3,0xb6,0x92,0x72,0xed,0x70,0xe0,0x8a,0xaa,0x01,0xe0,0xe1,
  0xc7,0x9c,0x42,0xac,0x2b,0xe4,0x6d,0xa2,0x8e,0x86,0xcb,0x3d,0xa8,0x63,0x6d,0x07,
  0x05,0x27,0x74,0xd4,0x9a,0x55,0xdb,0xa4,0xda,0x65,0x5b,0x0a,0x39,0x13,0xb1,0xd3,
  0x9e,0x87,0xa0,0x57,0xcf,0x54,0xe0,0x71,0xbc,0x0f,0xe5,0x55,0x4d,0x84,0x64,0xc3,
  /*     (2^156 + 2^104 + 2^52 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x32,0x82,0xce,0xec,0xdd,0xb8,0x67,0xc9,0xca,0xb9,0xa0,0x02,0x67,0x35,0xf7,0xfa,
  0xb0,0xae,0xcb,0xda,0x19,0xb1,0x1b,0x31,0x0f,0xb8,0xfa,0xa1,0x12,0x28,0x74,0x95,
  0x76,0x71,0xfc,0xfa,0x74,0x79,0xa6,0x7b,0x29,0xae,0x79,0x9d,0x4a,0x1a,0x12,0x02,
  0x22,0xf8,0x56,0x62,0x39,0xf9,0xe0,0x85,0xf3,0x75,0x73,0x4b,0x15,0xca,0xa3,0xb9,
  /*     (2^208 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0xd2,0x19,0x7d,0x5f,0x92,0x90,0x10,0x32,0x3a,0x7c,0x10,0xe4,0x02,0x52,0x89,0xf2,
  0x0b,0x86,0x0c,0xf3,0x72,0xe0,0x10,0x34,0xb1,0xf5,0xc0,0x26,0xe3,0x75,0x42,0xca,
  0x8a,0x50,0xa5,0xb3,0xe6,0x07,0x7a,0xaf,0x46,0x5f,0x6c,0x11,0x7e,0xd9,0xf8,0xe7,
  0x8f,0xcb,0x9a,0x29,0xbf,0x6e,0x43,0xaa,0xfa,0x06,0xf8,0x35,0x26,0x7c,0xa2,0xf6,
  /*     (2^208 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x19,0xd8,0xa4,0x31,0x33,0xed,0x76,0x33,0x32,0xe3,0xec,0xa4,0xe0,0xe1,0x81,0x40,
  0x0b,0xcf,0xec,0x60,0xe0,0xf5,0x88,0x65,0x17,0xd4,0xc9,0x57,0x77,0x6f,0x7f,0xe2,
  0xd6,0x35,0xaa,0x06,0xfb,0x91,0x50,0x31,0xfe,0x4f,0x4f,0xc8,0xae,0x61,0x7e,0xbd,
  0xe3,0xbd,0xe9,0x58,0xbe,0xc0,0xe8,0xc2,0x4d,0x8c,0x2c,0x6c,0x14,0x40,0xb7,0x86,
  /*     (2^208 + 2^52 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x19,0xdd,0x5b,0x75,0x42,0xbc,0x26,0xea,0x50,0xb6,0x64,0xbe,0x42,0x36,0x68,0xd2,
  0x45,0xb7,0x9e,0xad,0x94,0xcd,0x3b,0xc4,0x57,0x93,0x45,0xdf,0x1d,0x32,0x39,0x61,
  0xe2,0x55,0x43,0x8b,0x8a,0x16,0x71,0x8c,0x8a,0xec,0xb5,0x0a,0x89,0x42,0xac,0x93,
  0x7b,0x2e,0x71,0x1a,0x5d,0x03,0x31,0x58,0xc7,0xc1,0xfb,0xaa,0x36,0x77,0xae,0x8f,
  /*     (2^208 + 2^52 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x2f,0xc4,0x92,0x2f,0x05,0xc2,0xbf,0x15,0xde,0x09,0x33,0x9f,0x16,0xae,0xa0,0x1e,
  0x0f,0x89,0x5d,0x81,0xa1,0xfd,0x08,0x22,0x30,0x0b,0x30,0x60,0xeb,0xe2,0xb4,0xfa,
  0xe4,0x16,0xf2,0xb2,0xec,0x7a,0x33,0x81,0x3b,0x95,0xb5,0x71,0x5e,0xb1,0xcd,0x73,
  0x16,0xc8,0xcc,0xa6,0x25,0x08,0x6d,0xe5,0x9d,0x32,0xd8,0x9f,0xcc,0xcf,0xd4,0x5e,
  /*     (2^208 + 2^104 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0xe1,0x9f,0x13,0xa3,0xbe,0x88,0xd9,0x14,0x78,0xb2,0xb2,0x90,0x16,0x52,0x0c,0xee,
  0xb5,0xc8,0x44,0x4f,0xbd,0x11,0x90,0x0c,0x84,0x0d,0xbc,0xbf,0x79,0x72,0xbc,0xdf,
  0x87,0x2c,0x7b,0x95,0xa2,0x6b,0x14,0x41,0x2c,0x8d,0xd0,0x74,0xcf,0x31,0xe0,0xbb,
  0xc9,0xfc,0x18,0x3c,0xe0,0xb4,0x22,0x4b,0x05,0x2d,0xdc,0x89,0x49,0xd3,0xc0,0xdf,
  /*     (2^208 + 2^104 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x99,0xe9,0xab,0xd3,0x0a,0xf8,0x53,0xd5,0xa6,0x94,0xa5,0x5a,0xe1,0xbc,0x23,0xab,
  0xbb,0x35,0xef,0xaf,0xdb,0xb3,0x14,0x39,0xf3,0x1d,0x76,0x60,0x22,0xb7,0x35,0xe2,
  0x27,0x54,0x27,0xd8,0x07,0x82,0xa2,0x31,0x12,0xa5,0x0c,0x29,0x16,0x7a,0xb8,0x3a,
  0x16,0x49,0xe3,0xb7,0xe3,0xcd,0xcf,0x9c,0xd7,0x28,0x66,0x4c,0x79,0xd1,0x97,0x17,
  /*     (2^208 + 2^104 + 2^52 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0xc6,0xb3,0x8b,0x3b,0x37,0x31,0x83,0xf8,0xdf,0x1d,0x1f,0x52,0xdc,0x7e,0xac,0xe6,
  0x88,0xe9,0x3d,0x23,0x28,0x8f,0x82,0x43,0x20,0x2e,0x5c,0x5a,0xcb,0x22,0x71,0x5e,
  0xf1,0x4f,0xd1,0x3c,0x0f,0xcb,0x60,0x36,0xaf,0xf4,0xa4,0x47,0xfa,0xac,0x41,0xc9,
  0xa9,0xd3,0x7d,0xff,0x6b,0xfa,0x98,0x35,0x77,0x79,0x8b,0x7f,0x3e,0xac,0x9c,0x4b,
  /*     (2^208 + 2^104 + 2^52 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x88,0x92,0xb6,0x93,0x3c,0x45,0xe5,0xde,0x67,0xe2,0xba,0x9a,0x8a,0xae,0x20,0xb4,
  0xe1,0xdb,0x3a,0x18,0xea,0x41,0x2c,0x82,0x13,0xec,0x35,0x59,0x77,0x17,0x10,0x7d,
  0x2f,0x44,0x1c,0xd4,0x8e,0xec,0xb8,0x40,0x87,0x34,0xf8,0x3f,0x78,0x0d,0xe4,0xaf,
  0x13,0x56,0xac,0x09,0x95,0x4d,0xb7,0x87,0xb2,0xd5,0x64,0x82,0x4c,0x41,0x9a,0xd4,
  /*     (2^208 + 2^156 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x62,0xbf,0x8c,0x2c,0x87,0x0e,0xa1,0x33,0x76,0x33,0x30,0xf5,0xe5,0xe8,0xcd,0xfb,
  0x37,0xad,0x4a,0x1a,0x3e,0x37,0xf5,0x8a,0xd6,0xc8,0xe4,0xb7,0x30,0x0c,0x0e,0x39,
  0xaf,0x9a,0x87,0x78,0x62,0xc2,0x33,0x38,0xf0,0x48,0x6d,0xe5,0xbe,0x49,0xd9,0xfe,
  0xc8,0x89,0xd8,0xa5,0xfb,0x18,0x86,0xc0,0x03,0xfb,0xc6,0x3a,0x76,0x3c,0xca,0xc9,
  /*     (2^208 + 2^156 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x6d,0x18,0x46,0x62,0x25,0xe7,0x2e,0xb9,0xe8,0x16,0x32,0xa5,0x0b,0xab,0xd5,0xaa,
  0xc2,0x99,0xec,0x02,0xb6,0xa8,0x30,0xcc,0x2d,0x6b,0x1d,0x22,0x23,0x2d,0x45,0x3c,
  0xbd,0xa7,0x76,0xde,0xd5,0xf1,0x47,0xa0,0x09,0x52,0x61,0x73,0xef,0x86,0x0a,0x04,
  0xcc,0x17,0x74,0x5d,0x24,0xf6,0x82,0x3d,0x5b,0x38,0xed,0x5d,0xb9,0x75,0x77,0x4e,
  /*     (2^208 + 2^156 + 2^52 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x4e,0x2c,0xfa,0xf1,0x77,0x48,0xe3,0xac,0xd6,0x6f,0xe9,0x93,0x28,0x26,0xf1,0x38,
  0x04,0x68,0x56,0x5c,0x63,0x74,0xc4,0x7e,0x12,0x1e,0x6a,0x71,0x3c,0x29,0x43,0xff,
  0x00,0xb5,0xcf,0xa9,0x44,0x0a,0x35,0xe8,0xad,0x3e,0x29,0x3e,0xb7,0x7c,0x8f,0xac,
  0xa3,0x84,0x5c,0x8c,0x66,0xff,0xb5,0xb4,0xe9,0xba,0xaa,0x2c,0x47,0x08,0xa6,0xc8,
  /*     (2^208 + 2^156 + 2^52 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x75,0xcd,0x56,0x21,0xd8,0xed,0xeb,0xea,0x1e,0x0a,0x91,0x9b,0x72,0xb2,0xc4,0x91,
  0x5c,0x1e,0xe5,0x8b,0x16,0xbd,0xb9,0x97,0xaa,0xcc,0x9b,0xa3,0xe9,0x45,0xdc,0x0b,
  0xd9,0xe9,0x45,0x99,0x9a,0xb0,0x62,0x33,0x8c,0xad,0x32,0x75,0x44,0xd3,0x3f,0xce,
  0x06,0x95,0x0d,0x54,0x82,0xcc,0x55,0x17,0xaf,0x84,0x79,0xd4,0x4d,0xe2,0xfb,0x3c,
  /*     (2^208 + 2^156 + 2^104 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x42,0x3b,0xbd,0xc1,0x5f,0x22,0x41,0x1a,0xc7,0x9d,0x46,0x60,0xaf,0xf7,0x9a,0x46,
  0x11,0xc5,0xce,0xd4,0xf6,0xb8,0x19,0xa2,0x3b,0x44,0x69,0x94,0x83,0xe1,0xa2,0x46,
  0x0e,0x03,0x6e,0x47,0x34,0xab,0x0d,0x07,0xc0,0xca,0x19,0xe3,0x4e,0x90,0x9d,0xc8,
  0x80,0x8d,0x67,0x53,0xe7,0x38,0x65,0x7b,0x22,0x65,0x22,0x51,0xa9,0x64,0x03,0x9d,
  /*     (2^208 + 2^156 + 2^104 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x55,0xbe,0xb0,0x06,0xc8,0xf6,0x85,0x78,0x19,0x41,0x9c,0x53,0x4d,0xf9,0x4c,0xa5,
  0xb0,0x8a,0x71,0x55,0x16,0x71,0x53,0x17,0x9e,0x43,0x7e,0xaa,0x24,0x86,0x50,0x84,
  0x8c,0x83,0xa8,0xa9,0x4f,0x89,0xf6,0xf0,0x90,0xbd,0x20,0x18,0x57,0x09,0xc3,0x76,
  0x5c,0x36,0x4c,0xc5,0x32,0x4b,0x57,0x53,0xaf,0xdf,0xc9,0x6a,0x43,0x90,0x08,0xcd,
  /*     (2^208 + 2^156 + 2^104 + 2^52 + 1) * G in Montgomery representation */
  0x04,0x41,0x04,
  0xb5,0x15,0x65,0xd7,0x9d,0x2d,0x15,0x2e,0x18,0xc9,0x77,0xff,0x78,0xa4,0xdd,0xdd,
  0x23,0xdf,0x10,0x41,0xec,0x7e,0x66,0xdb,0xbe,0x47,0xdd,0x50,0x27,0xea,0xfc,0xc0,
  0x55,0x24,0x86,0x67,0x52,0x20,0x08,0x39,0xa0,0x64,0xd3,0x9c,0x1d,0x3b,0x43,0xca,
  0xbb,0xc1,0x5b,0x20,0x7d,0x86,0xb2,0xca,0x24,0xf6,0xa6,0xd5,0x78,0xf4,0xa4,0xde,
  /*     (2^208 + 2^156 + 2^104 + 2^52 + 2) * G in Montgomery representation */
  0x04,0x41,0x04,
  0x87,0x3e,0x4a,0xb9,0xed,0xb0,0x23,0xe5,0x8f,0xe7,0xd9,0x7f,0xf6,0x93,0xdf,0x93,
  0xf9,0x92,0xfc,0x07,0x29,0xc8,0x0f,0x16,0x14,0x94,0x32,0xe5,0x90,0x7e,0xc4,0x45,
  0x6f,0x5e,0x40,0xd7,0x47,0x72,0x8c,0xa2,0xe6,0x28,0xc9,0xf7,0xf2,0x9e,0x3c,0xa6,
  0xa0,0xbf,0x53,0x11,0x60,0x92,0xf7,0xa5,0x70,0x6f,0x31,0x3a,0x70,0x05,0x1f,0x92,
  /*     correction point D = - ((2^52 - 1) * G) in Montgomery representation */
  0x04,0x41,0x04,
  0x08,0xed,0xba,0x2d,0xbe,0xbe,0x63,0x17,0xd4,0x5b,0x61,0xb6,0x95,0x63,0xbd,0x8a,
  0xfc,0x6f,0x0d,0xf0,0x69,0x07,0xd2,0xf0,0x99,0x38,0x1c,0x58,0x81,0xbe,0xb5,0x45,
  0xfb,0x59,0x5d,0x5b,0xca,0x0e,0x1d,0x57,0x9f,0x8d,0xa9,0xda,0x22,0x87,0x98,0xb0,
  0xbe,0xb1,0x6d,0xdd,0xb0,0xb2,0xdd,0x2c,0x50,0x7b,0xe3,0x82,0x65,0x31,0x37,0x72,
};
#endif
#endif /* CRYPTO_ECC_SUPPORT_ENABLED */

#define CRYPTO_STOP_SEC_CONST_32BIT
#include "MemMap.h"

#define CRYPTO_START_SEC_VAR_NOINIT_32BIT
#include "MemMap.h"
#define CRYPTO_STOP_SEC_VAR_NOINIT_32BIT
#include "MemMap.h"

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/
#define CRYPTO_START_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h"
STATIC VAR(Crypto_StateType, CRYPTO_VAR_ZERO_INIT)       Crypto_State = CRYPTO_STATE_UNINIT;
#define CRYPTO_STOP_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h"

#define CRYPTO_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h"
STATIC eslt_WorkSpaceFIPS186 *Crypto_esl_WS_RNG;  /* inserted from ESLib_RNG.c */
STATIC eslt_WorkSpaceRNG  Crypto_RngWs;                                                                                 /* PRQA S 3218 */ /* MD_CRYPTO_8.7_3218 */
#define CRYPTO_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h"

#define CRYPTO_START_SEC_VAR_NOINIT_16BIT
#include "MemMap.h"
#define CRYPTO_STOP_SEC_VAR_NOINIT_16BIT
#include "MemMap.h"

#define CRYPTO_START_SEC_VAR_NOINIT_8BIT
#include "MemMap.h"
#if ( CRYPTO_VENABLE_CANOE_WRITE_STRING == STD_ON )
STATIC VAR(sint8, CRYPTO_VAR_NOINIT)                Crypto_CanoeWriteStr[256];
#endif
  /* CRYPTO_VENABLE_CANOE_WRITE_STRING */
#define CRYPTO_STOP_SEC_VAR_NOINIT_8BIT
#include "MemMap.h"

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define CRYPTO_START_SEC_CODE
#include "MemMap.h"

STATIC FUNC(eslt_ErrorCode, CRYPTO_CODE) Crypto_VerifyRSAComplete(
    P2VAR(eslt_WorkSpaceRSAver, AUTOMATIC, CRYPTO_APPL_VAR) workSpace,
    uint8                 DigestValueLen,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) DigestValuePtr,
    uint8                 DigestType,
    uint16                keyPairModuleSize,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) keyPairModule,
    uint16                publicKeyExponentSize,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) publicKeyExponent,
    uint16                signatureSize,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) signature);

/* There is no generated configuration for the module 'Crypto'. Therefore an extern function declaration is needed
   since the header of the file implementing the function can not be specified (because it is not known at module
   implementation time). */

extern void Appl_Crypto_GetRandArray(  /* PRQA S 3447 */ /* MD_CRYPTO_8.8_3447_external_linkage */
    uint8* TgtDataPtr,
    uint16 TgtLen );

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Crypto_InitMemory
 *********************************************************************************************************************/
/*! \brief        Init internal module state variables
 *  \param[in]    void
 *  \return       void
 *  \note         Has to be called before any other calls to the module.
 *                <br>Not reentrant
 *  \context      system startup / task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_InitMemory(void)
{
  Crypto_State = CRYPTO_STATE_UNINIT;
}

/**********************************************************************************************************************
 *  Crypto_Init
 *********************************************************************************************************************/
/*! \brief        Initialization of the Crypto component.
 *  \param[in]    void
 *  \return       void
 *  \note         This function has to be called after Crypto_InitMemory() and before any other function of this module.
 *                <br>Not reentrant
 *  \context      system startup / task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_Init( void )
{
  uint8 RandomSeed[CRYPTO_RAND_SSED_LEN];

  (void)esl_initESCryptoLib();
  CANOE_WRITE_STRING("Crypto: Init function was called");

  /* init random seed */
  Appl_Crypto_GetRandArray(&RandomSeed[0], CRYPTO_RAND_SSED_LEN);

  /* init crypto random generator */
  (void)esl_initWorkSpaceHeader(&Crypto_RngWs.header, ESL_MAXSIZEOF_WS_RNG, NULL_PTR);
  (void)esl_initRNG(&Crypto_RngWs, CRYPTO_RAND_SSED_LEN, &RandomSeed[0], NULL_PTR);  /* standard random generator has to be initialized */

  Crypto_State = CRYPTO_STATE_INIT;

}

/**********************************************************************************************************************
 *  Crypto_CheckInit
 *********************************************************************************************************************/
/*! \brief        Check if Crypto_Init was called.
 *                <br>This is needed because most crypto functions are implemented by a third party and are called
 *                directly, so no DET-checks can be added here.
 *  \param[in]    void
 *  \return       TRUE           module has been initialized
 *                <br>FALSE      module has not been initialized
 *  \note         Has to be called before usage of the module
 *  \context      initialization
 *********************************************************************************************************************/
FUNC(boolean, CRYPTO_CODE) Crypto_CheckInit( void )
{
  boolean retVal;

  if(CRYPTO_STATE_INIT == Crypto_State)
  {
    retVal = TRUE;
  }
  else
  {
    retVal = FALSE;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Crypto_GetVersionInfo
 *********************************************************************************************************************/
/*! \brief        Get Crypto software version.
 *                <br>Read the module id, the vendor id and the software implementation version.
 *  \param[in]    VersionInfoPtr        pointer for version information
 *  \return       void
 *  \note         none
 *  \context      task level
 *********************************************************************************************************************/
#if (CRYPTO_VERSION_INFO_API == STD_ON)
FUNC(void, CRYPTO_CODE) Crypto_GetVersionInfo(
    P2VAR(Std_VersionInfoType, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) VersionInfoPtr )
{
  CRYPTO_CheckDetErrorReturnVoid((NULL_PTR != VersionInfoPtr), CRYPTO_API_ID_GET_VERSION_INFO, CRYPTO_E_INV_POINTER);

  VersionInfoPtr->vendorID = CRYPTO_VENDOR_ID;
  VersionInfoPtr->moduleID = CRYPTO_MODULE_ID;
  VersionInfoPtr->sw_major_version = CRYPTO_SW_MAJOR_VERSION;
  VersionInfoPtr->sw_minor_version = CRYPTO_SW_MINOR_VERSION;
  VersionInfoPtr->sw_patch_version = CRYPTO_SW_PATCH_VERSION;
}
#endif
  /* CRYPTO_VERSION_INFO_API */

/**********************************************************************************************************************
 *  Crypto_HmacMd5Init
 *********************************************************************************************************************/
/*! \brief      Initialize hmac MD5 calculation
 *  \param[in]  HmacStorePtr     pointer for the temporary data
 *  \param[in]  KeyDataPtr       pointer to the key data
 *  \param[in]  KeyLenByte       key data length in bytes
 *  \return     void
 *  \note       none
 *  \context    task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_HmacMd5Init(
    P2VAR(Crypto_HmacMd5StoreType, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) HmacStorePtr,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) KeyDataPtr,
    uint32 KeyLenByte )
{
  uint32 Idx;

  esl_initWorkSpaceHeader( &(HmacStorePtr->Store.header), ESL_MAXSIZEOF_WS_MD5, NULL_PTR );

  /* adjust key length (fips-198a, Step1-3) */
  if ( CRYPTO_CODE_MD5_RAW_BLOCK_LEN < KeyLenByte )
  { /* fill with hashed key (shorten key) */
    esl_initMD5( &HmacStorePtr->Store );
    esl_updateMD5( &HmacStorePtr->Store, (uint16)KeyLenByte, &KeyDataPtr[0] );
    esl_finalizeMD5( &HmacStorePtr->Store, &HmacStorePtr->KeyDataLocIpad[0] );
    for ( Idx = CRYPTO_CODE_MD5_CODED_BLOCK_LEN; Idx < CRYPTO_CODE_MD5_RAW_BLOCK_LEN; Idx++ )
    { /* fill up with zeros */
      HmacStorePtr->KeyDataLocIpad[Idx] = 0;
    }
  }
  else
  { /* fill with raw key (full length) */
    for ( Idx = 0; Idx < KeyLenByte; Idx++ )
    { /* fill with key */
      HmacStorePtr->KeyDataLocIpad[Idx] = KeyDataPtr[Idx];
    }
    for ( Idx = KeyLenByte; Idx < CRYPTO_CODE_MD5_RAW_BLOCK_LEN; Idx++ )
    { /* fill up with zeros */
      HmacStorePtr->KeyDataLocIpad[Idx] = 0;
    }
  }
  /* prepare inner hash (K0 XOR ipad, hash ipad+src (fips-198a, Step4-6)) */
  for ( Idx = 0; Idx < CRYPTO_CODE_MD5_RAW_BLOCK_LEN; Idx++ )
  { /* create K0 with Ipad and Opad */
    HmacStorePtr->KeyDataLocOpad[Idx] = HmacStorePtr->KeyDataLocIpad[Idx] ^ CRYPTO_CODE_HMAC_OPAD;
    HmacStorePtr->KeyDataLocIpad[Idx] ^= CRYPTO_CODE_HMAC_IPAD;
  }
  esl_initMD5( &HmacStorePtr->Store );
  esl_updateMD5( &HmacStorePtr->Store, CRYPTO_CODE_MD5_RAW_BLOCK_LEN, &HmacStorePtr->KeyDataLocIpad[0] );
}

/**********************************************************************************************************************
 *  Crypto_HmacMd5Encode
 *********************************************************************************************************************/
/*! \brief      Proceed the hmac MD5 calculation
 *  \param[in]  HmacStorePtr     pointer for the temporary data
 *  \param[in]  SrcDataPtr       pointer to the raw data
 *  \param[in]  SrcLenByte       raw data length in bytes
 *  \return     void
 *  \note       none
 *  \context    task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_HmacMd5Encode(
    P2VAR(Crypto_HmacMd5StoreType, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) HmacStorePtr,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) SrcDataPtr,
    uint32 SrcLenByte )
{
  /* update inner hash (K0 XOR ipad, hash ipad+src (fips-198a, Step4-6)) */
  esl_updateMD5( &HmacStorePtr->Store, (uint16)SrcLenByte, &SrcDataPtr[0] );
}

/**********************************************************************************************************************
 *  Crypto_HmacMd5End
 *********************************************************************************************************************/
/*! \brief      Finalize the hmac MD5 calculation
 *  \param[in]  HmacStorePtr     pointer for the temporary data
 *  \param[in]  TgtDataPtr       pointer for the decoded data
 *  \return     void
 *  \note       none
 *  \context    task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_HmacMd5End(
    P2VAR(Crypto_HmacMd5StoreType, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) HmacStorePtr,
    P2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) TgtDataPtr )
{
  esl_finalizeMD5( &HmacStorePtr->Store, &TgtDataPtr[0] );

  /* perform outer hash (K0 XOR opad, inner digest (fips-198a, Step6-9)) */
  esl_initMD5( &HmacStorePtr->Store );
  esl_updateMD5( &HmacStorePtr->Store, CRYPTO_CODE_MD5_RAW_BLOCK_LEN, &HmacStorePtr->KeyDataLocOpad[0] );
  esl_updateMD5( &HmacStorePtr->Store, CRYPTO_CODE_MD5_CODED_BLOCK_LEN, &TgtDataPtr[0] );
  esl_finalizeMD5( &HmacStorePtr->Store, &TgtDataPtr[0] );
}

/**********************************************************************************************************************
 *  Crypto_HmacSha256Init
 *********************************************************************************************************************/
/*! \brief      Initialize hmac SHA256 calculation
 *  \param[in]  HmacStorePtr     pointer for the temporary data
 *  \param[in]  KeyDataPtr       pointer to the key data
 *  \param[in]  KeyLenByte       key data length in bytes
 *  \return     void
 *  \note       none
 *  \context    task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_HmacSha256Init(
    P2VAR(Crypto_HmacSha256StoreType, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) HmacStorePtr,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) KeyDataPtr,
    uint32 KeyLenByte )
{
  uint32 Idx;

  esl_initWorkSpaceHeader( &(HmacStorePtr->Store.header), ESL_MAXSIZEOF_WS_SHA256, NULL_PTR );

  /* adjust key length (fips-198a, Step1-3) */
  if ( CRYPTO_CODE_SHA256_RAW_BLOCK_LEN < KeyLenByte )
  { /* fill with hashed key (shorten key) */
    esl_initSHA256( &HmacStorePtr->Store );
    esl_updateSHA256( &HmacStorePtr->Store, (uint16)KeyLenByte, &KeyDataPtr[0] );
    esl_finalizeSHA256( &HmacStorePtr->Store, &HmacStorePtr->KeyDataLocIpad[0] );
    for ( Idx = CRYPTO_CODE_SHA256_CODED_BLOCK_LEN; Idx < CRYPTO_CODE_SHA256_RAW_BLOCK_LEN; Idx++ )
    { /* fill up with zeros */
      HmacStorePtr->KeyDataLocIpad[Idx] = 0;
    }
  }
  else
  { /* fill with raw key (full length) */
    for ( Idx = 0; Idx < KeyLenByte; Idx++ )
    { /* fill with key */
      HmacStorePtr->KeyDataLocIpad[Idx] = KeyDataPtr[Idx];
    }
    for ( Idx = KeyLenByte; Idx < CRYPTO_CODE_SHA256_RAW_BLOCK_LEN; Idx++ )
    { /* fill up with zeros */
      HmacStorePtr->KeyDataLocIpad[Idx] = 0;
    }
  }
  /* prepare inner hash (K0 XOR ipad, hash ipad+src (fips-198a, Step4-6)) */
  for ( Idx = 0; Idx < CRYPTO_CODE_SHA256_RAW_BLOCK_LEN; Idx++ )
  { /* create K0 with Ipad and Opad */
    HmacStorePtr->KeyDataLocOpad[Idx] = HmacStorePtr->KeyDataLocIpad[Idx] ^ CRYPTO_CODE_HMAC_OPAD;
    HmacStorePtr->KeyDataLocIpad[Idx] ^= CRYPTO_CODE_HMAC_IPAD;
  }
  esl_initSHA256( &HmacStorePtr->Store );
  esl_updateSHA256( &HmacStorePtr->Store, CRYPTO_CODE_SHA256_RAW_BLOCK_LEN, &HmacStorePtr->KeyDataLocIpad[0] );
}

/**********************************************************************************************************************
 *  Crypto_HmacSha256Encode
 *********************************************************************************************************************/
/*! \brief      Proceed the hmac SHA256 calculation
 *  \param[in]  HmacStorePtr     pointer for the temporary data
 *  \param[in]  SrcDataPtr       pointer to the raw data
 *  \param[in]  SrcLenByte       raw data length in bytes
 *  \return     void
 *  \note       none
 *  \context    task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_HmacSha256Encode(
    P2VAR(Crypto_HmacSha256StoreType, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) HmacStorePtr,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) SrcDataPtr,
    uint32 SrcLenByte )
{
  /* update inner hash (K0 XOR ipad, hash ipad+src (fips-198a, Step4-6)) */
  esl_updateSHA256( &HmacStorePtr->Store, (uint16)SrcLenByte, &SrcDataPtr[0] );
}

/**********************************************************************************************************************
 *  Crypto_HmacSha256End
 *********************************************************************************************************************/
/*! \brief      Finalize the hmac SHA256 calculation
 *  \param[in]  HmacStorePtr     pointer for the temporary data
 *  \param[in]  TgtDataPtr       pointer for the decoded data
 *  \return     void
 *  \note       none
 *  \context    task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_HmacSha256End(
    P2VAR(Crypto_HmacSha256StoreType, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) HmacStorePtr,
    P2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) TgtDataPtr )
{
  esl_finalizeSHA256( &HmacStorePtr->Store, &TgtDataPtr[0] );

  /* perform outer hash (K0 XOR opad, inner digest (fips-198a, Step6-9)) */
  esl_initSHA256( &HmacStorePtr->Store );
  esl_updateSHA256( &HmacStorePtr->Store, CRYPTO_CODE_SHA256_RAW_BLOCK_LEN, &HmacStorePtr->KeyDataLocOpad[0] );
  esl_updateSHA256( &HmacStorePtr->Store, CRYPTO_CODE_SHA256_CODED_BLOCK_LEN, &TgtDataPtr[0] );
  esl_finalizeSHA256( &HmacStorePtr->Store, &TgtDataPtr[0] );
}

#if( (defined CRYPTO_RSA_SUPPORT_ENABLED) && (STD_ON == CRYPTO_RSA_SUPPORT_ENABLED) )

/* signature verification ----------------------------------------------------------------------- */
/* signature verification ----------------------------------------------------------------------- */
/* signature verification ----------------------------------------------------------------------- */

/***********************************************************************************************************************
 *  Crypto_initVerifyRSAMD5_V15
 **********************************************************************************************************************/
/*! \brief        Initialize verification of RSA with MD5 digital signature
 *                <br>Functionality analog to esl_initVerifyRSASHA1_V15(() of cryptovision.
 *  \param[in]    workSpace                pointer to signature workspace
 *  \param[in]    keyPairModuleSize        size (byte) of RSA module
 *  \param[in]    keyPairModule            pointer to RSA module
 *  \param[in]    publicKeyExponentSize    size (byte) of RSA public exponent
 *  \param[in]    publicKeyExponent        pointer to RSA public exponent
 *  \return       ESL_ERC_NO_ERROR         initialization was successful
 *                <br>ESL_ERC_WS_TOO_SMALL     given workspace is too small
 *                <br>others                   other init errors
 *  \note         workspace has to be initialized before
 *  \context      task level
 **********************************************************************************************************************/
FUNC(eslt_ErrorCode, CRYPTO_CODE) Crypto_initVerifyRSAMD5_V15(
    P2VAR(eslt_WorkSpaceRSAMD5ver, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) workSpace,
    eslt_Length keyPairModuleSize,
    P2CONST(eslt_Byte, AUTOMATIC, CRYPTO_APPL_DATA) keyPairModule,
    eslt_Length publicKeyExponentSize,
    P2CONST(eslt_Byte, AUTOMATIC, CRYPTO_APPL_DATA) publicKeyExponent )
{
  eslt_ErrorCode            returnValue;
  eslt_Byte                 *wsRAW;
  eslt_WorkSpaceHeader      *wsHeaderV15;
  eslt_WorkSpaceMD5         *wsMD5;                                                                                     /* PRQA S 781 */ /* MD_CRYPTO_5.6_0781_well_known_names */
  eslt_WorkSpaceRSAver_prim *wsPRIM;

  returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* get underlying workspace pointer */
    wsRAW       = (eslt_Byte *)workSpace;                                                                               /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
    wsHeaderV15 = (eslt_WorkSpaceHeader *)wsRAW;                                                                        /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
    wsMD5       = (eslt_WorkSpaceMD5 *)(wsRAW + ESL_WS_RSA_V15_SV_MD5_WS_MD5(keyPairModuleSize));                       /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
    wsPRIM      = (eslt_WorkSpaceRSAver_prim *)(wsRAW + ESL_WS_RSA_V15_SV_MD5_WS_RSA_PRIM(keyPairModuleSize));          /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */

    if (wsHeaderV15->size < (ESL_SIZEOF_WS_RSA_V15_SV_MD5_OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)))
    {
      returnValue = ESL_ERC_WS_TOO_SMALL;
    }
    else
    {
      wsMD5->header.size = (eslt_Length)(sizeof(eslt_WorkSpaceMD5) - sizeof(eslt_WorkSpaceHeader));
      wsMD5->header.watchdog = wsHeaderV15->watchdog;
      returnValue = esl_initMD5(wsMD5);
      if (ESL_ERC_NO_ERROR == returnValue)
      {
        wsPRIM->header.size = (eslt_Length)(wsHeaderV15->size - (ESL_SIZEOF_WS_RSA_V15_SV_MD5_OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)));
        wsPRIM->header.watchdog = wsHeaderV15->watchdog;
        returnValue = esl_initVerifyRSA_prim(wsPRIM, keyPairModuleSize, keyPairModule, publicKeyExponentSize, publicKeyExponent);
      }
    }
    if (ESL_ERC_NO_ERROR == returnValue)
    {
      wsHeaderV15->status = (ESL_WST_ALGO_RSA | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);
      /* store message buffer length in workspace */
      wsRAW[ESL_WS_RSA_V15_SV_MD5_BUFFERLENGTH]     = (eslt_Byte)(keyPairModuleSize >> 8);
      wsRAW[ESL_WS_RSA_V15_SV_MD5_BUFFERLENGTH + 1] = (eslt_Byte)(keyPairModuleSize);
    }
  }
  return returnValue;
}

/***********************************************************************************************************************
 *  Crypto_updateVerifyRSAMD5_V15
 **********************************************************************************************************************/
/*! \brief        Update verification of RSA with MD5 digital signature
 *                <br>Functionality analog to esl_updateVerifyRSASHA1_V15(() of cryptovision.
 *  \param[in]    workSpace                pointer to signature workspace
 *  \param[in]    inputSize                size (byte) of input data
 *  \param[in]    input                    pointer to input data
 *  \return       ESL_ERC_NO_ERROR         update was successful
 *                <br>others               update was not successful
 *  \note         Crypto_initVerifyRSAMD5_V15() has to be executed before
 *  \context      task level
 **********************************************************************************************************************/
FUNC(eslt_ErrorCode, CRYPTO_CODE) Crypto_updateVerifyRSAMD5_V15(
    P2VAR(eslt_WorkSpaceRSAMD5ver, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) workSpace,
    eslt_Length inputSize,
    P2CONST(eslt_Byte, AUTOMATIC, CRYPTO_APPL_DATA) input )
{
  eslt_ErrorCode            returnValue;
  eslt_Byte                 *wsRAW;
  eslt_WorkSpaceHeader      *wsHeaderV15;
  eslt_Length               keyPairModuleSize;
  eslt_WorkSpaceMD5         *wsMD5;                                                                                     /* PRQA S 781 */ /* MD_CRYPTO_5.6_0781_well_known_names */

  returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* get underlying workspace pointer */
    wsRAW             = (eslt_Byte *)workSpace;                                                                         /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
    wsHeaderV15       = (eslt_WorkSpaceHeader *)wsRAW;                                                                  /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
    keyPairModuleSize = (eslt_Length)(((eslt_Length)wsRAW[ESL_WS_RSA_V15_SV_MD5_BUFFERLENGTH] << 8) + (wsRAW[ESL_WS_RSA_V15_SV_MD5_BUFFERLENGTH + 1]));
    wsMD5             = (eslt_WorkSpaceMD5 *)(wsRAW + ESL_WS_RSA_V15_SV_MD5_WS_MD5(keyPairModuleSize));                 /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */

    if (ESL_WST_ALGO_RSA != (wsHeaderV15->status & ESL_WST_M_ALGO))
    {
      returnValue = ESL_ERC_WS_STATE_INVALID;
    }
    else if (0 == (wsHeaderV15->status & ESL_WST_M_RUNNING))
    {
      returnValue = ESL_ERC_WS_STATE_INVALID;
    }
    else
    {
      returnValue = esl_updateMD5(wsMD5, inputSize, input);
    }
  }
  return returnValue;
}

/***********************************************************************************************************************
 *  Crypto_finalizeVerifyRSAMD5_V15
 **********************************************************************************************************************/
/*! \brief        Finalize verification of RSA with MD5 digital signature
 *                <br>Functionality analog to esl_finalizeVerifyRSASHA1_V15(() of cryptovision.
 *  \param[in]    workSpace                pointer to signature workspace
 *  \param[in]    signatureSize            max signature size
 *  \param[in]    signature                pointer to where the signature shall be stored
 *  \return       ESL_ERC_NO_ERROR         finalization was successful
 *                <br>others               finalization was not successful
 *  \note         Crypto_initSignRSAMD5_V15() has to be executed before
 *  \context      task level
 **********************************************************************************************************************/
FUNC(eslt_ErrorCode, CRYPTO_CODE) Crypto_finalizeVerifyRSAMD5_V15(
    P2VAR(eslt_WorkSpaceRSAMD5ver, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) workSpace,
    eslt_Length signatureSize,
    P2CONST(eslt_Byte, AUTOMATIC, CRYPTO_APPL_DATA) signature)
{
  eslt_ErrorCode            returnValue;
  eslt_Byte                 *wsRAW;
  eslt_WorkSpaceMD5         *wsMD5;                                                                                     /* PRQA S 781 */ /* MD_CRYPTO_5.6_0781_well_known_names */
  eslt_WorkSpaceRSAver_prim *wsPRIM;
  eslt_WorkSpaceHeader      *wsHeaderV15;
  eslt_Length               keyPairModuleSize, i, padBytesNeeded;  /* variable naming similar to CryptoCv-LIB */
  eslt_Byte                 *messageBuf;
  const eslt_Byte           eslt_ASN1_DIGESTINFO_MD5[] = ASN1_DIGESTINFO_MD5;

  returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    i                 = 0;
    /* get underlying workspace pointer */
    wsRAW             = (eslt_Byte *)workSpace;                                                                         /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
    wsHeaderV15       = (eslt_WorkSpaceHeader *)wsRAW;                                                                  /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
    keyPairModuleSize = (eslt_Length)(((eslt_Length)wsRAW[ESL_WS_RSA_V15_SV_MD5_BUFFERLENGTH] << 8) + (wsRAW[ESL_WS_RSA_V15_SV_MD5_BUFFERLENGTH + 1]));
    wsMD5             = (eslt_WorkSpaceMD5 *)(wsRAW + ESL_WS_RSA_V15_SV_MD5_WS_MD5(keyPairModuleSize));                 /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
    wsPRIM            = (eslt_WorkSpaceRSAver_prim *)(wsRAW + ESL_WS_RSA_V15_SV_MD5_WS_RSA_PRIM(keyPairModuleSize));    /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
    messageBuf        = wsRAW + ESL_WS_RSA_V15_SV_MD5_BUFFER;

    if (ESL_WST_ALGO_RSA != (wsHeaderV15->status & ESL_WST_M_ALGO))
    {
      returnValue = ESL_ERC_WS_STATE_INVALID;
    }
    else if (0 == (wsHeaderV15->status & ESL_WST_M_RUNNING))
    {
      returnValue = ESL_ERC_WS_STATE_INVALID;
    }
    else
    {
      padBytesNeeded = (eslt_Length)(keyPairModuleSize - ASN1_SIZEOF_PARAMETERS_WITH_FIXED_LENGTH_MD5);
      if (padBytesNeeded < ASN1_MINIMAL_PADDING_LENGTH)
      {
        returnValue = ESL_ERC_PARAMETER_INVALID;
      }
      else
      {
        returnValue = esl_verifyRSA_prim(wsPRIM, signatureSize, signature, &keyPairModuleSize, messageBuf);
        if (ESL_ERC_NO_ERROR == returnValue)
        {
          if ((signatureSize != keyPairModuleSize) || (0x00 != messageBuf[0]) || (0x01 != messageBuf[1]))
          {
            returnValue = ESL_ERC_RSA_SIGNATURE_INVALID;
          }
          else
          {
            i = 2;
            while ((0x00 != messageBuf[i]) && (i < (keyPairModuleSize - ESL_SIZEOF_MD5_DIGEST)))
            {
              i++;
            }
            if (0x00 != messageBuf[i])
            {
              returnValue = ESL_ERC_RSA_SIGNATURE_INVALID;
            }
            else
            {
              i++;
              if (ASN1_SIZEOF_MINIMAL_PADDING > i)
              {
                returnValue = ESL_ERC_RSA_SIGNATURE_INVALID;
              }
              else
              {
                if (0 != actMemcmp((const actU8 *)eslt_ASN1_DIGESTINFO_MD5, (const actU8 *)(&messageBuf[i]), 18))
                {
                  returnValue = ESL_ERC_RSA_SIGNATURE_INVALID;
                }
                else
                {
                  i += 18;
                  returnValue = esl_finalizeMD5(wsMD5, messageBuf);
                  if (ESL_ERC_NO_ERROR == returnValue)
                  {
                    if (0 != actMemcmp((const actU8 *)messageBuf, (const actU8 *)(&messageBuf[i]), ESL_SIZEOF_MD5_DIGEST))
                    {
                      returnValue = ESL_ERC_RSA_SIGNATURE_INVALID;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return returnValue;
}

/* signature generation generalized ------------------------------------------------------------- */
/* signature generation generalized ------------------------------------------------------------- */
/* signature generation generalized ------------------------------------------------------------- */

/* signature generation function gets following parameters:
   - workspace pointer
   - RSA key elements P, Q, DP, DQ, QI pointer and length
   - calculated message hash pointer and lenght (e.g. sha1 OR md5)
   - pointer to signature
*/
/* derived from function using RSA and MD5 */

/***********************************************************************************************************************
 *  Crypto_SignRSACRTgen_V15
 **********************************************************************************************************************/
/*! \brief         Calculate RSA digital signature using a pre-calculated hash value
 *                 <br>Functionality analog to esl_initSignRSACRTSHA1_V15(), esl_updateSignRSACRTSHA1_V15() and
 *                 esl_finalizeSignRSACRTSHA1_V15() of cryptovision.
 *  \param[in]     workSpace                  pointer to signature workspace
 *  \param[in]     keyPairPrimePSize          size (byte) of RSA key parameter P
 *  \param[in]     keyPairPrimeP              pointer to RSA key parameter P
 *  \param[in]     keyPairPrimeQSize          size (byte) of RSA key parameter Q
 *  \param[in]     keyPairPrimeQ              pointer to RSA key parameter Q
 *  \param[in]     privateKeyExponentDPSize   size (byte) of RSA key parameter DP
 *  \param[in]     privateKeyExponentDP       pointer to RSA key parameter DP
 *  \param[in]     privateKeyExponentDQSize   size (byte) of RSA key parameter DQ
 *  \param[in]     privateKeyExponentDQ       pointer to RSA key parameter DQ
 *  \param[in]     privateKeyInverseQISize    size (byte) of RSA key parameter QI
 *  \param[in]     privateKeyInverseQI        pointer to RSA key parameter QI
 *  \param[in]     hashSize                   size (byte) of input / hash
 *  \param[in]     hashPtr                    pointer to input / hash
 *  \param[in,out] signatureSize              max signature size
 *  \param[in]     signature                  pointer to where the signature shall be stored
 *  \return        ESL_ERC_NO_ERROR           initialization was successful
 *                 <br>ESL_ERC_WS_TOO_SMALL   given workspace is too small
 *                 <br>others                 other init errors
 *  \note          Workspace has to be initialized before. Derived from function using RSA and MD5.
 *  \context       task level
 **********************************************************************************************************************/
FUNC(eslt_ErrorCode, CRYPTO_CODE) Crypto_SignRSACRTgen_V15(
    P2VAR(eslt_WorkSpaceRSACRTsig, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) workSpace,
    uint16                                                 keyPairPrimePSize,
    CONSTP2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)  keyPairPrimeP,
    uint16                                                 keyPairPrimeQSize,
    CONSTP2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)  keyPairPrimeQ,
    uint16                                                 privateKeyExponentDPSize,
    CONSTP2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)  privateKeyExponentDP,
    uint16                                                 privateKeyExponentDQSize,
    CONSTP2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)  privateKeyExponentDQ,
    uint16                                                 privateKeyInverseQISize,
    CONSTP2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)  privateKeyInverseQI,
    uint16                                                 hashSize,
    CONSTP2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)  hashPtr,
    P2VAR(uint16, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)      signatureSize,
    CONSTP2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)  signature )
{
  eslt_ErrorCode            returnValue;
  eslt_Byte                 *wsRAW;
  eslt_WorkSpaceHeader      *wsHeaderV15;
  eslt_Length               keyPairModuleSize;
  /* wsSHA1 will stay unused */
  eslt_WorkSpaceRSACRTsig_prim *wsPRIM;

  returnValue = ESL_ERC_NO_ERROR;
  wsRAW       = (eslt_Byte *)workSpace;                                                                                 /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
  wsHeaderV15 = (eslt_WorkSpaceHeader *)wsRAW;                                                                          /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* get underlying workspace pointer */
    keyPairModuleSize = (eslt_Length)(keyPairPrimePSize + keyPairPrimeQSize);
    wsPRIM            = (eslt_WorkSpaceRSACRTsig_prim *)(wsRAW + ESL_WS_RSA_V15_SV_SHA1_WS_RSA_PRIM(keyPairModuleSize));  /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */

    if (wsHeaderV15->size < (ESL_SIZEOF_WS_RSA_V15_SV_SHA1_OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)))
    {
      returnValue = ESL_ERC_WS_TOO_SMALL;
    }
    else
    {
      if (keyPairModuleSize < CRYPTO_RSA_KEY_PAIR_MODULE_SIZE_MIN)
      {
        returnValue = ESL_ERC_RSA_MODULE_OUT_OF_RANGE;
      }
      else
      {
        wsPRIM->header.size = (eslt_Length)(wsHeaderV15->size - (ESL_SIZEOF_WS_RSA_V15_SV_SHA1_OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)));
        wsPRIM->header.watchdog = wsHeaderV15->watchdog;
        returnValue =  esl_initSignRSACRT_prim(wsPRIM,
                                               keyPairPrimePSize, keyPairPrimeP,
                                               keyPairPrimeQSize, keyPairPrimeQ,
                                               privateKeyExponentDPSize, privateKeyExponentDP,
                                               privateKeyExponentDQSize, privateKeyExponentDQ,
                                               privateKeyInverseQISize, privateKeyInverseQI);
      }
    }
    if (ESL_ERC_NO_ERROR == returnValue)
    {
      wsHeaderV15->status = (ESL_WST_ALGO_RSA | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);
      /* store message buffer length in workspace */
      wsRAW[ESL_WS_RSA_V15_SV_SHA1_BUFFERLENGTH]     = (eslt_Byte)(keyPairModuleSize >> 8);
      wsRAW[ESL_WS_RSA_V15_SV_SHA1_BUFFERLENGTH + 1] = (eslt_Byte)(keyPairModuleSize);
    }
  }


  /* END OF INIT --------------------------------------------------------------- */

  /* START OF UPDATE ----------------------------------------------------------- */
  /* nothing to be done */
  /* END OF UPDATE ------------------------------------------------------------- */

  /* START OF FINALIZE --------------------------------------------------------- */

  if(ESL_ERC_NO_ERROR == returnValue)
  {
    eslt_Length        i, padBytesNeeded;
    eslt_Byte          *messageBuf;

    if (!workSpace)
    {
      returnValue = ESL_ERC_PARAMETER_INVALID;
    }
    else
    {
      i                 = 0;
      /* get underlying workspace pointer */
      keyPairModuleSize = (eslt_Length)(((eslt_Length)wsRAW[ESL_WS_RSA_V15_SV_SHA1_BUFFERLENGTH] << 8) + (wsRAW[ESL_WS_RSA_V15_SV_SHA1_BUFFERLENGTH + 1]));
      wsPRIM            = (eslt_WorkSpaceRSACRTsig_prim *)(wsRAW + ESL_WS_RSA_V15_SV_SHA1_WS_RSA_PRIM(keyPairModuleSize));  /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
      messageBuf        = wsRAW + ESL_WS_RSA_V15_SV_SHA1_BUFFER;
      keyPairModuleSize = (eslt_Length)( ((actRSACRTSTRUCT*)wsPRIM->wsRSA)->n_bytes );                                  /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
      padBytesNeeded    = (eslt_Length)(keyPairModuleSize - ((hashSize)+3));

      if (ESL_WST_ALGO_RSA != (wsHeaderV15->status & ESL_WST_M_ALGO))
      {
        returnValue = ESL_ERC_WS_STATE_INVALID;
      }
      else if (0 == (wsHeaderV15->status & ESL_WST_M_RUNNING))
      {
        returnValue = ESL_ERC_WS_STATE_INVALID;
      }
      else if (keyPairModuleSize < (ASN1_MINIMAL_SIGVER_KEY_LENGTH_NULL + hashSize))  /* modified for external hash */
      {
        returnValue = ESL_ERC_PARAMETER_INVALID;
      }
      else
      {
        messageBuf[i++] = 0x00;
        messageBuf[i++] = 0x01;
        if (padBytesNeeded < ASN1_MINIMAL_PADDING_LENGTH)
        {
          returnValue = ESL_ERC_PARAMETER_INVALID;
        }
        else
        {
          actMemset((actU8 *)(messageBuf + i), 0xff, (unsigned int) padBytesNeeded);                                    /* PRQA S 5013 */ /* MD_CRYPTO_6.3_5013_basic_types_for_ext_API */
          i = (eslt_Length)(padBytesNeeded + i);
          messageBuf[i++] = 0x00;
          actMemcpy((actU8 *)(messageBuf + i), (const actU8 *)hashPtr, hashSize);  /* modified for external hash */

          returnValue = esl_signRSACRT_prim(wsPRIM, keyPairModuleSize, messageBuf, signatureSize, signature);
        }
      }
    }
  }
  return returnValue;
}

/**********************************************************************************************************************
 *  Crypto_ValidateRsaSignature
 **********************************************************************************************************************/
/*! \brief          This function is used to validate an RSA signature.
 *  \param[in]      RsaWorkSpPtr        Pointer to the Crypto workspace that is used for signature validation
 *  \param[in]      SignatureValuePtr   Pointer to the Signature value buffer
 *  \param[in]      PubKeyPtr           Pointer to the public key information that shall be used to verify this signature
 *  \param[in]      PubExpPtr           Pointer to the public exponent information that shall be used to verify this signature
 *  \param[in]      DigestValuePtr      Pointer to the digest value against which the signature shall be verified
 *  \param[in]      SignatureValueLen   Length of the signature
 *  \param[in]      PubKeyLen           Length of the public key
 *  \param[in]      PubExpLen           Length of the public exponent
 *  \param[in]      DigestValueLen      Length of the digest value
 *  \param[in]      DigestType          Type of the digest (sha1, sha256, md5)
 *  \return         E_OK                Signature validation finished successfully. The signature is valid.\n
 *                  <br>E_NOT_OK            Error during signature validation. The signature is invalid.\n
 *  \note           none
 *  \context        task level
 **********************************************************************************************************************/
FUNC(Std_ReturnType, CRYPTO_CODE) Crypto_ValidateRsaSignature(
    P2VAR(eslt_WorkSpaceRSAver, AUTOMATIC, CRYPTO_APPL_VAR) RsaWorkSpPtr,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) SignatureValuePtr,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) PubKeyPtr,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) PubExpPtr,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) DigestValuePtr,
    uint16 SignatureValueLen,
    uint16 PubKeyLen,
    uint16 PubExpLen,
    uint8 DigestValueLen,
    uint8 DigestType )
{
  Std_ReturnType RetValue;
  eslt_ErrorCode RetCv;

  /* DET checks */
  CRYPTO_CheckDetErrorReturnVoid((NULL_PTR != RsaWorkSpPtr), CRYPTO_API_ID_VALIDATE_RSA_SIGNATURE, CRYPTO_E_INV_POINTER);
  CRYPTO_CheckDetErrorReturnVoid((NULL_PTR != SignatureValuePtr), CRYPTO_API_ID_VALIDATE_RSA_SIGNATURE, CRYPTO_E_INV_POINTER);
  CRYPTO_CheckDetErrorReturnVoid((NULL_PTR != PubKeyPtr), CRYPTO_API_ID_VALIDATE_RSA_SIGNATURE, CRYPTO_E_INV_POINTER);
  CRYPTO_CheckDetErrorReturnVoid((NULL_PTR != PubExpPtr), CRYPTO_API_ID_VALIDATE_RSA_SIGNATURE, CRYPTO_E_INV_POINTER);
  CRYPTO_CheckDetErrorReturnVoid((NULL_PTR != DigestValuePtr), CRYPTO_API_ID_VALIDATE_RSA_SIGNATURE, CRYPTO_E_INV_POINTER);
  CRYPTO_CheckDetErrorReturnVoid((0 != PubKeyLen), CRYPTO_API_ID_VALIDATE_RSA_SIGNATURE, CRYPTO_E_INV_PARAM);
  CRYPTO_CheckDetErrorReturnVoid((0 != PubExpLen), CRYPTO_API_ID_VALIDATE_RSA_SIGNATURE, CRYPTO_E_INV_PARAM);
  CRYPTO_CheckDetErrorReturnVoid((0 != DigestValueLen), CRYPTO_API_ID_VALIDATE_RSA_SIGNATURE, CRYPTO_E_INV_PARAM);
  CRYPTO_CheckDetErrorReturnVoid((CRYPTO_HASH_ALGORITHM_NONE != DigestType), CRYPTO_API_ID_VALIDATE_RSA_SIGNATURE, CRYPTO_E_INV_PARAM);

  RetValue = E_NOT_OK;
  RetCv    = ESL_ERC_RSA_SIGNATURE_INVALID;

  if( (PubKeyLen != SignatureValueLen)  /* the public key needs to have the same length as the signuature value */ ||
      (PubKeyLen <= PubExpLen) )  /* the key needs to be longer than the exponent */
  {
    /* error */
    CRYPTO_CallDetReportError(CRYPTO_API_ID_VALIDATE_RSA_SIGNATURE, CRYPTO_E_INV_PARAM)
  }
  else
  {
    /* initialize Crypto workspace */
    (void)esl_initWorkSpaceHeader(&RsaWorkSpPtr->header, ESL_MAXSIZEOF_WS_RSA_VER, 0);

    /* verify signature */
    RetCv = Crypto_VerifyRSAComplete(RsaWorkSpPtr, DigestValueLen, DigestValuePtr, DigestType, PubKeyLen, PubKeyPtr,
      PubExpLen, PubExpPtr, SignatureValueLen, SignatureValuePtr);
    if(ESL_ERC_NO_ERROR == RetCv)
    {
      /* signature is valid */
      RetValue = E_OK;
    }
  }

  return RetValue;
}

/**********************************************************************************************************************
 *  Crypto_VerifyRSAComplete
 **********************************************************************************************************************/
/*! \brief          This function is used to validate an RSA signature.
 *  \param[in]      workSpace               Pointer to the Crypto workspace that is used for signature validation
 *  \param[in]      DigestValueLen          Length of the digest value
 *  \param[in]      DigestValuePtr          Pointer to the digest value against which the signature shall be verified
 *  \param[in]      DigestType              Type of the digest (sha1, sha256, md5)
 *  \param[in]      keyPairModuleSize       Length of the public key
 *  \param[in]      keyPairModule           Pointer to the public key information that shall be used to verify this signature
 *  \param[in]      publicKeyExponentSize   Length of the public exponent
 *  \param[in]      publicKeyExponent       Pointer to the public exponent information that shall be used to verify this signature
 *  \param[in]      signatureSize           Length of the signature
 *  \param[in]      signature               Pointer to the Signature value buffer
 *  \return        ESL_ERC_NO_ERROR <br>
 *                 ESL_ERC_WS_TOO_SMALL <br>
 *                 ESL_ERC_WS_STATE_INVALID <br>
 *                 ESL_ERC_PARAMETER_INVALID <br>
 *                 ESL_ERC_RSA_SIGNATURE_OUT_OF_RANGE - 0 <= (int)signature <= modul-1, where module = PrimeP*PrimeQ <br>
 *                 ESL_ERC_RSA_ENCODING_INVALID <br>
 *                 ESL_ERC_RSA_PUBKEY_INVALID <br>
 *                 ESL_ERC_RSA_NO_RSA_WITH_SHA1_SIGNATURE <br>
 *                 ESL_ERC_RSA_SIGNATURE_INVALID
 *  \note           function derived from esl_initVerifyRSASHA1_V15, update and finish
 *  \context        task level
 **********************************************************************************************************************/
STATIC FUNC(eslt_ErrorCode, CRYPTO_CODE) Crypto_VerifyRSAComplete(
    P2VAR(eslt_WorkSpaceRSAver, AUTOMATIC, CRYPTO_APPL_VAR) workSpace,
    uint8                 DigestValueLen,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) DigestValuePtr,
    uint8                 DigestType,
    uint16                keyPairModuleSize,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) keyPairModule,
    uint16                publicKeyExponentSize,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) publicKeyExponent,
    uint16                signatureSize,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) signature)
{
  eslt_ErrorCode            returnValue;
  eslt_Byte                 *wsRAW;
  eslt_WorkSpaceHeader      *wsHeaderV15;
  eslt_WorkSpaceRSAver_prim *wsPRIM;

  eslt_Length               i;
  eslt_Length               padBytesNeeded;
  eslt_Byte                 *messageBuf;
  const eslt_Byte           eslt_ASN1_DIGESTINFO_MD5[]    = ASN1_DIGESTINFO_MD5;
  const eslt_Byte           eslt_ASN1_DIGESTINFO_SHA1[]   = ASN1_DIGESTINFO_SHA1;
  const eslt_Byte           eslt_ASN1_DIGESTINFO_SHA256[] = ASN1_DIGESTINFO_SHA256;

  returnValue = ESL_ERC_NO_ERROR;

  /* INIT (esl_initVerifyRSASHA1_V15) ---------------- */

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* get underlying workspace pointer */
    wsRAW       = (eslt_Byte *)workSpace;                                                                               /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
    wsHeaderV15 = (eslt_WorkSpaceHeader *)workSpace;                                                                    /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
    wsPRIM      = (eslt_WorkSpaceRSAver_prim *)(&wsRAW[ESL_WS_RSA_V15_SV_SHA1_WS_RSA_PRIM(keyPairModuleSize)]);         /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */

    if (wsHeaderV15->size < (ESL_SIZEOF_WS_RSA_V15_SV_SHA1_OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)))
    {
      returnValue = ESL_ERC_WS_TOO_SMALL;
    }
    else
    {
      wsPRIM->header.size = (eslt_Length)(wsHeaderV15->size - (ESL_SIZEOF_WS_RSA_V15_SV_SHA1_OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)));
      wsPRIM->header.watchdog = wsHeaderV15->watchdog;
      returnValue = esl_initVerifyRSA_prim(wsPRIM, keyPairModuleSize, keyPairModule, publicKeyExponentSize, publicKeyExponent);
    }
    if (ESL_ERC_NO_ERROR == returnValue)
    {
      wsHeaderV15->status = (ESL_WST_ALGO_RSA | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);
      /* store message buffer length in workspace */
      wsRAW[ESL_WS_RSA_V15_SV_SHA1_BUFFERLENGTH]     = (eslt_Byte)(keyPairModuleSize >> 8);
      wsRAW[ESL_WS_RSA_V15_SV_SHA1_BUFFERLENGTH + 1] = (eslt_Byte)(keyPairModuleSize);
    }
  }

  if (returnValue == ESL_ERC_NO_ERROR)
  {
    if (   (DigestType != CRYPTO_HASH_ALGORITHM_MD5)
        && (DigestType != CRYPTO_HASH_ALGORITHM_SHA1)
        && (DigestType != CRYPTO_HASH_ALGORITHM_SHA256))
    {
      /* DigestType has a not supported value */
      returnValue = ESL_ERC_PARAMETER_INVALID;
    }
  }

  /* UPDATE (esl_updateVerifyRSASHA1_V15) ------------ */

  /* update hash, nothing to be done here since hash is already available as precalculated input value */

  /* FINISH (esl_finishVerifyRSASHA1_V15) ------------ */

  if (returnValue == ESL_ERC_NO_ERROR)
  {
    i                 = 0;
    /* get underlying workspace pointer */
    wsRAW             = (eslt_Byte *)workSpace;                                                                         /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
    wsHeaderV15       = (eslt_WorkSpaceHeader *)wsRAW;                                                                  /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
    keyPairModuleSize = (eslt_Length)(((eslt_Length)wsRAW[ESL_WS_RSA_V15_SV_SHA1_BUFFERLENGTH] << 8) + (wsRAW[ESL_WS_RSA_V15_SV_SHA1_BUFFERLENGTH + 1]));
    wsPRIM            = (eslt_WorkSpaceRSAver_prim *)(&wsRAW[ESL_WS_RSA_V15_SV_SHA1_WS_RSA_PRIM(keyPairModuleSize)]);   /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */
    messageBuf        = wsRAW + ESL_WS_RSA_V15_SV_SHA1_BUFFER;

    if (ESL_WST_ALGO_RSA != (wsHeaderV15->status & ESL_WST_M_ALGO))
    {
      returnValue = ESL_ERC_WS_STATE_INVALID;
    }
    else if (0 == (wsHeaderV15->status & ESL_WST_M_RUNNING))
    {
      returnValue = ESL_ERC_WS_STATE_INVALID;
    }
    else
    {
      padBytesNeeded = (eslt_Length)(keyPairModuleSize - ASN1_SIZEOF_PARAMETERS_WITH_FIXED_LENGTH);
      if (padBytesNeeded < ASN1_MINIMAL_PADDING_LENGTH)
      {
        returnValue = ESL_ERC_PARAMETER_INVALID;
      }
      else
      {
        returnValue = esl_verifyRSA_prim(wsPRIM, signatureSize, signature, &keyPairModuleSize, messageBuf);
        if (ESL_ERC_NO_ERROR == returnValue) {
          if ((signatureSize != keyPairModuleSize) || (0x00 != messageBuf[0]) || (0x01 != messageBuf[1]))
          {
            returnValue = ESL_ERC_RSA_SIGNATURE_INVALID;
          }
          else
          {
            i = 2;
            while ((0x00 != messageBuf[i]) && (i < (keyPairModuleSize - DigestValueLen)))
            {
              i++;
            }
            if (0x00 != messageBuf[i])
            {
              returnValue = ESL_ERC_RSA_SIGNATURE_INVALID;
            }
            else
            {
              i++;
              if (ASN1_SIZEOF_MINIMAL_PADDING > i)
              {
                returnValue = ESL_ERC_RSA_SIGNATURE_INVALID;
              }
              else
              {
                const uint8 *DigestInfoPtr;
                uint8 DigestInfoLen;
                switch(DigestType)
                {
                case CRYPTO_HASH_ALGORITHM_MD5:
                  DigestInfoPtr = eslt_ASN1_DIGESTINFO_MD5;
                  DigestInfoLen = CRYPTO_ASN1_DIGESTINFO_MD5_LEN;
                  break;
                case CRYPTO_HASH_ALGORITHM_SHA1:
                  DigestInfoPtr = eslt_ASN1_DIGESTINFO_SHA1;
                  DigestInfoLen = CRYPTO_ASN1_DIGESTINFO_SHA1_LEN;
                  break;
                case CRYPTO_HASH_ALGORITHM_SHA256:
                  DigestInfoPtr = eslt_ASN1_DIGESTINFO_SHA256;
                  DigestInfoLen = CRYPTO_ASN1_DIGESTINFO_SHA256_LEN;
                  break;
                default:
                  /* error */
                  returnValue = ESL_ERC_RSA_SIGNATURE_INVALID;
                }
                if(ESL_ERC_NO_ERROR == returnValue)
                {
                  if (0 != actMemcmp((const actU8 *)DigestInfoPtr, (const actU8 *)(messageBuf + i), (unsigned int)DigestInfoLen))  /* PRQA S 5013 */ /* MD_CRYPTO_6.3_5013_basic_types_for_ext_API */
                  {
                    returnValue = ESL_ERC_RSA_SIGNATURE_INVALID;
                  }
                  else
                  {
                    i += DigestInfoLen;

                    if (0 != actMemcmp((const actU8 *)DigestValuePtr, (const actU8 *)(messageBuf + i), DigestValueLen))
                    {
                      returnValue = ESL_ERC_RSA_SIGNATURE_INVALID;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return returnValue;
}

#endif  /* RSA support */

#if( (defined CRYPTO_ECC_SUPPORT_ENABLED) && (STD_ON == CRYPTO_ECC_SUPPORT_ENABLED) )
/**********************************************************************************************************************
 *  Crypto_GenerateEcdsaSignature
 **********************************************************************************************************************/
/*! \brief          This function is used to generate a ECDSA signature using ANSIp256r1 curve.
 *  \param[in]      EcWorkSpPtr         Pointer to the Crypto workspace that is used for signature generation
 *  \param[in,out]  SignatureValuePtr   Pointer to the Signature value buffer
 *  \param[in]      PrivKeyPtr          Pointer to the private key information that shall be used for this signature
 *  \param[in]      DigestValuePtr      Pointer to the digest value that is used signed
 *  \param[in,out]  SignatureValueLen   Pointer to the length of the signature, shall be maximum buffer size when calling
 *                                      this function and will hold the generated signature value length when returning
 *  \param[in]      PrivKeyLen          Length of the private key
 *  \param[in]      DigestValueLen      Length of the digest value
 *  \param[in]      BerEncoded          Signature Value is BER encoded
 *  \return         E_OK                Signature generated successfully and stored at SignatureValuePtr with SignatureValueLen\n
 *                  <br>E_NOT_OK            Error during signature generation. No signature value is available.\n
 *  \note           none
 *  \context        task level
 **********************************************************************************************************************/
FUNC(Std_ReturnType, CRYPTO_CODE) Crypto_GenerateEcdsaSignature(
  P2VAR(eslt_WorkSpaceEcP, AUTOMATIC, CRYPTO_APPL_VAR) EcWorkSpPtr,
  P2VAR(uint8, AUTOMATIC, CRYPTO_APPL_VAR) SignatureValuePtr,
  P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) PrivKeyPtr,
  P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) DigestValuePtr,
  P2VAR(uint16, AUTOMATIC, CRYPTO_APPL_VAR) SignatureValueLen,
  uint16 PrivKeyLen,
  uint8 DigestValueLen,
  boolean BerEncoded)
{
  Std_ReturnType RetValue = E_NOT_OK;
  P2CONST(eslt_EccDomain, AUTOMATIC, CRYPTO_APPL_DATA)    EcDomainPtr     = &Crypto_EccCurveANSIp256r1Domain[0];
  P2CONST(eslt_EccDomainExt, AUTOMATIC, CRYPTO_APPL_DATA) EcDomainExtPtr  = &Crypto_EccCurveANSIp256r1DomainExt[0];
  P2CONST(eslt_EccDomainExt, AUTOMATIC, CRYPTO_APPL_DATA) EcSpeedUpExtPtr = &Crypto_EccCurveANSIp256r1SpeedUpExt[0];

  /* initialize Crypto workspace */
  if (   (ESL_ERC_NO_ERROR == esl_initWorkSpaceHeader(&EcWorkSpPtr->header, ESL_MAXSIZEOF_WS_ECP, 0))                   /* PRQA S 3415 */ /* MD_Crypto_12.4_ReadOnly */
      && (ESL_ERC_NO_ERROR == esl_initSignDSAEcP_prim(EcWorkSpPtr, EcDomainPtr, EcDomainExtPtr, EcSpeedUpExtPtr)) )
  {
    eslt_ErrorCode RetCv = ESL_ERC_ECC_PRIVKEY_INVALID;
    uint16 SignCompLen = esl_getLengthOfEcPsignature_comp(EcDomainPtr);
    uint16 SignTotalLen = 2*(SignCompLen);
    P2VAR(uint8, AUTOMATIC, CRYPTO_APPL_VAR) SignRPtr;
    P2VAR(uint8, AUTOMATIC, CRYPTO_APPL_VAR) SignSPtr;
    if(TRUE == BerEncoded)
    {
      SignTotalLen += 4;
    }

    if(TRUE == BerEncoded)
    {
      /* signature value needs to be BER encoded. Length of BER encodes r and s values not known yet */
      /* zero byte will be added in case first byte >= 0x80, leading zero bytes will be deleted in case of 9 consecutive 0-Bits*/
      /* set pointer to worst case position, copy may be required */
      SignatureValuePtr[4] = 0x00;
      SignRPtr   = &SignatureValuePtr[5];
      SignatureValuePtr[4 + (SignCompLen + 3)] = 0x00;
      SignSPtr   = &SignatureValuePtr[5 + (SignCompLen + 3)];
    }
    else
    {
      SignRPtr   = &SignatureValuePtr[0];
      SignSPtr   = &SignatureValuePtr[SignCompLen];
    }

    /* Private key length needs to be 32 for Crypto lib -> add missing zeros */
    if(32 == PrivKeyLen)
    {
      RetCv = esl_signDSAEcP_prim(EcWorkSpPtr, DigestValueLen, DigestValuePtr, PrivKeyPtr,
      &SignCompLen, SignRPtr, &SignCompLen, SignSPtr);
    }
    else if(PrivKeyLen < 32)
    {
      /* leading zeros missing */
      uint8 PrivKey[32];
      VStdMemClr(&PrivKey[0], 32);
      VStdMemCpy(&PrivKey[32-PrivKeyLen], PrivKeyPtr, PrivKeyLen);
      RetCv = esl_signDSAEcP_prim(EcWorkSpPtr, DigestValueLen, DigestValuePtr, &PrivKey[0],
      &SignCompLen, SignRPtr, &SignCompLen, SignSPtr);
    }
    else
    {
      /* nothing to do, key is invalid */
    }
    if(ESL_ERC_NO_ERROR == RetCv)
    {
      if(TRUE == BerEncoded)
      {
        uint8_least count = 0;
        /* determin required r and s length */
        uint8_least rLength = (uint8_least)SignCompLen + 1;
        uint8_least sLength = (uint8_least)SignCompLen + 1;
        /* include leading zero byte -> will be removed in case signature does not start with a 1 Bit */
        SignRPtr--;
        SignSPtr--;
        /* BER encoded value shall not start with 9 leading zero bits */
        while(((0x00 == SignRPtr[0]) && (0x00 == (uint8)(SignRPtr[1] & 0x80))) && (rLength > 1))
        {
          SignRPtr = &SignRPtr[1];
          rLength--;
        }
        /* BER encoded value shall not start with 9 leading zero bits */
        while(((0x00 == SignSPtr[0]) && (0x00 == (uint8)(SignSPtr[1] & 0x80))) && (sLength > 1))
        {
          SignSPtr = &SignSPtr[1];
          sLength--;
        }
        SignTotalLen = (uint16)(rLength + sLength + 4);
        /* do some manual BER encoding */
        SignatureValuePtr[0] = 0x30; /* sequence */
        SignatureValuePtr[1] = (uint8)SignTotalLen;
        SignatureValuePtr[2] = 0x02;  /* integer */
        SignatureValuePtr[3] = (uint8)rLength;
        if(rLength < (uint16)(SignCompLen + 1))
        {
          /* remove leading zero bytes to avoid 9 consecutive 0 Bits */
          while(count < rLength)
          {
            SignatureValuePtr[4+count] = SignRPtr[count];
            count++;
          }
        }
        SignatureValuePtr[4+rLength] = 0x02;  /* integer */
        SignatureValuePtr[5+rLength] = (uint8)sLength;
        if((sLength < (uint16)(SignCompLen + 1)) || (rLength < (uint16)(SignCompLen + 1)))
        {
          /* remove leading zero bytes to avoid 9 consecutive 0 Bits */
          count = 0;
          while(count < sLength)
          {
            SignatureValuePtr[6+rLength+count] = SignSPtr[count];
            count++;
          }
        }
        /* set total length of the BER encoded signature value */
        *SignatureValueLen = 2 + SignTotalLen;
      }
      else
      {
        *SignatureValueLen = SignTotalLen;
      }
      RetValue = E_OK;
    }
  }
  return RetValue;
}

/**********************************************************************************************************************
 *  Crypto_ValidateEcdsaSignature
 **********************************************************************************************************************/
/*! \brief          This function is used to validate a ECDSA signature using ANSIp256r1 curve.
 *  \param[in]      EcWorkSpPtr         Pointer to the Crypto workspace that is used for signature validation
 *  \param[in]      SignatureValuePtr   Pointer to the Signature value buffer
 *  \param[in]      PubKeyPtr           Pointer to the public key information that shall be used to verify this signature
 *  \param[in]      DigestValuePtr      Pointer to the digest value against which the signature shall be verified
 *  \param[in]      SignatureValueLen   Length of the signature
 *  \param[in]      PubKeyLen           Length of the public key
 *  \param[in]      DigestValueLen      Length of the digest value
 *  \param[in]      BerEncoded          Signature Value is BER encoded
 *  \return         E_OK                Signature validation finished successfully. The signature is valid.\n
 *                  <br>E_NOT_OK            Error during signature validation. The signature is invalid.\n
 *  \note           none
 *  \context        task level
 **********************************************************************************************************************/
FUNC(Std_ReturnType, CRYPTO_CODE) Crypto_ValidateEcdsaSignature(
  P2VAR(eslt_WorkSpaceEcP, AUTOMATIC, CRYPTO_APPL_VAR) EcWorkSpPtr,
  P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) SignatureValuePtr,
  P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) PubKeyPtr,
  P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) DigestValuePtr,
  uint16 SignatureValueLen,
  uint16 PubKeyLen,
  uint8 DigestValueLen,
  boolean BerEncoded)
{
  Std_ReturnType RetValue = E_NOT_OK;
  P2CONST(eslt_EccDomain, AUTOMATIC, CRYPTO_APPL_DATA)    EcDomainPtr     = &Crypto_EccCurveANSIp256r1Domain[0];
  P2CONST(eslt_EccDomainExt, AUTOMATIC, CRYPTO_APPL_DATA) EcDomainExtPtr  = &Crypto_EccCurveANSIp256r1DomainExt[0];
  P2CONST(eslt_EccDomainExt, AUTOMATIC, CRYPTO_APPL_DATA) EcSpeedUpExtPtr = &Crypto_EccCurveANSIp256r1SpeedUpExt[0];

  /* initialize Crypto workspace */
  if (   (ESL_ERC_NO_ERROR == esl_initWorkSpaceHeader(&EcWorkSpPtr->header, ESL_MAXSIZEOF_WS_ECP, 0))                   /* PRQA S 3415 */ /* MD_Crypto_12.4_ReadOnly */
      && (ESL_ERC_NO_ERROR == esl_initSignDSAEcP_prim(EcWorkSpPtr, EcDomainPtr, EcDomainExtPtr, EcSpeedUpExtPtr)))
  {
    eslt_ErrorCode RetCv = ESL_ERC_ECC_SIGNATURE_INVALID;
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) SignRPtr;
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) SignSPtr;
    uint8_least rLength;
    uint8_least sLength;
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) PubKeyXPtr;
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) PubKeyYPtr;
    uint8 EcPubKeyLen  = (uint8)esl_getLengthOfEcPpublicKey_comp(EcDomainPtr);

    /* check validity of public key */
    if (   (PubKeyLen == ((2 * EcPubKeyLen) + 1))
        && (PubKeyPtr[0] == 0x04)) /* uncompressed point */
    {
      /* public key contain x and y value */
      PubKeyXPtr = &PubKeyPtr[1];
      PubKeyYPtr = &PubKeyPtr[1 + EcPubKeyLen];

      if (TRUE == BerEncoded)
      {
        /* signature value is BER encoded */
        /* zero byte will be added in case first byte >= 0x80, leading zero bytes will be deleted in case of 9 consecutive 0-Bits */
        /* do some manual BER decoding */
        if (   (SignatureValueLen > 4)
            && (SignatureValuePtr[0] == 0x30) /* sequence */
            /* SignatureValuePtr[1] is the signature total length. this value is not evaluated here, only r and s length are taking into account later */
            && (SignatureValuePtr[2] == 0x02)) /* integer */
        {
          /* read r length and check total length */
          rLength = SignatureValuePtr[3];
          if (   (SignatureValueLen > (4 + rLength + 2))
              && (SignatureValuePtr[4 + rLength] == 0x02)) /* integer */
          {
            sLength = SignatureValuePtr[5 + rLength];
            /* get r and s value pointers, value first because rLength may be manupulated */
            if (0x21 == sLength)
            {
              SignSPtr = &SignatureValuePtr[7 + rLength];
              sLength--;
            }
            else
            {
              SignSPtr = &SignatureValuePtr[6 + rLength];
            }
            if (0x21 == rLength)
            {
              SignRPtr = &SignatureValuePtr[5];
              rLength--;
            }
            else
            {
              SignRPtr = &SignatureValuePtr[4];
            }
            /* verify signature */
            RetCv = esl_verifyDSAEcP_prim(EcWorkSpPtr, DigestValueLen, DigestValuePtr, PubKeyXPtr, PubKeyYPtr, (eslt_Length)rLength, SignRPtr, (eslt_Length)sLength, SignSPtr);
            if (ESL_ERC_NO_ERROR == RetCv)
            {
              /* signature is valid */
              RetValue = E_OK;
            }
          }
        }
      }
      else
      {
        rLength = (uint8)esl_getLengthOfEcPsignature_comp(EcDomainPtr);
        if (SignatureValueLen == (2 * rLength))
        {
          sLength = rLength;
          SignRPtr = &SignatureValuePtr[0];
          SignSPtr = &SignatureValuePtr[rLength];
          /* verify signature */
          RetCv = esl_verifyDSAEcP_prim(EcWorkSpPtr, DigestValueLen, DigestValuePtr, PubKeyXPtr, PubKeyYPtr, (eslt_Length)rLength, SignRPtr, (eslt_Length)sLength, SignSPtr);
          if (ESL_ERC_NO_ERROR == RetCv)
          {
            /* signature is valid */
            RetValue = E_OK;
          }
        }
      }
    }
  }

  return RetValue;
}
#endif

 /**********************************************************************************************************************
 *  Random Number Generator
 *********************************************************************************************************************/

 /**********************************************************************************************************************
 *  esl_initRNG
 *********************************************************************************************************************/
/*! \brief      initializes the random number generator
 *  \param[in]  workSpace           pointer to workspace for random generator
 *  \param[in]  entropyLength       The length of the given entropy used as seed for the RNG.
 *  \param[in]  entropy             pointer to the value that has to be used as seed.
 *  \param[in]  savedState          pointer to a state that has been saved after the previous initialisation. Length is
 *                                  given by ESL_SIZEOF_RNGFIPS186_STATE. If this pointer is set to NULL, no savedState
 *                                  is used.
 *  \return     ESL_ERC_NO_ERROR            initialization was successful
 *              <br>ESL_ERC_PARAMETER_INVALID   error: input parameter is NULL
 *              <br>ESL_ERC_WS_TOO_SMALL        error: work space too small
 *              <br>ESL_ERC_MODE_INVALID        error: block or padding mode is invalid
 *              <br>others                      other init errors
 *  \pre        the workspace has to be initialized before
 *  \context    task level
 *  \note       Function copied from CV (with minor adaptions)
 *********************************************************************************************************************/
FUNC(eslt_ErrorCode, CRYPTO_CODE) esl_initRNG(
    P2VAR(eslt_WorkSpaceRNG, CRYPTO_APPL_DATA, CRYPTO_APPL_VAR)  workSpace,
    CONST(eslt_Length, AUTOMATIC)                                entropyLength,
    P2CONST(eslt_Byte, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)       entropy,
    P2VAR(eslt_Byte, CRYPTO_APPL_DATA, CRYPTO_APPL_VAR)          savedState   )
{
   eslt_ErrorCode tmpERC;
   /* lockRNG(); */

   /* initialze static workspace pointer */
   Crypto_esl_WS_RNG = (eslt_WorkSpaceFIPS186*)workSpace;                                                               /* PRQA S 310 */ /* MD_CRYPTO_11.4_0310_ws */

   /* call FIPS186 core generator */
   tmpERC = esl_initFIPS186( Crypto_esl_WS_RNG, entropyLength, entropy, savedState);

   /* unlockRNG(); */
   return tmpERC;
}

/***********************************************************************************************************************
 *  esl_getBytesRNG
 **********************************************************************************************************************/
/*! \brief      Extern function needed by the ESLib of CV to get a random number
 *  \param[in]  targetLength        The length (byte) of the random number needed
 *  \param[in]  targetPtr           Pointer to the random number target array
 *  \return     ESL_ERC_NO_ERROR    random generation was successful
 *              <br>else                random generation was not successful
 *  \note       esl_initRNG() has to be executed before
 *  \context    task level
 **********************************************************************************************************************/
FUNC(eslt_ErrorCode, CRYPTO_CODE) esl_getBytesRNG(
    CONST(uint16, AUTOMATIC) targetLength,
    P2VAR(uint8, AUTOMATIC, CRYPTO_APPL_VAR) targetPtr )
{
  /*
    Parameter types used in this function are compatible to the CV lib.
    Original naming by CV:
    eslt_ErrorCode esl_getBytesRNG( const eslt_Length targetLength, eslt_Byte *target );
  */


  eslt_ErrorCode tmpERC;
  tmpERC = esl_getBytesFIPS186(Crypto_esl_WS_RNG, targetLength, targetPtr);
  return tmpERC;
}

/***********************************************************************************************************************
 *  Crypto_StirRNG
 **********************************************************************************************************************/
/*! \brief      Extern function needed by the ESLib of CV to get a random number
 *  \param[in]  InputLen            The length (byte) of the input data
 *  \param[in]  InputPtr            Pointer to the input data for stirring
 *  \return     E_OK                stirring was successfully <br>
 *              E_NOT_OK            stirring failed
 *  \note       The function esl_initRNG() has to be executed before. The minimum length of input data is 20 byte.
 *  \context    task level
 **********************************************************************************************************************/
FUNC(Std_ReturnType, CRYPTO_CODE) Crypto_StirRNG(
    CONST(uint16, AUTOMATIC) InputLen,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) InputPtr )
{
  Std_ReturnType RetValue;

  /* DET check */
  CRYPTO_CheckDetErrorReturnValue((NULL_PTR != InputPtr), CRYPTO_API_ID_STIR_RNG, CRYPTO_E_INV_POINTER, E_NOT_OK);

  if(InputLen >= 20)
  {
    /* The minimum input length is 20 byte since the eslib internally uses a SHA1. This length check is not done
       properly or the result is not handled in underlaying functions so it has to be done here separately */
    if(ESL_ERC_NO_ERROR == esl_stirFIPS186( Crypto_esl_WS_RNG, InputLen, (eslt_Byte*)InputPtr ))  /* PRQA S 311 */ /* MD_CRYPTO_11.5_0311_a */
    {
      RetValue = E_OK;
    }
    else
    {
      RetValue = E_NOT_OK;
    }
  }
  else
  {
    RetValue = E_NOT_OK;
  }

  return RetValue;
}

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

#define CRYPTO_STOP_SEC_CODE
#include "MemMap.h"
/* PRQA L:ASR_MEMMAP */

/* module specific MISRA deviations:
   MD_CRYPTO_5.6_0781_well_known_names:
      Reason:     Readability is increased by using well known names.
      Risk:       Elements can be confused with each other.
      Prevention: Covered by code review.
   MD_CRYPTO_6.3_5013_basic_types_for_ext_API:
      Reason:     Usage of basic types is defined by the external API
      Risk:       problem with different platforms
      Prevention: not possible
   MD_CRYPTO_8.7_3218:
     Reason:      Data is not defined within the funtion scope to increase the readibility of the function
                  implementation.
     Risk:        No functional risk.
     Prevention:  Not required.
   MD_CRYPTO_8.8_3447_external_linkage:
      Reason:     An external declaration for a foreign API is used because it is unknown who implementes this API
                  required for the Crypto module and therefore no corresponding include file can be used.
      Risk:       No functional risk.
      Prevention: Covered by code review.
   MD_CRYPTO_11.4_0310_ws:
      Reason:     Mapping an access pointer to a generic WorkSpace array
      Risk:       This might cause access problems on some platforms if elements are not aligned properly.
      Prevention: Covered by code review.
   MD_CRYPTO_11.5_0311_a:
      Reason:     Mapping a const pointer to a var pointer to adapt to the called API esl_stirFIPS186.
      Risk:       The called function might try to write to the given array although it is constant. But the called
                  function only forwards the data to another function that takes this pointer as a const pointer. So
                  this is a small inconsistency in the used library.
      Prevention: Covered by code review.
   MD_Crypto_12.4_ReadOnly: The right hand operand of a logical && or || operator shall not contain side effects.
      Reason:     No side effect possible because the function does not modify any variable.
                  It is intended that this function is only called if the first check fails (||) / succeeded (&&).
      Risk:       None.
      Prevention: n/a
*/

/**********************************************************************************************************************
 *  END OF FILE: Crypto.c
 *********************************************************************************************************************/
