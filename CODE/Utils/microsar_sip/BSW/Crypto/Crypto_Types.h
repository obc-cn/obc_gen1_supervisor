/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2018 by Vector Informatik GmbH.                                                  All rights reserved.
 * 
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  Crypto_Types.h
 *      Project:  MICROSAR IP
 *       Module:  MICROSAR Crypto
 *    Generator:  -
 *
 *  Description:  types header
 *  
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Crypto module. >> Crypto.h
 *********************************************************************************************************************/

#if !defined (CRYPTO_TYPES_H)
# define CRYPTO_TYPES_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Std_Types.h"
#include "ESLib_t.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
#define CRYPTO_CODE_MD5_RAW_BLOCK_LEN                    ((uint8) 64u)
#define CRYPTO_CODE_MD5_CODED_BLOCK_LEN                  ((uint8) 16u)
#define CRYPTO_CODE_SHA256_RAW_BLOCK_LEN                 ((uint8) 64u)
#define CRYPTO_CODE_SHA256_CODED_BLOCK_LEN               ((uint8) 32u)

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
typedef uint8     Crypto_StateType;

typedef struct {
  eslt_WorkSpaceMD5 Store;
  uint8            KeyDataLocIpad[CRYPTO_CODE_MD5_RAW_BLOCK_LEN];
  uint8            KeyDataLocOpad[CRYPTO_CODE_MD5_RAW_BLOCK_LEN];
} Crypto_HmacMd5StoreType;

typedef struct {
  eslt_WorkSpaceSHA256 Store;
  uint8            KeyDataLocIpad[CRYPTO_CODE_SHA256_RAW_BLOCK_LEN];
  uint8            KeyDataLocOpad[CRYPTO_CODE_SHA256_RAW_BLOCK_LEN];
} Crypto_HmacSha256StoreType;


#endif 
  /* CRYPTO_TYPES_H */
/**********************************************************************************************************************
 *  END OF FILE: Crypto_Types.h
 *********************************************************************************************************************/
