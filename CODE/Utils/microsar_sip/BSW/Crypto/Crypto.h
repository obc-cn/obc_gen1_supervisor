/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2018 by Vector Informatik GmbH.                                                  All rights reserved.
 * 
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  Crypto.h
 *      Project:  MICROSAR IP
 *       Module:  MICROSAR Crypto
 *    Generator:  -
 *
 *  Description:  module header
 *  
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  -------------------------------------------------------------------------------------------------------------------
 *  Thorsten Albers               visal         Vector Informatik GmbH
 *  Gunnar Meiss                  visms         Vector Informatik GmbH
 *  -------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  01.00.00  2010-10-01  visal   -             created
 *            2010-10-19  visal   -             some cryptovision files renamed (incl. corresponding includes)
 *            2010-10-19  visal   -             HmacMd5 functions moved here
 *  01.00.01  2010-11-12  visal   -             invalid include 'SchM_Crypto.h' removed
 *            2010-11-16  visal   ESCAN00046938 compiler error: double defined types with DiabData fixed
 *  01.00.02  2011-01-26  visal   -             signature handling using md5 hash algorithm implemented
 *  01.01.00  2011-03-08  visal   ESCAN00047170 Move random function to external file
 *  01.02.00  2011-05-06  visal   -             Implement HmacSha256 functions
 *            2011-06-08  visal   -             insert ECC curve paramters, generated with cryptovision tool
 *  01.02.01  2011-06-09  visal   -             include new version of the cryptovision library, patched by Vector
 *                                              add some new files (actDES.x, actRC2.x, actTDES.x, actBNMult2.c,
 *                                              ESLib_DESdec.c, ESLib_DESenc.c, ESLib_DESkey.c,
 *                                              ESLib_TDES.., ESLib_RC2..)
 *  01.02.02  2011-06-27  visal   ESCAN00051969 TABs can cause problems with some compilers, remove in CV and VI code
 *            2011-06-27  visal   -             cleanup comments
 *  01.02.03  2011-08-19  visal   -             cleanup comments, use cryptographic random generator
 *  01.02.04  2011-08-26  visal   -             add doxygen comments, move functions inside Crypto.c
 *  01.02.05  2011-09-23  visal   ESCAN00053821 change config to increase speed of crypto operations
 *            2011-09-27  visal   -             add function to calc RSA signature with P-Q parameters
 *  01.02.06  2011-10-27  visal   ESCAN00054533 Change ModuleId to 255 (CDD) and set InstanceId instead
 *            2011-10-27  visal   -             Add API Crypto_CheckInit
 *  01.02.07  2012-03-27  visal   -             rework doxygen comments
 *            2012-03-27  visal   -             remove GetVersionInfo makro API
 *            2012-03-29  visal   -             rework some compiler abstractions
 *  01.03.00  2012-05-14  visal   -             rename esl_WS_RNG to Crypto_esl_WS_RNG
 *            2012-05-14  visal   -             Add API Crypto_StirRNG to improve random number generation
 *            2012-06-13  visal   -             add option to disable all RSA handling / APIs
 *            2012-06-13  visal   -             removed unused RSA functions
 *  01.04.00  2012-09-24  visal   -             use global header history for _MemMap.inc and _Compiler_Cfg.inc
 *            2012-10-02  visal   ESCAN00061888 Prepare code for usage with ASR4
 *            2012-12-17  visdd   ESCAN00063765 Add generation and validation API for ECDSA signatures (curve ANSIp256r1)
 *            2013-02-06  visal   ESCAN00062247 AR4-220: Remove STATIC 
 *            2013-02-07  visal   -             Rework some compiler abstractions
 *  01.04.01  2013-07-11  visdd   ESCAN00068935 Add possibility to generate and validate raw ECDSA signatures
 *            2013-07-22  visal   -             Add CPU define V_CPU_FCR4 in file ESLib_platform_t.h
 *            2013-07-22  visal   -             Removed commented code segments, code cleanup, adapt doyxgen comments
 *            2013-07-22  visal   ESCAN00056366 VAR_INIT / VAR_ZERO_INIT Memory Mapping sections
 *            2013-07-25  visal   ESCAN00069325 Compiler error: error C2143 (syntax error) because of missing compiler abstraction
 *  01.04.02  2013-09-06  visalr  ESCAN00070283 ASR4 support: C_CPUTYPE_LITTLEENDIAN missing
 *  01.04.02  2013-10-11  visal   ESCAN00070022 Missing defines for Autosar4 configs
 *  01.04.03  2014-01-20  visal   ESCAN00071724 Crypto_SignRSACRTgen_V15() uses static hash size
 *            2014-01-20  visal   ESCAN00073079 Add uint64 typedef for platform TriCore
 *  01.04.04  2014-02-03  visal   ESCAN00073333 Compiler error: ESLib_platform_t.h: Current CPU type is not supported
 *                                                by the Vector Crypto module!
 *  01.04.05  2014-05-16  visal   ESCAN00075609 Certificate validation fails erroneous
 *            2014-05-21  visal   ESCAN00075809 Compiler error: GetVersionInfo symbol is not available
 *  01.04.06  2016-03-24  visal   ESCAN00088980 Crypto_StirRNG ignored when InputData smaller than 20 Bytes
 *            2016-03-24  visal   ESCAN00078501 Use uint64 typedef from Platform_Types.h
 *            2016-03-24  visal   ESCAN00089132 Remove extern declaration from functions
 *            2016-03-24  visal   ESCAN00089134 Move module history information to main header file
 *            2016-03-24  visal   ESCAN00089135 Rework code to take care of correct type conversions where necessary
 *  02.00.00  2016-04-21  visal   ESCAN00089644 Update and extend crypto library
 *  03.00.00  2016-07-25  visal   ESCAN00091237 FEAT-1583: Use of TLS and CAL/CSM/SecOC in one SIP
 *  03.01.00  2016-10-21  visal   ESCAN00092469 Implement function to verify RSA signature with hash value as input
 *            2016-10-24  visal   ESCAN00092049 Variable in ZERO_INIT section has no value assignment
 *  04.00.00  2018-04-11  visms   STORYC-4846   Remove support for v_cfg.h in Cfg5 systems
 *            2018-04-13  visal   -             Integrated review findings for v3.01.00
 *  04.00.01  2019-02-01  visal   ESCAN00101966 Wrong value of define SYSSERVICE_CRYPTO_VERSION
 *********************************************************************************************************************/

#if !defined (CRYPTO_H)
# define CRYPTO_H



/**********************************************************************************************************************
 * MISRA JUSTIFICATION
 *********************************************************************************************************************/
/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Crypto_Types.h"
#include "Crypto_Cfg.h"
#include "ESLib.h"

#include "Std_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
/* ##V_CFG_MANAGEMENT ##CQProject :   SYSSERVICE_CRYPTO CQComponent : Implementation */
#define SYSSERVICE_CRYPTO_VERSION                (0x0400u) /* BCD coded version number */
#define SYSSERVICE_CRYPTO_RELEASE_VERSION          (0x01u) /* BCD coded release version number */

/* Component Version Information */
#define CRYPTO_SW_MAJOR_VERSION                        4u
#define CRYPTO_SW_MINOR_VERSION                        0u
#define CRYPTO_SW_PATCH_VERSION                        1u

/* Crypto ModuleId */
#define CRYPTO_VENDOR_ID                                 30u   /* Vector ID */
#define CRYPTO_MODULE_ID                                255u   /* CDD - CRYPTO identified by InstanceId */
#define CRYPTO_INSTANCE_ID                              104u   /* InstanceId chosen by Vector */

/* Crypto ApiIds */
#define CRYPTO_API_ID_INIT                             0x01u
#define CRYPTO_API_ID_GET_VERSION_INFO                 0x02u
#define CRYPTO_API_ID_MAIN_FUNCTION                    0x03u
#define CRYPTO_API_ID_STIR_RNG                         0x04u
#define CRYPTO_API_ID_VALIDATE_RSA_SIGNATURE           0x05u

/* CRYPTO DET errors */
#define CRYPTO_E_NOT_INITIALIZED                       0x01u
#define CRYPTO_E_INV_POINTER                           0x02u
#define CRYPTO_E_INV_PARAM                             0x03u

/* CRYPTO state */
#define CRYPTO_STATE_UNINIT                            0x00u
#define CRYPTO_STATE_INIT                              0x01u


#define ESL_SIZEOF_WS_RSA_V15_SV_MD5_OVERHEAD_1024              ((((sizeof(eslt_WorkSpaceHeader) + sizeof(eslt_Length)) + 128u) + sizeof(eslt_WorkSpaceMD5)) + (4u*(ACT_MAX_ALIGN_OFFSET)))
#define ESL_SIZEOF_WS_RSA_V15_SV_MD5_OVERHEAD_2048              ((ESL_SIZEOF_WS_RSA_V15_SV_MD5_OVERHEAD_1024) + 128u)
#define ESL_SIZEOF_WS_RSA_V15_SV_MD5_HASH_OVERHEAD_2048   (ESL_SIZEOF_WS_RSA_V15_SV_MD5_OVERHEAD_2048)
#define ESL_MAXSIZEOF_WS_RSA_MD5_SIG                (ACT_ALIGN ((ESL_MAXSIZEOF_WS_RSA_SIG_PRIM)     + (ESL_SIZEOF_WS_RSA_V15_SV_MD5_HASH_OVERHEAD_2048)))
#define ESL_MAXSIZEOF_WS_RSA_MD5_VER                (ACT_ALIGN ((ESL_MAXSIZEOF_WS_RSA_VER_PRIM)     + (ESL_SIZEOF_WS_RSA_V15_SV_MD5_HASH_OVERHEAD_2048)))

typedef struct {
              eslt_WorkSpaceHeader header;
              eslt_Byte wsRSA[ ESL_MAXSIZEOF_WS_RSA_MD5_SIG];
      }eslt_WorkSpaceRSAMD5sig;
typedef struct {
              eslt_WorkSpaceHeader header;
              eslt_Byte wsRSA[ ESL_MAXSIZEOF_WS_RSA_MD5_VER];
      }eslt_WorkSpaceRSAMD5ver;

#define CRYPTO_ESL_SIZE_OF_WS_HEADER  (sizeof(eslt_WorkSpaceHeader))

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/
/* PRQA S 5087,3614 ASR_MEMMAP */ /* MISRA 19.1: AUTOSAR Memory Mapping */
#define CRYPTO_START_SEC_CONST_32BIT
#include "MemMap.h"

#if (CRYPTO_CV_BYTE_PER_DIGIT == 4)
extern const uint8 Crypto_EccCurveANSIp256r1Domain[227];
extern const uint8 Crypto_EccCurveANSIp256r1DomainExt[225];
extern const uint8 Crypto_EccCurveANSIp256r1SpeedUpExt[2155];
#else  /* (CRYPTO_CV_BYTE_PER_DIGIT == 2) */
extern const uint8 Crypto_EccCurveANSIp256r1Domain[227];
extern const uint8 Crypto_EccCurveANSIp256r1DomainExt[221];
extern const uint8 Crypto_EccCurveANSIp256r1SpeedUpExt[547];
#endif

#define CRYPTO_STOP_SEC_CONST_32BIT
#include "MemMap.h"

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
/* PRQA S 5087,3614 ASR_MEMMAP */ /* MISRA 19.1: AUTOSAR Memory Mapping */
#define CRYPTO_START_SEC_CODE
#include "MemMap.h"

/**********************************************************************************************************************
 *  Crypto_InitMemory
 *********************************************************************************************************************/
/*! \brief        Init internal module state variables
 *  \param[in]    void
 *  \return       void
 *  \note         Has to be called before any other calls to the module.
 *                <br>Not reentrant
 *  \context      system startup / task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_InitMemory(void);

/**********************************************************************************************************************
 *  Crypto_Init
 *********************************************************************************************************************/
/*! \brief        Initialization of the Crypto component.
 *  \param[in]    void
 *  \return       void
 *  \note         This function has to be called after Crypto_InitMemory() and before any other function of this module.
 *                <br>Not reentrant
 *  \context      system startup / task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_Init( void );

/**********************************************************************************************************************
 *  Crypto_CheckInit
 *********************************************************************************************************************/
/*! \brief        Check if Crypto_Init was called.
 *                <br>This is needed because most crypto functions are implemented by a third party and are called
 *                directly, so no DET-checks can be added here.
 *  \param[in]    void
 *  \return       TRUE           module has been initialized
 *                <br>FALSE      module has not been initialized
 *  \note         Has to be called before usage of the module
 *  \context      initialization
 *********************************************************************************************************************/
FUNC(boolean, CRYPTO_CODE) Crypto_CheckInit( void );

/**********************************************************************************************************************
 *  Crypto_GetVersionInfo
 *********************************************************************************************************************/
/*! \brief        Get Crypto software version.
 *                <br>Read the module id, the vendor id and the software implementation version.
 *  \param[in]    VersionInfoPtr        pointer for version information
 *  \return       void
 *  \note         none
 *  \context      task level
 *********************************************************************************************************************/
#if (CRYPTO_VERSION_INFO_API == STD_ON)
FUNC(void, CRYPTO_CODE) Crypto_GetVersionInfo(
    P2VAR(Std_VersionInfoType, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) VersionInfoPtr );
#endif
  /* (CRYPTO_VERSION_INFO_API == STD_ON) */

/**********************************************************************************************************************
 *  Crypto_HmacMd5Init
 *********************************************************************************************************************/
/*! \brief      Initialize hmac MD5 calculation
 *  \param[in]  HmacStorePtr     pointer for the temporary data
 *  \param[in]  KeyDataPtr       pointer to the key data
 *  \param[in]  KeyLenByte       key data length in bytes
 *  \return     void
 *  \note       none
 *  \context    task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_HmacMd5Init(
    P2VAR(Crypto_HmacMd5StoreType, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) HmacStorePtr,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) KeyDataPtr,
    uint32 KeyLenByte );

/**********************************************************************************************************************
 *  Crypto_HmacMd5Encode
 *********************************************************************************************************************/
/*! \brief      Proceed the hmac MD5 calculation
 *  \param[in]  HmacStorePtr     pointer for the temporary data
 *  \param[in]  SrcDataPtr       pointer to the raw data
 *  \param[in]  SrcLenByte       raw data length in bytes
 *  \return     void
 *  \note       none
 *  \context    task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_HmacMd5Encode(
    P2VAR(Crypto_HmacMd5StoreType, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) HmacStorePtr,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) SrcDataPtr,
    uint32 SrcLenByte );

/**********************************************************************************************************************
 *  Crypto_HmacMd5End
 *********************************************************************************************************************/
/*! \brief      Finalize the hmac MD5 calculation
 *  \param[in]  HmacStorePtr     pointer for the temporary data
 *  \param[in]  TgtDataPtr       pointer for the decoded data
 *  \return     void
 *  \note       none
 *  \context    task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_HmacMd5End(
    P2VAR(Crypto_HmacMd5StoreType, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) HmacStorePtr,
    P2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) TgtDataPtr );

/**********************************************************************************************************************
 *  Crypto_HmacSha256Init
 *********************************************************************************************************************/
/*! \brief      Initialize hmac SHA256 calculation
 *  \param[in]  HmacStorePtr     pointer for the temporary data
 *  \param[in]  KeyDataPtr       pointer to the key data
 *  \param[in]  KeyLenByte       key data length in bytes
 *  \return     void
 *  \note       none
 *  \context    task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_HmacSha256Init(
    P2VAR(Crypto_HmacSha256StoreType, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) HmacStorePtr,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) KeyDataPtr,
    uint32 KeyLenByte );

/**********************************************************************************************************************
 *  Crypto_HmacSha256Encode
 *********************************************************************************************************************/
/*! \brief      Proceed the hmac SHA256 calculation
 *  \param[in]  HmacStorePtr     pointer for the temporary data
 *  \param[in]  SrcDataPtr       pointer to the raw data
 *  \param[in]  SrcLenByte       raw data length in bytes
 *  \return     void
 *  \note       none
 *  \context    task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_HmacSha256Encode(
    P2VAR(Crypto_HmacSha256StoreType, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) HmacStorePtr,
    P2CONST(uint8, CRYPTO_APPL_DATA, CRYPTO_CONST) SrcDataPtr,
    uint32 SrcLenByte );

/**********************************************************************************************************************
 *  Crypto_HmacSha256End
 *********************************************************************************************************************/
/*! \brief      Finalize the hmac SHA256 calculation
 *  \param[in]  HmacStorePtr     pointer for the temporary data
 *  \param[in]  TgtDataPtr       pointer for the decoded data
 *  \return     void
 *  \note       none
 *  \context    task level
 *********************************************************************************************************************/
FUNC(void, CRYPTO_CODE) Crypto_HmacSha256End(
    P2VAR(Crypto_HmacSha256StoreType, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) HmacStorePtr,
    P2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) TgtDataPtr );

/* signature generation ----------------------------------------------------------------------- */
#if( (defined CRYPTO_RSA_SUPPORT_ENABLED) && (STD_ON == CRYPTO_RSA_SUPPORT_ENABLED) )

/* signature verification ----------------------------------------------------------------------- */

/***********************************************************************************************************************
 *  Crypto_initVerifyRSAMD5_V15
 **********************************************************************************************************************/
/*! \brief        Initialize verification of RSA with MD5 digital signature
 *                <br>Functionality analog to esl_initVerifyRSASHA1_V15(() of cryptovision.
 *  \param[in]    workSpace                pointer to signature workspace
 *  \param[in]    keyPairModuleSize        size (byte) of RSA module
 *  \param[in]    keyPairModule            pointer to RSA module
 *  \param[in]    publicKeyExponentSize    size (byte) of RSA public exponent
 *  \param[in]    publicKeyExponent        pointer to RSA public exponent
 *  \return       ESL_ERC_NO_ERROR         initialization was successful
 *                <br>ESL_ERC_WS_TOO_SMALL     given workspace is too small
 *                <br>others                   other init errors
 *  \note         workspace has to be initialized before
 *  \context      task level
 **********************************************************************************************************************/
FUNC(eslt_ErrorCode, CRYPTO_CODE) Crypto_initVerifyRSAMD5_V15(
    P2VAR(eslt_WorkSpaceRSAMD5ver, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) workSpace,
    eslt_Length keyPairModuleSize,
    P2CONST(eslt_Byte, AUTOMATIC, CRYPTO_APPL_DATA) keyPairModule,
    eslt_Length publicKeyExponentSize,
    P2CONST(eslt_Byte, AUTOMATIC, CRYPTO_APPL_DATA) publicKeyExponent );

/***********************************************************************************************************************
 *  Crypto_updateVerifyRSAMD5_V15
 **********************************************************************************************************************/
/*! \brief        Update verification of RSA with MD5 digital signature
 *                <br>Functionality analog to esl_updateVerifyRSASHA1_V15(() of cryptovision.
 *  \param[in]    workSpace                pointer to signature workspace
 *  \param[in]    inputSize                size (byte) of input data
 *  \param[in]    input                    pointer to input data
 *  \return       ESL_ERC_NO_ERROR         update was successful
 *                <br>others               update was not successful
 *  \note         Crypto_initVerifyRSAMD5_V15() has to be executed before
 *  \context      task level
 **********************************************************************************************************************/
FUNC(eslt_ErrorCode, CRYPTO_CODE) Crypto_updateVerifyRSAMD5_V15(
    P2VAR(eslt_WorkSpaceRSAMD5ver, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) workSpace,
    eslt_Length inputSize,
    P2CONST(eslt_Byte, AUTOMATIC, CRYPTO_APPL_DATA) input );

/***********************************************************************************************************************
 *  Crypto_finalizeVerifyRSAMD5_V15
 **********************************************************************************************************************/
/*! \brief        Finalize verification of RSA with MD5 digital signature
 *                <br>Functionality analog to esl_finalizeVerifyRSASHA1_V15(() of cryptovision.
 *  \param[in]    workSpace                pointer to signature workspace
 *  \param[in]    signatureSize            max signature size
 *  \param[in]    signature                pointer to where the signature shall be stored
 *  \return       ESL_ERC_NO_ERROR         finalization was successful
 *                <br>others               finalization was not successful
 *  \note         Crypto_initSignRSAMD5_V15() has to be executed before
 *  \context      task level
 **********************************************************************************************************************/
FUNC(eslt_ErrorCode, CRYPTO_CODE) Crypto_finalizeVerifyRSAMD5_V15(
    P2VAR(eslt_WorkSpaceRSAMD5ver, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) workSpace,
    eslt_Length signatureSize,
    P2CONST(eslt_Byte, AUTOMATIC, CRYPTO_APPL_DATA) signature);

/* signature generation generalized ------------------------------------------------------------- */
/***********************************************************************************************************************
 *  Crypto_SignRSACRTgen_V15
 **********************************************************************************************************************/
/*! \brief         Calculate RSA digital signature using a pre-calculated hash value
 *                 <br>Functionality analog to esl_initSignRSACRTSHA1_V15(), esl_updateSignRSACRTSHA1_V15() and
 *                 esl_finalizeSignRSACRTSHA1_V15() of cryptovision.
 *  \param[in]     workSpace                  pointer to signature workspace
 *  \param[in]     keyPairPrimePSize          size (byte) of RSA key parameter P
 *  \param[in]     keyPairPrimeP              pointer to RSA key parameter P
 *  \param[in]     keyPairPrimeQSize          size (byte) of RSA key parameter Q
 *  \param[in]     keyPairPrimeQ              pointer to RSA key parameter Q
 *  \param[in]     privateKeyExponentDPSize   size (byte) of RSA key parameter DP
 *  \param[in]     privateKeyExponentDP       pointer to RSA key parameter DP
 *  \param[in]     privateKeyExponentDQSize   size (byte) of RSA key parameter DQ
 *  \param[in]     privateKeyExponentDQ       pointer to RSA key parameter DQ
 *  \param[in]     privateKeyInverseQISize    size (byte) of RSA key parameter QI
 *  \param[in]     privateKeyInverseQI        pointer to RSA key parameter QI
 *  \param[in]     hashSize                   size (byte) of input / hash
 *  \param[in]     hashPtr                    pointer to input / hash
 *  \param[in,out] signatureSize              max signature size
 *  \param[in]     signature                  pointer to where the signature shall be stored
 *  \return        ESL_ERC_NO_ERROR           initialization was successful
 *                 <br>ESL_ERC_WS_TOO_SMALL   given workspace is too small
 *                 <br>others                 other init errors
 *  \note          Workspace has to be initialized before. Derived from function using RSA and MD5.
 *  \context       task level
 **********************************************************************************************************************/
FUNC(eslt_ErrorCode, CRYPTO_CODE) Crypto_SignRSACRTgen_V15(
    P2VAR(eslt_WorkSpaceRSACRTsig, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA) workSpace,
    uint16                                                 keyPairPrimePSize,
    CONSTP2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)  keyPairPrimeP,
    uint16                                                 keyPairPrimeQSize,
    CONSTP2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)  keyPairPrimeQ,
    uint16                                                 privateKeyExponentDPSize,
    CONSTP2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)  privateKeyExponentDP,
    uint16                                                 privateKeyExponentDQSize,
    CONSTP2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)  privateKeyExponentDQ,
    uint16                                                 privateKeyInverseQISize,
    CONSTP2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)  privateKeyInverseQI,
    uint16                                                 hashSize,
    CONSTP2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)  hashPtr,
    P2VAR(uint16, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)      signatureSize,
    CONSTP2VAR(uint8, CRYPTO_APPL_DATA, CRYPTO_APPL_DATA)  signature );

/**********************************************************************************************************************
 *  Crypto_ValidateRsaSignature
 **********************************************************************************************************************/
/*! \brief          This function is used to validate a RSA signature.
 *  \param[in]      RsaWorkSpPtr        Pointer to the Crypto workspace that is used for signature validation
 *  \param[in]      SignatureValuePtr   Pointer to the Signature value buffer
 *  \param[in]      PubKeyPtr           Pointer to the public key information that shall be used to verify this signature
 *  \param[in]      PubExpPtr           Pointer to the public exponent information that shall be used to verify this signature
 *  \param[in]      DigestValuePtr      Pointer to the digest value against which the signature shall be verified
 *  \param[in]      SignatureValueLen   Length of the signature
 *  \param[in]      PubKeyLen           Length of the public key
 *  \param[in]      PubExpLen           Length of the public exponent
 *  \param[in]      DigestValueLen      Length of the digest value
 *  \param[in]      DigestType          Type of the digest (sha1, sha256, md5)
 *  \return         E_OK                Signature validation finished successfully. The signature is valid.\n
 *                  <br>E_NOT_OK            Error during signature validation. The signature is invalid.\n
 *  \note           none
 *  \context        task level
 **********************************************************************************************************************/
FUNC(Std_ReturnType, CRYPTO_CODE) Crypto_ValidateRsaSignature(
    P2VAR(eslt_WorkSpaceRSAver, AUTOMATIC, CRYPTO_APPL_VAR) RsaWorkSpPtr,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) SignatureValuePtr,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) PubKeyPtr,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) PubExpPtr,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) DigestValuePtr,
    uint16 SignatureValueLen,
    uint16 PubKeyLen,
    uint16 PubExpLen,
    uint8 DigestValueLen,
    uint8 DigestType );

#endif  /* RSA support */

#if( (defined CRYPTO_ECC_SUPPORT_ENABLED) && (STD_ON == CRYPTO_ECC_SUPPORT_ENABLED) )
/**********************************************************************************************************************
 *  Crypto_GenerateEcdsaSignature
 **********************************************************************************************************************/
/*! \brief          This function is used to generate a ECDSA signature using ANSIp256r1 curve.
 *  \param[in]      EcWorkSpPtr         Pointer to the Crypto workspace that is used for signature generation
 *  \param[in,out]  SignatureValuePtr   Pointer to the Signature value buffer
 *  \param[in]      PrivKeyPtr          Pointer to the private key information that shall be used for this signature
 *  \param[in]      DigestValuePtr      Pointer to the digest value that is used signed
 *  \param[in,out]  SignatureValueLen   Pointer to the length of the signature, shall be maximum buffer size when calling
 *                                      this function and will hold the generated signature value length when returning
 *  \param[in]      PrivKeyLen          Length of the private key
 *  \param[in]      DigestValueLen      Length of the digest value
 *  \param[in]      BerEncoded          Signature Value is BER encoded
 *  \return         E_OK                Signature generated successfully and stored at SignatureValuePtr with SignatureValueLen\n
 *                  <br>E_NOT_OK            Error during signature generation. No signature value is available.\n
 *  \note           none
 *  \context        task level
 **********************************************************************************************************************/
FUNC(Std_ReturnType, CRYPTO_CODE) Crypto_GenerateEcdsaSignature(
    P2VAR(eslt_WorkSpaceEcP, AUTOMATIC, CRYPTO_APPL_VAR) EcWorkSpPtr,
    P2VAR(uint8, AUTOMATIC, CRYPTO_APPL_VAR) SignatureValuePtr,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) PrivKeyPtr,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) DigestValuePtr,
    P2VAR(uint16, AUTOMATIC, CRYPTO_APPL_VAR) SignatureValueLen,
    uint16 PrivKeyLen,
    uint8 DigestValueLen,
    boolean BerEncoded );

/**********************************************************************************************************************
 *  Crypto_ValidateEcdsaSignature
 **********************************************************************************************************************/
/*! \brief          This function is used to validate a ECDSA signature using ANSIp256r1 curve.
 *  \param[in]      EcWorkSpPtr         Pointer to the Crypto workspace that is used for signature validation
 *  \param[in]      SignatureValuePtr   Pointer to the Signature value buffer
 *  \param[in]      PubKeyPtr           Pointer to the public key information that shall be used to verify this signature
 *  \param[in]      DigestValuePtr      Pointer to the digest value against which the signature shall be verified
 *  \param[in]      SignatureValueLen   Length of the signature
 *  \param[in]      PubKeyLen           Length of the public key
 *  \param[in]      DigestValueLen      Length of the digest value
 *  \param[in]      BerEncoded          Signature Value is BER encoded
 *  \return         E_OK                Signature validation finished successfully. The signature is valid.\n
 *                  <br>E_NOT_OK            Error during signature validation. The signature is invalid.\n
 *  \note           none
 *  \context        task level
 **********************************************************************************************************************/
FUNC(Std_ReturnType, CRYPTO_CODE) Crypto_ValidateEcdsaSignature(
    P2VAR(eslt_WorkSpaceEcP, AUTOMATIC, CRYPTO_APPL_VAR) EcWorkSpPtr,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) SignatureValuePtr,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) PubKeyPtr,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) DigestValuePtr,
    uint16 SignatureValueLen,
    uint16 PubKeyLen,
    uint8 DigestValueLen,
    boolean BerEncoded );
#endif

/***********************************************************************************************************************
 *  Crypto_StirRNG
 **********************************************************************************************************************/
/*! \brief      Extern function needed by the ESLib of CV to get a random number
 *  \param[in]  InputLen            The length (byte) of the input data
 *  \param[in]  InputPtr            Pointer to the input data for stirring
 *  \return     E_OK                stirring was successfully <br>
 *              E_NOT_OK            stirring failed
 *  \note       The function esl_initRNG() has to be executed before. The minimum length of input data is 20 byte.
 *  \context    task level
 **********************************************************************************************************************/
FUNC(Std_ReturnType, CRYPTO_CODE) Crypto_StirRNG(
    CONST(uint16, AUTOMATIC) InputLen,
    P2CONST(uint8, AUTOMATIC, CRYPTO_APPL_DATA) InputPtr );

#define CRYPTO_STOP_SEC_CODE
#include "MemMap.h"
/* PRQA L:ASR_MEMMAP */
#endif 
  /* CRYPTO_H */
/**********************************************************************************************************************
 *  END OF FILE: Crypto.h
 *********************************************************************************************************************/
