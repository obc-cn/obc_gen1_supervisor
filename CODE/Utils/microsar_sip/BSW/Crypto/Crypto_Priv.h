/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2018 by Vector Informatik GmbH.                                                  All rights reserved.
 * 
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  Crypto_Priv.h
 *      Project:  MICROSAR IP
 *       Module:  MICROSAR Crypto
 *    Generator:  -
 *
 *  Description:  private header
 *  
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Crypto module. >> Crypto.h
 *********************************************************************************************************************/

#if !defined (CRYPTO_PRIV_H)
# define CRYPTO_PRIV_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Crypto_Types.h"
#include "Crypto_Cfg.h"

#if (CRYPTO_PROD_ERROR_DETECT == STD_ON)
# include "Dem.h"
#endif
#if (CRYPTO_DEV_ERROR_DETECT == STD_ON)
# include "Det.h"
#endif


/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3453,3458 FCT_LIKE_MACROS */ /* MISRA 19.4,19.7: Macros improve code readability */
/*
#define CRYPTO_BEGIN_CRITICAL_SECTION() SchM_Enter_Crypto(CRYPTO_EXCLUSIVE_AREA_0)
#define CRYPTO_END_CRITICAL_SECTION()   SchM_Exit_Crypto(CRYPTO_EXCLUSIVE_AREA_0)
*/

/* BRS Time Measurement Macros */

/* Diagnostic Event Manager */
#if ( CRYPTO_PROD_ERROR_DETECT == STD_ON )
#define CRYPTO_VDEM_REPORT_ERROR_STATUS(ErrId)   (Dem_ReportErrorStatus((ErrId), DEM_EVENT_STATUS_FAILED))
#else
#define CRYPTO_VDEM_REPORT_ERROR_STATUS(ErrId, EvId)
#endif
  /* CRYPTO_PROD_ERROR_DETECT */

/* Development Error Tracer */
#if ( CRYPTO_DEV_ERROR_DETECT == STD_ON )
#define CRYPTO_CheckDetErrorReturnVoid(  CONDITION, API_ID, ERROR_CODE )       { if(!(CONDITION)) {\
  (void)Det_ReportError( CRYPTO_MODULE_ID, CRYPTO_INSTANCE_ID, (API_ID), (ERROR_CODE)); return; } }  /* PRQA S 3458 */ /*  MD_MSR_19.4 */
#define CRYPTO_CheckDetErrorReturnValue( CONDITION, API_ID, ERROR_CODE, RET_VAL ) \
  { if(!(CONDITION)) {\
  (void)Det_ReportError( CRYPTO_MODULE_ID, CRYPTO_INSTANCE_ID, (API_ID), (ERROR_CODE)); return (RET_VAL); } }  /* PRQA S 3458 */ /*  MD_MSR_19.4 */
#define CRYPTO_CheckDetErrorContinue(    CONDITION, API_ID, ERROR_CODE )       { if(!(CONDITION)) {\
  (void)Det_ReportError( CRYPTO_MODULE_ID, CRYPTO_INSTANCE_ID, (API_ID), (ERROR_CODE)); } }  /* PRQA S 3458 */ /*  MD_MSR_19.4 */
#define CRYPTO_CallDetReportError( API_ID, ERROR_CODE )  \
  (void)Det_ReportError( CRYPTO_MODULE_ID, CRYPTO_INSTANCE_ID, (API_ID), (ERROR_CODE))  /* PRQA S 3458 */ /*  MD_MSR_19.4 */
#else
#define CRYPTO_CheckDetErrorReturnVoid(  CONDITION, API_ID, ERROR_CODE )
#define CRYPTO_CheckDetErrorReturnValue( CONDITION, API_ID, ERROR_CODE, RET_VAL )
#define CRYPTO_CheckDetErrorContinue(    CONDITION, API_ID, ERROR_CODE )
#define CRYPTO_CallDetReportError(                  API_ID, ERROR_CODE )
#endif
  /* CRYPTO_DEV_ERROR_DETECT */

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/
/* PRQA S 5087,3614 ASR_MEMMAP */ /* MISRA 19.1: AUTOSAR Memory Mapping */
#define CRYPTO_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h"
#define CRYPTO_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h"

/* PRQA L:ASR_MEMMAP */
/* PRQA L:FCT_LIKE_MACROS */
#endif
  /* CRYPTO_PRIV_H */
/**********************************************************************************************************************
 *  END OF FILE: Crypto_Priv.h
 *********************************************************************************************************************/
