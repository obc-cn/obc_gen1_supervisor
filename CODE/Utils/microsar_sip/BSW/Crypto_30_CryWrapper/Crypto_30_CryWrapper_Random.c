/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*      \file  Crypto_30_CryWrapper_Random.c
 *      \brief  MICROSAR Crypto Driver (Crypto)
 *
 *      \details  Dispatcher for RANDOM Services
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/

#define CRYPTO_30_CRYWRAPPER_RANDOM_SOURCE
/* PRQA S 0777, 0779, 3453 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779, MD_MSR_19.7 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Crypto_30_CryWrapper.h"
#include "Crypto_30_CryWrapper_Services.h"


/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#define CRYPTO_30_CRYWRAPPER_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/
#if (CRYPTO_30_CRYWRAPPER_SERVICE_RANDOM == STD_ON)
/**********************************************************************************************************************
 *  Crypto_30_CryWrapper_DispatchRandom()
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, CRYPTO_30_CRYWRAPPER_CODE) Crypto_30_CryWrapper_DispatchRandom(uint32 objectId,
  P2CONST(Crypto_JobType, AUTOMATIC, CRYPTO_30_CRYWRAPPER_APPL_VAR) job,
  Crypto_OperationModeType mode)
{
  /* ----- Local Variables ------------------------------------------------ */
  Std_ReturnType retVal = E_NOT_OK;

# if (CRYPTO_30_CRYWRAPPER_RANDOMSEEDCONFIG == STD_ON) /* COV_CRYPTO_30_CRYWRAPPER_PREVENTED_CRY_CONFIG */
  uint32 primCfgIdx;
  Crypto_30_CryWrapper_JobsIterType jobIdx;
  boolean jobWasFound = FALSE;
  Csm_ReturnType retValCry = CSM_E_NOT_OK;

  /* ----- Implementation ------------------------------------------------- */
  /* #01 Search for a matching Job to decide which CRY configuration shall be used. */
  for (primCfgIdx = 0; (primCfgIdx < Crypto_30_CryWrapper_GetSizeOfRandomGenerateConfig()) && (jobWasFound == FALSE); primCfgIdx++)
  {
    for (jobIdx = Crypto_30_CryWrapper_GetJobsStartIdxOfRandomGenerateConfig(primCfgIdx); jobIdx < Crypto_30_CryWrapper_GetJobsEndIdxOfRandomGenerateConfig(primCfgIdx); jobIdx++)
    {
      if (Crypto_30_CryWrapper_GetJobs(jobIdx) == job->jobInfo->jobId)
      {
        jobWasFound = TRUE;
        /* #05 Distinguish modes. */
        switch (mode)
        {
          /* #40 Handle operationmode UPDATE. */
          case CRYPTO_OPERATIONMODE_UPDATE:
          {
            /* #45 Call Cry update function. */
            retValCry = Crypto_30_CryWrapper_GetPlainFuncOfRandomGenerateConfig(primCfgIdx)( /* SBSW_CRYPTO_30_CRYWRAPPER_CRY_FUNCTION_JOB_MEMBER_FORWARDING */
              Crypto_30_CryWrapper_GetInitPtrOfRandomGenerateConfig(primCfgIdx),
              job->jobPrimitiveInputOutput.outputPtr,
              *job->jobPrimitiveInputOutput.outputLengthPtr);
            break;
          }
          default:
          {
            /* #48 Return E_OK in other cases than update. */
            retValCry = CSM_E_OK;
            break;
          }
        }
      }
    }
  }

  /* #110 Return E_OK if Cry function call was successful. */
  if (retValCry == CSM_E_OK)
  {
    retVal = E_OK;
  }
  else if (retValCry == CSM_E_BUSY)
  {
    retVal = CRYPTO_E_BUSY;
  }
  else
  {
  }

# else
  CRYPTO_30_CRYWRAPPER_DUMMY_STATEMENT(mode); /* PRQA S 3112 */ /* MD_MSR_14.2 */ /*lint !e438 */
  CRYPTO_30_CRYWRAPPER_DUMMY_STATEMENT_CONST(job); /* PRQA S 3112 */ /* MD_MSR_14.2 */ /*lint !e438 */
# endif

  CRYPTO_30_CRYWRAPPER_DUMMY_STATEMENT(objectId); /* PRQA S 3112 */ /* MD_MSR_14.2 */ /*lint !e438 */

  return retVal;
} /* PRQA S 6030, 6080 */ /* MD_MSR_STCYC, MD_MSR_STMIF */

#endif /* (CRYPTO_30_CRYWRAPPER_SERVICE_RANDOM == STD_ON) */

#define CRYPTO_30_CRYWRAPPER_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *  END OF FILE: Crypto_30_CryWrapper_Random.c
 *********************************************************************************************************************/
