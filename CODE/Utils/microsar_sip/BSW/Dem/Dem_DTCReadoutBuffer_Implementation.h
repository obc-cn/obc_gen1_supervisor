/* ********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 */
/*! \addtogroup Dem_DTCReadoutBuffer
 *  \{
 *  \file       Dem_DTCReadoutBuffer_Implementation.h
 *  \brief      Diagnostic Event Manager (Dem) Implementation file
 *  \details    Buffer event specific data after Dem_DisableDTCRecordUpdate() call.
 *********************************************************************************************************************/

/* ********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  -------------------------------------------------------------------------------------------------------------------
 *  Savas Ates                    vissat        Vector Informatik GmbH
 *  Anna Bosch                    visanh        Vector Informatik GmbH
 *  Stefan Huebner                vishrs        Vector Informatik GmbH
 *  Thomas Dedler                 visdth        Vector Informatik GmbH
 *  Alexander Ditte               visade        Vector Informatik GmbH
 *  Matthias Heil                 vismhe        Vector Informatik GmbH
 *  Erik Jeglorz                  visejz        Vector Informatik GmbH
 *  Friederike Hitzler            visfrs        Vector Informatik GmbH
 *  Aswin Vijayamohanan Nair      visavi        Vector Informatik GmbH
 *  Fabian Wild                   viszfa        Vector Informatik GmbH
 *  Erwin Stamm                   visern        Vector Informatik GmbH
 *  -------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  REFER TO DEM.H
 *********************************************************************************************************************/

#if !defined (DEM_DTCREADOUTBUFFER_IMPLEMENTATION_H)
#define DEM_DTCREADOUTBUFFER_IMPLEMENTATION_H

/* ********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/

                                                  /* Own subcomponent header */
/* ------------------------------------------------------------------------- */
#include "Dem_DTCReadoutBuffer_Interface.h"

                                                    /* Used subcomponent API */
/* ------------------------------------------------------------------------- */
#include "Dem_MemoryEntry_Implementation.h"
#include "Dem_SnapshotEntry_Implementation.h"
#include "Dem_ExtendedEntry_Implementation.h"
#include "Dem_DTCSelector_Implementation.h"
#include "Dem_Scheduler_Implementation.h"
#include "Dem_MemCopy.h"
#include "Dem_Memory_Implementation.h"

                                                   /* Subcomponents callbacks*/
/* ------------------------------------------------------------------------- */
#include "Dem_Error_Interface.h"
#include "Dem_DTC_Interface.h"


/* ********************************************************************************************************************
 *  SUBCOMPONENT CONSTANT MACROS
 *********************************************************************************************************************/

/*! Filename declaration */
#define DEM_DTCREADOUTBUFFER_IMPLEMENTATION_FILENAME "Dem_DTCReadoutBuffer_Implementation.h"

/*!
 * \addtogroup  Dem_DTCReadoutBuffer_StatusType
 * \{
 */
#define DEM_DTCREADOUTBUFFER_STATUS_UNUSED            0  /*!< Initialization value, buffer is unused and contains invalid data */
#define DEM_DTCREADOUTBUFFER_STATUS_QUEUED            1  /*!< Buffer contains EventId, MemoryId configuration - waiting for MainFunction to copy the data */
#define DEM_DTCREADOUTBUFFER_STATUS_IN_USE            2  /*!< Buffer contains configuration and event data */
#define DEM_DTCREADOUTBUFFER_STATUS_NOT_STORED        3  /*!< Buffer contains EventId, MemoryId configuration - no data found to copy */
/*!
 * \}
 */

/* ********************************************************************************************************************
 *  SUBCOMPONENT FUNCTION MACROS
 *********************************************************************************************************************/

/* ********************************************************************************************************************
 *  SUBCOMPONENT DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/* ********************************************************************************************************************
 *  SUBCOMPONENT FUNCTION DEFINITIONS
 *********************************************************************************************************************/
#define DEM_START_SEC_CODE
#include "MemMap.h"                                                                                                              /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*!
 * \addtogroup Dem_DTCReadoutBuffer_Properties
 * \{
 */

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_TestDataRequested
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(boolean, DEM_CODE)
Dem_DTCReadoutBuffer_TestDataRequested(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId
  )
{
  return (boolean)(Dem_DTCReadoutBuffer_GetState(ReadoutBufferId) != DEM_DTCREADOUTBUFFER_STATUS_UNUSED);
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_TestResultReady
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(boolean, DEM_CODE)
Dem_DTCReadoutBuffer_TestResultReady(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId
  )
{
  Dem_DTCReadoutBuffer_StatusType lBufferStatus;
  lBufferStatus = Dem_DTCReadoutBuffer_GetState(ReadoutBufferId);
  return (boolean)( (lBufferStatus == DEM_DTCREADOUTBUFFER_STATUS_IN_USE)
                          || (lBufferStatus == DEM_DTCREADOUTBUFFER_STATUS_NOT_STORED) );
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_TestStoredDataAvailable
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(boolean, DEM_CODE)
Dem_DTCReadoutBuffer_TestStoredDataAvailable(                                                                                    /* PRQA S 3219 */ /* MD_DEM_14.1_ACCESSOR */
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId
  )
{
  return (boolean)(Dem_DTCReadoutBuffer_GetState(ReadoutBufferId) == DEM_DTCREADOUTBUFFER_STATUS_IN_USE);
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_TestExtendedRecordSelected
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(boolean, DEM_CODE)
Dem_DTCReadoutBuffer_TestExtendedRecordSelected(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  return Dem_DTCReadoutBuffer_GetExtendedIteratorPtr(ReadoutBufferId, ReadoutBufferDataIndex)->ExtendedRecordSelected;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetExtendedDataEventId
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(Dem_EventIdType, DEM_CODE)
Dem_DTCReadoutBuffer_GetExtendedDataEventId(                                                                                     /* PRQA S 3219 */ /* MD_DEM_14.1_ACCESSOR */
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  return Dem_DTCReadoutBuffer_GetExtendedIteratorPtr(ReadoutBufferId, ReadoutBufferDataIndex)->EventId;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetExtendedDataMemoryEntryId
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(Dem_Cfg_MemoryEntryHandleType, DEM_CODE)
Dem_DTCReadoutBuffer_GetExtendedDataMemoryEntryId(                                                                               /* PRQA S 3219 */ /* MD_DEM_14.1_ACCESSOR */
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  return Dem_DTCReadoutBuffer_GetExtendedIteratorPtr(ReadoutBufferId, ReadoutBufferDataIndex)->MemoryEntryId;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetExtendedEntryIndex
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(uint8, DEM_CODE)
Dem_DTCReadoutBuffer_GetExtendedEntryIndex(                                                                                      /* PRQA S 3219 */ /* MD_DEM_14.1_ACCESSOR */
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  return Dem_DTCReadoutBuffer_GetExtendedIteratorPtr(ReadoutBufferId, ReadoutBufferDataIndex)->ExtendedEntryIndex;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetExtendedEntryIterator
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
DEM_LOCAL_INLINE FUNC(Dem_ExtendedEntry_IterType, DEM_CODE)
Dem_DTCReadoutBuffer_GetExtendedEntryIterator(                                                                                   /* PRQA S 3219 */ /* MD_DEM_14.1_ACCESSOR */
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  return Dem_DTCReadoutBuffer_GetExtendedIteratorPtr(ReadoutBufferId, ReadoutBufferDataIndex)->ExtendedEntryIter;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetExtendedEntryIterator
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetExtendedEntryIterator(                                                                                   /* PRQA S 3219 */ /* MD_DEM_14.1_ACCESSOR */
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex,
  CONST(Dem_ExtendedEntry_IterType, AUTOMATIC)  ExtendedEntryIter
  )
{
  Dem_DTCReadoutBuffer_GetReadoutBufferDataSafe(                                                                                 /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER_READOUTBUFFERDATA */
    ReadoutBufferId, ReadoutBufferDataIndex)->ExtendedIterator.ExtendedEntryIter = ExtendedEntryIter;                           
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_IncExtendedEntryIndex
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_IncExtendedEntryIndex(                                                                                      /* PRQA S 3219 */ /* MD_DEM_14.1_ACCESSOR */
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex,
  CONST(uint8, AUTOMATIC)  ERecType
  )
{
# if (DEM_CFG_SUPPORT_USER_ERECS == STD_ON)
  if (ERecType == DEM_CFG_EREC_TYPE_USER)
  {
    uint8 lExtendedEntryIndex;

    lExtendedEntryIndex = Dem_DTCReadoutBuffer_GetExtendedEntryIndex(
                            ReadoutBufferId, ReadoutBufferDataIndex) + 1;

    Dem_DTCReadoutBuffer_SetExtendedEntryIndex(
      ReadoutBufferId, ReadoutBufferDataIndex, lExtendedEntryIndex);
  }
# else
  DEM_IGNORE_UNUSED_CONST_ARGUMENT(ReadoutBufferId)                                                                              /* PRQA S 3112 */ /* MD_DEM_14.2 */
  DEM_IGNORE_UNUSED_CONST_ARGUMENT(ERecType)                                                                                     /* PRQA S 3112 */ /* MD_DEM_14.2 */
  DEM_IGNORE_UNUSED_CONST_ARGUMENT(ReadoutBufferDataIndex)                                                                      /* PRQA S 3112 */ /* MD_DEM_14.2 */
# endif
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetExtendedDataNumber
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
DEM_LOCAL_INLINE FUNC(uint8, DEM_CODE)
Dem_DTCReadoutBuffer_GetExtendedDataNumber(                                                                                      /* PRQA S 3219 */ /* MD_DEM_14.1_ACCESSOR */
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  return Dem_DTCReadoutBuffer_GetExtendedIteratorPtr(ReadoutBufferId, ReadoutBufferDataIndex)->ExtendedDataNumber;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_TestSnapshotRecordSelected
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(boolean, DEM_CODE)
Dem_DTCReadoutBuffer_TestSnapshotRecordSelected(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId
  )
{
  return Dem_DTCReadoutBuffer_GetReadoutBuffer(ReadoutBufferId)->SnapshotSelected;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetSnapshotRecordSelected
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetSnapshotRecordSelected(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(boolean, AUTOMATIC) SnapshotSelected
  )
{
  Dem_DTCReadoutBuffer_GetReadoutBufferSafe(ReadoutBufferId)->SnapshotSelected = SnapshotSelected;                               /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER */
}
#endif

#if ((DEM_CFG_SUPPORT_DCM == STD_ON) && (DEM_CFG_SUPPORT_SRECS == STD_ON))
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetSnapshotEntryIterator
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
DEM_LOCAL_INLINE FUNC(Dem_Cfg_SnapshotEntry_IterType, DEM_CODE)
Dem_DTCReadoutBuffer_GetSnapshotEntryIterator(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
)
{
  return Dem_DTCReadoutBuffer_GetSnapshotIteratorPtr(ReadoutBufferId, ReadoutBufferDataIndex)->SnapshotEntryIter;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetSnapshotEntryIterator
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetSnapshotEntryIterator(                                                                                   /* PRQA S 3219 */ /* MD_DEM_14.1_ACCESSOR */
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex,
  CONST(Dem_Cfg_SnapshotEntry_IterType, AUTOMATIC)  SnapshotEntryIter
)
{
  Dem_DTCReadoutBuffer_GetReadoutBufferDataSafe(                                                                                 /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER_READOUTBUFFERDATA */
    ReadoutBufferId, ReadoutBufferDataIndex)->SnapshotIterator.SnapshotEntryIter = SnapshotEntryIter;                           
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetSnapshotRecordNumber
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(uint8, DEM_CODE)
Dem_DTCReadoutBuffer_GetSnapshotRecordNumber(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId
)
{
  return Dem_DTCReadoutBuffer_GetReadoutBuffer(ReadoutBufferId)->SelectedSnapshotRecordNumber;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetSnapshotRecordNumber
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetSnapshotRecordNumber(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(uint8, AUTOMATIC)  SelectedSnapshotRecordNumber
)
{
  Dem_DTCReadoutBuffer_GetReadoutBufferSafe(ReadoutBufferId)->SelectedSnapshotRecordNumber = SelectedSnapshotRecordNumber;       /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER */                          
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetSnapshotRecordSource
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(Dem_DTCReadoutBuffer_SnapshotSourceType, DEM_CODE)
Dem_DTCReadoutBuffer_GetSnapshotRecordSource(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId
)
{
  return Dem_DTCReadoutBuffer_GetReadoutBuffer(ReadoutBufferId)->Source;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetSnapshotRecordSource
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetSnapshotRecordSource(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_SnapshotSourceType, AUTOMATIC)  Source
)
{
  Dem_DTCReadoutBuffer_GetReadoutBufferSafe(ReadoutBufferId)->Source = Source;                                                   /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER */
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetSnapshotSourceIterator
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(Dem_Cfg_ComplexIterType, DEM_CODE)
Dem_DTCReadoutBuffer_GetSnapshotSourceIterator(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
)
{
  return Dem_DTCReadoutBuffer_GetSnapshotIteratorPtr(ReadoutBufferId, ReadoutBufferDataIndex)->SourceIterator;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetSnapshotSourceIterator
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetSnapshotSourceIterator(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex,
  CONST(Dem_Cfg_ComplexIterType, AUTOMATIC)  SourceIterator
)
{
  Dem_DTCReadoutBuffer_GetReadoutBufferDataSafe(ReadoutBufferId,                                                                 /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER_READOUTBUFFERDATA */
    ReadoutBufferDataIndex)->SnapshotIterator.SourceIterator = SourceIterator;                                                  
}
#endif

/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetMemoryEntryId
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
DEM_LOCAL_INLINE FUNC(Dem_Cfg_MemoryEntryHandleType, DEM_CODE)
Dem_DTCReadoutBuffer_GetMemoryEntryId(                                                                                           /* PRQA S 3219 */ /* MD_DEM_14.1_ACCESSOR */
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  Dem_Cfg_MemoryEntryHandleType lReadoutBufferHandle;

#if (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_OFF)
  DEM_IGNORE_UNUSED_CONST_ARGUMENT(ReadoutBufferDataIndex)                                                                      /* PRQA S 3112 */ /* MD_DEM_14.2 */
  lReadoutBufferHandle = (DEM_MEMORYENTRY_HANDLE_READOUTBUFFER + ReadoutBufferId);
#else
  lReadoutBufferHandle = (DEM_MEMORYENTRY_HANDLE_READOUTBUFFER 
                          + (ReadoutBufferId * Dem_Cfg_SizeOfReadOutBufferData())
                          + ReadoutBufferDataIndex);
#endif
  return lReadoutBufferHandle;
}

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetEventId
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(Dem_EventIdType, DEM_CODE)
Dem_DTCReadoutBuffer_GetEventId(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  Dem_EventIdType lReturnVal;

  lReturnVal = (Dem_EventIdType)Dem_DTCReadoutBuffer_GetReadoutBufferData(
                                ReadoutBufferId, ReadoutBufferDataIndex)->EventId;

  return lReturnVal;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetEventId
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetEventId(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex,
  CONST(Dem_EventIdType, AUTOMATIC) EventId
  )
{
  Dem_DTCReadoutBuffer_GetReadoutBufferDataSafe(ReadoutBufferId, ReadoutBufferDataIndex)->EventId = EventId;                    /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER_READOUTBUFFERDATA */
}
#endif

#if ((DEM_CFG_SUPPORT_DCM == STD_ON) && (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON))
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetMasterEventId
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(Dem_EventIdType, DEM_CODE)
Dem_DTCReadoutBuffer_GetMasterEventId(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId
  )
{
  Dem_EventIdType lReturnVal;
  lReturnVal = (Dem_EventIdType)Dem_DTCReadoutBuffer_GetReadoutBuffer(
                                ReadoutBufferId)->MasterEventId;

  return lReturnVal;
}
#endif

#if ((DEM_CFG_SUPPORT_DCM == STD_ON) && (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON))
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetMasterEventId
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetMasterEventId(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(Dem_EventIdType, AUTOMATIC) MasterEventId
  )
{
  Dem_DTCReadoutBuffer_GetReadoutBufferSafe(ReadoutBufferId)->MasterEventId = MasterEventId;                                     /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER */
}
#endif

#if ((DEM_CFG_SUPPORT_DCM == STD_ON) && (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON))
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetNumberOfOccupiedSlots
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(uint8, DEM_CODE)
Dem_DTCReadoutBuffer_GetNumberOfOccupiedSlots(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId
  )
{
  uint8 lReturnVal;
  lReturnVal = Dem_DTCReadoutBuffer_GetReadoutBuffer(ReadoutBufferId)->NumOccupiedSlots;

  return lReturnVal;
}
#endif

#if ((DEM_CFG_SUPPORT_DCM == STD_ON) && (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON))
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetNumberOfOccupiedSlots
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetNumberOfOccupiedSlots(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(uint8, AUTOMATIC) NumOccupiedSlots
  )
{
  Dem_DTCReadoutBuffer_GetReadoutBufferSafe(ReadoutBufferId)->NumOccupiedSlots = NumOccupiedSlots;                               /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER */
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetMemoryId
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(uint8, DEM_CODE)
Dem_DTCReadoutBuffer_GetMemoryId(                                                                                                /* PRQA S 3219 */ /* MD_DEM_14.1_ACCESSOR */
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId
  )
{
  return Dem_DTCReadoutBuffer_GetReadoutBuffer(ReadoutBufferId)->MemoryId;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetMemoryId
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetMemoryId(                                                                                                
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(uint8, AUTOMATIC) MemoryId
  )
{
  Dem_DTCReadoutBuffer_GetReadoutBufferSafe(ReadoutBufferId)->MemoryId = MemoryId;                                               /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER */
}
#endif

#if ( (DEM_CFG_SUPPORT_DCM == STD_ON) \
   && (DEM_CFG_SUPPORT_OBDII == STD_ON) \
   && (DEM_CFG_SUPPORT_OBDII_FREEZEFRAME_IN_SVC19 == STD_ON) )
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetOBDData_IsStored
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetOBDData_IsStored(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(boolean, AUTOMATIC) IsStored
  )
{
  Dem_DTCReadoutBuffer_GetReadoutBufferSafe(ReadoutBufferId)->OBDFreezeFrameData.IsStored = IsStored;                            /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER */
}
#endif

#if ( (DEM_CFG_SUPPORT_DCM == STD_ON) \
   && (DEM_CFG_SUPPORT_OBDII == STD_ON) \
   && (DEM_CFG_SUPPORT_OBDII_FREEZEFRAME_IN_SVC19 == STD_ON) )
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetOBDData_IsStored
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(boolean, DEM_CODE)
Dem_DTCReadoutBuffer_GetOBDData_IsStored(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId
  )
{
  return Dem_DTCReadoutBuffer_GetReadoutBuffer(ReadoutBufferId)->OBDFreezeFrameData.IsStored;
}
#endif

#if ( (DEM_CFG_SUPPORT_DCM == STD_ON) \
   && (DEM_CFG_SUPPORT_OBDII == STD_ON) \
   && (DEM_CFG_SUPPORT_OBDII_FREEZEFRAME_IN_SVC19 == STD_ON) )
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetPtrToOBDFreezeFrameBuffer
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(Dem_DataPtrType, DEM_CODE)
Dem_DTCReadoutBuffer_GetPtrToOBDFreezeFrameBuffer(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId
  )
{
  return &(Dem_DTCReadoutBuffer_GetReadoutBufferSafe(ReadoutBufferId)->OBDFreezeFrameData.ObdFreezeFrame[0]);
}
#endif

/*!
 * \}
 */
#define DEM_STOP_SEC_CODE
#include "MemMap.h"                                                                                                              /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define DEM_START_SEC_CODE
#include "MemMap.h"                                                                                                              /* PRQA S 5087 */ /* MD_MSR_19.1 */
/*!
 * \addtogroup Dem_DTCReadoutBuffer_Private
 * \{
 */

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetReadoutBufferEntry()
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *****************************************************************************/
DEM_LOCAL FUNC(void, DEM_CODE) 
Dem_DTCReadoutBuffer_SetReadoutBufferEntry(                                                                       
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(uint16, AUTOMATIC)  MasterEventId,
  CONST(uint8, AUTOMATIC)  MemoryId,
  CONST(uint8, AUTOMATIC)  State,
  CONST(boolean, AUTOMATIC)  CombinedEventSelected
  )
{
  uint8 lSizeOfReadoutBufferDataEntries;
  Dem_Cfg_DTCReadoutBuffer_SnapshotDataIteratorType lSnapshotIterator;
  Dem_Cfg_DTCReadoutBuffer_ExtendedDataIteratorType lExtendedIterator;

  Dem_DTCReadoutBuffer_SnapshotRecord_Init(&lSnapshotIterator);                                                                  /* SBSW_DEM_POINTER_FORWARD_STACK */
  Dem_DTCReadoutBuffer_ExtendedDataRecord_Init(&lExtendedIterator);                                                              /* SBSW_DEM_POINTER_FORWARD_STACK */
  DEM_IGNORE_UNUSED_CONST_ARGUMENT(CombinedEventSelected)                                                                        /* PRQA S 3112 */ /* MD_DEM_14.2 */

# if (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON)
  Dem_DTCReadoutBuffer_SetMasterEventId(ReadoutBufferId, MasterEventId);
  Dem_DTCReadoutBuffer_SetNumberOfOccupiedSlots(ReadoutBufferId, 0x00U);
  if (CombinedEventSelected == TRUE)
  {  
    /* Initialize all readout buffer data entries */
    lSizeOfReadoutBufferDataEntries = Dem_Cfg_SizeOfReadOutBufferData();
  }
  else
# endif
  {
    /* Initialize default readout buffer data entries */
    lSizeOfReadoutBufferDataEntries = 0x01U;
  }

  {
    Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType lReadoutBufferDataIndex;

    for (lReadoutBufferDataIndex = 0x00U;
         lReadoutBufferDataIndex < lSizeOfReadoutBufferDataEntries;
         lReadoutBufferDataIndex++)
    {
      Dem_DTCReadoutBuffer_SetSnapshotIterator(ReadoutBufferId,                                                                    
                                               lReadoutBufferDataIndex,        
                                               &lSnapshotIterator);                                                              /* SBSW_DEM_POINTER_FORWARD_STACK */
      Dem_DTCReadoutBuffer_SetExtendedIterator(ReadoutBufferId,                                                                  
                                               lReadoutBufferDataIndex,        
                                               &lExtendedIterator);                                                              /* SBSW_DEM_POINTER_FORWARD_STACK */
    }
  }

  Dem_DTCReadoutBuffer_SetEventId(ReadoutBufferId, DEM_DTCREADOUTBUFFER_DATAINDEX_DEFAULT, MasterEventId);
  Dem_DTCReadoutBuffer_SetMemoryId(ReadoutBufferId, MemoryId);
  Dem_DTCReadoutBuffer_SetSnapshotRecordSource(ReadoutBufferId, Dem_DTCReadoutBuffer_SnapshotSource_Invalid);
  Dem_DTCReadoutBuffer_SetSnapshotRecordNumber(ReadoutBufferId, 0x00);
  Dem_DTCReadoutBuffer_SetSnapshotRecordSelected(ReadoutBufferId, FALSE);
  Dem_DTCReadoutBuffer_SetState(ReadoutBufferId, State);
}                                                                                                                                /* PRQA S 6050, 6060 */ /* MD_MSR_STCAL, MD_MSR_STPAR */
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_InitializeSnapshotIterator()
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *****************************************************************************/
DEM_LOCAL FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_InitializeSnapshotIterator(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  Dem_Cfg_DTCReadoutBuffer_SnapshotDataIteratorType lSnapshotDataIterator;

  lSnapshotDataIterator = *Dem_DTCReadoutBuffer_GetSnapshotIteratorPtr(
                            ReadoutBufferId,
                            ReadoutBufferDataIndex);

# if (DEM_CFG_SUPPORT_SRECS == STD_ON)
  {
    Dem_EventIdType lEventId;

    lEventId = Dem_DTCReadoutBuffer_GetEventId(
                 ReadoutBufferId, 
                 ReadoutBufferDataIndex);

    Dem_SnapshotEntry_IteratorInit(lEventId,
                                   Dem_DTCReadoutBuffer_GetMemoryEntryId(
                                     ReadoutBufferId, 
                                     ReadoutBufferDataIndex), 
                                   &(lSnapshotDataIterator.SnapshotEntryIter));                                                  /* SBSW_DEM_POINTER_FORWARD_STACK */
  }
# endif

  Dem_Cfg_ComplexIterClear(&(lSnapshotDataIterator.SourceIterator));                                                             /* SBSW_DEM_POINTER_FORWARD_STACK */
  Dem_DTCReadoutBuffer_SetSnapshotIterator(ReadoutBufferId,                                                                      /* SBSW_DEM_POINTER_FORWARD_STACK */
                                           ReadoutBufferDataIndex,
                                           &lSnapshotDataIterator);
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetReadoutBufferSafe()
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *****************************************************************************/
DEM_LOCAL FUNC(Dem_Cfg_ReadoutBufferEntryPtrType, DEM_CODE)
Dem_DTCReadoutBuffer_GetReadoutBufferSafe(                                                                              
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId
  )
{
  Dem_Cfg_ReadoutBufferEntryPtrType lResult;
# if (DEM_DEV_RUNTIME_CHECKS == STD_ON)
  if (ReadoutBufferId >= Dem_Cfg_GetSizeOfReadoutBuffer())
  {
    Dem_Error_RunTimeCheckFailed(DEM_DTCREADOUTBUFFER_IMPLEMENTATION_FILENAME, __LINE__);                                        /* SBSW_DEM_POINTER_RUNTIME_CHECK */
    lResult = &Dem_Cfg_GetReadoutBuffer(0);
  }
  else
# endif
  {
    lResult =  Dem_Cfg_GetAddrReadoutBuffer(ReadoutBufferId);
  }

  return lResult;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetReadoutBuffer()
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *****************************************************************************/
DEM_LOCAL FUNC(Dem_Cfg_ReadoutBufferEntryPtrType, DEM_CODE)
Dem_DTCReadoutBuffer_GetReadoutBuffer(                                                                                           
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId
  )
{
  Dem_Cfg_ReadoutBufferEntryPtrType lResult;

  Dem_Internal_AssertContinue(ReadoutBufferId < Dem_Cfg_GetSizeOfReadoutBuffer(), DEM_E_INCONSISTENT_STATE)
  lResult =  Dem_Cfg_GetAddrReadoutBuffer(ReadoutBufferId);

  return lResult;
}
#endif

/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetReadoutBufferDataSafe
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *****************************************************************************/
DEM_LOCAL FUNC(Dem_Cfg_ReadoutBufferDataPtrType, DEM_CODE)
Dem_DTCReadoutBuffer_GetReadoutBufferDataSafe(                                                                                   /* PRQA S 3219 */ /* MD_DEM_14.1_ACCESSOR */
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  Dem_Cfg_ReadoutBufferDataPtrType lResult;
# if (DEM_DEV_RUNTIME_CHECKS == STD_ON)
  if ( (ReadoutBufferId >= Dem_Cfg_GetSizeOfReadoutBuffer())                                                                     /* PRQA S 3415 */ /* MD_DEM_12.4_cf */
    || (ReadoutBufferDataIndex >= Dem_Cfg_SizeOfReadOutBufferData()))
  {
    Dem_Error_RunTimeCheckFailed(DEM_DTCREADOUTBUFFER_IMPLEMENTATION_FILENAME, __LINE__);                                        /* SBSW_DEM_POINTER_RUNTIME_CHECK */
    lResult = &(Dem_Cfg_GetReadoutBuffer(0).ReadOutBufferData[0]);
  }
  else
# endif
  {
    lResult = &(Dem_Cfg_GetReadoutBuffer(ReadoutBufferId).ReadOutBufferData[ReadoutBufferDataIndex]);
  }

  return lResult;
}

/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetReadoutBufferData()
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *****************************************************************************/
DEM_LOCAL FUNC(Dem_Cfg_ReadoutBufferDataPtrType, DEM_CODE)
Dem_DTCReadoutBuffer_GetReadoutBufferData(                                                                                       /* PRQA S 3219 */ /* MD_DEM_14.1_ACCESSOR */
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  Dem_Cfg_ReadoutBufferDataPtrType lResult;

  Dem_Internal_AssertContinue(ReadoutBufferDataIndex < Dem_Cfg_SizeOfReadOutBufferData(), DEM_E_INCONSISTENT_STATE)
    lResult = &(Dem_Cfg_GetReadoutBuffer(ReadoutBufferId).ReadOutBufferData[ReadoutBufferDataIndex]);

  return lResult;
}

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_ExtendedDataRecord_Init
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_ExtendedDataRecord_Init(
  Dem_Cfg_DTCReadoutBuffer_ExtendedDataIteratorPtrType ExtendedDataIterator
)
{
# if(DEM_CFG_SUPPORT_ERECS == STD_ON)
  Dem_ExtendedEntry_IterInit(DEM_EVENT_INVALID, &(ExtendedDataIterator->ExtendedEntryIter));                                     /* SBSW_DEM_POINTER_FORWARD_ARGUMENT_STRUCT_MEMBER */
  ExtendedDataIterator->MemoryEntryId = DEM_MEMORYENTRY_HANDLE_INVALID;                                                          /* SBSW_DEM_POINTER_WRITE_ARGUMENT */
  ExtendedDataIterator->ExtendedEntryIndex = 0;                                                                                  /* SBSW_DEM_POINTER_WRITE_ARGUMENT */
  ExtendedDataIterator->EventId = DEM_EVENT_INVALID;                                                                             /* SBSW_DEM_POINTER_WRITE_ARGUMENT */
# endif
  ExtendedDataIterator->ExtendedDataNumber = 0;                                                                                  /* SBSW_DEM_POINTER_WRITE_ARGUMENT */
  ExtendedDataIterator->ExtendedRecordSelected = FALSE;                                                                          /* SBSW_DEM_POINTER_WRITE_ARGUMENT */
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SnapshotRecord_Init
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SnapshotRecord_Init(
  Dem_Cfg_DTCReadoutBuffer_SnapshotDataIteratorPtrType SnapshotDataIterator
  )
{
  Dem_Cfg_ComplexIterClear(&(SnapshotDataIterator->SourceIterator));                                                             /* SBSW_DEM_POINTER_FORWARD_ARGUMENT_STRUCT_MEMBER */
# if (DEM_CFG_SUPPORT_SRECS == STD_ON)
  Dem_SnapshotEntry_IteratorInit(DEM_EVENT_INVALID, DEM_MEMORYENTRY_HANDLE_INVALID, &(SnapshotDataIterator->SnapshotEntryIter)); /* SBSW_DEM_POINTER_FORWARD_ARGUMENT_STRUCT_MEMBER */
# endif
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetExtendedIteratorPtr
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(Dem_Cfg_DTCReadoutBuffer_ExtendedDataIteratorPtrType, DEM_CODE)
Dem_DTCReadoutBuffer_GetExtendedIteratorPtr(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  Dem_Cfg_DTCReadoutBuffer_ExtendedDataIteratorPtrType lReturnValue;

  Dem_Internal_AssertContinue(ReadoutBufferDataIndex < Dem_Cfg_SizeOfReadOutBufferData(), DEM_E_INCONSISTENT_STATE)

  lReturnValue = &(Dem_Cfg_GetReadoutBuffer(ReadoutBufferId).ReadOutBufferData[ReadoutBufferDataIndex].ExtendedIterator);

  return lReturnValue;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetExtendedIterator
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetExtendedIterator(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex,
  Dem_Cfg_DTCReadoutBuffer_ExtendedDataIteratorConstPtrType  ExtendedDataIterator
  )
{
  Dem_DTCReadoutBuffer_GetReadoutBufferDataSafe(                                                                                 /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER_READOUTBUFFERDATA */
    ReadoutBufferId, ReadoutBufferDataIndex)->ExtendedIterator = *ExtendedDataIterator;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetSnapshotIteratorPtr
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(Dem_Cfg_DTCReadoutBuffer_SnapshotDataIteratorPtrType, DEM_CODE)
Dem_DTCReadoutBuffer_GetSnapshotIteratorPtr(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  Dem_Cfg_DTCReadoutBuffer_SnapshotDataIteratorPtrType lReturnValue;

  Dem_Internal_AssertContinue(ReadoutBufferDataIndex < Dem_Cfg_SizeOfReadOutBufferData(), DEM_E_INCONSISTENT_STATE)
  lReturnValue = &(Dem_Cfg_GetReadoutBuffer(ReadoutBufferId).ReadOutBufferData[ReadoutBufferDataIndex].SnapshotIterator);

  return lReturnValue;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetSnapshotIterator
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetSnapshotIterator(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex,
  Dem_Cfg_DTCReadoutBuffer_SnapshotDataIteratorConstPtrType  SnapshotIterator
  )
{
  Dem_DTCReadoutBuffer_GetReadoutBufferDataSafe(                                                                                 /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER_READOUTBUFFERDATA */
    ReadoutBufferId, ReadoutBufferDataIndex)->SnapshotIterator = *SnapshotIterator;                                     
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetState
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(Dem_DTCReadoutBuffer_StatusType, DEM_CODE)
Dem_DTCReadoutBuffer_GetState(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId
  )
{
  return Dem_DTCReadoutBuffer_GetReadoutBuffer(ReadoutBufferId)->State;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetState()
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetState(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(uint8, AUTOMATIC)  State
  )
{
  Dem_DTCReadoutBuffer_GetReadoutBufferSafe(ReadoutBufferId)->State = State;                                                     /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER */                           
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetExtendedEntryIndex
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetExtendedEntryIndex(                                                                                      /* PRQA S 3219 */ /* MD_DEM_14.1_ACCESSOR */
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex,
  CONST(uint8, AUTOMATIC)  ExtendedEntryIndex
  )
{
  Dem_DTCReadoutBuffer_GetReadoutBufferDataSafe(                                                                                 /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER_READOUTBUFFERDATA */
    ReadoutBufferId, ReadoutBufferDataIndex)->ExtendedIterator.ExtendedEntryIndex = ExtendedEntryIndex;          
}
#endif

#if ( (DEM_CFG_SUPPORT_DCM == STD_ON) \
   && (DEM_CFG_SUPPORT_OBDII == STD_ON) \
   && (DEM_CFG_SUPPORT_OBDII_FREEZEFRAME_IN_SVC19 == STD_ON) )
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_CopyOBDFreezeFrame
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_CopyOBDFreezeFrame(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(Dem_EventIdType, AUTOMATIC)  EventId
  )
{
  uint8 lSelectedIndex;

  lSelectedIndex = Dem_Mem_FreezeFrameFindIndex(EventId);
  if (lSelectedIndex == Dem_Cfg_GlobalObdIIFFCount())
  {
    Dem_DTCReadoutBuffer_SetOBDData_IsStored(ReadoutBufferId, FALSE);
  }
  else
  {
    Dem_SharedDataPtrType lSourceEntry;
    Dem_DataPtrType lDestinationEntry;
    uint16 lObdFreezeFrameMaxSize;

    lSourceEntry = Dem_Mem_FreezeFrameObdIIGetDataPtr(lSelectedIndex);
    lDestinationEntry = Dem_DTCReadoutBuffer_GetPtrToOBDFreezeFrameBuffer(ReadoutBufferId);
    lObdFreezeFrameMaxSize = Dem_Cfg_GlobalObdFreezeFrameMaxSize();
    Dem_MemCpy((Dem_SharedDataPtrType)lDestinationEntry,                                                                         /* PRQA S 0602 */ /* MD_DEM_20.2 */ /* SBSW_DEM_POINTER_FORWARD_READOUTBUFFER_OBDFREEZEFRAME */
                    (Dem_SharedDataPtrType)lSourceEntry,
                    lObdFreezeFrameMaxSize);
    Dem_DTCReadoutBuffer_SetOBDData_IsStored(ReadoutBufferId, TRUE);
  }
}                                                                                                                                /* PRQA S 6050 */ /* MD_MSR_STCAL */
#endif

/*!
 * \}
 */
#define DEM_STOP_SEC_CODE
#include "MemMap.h"                                                                                                              /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define DEM_START_SEC_CODE
#include "MemMap.h"                                                                                                              /* PRQA S 5087 */ /* MD_MSR_19.1 */
/*!
 * \addtogroup Dem_DTCReadoutBuffer_Public
 * \{
 */

#if (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_TestCombinedEventSelected
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(boolean, DEM_CODE)
Dem_DTCReadoutBuffer_TestCombinedEventSelected(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId
  )
{
  boolean lReturnValue;
  lReturnValue = FALSE;

  if (Dem_Cfg_EventCombinedGroup(Dem_DTCReadoutBuffer_GetMasterEventId(ReadoutBufferId))
        != DEM_CFG_COMBINED_GROUP_INVALID)
  {
    lReturnValue = TRUE;
  }

  return lReturnValue;
}
#endif

#if ((DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON) && (DEM_CFG_SUPPORT_SRECS == STD_ON))
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_RecordNumberIteratorExists
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(boolean, DEM_CODE)
Dem_DTCReadoutBuffer_RecordNumberIteratorExists(
  CONSTP2CONST(Dem_DTCReadoutBuffer_RecordNumberIteratorType, AUTOMATIC, AUTOMATIC)  IterPtr
  )
{
  return (boolean)(IterPtr->mIdx < IterPtr->mEndIdx);
}
#endif

#if ((DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON) && (DEM_CFG_SUPPORT_SRECS == STD_ON))
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_RecordNumberIteratorGet
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(uint8, DEM_CODE)
Dem_DTCReadoutBuffer_RecordNumberIteratorGet(
  CONSTP2CONST(Dem_DTCReadoutBuffer_RecordNumberIteratorType, AUTOMATIC, AUTOMATIC)  IterPtr
  )
{
  DEM_IGNORE_UNUSED_CONST_ARGUMENT(IterPtr)                                                                                      /* PRQA S 3112 */ /* MD_DEM_14.2 */
  return Dem_Cfg_GetRecordIdOfRecordsOfCombinedDTCs(Dem_Cfg_GetRecordsOfCombinedDTCs_DTC2FF_Ind(IterPtr->mIdx));
}
#endif

#if ((DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON) && (DEM_CFG_SUPPORT_SRECS == STD_ON))
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetSnapShotHeaderBitMaskIndex
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
DEM_LOCAL_INLINE FUNC(uint8, DEM_CODE)
Dem_DTCReadoutBuffer_GetSnapShotHeaderBitMaskIndex(
  CONSTP2CONST(Dem_DTCReadoutBuffer_RecordNumberIteratorType, AUTOMATIC, AUTOMATIC)  IterPtr,
  CONST(Dem_Cfg_CombinedGroupIndexType, AUTOMATIC) CombinedGroupIdx
  )
{
  uint16 lCurrentIndex;
  uint16 lStartIndex;

  DEM_IGNORE_UNUSED_CONST_ARGUMENT(IterPtr)                                                                                      /* PRQA S 3112 */ /* MD_DEM_14.2 */
  DEM_IGNORE_UNUSED_CONST_ARGUMENT(CombinedGroupIdx)                                                                             /* PRQA S 3112 */ /* MD_DEM_14.2 */

  lCurrentIndex = (IterPtr->mIdx);
  lStartIndex = Dem_Cfg_GetRecordsOfCombinedDTCs_DTC2FF_IndStartIdxOfCombinedGroups(CombinedGroupIdx);

  /* The difference can only go from 0 to 31. Hence downcasting does not result in data loss */
  return (uint8)(lCurrentIndex - lStartIndex);
}
#endif

#if ((DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON) && (DEM_CFG_SUPPORT_SRECS == STD_ON)) 
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_RecordNumberIteratorInit
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_RecordNumberIteratorInit(
  CONSTP2VAR(Dem_DTCReadoutBuffer_RecordNumberIteratorType, AUTOMATIC, AUTOMATIC)  IterPtr,
  CONST(Dem_Cfg_CombinedGroupIndexType, AUTOMATIC) CombinedGroupIdx
  )
{
  DEM_IGNORE_UNUSED_CONST_ARGUMENT(CombinedGroupIdx)                                                                             /* PRQA S 3112 */ /* MD_DEM_14.2 */
  IterPtr->mIdx = Dem_Cfg_GetRecordsOfCombinedDTCs_DTC2FF_IndStartIdxOfCombinedGroups(CombinedGroupIdx);                         /* SBSW_DEM_POINTER_WRITE_ARGUMENT */
  IterPtr->mEndIdx = Dem_Cfg_GetRecordsOfCombinedDTCs_DTC2FF_IndEndIdxOfCombinedGroups(CombinedGroupIdx);                        /* SBSW_DEM_POINTER_WRITE_ARGUMENT */
}
#endif

#if ((DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON) && (DEM_CFG_SUPPORT_SRECS == STD_ON)) 
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_RecordNumberIteratorNext
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_RecordNumberIteratorNext(
  CONSTP2VAR(Dem_DTCReadoutBuffer_RecordNumberIteratorType, AUTOMATIC, AUTOMATIC) IterPtr
  )
{
  IterPtr->mIdx += 1;                                                                                                            /* SBSW_DEM_POINTER_WRITE_ARGUMENT */
}
#endif

#if ((DEM_CFG_SUPPORT_DCM == STD_ON)\
     && (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON)\
     && (DEM_CFG_SUPPORT_SRECS == STD_ON)) 
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetRecordNumberIterator
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(Dem_DTCReadoutBuffer_RecordNumberIteratorType, DEM_CODE)
Dem_DTCReadoutBuffer_GetRecordNumberIterator(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId
  )
{
  Dem_DTCReadoutBuffer_RecordNumberIteratorType lReturnValue;

  lReturnValue = Dem_DTCReadoutBuffer_GetReadoutBuffer(ReadoutBufferId)->RecordNumberIterator;

  return lReturnValue;
}
#endif

#if ((DEM_CFG_SUPPORT_DCM == STD_ON)\
     && (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON)\
     && (DEM_CFG_SUPPORT_SRECS == STD_ON)) 
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SetRecordNumberIterator
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SetRecordNumberIterator(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC)  ReadoutBufferId,
  CONSTP2CONST(Dem_DTCReadoutBuffer_RecordNumberIteratorType, AUTOMATIC, AUTOMATIC) RecordIterPtr
  )
{
  Dem_DTCReadoutBuffer_GetReadoutBufferSafe(ReadoutBufferId)->RecordNumberIterator = *RecordIterPtr;                             /* SBSW_DEM_ARRAY_POINTER_READOUTBUFFER */
}
#endif

#if ((DEM_CFG_SUPPORT_DCM == STD_ON) && (DEM_CFG_SUPPORT_GLOBAL_SRECS == STD_ON))
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_GetGlobalSnapshotDataPtr
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(Dem_DataPtrType, DEM_CODE)
Dem_DTCReadoutBuffer_GetGlobalSnapshotDataPtr(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  return &(Dem_DTCReadoutBuffer_GetReadoutBufferData(
             ReadoutBufferId, ReadoutBufferDataIndex)->Data.GlobalSnapshotData[0]);
}
#endif

#if ((DEM_CFG_SUPPORT_DCM == STD_ON) && (DEM_CFG_SUPPORT_GLOBAL_SRECS == STD_ON))
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_TestGlobalSnapshotStored
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
DEM_LOCAL_INLINE FUNC(boolean, DEM_CODE)
Dem_DTCReadoutBuffer_TestGlobalSnapshotStored(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex
  )
{
  return Dem_DTCReadoutBuffer_GetReadoutBufferData(
             ReadoutBufferId, ReadoutBufferDataIndex)->Data.GlobalSnapshotRecordStored;
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_Init
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_Init(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId
  )
{
  boolean lCombinedDTCSelected;

#if (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON)
  lCombinedDTCSelected = TRUE;
#else
  lCombinedDTCSelected = FALSE;
#endif

  Dem_DTCReadoutBuffer_SetReadoutBufferEntry(ReadoutBufferId, 
                                             DEM_EVENT_INVALID, 
                                             DEM_CFG_MEMORYID_INVALID,
                                             DEM_DTCREADOUTBUFFER_STATUS_UNUSED, 
                                             lCombinedDTCSelected); 
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_CheckAndSetState
 *****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(Std_ReturnType, DEM_CODE)
Dem_DTCReadoutBuffer_CheckAndSetState(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(Dem_EventIdType, AUTOMATIC)                 EventId,
  CONST(uint8, AUTOMATIC)                           MemoryId,
  CONST(Dem_DTCSelector_HandleType, AUTOMATIC)      DTCSelector
  )
{
  Std_ReturnType lReturnValue;
  boolean lCombinedDTCSelected;
# if (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON)
  Dem_Cfg_CombinedGroupIndexType lGroupId;        
# endif

  if (Dem_DTCReadoutBuffer_GetState(ReadoutBufferId) == DEM_DTCREADOUTBUFFER_STATUS_UNUSED)
  {
    /* new request */
    Dem_DTCSelector_LockSelectDTC(DTCSelector);

# if (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON)
    lGroupId = Dem_Cfg_EventCombinedGroup(EventId);
    if (lGroupId != DEM_CFG_COMBINED_GROUP_INVALID)
    {
      lCombinedDTCSelected = TRUE;
    }
    else
# endif
    {
      lCombinedDTCSelected = FALSE;
    }
    
    Dem_DTCReadoutBuffer_SetReadoutBufferEntry(ReadoutBufferId,
                                               EventId,
                                               MemoryId,
                                               DEM_DTCREADOUTBUFFER_STATUS_QUEUED,
                                               lCombinedDTCSelected);

    /* Scheduler task needs to be enabled last. This ensures readout buffer data are 
       written back to memory before executing the task.*/
    Dem_Scheduler_EnableTaskOnce(Dem_Scheduler_Task_FilterEventData);
    lReturnValue = DEM_PENDING;
  }
  else 
  if (
# if (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON)
      (EventId == Dem_DTCReadoutBuffer_GetMasterEventId(ReadoutBufferId))                                                        /* PRQA S 3415 */ /* MD_DEM_12.4_cf */
# else
      (EventId == Dem_DTCReadoutBuffer_GetEventId(ReadoutBufferId, DEM_DTCREADOUTBUFFER_DATAINDEX_DEFAULT))                      /* PRQA S 3415 */ /* MD_DEM_12.4_cf */
# endif
     && (MemoryId == Dem_DTCReadoutBuffer_GetMemoryId(ReadoutBufferId))                                                          
     )
  {
    /* repeated request with identical parameters */
    if (Dem_DTCReadoutBuffer_GetState(ReadoutBufferId) == DEM_DTCREADOUTBUFFER_STATUS_QUEUED)
    {
      /* request is still queued, copying of data is not finished */
      lReturnValue = DEM_PENDING;
    }
    else
    {
      /* copying of data is finished */
      lReturnValue = E_OK;
    }
  }
  else
  {
    /* repeated request with different parameters */
    lReturnValue = E_NOT_OK;
  }

  return lReturnValue;
}                                                                                                                                /* PRQA S 6050 */ /* MD_MSR_STCAL */
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
% Dem_DTCReadoutBuffer_FillData_NormalEvent
*****************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_FillData_NormalEvent(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(Dem_DTCSelector_HandleType, AUTOMATIC)      DTCSelector
)
{
  if (ReadoutBufferId != DEM_CFG_READOUTBUFFER_INVALID)
  {
    /* copy memory entry only for readout buffer in state QUEUED */
    if (Dem_DTCReadoutBuffer_GetState(ReadoutBufferId) == DEM_DTCREADOUTBUFFER_STATUS_QUEUED)
    {
      Dem_Cfg_MemoryIndexType lMemoryIndex;
      Dem_EventIdType lEventId;

      lEventId = Dem_DTCReadoutBuffer_GetEventId(ReadoutBufferId, DEM_DTCREADOUTBUFFER_DATAINDEX_DEFAULT);
# if (DEM_CFG_SUPPORT_OBDII_FREEZEFRAME_IN_SVC19 ==STD_ON) && (DEM_CFG_SUPPORT_OBDII == STD_ON)
      Dem_DTCReadoutBuffer_CopyOBDFreezeFrame(ReadoutBufferId, lEventId);
# endif

      lMemoryIndex = Dem_Memory_FindIndex(lEventId);
      if (lMemoryIndex == DEM_MEM_INVALID_MEMORY_INDEX)
      {
        Dem_MemoryEntry_InitializeEntry(Dem_DTCReadoutBuffer_GetMemoryEntryId(ReadoutBufferId, DEM_DTCREADOUTBUFFER_DATAINDEX_DEFAULT));
        Dem_EnterCritical_DcmApi();
        if (Dem_DTCReadoutBuffer_GetState(ReadoutBufferId) == DEM_DTCREADOUTBUFFER_STATUS_QUEUED)
        {
          Dem_DTCReadoutBuffer_SetState(ReadoutBufferId, DEM_DTCREADOUTBUFFER_STATUS_NOT_STORED);
          Dem_DTCSelector_ReleaseSelectDTC(DTCSelector);
        }
        Dem_LeaveCritical_DcmApi();
      }
      else
      {
        Dem_MemoryEntry_CopyEntry(Dem_DTCReadoutBuffer_GetMemoryEntryId(ReadoutBufferId, DEM_DTCREADOUTBUFFER_DATAINDEX_DEFAULT),
                                  Dem_MemoryEntry_GetId(lMemoryIndex));
        Dem_EnterCritical_DcmApi();
        if (Dem_DTCReadoutBuffer_GetState(ReadoutBufferId) == DEM_DTCREADOUTBUFFER_STATUS_QUEUED)
        {
          Dem_DTCReadoutBuffer_SetState(ReadoutBufferId, DEM_DTCREADOUTBUFFER_STATUS_IN_USE);
          Dem_DTCSelector_ReleaseSelectDTC(DTCSelector);
        }
        Dem_LeaveCritical_DcmApi();
      }
# if (DEM_FEATURE_NEED_TIME_SERIES == STD_ON)
      lMemoryIndex = Dem_Mem_TimeSeriesFindIndex(lEventId);
      if (lMemoryIndex == DEM_MEM_INVALID_MEMORY_INDEX)
      {
        /* no time series entry for event - there is no data to be copied */
        /* Mark the buffer as unused - this can be used to verify the validity of statistic records */
        Dem_Cfg_TimeSeriesReadoutBuffer.EventId = DEM_EVENT_INVALID;
        /* Initialize 'no record stored', this allows a common test for the validity of stored records */
        Dem_TimeSeriesData_EntryInit(&Dem_Cfg_TimeSeriesReadoutBuffer);                                                          /* SBSW_DEM_POINTER_FORWARD_GLOBAL */
      }
      else
      {
        /* copy time series entry */
        Dem_MemCpy((Dem_DataPtrType)(&Dem_Cfg_TimeSeriesReadoutBuffer),                                                          /* PRQA S 0310, 3305, 0602 */ /* MD_DEM_11.4_cpy, MD_DEM_3305, MD_DEM_20.2 */ /* SBSW_DEM_POINTER_MEMCPY_TIMESERIESREADOUTBUFFER */
          (Dem_NvDataPtrType)(Dem_Mem_TimeSeriesGetEntry(lMemoryIndex)),
          sizeof(Dem_Cfg_TimeSeriesReadoutBuffer));
      }
# endif
    }
  }
}                                                                                                                                /* PRQA S 6050 */ /* MD_MSR_STCAL */
#endif

#if ((DEM_CFG_SUPPORT_DCM == STD_ON) && (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON))
/* ****************************************************************************
% Dem_DTCReadoutBuffer_FillData_CombinedEvent
*****************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_FillData_CombinedEvent(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(Dem_DTCSelector_HandleType, AUTOMATIC)      DTCSelector
)
{ /* copy memory entry only for valid readout buffer in state QUEUED */
  if ((ReadoutBufferId != DEM_CFG_READOUTBUFFER_INVALID)                                                                         /* PRQA S 3415 */ /* MD_DEM_12.4_cf */
      && (Dem_DTCReadoutBuffer_GetState(ReadoutBufferId) == DEM_DTCREADOUTBUFFER_STATUS_QUEUED))
  {
    Dem_Cfg_MemoryIndexType lMemoryIndex;
    Dem_Cfg_CombinedGroupIndexType lGroupId;
    Dem_Cfg_CombinedGroupIterType lCombinedIter;
    Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType lReadoutBufferDataIndex;
    uint8 lReadoutBufferState;

    lReadoutBufferDataIndex = 0;
    lReadoutBufferState = DEM_DTCREADOUTBUFFER_STATUS_NOT_STORED;
    lGroupId = Dem_Cfg_EventCombinedGroup(Dem_DTCReadoutBuffer_GetMasterEventId(ReadoutBufferId));
      
    /* Iterate through all sub-events */
    for (Dem_Cfg_CombinedGroupIterInit(lGroupId, &lCombinedIter);                                                                /* SBSW_DEM_CALL_ITERATOR_POINTER */
          Dem_Cfg_CombinedGroupIterExists(&lCombinedIter) == TRUE;                                                               /* SBSW_DEM_CALL_ITERATOR_POINTER */
          Dem_Cfg_CombinedGroupIterNext(&lCombinedIter))                                                                         /* SBSW_DEM_CALL_ITERATOR_POINTER */
    {
      Dem_EventIdType lSubEventId;
      lSubEventId = Dem_Cfg_CombinedGroupIterGet(&lCombinedIter);                                                                /* SBSW_DEM_CALL_ITERATOR_POINTER */

# if (DEM_CFG_SUPPORT_OBDII_FREEZEFRAME_IN_SVC19 ==STD_ON) && (DEM_CFG_SUPPORT_OBDII == STD_ON)                                  /* COV_DEM_UNSUPPORTED_FEATURE_EVENTCOMBINATION_TYPE2_SVC_0x19 XF xf tf */
      /* Only one subevent can have OBDII freeze frame stored */
      if ((Dem_Cfg_EventObdRelated(lSubEventId) == TRUE)
          && (Dem_DTCReadoutBuffer_GetOBDData_IsStored(ReadoutBufferId) == FALSE))
      {
        Dem_DTCReadoutBuffer_CopyOBDFreezeFrame(ReadoutBufferId, lSubEventId);
      }
# endif

      if (Dem_DTC_TestStoredStatus(lSubEventId) == TRUE)
      {
        lMemoryIndex = Dem_Memory_FindIndex(lSubEventId);
        if ( (lMemoryIndex != DEM_MEM_INVALID_MEMORY_INDEX)                                                                      /* PRQA S 3415 */ /* MD_DEM_12.4_cf */
          && (lReadoutBufferDataIndex < Dem_Cfg_SizeOfReadOutBufferData()))
        {
          Dem_Cfg_MemoryEntryHandleType lReadoutBufferMemoryEntryId;
          lReadoutBufferMemoryEntryId = Dem_DTCReadoutBuffer_GetMemoryEntryId(
                                          ReadoutBufferId,
                                          lReadoutBufferDataIndex);
          Dem_MemoryEntry_CopyEntry(lReadoutBufferMemoryEntryId, Dem_MemoryEntry_GetId(lMemoryIndex));
          Dem_DTCReadoutBuffer_SetEventId(ReadoutBufferId,
                                          lReadoutBufferDataIndex,
                                          lSubEventId);
          lReadoutBufferDataIndex++;
        }
        else
        {
          /* Either there are more number of subevents stored 
              OR generated arrays are smaller than expected 
              OR a subevent memory entry was deleted during copying. */
          Dem_Internal_AssertAlways(DEM_E_INCONSISTENT_STATE)
        }
      }
    }
    Dem_DTCReadoutBuffer_SetNumberOfOccupiedSlots(ReadoutBufferId, lReadoutBufferDataIndex);
    if (lReadoutBufferDataIndex > 0)
    {
      lReadoutBufferState = DEM_DTCREADOUTBUFFER_STATUS_IN_USE;
    }

    /* Initialize all unused memory entries */
    while ((lReadoutBufferDataIndex < Dem_Cfg_SizeOfReadOutBufferData()))
    {
      Dem_Cfg_MemoryEntryHandleType lReadoutBufferMemoryEntryId;
      lReadoutBufferMemoryEntryId = Dem_DTCReadoutBuffer_GetMemoryEntryId(
                                      ReadoutBufferId,
                                      lReadoutBufferDataIndex);
      Dem_MemoryEntry_InitializeEntry(lReadoutBufferMemoryEntryId);
      lReadoutBufferDataIndex++;
    }

    /* Set new ReadoutBuffer state and release SelectDTC*/
    Dem_EnterCritical_DcmApi();
    if (Dem_DTCReadoutBuffer_GetState(ReadoutBufferId) == DEM_DTCREADOUTBUFFER_STATUS_QUEUED)
    {
      Dem_DTCReadoutBuffer_SetState(ReadoutBufferId, lReadoutBufferState);
      Dem_DTCSelector_ReleaseSelectDTC(DTCSelector);
    }
    Dem_LeaveCritical_DcmApi();
  }
}                                                                                                                                /* PRQA S 6050 */ /* MD_MSR_STCAL */
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
% Dem_DTCReadoutBuffer_FreeBuffer
*****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_FreeBuffer(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(Dem_DTCSelector_HandleType, AUTOMATIC)      DTCSelector
  )
{
  Dem_EnterCritical_DcmApi();
  if (Dem_DTCReadoutBuffer_GetState(ReadoutBufferId) == DEM_DTCREADOUTBUFFER_STATUS_QUEUED)
  {
    Dem_DTCSelector_ReleaseSelectDTC(DTCSelector);
  }
  Dem_DTCReadoutBuffer_SetState(ReadoutBufferId, DEM_DTCREADOUTBUFFER_STATUS_UNUSED);
  Dem_LeaveCritical_DcmApi();
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
 % Dem_DTCReadoutBuffer_SelectExtendedDataRecord
 *****************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SelectExtendedDataRecord(
CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
CONST(Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType, AUTOMATIC)  ReadoutBufferDataIndex,
CONST(uint8, AUTOMATIC) ExtendedDataNumber
)
{
  Dem_Cfg_DTCReadoutBuffer_ExtendedDataIteratorType lExtendedDataIterator;

  lExtendedDataIterator = *Dem_DTCReadoutBuffer_GetExtendedIteratorPtr(ReadoutBufferId, ReadoutBufferDataIndex);
# if (DEM_CFG_SUPPORT_ERECS == STD_ON) 
  {
    Dem_EventIdType lEventId;

    lEventId = Dem_DTCReadoutBuffer_GetEventId(ReadoutBufferId, ReadoutBufferDataIndex);
    Dem_ExtendedEntry_IterInit(lEventId, &(lExtendedDataIterator.ExtendedEntryIter));                                            /* SBSW_DEM_POINTER_FORWARD_STACK */
    lExtendedDataIterator.MemoryEntryId = Dem_DTCReadoutBuffer_GetMemoryEntryId(ReadoutBufferId, ReadoutBufferDataIndex);
    lExtendedDataIterator.ExtendedEntryIndex = 0;
    lExtendedDataIterator.EventId = lEventId;
  }
# endif
  lExtendedDataIterator.ExtendedDataNumber = ExtendedDataNumber;
  lExtendedDataIterator.ExtendedRecordSelected = TRUE;
  
  Dem_DTCReadoutBuffer_SetExtendedIterator(ReadoutBufferId, ReadoutBufferDataIndex, &lExtendedDataIterator);                    /* SBSW_DEM_POINTER_FORWARD_STACK */
                                                 
}
#endif

#if (DEM_CFG_SUPPORT_DCM == STD_ON)
/* ****************************************************************************
% Dem_DTCReadoutBuffer_SelectSnapshotRecord
*****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *****************************************************************************/
DEM_LOCAL_INLINE FUNC(void, DEM_CODE)
Dem_DTCReadoutBuffer_SelectSnapshotRecord(
  CONST(Dem_DTCReadoutBuffer_HandleType, AUTOMATIC) ReadoutBufferId,
  CONST(uint8, AUTOMATIC) RecordNumber
)
{
  Dem_DTCReadoutBuffer_ReadoutBufferDataIndexType lReadoutBufferDataIndex;
# if (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON)  
  uint8 lNumOccupiedReadoutBufferSlots;
# endif

  lReadoutBufferDataIndex = DEM_DTCREADOUTBUFFER_DATAINDEX_DEFAULT;
  Dem_DTCReadoutBuffer_SetSnapshotRecordNumber(ReadoutBufferId, RecordNumber);
  Dem_DTCReadoutBuffer_SetSnapshotRecordSource(ReadoutBufferId, Dem_DTCReadoutBuffer_SnapshotSource_Invalid);
  Dem_DTCReadoutBuffer_SetSnapshotRecordSelected(ReadoutBufferId, TRUE);

# if (DEM_CFG_SUPPORT_TYPE2_COMBINATION == STD_ON)  
  if (Dem_DTCReadoutBuffer_TestCombinedEventSelected(ReadoutBufferId) == TRUE)
  {
    lNumOccupiedReadoutBufferSlots = Dem_DTCReadoutBuffer_GetNumberOfOccupiedSlots(ReadoutBufferId);
    for ( /* Initialized to Number of occupied slots */;
         lReadoutBufferDataIndex < lNumOccupiedReadoutBufferSlots;
         lReadoutBufferDataIndex++)
    {
      Dem_DTCReadoutBuffer_InitializeSnapshotIterator(ReadoutBufferId, lReadoutBufferDataIndex);
    }
  }
  else
# endif
  {
    Dem_DTCReadoutBuffer_InitializeSnapshotIterator(ReadoutBufferId, lReadoutBufferDataIndex);
  }
}
#endif

/*!
 * \}
 */
#define DEM_STOP_SEC_CODE
#include "MemMap.h"                                                                                                              /* PRQA S 5087 */ /* MD_MSR_19.1 */

#endif /* DEM_DTCREADOUTBUFFER_IMPLEMENTATION_H */

/*!
 * \}
 */
/* ********************************************************************************************************************
 *  END OF FILE: Dem_DTCReadoutBuffer_Implementation.h
 *********************************************************************************************************************/
