/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 * 
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  EthTrcv_30_Ar7000_Types.h
 *        \brief  Ethernet transceiver driver QCA 7005 types header file
 *
 *      \details  Vector static code types header header file for the Ethernet transceiver driver QCA 7005 module.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file EthTrcv_30_Ar7000.h.
 *********************************************************************************************************************/

#if !defined (ETHTRCV_30_AR7000_TYPES_H)
# define ETHTRCV_30_AR7000_TYPES_H

/* PRQA S 0779 LONG_IDENTIFIERS */ /* MD_EthTrcv_30_Ar7000_0779 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "EthTrcv_GeneralTypes.h"
#include "Eth_GeneralTypes.h"
#include "EthTrcv_30_Ar7000_Cfg.h"

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
typedef enum
{
  ETHTRCV_30_AR7000_RT_OK      = E_OK,
  ETHTRCV_30_AR7000_RT_NOT_OK  = E_NOT_OK,
  ETHTRCV_30_AR7000_RT_PENDING = 2u
} EthTrcv_30_Ar7000_ReturnType;

/* PRQA S 0750 7 */ /* MD_EthTrcv_30_Ar7000_0750_0759 */
typedef union
{
  P2VAR(Eth_DataType, ETHTRCV_30_AR7000_VAR_NOINIT, ETHTRCV_30_AR7000_VAR_NOINIT) Native;
  P2VAR(uint32      , ETHTRCV_30_AR7000_VAR_NOINIT, ETHTRCV_30_AR7000_VAR_NOINIT) U32;
  P2VAR(uint16      , ETHTRCV_30_AR7000_VAR_NOINIT, ETHTRCV_30_AR7000_VAR_NOINIT) U16;
  P2VAR(uint8       , ETHTRCV_30_AR7000_VAR_NOINIT, ETHTRCV_30_AR7000_VAR_NOINIT) U8;
} EthTrcv_30_Ar7000_EthernetBufferUnionType;

typedef struct
{
  EthTrcv_30_Ar7000_EthernetBufferUnionType Buffer; /* PRQA S 0759 */ /* MD_EthTrcv_30_Ar7000_0750_0759 */
  uint16 BufLen;
} EthTrcv_30_Ar7000_EthernetBufferType;

#if ( ETHTRCV_30_AR7000_ENABLE_WRITE_DAK == STD_ON )
typedef uint8 EthTrcv_30_Ar7000_DakType[ETHTRCV_30_AR7000_DAK_SIZE_BYTE];
#endif /* ( ETHTRCV_30_AR7000_ENABLE_WRITE_DAK == STD_ON ) */

typedef uint8 EthTrcv_30_Ar7000_ConfigType;

#if ( ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API == STD_ON )
typedef uint8 EthTrcv_30_Ar7000_PBActionType;
#define ETHTRCV_30_AR7000_PBACTION_JOIN                     1U
#define ETHTRCV_30_AR7000_PBACTION_LEAVE                    2U
#define ETHTRCV_30_AR7000_PBACTION_STATUS                   3U
#endif /* ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API */

#if ((defined ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API ))
typedef struct 
{
  uint8  Option;
  uint32 PropVersion;
  uint32 PropId;
  uint32 PropDataLength;
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_CONST, ETHTRCV_30_AR7000_APPL_DATA) PropData;
} EthTrcv_30_Ar7000_SetPropertyParamterType;

typedef uint8 EthTrcv_30_Ar7000_SetPropType;
/* Set Property MStatus return values */
#define ETHTRCV_30_AR7000_SETPROP_SUCCESS                        0x00U
#define ETHTRCV_30_AR7000_SETPROP_FAILURE                        0x01U
#define ETHTRCV_30_AR7000_SETPROP_PROP_NOT_SUPPORTED             0x02U
#define ETHTRCV_30_AR7000_SETPROP_VER_NOT_SUPPORTED              0x03U
#define ETHTRCV_30_AR7000_SETPROP_PROP_NOT_PERSISTENT            0x04U
#define ETHTRCV_30_AR7000_SETPROP_FLASH_FAILURE                  0x05U
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API */

#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && ( STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC ))
typedef uint8  EthTrcv_30_Ar7000_Slac_StartModeType;
/* validation modes */
#define ETHTRCV_30_AR7000_SLAC_START_MODE_NO_VALID_WHEN_POT_FOUND  0x00U
#define ETHTRCV_30_AR7000_SLAC_START_MODE_VALID_WHEN_POT_FOUND     0x01U
#define ETHTRCV_30_AR7000_SLAC_START_MODE_VALID_ANYTIME            0x03U
#define ETHTRCV_30_AR7000_SLAC_START_MODE_FORCE_VALID              0x04U
#define ETHTRCV_30_AR7000_SLAC_START_MODE_SKIP_VALID               0x08U
#define ETHTRCV_30_AR7000_SLAC_START_MODE_LOCAL_AMP_MAP_EXCH       0x10U
#define ETHTRCV_30_AR7000_SLAC_START_MODE_MATCH_ON_VALID_FAILED    0x20U
#define ETHTRCV_30_AR7000_SLAC_START_MODE_NEVER_VALIDATE           0x40U
#define ETHTRCV_30_AR7000_SLAC_START_MODE_AMP_MAP_TRIG_BY_EVSE     0x80U

typedef uint8  EthTrcv_30_Ar7000_Slac_AssocStepType;
/* SLAC association step numbers (already bit shifted) */
#define ETHTRCV_30_AR7000_SLAC_STEP_1_SET_CCO                      0x01U
#define ETHTRCV_30_AR7000_SLAC_STEP_2_SET_PRIO                     0x02U
#define ETHTRCV_30_AR7000_SLAC_STEP_3_PARAM_REQ                    0x03U
#define ETHTRCV_30_AR7000_SLAC_STEP_4_SOUNDING                     0x04U
#define ETHTRCV_30_AR7000_SLAC_STEP_5_SELECT_EVSE                  0x05U
#define ETHTRCV_30_AR7000_SLAC_STEP_6_VALIDATE_REQ                 0x06U
#define ETHTRCV_30_AR7000_SLAC_STEP_7_EV_NOT_READY                 0x07U
#define ETHTRCV_30_AR7000_SLAC_STEP_8_VALID_FAILED                 0x08U
#define ETHTRCV_30_AR7000_SLAC_STEP_9_MATCH_REQ                    0x09U
#define ETHTRCV_30_AR7000_SLAC_STEP_10_SET_KEY_REQ                 0x0AU
#define ETHTRCV_30_AR7000_SLAC_STEP_11_NO_LINK                     0x0BU
#define ETHTRCV_30_AR7000_SLAC_STEP_12_AMP_MAP_REQ                 0x0CU
#define ETHTRCV_30_AR7000_SLAC_STEP_13_REMOTE_AMP_MAP              0x0DU
#define ETHTRCV_30_AR7000_SLAC_STEP_14_NO_LINK_WITH_AMP_MAP        0x0EU

#define ETHTRCV_30_AR7000_SLAC_E_ASSOC_STEP_SHIFT                     4U
#define ETHTRCV_30_AR7000_SLAC_E_ASSOC_STEP_SHIFTED_MASK           0xF0U

typedef uint8  EthTrcv_30_Ar7000_Slac_AssocStatusType;
/* association errors */
#define ETHTRCV_30_AR7000_SLAC_E_NO_ERROR                          0x00U
#define ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE                  0x01U
#define ETHTRCV_30_AR7000_SLAC_E_TIMEOUT                           0x02U
#define ETHTRCV_30_AR7000_SLAC_E_NO_DIRECT                         0x03U
#define ETHTRCV_30_AR7000_SLAC_E_MULTIPLE_DIRECT                   0x04U
#define ETHTRCV_30_AR7000_SLAC_E_NO_DIRECT_OR_INDIRECT             0x05U
#define ETHTRCV_30_AR7000_SLAC_E_VALIDATION_NO_SUCCESS             0x06U
#define ETHTRCV_30_AR7000_SLAC_E_EVSE_FAILURE                      0x07U
#define ETHTRCV_30_AR7000_SLAC_E_EVSE_NOT_READY                    0x08U
#define ETHTRCV_30_AR7000_SLAC_E_AMP_MAP                           0x09U
#define ETHTRCV_30_AR7000_SLAC_E_PREPARE_VALIDATION                0x0AU
#define ETHTRCV_30_AR7000_SLAC_E_NO_VALIDATION_SUPPORT             0x0BU
#define ETHTRCV_30_AR7000_SLAC_E_REMOTE_AMP_MAP_RCVD               0x0CU

#define ETHTRCV_30_AR7000_SLAC_E_ASSOC_STATUS_MASK                 0x0FU

typedef uint8  EthTrcv_30_Ar7000_Slac_DiscoverStatusType;
/* discover status type */
#define ETHTRCV_30_AR7000_SLAC_EVSE_FOUND                          0x00U
#define ETHTRCV_30_AR7000_SLAC_EVSE_NOT_FOUND                      0x01U
#define ETHTRCV_30_AR7000_SLAC_EVSE_POTENTIALLY_FOUND              0x02U
#define ETHTRCV_30_AR7000_SLAC_EVSE_CONNECTED                      0x03U

typedef struct 
{
  EthTrcv_PhysAddrType SlacEvseMacAddr;
  uint8 SlacAverageAtten;
  boolean SlacAttenCharRecvd;
  uint8 SlacTogglesDetected;
  EthTrcv_30_Ar7000_Slac_DiscoverStatusType SlacDiscoveryStatus;
} EthTrcv_30_Ar7000_Slac_AttenCharProfileType;

typedef enum EthTrcv_30_Ar7000_Slac_StateTypeEnum
{
  ETHTRCV_30_AR7000_SLAC_STATE_PARM_DISCOVERY_REQ,
  ETHTRCV_30_AR7000_SLAC_STATE_PARM_DISCOVERY_CNF,
  ETHTRCV_30_AR7000_SLAC_STATE_START_ATTEN_CHAR_IND,
  ETHTRCV_30_AR7000_SLAC_STATE_SOUNDING_IND,
  ETHTRCV_30_AR7000_SLAC_STATE_ATTEN_CHAR_IND,
  ETHTRCV_30_AR7000_SLAC_STATE_ATTEN_CHAR_RSP,
  ETHTRCV_30_AR7000_SLAC_STATE_MATCH_REQ,
  ETHTRCV_30_AR7000_SLAC_STATE_MATCH_CNF,
  ETHTRCV_30_AR7000_SLAC_STATE_SET_KEY_REQ,
  ETHTRCV_30_AR7000_SLAC_STATE_SET_KEY_CNF,
  ETHTRCV_30_AR7000_SLAC_STATE_COMPLETE
} EthTrcv_30_Ar7000_Slac_StateType;

typedef struct EthTrcv_30_Ar7000_Slac_Msg_ParmDiscoveryCnfTypeStruct
{
  uint8 SlacRunID[8];
  uint8 SlacNumSounds;
  uint8 SlacTimeOut;
  uint8 SlacRespType;
  uint8 SlacForwardingSta[6];
  /* Overall size: 17 bytes */
}  EthTrcv_30_Ar7000_Slac_Msg_ParmDiscoveryCnfType;


typedef struct EthTrcv_30_Ar7000_Slac_Msg_StartAttenCharIndTypeStruct
{
  uint8 SlacRunID[8];
  uint8 SlacNumSounds;
  uint8 SlacTimeOut;
  uint8 SlacRespType;
  uint8 SlacForwardingSta[6];
  /* Overall size: 17 bytes */
}  EthTrcv_30_Ar7000_Slac_Msg_StartAttenCharIndType;


typedef struct EthTrcv_30_Ar7000_Slac_Msg_SoundingIndTypeStruct
{
  uint8 SlacRunID[8];
  uint8 SlacCnt;
  /* Overall size: 9 bytes */
}  EthTrcv_30_Ar7000_Slac_Msg_SoundingIndType;
  

typedef struct EthTrcv_30_Ar7000_Slac_Msg_AttenCharIndTypeStruct
{
  uint8 SlacRunID[8];
  uint8 SlacSourceAddr[6];
  uint8 SlacSourceId[17];
  uint8 SlacRespId[17];
  uint8 SlacNumSounds;
  uint8 SlacNumGroups;
  uint8 SlacAag[58];
  /* Overall size: 108 bytes */
}  EthTrcv_30_Ar7000_Slac_Msg_AttenCharIndType;

typedef struct EthTrcv_30_Ar7000_Slac_Msg_AttenCharRspTypeStruct
{
  uint8 SlacRunID[8];
  uint8 SlacSourceAddr[6];
  uint8 SlacSourceId[17];
  uint8 SlacRespId[17];
  uint8 SlacResult;
  /* Overall size: 49 bytes */
}  EthTrcv_30_Ar7000_Slac_Msg_AttenCharRspType;

typedef struct EthTrcv_30_Ar7000_Slac_Msg_MatchReqTypeStruct
{
  uint8 SlacRunID[8];
  uint8 SlacPevId[17];
  uint8 SlacPevMac[6];
  uint8 SlacEvseId[17];
  uint8 SlacEvseMac[6];
  /* Overall size: 77 bytes */
}  EthTrcv_30_Ar7000_Slac_Msg_MatchReqType;

typedef struct EthTrcv_30_Ar7000_Slac_Msg_MatchCnfTypeStruct
{
  uint8 SlacRunID[8];
  uint8 SlacPevId[17];
  uint8 SlacPevMac[6];
  uint8 SlacEvseId[17];
  uint8 SlacEvseMac[6];
  uint8 SlacNid[7];
  uint8 SlacNmk[16];
  /* Overall size: 77 bytes */
}  EthTrcv_30_Ar7000_Slac_Msg_MatchCnfType;

#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */

#if ( ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)) || \
      ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)          && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK))          || \
      ((defined ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)      && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)))     || \
      ((defined ETHTRCV_30_AR7000_CALC_NMK_LOCALLY)          && (STD_ON == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY))
typedef struct EthTrcv_30_Ar7000_NVMHeader 
{
  uint16 MajorVersion; 
  uint16 MinorVersion;
  uint32 AppletExecutionmask;
  uint32 ImageNvmAddress;
  uint32 ImageMemoryAddress;
  uint32 ImageLength;
  uint32 ImageChecksum;
  uint32 AppletEntryPtr;
  uint32 NextNvmHeaderPtr;
  uint32 PreviousNvmHeaderPtr;
  uint32 EntryType;
  uint16 ModuleId;
  uint16 ModuleSubId;
  uint16 AppletEntryVersion;
  uint16 Reserved0;
  uint32 Reserved1;
  uint32 Reserved2;
  uint32 Reserved3;
  uint32 Reserved4;
  uint32 Reserved5;
  uint32 Reserved6;
  uint32 Reserved7;
  uint32 Reserved8;
  uint32 Reserved9;
  uint32 Reserved10;
  uint32 Reserved11;
  uint32 HeaderChecksum;
} EthTrcv_30_Ar7000_NVMHeaderType;
#endif /* (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) ||
          (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)          ||
          (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK))     ||
          (STD_ON == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY) */


/* PRQA L:LONG_IDENTIFIERS */ /* MD_EthTrcv_30_Ar7000_0779 */

#endif /* ETHTRCV_30_AR7000_TYPES_H */

/**********************************************************************************************************************
 *  END OF FILE: EthTrcv_30_Ar7000_Types.h
 *********************************************************************************************************************/

