/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  EthTrcv_30_Ar7000.c
 *        \brief  Ethernet transceiver driver QCA 7005 main source file
 *
 *      \details  Vector static code implementation for the Ethernet transceiver driver QCA 7005 module.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file EthTrcv_30_Ar7000.h.
 *********************************************************************************************************************/

#define ETHTRCV_30_AR7000_SOURCE

/*lint -e438 */ /* Suppress ID438 because Config pointer only used in Post-Build Variant */
/*lint -e451 */ /* Suppress ID451 because MemMap.h cannot use a include guard */
/*lint -e506 */ /* Suppress ID506 due to MD_MSR_14.1 */
/*lint -e537 */ /* Suppress ID537 due to MD_MSR_19.1 */
/*lint -e652 */ /* Used for Vector API optimization */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 ETHTRCV_30_AR7000_C_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "EthTrcv_30_Ar7000.h"
#include "EthTrcv_30_Ar7000_Priv.h"
#include "EthTrcv_30_Ar7000_Prot.h"
#include "EthIf.h"
#include "IpBase.h"

#if ( ( (ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON) && (ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON) ) || \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_SM_HOOK_ROUTINE ) )
#include "EthTrcv_30_Ar7000_Cbk.h"
#endif

#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC )
#include "EthTrcv_30_Ar7000_Slac.h"
#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */

/* PRQA L:ETHTRCV_30_AR7000_C_IF_NESTING */ /* MD_MSR_1.1_828 */

/**********************************************************************************************************************
 *  Binary Includes
 *********************************************************************************************************************/
#if (defined ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD )
#include "_EthTrcv_30_Ar7000_Memctrl.h"
#include "_EthTrcv_30_Ar7000_Firmware.h"
#include "_EthTrcv_30_Ar7000_Pib.h"
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/
/* Check consistency of source and header file. */
#if ( (ETHTRCV_30_AR7000_SW_MAJOR_VERSION != 8U) || \
      (ETHTRCV_30_AR7000_SW_MINOR_VERSION != 0U) || \
      (ETHTRCV_30_AR7000_SW_PATCH_VERSION != 0U) )
  #error "EthTrcv_30_Ar7000.c: Source and Header file are inconsistent!"
#endif

/**************************************************************************************************
* Local defines
**************************************************************************************************/
#define ETHTRCV_30_AR7000_NO_WAIT_ON_CONF                0x00U
#define ETHTRCV_30_AR7000_WAIT_ON_CONF                   0x01U

#define ETHTRCV_30_AR7000_STATE_NOT_READY                0x00U
#define ETHTRCV_30_AR7000_STATE_READY                    0x01U

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
#define ETHTRCV_30_AR7000_DOWNLSTATUS_MEMCTRL               0U
#define ETHTRCV_30_AR7000_DOWNLSTATUS_PIB                   1U
#define ETHTRCV_30_AR7000_DOWNLSTATUS_FW                    2U
#define ETHTRCV_30_AR7000_DOWNLSTATUS_DONE                  3U

#define ETHTRCV_30_AR7000_NO_HST_ACTION_REQUIRED         0xFFU
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */


/* ETHTRCV Module Operation state */
#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ) || \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_MODOP_API ) || \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_READ_NMK )
#define ETHTRCV_30_AR7000_MODOP_STATE_NOT_USED           0x00U
#define ETHTRCV_30_AR7000_MODOP_STATE_USED               0x01U
#endif /* ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ||
          ETHTRCV_30_AR7000_ENABLE_MODOP_API ||
          ETHTRCV_30_AR7000_ENABLE_READ_NMK */

#ifndef ETHTRCV_30_AR7000_LOCAL
#define ETHTRCV_30_AR7000_LOCAL static
#endif /* ETHTRCV_30_AR7000_LOCAL */

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
typedef P2FUNC(void, ETHTRCV_30_AR7000_CODE, EthTrcv_30_Ar7000_FailedApiCallType) \
                                    (uint8, P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA));

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
typedef struct
{
# if ((defined ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD) && (STD_OFF==ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD))
  boolean NeedToSend;
# endif /* ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD */
  uint32 TotalLen;
  uint32 CurPartLen;
  uint32 CurPartDestAddr;
  uint32 StartAddr;
  P2CONST(Eth_DataType, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr;
  uint32 Flags;
  uint32 Checksum;
} EthTrcv_30_Ar7000_MME_WaEA_ParamType;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

/**********************************************************************************************************************
 *  GLOBAL DATA
 **********************************************************************************************************************/
#define ETHTRCV_30_AR7000_START_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#if ( (STD_ON==ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP) )
VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_WillResetAfterPibUpload;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD && ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP */

#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC )
VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_FwReadyForSlacLastResult;
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC */

#define ETHTRCV_30_AR7000_STOP_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#define ETHTRCV_30_AR7000_START_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
VAR(EthTrcv_StateType, ETHTRCV_30_AR7000_VAR_ZERO_INIT) EthTrcv_30_Ar7000_State = ETHTRCV_STATE_UNINIT;
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON */

#define ETHTRCV_30_AR7000_STOP_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *  LOCAL DATA
 **********************************************************************************************************************/
#define ETHTRCV_30_AR7000_START_SEC_CONST_8BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

CONST(uint8, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_AtherosODA[6] = {
  0x00, 0xb0, 0x52, 0x00, 0x00, 0x01
};

#define ETHTRCV_30_AR7000_STOP_SEC_CONST_8BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#define ETHTRCV_30_AR7000_START_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
/* PRQA S 3218 1 */ /* MD_Ar7000_3218 */
#if (((defined ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP)) || \
     ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)) || \
     ((defined ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)) || \
     ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_DAK) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK))|| \
     ((defined ETHTRCV_30_AR7000_CALC_NMK_LOCALLY) && (STD_ON == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY)) || \
     ((defined ETHTRCV_30_AR7000_ENABLE_SET_PIB_DATA_API) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SET_PIB_DATA_API)))
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_PibRamCpy[ETHTRCV_30_AR7000_PIB_SIZE];
ETHTRCV_30_AR7000_LOCAL VAR(boolean, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_PibRamCpyValid;
#endif /* ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP         ||
          ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS ||
          ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK      ||
          ETHTRCV_30_AR7000_ENABLE_WRITE_DAK          ||
          ETHTRCV_30_AR7000_CALC_NMK_LOCALLY          ||
          ETHTRCV_30_AR7000_ENABLE_SET_PIB_DATA_API */

#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#if ( ETHTRCV_30_AR7000_ENABLE_READ_NMK == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_Nmk[ETHTRCV_30_AR7000_NMK_SIZE_BYTE];
#endif /* ETHTRCV_30_AR7000_ENABLE_READ_NMK */

#if ( ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_SwVersion[48];
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_SwMStatus;
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION */

#if ( ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_SetPropertyStatus;
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API */

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON ) || \
    ( ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP == STD_ON )
/* PRQA S 3218 1 */ /* MD_EthTrcv_30_Ar7000_3218_2 */
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_LastFrHeader[ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE];
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD ||
          ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP */

#define ETHTRCV_30_AR7000_STOP_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define ETHTRCV_30_AR7000_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
/* PRQA S 0779 1 */ /* MD_EthTrcv_30_Ar7000_0779 */
ETHTRCV_30_AR7000_LOCAL VAR(EthTrcv_30_Ar7000_MME_WaEA_ParamType, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_MME_WaEA_Params;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON ) || \
    ( ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP == STD_ON )
ETHTRCV_30_AR7000_LOCAL EthTrcv_30_Ar7000_FailedApiCallType EthTrcv_30_Ar7000_FailedApiCall;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD ||
          ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP */

#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ) || \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_MODOP_API ) || \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_READ_NMK )
ETHTRCV_30_AR7000_LOCAL VAR (EthTrcv_30_Ar7000_ModOp_Rd_Params, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_ModOpParams;
#endif /* ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ||
          ETHTRCV_30_AR7000_ENABLE_MODOP_API  ||
          ETHTRCV_30_AR7000_ENABLE_READ_NMK */

#if (defined ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD )
ETHTRCV_30_AR7000_LOCAL P2CONST(uint8, ETHTRCV_30_AR7000_VAR_NOINIT,  ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_PIBData;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#define ETHTRCV_30_AR7000_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#define ETHTRCV_30_AR7000_START_SEC_VAR_NOINIT_32BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

ETHTRCV_30_AR7000_LOCAL VAR(uint32, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_CycleCnt;

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint32, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_ServerSid;
ETHTRCV_30_AR7000_LOCAL VAR(uint32, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_ClientSid;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */


#define ETHTRCV_30_AR7000_STOP_SEC_VAR_NOINIT_32BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#define ETHTRCV_30_AR7000_START_SEC_VAR_NOINIT_16BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */


#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
# if ( ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint16, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_FwDownloadAndStartCnt;
# endif /* ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT */
# if ( ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_SLAC_COM_EVENT == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint16, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_FwReadyForPlcComCnt;
# endif /* ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_SLAC_COM_EVENT */
ETHTRCV_30_AR7000_LOCAL VAR(uint16, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_LinkStateLockCnt;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

ETHTRCV_30_AR7000_LOCAL VAR(uint16, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_HeartBeatCnt;

#if ( ETHTRCV_30_AR7000_ENABLE_READ_NMK == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint16, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_ReadNmkCycCnt;
#endif /* ETHTRCV_30_AR7000_ENABLE_READ_NMK */

#if ( ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint16, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_SCConfCycCnt;
#endif /* ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API */

#if ( ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint16, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_SetKeyConfCycCnt;
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_NMK */

#if ( ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint16, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_GetSwVersionCycCnt;
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION */

#if ( ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint16, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_SetPropertyCycCnt;
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API */

#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ) || \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_MODOP_API ) || \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_READ_NMK )
ETHTRCV_30_AR7000_LOCAL VAR(uint16, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_ModOpCycCnt;
#endif /* ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ||
          ETHTRCV_30_AR7000_ENABLE_MODOP_API  ||
          ETHTRCV_30_AR7000_ENABLE_READ_NMK */

#define ETHTRCV_30_AR7000_STOP_SEC_VAR_NOINIT_16BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#define ETHTRCV_30_AR7000_START_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

ETHTRCV_30_AR7000_LOCAL VAR(EthTrcv_LinkStateType, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_LinkState;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_DwldAttempts;
ETHTRCV_30_AR7000_LOCAL VAR(EthTrcv_ModeType, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_ModeState;

#if ( ETHTRCV_30_AR7000_ENABLE_READ_NMK == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_IsReadingNmk;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_NmkReady;
#endif /* ETHTRCV_30_AR7000_ENABLE_READ_NMK */

#if ( ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_IsChangingSCState;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_SCStateConfReady;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_PbAvlnStatus;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_PbMStatus;
#endif /* ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API */

#if ( ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_SetKeyIsChanging;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_SetKeyCnt;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_SetKeyConfReady;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_SetKeyMStatus;
ETHTRCV_30_AR7000_LOCAL VAR(EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_SetKeyRetVal;
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_NMK */

#if ( ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_IsReadingSwVersion;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_SWVersionAvailable;
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION */

#if ( ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_IsSettingProperty;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_SetPropertyCnfAvailable;
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API */

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_DownlStatus;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_ApiCallRetCnt;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_WriteAppletLockCnt;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_AppletNrLocked;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_HstActionIdx;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_HstActionCnt;
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_HstAction[ETHTRCV_30_AR7000_HST_ACTION_QUEUE_SIZE];
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START )
  ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_FwDwldStartCbkCalled;
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START */

#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_START )
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_FwStartedCbkCalled;
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_START */

#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC )
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_FwReadyForSlacCbkCalled;
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC */

#if ( ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP == STD_ON ) || \
    ( ETHTRCV_30_AR7000_ENABLE_READ_NMK == STD_ON ) ||\
    ( ETHTRCV_30_AR7000_ENABLE_MODOP_API == STD_ON )
ETHTRCV_30_AR7000_LOCAL VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_ModOpRetryCnt;
#endif /* ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ||
          ETHTRCV_30_AR7000_ENABLE_READ_NMK   ||
          ETHTRCV_30_AR7000_ENABLE_MODOP_API*/

#define ETHTRCV_30_AR7000_STOP_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/
#define ETHTRCV_30_AR7000_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#if ( ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION == STD_ON )
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_GetSwVersion
 **********************************************************************************************************************/
/*!
 * \brief       Sends a get version request MME to the QCA.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \return      E_OK           The host has successfully send the request MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_GetSwVersion(
  uint8 TrcvIdx);
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION */

#if (ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API == STD_ON)
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_SetProperty
 **********************************************************************************************************************/
/*!
 * \brief       Sends a set property request MME to the QCA.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   SetPropParams  Pointer to valid property parameter block
 * \return      E_OK           The host has successfully send the request MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_SetProperty(
  uint8  TrcvIdx,
  CONSTP2CONST(EthTrcv_30_Ar7000_SetPropertyParamterType,ETHTRCV_30_AR7000_APPL_CONST, ETHTRCV_30_AR7000_APPL_CONST)
    SetPropParams);
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API */

#if (   (   (ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON) \
         && (ETHTRCV_30_AR7000_CALC_NMK_LOCALLY == STD_OFF) ) \
     || (ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API == STD_ON) )
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_SetKey
 **********************************************************************************************************************/
/*!
 * \brief       Sends a set key request MME to the QCA.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   NmkPtr         Pointer to network membership key
 * \return      E_OK           The host has successfully send the request MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SET_NMK &&
 *              ((ETHTRCV_30_AR7000_CALC_NMK_LOCALLY == STD_OFF) || ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API)
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_SetKey(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) NmkPtr);
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetKeyCnfStatus
 **********************************************************************************************************************/
/*!
 * \brief       Read 'set network membership key' result.
 * \details     Rx-Handler for 'set key' confirm MME received from the QCA.
 * \param[in]   TrcvIdx         Zero based index of the transceiver
 * \param[in]   DataPtr         Received Ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SET_NMK &&
 *              ((ETHTRCV_30_AR7000_CALC_NMK_LOCALLY == STD_OFF) || ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API)
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetKeyCnfStatus(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr);
#endif /* (   (   (ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON) \
               && (ETHTRCV_30_AR7000_CALC_NMK_LOCALLY == STD_OFF) ) \
           || (ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API == STD_ON) ) */

#if ( ETHTRCV_30_AR7000_ENABLE_RESET_DEVICE == STD_ON )
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_ResetDevice
 **********************************************************************************************************************/
/*!
 * \brief       Sends a reset device request MME to the QCA
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \return      E_OK           The host has successfully send the request MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_RESET_DEVICE
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_ResetDevice(
  uint8 TrcvIdx);
#endif /* ETHTRCV_30_AR7000_ENABLE_RESET_DEVICE */

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetLinkStatus
 **********************************************************************************************************************/
/*!
 * \brief       Read the QCA link-state.
 * \details     Rx-Handler for 'powerline link status' confirm MME received from the QCA. Read the link-state and
 *              reset the heart beat counter if it up.
 * \param[in]   TrcvIdx         Zero based index of the transceiver
 * \param[in]   DataPtr         Received Ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetLinkStatus(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr);

#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API )
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_PBEncryption
 **********************************************************************************************************************/
/*!
 * \brief       Sends a 'push button encryption' request MME to the QCA.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   PBAction       Push Button Action
 * \return      E_OK           The host has successfully send the request MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_PBEncryption(
  uint8 TrcvIdx,
  uint8 PBAction);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_PBEncryption
 **********************************************************************************************************************/
/*!
 * \brief       Triggers a push button encryption MME transmission.
 * \details     Triggering a push button encryption MME request if no request is pending. The function is also used to
 *              poll the communication status and to get the QCA confirmation result.
 * \param[in]   TrcvIdx          Zero based index of the transceiver
 * \param[in]   PBAction         Push Button Action
 *                               range:
 *                                 1=Start Simple Connect
 *                                 2=Randomize NMK
 *                                 3=Read AVLN Membership Status)
 *                                 4=Reset Factory Default
 * \param[out]  MStatus          Received MME confirmation status
 * \param[out]  AVLNStatus       HomePlug AV Logical-Network status
 * \return      ETHTRCV_30_AR7000_RT_OK      Pushbutton encryption action was successful
 * \return      ETHTRCV_30_AR7000_RT_NOT_OK  Pushbutton encryption timeout was expired or a failure occurred during
 *                                           MME sending
 * \return      ETHTRCV_30_AR7000_RT_PENDING QCA MME confirmation is pending
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API
 */
ETHTRCV_30_AR7000_LOCAL FUNC(EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_PBEncryption(
        uint8                                                          TrcvIdx,
        EthTrcv_30_Ar7000_PBActionType                                 PBAction,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) MStatus,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) AVLNStatus);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetPBEncryptionStatus
 **********************************************************************************************************************/
/*!
 * \brief       Reads 'push button encryption' MME result.
 * \details     Rx-Handler for 'push button encryption' confirm MME received from the QCA.
 * \param[in]   TrcvIdx         Zero based index of the transceiver
 * \param[in]   DataPtr         Received Ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetPBEncryptionStatus(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr);
#endif /* ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API */

#if (   (   (ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON) \
         && (ETHTRCV_30_AR7000_CALC_NMK_LOCALLY == STD_OFF) ) \
     || (ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API == STD_ON) )
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetKeyStatus
 **********************************************************************************************************************/
/*!
 * \brief       Send a set key message or check status
 * \details     If this API is called the first time, it does generate a random NMK and transmits it within a
 *              VS_SET_KEY.REQ MME message (returns ETHTRCV_30_AR7000_RT_PENDING on success). On subsequent calls it
 *              does check status of the previous transmission (returns ETHTRCV_30_AR7000_RT_PENDING until response
 *              is received). As soon as the response from the QCA7005 has been received, it returns
 *              ETHTRCV_30_AR7000_RT_OK. In that case the MStatus parameter can be checked for
 *              success.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[out]  MStatus        MME response message status
 * \return      ETHTRCV_30_AR7000_RT_OK      Reset was successful
 * \return      ETHTRCV_30_AR7000_RT_NOT_OK  Reset timeout was expired or a failure occurred during MME sending
 * \return      ETHTRCV_30_AR7000_RT_PENDING QCA MME confirmation is pending
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SET_NMK
 */
ETHTRCV_30_AR7000_LOCAL FUNC(EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetKeyStatus(
        uint8                                                          TrcvIdx,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) MStatus);
#endif
  /* (   (   (ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON) \
         && (ETHTRCV_30_AR7000_CALC_NMK_LOCALLY == STD_OFF) ) \
     || (ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API == STD_ON) ) */

#if ( ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION == STD_ON )
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetSoftwareVersion
 **********************************************************************************************************************/
/*!
 * \brief       Reads 'get software version' MME result.
 * \details     Rx-Handler for 'get software version' confirm MME received from the QCA.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   DataPtr        Received Ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetSoftwareVersion(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr);
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION */

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_HstActionRequired
 **********************************************************************************************************************/
/*!
 * \brief       Triggers 'host action' confirm MME.
 * \details     Rx-Handler for 'host action' request MME received from the QCA. Triggering a 'host action' confirm MME
 *              transmission to the QCA and the future host action processing.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   DataPtr        Received Ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD
 */
/* PRQA S 0779 2 */ /* MD_EthTrcv_30_Ar7000_0779 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_HstActionRequired(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_HstActionRequiredSecondPhase
 **********************************************************************************************************************/
/*!
 * \brief       Triggers the specific 'host action' MME requested by the 'host action' request MME.
 * \details     Send a specific MME request related on the 'HostAction' after the 'host action confirm' MME was
 *              transmitted to the QCA.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD
 */
/* PRQA S 0779 2 */ /* MD_EthTrcv_30_Ar7000_0779 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_HstActionRequiredSecondPhase(
  uint8 TrcvIdx);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_WriteAndExecuteApplet
 **********************************************************************************************************************/
/*!
 * \brief       Sends a 'write and execute applet' request to the QCA.
 * \details     This MME is needed for the firmware download process which includes: softloader applet, PIB block and
 *              the firmware.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   Params         Parameter needed for the downloading process e.g. offset address. This parameter is
 *                             different for each component (softloader applet, PIB block, firmware).
 * \return      E_OK           The host has successfully send the request MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_WriteAndExecuteApplet(
          uint8                                                                                             TrcvIdx,
  P2CONST(EthTrcv_30_Ar7000_MME_WaEA_ParamType, ETHTRCV_30_AR7000_APPL_CONST, ETHTRCV_30_AR7000_APPL_CONST) Params );

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_HostAction
 **********************************************************************************************************************/
/*!
 * \brief       Sends 'host action' response MME to QCA
 * \details     This MME is needed to reply to the 'host action' request MME message.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   HarRequestPtr  Host-Action-Request data received from the QCA
 * \return      E_OK           The host has successfully send the response MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_HostAction(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) HarRequestPtr);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_WriteAndExecuteApplet
 **********************************************************************************************************************/
/*!
 * \brief       Triggers 'write and execute applet' request MME.
 * \details     Sends data chunks needed for the boot process (softloader applet, PIB block and firmware) to the QCA.
 *              It is also possible to send only the firmware e.g. for firmware update.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   DataPtr        Received Ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous FALSE
 * \config      ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_WriteAndExecuteApplet(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_VCalcChecksum
 **********************************************************************************************************************/
/*!
 * \brief       Calculates a checksum.
 * \details     Compute the 32 bit checksum of a memory segment. The calculated checksum is the one's complement of
 *              the XOR of all 32-bit words.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   DataPtr        Memory segment start
 * \param[in]   DataLenByte    Memory segment size in bytes
 * \return      uint32         32bit checksum of a given memory segment
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD
 * \note        The DataLenBytes should be an even multiple of 4-bytes or trailing bytes will be excluded from the
 *              calculation.
 */
ETHTRCV_30_AR7000_LOCAL FUNC(uint32, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_VCalcChecksum(
          uint8                                                             TrcvIdx,
  P2CONST(uint32, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr,
          uint32                                                            DataLenByte);
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ) || \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_READ_NMK )   ||   \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_MODOP_API )
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_ModuleOperation
 **********************************************************************************************************************/
/*!
 * \brief       Reads 'module operation' MME result.
 * \details     Rx-Handler for 'module operation' confirm MME received from the QCA. Verify that the received MME
 *              confirmation is OK. Extract the requested QCA module information. This can be a PIB block or a NMK.
 *              Inform the calling application using the dedicated callbacks if there are available.
 * \param[in]   TrcvIdx         Zero based index of the transceiver
 * \param[in]   DataPtr         Received Ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous FALSE
 * \config      ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP || ETHTRCV_30_AR7000_ENABLE_READ_NMK ||
 *              ETHTRCV_30_AR7000_ENABLE_MODOP_API
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_ModuleOperation(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr);
#endif /* STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ||
          STD_ON == ETHTRCV_30_AR7000_ENABLE_READ_NMK   ||
          STD_ON == ETHTRCV_30_AR7000_ENABLE_MODOP_API*/

#if ( ( STD_ON == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY )          || \
      ( STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS ) || \
      ( STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK ))
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_VGetPibModuleOffset
 **********************************************************************************************************************/
/*!
 * \brief       Searching a NVM chain for the existence of a PIB module
 * \details     -
 * \param[in]   PibPtr        The NVM chain were the search is done
 * \param[out]  ModOffset     The PIB module image offset
 * \param[out]  ModHdrOffset  The PIB module header offset
 * \return      E_OK          PIB module found within the given NVM chain
 * \return      E_NOT_OK      PIB module not found within the given NVM chain
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_CALC_NMK_LOCALLY || ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS ||
 *              ETHTRCV_30_AR7000_ENABLE_WRITE_DAK
 */
ETHTRCV_30_AR7000_LOCAL FUNC (uint8, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_VGetPibModuleOffset(
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_DATA)                            PibPtr,
  P2VAR(uint8*, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_DATA)                           ModOffset,
  P2VAR(EthTrcv_30_Ar7000_NVMHeaderType*, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_DATA) ModHdrOffset);
#endif /* STD_ON == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY          ||
          STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS ||
          STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK */

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 **********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 **********************************************************************************************************************/

/**********************************************************************************************************************
 *  MME's
 **********************************************************************************************************************/
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_GetLinkStatus
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_GetLinkStatus(
  uint8 TrcvIdx)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  Std_ReturnType retVal = E_NOT_OK;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE;

  /* #10 Request a ethernet frame buffer for the get link status request MME frame */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
        (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros
       *       Transmit ethernet frame
       */
      /* This MME doesn't require any further data */
      EthTrcv_30_Ar7000_VFrameHeader(TrcvIdx, (ETHTRCV_30_AR7000_MME_VS_PL_LNK_STATUS | ETHTRCV_30_AR7000_MME_REQ), &ethBuf);
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      if (E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE,
                    (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE, (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]))
      {
        retVal = E_OK;
      }
    }

    /* #40 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0,
                                   (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]);
    }
  }
  return(retVal);
} /* EthTrcv_30_Ar7000_MME_GetLinkStatus() */

#if (ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION == STD_ON)
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_GetSwVersion
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_GetSwVersion(
  uint8 TrcvIdx)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  Std_ReturnType retVal = E_NOT_OK;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE;

  /* #10 Request a ethernet frame buffer for the get software version request MME frame */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
       (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros
       *       Transmit ethernet frame
       */
      /* This MME doesn't require any further data */
      EthTrcv_30_Ar7000_VFrameHeader(TrcvIdx, (ETHTRCV_30_AR7000_MME_VS_SW_VER | ETHTRCV_30_AR7000_MME_REQ), &ethBuf);
      /* Do not use cookie - already null initialized by EthTrcv_30_Ar7000_VFrameHeader */

      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      if (E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE, (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE,
                    (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]))
      {
        /* successful */
        retVal = E_OK;
      }
    }

    /* #40 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
       /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0,
                                    (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]);
    }
  }

  return retVal;
} /* EthTrcv_30_Ar7000_MME_GetSwVersion() */
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION */

#if (ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API == STD_ON)
 /***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_SetProperty
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_SetProperty(
  uint8 TrcvIdx,
  CONSTP2CONST(EthTrcv_30_Ar7000_SetPropertyParamterType, ETHTRCV_30_AR7000_APPL_CONST, ETHTRCV_30_AR7000_APPL_CONST)
    SetPropParams)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  P2VAR(uint8, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) bufPtrSetProperty;
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  uint8 u8Idx = 0;
  Std_ReturnType retVal = E_NOT_OK;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE + (uint16)SetPropParams->PropDataLength;

  /* #10 Request a ethernet frame buffer for the set property request MME frame */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
       (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= ((uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE + SetPropParams->PropDataLength)) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros */
      EthTrcv_30_Ar7000_VFrameHeader(TrcvIdx, (ETHTRCV_30_AR7000_MME_VS_SET_PROPERTY | ETHTRCV_30_AR7000_MME_REQ), &ethBuf);

      /* #40 Fill ethernet frame with set property request specific portion */
      bufPtrSetProperty = &(ethBuf.Buffer.U8[6]);
      /* Do not use cookie - already null initialized by EthTrcv_30_Ar7000_VFrameHeader */

      /* Option field */
      bufPtrSetProperty[4u] = SetPropParams->Option;
      /* 3 bytes reserved */

      /* Property version field */
      bufPtrSetProperty[8u]  = (uint8) (SetPropParams->PropVersion & 0x000000FFu);
      bufPtrSetProperty[9u]  = (uint8)((SetPropParams->PropVersion & 0x0000FF00u) >> 8u);
      bufPtrSetProperty[10u] = (uint8)((SetPropParams->PropVersion & 0x00FF0000u) >> 16u);
      bufPtrSetProperty[11u] = (uint8)((SetPropParams->PropVersion & 0xFF000000u) >> 24u);

      /* Proprety ID field */
      bufPtrSetProperty[12u] = (uint8) (SetPropParams->PropId & 0x000000FFu);
      bufPtrSetProperty[13u] = (uint8)((SetPropParams->PropId & 0x0000FF00u) >> 8u);
      bufPtrSetProperty[14u] = (uint8)((SetPropParams->PropId & 0x00FF0000u) >> 16u);
      bufPtrSetProperty[15u] = (uint8)((SetPropParams->PropId & 0xFF000000u) >> 24u);

      /* Property Data Length field */
      bufPtrSetProperty[16u] = (uint8) (SetPropParams->PropDataLength & 0x000000FFu);
      bufPtrSetProperty[17u] = (uint8)((SetPropParams->PropDataLength & 0x0000FF00u) >> 8u);
      bufPtrSetProperty[18u] = (uint8)((SetPropParams->PropDataLength & 0x00FF0000u) >> 16u);
      bufPtrSetProperty[19u] = (uint8)((SetPropParams->PropDataLength & 0xFF000000u) >> 24u);

      /* Data field */
      while ( u8Idx < SetPropParams->PropDataLength )
      {
        bufPtrSetProperty[u8Idx + 20u] = SetPropParams->PropData[u8Idx];
        u8Idx++;
      }

      /* #50 Transmit ethernet frame */
      /* PRQA S 0311 4 */ /* MD_EthTrcv_30_Ar7000_0311 */
      if(E_OK == EthIf_Transmit(
                   EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE,TRUE,
                   (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE + (uint16)SetPropParams->PropDataLength,
                   (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]))
      {
        /* successful */
        retVal = E_OK;
      }
    }

    /* #60 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
       /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0,
                                    (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]);
    }
  }

  return(retVal);
} /* EthTrcv_30_Ar7000_MME_SetProperty() */
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API */

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_WriteAndExecuteApplet
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_WriteAndExecuteApplet(
          uint8                                                                                             TrcvIdx,
  P2CONST(EthTrcv_30_Ar7000_MME_WaEA_ParamType, ETHTRCV_30_AR7000_APPL_CONST, ETHTRCV_30_AR7000_APPL_CONST) Params)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  P2VAR(uint8, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) bufPtrWaEA;
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  Std_ReturnType retVal = E_NOT_OK;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 If ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START is enabled
   *       Verify we are enter the function the first time
   *         Inform application that firmware download has been started by using the callback
   */
#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START )
  if ( FALSE == EthTrcv_30_Ar7000_FwDwldStartCbkCalled)
  {
    EthTrcv_30_Ar7000_FwDwldStartCbkCalled = TRUE;
    ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START();
  }
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START */

  ethBuf.BufLen = (uint16)(Params->CurPartLen + ETHTRCV_30_AR7000_MME_WAE_HEADER_LEN);

  /* #20 Request a ethernet frame buffer for the set network membership key request MME frame */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
       (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #30 Verify requested buffer size */
    if ( ethBuf.BufLen >= ((uint16)(Params->CurPartLen + ETHTRCV_30_AR7000_MME_WAE_HEADER_LEN)) ) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #40 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros */
      EthTrcv_30_Ar7000_VFrameHeader(TrcvIdx, (ETHTRCV_30_AR7000_MME_VS_WRITE_AND_EXECUTE_APPLET | ETHTRCV_30_AR7000_MME_REQ), &ethBuf);

      /* #50 Fill ethernet frame with write and execute applet request specific portion */
      bufPtrWaEA = &(ethBuf.Buffer.U8[6]);

      /* VS_WRITE_AND_EXECUTE_APPLET:
       * Fiel offset [Byte] | Field size [Byte] |    Field name       |
       *          0 --------+-------------------+---------------------+
       *                    |         4         |  CLIENT_SESSION_ID  |
       *          4 --------+-------------------+---------------------+
       *                    |         4         |  SERVER_SESSION_ID  |
       *          8 --------+-------------------+---------------------+
       *                    |         4         |       FLAGS         |
       *         12 --------+-------------------+---------------------+
       *                    |         8         |  ALLOWED_MEM_TYPES  |
       *         20 --------+-------------------+---------------------+
       *                    |         4         |    TOTAL_LENGTH     |
       *         24 --------+-------------------+---------------------+
       *                    |         4         |   CURR_PART_LENGTH  |
       *         28 --------+-------------------+---------------------+
       *                    |         4         | CURR_PART_DEST_ADDR |
       *         32 --------+-------------------+---------------------+
       *                    |         4         |     START_ADDR      |
       *         36 --------+-------------------+---------------------+
       *                    |         4         |      CHECKSUM       |
       *         40 --------+-------------------+---------------------+
       *                    |         8         |      RESERVED       |
       *         48 --------+-------------------+---------------------+
       *                    |      variable     |        EXE          |
       *            --------+-------------------+---------------------+
       */

      /* Client Session ID */
      bufPtrWaEA[0u] = (uint8) (EthTrcv_30_Ar7000_ClientSid & 0x000000FFu);
      bufPtrWaEA[1u] = (uint8)((EthTrcv_30_Ar7000_ClientSid & 0x0000FF00u) >> 8u);
      bufPtrWaEA[2u] = (uint8)((EthTrcv_30_Ar7000_ClientSid & 0x00FF0000u) >> 16u);
      bufPtrWaEA[3u] = (uint8)((EthTrcv_30_Ar7000_ClientSid & 0xFF000000u) >> 24u);

      /* Server Session ID */
      bufPtrWaEA[4u] = (uint8) (EthTrcv_30_Ar7000_ServerSid & 0x000000FFu);
      bufPtrWaEA[5u] = (uint8)((EthTrcv_30_Ar7000_ServerSid & 0x0000FF00u) >> 8u);
      bufPtrWaEA[6u] = (uint8)((EthTrcv_30_Ar7000_ServerSid & 0x00FF0000u) >> 16u);
      bufPtrWaEA[7u] = (uint8)((EthTrcv_30_Ar7000_ServerSid & 0xFF000000u) >> 24u);

      /* Flags */
      bufPtrWaEA[8u]  = (uint8) (Params->Flags & 0x000000FFu);
      bufPtrWaEA[9u]  = (uint8)((Params->Flags & 0x0000FF00u) >> 8u);
      bufPtrWaEA[10u] = (uint8)((Params->Flags & 0x00FF0000u) >> 16u);
      bufPtrWaEA[11u] = (uint8)((Params->Flags & 0xFF000000u) >> 24u);

      /* Allowed Mem Types - 8 bytes */
      bufPtrWaEA[12u] = 0x01u; /* Server decides which memory is best */
      bufPtrWaEA[13u] = 0x00u;
      bufPtrWaEA[14u] = 0x00u;
      bufPtrWaEA[15u] = 0x00u;
      bufPtrWaEA[16u] = 0x00u;
      bufPtrWaEA[17u] = 0x00u;
      bufPtrWaEA[18u] = 0x00u;
      bufPtrWaEA[19u] = 0x00u;

      /* Total Length */
      bufPtrWaEA[20u] = (uint8) (Params->TotalLen & 0x000000FFu);
      bufPtrWaEA[21u] = (uint8)((Params->TotalLen & 0x0000FF00u) >> 8u);
      bufPtrWaEA[22u] = (uint8)((Params->TotalLen & 0x00FF0000u) >> 16u);
      bufPtrWaEA[23u] = (uint8)((Params->TotalLen & 0xFF000000u) >> 24u);

      /* Current Part Length */
      bufPtrWaEA[24u] = (uint8) (Params->CurPartLen & 0x000000FFu);
      bufPtrWaEA[25u] = (uint8)((Params->CurPartLen & 0x0000FF00u) >> 8u);
      bufPtrWaEA[26u] = (uint8)((Params->CurPartLen & 0x00FF0000u) >> 16u);
      bufPtrWaEA[27u] = (uint8)((Params->CurPartLen & 0xFF000000u) >> 24u);

      /* Current Part Destination Address */
      bufPtrWaEA[28u] = (uint8) (Params->CurPartDestAddr & 0x000000FFu);
      bufPtrWaEA[29u] = (uint8)((Params->CurPartDestAddr & 0x0000FF00u) >> 8u);
      bufPtrWaEA[30u] = (uint8)((Params->CurPartDestAddr & 0x00FF0000u) >> 16u);
      bufPtrWaEA[31u] = (uint8)((Params->CurPartDestAddr & 0xFF000000u) >> 24u);

      /* Start Address */
      bufPtrWaEA[32u] = (uint8) (Params->StartAddr & 0x000000FFu);
      bufPtrWaEA[33u] = (uint8)((Params->StartAddr & 0x0000FF00u) >> 8u);
      bufPtrWaEA[34u] = (uint8)((Params->StartAddr & 0x00FF0000u) >> 16u);
      bufPtrWaEA[35u] = (uint8)((Params->StartAddr & 0xFF000000u) >> 24u);

      /* Checksum */
      if ( ( Params->Flags & ETHTRCV_30_AR7000_MME_WAE_FLAG_EXECUTE) == ETHTRCV_30_AR7000_MME_WAE_FLAG_EXECUTE )
      {
          bufPtrWaEA[36u] = (uint8) (Params->Checksum & 0x000000FFu);
          bufPtrWaEA[37u] = (uint8)((Params->Checksum & 0x0000FF00u) >> 8u);
          bufPtrWaEA[38u] = (uint8)((Params->Checksum & 0x00FF0000u) >> 16u);
          bufPtrWaEA[39u] = (uint8)((Params->Checksum & 0xFF000000u) >> 24u);
      }

      /* Reserved: 8 bytes already set to zero in EthTrcv_30_Ar7000_VFrameHeader() */

      /* Data */
      /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
      IpBase_Copy(
        (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&bufPtrWaEA[48u],
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&Params->DataPtr[0],
        Params->CurPartLen
        );

      /* #60 Transmit ethernet frame */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      if (E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE, (uint16)Params->CurPartLen + ETHTRCV_30_AR7000_MME_WAE_HEADER_LEN,
                                            (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]))
      {
        retVal = E_OK;
      }
    }

    /* #70 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
       /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0,
                                    (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]);
    }
  }
  return retVal;
} /* EthTrcv_30_Ar7000_MME_WriteAndExecuteApplet() */
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#if (   (   (ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON) \
         && (ETHTRCV_30_AR7000_CALC_NMK_LOCALLY == STD_OFF) ) \
     || (ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API == STD_ON) )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_SetKey
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_SetKey(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) NmkPtr)
{
   /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8_least nmkIdx;
  uint8  ethBufIdx;
  Std_ReturnType retVal = E_NOT_OK;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE;

  /* #10 Request a ethernet frame buffer for the set network membership key request MME frame */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
       (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros */
      EthTrcv_30_Ar7000_VFrameHeader(TrcvIdx, (ETHTRCV_30_AR7000_MME_VS_SET_KEY | ETHTRCV_30_AR7000_MME_REQ), &ethBuf);

      /* #40 Fill ethernet frame with set key request specific portion */
      ethBuf.Buffer.U8[6] = (uint8)0x01;
      for (nmkIdx = 0; nmkIdx < ETHTRCV_30_AR7000_NMK_SIZE_BYTE; nmkIdx++)
      { /* Copy NMK */
        ethBuf.Buffer.U8[7 + nmkIdx] = NmkPtr[nmkIdx];
      }
      /* Local Set */
      ethBuf.Buffer.U8[23] = 0x0f;

      /* #50 Transmit ethernet frame */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      if(E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE, \
                                (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE, (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0])
        )
      {
        /* successful */
        retVal = E_OK;
      }
    }

    /* #60 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
       /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0,
                                    (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]);
    }
  }
  return retVal;
} /* EthTrcv_30_Ar7000_MME_SetKey() */
#endif /* (   (   (ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON) \
               && (ETHTRCV_30_AR7000_CALC_NMK_LOCALLY == STD_OFF) ) \
           || (ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API == STD_ON) ) */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_CmSetKeyReq
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_CmSetKeyReq(
    uint8 TrcvIdx,
    P2CONST(uint8, ETHTRCV_30_AR7000_APPL_CONST, ETHTRCV_30_AR7000_APPL_CONST) NidPtr,
    P2CONST(uint8, ETHTRCV_30_AR7000_APPL_CONST, ETHTRCV_30_AR7000_APPL_CONST) NmkPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  Std_ReturnType retVal = E_NOT_OK;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_MME_SLAC_CM_SET_KEY_REQ_LEN4;

  /* #10 Request a ethernet frame buffer for the set network membership key and network identifier request MME frame */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
        (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= (uint16)ETHTRCV_30_AR7000_MME_SLAC_CM_SET_KEY_REQ_LEN4) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros */
      EthTrcv_30_Ar7000_VFrameHeaderGeneric(
        TrcvIdx,
        (ETHTRCV_30_AR7000_MME_CM_SET_KEY | ETHTRCV_30_AR7000_MME_REQ),
        &ethBuf,
        (uint16)ETHTRCV_30_AR7000_MME_SLAC_CM_SET_KEY_REQ_LEN4);

      /* #40 Fill ethernet frame with set key request specific portion */
      /* Key type is always NMK (AES-128) */
      ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_SLAC_CM_SET_KEY_TYPE] = 0x01u;

      /* Nonces are always zero - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      /* Protocol ID is always 0x04 */
      ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_SLAC_CM_SET_KEY_PID] = 0x04u;

      /* PRN, PMN and CCo Capability are always zero - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      /* NID */
      /* PRQA S 0310, 3305 3 */ /* MD_EthTrcv_30_Ar7000_0310 */
      IpBase_Copy(
        (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&(ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_SLAC_CM_SET_KEY_NID]),
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))NidPtr,
        ETHTRCV_30_AR7000_NID_SIZE_BYTE
      );

      /* New EKS */
      ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_SLAC_CM_SET_KEY_EKS] = 0x01u;

      /* NMK */
      /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
      IpBase_Copy(
        (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&(ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_SLAC_CM_SET_KEY_NMK]),
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))NmkPtr,
        ETHTRCV_30_AR7000_NMK_SIZE_BYTE
      );

      /* #50 Transmit ethernet frame */
      /* PRQA S 0311 3 */ /* MD_EthTrcv_30_Ar7000_0311 */
      if(E_OK == EthIf_Transmit(
           EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE, (uint16)ETHTRCV_30_AR7000_MME_SLAC_CM_SET_KEY_REQ_LEN,
             (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0])
      )
      {
        retVal = E_OK;
      }
    }

    /* #60 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, \
                             FALSE, (uint16)0, (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]);
    }
  }
  return retVal;
} /* EthTrcv_30_Ar7000_MME_CmSetKeyReq() */

#if ( ETHTRCV_30_AR7000_ENABLE_RESET_DEVICE == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_ResetDevice
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_ResetDevice(
  uint8 TrcvIdx)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  Std_ReturnType retVal = E_NOT_OK;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE;

  /* #10 Request a ethernet frame buffer for the reset device MME frame */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
       (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros
       *       Transmit ethernet frame
       */
      /* This MME doesn't require any further data */
      /* PRQA S 0311 3 */ /* MD_EthTrcv_30_Ar7000_0311 */
      EthTrcv_30_Ar7000_VFrameHeader(TrcvIdx, (ETHTRCV_30_AR7000_MME_VS_RS_DEV | ETHTRCV_30_AR7000_MME_REQ), &ethBuf);
      if (E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE, \
                                 (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE, (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0])
      )
      {
        retVal = E_OK;
      }
    }

    /* #40 Verify transmission and release ethernet buffer if it fails */
    if ( E_NOT_OK == retVal )
    {
      /* Release buffer */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0, \
                             (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]);
    }
  }
  return retVal;
} /* EthTrcv_30_Ar7000_MME_ResetDevice() */
#endif /* ETHTRCV_30_AR7000_ENABLE_RESET_DEVICE */

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_HostAction
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_HostAction(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) HarRequestPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  Std_ReturnType retVal = E_NOT_OK;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE;
  /* Host Action Response */
  /* #10 Request a ethernet frame buffer for the host action response MME frame */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
        (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros */
      EthTrcv_30_Ar7000_VFrameHeader(TrcvIdx, (ETHTRCV_30_AR7000_MME_VS_HST_ACTION | ETHTRCV_30_AR7000_MME_RSP), &ethBuf);
      /* #40 Fill ethernet frame with response content */
      ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_MSTATUS]                       = 0x00;
      /* Following data fields:
       * - ETHTRCV_30_AR7000_MME_HARES_MAJOR_VERSION
       * - ETHTRCV_30_AR7000_MME_HARES_MINOR_VERSION
       * - ETHTRCV_30_AR7000_MME_HARES_HOST_ACTION_REQ
       * - ETHTRCV_30_AR7000_MME_HARES_SESSION_ID
       * - ETHTRCV_30_AR7000_MME_HARES_OUTSTANDING_RETRIES
       * are only required for the following MME frames (introduced with QCA7005 firmware version 1.2.1):
       * - ETHTRCV_30_AR7000_HST_ACT_DEVICE_REBOOTED
       * - ETHTRCV_30_AR7000_HST_ACT_DEVICE_READY_FOR_HIF_COM
       * - ETHTRCV_30_AR7000_HST_ACT_DEVICE_READY_FOR_PLC_COM
       * - ETHTRCV_30_AR7000_HST_ACT_MEMBER_OF_AVLN_WITH_PEERS
       * In older requests this positions are not used in the request and shall be set to 0x00, there for they can be just copied for all responses.
       */
      ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_HARES_MAJOR_VERSION]           = HarRequestPtr[ETHTRCV_30_AR7000_MME_HAREQ_MAJOR_VERSION];
      ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_HARES_MINOR_VERSION]           = HarRequestPtr[ETHTRCV_30_AR7000_MME_HAREQ_MINOR_VERSION];
      ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_HARES_HOST_ACTION_REQ]         = HarRequestPtr[ETHTRCV_30_AR7000_MME_HAREQ_HOST_ACTION_REQ];
      ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_HARES_SESSION_ID]              = HarRequestPtr[ETHTRCV_30_AR7000_MME_HAREQ_SESSION_ID];
      ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_HARES_OUTSTANDING_RETRIES]     = HarRequestPtr[ETHTRCV_30_AR7000_MME_HAREQ_OUTSTANDING_RETRIES];
      ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_HARES_OUTSTANDING_RETRIES + 1] = HarRequestPtr[ETHTRCV_30_AR7000_MME_HAREQ_OUTSTANDING_RETRIES + 1];

      /* #50 Transmit ethernet frame */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      if(E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE, \
                                (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE, (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0])
      )
      {
        retVal = E_OK;
      }
    }

    /* #60 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
       /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0,
                                    (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]);
    }
  }
  return retVal;
} /* EthTrcv_30_Ar7000_MME_HostAction() */
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_PBEncryption
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_PBEncryption(
  uint8 TrcvIdx,
  uint8 PBAction )
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8_least  pbEncIdx;
  uint8  ethBufIdx;
  Std_ReturnType retVal = E_NOT_OK;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE;

  /* #10 Request a ethernet frame buffer for the push button encryption MME frame */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
        (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Initialize ethernet buffer with zeros */
      /* default block size is of type '1 byte' and ethBuf.Buffer.Native of type '4 byte' */
      for (pbEncIdx = 0; pbEncIdx < ((uint8_least)(ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE >> 2u)); pbEncIdx++)
      {
        ethBuf.Buffer.Native[pbEncIdx] = 0u;
      }

      /* #40 Fill ethernet frame with push button encryption data */
      ethBuf.Buffer.U8[0] = (uint8)0x00u; /* MMV */
      ethBuf.Buffer.U8[1] = (uint8)((uint16)(ETHTRCV_30_AR7000_MME_MS_PB_ENC | ETHTRCV_30_AR7000_MME_REQ) & 0x00FFu); /* MMTYPE */
      ethBuf.Buffer.U8[2] = (uint8)((uint16)(ETHTRCV_30_AR7000_MME_MS_PB_ENC | ETHTRCV_30_AR7000_MME_REQ) >> 8u);
      ethBuf.Buffer.U8[3] = (uint8)PBAction; /* PBACTION */

      /* #50 Transmit ethernet frame */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      if(E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE, \
                                (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE, (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0])
      )
      {
        retVal = E_OK;
      }
    }

    /* #60 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0u,
                                    (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]);
    }
  }
  return retVal;
} /* EthTrcv_30_Ar7000_MME_PBEncryption() */
#endif /* ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API == STD_ON */

#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ) || \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_MODOP_API ) || \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_READ_NMK )
 /***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_ModuleOperation
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_ModuleOperation(
        uint8                                                                                   TrcvIdx,
        uint16                                                                                  ModOp,
  P2VAR(EthTrcv_30_Ar7000_ModOp_Params, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) Params )
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  P2VAR(uint8, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) bufPtrModOp;
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  Std_ReturnType retVal = E_NOT_OK;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Verify that module operation total length is a multiple of 4 */
  if ((Params->ModOpTotLen & 3) == 0 )
  {
    ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE;

    /* #20 Request a ethernet frame buffer for the module operation MME frame */
    if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
         (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
    {
      /* #30 Verify requested buffer size */
      if (ethBuf.BufLen >= (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
      {
        /* #40 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros */
        EthTrcv_30_Ar7000_VFrameHeader(
          TrcvIdx,
          (ETHTRCV_30_AR7000_MME_VS_MODULE_OPERATION | ETHTRCV_30_AR7000_MME_REQ),
          &ethBuf);

        /* #50 Fill ethernet frame with module operation specific portion */
        if (ETHTRCV_30_AR7000_MODOP_RD_MEM == ModOp)
        {
          /* Set NUM_OP_DATA field to one */
          ethBuf.Buffer.U8[10] = 0x01;

          /* Set pointer to module operation specific offset */
          bufPtrModOp = &(ethBuf.Buffer.U8[11]);

          /* Fields are not aligned and must be accessed bytewise */
          bufPtrModOp[0] = (uint8) (ModOp & 0x00FFu);
          bufPtrModOp[1] = (uint8)((ModOp & 0xFF00u) >> 8);

          bufPtrModOp[2] = (uint8) (Params->ModOpFrameLen & 0x00FFu);
          bufPtrModOp[3] = (uint8)((Params->ModOpFrameLen & 0xFF00u) >> 8);

          /* PRQA S 0310 14 */ /* MD_EthTrcv_30_Ar7000_0310 */
          bufPtrModOp[8] = (uint8) (((EthTrcv_30_Ar7000_ModOp_Rd_Params*)Params)->ModOpModuleId & 0x00FFu);
          bufPtrModOp[9] = (uint8)((((EthTrcv_30_Ar7000_ModOp_Rd_Params*)Params)->ModOpModuleId & 0xFF00u) >> 8);

          bufPtrModOp[10] = (uint8) (((EthTrcv_30_Ar7000_ModOp_Rd_Params*)Params)->ModOpModuleSubId & 0x00FFu);
          bufPtrModOp[11] = (uint8)((((EthTrcv_30_Ar7000_ModOp_Rd_Params*)Params)->ModOpModuleSubId & 0xFF00u) >> 8);

          bufPtrModOp[12] = (uint8) (((EthTrcv_30_Ar7000_ModOp_Rd_Params*)Params)->ModOpDataLenTx & 0x00FFu);
          bufPtrModOp[13] = (uint8)((((EthTrcv_30_Ar7000_ModOp_Rd_Params*)Params)->ModOpDataLenTx & 0xFF00u) >> 8);

          bufPtrModOp[14] = (uint8) (((EthTrcv_30_Ar7000_ModOp_Rd_Params*)Params)->ModOpCurOffset & 0x000000FFu);
          bufPtrModOp[15] = (uint8)((((EthTrcv_30_Ar7000_ModOp_Rd_Params*)Params)->ModOpCurOffset & 0x0000FF00u) >> 8);
          bufPtrModOp[16] = (uint8)((((EthTrcv_30_Ar7000_ModOp_Rd_Params*)Params)->ModOpCurOffset & 0x00FF0000u) >> 16);
          bufPtrModOp[17] = (uint8)((((EthTrcv_30_Ar7000_ModOp_Rd_Params*)Params)->ModOpCurOffset & 0xFF000000u) >> 24);

          /* #60 Transmit ethernet frame */
          /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
          if(E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE, \
                                    (uint16)ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE, (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0])
          )
          {
            EthTrcv_30_Ar7000_ModOpCycCnt = 1u;
            Params->ModOpState = ETHTRCV_30_AR7000_MODOP_STATE_USED;
            retVal = E_OK;
          }
          else
          {
           /* Release buffer */
            /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
           (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0, \
                             (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]);
          }
        }
        /* #70 Release buffer if the ModOp is not supported */
        else
        {
          /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
          (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0, \
                               (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]);
          /* E_NOT_OK */
        }
      }
      /* #80 Release buffer if the size is too small */
      else
      { /* PRQA S 3201 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
        /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
        (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0, \
                             (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_AtherosODA[0]);
        /* E_NOT_OK */
      }
    }
  }

  return retVal;
} /* EthTrcv_30_Ar7000_MME_ModuleOperation() */ /* PRQA S 6080 */ /* MD_MSR_STMIF */
#endif /* ( STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ) ||
          ( STD_ON == ETHTRCV_30_AR7000_ENABLE_MODOP_API )  ||
          ( STD_ON == ETHTRCV_30_AR7000_ENABLE_READ_NMK ) */

/**********************************************************************************************************************
 *  MME Requests and Indications
 **********************************************************************************************************************/
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetLinkStatus
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetLinkStatus(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr)
{
  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */


#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
  if (   (EthTrcv_30_Ar7000_ModeState == ETHTRCV_MODE_DOWN)
      || (EthTrcv_30_Ar7000_LinkStateLockCnt > 0)
      || (EthTrcv_30_Ar7000_DownlStatus != ETHTRCV_30_AR7000_DOWNLSTATUS_DONE) )
  {
    EthTrcv_30_Ar7000_LinkState = ETHTRCV_LINK_STATE_DOWN;
  }
#else
  if ( (ETHTRCV_MODE_DOWN == EthTrcv_30_Ar7000_ModeState) )
  {
    EthTrcv_30_Ar7000_LinkState = ETHTRCV_LINK_STATE_DOWN;
  }
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */
  /* #10 Verify that received confirmation MME is OK */
  else if( 0x00 == DataPtr[ETHTRCV_30_AR7000_MME_MSTATUS] )
  {
    ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    /* #20 Set local link state */
    EthTrcv_30_Ar7000_LinkState = DataPtr[ETHTRCV_30_AR7000_MME_LINKSTATUS];

    /* #30 Response received -> reset heart beat */
    EthTrcv_30_Ar7000_HeartBeatCnt = 0;

    /* #40 Response received -> reset download attempts to initial value */
    EthTrcv_30_Ar7000_DwldAttempts = ETHTRCV_30_AR7000_FW_DWLD_ATTEMPTS;
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  }
  /* #50 Received confirmation MME is NOT_OK */
  else
  {
    /* #60 MStatus is FAILURE. Ignore and wait for next link state MME */
  }

  /* #70 If CALLBACKS are enabled
   *       Inform application using the CALLBACKS
   */
#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_START )
  /* First heart beat indication received - firmware is running. Inform user callback function */
  ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  if ( FALSE == EthTrcv_30_Ar7000_FwStartedCbkCalled )
  {
    EthTrcv_30_Ar7000_FwStartedCbkCalled = TRUE;
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    ETHTRCV_30_AR7000_APPL_CBK_FW_START(TRUE);
# if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START )
    /* Reset FW download start call back info */
    EthTrcv_30_Ar7000_FwDwldStartCbkCalled = FALSE;
# endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START */
  }
  else
  {
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  }
#else
# if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START )
  /* Reset FW download start call back info */
  ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  if (FALSE != EthTrcv_30_Ar7000_FwDwldStartCbkCalled)
  {
    EthTrcv_30_Ar7000_FwDwldStartCbkCalled = FALSE;
  }
  ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
# endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START */
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_START */
} /* EthTrcv_30_Ar7000_SetLinkStatus() */

#if ( ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetKeyCnfStatus
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetKeyCnfStatus(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr)
{
  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

  /* #10 Reset timeout counter
   *     Save 'set network membership key' result
   */
  ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  if ( ETHTRCV_30_AR7000_WAIT_ON_CONF == EthTrcv_30_Ar7000_SetKeyIsChanging )
  {
    EthTrcv_30_Ar7000_SetKeyIsChanging = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
    EthTrcv_30_Ar7000_SetKeyConfReady = ETHTRCV_30_AR7000_STATE_READY;
    EthTrcv_30_Ar7000_SetKeyConfCycCnt = 0;
    EthTrcv_30_Ar7000_SetKeyMStatus = DataPtr[ETHTRCV_30_AR7000_MME_VS_MSTATUS];
  }
  ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
} /* EthTrcv_30_Ar7000_SetKeyCnfStatus() */
#endif
  /* ETHTRCV_30_AR7000_ENABLE_SET_NMK */
#if ( ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetPBEncryptionStatus
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetPBEncryptionStatus(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr)
{
  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

  ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  /* #10 Reset timeout counter */
  EthTrcv_30_Ar7000_IsChangingSCState = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
  EthTrcv_30_Ar7000_SCStateConfReady = ETHTRCV_30_AR7000_STATE_READY;
  EthTrcv_30_Ar7000_SCConfCycCnt = 0;

  /* #20 Save 'push button encryption' status and logical network status */
  EthTrcv_30_Ar7000_PbMStatus = DataPtr[ETHTRCV_30_AR7000_MME_MS_MSTATUS];
  EthTrcv_30_Ar7000_PbAvlnStatus = DataPtr[ETHTRCV_30_AR7000_MME_MS_AVLNSTATUS];

  ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

  /* #30 If ETHTRCV_30_AR7000_APPL_CBK_SIMPLE_CONNECT is defined
   *       Inform application using the callback
   */
#if defined ( ETHTRCV_30_AR7000_APPL_CBK_SIMPLE_CONNECT )
  ETHTRCV_30_AR7000_APPL_CBK_SIMPLE_CONNECT();
#endif /* ETHTRCV_30_AR7000_APPL_CBK_SIMPLE_CONNECT */
} /* EthTrcv_30_Ar7000_SetPBEncryptionStatus() */
#endif /* ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API */

#if ( ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetSoftwareVersion
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetSoftwareVersion(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8_least versionIdx;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

  ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  /* #10 Reset software version confirmation timeout counter*/
  EthTrcv_30_Ar7000_IsReadingSwVersion = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
  EthTrcv_30_Ar7000_SWVersionAvailable = ETHTRCV_30_AR7000_STATE_READY;
  EthTrcv_30_Ar7000_GetSwVersionCycCnt = 0;

  /* #20 Read software version confirm MME status */
  EthTrcv_30_Ar7000_SwMStatus = DataPtr[ETHTRCV_30_AR7000_MME_VS_MSTATUS];

  /* #30 Extract software version  */
  for (versionIdx = 0; versionIdx < DataPtr[ETHTRCV_30_AR7000_MME_VS_MVERLENGTH];  versionIdx++)
  {
    /* Copy Software Version */
    if (versionIdx < sizeof(EthTrcv_30_Ar7000_SwVersion))
    {
      EthTrcv_30_Ar7000_SwVersion[versionIdx] = DataPtr[ETHTRCV_30_AR7000_MME_VS_MVERSION + versionIdx];
    }
    else
    {
      break;
    }
  }
  ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
} /* EthTrcv_30_Ar7000_SetSoftwareVersion() */
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION */

#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_HstActionRequired
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_HstActionRequired(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  Std_ReturnType resVal; /* Result value of called functions */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Answer Host-Action-Required (HAR) requests */
  resVal = EthTrcv_30_Ar7000_MME_HostAction(TrcvIdx, &DataPtr[0]);

  /* #20 Verify confirm MME was successful transmitted */
  if (E_OK == resVal)
  {
    ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    /* #30 Verify that HostAction queue has a free element available */
    if (ETHTRCV_30_AR7000_HST_ACTION_QUEUE_SIZE > EthTrcv_30_Ar7000_HstActionCnt)
    {
      /* #40 Verify that the we don't push the same event to queue which is already stored as last element */
      if (0 == EthTrcv_30_Ar7000_HstActionCnt)
      {
        /* Queue is empty, store new HAR request */
        EthTrcv_30_Ar7000_HstAction[EthTrcv_30_Ar7000_HstActionCnt] = DataPtr[ETHTRCV_30_AR7000_MME_HAREQ_HOST_ACTION_REQ];
        EthTrcv_30_Ar7000_HstActionCnt++;
      }
      else
      {
        /* Check last queue element */
        if (EthTrcv_30_Ar7000_HstAction[EthTrcv_30_Ar7000_HstActionCnt - 1] != DataPtr[ETHTRCV_30_AR7000_MME_HAREQ_HOST_ACTION_REQ])
        {
          /* Store new HAR request */
          EthTrcv_30_Ar7000_HstAction[EthTrcv_30_Ar7000_HstActionCnt] = DataPtr[ETHTRCV_30_AR7000_MME_HAREQ_HOST_ACTION_REQ];
          EthTrcv_30_Ar7000_HstActionCnt++;
        }
        else
        {
          /* This HAR request is already stored, do not store multiple same event in a row */
        }
      }
    }
    else
    {
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
      (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_HST_ACTION_REQ, ETHTRCV_30_AR7000_E_QUEUE_SIZE_TO_SMALL);
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_REPORT */
    }
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  }
} /* EthTrcv_30_Ar7000_HstActionRequired() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_HstActionRequiredSecondPhase
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_HstActionRequiredSecondPhase(
    uint8 TrcvIdx)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  Std_ReturnType resVal = E_OK; /* Result value of called functions, clear host action in case error handling is not required */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Process all received host action requests */
  while (EthTrcv_30_Ar7000_HstActionIdx < EthTrcv_30_Ar7000_HstActionCnt)
  {
    switch ( EthTrcv_30_Ar7000_HstAction[EthTrcv_30_Ar7000_HstActionIdx] )
    {
    case ETHTRCV_30_AR7000_HST_ACT_LOADER_READY_FOR_SDRAM:
      /* QCA7005 has just been (re-)booted and requires a (new) firmware download */
      /* Reset link information */
      EthTrcv_30_Ar7000_LinkStateLockCnt = ETHTRCV_30_AR7000_LINKSTATE_LOCK_CYCLES;
      EthTrcv_30_Ar7000_LinkState        = ETHTRCV_LINK_STATE_DOWN;

      /* Send Write and Execute Applet with
        * - absolute adresses
        * - absolute entry point address
        * - Memctrl applet data
        */
      if (ETHTRCV_30_AR7000_HST_ACT_LOADER_READY_FOR_SDRAM != EthTrcv_30_Ar7000_AppletNrLocked)
      {
        ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
        /* state of firmware download state machine is "MEMCTRL download" */
        EthTrcv_30_Ar7000_DownlStatus = ETHTRCV_30_AR7000_DOWNLSTATUS_MEMCTRL;

#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_START )
        EthTrcv_30_Ar7000_FwStartedCbkCalled = FALSE;
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_START */
#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC )
        EthTrcv_30_Ar7000_FwReadyForSlacCbkCalled = FALSE;
        EthTrcv_30_Ar7000_FwReadyForSlacLastResult = FALSE;
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC */
        /* Reset timeouts */
# if (ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT == STD_ON)
        EthTrcv_30_Ar7000_FwDownloadAndStartCnt = ETHTRCV_30_AR7000_FIRMWARE_DOWNLOAD_AND_START_CYCLES;
#endif /* ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT */
# if (ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT == STD_ON)
        EthTrcv_30_Ar7000_FwReadyForPlcComCnt = ETHTRCV_30_AR7000_FIRMWARE_READY_FOR_SLAC_COM_CYCLES;
#endif /* ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT */

#if (   (defined ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD) \
     && (STD_OFF == ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD) \
     && (defined ETHTRCV_30_AR7000_APPL_CBK_TRIG_DWLD_REQ))
        /* Do not call application inside a critical section */
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
        (void)ETHTRCV_30_AR7000_APPL_CBK_TRIG_DWLD_REQ();
        ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
#endif /* ETHTRCV_30_AR7000_APPL_CBK_TRIG_DWLD_REQ */

        EthTrcv_30_Ar7000_WriteAppletLockCnt              = ETHTRCV_30_AR7000_WRITE_APPLET_LOCK_CYCLES;
        EthTrcv_30_Ar7000_AppletNrLocked                  = ETHTRCV_30_AR7000_HST_ACT_LOADER_READY_FOR_SDRAM;

        EthTrcv_30_Ar7000_MME_WaEA_Params.TotalLen        = ETHTRCV_30_AR7000_MEMCTL_LENGTH;
        EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartLen      = ETHTRCV_30_AR7000_ETHBLOCKSIZE;
        EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartDestAddr = ETHTRCV_30_AR7000_MEMCTL_OFFSET;
        EthTrcv_30_Ar7000_MME_WaEA_Params.StartAddr       = ETHTRCV_30_AR7000_MEMCTL_START;
        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
        EthTrcv_30_Ar7000_MME_WaEA_Params.DataPtr         =
          (P2CONST(Eth_DataType, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST)) MemctrlData;
        EthTrcv_30_Ar7000_MME_WaEA_Params.Flags           = ETHTRCV_30_AR7000_MME_WAE_FLAG_ABS_ADDR;
        EthTrcv_30_Ar7000_MME_WaEA_Params.Checksum        = ETHTRCV_30_AR7000_MEMCTL_CRC;

        resVal = EthTrcv_30_Ar7000_MME_WriteAndExecuteApplet(
          EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
          &EthTrcv_30_Ar7000_MME_WaEA_Params);
        if (E_NOT_OK == resVal)
        {
          EthTrcv_30_Ar7000_WriteAppletLockCnt = 0;
          EthTrcv_30_Ar7000_AppletNrLocked = ETHTRCV_30_AR7000_NO_APPLET_LOCKED;
        }
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      }
      break;
    case ETHTRCV_30_AR7000_HST_ACT_LOADER_READY:
      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      if ( ETHTRCV_30_AR7000_DOWNLSTATUS_PIB == EthTrcv_30_Ar7000_DownlStatus )
      {
        /* Send Write and Execute PIB block with:
         * - absolute adresses
         * - a entry point address of zero, since PIB does not have a executable entry point function
         * - PIB data
         */
        if (ETHTRCV_30_AR7000_HST_ACT_LOADER_READY != EthTrcv_30_Ar7000_AppletNrLocked)
        {
          EthTrcv_30_Ar7000_WriteAppletLockCnt = ETHTRCV_30_AR7000_WRITE_APPLET_LOCK_CYCLES;
          EthTrcv_30_Ar7000_AppletNrLocked = ETHTRCV_30_AR7000_HST_ACT_LOADER_READY;

#if (((defined ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP)         && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP))         || \
     ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)) || \
     ((defined ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)      && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK))      || \
     ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)          && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK))          || \
     ((defined ETHTRCV_30_AR7000_CALC_NMK_LOCALLY)          && (STD_ON == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY))          || \
     ((defined ETHTRCV_30_AR7000_ENABLE_SET_PIB_DATA_API)   && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SET_PIB_DATA_API)))
          if ( TRUE == EthTrcv_30_Ar7000_PibRamCpyValid )
          {
            EthTrcv_30_Ar7000_MME_WaEA_Params.TotalLen        = ETHTRCV_30_AR7000_PIB_SIZE;
            EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartLen      = ETHTRCV_30_AR7000_ETHBLOCKSIZE;
            EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartDestAddr = ETHTRCV_30_AR7000_PIB_OFFSET;
            EthTrcv_30_Ar7000_MME_WaEA_Params.StartAddr       = 0x00;
            /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
            EthTrcv_30_Ar7000_MME_WaEA_Params.DataPtr         =
              (P2CONST(Eth_DataType, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST)) EthTrcv_30_Ar7000_PibRamCpy;
            EthTrcv_30_Ar7000_MME_WaEA_Params.Flags           = ETHTRCV_30_AR7000_MME_WAE_FLAG_ABS_ADDR;
            EthTrcv_30_Ar7000_MME_WaEA_Params.Checksum        = 0x00;

            resVal = EthTrcv_30_Ar7000_MME_WriteAndExecuteApplet(
              TrcvIdx,
              &EthTrcv_30_Ar7000_MME_WaEA_Params);
          }
          else
#endif /* (STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP))         || \
          (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)) || \
          (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK))      || \
          (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK))          || \
          (STD_ON == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY))          || \
          (STD_ON == ETHTRCV_30_AR7000_ENABLE_SET_PIB_DATA_API))) */
          {
            EthTrcv_30_Ar7000_MME_WaEA_Params.TotalLen        = ETHTRCV_30_AR7000_PIB_SIZE;
            EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartLen      = ETHTRCV_30_AR7000_ETHBLOCKSIZE;
            EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartDestAddr = ETHTRCV_30_AR7000_PIB_OFFSET;
            EthTrcv_30_Ar7000_MME_WaEA_Params.StartAddr       = 0x00;
            /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
            EthTrcv_30_Ar7000_MME_WaEA_Params.DataPtr         =
              (P2CONST(Eth_DataType, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST)) EthTrcv_30_Ar7000_PIBData;
            EthTrcv_30_Ar7000_MME_WaEA_Params.Flags           = ETHTRCV_30_AR7000_MME_WAE_FLAG_ABS_ADDR;
            EthTrcv_30_Ar7000_MME_WaEA_Params.Checksum        = 0x00;

            resVal = EthTrcv_30_Ar7000_MME_WriteAndExecuteApplet(
              TrcvIdx,
              &EthTrcv_30_Ar7000_MME_WaEA_Params);
          }
          if (E_NOT_OK == resVal)
          {
            EthTrcv_30_Ar7000_WriteAppletLockCnt = 0;
            EthTrcv_30_Ar7000_AppletNrLocked = ETHTRCV_30_AR7000_NO_APPLET_LOCKED;
          }
        }
      }
      else if ( ETHTRCV_30_AR7000_DOWNLSTATUS_FW == EthTrcv_30_Ar7000_DownlStatus )
      {
        /* Send Write and Execute firmware with
         * - absolute adresses
         * - the absolute entry point address
         * - firmware data
         */
        EthTrcv_30_Ar7000_MME_WaEA_Params.TotalLen        = ETHTRCV_30_AR7000_FIRMWARE_LENGTH;
        EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartLen      = ETHTRCV_30_AR7000_ETHBLOCKSIZE;
        EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartDestAddr = ETHTRCV_30_AR7000_FIRMWARE_OFFSET;
        EthTrcv_30_Ar7000_MME_WaEA_Params.StartAddr       = ETHTRCV_30_AR7000_FIRMWARE_START;
        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
        EthTrcv_30_Ar7000_MME_WaEA_Params.DataPtr         =
          (P2CONST(Eth_DataType, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST)) FirmwareData;
        EthTrcv_30_Ar7000_MME_WaEA_Params.Flags           = ETHTRCV_30_AR7000_MME_WAE_FLAG_ABS_ADDR;
        EthTrcv_30_Ar7000_MME_WaEA_Params.Checksum        = ETHTRCV_30_AR7000_FIRMWARE_CRC;

        resVal = EthTrcv_30_Ar7000_MME_WriteAndExecuteApplet(
          TrcvIdx,
          &EthTrcv_30_Ar7000_MME_WaEA_Params);
        if (E_NOT_OK == resVal)
        {
          /* No special applet has been locked for this status, so no release is required for a retry */
        }
      }
      else
      {
        /* Unknown state - response Host Action Required frame to stop broadcasting */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
        (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_HST_ACTION_REQ_SECOND_PHASE, ETHTRCV_30_AR7000_E_FW_DWLD);
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_REPORT */
      }
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      break;
#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP )
    case ETHTRCV_30_AR7000_HST_ACT_PIB_UPDATE_READY:
    case ETHTRCV_30_AR7000_HST_ACT_FW_PIB_READY:
    case ETHTRCV_30_AR7000_HST_ACT_BGR_PIB_UPDATE_READY:

      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      if (ETHTRCV_30_AR7000_HST_ACT_PIB_UPDATE_READY != EthTrcv_30_Ar7000_AppletNrLocked)
      {
        EthTrcv_30_Ar7000_WriteAppletLockCnt = ETHTRCV_30_AR7000_WRITE_APPLET_LOCK_CYCLES;
        EthTrcv_30_Ar7000_AppletNrLocked = ETHTRCV_30_AR7000_HST_ACT_PIB_UPDATE_READY;

        if ( EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpState == ETHTRCV_30_AR7000_MODOP_STATE_NOT_USED)
        {
          /* Send Module Operation Frame for Read Request */
          EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpFrameLen = ETHTRCV_30_AR7000_MODOP_RD_HDR_LEN;
          /* PRQA S 0310, 3305 1 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
          EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpBufPtr   = (P2VAR(uint32, ETHTRCV_30_AR7000_VAR_NOINIT, ETHTRCV_30_AR7000_VAR_NOINIT))&EthTrcv_30_Ar7000_PibRamCpy[0];
          EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpTotLen   = ETHTRCV_30_AR7000_PIB_SIZE;
          EthTrcv_30_Ar7000_ModOpParams.ModOpModuleId            = ETHTRCV_30_AR7000_MOD_ID_PIB;
          EthTrcv_30_Ar7000_ModOpParams.ModOpModuleSubId         = 0x0000u;
          EthTrcv_30_Ar7000_ModOpParams.ModOpDataLenTx           = ETHTRCV_30_AR7000_MODOP_MAX_DATA_SIZE;
          EthTrcv_30_Ar7000_ModOpParams.ModOpStartOffset         = 0x00000000u;
          EthTrcv_30_Ar7000_ModOpParams.ModOpCurOffset           = 0x00000000u;

          if (ETHTRCV_30_AR7000_HST_ACT_BGR_PIB_UPDATE_READY != EthTrcv_30_Ar7000_HstAction[EthTrcv_30_Ar7000_HstActionIdx])
          {
            EthTrcv_30_Ar7000_WillResetAfterPibUpload = TRUE;
          }

          /* PRQA S 0310 4 */ /* MD_EthTrcv_30_Ar7000_0310_3 */
          resVal = EthTrcv_30_Ar7000_MME_ModuleOperation(
            TrcvIdx,
            ETHTRCV_30_AR7000_MODOP_RD_MEM,
            (P2VAR(EthTrcv_30_Ar7000_ModOp_Params, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))&EthTrcv_30_Ar7000_ModOpParams);
        }
        else
        {
          resVal = E_NOT_OK;
        }
        if (E_OK == resVal)
        {
#if (   (defined ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD) \
     && (STD_OFF == ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD) \
     && (defined ETHTRCV_30_AR7000_APPL_CBK_TRIG_DWLD_REQ) )
          /* Do not call application inside a critical section */
          ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
          (void)ETHTRCV_30_AR7000_APPL_CBK_TRIG_DWLD_REQ();
          ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
#endif /* ETHTRCV_30_AR7000_APPL_CBK_TRIG_DWLD_REQ */
        }
        else
        {
          EthTrcv_30_Ar7000_WriteAppletLockCnt = 0;
          EthTrcv_30_Ar7000_AppletNrLocked = ETHTRCV_30_AR7000_NO_APPLET_LOCKED;
        }
      }
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      break;
#endif /* ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP */

    case ETHTRCV_30_AR7000_HST_ACT_DEVICE_READY_FOR_HIF_COM:
      /* Firmware is sucessfully started, but is currently bussy. Wait for ETHTRCV_30_AR7000_HST_ACT_DEVICE_READY_FOR_PLC_COM */
#if (STD_ON == ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT)
      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      /* Reset download attempts to initial value */
      EthTrcv_30_Ar7000_DwldAttempts = ETHTRCV_30_AR7000_FW_DWLD_ATTEMPTS;

      /* Stop timer */
      EthTrcv_30_Ar7000_FwDownloadAndStartCnt = 0;
# if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_START )
      /* Inform user callback function */
      if ( FALSE == EthTrcv_30_Ar7000_FwStartedCbkCalled )
      {
        EthTrcv_30_Ar7000_FwStartedCbkCalled = TRUE;
        /* Do not call application inside a critical section */
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
        ETHTRCV_30_AR7000_APPL_CBK_FW_START(TRUE);
        ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
#  if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START )
        /* Reset FW download start call back info */
        EthTrcv_30_Ar7000_FwDwldStartCbkCalled = FALSE;
#  endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START */
      }
# else
#  if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START )
      /* Reset FW download start call back info */
      if (FALSE != EthTrcv_30_Ar7000_FwDwldStartCbkCalled)
      {
        EthTrcv_30_Ar7000_FwDwldStartCbkCalled = FALSE;
      }
#  endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START */
# endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_START */
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
#else
# if (ETHTRCV_30_AR7000_ENABLE_DET_ON_DISABLED_HST_ACT_EVENTS == STD_ON)
      /* This event is enabled in the PIB file, but disabled in the ECU configuration.
       * It is recommended to enable this event in the ECU confugration.
       */
      (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_HST_ACTION_REQ_SECOND_PHASE, ETHTRCV_30_AR7000_E_DISABLED_FEATURE);
# endif
#endif /* ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT */
      break;
    case ETHTRCV_30_AR7000_HST_ACT_DEVICE_READY_FOR_PLC_COM:
      /* QCA is ready for SLAC communication, inform application. */

#if (STD_ON == ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_SLAC_COM_EVENT)
      /* Stop timer */
      EthTrcv_30_Ar7000_FwReadyForPlcComCnt = 0;

# if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC )
      /* Inform user callback function */
      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      if ( FALSE == EthTrcv_30_Ar7000_FwReadyForSlacCbkCalled )
      {
        EthTrcv_30_Ar7000_FwReadyForSlacCbkCalled = TRUE;
        EthTrcv_30_Ar7000_FwReadyForSlacLastResult = TRUE;
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
        ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC(EthTrcv_30_Ar7000_FwReadyForSlacLastResult);
      }
      else
      {
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      }
# endif
#else
# if (ETHTRCV_30_AR7000_ENABLE_DET_ON_DISABLED_HST_ACT_EVENTS == STD_ON)
      /* This event is enabled in the PIB file, but disabled in the ECU configuration.
       * It is recommended to enable this event in the ECU confugration. It may speed up the start up process until SLAC can be started.
       */
      (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_HST_ACTION_REQ_SECOND_PHASE, ETHTRCV_30_AR7000_E_DISABLED_FEATURE);
# endif
#endif /* ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_SLAC_COM_EVENT */
      break;
    case ETHTRCV_30_AR7000_HST_ACT_MEMBER_OF_AVLN_WITH_PEERS:
      /* Link state changed to 1 */

#if (STD_ON == ETHTRCV_30_AR7000_ENABLE_MEMBER_OF_AVLN_WITH_PEERS_EVENT)
      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      /* Set local link state */
      EthTrcv_30_Ar7000_LinkState = ETHTRCV_LINK_STATE_ACTIVE;
      EthTrcv_30_Ar7000_LinkStateLockCnt = 0;

      /* Set heart beat count to zero */
      EthTrcv_30_Ar7000_HeartBeatCnt = 0;
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
#else
# if (ETHTRCV_30_AR7000_ENABLE_DET_ON_DISABLED_HST_ACT_EVENTS == STD_ON)
      /* This event is enabled in the PIB file, but disabled in the ECU configuration.
       * It is recommended to enable this event in the ECU confugration. It may speed up the start up process until SLAC can be started.
       */
      (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_HST_ACTION_REQ_SECOND_PHASE, ETHTRCV_30_AR7000_E_DISABLED_FEATURE);
# endif
#endif /* ETHTRCV_30_AR7000_ENABLE_MEMBER_OF_AVLN_WITH_PEERS_EVENT */

      break;

    default:
      break;
    }

    /* #20 Verify that host action is successfully answered */
    if (E_OK == resVal)
    {
      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      /* #30 Host action request successfully processed, go to next request */
      EthTrcv_30_Ar7000_HstAction[EthTrcv_30_Ar7000_HstActionIdx] = ETHTRCV_30_AR7000_NO_HST_ACTION_REQUIRED;
      EthTrcv_30_Ar7000_HstActionIdx++;
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    }
    /* #40 Host action isn't successfully answered */
    else
    {
      /* #50 Host action not processed, move current HAR to first postition in queue. */
      if (0 != EthTrcv_30_Ar7000_HstActionIdx)
      {
        uint8_least TmpIdx = 0;
        uint8 NewQueueEndIdx;

        ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
        /* Move all HAR's to beginning of the queue */
        for (; EthTrcv_30_Ar7000_HstActionIdx < EthTrcv_30_Ar7000_HstActionCnt; EthTrcv_30_Ar7000_HstActionIdx++)
        {
          EthTrcv_30_Ar7000_HstAction[TmpIdx] = EthTrcv_30_Ar7000_HstAction[EthTrcv_30_Ar7000_HstActionIdx];
          TmpIdx++;
        }
        NewQueueEndIdx = (uint8)TmpIdx;
        /* Reset free elements */
        for (; TmpIdx < EthTrcv_30_Ar7000_HstActionCnt; TmpIdx++)
        {
          EthTrcv_30_Ar7000_HstAction[TmpIdx] = ETHTRCV_30_AR7000_NO_HST_ACTION_REQUIRED;
        }
        /* Set queue position to remaining elements */
        EthTrcv_30_Ar7000_HstActionIdx = 0;
        EthTrcv_30_Ar7000_HstActionCnt = NewQueueEndIdx;
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      }
      break;
    }
  } /* while (EthTrcv_30_Ar7000_HstActionIdx < EthTrcv_30_Ar7000_HstActionCnt) */

  /* #60 Verify if queue can be reset */
  ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  if (EthTrcv_30_Ar7000_HstActionIdx == EthTrcv_30_Ar7000_HstActionCnt)
  {
    EthTrcv_30_Ar7000_HstActionIdx = 0;
    EthTrcv_30_Ar7000_HstActionCnt = 0;
  }
  ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
} /* EthTrcv_30_Ar7000_HstActionRequiredSecondPhase() */ /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_WriteAndExecuteApplet
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_WriteAndExecuteApplet(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint32 waeFlags;
  uint32 status;
  uint32 waeStartAddr;
  uint32 pibChecksum;
  uint32 totalLen;
  uint32 bufCurAddrOffs;
  uint32 bufEndAddr;
  uint32 bytesTx;
  uint32 bytesDone;
  uint32 partLen;
  uint32 partDestAddr;
  boolean errorReturn = FALSE;

#if ((defined ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD) && (STD_ON==ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD))
  uint8 u8Idx;
  Std_ReturnType resVal = E_OK; /* Result value of called functions */
#endif /* ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Parse the 'write and execute applet response' MME header */
  /* Set working buffer to the STATUS field */
  P2CONST(uint8, AUTOMATIC, ETHTRCV_30_AR7000_CONST) BufPtrWaEA = &(DataPtr)[6];

  /* status -> 4 Byte */
  status = ETHTRCV_30_AR7000_U8PTRDATA_TO_VAL32(BufPtrWaEA, 0);

  /* Client Session ID -> of no interest -> 4 Byte*/

  /* Server Session ID -> 4 Byte*/
  EthTrcv_30_Ar7000_ServerSid = ETHTRCV_30_AR7000_U8PTRDATA_TO_VAL32(BufPtrWaEA, 8);

  /* Flags -> 4 Byte*/
  waeFlags = ETHTRCV_30_AR7000_U8PTRDATA_TO_VAL32(BufPtrWaEA, 12);

  /* Allowed Mem Types -> of no interest -> 8 Byte */

  /* Total Length -> 4 Byte*/
  totalLen = ETHTRCV_30_AR7000_U8PTRDATA_TO_VAL32(BufPtrWaEA, 24);

  /* Current Part Length -> 4 Byte */
  partLen = ETHTRCV_30_AR7000_U8PTRDATA_TO_VAL32(BufPtrWaEA, 28);

  /* Current Part Destination Address -> of no interest -> 4 Byte */
  partDestAddr = ETHTRCV_30_AR7000_U8PTRDATA_TO_VAL32(BufPtrWaEA, 32);

  /* Start Address -> 4 Byte */
  waeStartAddr = ETHTRCV_30_AR7000_U8PTRDATA_TO_VAL32(BufPtrWaEA, 36);

  /* Checksum -> of no interest -> 4 Byte */
  /* Reserved -> of no interest -> 8 Byte */
  /* Current Part Absolute Address -> of no interest -> 4 Byte */
  /* Absolute Start Address -> of no interest -> 4 Byte */

  /* calculate start address for next junk */
  bufCurAddrOffs = partDestAddr + partLen;
  pibChecksum    = 0;

  switch (EthTrcv_30_Ar7000_DownlStatus)
  {
  case ETHTRCV_30_AR7000_DOWNLSTATUS_MEMCTRL:
    bufEndAddr = ETHTRCV_30_AR7000_MEMCTL_OFFSET + totalLen;
    break;
  case ETHTRCV_30_AR7000_DOWNLSTATUS_PIB:
    bufEndAddr = ETHTRCV_30_AR7000_PIB_OFFSET + ETHTRCV_30_AR7000_PIB_SIZE;
    break;
  case ETHTRCV_30_AR7000_DOWNLSTATUS_FW:
    bufEndAddr = ETHTRCV_30_AR7000_FIRMWARE_OFFSET + ETHTRCV_30_AR7000_FIRMWARE_LENGTH;
    break;
  default:
    /* Ignore */
    bufEndAddr = 0xFFFFFFFFu;
    errorReturn = TRUE;
    break;
  }
  if (errorReturn == FALSE)
  {
    /* #20 Calculate amount of data to be transmitted and finished */
    bytesTx   = bufEndAddr - bufCurAddrOffs;
    bytesDone = totalLen - bytesTx;
  }
  else
  {
    bytesTx = 0u;
    bytesDone = 0u;
  }

  /* #30 Do some error checks:
   *       Verify the response MME status
   *       Verify that the read bytes not exceed the bytes totally available
   */
  if ( (errorReturn == FALSE) && (status != 0) )
  {
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
# if ( ETHTRCV_30_AR7000_ENABLE_DET_ON_FW_OR_COM_ERROR == STD_ON )
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_WRITE_AND_EXECUTE_APPLET, ETHTRCV_30_AR7000_E_FW_DWLD);
# endif /* ETHTRCV_30_AR7000_ENABLE_DET_ON_FW_OR_COM_ERROR */
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_REPORT */

    EthTrcv_30_Ar7000_DwldAttempts--;
    if (0 == EthTrcv_30_Ar7000_DwldAttempts)
    {
      EthTrcv_30_Ar7000_DwldAttempts = ETHTRCV_30_AR7000_FW_DWLD_ATTEMPTS;
#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_START )
      /* Application callout */
      ETHTRCV_30_AR7000_APPL_CBK_FW_START(FALSE);
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_START */
      /* DEM Error */
#if ( ETHTRCV_30_AR7000_DEM_ERROR_DETECT == STD_ON )
      ETHTRCV_30_AR7000_DEM_REPORT_ERROR_STATUS(ETHTRCV_30_AR7000_E_ACCESS);
#endif /* ETHTRCV_30_AR7000_DEM_ERROR_DETECT */

    }
    else
    {
      /* reset */
      if ( E_NOT_OK == ETH_WRITESPI(EthTrcv_30_Ar7000_VCfgGetEthCtrlIdx(), ETHTRCV_30_AR7000_ETH_SPI_REG_CONFIG, ETHTRCV_30_AR7000_SPI_SOC_CORE_RESET))
      {
      /* DEM Error */
#if ( ETHTRCV_30_AR7000_DEM_ERROR_DETECT == STD_ON )
        ETHTRCV_30_AR7000_DEM_REPORT_ERROR_STATUS(ETHTRCV_30_AR7000_E_ACCESS);
#endif /* ETHTRCV_30_AR7000_DEM_ERROR_DETECT */
      }
    }
    errorReturn = TRUE;
  }
  else if ( (errorReturn == FALSE) && (bytesDone > totalLen) )
  {
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
# if ( ETHTRCV_30_AR7000_ENABLE_DET_ON_FW_OR_COM_ERROR == STD_ON )
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_WRITE_AND_EXECUTE_APPLET, ETHTRCV_30_AR7000_E_FW_DWLD);
# endif /* ETHTRCV_30_AR7000_ENABLE_DET_ON_FW_OR_COM_ERROR */
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_REPORT */

    EthTrcv_30_Ar7000_DwldAttempts--;
    if (0 == EthTrcv_30_Ar7000_DwldAttempts)
    {
      EthTrcv_30_Ar7000_DwldAttempts = ETHTRCV_30_AR7000_FW_DWLD_ATTEMPTS;
#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_START )
      /* Application callout */
      ETHTRCV_30_AR7000_APPL_CBK_FW_START(FALSE);
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_START */
      /* DEM error */
#if ( ETHTRCV_30_AR7000_DEM_ERROR_DETECT == STD_ON )
      ETHTRCV_30_AR7000_DEM_REPORT_ERROR_STATUS(ETHTRCV_30_AR7000_E_ACCESS);
#endif /* ETHTRCV_30_AR7000_DEM_ERROR_DETECT */
    }
    else
    {
      /* reset */
      if ( E_NOT_OK == ETH_WRITESPI(EthTrcv_30_Ar7000_VCfgGetEthCtrlIdx(), ETHTRCV_30_AR7000_ETH_SPI_REG_CONFIG, ETHTRCV_30_AR7000_SPI_SOC_CORE_RESET))
      {
      /* DEM Error */
#if ( ETHTRCV_30_AR7000_DEM_ERROR_DETECT == STD_ON )
        ETHTRCV_30_AR7000_DEM_REPORT_ERROR_STATUS(ETHTRCV_30_AR7000_E_ACCESS);
#endif /* ETHTRCV_30_AR7000_DEM_ERROR_DETECT */
      }
    }
    errorReturn = TRUE;
  }
  else
  {
    /* nothing to do */
  }

  /* #40 Verify that we have finished transmission  */
  if ( (errorReturn == FALSE) && (bytesTx == 0) )
  {
    EthTrcv_30_Ar7000_DownlStatus++;
    EthTrcv_30_Ar7000_ClientSid++;
    /* Release the locked applet */
    EthTrcv_30_Ar7000_WriteAppletLockCnt = 0;
    EthTrcv_30_Ar7000_AppletNrLocked     = ETHTRCV_30_AR7000_NO_APPLET_LOCKED;

    if (ETHTRCV_30_AR7000_DOWNLSTATUS_FW == EthTrcv_30_Ar7000_DownlStatus)
    {
      /* Prepare firmware download */
      bytesTx        = ETHTRCV_30_AR7000_FIRMWARE_LENGTH;
      bufCurAddrOffs = ETHTRCV_30_AR7000_FIRMWARE_OFFSET;
      waeStartAddr   = ETHTRCV_30_AR7000_FIRMWARE_START;
      bytesDone      = 0;
      waeFlags       = ETHTRCV_30_AR7000_MME_WAE_FLAG_ABS_ADDR;
    }
    else if ( ETHTRCV_30_AR7000_DOWNLSTATUS_FW < EthTrcv_30_Ar7000_DownlStatus )
    {
      /* finished firmware download process */
      EthTrcv_30_Ar7000_LinkState          = ETHTRCV_LINK_STATE_DOWN;
      EthTrcv_30_Ar7000_LinkStateLockCnt   = 0;
    }
    else
    {
      /* Nothing to do */
    }
  }

  /* #50 Verify that we have remaining data chunks */
  if ( (errorReturn == FALSE) && (bytesTx > 0) )
  {
    ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    /* #60 Determine maximum transmission data size */
    if (bytesTx > ETHTRCV_30_AR7000_ETHBLOCKSIZE)
    {
      bytesTx = ETHTRCV_30_AR7000_ETHBLOCKSIZE;
    }
    else
    {
      /* #70 Set execute flag for last data chunk transfer if it is a executable (fw or softloader applet) */
      if ( ETHTRCV_30_AR7000_DOWNLSTATUS_PIB != EthTrcv_30_Ar7000_DownlStatus )
      {
        waeFlags |= ETHTRCV_30_AR7000_MME_WAE_FLAG_EXECUTE;
      }
      /* #80 Calculate PIB checksum if the data chunk was of type PIB */
      else
      {
#if (   ((defined ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP)         && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP)) \
     || ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)) \
     || ((defined ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)      && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)) \
     || ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)          && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)) \
     || ((defined ETHTRCV_30_AR7000_CALC_NMK_LOCALLY)          && (STD_ON == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY)) \
     || ((defined ETHTRCV_30_AR7000_ENABLE_SET_PIB_DATA_API)   && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SET_PIB_DATA_API)))
        /* Calculate Checksum for Pib */
        if ( TRUE == EthTrcv_30_Ar7000_PibRamCpyValid )
        {
          /* PRQA S 0310, 3305 1 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
          pibChecksum = EthTrcv_30_Ar7000_VCalcChecksum(TrcvIdx, (P2CONST(uint32, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))EthTrcv_30_Ar7000_PibRamCpy, ETHTRCV_30_AR7000_PIB_SIZE);
        }
        else
#endif /*    (STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP)
          || (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)
          || (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)
          || (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)
          || (STD_ON == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY)
          || (STD_ON == ETHTRCV_30_AR7000_ENABLE_SET_PIB_DATA_API) */
        {
          /* PRQA S 0310, 3305 1 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
          pibChecksum = EthTrcv_30_Ar7000_VCalcChecksum(TrcvIdx, (P2CONST(uint32, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))EthTrcv_30_Ar7000_PIBData, ETHTRCV_30_AR7000_PIB_SIZE);
        }
      }
    }

    /* #90 Transmit data chunk to QCA */
    switch (EthTrcv_30_Ar7000_DownlStatus)
    {
    case ETHTRCV_30_AR7000_DOWNLSTATUS_MEMCTRL:
      /* Transmit MemCtrl */
      EthTrcv_30_Ar7000_MME_WaEA_Params.TotalLen        = ETHTRCV_30_AR7000_MEMCTL_LENGTH;
      EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartLen      = bytesTx;
      EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartDestAddr = bufCurAddrOffs;
      EthTrcv_30_Ar7000_MME_WaEA_Params.StartAddr       = waeStartAddr;
      /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
      EthTrcv_30_Ar7000_MME_WaEA_Params.DataPtr         =
        (P2CONST(Eth_DataType, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST)) &(MemctrlData[bytesDone]);
      EthTrcv_30_Ar7000_MME_WaEA_Params.Flags           = waeFlags;
      EthTrcv_30_Ar7000_MME_WaEA_Params.Checksum        = ETHTRCV_30_AR7000_MEMCTL_CRC;

#if ((defined ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD) && (STD_OFF==ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD))
      EthTrcv_30_Ar7000_MME_WaEA_Params.NeedToSend = TRUE;
#else
      resVal = EthTrcv_30_Ar7000_MME_WriteAndExecuteApplet(
        TrcvIdx,
        &EthTrcv_30_Ar7000_MME_WaEA_Params);
#endif /* ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD */
      break;
    case ETHTRCV_30_AR7000_DOWNLSTATUS_PIB:
      /* Transmit PIB */

#if (((defined ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP)         && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP))         || \
     ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)) || \
     ((defined ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)      && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK))      || \
     ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)          && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK))          || \
     ((defined ETHTRCV_30_AR7000_CALC_NMK_LOCALLY)          && (STD_ON == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY))          || \
     ((defined ETHTRCV_30_AR7000_ENABLE_SET_PIB_DATA_API)   && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SET_PIB_DATA_API)))
      if ( TRUE == EthTrcv_30_Ar7000_PibRamCpyValid )
      {
        EthTrcv_30_Ar7000_MME_WaEA_Params.TotalLen        = ETHTRCV_30_AR7000_PIB_SIZE;
        EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartLen      = bytesTx;
        EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartDestAddr = bufCurAddrOffs;
        EthTrcv_30_Ar7000_MME_WaEA_Params.StartAddr       = waeStartAddr;
        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
        EthTrcv_30_Ar7000_MME_WaEA_Params.DataPtr         =
          (P2CONST(Eth_DataType, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST)) &(EthTrcv_30_Ar7000_PibRamCpy[bytesDone]);
        EthTrcv_30_Ar7000_MME_WaEA_Params.Flags           = waeFlags;
        EthTrcv_30_Ar7000_MME_WaEA_Params.Checksum        = pibChecksum;
#if ((defined ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD) && (STD_OFF==ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD))
        EthTrcv_30_Ar7000_MME_WaEA_Params.NeedToSend = TRUE;
#else
        resVal = EthTrcv_30_Ar7000_MME_WriteAndExecuteApplet(
          TrcvIdx,
          &EthTrcv_30_Ar7000_MME_WaEA_Params);
#endif /* ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD */
      }
      else
#endif /* ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP, ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS,
        * ETHTRCV_30_AR7000_ENABLE_WRITE_DAK, ETHTRCV_30_AR7000_CALC_NMK_LOCALLY */
      {
        EthTrcv_30_Ar7000_MME_WaEA_Params.TotalLen        = ETHTRCV_30_AR7000_PIB_SIZE;
        EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartLen      = bytesTx;
        EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartDestAddr = bufCurAddrOffs;
        EthTrcv_30_Ar7000_MME_WaEA_Params.StartAddr       = waeStartAddr;
        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
        EthTrcv_30_Ar7000_MME_WaEA_Params.DataPtr         =
          (P2CONST(Eth_DataType, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST)) &(EthTrcv_30_Ar7000_PIBData[bytesDone]);
        EthTrcv_30_Ar7000_MME_WaEA_Params.Flags           = waeFlags;
        EthTrcv_30_Ar7000_MME_WaEA_Params.Checksum        = pibChecksum;
#if ((defined ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD) && (STD_OFF==ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD))
        EthTrcv_30_Ar7000_MME_WaEA_Params.NeedToSend = TRUE;
#else
        resVal = EthTrcv_30_Ar7000_MME_WriteAndExecuteApplet(
          TrcvIdx,
          &EthTrcv_30_Ar7000_MME_WaEA_Params);
#endif /* ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD */
      }
      break;
    case ETHTRCV_30_AR7000_DOWNLSTATUS_FW:
      /* Transmit FW */
      EthTrcv_30_Ar7000_MME_WaEA_Params.TotalLen        = ETHTRCV_30_AR7000_FIRMWARE_LENGTH;
      EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartLen      = bytesTx;
      EthTrcv_30_Ar7000_MME_WaEA_Params.CurPartDestAddr = bufCurAddrOffs;
      EthTrcv_30_Ar7000_MME_WaEA_Params.StartAddr       = waeStartAddr;
      /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
      EthTrcv_30_Ar7000_MME_WaEA_Params.DataPtr =
        (P2CONST(Eth_DataType, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST)) &(FirmwareData[bytesDone]);
      EthTrcv_30_Ar7000_MME_WaEA_Params.Flags           = waeFlags;
      EthTrcv_30_Ar7000_MME_WaEA_Params.Checksum        = ETHTRCV_30_AR7000_FIRMWARE_CRC;

#if ((defined ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD) && (STD_OFF==ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD))
      EthTrcv_30_Ar7000_MME_WaEA_Params.NeedToSend = TRUE;
#else
      resVal = EthTrcv_30_Ar7000_MME_WriteAndExecuteApplet(
        TrcvIdx,
        &EthTrcv_30_Ar7000_MME_WaEA_Params);
#endif /* ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD */
      break;
    default:
      break;
    }
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  }

  /* #100 If ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD is enabled
   *       If data chunk transmission status is NOT_OK
   *         Safe the failed frame
   *         Re-Trigger WriteAndExecuteApplet() by set FailedApiCall (See MainFunction())
   */
#if ((defined ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD) && (STD_ON==ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD))
  if ( (errorReturn == FALSE) && (E_NOT_OK == resVal) )
  {
    /* Copy frame for later use */
    u8Idx = 0;
    while ( u8Idx < ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE )
    {
      EthTrcv_30_Ar7000_LastFrHeader[u8Idx] = DataPtr[u8Idx];
      u8Idx++;
    }

    EthTrcv_30_Ar7000_FailedApiCall = EthTrcv_30_Ar7000_WriteAndExecuteApplet;
  }
#endif /* ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD */
} /* EthTrcv_30_Ar7000_WriteAndExecuteApplet() */ /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ) || \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_READ_NMK)    || \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_MODOP_API)
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_ModuleOperation
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_ModuleOperation(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  P2CONST(uint8, AUTOMATIC, ETHTRCV_30_AR7000_CONST) bufPtrModOp = &DataPtr[ETHTRCV_30_AR7000_MME_VS_MSTATUS];

  uint32 ethBufIdx;
  uint16 modOp;
  uint16 mStatus;
  Std_ReturnType resVal = E_NOT_OK; /* Result value of called functions */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  mStatus = ETHTRCV_30_AR7000_U8PTRDATA_TO_VAL16(bufPtrModOp, 0);

  ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

  /* #10 Verify that received MME confirmation packet status is ok */
  if ( ETHTRCV_30_AR7000_MSTATUS_OK == mStatus)
  {
    /* #20 Set data pointer to Module Operation Header */
    bufPtrModOp = &DataPtr[15];

    modOp = ETHTRCV_30_AR7000_U8PTRDATA_TO_VAL16(bufPtrModOp, 0);

    /* #30 Verify that module operation was 'read module from QCA memory' (we only send read request's) */
    if ( ETHTRCV_30_AR7000_MODOP_RD_MEM == modOp )
    {
      EthTrcv_30_Ar7000_ModOpParams.ModOpDataLenTx = ETHTRCV_30_AR7000_U8PTRDATA_TO_VAL16(bufPtrModOp, 12);

      EthTrcv_30_Ar7000_ModOpParams.ModOpCurOffset = ETHTRCV_30_AR7000_U8PTRDATA_TO_VAL32(bufPtrModOp, 14);

      ethBufIdx = EthTrcv_30_Ar7000_ModOpParams.ModOpCurOffset - EthTrcv_30_Ar7000_ModOpParams.ModOpStartOffset;

      /* #40 Copy received module data */
      if ( EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpTotLen >= (ethBufIdx + EthTrcv_30_Ar7000_ModOpParams.ModOpDataLenTx) )
      {
        /* #50 First data chunk */
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpBufPtr[ethBufIdx >> 2],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&bufPtrModOp[18],
          EthTrcv_30_Ar7000_ModOpParams.ModOpDataLenTx
        );

        /* #60 Verify if more data chunks must be read */
        if (EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpTotLen > (ethBufIdx + EthTrcv_30_Ar7000_ModOpParams.ModOpDataLenTx) )
        {
          EthTrcv_30_Ar7000_ModOpParams.ModOpCurOffset += EthTrcv_30_Ar7000_ModOpParams.ModOpDataLenTx;
          /* PRQA S 0310 4 */ /* MD_EthTrcv_30_Ar7000_0310_3 */
          resVal = EthTrcv_30_Ar7000_MME_ModuleOperation(
            TrcvIdx,
            modOp,
            (P2VAR(EthTrcv_30_Ar7000_ModOp_Params, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))&EthTrcv_30_Ar7000_ModOpParams);
        }

        /* #70 Reading data chunks finished
         *        Reset ModOp-timeout counter
         *        If ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP is enabled
         *          Verify that received data indicates a PIB block
         *            Set RamCpy Indication to valid
         *        If ETHTRCV_30_AR7000_ENABLE_READ_NMK is enabled
         *          Verify that received data indicates a NMK
         *            Reset NMK timeout counter
         */
        else
        {
          EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpState = ETHTRCV_30_AR7000_MODOP_STATE_NOT_USED;
          EthTrcv_30_Ar7000_ModOpRetryCnt = 0;

#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP )
          /* PRQA S 0310, 3305 1 */ /* MD_EthTrcv_30_Ar7000_0310 */
          if (EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpBufPtr == (P2VAR(uint32, ETHTRCV_30_AR7000_VAR_NOINIT, ETHTRCV_30_AR7000_VAR_NOINIT))EthTrcv_30_Ar7000_PibRamCpy)
          {
            EthTrcv_30_Ar7000_PibRamCpyValid = TRUE;

            /* Callback */
#if defined ( ETHTRCV_30_AR7000_APPL_CBK_PIB_CHANGE )
            ETHTRCV_30_AR7000_APPL_CBK_PIB_CHANGE();
#endif /* ETHTRCV_30_AR7000_APPL_CBK_PIB_CHANGE */

            EthTrcv_30_Ar7000_WriteAppletLockCnt = 0;
            EthTrcv_30_Ar7000_AppletNrLocked = ETHTRCV_30_AR7000_NO_APPLET_LOCKED;

            if (TRUE == EthTrcv_30_Ar7000_WillResetAfterPibUpload)
            {
              resVal = EthTrcv_30_Ar7000_ResetDevice(TrcvIdx);
            }
            else
            {
              resVal = E_OK;
            }
          }
          else
          {
            resVal = E_OK;
          }
#endif /* ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP */
#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_READ_NMK )
          /* PRQA S 0310, 3305 1 */ /* MD_EthTrcv_30_Ar7000_0310_4 */
          if ( EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpBufPtr == (P2VAR(uint32, ETHTRCV_30_AR7000_VAR_NOINIT, ETHTRCV_30_AR7000_VAR_NOINIT))EthTrcv_30_Ar7000_Nmk )
          {
            EthTrcv_30_Ar7000_IsReadingNmk = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
            EthTrcv_30_Ar7000_NmkReady = ETHTRCV_30_AR7000_STATE_READY;
            EthTrcv_30_Ar7000_ReadNmkCycCnt = 0;

            EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpState = ETHTRCV_30_AR7000_MODOP_STATE_NOT_USED;
            /* Callback */
#if defined ( ETHTRCV_30_AR7000_APPL_CBK_READ_NMK )
            ETHTRCV_30_AR7000_APPL_CBK_READ_NMK();
#endif /* ETHTRCV_30_AR7000_APPL_CBK_READ_NMK */
          }
#endif /* ETHTRCV_30_AR7000_ENABLE_READ_NMK */
        }
      }
      else
      {
        resVal = E_NOT_OK;
      }
    }
  }
  else
  {
    resVal = E_NOT_OK;
  }

  /* #80 Retry the module operation 'NMK read' or 'PIB read' if a failure occurred in the previously sequence */
  if ( E_NOT_OK == resVal)
  {
    /* try again */
#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP )
    /* PRQA S 0310, 3305 1 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
    if (EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpBufPtr == (P2VAR(uint32, ETHTRCV_30_AR7000_VAR_NOINIT, ETHTRCV_30_AR7000_VAR_NOINIT))EthTrcv_30_Ar7000_PibRamCpy)
    {
      EthTrcv_30_Ar7000_ModOpParams.ModOpDataLenTx    = ETHTRCV_30_AR7000_MODOP_MAX_DATA_SIZE;
      EthTrcv_30_Ar7000_ModOpParams.ModOpCurOffset    = 0x00000000u;
      EthTrcv_30_Ar7000_ModOpParams.ModOpStartOffset  = 0x00000000u;
    }
#endif /* ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP */
#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_READ_NMK )
    /* PRQA S 0310, 3305 1 */ /* MD_EthTrcv_30_Ar7000_0310_4 */
    if ( EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpBufPtr == (P2VAR(uint32, ETHTRCV_30_AR7000_VAR_NOINIT, ETHTRCV_30_AR7000_VAR_NOINIT))EthTrcv_30_Ar7000_Nmk )
    {
      EthTrcv_30_Ar7000_ModOpParams.ModOpDataLenTx    = ETHTRCV_30_AR7000_NMK_SIZE_BYTE;
      EthTrcv_30_Ar7000_ModOpParams.ModOpCurOffset    = ETHTRCV_30_AR7000_NMK_PIB_OFFSET;
      EthTrcv_30_Ar7000_ModOpParams.ModOpStartOffset  = ETHTRCV_30_AR7000_NMK_PIB_OFFSET;
    }
#endif /* ETHTRCV_30_AR7000_ENABLE_READ_NMK */

    EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpState = ETHTRCV_30_AR7000_MODOP_STATE_NOT_USED;

    /* update module operation timeout counter and send the module operation MME request again */
    if ( ETHTRCV_30_AR7000_MODOP_MAX_RETRANS >= EthTrcv_30_Ar7000_ModOpRetryCnt )
    {
      EthTrcv_30_Ar7000_ModOpRetryCnt++;
      /* PRQA S 0310 4 */ /* MD_EthTrcv_30_Ar7000_0310_3 */
      resVal = EthTrcv_30_Ar7000_MME_ModuleOperation(
        TrcvIdx,
        ETHTRCV_30_AR7000_MODOP_RD_MEM,
        (P2VAR(EthTrcv_30_Ar7000_ModOp_Params, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))&EthTrcv_30_Ar7000_ModOpParams);

      if ( E_NOT_OK == resVal )
      {
        /* no further retransmissions at this point */
        EthTrcv_30_Ar7000_ModOpRetryCnt = 0;
      }
    }
    else
    {
      /* No further retransmissions at this point - try again later */
      EthTrcv_30_Ar7000_ModOpRetryCnt = 0;
    }
  }

  ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
} /* EthTrcv_30_Ar7000_ModuleOperation() */ /* PRQA S 6010, 6030, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STMIF */
#endif /* ( STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ) ||
          ( STD_ON == ETHTRCV_30_AR7000_ENABLE_READ_NMK)    ||
          ( STD_ON == ETHTRCV_30_AR7000_ENABLE_MODOP_API) */

#if ( ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_PBEncryption
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_PBEncryption(
        uint8                                                          TrcvIdx,
        EthTrcv_30_Ar7000_PBActionType                                 PBAction,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) MStatus,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) AVLNStatus)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_ReturnType retVal = ETHTRCV_30_AR7000_RT_NOT_OK; /* Return value of this function */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Verify timeout counter (see MainFunction()) not exceeded */
  if ( ETHTRCV_30_AR7000_CONFTIMEOUT <= EthTrcv_30_Ar7000_SCConfCycCnt )
  {
    EthTrcv_30_Ar7000_SCStateConfReady = ETHTRCV_30_AR7000_STATE_NOT_READY;
    EthTrcv_30_Ar7000_IsChangingSCState = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
    EthTrcv_30_Ar7000_SCConfCycCnt = 0;

    /* ETHTRCV_30_AR7000_RT_NOT_OK */
  }

  /* #20 Verify QCA confirmation
   *       Inform caller if it is still pending
   */
  else if ( ETHTRCV_30_AR7000_WAIT_ON_CONF == EthTrcv_30_Ar7000_IsChangingSCState )
  {
    retVal = ETHTRCV_30_AR7000_RT_PENDING;
  }

  /* #30 Verify received QCA confirmation
   *       Fill output parameter with network status information and received MME confirmation status
   */
  else if ( (ETHTRCV_30_AR7000_NO_WAIT_ON_CONF == EthTrcv_30_Ar7000_IsChangingSCState) &&
       (ETHTRCV_30_AR7000_STATE_READY == EthTrcv_30_Ar7000_SCStateConfReady)
     )
  {
    EthTrcv_30_Ar7000_SCStateConfReady = ETHTRCV_30_AR7000_STATE_NOT_READY;
    *MStatus = EthTrcv_30_Ar7000_PbMStatus;
    *AVLNStatus = EthTrcv_30_Ar7000_PbAvlnStatus;
    retVal = ETHTRCV_30_AR7000_RT_OK;
  }

  /* #40 No PBEncrytion() call pending
   *       Send push button encryption MME request to QCA
   *         Activate the timeout counter
   *         Inform the caller that QCA confirmation is pending
   */
  else
  {

    if ( E_OK == EthTrcv_30_Ar7000_MME_PBEncryption(TrcvIdx, PBAction) )
    {
      EthTrcv_30_Ar7000_IsChangingSCState = ETHTRCV_30_AR7000_WAIT_ON_CONF;
      EthTrcv_30_Ar7000_SCStateConfReady = ETHTRCV_30_AR7000_STATE_NOT_READY;
#if (defined ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD )
      /* NMK will be randomized, set link information to DOWN and activate LinkStateLockCnt, to prevent link state toggling.
       * Else QCA7005 may indicate a link during NMK change process even if there is no link available.
       */
      EthTrcv_30_Ar7000_LinkStateLockCnt = ETHTRCV_30_AR7000_LINKSTATE_LOCK_CYCLES;
      EthTrcv_30_Ar7000_LinkState        = ETHTRCV_LINK_STATE_DOWN;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

      retVal = ETHTRCV_30_AR7000_RT_PENDING;
    }
    else
    {
      /* ETHTRCV_30_AR7000_RT_NOT_OK */
    }
  }

  return retVal;
} /* EthTrcv_30_Ar7000_PBEncryption() */
#endif /* ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API == STD_ON */

/**********************************************************************************************************************
 *  Others
 **********************************************************************************************************************/
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_VFrameHeader
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_VFrameHeader(
    uint8                                                                                               TrcvIdx,
    uint16                                                                                              MMType,
    P2VAR(EthTrcv_30_Ar7000_EthernetBufferType, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) EthBufPtr)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthBufPtr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else if (EthBufPtr->BufLen < ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_CONFIG;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

    /* #20 Initialize the given Ethernet buffer to zero */
    /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310 */
    IpBase_Fill((P2VAR(IpBase_CopyDataType, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR))&EthBufPtr->Buffer.U8[6],
      0x00, ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE - 6);

    /* #30 Set MME-Header
     *       MME-Version: 0
     *       MME-Type: MMType
     *       Atheros-OUI
     */
    EthBufPtr->Buffer.U8[0] = (uint8)0x00;          /* MMV */
    EthBufPtr->Buffer.U8[1] = (uint8)(MMType >> 0); /* MMTYPE */
    EthBufPtr->Buffer.U8[2] = (uint8)(MMType >> 8); /* MMTYPE */
    EthBufPtr->Buffer.U8[3] = (uint8)0x00;          /* Atheros OUI */
    EthBufPtr->Buffer.U8[4] = (uint8)0xb0;          /* Atheros OUI */
    EthBufPtr->Buffer.U8[5] = (uint8)0x52;          /* Atheros OUI */
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_V_FRAME_HEADER, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif
} /* EthTrcv_30_Ar7000_VFrameHeader() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_VFrameHeaderGeneric
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_VFrameHeaderGeneric(
        uint8                                                                                         TrcvIdx,
        uint16                                                                                        MMType,
  P2VAR(EthTrcv_30_Ar7000_EthernetBufferType, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) EthBufPtr,
        uint16                                                                                        ZeroInitLenByte)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthBufPtr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else if (EthBufPtr->BufLen < ZeroInitLenByte)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_CONFIG;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

    /* #20 Initialize the given Ethernet buffer to zero with ZeroInitLenByte bytes */
    /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310 */
    IpBase_Fill((P2VAR(IpBase_CopyDataType, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR))&EthBufPtr->Buffer.U8[5],
      0x00, ZeroInitLenByte - 5);

    /* #30 Set generic MME-Header parts:
     *       MME-Version = 1
     *       MME-Fragementation = no
     *       MME-Type = MMType
     */
    EthBufPtr->Buffer.U8[0] = (uint8)0x01;          /* MMV */
    EthBufPtr->Buffer.U8[1] = (uint8)(MMType >> 0); /* MMTYPE */
    EthBufPtr->Buffer.U8[2] = (uint8)(MMType >> 8); /* MMTYPE */
    EthBufPtr->Buffer.U8[3] = (uint8)0x00;          /* No fragmentation */
    EthBufPtr->Buffer.U8[4] = (uint8)0x00;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_V_FRAME_HEADER, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif
} /* EthTrcv_30_Ar7000_VFrameHeaderGeneric() */

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_VCalcChecksum
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(uint32, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_VCalcChecksum(
    uint8 TrcvIdx,
    P2CONST(uint32, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr,
    uint32 DataLenByte)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint32 v32BitXor = 0;
  uint32 u32Idx;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

  /* #10 Converse DataLenByte to use it as index for a 32it(4byte) array */
  DataLenByte = DataLenByte >> 2;

  /* #20 Iterate over the data array and XOR each element */
  u32Idx = DataLenByte;
  while ( 0 < u32Idx)
  {
    u32Idx--;
    v32BitXor ^= DataPtr[u32Idx];/* XOR each word */
  }

  /* #30 The finally checksum is the one's complement of the previous XOR iteration */
  v32BitXor = ~v32BitXor;

  return v32BitXor;
} /* EthTrcv_30_Ar7000_VCalcChecksum() */
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#if ( ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)) || \
      ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)          && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK))          || \
      ((defined ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)      && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)))     || \
      ((defined ETHTRCV_30_AR7000_CALC_NMK_LOCALLY)          && (STD_ON == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY))
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_VGetPibModuleOffset
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC (uint8, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_VGetPibModuleOffset(
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_DATA)                            PibPtr,
  P2VAR(uint8*, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_DATA)                           ModOffset,
  P2VAR(EthTrcv_30_Ar7000_NVMHeaderType*, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_DATA) ModHdrOffset)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  P2VAR(EthTrcv_30_Ar7000_NVMHeaderType, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_DATA) curModHeader;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_5 */
  curModHeader = (P2VAR(EthTrcv_30_Ar7000_NVMHeaderType, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_DATA)) PibPtr;

  /* #10 Iterate over the NVM chain to find the PIB module header
   *     Iteration is done if we found the header or if we reached the last element in the chain
   */
  /* PRQA S 3415 2 */ /* MD_EthTrcv_30_Ar7000_3415 */
  while ( (ETHTRCV_30_AR7000_HTOLIEN32(curModHeader->EntryType) != ETHTRCV_30_AR7000_ENTRY_TYPE_PIB) &&
         ( ETHTRCV_30_AR7000_HTOLIEN32(curModHeader->NextNvmHeaderPtr) != 0xFFFFFFFFu) )
  {
    /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_5 */
    curModHeader = (P2VAR(EthTrcv_30_Ar7000_NVMHeaderType, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_DATA))
                      &PibPtr[ETHTRCV_30_AR7000_HTOLIEN32(curModHeader->NextNvmHeaderPtr)];
  }

  /* #20 Inform calling routine if PIB module was found within the chain */
  if ( ETHTRCV_30_AR7000_ENTRY_TYPE_PIB == ETHTRCV_30_AR7000_HTOLIEN32(curModHeader->EntryType) )
  {
    /* PIB Module Header found */
    *ModOffset = &PibPtr[ETHTRCV_30_AR7000_HTOLIEN32(curModHeader->ImageNvmAddress)];
    *ModHdrOffset = (P2VAR(EthTrcv_30_Ar7000_NVMHeaderType, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_DATA))curModHeader;

    retVal = E_OK;
  }

  return retVal;
} /* EthTrcv_30_Ar7000_VGetPibModuleOffset() */
#endif /* STD_ON == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY          ||
          STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS ||
          STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK          ||
          STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK */


/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_InitMemory
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_InitMemory( void )
{
  /* --------------------------------------------- Implementation -------------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  /* #10 If DET is enabled set state to UNINIT */
  EthTrcv_30_Ar7000_State = ETHTRCV_STATE_UNINIT;
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON */


  return;
} /* EthTrcv_30_Ar7000_InitMemory() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Init
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Init(
    P2CONST(EthTrcv_30_Ar7000_ConfigType, AUTOMATIC, AUTOMATIC) CfgPtr)
{
  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(CfgPtr); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

  EthTrcv_30_Ar7000_CycleCnt = ETHTRCV_30_AR7000_LINKSTATEPOLLINGFREQ;

#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  EthTrcv_30_Ar7000_State = ETHTRCV_STATE_INIT;
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON */

  /* #10 Set PIB data block */
#if (defined ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD )
  EthTrcv_30_Ar7000_PIBData = PIBData;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

  /* #20 If ETHTRCV_30_AR7000_ENABLE_SLAC is enabled
   *       Initialize SLAC protocol control struct
   */
#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC )
  EthTrcv_30_Ar7000_Slac_Init();
#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */

#if ( ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON )
  EthTrcv_30_Ar7000_SetKeyCnt = 0;
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_NMK */
} /* EthTrcv_30_Ar7000_Init() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_TransceiverInit
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_TransceiverInit(
    uint8 TrcvIdx,
    uint8 CfgIdx)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
#if (   ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)) \
     || ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)          && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)) \
     || ((defined ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)      && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)) )
  P2VAR(EthTrcv_30_Ar7000_NVMHeaderType, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) moduleHdrOffset;
  P2VAR(uint8, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) pibModuleOffset;
  uint32 pibChecksum;
#endif /* (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) ||
          (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)          ||
          (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK) */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal; /* Return value of this function */
  Std_ReturnType resVal; /* Result value of called functions */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = E_NOT_OK;
  if (EthTrcv_30_Ar7000_State == ETHTRCV_STATE_UNINIT)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (CfgIdx >= ETHTRCV_30_AR7000_MAX_CFGS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_CONFIG;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 trigger start of runtime measurement
     *     Set link-state and mode-state to down as default
     *     Initialize all transceiver related variables and structs
     */
    EthTrcv_30_Ar7000_Rtm_Start(TransceiverInit);

#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
    EthTrcv_30_Ar7000_State = ETHTRCV_STATE_ACTIVE;
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON */

    /* Link-State is per default down */
    EthTrcv_30_Ar7000_LinkState = ETHTRCV_LINK_STATE_DOWN;
    EthTrcv_30_Ar7000_HeartBeatCnt = 0;
    EthTrcv_30_Ar7000_DwldAttempts = ETHTRCV_30_AR7000_FW_DWLD_ATTEMPTS;

    /* Mode state is also down */
    /* Init controller and CPU mode */
    resVal = EthTrcv_30_Ar7000_SetTransceiverMode(TrcvIdx, ETHTRCV_MODE_DOWN);

    if (E_OK == resVal)
    {

#if ( ETHTRCV_30_AR7000_ENABLE_READ_NMK == STD_ON )
      EthTrcv_30_Ar7000_IsReadingNmk  = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
      EthTrcv_30_Ar7000_NmkReady      = ETHTRCV_30_AR7000_STATE_NOT_READY;
      EthTrcv_30_Ar7000_ReadNmkCycCnt = 0;
#endif /* ETHTRCV_30_AR7000_ENABLE_READ_NMK */

#if ( ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API == STD_ON )
      EthTrcv_30_Ar7000_IsChangingSCState = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
      EthTrcv_30_Ar7000_SCStateConfReady  = ETHTRCV_30_AR7000_STATE_NOT_READY;
      EthTrcv_30_Ar7000_SCConfCycCnt      = 0;
      EthTrcv_30_Ar7000_PbAvlnStatus      = 0;
      EthTrcv_30_Ar7000_PbMStatus         = 0;
#endif /* ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API */

#if ( ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON )
      EthTrcv_30_Ar7000_SetKeyIsChanging = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
      EthTrcv_30_Ar7000_SetKeyConfReady = ETHTRCV_30_AR7000_STATE_NOT_READY;
      EthTrcv_30_Ar7000_SetKeyConfCycCnt = 0;
      EthTrcv_30_Ar7000_SetKeyMStatus = 0;
#endif /* ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API */

#if ( ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION == STD_ON )
      EthTrcv_30_Ar7000_IsReadingSwVersion = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
      EthTrcv_30_Ar7000_SWVersionAvailable = ETHTRCV_30_AR7000_STATE_NOT_READY;
      EthTrcv_30_Ar7000_GetSwVersionCycCnt = 0;
      EthTrcv_30_Ar7000_SwMStatus          = 0;
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION */

#if ( ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API == STD_ON )
      EthTrcv_30_Ar7000_IsSettingProperty       = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
      EthTrcv_30_Ar7000_SetPropertyCnfAvailable = ETHTRCV_30_AR7000_STATE_NOT_READY;
      EthTrcv_30_Ar7000_SetPropertyCycCnt       = 0;
      EthTrcv_30_Ar7000_SetPropertyStatus       = 0;
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API */

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
      EthTrcv_30_Ar7000_DownlStatus                = ETHTRCV_30_AR7000_DOWNLSTATUS_MEMCTRL;
      EthTrcv_30_Ar7000_ClientSid                  = 0;
      EthTrcv_30_Ar7000_FailedApiCall              = (EthTrcv_30_Ar7000_FailedApiCallType)0;
      EthTrcv_30_Ar7000_ApiCallRetCnt              = 0;
      EthTrcv_30_Ar7000_WriteAppletLockCnt         = 0;
      EthTrcv_30_Ar7000_AppletNrLocked             = ETHTRCV_30_AR7000_NO_APPLET_LOCKED;
# if ( ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT == STD_ON )
      EthTrcv_30_Ar7000_FwDownloadAndStartCnt = ETHTRCV_30_AR7000_FIRMWARE_DOWNLOAD_AND_START_CYCLES;
# endif /* ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT */
# if ( ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_SLAC_COM_EVENT == STD_ON )
      EthTrcv_30_Ar7000_FwReadyForPlcComCnt = ETHTRCV_30_AR7000_FIRMWARE_READY_FOR_SLAC_COM_CYCLES;
# endif /* ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_SLAC_COM_EVENT */
      EthTrcv_30_Ar7000_LinkStateLockCnt           = ETHTRCV_30_AR7000_LINKSTATE_LOCK_CYCLES;
      {
        uint8_least hstActionIdx;
        for (hstActionIdx = 0; hstActionIdx < ETHTRCV_30_AR7000_HST_ACTION_QUEUE_SIZE; hstActionIdx++)
        {
          EthTrcv_30_Ar7000_HstAction[hstActionIdx] = ETHTRCV_30_AR7000_NO_HST_ACTION_REQUIRED;
        }
        EthTrcv_30_Ar7000_HstActionIdx = 0;
        EthTrcv_30_Ar7000_HstActionCnt = 0;
      }
# if ((defined ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD) && (STD_OFF==ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD))
      EthTrcv_30_Ar7000_MME_WaEA_Params.NeedToSend = FALSE;
# endif /* ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD */
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START )
      EthTrcv_30_Ar7000_FwDwldStartCbkCalled = FALSE;
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START */

#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_START )
      EthTrcv_30_Ar7000_FwStartedCbkCalled = FALSE;
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_START */

#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC )
      EthTrcv_30_Ar7000_FwReadyForSlacCbkCalled = FALSE;
      EthTrcv_30_Ar7000_FwReadyForSlacLastResult = FALSE;
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC */

#if (   (ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP == STD_ON) \
     || (ETHTRCV_30_AR7000_ENABLE_READ_NMK == STD_ON) \
     || (ETHTRCV_30_AR7000_ENABLE_MODOP_API == STD_ON) )
      EthTrcv_30_Ar7000_ModOpRetryCnt                       = 0;
      EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpState   = ETHTRCV_30_AR7000_MODOP_STATE_NOT_USED;
      EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpLockCnt = 0;
      EthTrcv_30_Ar7000_ModOpCycCnt                         = 0;
#endif /* ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP || ETHTRCV_30_AR7000_ENABLE_MODOP_API ||
        * ETHTRCV_30_AR7000_ENABLE_READ_NMK */

#if (   (STD_ON == ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD) \
     && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP) )
      EthTrcv_30_Ar7000_WillResetAfterPibUpload = FALSE;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD && ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP */

#if (   ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)) \
     || ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)          && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)) \
     || ((defined ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)      && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)) )

      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      /* #30 Copy PIB to RAM */
      /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
      IpBase_Copy(
            (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_PibRamCpy,
            (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_PIBData,
            ETHTRCV_30_AR7000_PIB_SIZE
          );

      resVal = EthTrcv_30_Ar7000_VGetPibModuleOffset(EthTrcv_30_Ar7000_PibRamCpy, &pibModuleOffset, &moduleHdrOffset);

      if ( E_OK == resVal )
      {
# if (   ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)) \
     || ((defined ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)      && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)) )
        /* Set MAC address */
        pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 0u] = EthTrcv_30_Ar7000_VCfgGetPhysAddr()[0];
        pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 1u] = EthTrcv_30_Ar7000_VCfgGetPhysAddr()[1];
        pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 2u] = EthTrcv_30_Ar7000_VCfgGetPhysAddr()[2];
        pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 3u] = EthTrcv_30_Ar7000_VCfgGetPhysAddr()[3];
        pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 4u] = EthTrcv_30_Ar7000_VCfgGetPhysAddr()[4];
        pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 5u] = EthTrcv_30_Ar7000_VCfgGetPhysAddr()[5];
# endif /* ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS || ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK */

# if (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)
        /* Write DAK */
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310_1 */
        IpBase_Copy(
            (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&pibModuleOffset[ETHTRCV_30_AR7000_DAK_PIB_OFFSET],
            (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_VCfgGetDak(),
            ETHTRCV_30_AR7000_DAK_SIZE_BYTE
          );
# endif /* ETHTRCV_30_AR7000_ENABLE_WRITE_DAK */

        /* Checksum module */
        moduleHdrOffset->ImageChecksum = 0x00000000;  /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
        pibChecksum = EthTrcv_30_Ar7000_VCalcChecksum(TrcvIdx,
          (P2VAR(uint32, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))pibModuleOffset, ETHTRCV_30_AR7000_LIENTOH32(moduleHdrOffset->ImageLength)); /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        moduleHdrOffset->ImageChecksum = pibChecksum;    /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */

        /* Checksum header */
        moduleHdrOffset->HeaderChecksum = 0x00000000;  /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */

        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_5 */
        pibChecksum = EthTrcv_30_Ar7000_VCalcChecksum(TrcvIdx,
          (P2VAR(uint32, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))moduleHdrOffset, sizeof(*moduleHdrOffset)); /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        moduleHdrOffset->HeaderChecksum = pibChecksum;    /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */

        EthTrcv_30_Ar7000_PibRamCpyValid = TRUE;
      }
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
#else
# if (((defined ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP)) || \
     ((defined ETHTRCV_30_AR7000_CALC_NMK_LOCALLY) && (STD_ON == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY)))
      EthTrcv_30_Ar7000_PibRamCpyValid = FALSE;
# endif /* ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP || ETHTRCV_30_AR7000_CALC_NMK_LOCALLY */

#endif /* ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS || ETHTRCV_30_AR7000_ENABLE_WRITE_DAK || ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK */
    }

    /* Set return value */
    retVal = resVal;

    /* #40 trigger stop of runtime measurement */
    EthTrcv_30_Ar7000_Rtm_Stop(TransceiverInit);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_TRANSCEIVER_INIT, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(CfgIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_TransceiverInit() */ /* PRQA S 6080, 6050 */ /* MD_MSR_STMIF, MD_MSR_STCAL */

#if ( ETHTRCV_30_AR7000_ENABLE_SET_TRCV_MODE == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetTransceiverMode
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetTransceiverMode(
  uint8            TrcvIdx,
  EthTrcv_ModeType TrcvMode)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal; /* Return value of this function */
  Std_ReturnType resVal; /* Result value of called functions */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = E_NOT_OK;
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Enable/disable transceiver
     *     Verify that enable/disable was OK
     *       Set ModeState
     *       Set LinkState to down if transceiver was disabled
     */
    if ( ETHTRCV_MODE_ACTIVE == TrcvMode )
    {
      /* enable core */
      resVal = ETH_WRITESPI(EthTrcv_30_Ar7000_VCfgGetEthCtrlIdx(), ETHTRCV_30_AR7000_ETH_SPI_REG_CONFIG, ETHTRCV_30_AR7000_SPI_IO_ENABLE);
    }
    else
    {
      /* disable core */
      resVal = ETH_WRITESPI(EthTrcv_30_Ar7000_VCfgGetEthCtrlIdx(), ETHTRCV_30_AR7000_ETH_SPI_REG_CONFIG, ETHTRCV_30_AR7000_SPI_IO_DISABLE);
    }

    if (E_OK == resVal)
    {
      EthTrcv_30_Ar7000_ModeState = TrcvMode;
      if (ETHTRCV_MODE_DOWN == TrcvMode)
      {
        EthTrcv_30_Ar7000_LinkState = ETHTRCV_LINK_STATE_DOWN;
      }
    }

    /* Set return value */
    retVal = resVal;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_SET_TRANSCEIVER_MODE, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_SetTransceiverMode() */
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_TRCV_MODE */

#if ( ETHTRCV_30_AR7000_ENABLE_GET_TRCV_MODE == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_GetTransceiverMode
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_GetTransceiverMode(
        uint8                                   TrcvIdx,
  P2VAR(EthTrcv_ModeType, AUTOMATIC, AUTOMATIC) TrcvModePtr)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = E_NOT_OK;
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (TrcvModePtr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Inform calling application about mode state */
    *TrcvModePtr = EthTrcv_30_Ar7000_ModeState;

    /* Set return value */
    retVal = E_OK;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_GET_TRANSCEIVER_MODE, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif


  return retVal;
} /* EthTrcv_30_Ar7000_GetTransceiverMode() */
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_TRCV_MODE */

#if ( ETHTRCV_30_AR7000_ENABLE_START_AUTO_NEG == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_StartAutoNegotiation
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_StartAutoNegotiation(
  uint8 TrcvIdx)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else
#endif
  {
    /* Auto-Negotiation is not supported by QCA */
    /* Return E_NOT_OK, this is not supported */
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_START_AUTO_NEG, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_StartAutoNegotiation() */
#endif /* ETHTRCV_30_AR7000_ENABLE_START_AUTO_NEG */

#if ( ETHTRCV_30_AR7000_ENABLE_GET_LINK_STATE == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_GetLinkState
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_GetLinkState(
        uint8                                        TrcvIdx,
  P2VAR(EthTrcv_LinkStateType, AUTOMATIC, AUTOMATIC) LinkStatePtr)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = E_NOT_OK;
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (LinkStatePtr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Inform calling application about link state */
    if(ETHTRCV_LINK_STATE_DOWN == EthTrcv_30_Ar7000_LinkState)
    {
      *LinkStatePtr = ETHTRCV_LINK_STATE_DOWN;
    }
    else
    {
      *LinkStatePtr = ETHTRCV_LINK_STATE_ACTIVE;
    }
    /* Set return value */
    retVal = E_OK;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_GET_LINK_STATE, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_GetLinkState() */
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_LINK_STATE */

#if ( ETHTRCV_30_AR7000_ENABLE_GET_BAUD_RATE == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_GetBaudRate
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_GetBaudRate(
        uint8                                       TrcvIdx,
  P2VAR(EthTrcv_BaudRateType, AUTOMATIC, AUTOMATIC) BaudRatePtr)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = E_NOT_OK;
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (BaudRatePtr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Inform calling application */
    /* Baudrate of 100MBit with QCA7005 not possible */
    *BaudRatePtr = ETHTRCV_BAUD_RATE_10MBIT;
    /* Set return value */
    retVal = E_OK;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_GET_BAUD_RATE, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_GetBaudRate() */
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_BAUD_RATE */

#if ( ETHTRCV_30_AR7000_ENABLE_GET_DUPLEX_MODE == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_GetDuplexMode
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_GetDuplexMode(
    uint8 TrcvIdx,
    P2VAR(EthTrcv_DuplexModeType, AUTOMATIC, AUTOMATIC) DuplexModePtr)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = E_NOT_OK;
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (DuplexModePtr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Inform calling application */
    *DuplexModePtr = ETHTRCV_DUPLEX_MODE_FULL;
    /* Set return value */
    retVal = E_OK;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_GET_DUPLEX_MODE, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_GetDuplexMode() */
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_DUPLEX_MODE */

#if ( ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetNmk
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetNmk(
    uint8 TrcvIdx,
    P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) NmkPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
#if ( STD_ON == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY )
  P2VAR(EthTrcv_30_Ar7000_NVMHeaderType, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) moduleHdrOffset;
  P2VAR(uint8, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) pibModuleOffset;
  uint32 pibChecksum;
  uint8_least pibIdx;
  uint8 hashedNid [32];
#endif
#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
  uint8_least slacIdx;
#endif
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal; /* Return value of this function */
  Std_ReturnType resVal; /* Result value of called functions */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = E_NOT_OK;
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (NmkPtr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If ETHTRCV_30_AR7000_CALC_NMK_LOCALLY is enabled
     *       Just send the network membership key to the QCA (not permanently).
     *     else
     *       Update the network membership key within the PIB (host side) and send the PIB and firmware again to the QCA.
     *       (permanently).
     */
#if ( STD_OFF == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY )
    resVal = EthTrcv_30_Ar7000_MME_SetKey(TrcvIdx, NmkPtr);
#else
    ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    /* Set key directly in the RAM copy of the PIB. */

    if ( FALSE == EthTrcv_30_Ar7000_PibRamCpyValid )
    {
      /* Copy PIB to RAM */
      /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
      IpBase_Copy(
        (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_PibRamCpy,
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_PIBData,
        ETHTRCV_30_AR7000_PIB_SIZE
      );
    }

    resVal = ETHTRCV_30_AR7000_HASH_NID(NmkPtr, hashedNid);
    if ( E_OK == resVal )
    {
      resVal = EthTrcv_30_Ar7000_VGetPibModuleOffset(EthTrcv_30_Ar7000_PibRamCpy, &pibModuleOffset, &moduleHdrOffset);

      if ( E_OK == resVal )
      {
        for (pibIdx = 0; pibIdx < ETHTRCV_30_AR7000_NID_SIZE_BYTE; pibIdx++)
        {
          pibModuleOffset[ETHTRCV_30_AR7000_NID_PIB_OFFSET + pibIdx] = hashedNid[pibIdx];
        }

        for (pibIdx = 0; pibIdx < ETHTRCV_30_AR7000_NMK_SIZE_BYTE; pibIdx++)
        {
          pibModuleOffset[ETHTRCV_30_AR7000_NMK_PIB_OFFSET + pibIdx] = NmkPtr[pibIdx];
        }

        /* Checksum module */
        moduleHdrOffset->ImageChecksum = 0x00000000;  /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
        pibChecksum = EthTrcv_30_Ar7000_VCalcChecksum(TrcvIdx, \
          (P2VAR(uint32, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))pibModuleOffset, ETHTRCV_30_AR7000_LIENTOH32(moduleHdrOffset->ImageLength)); /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        moduleHdrOffset->ImageChecksum = pibChecksum;    /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */

         /* Checksum header */
        moduleHdrOffset->HeaderChecksum = 0x00000000;  /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_5 */
        pibChecksum = EthTrcv_30_Ar7000_VCalcChecksum(TrcvIdx, \
          (P2VAR(uint32, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))moduleHdrOffset, sizeof(*moduleHdrOffset)); /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        moduleHdrOffset->HeaderChecksum = pibChecksum;    /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */

        EthTrcv_30_Ar7000_PibRamCpyValid = TRUE;

        /* #30 Send new NMK and NID to QCA7005 in case firmware was already downloaded */
        if (EthTrcv_30_Ar7000_DownlStatus == ETHTRCV_30_AR7000_DOWNLSTATUS_DONE)
        {
          /* Do not reset QCA7005, it will decide if a reset is necessary, depending on the PIB configuration */
          resVal = EthTrcv_30_Ar7000_MME_CmSetKeyReq(TrcvIdx, &hashedNid[0], NmkPtr);
        }
      }
    }
#endif /* ETHTRCV_30_AR7000_CALC_NMK_LOCALLY */

    /* #40 If ETHTRCV_30_AR7000_ENABLE_SLAC is enabled
     *       Store the NMK and NID for later usage
     */
#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
    if (E_OK == resVal)
    {
      /* Store NMK for later DLink indications */
      for(slacIdx = 0; slacIdx < ETHTRCV_30_AR7000_NMK_SIZE_BYTE; slacIdx++)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk[slacIdx] = NmkPtr[slacIdx];
      }
      /* Store NID value */
      for(slacIdx = 0; slacIdx < ETHTRCV_30_AR7000_NID_SIZE_BYTE; slacIdx++)
      {
# if ( STD_OFF == ETHTRCV_30_AR7000_CALC_NMK_LOCALLY )
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid[slacIdx] = 0;
# else
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid[slacIdx] = hashedNid[slacIdx];
# endif
      }
    }
#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */
#if ( STD_OFF != ETHTRCV_30_AR7000_CALC_NMK_LOCALLY )
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
#endif
    /* Set return value */
    retVal = resVal;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_SET_NMK, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_SetNmk() */ /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_NMK */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetNmkAndNid
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetNmkAndNid(
    uint8 TrcvIdx,
    P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) NmkPtr,
    P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) NidPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
  uint8_least nmkIdx;
  uint8_least nidIdx;
#endif
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal; /* Return value of this function */
  Std_ReturnType resVal; /* Result value of called functions */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = E_NOT_OK;
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (NmkPtr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else if (NidPtr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Send MME request for setting network identifier and network membership key to QCA */
    resVal = EthTrcv_30_Ar7000_MME_CmSetKeyReq(TrcvIdx, NidPtr, NmkPtr);

#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
    /* #30 Verify that request return value is okay
     *       Store the NID and NMK for later usage
     */
    if (E_OK == resVal)
    {
      /* Store NMK for later DLink indications */
      for(nmkIdx = 0; nmkIdx < ETHTRCV_30_AR7000_NMK_SIZE_BYTE; nmkIdx++)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk[nmkIdx] = NmkPtr[nmkIdx];
      }
      /* Store NID value */
      for(nidIdx = 0; nidIdx < ETHTRCV_30_AR7000_NID_SIZE_BYTE; nidIdx++)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid[nidIdx] = NidPtr[nidIdx];
      }
    }
#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */
    /* Set return value */
    retVal = resVal;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_SET_NMK_AND_NID, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_SetNmkAndNid() */ /* PRQA S 6080 */ /* MD_MSR_STMIF */

#if ( ETHTRCV_30_AR7000_ENABLE_READ_NMK == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_ReadNmk
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_ReadNmk(
    uint8 TrcvIdx,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) NmkPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8_least nmkIdx;
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  EthTrcv_30_Ar7000_ReturnType retVal = ETHTRCV_30_AR7000_RT_NOT_OK; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (NmkPtr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Verify timeout counter (see MainFunction()) not exceeded */
    if ( ETHTRCV_30_AR7000_CONFTIMEOUT <= EthTrcv_30_Ar7000_ReadNmkCycCnt )
    {
      EthTrcv_30_Ar7000_NmkReady = ETHTRCV_30_AR7000_STATE_NOT_READY;
      EthTrcv_30_Ar7000_IsReadingNmk = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
      EthTrcv_30_Ar7000_ReadNmkCycCnt = 0;

      /* ETHTRCV_30_AR7000_RT_NOT_OK */
    }

    /* #30 Inform calling application that QCA confirmation is pending */
    else if ( ETHTRCV_30_AR7000_WAIT_ON_CONF == EthTrcv_30_Ar7000_IsReadingNmk )
    {
      retVal = ETHTRCV_30_AR7000_RT_PENDING;
    }

    /* #40 Got QCA confirmation
     *       Return current set network membership key
     */
    else if (   (ETHTRCV_30_AR7000_NO_WAIT_ON_CONF == EthTrcv_30_Ar7000_IsReadingNmk)
             && (ETHTRCV_30_AR7000_STATE_READY == EthTrcv_30_Ar7000_NmkReady) )
    {
      /* Copy NMK */
      for(nmkIdx = 0; nmkIdx < ETHTRCV_30_AR7000_NMK_SIZE_BYTE; nmkIdx++)
      {
        NmkPtr[nmkIdx] = EthTrcv_30_Ar7000_Nmk[nmkIdx];
      }
      EthTrcv_30_Ar7000_NmkReady = ETHTRCV_30_AR7000_STATE_NOT_READY;

      retVal = ETHTRCV_30_AR7000_RT_OK;
    }

    /* #50 No ReadNmk() pending
     *       Get lock for module operation
     *       Set the module operation related data for reading the whole pib (which contain the nmk)
     *       Send MME request to QCA
     *         Activate the timeout counter
     *         Inform the calling application that QCA confirmation is pending
     */
    else
    {
      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      /* Get ModOp Lock */
      EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpLockCnt++;

      if (1 == EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpLockCnt)
      {
        /* Send Module Operation Frame for Read Request */
        if (EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpState == ETHTRCV_30_AR7000_MODOP_STATE_NOT_USED)
        {
          EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpState = ETHTRCV_30_AR7000_MODOP_STATE_USED;

          ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

          EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpFrameLen = ETHTRCV_30_AR7000_MODOP_RD_HDR_LEN;
          /* PRQA S 0310, 3305 1 */ /* MD_EthTrcv_30_Ar7000_0310_4 */
          EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpBufPtr   = (P2VAR(uint32, ETHTRCV_30_AR7000_VAR_NOINIT, ETHTRCV_30_AR7000_VAR_NOINIT))&EthTrcv_30_Ar7000_Nmk[0];
          EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpTotLen   = ETHTRCV_30_AR7000_NMK_SIZE_BYTE;
          EthTrcv_30_Ar7000_ModOpParams.ModOpModuleId            = ETHTRCV_30_AR7000_MOD_ID_PIB;
          EthTrcv_30_Ar7000_ModOpParams.ModOpModuleSubId         = 0x0000u;
          EthTrcv_30_Ar7000_ModOpParams.ModOpDataLenTx           = ETHTRCV_30_AR7000_NMK_SIZE_BYTE;
          EthTrcv_30_Ar7000_ModOpParams.ModOpCurOffset           = ETHTRCV_30_AR7000_PIB_MODULE_OFFSET + ETHTRCV_30_AR7000_NMK_PIB_OFFSET;
          EthTrcv_30_Ar7000_ModOpParams.ModOpStartOffset         = ETHTRCV_30_AR7000_PIB_MODULE_OFFSET + ETHTRCV_30_AR7000_NMK_PIB_OFFSET;

          /* PRQA S 0310 1 */ /* MD_EthTrcv_30_Ar7000_0310_3 */
          if(E_OK == EthTrcv_30_Ar7000_MME_ModuleOperation(TrcvIdx, ETHTRCV_30_AR7000_MODOP_RD_MEM, (EthTrcv_30_Ar7000_ModOp_Params*)&EthTrcv_30_Ar7000_ModOpParams))
          {
            /* successful */
            EthTrcv_30_Ar7000_IsReadingNmk = ETHTRCV_30_AR7000_WAIT_ON_CONF;
            EthTrcv_30_Ar7000_NmkReady = ETHTRCV_30_AR7000_STATE_NOT_READY;
            retVal = ETHTRCV_30_AR7000_RT_PENDING;
          }
          else
          {
            /* ETHTRCV_30_AR7000_RT_NOT_OK */
          }
        }
        else
        {
          ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
          /* ETHTRCV_30_AR7000_RT_NOT_OK */
        }
      }
      else
      {
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
        /* ETHTRCV_30_AR7000_RT_NOT_OK */
      }
      EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpLockCnt--;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_READ_NMK, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_ReadNmk() */ /* PRQA S 6030, 6080 */ /* MD_MSR_STCYC, MD_MSR_STMIF */
#endif /* ETHTRCV_30_AR7000_ENABLE_READ_NMK */

#if (ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION == STD_ON)
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_GetSwVersion
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_GetSwVersion(
    uint8 TrcvIdx,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) SwVersionPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8_least swVersionIdx;
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  EthTrcv_30_Ar7000_ReturnType retVal = ETHTRCV_30_AR7000_RT_NOT_OK; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  /* SwVersionPtr == NULL_PTR is allowed */
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Verify timeout counter (see MainFunction()) not exceeded */
    if ( ETHTRCV_30_AR7000_CONFTIMEOUT <= EthTrcv_30_Ar7000_GetSwVersionCycCnt )
    {
      EthTrcv_30_Ar7000_SWVersionAvailable = ETHTRCV_30_AR7000_STATE_NOT_READY;
      EthTrcv_30_Ar7000_IsReadingSwVersion = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
      EthTrcv_30_Ar7000_GetSwVersionCycCnt = 0;

      /* ETHTRCV_30_AR7000_RT_NOT_OK */
    }

    /* #30 Inform calling application that QCA confirmation is pending */
    else if ( ETHTRCV_30_AR7000_WAIT_ON_CONF == EthTrcv_30_Ar7000_IsReadingSwVersion )
    {
      retVal = ETHTRCV_30_AR7000_RT_PENDING;
    }

    /* #40 Got QCA confirmation
     *       Verify that confirmation status was 'success'
     *         Return QCA software version
     */
    else if (   (ETHTRCV_30_AR7000_NO_WAIT_ON_CONF == EthTrcv_30_Ar7000_IsReadingSwVersion)
             && (ETHTRCV_30_AR7000_STATE_READY == EthTrcv_30_Ar7000_SWVersionAvailable) )
    {
      if ( ETHTRCV_30_AR7000_MSTATUS_OK == EthTrcv_30_Ar7000_SwMStatus )
      {
        if ( NULL_PTR != SwVersionPtr )
        {
          /* Copy SW Version */
          for (swVersionIdx = 0; swVersionIdx < sizeof(EthTrcv_30_Ar7000_SwVersion); swVersionIdx++)
          {
            SwVersionPtr[swVersionIdx] = EthTrcv_30_Ar7000_SwVersion[swVersionIdx];
          }
        }
        EthTrcv_30_Ar7000_SWVersionAvailable = ETHTRCV_30_AR7000_STATE_NOT_READY;

        retVal = ETHTRCV_30_AR7000_RT_OK;
      }
      else
      {
        /* ETHTRCV_30_AR7000_RT_NOT_OK */
      }
    }

    /* #50 No GetSwVersion() call pending
     *       Send MME request to QCA
     *         Activate the timeout counter
     *         Inform the calling application that QCA confirmation is pending
     */
    else
    {
      if ( E_OK == EthTrcv_30_Ar7000_MME_GetSwVersion(TrcvIdx) )
      {
        /* successful */
        EthTrcv_30_Ar7000_IsReadingSwVersion = ETHTRCV_30_AR7000_WAIT_ON_CONF;
        EthTrcv_30_Ar7000_SWVersionAvailable = ETHTRCV_30_AR7000_STATE_NOT_READY;
        retVal = ETHTRCV_30_AR7000_RT_PENDING;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_GET_SW_VERSION, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_GetSwVersion() */ /* PRQA S 6030, 6080 */ /* MD_MSR_STCYC, MD_MSR_STMIF */
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION */

#if ( ETHTRCV_30_AR7000_ENABLE_RESET_DEVICE == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_ResetDevice
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_ResetDevice(
    uint8 TrcvIdx)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal; /* Return value of this function */
  Std_ReturnType resVal; /* Result value of called functions */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = E_NOT_OK;
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else
#endif
  {
#if ( (STD_ON==ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP) )
    EthTrcv_30_Ar7000_WillResetAfterPibUpload = FALSE;
#endif

    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Reset the QCA7000 */
    resVal = EthTrcv_30_Ar7000_MME_ResetDevice(TrcvIdx);

    /* Set return value */
    retVal = resVal;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_RESET_DEVICE, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_ResetDevice() */
#endif /* ETHTRCV_30_AR7000_ENABLE_RESET_DEVICE */

#if ( ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_JoinScState
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_JoinScState(
        uint8                                                          TrcvIdx,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) MStatus)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8 AVLNStatus;
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  EthTrcv_30_Ar7000_ReturnType retVal; /* Return value of this function */
  EthTrcv_30_Ar7000_ReturnType resVal; /* Result value of called functions */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = ETHTRCV_30_AR7000_RT_NOT_OK;
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (MStatus == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Join a SimpleConnect Logical Network is done by a 'pushbutton simple connect' MME request */
    resVal = EthTrcv_30_Ar7000_PBEncryption(TrcvIdx, ETHTRCV_30_AR7000_PBACTION_JOIN, MStatus, &AVLNStatus);

    /* Set return value */
    retVal = resVal;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_JOIN_SC_STATE, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif
  return retVal;

} /* EthTrcv_30_Ar7000_JoinScState() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_LeaveScState
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_LeaveScState(
        uint8                                                          TrcvIdx,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) MStatus)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8 AVLNStatus;
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  EthTrcv_30_Ar7000_ReturnType retVal; /* Return value of this function */
  EthTrcv_30_Ar7000_ReturnType resVal; /* Result value of called functions */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = ETHTRCV_30_AR7000_RT_NOT_OK;
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (MStatus == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Leave a SimpleConnect Logical Network is done by a 'pushbutton simple connect' MME request */
    resVal = EthTrcv_30_Ar7000_PBEncryption(TrcvIdx, ETHTRCV_30_AR7000_PBACTION_LEAVE, MStatus, &AVLNStatus);

    /* Set return value */
    retVal = resVal;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_LEAVE_SC_STATE, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif
  return retVal;
} /* EthTrcv_30_Ar7000_LeaveScState() */

#if ( ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_ResetAndLeave
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_ResetAndLeave(
  uint8   TrcvIdx,
  boolean RandomizeNmkInAnyCase)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal; /* Return value of this function */
  Std_ReturnType resVal; /* Result value of called functions */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = E_NOT_OK;
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
# if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
  else if ((ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) == 0)
  {
    errorId = ETHTRCV_30_AR7000_SLAC_E_NOT_INITIALIZED;
  }
# endif
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Leave network by setting random network membership key value */
    if (   (TRUE == RandomizeNmkInAnyCase) /* Always */
        || (ETHTRCV_LINK_STATE_ACTIVE == EthTrcv_30_Ar7000_LinkState) /* If link is present */
#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
        || ( (ETHTRCV_30_AR7000_SLAC_FLAG_LINK_VALID & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 ) /* If SLAC was running */
#endif
       )
    {
      uint8 dummyMStatus;
      EthTrcv_30_Ar7000_SetKeyRetVal = EthTrcv_30_Ar7000_SetKeyStatus(TrcvIdx, &dummyMStatus);
      if (ETHTRCV_30_AR7000_RT_PENDING == EthTrcv_30_Ar7000_SetKeyRetVal)
      {
        resVal = E_OK;
      }
      else
      {
        resVal = (Std_ReturnType)EthTrcv_30_Ar7000_SetKeyRetVal;
      }
    }
    else
    {
      /* No link was established and SLAC was not running, nothing to do */
      resVal = E_OK;
    }

#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
    /* #30 Verify SLAC state */
    if (   (resVal == E_OK)
        && (ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED != EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) )
    {
      /*** Reset SLAC ***/
      /* clear SLAC link valid flag, do not call LeaveScState API */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags &= (uint16)(~ETHTRCV_30_AR7000_SLAC_FLAG_LINK_VALID);
      /* Call SLAC reset API */
      resVal = EthTrcv_30_Ar7000_Slac_Reset(TrcvIdx);
    }
    else
    {
      /* SLAC was not runing, just call D-Link signal callback */
      (void)ETHTRCV_30_AR7000_APPL_CBK_SLAC_DLINK_READY(ETHTRCV_LINK_STATE_DOWN,
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid);
      resVal = E_OK;
    }
#endif
  /* ETHTRCV_30_AR7000_ENABLE_SLAC */

    /* Set return value */
    retVal = resVal;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_RESET_AND_LEAVE, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_ResetAndLeave() */ /* PRQA S 6080 */ /* MD_MSR_STMIF */
#endif /* ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API */

#if (   (   (ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON) \
         && (ETHTRCV_30_AR7000_CALC_NMK_LOCALLY == STD_OFF) ) \
     || (ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API == STD_ON) )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetKeyStatus
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetKeyStatus(
    uint8 TrcvIdx,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) MStatus)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  EthTrcv_30_Ar7000_ReturnType retVal = ETHTRCV_30_AR7000_RT_NOT_OK; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (MStatus == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Verify timeout counter (see MainFunction()) not exceeded */
    if ( ETHTRCV_30_AR7000_CONFTIMEOUT <= EthTrcv_30_Ar7000_SetKeyConfCycCnt )
    {
      EthTrcv_30_Ar7000_SetKeyConfReady = ETHTRCV_30_AR7000_STATE_NOT_READY;
      EthTrcv_30_Ar7000_SetKeyIsChanging = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
      EthTrcv_30_Ar7000_SetKeyConfCycCnt = 0;

      /* ETHTRCV_30_AR7000_RT_NOT_OK */
    }

    /* #30 Inform calling application that QCA confirmation is pending */
    else if ( ETHTRCV_30_AR7000_WAIT_ON_CONF == EthTrcv_30_Ar7000_SetKeyIsChanging )
    {
      retVal = ETHTRCV_30_AR7000_RT_PENDING;
    }

    /* #40 Got QCA confirmation
     *     Verify that confirmation status was 'success'
     *       Return QCA confirmation status to calling application.
     */
    else if ( (ETHTRCV_30_AR7000_NO_WAIT_ON_CONF == EthTrcv_30_Ar7000_SetKeyIsChanging) &&
         (ETHTRCV_30_AR7000_STATE_READY == EthTrcv_30_Ar7000_SetKeyConfReady)
       )
    {
      EthTrcv_30_Ar7000_SetKeyConfReady = ETHTRCV_30_AR7000_STATE_NOT_READY;
      *MStatus = EthTrcv_30_Ar7000_SetKeyMStatus;
      retVal = ETHTRCV_30_AR7000_RT_OK;
    }

    /* #50 No SetKeyStatus() pending
     *       Generate random network membership key
     *       Send set key MME request
     *       Activate the timeout counter
     *       Inform calling application that QCA confirmation is pending
     */
    else
    {
      /* Get random NMK */
#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
      uint8 RndNmk[16];
      (void)ETHTRCV_30_AR7000_APPL_CBK_SLAC_GET_RND(&RndNmk[0], 16);
#else
      uint8 RndNmk[16] = {123, 98, 65, 82, 201, 167, 1, 46, 73, 10, 35, 12, 246, 7, 25, 16}; /* Set fixed 'random' value in case random function is not available */
#endif
      /* NMK will be randomized, set link information to DOWN and activate LinkStateLockCnt, to prevent link state toggling.
       * Else QCA7005 may indicate a link during NMK change process even if there is no link available.
       */
#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
      EthTrcv_30_Ar7000_LinkStateLockCnt = ETHTRCV_30_AR7000_LINKSTATE_LOCK_CYCLES;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */
      EthTrcv_30_Ar7000_LinkState        = ETHTRCV_LINK_STATE_DOWN;

      if ( E_OK == EthTrcv_30_Ar7000_MME_SetKey(TrcvIdx, &RndNmk[0]) )
      {
        EthTrcv_30_Ar7000_SetKeyIsChanging = ETHTRCV_30_AR7000_WAIT_ON_CONF;
        EthTrcv_30_Ar7000_SetKeyConfReady = ETHTRCV_30_AR7000_STATE_NOT_READY;
        retVal = ETHTRCV_30_AR7000_RT_PENDING;
      }
      else
      {
        /* ETHTRCV_30_AR7000_RT_NOT_OK */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_SET_KEY_STATUS, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_SetKeyStatus() */ /* PRQA S 6080 */ /* MD_MSR_STMIF */
#endif
  /* (   (   (ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON) \
         && (ETHTRCV_30_AR7000_CALC_NMK_LOCALLY == STD_OFF) ) \
     || (ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API == STD_ON) ) */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_ReadAVLNStatus
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_ReadAVLNStatus(
        uint8                                                          TrcvIdx,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) MStatus,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) AVLNStatus)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  EthTrcv_30_Ar7000_ReturnType retVal; /* Return value of this function */
  EthTrcv_30_Ar7000_ReturnType resVal; /* Result value of called functions */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = ETHTRCV_30_AR7000_RT_NOT_OK;
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (MStatus == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else if (AVLNStatus == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Request HomePlug AV Logical Network (AVLN) status is done by a 'pushbutton simple connect' MME request */
    resVal = EthTrcv_30_Ar7000_PBEncryption(TrcvIdx, ETHTRCV_30_AR7000_PBACTION_STATUS, MStatus, AVLNStatus);

    /* Set return value */
    retVal = resVal;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_READ_AVLN_STATUS, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_ReadAVLNStatus() */
#endif /* ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API */

#if ( ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_WritePhysAddr
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_WritePhysAddr(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) PhysAddr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  P2VAR(EthTrcv_30_Ar7000_NVMHeaderType, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) moduleHdrOffset;
  P2VAR(uint8, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) pibModuleOffset;
  P2VAR(uint8, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) localPhysAddrPtr;
  uint32 pibChecksum;
  uint8 status;
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal; /* Return value of this function */
  Std_ReturnType resVal; /* Result value of called functions */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = E_NOT_OK;
  if (EthTrcv_30_Ar7000_State == ETHTRCV_STATE_UNINIT)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (PhysAddr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

    /* #20 Get MAC address NvM block */
    status = NVM_REQ_PENDING;
    resVal = NvM_GetErrorStatus(EthTrcv_30_Ar7000_VCfgGetPhysAddrBlockId(TrcvIdx), &status);

    if ((E_OK == resVal) && (status != NVM_REQ_PENDING))
    {
      localPhysAddrPtr = &(EthTrcv_30_Ar7000_VCfgGetPhysAddr()[0]);
      /* #30 Update the host memory NvM block */
      localPhysAddrPtr[0] = PhysAddr[0];
      localPhysAddrPtr[1] = PhysAddr[1];
      localPhysAddrPtr[2] = PhysAddr[2];
      localPhysAddrPtr[3] = PhysAddr[3];
      localPhysAddrPtr[4] = PhysAddr[4];
      localPhysAddrPtr[5] = PhysAddr[5];
      {
        /* #40 Get PIB RAM copy */
        resVal = EthTrcv_30_Ar7000_VGetPibModuleOffset(EthTrcv_30_Ar7000_PibRamCpy, &pibModuleOffset, &moduleHdrOffset);
      }

      if ( E_OK == resVal )
      {

        /* #50 Update PIB block */
        /* Set new MAC for the QCA7000 */
        pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 0u] = PhysAddr[0];
        pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 1u] = PhysAddr[1];
        pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 2u] = PhysAddr[2];
        pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 3u] = PhysAddr[3];
        pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 4u] = PhysAddr[4];
        pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 5u] = PhysAddr[5];

        /* Checksum module */
        moduleHdrOffset->ImageChecksum = 0x00000000;  /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
        pibChecksum = EthTrcv_30_Ar7000_VCalcChecksum(TrcvIdx,
          (P2VAR(uint32, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))pibModuleOffset, ETHTRCV_30_AR7000_LIENTOH32(moduleHdrOffset->ImageLength)); /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        moduleHdrOffset->ImageChecksum = pibChecksum;    /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */

         /* Checksum header */
        moduleHdrOffset->HeaderChecksum = 0x00000000;  /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_5 */
        pibChecksum = EthTrcv_30_Ar7000_VCalcChecksum(TrcvIdx,
          (P2VAR(uint32, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))moduleHdrOffset, sizeof(*moduleHdrOffset)); /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        moduleHdrOffset->HeaderChecksum = pibChecksum;    /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */

        /* #60 Indicate PIB block and NvM RAM block as valid */
        EthTrcv_30_Ar7000_PibRamCpyValid = TRUE;

        /* set block status to true */
        resVal = NvM_SetRamBlockStatus(EthTrcv_30_Ar7000_VCfgGetPhysAddrBlockId(TrcvIdx), TRUE);

        /* Restart firmware if already running */
        if (EthTrcv_30_Ar7000_DownlStatus == ETHTRCV_30_AR7000_DOWNLSTATUS_DONE)
        {
          resVal |= EthTrcv_30_Ar7000_ResetDevice(TrcvIdx);
        }
      }
    }
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

    /* Set return value */
    retVal = resVal;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_WRITE_PHYS_ADDR, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_WritePhysAddr() */ /* PRQA S 6050, 6080 */ /* MD_MSR_STCAL, MD_MSR_STMIF */
#endif /* ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS */

#if ( ETHTRCV_30_AR7000_ENABLE_WRITE_DAK == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_WriteDak
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_WriteDak(
               uint8                                                             TrcvIdx,
  CONSTP2CONST(uint8, ETHTRCV_30_AR7000_APPL_CONST, ETHTRCV_30_AR7000_APPL_DATA) Dak)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  P2VAR(EthTrcv_30_Ar7000_NVMHeaderType, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) moduleHdrOffset;
  P2VAR(uint8, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) pibModuleOffset;
  uint32 pibChecksum;
  uint8 nvMStatus;
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal; /* Return value of this function */
  Std_ReturnType resVal; /* Result value of called functions */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = E_NOT_OK;
  if (EthTrcv_30_Ar7000_State == ETHTRCV_STATE_UNINIT)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (Dak == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

    /* #20 Get DAK NvM RAM block */
    nvMStatus = NVM_REQ_PENDING;
    resVal = NvM_GetErrorStatus(EthTrcv_30_Ar7000_VCfgGetDakBlockId(TrcvIdx), &nvMStatus);

    if((E_OK == resVal) && (nvMStatus != NVM_REQ_PENDING))
    {
      /* #30 Copy application given Device Access Key into NvM RAM block */
      /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310_1 */
      IpBase_Copy(
        (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_VCfgGetDak(),
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))Dak,
        ETHTRCV_30_AR7000_DAK_SIZE_BYTE
      );

      resVal = EthTrcv_30_Ar7000_VGetPibModuleOffset(EthTrcv_30_Ar7000_PibRamCpy, &pibModuleOffset, &moduleHdrOffset);

      if ( E_OK == resVal )
      {

        /* #40 Copy application given Device Access Key into local PIB file */
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310_1 */
        IpBase_Copy(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&pibModuleOffset[ETHTRCV_30_AR7000_DAK_PIB_OFFSET],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))Dak,
          ETHTRCV_30_AR7000_DAK_SIZE_BYTE
        );

        /* Checksum module */
        moduleHdrOffset->ImageChecksum = 0x00000000;  /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
        pibChecksum = EthTrcv_30_Ar7000_VCalcChecksum(TrcvIdx,
          (P2VAR(uint32, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))pibModuleOffset, ETHTRCV_30_AR7000_LIENTOH32(moduleHdrOffset->ImageLength)); /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        moduleHdrOffset->ImageChecksum = pibChecksum;    /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */

          /* Checksum header */
        moduleHdrOffset->HeaderChecksum = 0x00000000;  /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_5 */
        pibChecksum = EthTrcv_30_Ar7000_VCalcChecksum(TrcvIdx,
          (P2VAR(uint32, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))moduleHdrOffset, sizeof(*moduleHdrOffset)); /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
        moduleHdrOffset->HeaderChecksum = pibChecksum;    /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */

        /* #50 Indicate local PIB copy and NvM RAM block as valid */
        EthTrcv_30_Ar7000_PibRamCpyValid = TRUE;

        /* Restart firmware if already running */
        if (EthTrcv_30_Ar7000_DownlStatus == ETHTRCV_30_AR7000_DOWNLSTATUS_DONE)
        {
          resVal = EthTrcv_30_Ar7000_ResetDevice(TrcvIdx);
        }

        /* set block status to true */
        resVal |= NvM_SetRamBlockStatus(EthTrcv_30_Ar7000_VCfgGetDakBlockId(TrcvIdx), TRUE);
      }
    }
    else
    {
      resVal = E_NOT_OK;
    }
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

    /* Set return value */
    retVal = resVal;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_WRITE_DAK, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_WriteDak() */ /* PRQA S 6050, 6080 */ /* MD_MSR_STCAL, MD_MSR_STMIF */
#endif /* ETHTRCV_30_AR7000_ENABLE_WRITE_DAK */

#if (ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API == STD_ON)
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetProperty
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetProperty(
  uint8 TrcvIdx,
  CONSTP2CONST(EthTrcv_30_Ar7000_SetPropertyParamterType, ETHTRCV_30_AR7000_APPL_CONST, ETHTRCV_30_AR7000_APPL_CONST)
    SetPropParams,
  P2VAR(EthTrcv_30_Ar7000_SetPropType, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) MStatus)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  EthTrcv_30_Ar7000_ReturnType retVal = ETHTRCV_30_AR7000_RT_NOT_OK; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthTrcv_30_Ar7000_State == ETHTRCV_STATE_UNINIT)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (SetPropParams == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Verify timeout counter (see MainFunction()) not exceeded */
    if ( ETHTRCV_30_AR7000_CONFTIMEOUT <= EthTrcv_30_Ar7000_SetPropertyCycCnt )
    {
      EthTrcv_30_Ar7000_SetPropertyCnfAvailable = ETHTRCV_30_AR7000_STATE_NOT_READY;
      EthTrcv_30_Ar7000_IsSettingProperty       = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
      EthTrcv_30_Ar7000_SetPropertyCycCnt       = 0;

      /* ETHTRCV_30_AR7000_RT_NOT_OK */
    }

    /* #30 Inform calling application that QCA confirmation is pending */
    else if ( ETHTRCV_30_AR7000_WAIT_ON_CONF == EthTrcv_30_Ar7000_IsSettingProperty )
    {
      retVal = ETHTRCV_30_AR7000_RT_PENDING;
    }

    /* #40 Got QCA confirmation
     *     Verify that confirmation status was 'success'
     *       Return QCA confirmation status to calling application.
     */
    else if (   (ETHTRCV_30_AR7000_NO_WAIT_ON_CONF == EthTrcv_30_Ar7000_IsSettingProperty)
             && (ETHTRCV_30_AR7000_STATE_READY == EthTrcv_30_Ar7000_SetPropertyCnfAvailable) )
    {
      EthTrcv_30_Ar7000_SetPropertyCnfAvailable = ETHTRCV_30_AR7000_STATE_NOT_READY;
      /* Set out value MStatus */
      *MStatus = (EthTrcv_30_Ar7000_SetPropType)EthTrcv_30_Ar7000_SetPropertyStatus;
      /* Set return value according to MStatus */
      if ( ETHTRCV_30_AR7000_SETPROP_SUCCESS == EthTrcv_30_Ar7000_SetPropertyStatus )
      {
        retVal = ETHTRCV_30_AR7000_RT_OK;
      }
      else
      {
        /* ETHTRCV_30_AR7000_RT_NOT_OK */
      }
    }

    /* #50 No SetProperty() pending
     *       Send set property MME request
     *       Activate the timeout counter
     *       Inform calling application that QCA confirmation is pending
     */
    else
    {
      if ( E_OK == EthTrcv_30_Ar7000_MME_SetProperty(TrcvIdx, SetPropParams) )
      {
        /* successful */
        EthTrcv_30_Ar7000_IsSettingProperty = ETHTRCV_30_AR7000_WAIT_ON_CONF;
        EthTrcv_30_Ar7000_SetPropertyCnfAvailable = ETHTRCV_30_AR7000_STATE_NOT_READY;
        retVal = ETHTRCV_30_AR7000_RT_PENDING;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_SET_PROPERTY, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_SetProperty() */ /* PRQA S 6080 */ /* MD_MSR_STMIF */
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API */

#if (ETHTRCV_30_AR7000_ENABLE_SET_PIB_DATA_API == STD_ON)
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_SetPibData
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_SetPibData(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) Pib)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
#if (   ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)) \
     || ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)          && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)) \
     || ((defined ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)      && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)) )
  P2VAR(EthTrcv_30_Ar7000_NVMHeaderType, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) moduleHdrOffset;
  P2VAR(uint8, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) pibModuleOffset;
  uint32 pibChecksum;
  Std_ReturnType resVal; /* Result value of called functions */
#endif /* (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)  ||
          (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)           ||
          (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK) */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthTrcv_30_Ar7000_State == ETHTRCV_STATE_UNINIT)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  /* Pib == NULL_PTR is allowed */
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Determine the PIB update base data*/
    if ( NULL_PTR == Pib )
    {
      /* Restore default */
      EthTrcv_30_Ar7000_PIBData = PIBData;
    }
    else
    {
      EthTrcv_30_Ar7000_PIBData = Pib;
    }

#if (   ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)) \
     || ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)          && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)) \
     || ((defined ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)      && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)) )
    ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    EthTrcv_30_Ar7000_PibRamCpyValid = FALSE;

    /* #30 Copy PIB to RAM */
    /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
    IpBase_Copy(
      (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_PibRamCpy,
      (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_PIBData,
      ETHTRCV_30_AR7000_PIB_SIZE
    );

    resVal = EthTrcv_30_Ar7000_VGetPibModuleOffset(EthTrcv_30_Ar7000_PibRamCpy, &pibModuleOffset, &moduleHdrOffset);

    /* #40 Update the PIB RAM copy */
    if ( E_OK == resVal )
    {
# if (   ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS)) \
      || ((defined ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)      && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK)) )
      /* Set MAC address */
      pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 0u] = EthTrcv_30_Ar7000_VCfgGetPhysAddr()[0];
      pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 1u] = EthTrcv_30_Ar7000_VCfgGetPhysAddr()[1];
      pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 2u] = EthTrcv_30_Ar7000_VCfgGetPhysAddr()[2];
      pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 3u] = EthTrcv_30_Ar7000_VCfgGetPhysAddr()[3];
      pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 4u] = EthTrcv_30_Ar7000_VCfgGetPhysAddr()[4];
      pibModuleOffset[ETHTRCV_30_AR7000_MAC_PIB_OFFSET + 5u] = EthTrcv_30_Ar7000_VCfgGetPhysAddr()[5];
# endif /* ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS */

# if (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK)
      /* Write DAK */
      /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310_1 */
      IpBase_Copy(
        (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&pibModuleOffset[ETHTRCV_30_AR7000_DAK_PIB_OFFSET],
        (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_VCfgGetDak(),
        ETHTRCV_30_AR7000_DAK_SIZE_BYTE
      );
# endif /* ETHTRCV_30_AR7000_ENABLE_WRITE_DAK */

      /* Checksum module */
      moduleHdrOffset->ImageChecksum = 0x00000000;  /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */
      /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
      pibChecksum = EthTrcv_30_Ar7000_VCalcChecksum(TrcvIdx,
        (P2VAR(uint32, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))pibModuleOffset, ETHTRCV_30_AR7000_LIENTOH32(moduleHdrOffset->ImageLength));
      moduleHdrOffset->ImageChecksum = pibChecksum;    /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */

        /* Checksum header */
      moduleHdrOffset->HeaderChecksum = 0x00000000;  /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */

      /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310_5 */
      pibChecksum = EthTrcv_30_Ar7000_VCalcChecksum(TrcvIdx,
        (P2VAR(uint32, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))moduleHdrOffset, sizeof(*moduleHdrOffset));
      moduleHdrOffset->HeaderChecksum = pibChecksum;    /* PRQA S 3305 */ /* MD_EthTrcv_30_Ar7000_3305 */

      /* #50 Set PIB RAM copy to valid state for future use*/
      EthTrcv_30_Ar7000_PibRamCpyValid = TRUE;

      /* Set return value */
      retVal = E_OK;
    }
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
#else
    EthTrcv_30_Ar7000_PibRamCpyValid = FALSE;
    retVal = E_OK;
#endif /* (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) ||
          (STD_ON == ETHTRCV_30_AR7000_ENABLE_WRITE_DAK) */
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_SET_PIB_DATA, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_SetPibData() */
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_PIB_DATA_API */

#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD ) && ( STD_OFF == ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD)
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_TriggerDwld
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_TriggerDwld(
    uint8 TrcvIdx)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  Std_ReturnType resValWaEA;
  EthTrcv_30_Ar7000_ReturnType retVal; /* Return value of this function */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  /* #10 Get lock for module operation */
  EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpLockCnt++;

  /* #20 Verify valid lock request */
  if (1 == EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpLockCnt)
  {
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

    /* #30 Verify that a firmware has been transfered previously or MME_ModuleOperation() was called previously */
    if (   (ETHTRCV_30_AR7000_MODOP_STATE_USED == EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpState)
        || (ETHTRCV_30_AR7000_DOWNLSTATUS_FW >= EthTrcv_30_Ar7000_DownlStatus))
    {
      /* #40 Verify that TriggerDwld() wasn't called previously */
      if (TRUE == EthTrcv_30_Ar7000_MME_WaEA_Params.NeedToSend)
      {
        /* #50 Trigger firmware download process. */
        resValWaEA = EthTrcv_30_Ar7000_MME_WriteAndExecuteApplet(
          TrcvIdx,
          &EthTrcv_30_Ar7000_MME_WaEA_Params);

        if ( E_NOT_OK == resValWaEA )
        {
          retVal = ETHTRCV_30_AR7000_RT_NOT_OK;
        }
        else
        {
          retVal = ETHTRCV_30_AR7000_RT_PENDING;
        }
        EthTrcv_30_Ar7000_MME_WaEA_Params.NeedToSend = FALSE;
      }
      else
      {
        retVal = ETHTRCV_30_AR7000_RT_PENDING;
      }
    }
    else
    {
      retVal = ETHTRCV_30_AR7000_RT_OK;
    }
  }
  else
  {
    retVal = ETHTRCV_30_AR7000_RT_PENDING;
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  }
  EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpLockCnt--;

  return retVal;
} /* EthTrcv_30_Ar7000_TriggerDwld() */
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD, ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD */

#if ( ETHTRCV_30_AR7000_VERSION_INFO_API == STD_ON )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_GetVersionInfo
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_GetVersionInfo(
  P2VAR(Std_VersionInfoType, AUTOMATIC, AUTOMATIC) VersionInfoPtr)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (VersionInfoPtr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    VersionInfoPtr->vendorID = ETHTRCV_30_AR7000_VENDOR_ID;
    VersionInfoPtr->moduleID = ETHTRCV_30_AR7000_MODULE_ID;
    VersionInfoPtr->sw_major_version = ETHTRCV_30_AR7000_SW_MAJOR_VERSION;
    VersionInfoPtr->sw_minor_version = ETHTRCV_30_AR7000_SW_MINOR_VERSION;
    VersionInfoPtr->sw_patch_version = ETHTRCV_30_AR7000_SW_PATCH_VERSION;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_GET_VERSION_INFO, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif
} /* EthTrcv_30_Ar7000_GetVersionInfo() */
#endif /* ETHTRCV_30_AR7000_VERSION_INFO_API == STD_ON */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MainFunction
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MainFunction(void)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
  P2FUNC(void, AUTOMATIC, tmpRetryCallFct)(uint8, P2CONST(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST));
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Invoke SLAC State-Machine handling */
#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC )
  EthTrcv_30_Ar7000_Slac_MainFunction();
#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */

  /* #20 Verify that transceiver is enabled */
  if ( ETHTRCV_MODE_ACTIVE == EthTrcv_30_Ar7000_ModeState )
  {
#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
    /* #30 Handle Firmware download timeout errors if ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT or
     *      ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_SLAC_COM_EVENT is enabled
     */
    /* Add critical section due to timer stop in interrupt context */
    ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
# if (ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT == STD_ON)
    if (0 < EthTrcv_30_Ar7000_FwDownloadAndStartCnt)
    {
      /* Firmware download is active */
      if (1 == EthTrcv_30_Ar7000_FwDownloadAndStartCnt)
      {
        /* Stop timer and continue */
        EthTrcv_30_Ar7000_FwDownloadAndStartCnt = 0;
        /* Stop critical section befor calling other modules */
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

        /* Firmware download timed out */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
       (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_MAINFUNCTION, ETHTRCV_30_AR7000_E_FW_DOWNLOAD_TIMEOUT);
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_REPORT */

        /* Inform user about the timeout */
#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_START )
        ETHTRCV_30_AR7000_APPL_CBK_FW_START(FALSE);
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_START  */
      }
      else
      {
        EthTrcv_30_Ar7000_FwDownloadAndStartCnt--;
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      }
    }
    else
#  if (ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_SLAC_COM_EVENT == STD_ON)
    if (0 < EthTrcv_30_Ar7000_FwReadyForPlcComCnt)
    {
      /* Firmware download is active */
      if (1 == EthTrcv_30_Ar7000_FwReadyForPlcComCnt)
      {
        /* Stop timer and continue */
        EthTrcv_30_Ar7000_FwReadyForPlcComCnt = 0;
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
        /* Stop critical section befor calling other modules */

        /* Firmware download timed out */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
        (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_MAINFUNCTION, ETHTRCV_30_AR7000_E_FW_READY_FOR_PLC_COM_TIMEOUT);
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_REPORT */

        /* Inform user about the timeout */
#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC )
        EthTrcv_30_Ar7000_FwReadyForSlacLastResult = FALSE;
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC */

#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
        ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC(EthTrcv_30_Ar7000_FwReadyForSlacLastResult);
#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */
      }
      else
      {
        EthTrcv_30_Ar7000_FwReadyForPlcComCnt--;
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      }
    }
    else
#  endif /* ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_SLAC_COM_EVENT */
# endif /* ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT */
    /* #40 Wait until firmware download is finished */
    if ( 0 < EthTrcv_30_Ar7000_LinkStateLockCnt )
    {
      EthTrcv_30_Ar7000_LinkStateLockCnt--;
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    }
    else
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */
    {
#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

      /* #50 Cyclically request Link-State */
      EthTrcv_30_Ar7000_CycleCnt--;
      if(0 == EthTrcv_30_Ar7000_CycleCnt)
      {
        EthTrcv_30_Ar7000_CycleCnt = ETHTRCV_30_AR7000_LINKSTATEPOLLINGFREQ;
        (void)EthTrcv_30_Ar7000_MME_GetLinkStatus(0);
      }

      /* #60 Reset the QCA hardware if it does not answer the MME_GetLinkStatus() requested with a valid response */
      /*      Increase heart beat counter, reseted by a link response of the QCA7000 */
      EthTrcv_30_Ar7000_HeartBeatCnt++;
      if ( EthTrcv_30_Ar7000_HeartBeatCnt > ETHTRCV_30_AR7000_HEARTBEAT_TIMEOUT )
      {
        EthTrcv_30_Ar7000_HeartBeatCnt = 0;
        EthTrcv_30_Ar7000_LinkState = ETHTRCV_LINK_STATE_DOWN;
#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
        EthTrcv_30_Ar7000_WriteAppletLockCnt = 0;
        EthTrcv_30_Ar7000_AppletNrLocked = ETHTRCV_30_AR7000_NO_APPLET_LOCKED;
# if ( ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT == STD_ON )
        EthTrcv_30_Ar7000_FwDownloadAndStartCnt = ETHTRCV_30_AR7000_FIRMWARE_DOWNLOAD_AND_START_CYCLES;
#endif /* ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT */
# if ( ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_SLAC_COM_EVENT == STD_ON )
        EthTrcv_30_Ar7000_FwReadyForPlcComCnt = ETHTRCV_30_AR7000_FIRMWARE_READY_FOR_SLAC_COM_CYCLES;
#endif /* ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_SLAC_COM_EVENT */
        EthTrcv_30_Ar7000_LinkStateLockCnt = ETHTRCV_30_AR7000_LINKSTATE_LOCK_CYCLES;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
#if ( ETHTRCV_30_AR7000_ENABLE_DET_ON_FW_OR_COM_ERROR == STD_ON )
        (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_MAINFUNCTION, ETHTRCV_30_AR7000_E_TRCV_CONNECTION_LOST_OR_OVERLOADED);
#endif /* ETHTRCV_30_AR7000_ENABLE_DET_ON_FW_OR_COM_ERROR */
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_REPORT */
        EthTrcv_30_Ar7000_DwldAttempts--;
        if (0 == EthTrcv_30_Ar7000_DwldAttempts)
        {
          EthTrcv_30_Ar7000_DwldAttempts = ETHTRCV_30_AR7000_FW_DWLD_ATTEMPTS;

#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_START )
          /* Application callout */
          ETHTRCV_30_AR7000_APPL_CBK_FW_START(FALSE);
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_START */
#if ( ETHTRCV_30_AR7000_DEM_ERROR_DETECT == STD_ON )
          ETHTRCV_30_AR7000_DEM_REPORT_ERROR_STATUS(ETHTRCV_30_AR7000_E_ACCESS);
#endif /* ETHTRCV_30_AR7000_DEM_ERROR_DETECT */
        }
        else
        {
          /* reset */
          if ( E_NOT_OK == ETH_WRITESPI(EthTrcv_30_Ar7000_VCfgGetEthCtrlIdx(), ETHTRCV_30_AR7000_ETH_SPI_REG_CONFIG, ETHTRCV_30_AR7000_SPI_SOC_CORE_RESET))
          {
            /* DEM Error */
#if ( ETHTRCV_30_AR7000_DEM_ERROR_DETECT == STD_ON )
            ETHTRCV_30_AR7000_DEM_REPORT_ERROR_STATUS(ETHTRCV_30_AR7000_E_ACCESS);
#endif /* ETHTRCV_30_AR7000_DEM_ERROR_DETECT */
          }
        }
      }
    }
    /* #70 Handle timeout counters for different services */
#if ( ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API == STD_ON )
    if ( ETHTRCV_30_AR7000_WAIT_ON_CONF == EthTrcv_30_Ar7000_IsChangingSCState )
    {
      EthTrcv_30_Ar7000_SCConfCycCnt++;
    }
#endif /* ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API */

#if ( ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON )
    if ( ETHTRCV_30_AR7000_WAIT_ON_CONF == EthTrcv_30_Ar7000_SetKeyIsChanging )
    {
      EthTrcv_30_Ar7000_SetKeyConfCycCnt++;
    }
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_NMK */

#if ( ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION == STD_ON )
    if ( ETHTRCV_30_AR7000_WAIT_ON_CONF == EthTrcv_30_Ar7000_IsReadingSwVersion )
    {
      EthTrcv_30_Ar7000_GetSwVersionCycCnt++;
    }
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION */

#if ( ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API == STD_ON )
    if ( ETHTRCV_30_AR7000_WAIT_ON_CONF == EthTrcv_30_Ar7000_IsSettingProperty )
    {
      EthTrcv_30_Ar7000_SetPropertyCycCnt++;
    }
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API */

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
    if ( 0 < EthTrcv_30_Ar7000_WriteAppletLockCnt )
    {
      EthTrcv_30_Ar7000_WriteAppletLockCnt--;
      if ( 0 == EthTrcv_30_Ar7000_WriteAppletLockCnt )
      {
        /* Error applet timed out, just release the applet. Firmware download retry is done by timer EthTrcv_30_Ar7000_HeartBeatCnt */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
        (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_MAINFUNCTION, ETHTRCV_30_AR7000_E_APPLET_TIMEOUT);
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_REPORT */
        EthTrcv_30_Ar7000_AppletNrLocked = ETHTRCV_30_AR7000_NO_APPLET_LOCKED;
      }
    }
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ) || \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_MODOP_API ) || \
    ( STD_ON == ETHTRCV_30_AR7000_ENABLE_READ_NMK )
    if ( ETHTRCV_30_AR7000_MODOP_STATE_USED == EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpState )
    {
      if (EthTrcv_30_Ar7000_ModOpCycCnt > 0)
      {
        EthTrcv_30_Ar7000_ModOpCycCnt++;
        if ( (ETHTRCV_30_AR7000_CONFTIMEOUT + 1u) < EthTrcv_30_Ar7000_ModOpCycCnt)
        {
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
          (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_MAINFUNCTION, ETHTRCV_30_AR7000_E_MODOP_TIMEOUT);
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_REPORT */
          /* Timeout - set to not used */
          EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpState = ETHTRCV_30_AR7000_MODOP_STATE_NOT_USED;
        }
      }

#if ( ETHTRCV_30_AR7000_ENABLE_READ_NMK == STD_ON )
      if ( ETHTRCV_30_AR7000_WAIT_ON_CONF == EthTrcv_30_Ar7000_IsReadingNmk )
      {
        EthTrcv_30_Ar7000_ReadNmkCycCnt++;
        /* Reset locked state regardless of user polling EthTrcv_30_Ar7000_ReadNmk() */
        if ( ETHTRCV_30_AR7000_CONFTIMEOUT < EthTrcv_30_Ar7000_ModOpCycCnt)
        {
          EthTrcv_30_Ar7000_ModOpParams.BaseParams.ModOpState = ETHTRCV_30_AR7000_MODOP_STATE_NOT_USED;
        }
      }
#endif /* ETHTRCV_30_AR7000_ENABLE_READ_NMK */
    }
#endif /* ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP ||
          ETHTRCV_30_AR7000_ENABLE_MODOP_API  ||
          ETHTRCV_30_AR7000_ENABLE_READ_NMK */

    /* #80 Handle WriteAndExecutionApplet() call failures during firmware download. Try to execute
     *      WriteAndExecutionApplet() command again till it was successful or the limit
     *      ETHTRCV_30_AR7000_MAX_API_CALL_RET_CNT is reached
     */
#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON ) || \
    ( ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP        == STD_ON )
    if ( (EthTrcv_30_Ar7000_FailedApiCallType)0 != EthTrcv_30_Ar7000_FailedApiCall )
    {
      tmpRetryCallFct = (EthTrcv_30_Ar7000_FailedApiCallType)EthTrcv_30_Ar7000_FailedApiCall;
      EthTrcv_30_Ar7000_FailedApiCall = (EthTrcv_30_Ar7000_FailedApiCallType)0;
      tmpRetryCallFct(0, EthTrcv_30_Ar7000_LastFrHeader);
      if ( (EthTrcv_30_Ar7000_FailedApiCallType)0 != EthTrcv_30_Ar7000_FailedApiCall )
      {
        EthTrcv_30_Ar7000_ApiCallRetCnt++;
        if ( ETHTRCV_30_AR7000_MAX_API_CALL_RET_CNT <= EthTrcv_30_Ar7000_ApiCallRetCnt )
        {
#if ( ETHTRCV_30_AR7000_DEM_ERROR_DETECT == STD_ON )
          ETHTRCV_30_AR7000_DEM_REPORT_ERROR_STATUS(ETHTRCV_30_AR7000_E_ACCESS);
#endif /* ETHTRCV_30_AR7000_DEM_ERROR_DETECT */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
          (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_MAINFUNCTION, ETHTRCV_30_AR7000_E_TRCV_CONNECTION_LOST_OR_OVERLOADED);
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_REPORT */
        }
      }
      else
      {
        EthTrcv_30_Ar7000_ApiCallRetCnt = 0;
      }
    }
#endif /* ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON ) ||
          ( ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP        == STD_ON ) */
  }

  /* #90 Poll QCA Network membership key status and set it to random if a previously key change was called. */
#if (   (   (ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON) \
         && (ETHTRCV_30_AR7000_CALC_NMK_LOCALLY == STD_OFF) ) \
     || (ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API == STD_ON) )
  /* Polling API */
  if (ETHTRCV_30_AR7000_RT_PENDING == EthTrcv_30_Ar7000_SetKeyRetVal)
  {
    uint8 MStatus;
    EthTrcv_30_Ar7000_SetKeyRetVal = EthTrcv_30_Ar7000_SetKeyStatus(0, &MStatus);

    if (ETHTRCV_30_AR7000_RT_OK == EthTrcv_30_Ar7000_SetKeyRetVal)
    {
      EthTrcv_30_Ar7000_SetKeyCnt = 0;
    }
  }
  if (ETHTRCV_30_AR7000_RT_NOT_OK == EthTrcv_30_Ar7000_SetKeyRetVal)
  {
    if ( ETHTRCV_30_AR7000_SET_KEY_MAX_RETRIES > EthTrcv_30_Ar7000_SetKeyCnt)
    {
      uint8 MStatus;
      EthTrcv_30_Ar7000_SetKeyCnt++;
      EthTrcv_30_Ar7000_SetKeyRetVal = EthTrcv_30_Ar7000_SetKeyStatus(0, &MStatus);
    }
    else
    {
      EthTrcv_30_Ar7000_SetKeyCnt = 0;
      EthTrcv_30_Ar7000_SetKeyRetVal = ETHTRCV_30_AR7000_RT_OK;
    }
  }
#endif
  /* (   (   (ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON) \
         && (ETHTRCV_30_AR7000_CALC_NMK_LOCALLY == STD_OFF) ) \
     || (ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API == STD_ON) ) */

  /* #100 Some Request<->Confirm Pairs need some more well defined flow sequences */
#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
  if (ETHTRCV_30_AR7000_NO_HST_ACTION_REQUIRED != EthTrcv_30_Ar7000_HstAction[EthTrcv_30_Ar7000_HstActionIdx])
  {
    EthTrcv_30_Ar7000_HstActionRequiredSecondPhase(0);
  }
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */
} /* EthTrcv_30_Ar7000_MainFunction() */ /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_RxIndication
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
/* PRQA S 3673 7 */ /* MD_EthTrcv_30_Ar7000_3673 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_RxIndication(
        uint8                                                          EthIfCtrlIdx,
        Eth_FrameType                                                  FrameType,
        boolean                                                        IsBroadcast,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) PhysAddrPtr,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) DataU8Ptr,
        uint16                                                         LenByte )
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint16 mmType;
  uint8  trcvIdx;
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (EthIfCtrlIdx != EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx())
  {
    errorId = ETHTRCV_30_AR7000_E_INV_CONFIG;
  }
  else if (DataU8Ptr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    trcvIdx = 0;
    /* #20 Parse Vendor specific Management Message Type from received packets and call the specific handle routines. */
    /* PRQA S 0310 2 */ /* MD_EthTrcv_30_Ar7000_0310 */
    mmType  = (uint16)(((uint16)DataU8Ptr[ETHTRCV_30_AR7000_MME_MMTYPE + 0]) << 0);
    mmType |= (uint16)(((uint16)DataU8Ptr[ETHTRCV_30_AR7000_MME_MMTYPE + 1]) << 8);


    switch(mmType)
    {
    case ETHTRCV_30_AR7000_MME_VS_PL_LNK_STATUS | ETHTRCV_30_AR7000_MME_CNF:                    /* Link Status */
      EthTrcv_30_Ar7000_SetLinkStatus(
        trcvIdx,
        DataU8Ptr);
      break;

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )
    case ETHTRCV_30_AR7000_MME_VS_WRITE_AND_EXECUTE_APPLET | ETHTRCV_30_AR7000_MME_CNF:         /* Write and Execute Applet */
      EthTrcv_30_Ar7000_WriteAndExecuteApplet(
        trcvIdx,
        DataU8Ptr);
      break;

    case ETHTRCV_30_AR7000_MME_VS_HST_ACTION | ETHTRCV_30_AR7000_MME_IND:                       /* Host Action Required */
      EthTrcv_30_Ar7000_HstActionRequired(
        trcvIdx,
        DataU8Ptr);
      break;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#if ( (STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP) || \
      (STD_ON == ETHTRCV_30_AR7000_ENABLE_READ_NMK)   || \
      (STD_ON == ETHTRCV_30_AR7000_ENABLE_MODOP_API) )
    case ETHTRCV_30_AR7000_MME_VS_MODULE_OPERATION | ETHTRCV_30_AR7000_MME_CNF:               /* Module Operation */
      EthTrcv_30_Ar7000_ModuleOperation(
        trcvIdx,
        DataU8Ptr);
      break;
#endif /* (STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP) ||
          (STD_ON == ETHTRCV_30_AR7000_ENABLE_READ_NMK)   ||
          (STD_ON == ETHTRCV_30_AR7000_ENABLE_MODOP_API) ) */
#if ( ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON )
    case ETHTRCV_30_AR7000_MME_VS_SET_KEY | ETHTRCV_30_AR7000_MME_CNF:                          /* Set NMK */
      EthTrcv_30_Ar7000_SetKeyCnfStatus(
        trcvIdx,
        (P2CONST(uint8, AUTOMATIC, ETHTRCV_30_AR7000_CONST))DataU8Ptr);
      break;
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_NMK */

#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API )
    case ETHTRCV_30_AR7000_MME_MS_PB_ENC | ETHTRCV_30_AR7000_MME_CNF:                           /* Pushbutton Encryption */
      EthTrcv_30_Ar7000_SetPBEncryptionStatus(
        trcvIdx,
        DataU8Ptr);
      break;
#endif /* ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API */

#if ( ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION == STD_ON )
    case ETHTRCV_30_AR7000_MME_VS_SW_VER | ETHTRCV_30_AR7000_MME_CNF:                          /* Software version */
      EthTrcv_30_Ar7000_SetSoftwareVersion(
        trcvIdx,
        DataU8Ptr);
      break;
#endif /* ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION */

#if ( ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD == STD_ON )                                  /* Reset device */
    case ETHTRCV_30_AR7000_MME_VS_RS_DEV | ETHTRCV_30_AR7000_MME_CNF:
# if ( ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT == STD_ON )
      EthTrcv_30_Ar7000_FwDownloadAndStartCnt = ETHTRCV_30_AR7000_FIRMWARE_DOWNLOAD_AND_START_CYCLES;
#endif /* ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT */
# if ( ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_SLAC_COM_EVENT == STD_ON )
      EthTrcv_30_Ar7000_FwReadyForPlcComCnt = ETHTRCV_30_AR7000_FIRMWARE_READY_FOR_SLAC_COM_CYCLES;
#endif /* ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_SLAC_COM_EVENT */
      EthTrcv_30_Ar7000_LinkStateLockCnt = ETHTRCV_30_AR7000_LINKSTATE_LOCK_CYCLES;
      EthTrcv_30_Ar7000_LinkState        = ETHTRCV_LINK_STATE_DOWN;
      /* Reset firmware download information to prepare for the next required firmware download */
      EthTrcv_30_Ar7000_WriteAppletLockCnt = 0;
      EthTrcv_30_Ar7000_AppletNrLocked = ETHTRCV_30_AR7000_NO_APPLET_LOCKED;
      break;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD */

#if ( ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API == STD_ON )                                   /* Set Property */
    case ETHTRCV_30_AR7000_MME_VS_SET_PROPERTY | ETHTRCV_30_AR7000_MME_CNF:
      EthTrcv_30_Ar7000_IsSettingProperty       = ETHTRCV_30_AR7000_NO_WAIT_ON_CONF;
      EthTrcv_30_Ar7000_SetPropertyCnfAvailable = ETHTRCV_30_AR7000_STATE_READY;
      EthTrcv_30_Ar7000_SetPropertyCycCnt       = 0;
      /* PRQA S 0310 2 */ /* MD_EthTrcv_30_Ar7000_0310 */
      EthTrcv_30_Ar7000_SetPropertyStatus = DataU8Ptr[ETHTRCV_30_AR7000_MME_VS_MSTATUS];
      break;
#endif /* ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API */

    default:
      break;
    }

    /* #30 Parse SLAC related Management Message Type from received packets and call the specific handle routines. */
#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC )
    EthTrcv_30_Ar7000_MME_Slac_RxIndication(
      trcvIdx,
      FrameType,
      IsBroadcast,
      PhysAddrPtr,
      DataU8Ptr,
      LenByte);
#else
    ETHTRCV_30_AR7000_DUMMY_STATEMENT(FrameType); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
    ETHTRCV_30_AR7000_DUMMY_STATEMENT(IsBroadcast); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
    ETHTRCV_30_AR7000_DUMMY_STATEMENT(PhysAddrPtr); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#if ( STD_OFF == ETHTRCV_30_AR7000_ENABLE_SM_HOOK_ROUTINE )
    ETHTRCV_30_AR7000_DUMMY_STATEMENT(DataU8Ptr); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
    ETHTRCV_30_AR7000_DUMMY_STATEMENT(LenByte); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif
#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */

    /* #40 Call user defined application handler if ETHTRCV_30_AR7000_ENABLE_SM_HOOK_ROUTINE is enabled. */
#if ( STD_ON == ETHTRCV_30_AR7000_ENABLE_SM_HOOK_ROUTINE )
    Appl_TrcvHookRoutine(DataU8Ptr, LenByte);
#endif /* ETHTRCV_30_AR7000_ENABLE_SM_HOOK_ROUTINE */
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_RXINDICATION, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(EthIfCtrlIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif
} /* EthTrcv_30_Ar7000_MME_RxIndication() */ /* PRQA S 6030, 6050, 6060 */ /* MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_TxConfirmation
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_TxConfirmation(
    uint8 EthIfCtrlIdx,
    uint8 BufIdx )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx() != EthIfCtrlIdx)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    ETHTRCV_30_AR7000_DUMMY_STATEMENT(BufIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_TXCONFIRMATION, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(EthIfCtrlIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif
} /* EthTrcv_30_Ar7000_MME_TxConfirmation() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_TrcvLinkStateChgCbk
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_TrcvLinkStateChgCbk(
    uint8 EthIfCtrlIdx,
    EthTrcv_LinkStateType TrcvLinkState)
{
  /* #10 Call SLAC protocol callback */
#if (defined(ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
  EthTrcv_30_Ar7000_Slac_TrcvLinkStateChgCbk(EthIfCtrlIdx, TrcvLinkState);
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(EthIfCtrlIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvLinkState); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif
} /* EthTrcv_30_Ar7000_TrcvLinkStateChgCbk() */

#define ETHTRCV_30_AR7000_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

/* module specific MISRA deviations:
 MD_EthTrcv_30_Ar7000_0750_0759:
      Reason:     The union simplifies the access to the Ethernet buffer. Without an union the buffer needs to be from type
                  Eth_DataType (uint32). Byte-wise access to the buffer always requires a cast and a justification of
                  message 3305 and 0310. The union enables an easy byte-by-byte access that is necessary to process
                  the MME frames.
      Risk:       Some coding standards advocate that unions should be avoided
      Prevention: There is no known compiler that does not support unions
 MD_EthTrcv_30_Ar7000_0779:
      Reason:     The AUTOSAR short name is too long.
      Risk:       There is no risk.
      Prevention: -
 MD_EthTrcv_30_Ar7000_3199:
      Reason:     Dummy statement to avoid compiler warnings.
      Risk:       There is no risk as such statements have no effect on the code.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_0311:
      Reason:     Required by AUTOSAR API of the Ethernet Interface. The MAC address is not defined as const.
      Risk:       There is no risk since the EthIf component does not change the MAC.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_0310:
      Reason:     The pointer where the cast led to has different alignment. The cast
                  is necessary to get a bytewise access to the 32bit Ethernet buffer and vice versa.
      Risk:       There is no risk since the array is a continuous data buffer.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_0310_1:
      Reason:     The DAK has a size that is divisable by four. The cast to 32 bit values does not
                  lead to an invalid access and optimizes the copy process.
      Risk:       There is no risk since the array is a continuous data buffer.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_0310_2:
      Reason:     The Memory Ctrl, PIB and FW applet have a size that is divisable by four.
                  The cast to 32 bit values does not lead to an invalid access and optimizes the copy process.
      Risk:       There is no risk since the array is a continuous data buffer.
      Prevention: Covered by code review. The size of Memory Ctrl, PIB and FW is checked by preprocessor directive.
 MD_EthTrcv_30_Ar7000_0310_3:
      Reason:     Casting is used to support C-style inheritance. A pointer to a EthTrcv_30_Ar7000_ModOp_Rd_Params
                  object is always a pointer to a EthTrcv_30_Ar7000_ModOp_Params object, too. The
                  EthTrcv_30_Ar7000_ModOp_Params object is the first element in the
                  EthTrcv_30_Ar7000_ModOp_Rd_Params.
      Risk:       There is no risk since the array is a continuous data buffer.
      Prevention: Covered by code review. The size of Memory Ctrl, PIB and FW is checked by preprocessor directive.
 MD_EthTrcv_30_Ar7000_0310_4:
      Reason:     The module operation API only allows to read 4 byte blocks at a time. The NMK size is 4 bytes
                  divisable. This is ensured by GENy
      Risk:       There is no risk.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_0310_5:
      Reason:     Cast PIB data block to NvMHeaderType structure to simplify the access to the module fields.
                  The PIB data block is very extensive and a offset-based access is hard to interpret.
      Risk:       There is no known risk.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen:
      Reason:     For some reason QCA does not recognize that ethBuf.BufLen can be changed inside the previous
                  call to EthIf_ProvideTxBuffer.
      Risk:       In case a compiler has the same issue, errors may not be detected.
      Prevention: Covered by code review functional test cases.
 MD_EthTrcv_30_Ar7000_3305
      Reason:     The pointer where the cast led to has different alignment. The cast
                  is necessary to get a 4 byte aligned access.
      Risk:       There is no risk since the array is a continuous data buffer.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_3218
      Reason:     Debugging variable is global to simplify the access via debugger.
      Risk:       There is no risk. Debugging variables are switched off in productive systems.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_3218_2
      Reason:     This variable is used to store the header of a previous message, there for it cannot be a normal
                  local variable. It must not be a local static variable, because local static variables cannot be
                  mapped using the ASR memory mapping. Therefore this variable must be a global staic variable,
                  because these ones can be mapped.
      Risk:       There is no risk.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_3415
      Reason:     The called API does only perform a swap operation for the comparison, therefore its execution does
                  not have any effects on the following code.
      Risk:       There is no risk.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_3673:
      Reason:     Attribute definition is required by AUTOSAR.
      Risk:       There is no risk
      Prevention: Covered by code review.
*/

/**********************************************************************************************************************
 *  END OF FILE: EthTrcv_30_Ar7000.c
 *********************************************************************************************************************/

