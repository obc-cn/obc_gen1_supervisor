/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  EthTrcv_30_Ar7000_Slac.h
 *        \brief  Ethernet transceiver driver QCA 7005 SLAC header file
 *
 *      \details  Vector static code header file for the Ethernet transceiver driver QCA 7005 module. This file
 *                contains all SLAC protocol related implementations.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file EthTrcv_30_Ar7000.h.
 *********************************************************************************************************************/

#if !defined (ETHTRCV_30_AR7000_SLAC_H)
# define ETHTRCV_30_AR7000_SLAC_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "EthTrcv_30_Ar7000_Lcfg.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
#if (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC)
/* ETHTRCV SLAC API IDs */
#define ETHTRCV_30_AR7000_API_ID_SLAC_INIT                          0x30U
#define ETHTRCV_30_AR7000_API_ID_SLAC_START                         0x31U
#define ETHTRCV_30_AR7000_API_ID_SLAC_TERMINATE                     0x32U
#define ETHTRCV_30_AR7000_API_ID_SLAC_RESET                         0x33U
#define ETHTRCV_30_AR7000_API_ID_SLAC_TOGGLE_CONFIRMATION           0x34U
#define ETHTRCV_30_AR7000_API_ID_GET_ATTEN_PROFILE_TBL              0x35U
#define ETHTRCV_30_AR7000_API_ID_DIAG_DATA_WRITE_ACCESS             0x36U
#define ETHTRCV_30_AR7000_API_ID_DIAG_DATA_READ_ACCESS              0x37U
#define ETHTRCV_30_AR7000_API_ID_SLAC_MAINFUNCTION                  0x38U
#define ETHTRCV_30_AR7000_API_ID_GET_UNFILTERED_ATTEN_PROFILE_MEANS 0x39u

/* ETHTRCV SLAC DET errors */
#define ETHTRCV_30_AR7000_SLAC_E_NOT_INITIALIZED                   0x10U
#define ETHTRCV_30_AR7000_SLAC_E_INV_PARAMETER                     0x11U
#define ETHTRCV_30_AR7000_SLAC_E_NVM                               0x12U

/* Diagnostic bock identifiers */
#define ETHTRCV_30_AR7000_SLAC_DIAG_ID_CALIB_DATA                  0x01U
#define ETHTRCV_30_AR7000_SLAC_DIAG_ID_AMP_MAP_DATA                0x02U

/**********************************************************************************************************************
 *  GLOBAL DATA
 **********************************************************************************************************************/
#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#if (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC)
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_Init
 **********************************************************************************************************************/
/*!
 * \brief       Initializes the SLAC mechanism
 * \details     -
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 * \note        Has to be called before usage of the module
 * \trace       CREQ-163901
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_Init(void);

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_Start
 **********************************************************************************************************************/
/*!
 * \brief       Starts the SLAC mechanism
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   StartMode      SLAC operation mode (Refer to integration chapter for more detailed information):\n
 *                               - 0b------00: do not request validation if SLAC result is potentially found\n
 *                               - 0b------01: request validation if SLAC result is potentially found\n
 *                               - 0b------11: request validation in any case\n
 *                               - 0b-----0--: in case of validation request, do not force EVSE to do validation\n
 *                               - 0b-----1--: in case of validation request, force EVSE to do validation\n
 *                               - 0b----1000: skip validation and resume with last known MAC address\n
 *                               - 0b---1----: exchange local amplitude map\n
 *                               - 0b--1-----: start matching even if validation has falid\n
 *                               - 0b-1--0000: disable validation completely\n
 *                               - 0b1--0----: exchange local amplitude map if triggered by EVSE
 * \return      E_OK           Starting SLAC was successful
 * \return      E_NOT_OK       Failure
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 * \trace       CREQ-145739
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_Start(
  uint8                                TrcvIdx,
  EthTrcv_30_Ar7000_Slac_StartModeType StartMode);

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_Terminate
 **********************************************************************************************************************/
/*!
 * \brief       Terminates the SLAC mechanism.
 * \details     When the SLAC mechanism gets terminated, the connection parameters of the last valid sessions are
 *              stored. When restarting SLAC, the EVSE MAC address, NMK and NID are reused, if possible.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \return      E_OK           Terminating SLAC was successful
 * \return      E_NOT_OK       Failure during leave network
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 * \trace       CREQ-145740
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_Terminate(
  uint8 TrcvIdx);

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_Reset
 **********************************************************************************************************************/
/*!
 * \brief       Resets the SLAC mechanism
 * \details     When the SLAC mechanism is reset, the connection parameters of the last session are discarded.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \return      E_OK           Reset SLAC was successful
 * \return      E_NOT_OK       Failure during leave network
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 * \trace       CREQ-145738
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_Reset(
  uint8 TrcvIdx);

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_ToggleConfirmation
 **********************************************************************************************************************/
/*!
 * \brief       Toggle confirmation, called after toggle request by the application.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \return      E_OK           Toggle confirm accepted
 * \return      E_NOT_OK       Toggle confirm not accepted
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous FALSE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 * \trace       CREQ-145742
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_ToggleConfirmation(
  uint8 TrcvIdx);

#if ( STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_GET_ATABLE_API )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_GetAttenProfileTbl
 **********************************************************************************************************************/
/*!
 * \brief          Returns the attenuation profile table of the last or current running SLAC process.
 * \details        The table contains the mean attenuation value, the MAC address, the detected toggles and the
 *                 discovery status of all EVSEs that were found or potentially found, ordered by an increasing mean
 *                 attenuation value.\n
 *                 An entry of the table is a structure of type EthTrcv_30_Ar7000_Slac_AttenCharProfileType. The type is
 *                 public and may be used by the application. The same applies for the type
 *                 EthTrcv_30_Ar7000_Slac_DiscoverStatusType that is used as a member within an entry. The discovery
 *                 status may be populated with one of the values below:\n
 *                   - ETHTRCV_30_AR7000_SLAC_EVSE_FOUND\n
 *                   - ETHTRCV_30_AR7000_SLAC_EVSE_POTENTIALLY_FOUND\n
 *                 See SLAC integration chapter for a detailed assembly of an attenuation table entry.
 *  \param[in]     TrcvIdx          Zero based index of the transceiver
 *  \param[out]    AttenProfileTbl  Pointer to the attenuation table. The buffer the pointer points to must be large
 *                                  enough to hold the number of entries specified by the parameter TblEntries.
 *                                  The size of the table can be calculated by the following formula:\n
 *                                    sizeof(EthTrcv_30_Ar7000_Slac_AttenCharProfileType) * TblEntries
 *  \param[in,out] TblEntries       TblEntries is an in/out pointer.\n
 *                                    [in]:  Number of entries the user is interested into.\n
 *                                    [out]: When TblEntries is larger than the available number of valid entries
 *                                           within the attenuation table it is adapted to the number of valid entries.
 *                                           The returned table contains as much entries as specified by the returned
 *                                           value of TblEntries.
 * \return         E_OK             Table content valid
 * \return         E_NOT_OK         Table content is empty. The SLAC process was not running yet or did not approached
 *                                  until the attenuation profile reception.
 * \pre            -
 * \context        TASK|ISR2
 * \reentrant      TRUE
 * \synchronous    TRUE
 * \config         ETHTRCV_30_AR7000_ENABLE_SLAC && ETHTRCV_30_AR7000_SLAC_ENABLE_GET_ATABLE_API
 * \trace          CREQ-145741
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_GetAttenProfileTbl(
    uint8 TrcvIdx,
    P2VAR(EthTrcv_30_Ar7000_Slac_AttenCharProfileType, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA)
      AttenProfileTable,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) TblEntries);
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_GET_ATABLE_API */

#if ( STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_GET_ATABLE_API )
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_GetUnfilteredAttenProfileMeans
 **********************************************************************************************************************/
/*!
 * \brief         Reads the attenuation profile means of the last or current running SLAC process.
 * \details       -
 * \param[in]     TrcvIdx               Zero based index of the transceiver
 * \param[in,out] AttenProfileMeansAry  Pointer to the attenuation means array. The buffer the pointer points to must
 *                                      be large enough to hold the number of entries specified by the parameter
 *                                      MeansEntries.
 *                                        [in]:  Pointer to the buffer were the array can be written to.
 *                                        [out]: Pointer to the first element in attenuation means array.
 * \param[in,out] MeansEntries          MeansEntries is an in/out pointer.\n
 *                                        [in]:  Number of entries the user is interested into.\n
 *                                        [out]: When MeansEntries is larger than the available number of valid entries
 *                                               within the attenuation table it is adapted to the number of valid
 *                                               entries. The returned table contains as much entries as specified by
 *                                               the returned value of MeansEntries.
 * \return        E_OK                  Array content valid
 * \return        E_NOT_OK              Array content is empty. The SLAC process was not running yet or did not
 *                                      approached until the attenuation profile reception.
 * \pre           -
 * \context       TASK|ISR2
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \config        ETHTRCV_30_AR7000_ENABLE_SLAC && ETHTRCV_30_AR7000_SLAC_ENABLE_GET_ATABLE_API
 * \trace         CREQ-145741
 */
extern FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_GetUnfilteredAttenProfileMeans(
        uint8                                                            TrcvIdx,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) AttenProfileMeansAry,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   MeansEntries);
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_GET_ATABLE_API */

#if ( ( STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP ) || ( STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION ) )
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_DiagDataWriteAccess
 *********************************************************************************************************************/
/*!
 * \brief       Writes amplitude map and calibration data block.
 * \details     Write access to the diagnostic data blocks stated below (Data identifiers bracketed):\n
 *                - Attenuation calibration data      (ETHTRCV_30_AR7000_SLAC_DIAG_ID_CALIB_DATA)
 *                - Amplitude map data                (ETHTRCV_30_AR7000_SLAC_DIAG_ID_AMP_MAP_DATA)
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   DataId         Data identifier
 * \param[in]   DataPtr        Pointer with address of the diagnostic data
 * \param[in]   DataLen        Length of the diagnostic data in bytes
 * \return      E_OK           Diagnostic data written successfully\n
 *              E_NOT_OK       Invalid parameter (data identifier not found, NULL_PTR parameter, invalid length)
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC &&
 *              (ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP || ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION)
 * \trace       CREQ-163902
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_DiagDataWriteAccess(
          uint8                                          TrcvIdx,
          uint8                                          DataId,
  P2CONST(uint8, AUTOMATIC, ETHTRCV_30_AR7000_APPL_DATA) DataPtr,
          uint16                                         DataLen);
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP || ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

#if ( ( STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP ) || ( STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION ) )
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_DiagDataReadAccess
 *********************************************************************************************************************/
/*!
 * \brief         Reads out the amplitude map and calibration data block.
 * \details       Read access to the diagnostic data blocks stated below (Data identifiers bracketed):\n
 *                  - Attenuation calibration data      (ETHTRCV_30_AR7000_SLAC_DIAG_ID_CALIB_DATA)
 *                  - Amplitude map data                (ETHTRCV_30_AR7000_SLAC_DIAG_ID_AMP_MAP_DATA)
 * \param[in]     TrcvIdx        Zero based index of the transceiver
 * \param[in]     DataId         Data identifier
 * \param[out]    DataPtr        Buffer to be populated with the diagnostic data
 * \param[in,out] DataLen        DataLen is an in/out pointer.\n
 *                                 [in]:  The maximum length in bytes the buffer can hold.\n
 *                                 [out]: The actual amount of data that was written into the buffer. The actual
 *                                          amount is equal or less than the maximum length that was handed over.
 * \return        E_OK           Diagnostic data read successfully\n
 * \return        E_NOT_OK       Invalid parameter (data identifier not found, NULL_PTR parameter) or data not
 *                               ready. Try again later.
 * \pre           -
 * \context       TASK|ISR2
 * \reentrant     TRUE
 * \synchronous   TRUE
 * \config        ETHTRCV_30_AR7000_ENABLE_SLAC &&
 *                (ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP || ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION)
 * \trace         CREQ-163902
 */
/* PRQA S 0777 5  */ /* MD_EthTrcv_30_Ar7000_0777 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_DiagDataReadAccess(
    uint8 TrcvIdx,
    uint8 DataId,
    P2VAR(uint8, AUTOMATIC, ETHTRCV_30_AR7000_APPL_DATA) DataPtr,
    P2VAR(uint16, AUTOMATIC, ETHTRCV_30_AR7000_APPL_VAR) DataLen);
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP || ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_TrcvLinkStateChgCbk
 **********************************************************************************************************************/
/*!
 * \brief       Transceiver link state change callback.
 * \details     -
 * \param[in]   EthIfCtrlIdx   Index of the Ethernet interface controller
 * \param[in]   TrcvLinkState  Transceiver link state
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 * \trace       CREQ-163903
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_TrcvLinkStateChgCbk(
    uint8 EthIfCtrlIdx,
    EthTrcv_LinkStateType TrcvLinkState);

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_MainFunction
 **********************************************************************************************************************/
/*!
 * \brief       Cyclic main function
 * \details     -
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_MainFunction(void);

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_RxIndication
 **********************************************************************************************************************/
/*!
 * \brief       <Brief description of the function's functionality>
 * \details     This service is called by the transceiver module if a MME message was successfully received.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   FrameType      Ethernet frame type
 * \param[in]   IsBroadcast    Broadcast indication
 * \param[in]   PhysAddrPtr    Pointer to MAC address
 * \param[in]   DataPtr        Pointer to received data frame
 * \param[in]   LenByte        Length of received frame
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   FALSE
 * \synchronous FALSE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 * \trace       CREQ-163904
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_RxIndication(
        uint8                                                          TrcvIdx,
        Eth_FrameType                                                  FrameType,
        boolean                                                        IsBroadcast,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) PhysAddrPtr,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) DataU8Ptr,
        uint16                                                         LenByte);

#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */

#define ETHTRCV_30_AR7000_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#endif /* ETHTRCV_30_AR7000_SLAC_H */

/**********************************************************************************************************************
 *  END OF FILE: EthTrcv_30_Ar7000_Slac.h
 *********************************************************************************************************************/

