/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  EthTrcv_30_Ar7000_Slac.c
 *        \brief  Ethernet transceiver driver QCA 7005 SLAC source file
 *
 *      \details  Vector static code implementation for the Ethernet transceiver driver QCA 7005 module. This file
 *                contains all SLAC protocol related implementations.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file EthTrcv_30_Ar7000.h.
 *********************************************************************************************************************/

#define ETHTRCV_30_AR7000_SLAC_SOURCE

/*lint -e438 */ /* Suppress ID438 because Config pointer only used in Post-Build Variant */
/*lint -e451 */ /* Suppress ID451 because MemMap.h cannot use a include guard */
/*lint -e506 */ /* Suppress ID506 due to MD_MSR_14.1 */
/*lint -e537 */ /* Suppress ID537 due to MD_MSR_19.1 */

/* PRQA S 0779 LONG_IDENTIFIERS */ /* MD_EthTrcv_30_Ar7000_0779 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 ETHTRCV_30_AR7000_SLAC_C_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "EthTrcv_30_Ar7000_Slac.h"
#include "EthTrcv_30_Ar7000_Priv.h"
#include "EthTrcv_30_Ar7000.h"
#include "IpBase.h"
#include "EthIf.h"

#if (((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP)  && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP)) || \
     ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION)))
#include "NvM.h"
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP || ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION */
/* PRQA L:ETHTRCV_30_AR7000_SLAC_C_IF_NESTING */ /* MD_MSR_1.1_828 */

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/
/* Check consistency of source and header file. */
#if ( (ETHTRCV_30_AR7000_SW_MAJOR_VERSION != 8U) || \
      (ETHTRCV_30_AR7000_SW_MINOR_VERSION != 0U) || \
      (ETHTRCV_30_AR7000_SW_PATCH_VERSION != 0U) )
  #error "EthTrcv_30_Ar7000.c: Source and Header file are inconsistent!"
#endif

/**********************************************************************************************************************
 *  LOCAL DEFINES
 **********************************************************************************************************************/
#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
/* Special Join modes */
#define ETHTRCV_30_AR7000_SLAC_JOIN_ALL_TOGGLES                   0x40U
#define ETHTRCV_30_AR7000_SLAC_JOIN_LAST_APPRVD                   0x80U

/* validation results */
#define ETHTRCV_30_AR7000_SLAC_VALID_NOT_READY                    0x00U
#define ETHTRCV_30_AR7000_SLAC_VALID_READY                        0x01U
#define ETHTRCV_30_AR7000_SLAC_VALID_SUCCESS                      0x02U
#define ETHTRCV_30_AR7000_SLAC_VALID_FAILURE                      0x03U
#define ETHTRCV_30_AR7000_SLAC_VALID_NOT_REQUIRED                 0x04U

#define ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_BEGIN                 0x10U
#define ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_REQUESTED             0x20U
#define ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_END                   0x40U
#define ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_ANSWERED              0x80U
#define ETHTRCV_30_AR7000_SLAC_VALID_INVALID                      0x1FU

/* NvM read states */
#define ETHTRCV_30_AR7000_SLAC_READ_STATE_DATA                    0x00U
#define ETHTRCV_30_AR7000_SLAC_READ_STATE_DONE                    0x01U
#define ETHTRCV_30_AR7000_SLAC_READ_STATE_PENDING                 0x02U
#define ETHTRCV_30_AR7000_SLAC_READ_STATE_ERROR                   0x03U

/* Response types */
#define ETHTRCV_30_AR7000_SLAC_RESP_TYPE_HLE                      0x00U
#define ETHTRCV_30_AR7000_SLAC_RESP_TYPE_STA                      0x01U

/* Polling retrials */
#define ETHTRCV_30_AR7000_SLAC_LEAVESCSTATE_MAX_RETRIES           0x03U

/* Invalid profile Index */
#define ETHTRCV_30_AR7000_SLAC_INVALID_PROFILE_IDX              0xFFFFU
#define ETHTRCV_30_AR7000_SLAC_INVALID_ATTENUATION                0xFFU

#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */

#ifndef ETHTRCV_30_AR7000_LOCAL
#define ETHTRCV_30_AR7000_LOCAL static
#endif /* ETHTRCV_30_AR7000_LOCAL */

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))

#if (((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP)  && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP)) || \
     ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION)))
typedef uint8 EthTrcv_30_Ar7000_Slac_NvmReadStateType;
#endif /* (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP)) ||
          (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) */

/* SlacFlags Assembly
 *
 *  0                 1                 2                 3
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |    INIALIZED    |    SET CCO      |  SET CAP3 PRIO  |  PARAM REQUEST  |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | SOUNDING/ATTEN  |   VALIDATING 1  |   VALIDATING 2  |   CONFIRMING    |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *  4                 5                 6                 7
 *  8                 9                 10                11
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |     JOINING     |  AMP MAP EXCH   | AMP MAP REQ RECV| AMP MAP CNF RECV|
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |              UNUSED               |  WAIT FOR LINK  |CONN.PARAMS VALID|
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *  12                13                14                15
 */

/* Slac Validation Result Assembly
 *
 *  0                 1                 2                 3
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                  VALIDATION RESULT                  |     UNUSED      |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |  TOGGLE BEGIN   |TOGGLE REQUESTED |   TOGGLE END    | TOGGLE ANSWERED |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *  4                 5                 6                 7
 *
 */

#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
/* PRQA S 0750 9 */ /* MD_EthTrcv_30_Ar7000_0750 */
typedef union EthTrcv_30_Ar7000_Slac_MsgBufTypeUnion
{
  EthTrcv_30_Ar7000_Slac_Msg_ParmDiscoveryCnfType  SlacParmDiscoveryCnf;
  EthTrcv_30_Ar7000_Slac_Msg_StartAttenCharIndType SlacStartAttenCharInd;
  EthTrcv_30_Ar7000_Slac_Msg_SoundingIndType       SlacSoundingInd;
  EthTrcv_30_Ar7000_Slac_Msg_AttenCharIndType      SlacAttenCharInd;
  EthTrcv_30_Ar7000_Slac_Msg_AttenCharRspType      SlacAttenCharRsp;
  EthTrcv_30_Ar7000_Slac_Msg_MatchReqType          SlacMatchReq;
  EthTrcv_30_Ar7000_Slac_Msg_MatchCnfType          SlacMatchCnf;
} EthTrcv_30_Ar7000_Slac_MsgBufType;
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */

#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */

/**********************************************************************************************************************
 *  LOCAL DATA
 **********************************************************************************************************************/
#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))

#define ETHTRCV_30_AR7000_START_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

ETHTRCV_30_AR7000_LOCAL VAR (uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_Slac_LastConfirmMode;

#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP))
ETHTRCV_30_AR7000_LOCAL VAR (EthTrcv_30_Ar7000_Slac_NvmReadStateType, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_Slac_AmpMapNvmState;
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION))
ETHTRCV_30_AR7000_LOCAL VAR (EthTrcv_30_Ar7000_Slac_NvmReadStateType, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_Slac_CalibBlockNvmState;
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION*/

ETHTRCV_30_AR7000_LOCAL VAR (EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_Slac_LeaveScStateRetVal;
ETHTRCV_30_AR7000_LOCAL VAR (uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_Slac_LeaveScStateCnt;

#define ETHTRCV_30_AR7000_STOP_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#define ETHTRCV_30_AR7000_START_SEC_VAR_NOINIT_UNSPECIFIED_NVM
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION))
ETHTRCV_30_AR7000_LOCAL sint8 EthTrcv_30_Ar7000_Slac_AttenCalibrationValues[ETHTRCV_30_AR7000_SLAC_ATTEN_CALIBRATION_VALUE_NUM];
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION */

#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP))
ETHTRCV_30_AR7000_LOCAL uint8 EthTrcv_30_Ar7000_Slac_AmpMap[(ETHTRCV_30_AR7000_SLAC_AMP_MAP_ENTRIES + 1) / 2];
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

#define ETHTRCV_30_AR7000_STOP_SEC_VAR_NOINIT_UNSPECIFIED_NVM
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#define ETHTRCV_30_AR7000_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

ETHTRCV_30_AR7000_LOCAL EthTrcv_30_Ar7000_Slac_AttenCharProfileType EthTrcv_30_Ar7000_Slac_Profiles [ETHTRCV_30_AR7000_SLAC_MAX_ATTEN_CHAR_PROFILES];

#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
/* The AttenCharRsp structure is the biggest structure of all message types. Allocate a buffer that holds
 * at least the maximum message structure. All other structures fit into the buffer as well */
/* PRQA S 0759 1 */ /* MD_EthTrcv_30_Ar7000_0750_0759 */
ETHTRCV_30_AR7000_LOCAL EthTrcv_30_Ar7000_Slac_MsgBufType EthTrcv_30_Ar7000_Slac_MsgBuf;
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */

#define ETHTRCV_30_AR7000_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#define ETHTRCV_30_AR7000_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */
ETHTRCV_30_AR7000_LOCAL CONST(uint8, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_Slac_MNBC_Addr[6] =
{
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
};
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE))
ETHTRCV_30_AR7000_LOCAL CONST(uint8, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_Slac_CCO_Val[1] = {0x01}; /* Be never CCO: Enable PEV */
ETHTRCV_30_AR7000_LOCAL CONST(EthTrcv_30_Ar7000_SetPropertyParamterType, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_Slac_CCO_Prop_Params =
{
  0x01,         /* Option: ApplyNow_DoNotPersist */
  0x00000000,   /* Property Version */
  0x00000071,   /* Property ID */
  0x00000001,   /* Property Length */
  &EthTrcv_30_Ar7000_Slac_CCO_Val[0]
};
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO))
ETHTRCV_30_AR7000_LOCAL CONST(uint8, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_Slac_CAP3_Prop_Val[4] = {0x01, 0x03, 0x03, 0x02};
/* PRQA S 3218 8 */ /* MD_EthTrcv_30_Ar7000_3218 */
ETHTRCV_30_AR7000_LOCAL CONST(EthTrcv_30_Ar7000_SetPropertyParamterType, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_Slac_CAP3_Prop_Params =
{
  0x01,         /* Option */
  0x00000000,   /* Property Version */
  0x00000064,   /* Property ID */
  0x00000004,   /* Property Length */
  &EthTrcv_30_Ar7000_Slac_CAP3_Prop_Val[0]
};
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO */

#define ETHTRCV_30_AR7000_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *  GLOBAL DATA
 **********************************************************************************************************************/
#define ETHTRCV_30_AR7000_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

VAR(EthTrcv_30_Ar7000_Slac_MgmtType, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_Slac_MgmtStruct; /* Is accessed from EthTrcv_30_Ar7000.c */

#define ETHTRCV_30_AR7000_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/
#define ETHTRCV_30_AR7000_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
/* MME */
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_ParamReq
 **********************************************************************************************************************/
/*!
 * \brief       Sends a 'SLAC parameter' request MME into the powerline communication network.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \return      E_OK           The host has successfully send the request MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_ParamReq(
  uint8 TrcvIdx);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_StartAttenCharInd
 **********************************************************************************************************************/
/*!
 * \brief       Sends a 'SLAC start attenuation characterization' indication MME into the powerline communication
 *              network.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \return      E_OK           The host has successfully send the request MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_StartAttenCharInd(
  uint8 TrcvIdx);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_SoundInd
 **********************************************************************************************************************/
/*!
 * \brief       Sends a 'SLAC sound' indication MME into the powerline communication network.
 * \details     This packets are needed to measure the attenuation.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \return      E_OK           The host has successfully send the request MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_SoundInd(
  uint8 TrcvIdx);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_AttenCharResp
 **********************************************************************************************************************/
/*!
 * \brief       Sends a 'SLAC characterization' response MME to the specified EVSE.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   DestAddr       6-Byte array which contains the MAC address of the specific EVSE
 * \param[in]   Result         Result of the processed received 'attenuation characterization' indication MME
 * \return      E_OK           The host has successfully send the request MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_AttenCharResp(
  uint8                TrcvIdx,
  EthTrcv_PhysAddrType DestAddr,
  uint8                Result);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_ValidateReq
 **********************************************************************************************************************/
/*!
 * \brief       Sends a 'SLAC validation' request MME to the specified EVSE.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   Timeout        Timeout value for the internal timer e.g.  0 == 100 ms, 1 == 200 ms, ...
 * \param[in]   DestAddr       6-Byte array which contains the MAC address of the specific EVSE
 * \return      E_OK           The host has successfully send the request MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_ValidateReq(
  uint8                TrcvIdx,
  uint8                Timeout,
  EthTrcv_PhysAddrType DestAddr);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_MatchReq
 **********************************************************************************************************************/
/*!
 * \brief       Sends a 'SLAC match' request MME to the specified EVSE.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   DestAddr       6-Byte array which contains the MAC address of the specific EVSE
 * \return      E_OK           The host has successfully send the request MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_MatchReq(
  uint8                TrcvIdx,
  EthTrcv_PhysAddrType DestAddr);

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE))
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_AmpMapReq
 **********************************************************************************************************************/
/*!
 * \brief       Sends a 'SLAC amplitude map' request MME to the specified EVSE
 * \details     This sequence is needed for the amplitude map exchange process.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   AmpMap         Pointer to the start address of an amplitude map array
 * \param[in]   AmpMapEntries  Amount of amplitude map entries
 * \param[in]   DestAddr       6-Byte array which contains the MAC address of the specific EVSE
 * \return      E_OK           The host has successfully send the request MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC && ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_AmpMapReq(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) AmpMap,
          uint16                                                           AmpMapEntries,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DestAddr);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_AmpMapCnf
 **********************************************************************************************************************/
/*!
 * \brief       Sends a 'SLAC amplitude map' confirmation MME to the specified EVSE
 * \details     Confirmation is done upon a received 'SLAC amplitude map' request MME.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   ResType        Result of the processed received 'SLAC amplitude map' request MME.
 * \param[in]   DestAddr       6-Byte array which contains the MAC address of the specific EVSE
 * \return      E_OK           The host has successfully send the request MME.
 * \return      E_NOT_OK       Request a ethernet buffer or ethernet transmission failed.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC && ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_AmpMapCnf(
  uint8                TrcvIdx,
  uint8                ResType,
  EthTrcv_PhysAddrType DestAddr);
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE */

/* Handler */
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VHandleParamCnf
 **********************************************************************************************************************/
/*!
 * \brief       Handles received 'SLAC parameter' confirmation MME.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   IsBroadcast    Ethernet package broadcast indication
 * \param[in]   PhysAddrPtr    Source MAC address (Sender of this package)
 * \param[in]   DataPtr        Received Ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleParamCnf(
          uint8                                                            TrcvIdx,
          boolean                                                          IsBroadcast,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   PhysAddrPtr,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VHandleAttenCharInd
 **********************************************************************************************************************/
/*!
 * \brief       Handles received 'SLAC attenuation characterization' indication MME.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   IsBroadcast    Ethernet package broadcast indication
 * \param[in]   PhysAddrPtr    Source MAC address (Sender of this package)
 * \param[in]   DataPtr        Received Ethernet frame starting at the MME Version
 * \param[in]   LenByte        Payload length of the received ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleAttenCharInd(
          uint8                                                            TrcvIdx,
          boolean                                                          IsBroadcast,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   PhysAddrPtr,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr,
          uint16                                                           LenByte);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VHandleValidationCnf1
 **********************************************************************************************************************/
/*!
 * \brief       Handles received 'SLAC validation' confirmation MME, part 1.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   IsBroadcast    Ethernet package broadcast indication
 * \param[in]   PhysAddrPtr    Source MAC address (Sender of this package)
 * \param[in]   DataPtr        Received Ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 * \note        Validation is split into two Handlers this is the 1st handling phase.
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleValidationCnf1(
          uint8                                                            TrcvIdx,
          boolean                                                          IsBroadcast,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   PhysAddrPtr,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VHandleValidationCnf2
 **********************************************************************************************************************/
/*!
 * \brief       Handles received 'SLAC validation' confirmation MME, part 2.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   IsBroadcast    Ethernet package broadcast indication
 * \param[in]   PhysAddrPtr    Source MAC address (Sender of this package)
 * \param[in]   DataPtr        Received Ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 * \note        Validation is split into two Handlers this is the 2nd handling phase.
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleValidationCnf2(
          uint8                                                            TrcvIdx,
          boolean                                                          IsBroadcast,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   PhysAddrPtr,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VHandleMatchCnf
 **********************************************************************************************************************/
/*!
 * \brief       Handles received 'SLAC match' confirmation MME.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   IsBroadcast    Ethernet package broadcast indication
 * \param[in]   PhysAddrPtr    Source MAC address (Sender of this package)
 * \param[in]   DataPtr        Received Ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleMatchCnf(
          uint8                                                            TrcvIdx,
          boolean                                                          IsBroadcast,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   PhysAddrPtr,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VHandleCmSetKeyCnf
 **********************************************************************************************************************/
/*!
 * \brief       Handles received 'Set key' confirmation MME.
 * \details     This confirmation is send from the local QCA to the host to indicate the status of the network
 *              membership key and network id update.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   DataPtr        Received Ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleCmSetKeyCnf(
          uint8 TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr);

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE))
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VHandleAmpMapReq
 **********************************************************************************************************************/
/*!
 * \brief       Handles received 'SLAC amplitude map exchange' request from the EVSE
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   IsBroadcast    Ethernet package broadcast indication
 * \param[in]   PhysAddrPtr    Source MAC address (Sender of this package)
 * \param[in]   DataPtr        Received Ethernet frame starting at the MME Version
 * \param[in]   LenByte        Payload length of the received ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC && ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleAmpMapReq(
          uint8                                                            TrcvIdx,
          boolean                                                          IsBroadcast,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   PhysAddrPtr,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr,
          uint16                                                           LenByte);

#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP))
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VHandleAmpMapCnf
 **********************************************************************************************************************/
/*!
 * \brief       Handles received 'SLAC amplitude map exchange' confirmation from the EVSE
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   IsBroadcast    Ethernet package broadcast indication
 * \param[in]   PhysAddrPtr    Source MAC address (Sender of this package)
 * \param[in]   DataPtr        Received Ethernet frame starting at the MME Version
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC && ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE &&
 *              ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleAmpMapCnf(
          uint8                                                            TrcvIdx,
          boolean                                                          IsBroadcast,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   PhysAddrPtr,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr);
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE */

#if (((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP)  && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP)) || \
     ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION)))
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_VNvmReadBlock
 **********************************************************************************************************************/
/*!
 * \brief       Reads a given NvM block to a given destination address.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   BlockId        NvM block id to read
 * \param[out]  NvmReadState   Status of the read process:
 *                               ETHTRCV_30_AR7000_SLAC_READ_STATE_DONE
 *                               ETHTRCV_30_AR7000_SLAC_READ_STATE_ERROR
 *                               ETHTRCV_30_AR7000_SLAC_READ_STATE_PENDING
 * \param[out]  DstPtr         The destination address where the NvM block should be read to.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC &&
 *              (ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP || ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION)
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_VNvmReadBlock(
        uint8                                                                                            TrcvIdx,
        NvM_BlockIdType                                                                                  BlockId,
  P2VAR(EthTrcv_30_Ar7000_Slac_NvmReadStateType, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) NvmReadState,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)                                   DstPtr);
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP ||
          ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION */

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_IsKnownAddr
 **********************************************************************************************************************/
/*!
 * \brief       Compares a given MAC address with the known EVSE MAC addresses
 * \details     -
 * \param[in]   TrcvIdx                                       Zero based index of the transceiver
 * \param[in]   Addr                                          MAC address to compare
 * \return      ETHTRCV_30_AR7000_SLAC_INVALID_PROFILE_IDX    No SLAC profile with given MAC address found.
 * \return      uint16                                        SLAC profile index of matched MAC address.
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(uint16, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_IsKnownAddr(
  uint8                TrcvIdx,
  EthTrcv_PhysAddrType Addr);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSelectEvseAndValidate
 **********************************************************************************************************************/
/*!
 * \brief       Handles the verification or confirmation process during the SLAC setup.
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSelectEvseAndValidate(
  uint8 TrcvIdx);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VOrderEVSEs
 **********************************************************************************************************************/
/*!
 * \brief       Orders the found EVSEs.
 * \details     Ordering is done by the attenuation level. The lowest attenuation is the first element.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \return      E_OK           Ordering was successful
 * \return      E_NOT_OK       No direct or indirect EVSE found for ordering
 * \pre         This API has to be called inside a critical section
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VOrderEVSEs(
  uint8 TrcvIdx);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VConfirming
 **********************************************************************************************************************/
/*!
 * \brief       Specify the EVSE where the 'match request' MME should be send to.
 * \details     Based on the 'Mode' input a different EVSE will receive the 'match request' MME.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   Mode           Connecting Mode:
 *                               ETHTRCV_30_AR7000_SLAC_JOIN_LAST_APPRVD:
 *                               Joins the EVSE used during the last session.
 *                               ETHTRCV_30_AR7000_SLAC_JOIN_ALL_TOGGLES:
 *                               Joins the EVSE from which we received all toggles during the validation process.
 *                               Else [range: uint8]:
 *                               Joins a user given EVSE
 * \return      E_OK           'Match Request' MME successful send -> Joining to a EVSE initiated
 * \return      E_NOT_OK       No SLAC profile (EVSE) found to join to.
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VConfirming(
  uint8 TrcvIdx,
  uint8 Mode);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VEnterAmpMapState
 **********************************************************************************************************************/
/*!
 * \brief       Triggers the 'amplitude map exchange' process
 * \details     -
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \pre         This API has to be called inside a critical section
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VEnterAmpMapState(
  uint8 TrcvIdx);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VAssociationStatus
 **********************************************************************************************************************/
/*!
 * \brief       Informs application about SLAC status.
 * \details     Performs a SLAC protocol reset on a critical failure status.
 * \param[in]   TrcvIdx        Zero based index of the transceiver
 * \param[in]   AssocStep      Failed step
 * \param[in]   AssocStatus    Failure status
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VAssociationStatus(
  uint8                                  TrcvIdx,
  EthTrcv_30_Ar7000_Slac_AssocStepType   AssocStep,
  EthTrcv_30_Ar7000_Slac_AssocStatusType AssocStatus);

/* State Machine */
#if (((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE)) || \
     ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO)))
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_N_SetProperty
 **********************************************************************************************************************/
/*!
 * \brief       Send SET_PROPERTY MME and wait for response
 * \details     -
 * \param[in]   SetPropParams  Pointer to structure of type EthTrcv_30_Ar7000_SetPropertyParamterTyp
 * \param[in]   StepNr         Current stept number in SLAC association process
 * \return      ETHTRCV_30_AR7000_RT_OK           Setting property is done
 * \return      ETHTRCV_30_AR7000_RT_NOT_OK       Setting property failed
 * \return      ETHTRCV_30_AR7000_RT_PENDING      Setting property is pending
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC &&
 *              (ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE || ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO)
 */
ETHTRCV_30_AR7000_LOCAL FUNC (EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_N_SetProperty(
  CONSTP2CONST(EthTrcv_30_Ar7000_SetPropertyParamterType, ETHTRCV_30_AR7000_APPL_CONST, ETHTRCV_30_AR7000_APPL_CONST)
                                       SetPropParams,
  EthTrcv_30_Ar7000_Slac_AssocStepType StepNr);
#endif /* (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE)) ||
          (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO) */

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_4_ParamReq
 **********************************************************************************************************************/
/*!
 * \brief       Triggers 'SLAC parameter' request MME transmission.
 * \details     Wait for responses and send StartAttenChar indication.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_4_ParamReq(void);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_5_Sounding
 **********************************************************************************************************************/
/*!
 * \brief       Sends sounding packets.
 * \details     Send packets and wait for ATTEN_CHAR.IND, select EVSE and send Validation Request
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_5_Sounding(void);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_6_Validation1
 **********************************************************************************************************************/
/*!
 * \brief       Wait for 'validation' confirm MME from EVSE.
 * \details     Retry validation request if the current EVSE does not confirm the request within a specified time.
 *              Select the 'next' best EVSE after n-retries and start the validation request again.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_6_Validation1(void);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_7_Validation2
 **********************************************************************************************************************/
/*!
 * \brief       Perform the validation process by toggling the CP pin.
 * \details     Wait until resp. time is expired and start validation or goto State 6 (CONFIRMING)
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_7_Validation2(void);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_8_Confirming
 **********************************************************************************************************************/
/*!
 * \brief       Waits for 'SLAC match' request confirmation from EVSE.
 * \details     Resend 'SLAC match' request if timeout occurs.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_8_Confirming(void);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_9_Joining
 **********************************************************************************************************************/
/*!
 * \brief       Polls the link state.
 * \details     The link state must become active within a specified timeout else the QCA will be reseted.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_9_Joining(void);

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE))
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_10_AmpMapExchangeLocal
 **********************************************************************************************************************/
/*!
 * \brief       Sends EV 'amplitude map' request to EVSE.
 * \details     -
 * \pre         SlacMode must be ETHTRCV_30_AR7000_SLAC_START_MODE_LOCAL_AMP_MAP_EXCH
 *              (see. EthTrcv_30_Ar7000_Slac_Start()). Otherwise the function won't be called.
 * \return      E_OK           Everthing went as expected
 * \return      E_NOT_OK       Assoc status with Slac_Reset was called
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC && ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE
 */
ETHTRCV_30_AR7000_LOCAL FUNC (Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_10_AmpMapExchangeLocal(void);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_10_AmpMapExchangeRemote
 **********************************************************************************************************************/
/*!
 * \brief       Wait for QCA 'amplitude map' request MME.
 * \details     -
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC && ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE
 */
ETHTRCV_30_AR7000_LOCAL FUNC (void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_10_AmpMapExchangeRemote(void);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_11_AmpMapExchangePlcNode
 **********************************************************************************************************************/
/*!
 * \brief       Sets the remote amplitude map
 * \details     -
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC && ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE
 */
ETHTRCV_30_AR7000_LOCAL FUNC (void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_11_AmpMapExchangePlcNode(void);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_12_AmpMapEnd
 **********************************************************************************************************************/
/*!
 * \brief       Waits for the amplitude map confirmation from QCA and calls DLINK_READY
 * \details     -
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      ETHTRCV_30_AR7000_ENABLE_SLAC && ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE
 */
ETHTRCV_30_AR7000_LOCAL FUNC (void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_12_AmpMapEnd(void);
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE */

#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 **********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 **********************************************************************************************************************/
#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
/**********************************************************************************************************************
 *  MME's
 **********************************************************************************************************************/
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_ParamReq
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_ParamReq(
  uint8 TrcvIdx)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  uint8  dataIdx;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_MME_SLAC_PARAM_REQ_LEN4;

  /* #10 Request a ethernet frame buffer for the 'slac parameter' request MME frame */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
        (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= (uint16)ETHTRCV_30_AR7000_MME_SLAC_PARAM_REQ_LEN4) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros */
      EthTrcv_30_Ar7000_VFrameHeaderGeneric(
        TrcvIdx,
        (ETHTRCV_30_AR7000_MME_CM_SLAC_PARAM | ETHTRCV_30_AR7000_MME_REQ),
        &ethBuf,
        (uint16)ETHTRCV_30_AR7000_MME_SLAC_PARAM_REQ_LEN4);

      /* #40 Fill ethernet frame with 'slac parameter' request specific portion */
      /* Application type is 0x00 (PEV-EVSE association) - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      /* Security is disabled (0x00) - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      /* run identifier */
      dataIdx = 0;
      while (dataIdx < 8)
      {
        ethBuf.Buffer.U8[dataIdx + 7] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId[dataIdx];
        dataIdx++;
      }
      /* #50 Transmit ethernet frame */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      if (E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE, \
           (uint16)ETHTRCV_30_AR7000_MME_SLAC_PARAM_REQ_LEN, (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_Slac_MNBC_Addr[0])
      )
      {
#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
        ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_PARM_DISCOVERY_REQ, FALSE, NULL_PTR);
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */
        retVal = E_OK;
      }
    }

    /* #60 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0, \
                             (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_Slac_MNBC_Addr[0]);
    }
  }
  return retVal;
} /* EthTrcv_30_Ar7000_MME_Slac_ParamReq() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_StartAttenCharInd
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_StartAttenCharInd(
  uint8 TrcvIdx)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  uint8  dataIdx;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_MME_SLAC_START_ATTEN_CHAR_IND_LEN4;

  /* #10 Request a ethernet frame buffer for the 'start attenuation characteristics' indication MME frame */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
          (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= (uint16)ETHTRCV_30_AR7000_MME_SLAC_START_ATTEN_CHAR_IND_LEN4) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros */
      EthTrcv_30_Ar7000_VFrameHeaderGeneric(
        TrcvIdx,
        (ETHTRCV_30_AR7000_MME_CM_START_ATTEN_CHAR | ETHTRCV_30_AR7000_MME_IND),
        &ethBuf,
        (uint16)ETHTRCV_30_AR7000_MME_SLAC_START_ATTEN_CHAR_IND_LEN4);

      /* #40 Fill ethernet frame with 'start attenuation characteristics' indication specific portion */
      /* Application type is 0x00 (PEV-EVSE association) - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      /* Security is disabled (0x00) - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

      /* NUM_SOUNDS */
      ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_SLAC_START_ATTEN_NUM_SOUNDS] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCntSounds;

      /* Time_Out */
      ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_SLAC_START_ATTEN_TIMEOUT] = (uint8)(EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacTimeOut / 100);

      /* RESP_TYPE */
      ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_SLAC_START_ATTEN_RESP_TYPE] = ETHTRCV_30_AR7000_SLAC_RESP_TYPE;

      /* Forwarding station */
      /* 6 bytes - ignored */
#if ((defined ETHTRCV_30_AR7000_SLAC_RESP_TYPE) && (ETHTRCV_30_AR7000_SLAC_RESP_TYPE_STA == ETHTRCV_30_AR7000_SLAC_RESP_TYPE))
      /* PRQA S 0310, 3305 3 */ /* MD_EthTrcv_30_Ar7000_0310 */
      IpBase_Copy(
        (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&(ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_SLAC_START_ATTEN_FWD_STA]),
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFwdSta,
        sizeof(EthTrcv_PhysAddrType)
      );
#endif /* ETHTRCV_30_AR7000_SLAC_RESP_TYPE */

      /* Run identifier */
      dataIdx = 0;
      while (dataIdx < 8)
      {
        ethBuf.Buffer.U8[dataIdx + ETHTRCV_30_AR7000_MME_SLAC_START_ATTEN_RUNID] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId[dataIdx];
        dataIdx++;
      }

      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

      /* #50 Transmit ethernet frame */
      /* PRQA S 0311 3 */ /* MD_EthTrcv_30_Ar7000_0311 */
      if(E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE,
           (uint16)ETHTRCV_30_AR7000_MME_SLAC_START_ATTEN_CHAR_IND_LEN,
           (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_Slac_MNBC_Addr[0])
      )
      {
#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
        ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
        {
          P2VAR(EthTrcv_30_Ar7000_Slac_Msg_StartAttenCharIndType, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) SlacMsg =
            &EthTrcv_30_Ar7000_Slac_MsgBuf.SlacStartAttenCharInd;

          SlacMsg->SlacNumSounds = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCntSounds;
          SlacMsg->SlacTimeOut = (uint8)(EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacTimeOut / 100);
          SlacMsg->SlacRespType = ETHTRCV_30_AR7000_SLAC_RESP_TYPE;
          /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
          IpBase_Copy(
            (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacRunID,
            (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId,
            8
          );
          /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
          IpBase_Copy(
            (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacForwardingSta,
            (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFwdSta,
            sizeof(EthTrcv_PhysAddrType)
          );
          ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

          /* PRQA S 0310, 3305 4 */ /* MD_EthTrcv_30_Ar7000_0310 */
          ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(
            ETHTRCV_30_AR7000_SLAC_STATE_START_ATTEN_CHAR_IND,
            TRUE,
            (P2VAR(void, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))SlacMsg
          );
        }
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */

        retVal = E_OK;
      }
    }

    /* #60 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0, \
                             (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_Slac_MNBC_Addr[0]);
    }
  }
  return retVal;
} /* EthTrcv_30_Ar7000_MME_Slac_StartAttenCharInd() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_SoundInd
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_SoundInd(
    uint8 TrcvIdx)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  uint8  dataIdx;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_MME_SLAC_SOUND_IND_LEN4;

  /* #10 Request a ethernet frame buffer for the 'mnbc sound' indication MME frame */
  /* CM_MNBC_SOUND.IND is used as part of SLAC protocol to estimate the attenuation profile of a transmission from an
   * PEV at EVSEs.
   */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
          (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= (uint16)ETHTRCV_30_AR7000_MME_SLAC_SOUND_IND_LEN4) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros */
      EthTrcv_30_Ar7000_VFrameHeaderGeneric(
        TrcvIdx,
        (ETHTRCV_30_AR7000_MME_CM_MNBC_SOUND | ETHTRCV_30_AR7000_MME_IND),
        &ethBuf,
        (uint16)ETHTRCV_30_AR7000_MME_SLAC_SOUND_IND_LEN4);

      /* #40 Fill ethernet frame with 'mnbc sound' indication specific portion */
      /* Application type is 0x00 (PEV-EVSE association) - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      /* Security is disabled (0x00) - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      /* Sender ID - not used (17 bytes) */

      /* Countdown timer for number of sounds remaining */
      ethBuf.Buffer.U8[24] = (uint8)(EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCntSounds - 1u);

      /* Run identifier */
      dataIdx = 0;
      while (dataIdx < 8)
      {
        ethBuf.Buffer.U8[dataIdx + 25] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId[dataIdx];
        dataIdx++;
      }

      /* 8 Bytes reserved */

      /* Random number */
      /* PRQA S 0310 3 */ /* MD_EthTrcv_30_Ar7000_0310 */
      (void)ETHTRCV_30_AR7000_APPL_CBK_SLAC_GET_RND(
        &(ethBuf.Buffer.U8[41]),
        ETHTRCV_30_AR7000_SLAC_SOUND_RND_LEN_BYTE);

      /* #50 Transmit ethernet frame */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      if(E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE, \
          (uint16)ETHTRCV_30_AR7000_MME_SLAC_SOUND_IND_LEN, (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_Slac_MNBC_Addr[0])
      )
      {
#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
        P2VAR(EthTrcv_30_Ar7000_Slac_Msg_SoundingIndType, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) SlacMsg =
          &EthTrcv_30_Ar7000_Slac_MsgBuf.SlacSoundingInd;

        SlacMsg->SlacCnt = (uint8)(EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCntSounds - 1u);
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacRunID,
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId,
          8
        );

        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310 */
        ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_SOUNDING_IND, TRUE,
          (P2VAR(void, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))SlacMsg);
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */

        retVal = E_OK;
      }
    }

    /* #60 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0, \
                             (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&EthTrcv_30_Ar7000_Slac_MNBC_Addr[0]);
    }
  }
  return retVal;
} /* EthTrcv_30_Ar7000_MME_Slac_SoundInd() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_AttenCharResp
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_AttenCharResp(
  uint8                TrcvIdx,
  EthTrcv_PhysAddrType DestAddr,
  uint8                Result)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  uint8  dataIdx;
  EthTrcv_PhysAddrType macAddr;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_RESP_LEN4;

  /* #10 Request a ethernet frame buffer for the 'attenuation characteristics' response MME frame */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
         (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= (uint16)ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_RESP_LEN4) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros */
      EthTrcv_30_Ar7000_VFrameHeaderGeneric(
        TrcvIdx,
        (ETHTRCV_30_AR7000_MME_CM_ATTEN_CHAR | ETHTRCV_30_AR7000_MME_RSP),
        &ethBuf,
        (uint16)ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_RESP_LEN4);

      /* #40 Fill ethernet frame with 'attenuation characteristics' response specific portion */
      /* Application type is 0x00 (PEV-EVSE association) - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      /* Security is disabled (0x00) - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      /* MAC */
      EthIf_GetPhysAddr(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), macAddr);
      dataIdx = 0;
      while (dataIdx < sizeof(EthTrcv_PhysAddrType))
      {
        ethBuf.Buffer.U8[dataIdx + ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_RSP_MAC] = macAddr[dataIdx];
        dataIdx++;
      }

      /* Run identifier */
      dataIdx = 0;
      while (dataIdx < 8)
      {
        ethBuf.Buffer.U8[dataIdx + ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_RSP_RUNID] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId[dataIdx];
        dataIdx++;
      }

      /* Source ID and Response ID are always zero - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      ethBuf.Buffer.U8[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_RSP_RESULT] = Result;

      /* #50 Transmit ethernet frame */
      if(E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE, \
                 (uint16)ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_RESP_LEN, &DestAddr[0])
      )
      {
#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
        P2VAR(EthTrcv_30_Ar7000_Slac_Msg_AttenCharRspType, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) SlacMsg =
          &EthTrcv_30_Ar7000_Slac_MsgBuf.SlacAttenCharRsp;

        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacRunID,
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId,
          8
        );
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacSourceAddr,
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))macAddr,
          sizeof(EthTrcv_PhysAddrType)
        );
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Fill(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacSourceId,
          0x00,
          sizeof(SlacMsg->SlacSourceId)
        );
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Fill(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacRespId,
          0x00,
          sizeof(SlacMsg->SlacRespId)
        );
        SlacMsg->SlacResult = Result;

        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310 */
        ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_ATTEN_CHAR_RSP, TRUE,
          (P2VAR(void, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))SlacMsg);
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */

        retVal = E_OK;
      }
    }

    /* #60 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, (uint16)0, \
                           &DestAddr[0]);
    }
  }
  return retVal;
} /* EthTrcv_30_Ar7000_MME_Slac_AttenCharResp() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_ValidateReq
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_ValidateReq(
  uint8                TrcvIdx,
  uint8                Timeout,
  EthTrcv_PhysAddrType DestAddr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_MME_SLAC_VALIDATE_REQ_LEN4;

  /* #10 Request a ethernet frame buffer for the 'validate' request MME frame */
  /* CM_VALIDATE.REQ is used in PEV-EVSE association as part of out-of-band Validation. */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
        (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= (uint16)ETHTRCV_30_AR7000_MME_SLAC_VALIDATE_REQ_LEN4) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros */
      EthTrcv_30_Ar7000_VFrameHeaderGeneric(
        TrcvIdx,
        (ETHTRCV_30_AR7000_MME_CM_VALIDATE | ETHTRCV_30_AR7000_MME_REQ),
        &ethBuf,
        (uint16)ETHTRCV_30_AR7000_MME_SLAC_VALIDATE_REQ_LEN4);

      /* #40 Fill ethernet frame with 'validate' request specific portion */
      /* Signal type is 0x00 (PEV-EVSE association) - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      /* Timer - rounded up to 100 ms */
      /* 0 == 100 ms, 1 == 200 ms, ... */
      ethBuf.Buffer.U8[6] = Timeout;

      /* Result is always Ready */
      ethBuf.Buffer.U8[7] = 0x01;

      /* #50 Transmit ethernet frame */
      if (E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE, \
                                 (uint16)ETHTRCV_30_AR7000_MME_SLAC_VALIDATE_REQ_LEN, &DestAddr[0])
      )
      {
        retVal = E_OK;
      }
    }

    /* #60 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, FALSE, \
                           (uint16)0, &DestAddr[0]);
    }
  }
  return retVal;
} /* EthTrcv_30_Ar7000_MME_Slac_ValidateReq() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_MatchReq
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_MatchReq(
  uint8                TrcvIdx,
  EthTrcv_PhysAddrType DestAddr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  uint8  dataIdx;
  EthTrcv_PhysAddrType srcAddr;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_MME_SLAC_MATCH_REQ_LEN4;

  /* #10 Request a ethernet frame buffer for the 'slac match' request MME frame */
  /* It is used by the  PEV to indicate to an EVSE that it is connected to it through a charging cordset. */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
        (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= (uint16)ETHTRCV_30_AR7000_MME_SLAC_MATCH_REQ_LEN4) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros */
      EthTrcv_30_Ar7000_VFrameHeaderGeneric(
        TrcvIdx,
        (ETHTRCV_30_AR7000_MME_CM_SLAC_MATCH | ETHTRCV_30_AR7000_MME_REQ),
        &ethBuf,
        (uint16)ETHTRCV_30_AR7000_MME_SLAC_MATCH_REQ_LEN4);

      /* #40 Fill ethernet frame with 'slac match' request specific portion */
      /* Application type is 0x00 (PEV-EVSE association) - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      /* Security is disabled (0x00) - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      /* MVFLength statically set to 62 (little endian) */
      ethBuf.Buffer.U8[7] = 62u;
      ethBuf.Buffer.U8[8] = 0u;

      /* PEV ID is ignored - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      /* PEV MAC */
      dataIdx = 0;
      EthIf_GetPhysAddr(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), srcAddr);
      while (dataIdx < sizeof(EthTrcv_PhysAddrType))
      {
        ethBuf.Buffer.U8[dataIdx + 26] = srcAddr[dataIdx];
        dataIdx++;
      }

      /* EVSE ID is ignored - buffer already zero initialized by EthTrcv_30_Ar7000_VFrameHeaderGeneric */

      /* EVSE MAC */
      ethBuf.Buffer.U8[49] = DestAddr[0];
      ethBuf.Buffer.U8[50] = DestAddr[1];
      ethBuf.Buffer.U8[51] = DestAddr[2];
      ethBuf.Buffer.U8[52] = DestAddr[3];
      ethBuf.Buffer.U8[53] = DestAddr[4];
      ethBuf.Buffer.U8[54] = DestAddr[5];

      /* Run identifier */
      dataIdx = 0;
      while (dataIdx < 8)
      {
        ethBuf.Buffer.U8[dataIdx + 55] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId[dataIdx];
        dataIdx++;
      }

      /* 8 Bytes reserverd */
      dataIdx = 0;
      while (dataIdx < 8)
      {
        ethBuf.Buffer.U8[dataIdx + 63] = 0;
        dataIdx++;
      }

      /* #50 Transmit ethernet frame */
      if(E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE, \
                                (uint16)ETHTRCV_30_AR7000_MME_SLAC_MATCH_REQ_LEN, &DestAddr[0]) /* PRQA S 0311 */ /* MD_EthTrcv_30_Ar7000_0311 */
      )
      {
#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
        P2VAR(EthTrcv_30_Ar7000_Slac_Msg_MatchReqType, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) SlacMsg =
          &EthTrcv_30_Ar7000_Slac_MsgBuf.SlacMatchReq;

        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacRunID,
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId,
          8
        );
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Fill(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacPevId,
          0x00,
          sizeof(SlacMsg->SlacPevId)
        );
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
         IpBase_Copy(
           (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacPevMac,
           (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))srcAddr,
           sizeof(SlacMsg->SlacPevMac)
         );
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Fill(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacEvseId,
          0x00,
          sizeof(SlacMsg->SlacEvseId)
        );
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
       IpBase_Copy(
         (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacEvseMac,
         (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))DestAddr,
         sizeof(SlacMsg->SlacEvseMac)
       );

       /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310 */
       ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_MATCH_REQ, TRUE,
         (P2VAR(void, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))SlacMsg);
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */

        retVal = E_OK;
      }
    }

    /* #60 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, \
                            FALSE, (uint16)0, &DestAddr[0]);
    }
  }
  return retVal;
} /* EthTrcv_30_Ar7000_MME_Slac_MatchReq() */ /* PRQA S 6050 */ /* MD_MSR_STCAL */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE))
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_AmpMapReq
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_AmpMapReq(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) AmpMap,
          uint16                                                           AmpMapEntries,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DestAddr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint16 ampEntriesSize;
  uint16 dataIdx;
  uint8  ethBufIdx;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* Each map entry has 4 bits - round up uneven number of entries */
  ampEntriesSize = (AmpMapEntries + 1) / 2;
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_REQ_STATIC_LEN4 + ampEntriesSize;

  /* #10 Request a ethernet frame buffer for the 'amplitude map' request MME frame */
  /* EVSE/PEV may optionally provide PEV/EVSE with its Amplitude Map */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
        (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= ((uint16)ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_REQ_STATIC_LEN4 + (uint16)ampEntriesSize)) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros */
      EthTrcv_30_Ar7000_VFrameHeaderGeneric(
        TrcvIdx,
        (ETHTRCV_30_AR7000_MME_CM_AMP_MAP | ETHTRCV_30_AR7000_MME_REQ),
        &ethBuf,
        (uint16)ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_REQ_STATIC_LEN4);

      /* #40 Fill ethernet frame with 'amplitude map' request specific portion */
      /* Number of entries */
      ethBuf.Buffer.U8[5] = (uint8)((AmpMapEntries >> 0) & 0x00FFu);
      ethBuf.Buffer.U8[6] = (uint8)((AmpMapEntries >> 8) & 0x00FFu);

      /* Amp Map */
      dataIdx = 0;
      while (dataIdx < ampEntriesSize)
      {
        ethBuf.Buffer.U8[dataIdx + 7] = AmpMap[dataIdx];
        dataIdx++;
      }

      /* #50 Transmit ethernet frame */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      if (E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE,
         (uint16)ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_REQ_STATIC_LEN + ampEntriesSize, (P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&DestAddr[0]))
      {
        retVal = E_OK;
      }
    }

    /* #60 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, \
        FALSE, (uint16)0,(P2VAR(uint8, ETHTRCV_30_AR7000_CONST, ETHTRCV_30_AR7000_CONST))&DestAddr[0]);
    }
  }
  return retVal;
} /* EthTrcv_30_Ar7000_MME_Slac_AmpMapReq() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_AmpMapCnf
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_AmpMapCnf(
  uint8                TrcvIdx,
  uint8                ResType,
  EthTrcv_PhysAddrType DestAddr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_EthernetBufferType ethBuf;
  uint8  ethBufIdx;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ethBuf.BufLen = (uint16)ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_RESP_LEN4;

  /* #10 Request a ethernet frame buffer for the 'amplitude map' confirm MME frame */
  /* EVSE/PEV may optionally provide PEV/EVSE with its Amplitude Map */
  if (BUFREQ_OK == EthIf_ProvideTxBuffer(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(),
         (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, ETHTRCV_30_AR7000_TX_PRIO, &ethBufIdx, &(ethBuf.Buffer.Native), &(ethBuf.BufLen)))
  {
    /* #20 Verify requested buffer size */
    if (ethBuf.BufLen >= ((uint16)ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_RESP_LEN4)) /* PRQA S 3355, 3358 */ /* MD_EthTrcv_30_Ar7000_3201_3355_3358_EthBufLen */
    {
      /* #30 Fill ethernet frame buffer with MME frame header and initialize ethernet frame with zeros */
      EthTrcv_30_Ar7000_VFrameHeaderGeneric(
        TrcvIdx,
        (ETHTRCV_30_AR7000_MME_CM_AMP_MAP | ETHTRCV_30_AR7000_MME_CNF),
        &ethBuf,
        (uint16)ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_RESP_LEN4);

      /* #40 Fill ethernet frame with 'amplitude map' confirm specific portion */
      /* Status */
      ethBuf.Buffer.U8[5] = ResType;

      /* #50 Transmit ethernet frame */
      if (E_OK == EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, TRUE, \
                                 (uint16)ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_RESP_LEN, &DestAddr[0]) /* PRQA S 0311 */ /* MD_EthTrcv_30_Ar7000_0311 */
      )
      {
        retVal = E_OK;
      }
    }

    /* #60 Verify transmission and release ethernet buffer if it fails */
    if (E_NOT_OK == retVal)
    {
      /* Release buffer */
      /* PRQA S 0311 2 */ /* MD_EthTrcv_30_Ar7000_0311 */
      (void)EthIf_Transmit(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), ethBufIdx, (Eth_FrameType)ETHTRCV_30_AR7000_MTYPE, \
                            FALSE, (uint16)0, &DestAddr[0]);
    }
  }
  return retVal;
} /* EthTrcv_30_Ar7000_MME_Slac_AmpMapCnf() */
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

/**********************************************************************************************************************
 *  Handlers
 **********************************************************************************************************************/
/***********************************************************************************************************************
*  EthTrcv_30_Ar7000_Slac_VHandleParamCnf
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 3673 5 */ /* MD_EthTrcv_30_Ar7000_3673 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleParamCnf(
          uint8                                                            TrcvIdx,
          boolean                                                          IsBroadcast,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   PhysAddrPtr,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8_least physAddrIdx;
  uint8_least runIdIdx;
  uint8_least profileIdx;
  EthTrcv_30_Ar7000_Slac_AssocStatusType assocStatus = ETHTRCV_30_AR7000_SLAC_E_NO_ERROR;
  boolean isDifferent;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

  /* #10 Parse 'SLAC parameter' confirm message(s) form EVSE's
   *     Do some error checks on received message
   */
  /* check NUM M_SOUNDS */
  if ( (ETHTRCV_30_AR7000_SLAC_C_ASSOC_MNBC_MIN > DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_NUM_SOUNDS]) ||
       (ETHTRCV_30_AR7000_SLAC_C_ASSOC_MNBC_MAX < DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_NUM_SOUNDS]))
  {
    assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
  }

  /* check Time_Out */
  if ( (ETHTRCV_30_AR7000_SLAC_T_ASSOC_MNBC_TIMEOUT_MAX < DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_TIMEOUT]) ||
       (ETHTRCV_30_AR7000_SLAC_T_ASSOC_MNBC_TIMEOUT_MIN > DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_TIMEOUT]))
  {
    assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
  }

  /* check RESP_TYPE */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_RESP_TYPE_CHECK) && (STD_ON==ETHTRCV_30_AR7000_SLAC_ENABLE_RESP_TYPE_CHECK))
  if (ETHTRCV_30_AR7000_SLAC_RESP_TYPE != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_TYPE])
  {
    assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
  }

#if ((defined ETHTRCV_30_AR7000_SLAC_RESP_TYPE) && (ETHTRCV_30_AR7000_SLAC_RESP_TYPE_STA == ETHTRCV_30_AR7000_SLAC_RESP_TYPE))
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_FIELD_CHECKS) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_FIELD_CHECKS))
  /* check FORWARDING_STA */
  {
    EthTrcv_PhysAddrType OwnAddr;

    EthIf_GetPhysAddr(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), OwnAddr);
    for (physAddrIdx = 0; physAddrIdx < sizeof(EthTrcv_PhysAddrType); physAddrIdx++)
    {
      if ( OwnAddr[physAddrIdx] != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_FWD_STA + physAddrIdx] )
      {
        assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
        break;
      }
    }
  }
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_FIELD_CHECKS */
#endif /* ETHTRCV_30_AR7000_SLAC_RESP_TYPE */

#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_RESP_TYPE_CHECK */

  /* check SECURITY TYPE, we only provide 'No Security == 0' */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SECURITY_TYPE_CHECK) && (STD_ON==ETHTRCV_30_AR7000_SLAC_ENABLE_SECURITY_TYPE_CHECK))
  if (0u != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_SEC_TYPE])
  {
    assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
  }
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SECURITY_TYPE_CHECK */

  /* bytewise compare run id */
  for (runIdIdx = 0; runIdIdx < sizeof(EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId); runIdIdx++)
  {
    if ( EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId[runIdIdx] != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_RUNID + runIdIdx] )
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
      break;
    }
  }

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS) && (STD_ON==ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS))
  /* check destination addr for unicast */
  if ( TRUE == IsBroadcast )
  {
    assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(IsBroadcast); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS */

  /* check that a response from same MAC wasn't received yet */
  for (profileIdx = 0; profileIdx < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles; profileIdx++)
  {
    isDifferent = FALSE;
    physAddrIdx = sizeof(EthTrcv_PhysAddrType);
    while ( 0 < physAddrIdx)
    {
      physAddrIdx--;
      if (PhysAddrPtr[physAddrIdx] !=
          EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacEvseMacAddr[physAddrIdx])
      {
        isDifferent = TRUE;
        break;
      }
    }
    if (FALSE == isDifferent)
    {
      /* Discard message, EVSE is already known */
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
      break;
    }
  }

  /* #20 Verify that we have received a valid package. */
  if (ETHTRCV_30_AR7000_SLAC_E_NO_ERROR == assocStatus)
  {
    /* #30 Save message content for later usage. */
    ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

    /* Is needed by Slac_VSM_4_ParamReq() to iterate over all received packages. */
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacParamCnfRecvd++;

    /* use highest number of sounds and smallest timeout settings */
    if (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumSounds < DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_NUM_SOUNDS])
    {
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumSounds = DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_NUM_SOUNDS];
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCntSounds = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumSounds;
    }
    /*  The time value is in multiples of 100msec e.g. 0x1=100ms, 0x2=200ms */
    if ( EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacTimeOut < ((uint16)DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_TIMEOUT] * 100u))
    {
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacTimeOut = (uint16)DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_TIMEOUT] * 100u;
    }
#if ((defined ETHTRCV_30_AR7000_SLAC_RESP_TYPE) && (ETHTRCV_30_AR7000_SLAC_RESP_TYPE_STA == ETHTRCV_30_AR7000_SLAC_RESP_TYPE))
    /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
    IpBase_Copy(
      (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFwdSta,
      (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_FWD_STA],
      sizeof(EthTrcv_PhysAddrType)
    );
#endif /* ETHTRCV_30_AR7000_SLAC_RESP_TYPE */

    /* Save MAC address to associate profile later on */
    /* Get unicast address of EVSE */
    if (ETHTRCV_30_AR7000_SLAC_MAX_ATTEN_CHAR_PROFILES > EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles)
    {
      physAddrIdx = sizeof(EthTrcv_PhysAddrType);
      while (0 < physAddrIdx)
      {
        physAddrIdx--;
        EthTrcv_30_Ar7000_Slac_Profiles[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles].SlacEvseMacAddr[physAddrIdx] =
          PhysAddrPtr[physAddrIdx];
      }
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles++;
    }

    /* #40 If ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK is enabled
     *        Inform the application by using the callback
     */
#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
    {
      P2VAR(EthTrcv_30_Ar7000_Slac_Msg_ParmDiscoveryCnfType, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) SlacMsg =
        &EthTrcv_30_Ar7000_Slac_MsgBuf.SlacParmDiscoveryCnf;

      SlacMsg->SlacNumSounds = DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_NUM_SOUNDS];
      SlacMsg->SlacTimeOut = DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_TIMEOUT];
      SlacMsg->SlacRespType = DataPtr[ETHTRCV_30_AR7000_MME_SLAC_PARAM_CNF_TYPE];
      /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
      IpBase_Copy(
        (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacRunID,
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId,
        8
      );
      /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
      IpBase_Copy(
        (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacForwardingSta,
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFwdSta,
        sizeof(EthTrcv_PhysAddrType)
      );
      /* PRQA S 0310, 3305 4 */ /* MD_EthTrcv_30_Ar7000_0310 */
      ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(
        ETHTRCV_30_AR7000_SLAC_STATE_PARM_DISCOVERY_CNF,
        TRUE,
        (P2VAR(void, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))SlacMsg
      );
    }
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */

    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

    /* sounding packets are sent in main function */
  }
  else
  {
    /* Discard confirmation and wait for next */
    /* Solved by timeout when further confirmations are missing */
  }
} /* EthTrcv_30_Ar7000_Slac_VHandleParamCnf() */ /* PRQA S 6010, 6030 */ /* MD_MSR_STPTH, MD_MSR_STCYC */

/**********************************************************************************************************************
*  EthTrcv_30_Ar7000_Slac_VHandleAttenCharInd
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 3673 6 */ /* MD_EthTrcv_30_Ar7000_3673 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleAttenCharInd(
          uint8                                                            TrcvIdx,
          boolean                                                          IsBroadcast,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   PhysAddrPtr,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr,
          uint16                                                           LenByte)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION))
  uint16 calibratedAtten;
  sint16 calibratedVal;
  uint8 calibrationIdx;
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION */
  uint8 numGroups;
  uint16 meanAttn;
  uint16_least profileIdx;
  uint8_least physAddrIdx;
  uint8_least runIdIdx;
  uint8_least groupsIdx;
  EthTrcv_30_Ar7000_Slac_AssocStatusType assocStatus = ETHTRCV_30_AR7000_SLAC_E_NO_ERROR;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Find MAC address in SLAC profiles */
  profileIdx = EthTrcv_30_Ar7000_Slac_IsKnownAddr(TrcvIdx, PhysAddrPtr);

  /* #20 Add MAC address to a new SLAC profile if it wasn't found */
  if ( ETHTRCV_30_AR7000_SLAC_INVALID_PROFILE_IDX == profileIdx )
  {
    /* EVSE did not participate during parameter exchange phase. Create a new entry in profile array */
    /* Get unicast address of EVSE */
    if (ETHTRCV_30_AR7000_SLAC_MAX_ATTEN_CHAR_PROFILES > EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles)
    {
      physAddrIdx = sizeof(EthTrcv_PhysAddrType);
      while (0 < physAddrIdx)
      {
        physAddrIdx--;
        EthTrcv_30_Ar7000_Slac_Profiles[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles].SlacEvseMacAddr[physAddrIdx] =
          PhysAddrPtr[physAddrIdx];
      }
      profileIdx = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles++;
    }
  }

  /* #30 Verify that MAC address correspond to a valid SLAC profile */
  if ( ETHTRCV_30_AR7000_SLAC_INVALID_PROFILE_IDX != profileIdx )
  {
    /* #40 Set MAC found indicator */
    EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacAttenCharRecvd = TRUE;

    /* #50 Do some basic runtime checks */
    /* check APPLICATION_TYPE: 0x00 = PEV-EVSE Association, others are not allowed */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_APPL_TYPE_CHECK) && (STD_ON==ETHTRCV_30_AR7000_SLAC_ENABLE_APPL_TYPE_CHECK))
    if (0u != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_APPL_TYPE])
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
    }
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_APPL_TYPE_CHECK */

    /* check SECURITY_TYPE: 0x00 = No Security, we only provide this type */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SECURITY_TYPE_CHECK) && (STD_ON==ETHTRCV_30_AR7000_SLAC_ENABLE_SECURITY_TYPE_CHECK))
    if (0u != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_SEC_TYPE])
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
    }
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SECURITY_TYPE_CHECK */

    /* RunID */
    for (runIdIdx = 0; runIdIdx < sizeof(EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId); runIdIdx++)
    {
      if ( EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId[runIdIdx] != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_RUNID + runIdIdx] )
      {
        assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
        break;
      }
    }

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_NUM_SOUND_CHECK) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_NUM_SOUND_CHECK))
    /* check NumSounds */
    if ( ETHTRCV_30_AR7000_SLAC_MIN_DETECTED_NUM_SOUNDS > DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_NUM_SND])
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
    }
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_NUM_SOUND_CHECK */

    /* check numGroups */
    if ( ETHTRCV_30_AR7000_SLAC_NUM_PROFILE_GROUPS != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_NUM_GRP])
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
    }

    /* check length of message */
    if ((ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_PROFILE + ETHTRCV_30_AR7000_SLAC_NUM_PROFILE_GROUPS) > LenByte)
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
    }

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_FIELD_CHECKS) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_FIELD_CHECKS))
    /* compare MAC */
    {
      EthTrcv_PhysAddrType OwnAddr;

      EthIf_GetPhysAddr(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), OwnAddr);
      physAddrIdx = 0;
      while (physAddrIdx < sizeof(EthTrcv_PhysAddrType))
      {
        if ( OwnAddr[physAddrIdx] != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_MAC + physAddrIdx] )
        {
          assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
          break;
        }
        physAddrIdx++;
      }

    }
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_FIELD_CHECKS */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS))
    /* check destination addr for unicast */
    if (TRUE == IsBroadcast)
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
    }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(IsBroadcast); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS */

    /* #60 Verify that all runtime checks were successful */
    if (ETHTRCV_30_AR7000_SLAC_E_NO_ERROR == assocStatus)
    {
      /* #70 Calculate mean attenuation */
      numGroups = DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_NUM_GRP];
      meanAttn = 0;
      for (groupsIdx = 0; groupsIdx < numGroups; groupsIdx++)
      {
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION))
        /* Calibration */
        calibrationIdx = (uint8)(groupsIdx / ETHTRCV_30_AR7000_SLAC_GROUPS_PRO_CALIB_VALUE);
        if ((DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_PROFILE + groupsIdx] + EthTrcv_30_Ar7000_Slac_AttenCalibrationValues[calibrationIdx]) > 0)
        {
          calibratedVal = (sint16)((sint8)DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_PROFILE + groupsIdx] +
              EthTrcv_30_Ar7000_Slac_AttenCalibrationValues[calibrationIdx]);
          calibratedAtten = (uint16)calibratedVal;
        }
        else
        {
          calibratedAtten = 0;
        }

        /* Mean attenuation */
        meanAttn += calibratedAtten;
#else
        /* Mean attenuation */
        meanAttn += DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_PROFILE + groupsIdx];
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION */
      }
      /* We do want to always round up meanAttn when dividing by numGroups.
       * The interresting value range of meanAttn is 26-46 dB for the "DIRECT"/"INDIRECT"/"NOT FOUND" decision. In that
       * range we will not get a calculation error by decrementing numGroups by 1. The calculation error will start at
       * a meanAttn of 57.98 dB. This value is already outside the specified range of the connection decision. And
       * will always result in "NOT FOUND", regardless of its value is +-1 dB.
       */
      meanAttn += (numGroups - 1);
      meanAttn /= numGroups;

      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

      /* #80 If ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK is enabled
       *         Inform application by using the callback
       */
#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
      {
        P2VAR(EthTrcv_30_Ar7000_Slac_Msg_AttenCharIndType, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) SlacMsg =
          &EthTrcv_30_Ar7000_Slac_MsgBuf.SlacAttenCharInd;

        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacRunID,
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId,
          8
        );
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacSourceAddr,
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_MAC],
          sizeof(EthTrcv_PhysAddrType)
        );
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacSourceId,
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_SOURCE_ID],
          sizeof(SlacMsg->SlacSourceId)
        );
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacRespId,
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_RESP_ID],
          sizeof(SlacMsg->SlacRespId)
        );
        SlacMsg->SlacNumSounds = DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_NUM_SND];
        SlacMsg->SlacNumGroups = DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_NUM_GRP];
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacAag,
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&DataPtr[ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_IND_PROFILE],
          sizeof(SlacMsg->SlacAag)
        );

        /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310 */
        ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_ATTEN_CHAR_IND, TRUE,
          (P2VAR(void, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))SlacMsg);
      }
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */

      /* #90 Send 'attenuation characterization' response MME */
      /* PRQA S 0310 4 */ /* MD_EthTrcv_30_Ar7000_0310_1 */
      (void)EthTrcv_30_Ar7000_MME_Slac_AttenCharResp(
        TrcvIdx,
        EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacEvseMacAddr,
        ETHTRCV_30_AR7000_MME_SLAC_ATTEN_CHAR_RSP_RESULT_SUCCESS);

      /* #100 Signal VSM_5_Sounding state that attenuation characterization indication was received */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAttnCharIndRecvd++;

#if ((defined ETHTRCV_30_AR7000_SLAC_WAIT_ON_ATTEN_RESULTS) && (STD_OFF == ETHTRCV_30_AR7000_SLAC_WAIT_ON_ATTEN_RESULTS))
      if (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAttnCharIndRecvd >= EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacParamCnfRecvd)
      {
        /* do not wait for further attenuation char indications because nParamRsp == nAttnCharInd */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = 0;
      }
#endif /* ETHTRCV_30_AR7000_SLAC_WAIT_ON_ATTEN_RESULTS */

      EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacAverageAtten = (uint8)meanAttn;

      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    }
    else
    {
      /* Discard indication and wait for next */
      /* Solved by timeout when further indications are missing */
    }
  }
} /* EthTrcv_30_Ar7000_Slac_VHandleAttenCharInd() */ /* PRQA S 6010, 6030 */ /* MD_MSR_STPTH, MD_MSR_STCYC */

/**********************************************************************************************************************
*  EthTrcv_30_Ar7000_Slac_VHandleValidationCnf1
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 3673 5 */ /* MD_EthTrcv_30_Ar7000_3673 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleValidationCnf1(
          uint8                                                            TrcvIdx,
          boolean                                                          IsBroadcast,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   PhysAddrPtr,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8 physAddrIdx;
  EthTrcv_30_Ar7000_Slac_ValidationResultType validResult;
  EthTrcv_30_Ar7000_Slac_AssocStatusType assocStatus = ETHTRCV_30_AR7000_SLAC_E_NO_ERROR;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Do some basic runtime checks */
  /* compare MAC */
  physAddrIdx = sizeof(EthTrcv_PhysAddrType);
  while ( 0 < physAddrIdx)
  {
    physAddrIdx--;
    if (PhysAddrPtr[physAddrIdx] !=
       EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacEvseMacAddr[physAddrIdx])
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
      break;
    }
  }

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SIGNAL_TYPE_CHECK) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SIGNAL_TYPE_CHECK))
  /* check SignalType: only 0x00 is valid. */
  if ( 0 != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_VALID_SIGNAL_TYPE])
  {
    assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
  }
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SIGNAL_TYPE_CHECK */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS))
  /* check destination addr for unicast */
  if ( TRUE == IsBroadcast )
  {
    assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(IsBroadcast); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS */

  /* #20 Verify that all basic runtime checks are passed. */
  if (ETHTRCV_30_AR7000_SLAC_E_NO_ERROR == assocStatus)
  {
    /* #30 Verify confirmation MME result
     *      ready|not required => initiate transition to validation step2
     *      not ready          => give the validation process more time
     *                            OR
     *                         => try the next element from the SLAC profile list (if the retry counter is exceeded)
     *      failure            => Skip validation an initiate transition to confirm step
     *                            OR
     *                         => try the next element from the SLAC profile list
     *      else               => try the next element from the SLAC profile list
     */
    validResult = DataPtr[ETHTRCV_30_AR7000_MME_SLAC_VALID_RESULT];

    if ( (ETHTRCV_30_AR7000_SLAC_VALID_READY == validResult) ||
         (ETHTRCV_30_AR7000_SLAC_VALID_NOT_REQUIRED == validResult))
    {
      /* next step */
      {
        uint16 TempFlags;
        TempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
        TempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_VALIDATING2;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = TempFlags;
      }
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = 0; /* no timeout between validation phase 1 and 2 */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult = validResult;
    }
    else if ( ETHTRCV_30_AR7000_SLAC_VALID_NOT_READY == validResult )
    {
      if (0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries)
      {
        /* solved by timeout - wait further 100 ms - retry */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_VALIDATE_REQ_RETRY;
      }
      else
      {
         /* Validate next */
        EthTrcv_30_Ar7000_Slac_VAssociationStatus(
              TrcvIdx,
              ETHTRCV_30_AR7000_SLAC_STEP_6_VALIDATE_REQ,
              ETHTRCV_30_AR7000_SLAC_E_EVSE_NOT_READY);
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation++;
        /* set retries */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;
        /* start timeout */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
        EthTrcv_30_Ar7000_Slac_VSelectEvseAndValidate(TrcvIdx);
      }
    }
    else if ( ETHTRCV_30_AR7000_SLAC_VALID_FAILURE == validResult )
    {
      /* EVSE does not support validation */
      EthTrcv_30_Ar7000_Slac_VAssociationStatus(
            TrcvIdx,
            ETHTRCV_30_AR7000_SLAC_STEP_6_VALIDATE_REQ,
            ETHTRCV_30_AR7000_SLAC_E_NO_VALIDATION_SUPPORT);

      if ((EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode & ETHTRCV_30_AR7000_SLAC_START_MODE_MATCH_ON_VALID_FAILED) != 0)
      {
        /* Validation is skipped and SLAC advances to the matching step with this EVSE */
        (void)EthTrcv_30_Ar7000_Slac_VConfirming(0, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation);
        /* start timeout for confirmation */
        {
          uint16 TempFlags;
          TempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
          TempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_CONFIRMING;
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = TempFlags;
        }
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_MATCH_REREQU - 1;
      }
      else
      {
        /* Continue to next EVSE */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation++;
        /* set retries */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;
        /* start timeout */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
        EthTrcv_30_Ar7000_Slac_VSelectEvseAndValidate(TrcvIdx);
      }
    }
    else
    {
      /* not successful */
      /* Validate next */
      EthTrcv_30_Ar7000_Slac_VAssociationStatus(
            TrcvIdx,
            ETHTRCV_30_AR7000_SLAC_STEP_6_VALIDATE_REQ,
            ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE);
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation++;
      /* set retries */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;
      /* start timeout */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
      EthTrcv_30_Ar7000_Slac_VSelectEvseAndValidate(TrcvIdx);
    }
  }
  else
  {
    /* Wrong MAC address. Discard */
  }
} /* EthTrcv_30_Ar7000_Slac_VHandleValidationCnf1() */ /* PRQA S 6010, 6030, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STMIF */

/***********************************************************************************************************************
*  EthTrcv_30_Ar7000_Slac_VHandleValidationCnf2
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
/* PRQA S 3673 5 */ /* MD_EthTrcv_30_Ar7000_3673 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleValidationCnf2(
          uint8                                                            TrcvIdx,
          boolean                                                          IsBroadcast,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   PhysAddrPtr,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8 physAddrIdx;
  EthTrcv_30_Ar7000_Slac_ValidationResultType validResult;
  EthTrcv_30_Ar7000_Slac_AssocStatusType assocStatus = ETHTRCV_30_AR7000_SLAC_E_NO_ERROR;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  if ( (ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_END & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult) != 0)
  {
    /* #10 Do some basic runtime checks */
    /* Compare MACs*/
    physAddrIdx = sizeof(EthTrcv_PhysAddrType);
    while ( 0 < physAddrIdx)
    {
      physAddrIdx--;
      if (PhysAddrPtr[physAddrIdx] !=
         EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacEvseMacAddr[physAddrIdx])
      {
        assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
        break;
      }
    }

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SIGNAL_TYPE_CHECK) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SIGNAL_TYPE_CHECK))
    /* check NumSounds */
    if ( 0 != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_VALID_SIGNAL_TYPE])
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
    }
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SIGNAL_TYPE_CHECK */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS))
    /* check destination addr for unicast */
    if ( TRUE == IsBroadcast )
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
    }
#else
    ETHTRCV_30_AR7000_DUMMY_STATEMENT(IsBroadcast); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS */

    /* #20 Verify that all basic runtime checks are passed. */
    if (ETHTRCV_30_AR7000_SLAC_E_NO_ERROR == assocStatus)
    {
      /* #30 Verify received result */
      validResult = DataPtr[ETHTRCV_30_AR7000_MME_SLAC_VALID_RESULT];

      if ( ETHTRCV_30_AR7000_SLAC_VALID_SUCCESS == validResult )
      {
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacTogglesDetected = DataPtr[ETHTRCV_30_AR7000_MME_SLAC_VALID_TOGGLES];
          if ( EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurToggleNum !=
               EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacTogglesDetected)
          {
            /* not all toggles detected , validate next */
            EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult = ETHTRCV_30_AR7000_SLAC_VALID_INVALID;
            /* invalidate toggle count */
            EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacTogglesDetected = 0u;
          }
          else
          {
            /* all toggles detected - next step see EthTrcv_30_Ar7000_VSM_7_Validation2: Substate 5 */
            EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult |= ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_ANSWERED;
            /* Set timeout to zero, to trigger matching phase immediately */
            EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = 0;
          }
      }
      else
      {
        /* wrong result - validate next */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult = ETHTRCV_30_AR7000_SLAC_VALID_INVALID;
        EthTrcv_30_Ar7000_Slac_VAssociationStatus(
              TrcvIdx,
              ETHTRCV_30_AR7000_SLAC_STEP_8_VALID_FAILED,
              ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE);
      }

      /* #40 Process next SLAC profile item if the result was invalid */
      if ( ETHTRCV_30_AR7000_SLAC_VALID_INVALID == EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult)
      {
        /* validate next */
        /* increment CurProfileValidation */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation++;
        /* set retries */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;
        /* start timeout */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
        EthTrcv_30_Ar7000_Slac_VSelectEvseAndValidate(TrcvIdx);
      }
    }
    else
    {
      /* Wrong MAC address. Discard */
    }
  }
  else
  {
    /* Validation received before toggle confirmation - ignore */
  }
} /* EthTrcv_30_Ar7000_Slac_VHandleValidationCnf2() */ /* PRQA S 6010 */ /* MD_MSR_STPTH */

/**********************************************************************************************************************
*  EthTrcv_30_Ar7000_Slac_VHandleMatchCnf
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 3673 5 */ /* MD_EthTrcv_30_Ar7000_3673 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleMatchCnf(
          uint8                                                            TrcvIdx,
          boolean                                                          IsBroadcast,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   PhysAddrPtr,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8_least physAddrIdx;
  uint8_least runIdIdx;
  uint8_least nidIdx;
  uint8_least nmkIdx;
  uint8 nidMask = 0;
  EthTrcv_30_Ar7000_Slac_AssocStatusType assocStatus = ETHTRCV_30_AR7000_SLAC_E_NO_ERROR;
  Std_ReturnType resVal; /* Result value of called functions */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Do some basic runtime checks:
   *     1. Verify EVSE MAC address
   *     2. Verify RunID
   */
  /* compare EVSE MAC */
  {
    for (physAddrIdx = 0; physAddrIdx < sizeof(EthTrcv_PhysAddrType); physAddrIdx++)
    {
      if ( EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr[physAddrIdx] != PhysAddrPtr[physAddrIdx] )
      {
        assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
        break;
      }
    }
  }

  /* compare run id */
  for (runIdIdx = 0; runIdIdx < sizeof(EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId); runIdIdx++)
  {
    if ( EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId[runIdIdx] != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_MATCH_RUNID + runIdIdx] )
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
      break;
    }
  }

  /* #20 Verify that all tests passed */
  if (ETHTRCV_30_AR7000_SLAC_E_NO_ERROR == assocStatus)
  {
    /* #30 Do some more runtime tests. */
    /* check APPLICATION_TYPE. Only 0x00 is valid for PEV-EVSE Association */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_APPL_TYPE_CHECK) && (STD_ON==ETHTRCV_30_AR7000_SLAC_ENABLE_APPL_TYPE_CHECK))
    if (0u != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_MATCH_APPL_TYPE])
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
    }
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_APPL_TYPE_CHECK */

    /* check SECURITY_TYPE. We support only 0x00 - 'No Security' */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SECURITY_TYPE_CHECK) && (STD_ON==ETHTRCV_30_AR7000_SLAC_ENABLE_SECURITY_TYPE_CHECK))
    if (0u != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_MATCH_SEC_TYPE])
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
    }
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SECURITY_TYPE_CHECK */

    /* check MVFLength */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_MVF_CHECK) && (STD_ON==ETHTRCV_30_AR7000_SLAC_ENABLE_MVF_CHECK))
    if (ETHTRCV_30_AR7000_SLAC_MATCH_MVF_LEN != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_MATCH_MVF_LEN])
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
    }
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_MVF_CHECK */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS))
    /* check destination addr for unicast */
    if ( TRUE == IsBroadcast )
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
    }
#else
    ETHTRCV_30_AR7000_DUMMY_STATEMENT(IsBroadcast); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_FIELD_CHECKS) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_FIELD_CHECKS))
    /* check EV MAC */
    {
      EthTrcv_PhysAddrType OwnAddr;

      (void)EthIf_GetPhysAddr(EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx(), OwnAddr);
      for (physAddrIdx = 0; physAddrIdx < sizeof(EthTrcv_PhysAddrType); physAddrIdx++)
      {
        if ( OwnAddr[physAddrIdx] != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_MATCH_PEV_MAC + physAddrIdx] )
        {
          assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
          break;
        }
      }
    }

    /* compare EVSE MAC */
    {
      for (physAddrIdx = 0; physAddrIdx < sizeof(EthTrcv_PhysAddrType); physAddrIdx++)
      {
        if ( EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr[physAddrIdx] != DataPtr[ETHTRCV_30_AR7000_MME_SLAC_MATCH_EVSE_MAC + physAddrIdx] )
        {
          assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
          break;
        }
      }
    }
#endif

    /* #40 Verify that all tests passed */
    if (ETHTRCV_30_AR7000_SLAC_E_NO_ERROR == assocStatus)
    {
      /* #50 Extract network ID (nid) and network membership key (nmk) from received MME packet */
      for (nidIdx = 0; nidIdx < ETHTRCV_30_AR7000_NID_SIZE_BYTE; nidIdx++)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid[nidIdx] = DataPtr[ETHTRCV_30_AR7000_MME_SLAC_MATCH_NID + nidIdx];
        nidMask |= EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid[nidIdx];
      }

      for (nmkIdx = 0; nmkIdx < ETHTRCV_30_AR7000_NMK_SIZE_BYTE; nmkIdx++)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk[nmkIdx] = DataPtr[ETHTRCV_30_AR7000_MME_SLAC_MATCH_NMK + nmkIdx];
      }

      /* #60 Trigger state machine transition */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = ETHTRCV_30_AR7000_SLAC_FLAG_JOINING | ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED;

      /* #70 If ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK is enabled
       *         Inform application using the callback
       */
#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
      {
        P2VAR(EthTrcv_30_Ar7000_Slac_Msg_MatchCnfType, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) SlacMsg =
          &EthTrcv_30_Ar7000_Slac_MsgBuf.SlacMatchCnf;

        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacRunID,
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId,
          8
        );
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacPevId,
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&DataPtr[ETHTRCV_30_AR7000_MME_SLAC_MATCH_PEV_ID],
          sizeof(SlacMsg->SlacPevId)
        );
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
         IpBase_Copy(
           (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacPevMac,
           (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&DataPtr[ETHTRCV_30_AR7000_MME_SLAC_MATCH_PEV_MAC],
           sizeof(SlacMsg->SlacPevMac)
         );
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
          (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacEvseId,
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&DataPtr[ETHTRCV_30_AR7000_MME_SLAC_MATCH_EVSE_ID],
          sizeof(SlacMsg->SlacEvseId)
        );
        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
       IpBase_Copy(
         (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacEvseMac,
         (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&DataPtr[ETHTRCV_30_AR7000_MME_SLAC_MATCH_EVSE_MAC],
         sizeof(SlacMsg->SlacEvseMac)
       );
       /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
       IpBase_Copy(
         (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacNid,
         (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&DataPtr[ETHTRCV_30_AR7000_MME_SLAC_MATCH_NID],
         sizeof(SlacMsg->SlacNid)
       );
       /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
       IpBase_Copy(
         (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))SlacMsg->SlacNmk,
         (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&DataPtr[ETHTRCV_30_AR7000_MME_SLAC_MATCH_NMK],
         sizeof(SlacMsg->SlacNmk)
       );

       /* PRQA S 0310, 3305 2 */ /* MD_EthTrcv_30_Ar7000_0310 */
       ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_MATCH_CNF, TRUE,
         (P2VAR(void, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))SlacMsg);
      }
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */

      /* #80 Join the network */
      if (0 == nidMask)
      {
        /* no NID specified - use VS_SET_KEY message to set key */
#if ( ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON )
        resVal = EthTrcv_30_Ar7000_SetNmk(TrcvIdx, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk);

        if (E_OK == resVal)
        {
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_WAIT_FOR_LINK;
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_JOIN;
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = 0;

#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
          ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_SET_KEY_REQ, FALSE, NULL_PTR);
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */
        }
#else
        resVal = E_NOT_OK;
#endif /* STD_ON == ETHTRCV_30_AR7000_ENABLE_SET_NMK */
      }
      else
      {
        /* NID specified - use CM_SET_KEY message to set key */
        resVal = EthTrcv_30_Ar7000_MME_CmSetKeyReq(
          TrcvIdx,
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid,
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk);
        if (E_OK == resVal)
        {
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;

#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
          ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_SET_KEY_REQ, FALSE, NULL_PTR);
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */
        }
      }

      /* #90 Verify that 'Join the network' was successful */
      if (E_OK == resVal)
      {
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE))
        /* The AMP map response may be delivered before link is reported and SlacFlags are switched to AMP MAP state */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapRespRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE */
      }
      else
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = 0; /* resend the set key MME next main cycle */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;
      }
    }
    /* #100 Else
     *        Try to trigger 'SALC match' process again
     *        Else
     *          Reset SLAC protocol
     */
    else
    {
      if (0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries--;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;

        /* Resend confirmation request */
        (void)EthTrcv_30_Ar7000_Slac_VConfirming(TrcvIdx, EthTrcv_30_Ar7000_Slac_LastConfirmMode);
      }
      else
      {
        EthTrcv_30_Ar7000_Slac_VAssociationStatus(
          TrcvIdx,
          ETHTRCV_30_AR7000_SLAC_STEP_9_MATCH_REQ,
          ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE);
      }
    }
  }
} /* EthTrcv_30_Ar7000_Slac_VHandleMatchCnf() */ /* PRQA S 6010, 6030 */ /* MD_MSR_STPTH, MD_MSR_STCYC */

/***********************************************************************************************************************
*  EthTrcv_30_Ar7000_Slac_VHandleCmSetKeyCnf
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleCmSetKeyCnf(
          uint8                                                            TrcvIdx,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr)
{
  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Verify that 'set key' request MME was succesful */
  if (ETHTRCV_30_AR7000_MME_SLAC_CM_SET_KEY_RESULT_SUCCESS == DataPtr[ETHTRCV_30_AR7000_MME_SLAC_CM_SET_KEY_RESULT])
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_WAIT_FOR_LINK;
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_JOIN;
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = 0;

#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
    ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_SET_KEY_CNF, FALSE, NULL_PTR);
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */
  }
  /* #20 Else
   *       Try to resend the 'set key' request MME.
   */
  else
  {
    if (0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries)
    {
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries--;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;

      /* Resend set key request */
      /* NID specified - use CM_SET_KEY message to set key */
      (void)EthTrcv_30_Ar7000_MME_CmSetKeyReq(
        TrcvIdx,
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid,
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk);

#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
        ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_SET_KEY_REQ, FALSE, NULL_PTR);
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */
    }
    else
    {
      EthTrcv_30_Ar7000_Slac_VAssociationStatus(
        TrcvIdx,
        ETHTRCV_30_AR7000_SLAC_STEP_10_SET_KEY_REQ,
        ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE);
    }
  }

} /* EthTrcv_30_Ar7000_Slac_VHandleCmSetKeyCnf() */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE))
#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP))
/***********************************************************************************************************************
*  EthTrcv_30_Ar7000_Slac_VHandleAmpMapCnf
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 3673 5 */ /* MD_EthTrcv_30_Ar7000_3673 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleAmpMapCnf(
          uint8                                                            TrcvIdx,
          boolean                                                          IsBroadcast,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   PhysAddrPtr,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8 physAddrIdx;
  boolean isPhysAddrValid = TRUE;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Do some basic runtime checks */
  /* compare MAC */
  physAddrIdx = sizeof(Eth_PhysAddrType);
  while ( 0 < physAddrIdx)
  {
    physAddrIdx--;
    if (PhysAddrPtr[physAddrIdx] !=
       EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacEvseMacAddr[physAddrIdx])
    {
      isPhysAddrValid = FALSE;
      break;
    }
  }

  /* check destination addr */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS))
  if ( TRUE == IsBroadcast )
  {
    isPhysAddrValid = FALSE;
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(IsBroadcast); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS */

  /* #20 Verify that all checks are passed */
  if (TRUE == isPhysAddrValid)
  {
    /* #30 Verify that amplitude map was successful set EVSE-side */
    if (0u == DataPtr[ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_RESULT])
    {
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_LOCAL;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapLocalRspRcvd = TRUE;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = 0u;
    }
    /* #40 Else
     *        Retry sending amplitude map to EVSE
     */
    else
    {
      if (0u < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries--;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;

        /* Resend amp map request */
        (void)EthTrcv_30_Ar7000_MME_Slac_AmpMapReq(
          TrcvIdx,
          EthTrcv_30_Ar7000_Slac_AmpMap,
          ETHTRCV_30_AR7000_SLAC_AMP_MAP_ENTRIES,
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr);
      }
      else
      {
        EthTrcv_30_Ar7000_Slac_VAssociationStatus(
          TrcvIdx,
          ETHTRCV_30_AR7000_SLAC_STEP_12_AMP_MAP_REQ,
          ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE);
      }
    }
  }
  /* #50 Else
   *       Verify that the confirmation is from the local QCA. If not ignore message
   */
  else
  {
    if (   (0 == DataPtr[ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_RESULT])
        && ((ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_SET & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) == ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_SET) )
    {
      /* Assume that the confirmation is from the local QCA and set respective flag */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapQcaRspRcvd = TRUE;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = 0;
    }
  }
} /* EthTrcv_30_Ar7000_Slac_VHandleAmpMapCnf() */
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

/***********************************************************************************************************************
*  EthTrcv_30_Ar7000_Slac_VHandleAmpMapReq
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 3673 6 */ /* MD_EthTrcv_30_Ar7000_3673 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VHandleAmpMapReq(
          uint8                                                            TrcvIdx,
          boolean                                                          IsBroadcast,
    P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   PhysAddrPtr,
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) DataPtr,
          uint16                                                           LenByte)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  P2CONST(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) dataPtrAmpMap;
  uint16 ampMapEntries;
  uint16 dataIdx = 0;
  uint8 physAddrIdx;
  EthTrcv_30_Ar7000_Slac_AssocStatusType assocStatus = ETHTRCV_30_AR7000_SLAC_E_NO_ERROR;
  boolean isPhysAddrValid = TRUE;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Do some basic runtime checks */
  /* compare MAC */
  physAddrIdx = sizeof(EthTrcv_PhysAddrType);
  while ( 0 < physAddrIdx)
  {
    physAddrIdx--;
    if (PhysAddrPtr[physAddrIdx] !=
       EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacEvseMacAddr[physAddrIdx])
    {
      isPhysAddrValid = FALSE;
      break;
    }
  }

  /* check destination addr */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS))
  if ( TRUE == IsBroadcast )
  {
    assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(IsBroadcast); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS */

  /* #20 Verify that the source MAC address from this ethernet frame equals to the connected EVSE MAC address. */
  if (TRUE == isPhysAddrValid)
  {
    /* #30 Do some basic runtime checks with the amplitude map data */
    /* set pointer to entry count parameter */
    /* PRQA S 0310, 3305 1 */ /* MD_EthTrcv_30_Ar7000_3305 */
    dataPtrAmpMap = &DataPtr[ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_ENTRIES];
    /* count of entries */
    ampMapEntries = ETHTRCV_30_AR7000_U8PTRDATA_TO_VAL16(dataPtrAmpMap, 0);

    /* check AMLEN field */
    if (ETHTRCV_30_AR7000_SLAC_AMP_MAP_ENTRIES != ampMapEntries)
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
    }

    /* check length of message. Amlen / (4 bit/group) + 1/2 (roundup) + two byte AMLEN */
    if (   ( (ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_DATA + ((ampMapEntries + 1u) / 2u)) > LenByte )
        || ( ((ampMapEntries + 1u) / 2u) > ((uint16)sizeof(EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurAmpMap)) ) )
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE;
    }

    /* #40 Verify that the amplitude map data is correct */
    if (ETHTRCV_30_AR7000_SLAC_E_NO_ERROR == assocStatus)
    {
      /* #50 Save received 'amplitude map' data */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapRxEntries = ampMapEntries;

      /* set data pointer to the first amp map data */
      dataPtrAmpMap = &DataPtr[ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_DATA];

      while (((ETHTRCV_30_AR7000_SLAC_AMP_MAP_ENTRIES + 1) / 2) > dataIdx)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurAmpMap[dataIdx] = dataPtrAmpMap[dataIdx];
        dataIdx++;
      }

      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapRemoteReqRcvd = TRUE;

      /* #60 Inform EVSE that 'amplitude map' request was successful
       *       Inform Application that 'amplitude map' exchange was successful
       */
      (void)EthTrcv_30_Ar7000_MME_Slac_AmpMapCnf(
              TrcvIdx,
              ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_RESULT_SUCCESS,
              PhysAddrPtr);

      EthTrcv_30_Ar7000_Slac_VAssociationStatus(
        TrcvIdx,
        ETHTRCV_30_AR7000_SLAC_STEP_13_REMOTE_AMP_MAP,
        ETHTRCV_30_AR7000_SLAC_E_REMOTE_AMP_MAP_RCVD);
    }
    /* #70 Else
     *        Inform EVSE that 'amplitude map' request failed.
     *        Verify that we can wait for another request
     *        Else
     *          Inform application that 'amplitude map' exchange is failed -> reset SLAC protocol.
     */
    else
    {
      (void)EthTrcv_30_Ar7000_MME_Slac_AmpMapCnf(
              TrcvIdx,
              ETHTRCV_30_AR7000_MME_SLAC_AMP_MAP_RESULT_FAILURE,
              PhysAddrPtr);

      if (0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapRespRetries)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapRespRetries--;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
      }
      else
      {
        EthTrcv_30_Ar7000_Slac_VAssociationStatus(
          TrcvIdx,
          ETHTRCV_30_AR7000_SLAC_STEP_13_REMOTE_AMP_MAP,
          ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE);
      }
    }
  }
} /* EthTrcv_30_Ar7000_Slac_VHandleAmpMapReq */ /* PRQA S 6010 */ /* MD_MSR_STPTH */

#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE */

/**********************************************************************************************************************
 *  Others
 **********************************************************************************************************************/
/***********************************************************************************************************************
*  EthTrcv_30_Ar7000_Slac_IsKnownAddr
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
/* PRQA S 3673 3 */ /* MD_EthTrcv_30_Ar7000_3673 */
ETHTRCV_30_AR7000_LOCAL FUNC(uint16, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_IsKnownAddr(
  uint8                TrcvIdx,
  EthTrcv_PhysAddrType Addr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint16 profileIdx = (uint16)ETHTRCV_30_AR7000_SLAC_INVALID_PROFILE_IDX;
  uint8 physAddrIdx;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

  /* #10 Iterate through all known SLAC profiles and compare MAC address */
  physAddrIdx = 0;
  while (physAddrIdx < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles)
  {
    boolean isMacMatch = TRUE;
    uint8 macIdx = sizeof(EthTrcv_PhysAddrType);
    /* Check MAC address */
    while (0 < macIdx)
    {
      macIdx--;
      if (EthTrcv_30_Ar7000_Slac_Profiles[physAddrIdx].SlacEvseMacAddr[macIdx] != Addr[macIdx])
      {
        isMacMatch = FALSE;
        break;
      }
    }

    if (TRUE == isMacMatch)
    {
      profileIdx = physAddrIdx;
      break;
    }

    physAddrIdx++;
  }

  return profileIdx;
} /* EthTrcv_30_Ar7000_Slac_IsKnownAddr() */

/**********************************************************************************************************************
*  EthTrcv_30_Ar7000_Slac_VSelectEvseAndValidate
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSelectEvseAndValidate(
  uint8 TrcvIdx)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  uint16 tempFlags;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Verify that the current processing SLAC profile is valid */
  if (   (ETHTRCV_30_AR7000_SLAC_MAX_ATTEN_CHAR_PROFILES <= EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation)
      || (NULL_PTR == EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]) )
  {
    /* No more stations */
    EthTrcv_30_Ar7000_Slac_VAssociationStatus(
      TrcvIdx,
      ETHTRCV_30_AR7000_SLAC_STEP_8_VALID_FAILED,
      ETHTRCV_30_AR7000_SLAC_E_VALIDATION_NO_SUCCESS);
  }
  /* #20 Else-If Verify that the current processing SLAC profile is a 'direct' EVSE */
  else if (ETHTRCV_30_AR7000_SLAC_EVSE_FOUND == EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacDiscoveryStatus)
  {
    /* #30 Verify if the SLAC profile item needs a validation
     *      Trigger state transition to state 'validation' or 'confirmation'.
     */
    if ( (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode & ETHTRCV_30_AR7000_SLAC_START_MODE_VALID_ANYTIME) == ETHTRCV_30_AR7000_SLAC_START_MODE_VALID_ANYTIME )
    {
      /* Validate */
      EthTrcv_30_Ar7000_Slac_VAssociationStatus(
        TrcvIdx,
        ETHTRCV_30_AR7000_SLAC_STEP_6_VALIDATE_REQ,
        ETHTRCV_30_AR7000_SLAC_E_PREPARE_VALIDATION);

      (void)EthTrcv_30_Ar7000_MME_Slac_ValidateReq(
        TrcvIdx,
        0,
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacEvseMacAddr);

      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
      tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_VALIDATING1;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    }
    else
    {
      /* do not validate */
      if ( ( (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode & ETHTRCV_30_AR7000_SLAC_START_MODE_SKIP_VALID) != 0 ) &&
          ( (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_CON_PARAMS_VALID) != 0 ) )
      {
        if (ETHTRCV_30_AR7000_SLAC_INVALID_PROFILE_IDX !=
          EthTrcv_30_Ar7000_Slac_IsKnownAddr(TrcvIdx, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr))
        {
          /* Send confirmation to the EVSE of the last session */
          (void)EthTrcv_30_Ar7000_Slac_VConfirming(TrcvIdx, ETHTRCV_30_AR7000_SLAC_JOIN_LAST_APPRVD);
        }
        else
        {
          /* send to the EVSE with the best fitting attenuation profile */
          (void)EthTrcv_30_Ar7000_Slac_VConfirming(TrcvIdx, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation);
        }
      }
      else
      {
         (void)EthTrcv_30_Ar7000_Slac_VConfirming(TrcvIdx, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation);
      }
      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
      tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_CONFIRMING;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

      /* overwrite retries and timeout for matching phase */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_MATCH_REREQU - 1;
    }
  }
  /* #40 Else-If Verify that the current processing SLAC profile is a 'indirect' EVSE */
  else if ( ETHTRCV_30_AR7000_SLAC_EVSE_POTENTIALLY_FOUND ==
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacDiscoveryStatus )
  {
    /* #50 Verify if the SLAC profile item needs a validation
     *      Trigger state transition to state 'validation' or 'confirmation'.
     */
    /* Validation depending on validation mode */
    if ( ( (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode & ETHTRCV_30_AR7000_SLAC_START_MODE_SKIP_VALID) != 0 ) &&
        ( (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_CON_PARAMS_VALID) != 0 ) )
    {
      if (ETHTRCV_30_AR7000_SLAC_INVALID_PROFILE_IDX !=
        EthTrcv_30_Ar7000_Slac_IsKnownAddr(TrcvIdx, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr))
      {
        /* Send confirmation to the EVSE of the last session */
        (void)EthTrcv_30_Ar7000_Slac_VConfirming(TrcvIdx, ETHTRCV_30_AR7000_SLAC_JOIN_LAST_APPRVD);

        ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
        tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
        tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_CONFIRMING;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

        /* overwrite retries and timeout for matching phase */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_MATCH_REREQU - 1;
      }
      else
      {
        /* Validate */
        EthTrcv_30_Ar7000_Slac_VAssociationStatus(
          TrcvIdx,
          ETHTRCV_30_AR7000_SLAC_STEP_6_VALIDATE_REQ,
          ETHTRCV_30_AR7000_SLAC_E_PREPARE_VALIDATION);

        (void)EthTrcv_30_Ar7000_MME_Slac_ValidateReq(
          TrcvIdx,
          0,
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacEvseMacAddr);
        ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
        tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
        tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_VALIDATING1;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      }
    }
    else if ( (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode & ETHTRCV_30_AR7000_SLAC_START_MODE_VALID_WHEN_POT_FOUND) != 0 )
    {
      /* Validate */
      EthTrcv_30_Ar7000_Slac_VAssociationStatus(
        TrcvIdx,
        ETHTRCV_30_AR7000_SLAC_STEP_6_VALIDATE_REQ,
        ETHTRCV_30_AR7000_SLAC_E_PREPARE_VALIDATION);

      (void)EthTrcv_30_Ar7000_MME_Slac_ValidateReq(
        TrcvIdx,
        0,
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacEvseMacAddr);

      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
      tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_VALIDATING1;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    }
    else
    {
      /* Confirming */
      (void)EthTrcv_30_Ar7000_Slac_VConfirming(TrcvIdx, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation);

      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
      tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_CONFIRMING;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

      /* overwrite retries and timeout for matching phase */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_MATCH_REREQU - 1;
    }
  }
  /* #60 Else The discovery status of the current processing SLAC profile wasn't found -> this is not possible */
  else
  {
    /* not possible */
  }
} /* EthTrcv_30_Ar7000_Slac_VSelectEvseAndValidate() */ /* PRQA S 6080 */ /* MD_MSR_STMIF */

/***********************************************************************************************************************
*  EthTrcv_30_Ar7000_Slac_VOrderEVSEs
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VOrderEVSEs(
  uint8 TrcvIdx)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  P2VAR(EthTrcv_30_Ar7000_Slac_AttenCharProfileType, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT) tempProfile;
  uint8_least profileIdx;
  uint8 numValidProfiles;
  EthTrcv_30_Ar7000_Slac_AssocStatusType assocStatus = ETHTRCV_30_AR7000_SLAC_E_NO_ERROR;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  boolean orderComplete;
  boolean hasDirectEVSE = FALSE;
  boolean hasIndirectEVSE = FALSE;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Iterate over all SLAC profiles.
   *       Classify each profile according to their attenuation level. There are 2 types: direct, indirect EVSE.
   */
  numValidProfiles = 0;
  for (profileIdx = 0; profileIdx < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles; profileIdx++)
  {
    /* classification - only done in first loop of bubble sort algorithm */
#if ((defined ETHTRCV_30_AR7000_SLAC_C_ASSOC_SIGNALATTN_MIN) && (0 == ETHTRCV_30_AR7000_SLAC_C_ASSOC_SIGNALATTN_MIN))
    /* Avoid compiler warning. Do not check an unsigned integer against zero */
    if ( (TRUE == EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacAttenCharRecvd) &&
         (ETHTRCV_30_AR7000_SLAC_C_ASSOC_SIGNALATTN_DIRECT >= EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacAverageAtten)
       )
#else
    if ( (TRUE == EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacAttenCharRecvd) &&
         (ETHTRCV_30_AR7000_SLAC_C_ASSOC_SIGNALATTN_MIN <= EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacAverageAtten) &&
         (ETHTRCV_30_AR7000_SLAC_C_ASSOC_SIGNALATTN_DIRECT >= EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacAverageAtten)
       )
#endif /* ETHTRCV_30_AR7000_SLAC_C_ASSOC_SIGNALATTN_MIN */
    {
      if (FALSE == hasDirectEVSE)
      {
        hasDirectEVSE = TRUE;
      }
      else
      {
        /* multiple direct */
        if ((EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode & ETHTRCV_30_AR7000_SLAC_START_MODE_NEVER_VALIDATE) == 0)
        {
          /* When multiple direct occurs a validation in all cases is necessary -> overwrite start mode */
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode |= ETHTRCV_30_AR7000_SLAC_START_MODE_VALID_ANYTIME;
        }
        assocStatus = ETHTRCV_30_AR7000_SLAC_E_MULTIPLE_DIRECT;
      }
     EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacDiscoveryStatus = ETHTRCV_30_AR7000_SLAC_EVSE_FOUND;
     EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[numValidProfiles] = &EthTrcv_30_Ar7000_Slac_Profiles[profileIdx];
     numValidProfiles++;
    }
    else if ( (TRUE == EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacAttenCharRecvd) &&
              (ETHTRCV_30_AR7000_SLAC_C_ASSOC_SIGNALATTN_DIRECT < EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacAverageAtten) &&
              (ETHTRCV_30_AR7000_SLAC_C_ASSOC_SIGNALATTN_INDIRECT >= EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacAverageAtten)
            )
    {
      if ( FALSE == hasIndirectEVSE )
      {
        hasIndirectEVSE = TRUE;
      }
      else
      {
        if ((EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode & ETHTRCV_30_AR7000_SLAC_START_MODE_NEVER_VALIDATE) == 0)
        {
          /* When multiple EVSEs are potentionally found, the start mode 0x00 (No validation) is not practicable */
          /* overwrite start mode */
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode |= ETHTRCV_30_AR7000_SLAC_START_MODE_VALID_WHEN_POT_FOUND;
        }
      }
      EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacDiscoveryStatus = ETHTRCV_30_AR7000_SLAC_EVSE_POTENTIALLY_FOUND;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[numValidProfiles] = &EthTrcv_30_Ar7000_Slac_Profiles[profileIdx];
      numValidProfiles++;
    }
    else
    {
      /* Profile not valid */
    }
  }

  /* #20 Validate the profile classification */
  if ( FALSE == hasDirectEVSE)
  {
    if ( FALSE == hasIndirectEVSE )
    {
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_NO_DIRECT_OR_INDIRECT;
      /* E_NOT_OK */
    }
    else
    {
      /* no direct */
      assocStatus = ETHTRCV_30_AR7000_SLAC_E_NO_DIRECT;
      retVal = E_OK;
    }
  }
  else
  {
    retVal = E_OK;
  }

  /* #30 Sort all profiles according to there attenuation*/
  if (1 < numValidProfiles)
  {
    do
    {
      orderComplete = TRUE;
      for (profileIdx = 0u; profileIdx < ((uint8_least)(numValidProfiles - 1)); profileIdx++)
      {
        if ( EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[profileIdx]->SlacAverageAtten >
              EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[profileIdx + 1]->SlacAverageAtten)
        {
          /* swap */
          tempProfile = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[profileIdx];
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[profileIdx] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[profileIdx + 1];
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[profileIdx + 1] = tempProfile;
          orderComplete = FALSE;
        }
      }
    } while ( FALSE == orderComplete );
  }

  /* #40 Verify that we found at least on direct or indirect EVSE.
   *     Else
   *       Order EVSE failed -> reset SLAC protocol
   */
  if (ETHTRCV_30_AR7000_SLAC_E_NO_ERROR != assocStatus)
  {
    EthTrcv_30_Ar7000_Slac_VAssociationStatus(
      TrcvIdx,
      ETHTRCV_30_AR7000_SLAC_STEP_5_SELECT_EVSE,
      assocStatus);
  }
  else
  {
    /* nothing to do */
  }

  return retVal;
} /* EthTrcv_30_Ar7000_Slac_VOrderEVSEs() */ /* PRQA S 6010, 6030, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STMIF */

/***********************************************************************************************************************
*  EthTrcv_30_Ar7000_Slac_VConfirming
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VConfirming(
  uint8 TrcvIdx,
  uint8 Mode)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8_least profileIdx;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  EthTrcv_30_Ar7000_Slac_LastConfirmMode = Mode;

  /* #10 Send 'match request' MME to a given EVSE.
   *     The EVSE where the request is send to is determined by the 'Mode'
   */
  if ( ETHTRCV_30_AR7000_SLAC_JOIN_LAST_APPRVD == Mode )
  {
    /* Caller is responsible to check that connection parameters are valid */
    retVal = EthTrcv_30_Ar7000_MME_Slac_MatchReq(TrcvIdx, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr);
  }
  else if ( ETHTRCV_30_AR7000_SLAC_JOIN_ALL_TOGGLES == Mode )
  {
    for (profileIdx = 0; profileIdx < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles; profileIdx++)
    {
      if ( EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurToggleNum == EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[profileIdx]->SlacTogglesDetected)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr[0] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[profileIdx]->SlacEvseMacAddr[0];
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr[1] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[profileIdx]->SlacEvseMacAddr[1];
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr[2] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[profileIdx]->SlacEvseMacAddr[2];
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr[3] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[profileIdx]->SlacEvseMacAddr[3];
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr[4] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[profileIdx]->SlacEvseMacAddr[4];
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr[5] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[profileIdx]->SlacEvseMacAddr[5];

        retVal = EthTrcv_30_Ar7000_MME_Slac_MatchReq(TrcvIdx, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr);

        break;
      }
    }
    if ( EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles == profileIdx )
    {
      /* no station found */
      /* should not happen at this point - avoided by EthTrcv_30_Ar7000_VHandleValidationCnf2 */
      EthTrcv_30_Ar7000_Slac_VAssociationStatus(
        TrcvIdx,
        ETHTRCV_30_AR7000_SLAC_STEP_8_VALID_FAILED,
        ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE);
    }
  }
  else if ( Mode < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles )
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr[0] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[Mode]->SlacEvseMacAddr[0];
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr[1] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[Mode]->SlacEvseMacAddr[1];
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr[2] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[Mode]->SlacEvseMacAddr[2];
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr[3] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[Mode]->SlacEvseMacAddr[3];
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr[4] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[Mode]->SlacEvseMacAddr[4];
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr[5] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[Mode]->SlacEvseMacAddr[5];

    retVal = EthTrcv_30_Ar7000_MME_Slac_MatchReq(TrcvIdx, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr);
  }
  else
  {
    retVal = E_NOT_OK;
  }

  return retVal;
} /* EthTrcv_30_Ar7000_Slac_VConfirming() */

/***********************************************************************************************************************
*  EthTrcv_30_Ar7000_Slac_VEnterAmpMapState
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VEnterAmpMapState(
  uint8 TrcvIdx)
{
  /* #10 If ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE is enabled
   *       Trigger state transition to 'amplitude exchange'
   *     Else
   *       Mark current SLAC profile as active and connected to a EVSE
   */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE))
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  /* link active - valid connection parameters*/
  uint16 tempFlags;
  tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
  tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_START;
  EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

  /* Start timeout */

  /* Send first amp map request after some delay. This is done because the amplitude map exchange phase is
   * triggered on both stations after a power line link is established. Since both stations do most likely not
   * detect the power line link at the exact same time, the counterpart station may not be ready if the request
   * is sent immediately, because it hasn't detected the link yet. */
#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP))
# if ((defined ETHTRCV_30_AR7000_SLAC_RETAIN_LOCAL_AMP_MAP) && (STD_OFF == ETHTRCV_30_AR7000_SLAC_RETAIN_LOCAL_AMP_MAP))
  if ((EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode & ETHTRCV_30_AR7000_SLAC_START_MODE_LOCAL_AMP_MAP_EXCH) != 0)
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_AMP_MAP_EXCH_DELAY;
  }
  else
# endif /* (STD_OFF == ETHTRCV_30_AR7000_SLAC_RETAIN_LOCAL_AMP_MAP) */
#endif /* (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) */
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = 0;
    /* In case AmpMap shall not be sent, mark as already sent */
    if ((EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode & ETHTRCV_30_AR7000_SLAC_START_MODE_AMP_MAP_TRIG_BY_EVSE) == 0)
    {
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_LOCAL;
    }
  }
  /* Since the own request is sent after ETHTRCV_30_AR7000_SLAC_AMP_MAP_EXCH_DELAY, the T_AMP_MAP_EXCHANGE
   * is also extended by ETHTRCV_30_AR7000_SLAC_AMP_MAP_EXCH_DELAY */
  EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapTimeout = ETHTRCV_30_AR7000_SLAC_T_AMP_MAP_EXCHANGE + ETHTRCV_30_AR7000_SLAC_AMP_MAP_EXCH_DELAY;
  EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU;
  EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapRespRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;

#else
  /* link active - valid connection parameters*/
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  {
    uint16 tempFlags;
    tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
    tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_CON_PARAMS_VALID;
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
  }

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

  EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacDiscoveryStatus =
    ETHTRCV_30_AR7000_SLAC_EVSE_CONNECTED;
  (void)ETHTRCV_30_AR7000_APPL_CBK_SLAC_DLINK_READY(ETHTRCV_LINK_STATE_ACTIVE,
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid);

#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
    ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_COMPLETE, FALSE, NULL_PTR);
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE */
} /* EthTrcv_30_Ar7000_Slac_VEnterAmpMapState() */

/***********************************************************************************************************************
*  EthTrcv_30_Ar7000_Slac_VAssociationStatus
**********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VAssociationStatus(
  uint8                                  TrcvIdx,
  EthTrcv_30_Ar7000_Slac_AssocStepType   AssocStep,
  EthTrcv_30_Ar7000_Slac_AssocStatusType AssocStatus)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8 mergedStatus;
#if ( (defined ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATUS_ERROR_FLAG) && (ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATUS_ERROR_FLAG == STD_ON) )
  boolean errorDetected;
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATUS_ERROR_FLAG */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Verify that association step and status is not critical
   *       do nothing
   *     Else
   *       Reset SLAC protocol
   */
  if (   (   (ETHTRCV_30_AR7000_SLAC_STEP_5_SELECT_EVSE == AssocStep)
          && (ETHTRCV_30_AR7000_SLAC_E_NO_DIRECT_OR_INDIRECT != AssocStatus))
      || (ETHTRCV_30_AR7000_SLAC_STEP_6_VALIDATE_REQ == AssocStep)
      || (   (ETHTRCV_30_AR7000_SLAC_STEP_8_VALID_FAILED == AssocStep)
          && (ETHTRCV_30_AR7000_SLAC_E_VALIDATION_NO_SUCCESS != AssocStatus))
      || (ETHTRCV_30_AR7000_SLAC_E_REMOTE_AMP_MAP_RCVD == AssocStatus)
     )
  {
    /* do not reset */
#if ( (defined ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATUS_ERROR_FLAG) && (ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATUS_ERROR_FLAG == STD_ON) )
    errorDetected = FALSE;
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATUS_ERROR_FLAG */
  }
  else
  {
#if ( (defined ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATUS_ERROR_FLAG) && (ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATUS_ERROR_FLAG == STD_ON) )
    errorDetected = TRUE;
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATUS_ERROR_FLAG */
    (void)EthTrcv_30_Ar7000_Slac_Reset(TrcvIdx);
  }

  mergedStatus = (uint8)( ((uint8)(ETHTRCV_30_AR7000_SLAC_E_ASSOC_STATUS_MASK & AssocStatus) ) |
                          ((uint8)(( (uint8)(ETHTRCV_30_AR7000_SLAC_E_ASSOC_STATUS_MASK & AssocStep) ) << ETHTRCV_30_AR7000_SLAC_E_ASSOC_STEP_SHIFT)) );

  /* #20 Inform application */
  /* association status */
#if ( (defined ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATUS_ERROR_FLAG) && (ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATUS_ERROR_FLAG == STD_ON) )
  /* In case a SCC module version is used that does not support the errorDetected parameter, disabled the paraemter in the
   * configuration tool at:
   * /MICROSAR/EthTrcv_Ar7000/EthTrcvConfigSet/EthTrcvSlacConfig/EthTrcvSlacCallbacks/EthTrcvAssocStatusErrorFlag
   */
  (void)ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATUS(mergedStatus, errorDetected);
#else
  (void)ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATUS(mergedStatus);
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATUS_ERROR_FLAG */
} /* EthTrcv_30_Ar7000_Slac_VAssociationStatus() */

#if (((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP)  && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP)) || \
     ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION)))
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_VNvmReadBlock
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_VNvmReadBlock(
        uint8                                                                                            TrcvIdx,
        NvM_BlockIdType                                                                                  BlockId,
  P2VAR(EthTrcv_30_Ar7000_Slac_NvmReadStateType, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) NvmReadState,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)                                   DstPtr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  Std_ReturnType resVal; /* Result value of called functions */
  uint8 nvMRetVal;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

  /* #10 Verify that NvM block read-state is ready for reading
   *       Read NvM block
   *       Verify that triggering read NvM block was successful
   *          Set NvM block read-state to pending
   */
  if ( ETHTRCV_30_AR7000_SLAC_READ_STATE_DATA == *NvmReadState )
  {
    (void)NvM_GetErrorStatus(BlockId, &nvMRetVal);

    if ( NVM_REQ_PENDING != nvMRetVal)
    {
      resVal = NvM_ReadBlock(BlockId, DstPtr);
      if ( E_OK == resVal)
      {
        *NvmReadState = ETHTRCV_30_AR7000_SLAC_READ_STATE_PENDING;
      }
      else
      {
        *NvmReadState = ETHTRCV_30_AR7000_SLAC_READ_STATE_ERROR;
      }
    }
  }

  /* #20 Verify that NvM block read-state is pending
   *       Request NvM read status
   *       Set NvM block read-state to indicate: DONE, ERROR or PENDING
   */
  if ( ETHTRCV_30_AR7000_SLAC_READ_STATE_PENDING == *NvmReadState )
  {
    (void)NvM_GetErrorStatus(BlockId, &nvMRetVal);
    if ((NVM_REQ_OK == nvMRetVal) || (NVM_REQ_INTEGRITY_FAILED == nvMRetVal) || (NVM_REQ_NV_INVALIDATED == nvMRetVal) )
    {
      /* Read complete, RAM or ROM loaded */
      *NvmReadState = ETHTRCV_30_AR7000_SLAC_READ_STATE_DONE;
    }
    else if ( NVM_REQ_PENDING != nvMRetVal )
    {
      /* error */
      *NvmReadState = ETHTRCV_30_AR7000_SLAC_READ_STATE_ERROR;
    }
    else
    {
      *NvmReadState = ETHTRCV_30_AR7000_SLAC_READ_STATE_PENDING;
    }
  }
} /* EthTrcv_30_Ar7000_VNvmReadBlock() */
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP ||
          ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION */

/**********************************************************************************************************************
 *  State Machine
 **********************************************************************************************************************/
#if (((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE)) || \
     ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO)))
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_N_SetProperty
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC (EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_N_SetProperty(
    CONSTP2CONST(EthTrcv_30_Ar7000_SetPropertyParamterType, ETHTRCV_30_AR7000_APPL_CONST, ETHTRCV_30_AR7000_APPL_CONST)
                                         SetPropParams,
    EthTrcv_30_Ar7000_Slac_AssocStepType StepNr)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_SetPropType mStatus;
  EthTrcv_30_Ar7000_ReturnType retVal = ETHTRCV_30_AR7000_RT_NOT_OK; /* Return value of this function */
  EthTrcv_30_Ar7000_ReturnType resVal; /* Result value of called functions */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Trigger a 'set property' process with the given properties OR poll the process status if it is already
   *     triggert.
   */
  resVal = EthTrcv_30_Ar7000_SetProperty(0, SetPropParams, &mStatus);
  /* #20 Verify that 'set property' has finished successful.
   *       Verify that the QCA has set the given properties.
   *       Else
   *         Verify that the current state can be executed again
   *         Else
   *           Current state execution was not successful -> reset SLAC protocol
   */
  if (ETHTRCV_30_AR7000_RT_OK == resVal)
  {
    if (ETHTRCV_30_AR7000_SETPROP_SUCCESS == mStatus)
    {
      retVal = ETHTRCV_30_AR7000_RT_OK;
    }
    else
    {
      if ( 0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries--;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
      }
      else
      {
        /* not successful */
        EthTrcv_30_Ar7000_Slac_VAssociationStatus(
          0,
          StepNr,
          ETHTRCV_30_AR7000_SLAC_E_INVALID_RESPONSE);
      }
    }
  }
  /* #30 Else 'set property' needs some time or failed.
   *       Verify that the process time for the current state is not expired
   *       Else
   *         Verify that the current state can be executed again
   *         Else
   *           Current state execution was not successful  -> reset SLAC protocol
   */
  else
  {
    if ( ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME <= EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout)
    {
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout -= ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME;
      /* In case the timer is still running, return the return value of EthTrcv_30_Ar7000_SetProperty() */
      retVal = resVal;
    }
    else
    {
      if ( 0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries--;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
      }
      else
      {
        /* not successful */
        EthTrcv_30_Ar7000_Slac_VAssociationStatus(
          0,
          StepNr,
          ETHTRCV_30_AR7000_SLAC_E_TIMEOUT);
      }
    }
  }

  return retVal;
} /* EthTrcv_30_Ar7000_Slac_VSM_N_SetProperty() */
#endif /* (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE) ||
          (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO) */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_4_ParamReq
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC (void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_4_ParamReq(void)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  uint16 tempFlags;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Verify that the execution time of the 'SLAC parameter request' state is not expired. */
  if ( ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME <= EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout)
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout -= ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME;
  }
  /* #20 Else the state execution time ends*/
  else
  {
    /* #30 Verify that we received a 'SLAC parameter' confirmation MME from at least one EVSE */
    if ( 0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacParamCnfRecvd )
    {

      /* #40 Send Start Attenuation packages */
      if (0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCntStartAtten)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMainFctCnt++;
        /* PRQA S 3355, 3358 2 */ /* MD_EthTrcv_30_Ar7000_3355 */
    #if (1 == ETHTRCV_30_AR7000_SLAC_T_BATCH_INTERVAL)
        /* avoid compiler warning when define is one */
    #else
        if ((EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMainFctCnt % ETHTRCV_30_AR7000_SLAC_T_BATCH_INTERVAL) == 0)
    #endif /* ETHTRCV_30_AR7000_SLAC_T_BATCH_INTERVAL */
        {
          if (E_OK == EthTrcv_30_Ar7000_MME_Slac_StartAttenCharInd(0))
          {
            EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCntStartAtten--;
          }
        }
      }
      /* #50 Trigger State5 transition */
      if (0 == EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCntStartAtten)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMainFctCnt = 0;
        /* start sounding in next main cycle */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_SOUNDING;
        /* Retry counter is used for CM_ATTEN_CHAR.RSP retry during sounding phase */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_ATTEN_CHAR_RSP_REREQU;

        ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
        tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
        tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_SOUNDING;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      }
    }
    /* #60 Else we don't receive a 'SLAC parameter' confirmation MME within the specified time */
    else
    {
      /* #70 Try to resend 'SLAC parameter' request MME
       *       Else
       *         'SLAC parameter' request failed -> reset SLAC protocol.
       */
      if ( 0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries--;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;

        (void)EthTrcv_30_Ar7000_MME_Slac_ParamReq(0);
      }
      else
      {
        /* not successful */
        EthTrcv_30_Ar7000_Slac_VAssociationStatus(
          0,
          ETHTRCV_30_AR7000_SLAC_STEP_3_PARAM_REQ,
          ETHTRCV_30_AR7000_SLAC_E_TIMEOUT);
      }
    }
  }
} /* EthTrcv_30_Ar7000_Slac_VSM_4_ParamReq() */ /* PRQA S 6080 */ /* MD_MSR_STMIF */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_5_Sounding
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC (void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_5_Sounding(void)
{
  /* #10 Verify that we have a valid number of sounds received in the previous step
   *       Send sounding packet every T_BATCH_INTERVAL
   */
  if (0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCntSounds)
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMainFctCnt++;
    /* PRQA S 3355, 3358 2 */ /* MD_EthTrcv_30_Ar7000_3355 */
#if (1 == ETHTRCV_30_AR7000_SLAC_T_BATCH_INTERVAL)
    /* avoid compiler warning when define is one */
#else
    if ((EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMainFctCnt % ETHTRCV_30_AR7000_SLAC_T_BATCH_INTERVAL) == 0)
#endif /* ETHTRCV_30_AR7000_SLAC_T_BATCH_INTERVAL */
    {
      if (E_OK == EthTrcv_30_Ar7000_MME_Slac_SoundInd(0))
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCntSounds--;
      }
    }
  }

  /* #20 Verify that the execution time of the 'SLAC sounding' state is not expired. */
  if ( ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME <= EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout)
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout -= ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME;
  }
  /* #30 Else state is finished (timeout expired) */
  else
  {
    /* #40 Verify all sound packages are send successful
     *        Wait till 'attenuation characterisation' indication MME is received from at least one EVSE.
     *        Trigger State6 transition
     *      Else
     *        State 'sounding' failed -> reset SLAC protocol
     */
    if (0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCntSounds)
    {
      /* not successful - Could not send all sounding packets */
      EthTrcv_30_Ar7000_Slac_VAssociationStatus(
        0,
        ETHTRCV_30_AR7000_SLAC_STEP_4_SOUNDING,
        ETHTRCV_30_AR7000_SLAC_E_TIMEOUT);
    }
    else
    {
      /* next step - Select EVSE and validate */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMainFctCnt = 0;
      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      if (0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAttnCharIndRecvd)
      {
        /* TT_EV_atten_results timeout expired -> immediately remove state flag to avoid handling of consecutive CM_ATTEN_CHAR.INDs */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags &= (uint16)(~(uint32)ETHTRCV_30_AR7000_SLAC_FLAG_SOUNDING);
        /* Order EVSEs */
        if (E_OK == EthTrcv_30_Ar7000_Slac_VOrderEVSEs(0))
        {
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;
          /* start timeout */
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
          EthTrcv_30_Ar7000_Slac_VSelectEvseAndValidate(0);
        }
        else
        {
          /* not possible. State machine gets reset on E_NOT_OK */
        }
      }
      else
      {
        EthTrcv_30_Ar7000_Slac_VAssociationStatus(
        0,
        ETHTRCV_30_AR7000_SLAC_STEP_4_SOUNDING,
        ETHTRCV_30_AR7000_SLAC_E_TIMEOUT);
      }
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    }
  }
} /* EthTrcv_30_Ar7000_Slac_VSM_5_Sounding() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_6_Validation1
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC (void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_6_Validation1(void)
{
  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

  /* #10 Verify that 'validate' confirmation MME is received within the timeout. */
  if ( ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME <= EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout)
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout -= ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME;
  }

  /* #20 Else-If The timer is expired
   *       Verify that we can resend the validation request
   */
  else if ( 0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries)
  {
    /* reduce retries */
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries--;
    /* start timeout */
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
    EthTrcv_30_Ar7000_Slac_VSelectEvseAndValidate(0);
  }
  /* #30 Else
   *       Validation was not successful for the current processing validation element.
   *       Inform the application.
   *       Try the next element from the sorted SLAC profiles list.
   */
  else
  {
    /* not successful */
    /* Validate next */
    EthTrcv_30_Ar7000_Slac_VAssociationStatus(
          0,
          ETHTRCV_30_AR7000_SLAC_STEP_6_VALIDATE_REQ,
          ETHTRCV_30_AR7000_SLAC_E_TIMEOUT);
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation++;
    /* set retries */
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;
    /* start timeout */
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
    EthTrcv_30_Ar7000_Slac_VSelectEvseAndValidate(0);
  }

  ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
} /* EthTrcv_30_Ar7000_Slac_VSM_6_Validation1() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_7_Validation2
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC (void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_7_Validation2(void)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint16 toggleTimeout;
  uint16 tempFlags;

  /* When a timeout is active, first handle the timeout. */
  if ( ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME <= EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout)
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout -= ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME;
  }

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * -------> | Substate 1 | VALIDATION BEGIN                           |
   * -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * -------> | Action     | Send validation request                    |
   * -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   */
  /* #10 Verify that the validation process (IS0-15118 9.4) has not been started yet */
  else if ((ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_BEGIN & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult) == 0)
  {
    /* #20 Verify that validation is needed by EVSE or forced to use it */
    if (   ((ETHTRCV_30_AR7000_SLAC_START_MODE_FORCE_VALID & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode) != 0)
        || (ETHTRCV_30_AR7000_SLAC_VALID_READY == EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult) )
    {
      /* #30 Retrieve toggle number from application callback
       *       Verify that the number is valid (>0).
       *       Calculate toggle timeout
       *       Send 'validate' request MME to EVSE
       */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurToggleNum = (uint8)ETHTRCV_30_AR7000_APPL_CBK_SLAC_GET_VALIDATE_TOGGLES();

      if (0 == EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurToggleNum)
      {
        /* not valid - call DET and reset */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
        (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_SLAC_MAINFUNCTION, ETHTRCV_30_AR7000_SLAC_E_INV_PARAMETER);
#endif
        (void)EthTrcv_30_Ar7000_Slac_Reset(0);
      }
      else
      {
        toggleTimeout = (uint16)ETHTRCV_30_AR7000_SLAC_CALC_TOGGLE_TIMEOUT(EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurToggleNum);
        if ((1 & toggleTimeout) == 1)
        {
          /* round up */
          toggleTimeout++;
        }
        /* toggle time given in multiple of 50 ms. Validate request field is in multiples of 100 ms whereas 0 equals 100ms,
         * 1 equals 200ms, ... */
        toggleTimeout = ( toggleTimeout >> 1) - 1;
        (void)EthTrcv_30_Ar7000_MME_Slac_ValidateReq(
          0,
          (uint8)toggleTimeout,
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacEvseMacAddr);

        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult |= ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_BEGIN;
        /* Goto Substate 2. Toggle request in next main cycle */

        /* if second validation confirmation is missing do not retry to validate the same EVSE again */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = 0;
      }
    }
    /* #40 Else-If Verify that validation is not required by EVSE */
    else if  (ETHTRCV_30_AR7000_SLAC_VALID_NOT_REQUIRED == EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult)
    {
      /* #50 Trigger 'confirmation' request sending
       *       Trigger state transition to confirmation
       */
      /* confirm */
      if ( (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_CON_PARAMS_VALID) != 0 )
      {
         (void)EthTrcv_30_Ar7000_Slac_VConfirming(0, ETHTRCV_30_AR7000_SLAC_JOIN_LAST_APPRVD);
      }
      else
      {
         (void)EthTrcv_30_Ar7000_Slac_VConfirming(
           0,
           EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation );
      }

      /* start timeout */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_MATCH_REREQU - 1;

      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
      tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_CONFIRMING;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    }
    else
    {
      /* state already handled by EthTrcv_30_Ar7000_Slac_VHandleValidationCnf1 */
    }
  }

  /* -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * -------> | Substate 2 | VALIDATION REQUESTED                       |
   * -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * -------> | Action     | Call application toggle function           |
   * -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   */
  /* #60 Start toggling */
  else if ((ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_REQUESTED & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult) == 0)
  {
    /* #70 Inform the application to start toggling */
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult |= ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_REQUESTED;
    (void)ETHTRCV_30_AR7000_APPL_CBK_SLAC_TOGGLE_REQ(EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurToggleNum);

    /* #80 Set timeout */
    /* CALC_TOGGLE_TIMEOUT returns in unit of 50 ms */
    toggleTimeout = ETHTRCV_30_AR7000_SLAC_CALC_TOGGLE_TIMEOUT(EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurToggleNum);
    if ((1 & toggleTimeout) == 1)
    {
      /* round up , because timeout field in validation request is rounded up, too */
      toggleTimeout++;
    }

    /* Timeout assembly: Toggle timeout + buffer (ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE) */
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = (50 * toggleTimeout) + (uint16)ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
    /* timeout equals zero is checked upwards */
  }

  /* -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * -------> | Substate 3 | VALIDATION END                             |
   * -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * -------> | Action     | Application returned, wait for ValidCnf    |
   * -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   */
  /* #90 Verify that the application had called the ToggleConfirmation within the previous set timeout */
  else if ((ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_END & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult) == 0)
  {
    /* The application have to call ToggleConfirmation after it finished toggling otherwise a timeout occur */
    EthTrcv_30_Ar7000_Slac_VAssociationStatus(
      0,
      ETHTRCV_30_AR7000_SLAC_STEP_7_EV_NOT_READY,
      ETHTRCV_30_AR7000_SLAC_E_TIMEOUT);
  }

  /* -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * -------> | Substate 4 | VALIDATION NOT ANSWERED                    |
   * -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * -------> | Action     | ValidCnf not received, goto Substate 2     |
   * -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   */
  /* #100 'Validate' confirmation MME received but with invalid result */
  else if ((ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_ANSWERED & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult) == 0)
  {
    ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    /* no confirmation to second validation request received - continue to next EVSE */
    EthTrcv_30_Ar7000_Slac_VAssociationStatus(
      0,
      ETHTRCV_30_AR7000_SLAC_STEP_8_VALID_FAILED,
      ETHTRCV_30_AR7000_SLAC_E_TIMEOUT);
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation++;
    /* set retries */
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;
    /* start timeout */
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
    EthTrcv_30_Ar7000_Slac_VSelectEvseAndValidate(0);
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  }

  /* -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * -------> | Substate 5 | VALIDATION ANSWERED                        |
   * -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * -------> | Action     | ValidCnf received, goto State 6 (CONFIRM)  |
   * -------> +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   */
  /* #110 'Validate' confirmation MME received with valid result
   *     Trigger sending 'match' request MME
   *     Trigger state transition to confirm state
   */
  else if ((ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_ANSWERED & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult) != 0)
  {
    /* find EVSE that detected all toggles and join */
    (void)EthTrcv_30_Ar7000_Slac_VConfirming(0, ETHTRCV_30_AR7000_SLAC_JOIN_ALL_TOGGLES);
    /* start timeout for confirmation */
    ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
    tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_CONFIRMING;
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_MATCH_REREQU - 1;
  }
  else
  {
    /* not possible - end state is either Substate 4 or Substate 5 */
  }
} /* EthTrcv_30_Ar7000_Slac_VSM_7_Validation2() */ /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_8_Confirming
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC (void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_8_Confirming(void)
{
  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Wait till timeout expired or 'match' confirm MME is received. */
  if ( ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME <= EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout)
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout -= ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME;
  }
  else
  {
    /* timed out - resend request */
    if ( 0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries)
    {
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries--;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;

      /* Resend confirmation request */
      (void)EthTrcv_30_Ar7000_Slac_VConfirming(0, EthTrcv_30_Ar7000_Slac_LastConfirmMode);
    }
    else
    {
      /* not successful */
      EthTrcv_30_Ar7000_Slac_VAssociationStatus(
        0,
        ETHTRCV_30_AR7000_SLAC_STEP_9_MATCH_REQ,
        ETHTRCV_30_AR7000_SLAC_E_TIMEOUT);
    }
  }
} /* EthTrcv_30_Ar7000_Slac_VSM_8_Confirming() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_9_Joining
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC (void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_9_Joining(void)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8 nidIdx = 0;
  uint8 nidMask = 0;
#if ( ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON )
  Std_ReturnType resVal; /* Result value of called functions */
#endif /* STD_ON == ETHTRCV_30_AR7000_ENABLE_SET_NMK */

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Poll the powerline communication link status
   *       If the link state is becomes active -> trigger state transition (see Slac_TrcvLinkStateChgCbk())
   *     If link state came not active within the specified timeout -> reset SLAC protocol
   *
   */
  if ( ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME <= EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout)
  {
    /* Periodically trigger link status. The link status in EthTrcv_30_Ar7000_MainFunction is usually triggered very slow.
     * Amplitude map exchange is started on both sides with gaining a power line link. Therefore the link indication
     * is important and must be very exact during this phase */
    if ((ETHTRCV_30_AR7000_SLAC_FAST_LINK_POLL_PERIOD % EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout) == 0)
    {
      (void)EthTrcv_30_Ar7000_MME_GetLinkStatus(0);
    }

    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout -= ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME;
  }
  else if ( (0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries) &&
        ((ETHTRCV_30_AR7000_SLAC_FLAG_WAIT_FOR_LINK & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) == 0))
  {
    while (ETHTRCV_30_AR7000_NID_SIZE_BYTE > nidIdx)
    {
      nidMask |= EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid[nidIdx];
      nidIdx++;
    }

    /* Resend set key request */
    if (0 == nidMask)
    {
      /* no NID specified - use VS_SET_KEY message to set key */
#if ( ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON )
      resVal = EthTrcv_30_Ar7000_SetNmk(0, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk);

      if (E_OK != resVal)
#endif /* STD_ON == ETHTRCV_30_AR7000_ENABLE_SET_NMK */
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries--;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = 0; /* resend the set key MME next main cycle */
      }
#if ( ETHTRCV_30_AR7000_ENABLE_SET_NMK == STD_ON )
      else
      {
        /* start timeout */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_JOIN;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = 0;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_WAIT_FOR_LINK;

#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
        ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_SET_KEY_REQ, FALSE, NULL_PTR);
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */
      }
#endif /* STD_ON == ETHTRCV_30_AR7000_ENABLE_SET_NMK */
    }
    else
    {
      /* NID specified - use CM_SET_KEY message to set key */
      (void)EthTrcv_30_Ar7000_MME_CmSetKeyReq(
        0,
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid,
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk);
      /* start timeout */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries--;

#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
      ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_SET_KEY_REQ, FALSE, NULL_PTR);
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */
    }
  }
  else if ((ETHTRCV_30_AR7000_SLAC_FLAG_WAIT_FOR_LINK & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0)
  {
    /* not successful */
    EthTrcv_30_Ar7000_Slac_VAssociationStatus(
      0,
      ETHTRCV_30_AR7000_SLAC_STEP_11_NO_LINK,
      ETHTRCV_30_AR7000_SLAC_E_TIMEOUT);
  }
  else
  {
    /* not successful */
    EthTrcv_30_Ar7000_Slac_VAssociationStatus(
      0,
      ETHTRCV_30_AR7000_SLAC_STEP_10_SET_KEY_REQ,
      ETHTRCV_30_AR7000_SLAC_E_TIMEOUT);
  }
} /* EthTrcv_30_Ar7000_Slac_VSM_9_Joining() */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE))
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_10_AmpMapExchangeLocal
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC (Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_10_AmpMapExchangeLocal(void)
{
  Std_ReturnType retVal = E_OK;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 +---------------------------------------------+------------+-------------------------------------------------+
   *     |                   Config                    |   On/Off   |                                 Behavior        |
   *     +---------------------------------------------+------------+-------------------------------------------------+
   *     | ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP && | ON         | Wait till EVSE 'amplitude map' request ends and |
   *     | ETHTRCV_30_AR7000_SLAC_RETAIN_LOCAL_AMP_MAP | ON         | send own 'amplitude map' request to install the |
   *     |                                             |            | amplitude map data to the EVSE                  |
   *     +---------------------------------------------+------------+-------------------------------------------------+
   *     | ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP && | ON         | Do nothing. This is just a dummy function       |
   *     | ETHTRCV_30_AR7000_SLAC_RETAIN_LOCAL_AMP_MAP | OFF        |                                                 |
   *     +---------------------------------------------+------------+-------------------------------------------------+
   *     | ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP && | OFF        | Wait for remote amplitude                       |
   *     | ETHTRCV_30_AR7000_SLAC_RETAIN_LOCAL_AMP_MAP | Don't Cate |                                                 |
   *     +---------------------------------------------+------------+-------------------------------------------------+
   */

#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP))

#if ((defined ETHTRCV_30_AR7000_SLAC_RETAIN_LOCAL_AMP_MAP) && (STD_OFF == ETHTRCV_30_AR7000_SLAC_RETAIN_LOCAL_AMP_MAP))
  if ( ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME <= EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout)
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout -= ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME;
  }
  else if ( (0 < EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries) &&
          ((ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_LOCAL & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) == 0))
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries--;
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;

    /* Resend amp map request */
    (void)EthTrcv_30_Ar7000_MME_Slac_AmpMapReq(
      0,
      EthTrcv_30_Ar7000_Slac_AmpMap,
      ETHTRCV_30_AR7000_SLAC_AMP_MAP_ENTRIES,
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacPhysAddr);
  }
  else if ((ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_LOCAL & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) == 0)
  {
    /* not successful */
    EthTrcv_30_Ar7000_Slac_VAssociationStatus(
      0,
      ETHTRCV_30_AR7000_SLAC_STEP_12_AMP_MAP_REQ,
      ETHTRCV_30_AR7000_SLAC_E_TIMEOUT);
    retVal = E_NOT_OK;
  }
  else
  {
    /* not relevant */
  }
#endif /* ETHTRCV_30_AR7000_SLAC_RETAIN_LOCAL_AMP_MAP */

#else
  /* Wait for remote AMP Map */
  EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_LOCAL;
  /* simulate reception of local amp map response */
  EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapQcaRspRcvd = TRUE;
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

  return retVal;
} /* EthTrcv_30_Ar7000_Slac_VSM_10_AmpMapExchangeLocal() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_10_AmpMapExchangeRemote
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC (void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_10_AmpMapExchangeRemote(void)
{
  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Verify that EVSE sends 'amplitude map' requests  within the timeout (see Slac_VHandleAmpMapReq()).
   *     Else
   *       EVSE didn't request our 'amplitude map' data.
   *       Prevent out if time EVSE 'amplitude map' data requests
   *       Trigger state transition to 'amplitude map 2'
   */
  if ( ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME <= EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapTimeout)
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapTimeout -= ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME;
  }
  else
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_REMOTE;
  }
} /* EthTrcv_30_Ar7000_Slac_VSM_10_AmpMapExchangeRemote() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_11_AmpMapExchangePlcNode
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC (void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_11_AmpMapExchangePlcNode(void)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint16 tempFlags;
  Std_ReturnType resVal; /* Result value of called functions */

#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP))
  uint16 ampMapIdx;
  uint8 lowerCarrierLocal;
  uint8 upperCarrierLocal;
  uint8 lowerCarrierRemote;
  uint8 upperCarrierRemote;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Verify that amplitude map exchange was self initiated and confirmation received from EVSE */
  if (TRUE == EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapLocalRspRcvd)
  {
    /* #20 Verify that 'amplitude map' exchange was initiated by EVSE */
    if (TRUE  == EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapRemoteReqRcvd)
    {
     /* #30 Merge local amplitude map into the received remote map by selecting the maximum value for each carrier */
      ampMapIdx = 0;
      while (((ETHTRCV_30_AR7000_SLAC_AMP_MAP_ENTRIES + 1) / 2) > ampMapIdx)
      {

        lowerCarrierLocal  = (uint8)((uint32)EthTrcv_30_Ar7000_Slac_AmpMap[ampMapIdx] & 0x0F);
        upperCarrierLocal  = (uint8)((uint32)EthTrcv_30_Ar7000_Slac_AmpMap[ampMapIdx] & 0xF0);

        lowerCarrierRemote = (uint8)((uint32)EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurAmpMap[ampMapIdx] & 0x0F);
        upperCarrierRemote = (uint8)((uint32)EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurAmpMap[ampMapIdx] & 0xF0);

        if (lowerCarrierLocal > lowerCarrierRemote)
        {
          /* local carrier is maximum */
          /* Delete carrier in SlacCurAmpMap */
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurAmpMap[ampMapIdx] &= (uint8)(0xF0u);
          /* Store local carrier in SlacCurAmpMap */
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurAmpMap[ampMapIdx] |= (uint8)(EthTrcv_30_Ar7000_Slac_AmpMap[ampMapIdx] & 0x0Fu);
        }
        if (upperCarrierLocal > upperCarrierRemote)
        {
          /* local carrier is maximum */
          /* Delete carrier in SlacCurAmpMap */
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurAmpMap[ampMapIdx] &= (uint8)(0x0Fu);
          /* Store local carrier in SlacCurAmpMap */
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurAmpMap[ampMapIdx] |= (uint8)(EthTrcv_30_Ar7000_Slac_AmpMap[ampMapIdx] & 0xF0u);
        }

        ampMapIdx++;
      }

      /* #40 Update QCA amplitude map data with merged data */
      resVal = EthTrcv_30_Ar7000_MME_Slac_AmpMapReq(
        0,
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurAmpMap,
        ETHTRCV_30_AR7000_SLAC_AMP_MAP_ENTRIES,
        EthTrcv_30_Ar7000_AtherosODA);
    }
    /* #50 Else We use our local amplitude map -> no merge needed */
    else
    {
      /* #60 Update QCA amplitude data use local amplitude map */
      resVal =  EthTrcv_30_Ar7000_MME_Slac_AmpMapReq(
        0,
        EthTrcv_30_Ar7000_Slac_AmpMap,
        ETHTRCV_30_AR7000_SLAC_AMP_MAP_ENTRIES,
        EthTrcv_30_Ar7000_AtherosODA);
    }

    /* #70 Verify that triggering update was successful
     *        Trigger state transition to state 'amplitude map3'
     */
    if (E_OK == resVal)
    {
      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
      tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_SET;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = 0;
    }
  }
  else
#endif /* (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) */

  /* #80 Else-If
   *       Verify amplitude map exchange was initiated by EVSE (not self initiated) by 'amplitude map' request */
  if (TRUE == EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapRemoteReqRcvd)
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_JOIN;
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = 0;

    /* #90 Update QCA amplitude map using the remote amplitude map - no merge into a local amplitude map */
   resVal =  EthTrcv_30_Ar7000_MME_Slac_AmpMapReq(
      0,
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurAmpMap,
      ETHTRCV_30_AR7000_SLAC_AMP_MAP_ENTRIES,
      EthTrcv_30_Ar7000_AtherosODA);

    if (E_OK == resVal)
    {
      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
      tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_SET;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = 0;
    }
  }
  /* #100 Else
   *       Amplitude map exchange was not self initiated and no request from EVSE was received */
  else
  {
    /* #110 Inform application that connection parameters are valid  */
    ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
    tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_CON_PARAMS_VALID;
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacDiscoveryStatus =
      ETHTRCV_30_AR7000_SLAC_EVSE_CONNECTED;
    (void)ETHTRCV_30_AR7000_APPL_CBK_SLAC_DLINK_READY(ETHTRCV_LINK_STATE_ACTIVE,
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid);

#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
    ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_COMPLETE, FALSE, NULL_PTR);
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */
  }
} /* EthTrcv_30_Ar7000_Slac_VSM_11_AmpMapExchangePlcNode() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_VSM_12_AmpMapEnd
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
ETHTRCV_30_AR7000_LOCAL FUNC (void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_VSM_12_AmpMapEnd(void)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  uint16 tempFlags;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* #10 Wait for the amplitude map confirmation of local QCA and inform application by calling DLINK_READY */
  if ( ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME <= EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout)
  {
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout -= ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME;
  }
  else
  {
    if (TRUE == EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapQcaRspRcvd)
    {
      /* link active - valid connection parameters*/
      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
      tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_CON_PARAMS_VALID;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation]->SlacDiscoveryStatus =
        ETHTRCV_30_AR7000_SLAC_EVSE_CONNECTED;
      (void)ETHTRCV_30_AR7000_APPL_CBK_SLAC_DLINK_READY(ETHTRCV_LINK_STATE_ACTIVE,
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid);

#if (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK)
      ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE(ETHTRCV_30_AR7000_SLAC_STATE_COMPLETE, FALSE, NULL_PTR);
#endif /* STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK */
    }
    else
    {
      /* not successful */
      EthTrcv_30_Ar7000_Slac_VAssociationStatus(
        0,
        ETHTRCV_30_AR7000_SLAC_STEP_14_NO_LINK_WITH_AMP_MAP,
        ETHTRCV_30_AR7000_SLAC_E_TIMEOUT);
    }
  }
} /* EthTrcv_30_Ar7000_Slac_VSM_12_AmpMapEnd() */
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE */

#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 **********************************************************************************************************************/
#if (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC)
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_Init
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_Init(void)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthTrcv_30_Ar7000_State < ETHTRCV_STATE_INIT)
  {
    errorId = ETHTRCV_30_AR7000_SLAC_E_NOT_INITIALIZED;
  }
  else
#endif
  {
    /* #20 Initialize SLAC management structure */
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED; /* Flags */
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode  = 0x00; /* Mode */

    /* #30 Trigger NvM read on next schedule of Slac_MainFunction() */
#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP))
    EthTrcv_30_Ar7000_Slac_AmpMapNvmState = ETHTRCV_30_AR7000_SLAC_READ_STATE_DATA;
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION))
    EthTrcv_30_Ar7000_Slac_CalibBlockNvmState = ETHTRCV_30_AR7000_SLAC_READ_STATE_DATA;
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION*/

    EthTrcv_30_Ar7000_Slac_LeaveScStateRetVal = ETHTRCV_30_AR7000_RT_OK;
    EthTrcv_30_Ar7000_Slac_LeaveScStateCnt = 0;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_SLAC_INIT, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif
} /* EthTrcv_30_Ar7000_Slac_Init() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_Start
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_Start(
    uint8 TrcvIdx,
    EthTrcv_30_Ar7000_Slac_StartModeType StartMode)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  uint8_least profileIdx;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */
  EthTrcv_30_Ar7000_ReturnType resVal; /* Result value of called functions */
#if (((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE)) ||\
     ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO)))
  uint8_least propertyIdx;
  uint8 mStatus;
#endif /* (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE) ||
          (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO) */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if ( (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED) == 0)
  {
    errorId = ETHTRCV_30_AR7000_SLAC_E_NOT_INITIALIZED;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* start critical section */
    ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

    /* #20 Verify that SLAC is not running at the moment */
    /* do not restart SLAC when already running. Flags also acts as kind of semaphore here */
    if (   (  (  (uint16)(~((uint32)ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED))
               & (uint16)(~((uint32)ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK))
               & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0)
#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC )
        || (FALSE == EthTrcv_30_Ar7000_FwReadyForSlacLastResult)
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC */
       )
    {
      /* retVal: E_NOT_OK */

      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    }
    else
    {

      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

      /* #30 Generate new run id */
      (void)ETHTRCV_30_AR7000_APPL_CBK_SLAC_GET_RND(EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacRunId, ETHTRCV_30_AR7000_SLAC_RUNID_LEN_BYTE);

      /* #40 If ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP && ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION is enabled
       *        Verify that the data is already loaded
       *      If ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE is enabled
       *        Send Central Coordinator properties to QCA
       *      Else if ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO is enabled
       *        Send Channel Access Priority (CAP) properies to QCA
       *      Else
       *        Request Parameters from network
       */
#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP))
      if ( ETHTRCV_30_AR7000_SLAC_READ_STATE_DONE != EthTrcv_30_Ar7000_Slac_AmpMapNvmState)
      {
        resVal = ETHTRCV_30_AR7000_RT_NOT_OK;
      }
      else
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION))
      if ( ETHTRCV_30_AR7000_SLAC_READ_STATE_DONE != EthTrcv_30_Ar7000_Slac_CalibBlockNvmState)
      {
        resVal = ETHTRCV_30_AR7000_RT_NOT_OK;
      }
      else
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION*/
      {

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE))
        /* Set CCO */
        propertyIdx = 0;
        do
        {
          resVal = EthTrcv_30_Ar7000_SetProperty(TrcvIdx, &EthTrcv_30_Ar7000_Slac_CCO_Prop_Params, &mStatus);
          propertyIdx++;
        } while ( (ETHTRCV_30_AR7000_RT_NOT_OK == resVal) && (3 > propertyIdx) );
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_SET_CCO;
#elif ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO))
        /* Set Prio */
        propertyIdx = 0;
        do
        {
          resVal = EthTrcv_30_Ar7000_SetProperty(TrcvIdx, &EthTrcv_30_Ar7000_Slac_CAP3_Prop_Params, &mStatus);
          propertyIdx++;
        } while ( (resVal == ETHTRCV_30_AR7000_RT_NOT_OK) && (propertyIdx < 3) );
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_SET_CAP3_PRIO;
#else
        /* Parameter request */
        resVal = (EthTrcv_30_Ar7000_ReturnType)EthTrcv_30_Ar7000_MME_Slac_ParamReq(TrcvIdx);
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_PARAM_REQUEST;
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE, ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO */

      }

      /* #50 Verify that all parameters needed are set or requested */
      if ( (ETHTRCV_30_AR7000_RT_OK == resVal) || (ETHTRCV_30_AR7000_RT_PENDING == resVal) )
      {
        /* #60 Fill the SLAC management struct and profiles */
        retVal = E_OK;

         /* start timeout */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;

        /* Initialize */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMainFctCnt = 0;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacTimeOut = 0x0000u;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode = StartMode;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacParamCnfRecvd = 0;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumSounds = 0;
        /* Counter used for Start Attenuation Repetitions */
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCntStartAtten = ETHTRCV_30_AR7000_SLAC_START_ATTEN_REPETITIONS;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCntSounds = 0;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult = ETHTRCV_30_AR7000_SLAC_VALID_INVALID;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurToggleNum = 0;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAttnCharIndRecvd = 0;

        /* reset attenuation profiles */
        for (profileIdx = 0; profileIdx < ETHTRCV_30_AR7000_SLAC_MAX_ATTEN_CHAR_PROFILES; profileIdx++)
        {
           EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacAverageAtten = ETHTRCV_30_AR7000_SLAC_INVALID_ATTENUATION;
           EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacAttenCharRecvd = FALSE;
           EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacTogglesDetected = 0;
           EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacEvseMacAddr[0] = 0x00;
           EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacEvseMacAddr[1] = 0x00;
           EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacEvseMacAddr[2] = 0x00;
           EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacEvseMacAddr[3] = 0x00;
           EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacEvseMacAddr[4] = 0x00;
           EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacEvseMacAddr[5] = 0x00;

           EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[profileIdx] =
             (P2VAR(EthTrcv_30_Ar7000_Slac_AttenCharProfileType, ETHTRCV_30_AR7000_VAR_NOINIT, ETHTRCV_30_AR7000_VAR_NOINIT))NULL_PTR;
        }
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles = 0;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProfileValidation = 0;

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE))
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapLocalRspRcvd = FALSE;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapRemoteReqRcvd = FALSE;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapQcaRspRcvd = FALSE;
#endif /* (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE) */
      }
      else
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags &= ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_SLAC_START, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_Slac_Start() */ /* PRQA S 6030, 6080 */ /* MD_MSR_STCYC, MD_MSR_STMIF */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_Terminate
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_Terminate(
    uint8 TrcvIdx)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8 mStatus;
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = E_NOT_OK;
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if ( (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED) == 0)
  {
    errorId = ETHTRCV_30_AR7000_SLAC_E_NOT_INITIALIZED;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 leave network */
    if ( (ETHTRCV_30_AR7000_SLAC_FLAG_LINK_VALID & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
    {
      EthTrcv_30_Ar7000_Slac_LeaveScStateRetVal = EthTrcv_30_Ar7000_LeaveScState(TrcvIdx, &mStatus);
      if (ETHTRCV_30_AR7000_RT_PENDING == EthTrcv_30_Ar7000_Slac_LeaveScStateRetVal)
      {
        retVal = E_OK;
      }
      else
      {
        retVal = (Std_ReturnType)EthTrcv_30_Ar7000_Slac_LeaveScStateRetVal;
      }
    }
    else
    {
      retVal = E_OK;
    }
    /* #30 Reset flags completely, except for the initialized flag and the connection parameter valid flag*/
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags &= (ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_CON_PARAMS_VALID);

    /* #40 Call D-Link signal callback */
    (void)ETHTRCV_30_AR7000_APPL_CBK_SLAC_DLINK_READY(ETHTRCV_LINK_STATE_DOWN,
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_SLAC_TERMINATE, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_Slac_Terminate() */ /* PRQA S 6080 */ /* MD_MSR_STMIF */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_Reset
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_Reset(
    uint8 TrcvIdx)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8 mStatus;
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  retVal = E_NOT_OK;
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if ( (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED) == 0)
  {
    errorId = ETHTRCV_30_AR7000_SLAC_E_NOT_INITIALIZED;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Leave network */
    if ( (ETHTRCV_30_AR7000_SLAC_FLAG_LINK_VALID & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
    {
      EthTrcv_30_Ar7000_Slac_LeaveScStateRetVal = EthTrcv_30_Ar7000_LeaveScState(TrcvIdx, &mStatus);
      if (ETHTRCV_30_AR7000_RT_PENDING == EthTrcv_30_Ar7000_Slac_LeaveScStateRetVal)
      {
        retVal = E_OK;
      }
      else
      {
        retVal = (Std_ReturnType)EthTrcv_30_Ar7000_Slac_LeaveScStateRetVal;
      }
    }
    else
    {
      retVal = E_OK;
    }

    /* #30 Reset flags completely, except for the initialized flag */
    EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED; /* Flags */

    /* #40 Call D-Link signal callback */
    (void)ETHTRCV_30_AR7000_APPL_CBK_SLAC_DLINK_READY(ETHTRCV_LINK_STATE_DOWN,
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_SLAC_RESET, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_Slac_Reset() */ /* PRQA S 6080 */ /* MD_MSR_STMIF */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_ToggleConfirmation
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_ToggleConfirmation(
  uint8 TrcvIdx)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if ((EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED) == 0)
  {
    errorId = ETHTRCV_30_AR7000_SLAC_E_NOT_INITIALIZED;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    if ( (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_VALIDATING2) != 0)
    {
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult |= ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_END;
      retVal = E_OK;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_SLAC_TOGGLE_CONFIRMATION, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_Slac_ToggleConfirmation() */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_GET_ATABLE_API) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_GET_ATABLE_API))
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_GetAttenProfileTbl
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_GetAttenProfileTbl(
        uint8                                                          TrcvIdx,
  P2VAR(EthTrcv_30_Ar7000_Slac_AttenCharProfileType, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA)
                                                                       AttenProfileTable,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) TblEntries)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8 tableIdx;
  uint8 addrIdx;
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if ((EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED) == 0)
  {
    errorId = ETHTRCV_30_AR7000_SLAC_E_NOT_INITIALIZED;
  }
  else if (AttenProfileTable == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

    /* #20 Iterate over all available attenuation profiles and store them in the given table */
    tableIdx = 0;
    while (   (ETHTRCV_30_AR7000_SLAC_MAX_ATTEN_CHAR_PROFILES > tableIdx)
           && (tableIdx < *TblEntries)
           && (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[tableIdx] != NULL_PTR) )
    {
       AttenProfileTable[tableIdx].SlacAverageAtten = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[tableIdx]->SlacAverageAtten;
       AttenProfileTable[tableIdx].SlacDiscoveryStatus = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[tableIdx]->SlacDiscoveryStatus;
       AttenProfileTable[tableIdx].SlacTogglesDetected = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[tableIdx]->SlacTogglesDetected;

       addrIdx = 0;
       while ( sizeof(EthTrcv_PhysAddrType) > addrIdx)
       {
         AttenProfileTable[tableIdx].SlacEvseMacAddr[addrIdx] = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[tableIdx]->SlacEvseMacAddr[addrIdx];
         addrIdx++;
       }

       tableIdx++;
    }
    *TblEntries = tableIdx;

    /* #30 Verify the found table entries */
    if (0 != tableIdx)
    {
      retVal = E_OK;
    }
    ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_GET_ATTEN_PROFILE_TBL, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_Slac_GetAttenProfileTbl() */ /* PRQA S 6080 */ /* MD_MSR_STMIF */
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_GET_ATABLE_API */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_GET_ATABLE_API) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_GET_ATABLE_API))
/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_GetUnfilteredAttenProfileMeans
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_GetUnfilteredAttenProfileMeans(
        uint8                                                            TrcvIdx,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_DATA, ETHTRCV_30_AR7000_APPL_DATA) AttenProfileMeansAry,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR)   MeansEntries)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint8_least profileIdx;
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (EthTrcv_30_Ar7000_State != ETHTRCV_STATE_ACTIVE)
  {
    errorId = ETHTRCV_30_AR7000_E_NOT_INITIALIZED;
  }
  else if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if ((EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED) == 0)
  {
    errorId = ETHTRCV_30_AR7000_SLAC_E_NOT_INITIALIZED;
  }
  else if (AttenProfileMeansAry == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else if (MeansEntries == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Iterate over all SLAC profiles and read the attenuation profile */
    for (profileIdx = 0; (   (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNumProfiles > profileIdx)
                          && (profileIdx < *MeansEntries)
                          && (ETHTRCV_30_AR7000_SLAC_MAX_ATTEN_CHAR_PROFILES > profileIdx) ); profileIdx++)
    {
      if (TRUE == EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacAttenCharRecvd)
      {
        /* AttenChar was received in this session */
        AttenProfileMeansAry[profileIdx] = EthTrcv_30_Ar7000_Slac_Profiles[profileIdx].SlacAverageAtten;
      }
      else
      {
        /* AttenChar was not received in this session */
        AttenProfileMeansAry[profileIdx] = ETHTRCV_30_AR7000_SLAC_INVALID_ATTENUATION;
      }
    }
    *MeansEntries = (uint8)profileIdx;

    if (0 != profileIdx)
    {
      retVal = E_OK;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_GET_UNFILTERED_ATTEN_PROFILE_MEANS, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;

} /* EthTrcv_30_Ar7000_Slac_GetUnfilteredAttenProfileMeans() */ /* PRQA S 6080 */ /* MD_MSR_STMIF */
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_GET_ATABLE_API */

#if (((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP)  && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP)) || \
     ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION)))
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_DiagDataWriteAccess
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_DiagDataWriteAccess(
          uint8                                          TrcvIdx,
          uint8                                          DataId,
  P2CONST(uint8, AUTOMATIC, ETHTRCV_30_AR7000_APPL_DATA) DataPtr,
          uint16                                         DataLen)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP))
  uint16 ampMapIdx;
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION))
  uint16 calibrationValuesIdx;
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION */
  uint8 nvmRetVal;
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (DataPtr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Verify that Input parameter is amplitude_map_data
     *       Verify that we have enough space to store the new amplitude map
     *         Update the amplitude map
     *     Verify that Input parameter is calibration_data
     *       Verify that we have enough space to store the new calibration data
     *         Update calibration data
     */
#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP))
    if ( ETHTRCV_30_AR7000_SLAC_DIAG_ID_AMP_MAP_DATA == DataId )
    {
      ampMapIdx = 0;
      if ( sizeof(EthTrcv_30_Ar7000_Slac_AmpMap) >= DataLen)
      {
        while ( ampMapIdx < DataLen)
        {
          EthTrcv_30_Ar7000_Slac_AmpMap[ampMapIdx] = DataPtr[ampMapIdx];
          ampMapIdx++;
        }

        (void)NvM_GetErrorStatus(EthTrcv_30_Ar7000_Slac_VCfgGetAmpMapBlockId(), &nvmRetVal);
        if ( NVM_REQ_PENDING != nvmRetVal)
        {
          /* write block */
          retVal = NvM_WriteBlock(EthTrcv_30_Ar7000_Slac_VCfgGetAmpMapBlockId(), EthTrcv_30_Ar7000_Slac_AmpMap);
        }
        else
        {
          ETHTRCV_30_AR7000_SET_ERROR_ID(errorId, ETHTRCV_30_AR7000_SLAC_E_NVM); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

          /* E_NOT_OK */
        }
      }
      else
      {
        /* Invalid identifier */
        ETHTRCV_30_AR7000_SET_ERROR_ID(errorId, ETHTRCV_30_AR7000_SLAC_E_INV_PARAMETER); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

        /* E_NOT_OK */
      }
    }
    else
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION))
    if ( ETHTRCV_30_AR7000_SLAC_DIAG_ID_CALIB_DATA == DataId )
    {
      calibrationValuesIdx = 0;
      if ( sizeof(EthTrcv_30_Ar7000_Slac_AttenCalibrationValues) >= DataLen)
      {
        while ( calibrationValuesIdx < DataLen)
        {
          EthTrcv_30_Ar7000_Slac_AttenCalibrationValues[calibrationValuesIdx] = (sint8)DataPtr[calibrationValuesIdx];
          calibrationValuesIdx++;
        }

        (void)NvM_GetErrorStatus(EthTrcv_30_Ar7000_Slac_VCfgGetCalibBlockId(), &nvmRetVal);
        if ( NVM_REQ_PENDING != nvmRetVal)
        {
          /* write block */
          /* PRQA S 0310 3 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
          retVal = NvM_WriteBlock(
            EthTrcv_30_Ar7000_Slac_VCfgGetCalibBlockId(),
            (P2VAR(uint8, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))EthTrcv_30_Ar7000_Slac_AttenCalibrationValues);
        }
        else
        {
          ETHTRCV_30_AR7000_SET_ERROR_ID(errorId, ETHTRCV_30_AR7000_SLAC_E_NVM); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

          /* E_NOT_OK */
        }
      }
      else
      {
        /* Invalid identifier */
        ETHTRCV_30_AR7000_SET_ERROR_ID(errorId, ETHTRCV_30_AR7000_SLAC_E_INV_PARAMETER); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

        /* E_NOT_OK */
      }
    }
    else
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION */

    {
      /* Invalid identifier */
      ETHTRCV_30_AR7000_SET_ERROR_ID(errorId, ETHTRCV_30_AR7000_SLAC_E_INV_PARAMETER); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

      /* E_NOT_OK */
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_DIAG_DATA_WRITE_ACCESS, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_Slac_DiagDataWriteAccess() */ /* PRQA S 6030, 6080 */ /* MD_MSR_STCYC, MD_MSR_STMIF */
#endif  /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP || ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

#if (((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP)  && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP)) || \
     ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION)))
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_DiagDataReadAccess
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
/* PRQA S 0777 5  */ /* MD_EthTrcv_30_Ar7000_0777 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_DiagDataReadAccess(
        uint8                                          TrcvIdx,
        uint8                                          DataId,
  P2VAR(uint8, AUTOMATIC, ETHTRCV_30_AR7000_APPL_DATA) DataPtr,
  P2VAR(uint16, AUTOMATIC, ETHTRCV_30_AR7000_APPL_VAR) DataLen)
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  EthTrcv_30_Ar7000_DetErrorType errorId = ETHTRCV_30_AR7000_E_NO_ERROR;
  Std_ReturnType retVal = E_NOT_OK; /* Return value of this function */

  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  /* #10 Check plausibility of input parameters */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
  if (TrcvIdx >= ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_TRCV_IDX;
  }
  else if (DataPtr == NULL_PTR)
  {
    errorId = ETHTRCV_30_AR7000_E_INV_POINTER;
  }
  else
#endif
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Verify that Input parameter is amplitude_map_data
     *       Verify that amplitude map is already read from NvM memory
     *         Copy amplitude map
     *     Verify that Input parameter is calibration_data
     *       Verify that calibration data is already read from NvM memory
     *         Copy calibration data
     */
#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP))
    if ( ETHTRCV_30_AR7000_SLAC_DIAG_ID_AMP_MAP_DATA == DataId )
    {
      if ( ETHTRCV_30_AR7000_SLAC_READ_STATE_DONE == EthTrcv_30_Ar7000_Slac_AmpMapNvmState)
      {
        if  (*DataLen > sizeof(EthTrcv_30_Ar7000_Slac_AmpMap))
        {
          *DataLen = sizeof(EthTrcv_30_Ar7000_Slac_AmpMap);
        }

        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
               (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&DataPtr[0],
               (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&EthTrcv_30_Ar7000_Slac_AmpMap[0],
               *DataLen
             );

        retVal = E_OK;
      }
    }
    else
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION))
    if ( ETHTRCV_30_AR7000_SLAC_DIAG_ID_CALIB_DATA == DataId )
    {
      if ( ETHTRCV_30_AR7000_SLAC_READ_STATE_DONE == EthTrcv_30_Ar7000_Slac_CalibBlockNvmState)
      {
        if  (*DataLen > sizeof(EthTrcv_30_Ar7000_Slac_AttenCalibrationValues))
        {
          *DataLen = sizeof(EthTrcv_30_Ar7000_Slac_AttenCalibrationValues);
        }

        /* PRQA S 0310, 3305 5 */ /* MD_EthTrcv_30_Ar7000_0310 */
        IpBase_Copy(
               (P2VAR(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&DataPtr[0],
               (P2CONST(IpBase_CopyDataType, AUTOMATIC, IPBASE_APPL_DATA))&EthTrcv_30_Ar7000_Slac_AttenCalibrationValues[0],
               *DataLen
             );

        retVal = E_OK;
      }
    }
    else
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION */

    {
      /* Invalid identifier */
      ETHTRCV_30_AR7000_SET_ERROR_ID(errorId, ETHTRCV_30_AR7000_SLAC_E_INV_PARAMETER); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
  if (errorId != ETHTRCV_30_AR7000_E_NO_ERROR)
  {
    (void)Det_ReportError(ETHTRCV_30_AR7000_MODULE_ID, ETHTRCV_30_AR7000_INSTANCE_ID, ETHTRCV_30_AR7000_API_ID_DIAG_DATA_READ_ACCESS, errorId);
  }
#else
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(TrcvIdx); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif

  return retVal;
} /* EthTrcv_30_Ar7000_Slac_DiagDataReadAccess() */ /* PRQA S 6080 */ /* MD_MSR_STMIF */
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP || ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_TrcvLinkStateChgCbk
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_TrcvLinkStateChgCbk(
  uint8                 EthIfCtrlIdx,
  EthTrcv_LinkStateType TrcvLinkState)
{
  /* #10 Verify transceiver index */
  if (EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx() == EthIfCtrlIdx)
  {
    /* #20 Verify that link-state is active
     *        Set valid link indication for SLAC protocol
     *        Trigger Amplitude map exchange during next state machine invocation
     *      Else
     *        Inform application about link-state down using the callback
     */
    if ( ETHTRCV_LINK_STATE_ACTIVE == TrcvLinkState )
    {
      if ( (ETHTRCV_30_AR7000_SLAC_FLAG_JOINING & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0)
      {
        EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_LINK_VALID;

        ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
        /* Multiple QCA7005 transceivers are not supported, just use 0. Else a mapping would be required. */
        EthTrcv_30_Ar7000_Slac_VEnterAmpMapState(0);
        ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      }
    }
    else
    {
      /* Check if there is currently an active link, do not call twice in case of a reset */
      if ( (ETHTRCV_30_AR7000_SLAC_FLAG_LINK_VALID & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
      {
        /* call D-Link signal callback */
        (void)ETHTRCV_30_AR7000_APPL_CBK_SLAC_DLINK_READY(ETHTRCV_LINK_STATE_DOWN,
          EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNmk, EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacNid);
      }
    }
  }
} /* EthTrcv_30_Ar7000_Slac_TrcvLinkStateChgCbk() */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_Slac_MainFunction
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_MainFunction(void)
{
  /* #10 State1: Read local data blocks */
  /* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | State 1   | INIT                                                                                  |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Action    | Initialize and start state machine                                                    |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   |  -> State 2 (SET COO)                                                                 |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   |  -> State 3 (SET CAP3 PRIO)                                                           |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   |  -> State 4 (PARAM REQUEST)                                                           |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Reference | EthTrcv_30_Ar7000_Slac_Init                                                           |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Reference | EthTrcv_30_Ar7000_Slac_Start                                                          |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   */
  if ( ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED == EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags )
  {
#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP))
    if ( ETHTRCV_30_AR7000_SLAC_READ_STATE_DONE != EthTrcv_30_Ar7000_Slac_AmpMapNvmState)
    {
      EthTrcv_30_Ar7000_VNvmReadBlock(
         0,
         EthTrcv_30_Ar7000_Slac_VCfgGetAmpMapBlockId(),
         &EthTrcv_30_Ar7000_Slac_AmpMapNvmState,
         EthTrcv_30_Ar7000_Slac_AmpMap);
    }
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION))
    if ( ETHTRCV_30_AR7000_SLAC_READ_STATE_DONE != EthTrcv_30_Ar7000_Slac_CalibBlockNvmState)
    {
      /* PRQA S 0310 5 */ /* MD_EthTrcv_30_Ar7000_0310_2 */
      EthTrcv_30_Ar7000_VNvmReadBlock(
         0,
         EthTrcv_30_Ar7000_Slac_VCfgGetCalibBlockId(),
         &EthTrcv_30_Ar7000_Slac_CalibBlockNvmState,
         (P2VAR(uint8, AUTOMATIC, ETHTRCV_30_AR7000_VAR_NOINIT))EthTrcv_30_Ar7000_Slac_AttenCalibrationValues);
    }
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION*/
  }
  else
  /* #20 State2: Initialize QCA to be never Central Coordinator (CCo)
   *             Verify that initialization was successful
   *               If ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO is enabled
   *                 Initialize QCA channel access priority (CAP) AND trigger State3 transition
   *               Else
   *                 Trigger sending 'SLAC parameter' request MME AND trigger State4 transition
   */
  /* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | State 2   | SET CCO                                                                               |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Action    | Send SET_PROPERTY MME and wait for response                                           |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | Timeout(T_assoc_response) -> Association Error ->State 1 (INIT)                       |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | SetProperty API returns with E_OK -> State 4 (PARAM REQUEST)                          |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | SetProperty API returns with E_OK -> State 3 (SET CAP3 PRIO)                          |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | SetProperty API returns with E_NOT_OK -> Association Error ->State 1 (INIT)           |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Reference | EthTrcv_30_Ar7000_Slac_VHandleParamCnf                                                |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE))
  if ( (ETHTRCV_30_AR7000_SLAC_FLAG_SET_CCO & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
  {
    if (ETHTRCV_30_AR7000_RT_OK == EthTrcv_30_Ar7000_Slac_VSM_N_SetProperty(
                   &EthTrcv_30_Ar7000_Slac_CCO_Prop_Params,
                   ETHTRCV_30_AR7000_SLAC_STEP_1_SET_CCO))
    {
      uint16 tempFlags;

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO))
      /* Set CAP3 Prio */
      (void)EthTrcv_30_Ar7000_Slac_VSM_N_SetProperty(
         &EthTrcv_30_Ar7000_Slac_CAP3_Prop_Params,
         ETHTRCV_30_AR7000_SLAC_STEP_2_SET_PRIO);

      /* start timeout */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;

      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
      tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_SET_CAP3_PRIO;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
#else
      /* Parameter request */
      (void)EthTrcv_30_Ar7000_MME_Slac_ParamReq(0);

      /* start timeout */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;

      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
      tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_PARAM_REQUEST;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO */
    }
  }
  else
#endif /* ETHTRCV_30_AR7000_SLAC_FLAG_SET_CCO */

  /* #30 State3: Initialize QCA's QoS settings channel access priority (CAP)
   *             Verify that initialization was successful
   *               Trigger sending 'SLAC parameter' request MME AND trigger State4 transition
   */
  /* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | State 3   | SET CAP3 PRIO                                                                         |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Action    | Send SET_PROPERTY MME and wait for response                                           |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | Timeout(T_assoc_response) -> Association Error ->State 1 (INIT)                       |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | SetProperty API returns with E_OK -> State 4 (PARAM REQUEST)                          |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | SetProperty API returns with E_NOT_OK -> Association Error ->State 1 (INIT)           |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Reference | EthTrcv_30_Ar7000_Slac_VHandleParamCnf                                                |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO))
  if ( (ETHTRCV_30_AR7000_SLAC_FLAG_SET_CAP3_PRIO & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
  {
    if (ETHTRCV_30_AR7000_RT_OK == EthTrcv_30_Ar7000_Slac_VSM_N_SetProperty(
                   &EthTrcv_30_Ar7000_Slac_CAP3_Prop_Params,
                   ETHTRCV_30_AR7000_SLAC_STEP_2_SET_PRIO))
    {
      uint16 tempFlags;

      /* Parameter request */
      (void)EthTrcv_30_Ar7000_MME_Slac_ParamReq(0);

      /* start timeout */
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacCurProcTimeout = ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacReqRetries = ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU - 1;

      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      tempFlags = EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags & ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK;
      tempFlags |= ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED | ETHTRCV_30_AR7000_SLAC_FLAG_PARAM_REQUEST;
      EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags = tempFlags;
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    }
  }
  else
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO */

  /* #40 State4: Wait for 'SLAC parameter' confirmation MME and send StartAttenChar indication */
  /* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | State 4   | PARAM REQUEST                                                                         |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Action    | Send Parameter Request MME, wait for responses and send StartAttenChar indication     |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | Timeout(T_assoc_response) -> State 3 (SOUNDING)                                       |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | Timeout(T_assoc_response) -> Association Error ->State 1 (INIT)                       |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Reference | EthTrcv_30_Ar7000_Slac_VHandleParamCnf                                                |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   */
  if ( (ETHTRCV_30_AR7000_SLAC_FLAG_PARAM_REQUEST & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
  {
    EthTrcv_30_Ar7000_Slac_VSM_4_ParamReq();
  }

  /* #50 State5: Send 'SLAC sound indication' packets
   *             Wait for 'SLAC attenuation characterization' indication MME from EVSEs
   *             Order EVSEs
   *             Select EVSE and validate
   */
  /* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | State 5   | SOUNDING, SELECT EVSE                                                                 |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Action    | Send sounding packets, wait for ATTEN_CHAR.IND, select EVSE and send Validation Req.  |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | Timeout(T_assoc_MNBC_timeout) -> No direct, other error -> State 1 (INIT)             |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | Timeout(T_assoc_MNBC_timeout) -> Select EVSE + Validate -> State 4 (VALIDATING 1)     |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | Timeout(T_assoc_MNBC_timeout) -> Select EVSE + Skip Validation -> State 6 (CONFIRMING)|
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | Timeout(T_assoc_MNBC_timeout) -> No association indication received -> State 2        |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Reference | EthTrcv_30_Ar7000_Slac_VSelectEvseAndValidate                                         |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   */
  else if ( (ETHTRCV_30_AR7000_SLAC_FLAG_SOUNDING & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
  {
    EthTrcv_30_Ar7000_Slac_VSM_5_Sounding();
  }

  /* #60 State6: Request EVSE validation requirements. */
  /* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | State 6   | VALIDATION PHASE 1                                                                    |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Action    | Wait for Validation Cnf. and continue to VALIDATION PHASE 2                           |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | Timeout(T_assoc_response) -> Association Error -> State 1 (INIT)                      |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | RxIndication(Validation.Cnf) -> State 4 (VALIDATION PHASE 1 Restart)                  |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | RxIndication(Validation.Cnf) -> State 5 (VALIDATION PHASE 2)                          |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Reference | EthTrcv_30_Ar7000_VHandle_ValidationCnf1                                              |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   */
  else if ( (ETHTRCV_30_AR7000_SLAC_FLAG_VALIDATING1 & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
  {
    EthTrcv_30_Ar7000_Slac_VSM_6_Validation1();
  }

  /* #70 State7: Perform validation process if it is needed by EVSE. */
  /* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | State 7   | VALIDATION PHASE 2                                                                    |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Action    | Wait until resp. time is expired and start validation or goto State 6 (CONFIRMING)    |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | Timeout(T_assoc_response) -> Association Error -> State 1 (INIT)                      |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | RxIndication(Validation.Cnf) -> State 4 (VALIDATION PHASE 1)                          |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | RxIndication(Validation.Cnf) -> State 6 (CONFIRMING)                                  |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Reference | EthTrcv_30_Ar7000_VHandle_ValidationCnf2                                              |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   */
  else if ( (ETHTRCV_30_AR7000_SLAC_FLAG_VALIDATING2 & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
  {
    EthTrcv_30_Ar7000_Slac_VSM_7_Validation2();
  }

  /* #80 State8: Waits for response from previous send 'SLAC match' request MME */
 /* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | State 8   | CONFIRMING                                                                            |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Action    | Send Slac Match request to station that detected all BCB toggles + wait for response  |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Trigger   | Timeout(T_assoc_response) -> Association Error -> State 1 (INIT)                      |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Trigger   | No station detected all BCB toggles -> Association Error -> State 1 (INIT)            |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Trigger   | RxIndication(SlacMatch.Cnf) -> State 7 (JOINING)                                      |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Reference | EthTrcv_30_Ar7000_Slac_VConfirming                                                    |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  */
  else if ( (ETHTRCV_30_AR7000_SLAC_FLAG_CONFIRMING & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
  {
    EthTrcv_30_Ar7000_Slac_VSM_8_Confirming();
  }

  /* #90 State9: Poll link state and mark is valid if it is active. */
 /* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | State 9   | JOINING                                                                               |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Action    | Join network and set "Connection parameters valid" flag                               |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Trigger   | Timeout(T_assoc_join) -> Association Error -> State 1 (INIT)                          |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Trigger   | LinkStateChangeCbk(Link Active) -> State 8 (CONNECTION VALID)                         |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Trigger   | LinkStateChangeCbk(Link Active and Amplitude Map Exch. on) -> State 9 (AMPLITUDE MAP) |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Reference | EthTrcv_30_Ar7000_Slac_TrcvLinkStateChgCbk                                            |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Reference | EthTrcv_30_Ar7000_SetNmk                                                              |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  */
  else if ( (ETHTRCV_30_AR7000_SLAC_FLAG_JOINING & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
  {
    EthTrcv_30_Ar7000_Slac_VSM_9_Joining();
  }

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE))
  /* #100 State10: Exchange amplitude map data either:
   *              EVSE -> EV or
   *              EV -> EVSE
   */
 /* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | State 10   | AMPLITUDE MAP                                                                        |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Action    | Exchanges local and remote amplitude map                                              |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Trigger   | Timeout(T_assoc_response) -> Association Error -> State 1 (INIT)                      |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Trigger   | RxIndication(CM_AMP_MAP.CNF) -> Invalid Response -> Association Error -> State 1      |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  * | Trigger   | RxIndication(CM_AMP_MAP.CNF) -> Set Amp Map, reboot and wait for link                 |
  * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  */
  else if (   ((ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_START & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0)
           && ((ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_EXCHANGED & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_EXCHANGED) )
  {
    Std_ReturnType resVal = E_OK;

    if (   ((EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode & ETHTRCV_30_AR7000_SLAC_START_MODE_LOCAL_AMP_MAP_EXCH) != 0)
        || (   ((EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacMode & ETHTRCV_30_AR7000_SLAC_START_MODE_AMP_MAP_TRIG_BY_EVSE) != 0)
            && (EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacAmpMapRemoteReqRcvd == TRUE) ) )
    {
      resVal = EthTrcv_30_Ar7000_Slac_VSM_10_AmpMapExchangeLocal();
    }

    /* check if SLAC was already reset and EthTrcv_30_Ar7000_Slac_VSM_10_AmpMapExchangeRemote() must not be called */
    if (resVal == E_OK)
    {
      EthTrcv_30_Ar7000_Slac_VSM_10_AmpMapExchangeRemote();
    }
  }

  /* #110 State11: Update EV's QCA amplitude map. */
  /* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | State 11   | AMPLITUDE MAP 2                                                                      |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Action    | Sets merged amplitude map within QCA                                                  |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | Timeout(T_assoc_join) -> Association Error -> State 1 (INIT)                          |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   */
  else if ( (ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_EXCHANGED & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) == ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_EXCHANGED )
  {
    EthTrcv_30_Ar7000_Slac_VSM_11_AmpMapExchangePlcNode();
  }

  /* #120 State12: Wait for QCA amplitude map exchange confirmation and inform application by DLINK ready callback */
  /* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | State 12   | AMPLITUDE MAP 3                                                                      |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Action    | Calls DLINK Ready callback after Amp map was set                                      |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * | Trigger   | Timeout(T_assoc_join) -> Association Error -> State 1 (INIT)                          |
   * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   */
  else if ( (ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_SET & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
  {
    EthTrcv_30_Ar7000_Slac_VSM_12_AmpMapEnd();
  }

#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE */

  else
  {
    /* nothing to do */
  }

  /* Polling API */
  if (ETHTRCV_30_AR7000_RT_PENDING == EthTrcv_30_Ar7000_Slac_LeaveScStateRetVal)
  {
    uint8 mStatus;
    EthTrcv_30_Ar7000_Slac_LeaveScStateRetVal = EthTrcv_30_Ar7000_LeaveScState(0, &mStatus);

    if (ETHTRCV_30_AR7000_RT_OK == EthTrcv_30_Ar7000_Slac_LeaveScStateRetVal)
    {
      EthTrcv_30_Ar7000_Slac_LeaveScStateCnt = 0;
    }
  }
  if (ETHTRCV_30_AR7000_RT_NOT_OK == EthTrcv_30_Ar7000_Slac_LeaveScStateRetVal)
  {
    if ( ETHTRCV_30_AR7000_SLAC_LEAVESCSTATE_MAX_RETRIES > EthTrcv_30_Ar7000_Slac_LeaveScStateCnt)
    {
      uint8 mStatus;
      EthTrcv_30_Ar7000_Slac_LeaveScStateCnt++;
      EthTrcv_30_Ar7000_Slac_LeaveScStateRetVal = EthTrcv_30_Ar7000_LeaveScState(0, &mStatus);
    }
    else
    {
      EthTrcv_30_Ar7000_Slac_LeaveScStateCnt = 0;
      EthTrcv_30_Ar7000_Slac_LeaveScStateRetVal = ETHTRCV_30_AR7000_RT_OK;
    }
  }
} /* EthTrcv_30_Ar7000_Slac_MainFunction() */ /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */

/***********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_Slac_RxIndication
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
/* PRQA S 3673 7 */ /* MD_EthTrcv_30_Ar7000_3673 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_Slac_RxIndication(
        uint8                                                          TrcvIdx,
        Eth_FrameType                                                  FrameType,
        boolean                                                        IsBroadcast,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) PhysAddrPtr,
  P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) DataU8Ptr,
        uint16                                                         LenByte )
{
  /* -------------------------------------------- Local Variables -------------------------------------------------- */
  uint16 mmType;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  ETHTRCV_30_AR7000_DUMMY_STATEMENT(FrameType); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */

  /* #10 Parse Vendor specific Management Message Type from received packets.
   *     Call the specific handle routines.
   */
  mmType  = (uint16)( ((uint16)DataU8Ptr[ETHTRCV_30_AR7000_MME_MMTYPE + 0]) << 0 );
  mmType |= (uint16)( ((uint16)DataU8Ptr[ETHTRCV_30_AR7000_MME_MMTYPE + 1]) << 8 );

  switch(mmType)
  {
  case ETHTRCV_30_AR7000_MME_CM_SLAC_PARAM | ETHTRCV_30_AR7000_MME_CNF:
    if ( (ETHTRCV_30_AR7000_SLAC_FLAG_PARAM_REQUEST & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags ) != 0 )
    {
      EthTrcv_30_Ar7000_Slac_VHandleParamCnf(TrcvIdx, IsBroadcast, PhysAddrPtr, DataU8Ptr);
    }
    else
    {
      /* discard */
    }
    break;

  case ETHTRCV_30_AR7000_MME_CM_ATTEN_CHAR | ETHTRCV_30_AR7000_MME_IND:
    if ( (ETHTRCV_30_AR7000_SLAC_FLAG_SOUNDING & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags ) != 0 )
    {
      EthTrcv_30_Ar7000_Slac_VHandleAttenCharInd(TrcvIdx, IsBroadcast, PhysAddrPtr, DataU8Ptr, LenByte);
    }
    else
    {
      /* discard */
    }
    break;

  case ETHTRCV_30_AR7000_MME_CM_VALIDATE | ETHTRCV_30_AR7000_MME_CNF:
    if ( (ETHTRCV_30_AR7000_SLAC_FLAG_VALIDATING1 & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
    {
      EthTrcv_30_Ar7000_Slac_VHandleValidationCnf1(TrcvIdx, IsBroadcast, PhysAddrPtr, DataU8Ptr);
    }
    /* Check SlacFlags, discard this message in case SLAC was already reset due to message timeout */
    else if (   ((ETHTRCV_30_AR7000_SLAC_VALID_TOGGLE_END & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacValidationResult) != 0)
             && ((ETHTRCV_30_AR7000_SLAC_FLAG_VALIDATING2 & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags ) != 0) )
    {
      EthTrcv_30_Ar7000_Slac_VHandleValidationCnf2(TrcvIdx, IsBroadcast, PhysAddrPtr, DataU8Ptr);
    }
    else
    {
      /* discard and wait for next confirmation or ignore after SLAC reset */
    }
    break;

  case ETHTRCV_30_AR7000_MME_CM_SLAC_MATCH | ETHTRCV_30_AR7000_MME_CNF:
    if ( (ETHTRCV_30_AR7000_SLAC_FLAG_CONFIRMING & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
    {
      EthTrcv_30_Ar7000_Slac_VHandleMatchCnf(TrcvIdx, IsBroadcast, PhysAddrPtr, DataU8Ptr);
    }
    else
    {
      /* discard */
    }
    break;

  case ETHTRCV_30_AR7000_MME_CM_SET_KEY | ETHTRCV_30_AR7000_MME_CNF:
    if ( (ETHTRCV_30_AR7000_SLAC_FLAG_JOINING & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
    {
      EthTrcv_30_Ar7000_Slac_VHandleCmSetKeyCnf(TrcvIdx, DataU8Ptr);
    }
    else
    {
      /* discard */
    }
    break;

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE))
  case ETHTRCV_30_AR7000_MME_CM_AMP_MAP | ETHTRCV_30_AR7000_MME_CNF:
    if (   ((ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_START & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags ) != 0)
        || ((ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_SET & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags ) != 0) )

    {
#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP))
      EthTrcv_30_Ar7000_Slac_VHandleAmpMapCnf(TrcvIdx, IsBroadcast, PhysAddrPtr, DataU8Ptr);
#endif /* (STD_ON == ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) */
    }
    else
    {
      /* discard */
    }
    break;

  case ETHTRCV_30_AR7000_MME_CM_AMP_MAP | ETHTRCV_30_AR7000_MME_REQ:
    /* In case link detection is slower than on EVSE side an AMP MAP REQ could be received during joining phase */
    if ( (ETHTRCV_30_AR7000_SLAC_FLAG_JOINING & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags) != 0 )
    {
      ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      EthTrcv_30_Ar7000_Slac_VEnterAmpMapState(TrcvIdx);
      ETHTRCV_30_AR7000_END_CRITICAL_SECTION(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    }
    if (   ((ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_START & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags ) != 0)
        && ((ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_REMOTE & EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacFlags ) == 0) )
    {
      EthTrcv_30_Ar7000_Slac_VHandleAmpMapReq(TrcvIdx, IsBroadcast, PhysAddrPtr, DataU8Ptr, LenByte);
    }
    else
    {
      /* discard */
    }
    break;
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE */

  default:
    /* not to handle here */
    break;
  }
} /* EthTrcv_30_Ar7000_MME_Slac_RxIndication() */ /* PRQA S 6030, 6050, 6060 */ /* MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR */

#endif /* ETHTRCV_30_AR7000_ENABLE_SLAC */

#define ETHTRCV_30_AR7000_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

/* PRQA L:LONG_IDENTIFIERS */ /* MD_EthTrcv_30_Ar7000_0779 */

/* module specific MISRA deviations:
 MD_EthTrcv_30_Ar7000_3199:
      Reason:     Dummy statement to avoid compiler warnings.
      Risk:       There is no risk as such statements have no effect on the code.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_0311:
      Reason:     Required by AUTOSAR API of Ethernet Interface
      Risk:       There is no risk since the EthIf component does not change the MAC.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_0310:
      Reason:     The pointer where the cast led to has different alignment. The cast
                  is necessary to get a bytewise access.
      Risk:       There is no risk since the array is a continuous data buffer.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_0310_1:
      Reason:     The PhysAddrType and a pointer uint8 are compatible.
      Risk:       There is no risk.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_0310_2:
      Reason:     NvM_WriteBlock and NvM_ReadBlock only supports u8 pointers. The calibration values therefore
                  need to be casted from unsigned to signed and vice versa.
      Risk:       There is no risk.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_0311:
      Reason:     The pointer where the cast led to has different alignment. The MAC address is compared by 2 checks.
                  A bytewise comparison makes 6 checks necessary. This is an optimization.
      Risk:       There is no risk since the PhysAddrType always ensures a size of 6 Bytes ( 4 + 2 bytes)
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_3305
      Reason:     The pointer where the cast led to has different alignment. The cast
                  is necessary to get a 4 byte aligned access.
      Risk:       There is no risk since the array is a continuous data buffer.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_3305:
      Reason:     Efficient access to MAC address (not a bytewise access)
      Risk:       There is no risk.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_0715:
      Reason:     Not splitted to functions because of efficiency resons.
      Risk:       There is no risk.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_3218:
      Reason:     Depending on the ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE state CAP3 priority property frames
                  are sent within the Start- and the Main- function. When ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE
                  is STD_OFF and ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO is STD_OFF this warning is gone.
      Risk:       There is no risk.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_3355:
      Reason:     Configuration dependent. The result may be always true when SOUNDING2SOUNDING is defined to 1.
                  Define increases flexibility.
      Risk:       There is no risk.
      Prevention: Covered by code review.
 MD_EthTrcv_30_Ar7000_0777:
      Reason:     The module prefix EthTrcv_30_Ar7000_Slac is too long to comply with this rule in any case.
                  Rule conflicts with AUTOSAR requirement that specifies the module prefix.
      Risk:       There is no risk.
      Prevention: Not necessary.
 MD_EthTrcv_30_Ar7000_3673:
      Reason:     The physical address is passed as VAR pointer to the RXIndication function. The signature of
                  this function is specified by AUTOSAR.
      Risk:       No risk. Even if the address is modified, the upper layer that passes in the address don't care.
      Prevention: Not necessary.
  MD_EthTrcv_30_Ar7000_0779:
      Reason:     Component short name is really long, especially for the subcomponent Slac (EthTrcv_30_Ar7000_Slac...)
      Risk:       No risk.
      Prevention: Not necessary.
*/

/**********************************************************************************************************************
 *  END OF FILE: EthTrcv_30_Ar7000.c
 *********************************************************************************************************************/

