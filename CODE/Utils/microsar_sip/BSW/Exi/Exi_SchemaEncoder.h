/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 * 
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Exi_SchemaEncoder.h
 *        \brief  Efficient XML Interchange general encoder header file
 *
 *      \details  Vector static code header file for the Efficient XML Interchange sub-component general encoder.
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file Exi.h.
 *********************************************************************************************************************/
/* PRQA S 0857 EOF */ /* MD_Exi_1.1_0857 */ /* [L] Number of macro definitions exceeds 1024 - program is non-conforming. */

#if !defined (EXI_SCHEMA_ENCODER_H) /* PRQA S 0883 */ /* MD_Exi_19.15_0883 */
# define EXI_SCHEMA_ENCODER_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 EXI_SCHEMA_ENCODER_H_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "Exi.h"
#include "Exi_Priv.h"
#include "Exi_SchemaTypes.h"
#include "Exi_SAP_SchemaEncoder.h"
#include "Exi_XMLSIG_SchemaEncoder.h"
#include "Exi_DIN_SchemaEncoder.h"
#include "Exi_ISO_SchemaEncoder.h"
#include "Exi_ISO_ED2_DIS_SchemaEncoder.h"
/* PRQA L:EXI_SCHEMA_ENCODER_H_IF_NESTING */ /* MD_MSR_1.1_828 */

/* PRQA S 0777 IDENTIFIER_NAMES */ /* MD_Exi_5.1 */
/* EXI internal API Ids */
#define EXI_API_ID_ENCODE_ID 0x01U
#define EXI_API_ID_ENCODE_BASE64BINARY 0x02U
#define EXI_API_ID_ENCODE_STRING 0x03U
#define EXI_API_ID_ENCODE_SCHEMA_FRAGMENT 0x04U
#define EXI_API_ID_ENCODE_SCHEMA_ROOT 0x05U

/* Encoding default switches */
#ifndef EXI_ENCODE_SCHEMA_FRAGMENT
#define EXI_ENCODE_SCHEMA_FRAGMENT STD_OFF
#endif
#ifndef EXI_ENCODE_SCHEMA_ROOT
#define EXI_ENCODE_SCHEMA_ROOT STD_ON
#endif
#ifndef EXI_ENCODE_ID
#define EXI_ENCODE_ID STD_OFF
#endif
#ifndef EXI_ENCODE_BASE64BINARY
#define EXI_ENCODE_BASE64BINARY STD_OFF
#endif
#ifndef EXI_ENCODE_STRING
#define EXI_ENCODE_STRING STD_OFF
#endif


#define EXI_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/**********************************************************************************************************************
 *  FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Exi_Encode_ID
 *********************************************************************************************************************/
/*! \brief         Generates an EXI stream
 *  \details       Generates an EXI stream representing an Exi_IDType object
 *  \param[in,out] EncWsPtr                    pointer to EXI encoding workspace
 *  \param[in]     IDPtr                       pointer to Exi_IDType data struct to be encoded
 *  \pre           The provided workspace must be initialized
 *  \context       TASK (due to run time)
 *  \reentrant     TRUE (for different workspaces)
 *  \synchronous   TRUE
 *  \config        EXI_ENCODE_ID
 *********************************************************************************************************************/
#if (defined(EXI_ENCODE_ID) && (EXI_ENCODE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
extern FUNC(void, EXI_CODE) Exi_Encode_ID( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_IDType, AUTOMATIC, EXI_APPL_DATA) IDPtr );
#endif /* (defined(EXI_ENCODE_ID) && (EXI_ENCODE_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_base64Binary
 *********************************************************************************************************************/
/*! \brief         Generates an EXI stream
 *  \details       Generates an EXI stream representing an Exi_base64BinaryType object
 *  \param[in,out] EncWsPtr                    pointer to EXI encoding workspace
 *  \param[in]     base64BinaryPtr             pointer to Exi_base64BinaryType data struct to be encoded
 *  \pre           The provided workspace must be initialized
 *  \context       TASK (due to run time)
 *  \reentrant     TRUE (for different workspaces)
 *  \synchronous   TRUE
 *  \config        EXI_ENCODE_BASE64BINARY
 *********************************************************************************************************************/
#if (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
extern FUNC(void, EXI_CODE) Exi_Encode_base64Binary( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_base64BinaryType, AUTOMATIC, EXI_APPL_DATA) base64BinaryPtr );
#endif /* (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_string
 *********************************************************************************************************************/
/*! \brief         Generates an EXI stream
 *  \details       Generates an EXI stream representing an Exi_stringType object
 *  \param[in,out] EncWsPtr                    pointer to EXI encoding workspace
 *  \param[in]     stringPtr                   pointer to Exi_stringType data struct to be encoded
 *  \pre           The provided workspace must be initialized
 *  \context       TASK (due to run time)
 *  \reentrant     TRUE (for different workspaces)
 *  \synchronous   TRUE
 *  \config        EXI_ENCODE_STRING
 *********************************************************************************************************************/
#if (defined(EXI_ENCODE_STRING) && (EXI_ENCODE_STRING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
extern FUNC(void, EXI_CODE) Exi_Encode_string( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_stringType, AUTOMATIC, EXI_APPL_DATA) stringPtr );
#endif /* (defined(EXI_ENCODE_STRING) && (EXI_ENCODE_STRING == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_SchemaFragment
 *********************************************************************************************************************/
/*! \brief         Generates an EXI stream
 *  \details       Generates an EXI stream representing an Exi_SchemaFragment object
 *  \param[in,out] EncWsPtr                    pointer to EXI encoding workspace
 *  \param[in]     Namespace                   Namespace identifier to select the based schema namepace for this fragment
 *  \pre           The provided workspace must be initialized
 *  \context       TASK (due to run time)
 *  \reentrant     TRUE (for different workspaces)
 *  \synchronous   TRUE
 *  \config        EXI_ENCODE_SCHEMA_FRAGMENT
 *********************************************************************************************************************/
#if (defined(EXI_ENCODE_SCHEMA_FRAGMENT) && (EXI_ENCODE_SCHEMA_FRAGMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
extern FUNC(void, EXI_CODE) Exi_Encode_SchemaFragment( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       Exi_NamespaceIdType Namespace );
#endif /* (defined(EXI_ENCODE_SCHEMA_FRAGMENT) && (EXI_ENCODE_SCHEMA_FRAGMENT == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_SchemaRoot
 *********************************************************************************************************************/
/*! \brief         Generates an EXI stream
 *  \details       Generates an EXI stream representing an Exi_SchemaRoot object
 *  \param[in,out] EncWsPtr                    pointer to EXI encoding workspace
 *  \pre           The provided workspace must be initialized
 *  \context       TASK (due to run time)
 *  \reentrant     TRUE (for different workspaces)
 *  \synchronous   TRUE
 *  \config        EXI_ENCODE_SCHEMA_ROOT
 *********************************************************************************************************************/
#if (defined(EXI_ENCODE_SCHEMA_ROOT) && (EXI_ENCODE_SCHEMA_ROOT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
extern FUNC(void, EXI_CODE) Exi_Encode_SchemaRoot( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr );
#endif /* (defined(EXI_ENCODE_SCHEMA_ROOT) && (EXI_ENCODE_SCHEMA_ROOT == STD_ON)) */


#define EXI_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/* PRQA L:IDENTIFIER_NAMES */

#endif
  /* EXI_SCHEMA_ENCODER_H */
