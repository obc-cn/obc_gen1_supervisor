############################################################################### 
# File Name  : Exi_rules.mak 
# Description: Rules makefile 
#------------------------------------------------------------------------------
# COPYRIGHT
#------------------------------------------------------------------------------
# Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.
#------------------------------------------------------------------------------
# REVISION HISTORY
#------------------------------------------------------------------------------
# Version   Date        Author  Description
#------------------------------------------------------------------------------
# 1.00.00   2010-11-03  visalr  Created
# 1.00.01   2010-11-17  visal   minor name fixes
# 2.00.00   2012-02-01  visdd   Created (EXI development started)
# 2.00.01   2013-10-01  visdd   ESCAN00069263 Remove PBcfg.c
# 3.00.00   2013-11-14  visdd   ESCAN00071678 Support multiple schema versions
# 3.01.00   2014-03-06  visdd   ESCAN00074027 Customer specific extensions
# 4.00.00   2014-06-13  vissop  ESCAN00076252 Customer specific extensions
# 4.01.00   2014-07-18  vissop  ESCAN00077212 Adapt package attributes
# 5.00.00   2017-06-07  vissop  ESCAN00095448 Add ISO Ed2 CD2 support
# 5.00.00   2017-11-28  -       Remove obsolete schemata
# 6.00.00   2018-08-29  vissop  STORYC-5808   Add ISO Ed2 DIS support
# 6.00.01   2019-02-06  vircbl  Added support of component-based SIP structure
#------------------------------------------------------------------------------
# TemplateVersion = 1.02
###############################################################################


###############################################################
# REGISTRY
#

#ifndef EXI_SCHEMA_SAP
# get schema variables from the variant specific config file
#include $(ROOT)\$(GLOBAL_COMP_DIR)\Exi\$(if $(BSW_MAKE_DIR),$(BSW_MAKE_DIR),mak)\Exi_cfg.mak
#endif

#e.g.: LIBRARIES_TO_BUILD      +=    $(LIB_OUPUT_PATH)\vendorx_canlib1.$(LIB_FILE_SUFFIX)
LIBRARIES_TO_BUILD      += Exi
# General files
Exi_FILES               = Exi$(BSW_SRC_DIR)\Exi.c Exi$(BSW_SRC_DIR)\Exi_BSDecoder.c Exi$(BSW_SRC_DIR)\Exi_BSEncoder.c Exi$(BSW_SRC_DIR)\Exi_SchemaDecoder.c Exi$(BSW_SRC_DIR)\Exi_SchemaEncoder.c
# Use case specific files
Exi_FILES               += Exi$(BSW_SRC_DIR)\Exi_SAP_SchemaDecoder.c Exi$(BSW_SRC_DIR)\Exi_SAP_SchemaEncoder.c
Exi_FILES               += Exi$(BSW_SRC_DIR)\Exi_DIN_SchemaDecoder.c Exi$(BSW_SRC_DIR)\Exi_DIN_SchemaEncoder.c
Exi_FILES               += Exi$(BSW_SRC_DIR)\Exi_ISO_SchemaDecoder.c Exi$(BSW_SRC_DIR)\Exi_ISO_SchemaEncoder.c
Exi_FILES               += Exi$(BSW_SRC_DIR)\Exi_ISO_ED2_DIS_SchemaDecoder.c Exi$(BSW_SRC_DIR)\Exi_ISO_ED2_DIS_SchemaEncoder.c
Exi_FILES               += Exi$(BSW_SRC_DIR)\Exi_XMLSIG_SchemaDecoder.c Exi$(BSW_SRC_DIR)\Exi_XMLSIG_SchemaEncoder.c

# e.g.: CC_FILES_TO_BUILD       += drv\can_drv.c
ifeq ($(GET_IMPLEMENTION_FROM), LIB)
CC_FILES_TO_BUILD       += 
else
CC_FILES_TO_BUILD       += Exi$(BSW_SRC_DIR)\Exi.c Exi$(BSW_SRC_DIR)\Exi_BSDecoder.c Exi$(BSW_SRC_DIR)\Exi_BSEncoder.c Exi$(BSW_SRC_DIR)\Exi_SchemaDecoder.c Exi$(BSW_SRC_DIR)\Exi_SchemaEncoder.c
CC_FILES_TO_BUILD       += Exi$(BSW_SRC_DIR)\Exi_SAP_SchemaDecoder.c Exi$(BSW_SRC_DIR)\Exi_SAP_SchemaEncoder.c
CC_FILES_TO_BUILD       += Exi$(BSW_SRC_DIR)\Exi_DIN_SchemaDecoder.c Exi$(BSW_SRC_DIR)\Exi_DIN_SchemaEncoder.c
CC_FILES_TO_BUILD       += Exi$(BSW_SRC_DIR)\Exi_ISO_SchemaDecoder.c Exi$(BSW_SRC_DIR)\Exi_ISO_SchemaEncoder.c
CC_FILES_TO_BUILD       += Exi$(BSW_SRC_DIR)\Exi_ISO_ED2_DIS_SchemaDecoder.c Exi$(BSW_SRC_DIR)\Exi_ISO_ED2_DIS_SchemaEncoder.c
CC_FILES_TO_BUILD       += Exi$(BSW_SRC_DIR)\Exi_XMLSIG_SchemaDecoder.c Exi$(BSW_SRC_DIR)\Exi_XMLSIG_SchemaEncoder.c
endif
CPP_FILES_TO_BUILD      +=
ASM_FILES_TO_BUILD      +=

#LIBRARIES_LINK_ONLY     += (not yet supported)
#OBJECTS_LINK_ONLY       += (not yet supported)

#-------------------------------------------------------------------------------------------------
#only define new dirs, OBJ, LIB, LOG were created automatically 
#-------------------------------------------------------------------------------------------------
DIRECTORIES_TO_CREATE   +=

#DEPEND_GCC_OPTS         += (not yet supported)

# e.g.:  GENERATED_SOURCE_FILES += $(GENDATA_DIR)\drv_par.c
GENERATED_SOURCE_FILES  += $(GENDATA_DIR)\Exi_Lcfg.c

#e.g.: COMMON_SOURCE_FILES     += $(GENDATA_DIR)\v_par.c
COMMON_SOURCE_FILES     += 

#-------------------------------------------------------------------------------------------------
# <project>.dep & <projekt>.lnk & <project>.bin and.....
# all in err\ & obj\ & lst\ & lib\ & log\ will be deleted by clean-rule automatically
# so in this clean-rule it is only necessary to define additional files which
# were not delete automatically.
# e.g.: $(<PATH>)\can_test.c
#-------------------------------------------------------------------------------------------------
MAKE_CLEAN_RULES        +=
#MAKE_GENERATE_RULES     +=
#MAKE_COMPILER_RULES     +=
#MAKE_DEBUG_RULES        +=
#MAKE_CONFIG_RULES       +=
#MAKE_ADD_RULES          +=


###############################################################
# REQUIRED   (defined in BaseMake (global.Makefile.target.make...))
#
# SSC_ROOT		(required)
# PROJECT_ROOT	(required)
#
# LIB_OUTPUT_PATH	(optional)
# OBJ_OUTPUT_PATH	(optional)
#
# OBJ_FILE_SUFFIX	
# LIB_FILE_SUFFIX
#
###############################################################


###############################################################
# PROVIDE   this Section can be used to define own additional rules
#
# In vendorx_can_cfg.mak:
# Please configure the project file:
#CAN_CONFIG_FILE = $(PROJECT_ROOT)\source\network\can\my_can_config.cfg

#In vendorx_can_config :
#generate_can_config:
#$(SSC_ROOT)\core\com\can\tools\canconfiggen.exe -o $(CAN_CONFIG_FILE)


###############################################################
# SPECIFIC
#
# There are no rules defined for the Specific part of the 
# Rules-Makefile. Each author is free to create temporary 
# variables or to use other resources of GNU-MAKE
#
###############################################################

