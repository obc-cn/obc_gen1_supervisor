/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 * 
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Exi_SAP_SchemaDecoder.h
 *        \brief  Efficient XML Interchange SAP decoder header file
 *
 *      \details  Vector static code header file for the Efficient XML Interchange sub-component SAP decoder.
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file Exi.h.
 *********************************************************************************************************************/
/* PRQA S 0857 EOF */ /* MD_Exi_1.1_0857 */ /* [L] Number of macro definitions exceeds 1024 - program is non-conforming. */

#if !defined (EXI_SAP_SCHEMA_DECODER_H) /* PRQA S 0883 */ /* MD_Exi_19.15_0883 */
# define EXI_SAP_SCHEMA_DECODER_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 EXI_SAP_SCHEMA_DECODER_H_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "Exi.h"
#include "Exi_Priv.h"
#include "Exi_SchemaTypes.h"
#include "Exi_SchemaDecoder.h"
/* PRQA L:EXI_SAP_SCHEMA_DECODER_H_IF_NESTING */ /* MD_MSR_1.1_828 */

#if (!defined (EXI_ENABLE_DECODE_SAP_MESSAGE_SET))
# if (defined (EXI_ENABLE_SAP_MESSAGE_SET))
#  define EXI_ENABLE_DECODE_SAP_MESSAGE_SET   EXI_ENABLE_SAP_MESSAGE_SET
# else
#  define EXI_ENABLE_DECODE_SAP_MESSAGE_SET   STD_OFF
# endif
#endif

#if (defined(EXI_ENABLE_DECODE_SAP_MESSAGE_SET) && (EXI_ENABLE_DECODE_SAP_MESSAGE_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */

/* PRQA S 0777 IDENTIFIER_NAMES */ /* MD_Exi_5.1 */
/* EXI internal API Ids */
#define EXI_API_ID_DECODE_SAP_APP_PROTOCOL 0x01U
#define EXI_API_ID_DECODE_SAP_PROTOCOL_NAMESPACE 0x02U
#define EXI_API_ID_DECODE_SAP_RESPONSE_CODE 0x03U
#define EXI_API_ID_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ 0x04U
#define EXI_API_ID_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES 0x05U
#define EXI_API_ID_DECODE_SCHEMA_SET_SAP 0x06U

/* Decoding default switches */
#ifndef EXI_DECODE_SAP_APP_PROTOCOL
#define EXI_DECODE_SAP_APP_PROTOCOL STD_OFF
#endif
#ifndef EXI_DECODE_SAP_PROTOCOL_NAMESPACE
#define EXI_DECODE_SAP_PROTOCOL_NAMESPACE STD_OFF
#endif
#ifndef EXI_DECODE_SAP_RESPONSE_CODE
#define EXI_DECODE_SAP_RESPONSE_CODE STD_OFF
#endif
#ifndef EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ
#define EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ STD_OFF
#endif
#ifndef EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES
#define EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES STD_OFF
#endif
#ifndef EXI_DECODE_SCHEMA_SET_SAP
#define EXI_DECODE_SCHEMA_SET_SAP STD_OFF
#endif


#define EXI_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/**********************************************************************************************************************
 *  FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Exi_Decode_SAP_AppProtocol
 *********************************************************************************************************************/
/*! \brief         Decodes an EXI stream
 *  \details       Decodes an EXI stream representing an Exi_SAP_AppProtocolType object
 *  \param[in,out] DecWsPtr                    pointer to EXI decoding workspace
 *  \param[out]    elementPtrPtr               pointer to the decoded element
 *  \pre           The provided workspace must be initialized
 *  \context       TASK (due to run time)
 *  \reentrant     TRUE (for different workspaces)
 *  \synchronous   TRUE
 *  \config        EXI_DECODE_SAP_APP_PROTOCOL
 *********************************************************************************************************************/
#if (defined(EXI_DECODE_SAP_APP_PROTOCOL) && (EXI_DECODE_SAP_APP_PROTOCOL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
extern FUNC(void, EXI_CODE) Exi_Decode_SAP_AppProtocol( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_SAP_AppProtocolType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr );
#endif /* (defined(EXI_DECODE_SAP_APP_PROTOCOL) && (EXI_DECODE_SAP_APP_PROTOCOL == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_SAP_protocolNamespace
 *********************************************************************************************************************/
/*! \brief         Decodes an EXI stream
 *  \details       Decodes an EXI stream representing an Exi_SAP_protocolNamespaceType object
 *  \param[in,out] DecWsPtr                    pointer to EXI decoding workspace
 *  \param[out]    elementPtrPtr               pointer to the decoded element
 *  \pre           The provided workspace must be initialized
 *  \context       TASK (due to run time)
 *  \reentrant     TRUE (for different workspaces)
 *  \synchronous   TRUE
 *  \config        EXI_DECODE_SAP_PROTOCOL_NAMESPACE
 *********************************************************************************************************************/
#if (defined(EXI_DECODE_SAP_PROTOCOL_NAMESPACE) && (EXI_DECODE_SAP_PROTOCOL_NAMESPACE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
extern FUNC(void, EXI_CODE) Exi_Decode_SAP_protocolNamespace( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_SAP_protocolNamespaceType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr );
#endif /* (defined(EXI_DECODE_SAP_PROTOCOL_NAMESPACE) && (EXI_DECODE_SAP_PROTOCOL_NAMESPACE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_SAP_responseCode
 *********************************************************************************************************************/
/*! \brief         Decodes an EXI stream
 *  \details       Decodes an EXI stream representing an Exi_SAP_responseCodeType object
 *  \param[in,out] DecWsPtr                    pointer to EXI decoding workspace
 *  \param[in,out] responseCodePtr             in: pointer to memory; out: decoded value
 *  \pre           The provided workspace must be initialized
 *  \context       TASK (due to run time)
 *  \reentrant     TRUE (for different workspaces)
 *  \synchronous   TRUE
 *  \config        EXI_DECODE_SAP_RESPONSE_CODE
 *********************************************************************************************************************/
#if (defined(EXI_DECODE_SAP_RESPONSE_CODE) && (EXI_DECODE_SAP_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
extern FUNC(void, EXI_CODE) Exi_Decode_SAP_responseCode( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_SAP_responseCodeType, AUTOMATIC, EXI_APPL_VAR) responseCodePtr );
#endif /* (defined(EXI_DECODE_SAP_RESPONSE_CODE) && (EXI_DECODE_SAP_RESPONSE_CODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_SAP_supportedAppProtocolReq
 *********************************************************************************************************************/
/*! \brief         Decodes an EXI stream
 *  \details       Decodes an EXI stream representing an Exi_SAP_supportedAppProtocolReqType object
 *  \param[in,out] DecWsPtr                    pointer to EXI decoding workspace
 *  \param[out]    elementPtrPtr               pointer to the decoded element
 *  \pre           The provided workspace must be initialized
 *  \context       TASK (due to run time)
 *  \reentrant     TRUE (for different workspaces)
 *  \synchronous   TRUE
 *  \config        EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ
 *********************************************************************************************************************/
#if (defined(EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ) && (EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
extern FUNC(void, EXI_CODE) Exi_Decode_SAP_supportedAppProtocolReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_SAP_supportedAppProtocolReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr );
#endif /* (defined(EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ) && (EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_SAP_supportedAppProtocolRes
 *********************************************************************************************************************/
/*! \brief         Decodes an EXI stream
 *  \details       Decodes an EXI stream representing an Exi_SAP_supportedAppProtocolResType object
 *  \param[in,out] DecWsPtr                    pointer to EXI decoding workspace
 *  \param[out]    elementPtrPtr               pointer to the decoded element
 *  \pre           The provided workspace must be initialized
 *  \context       TASK (due to run time)
 *  \reentrant     TRUE (for different workspaces)
 *  \synchronous   TRUE
 *  \config        EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES
 *********************************************************************************************************************/
#if (defined(EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES) && (EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
extern FUNC(void, EXI_CODE) Exi_Decode_SAP_supportedAppProtocolRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_SAP_supportedAppProtocolResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr );
#endif /* (defined(EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES) && (EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_SchemaSet_SAP
 *********************************************************************************************************************/
/*! \brief         Decodes an EXI stream
 *  \details       Decodes an EXI stream representing an Exi_SchemaSet_SAP object
 *  \param[in,out] DecWsPtr                    pointer to EXI decoding workspace
 *  \pre           The provided workspace must be initialized
 *  \context       TASK (due to run time)
 *  \reentrant     TRUE (for different workspaces)
 *  \synchronous   TRUE
 *  \config        EXI_DECODE_SCHEMA_SET_SAP
 *  \trace         CREQ-154410
 *********************************************************************************************************************/
#if (defined(EXI_DECODE_SCHEMA_SET_SAP) && (EXI_DECODE_SCHEMA_SET_SAP == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
extern FUNC(void, EXI_CODE) Exi_Decode_SchemaSet_SAP( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr );
#endif /* (defined(EXI_DECODE_SCHEMA_SET_SAP) && (EXI_DECODE_SCHEMA_SET_SAP == STD_ON)) */


#define EXI_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/* PRQA L:IDENTIFIER_NAMES */

#endif /* (defined (EXI_ENABLE_DECODE_SAP_MESSAGE_SET) && (EXI_ENABLE_DECODE_SAP_MESSAGE_SET == STD_ON)) */

#endif
  /* EXI_SAP_SCHEMA_DECODER_H */
