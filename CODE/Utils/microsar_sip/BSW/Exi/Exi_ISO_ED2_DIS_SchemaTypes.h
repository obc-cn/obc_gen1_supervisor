/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 * 
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Exi_ISO_ED2_DIS_SchemaTypes.h
 *        \brief  Efficient XML Interchange ISO_ED2_DIS types header file
 *
 *      \details  Vector static code header file for the Efficient XML Interchange sub-component ISO_ED2_DIS types.
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file Exi.h.
 *********************************************************************************************************************/
/* PRQA S 0857 EOF */ /* MD_Exi_1.1_0857 */ /* [L] Number of macro definitions exceeds 1024 - program is non-conforming. */

#if !defined (EXI_ISO_ED2_DIS_SCHEMA_TYPES_H) /* PRQA S 0883 */ /* MD_Exi_19.15_0883 */
# define EXI_ISO_ED2_DIS_SCHEMA_TYPES_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 EXI_ISO_ED2_DIS_SCHEMA_TYPES_H_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "Exi_Types.h"
/* PRQA L:EXI_ISO_ED2_DIS_SCHEMA_TYPES_H_IF_NESTING */ /* MD_MSR_1.1_828 */

/* PRQA S 0779 IDENTIFIER_NAMES */ /* MD_Exi_5.1 */
/* PRQA S 0780 NAMESPACE */ /* MD_Exi_5.6 */
/* PRQA S 0750 UNION */ /* MD_Exi_18.4 */

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
/* Root elements */
#define EXI_ISO_ED2_DIS_ACBCREQ_CONTROL_MODE_TYPE                                        (Exi_RootElementIdType)113U
#define EXI_ISO_ED2_DIS_ACBCRES_CONTROL_MODE_TYPE                                        (Exi_RootElementIdType)114U
#define EXI_ISO_ED2_DIS_ACDPOSITIONING_REQ_TYPE                                          (Exi_RootElementIdType)115U
#define EXI_ISO_ED2_DIS_ACDPOSITIONING_RES_TYPE                                          (Exi_RootElementIdType)116U
#define EXI_ISO_ED2_DIS_ACD_SSENERGY_TRANSFER_MODE_TYPE                                  (Exi_RootElementIdType)117U
#define EXI_ISO_ED2_DIS_ACREQ_ALIGNMENT_CHECK_MECHANISM_TYPE                             (Exi_RootElementIdType)118U
#define EXI_ISO_ED2_DIS_ACRES_ALIGNMENT_CHECK_MECHANISM_TYPE                             (Exi_RootElementIdType)119U
#define EXI_ISO_ED2_DIS_AC_BIDIRECTIONAL_CONTROL_REQ_TYPE                                (Exi_RootElementIdType)120U
#define EXI_ISO_ED2_DIS_AC_BIDIRECTIONAL_CONTROL_RES_TYPE                                (Exi_RootElementIdType)121U
#define EXI_ISO_ED2_DIS_AC_CPDREQ_ENERGY_TRANSFER_MODE_TYPE                              (Exi_RootElementIdType)122U
#define EXI_ISO_ED2_DIS_AC_CPDRES_ENERGY_TRANSFER_MODE_TYPE                              (Exi_RootElementIdType)123U
#define EXI_ISO_ED2_DIS_AREQ_IDENTIFICATION_MODE_TYPE                                    (Exi_RootElementIdType)124U
#define EXI_ISO_ED2_DIS_ALIGNMENT_CHECK_REQ_TYPE                                         (Exi_RootElementIdType)125U
#define EXI_ISO_ED2_DIS_ALIGNMENT_CHECK_RES_TYPE                                         (Exi_RootElementIdType)126U
#define EXI_ISO_ED2_DIS_AUTHORIZATION_REQ_TYPE                                           (Exi_RootElementIdType)127U
#define EXI_ISO_ED2_DIS_AUTHORIZATION_RES_TYPE                                           (Exi_RootElementIdType)128U
#define EXI_ISO_ED2_DIS_BPT_AC_CPDREQ_ENERGY_TRANSFER_MODE_TYPE                          (Exi_RootElementIdType)129U
#define EXI_ISO_ED2_DIS_BPT_AC_CPDRES_ENERGY_TRANSFER_MODE_TYPE                          (Exi_RootElementIdType)130U
#define EXI_ISO_ED2_DIS_BPT_DC_CPDREQ_ENERGY_TRANSFER_MODE_TYPE                          (Exi_RootElementIdType)131U
#define EXI_ISO_ED2_DIS_BPT_DC_CPDRES_ENERGY_TRANSFER_MODE_TYPE                          (Exi_RootElementIdType)132U
#define EXI_ISO_ED2_DIS_BPT_SCHEDULED_PDREQ_CONTROL_MODE_TYPE                            (Exi_RootElementIdType)133U
#define EXI_ISO_ED2_DIS_BODY_BASE_TYPE                                                   (Exi_RootElementIdType)134U
#define EXI_ISO_ED2_DIS_CDREQ_CONTROL_MODE_TYPE                                          (Exi_RootElementIdType)135U
#define EXI_ISO_ED2_DIS_CLREQ_IDENTIFICATION_MODE_TYPE                                   (Exi_RootElementIdType)136U
#define EXI_ISO_ED2_DIS_CLRES_CONTROL_MODE_TYPE                                          (Exi_RootElementIdType)137U
#define EXI_ISO_ED2_DIS_CLRES_IDENTIFICATION_MODE_TYPE                                   (Exi_RootElementIdType)138U
#define EXI_ISO_ED2_DIS_CPDREQ_CONTROL_MODE_TYPE                                         (Exi_RootElementIdType)139U
#define EXI_ISO_ED2_DIS_CPDREQ_ENERGY_TRANSFER_MODE_TYPE                                 (Exi_RootElementIdType)140U
#define EXI_ISO_ED2_DIS_CPDRES_CONTROL_MODE_TYPE                                         (Exi_RootElementIdType)141U
#define EXI_ISO_ED2_DIS_CPDRES_ENERGY_TRANSFER_MODE_TYPE                                 (Exi_RootElementIdType)142U
#define EXI_ISO_ED2_DIS_CSREQ_CONTROL_MODE_TYPE                                          (Exi_RootElementIdType)143U
#define EXI_ISO_ED2_DIS_CSRES_CONTROL_MODE_TYPE                                          (Exi_RootElementIdType)144U
#define EXI_ISO_ED2_DIS_CABLE_CHECK_REQ_TYPE                                             (Exi_RootElementIdType)145U
#define EXI_ISO_ED2_DIS_CABLE_CHECK_RES_TYPE                                             (Exi_RootElementIdType)146U
#define EXI_ISO_ED2_DIS_CERTIFICATE_INSTALLATION_REQ_TYPE                                (Exi_RootElementIdType)147U
#define EXI_ISO_ED2_DIS_CERTIFICATE_INSTALLATION_RES_TYPE                                (Exi_RootElementIdType)148U
#define EXI_ISO_ED2_DIS_CHARGE_PARAMETER_DISCOVERY_REQ_TYPE                              (Exi_RootElementIdType)149U
#define EXI_ISO_ED2_DIS_CHARGE_PARAMETER_DISCOVERY_RES_TYPE                              (Exi_RootElementIdType)150U
#define EXI_ISO_ED2_DIS_CHARGING_STATUS_REQ_TYPE                                         (Exi_RootElementIdType)151U
#define EXI_ISO_ED2_DIS_CHARGING_STATUS_RES_TYPE                                         (Exi_RootElementIdType)152U
#define EXI_ISO_ED2_DIS_CONNECT_CHARGING_DEVICE_REQ_TYPE                                 (Exi_RootElementIdType)153U
#define EXI_ISO_ED2_DIS_CONNECT_CHARGING_DEVICE_RES_TYPE                                 (Exi_RootElementIdType)154U
#define EXI_ISO_ED2_DIS_COUPLING_ACREQ_ALIGNMENT_CHECK_MECHANISM_TYPE                    (Exi_RootElementIdType)155U
#define EXI_ISO_ED2_DIS_COUPLING_ACRES_ALIGNMENT_CHECK_MECHANISM_TYPE                    (Exi_RootElementIdType)156U
#define EXI_ISO_ED2_DIS_CURRENT_DEMAND_REQ_TYPE                                          (Exi_RootElementIdType)157U
#define EXI_ISO_ED2_DIS_CURRENT_DEMAND_RES_TYPE                                          (Exi_RootElementIdType)158U
#define EXI_ISO_ED2_DIS_DCBCREQ_CONTROL_MODE_TYPE                                        (Exi_RootElementIdType)159U
#define EXI_ISO_ED2_DIS_DC_BIDIRECTIONAL_CONTROL_REQ_TYPE                                (Exi_RootElementIdType)160U
#define EXI_ISO_ED2_DIS_DC_BIDIRECTIONAL_CONTROL_RES_TYPE                                (Exi_RootElementIdType)161U
#define EXI_ISO_ED2_DIS_DC_CPDREQ_ENERGY_TRANSFER_MODE_TYPE                              (Exi_RootElementIdType)162U
#define EXI_ISO_ED2_DIS_DC_CPDRES_ENERGY_TRANSFER_MODE_TYPE                              (Exi_RootElementIdType)163U
#define EXI_ISO_ED2_DIS_DISCONNECT_CHARGING_DEVICE_REQ_TYPE                              (Exi_RootElementIdType)164U
#define EXI_ISO_ED2_DIS_DISCONNECT_CHARGING_DEVICE_RES_TYPE                              (Exi_RootElementIdType)165U
#define EXI_ISO_ED2_DIS_DYNAMIC_ACBCREQ_CONTROL_MODE_TYPE                                (Exi_RootElementIdType)166U
#define EXI_ISO_ED2_DIS_DYNAMIC_ACBCRES_CONTROL_MODE_TYPE                                (Exi_RootElementIdType)167U
#define EXI_ISO_ED2_DIS_DYNAMIC_CDREQ_CONTROL_MODE_TYPE                                  (Exi_RootElementIdType)168U
#define EXI_ISO_ED2_DIS_DYNAMIC_CPDREQ_CONTROL_MODE_TYPE                                 (Exi_RootElementIdType)169U
#define EXI_ISO_ED2_DIS_DYNAMIC_CSREQ_CONTROL_MODE_TYPE                                  (Exi_RootElementIdType)170U
#define EXI_ISO_ED2_DIS_DYNAMIC_CSRES_CONTROL_MODE_TYPE                                  (Exi_RootElementIdType)171U
#define EXI_ISO_ED2_DIS_DYNAMIC_DCBCREQ_CONTROL_MODE_TYPE                                (Exi_RootElementIdType)172U
#define EXI_ISO_ED2_DIS_DYNAMIC_PDD_REQ_CONTROL_MODE_TYPE                                (Exi_RootElementIdType)173U
#define EXI_ISO_ED2_DIS_EVFINE_POSITIONING_PARAMETERS_TYPE                               (Exi_RootElementIdType)174U
#define EXI_ISO_ED2_DIS_EVFINE_POSITIONING_SETUP_PARAMETERS_TYPE                         (Exi_RootElementIdType)175U
#define EXI_ISO_ED2_DIS_EVSEFINE_POSITIONING_PARAMETERS_TYPE                             (Exi_RootElementIdType)176U
#define EXI_ISO_ED2_DIS_EVSEFINE_POSITIONING_SETUP_PARAMETERS_TYPE                       (Exi_RootElementIdType)177U
#define EXI_ISO_ED2_DIS_EFFICIENCY_ACREQ_ALIGNMENT_CHECK_MECHANISM_TYPE                  (Exi_RootElementIdType)178U
#define EXI_ISO_ED2_DIS_EFFICIENCY_ACRES_ALIGNMENT_CHECK_MECHANISM_TYPE                  (Exi_RootElementIdType)179U
#define EXI_ISO_ED2_DIS_FINE_POSITIONING_REQ_TYPE                                        (Exi_RootElementIdType)180U
#define EXI_ISO_ED2_DIS_FINE_POSITIONING_RES_TYPE                                        (Exi_RootElementIdType)181U
#define EXI_ISO_ED2_DIS_FINE_POSITIONING_SETUP_REQ_TYPE                                  (Exi_RootElementIdType)182U
#define EXI_ISO_ED2_DIS_FINE_POSITIONING_SETUP_RES_TYPE                                  (Exi_RootElementIdType)183U
#define EXI_ISO_ED2_DIS_GENERIC_EVFINE_POSITIONING_PARAMETERS_TYPE                       (Exi_RootElementIdType)184U
#define EXI_ISO_ED2_DIS_GENERIC_EVSEFINE_POSITIONING_PARAMETERS_TYPE                     (Exi_RootElementIdType)185U
#define EXI_ISO_ED2_DIS_IDENTIFICATION_DETAILS_REQ_TYPE                                  (Exi_RootElementIdType)186U
#define EXI_ISO_ED2_DIS_IDENTIFICATION_DETAILS_RES_TYPE                                  (Exi_RootElementIdType)187U
#define EXI_ISO_ED2_DIS_LFA_EVFINE_POSITIONING_PARAMETERS_TYPE                           (Exi_RootElementIdType)188U
#define EXI_ISO_ED2_DIS_LFA_EVFINE_POSITIONING_SETUP_PARAMETERS_TYPE                     (Exi_RootElementIdType)189U
#define EXI_ISO_ED2_DIS_LFA_EVSEFINE_POSITIONING_PARAMETERS_TYPE                         (Exi_RootElementIdType)190U
#define EXI_ISO_ED2_DIS_LFA_EVSEFINE_POSITIONING_SETUP_PARAMETERS_TYPE                   (Exi_RootElementIdType)191U
#define EXI_ISO_ED2_DIS_MRREQ_CONTROL_MODE_TYPE                                          (Exi_RootElementIdType)192U
#define EXI_ISO_ED2_DIS_MV_EVSEFINE_POSITIONING_PARAMETERS_TYPE                          (Exi_RootElementIdType)193U
#define EXI_ISO_ED2_DIS_MV_EVSEFINE_POSITIONING_SETUP_PARAMETERS_TYPE                    (Exi_RootElementIdType)194U
#define EXI_ISO_ED2_DIS_METERING_RECEIPT_REQ_TYPE                                        (Exi_RootElementIdType)195U
#define EXI_ISO_ED2_DIS_METERING_RECEIPT_RES_TYPE                                        (Exi_RootElementIdType)196U
#define EXI_ISO_ED2_DIS_PDREQ_CONTROL_MODE_TYPE                                          (Exi_RootElementIdType)197U
#define EXI_ISO_ED2_DIS_PDD_REQ_CONTROL_MODE_TYPE                                        (Exi_RootElementIdType)198U
#define EXI_ISO_ED2_DIS_PAIRING_REQ_TYPE                                                 (Exi_RootElementIdType)199U
#define EXI_ISO_ED2_DIS_PAIRING_RES_TYPE                                                 (Exi_RootElementIdType)200U
#define EXI_ISO_ED2_DIS_PN_C_AREQ_IDENTIFICATION_MODE_TYPE                               (Exi_RootElementIdType)201U
#define EXI_ISO_ED2_DIS_PN_C_CLREQ_IDENTIFICATION_MODE_TYPE                              (Exi_RootElementIdType)202U
#define EXI_ISO_ED2_DIS_PN_C_CLRES_IDENTIFICATION_MODE_TYPE                              (Exi_RootElementIdType)203U
#define EXI_ISO_ED2_DIS_POWER_DELIVERY_REQ_TYPE                                          (Exi_RootElementIdType)204U
#define EXI_ISO_ED2_DIS_POWER_DELIVERY_RES_TYPE                                          (Exi_RootElementIdType)205U
#define EXI_ISO_ED2_DIS_POWER_DEMAND_REQ_TYPE                                            (Exi_RootElementIdType)206U
#define EXI_ISO_ED2_DIS_POWER_DEMAND_RES_TYPE                                            (Exi_RootElementIdType)207U
#define EXI_ISO_ED2_DIS_POWER_SCHEDULE_ENTRY_TYPE                                        (Exi_RootElementIdType)208U
#define EXI_ISO_ED2_DIS_PRE_CHARGE_REQ_TYPE                                              (Exi_RootElementIdType)209U
#define EXI_ISO_ED2_DIS_PRE_CHARGE_RES_TYPE                                              (Exi_RootElementIdType)210U
#define EXI_ISO_ED2_DIS_RSSI_ACREQ_ALIGNMENT_CHECK_MECHANISM_TYPE                        (Exi_RootElementIdType)211U
#define EXI_ISO_ED2_DIS_RSSI_ACRES_ALIGNMENT_CHECK_MECHANISM_TYPE                        (Exi_RootElementIdType)212U
#define EXI_ISO_ED2_DIS_SDL_ENERGY_TRANSFER_MODE_TYPE                                    (Exi_RootElementIdType)213U
#define EXI_ISO_ED2_DIS_SSENERGY_TRANSFER_MODE_TYPE                                      (Exi_RootElementIdType)214U
#define EXI_ISO_ED2_DIS_SALES_TARIFF_ENTRY_TYPE                                          (Exi_RootElementIdType)215U
#define EXI_ISO_ED2_DIS_SCHEDULE_LIST_TYPE                                               (Exi_RootElementIdType)216U
#define EXI_ISO_ED2_DIS_SCHEDULED_ACBCREQ_CONTROL_MODE_TYPE                              (Exi_RootElementIdType)217U
#define EXI_ISO_ED2_DIS_SCHEDULED_ACBCRES_CONTROL_MODE_TYPE                              (Exi_RootElementIdType)218U
#define EXI_ISO_ED2_DIS_SCHEDULED_CDREQ_CONTROL_MODE_TYPE                                (Exi_RootElementIdType)219U
#define EXI_ISO_ED2_DIS_SCHEDULED_CLRES_CONTROL_MODE_TYPE                                (Exi_RootElementIdType)220U
#define EXI_ISO_ED2_DIS_SCHEDULED_CPDREQ_CONTROL_MODE_TYPE                               (Exi_RootElementIdType)221U
#define EXI_ISO_ED2_DIS_SCHEDULED_CPDRES_CONTROL_MODE_TYPE                               (Exi_RootElementIdType)222U
#define EXI_ISO_ED2_DIS_SCHEDULED_CSREQ_CONTROL_MODE_TYPE                                (Exi_RootElementIdType)223U
#define EXI_ISO_ED2_DIS_SCHEDULED_DCBCREQ_CONTROL_MODE_TYPE                              (Exi_RootElementIdType)224U
#define EXI_ISO_ED2_DIS_SCHEDULED_MRREQ_CONTROL_MODE_TYPE                                (Exi_RootElementIdType)225U
#define EXI_ISO_ED2_DIS_SCHEDULED_PDREQ_CONTROL_MODE_TYPE                                (Exi_RootElementIdType)226U
#define EXI_ISO_ED2_DIS_SCHEDULED_PDD_REQ_CONTROL_MODE_TYPE                              (Exi_RootElementIdType)227U
#define EXI_ISO_ED2_DIS_SERVICE_DETAIL_REQ_TYPE                                          (Exi_RootElementIdType)228U
#define EXI_ISO_ED2_DIS_SERVICE_DETAIL_RES_TYPE                                          (Exi_RootElementIdType)229U
#define EXI_ISO_ED2_DIS_SERVICE_DISCOVERY_REQ_TYPE                                       (Exi_RootElementIdType)230U
#define EXI_ISO_ED2_DIS_SERVICE_DISCOVERY_RES_TYPE                                       (Exi_RootElementIdType)231U
#define EXI_ISO_ED2_DIS_SERVICE_SELECTION_REQ_TYPE                                       (Exi_RootElementIdType)232U
#define EXI_ISO_ED2_DIS_SERVICE_SELECTION_RES_TYPE                                       (Exi_RootElementIdType)233U
#define EXI_ISO_ED2_DIS_SESSION_SETUP_REQ_TYPE                                           (Exi_RootElementIdType)234U
#define EXI_ISO_ED2_DIS_SESSION_SETUP_RES_TYPE                                           (Exi_RootElementIdType)235U
#define EXI_ISO_ED2_DIS_SESSION_STOP_REQ_TYPE                                            (Exi_RootElementIdType)236U
#define EXI_ISO_ED2_DIS_SESSION_STOP_RES_TYPE                                            (Exi_RootElementIdType)237U
#define EXI_ISO_ED2_DIS_SYSTEM_STATUS_REQ_TYPE                                           (Exi_RootElementIdType)238U
#define EXI_ISO_ED2_DIS_SYSTEM_STATUS_RES_TYPE                                           (Exi_RootElementIdType)239U
#define EXI_ISO_ED2_DIS_V2G_MESSAGE_TYPE                                                 (Exi_RootElementIdType)240U
#define EXI_ISO_ED2_DIS_VEHICLE_CHECK_IN_REQ_TYPE                                        (Exi_RootElementIdType)241U
#define EXI_ISO_ED2_DIS_VEHICLE_CHECK_IN_RES_TYPE                                        (Exi_RootElementIdType)242U
#define EXI_ISO_ED2_DIS_VEHICLE_CHECK_OUT_REQ_TYPE                                       (Exi_RootElementIdType)243U
#define EXI_ISO_ED2_DIS_VEHICLE_CHECK_OUT_RES_TYPE                                       (Exi_RootElementIdType)244U
#define EXI_ISO_ED2_DIS_VOLTAGE_ACREQ_ALIGNMENT_CHECK_MECHANISM_TYPE                     (Exi_RootElementIdType)245U
#define EXI_ISO_ED2_DIS_VOLTAGE_ACRES_ALIGNMENT_CHECK_MECHANISM_TYPE                     (Exi_RootElementIdType)246U
#define EXI_ISO_ED2_DIS_WPT_CPDREQ_ENERGY_TRANSFER_MODE_TYPE                             (Exi_RootElementIdType)247U
#define EXI_ISO_ED2_DIS_WPT_CPDRES_ENERGY_TRANSFER_MODE_TYPE                             (Exi_RootElementIdType)248U
#define EXI_ISO_ED2_DIS_WPT_SDL_ENERGY_TRANSFER_MODE_TYPE                                (Exi_RootElementIdType)249U
#define EXI_ISO_ED2_DIS_WELDING_DETECTION_REQ_TYPE                                       (Exi_RootElementIdType)250U
#define EXI_ISO_ED2_DIS_WELDING_DETECTION_RES_TYPE                                       (Exi_RootElementIdType)251U
/* Elements */
#define EXI_ISO_ED2_DIS_ACD_CPDREQ_ENERGY_TRANSFER_MODE_TYPE                             (Exi_RootElementIdType)564U
#define EXI_ISO_ED2_DIS_ACD_CPDRES_ENERGY_TRANSFER_MODE_TYPE                             (Exi_RootElementIdType)565U
#define EXI_ISO_ED2_DIS_ALIGNMENT_OFFSET_TYPE                                            (Exi_RootElementIdType)566U
#define EXI_ISO_ED2_DIS_ANGLE_GATO_VA_TYPE                                               (Exi_RootElementIdType)567U
#define EXI_ISO_ED2_DIS_BODY_TYPE                                                        (Exi_RootElementIdType)568U
#define EXI_ISO_ED2_DIS_BODY_ELEMENT_TYPE                                                (Exi_RootElementIdType)569U
#define EXI_ISO_ED2_DIS_BULK_CHARGING_COMPLETE_TYPE                                      (Exi_RootElementIdType)570U
#define EXI_ISO_ED2_DIS_BULK_SOC_TYPE                                                    (Exi_RootElementIdType)571U
#define EXI_ISO_ED2_DIS_BUY_BACK_TARIFF_TYPE                                             (Exi_RootElementIdType)572U
#define EXI_ISO_ED2_DIS_CERTIFICATE_TYPE                                                 (Exi_RootElementIdType)573U
#define EXI_ISO_ED2_DIS_CHARGE_PROGRESS_TYPE                                             (Exi_RootElementIdType)574U
#define EXI_ISO_ED2_DIS_CHARGED_ENERGY_READING_WH_TYPE                                   (Exi_RootElementIdType)575U
#define EXI_ISO_ED2_DIS_CHARGING_COMPLETE_TYPE                                           (Exi_RootElementIdType)576U
#define EXI_ISO_ED2_DIS_CHARGING_SESSION_TYPE                                            (Exi_RootElementIdType)577U
#define EXI_ISO_ED2_DIS_CHECK_OUT_TIME_TYPE                                              (Exi_RootElementIdType)578U
#define EXI_ISO_ED2_DIS_CONSUMPTION_COST_TYPE                                            (Exi_RootElementIdType)579U
#define EXI_ISO_ED2_DIS_CONTACT_WINDOW_XC_TYPE                                           (Exi_RootElementIdType)580U
#define EXI_ISO_ED2_DIS_CONTACT_WINDOW_YC_TYPE                                           (Exi_RootElementIdType)581U
#define EXI_ISO_ED2_DIS_CONTRACT_CERTIFICATE_CHAIN_TYPE                                  (Exi_RootElementIdType)582U
#define EXI_ISO_ED2_DIS_CONTRACT_CERTIFICATE_ENCRYPTED_PRIVATE_KEY_TYPE                  (Exi_RootElementIdType)583U
#define EXI_ISO_ED2_DIS_CONTRACT_SIGNATURE_CERT_CHAIN_TYPE                               (Exi_RootElementIdType)584U
#define EXI_ISO_ED2_DIS_COST_TYPE                                                        (Exi_RootElementIdType)585U
#define EXI_ISO_ED2_DIS_CURRENCY_TYPE                                                    (Exi_RootElementIdType)586U
#define EXI_ISO_ED2_DIS_CURRENT_RANGE_TYPE                                               (Exi_RootElementIdType)587U
#define EXI_ISO_ED2_DIS_CURRENT_SOC_TYPE                                                 (Exi_RootElementIdType)588U
#define EXI_ISO_ED2_DIS_DHPUBLIC_KEY_TYPE                                                (Exi_RootElementIdType)589U
#define EXI_ISO_ED2_DIS_DEPARTURE_TIME_TYPE                                              (Exi_RootElementIdType)590U
#define EXI_ISO_ED2_DIS_DISCHARGED_ENERGY_READING_WH_TYPE                                (Exi_RootElementIdType)591U
#define EXI_ISO_ED2_DIS_DISPLAY_PARAMETERS_TYPE                                          (Exi_RootElementIdType)592U
#define EXI_ISO_ED2_DIS_DISTANCE_TYPE                                                    (Exi_RootElementIdType)593U
#define EXI_ISO_ED2_DIS_EMAID_TYPE                                                       (Exi_RootElementIdType)594U
#define EXI_ISO_ED2_DIS_EPRICE_LEVEL_TYPE                                                (Exi_RootElementIdType)595U
#define EXI_ISO_ED2_DIS_EVASSOCIATION_STATUS_TYPE                                        (Exi_RootElementIdType)596U
#define EXI_ISO_ED2_DIS_EVCCID_TYPE                                                      (Exi_RootElementIdType)597U
#define EXI_ISO_ED2_DIS_EVCPSTATUS_TYPE                                                  (Exi_RootElementIdType)598U
#define EXI_ISO_ED2_DIS_EVCHECK_IN_STATUS_TYPE                                           (Exi_RootElementIdType)599U
#define EXI_ISO_ED2_DIS_EVCHECK_OUT_STATUS_TYPE                                          (Exi_RootElementIdType)600U
#define EXI_ISO_ED2_DIS_EVCURRENT_TO_RESS_TYPE                                           (Exi_RootElementIdType)601U
#define EXI_ISO_ED2_DIS_EVELECTRICAL_CHARGING_DEVICE_STATUS_TYPE                         (Exi_RootElementIdType)602U
#define EXI_ISO_ED2_DIS_EVERROR_STATUS_CODE_TYPE                                         (Exi_RootElementIdType)603U
#define EXI_ISO_ED2_DIS_EVID_TYPE                                                        (Exi_RootElementIdType)604U
#define EXI_ISO_ED2_DIS_EVIN_CHARGE_POSITION_TYPE                                        (Exi_RootElementIdType)605U
#define EXI_ISO_ED2_DIS_EVMAXIMUM_CHARGE_CURRENT_TYPE                                    (Exi_RootElementIdType)606U
#define EXI_ISO_ED2_DIS_EVMAXIMUM_CHARGE_POWER_TYPE                                      (Exi_RootElementIdType)607U
#define EXI_ISO_ED2_DIS_EVMAXIMUM_DISCHARGE_CURRENT_TYPE                                 (Exi_RootElementIdType)608U
#define EXI_ISO_ED2_DIS_EVMAXIMUM_DISCHARGE_POWER_TYPE                                   (Exi_RootElementIdType)609U
#define EXI_ISO_ED2_DIS_EVMAXIMUM_ENERGY_REQUEST_TYPE                                    (Exi_RootElementIdType)610U
#define EXI_ISO_ED2_DIS_EVMAXIMUM_POWER_TYPE                                             (Exi_RootElementIdType)611U
#define EXI_ISO_ED2_DIS_EVMAXIMUM_VOLTAGE_TYPE                                           (Exi_RootElementIdType)612U
#define EXI_ISO_ED2_DIS_EVMECHANICAL_CHARGING_DEVICE_STATUS_TYPE                         (Exi_RootElementIdType)613U
#define EXI_ISO_ED2_DIS_EVMINIMUM_CHARGE_CURRENT_TYPE                                    (Exi_RootElementIdType)614U
#define EXI_ISO_ED2_DIS_EVMINIMUM_CHARGE_POWER_TYPE                                      (Exi_RootElementIdType)615U
#define EXI_ISO_ED2_DIS_EVMINIMUM_DISCHARGE_CURRENT_TYPE                                 (Exi_RootElementIdType)616U
#define EXI_ISO_ED2_DIS_EVMINIMUM_DISCHARGE_POWER_TYPE                                   (Exi_RootElementIdType)617U
#define EXI_ISO_ED2_DIS_EVMINIMUM_ENERGY_REQUEST_TYPE                                    (Exi_RootElementIdType)618U
#define EXI_ISO_ED2_DIS_EVMINIMUM_POWER_TYPE                                             (Exi_RootElementIdType)619U
#define EXI_ISO_ED2_DIS_EVMINIMUM_VOLTAGE_TYPE                                           (Exi_RootElementIdType)620U
#define EXI_ISO_ED2_DIS_EVMOBILITY_STATUS_TYPE                                           (Exi_RootElementIdType)621U
#define EXI_ISO_ED2_DIS_EVOEMSTATUS_TYPE                                                 (Exi_RootElementIdType)622U
#define EXI_ISO_ED2_DIS_EVOPERATION_TYPE                                                 (Exi_RootElementIdType)623U
#define EXI_ISO_ED2_DIS_EVORIENTATION_TYPE                                               (Exi_RootElementIdType)624U
#define EXI_ISO_ED2_DIS_EVPOWER_PROFILE_TYPE                                             (Exi_RootElementIdType)625U
#define EXI_ISO_ED2_DIS_EVPOWER_PROFILE_ENTRY_TYPE                                       (Exi_RootElementIdType)626U
#define EXI_ISO_ED2_DIS_EVPOWER_TO_RESS_TYPE                                             (Exi_RootElementIdType)627U
#define EXI_ISO_ED2_DIS_EVPRESENT_ACTIVE_POWER_TYPE                                      (Exi_RootElementIdType)628U
#define EXI_ISO_ED2_DIS_EVPRESENT_REACTIVE_POWER_TYPE                                    (Exi_RootElementIdType)629U
#define EXI_ISO_ED2_DIS_EVPROCESSING_TYPE                                                (Exi_RootElementIdType)630U
#define EXI_ISO_ED2_DIS_EVRECEIVED_DCPOWER_TYPE                                          (Exi_RootElementIdType)631U
#define EXI_ISO_ED2_DIS_EVRELATIVE_ACD_X_DEVIATION_TYPE                                  (Exi_RootElementIdType)632U
#define EXI_ISO_ED2_DIS_EVRELATIVE_ACD_Y_DEVIATION_TYPE                                  (Exi_RootElementIdType)633U
#define EXI_ISO_ED2_DIS_EVREQUESTED_POWER_TYPE                                           (Exi_RootElementIdType)634U
#define EXI_ISO_ED2_DIS_EVSECHECK_OUT_STATUS_TYPE                                        (Exi_RootElementIdType)635U
#define EXI_ISO_ED2_DIS_EVSECOIL_CURRENT_TYPE                                            (Exi_RootElementIdType)636U
#define EXI_ISO_ED2_DIS_EVSECURRENT_LIMIT_ACHIEVED_TYPE                                  (Exi_RootElementIdType)637U
#define EXI_ISO_ED2_DIS_EVSEDCPRIMARY_POWER_TYPE                                         (Exi_RootElementIdType)638U
#define EXI_ISO_ED2_DIS_EVSEDISABLED_TYPE                                                (Exi_RootElementIdType)639U
#define EXI_ISO_ED2_DIS_EVSEELECTRICAL_CHARGING_DEVICE_STATUS_TYPE                       (Exi_RootElementIdType)640U
#define EXI_ISO_ED2_DIS_EVSEEMERGENCY_SHUTDOWN_TYPE                                      (Exi_RootElementIdType)641U
#define EXI_ISO_ED2_DIS_EVSEID_TYPE                                                      (Exi_RootElementIdType)642U
#define EXI_ISO_ED2_DIS_EVSEISOLATION_STATUS_TYPE                                        (Exi_RootElementIdType)643U
#define EXI_ISO_ED2_DIS_EVSEMALFUNCTION_TYPE                                             (Exi_RootElementIdType)644U
#define EXI_ISO_ED2_DIS_EVSEMAXIMUM_CHARGE_CURRENT_TYPE                                  (Exi_RootElementIdType)645U
#define EXI_ISO_ED2_DIS_EVSEMAXIMUM_CHARGE_POWER_TYPE                                    (Exi_RootElementIdType)646U
#define EXI_ISO_ED2_DIS_EVSEMAXIMUM_DISCHARGE_CURRENT_TYPE                               (Exi_RootElementIdType)647U
#define EXI_ISO_ED2_DIS_EVSEMAXIMUM_DISCHARGE_POWER_TYPE                                 (Exi_RootElementIdType)648U
#define EXI_ISO_ED2_DIS_EVSEMAXIMUM_POWER_TYPE                                           (Exi_RootElementIdType)649U
#define EXI_ISO_ED2_DIS_EVSEMAXIMUM_VOLTAGE_TYPE                                         (Exi_RootElementIdType)650U
#define EXI_ISO_ED2_DIS_EVSEMECHANICAL_CHARGING_DEVICE_STATUS_TYPE                       (Exi_RootElementIdType)651U
#define EXI_ISO_ED2_DIS_EVSEMINIMUM_CHARGE_CURRENT_TYPE                                  (Exi_RootElementIdType)652U
#define EXI_ISO_ED2_DIS_EVSEMINIMUM_DISCHARGE_CURRENT_TYPE                               (Exi_RootElementIdType)653U
#define EXI_ISO_ED2_DIS_EVSEMINIMUM_POWER_TYPE                                           (Exi_RootElementIdType)654U
#define EXI_ISO_ED2_DIS_EVSEMINIMUM_VOLTAGE_TYPE                                         (Exi_RootElementIdType)655U
#define EXI_ISO_ED2_DIS_EVSENOMINAL_FREQUENCY_TYPE                                       (Exi_RootElementIdType)656U
#define EXI_ISO_ED2_DIS_EVSENOMINAL_VOLTAGE_TYPE                                         (Exi_RootElementIdType)657U
#define EXI_ISO_ED2_DIS_EVSENOTIFICATION_TYPE                                            (Exi_RootElementIdType)658U
#define EXI_ISO_ED2_DIS_EVSEPOWER_FROM_GRID_TYPE                                         (Exi_RootElementIdType)659U
#define EXI_ISO_ED2_DIS_EVSEPOWER_LIMIT_TYPE                                             (Exi_RootElementIdType)660U
#define EXI_ISO_ED2_DIS_EVSEPOWER_LIMIT_ACHIEVED_TYPE                                    (Exi_RootElementIdType)661U
#define EXI_ISO_ED2_DIS_EVSEPRESENT_CURRENT_TYPE                                         (Exi_RootElementIdType)662U
#define EXI_ISO_ED2_DIS_EVSEPRESENT_VOLTAGE_TYPE                                         (Exi_RootElementIdType)663U
#define EXI_ISO_ED2_DIS_EVSEPROCESSING_TYPE                                              (Exi_RootElementIdType)664U
#define EXI_ISO_ED2_DIS_EVSEREADY_TO_CHARGE_TYPE                                         (Exi_RootElementIdType)665U
#define EXI_ISO_ED2_DIS_EVSESTATUS_TYPE                                                  (Exi_RootElementIdType)666U
#define EXI_ISO_ED2_DIS_EVSETARGET_ACTIVE_POWER_TYPE                                     (Exi_RootElementIdType)667U
#define EXI_ISO_ED2_DIS_EVSETARGET_FREQUENCY_TYPE                                        (Exi_RootElementIdType)668U
#define EXI_ISO_ED2_DIS_EVSETARGET_REACTIVE_POWER_TYPE                                   (Exi_RootElementIdType)669U
#define EXI_ISO_ED2_DIS_EVSETIMEOUT_TYPE                                                 (Exi_RootElementIdType)670U
#define EXI_ISO_ED2_DIS_EVSEUTILITY_INTERRUPT_EVENT_TYPE                                 (Exi_RootElementIdType)671U
#define EXI_ISO_ED2_DIS_EVSEVOLTAGE_LIMIT_ACHIEVED_TYPE                                  (Exi_RootElementIdType)672U
#define EXI_ISO_ED2_DIS_EVSTATUS_IMMOBILIZATION_REQUEST_TYPE                             (Exi_RootElementIdType)673U
#define EXI_ISO_ED2_DIS_EVSTATUS_IMMOBILIZED_TYPE                                        (Exi_RootElementIdType)674U
#define EXI_ISO_ED2_DIS_EVSTATUS_RESSSOC_TYPE                                            (Exi_RootElementIdType)675U
#define EXI_ISO_ED2_DIS_EVSTATUS_READY_TO_CHARGE_TYPE                                    (Exi_RootElementIdType)676U
#define EXI_ISO_ED2_DIS_EVSTATUS_WLANSTRENGTH_TYPE                                       (Exi_RootElementIdType)677U
#define EXI_ISO_ED2_DIS_EVTARGET_CURRENT_TYPE                                            (Exi_RootElementIdType)678U
#define EXI_ISO_ED2_DIS_EVTARGET_ENERGY_REQUEST_TYPE                                     (Exi_RootElementIdType)679U
#define EXI_ISO_ED2_DIS_EVTARGET_POWER_TYPE                                              (Exi_RootElementIdType)680U
#define EXI_ISO_ED2_DIS_EVTARGET_VOLTAGE_TYPE                                            (Exi_RootElementIdType)681U
#define EXI_ISO_ED2_DIS_EVTECHNICAL_STATUS_TYPE                                          (Exi_RootElementIdType)682U
#define EXI_ISO_ED2_DIS_EVVOLTAGE_TYPE                                                   (Exi_RootElementIdType)683U
#define EXI_ISO_ED2_DIS_EVVOLTAGE_TO_RESS_TYPE                                           (Exi_RootElementIdType)684U
#define EXI_ISO_ED2_DIS_EFFECTIVE_RADIATED_POWER_TYPE                                    (Exi_RootElementIdType)685U
#define EXI_ISO_ED2_DIS_ENERGY_TRANSFER_SERVICE_LIST_TYPE                                (Exi_RootElementIdType)686U
#define EXI_ISO_ED2_DIS_EXPONENT_TYPE                                                    (Exi_RootElementIdType)687U
#define EXI_ISO_ED2_DIS_FODSTATUS_TYPE                                                   (Exi_RootElementIdType)688U
#define EXI_ISO_ED2_DIS_FINE_POSITIONING_METHOD_TYPE                                     (Exi_RootElementIdType)689U
#define EXI_ISO_ED2_DIS_FOREIGN_OBJECT_DETECTED_TYPE                                     (Exi_RootElementIdType)690U
#define EXI_ISO_ED2_DIS_FREE_SERVICE_TYPE                                                (Exi_RootElementIdType)691U
#define EXI_ISO_ED2_DIS_FREQUENCY_CHANNEL_TYPE                                           (Exi_RootElementIdType)692U
#define EXI_ISO_ED2_DIS_GACOIL_CURRENT_TYPE                                              (Exi_RootElementIdType)693U
#define EXI_ISO_ED2_DIS_GAFREQUENCY_TYPE                                                 (Exi_RootElementIdType)694U
#define EXI_ISO_ED2_DIS_GAID_TYPE                                                        (Exi_RootElementIdType)695U
#define EXI_ISO_ED2_DIS_GAINPUT_POWER_TYPE                                               (Exi_RootElementIdType)696U
#define EXI_ISO_ED2_DIS_GEN_CHALLENGE_TYPE                                               (Exi_RootElementIdType)697U
#define EXI_ISO_ED2_DIS_GENERIC_TYPE                                                     (Exi_RootElementIdType)698U
#define EXI_ISO_ED2_DIS_GENERIC_PARAMETERS_TYPE                                          (Exi_RootElementIdType)699U
#define EXI_ISO_ED2_DIS_HEADER_TYPE                                                      (Exi_RootElementIdType)700U
#define EXI_ISO_ED2_DIS_IDENTIFICATION_OPTION_TYPE                                       (Exi_RootElementIdType)701U
#define EXI_ISO_ED2_DIS_IDENTIFICATION_OPTION_LIST_TYPE                                  (Exi_RootElementIdType)702U
#define EXI_ISO_ED2_DIS_INLET_HOT_TYPE                                                   (Exi_RootElementIdType)703U
#define EXI_ISO_ED2_DIS_LAGGING_ENERGY_READING_VARH_TYPE                                 (Exi_RootElementIdType)704U
#define EXI_ISO_ED2_DIS_LEADING_ENERGY_READING_VARH_TYPE                                 (Exi_RootElementIdType)705U
#define EXI_ISO_ED2_DIS_LIST_OF_ROOT_CERTIFICATE_IDS_TYPE                                (Exi_RootElementIdType)706U
#define EXI_ISO_ED2_DIS_LIVING_OBJECT_DETECTED_TYPE                                      (Exi_RootElementIdType)707U
#define EXI_ISO_ED2_DIS_MAGNETIC_VECTOR_TYPE                                             (Exi_RootElementIdType)708U
#define EXI_ISO_ED2_DIS_MAGNETIC_VECTOR_LIST_TYPE                                        (Exi_RootElementIdType)709U
#define EXI_ISO_ED2_DIS_MAGNETIC_VECTOR_SETUP_TYPE                                       (Exi_RootElementIdType)710U
#define EXI_ISO_ED2_DIS_MAGNETIC_VECTOR_SETUP_LIST_TYPE                                  (Exi_RootElementIdType)711U
#define EXI_ISO_ED2_DIS_MAX_SUPPORTING_POINTS_TYPE                                       (Exi_RootElementIdType)712U
#define EXI_ISO_ED2_DIS_MAXIMUM_SUPPORTED_CERTIFICATE_CHAINS_TYPE                        (Exi_RootElementIdType)713U
#define EXI_ISO_ED2_DIS_MEASUREMENT_DATA_TYPE                                            (Exi_RootElementIdType)714U
#define EXI_ISO_ED2_DIS_MEASUREMENT_DATA_LIST_TYPE                                       (Exi_RootElementIdType)715U
#define EXI_ISO_ED2_DIS_METER_ID_TYPE                                                    (Exi_RootElementIdType)716U
#define EXI_ISO_ED2_DIS_METER_INFO_TYPE                                                  (Exi_RootElementIdType)717U
#define EXI_ISO_ED2_DIS_METER_SIGNATURE_TYPE                                             (Exi_RootElementIdType)718U
#define EXI_ISO_ED2_DIS_METER_STATUS_TYPE                                                (Exi_RootElementIdType)719U
#define EXI_ISO_ED2_DIS_METERING_RECEIPT_REQUESTED_TYPE                                  (Exi_RootElementIdType)720U
#define EXI_ISO_ED2_DIS_MINIMUM_SOC_TYPE                                                 (Exi_RootElementIdType)721U
#define EXI_ISO_ED2_DIS_NOTIFICATION_MAX_DELAY_TYPE                                      (Exi_RootElementIdType)722U
#define EXI_ISO_ED2_DIS_NUM_EPRICE_LEVELS_TYPE                                           (Exi_RootElementIdType)723U
#define EXI_ISO_ED2_DIS_NUMBER_OF_SENSORS_TYPE                                           (Exi_RootElementIdType)724U
#define EXI_ISO_ED2_DIS_NUMBER_OF_SIGNAL_PACKAGES_TYPE                                   (Exi_RootElementIdType)725U
#define EXI_ISO_ED2_DIS_OEMPROVISIONING_CERTIFICATE_CHAIN_TYPE                           (Exi_RootElementIdType)726U
#define EXI_ISO_ED2_DIS_PACKAGE_INDEX_TYPE                                               (Exi_RootElementIdType)727U
#define EXI_ISO_ED2_DIS_PACKAGE_SEPARATION_TIME_TYPE                                     (Exi_RootElementIdType)728U
#define EXI_ISO_ED2_DIS_PAIRING_PARAMETERS_TYPE                                          (Exi_RootElementIdType)729U
#define EXI_ISO_ED2_DIS_PARAMETER_TYPE                                                   (Exi_RootElementIdType)730U
#define EXI_ISO_ED2_DIS_PARAMETER_SET_TYPE                                               (Exi_RootElementIdType)731U
#define EXI_ISO_ED2_DIS_PARAMETER_SET_ID_TYPE                                            (Exi_RootElementIdType)732U
#define EXI_ISO_ED2_DIS_PARKING_METHOD_TYPE                                              (Exi_RootElementIdType)733U
#define EXI_ISO_ED2_DIS_POWER_TYPE                                                       (Exi_RootElementIdType)734U
#define EXI_ISO_ED2_DIS_POWER_DEMAND_PARAMETERS_TYPE                                     (Exi_RootElementIdType)735U
#define EXI_ISO_ED2_DIS_POWER_DISCHARGE_SCHEDULE_TYPE                                    (Exi_RootElementIdType)736U
#define EXI_ISO_ED2_DIS_POWER_SCHEDULE_TYPE                                              (Exi_RootElementIdType)737U
#define EXI_ISO_ED2_DIS_PREAMBLE_TYPE                                                    (Exi_RootElementIdType)738U
#define EXI_ISO_ED2_DIS_PRIORITIZED_EMAIDS_TYPE                                          (Exi_RootElementIdType)739U
#define EXI_ISO_ED2_DIS_RECEIPT_REQUIRED_TYPE                                            (Exi_RootElementIdType)740U
#define EXI_ISO_ED2_DIS_REMAINING_CONTRACT_CERTIFICATE_CHAINS_TYPE                       (Exi_RootElementIdType)741U
#define EXI_ISO_ED2_DIS_REMAINING_TIME_TO_BULK_SOC_TYPE                                  (Exi_RootElementIdType)742U
#define EXI_ISO_ED2_DIS_REMAINING_TIME_TO_MAXIMUM_SOC_TYPE                               (Exi_RootElementIdType)743U
#define EXI_ISO_ED2_DIS_REMAINING_TIME_TO_MINIMUM_SOC_TYPE                               (Exi_RootElementIdType)744U
#define EXI_ISO_ED2_DIS_REMAINING_TIME_TO_TARGET_SOC_TYPE                                (Exi_RootElementIdType)745U
#define EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE                                               (Exi_RootElementIdType)746U
#define EXI_ISO_ED2_DIS_ROOT_CERTIFICATE_ID_TYPE                                         (Exi_RootElementIdType)747U
#define EXI_ISO_ED2_DIS_ROTATION_VATO_GA_TYPE                                            (Exi_RootElementIdType)748U
#define EXI_ISO_ED2_DIS_SAPROVISIONING_CERTIFICATE_CHAIN_TYPE                            (Exi_RootElementIdType)749U
#define EXI_ISO_ED2_DIS_SALES_TARIFF_TYPE                                                (Exi_RootElementIdType)750U
#define EXI_ISO_ED2_DIS_SALES_TARIFF_DESCRIPTION_TYPE                                    (Exi_RootElementIdType)751U
#define EXI_ISO_ED2_DIS_SALES_TARIFF_ID_TYPE                                             (Exi_RootElementIdType)752U
#define EXI_ISO_ED2_DIS_SAMPLING_RATE_TYPE                                               (Exi_RootElementIdType)753U
#define EXI_ISO_ED2_DIS_SCHEDULE_ORIGIN_TYPE                                             (Exi_RootElementIdType)754U
#define EXI_ISO_ED2_DIS_SCHEDULE_TUPLE_TYPE                                              (Exi_RootElementIdType)755U
#define EXI_ISO_ED2_DIS_SCHEDULE_TUPLE_ID_TYPE                                           (Exi_RootElementIdType)756U
#define EXI_ISO_ED2_DIS_SELECTED_ENERGY_TRANSFER_SERVICE_TYPE                            (Exi_RootElementIdType)757U
#define EXI_ISO_ED2_DIS_SELECTED_IDENTIFICATION_OPTION_TYPE                              (Exi_RootElementIdType)758U
#define EXI_ISO_ED2_DIS_SELECTED_SERVICE_TYPE                                            (Exi_RootElementIdType)759U
#define EXI_ISO_ED2_DIS_SELECTED_VASLIST_TYPE                                            (Exi_RootElementIdType)760U
#define EXI_ISO_ED2_DIS_SENSOR_TYPE                                                      (Exi_RootElementIdType)761U
#define EXI_ISO_ED2_DIS_SENSOR_ID_TYPE                                                   (Exi_RootElementIdType)762U
#define EXI_ISO_ED2_DIS_SENSOR_LIST_TYPE                                                 (Exi_RootElementIdType)763U
#define EXI_ISO_ED2_DIS_SENSOR_MEASUREMENTS_TYPE                                         (Exi_RootElementIdType)764U
#define EXI_ISO_ED2_DIS_SENSOR_ORDER_TYPE                                                (Exi_RootElementIdType)765U
#define EXI_ISO_ED2_DIS_SENSOR_ORIENTATION_TYPE                                          (Exi_RootElementIdType)766U
#define EXI_ISO_ED2_DIS_SENSOR_PACKAGE_TYPE                                              (Exi_RootElementIdType)767U
#define EXI_ISO_ED2_DIS_SENSOR_PACKAGE_LIST_TYPE                                         (Exi_RootElementIdType)768U
#define EXI_ISO_ED2_DIS_SENSOR_POSITION_TYPE                                             (Exi_RootElementIdType)769U
#define EXI_ISO_ED2_DIS_CARTESIAN_COORDINATES_TYPE                                       (Exi_RootElementIdType)770U
#define EXI_UNSIGNED_BYTE_TYPE                                                           (Exi_RootElementIdType)771U
#define EXI_ISO_ED2_DIS_SERVICE_TYPE                                                     (Exi_RootElementIdType)772U
#define EXI_ISO_ED2_DIS_SERVICE_ID_TYPE                                                  (Exi_RootElementIdType)773U
#define EXI_ISO_ED2_DIS_SERVICE_PARAMETER_LIST_TYPE                                      (Exi_RootElementIdType)774U
#define EXI_ISO_ED2_DIS_SESSION_ID_TYPE                                                  (Exi_RootElementIdType)775U
#define EXI_ISO_ED2_DIS_SIGNAL_FREQUENCY_TYPE                                            (Exi_RootElementIdType)776U
#define EXI_ISO_ED2_DIS_SIGNAL_PULSE_DURATION_TYPE                                       (Exi_RootElementIdType)777U
#define EXI_ISO_ED2_DIS_SIGNAL_SEPARATION_TIME_TYPE                                      (Exi_RootElementIdType)778U
#define EXI_ISO_ED2_DIS_SIGNATURE_TYPE                                                   (Exi_RootElementIdType)779U
#define EXI_ISO_ED2_DIS_STANDBY_TYPE                                                     (Exi_RootElementIdType)780U
#define EXI_ISO_ED2_DIS_SUB_CERTIFICATES_TYPE                                            (Exi_RootElementIdType)781U
#define EXI_ISO_ED2_DIS_SUPPORTED_SERVICE_IDS_TYPE                                       (Exi_RootElementIdType)782U
#define EXI_ISO_ED2_DIS_TMETER_TYPE                                                      (Exi_RootElementIdType)783U
#define EXI_ISO_ED2_DIS_TARGET_OFFSET_TYPE                                               (Exi_RootElementIdType)784U
#define EXI_ISO_ED2_DIS_TARGET_OFFSETX_TYPE                                              (Exi_RootElementIdType)785U
#define EXI_ISO_ED2_DIS_TARGET_OFFSETY_TYPE                                              (Exi_RootElementIdType)786U
#define EXI_ISO_ED2_DIS_TARGET_SOC_TYPE                                                  (Exi_RootElementIdType)787U
#define EXI_ISO_ED2_DIS_TIME_INTERVAL_TYPE                                               (Exi_RootElementIdType)788U
#define EXI_ISO_ED2_DIS_TIME_STAMP_TYPE                                                  (Exi_RootElementIdType)789U
#define EXI_ISO_ED2_DIS_USED_FREQUENCIES_TYPE                                            (Exi_RootElementIdType)790U
#define EXI_ISO_ED2_DIS_VACURRENT_TO_EV_TYPE                                             (Exi_RootElementIdType)791U
#define EXI_ISO_ED2_DIS_VAPOWER_RECEIVED_TYPE                                            (Exi_RootElementIdType)792U
#define EXI_ISO_ED2_DIS_VASLIST_TYPE                                                     (Exi_RootElementIdType)793U
#define EXI_ISO_ED2_DIS_VAVOLTAGE_TO_EV_TYPE                                             (Exi_RootElementIdType)794U
#define EXI_ISO_ED2_DIS_VALUE_TYPE                                                       (Exi_RootElementIdType)795U
#define EXI_ISO_ED2_DIS_VEHICLE_SPACE_TYPE                                               (Exi_RootElementIdType)796U
#define EXI_ISO_ED2_DIS_XCOORDINATE_TYPE                                                 (Exi_RootElementIdType)797U
#define EXI_ISO_ED2_DIS_YCOORDINATE_TYPE                                                 (Exi_RootElementIdType)798U
#define EXI_ISO_ED2_DIS_ZCOORDINATE_TYPE                                                 (Exi_RootElementIdType)799U
#define EXI_ISO_ED2_DIS_AMOUNT_TYPE                                                      (Exi_RootElementIdType)800U
#define EXI_ISO_ED2_DIS_BOOL_VALUE_TYPE                                                  (Exi_RootElementIdType)801U
#define EXI_ISO_ED2_DIS_BYTE_VALUE_TYPE                                                  (Exi_RootElementIdType)802U
#define EXI_ISO_ED2_DIS_COST_KIND_TYPE                                                   (Exi_RootElementIdType)803U
#define EXI_ISO_ED2_DIS_DURATION_TYPE                                                    (Exi_RootElementIdType)804U
#define EXI_ISO_ED2_DIS_INT_VALUE_TYPE                                                   (Exi_RootElementIdType)805U
#define EXI_ISO_ED2_DIS_PHYSICAL_VALUE_TYPE                                              (Exi_RootElementIdType)806U
#define EXI_ISO_ED2_DIS_SHORT_VALUE_TYPE                                                 (Exi_RootElementIdType)807U
#define EXI_ISO_ED2_DIS_START_TYPE                                                       (Exi_RootElementIdType)808U
#define EXI_ISO_ED2_DIS_START_VALUE_TYPE                                                 (Exi_RootElementIdType)809U
#define EXI_ISO_ED2_DIS_STRING_VALUE_TYPE                                                (Exi_RootElementIdType)810U
#define EXI_SCHEMA_SET_ISO_ED2_DIS_TYPE                                                  (Exi_RootElementIdType)4U
#ifndef EXI_MAX_BUFFER_SIZE_ISO_ED2_DIS_ATTRIBUTEID
  #define EXI_MAX_BUFFER_SIZE_ISO_ED2_DIS_ATTRIBUTEID EXI_MAX_BUFFER_SIZE_UNBOUNDED
#endif
#ifndef EXI_MAX_BUFFER_SIZE_ISO_ED2_DIS_ATTRIBUTENAME
  #define EXI_MAX_BUFFER_SIZE_ISO_ED2_DIS_ATTRIBUTENAME EXI_MAX_BUFFER_SIZE_UNBOUNDED
#endif

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
typedef struct Exi_ISO_ED2_DIS_sessionIDType Exi_ISO_ED2_DIS_sessionIDType;

struct Exi_ISO_ED2_DIS_sessionIDType
{
  uint16 Length;
  uint8 Buffer[8];
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_MessageHeaderType Exi_ISO_ED2_DIS_MessageHeaderType;

struct Exi_ISO_ED2_DIS_MessageHeaderType
{
  Exi_ISO_ED2_DIS_sessionIDType* SessionID;
  Exi_XMLSIG_SignatureType* Signature;
  Exi_UInt64 TimeStamp;
  Exi_BitType SignatureFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_BodyBaseType;

typedef struct Exi_ISO_ED2_DIS_BodyType Exi_ISO_ED2_DIS_BodyType;

struct Exi_ISO_ED2_DIS_BodyType
{
  Exi_ISO_ED2_DIS_BodyBaseType* BodyElement;
  Exi_RootElementIdType BodyElementElementId;
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_V2G_MessageType Exi_ISO_ED2_DIS_V2G_MessageType;

struct Exi_ISO_ED2_DIS_V2G_MessageType
{
  Exi_ISO_ED2_DIS_BodyType* Body;
  Exi_ISO_ED2_DIS_MessageHeaderType* Header;
};

typedef struct Exi_ISO_ED2_DIS_evccIDType Exi_ISO_ED2_DIS_evccIDType;

struct Exi_ISO_ED2_DIS_evccIDType
{
  uint16 Length;
  uint8 Buffer[6];
};

typedef void Exi_ISO_ED2_DIS_SSEnergyTransferModeType;

typedef struct Exi_ISO_ED2_DIS_SessionSetupReqType Exi_ISO_ED2_DIS_SessionSetupReqType;

struct Exi_ISO_ED2_DIS_SessionSetupReqType
{
  Exi_ISO_ED2_DIS_evccIDType* EVCCID;
  Exi_ISO_ED2_DIS_SSEnergyTransferModeType* SSEnergyTransferMode;
  Exi_RootElementIdType SSEnergyTransferModeElementId;
  Exi_BitType SSEnergyTransferModeFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_OK = 0u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_OK_NEW_SESSION_ESTABLISHED = 1u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_OK_OLD_SESSION_JOINED = 2u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_OK_CERTIFICATE_EXPIRES_SOON = 3u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_OK_ISOLATION_VALID = 4u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_OK_ISOLATION_WARNING = 5u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_WARNING_CERTIFICATE_EXPIRED = 6u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_WARNING_NO_CERTIFICATE_AVAILABLE = 7u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_WARNING_CERT_VALIDATION_ERROR = 8u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_WARNING_CERT_VERIFICATION_ERROR = 9u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_WARNING_CONTRACT_CANCELED = 10u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED = 11u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_SEQUENCE_ERROR = 12u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_SERVICE_IDINVALID = 13u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_UNKNOWN_SESSION = 14u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_SERVICE_SELECTION_INVALID = 15u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_SIGNATURE_ERROR = 16u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_IDENTIFICATION_SELECTION_INVALID = 17u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_CHALLENGE_INVALID = 18u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_WRONG_CHARGE_PARAMETER = 19u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_ISOLATION_FAULT = 20u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_POWER_DELIVERY_NOT_APPLIED = 21u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_TARIFF_SELECTION_INVALID = 22u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_CHARGING_PROFILE_INVALID = 23u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_METERING_SIGNATURE_NOT_VALID = 24u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_NO_CHARGE_SERVICE_SELECTED = 25u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_WRONG_ENERGY_TRANSFER_MODE = 26u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_CONTACTOR_ERROR = 27u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_CERTIFICATE_REVOKED = 28u,
  EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED_CERTIFICATE_NOT_YET_VALID = 29u
} Exi_ISO_ED2_DIS_responseCodeType;

typedef enum
{
  EXI_ISO_ED2_DIS_EVSENOTIFICATION_TYPE_STOP_CHARGING = 0u,
  EXI_ISO_ED2_DIS_EVSENOTIFICATION_TYPE_RE_NEGOTIATION = 1u
} Exi_ISO_ED2_DIS_EVSENotificationType;

typedef struct Exi_ISO_ED2_DIS_EVSEStatusType Exi_ISO_ED2_DIS_EVSEStatusType;

struct Exi_ISO_ED2_DIS_EVSEStatusType
{
  uint16 NotificationMaxDelay;
  Exi_ISO_ED2_DIS_EVSENotificationType EVSENotification;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_evseIDType Exi_ISO_ED2_DIS_evseIDType;

struct Exi_ISO_ED2_DIS_evseIDType
{
  uint16 Length;
  uint8 Buffer[37];
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 3 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 3 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_SessionSetupResType Exi_ISO_ED2_DIS_SessionSetupResType;

struct Exi_ISO_ED2_DIS_SessionSetupResType
{
  Exi_ISO_ED2_DIS_evseIDType* EVSEID;
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ServiceIDListType Exi_ISO_ED2_DIS_ServiceIDListType;

struct Exi_ISO_ED2_DIS_ServiceIDListType
{
  uint16 ServiceID[10];
  uint8 ServiceIDCount;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ServiceDiscoveryReqType Exi_ISO_ED2_DIS_ServiceDiscoveryReqType;

struct Exi_ISO_ED2_DIS_ServiceDiscoveryReqType
{
  Exi_ISO_ED2_DIS_ServiceIDListType* SupportedServiceIDs;
  Exi_BitType SupportedServiceIDsFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_IDENTIFICATION_OPTION_TYPE_CONTRACT = 0u,
  EXI_ISO_ED2_DIS_IDENTIFICATION_OPTION_TYPE_EXTERNAL_IDENTIFICATION = 1u
} Exi_ISO_ED2_DIS_identificationOptionType;

typedef struct Exi_ISO_ED2_DIS_IdentificationOptionListType Exi_ISO_ED2_DIS_IdentificationOptionListType;

struct Exi_ISO_ED2_DIS_IdentificationOptionListType
{
  Exi_ISO_ED2_DIS_identificationOptionType IdentificationOption[2];
  uint8 IdentificationOptionCount;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ServiceType Exi_ISO_ED2_DIS_ServiceType;

struct Exi_ISO_ED2_DIS_ServiceType
{
  struct Exi_ISO_ED2_DIS_ServiceType* NextServicePtr;
  uint16 ServiceID;
  boolean FreeService;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 3 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 3 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ServiceListType Exi_ISO_ED2_DIS_ServiceListType;

struct Exi_ISO_ED2_DIS_ServiceListType
{
  Exi_ISO_ED2_DIS_ServiceType* Service;
};

typedef struct Exi_ISO_ED2_DIS_ServiceDiscoveryResType Exi_ISO_ED2_DIS_ServiceDiscoveryResType;

struct Exi_ISO_ED2_DIS_ServiceDiscoveryResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_ServiceListType* EnergyTransferServiceList;
  Exi_ISO_ED2_DIS_IdentificationOptionListType* IdentificationOptionList;
  Exi_ISO_ED2_DIS_ServiceListType* VASList;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
  Exi_BitType VASListFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_SDlEnergyTransferModeType;

typedef struct Exi_ISO_ED2_DIS_ServiceDetailReqType Exi_ISO_ED2_DIS_ServiceDetailReqType;

struct Exi_ISO_ED2_DIS_ServiceDetailReqType
{
  Exi_ISO_ED2_DIS_SDlEnergyTransferModeType* SDlEnergyTransferMode;
  Exi_RootElementIdType SDlEnergyTransferModeElementId;
  uint16 ServiceID;
  Exi_BitType SDlEnergyTransferModeFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_AttributeNameType Exi_ISO_ED2_DIS_AttributeNameType;

struct Exi_ISO_ED2_DIS_AttributeNameType
{
  uint16 Length;
  uint8 Buffer[EXI_MAX_BUFFER_SIZE_ISO_ED2_DIS_ATTRIBUTENAME];
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( ( EXI_MAX_BUFFER_SIZE_ISO_ED2_DIS_ATTRIBUTENAME ) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( ( EXI_MAX_BUFFER_SIZE_ISO_ED2_DIS_ATTRIBUTENAME ) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( ( EXI_MAX_BUFFER_SIZE_ISO_ED2_DIS_ATTRIBUTENAME ) + 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_PhysicalValueType Exi_ISO_ED2_DIS_PhysicalValueType;

struct Exi_ISO_ED2_DIS_PhysicalValueType
{
  struct Exi_ISO_ED2_DIS_PhysicalValueType* NextEVSEMaximumChargeCurrentPtr;
  struct Exi_ISO_ED2_DIS_PhysicalValueType* NextEVSEMaximumDischargeCurrentPtr;
  struct Exi_ISO_ED2_DIS_PhysicalValueType* NextPowerPtr;
  sint16 Value;
  sint8 Exponent;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 3 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 3 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ParameterChoiceType Exi_ISO_ED2_DIS_ParameterChoiceType;

struct Exi_ISO_ED2_DIS_ParameterChoiceType
{
  union
  {
    Exi_ISO_ED2_DIS_PhysicalValueType* physicalValue;
    Exi_stringType* stringValue;
    sint32 intValue;
    sint16 shortValue;
    boolean boolValue;
    sint8 byteValue;
  } ChoiceValue;
  Exi_BitType physicalValueFlag : 1;
  Exi_BitType stringValueFlag : 1;
  Exi_BitType intValueFlag : 1;
  Exi_BitType shortValueFlag : 1;
  Exi_BitType boolValueFlag : 1;
  Exi_BitType byteValueFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ParameterType Exi_ISO_ED2_DIS_ParameterType;

struct Exi_ISO_ED2_DIS_ParameterType
{
  Exi_ISO_ED2_DIS_ParameterChoiceType* ChoiceElement;
  Exi_ISO_ED2_DIS_AttributeNameType* Name;
  struct Exi_ISO_ED2_DIS_ParameterType* NextParameterPtr;
};

typedef struct Exi_ISO_ED2_DIS_ParameterSetType Exi_ISO_ED2_DIS_ParameterSetType;

struct Exi_ISO_ED2_DIS_ParameterSetType
{
  struct Exi_ISO_ED2_DIS_ParameterSetType* NextGenericPtr;
  struct Exi_ISO_ED2_DIS_ParameterSetType* NextParameterSetPtr;
  Exi_ISO_ED2_DIS_ParameterType* Parameter;
  uint16 ParameterSetID;
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ServiceParameterListType Exi_ISO_ED2_DIS_ServiceParameterListType;

struct Exi_ISO_ED2_DIS_ServiceParameterListType
{
  Exi_ISO_ED2_DIS_ParameterSetType* ParameterSet;
};

typedef struct Exi_ISO_ED2_DIS_ServiceDetailResType Exi_ISO_ED2_DIS_ServiceDetailResType;

struct Exi_ISO_ED2_DIS_ServiceDetailResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_ServiceParameterListType* ServiceParameterList;
  uint16 ServiceID;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_PROCESSING_TYPE_FINISHED = 0u,
  EXI_ISO_ED2_DIS_PROCESSING_TYPE_ONGOING = 1u,
  EXI_ISO_ED2_DIS_PROCESSING_TYPE_ONGOING_WAITING_FOR_CUSTOMER_INTERACTION = 2u
} Exi_ISO_ED2_DIS_processingType;

typedef struct Exi_ISO_ED2_DIS_SelectedServiceType Exi_ISO_ED2_DIS_SelectedServiceType;

struct Exi_ISO_ED2_DIS_SelectedServiceType
{
  struct Exi_ISO_ED2_DIS_SelectedServiceType* NextSelectedServicePtr;
  uint16 ParameterSetID;
  uint16 ServiceID;
};

typedef struct Exi_ISO_ED2_DIS_SelectedServiceListType Exi_ISO_ED2_DIS_SelectedServiceListType;

struct Exi_ISO_ED2_DIS_SelectedServiceListType
{
  Exi_ISO_ED2_DIS_SelectedServiceType* SelectedService;
};

typedef struct Exi_ISO_ED2_DIS_ServiceSelectionReqType Exi_ISO_ED2_DIS_ServiceSelectionReqType;

struct Exi_ISO_ED2_DIS_ServiceSelectionReqType
{
  Exi_ISO_ED2_DIS_SelectedServiceType* SelectedEnergyTransferService;
  Exi_ISO_ED2_DIS_SelectedServiceListType* SelectedVASList;
  Exi_ISO_ED2_DIS_identificationOptionType SelectedIdentificationOption;
  Exi_ISO_ED2_DIS_processingType EVProcessing;
  Exi_BitType SelectedVASListFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ServiceSelectionResType Exi_ISO_ED2_DIS_ServiceSelectionResType;

struct Exi_ISO_ED2_DIS_ServiceSelectionResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_AttributeIdType Exi_ISO_ED2_DIS_AttributeIdType;

struct Exi_ISO_ED2_DIS_AttributeIdType
{
  uint16 Length;
  uint8 Buffer[EXI_MAX_BUFFER_SIZE_ISO_ED2_DIS_ATTRIBUTEID];
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( ( EXI_MAX_BUFFER_SIZE_ISO_ED2_DIS_ATTRIBUTEID ) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( ( EXI_MAX_BUFFER_SIZE_ISO_ED2_DIS_ATTRIBUTEID ) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( ( EXI_MAX_BUFFER_SIZE_ISO_ED2_DIS_ATTRIBUTEID ) + 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_certificateType Exi_ISO_ED2_DIS_certificateType;

struct Exi_ISO_ED2_DIS_certificateType
{
  struct Exi_ISO_ED2_DIS_certificateType* NextCertificatePtr;
  uint16 Length;
  uint8 Buffer[800];
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_SubCertificatesType Exi_ISO_ED2_DIS_SubCertificatesType;

struct Exi_ISO_ED2_DIS_SubCertificatesType
{
  Exi_ISO_ED2_DIS_certificateType* Certificate;
};

typedef struct Exi_ISO_ED2_DIS_CertificateChainType Exi_ISO_ED2_DIS_CertificateChainType;

struct Exi_ISO_ED2_DIS_CertificateChainType
{
  Exi_ISO_ED2_DIS_certificateType* Certificate;
  Exi_ISO_ED2_DIS_AttributeIdType* Id;
  Exi_ISO_ED2_DIS_SubCertificatesType* SubCertificates;
  Exi_BitType IdFlag : 1;
  Exi_BitType SubCertificatesFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_IdentificationDetailsReqType Exi_ISO_ED2_DIS_IdentificationDetailsReqType;

struct Exi_ISO_ED2_DIS_IdentificationDetailsReqType
{
  Exi_ISO_ED2_DIS_CertificateChainType* ContractSignatureCertChain;
};

typedef struct Exi_ISO_ED2_DIS_genChallengeType Exi_ISO_ED2_DIS_genChallengeType;

struct Exi_ISO_ED2_DIS_genChallengeType
{
  uint16 Length;
  uint8 Buffer[16];
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_IdentificationDetailsResType Exi_ISO_ED2_DIS_IdentificationDetailsResType;

struct Exi_ISO_ED2_DIS_IdentificationDetailsResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_genChallengeType* GenChallenge;
  Exi_ISO_ED2_DIS_processingType EVSEProcessing;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_AReqIdentificationModeType;

typedef struct Exi_ISO_ED2_DIS_AuthorizationReqType Exi_ISO_ED2_DIS_AuthorizationReqType;

struct Exi_ISO_ED2_DIS_AuthorizationReqType
{
  Exi_ISO_ED2_DIS_AReqIdentificationModeType* AReqIdentificationMode;
  Exi_RootElementIdType AReqIdentificationModeElementId;
  Exi_BitType AReqIdentificationModeFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_AuthorizationResType Exi_ISO_ED2_DIS_AuthorizationResType;

struct Exi_ISO_ED2_DIS_AuthorizationResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_processingType EVSEProcessing;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_CPDReqControlModeType;

typedef struct Exi_ISO_ED2_DIS_CPDReqEnergyTransferModeType Exi_ISO_ED2_DIS_CPDReqEnergyTransferModeType;

struct Exi_ISO_ED2_DIS_CPDReqEnergyTransferModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
  Exi_BitType EVMaximumEnergyRequestFlag : 1;
  Exi_BitType EVMinimumEnergyRequestFlag : 1;
  Exi_BitType EVTargetEnergyRequestFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ChargeParameterDiscoveryReqType Exi_ISO_ED2_DIS_ChargeParameterDiscoveryReqType;

struct Exi_ISO_ED2_DIS_ChargeParameterDiscoveryReqType
{
  Exi_ISO_ED2_DIS_CPDReqControlModeType* CPDReqControlMode;
  Exi_ISO_ED2_DIS_CPDReqEnergyTransferModeType* CPDReqEnergyTransferMode;
  Exi_RootElementIdType CPDReqControlModeElementId;
  Exi_RootElementIdType CPDReqEnergyTransferModeElementId;
};

typedef void Exi_ISO_ED2_DIS_CPDResControlModeType;

typedef void Exi_ISO_ED2_DIS_CPDResEnergyTransferModeType;

typedef struct Exi_ISO_ED2_DIS_ChargeParameterDiscoveryResType Exi_ISO_ED2_DIS_ChargeParameterDiscoveryResType;

struct Exi_ISO_ED2_DIS_ChargeParameterDiscoveryResType
{
  Exi_ISO_ED2_DIS_CPDResControlModeType* CPDResControlMode;
  Exi_ISO_ED2_DIS_CPDResEnergyTransferModeType* CPDResEnergyTransferMode;
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_RootElementIdType CPDResControlModeElementId;
  Exi_RootElementIdType CPDResEnergyTransferModeElementId;
  Exi_ISO_ED2_DIS_processingType EVSEProcessing;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType CPDResControlModeFlag : 1;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_CHARGE_PROGRESS_TYPE_START = 0u,
  EXI_ISO_ED2_DIS_CHARGE_PROGRESS_TYPE_RENEGOTIATE = 1u,
  EXI_ISO_ED2_DIS_CHARGE_PROGRESS_TYPE_STANDBY = 2u,
  EXI_ISO_ED2_DIS_CHARGE_PROGRESS_TYPE_STOP = 3u
} Exi_ISO_ED2_DIS_chargeProgressType;

typedef void Exi_ISO_ED2_DIS_PDReqControlModeType;

typedef struct Exi_ISO_ED2_DIS_PowerDeliveryReqType Exi_ISO_ED2_DIS_PowerDeliveryReqType;

struct Exi_ISO_ED2_DIS_PowerDeliveryReqType
{
  Exi_ISO_ED2_DIS_PDReqControlModeType* PDReqControlMode;
  Exi_RootElementIdType PDReqControlModeElementId;
  Exi_ISO_ED2_DIS_chargeProgressType ChargeProgress;
  Exi_BitType PDReqControlModeFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_PowerDeliveryResType Exi_ISO_ED2_DIS_PowerDeliveryResType;

struct Exi_ISO_ED2_DIS_PowerDeliveryResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_processingType EVSEProcessing;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_meterIDType Exi_ISO_ED2_DIS_meterIDType;

struct Exi_ISO_ED2_DIS_meterIDType
{
  uint16 Length;
  uint8 Buffer[32];
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_meterSignatureType Exi_ISO_ED2_DIS_meterSignatureType;

struct Exi_ISO_ED2_DIS_meterSignatureType
{
  uint16 Length;
  uint8 Buffer[64];
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_MeterInfoType Exi_ISO_ED2_DIS_MeterInfoType;

struct Exi_ISO_ED2_DIS_MeterInfoType
{
  Exi_ISO_ED2_DIS_meterIDType* MeterID;
  Exi_ISO_ED2_DIS_meterSignatureType* MeterSignature;
  Exi_SInt64 TMeter;
  Exi_UInt64 ChargedEnergyReadingWh;
  Exi_UInt64 DischargedEnergyReadingWh;
  Exi_UInt64 LaggingEnergyReadingVARh;
  Exi_UInt64 LeadingEnergyReadingVARh;
  sint16 MeterStatus;
  boolean ReceiptRequired;
  Exi_BitType ChargedEnergyReadingWhFlag : 1;
  Exi_BitType DischargedEnergyReadingWhFlag : 1;
  Exi_BitType LaggingEnergyReadingVARhFlag : 1;
  Exi_BitType LeadingEnergyReadingVARhFlag : 1;
  Exi_BitType MeterSignatureFlag : 1;
  Exi_BitType MeterStatusFlag : 1;
  Exi_BitType TMeterFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 3 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 3 ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_MRReqControlModeType;

typedef struct Exi_ISO_ED2_DIS_MeteringReceiptReqType Exi_ISO_ED2_DIS_MeteringReceiptReqType;

struct Exi_ISO_ED2_DIS_MeteringReceiptReqType
{
  Exi_ISO_ED2_DIS_AttributeIdType* Id;
  Exi_ISO_ED2_DIS_MRReqControlModeType* MRReqControlMode;
  Exi_ISO_ED2_DIS_MeterInfoType* MeterInfo;
  Exi_ISO_ED2_DIS_sessionIDType* SessionID;
  Exi_RootElementIdType MRReqControlModeElementId;
  uint8 ScheduleTupleID;
  Exi_BitType MRReqControlModeFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 3 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 3 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_MeteringReceiptResType Exi_ISO_ED2_DIS_MeteringReceiptResType;

struct Exi_ISO_ED2_DIS_MeteringReceiptResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_CHARGING_SESSION_TYPE_TERMINATE = 0u,
  EXI_ISO_ED2_DIS_CHARGING_SESSION_TYPE_PAUSE = 1u
} Exi_ISO_ED2_DIS_chargingSessionType;

typedef struct Exi_ISO_ED2_DIS_SessionStopReqType Exi_ISO_ED2_DIS_SessionStopReqType;

struct Exi_ISO_ED2_DIS_SessionStopReqType
{
  Exi_ISO_ED2_DIS_chargingSessionType ChargingSession;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_SessionStopResType Exi_ISO_ED2_DIS_SessionStopResType;

struct Exi_ISO_ED2_DIS_SessionStopResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ListOfRootCertificateIDsType Exi_ISO_ED2_DIS_ListOfRootCertificateIDsType;

struct Exi_ISO_ED2_DIS_ListOfRootCertificateIDsType
{
  Exi_XMLSIG_X509IssuerSerialType* RootCertificateID;
};

typedef struct Exi_ISO_ED2_DIS_emaIDType Exi_ISO_ED2_DIS_emaIDType;

struct Exi_ISO_ED2_DIS_emaIDType
{
  struct Exi_ISO_ED2_DIS_emaIDType* NextEMAIDPtr;
  uint16 Length;
  uint8 Buffer[15];
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_EMAIDListType Exi_ISO_ED2_DIS_EMAIDListType;

struct Exi_ISO_ED2_DIS_EMAIDListType
{
  Exi_ISO_ED2_DIS_emaIDType* EMAID;
};

typedef struct Exi_ISO_ED2_DIS_CertificateInstallationReqType Exi_ISO_ED2_DIS_CertificateInstallationReqType;

struct Exi_ISO_ED2_DIS_CertificateInstallationReqType
{
  Exi_ISO_ED2_DIS_AttributeIdType* Id;
  Exi_ISO_ED2_DIS_ListOfRootCertificateIDsType* ListOfRootCertificateIDs;
  Exi_ISO_ED2_DIS_CertificateChainType* OEMProvisioningCertificateChain;
  Exi_ISO_ED2_DIS_EMAIDListType* PrioritizedEMAIDs;
  uint8 MaximumSupportedCertificateChains;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ContractCertificateEncryptedPrivateKeyType Exi_ISO_ED2_DIS_ContractCertificateEncryptedPrivateKeyType;

struct Exi_ISO_ED2_DIS_ContractCertificateEncryptedPrivateKeyType
{
  Exi_ISO_ED2_DIS_AttributeIdType* Id;
  uint16 Length;
  uint8 Buffer[64];
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_DHPublicKeyType Exi_ISO_ED2_DIS_DHPublicKeyType;

struct Exi_ISO_ED2_DIS_DHPublicKeyType
{
  Exi_ISO_ED2_DIS_AttributeIdType* Id;
  uint16 Length;
  uint8 Buffer[65];
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 3 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 3 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_CertificateInstallationResType Exi_ISO_ED2_DIS_CertificateInstallationResType;

struct Exi_ISO_ED2_DIS_CertificateInstallationResType
{
  Exi_ISO_ED2_DIS_CertificateChainType* ContractCertificateChain;
  Exi_ISO_ED2_DIS_ContractCertificateEncryptedPrivateKeyType* ContractCertificateEncryptedPrivateKey;
  Exi_ISO_ED2_DIS_DHPublicKeyType* DHPublicKey;
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_CertificateChainType* SAProvisioningCertificateChain;
  Exi_ISO_ED2_DIS_processingType EVSEProcessing;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  uint8 RemainingContractCertificateChains;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_DisplayParametersType Exi_ISO_ED2_DIS_DisplayParametersType;

struct Exi_ISO_ED2_DIS_DisplayParametersType
{
  uint32 RemainingTimeToBulkSOC;
  uint32 RemainingTimeToMaximumSOC;
  uint32 RemainingTimeToMinimumSOC;
  uint32 RemainingTimeToTargetSOC;
  uint16 CurrentRange;
  boolean BulkChargingComplete;
  boolean ChargingComplete;
  boolean InletHot;
  sint8 CurrentSOC;
  sint8 MinimumSOC;
  Exi_BitType BulkChargingCompleteFlag : 1;
  Exi_BitType ChargingCompleteFlag : 1;
  Exi_BitType CurrentRangeFlag : 1;
  Exi_BitType CurrentSOCFlag : 1;
  Exi_BitType InletHotFlag : 1;
  Exi_BitType MinimumSOCFlag : 1;
  Exi_BitType RemainingTimeToBulkSOCFlag : 1;
  Exi_BitType RemainingTimeToMaximumSOCFlag : 1;
  Exi_BitType RemainingTimeToMinimumSOCFlag : 1;
  Exi_BitType RemainingTimeToTargetSOCFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 3 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 3 ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_CLReqIdentificationModeType;

typedef struct Exi_ISO_ED2_DIS_GenericElementType Exi_ISO_ED2_DIS_GenericElementType;

struct Exi_ISO_ED2_DIS_GenericElementType
{
  void* GenericElement;
  struct Exi_ISO_ED2_DIS_GenericElementType* NextGenericElementPtr;
  Exi_RootElementIdType GenericElementId;
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_CSReqControlModeType Exi_ISO_ED2_DIS_CSReqControlModeType;

struct Exi_ISO_ED2_DIS_CSReqControlModeType
{
  Exi_ISO_ED2_DIS_GenericElementType* GenericElement;
  Exi_BitType GenericElementFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ChargingStatusReqType Exi_ISO_ED2_DIS_ChargingStatusReqType;

struct Exi_ISO_ED2_DIS_ChargingStatusReqType
{
  Exi_ISO_ED2_DIS_CLReqIdentificationModeType* CLReqIdentificationMode;
  Exi_ISO_ED2_DIS_CSReqControlModeType* CSReqControlMode;
  Exi_ISO_ED2_DIS_DisplayParametersType* DisplayParameters;
  Exi_RootElementIdType CLReqIdentificationModeElementId;
  Exi_RootElementIdType CSReqControlModeElementId;
  Exi_BitType CLReqIdentificationModeFlag : 1;
  Exi_BitType DisplayParametersFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_CLResIdentificationModeType;

typedef void Exi_ISO_ED2_DIS_CLResControlModeType;

typedef void Exi_ISO_ED2_DIS_CSResControlModeType;

typedef struct Exi_ISO_ED2_DIS_ChargingStatusResType Exi_ISO_ED2_DIS_ChargingStatusResType;

struct Exi_ISO_ED2_DIS_ChargingStatusResType
{
  Exi_ISO_ED2_DIS_CLResControlModeType* CLResControlMode;
  Exi_ISO_ED2_DIS_CLResIdentificationModeType* CLResIdentificationMode;
  Exi_ISO_ED2_DIS_CSResControlModeType* CSResControlMode;
  Exi_ISO_ED2_DIS_evseIDType* EVSEID;
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSETargetFrequency;
  Exi_RootElementIdType CLResControlModeElementId;
  Exi_RootElementIdType CLResIdentificationModeElementId;
  Exi_RootElementIdType CSResControlModeElementId;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType CLResControlModeFlag : 1;
  Exi_BitType CLResIdentificationModeFlag : 1;
  Exi_BitType CSResControlModeFlag : 1;
  Exi_BitType EVSEStatusFlag : 1;
  Exi_BitType EVSETargetFrequencyFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_CableCheckReqType;

typedef struct Exi_ISO_ED2_DIS_CableCheckResType Exi_ISO_ED2_DIS_CableCheckResType;

struct Exi_ISO_ED2_DIS_CableCheckResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_processingType EVSEProcessing;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_PreChargeReqType Exi_ISO_ED2_DIS_PreChargeReqType;

struct Exi_ISO_ED2_DIS_PreChargeReqType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetVoltage;
};

typedef struct Exi_ISO_ED2_DIS_PreChargeResType Exi_ISO_ED2_DIS_PreChargeResType;

struct Exi_ISO_ED2_DIS_PreChargeResType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEPresentVoltage;
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_CDReqControlModeType Exi_ISO_ED2_DIS_CDReqControlModeType;

struct Exi_ISO_ED2_DIS_CDReqControlModeType
{
  Exi_ISO_ED2_DIS_GenericElementType* GenericElement;
  Exi_BitType GenericElementFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_CurrentDemandReqType Exi_ISO_ED2_DIS_CurrentDemandReqType;

struct Exi_ISO_ED2_DIS_CurrentDemandReqType
{
  Exi_ISO_ED2_DIS_CDReqControlModeType* CDReqControlMode;
  Exi_ISO_ED2_DIS_CLReqIdentificationModeType* CLReqIdentificationMode;
  Exi_ISO_ED2_DIS_DisplayParametersType* DisplayParameters;
  Exi_RootElementIdType CDReqControlModeElementId;
  Exi_RootElementIdType CLReqIdentificationModeElementId;
  Exi_BitType CLReqIdentificationModeFlag : 1;
  Exi_BitType DisplayParametersFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_CurrentDemandResType Exi_ISO_ED2_DIS_CurrentDemandResType;

struct Exi_ISO_ED2_DIS_CurrentDemandResType
{
  Exi_ISO_ED2_DIS_CLResControlModeType* CLResControlMode;
  Exi_ISO_ED2_DIS_CLResIdentificationModeType* CLResIdentificationMode;
  Exi_ISO_ED2_DIS_evseIDType* EVSEID;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEPresentCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEPresentVoltage;
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_RootElementIdType CLResControlModeElementId;
  Exi_RootElementIdType CLResIdentificationModeElementId;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  boolean EVSECurrentLimitAchieved;
  boolean EVSEPowerLimitAchieved;
  boolean EVSEVoltageLimitAchieved;
  Exi_BitType CLResControlModeFlag : 1;
  Exi_BitType CLResIdentificationModeFlag : 1;
  Exi_BitType EVSEMaximumChargeCurrentFlag : 1;
  Exi_BitType EVSEMaximumChargePowerFlag : 1;
  Exi_BitType EVSEMaximumVoltageFlag : 1;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 3 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 3 ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_WeldingDetectionReqType;

typedef struct Exi_ISO_ED2_DIS_WeldingDetectionResType Exi_ISO_ED2_DIS_WeldingDetectionResType;

struct Exi_ISO_ED2_DIS_WeldingDetectionResType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEPresentVoltage;
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_EVFinePositioningSetupParametersType;

typedef struct Exi_ISO_ED2_DIS_FinePositioningSetupReqType Exi_ISO_ED2_DIS_FinePositioningSetupReqType;

struct Exi_ISO_ED2_DIS_FinePositioningSetupReqType
{
  Exi_ISO_ED2_DIS_EVFinePositioningSetupParametersType* EVFinePositioningSetupParameters;
  Exi_RootElementIdType EVFinePositioningSetupParametersElementId;
  Exi_BitType EVFinePositioningSetupParametersFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_EVSEFinePositioningSetupParametersType;

typedef struct Exi_ISO_ED2_DIS_FinePositioningSetupResType Exi_ISO_ED2_DIS_FinePositioningSetupResType;

struct Exi_ISO_ED2_DIS_FinePositioningSetupResType
{
  Exi_ISO_ED2_DIS_EVSEFinePositioningSetupParametersType* EVSEFinePositioningSetupParameters;
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_RootElementIdType EVSEFinePositioningSetupParametersElementId;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEFinePositioningSetupParametersFlag : 1;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_EVFinePositioningParametersType;

typedef struct Exi_ISO_ED2_DIS_FinePositioningReqType Exi_ISO_ED2_DIS_FinePositioningReqType;

struct Exi_ISO_ED2_DIS_FinePositioningReqType
{
  Exi_ISO_ED2_DIS_EVFinePositioningParametersType* EVFinePositioningParameters;
  Exi_RootElementIdType EVFinePositioningParametersElementId;
  uint16 AlignmentOffset;
  Exi_ISO_ED2_DIS_processingType EVProcessing;
  Exi_BitType EVFinePositioningParametersFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_EVSEFinePositioningParametersType;

typedef struct Exi_ISO_ED2_DIS_FinePositioningResType Exi_ISO_ED2_DIS_FinePositioningResType;

struct Exi_ISO_ED2_DIS_FinePositioningResType
{
  Exi_ISO_ED2_DIS_EVSEFinePositioningParametersType* EVSEFinePositioningParameters;
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_RootElementIdType EVSEFinePositioningParametersElementId;
  uint16 AlignmentOffset;
  Exi_ISO_ED2_DIS_processingType EVSEProcessing;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEFinePositioningParametersFlag : 1;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_ACReqAlignmentCheckMechanismType;

typedef struct Exi_ISO_ED2_DIS_AlignmentCheckReqType Exi_ISO_ED2_DIS_AlignmentCheckReqType;

struct Exi_ISO_ED2_DIS_AlignmentCheckReqType
{
  Exi_ISO_ED2_DIS_ACReqAlignmentCheckMechanismType* ACReqAlignmentCheckMechanism;
  Exi_RootElementIdType ACReqAlignmentCheckMechanismElementId;
  Exi_ISO_ED2_DIS_processingType EVProcessing;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef void Exi_ISO_ED2_DIS_ACResAlignmentCheckMechanismType;

typedef struct Exi_ISO_ED2_DIS_AlignmentCheckResType Exi_ISO_ED2_DIS_AlignmentCheckResType;

struct Exi_ISO_ED2_DIS_AlignmentCheckResType
{
  Exi_ISO_ED2_DIS_ACResAlignmentCheckMechanismType* ACResAlignmentCheckMechanism;
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_RootElementIdType ACResAlignmentCheckMechanismElementId;
  Exi_ISO_ED2_DIS_processingType EVSEProcessing;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_PairingReqType Exi_ISO_ED2_DIS_PairingReqType;

struct Exi_ISO_ED2_DIS_PairingReqType
{
  Exi_ISO_ED2_DIS_ParameterSetType* PairingParameters;
  Exi_ISO_ED2_DIS_processingType EVProcessing;
  Exi_BitType PairingParametersFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_PairingResType Exi_ISO_ED2_DIS_PairingResType;

struct Exi_ISO_ED2_DIS_PairingResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_ParameterSetType* PairingParameters;
  Exi_ISO_ED2_DIS_processingType EVSEProcessing;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
  Exi_BitType PairingParametersFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_PDdReqControlModeType Exi_ISO_ED2_DIS_PDdReqControlModeType;

struct Exi_ISO_ED2_DIS_PDdReqControlModeType
{
  Exi_ISO_ED2_DIS_GenericElementType* GenericElement;
  Exi_BitType GenericElementFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_PowerDemandReqType Exi_ISO_ED2_DIS_PowerDemandReqType;

struct Exi_ISO_ED2_DIS_PowerDemandReqType
{
  Exi_ISO_ED2_DIS_CLReqIdentificationModeType* CLReqIdentificationMode;
  Exi_ISO_ED2_DIS_DisplayParametersType* DisplayParameters;
  Exi_ISO_ED2_DIS_PhysicalValueType* GACoilCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* GAFrequency;
  Exi_ISO_ED2_DIS_PDdReqControlModeType* PDdReqControlMode;
  Exi_ISO_ED2_DIS_ParameterSetType* PowerDemandParameters;
  Exi_ISO_ED2_DIS_PhysicalValueType* VACurrentToEV;
  Exi_ISO_ED2_DIS_PhysicalValueType* VAPowerReceived;
  Exi_ISO_ED2_DIS_PhysicalValueType* VAVoltageToEV;
  Exi_RootElementIdType CLReqIdentificationModeElementId;
  Exi_RootElementIdType PDdReqControlModeElementId;
  Exi_BitType CLReqIdentificationModeFlag : 1;
  Exi_BitType DisplayParametersFlag : 1;
  Exi_BitType PowerDemandParametersFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_PowerDemandResType Exi_ISO_ED2_DIS_PowerDemandResType;

struct Exi_ISO_ED2_DIS_PowerDemandResType
{
  Exi_ISO_ED2_DIS_CLResControlModeType* CLResControlMode;
  Exi_ISO_ED2_DIS_CLResIdentificationModeType* CLResIdentificationMode;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVRequestedPower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEDCPrimaryPower;
  Exi_ISO_ED2_DIS_evseIDType* EVSEID;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEPowerLimit;
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_PhysicalValueType* GACoilCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* GAFrequency;
  Exi_ISO_ED2_DIS_PhysicalValueType* GAInputPower;
  Exi_ISO_ED2_DIS_ParameterSetType* PowerDemandParameters;
  Exi_RootElementIdType CLResControlModeElementId;
  Exi_RootElementIdType CLResIdentificationModeElementId;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  boolean ForeignObjectDetected;
  boolean LivingObjectDetected;
  Exi_BitType CLResControlModeFlag : 1;
  Exi_BitType CLResIdentificationModeFlag : 1;
  Exi_BitType EVSEStatusFlag : 1;
  Exi_BitType PowerDemandParametersFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_EVCHECK_IN_STATUS_TYPE_CHECK_IN = 0u,
  EXI_ISO_ED2_DIS_EVCHECK_IN_STATUS_TYPE_PROCESSING = 1u,
  EXI_ISO_ED2_DIS_EVCHECK_IN_STATUS_TYPE_COMPLETED = 2u
} Exi_ISO_ED2_DIS_EVCheckInStatusType;

typedef enum
{
  EXI_ISO_ED2_DIS_PARKING_METHOD_TYPE_AUTO_PARKING = 0u,
  EXI_ISO_ED2_DIS_PARKING_METHOD_TYPE_MVGUIDE_MANUAL = 1u,
  EXI_ISO_ED2_DIS_PARKING_METHOD_TYPE_MANUAL = 2u
} Exi_ISO_ED2_DIS_parkingMethodType;

typedef struct Exi_ISO_ED2_DIS_VehicleCheckInReqType Exi_ISO_ED2_DIS_VehicleCheckInReqType;

struct Exi_ISO_ED2_DIS_VehicleCheckInReqType
{
  Exi_ISO_ED2_DIS_EVCheckInStatusType EVCheckInStatus;
  Exi_ISO_ED2_DIS_parkingMethodType ParkingMethod;
  Exi_BitType ParkingMethodFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_TargetPositionType Exi_ISO_ED2_DIS_TargetPositionType;

struct Exi_ISO_ED2_DIS_TargetPositionType
{
  uint16 TargetOffsetX;
  uint16 TargetOffsetY;
};

typedef struct Exi_ISO_ED2_DIS_VehicleCheckInResType Exi_ISO_ED2_DIS_VehicleCheckInResType;

struct Exi_ISO_ED2_DIS_VehicleCheckInResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_TargetPositionType* TargetOffset;
  uint16 VehicleSpace;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
  Exi_BitType TargetOffsetFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_EVCHECK_OUT_STATUS_TYPE_CHECK_OUT = 0u,
  EXI_ISO_ED2_DIS_EVCHECK_OUT_STATUS_TYPE_PROCESSING = 1u,
  EXI_ISO_ED2_DIS_EVCHECK_OUT_STATUS_TYPE_COMPLETED = 2u
} Exi_ISO_ED2_DIS_EVCheckOutStatusType;

typedef struct Exi_ISO_ED2_DIS_VehicleCheckOutReqType Exi_ISO_ED2_DIS_VehicleCheckOutReqType;

struct Exi_ISO_ED2_DIS_VehicleCheckOutReqType
{
  Exi_UInt64 CheckOutTime;
  Exi_ISO_ED2_DIS_EVCheckOutStatusType EVCheckOutStatus;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_EVSECHECK_OUT_STATUS_TYPE_SCHEDULED = 0u,
  EXI_ISO_ED2_DIS_EVSECHECK_OUT_STATUS_TYPE_COMPLETED = 1u
} Exi_ISO_ED2_DIS_EVSECheckOutStatusType;

typedef struct Exi_ISO_ED2_DIS_VehicleCheckOutResType Exi_ISO_ED2_DIS_VehicleCheckOutResType;

struct Exi_ISO_ED2_DIS_VehicleCheckOutResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_EVSECheckOutStatusType EVSECheckOutStatus;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_EV_OPERATION_TYPE_CHARGE = 0u,
  EXI_ISO_ED2_DIS_EV_OPERATION_TYPE_DISCHARGE = 1u,
  EXI_ISO_ED2_DIS_EV_OPERATION_TYPE_STANDBY = 2u
} Exi_ISO_ED2_DIS_evOperationType;

typedef struct Exi_ISO_ED2_DIS_ACBCReqControlModeType Exi_ISO_ED2_DIS_ACBCReqControlModeType;

struct Exi_ISO_ED2_DIS_ACBCReqControlModeType
{
  Exi_ISO_ED2_DIS_GenericElementType* GenericElement;
  Exi_BitType GenericElementFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_AC_BidirectionalControlReqType Exi_ISO_ED2_DIS_AC_BidirectionalControlReqType;

struct Exi_ISO_ED2_DIS_AC_BidirectionalControlReqType
{
  Exi_ISO_ED2_DIS_ACBCReqControlModeType* ACBCReqControlMode;
  Exi_ISO_ED2_DIS_CLReqIdentificationModeType* CLReqIdentificationMode;
  Exi_ISO_ED2_DIS_DisplayParametersType* DisplayParameters;
  Exi_RootElementIdType ACBCReqControlModeElementId;
  Exi_RootElementIdType CLReqIdentificationModeElementId;
  Exi_ISO_ED2_DIS_evOperationType EVOperation;
  Exi_BitType CLReqIdentificationModeFlag : 1;
  Exi_BitType DisplayParametersFlag : 1;
  Exi_BitType EVOperationFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ACBCResControlModeType Exi_ISO_ED2_DIS_ACBCResControlModeType;

struct Exi_ISO_ED2_DIS_ACBCResControlModeType
{
  Exi_ISO_ED2_DIS_GenericElementType* GenericElement;
  Exi_BitType GenericElementFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_AC_BidirectionalControlResType Exi_ISO_ED2_DIS_AC_BidirectionalControlResType;

struct Exi_ISO_ED2_DIS_AC_BidirectionalControlResType
{
  Exi_ISO_ED2_DIS_ACBCResControlModeType* ACBCResControlMode;
  Exi_ISO_ED2_DIS_CLResControlModeType* CLResControlMode;
  Exi_ISO_ED2_DIS_CLResIdentificationModeType* CLResIdentificationMode;
  Exi_ISO_ED2_DIS_evseIDType* EVSEID;
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSETargetFrequency;
  Exi_RootElementIdType ACBCResControlModeElementId;
  Exi_RootElementIdType CLResControlModeElementId;
  Exi_RootElementIdType CLResIdentificationModeElementId;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType CLResControlModeFlag : 1;
  Exi_BitType CLResIdentificationModeFlag : 1;
  Exi_BitType EVSEStatusFlag : 1;
  Exi_BitType EVSETargetFrequencyFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_DCBCReqControlModeType Exi_ISO_ED2_DIS_DCBCReqControlModeType;

struct Exi_ISO_ED2_DIS_DCBCReqControlModeType
{
  Exi_ISO_ED2_DIS_GenericElementType* GenericElement;
  Exi_BitType GenericElementFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_DC_BidirectionalControlReqType Exi_ISO_ED2_DIS_DC_BidirectionalControlReqType;

struct Exi_ISO_ED2_DIS_DC_BidirectionalControlReqType
{
  Exi_ISO_ED2_DIS_CLReqIdentificationModeType* CLReqIdentificationMode;
  Exi_ISO_ED2_DIS_DCBCReqControlModeType* DCBCReqControlMode;
  Exi_ISO_ED2_DIS_DisplayParametersType* DisplayParameters;
  Exi_RootElementIdType CLReqIdentificationModeElementId;
  Exi_RootElementIdType DCBCReqControlModeElementId;
  Exi_BitType CLReqIdentificationModeFlag : 1;
  Exi_BitType DisplayParametersFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_DC_BidirectionalControlResType Exi_ISO_ED2_DIS_DC_BidirectionalControlResType;

struct Exi_ISO_ED2_DIS_DC_BidirectionalControlResType
{
  Exi_ISO_ED2_DIS_CLResControlModeType* CLResControlMode;
  Exi_ISO_ED2_DIS_CLResIdentificationModeType* CLResIdentificationMode;
  Exi_ISO_ED2_DIS_evseIDType* EVSEID;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumDischargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumDischargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMinimumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEPresentCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEPresentVoltage;
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_RootElementIdType CLResControlModeElementId;
  Exi_RootElementIdType CLResIdentificationModeElementId;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  boolean EVSECurrentLimitAchieved;
  boolean EVSEPowerLimitAchieved;
  boolean EVSEVoltageLimitAchieved;
  Exi_BitType CLResControlModeFlag : 1;
  Exi_BitType CLResIdentificationModeFlag : 1;
  Exi_BitType EVSEMaximumChargeCurrentFlag : 1;
  Exi_BitType EVSEMaximumChargePowerFlag : 1;
  Exi_BitType EVSEMaximumDischargeCurrentFlag : 1;
  Exi_BitType EVSEMaximumDischargePowerFlag : 1;
  Exi_BitType EVSEMaximumVoltageFlag : 1;
  Exi_BitType EVSEMinimumVoltageFlag : 1;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 3 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 3 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ACDPositioningReqType Exi_ISO_ED2_DIS_ACDPositioningReqType;

struct Exi_ISO_ED2_DIS_ACDPositioningReqType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVOrientation;
  sint32 EVRelativeACD_X_Deviation;
  sint32 EVRelativeACD_Y_Deviation;
  boolean EVMobilityStatus;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ACDPositioningResType Exi_ISO_ED2_DIS_ACDPositioningResType;

struct Exi_ISO_ED2_DIS_ACDPositioningResType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVOrientation;
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  sint32 ContactWindow_Xc;
  sint32 ContactWindow_Yc;
  sint32 EVRelativeACD_X_Deviation;
  sint32 EVRelativeACD_Y_Deviation;
  Exi_ISO_ED2_DIS_processingType EVSEProcessing;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  boolean EVInChargePosition;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_ACD_MECHANICAL_CHARGING_DEVICE_STATUS_TYPE_HOME = 0u,
  EXI_ISO_ED2_DIS_ACD_MECHANICAL_CHARGING_DEVICE_STATUS_TYPE_MOVING = 1u,
  EXI_ISO_ED2_DIS_ACD_MECHANICAL_CHARGING_DEVICE_STATUS_TYPE_END_POSITION = 2u
} Exi_ISO_ED2_DIS_acdMechanicalChargingDeviceStatusType;

typedef struct Exi_ISO_ED2_DIS_ConnectChargingDeviceReqType Exi_ISO_ED2_DIS_ConnectChargingDeviceReqType;

struct Exi_ISO_ED2_DIS_ConnectChargingDeviceReqType
{
  Exi_ISO_ED2_DIS_acdMechanicalChargingDeviceStatusType EVElectricalChargingDeviceStatus;
  Exi_ISO_ED2_DIS_acdMechanicalChargingDeviceStatusType EVMechanicalChargingDeviceStatus;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ConnectChargingDeviceResType Exi_ISO_ED2_DIS_ConnectChargingDeviceResType;

struct Exi_ISO_ED2_DIS_ConnectChargingDeviceResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_acdMechanicalChargingDeviceStatusType EVSEElectricalChargingDeviceStatus;
  Exi_ISO_ED2_DIS_acdMechanicalChargingDeviceStatusType EVSEMechanicalChargingDeviceStatus;
  Exi_ISO_ED2_DIS_processingType EVSEProcessing;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (4 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (4 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (4 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_DisconnectChargingDeviceReqType Exi_ISO_ED2_DIS_DisconnectChargingDeviceReqType;

struct Exi_ISO_ED2_DIS_DisconnectChargingDeviceReqType
{
  Exi_ISO_ED2_DIS_acdMechanicalChargingDeviceStatusType EVElectricalChargingDeviceStatus;
  Exi_ISO_ED2_DIS_acdMechanicalChargingDeviceStatusType EVMechanicalChargingDeviceStatus;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_DisconnectChargingDeviceResType Exi_ISO_ED2_DIS_DisconnectChargingDeviceResType;

struct Exi_ISO_ED2_DIS_DisconnectChargingDeviceResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_acdMechanicalChargingDeviceStatusType EVSEElectricalChargingDeviceStatus;
  Exi_ISO_ED2_DIS_acdMechanicalChargingDeviceStatusType EVSEMechanicalChargingDeviceStatus;
  Exi_ISO_ED2_DIS_processingType EVSEProcessing;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (4 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (4 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (4 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_ACD_CPSTATUS_TYPE_STATEA = 0u,
  EXI_ISO_ED2_DIS_ACD_CPSTATUS_TYPE_STATEB = 1u,
  EXI_ISO_ED2_DIS_ACD_CPSTATUS_TYPE_STATEC = 2u,
  EXI_ISO_ED2_DIS_ACD_CPSTATUS_TYPE_STATED = 3u,
  EXI_ISO_ED2_DIS_ACD_CPSTATUS_TYPE_STATEE = 4u
} Exi_ISO_ED2_DIS_acdCPStatusType;

typedef enum
{
  EXI_ISO_ED2_DIS_ACD_ERROR_STATUS_CODE_TYPE_OK_NO_EVERROR = 0u,
  EXI_ISO_ED2_DIS_ACD_ERROR_STATUS_CODE_TYPE_FAILED_EMERGENCY_EVENT = 1u,
  EXI_ISO_ED2_DIS_ACD_ERROR_STATUS_CODE_TYPE_FAILED_BREAKER = 2u,
  EXI_ISO_ED2_DIS_ACD_ERROR_STATUS_CODE_TYPE_FAILED_RESSTEMPERATURE_INHIBIT = 3u,
  EXI_ISO_ED2_DIS_ACD_ERROR_STATUS_CODE_TYPE_FAILED_RESS = 4u,
  EXI_ISO_ED2_DIS_ACD_ERROR_STATUS_CODE_TYPE_FAILED_CHARGING_CURRENT_DIFFERENTIAL = 5u,
  EXI_ISO_ED2_DIS_ACD_ERROR_STATUS_CODE_TYPE_FAILED_CHARGING_VOLTAGE_OUT_OF_RANGE = 6u,
  EXI_ISO_ED2_DIS_ACD_ERROR_STATUS_CODE_TYPE_FAILED_OEMSPECIFIC1 = 7u,
  EXI_ISO_ED2_DIS_ACD_ERROR_STATUS_CODE_TYPE_FAILED_OEMSPECIFIC2 = 8u,
  EXI_ISO_ED2_DIS_ACD_ERROR_STATUS_CODE_TYPE_FAILED_OEMSPECIFIC3 = 9u,
  EXI_ISO_ED2_DIS_ACD_ERROR_STATUS_CODE_TYPE_FAILED_OEMSPECIFIC4 = 10u
} Exi_ISO_ED2_DIS_acdErrorStatusCodeType;

typedef struct Exi_ISO_ED2_DIS_EVTechnicalStatusType Exi_ISO_ED2_DIS_EVTechnicalStatusType;

struct Exi_ISO_ED2_DIS_EVTechnicalStatusType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVStatusWLANStrength;
  Exi_ISO_ED2_DIS_acdCPStatusType EVCPStatus;
  Exi_ISO_ED2_DIS_acdErrorStatusCodeType EVErrorStatusCode;
  boolean EVSETimeout;
  boolean EVStatusImmobilizationRequest;
  boolean EVStatusImmobilized;
  boolean EVStatusReadyToCharge;
  sint8 EVStatusRESSSOC;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (2 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_evOEMStatusType Exi_ISO_ED2_DIS_evOEMStatusType;

struct Exi_ISO_ED2_DIS_evOEMStatusType
{
  uint16 Length;
  uint8 Buffer[800];
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_SystemStatusReqType Exi_ISO_ED2_DIS_SystemStatusReqType;

struct Exi_ISO_ED2_DIS_SystemStatusReqType
{
  Exi_ISO_ED2_DIS_evOEMStatusType* EVOEMStatus;
  Exi_ISO_ED2_DIS_EVTechnicalStatusType* EVTechnicalStatus;
  Exi_ISO_ED2_DIS_acdMechanicalChargingDeviceStatusType EVMechanicalChargingDeviceStatus;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_ACD_ISOLATION_STATUS_TYPE_INVALID = 0u,
  EXI_ISO_ED2_DIS_ACD_ISOLATION_STATUS_TYPE_SAFE = 1u,
  EXI_ISO_ED2_DIS_ACD_ISOLATION_STATUS_TYPE_WARNING = 2u,
  EXI_ISO_ED2_DIS_ACD_ISOLATION_STATUS_TYPE_FAULT = 3u
} Exi_ISO_ED2_DIS_acdIsolationStatusType;

typedef struct Exi_ISO_ED2_DIS_SystemStatusResType Exi_ISO_ED2_DIS_SystemStatusResType;

struct Exi_ISO_ED2_DIS_SystemStatusResType
{
  Exi_ISO_ED2_DIS_EVSEStatusType* EVSEStatus;
  Exi_ISO_ED2_DIS_acdIsolationStatusType EVSEIsolationStatus;
  Exi_ISO_ED2_DIS_acdMechanicalChargingDeviceStatusType EVSEMechanicalChargingDeviceStatus;
  Exi_ISO_ED2_DIS_responseCodeType ResponseCode;
  boolean EVAssociationStatus;
  boolean EVInChargePosition;
  boolean EVSEDisabled;
  boolean EVSEEmergencyShutdown;
  boolean EVSEMalfunction;
  boolean EVSEReadyToCharge;
  boolean EVSEUtilityInterruptEvent;
  Exi_BitType EVSEStatusFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (3 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (3 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 3 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (3 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 3 ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_SCHEDULE_ORIGIN_TYPE_EV = 0u,
  EXI_ISO_ED2_DIS_SCHEDULE_ORIGIN_TYPE_SA = 1u
} Exi_ISO_ED2_DIS_scheduleOriginType;

typedef struct Exi_ISO_ED2_DIS_TimeIntervalType Exi_ISO_ED2_DIS_TimeIntervalType;

struct Exi_ISO_ED2_DIS_TimeIntervalType
{
  uint32 duration;
  uint32 start;
  Exi_BitType durationFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_PowerScheduleEntryType Exi_ISO_ED2_DIS_PowerScheduleEntryType;

struct Exi_ISO_ED2_DIS_PowerScheduleEntryType
{
  struct Exi_ISO_ED2_DIS_PowerScheduleEntryType* NextEVPowerProfileEntryPtr;
  struct Exi_ISO_ED2_DIS_PowerScheduleEntryType* NextPowerScheduleEntryPtr;
  Exi_ISO_ED2_DIS_PhysicalValueType* Power;
  Exi_ISO_ED2_DIS_TimeIntervalType* TimeInterval;
};

typedef struct Exi_ISO_ED2_DIS_PowerScheduleType Exi_ISO_ED2_DIS_PowerScheduleType;

struct Exi_ISO_ED2_DIS_PowerScheduleType
{
  Exi_ISO_ED2_DIS_PowerScheduleEntryType* PowerScheduleEntry;
  Exi_UInt64 TimeStamp;
};

typedef struct Exi_ISO_ED2_DIS_salesTariffDescriptionType Exi_ISO_ED2_DIS_salesTariffDescriptionType;

struct Exi_ISO_ED2_DIS_salesTariffDescriptionType
{
  uint16 Length;
  uint8 Buffer[32];
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_COST_KIND_TYPE_RELATIVE_PRICE_PERCENTAGE = 0u,
  EXI_ISO_ED2_DIS_COST_KIND_TYPE_RENEWABLE_GENERATION_PERCENTAGE = 1u,
  EXI_ISO_ED2_DIS_COST_KIND_TYPE_CARBON_DIOXIDE_EMISSION = 2u,
  EXI_ISO_ED2_DIS_COST_KIND_TYPE_ABSOLUTE_PRICE = 3u,
  EXI_ISO_ED2_DIS_COST_KIND_TYPE_SELF_SUFFICIENT_ENERGY_PERCENTAGE = 4u
} Exi_ISO_ED2_DIS_costKindType;

typedef struct Exi_ISO_ED2_DIS_CostType Exi_ISO_ED2_DIS_CostType;

struct Exi_ISO_ED2_DIS_CostType
{
  struct Exi_ISO_ED2_DIS_CostType* NextCostPtr;
  Exi_ISO_ED2_DIS_PhysicalValueType* amount;
  Exi_ISO_ED2_DIS_costKindType costKind;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ConsumptionCostType Exi_ISO_ED2_DIS_ConsumptionCostType;

struct Exi_ISO_ED2_DIS_ConsumptionCostType
{
  Exi_ISO_ED2_DIS_CostType* Cost;
  struct Exi_ISO_ED2_DIS_ConsumptionCostType* NextConsumptionCostPtr;
  Exi_ISO_ED2_DIS_PhysicalValueType* startValue;
};

typedef struct Exi_ISO_ED2_DIS_SalesTariffEntryType Exi_ISO_ED2_DIS_SalesTariffEntryType;

struct Exi_ISO_ED2_DIS_SalesTariffEntryType
{
  Exi_ISO_ED2_DIS_ConsumptionCostType* ConsumptionCost;
  struct Exi_ISO_ED2_DIS_SalesTariffEntryType* NextSalesTariffEntryPtr;
  Exi_ISO_ED2_DIS_TimeIntervalType* TimeInterval;
  uint8 EPriceLevel;
  Exi_BitType ConsumptionCostFlag : 1;
  Exi_BitType EPriceLevelFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_SalesTariffType Exi_ISO_ED2_DIS_SalesTariffType;

struct Exi_ISO_ED2_DIS_SalesTariffType
{
  Exi_stringType* Currency;
  Exi_ISO_ED2_DIS_AttributeIdType* Id;
  Exi_ISO_ED2_DIS_salesTariffDescriptionType* SalesTariffDescription;
  Exi_ISO_ED2_DIS_SalesTariffEntryType* SalesTariffEntry;
  Exi_UInt64 TimeStamp;
  uint8 NumEPriceLevels;
  uint8 SalesTariffID;
  Exi_BitType CurrencyFlag : 1;
  Exi_BitType NumEPriceLevelsFlag : 1;
  Exi_BitType SalesTariffDescriptionFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ScheduleTupleType Exi_ISO_ED2_DIS_ScheduleTupleType;

struct Exi_ISO_ED2_DIS_ScheduleTupleType
{
  Exi_ISO_ED2_DIS_SalesTariffType* BuyBackTariff;
  struct Exi_ISO_ED2_DIS_ScheduleTupleType* NextScheduleTuplePtr;
  Exi_ISO_ED2_DIS_PowerScheduleType* PowerDischargeSchedule;
  Exi_ISO_ED2_DIS_PowerScheduleType* PowerSchedule;
  Exi_ISO_ED2_DIS_SalesTariffType* SalesTariff;
  uint8 ScheduleTupleID;
  Exi_BitType BuyBackTariffFlag : 1;
  Exi_BitType PowerDischargeScheduleFlag : 1;
  Exi_BitType SalesTariffFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_ScheduleListType Exi_ISO_ED2_DIS_ScheduleListType;

struct Exi_ISO_ED2_DIS_ScheduleListType
{
  Exi_ISO_ED2_DIS_ScheduleTupleType* ScheduleTuple;
  Exi_ISO_ED2_DIS_scheduleOriginType ScheduleOrigin;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_CartesianCoordinatesType Exi_ISO_ED2_DIS_CartesianCoordinatesType;

struct Exi_ISO_ED2_DIS_CartesianCoordinatesType
{
  sint16 XCoordinate;
  sint16 YCoordinate;
  sint16 ZCoordinate;
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_SensorType Exi_ISO_ED2_DIS_SensorType;

struct Exi_ISO_ED2_DIS_SensorType
{
  struct Exi_ISO_ED2_DIS_SensorType* NextSensorPtr;
  Exi_ISO_ED2_DIS_CartesianCoordinatesType* SensorOrientation;
  Exi_ISO_ED2_DIS_CartesianCoordinatesType* SensorPosition;
  uint8 SensorID;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_SensorListType Exi_ISO_ED2_DIS_SensorListType;

struct Exi_ISO_ED2_DIS_SensorListType
{
  Exi_ISO_ED2_DIS_SensorType* Sensor;
};

typedef struct Exi_ISO_ED2_DIS_SensorOrderListType Exi_ISO_ED2_DIS_SensorOrderListType;

struct Exi_ISO_ED2_DIS_SensorOrderListType
{
  uint8 SensorPosition[255];
  uint8 SensorPositionCount;
};

typedef struct Exi_ISO_ED2_DIS_LFA_EVFinePositioningSetupParametersType Exi_ISO_ED2_DIS_LFA_EVFinePositioningSetupParametersType;

struct Exi_ISO_ED2_DIS_LFA_EVFinePositioningSetupParametersType
{
  Exi_ISO_ED2_DIS_SensorListType* SensorList;
  Exi_ISO_ED2_DIS_SensorOrderListType* SensorOrder;
  uint32 SamplingRate;
  uint16 UsedFrequencies;
  uint8 FinePositioningMethod;
  uint8 NumberOfSensors;
  uint8 PackageSeparationTime;
  uint8 SignalPulseDuration;
  uint8 SignalSeparationTime;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 3 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 3 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_LFA_EVSEFinePositioningSetupParametersType Exi_ISO_ED2_DIS_LFA_EVSEFinePositioningSetupParametersType;

struct Exi_ISO_ED2_DIS_LFA_EVSEFinePositioningSetupParametersType
{
  Exi_ISO_ED2_DIS_SensorListType* SensorList;
  Exi_ISO_ED2_DIS_SensorOrderListType* SensorOrder;
  uint16 SignalFrequency;
  uint8 NumberOfSensors;
  uint8 PackageSeparationTime;
  uint8 SignalPulseDuration;
  uint8 SignalSeparationTime;
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_MagneticVectorSetupType Exi_ISO_ED2_DIS_MagneticVectorSetupType;

struct Exi_ISO_ED2_DIS_MagneticVectorSetupType
{
  Exi_IDType* GAID;
  struct Exi_ISO_ED2_DIS_MagneticVectorSetupType* NextMagneticVectorSetupPtr;
  uint32 FrequencyChannel;
};

typedef struct Exi_ISO_ED2_DIS_MagneticVectorSetupListType Exi_ISO_ED2_DIS_MagneticVectorSetupListType;

struct Exi_ISO_ED2_DIS_MagneticVectorSetupListType
{
  Exi_ISO_ED2_DIS_MagneticVectorSetupType* MagneticVectorSetup;
};

typedef struct Exi_ISO_ED2_DIS_MV_EVSEFinePositioningSetupParametersType Exi_ISO_ED2_DIS_MV_EVSEFinePositioningSetupParametersType;

struct Exi_ISO_ED2_DIS_MV_EVSEFinePositioningSetupParametersType
{
  Exi_ISO_ED2_DIS_MagneticVectorSetupListType* MagneticVectorSetupList;
  uint32 FrequencyChannel;
  Exi_BitType FrequencyChannelFlag : 1;
  Exi_BitType MagneticVectorSetupListFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_Generic_EVFinePositioningParametersType Exi_ISO_ED2_DIS_Generic_EVFinePositioningParametersType;

struct Exi_ISO_ED2_DIS_Generic_EVFinePositioningParametersType
{
  Exi_ISO_ED2_DIS_ParameterSetType* GenericParameters;
};

typedef struct Exi_ISO_ED2_DIS_MeasurementDataListType Exi_ISO_ED2_DIS_MeasurementDataListType;

struct Exi_ISO_ED2_DIS_MeasurementDataListType
{
  uint16 MeasurementData[255];
  uint8 MeasurementDataCount;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 3 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 3 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_SensorMeasurementsType Exi_ISO_ED2_DIS_SensorMeasurementsType;

struct Exi_ISO_ED2_DIS_SensorMeasurementsType
{
  Exi_ISO_ED2_DIS_MeasurementDataListType* MeasurementDataList;
  struct Exi_ISO_ED2_DIS_SensorMeasurementsType* NextSensorMeasurementsPtr;
  sint8 EffectiveRadiatedPower;
  uint8 SensorID;
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_SensorPackageType Exi_ISO_ED2_DIS_SensorPackageType;

struct Exi_ISO_ED2_DIS_SensorPackageType
{
  struct Exi_ISO_ED2_DIS_SensorPackageType* NextSensorPackagePtr;
  Exi_ISO_ED2_DIS_SensorMeasurementsType* SensorMeasurements;
  uint32 PackageIndex;
};

typedef struct Exi_ISO_ED2_DIS_SensorPackageListType Exi_ISO_ED2_DIS_SensorPackageListType;

struct Exi_ISO_ED2_DIS_SensorPackageListType
{
  Exi_ISO_ED2_DIS_SensorPackageType* SensorPackage;
};

typedef struct Exi_ISO_ED2_DIS_LFA_EVFinePositioningParametersType Exi_ISO_ED2_DIS_LFA_EVFinePositioningParametersType;

struct Exi_ISO_ED2_DIS_LFA_EVFinePositioningParametersType
{
  Exi_ISO_ED2_DIS_SensorPackageListType* SensorPackageList;
  uint8 NumberOfSignalPackages;
  uint8 Preamble;
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_Generic_EVSEFinePositioningParametersType Exi_ISO_ED2_DIS_Generic_EVSEFinePositioningParametersType;

struct Exi_ISO_ED2_DIS_Generic_EVSEFinePositioningParametersType
{
  Exi_ISO_ED2_DIS_ParameterSetType* GenericParameters;
};

typedef struct Exi_ISO_ED2_DIS_LFA_EVSEFinePositioningParametersType Exi_ISO_ED2_DIS_LFA_EVSEFinePositioningParametersType;

struct Exi_ISO_ED2_DIS_LFA_EVSEFinePositioningParametersType
{
  Exi_ISO_ED2_DIS_SensorPackageListType* SensorPackageList;
  uint8 NumberOfSignalPackages;
  uint8 Preamble;
  Exi_BitType PreambleFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef enum
{
  EXI_ISO_ED2_DIS_FODSTATUS_TYPE_OBJECT_ON_PAD = 0u,
  EXI_ISO_ED2_DIS_FODSTATUS_TYPE_PAD_CLEAR = 1u,
  EXI_ISO_ED2_DIS_FODSTATUS_TYPE_UNKNOWN_ERROR = 2u
} Exi_ISO_ED2_DIS_FODStatusType;

typedef struct Exi_ISO_ED2_DIS_MagneticVectorType Exi_ISO_ED2_DIS_MagneticVectorType;

struct Exi_ISO_ED2_DIS_MagneticVectorType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* AngleGAtoVA;
  Exi_IDType* GAID;
  struct Exi_ISO_ED2_DIS_MagneticVectorType* NextMagneticVectorPtr;
  Exi_ISO_ED2_DIS_PhysicalValueType* RotationVAtoGA;
  uint16 Distance;
  Exi_ISO_ED2_DIS_FODStatusType FODStatus;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_MagneticVectorListType Exi_ISO_ED2_DIS_MagneticVectorListType;

struct Exi_ISO_ED2_DIS_MagneticVectorListType
{
  Exi_ISO_ED2_DIS_MagneticVectorType* MagneticVector;
};

typedef struct Exi_ISO_ED2_DIS_MV_EVSEFinePositioningParametersType Exi_ISO_ED2_DIS_MV_EVSEFinePositioningParametersType;

struct Exi_ISO_ED2_DIS_MV_EVSEFinePositioningParametersType
{
  Exi_ISO_ED2_DIS_MagneticVectorListType* MagneticVectorList;
};

typedef struct Exi_ISO_ED2_DIS_evIDType Exi_ISO_ED2_DIS_evIDType;

struct Exi_ISO_ED2_DIS_evIDType
{
  uint16 Length;
  uint8 Buffer[18];
};

typedef struct Exi_ISO_ED2_DIS_ACD_SSEnergyTransferModeType Exi_ISO_ED2_DIS_ACD_SSEnergyTransferModeType;

struct Exi_ISO_ED2_DIS_ACD_SSEnergyTransferModeType
{
  Exi_ISO_ED2_DIS_evIDType* EVID;
};

typedef struct Exi_ISO_ED2_DIS_WPT_SDlEnergyTransferModeType Exi_ISO_ED2_DIS_WPT_SDlEnergyTransferModeType;

struct Exi_ISO_ED2_DIS_WPT_SDlEnergyTransferModeType
{
  Exi_ISO_ED2_DIS_ServiceParameterListType* ServiceParameterList;
};

typedef struct Exi_ISO_ED2_DIS_PnC_AReqIdentificationModeType Exi_ISO_ED2_DIS_PnC_AReqIdentificationModeType;

struct Exi_ISO_ED2_DIS_PnC_AReqIdentificationModeType
{
  Exi_ISO_ED2_DIS_genChallengeType* GenChallenge;
  Exi_ISO_ED2_DIS_AttributeIdType* Id;
};

typedef struct Exi_ISO_ED2_DIS_Scheduled_CPDReqControlModeType Exi_ISO_ED2_DIS_Scheduled_CPDReqControlModeType;

struct Exi_ISO_ED2_DIS_Scheduled_CPDReqControlModeType
{
  Exi_ISO_ED2_DIS_ScheduleListType* ScheduleList;
  uint32 DepartureTime;
  uint16 MaxSupportingPoints;
  Exi_ISO_ED2_DIS_processingType EVProcessing;
  Exi_BitType DepartureTimeFlag : 1;
  Exi_BitType ScheduleListFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_Scheduled_CPDResControlModeType Exi_ISO_ED2_DIS_Scheduled_CPDResControlModeType;

struct Exi_ISO_ED2_DIS_Scheduled_CPDResControlModeType
{
  Exi_ISO_ED2_DIS_ScheduleListType* ScheduleList;
};

typedef struct Exi_ISO_ED2_DIS_Dynamic_CPDReqControlModeType Exi_ISO_ED2_DIS_Dynamic_CPDReqControlModeType;

struct Exi_ISO_ED2_DIS_Dynamic_CPDReqControlModeType
{
  uint32 DepartureTime;
};

typedef struct Exi_ISO_ED2_DIS_AC_CPDReqEnergyTransferModeType Exi_ISO_ED2_DIS_AC_CPDReqEnergyTransferModeType;

struct Exi_ISO_ED2_DIS_AC_CPDReqEnergyTransferModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
  Exi_BitType EVMaximumEnergyRequestFlag : 1;
  Exi_BitType EVMinimumEnergyRequestFlag : 1;
  Exi_BitType EVTargetEnergyRequestFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_AC_CPDResEnergyTransferModeType Exi_ISO_ED2_DIS_AC_CPDResEnergyTransferModeType;

struct Exi_ISO_ED2_DIS_AC_CPDResEnergyTransferModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSENominalFrequency;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSENominalVoltage;
};

typedef struct Exi_ISO_ED2_DIS_BPT_AC_CPDReqEnergyTransferModeType Exi_ISO_ED2_DIS_BPT_AC_CPDReqEnergyTransferModeType;

struct Exi_ISO_ED2_DIS_BPT_AC_CPDReqEnergyTransferModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumDischargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumDischargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumDischargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumDischargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
  Exi_BitType EVMaximumEnergyRequestFlag : 1;
  Exi_BitType EVMinimumEnergyRequestFlag : 1;
  Exi_BitType EVTargetEnergyRequestFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_BPT_AC_CPDResEnergyTransferModeType Exi_ISO_ED2_DIS_BPT_AC_CPDResEnergyTransferModeType;

struct Exi_ISO_ED2_DIS_BPT_AC_CPDResEnergyTransferModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumDischargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSENominalFrequency;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSENominalVoltage;
};

typedef struct Exi_ISO_ED2_DIS_DC_CPDReqEnergyTransferModeType Exi_ISO_ED2_DIS_DC_CPDReqEnergyTransferModeType;

struct Exi_ISO_ED2_DIS_DC_CPDReqEnergyTransferModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
  sint8 BulkSOC;
  sint8 TargetSOC;
  Exi_BitType BulkSOCFlag : 1;
  Exi_BitType EVMaximumChargePowerFlag : 1;
  Exi_BitType EVMaximumEnergyRequestFlag : 1;
  Exi_BitType EVMinimumChargePowerFlag : 1;
  Exi_BitType EVMinimumEnergyRequestFlag : 1;
  Exi_BitType EVTargetEnergyRequestFlag : 1;
  Exi_BitType TargetSOCFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_DC_CPDResEnergyTransferModeType Exi_ISO_ED2_DIS_DC_CPDResEnergyTransferModeType;

struct Exi_ISO_ED2_DIS_DC_CPDResEnergyTransferModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMinimumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMinimumVoltage;
};

typedef struct Exi_ISO_ED2_DIS_BPT_DC_CPDReqEnergyTransferModeType Exi_ISO_ED2_DIS_BPT_DC_CPDReqEnergyTransferModeType;

struct Exi_ISO_ED2_DIS_BPT_DC_CPDReqEnergyTransferModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumDischargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumDischargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumDischargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumDischargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
  sint8 BulkSOC;
  sint8 TargetSOC;
  Exi_BitType BulkSOCFlag : 1;
  Exi_BitType EVMaximumChargePowerFlag : 1;
  Exi_BitType EVMaximumDischargePowerFlag : 1;
  Exi_BitType EVMaximumEnergyRequestFlag : 1;
  Exi_BitType EVMinimumChargePowerFlag : 1;
  Exi_BitType EVMinimumDischargePowerFlag : 1;
  Exi_BitType EVMinimumEnergyRequestFlag : 1;
  Exi_BitType EVTargetEnergyRequestFlag : 1;
  Exi_BitType TargetSOCFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 2 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 2 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_BPT_DC_CPDResEnergyTransferModeType Exi_ISO_ED2_DIS_BPT_DC_CPDResEnergyTransferModeType;

struct Exi_ISO_ED2_DIS_BPT_DC_CPDResEnergyTransferModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumDischargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumDischargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMinimumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMinimumDischargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMinimumVoltage;
};

typedef struct Exi_ISO_ED2_DIS_WPT_CPDReqEnergyTransferModeType Exi_ISO_ED2_DIS_WPT_CPDReqEnergyTransferModeType;

struct Exi_ISO_ED2_DIS_WPT_CPDReqEnergyTransferModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumPower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumPower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
  Exi_BitType EVMaximumEnergyRequestFlag : 1;
  Exi_BitType EVMinimumEnergyRequestFlag : 1;
  Exi_BitType EVTargetEnergyRequestFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_WPT_CPDResEnergyTransferModeType Exi_ISO_ED2_DIS_WPT_CPDResEnergyTransferModeType;

struct Exi_ISO_ED2_DIS_WPT_CPDResEnergyTransferModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMaximumPower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEMinimumPower;
};

typedef struct Exi_ISO_ED2_DIS_EVPowerProfileType Exi_ISO_ED2_DIS_EVPowerProfileType;

struct Exi_ISO_ED2_DIS_EVPowerProfileType
{
  Exi_ISO_ED2_DIS_PowerScheduleEntryType* EVPowerProfileEntry;
};

typedef struct Exi_ISO_ED2_DIS_Scheduled_PDReqControlModeType Exi_ISO_ED2_DIS_Scheduled_PDReqControlModeType;

struct Exi_ISO_ED2_DIS_Scheduled_PDReqControlModeType
{
  Exi_ISO_ED2_DIS_EVPowerProfileType* EVPowerProfile;
  uint8 ScheduleTupleID;
  Exi_BitType EVPowerProfileFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_BPT_Scheduled_PDReqControlModeType Exi_ISO_ED2_DIS_BPT_Scheduled_PDReqControlModeType;

struct Exi_ISO_ED2_DIS_BPT_Scheduled_PDReqControlModeType
{
  Exi_ISO_ED2_DIS_EVPowerProfileType* EVPowerProfile;
  Exi_ISO_ED2_DIS_evOperationType EVOperation;
  uint8 ScheduleTupleID;
  Exi_BitType EVPowerProfileFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + (1 * EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE) + 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_Scheduled_MRReqControlModeType Exi_ISO_ED2_DIS_Scheduled_MRReqControlModeType;

struct Exi_ISO_ED2_DIS_Scheduled_MRReqControlModeType
{
  uint8 ScheduleTupleID;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_Scheduled_CLResControlModeType Exi_ISO_ED2_DIS_Scheduled_CLResControlModeType;

struct Exi_ISO_ED2_DIS_Scheduled_CLResControlModeType
{
  uint8 ScheduleTupleID;
  Exi_BitType ScheduleTupleIDFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_PnC_CLReqIdentificationModeType Exi_ISO_ED2_DIS_PnC_CLReqIdentificationModeType;

struct Exi_ISO_ED2_DIS_PnC_CLReqIdentificationModeType
{
  boolean MeteringReceiptRequested;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_PnC_CLResIdentificationModeType Exi_ISO_ED2_DIS_PnC_CLResIdentificationModeType;

struct Exi_ISO_ED2_DIS_PnC_CLResIdentificationModeType
{
  Exi_ISO_ED2_DIS_MeterInfoType* MeterInfo;
  Exi_BitType MeterInfoFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_Scheduled_CSReqControlModeType Exi_ISO_ED2_DIS_Scheduled_CSReqControlModeType;

struct Exi_ISO_ED2_DIS_Scheduled_CSReqControlModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
  boolean Standby;
  Exi_BitType EVMaximumChargeCurrentFlag : 1;
  Exi_BitType EVMaximumChargePowerFlag : 1;
  Exi_BitType EVMaximumEnergyRequestFlag : 1;
  Exi_BitType EVMinimumChargeCurrentFlag : 1;
  Exi_BitType EVMinimumEnergyRequestFlag : 1;
  Exi_BitType EVTargetEnergyRequestFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_Dynamic_CSReqControlModeType Exi_ISO_ED2_DIS_Dynamic_CSReqControlModeType;

struct Exi_ISO_ED2_DIS_Dynamic_CSReqControlModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
};

typedef struct Exi_ISO_ED2_DIS_Dynamic_CSResControlModeType Exi_ISO_ED2_DIS_Dynamic_CSResControlModeType;

struct Exi_ISO_ED2_DIS_Dynamic_CSResControlModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSETargetActivePower;
};

typedef struct Exi_ISO_ED2_DIS_Scheduled_CDReqControlModeType Exi_ISO_ED2_DIS_Scheduled_CDReqControlModeType;

struct Exi_ISO_ED2_DIS_Scheduled_CDReqControlModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetVoltage;
  boolean Standby;
  Exi_BitType EVMaximumChargeCurrentFlag : 1;
  Exi_BitType EVMaximumChargePowerFlag : 1;
  Exi_BitType EVMaximumEnergyRequestFlag : 1;
  Exi_BitType EVMaximumVoltageFlag : 1;
  Exi_BitType EVMinimumEnergyRequestFlag : 1;
  Exi_BitType EVTargetEnergyRequestFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_Dynamic_CDReqControlModeType Exi_ISO_ED2_DIS_Dynamic_CDReqControlModeType;

struct Exi_ISO_ED2_DIS_Dynamic_CDReqControlModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
};

typedef struct Exi_ISO_ED2_DIS_Scheduled_PDdReqControlModeType Exi_ISO_ED2_DIS_Scheduled_PDdReqControlModeType;

struct Exi_ISO_ED2_DIS_Scheduled_PDdReqControlModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumPower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumPower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVReceivedDCPower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetPower;
  boolean Standby;
  Exi_BitType EVMaximumEnergyRequestFlag : 1;
  Exi_BitType EVMaximumPowerFlag : 1;
  Exi_BitType EVMinimumEnergyRequestFlag : 1;
  Exi_BitType EVMinimumPowerFlag : 1;
  Exi_BitType EVTargetEnergyRequestFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) + 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_Dynamic_PDdReqControlModeType Exi_ISO_ED2_DIS_Dynamic_PDdReqControlModeType;

struct Exi_ISO_ED2_DIS_Dynamic_PDdReqControlModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumPower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumPower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
};

typedef struct Exi_ISO_ED2_DIS_Scheduled_ACBCReqControlModeType Exi_ISO_ED2_DIS_Scheduled_ACBCReqControlModeType;

struct Exi_ISO_ED2_DIS_Scheduled_ACBCReqControlModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumDischargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumDischargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumDischargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVPresentActivePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVPresentReactivePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
  boolean Standby;
  Exi_BitType EVMaximumChargeCurrentFlag : 1;
  Exi_BitType EVMaximumChargePowerFlag : 1;
  Exi_BitType EVMaximumDischargeCurrentFlag : 1;
  Exi_BitType EVMaximumDischargePowerFlag : 1;
  Exi_BitType EVMaximumEnergyRequestFlag : 1;
  Exi_BitType EVMinimumChargeCurrentFlag : 1;
  Exi_BitType EVMinimumDischargeCurrentFlag : 1;
  Exi_BitType EVMinimumEnergyRequestFlag : 1;
  Exi_BitType EVPresentActivePowerFlag : 1;
  Exi_BitType EVPresentReactivePowerFlag : 1;
  Exi_BitType EVTargetEnergyRequestFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_Scheduled_ACBCResControlModeType Exi_ISO_ED2_DIS_Scheduled_ACBCResControlModeType;

struct Exi_ISO_ED2_DIS_Scheduled_ACBCResControlModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSETargetActivePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSETargetReactivePower;
  Exi_BitType EVSETargetActivePowerFlag : 1;
  Exi_BitType EVSETargetReactivePowerFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( (1 * EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE) ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_Dynamic_ACBCReqControlModeType Exi_ISO_ED2_DIS_Dynamic_ACBCReqControlModeType;

struct Exi_ISO_ED2_DIS_Dynamic_ACBCReqControlModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumDischargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumDischargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumDischargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVPresentActivePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVPresentReactivePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
};

typedef struct Exi_ISO_ED2_DIS_Dynamic_ACBCResControlModeType Exi_ISO_ED2_DIS_Dynamic_ACBCResControlModeType;

struct Exi_ISO_ED2_DIS_Dynamic_ACBCResControlModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSETargetActivePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSETargetReactivePower;
};

typedef struct Exi_ISO_ED2_DIS_Scheduled_DCBCReqControlModeType Exi_ISO_ED2_DIS_Scheduled_DCBCReqControlModeType;

struct Exi_ISO_ED2_DIS_Scheduled_DCBCReqControlModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumDischargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumDischargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetVoltage;
  boolean Standby;
  Exi_BitType EVMaximumChargeCurrentFlag : 1;
  Exi_BitType EVMaximumChargePowerFlag : 1;
  Exi_BitType EVMaximumDischargeCurrentFlag : 1;
  Exi_BitType EVMaximumDischargePowerFlag : 1;
  Exi_BitType EVMaximumEnergyRequestFlag : 1;
  Exi_BitType EVMaximumVoltageFlag : 1;
  Exi_BitType EVMinimumEnergyRequestFlag : 1;
  Exi_BitType EVMinimumVoltageFlag : 1;
  Exi_BitType EVTargetEnergyRequestFlag : 1;
#if (STD_ON == EXI_STRUCTURE_PADDING_16BIT_ENABLE)
# if (0 != ( ( 1 ) % 2 ) )
  uint8 Exi_Padding16Bit[1];
# endif
#endif 
#if (STD_ON == EXI_STRUCTURE_PADDING_32BIT_ENABLE)
# if (0 != ( ( 1 ) % 4 ) )
  uint8 Exi_Padding32Bit[4 - (( 1 ) % 4)];
# endif
#endif 
};

typedef struct Exi_ISO_ED2_DIS_Dynamic_DCBCReqControlModeType Exi_ISO_ED2_DIS_Dynamic_DCBCReqControlModeType;

struct Exi_ISO_ED2_DIS_Dynamic_DCBCReqControlModeType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumChargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumDischargeCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumDischargePower;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMaximumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumEnergyRequest;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVMinimumVoltage;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVTargetEnergyRequest;
};

typedef struct Exi_ISO_ED2_DIS_Efficiency_ACReqAlignmentCheckMechanismType Exi_ISO_ED2_DIS_Efficiency_ACReqAlignmentCheckMechanismType;

struct Exi_ISO_ED2_DIS_Efficiency_ACReqAlignmentCheckMechanismType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVPowerToRESS;
};

typedef struct Exi_ISO_ED2_DIS_Efficiency_ACResAlignmentCheckMechanismType Exi_ISO_ED2_DIS_Efficiency_ACResAlignmentCheckMechanismType;

struct Exi_ISO_ED2_DIS_Efficiency_ACResAlignmentCheckMechanismType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEPowerFromGrid;
};

typedef struct Exi_ISO_ED2_DIS_RSSI_ACReqAlignmentCheckMechanismType Exi_ISO_ED2_DIS_RSSI_ACReqAlignmentCheckMechanismType;

struct Exi_ISO_ED2_DIS_RSSI_ACReqAlignmentCheckMechanismType
{
  Exi_ISO_ED2_DIS_ParameterSetType* Generic;
};

typedef struct Exi_ISO_ED2_DIS_RSSI_ACResAlignmentCheckMechanismType Exi_ISO_ED2_DIS_RSSI_ACResAlignmentCheckMechanismType;

struct Exi_ISO_ED2_DIS_RSSI_ACResAlignmentCheckMechanismType
{
  Exi_ISO_ED2_DIS_ParameterSetType* Generic;
};

typedef struct Exi_ISO_ED2_DIS_Voltage_ACReqAlignmentCheckMechanismType Exi_ISO_ED2_DIS_Voltage_ACReqAlignmentCheckMechanismType;

struct Exi_ISO_ED2_DIS_Voltage_ACReqAlignmentCheckMechanismType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVVoltage;
};

typedef struct Exi_ISO_ED2_DIS_Voltage_ACResAlignmentCheckMechanismType Exi_ISO_ED2_DIS_Voltage_ACResAlignmentCheckMechanismType;

struct Exi_ISO_ED2_DIS_Voltage_ACResAlignmentCheckMechanismType
{
  Exi_ISO_ED2_DIS_ParameterSetType* Generic;
};

typedef struct Exi_ISO_ED2_DIS_Coupling_ACReqAlignmentCheckMechanismType Exi_ISO_ED2_DIS_Coupling_ACReqAlignmentCheckMechanismType;

struct Exi_ISO_ED2_DIS_Coupling_ACReqAlignmentCheckMechanismType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVCurrentToRESS;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVPowerToRESS;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVVoltageToRESS;
};

typedef struct Exi_ISO_ED2_DIS_Coupling_ACResAlignmentCheckMechanismType Exi_ISO_ED2_DIS_Coupling_ACResAlignmentCheckMechanismType;

struct Exi_ISO_ED2_DIS_Coupling_ACResAlignmentCheckMechanismType
{
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSECoilCurrent;
  Exi_ISO_ED2_DIS_PhysicalValueType* EVSEPowerFromGrid;
};

/* PRQA L:IDENTIFIER_NAMES */
/* PRQA L:NAMESPACE */
/* PRQA L:UNION */


#endif
  /* EXI_ISO_ED2_DIS_SCHEMA_TYPES_H */
