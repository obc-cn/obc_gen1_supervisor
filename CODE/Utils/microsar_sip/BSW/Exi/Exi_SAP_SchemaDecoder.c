/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Exi_SAP_SchemaDecoder.c
 *        \brief  Efficient XML Interchange SAP decoder source file
 *
 *      \details  Vector static code implementation for the Efficient XML Interchange sub-component SAP decoder.
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file Exi.h.
 *********************************************************************************************************************/
/* PRQA S 0857 EOF */ /* MD_Exi_1.1_0857 */ /* [L] Number of macro definitions exceeds 1024 - program is non-conforming. */

#define EXI_SAP_SCHEMA_DECODER_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 EXI_SAP_SCHEMA_DECODER_C_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "Exi_SAP_SchemaDecoder.h"
#include "Exi_BSDecoder.h"
#include "Exi_Priv.h"
/* PRQA L:EXI_SAP_SCHEMA_DECODER_C_IF_NESTING */ /* MD_MSR_1.1_828 */
/**********************************************************************************************************************
*  VERSION CHECK
*********************************************************************************************************************/
#if ( (EXI_SW_MAJOR_VERSION != 6u) || (EXI_SW_MINOR_VERSION != 0u) || (EXI_SW_PATCH_VERSION != 1u) )
  #error "Vendor specific version numbers of Exi.h and Exi_SAP_SchemaDecoder.c are inconsistent"
#endif

#if (!defined (EXI_ENABLE_DECODE_SAP_MESSAGE_SET))
# if (defined (EXI_ENABLE_SAP_MESSAGE_SET))
#  define EXI_ENABLE_DECODE_SAP_MESSAGE_SET   EXI_ENABLE_SAP_MESSAGE_SET
# else
#  define EXI_ENABLE_DECODE_SAP_MESSAGE_SET   STD_OFF
# endif
#endif

#if (defined(EXI_ENABLE_DECODE_SAP_MESSAGE_SET) && (EXI_ENABLE_DECODE_SAP_MESSAGE_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */

/* PRQA S 0715 NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */ /* MD_Exi_1.1 */


#define EXI_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/**********************************************************************************************************************
 *  FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Exi_Decode_SAP_AppProtocol
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_SAP_APP_PROTOCOL) && (EXI_DECODE_SAP_APP_PROTOCOL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_SAP_AppProtocol( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_SAP_AppProtocolType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_SAP_AppProtocolType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_SAP_AppProtocolType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_SAP_AppProtocolType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element ProtocolNamespace */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    if(0 == exiEventCode) /* SE(ProtocolNamespace) */
    {
      #if (defined(EXI_DECODE_SAP_PROTOCOL_NAMESPACE) && (EXI_DECODE_SAP_PROTOCOL_NAMESPACE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_SAP_protocolNamespace(DecWsPtr, &(structPtr->ProtocolNamespace));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_SAP_APP_PROTOCOL, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_SAP_PROTOCOL_NAMESPACE) && (EXI_DECODE_SAP_PROTOCOL_NAMESPACE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    /* check exiEventCode equals EE(ProtocolNamespace) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element VersionNumberMajor */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    if(0 == exiEventCode) /* SE(VersionNumberMajor) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_SAP_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->VersionNumberMajor);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    /* check exiEventCode equals EE(VersionNumberMajor) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element VersionNumberMinor */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    if(0 == exiEventCode) /* SE(VersionNumberMinor) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_SAP_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->VersionNumberMinor);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    /* check exiEventCode equals EE(VersionNumberMinor) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element SchemaID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    if(0 == exiEventCode) /* SE(SchemaID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_SAP_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt8(&DecWsPtr->DecWs, &structPtr->SchemaID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    /* check exiEventCode equals EE(SchemaID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element Priority */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    if(0 == exiEventCode) /* SE(Priority) */
    {
      Exi_BitBufType value = 0;
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_SAP_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 5);
        structPtr->Priority = (uint8)(value + 1);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    /* check exiEventCode equals EE(Priority) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SAP_APP_PROTOCOL, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_SAP_APP_PROTOCOL) && (EXI_DECODE_SAP_APP_PROTOCOL == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_SAP_protocolNamespace
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_SAP_PROTOCOL_NAMESPACE) && (EXI_DECODE_SAP_PROTOCOL_NAMESPACE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_SAP_protocolNamespace( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_SAP_protocolNamespaceType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_SAP_protocolNamespaceType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_SAP_protocolNamespaceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_SAP_protocolNamespaceType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode protocolNamespace as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SAP_PROTOCOL_NAMESPACE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_SAP_PROTOCOL_NAMESPACE) && (EXI_DECODE_SAP_PROTOCOL_NAMESPACE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_SAP_responseCode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_SAP_RESPONSE_CODE) && (EXI_DECODE_SAP_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_SAP_responseCode( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_SAP_responseCodeType, AUTOMATIC, EXI_APPL_VAR) responseCodePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (responseCodePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value responseCode element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 3)
      {
        /* #40 Store value in output buffer */
        *responseCodePtr = (Exi_SAP_responseCodeType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SAP_RESPONSE_CODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_SAP_RESPONSE_CODE) && (EXI_DECODE_SAP_RESPONSE_CODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_SAP_supportedAppProtocolReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ) && (EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_SAP_supportedAppProtocolReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_SAP_supportedAppProtocolReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_SAP_supportedAppProtocolReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_SAP_AppProtocolType) lastAppProtocol;
  #if (defined(EXI_DECODE_SAP_APP_PROTOCOL) && (EXI_DECODE_SAP_APP_PROTOCOL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  uint16_least i;
  #endif /* #if (defined(EXI_DECODE_SAP_APP_PROTOCOL) && (EXI_DECODE_SAP_APP_PROTOCOL == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_SAP_supportedAppProtocolReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_SAP_supportedAppProtocolReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_SAP_SUPPORTED_APP_PROTOCOL_REQ_TYPE);
    /* #40 Decode multi occurence element AppProtocol, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    if(0 == exiEventCode) /* SE(AppProtocol) */
    {
      #if (defined(EXI_DECODE_SAP_APP_PROTOCOL) && (EXI_DECODE_SAP_APP_PROTOCOL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_SAP_AppProtocol(DecWsPtr, &(structPtr->AppProtocol));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_SAP_APP_PROTOCOL) && (EXI_DECODE_SAP_APP_PROTOCOL == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->AppProtocol->NextAppProtocolPtr = (Exi_SAP_AppProtocolType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    /* check exiEventCode equals EE(AppProtocol) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastAppProtocol = structPtr->AppProtocol;
    #if (defined(EXI_DECODE_SAP_APP_PROTOCOL) && (EXI_DECODE_SAP_APP_PROTOCOL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #50 Decode subsequent occurences of AppProtocol */
    for(i=0; i<19; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
      if(0 == exiEventCode)/* SE(AppProtocol) */
      {
        Exi_Decode_SAP_AppProtocol(DecWsPtr, &(lastAppProtocol->NextAppProtocolPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastAppProtocol = lastAppProtocol->NextAppProtocolPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(AppProtocol) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* (defined(EXI_DECODE_SAP_APP_PROTOCOL) && (EXI_DECODE_SAP_APP_PROTOCOL == STD_ON)) */
    lastAppProtocol->NextAppProtocolPtr = (Exi_SAP_AppProtocolType*)NULL_PTR;

    /* #60 If supportedAppProtocolReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      #if (defined(EXI_DECODE_SAP_APP_PROTOCOL) && (EXI_DECODE_SAP_APP_PROTOCOL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      exiEventCode = 0;
      if(19 == i)
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      }
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
      if(0 == exiEventCode)/* EE(supportedAppProtocolReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      #endif /* #if (defined(EXI_DECODE_SAP_APP_PROTOCOL) && (EXI_DECODE_SAP_APP_PROTOCOL == STD_ON)) */
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ) && (EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_SAP_supportedAppProtocolRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES) && (EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_SAP_supportedAppProtocolRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_SAP_supportedAppProtocolResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_SAP_supportedAppProtocolResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_SAP_supportedAppProtocolResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_SAP_supportedAppProtocolResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_SAP_SUPPORTED_APP_PROTOCOL_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_SAP_RESPONSE_CODE) && (EXI_DECODE_SAP_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_SAP_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_SAP_RESPONSE_CODE) && (EXI_DECODE_SAP_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element SchemaID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
    if(0 == exiEventCode) /* SE(SchemaID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_SAP_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->SchemaIDFlag = 1;
        Exi_VBSDecodeUInt8(&DecWsPtr->DecWs, &structPtr->SchemaID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->SchemaIDFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
      /* check exiEventCode equals EE(SchemaID) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #70 If supportedAppProtocolRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->SchemaIDFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_SAP_TYPE);
        if(0 == exiEventCode)/* EE(supportedAppProtocolRes) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(supportedAppProtocolRes) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES) && (EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_SchemaSet_SAP
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_SCHEMA_SET_SAP) && (EXI_DECODE_SCHEMA_SET_SAP == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_SchemaSet_SAP( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(void, AUTOMATIC, EXI_APPL_VAR) structPtr = NULL_PTR;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode all SAP schema elements */
    exiEventCode = 0;
    /* #30 Decode the event code of the element */
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* #40 Switch event code */
    switch(exiEventCode)
    {
    case 0: /* SE(supportedAppProtocolReq) */
      /* #50 Element supportedAppProtocolReq */
      {
        /* #60 Decode element supportedAppProtocolReq */
      #if (defined(EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ) && (EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_SAP_supportedAppProtocolReq(DecWsPtr, (Exi_SAP_supportedAppProtocolReqType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_SAP, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ) && (EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ == STD_ON)) */
      }
    case 1: /* SE(supportedAppProtocolRes) */
      /* #70 Element supportedAppProtocolRes */
      {
        /* #80 Decode element supportedAppProtocolRes */
      #if (defined(EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES) && (EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_SAP_supportedAppProtocolRes(DecWsPtr, (Exi_SAP_supportedAppProtocolResType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_SAP, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES) && (EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES == STD_ON)) */
      }
    default:
      /* #90 Unknown element */
      {
        /* #100 Set status code to error */
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_SAP, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_SCHEMA_SET_SAP) && (EXI_DECODE_SCHEMA_SET_SAP == STD_ON)) */


#define EXI_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/* PRQA L:NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */

#endif /* (defined (EXI_ENABLE_DECODE_SAP_MESSAGE_SET) && (EXI_ENABLE_DECODE_SAP_MESSAGE_SET == STD_ON)) */

