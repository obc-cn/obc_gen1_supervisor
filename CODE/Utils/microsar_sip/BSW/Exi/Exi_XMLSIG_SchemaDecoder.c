/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Exi_XMLSIG_SchemaDecoder.c
 *        \brief  Efficient XML Interchange XMLSIG decoder source file
 *
 *      \details  Vector static code implementation for the Efficient XML Interchange sub-component XMLSIG decoder.
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file Exi.h.
 *********************************************************************************************************************/
/* PRQA S 0857 EOF */ /* MD_Exi_1.1_0857 */ /* [L] Number of macro definitions exceeds 1024 - program is non-conforming. */

#define EXI_XMLSIG_SCHEMA_DECODER_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 EXI_XMLSIG_SCHEMA_DECODER_C_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "Exi_XMLSIG_SchemaDecoder.h"
#include "Exi_BSDecoder.h"
#include "Exi_Priv.h"
/* PRQA L:EXI_XMLSIG_SCHEMA_DECODER_C_IF_NESTING */ /* MD_MSR_1.1_828 */
/**********************************************************************************************************************
*  VERSION CHECK
*********************************************************************************************************************/
#if ( (EXI_SW_MAJOR_VERSION != 6u) || (EXI_SW_MINOR_VERSION != 0u) || (EXI_SW_PATCH_VERSION != 1u) )
  #error "Vendor specific version numbers of Exi.h and Exi_XMLSIG_SchemaDecoder.c are inconsistent"
#endif

#if (!defined (EXI_ENABLE_DECODE_XMLSIG_MESSAGE_SET))
# if (defined (EXI_ENABLE_XMLSIG_MESSAGE_SET))
#  define EXI_ENABLE_DECODE_XMLSIG_MESSAGE_SET   EXI_ENABLE_XMLSIG_MESSAGE_SET
# else
#  define EXI_ENABLE_DECODE_XMLSIG_MESSAGE_SET   STD_OFF
# endif
#endif

#if (defined(EXI_ENABLE_DECODE_XMLSIG_MESSAGE_SET) && (EXI_ENABLE_DECODE_XMLSIG_MESSAGE_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */

/* PRQA S 0715 NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */ /* MD_Exi_1.1 */


#define EXI_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/**********************************************************************************************************************
 *  FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_AttributeAlgorithm
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_AttributeAlgorithm( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_AttributeAlgorithmType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_AttributeAlgorithmType, AUTOMATIC, EXI_APPL_VAR) structPtr;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_AttributeAlgorithmType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_AttributeAlgorithmType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode AttributeAlgorithm as string value */
    structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
    Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_AttributeEncoding
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ENCODING) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ENCODING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_AttributeEncoding( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_AttributeEncodingType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_AttributeEncodingType, AUTOMATIC, EXI_APPL_VAR) structPtr;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_AttributeEncodingType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_AttributeEncodingType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode AttributeEncoding as string value */
    structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
    Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_ATTRIBUTE_ENCODING, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ENCODING) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ENCODING == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_AttributeId
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_AttributeId( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_AttributeIdType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_AttributeIdType, AUTOMATIC, EXI_APPL_VAR) structPtr;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_AttributeIdType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_AttributeIdType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode AttributeId as string value */
    structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
    Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_ATTRIBUTE_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_AttributeMime
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_MIME) && (EXI_DECODE_XMLSIG_ATTRIBUTE_MIME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_AttributeMime( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_AttributeMimeType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_AttributeMimeType, AUTOMATIC, EXI_APPL_VAR) structPtr;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_AttributeMimeType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_AttributeMimeType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode AttributeMime as string value */
    structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
    Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_ATTRIBUTE_MIME, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_MIME) && (EXI_DECODE_XMLSIG_ATTRIBUTE_MIME == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_AttributeTarget
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_TARGET) && (EXI_DECODE_XMLSIG_ATTRIBUTE_TARGET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_AttributeTarget( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_AttributeTargetType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_AttributeTargetType, AUTOMATIC, EXI_APPL_VAR) structPtr;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_AttributeTargetType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_AttributeTargetType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode AttributeTarget as string value */
    structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
    Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_ATTRIBUTE_TARGET, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_TARGET) && (EXI_DECODE_XMLSIG_ATTRIBUTE_TARGET == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_Attribute
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE) && (EXI_DECODE_XMLSIG_ATTRIBUTE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_Attribute( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_AttributeType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_AttributeType, AUTOMATIC, EXI_APPL_VAR) structPtr;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_AttributeType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_AttributeType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode Attribute as string value */
    structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
    Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_ATTRIBUTE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE) && (EXI_DECODE_XMLSIG_ATTRIBUTE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_AttributeURI
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_URI) && (EXI_DECODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_AttributeURI( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_AttributeURIType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_AttributeURIType, AUTOMATIC, EXI_APPL_VAR) structPtr;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_AttributeURIType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_AttributeURIType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode AttributeURI as string value */
    structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
    Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_ATTRIBUTE_URI, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_URI) && (EXI_DECODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_CanonicalizationMethod
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD) && (EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_CanonicalizationMethod( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_CanonicalizationMethodType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_CanonicalizationMethodType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_GenericElementType) lastGenericElement;
  #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_CanonicalizationMethodType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_CanonicalizationMethodType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_CANONICALIZATION_METHOD_TYPE);
    /* #40 Decode attribute Algorithm */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* AT(Algorithm) */
    {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeAlgorithm(DecWsPtr, &(structPtr->Algorithm));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_CANONICALIZATION_METHOD, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode multi occurence element GenericElement, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(GenericElement) */
    {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->GenericElement));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->GenericElementFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_CANONICALIZATION_METHOD, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->GenericElementFlag)
    {
      structPtr->GenericElement->NextGenericElementPtr = (Exi_XMLSIG_GenericElementType*)NULL_PTR;
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* check exiEventCode equals EE(GenericElement) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
      {
        /* an error occured in a previous step -> abort to avoid not required loop */
        return;
      }
      lastGenericElement = structPtr->GenericElement;
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
      /* #60 Decode subsequent occurences of GenericElement */
      for(i=0; i<(EXI_MAXOCCURS_GENERICELEMENT - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
      { /* PRQA S 3201 */ /* MD_MSR_14.1 */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
        if(0 == exiEventCode)/* SE(GenericElement) */
        {
          Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(lastGenericElement->NextGenericElementPtr));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          lastGenericElement = lastGenericElement->NextGenericElementPtr;
        }
        else if(1 == exiEventCode)/* reached next Tag */
        {
          DecWsPtr->DecWs.EERequired = FALSE;
          break;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* check exiEventCode equals EE(GenericElement) */
        if(0 != exiEventCode)
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      lastGenericElement->NextGenericElementPtr = (Exi_XMLSIG_GenericElementType*)NULL_PTR;
      exiEventCode = 0;
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
      if((EXI_MAXOCCURS_GENERICELEMENT - 1) == i)
      #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        if(1 == exiEventCode)
        {
          DecWsPtr->DecWs.EERequired = FALSE;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #70 If CanonicalizationMethod was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->GenericElementFlag)
      {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
        if(FALSE == DecWsPtr->DecWs.EERequired)
        {
          /* last element unbounded, EE(CanonicalizationMethod) already found, EXI stream end reached */
        }
        else
        {
          /* unexpected state */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        }
      #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      }
      else
      {
        /* EE(CanonicalizationMethod) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_CANONICALIZATION_METHOD, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD) && (EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_CryptoBinary
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_CryptoBinary( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_CryptoBinaryType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_CryptoBinaryType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_CryptoBinaryType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_CryptoBinaryType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode CryptoBinary as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_CRYPTO_BINARY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_DSAKeyValue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_DSAKEY_VALUE) && (EXI_DECODE_XMLSIG_DSAKEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_DSAKeyValue( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_DSAKeyValueType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_DSAKeyValueType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_DSAKeyValueType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_DSAKeyValueType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_DSAKEY_VALUE_TYPE);
    /* #40 Decode element P */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(P) */
    {
      #if (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_CryptoBinary(DecWsPtr, &(structPtr->P));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(P) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element Q */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(Q) */
    {
      #if (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_CryptoBinary(DecWsPtr, &(structPtr->Q));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(Q) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element G */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(G) */
    {
      #if (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_CryptoBinary(DecWsPtr, &(structPtr->G));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->GFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->GFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* check exiEventCode equals EE(G) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #70 Decode element Y with missing elements in front */
    if(0 == structPtr->GFlag)
    {
      /* SE(Y) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_CryptoBinary(DecWsPtr, &(structPtr->Y));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    }
    /* #80 Decode element Y without missing elements in front */
    else
    {
      /* #90 Decode element Y */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* SE(Y) */
      {
      #if (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_CryptoBinary(DecWsPtr, &(structPtr->Y));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(Y) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #100 Decode element J */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(J) */
    {
      #if (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_CryptoBinary(DecWsPtr, &(structPtr->J));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->JFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->JFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* check exiEventCode equals EE(J) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #110 Decode element Seed with missing elements in front */
    if(0 == structPtr->JFlag)
    {
      /* SE(Seed) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_CryptoBinary(DecWsPtr, &(structPtr->Seed));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    }
    /* #120 Decode element Seed without missing elements in front */
    else
    {
      /* #130 Decode element Seed */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* SE(Seed) */
      {
      #if (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_CryptoBinary(DecWsPtr, &(structPtr->Seed));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(Seed) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #140 Decode element PgenCounter */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(PgenCounter) */
    {
      #if (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_CryptoBinary(DecWsPtr, &(structPtr->PgenCounter));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(PgenCounter) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #150 If DSAKeyValue was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode)/* EE(DSAKeyValue) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_DSAKEY_VALUE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_DSAKEY_VALUE) && (EXI_DECODE_XMLSIG_DSAKEY_VALUE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_DigestMethod
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_DIGEST_METHOD) && (EXI_DECODE_XMLSIG_DIGEST_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_DigestMethod( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_DigestMethodType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_DigestMethodType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_GenericElementType) lastGenericElement;
  #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_DigestMethodType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_DigestMethodType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_DIGEST_METHOD_TYPE);
    /* #40 Decode attribute Algorithm */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* AT(Algorithm) */
    {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeAlgorithm(DecWsPtr, &(structPtr->Algorithm));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_DIGEST_METHOD, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode multi occurence element GenericElement, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(GenericElement) */
    {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->GenericElement));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->GenericElementFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_DIGEST_METHOD, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->GenericElementFlag)
    {
      structPtr->GenericElement->NextGenericElementPtr = (Exi_XMLSIG_GenericElementType*)NULL_PTR;
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* check exiEventCode equals EE(GenericElement) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
      {
        /* an error occured in a previous step -> abort to avoid not required loop */
        return;
      }
      lastGenericElement = structPtr->GenericElement;
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
      /* #60 Decode subsequent occurences of GenericElement */
      for(i=0; i<(EXI_MAXOCCURS_GENERICELEMENT - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
      { /* PRQA S 3201 */ /* MD_MSR_14.1 */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
        if(0 == exiEventCode)/* SE(GenericElement) */
        {
          Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(lastGenericElement->NextGenericElementPtr));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          lastGenericElement = lastGenericElement->NextGenericElementPtr;
        }
        else if(1 == exiEventCode)/* reached next Tag */
        {
          DecWsPtr->DecWs.EERequired = FALSE;
          break;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* check exiEventCode equals EE(GenericElement) */
        if(0 != exiEventCode)
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      lastGenericElement->NextGenericElementPtr = (Exi_XMLSIG_GenericElementType*)NULL_PTR;
      exiEventCode = 0;
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
      if((EXI_MAXOCCURS_GENERICELEMENT - 1) == i)
      #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        if(1 == exiEventCode)
        {
          DecWsPtr->DecWs.EERequired = FALSE;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #70 If DigestMethod was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->GenericElementFlag)
      {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
        if(FALSE == DecWsPtr->DecWs.EERequired)
        {
          /* last element unbounded, EE(DigestMethod) already found, EXI stream end reached */
        }
        else
        {
          /* unexpected state */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        }
      #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      }
      else
      {
        /* EE(DigestMethod) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_DIGEST_METHOD, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_DIGEST_METHOD) && (EXI_DECODE_XMLSIG_DIGEST_METHOD == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_DigestValue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_DIGEST_VALUE) && (EXI_DECODE_XMLSIG_DIGEST_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_DigestValue( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_DigestValueType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_DigestValueType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_DigestValueType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_DigestValueType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode DigestValue as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_DIGEST_VALUE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_DIGEST_VALUE) && (EXI_DECODE_XMLSIG_DIGEST_VALUE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_GenericElement
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_GenericElement( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_GenericElementType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_GenericElementType, AUTOMATIC, EXI_APPL_VAR) structPtr;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_GenericElementType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_GenericElementType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_GENERIC_ELEMENT, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_KeyInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_KEY_INFO) && (EXI_DECODE_XMLSIG_KEY_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_KeyInfo( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_KeyInfoType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_KeyInfoType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_KeyInfoChoiceType) lastChoiceElement;
  #if ((EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE - 1) > 0) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_KeyInfoType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_KeyInfoType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_KEY_INFO_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 4);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 9, 4, FALSE, TRUE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */
    }
    /* Choice element 'KeyName' will be decoded later */
    /* Choice element 'KeyValue' will be decoded later */
    /* Choice element 'RetrievalMethod' will be decoded later */
    /* Choice element 'X509Data' will be decoded later */
    /* Choice element 'PGPData' will be decoded later */
    /* Choice element 'SPKIData' will be decoded later */
    /* Choice element 'MgmtData' will be decoded later */
    /* Choice element 'GenericElement' will be decoded later */
    else if(exiEventCode < 9)
    {
      /* This is an EXI choice element. Event code is evalulated later, do not set Flag here because buffer check and ChoiceElement initialization is required first */
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode choice element KeyInfoChoice with missing elements in front */
    if(0 == structPtr->IdFlag)
    {
      /* Start of Choice Element */
      /* #60 Allocate buffer for this element */
      structPtr->ChoiceElement = (Exi_XMLSIG_KeyInfoChoiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_KeyInfoChoiceType), TRUE);
      if (NULL_PTR == (void*)structPtr->ChoiceElement)
      {
        return;
      }
      /* #70 Switch event code */
      switch(exiEventCode)
      {
      case 1: /* SE(KeyName) */
        /* #80 Element KeyName */
        {
          /* #90 Decode element KeyName */
        #if (defined(EXI_DECODE_XMLSIG_KEY_NAME) && (EXI_DECODE_XMLSIG_KEY_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          Exi_Decode_XMLSIG_KeyName(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.KeyName));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          structPtr->ChoiceElement->KeyNameFlag = 1;
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_XMLSIG_KEY_NAME) && (EXI_DECODE_XMLSIG_KEY_NAME == STD_ON)) */
        }
      case 2: /* SE(KeyValue) */
        /* #100 Element KeyValue */
        {
          /* #110 Decode element KeyValue */
        #if (defined(EXI_DECODE_XMLSIG_KEY_VALUE) && (EXI_DECODE_XMLSIG_KEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          Exi_Decode_XMLSIG_KeyValue(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.KeyValue));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          structPtr->ChoiceElement->KeyValueFlag = 1;
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_XMLSIG_KEY_VALUE) && (EXI_DECODE_XMLSIG_KEY_VALUE == STD_ON)) */
        }
      case 3: /* SE(RetrievalMethod) */
        /* #120 Element RetrievalMethod */
        {
          /* #130 Decode element RetrievalMethod */
        #if (defined(EXI_DECODE_XMLSIG_RETRIEVAL_METHOD) && (EXI_DECODE_XMLSIG_RETRIEVAL_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          DecWsPtr->DecWs.EERequired = TRUE;
          Exi_Decode_XMLSIG_RetrievalMethod(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.RetrievalMethod));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          structPtr->ChoiceElement->RetrievalMethodFlag = 1;
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_XMLSIG_RETRIEVAL_METHOD) && (EXI_DECODE_XMLSIG_RETRIEVAL_METHOD == STD_ON)) */
        }
      case 4: /* SE(X509Data) */
        /* #140 Element X509Data */
        {
          /* #150 Decode element X509Data */
        #if (defined(EXI_DECODE_XMLSIG_X509DATA) && (EXI_DECODE_XMLSIG_X509DATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          DecWsPtr->DecWs.EERequired = TRUE;
          Exi_Decode_XMLSIG_X509Data(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.X509Data));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          structPtr->ChoiceElement->X509DataFlag = 1;
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_XMLSIG_X509DATA) && (EXI_DECODE_XMLSIG_X509DATA == STD_ON)) */
        }
      case 5: /* SE(PGPData) */
        /* #160 Element PGPData */
        {
          /* #170 Decode element PGPData */
        #if (defined(EXI_DECODE_XMLSIG_PGPDATA) && (EXI_DECODE_XMLSIG_PGPDATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          Exi_Decode_XMLSIG_PGPData(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.PGPData));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          structPtr->ChoiceElement->PGPDataFlag = 1;
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_XMLSIG_PGPDATA) && (EXI_DECODE_XMLSIG_PGPDATA == STD_ON)) */
        }
      case 6: /* SE(SPKIData) */
        /* #180 Element SPKIData */
        {
          /* #190 Decode element SPKIData */
        #if (defined(EXI_DECODE_XMLSIG_SPKIDATA) && (EXI_DECODE_XMLSIG_SPKIDATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          DecWsPtr->DecWs.EERequired = TRUE;
          Exi_Decode_XMLSIG_SPKIData(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.SPKIData));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          structPtr->ChoiceElement->SPKIDataFlag = 1;
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_XMLSIG_SPKIDATA) && (EXI_DECODE_XMLSIG_SPKIDATA == STD_ON)) */
        }
      case 7: /* SE(MgmtData) */
        /* #200 Element MgmtData */
        {
          /* #210 Decode element MgmtData */
        #if (defined(EXI_DECODE_XMLSIG_MGMT_DATA) && (EXI_DECODE_XMLSIG_MGMT_DATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          Exi_Decode_XMLSIG_MgmtData(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.MgmtData));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          structPtr->ChoiceElement->MgmtDataFlag = 1;
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_XMLSIG_MGMT_DATA) && (EXI_DECODE_XMLSIG_MGMT_DATA == STD_ON)) */
        }
      case 8: /* SE(GenericElement) */
        /* #220 Element GenericElement */
        {
          /* #230 Decode element GenericElement */
        #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.GenericElement));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          structPtr->ChoiceElement->GenericElementFlag = 1;
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
        }
      default:
        /* #240 Unknown element */
        {
          /* #250 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* #260 Decode choice element ChoiceElement without missing elements in front */
    else
    {
      /* Start of Choice Element */
      /* #270 Allocate buffer for this element */
      structPtr->ChoiceElement = (Exi_XMLSIG_KeyInfoChoiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_KeyInfoChoiceType), TRUE);
      if (NULL_PTR == (void*)structPtr->ChoiceElement)
      {
        return;
      }
      /* #280 Decode KeyInfoChoice element */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 4);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 8, 4, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* SE(KeyName) */
      {
        #if (defined(EXI_DECODE_XMLSIG_KEY_NAME) && (EXI_DECODE_XMLSIG_KEY_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_KeyName(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.KeyName));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->ChoiceElement->KeyNameFlag = 1;
        #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
        #endif /* (defined(EXI_DECODE_XMLSIG_KEY_NAME) && (EXI_DECODE_XMLSIG_KEY_NAME == STD_ON)) */
      }
      else if(1 == exiEventCode) /* SE(KeyValue) */
      {
        #if (defined(EXI_DECODE_XMLSIG_KEY_VALUE) && (EXI_DECODE_XMLSIG_KEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_KeyValue(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.KeyValue));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->ChoiceElement->KeyValueFlag = 1;
        #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
        #endif /* (defined(EXI_DECODE_XMLSIG_KEY_VALUE) && (EXI_DECODE_XMLSIG_KEY_VALUE == STD_ON)) */
      }
      else if(2 == exiEventCode) /* SE(RetrievalMethod) */
      {
        #if (defined(EXI_DECODE_XMLSIG_RETRIEVAL_METHOD) && (EXI_DECODE_XMLSIG_RETRIEVAL_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_XMLSIG_RetrievalMethod(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.RetrievalMethod));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->ChoiceElement->RetrievalMethodFlag = 1;
        #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
        #endif /* (defined(EXI_DECODE_XMLSIG_RETRIEVAL_METHOD) && (EXI_DECODE_XMLSIG_RETRIEVAL_METHOD == STD_ON)) */
      }
      else if(3 == exiEventCode) /* SE(X509Data) */
      {
        #if (defined(EXI_DECODE_XMLSIG_X509DATA) && (EXI_DECODE_XMLSIG_X509DATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_XMLSIG_X509Data(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.X509Data));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->ChoiceElement->X509DataFlag = 1;
        #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
        #endif /* (defined(EXI_DECODE_XMLSIG_X509DATA) && (EXI_DECODE_XMLSIG_X509DATA == STD_ON)) */
      }
      else if(4 == exiEventCode) /* SE(PGPData) */
      {
        #if (defined(EXI_DECODE_XMLSIG_PGPDATA) && (EXI_DECODE_XMLSIG_PGPDATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_PGPData(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.PGPData));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->ChoiceElement->PGPDataFlag = 1;
        #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
        #endif /* (defined(EXI_DECODE_XMLSIG_PGPDATA) && (EXI_DECODE_XMLSIG_PGPDATA == STD_ON)) */
      }
      else if(5 == exiEventCode) /* SE(SPKIData) */
      {
        #if (defined(EXI_DECODE_XMLSIG_SPKIDATA) && (EXI_DECODE_XMLSIG_SPKIDATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_XMLSIG_SPKIData(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.SPKIData));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->ChoiceElement->SPKIDataFlag = 1;
        #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
        #endif /* (defined(EXI_DECODE_XMLSIG_SPKIDATA) && (EXI_DECODE_XMLSIG_SPKIDATA == STD_ON)) */
      }
      else if(6 == exiEventCode) /* SE(MgmtData) */
      {
        #if (defined(EXI_DECODE_XMLSIG_MGMT_DATA) && (EXI_DECODE_XMLSIG_MGMT_DATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_MgmtData(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.MgmtData));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->ChoiceElement->MgmtDataFlag = 1;
        #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
        #endif /* (defined(EXI_DECODE_XMLSIG_MGMT_DATA) && (EXI_DECODE_XMLSIG_MGMT_DATA == STD_ON)) */
      }
      else if(7 == exiEventCode) /* SE(GenericElement) */
      {
        #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.GenericElement));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->ChoiceElement->GenericElementFlag = 1;
        #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
        #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    structPtr->ChoiceElement->NextChoiceElementPtr = (Exi_XMLSIG_KeyInfoChoiceType*)NULL_PTR;
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_XMLSIG_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastChoiceElement = structPtr->ChoiceElement;
    #if ((EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE - 1) > 0)
    /* #290 Decode subsequent occurences of ChoiceElement */
    for(i=0; i<(EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 4);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 9, 4, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(8 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      /* Start of Choice Element */
      /* #300 Allocate buffer for this element */
      lastChoiceElement->NextChoiceElementPtr = (Exi_XMLSIG_KeyInfoChoiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_KeyInfoChoiceType), TRUE);
      if (NULL_PTR == (void*)lastChoiceElement->NextChoiceElementPtr)
      {
        return;
      }
      if(0 == exiEventCode) /* SE(KeyName) */
      {
      #if (defined(EXI_DECODE_XMLSIG_KEY_NAME) && EXI_DECODE_XMLSIG_KEY_NAME == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_KeyName(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.KeyName));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->KeyNameFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_KEY_NAME) && EXI_DECODE_XMLSIG_KEY_NAME == STD_ON) */
      }
      else if(1 == exiEventCode) /* SE(KeyValue) */
      {
      #if (defined(EXI_DECODE_XMLSIG_KEY_VALUE) && EXI_DECODE_XMLSIG_KEY_VALUE == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_KeyValue(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.KeyValue));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->KeyValueFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_KEY_VALUE) && EXI_DECODE_XMLSIG_KEY_VALUE == STD_ON) */
      }
      else if(2 == exiEventCode) /* SE(RetrievalMethod) */
      {
      #if (defined(EXI_DECODE_XMLSIG_RETRIEVAL_METHOD) && EXI_DECODE_XMLSIG_RETRIEVAL_METHOD == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_RetrievalMethod(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.RetrievalMethod));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->RetrievalMethodFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_RETRIEVAL_METHOD) && EXI_DECODE_XMLSIG_RETRIEVAL_METHOD == STD_ON) */
      }
      else if(3 == exiEventCode) /* SE(X509Data) */
      {
      #if (defined(EXI_DECODE_XMLSIG_X509DATA) && EXI_DECODE_XMLSIG_X509DATA == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_X509Data(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.X509Data));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->X509DataFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_X509DATA) && EXI_DECODE_XMLSIG_X509DATA == STD_ON) */
      }
      else if(4 == exiEventCode) /* SE(PGPData) */
      {
      #if (defined(EXI_DECODE_XMLSIG_PGPDATA) && EXI_DECODE_XMLSIG_PGPDATA == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_PGPData(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.PGPData));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->PGPDataFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_PGPDATA) && EXI_DECODE_XMLSIG_PGPDATA == STD_ON) */
      }
      else if(5 == exiEventCode) /* SE(SPKIData) */
      {
      #if (defined(EXI_DECODE_XMLSIG_SPKIDATA) && EXI_DECODE_XMLSIG_SPKIDATA == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_SPKIData(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.SPKIData));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->SPKIDataFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_SPKIDATA) && EXI_DECODE_XMLSIG_SPKIDATA == STD_ON) */
      }
      else if(6 == exiEventCode) /* SE(MgmtData) */
      {
      #if (defined(EXI_DECODE_XMLSIG_MGMT_DATA) && EXI_DECODE_XMLSIG_MGMT_DATA == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_MgmtData(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.MgmtData));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->MgmtDataFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_MGMT_DATA) && EXI_DECODE_XMLSIG_MGMT_DATA == STD_ON) */
      }
      else if(7 == exiEventCode) /* SE(GenericElement) */
      {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.GenericElement));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->GenericElementFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON) */
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(ChoiceElement) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(ChoiceElement) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE - 1) > 0) */
    lastChoiceElement->NextChoiceElementPtr = (Exi_XMLSIG_KeyInfoChoiceType*)NULL_PTR;
    exiEventCode = 0;
    #if ((EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE - 1) > 0)
    if((EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE - 1) > 0) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 4);
      if(8 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #310 If KeyInfo was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      #if ((EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE - 1) > 0)
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* last element unbounded, EE(KeyInfo) already found, EXI stream end reached */
      }
      else
      {
        /* unexpected state */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
      #endif /* #if ((EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE - 1) > 0) */
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_KEY_INFO, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_KEY_INFO) && (EXI_DECODE_XMLSIG_KEY_INFO == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_KeyName
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_KEY_NAME) && (EXI_DECODE_XMLSIG_KEY_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_KeyName( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_KeyNameType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_KeyNameType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_KeyNameType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_KeyNameType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode KeyName as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_KEY_NAME, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_KEY_NAME) && (EXI_DECODE_XMLSIG_KEY_NAME == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_KeyValue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_KEY_VALUE) && (EXI_DECODE_XMLSIG_KEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_KeyValue( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_KeyValueType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_KeyValueType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_KeyValueType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_KeyValueType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_KEY_VALUE_TYPE);
    /* Start of Choice Element */
    /* #40 Allocate buffer for this element */
    structPtr->ChoiceElement = (Exi_XMLSIG_KeyValueChoiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_KeyValueChoiceType), TRUE);
    if (NULL_PTR == (void*)structPtr->ChoiceElement)
    {
      return;
    }
    /* #50 Decode KeyValueChoice element */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(DSAKeyValue) */
    {
      #if (defined(EXI_DECODE_XMLSIG_DSAKEY_VALUE) && (EXI_DECODE_XMLSIG_DSAKEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_DSAKeyValue(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.DSAKeyValue));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->DSAKeyValueFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_VALUE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_DSAKEY_VALUE) && (EXI_DECODE_XMLSIG_DSAKEY_VALUE == STD_ON)) */
    }
    else if(1 == exiEventCode) /* SE(RSAKeyValue) */
    {
      #if (defined(EXI_DECODE_XMLSIG_RSAKEY_VALUE) && (EXI_DECODE_XMLSIG_RSAKEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_RSAKeyValue(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.RSAKeyValue));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->RSAKeyValueFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_VALUE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_RSAKEY_VALUE) && (EXI_DECODE_XMLSIG_RSAKEY_VALUE == STD_ON)) */
    }
    else if(2 == exiEventCode) /* SE(GenericElement) */
    {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.GenericElement));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->GenericElementFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_KEY_VALUE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(ChoiceElement) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 If KeyValue was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode)/* EE(KeyValue) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_KEY_VALUE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_KEY_VALUE) && (EXI_DECODE_XMLSIG_KEY_VALUE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_Manifest
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_MANIFEST) && (EXI_DECODE_XMLSIG_MANIFEST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_Manifest( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_ManifestType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_ManifestType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_ReferenceType) lastReference;
  #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_ManifestType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_ManifestType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_MANIFEST_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, TRUE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_MANIFEST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element Reference with missing elements in front */
    if(0 == structPtr->IdFlag)
    {
      /* SE(Reference) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_Reference(DecWsPtr, &(structPtr->Reference));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_MANIFEST, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) */
    }
    /* #60 Decode element Reference without missing elements in front */
    else
    {
      /* #70 Decode multi occurence element Reference, decode first occurence */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* SE(Reference) */
      {
      #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_Reference(DecWsPtr, &(structPtr->Reference));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_MANIFEST, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    structPtr->Reference->NextReferencePtr = (Exi_XMLSIG_ReferenceType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(Reference) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastReference = structPtr->Reference;
    #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0)
    /* #80 Decode subsequent occurences of Reference */
    for(i=0; i<(EXI_MAXOCCURS_XMLSIG_REFERENCE - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode)/* SE(Reference) */
      {
        Exi_Decode_XMLSIG_Reference(DecWsPtr, &(lastReference->NextReferencePtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastReference = lastReference->NextReferencePtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(Reference) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0) */
    #endif /* (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) */
    lastReference->NextReferencePtr = (Exi_XMLSIG_ReferenceType*)NULL_PTR;
    exiEventCode = 0;
    #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0)
    if((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0) */
    #endif /* #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #90 If Manifest was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0)
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* last element unbounded, EE(Manifest) already found, EXI stream end reached */
      }
      else
      {
        /* unexpected state */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
      #endif /* #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) */
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_MANIFEST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_MANIFEST) && (EXI_DECODE_XMLSIG_MANIFEST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_MgmtData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_MGMT_DATA) && (EXI_DECODE_XMLSIG_MGMT_DATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_MgmtData( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_MgmtDataType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_MgmtDataType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_MgmtDataType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_MgmtDataType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode MgmtData as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_MGMT_DATA, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_MGMT_DATA) && (EXI_DECODE_XMLSIG_MGMT_DATA == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_Object
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_Object( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_ObjectType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_ObjectType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_ObjectType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_ObjectType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_OBJECT_TYPE);
    /* #40 Decode attribute Encoding */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 4, 3, FALSE, TRUE, TRUE, 3, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* AT(Encoding) */
    {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ENCODING) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ENCODING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeEncoding(DecWsPtr, &(structPtr->Encoding));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EncodingFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_OBJECT, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ENCODING) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ENCODING == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(Id) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->IdFlag = 1;
    }
    else if(exiEventCode == 2) /* SE(MimeType) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->MimeTypeFlag = 1;
    }
    else if(exiEventCode == 3)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode optional element Id with missing elements in front */
    if((0 == structPtr->EncodingFlag) && (1 == structPtr->IdFlag))
    {
      /* SE(Id) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_OBJECT, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */
    }
    /* #60 Decode optional element Id without missing elements in front */
    else if(1 == structPtr->EncodingFlag)
    {
      /* #70 Decode attribute Id */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 3, FALSE, TRUE, TRUE, 3, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* AT(Id) */
      {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_AttributeId(DecWsPtr, &(structPtr->Id));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->IdFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_OBJECT, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(MimeType) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->MimeTypeFlag = 1;
      }
      else if(exiEventCode == 2)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    /* #80 Decode optional element MimeType with missing elements in front */
    if((0 == structPtr->IdFlag) && (1 == structPtr->MimeTypeFlag))
    {
      /* SE(MimeType) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_MIME) && (EXI_DECODE_XMLSIG_ATTRIBUTE_MIME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeMime(DecWsPtr, &(structPtr->MimeType));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_OBJECT, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_MIME) && (EXI_DECODE_XMLSIG_ATTRIBUTE_MIME == STD_ON)) */
    }
    /* #90 Decode optional element MimeType without missing elements in front */
    else if(1 == structPtr->IdFlag)
    {
      /* #100 Decode attribute MimeType */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, TRUE, TRUE, 3, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* AT(MimeType) */
      {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_MIME) && (EXI_DECODE_XMLSIG_ATTRIBUTE_MIME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_AttributeMime(DecWsPtr, &(structPtr->MimeType));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->MimeTypeFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_OBJECT, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_MIME) && (EXI_DECODE_XMLSIG_ATTRIBUTE_MIME == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    /* #110 Decode element GenericElement with missing elements in front */
    if(0 == structPtr->MimeTypeFlag)
    {
      /* SE(GenericElement) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->GenericElement));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_OBJECT, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
    }
    /* #120 Decode element GenericElement without missing elements in front */
    else
    {
      /* #130 Decode element GenericElement */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 3, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* SE(GenericElement) */
      {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->GenericElement));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_OBJECT, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(GenericElement) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #140 If Object was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode)/* EE(Object) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_OBJECT, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_PGPDataChoiceSeq0
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ0) && (EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ0 == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_PGPDataChoiceSeq0( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_PGPDataChoiceSeq0Type*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_PGPDataChoiceSeq0Type, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_GenericElementType) lastGenericElement;
  #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_PGPDataChoiceSeq0Type*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_PGPDataChoiceSeq0Type), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element PGPKeyID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(PGPKeyID) */
    {
      #if (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_base64Binary(DecWsPtr, &(structPtr->PGPKeyID), EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ0, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(PGPKeyID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element PGPKeyPacket */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(PGPKeyPacket) */
    {
      #if (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_base64Binary(DecWsPtr, &(structPtr->PGPKeyPacket), EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->PGPKeyPacketFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ0, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(GenericElement) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->GenericElementFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->PGPKeyPacketFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* check exiEventCode equals EE(PGPKeyPacket) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #50 Decode optional element GenericElement with missing elements in front */
    if((0 == structPtr->PGPKeyPacketFlag) && (1 == structPtr->GenericElementFlag))
    {
      /* SE(GenericElement) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->GenericElement));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ0, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
    }
    /* #60 Decode optional element GenericElement without missing elements in front */
    else if(1 == structPtr->PGPKeyPacketFlag)
    {
      /* #70 Decode multi occurence element GenericElement, decode first occurence */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* SE(GenericElement) */
      {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->GenericElement));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->GenericElementFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ0, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->GenericElementFlag)
    {
      structPtr->GenericElement->NextGenericElementPtr = (Exi_XMLSIG_GenericElementType*)NULL_PTR;
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* check exiEventCode equals EE(GenericElement) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
      {
        /* an error occured in a previous step -> abort to avoid not required loop */
        return;
      }
      lastGenericElement = structPtr->GenericElement;
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
      /* #80 Decode subsequent occurences of GenericElement */
      for(i=0; i<(EXI_MAXOCCURS_GENERICELEMENT - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
      { /* PRQA S 3201 */ /* MD_MSR_14.1 */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
        if(0 == exiEventCode)/* SE(GenericElement) */
        {
          Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(lastGenericElement->NextGenericElementPtr));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          lastGenericElement = lastGenericElement->NextGenericElementPtr;
        }
        else if(1 == exiEventCode)/* reached next Tag */
        {
          DecWsPtr->DecWs.EERequired = FALSE;
          break;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* check exiEventCode equals EE(GenericElement) */
        if(0 != exiEventCode)
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      lastGenericElement->NextGenericElementPtr = (Exi_XMLSIG_GenericElementType*)NULL_PTR;
      exiEventCode = 0;
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
      if((EXI_MAXOCCURS_GENERICELEMENT - 1) == i)
      #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        if(1 == exiEventCode)
        {
          DecWsPtr->DecWs.EERequired = FALSE;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ0, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ0) && (EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ0 == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_PGPDataChoiceSeq1
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ1) && (EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ1 == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_PGPDataChoiceSeq1( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_PGPDataChoiceSeq1Type*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_PGPDataChoiceSeq1Type, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_GenericElementType) lastGenericElement;
  #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_PGPDataChoiceSeq1Type*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_PGPDataChoiceSeq1Type), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element PGPKeyPacket */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(PGPKeyPacket) */
    {
      #if (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_base64Binary(DecWsPtr, &(structPtr->PGPKeyPacket), EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ1, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(PGPKeyPacket) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode multi occurence element GenericElement, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(GenericElement) */
    {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->GenericElement));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->GenericElementFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ1, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->GenericElementFlag)
    {
      structPtr->GenericElement->NextGenericElementPtr = (Exi_XMLSIG_GenericElementType*)NULL_PTR;
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* check exiEventCode equals EE(GenericElement) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
      {
        /* an error occured in a previous step -> abort to avoid not required loop */
        return;
      }
      lastGenericElement = structPtr->GenericElement;
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
      /* #50 Decode subsequent occurences of GenericElement */
      for(i=0; i<(EXI_MAXOCCURS_GENERICELEMENT - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
      { /* PRQA S 3201 */ /* MD_MSR_14.1 */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
        if(0 == exiEventCode)/* SE(GenericElement) */
        {
          Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(lastGenericElement->NextGenericElementPtr));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          lastGenericElement = lastGenericElement->NextGenericElementPtr;
        }
        else if(1 == exiEventCode)/* reached next Tag */
        {
          DecWsPtr->DecWs.EERequired = FALSE;
          break;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* check exiEventCode equals EE(GenericElement) */
        if(0 != exiEventCode)
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      lastGenericElement->NextGenericElementPtr = (Exi_XMLSIG_GenericElementType*)NULL_PTR;
      exiEventCode = 0;
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
      if((EXI_MAXOCCURS_GENERICELEMENT - 1) == i)
      #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        if(1 == exiEventCode)
        {
          DecWsPtr->DecWs.EERequired = FALSE;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ1, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ1) && (EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ1 == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_PGPData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_PGPDATA) && (EXI_DECODE_XMLSIG_PGPDATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_PGPData( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_PGPDataType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_PGPDataType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_PGPDataType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_PGPDataType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_PGPDATA_TYPE);
    /* Start of Choice Element */
    /* #40 Allocate buffer for this element */
    structPtr->ChoiceElement = (Exi_XMLSIG_PGPDataChoiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_PGPDataChoiceType), TRUE);
    if (NULL_PTR == (void*)structPtr->ChoiceElement)
    {
      return;
    }
    /* #50 Decode PGPDataChoice element */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(ChoiceSequence0) */
    {
      #if (defined(EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ0) && (EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ0 == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_XMLSIG_PGPDataChoiceSeq0(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.ChoiceSequence0));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->ChoiceSequence0Flag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_PGPDATA, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ0) && (EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ0 == STD_ON)) */
    }
    else if(1 == exiEventCode) /* SE(ChoiceSequence1) */
    {
      #if (defined(EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ1) && (EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ1 == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_XMLSIG_PGPDataChoiceSeq1(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.ChoiceSequence1));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->ChoiceSequence1Flag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_PGPDATA, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ1) && (EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ1 == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_XMLSIG_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #60 If PGPData was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode)/* EE(PGPData) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_PGPDATA, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_PGPDATA) && (EXI_DECODE_XMLSIG_PGPDATA == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_RSAKeyValue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_RSAKEY_VALUE) && (EXI_DECODE_XMLSIG_RSAKEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_RSAKeyValue( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_RSAKeyValueType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_RSAKeyValueType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_RSAKeyValueType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_RSAKeyValueType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_RSAKEY_VALUE_TYPE);
    /* #40 Decode element Modulus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(Modulus) */
    {
      #if (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_CryptoBinary(DecWsPtr, &(structPtr->Modulus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_RSAKEY_VALUE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(Modulus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element Exponent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(Exponent) */
    {
      #if (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_CryptoBinary(DecWsPtr, &(structPtr->Exponent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_RSAKEY_VALUE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_CRYPTO_BINARY) && (EXI_DECODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(Exponent) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 If RSAKeyValue was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode)/* EE(RSAKeyValue) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_RSAKEY_VALUE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_RSAKEY_VALUE) && (EXI_DECODE_XMLSIG_RSAKEY_VALUE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_Reference
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_Reference( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_ReferenceType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_ReferenceType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_ReferenceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_ReferenceType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_REFERENCE_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 5, 3, FALSE, TRUE, TRUE, 3, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(Type) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->TypeFlag = 1;
    }
    else if(exiEventCode == 2) /* SE(URI) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->URIFlag = 1;
    }
    else if(exiEventCode == 3) /* SE(Transforms) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->TransformsFlag = 1;
    }
    else if(exiEventCode == 4)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode optional element Type with missing elements in front */
    if((0 == structPtr->IdFlag) && (1 == structPtr->TypeFlag))
    {
      /* SE(Type) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE) && (EXI_DECODE_XMLSIG_ATTRIBUTE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_Attribute(DecWsPtr, &(structPtr->Type));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE) && (EXI_DECODE_XMLSIG_ATTRIBUTE == STD_ON)) */
    }
    /* #60 Decode optional element Type without missing elements in front */
    else if(1 == structPtr->IdFlag)
    {
      /* #70 Decode attribute Type */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 4, 3, FALSE, TRUE, TRUE, 3, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* AT(Type) */
      {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE) && (EXI_DECODE_XMLSIG_ATTRIBUTE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_Attribute(DecWsPtr, &(structPtr->Type));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->TypeFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE) && (EXI_DECODE_XMLSIG_ATTRIBUTE == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(URI) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->URIFlag = 1;
      }
      else if(exiEventCode == 2) /* SE(Transforms) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->TransformsFlag = 1;
      }
      else if(exiEventCode == 3)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    /* #80 Decode optional element URI with missing elements in front */
    if((0 == structPtr->TypeFlag) && (1 == structPtr->URIFlag))
    {
      /* SE(URI) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_URI) && (EXI_DECODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeURI(DecWsPtr, &(structPtr->URI));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_URI) && (EXI_DECODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) */
    }
    /* #90 Decode optional element URI without missing elements in front */
    else if(1 == structPtr->TypeFlag)
    {
      /* #100 Decode attribute URI */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, FALSE, TRUE, TRUE, 3, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* AT(URI) */
      {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_URI) && (EXI_DECODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_AttributeURI(DecWsPtr, &(structPtr->URI));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->URIFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_URI) && (EXI_DECODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(Transforms) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->TransformsFlag = 1;
      }
      else if(exiEventCode == 2)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    /* #110 Decode optional element Transforms with missing elements in front */
    if((0 == structPtr->URIFlag) && (1 == structPtr->TransformsFlag))
    {
      /* SE(Transforms) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_TRANSFORMS) && (EXI_DECODE_XMLSIG_TRANSFORMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_XMLSIG_Transforms(DecWsPtr, &(structPtr->Transforms));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_TRANSFORMS) && (EXI_DECODE_XMLSIG_TRANSFORMS == STD_ON)) */
    }
    /* #120 Decode optional element Transforms without missing elements in front */
    else if(1 == structPtr->URIFlag)
    {
      /* #130 Decode element Transforms */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, TRUE, 3, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* SE(Transforms) */
      {
      #if (defined(EXI_DECODE_XMLSIG_TRANSFORMS) && (EXI_DECODE_XMLSIG_TRANSFORMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_XMLSIG_Transforms(DecWsPtr, &(structPtr->Transforms));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->TransformsFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_TRANSFORMS) && (EXI_DECODE_XMLSIG_TRANSFORMS == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->TransformsFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_XMLSIG_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    /* #140 Decode element DigestMethod with missing elements in front */
    if(0 == structPtr->TransformsFlag)
    {
      /* SE(DigestMethod) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_DIGEST_METHOD) && (EXI_DECODE_XMLSIG_DIGEST_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_XMLSIG_DigestMethod(DecWsPtr, &(structPtr->DigestMethod));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_DIGEST_METHOD) && (EXI_DECODE_XMLSIG_DIGEST_METHOD == STD_ON)) */
    }
    /* #150 Decode element DigestMethod without missing elements in front */
    else
    {
      /* #160 Decode element DigestMethod */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 3, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* SE(DigestMethod) */
      {
      #if (defined(EXI_DECODE_XMLSIG_DIGEST_METHOD) && (EXI_DECODE_XMLSIG_DIGEST_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_XMLSIG_DigestMethod(DecWsPtr, &(structPtr->DigestMethod));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_DIGEST_METHOD) && (EXI_DECODE_XMLSIG_DIGEST_METHOD == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_XMLSIG_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #170 Decode element DigestValue */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 3, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(DigestValue) */
    {
      #if (defined(EXI_DECODE_XMLSIG_DIGEST_VALUE) && (EXI_DECODE_XMLSIG_DIGEST_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_DigestValue(DecWsPtr, &(structPtr->DigestValue));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_DIGEST_VALUE) && (EXI_DECODE_XMLSIG_DIGEST_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(DigestValue) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #180 If Reference was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode)/* EE(Reference) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_REFERENCE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_RetrievalMethod
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_RETRIEVAL_METHOD) && (EXI_DECODE_XMLSIG_RETRIEVAL_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_RetrievalMethod( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_RetrievalMethodType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_RetrievalMethodType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_RetrievalMethodType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_RetrievalMethodType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_RETRIEVAL_METHOD_TYPE);
    /* #40 Decode attribute Type */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 4, 3, FALSE, TRUE, TRUE, 2, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* AT(Type) */
    {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE) && (EXI_DECODE_XMLSIG_ATTRIBUTE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_Attribute(DecWsPtr, &(structPtr->Type));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->TypeFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_RETRIEVAL_METHOD, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE) && (EXI_DECODE_XMLSIG_ATTRIBUTE == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(URI) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->URIFlag = 1;
    }
    else if(exiEventCode == 2) /* SE(Transforms) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->TransformsFlag = 1;
    }
    else if(exiEventCode == 3)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode optional element URI with missing elements in front */
    if((0 == structPtr->TypeFlag) && (1 == structPtr->URIFlag))
    {
      /* SE(URI) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_URI) && (EXI_DECODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeURI(DecWsPtr, &(structPtr->URI));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_RETRIEVAL_METHOD, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_URI) && (EXI_DECODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) */
    }
    /* #60 Decode optional element URI without missing elements in front */
    else if(1 == structPtr->TypeFlag)
    {
      /* #70 Decode attribute URI */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, FALSE, TRUE, TRUE, 2, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* AT(URI) */
      {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_URI) && (EXI_DECODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_AttributeURI(DecWsPtr, &(structPtr->URI));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->URIFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_RETRIEVAL_METHOD, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_URI) && (EXI_DECODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(Transforms) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->TransformsFlag = 1;
      }
      else if(exiEventCode == 2)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    /* #80 Decode optional element Transforms with missing elements in front */
    if((0 == structPtr->URIFlag) && (1 == structPtr->TransformsFlag))
    {
      /* SE(Transforms) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_TRANSFORMS) && (EXI_DECODE_XMLSIG_TRANSFORMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_XMLSIG_Transforms(DecWsPtr, &(structPtr->Transforms));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_RETRIEVAL_METHOD, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_TRANSFORMS) && (EXI_DECODE_XMLSIG_TRANSFORMS == STD_ON)) */
    }
    /* #90 Decode optional element Transforms without missing elements in front */
    else if(1 == structPtr->URIFlag)
    {
      /* #100 Decode element Transforms */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, TRUE, 2, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* SE(Transforms) */
      {
      #if (defined(EXI_DECODE_XMLSIG_TRANSFORMS) && (EXI_DECODE_XMLSIG_TRANSFORMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_XMLSIG_Transforms(DecWsPtr, &(structPtr->Transforms));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->TransformsFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_RETRIEVAL_METHOD, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_TRANSFORMS) && (EXI_DECODE_XMLSIG_TRANSFORMS == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->TransformsFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_XMLSIG_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #110 If RetrievalMethod was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->TransformsFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
        if(0 == exiEventCode)/* EE(RetrievalMethod) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(RetrievalMethod) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_RETRIEVAL_METHOD, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_RETRIEVAL_METHOD) && (EXI_DECODE_XMLSIG_RETRIEVAL_METHOD == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_SPKIData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_SPKIDATA) && (EXI_DECODE_XMLSIG_SPKIDATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_SPKIData( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_SPKIDataType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_SPKIDataType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_SPKIDataType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_SPKIDataType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_SPKIDATA_TYPE);
    /* #40 Decode element SPKISexp */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(SPKISexp) */
    {
      #if (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_base64Binary(DecWsPtr, &(structPtr->SPKISexp), EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SPKIDATA, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(SPKISexp) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element GenericElement */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(GenericElement) */
    {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->GenericElement));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->GenericElementFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SPKIDATA, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->GenericElementFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* check exiEventCode equals EE(GenericElement) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #60 If SPKIData was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->GenericElementFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
        if(0 == exiEventCode)/* EE(SPKIData) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(SPKIData) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_SPKIDATA, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_SPKIDATA) && (EXI_DECODE_XMLSIG_SPKIDATA == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_SignatureMethod
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_SIGNATURE_METHOD) && (EXI_DECODE_XMLSIG_SIGNATURE_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_SignatureMethod( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_SignatureMethodType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_SignatureMethodType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_GenericElementType) lastGenericElement;
  #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_SignatureMethodType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_SignatureMethodType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_SIGNATURE_METHOD_TYPE);
    /* #40 Decode attribute Algorithm */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* AT(Algorithm) */
    {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeAlgorithm(DecWsPtr, &(structPtr->Algorithm));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_METHOD, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element HMACOutputLength */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 3, TRUE, TRUE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(HMACOutputLength) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->HMACOutputLengthFlag = 1;
        /* #60 Allocate buffer for this element */
        structPtr->HMACOutputLength = (Exi_BigIntType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_BigIntType), TRUE);
        if (NULL_PTR == (void*)structPtr->HMACOutputLength)
        {
          return;
        }
        Exi_VBSDecodeBigInt(&DecWsPtr->DecWs, structPtr->HMACOutputLength);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1) /* SE(GenericElement) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->GenericElementFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->HMACOutputLengthFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* check exiEventCode equals EE(HMACOutputLength) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #70 Decode optional element GenericElement with missing elements in front */
    if((0 == structPtr->HMACOutputLengthFlag) && (1 == structPtr->GenericElementFlag))
    {
      /* SE(GenericElement) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->GenericElement));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_METHOD, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
    }
    /* #80 Decode optional element GenericElement without missing elements in front */
    else if(1 == structPtr->HMACOutputLengthFlag)
    {
      /* #90 Decode multi occurence element GenericElement, decode first occurence */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* SE(GenericElement) */
      {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->GenericElement));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->GenericElementFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_METHOD, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->GenericElementFlag)
    {
      structPtr->GenericElement->NextGenericElementPtr = (Exi_XMLSIG_GenericElementType*)NULL_PTR;
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* check exiEventCode equals EE(GenericElement) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
      {
        /* an error occured in a previous step -> abort to avoid not required loop */
        return;
      }
      lastGenericElement = structPtr->GenericElement;
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
      /* #100 Decode subsequent occurences of GenericElement */
      for(i=0; i<(EXI_MAXOCCURS_GENERICELEMENT - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
      { /* PRQA S 3201 */ /* MD_MSR_14.1 */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
        if(0 == exiEventCode)/* SE(GenericElement) */
        {
          Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(lastGenericElement->NextGenericElementPtr));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          lastGenericElement = lastGenericElement->NextGenericElementPtr;
        }
        else if(1 == exiEventCode)/* reached next Tag */
        {
          DecWsPtr->DecWs.EERequired = FALSE;
          break;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* check exiEventCode equals EE(GenericElement) */
        if(0 != exiEventCode)
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      lastGenericElement->NextGenericElementPtr = (Exi_XMLSIG_GenericElementType*)NULL_PTR;
      exiEventCode = 0;
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
      if((EXI_MAXOCCURS_GENERICELEMENT - 1) == i)
      #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        if(1 == exiEventCode)
        {
          DecWsPtr->DecWs.EERequired = FALSE;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #110 If SignatureMethod was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->GenericElementFlag)
      {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0)
        if(FALSE == DecWsPtr->DecWs.EERequired)
        {
          /* last element unbounded, EE(SignatureMethod) already found, EXI stream end reached */
        }
        else
        {
          /* unexpected state */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        }
      #endif /* #if ((EXI_MAXOCCURS_GENERICELEMENT - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      }
      else
      {
        /* EE(SignatureMethod) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_METHOD, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE_METHOD) && (EXI_DECODE_XMLSIG_SIGNATURE_METHOD == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_SignatureProperties
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTIES) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTIES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_SignatureProperties( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_SignaturePropertiesType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_SignaturePropertiesType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_SignaturePropertyType) lastSignatureProperty;
  #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_SignaturePropertiesType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_SignaturePropertiesType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_SIGNATURE_PROPERTIES_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, TRUE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_PROPERTIES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element SignatureProperty with missing elements in front */
    if(0 == structPtr->IdFlag)
    {
      /* SE(SignatureProperty) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_XMLSIG_SignatureProperty(DecWsPtr, &(structPtr->SignatureProperty));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_PROPERTIES, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) */
    }
    /* #60 Decode element SignatureProperty without missing elements in front */
    else
    {
      /* #70 Decode multi occurence element SignatureProperty, decode first occurence */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* SE(SignatureProperty) */
      {
      #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_XMLSIG_SignatureProperty(DecWsPtr, &(structPtr->SignatureProperty));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_PROPERTIES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    structPtr->SignatureProperty->NextSignaturePropertyPtr = (Exi_XMLSIG_SignaturePropertyType*)NULL_PTR;
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_XMLSIG_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastSignatureProperty = structPtr->SignatureProperty;
    #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY - 1) > 0)
    /* #80 Decode subsequent occurences of SignatureProperty */
    for(i=0; i<(EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode)/* SE(SignatureProperty) */
      {
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_XMLSIG_SignatureProperty(DecWsPtr, &(lastSignatureProperty->NextSignaturePropertyPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastSignatureProperty = lastSignatureProperty->NextSignaturePropertyPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(SignatureProperty) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(SignatureProperty) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY - 1) > 0) */
    #endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) */
    lastSignatureProperty->NextSignaturePropertyPtr = (Exi_XMLSIG_SignaturePropertyType*)NULL_PTR;
    exiEventCode = 0;
    #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY - 1) > 0)
    if((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY - 1) > 0) */
    #endif /* #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #90 If SignatureProperties was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY - 1) > 0)
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* last element unbounded, EE(SignatureProperties) already found, EXI stream end reached */
      }
      else
      {
        /* unexpected state */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
      #endif /* #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) */
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_PROPERTIES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTIES) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTIES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_SignatureProperty
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_SignatureProperty( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_SignaturePropertyType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_SignaturePropertyType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_SignaturePropertyChoiceType) lastChoiceElement;
  #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE - 1) > 0) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_SignaturePropertyType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_SignaturePropertyType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_SIGNATURE_PROPERTY_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, TRUE, TRUE, 2, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_PROPERTY, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element Target with missing elements in front */
    if(0 == structPtr->IdFlag)
    {
      /* SE(Target) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_TARGET) && (EXI_DECODE_XMLSIG_ATTRIBUTE_TARGET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeTarget(DecWsPtr, &(structPtr->Target));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_PROPERTY, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_TARGET) && (EXI_DECODE_XMLSIG_ATTRIBUTE_TARGET == STD_ON)) */
    }
    /* #60 Decode element Target without missing elements in front */
    else
    {
      /* #70 Decode attribute Target */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, TRUE, 2, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* AT(Target) */
      {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_TARGET) && (EXI_DECODE_XMLSIG_ATTRIBUTE_TARGET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_AttributeTarget(DecWsPtr, &(structPtr->Target));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_PROPERTY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_TARGET) && (EXI_DECODE_XMLSIG_ATTRIBUTE_TARGET == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* Start of Choice Element */
    /* #80 Allocate buffer for this element */
    structPtr->ChoiceElement = (Exi_XMLSIG_SignaturePropertyChoiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_SignaturePropertyChoiceType), TRUE);
    if (NULL_PTR == (void*)structPtr->ChoiceElement)
    {
      return;
    }
    /* #90 Decode SignaturePropertyChoice element */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 2, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(GenericElement) */
    {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.GenericElement));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->GenericElementFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_PROPERTY, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->ChoiceElement->NextChoiceElementPtr = (Exi_XMLSIG_SignaturePropertyChoiceType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(ChoiceElement) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastChoiceElement = structPtr->ChoiceElement;
    #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE - 1) > 0)
    /* #100 Decode subsequent occurences of ChoiceElement */
    for(i=0; i<(EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      /* Start of Choice Element */
      /* #110 Allocate buffer for this element */
      lastChoiceElement->NextChoiceElementPtr = (Exi_XMLSIG_SignaturePropertyChoiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_SignaturePropertyChoiceType), TRUE);
      if (NULL_PTR == (void*)lastChoiceElement->NextChoiceElementPtr)
      {
        return;
      }
      if(0 == exiEventCode) /* SE(GenericElement) */
      {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.GenericElement));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->GenericElementFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_PROPERTY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON) */
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(ChoiceElement) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(ChoiceElement) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE - 1) > 0) */
    lastChoiceElement->NextChoiceElementPtr = (Exi_XMLSIG_SignaturePropertyChoiceType*)NULL_PTR;
    exiEventCode = 0;
    #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE - 1) > 0)
    if((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE - 1) > 0) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #120 If SignatureProperty was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE - 1) > 0)
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* last element unbounded, EE(SignatureProperty) already found, EXI stream end reached */
      }
      else
      {
        /* unexpected state */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
      #endif /* #if ((EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE - 1) > 0) */
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_PROPERTY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_Signature
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_SIGNATURE) && (EXI_DECODE_XMLSIG_SIGNATURE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_Signature( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_SignatureType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_SignatureType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_ObjectType) lastObject;
  #if (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_XMLSIG_OBJECT - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_XMLSIG_OBJECT - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_SignatureType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_SignatureType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_SIGNATURE_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, TRUE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element SignedInfo with missing elements in front */
    if(0 == structPtr->IdFlag)
    {
      /* SE(SignedInfo) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_SIGNED_INFO) && (EXI_DECODE_XMLSIG_SIGNED_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_XMLSIG_SignedInfo(DecWsPtr, &(structPtr->SignedInfo));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_SIGNED_INFO) && (EXI_DECODE_XMLSIG_SIGNED_INFO == STD_ON)) */
    }
    /* #60 Decode element SignedInfo without missing elements in front */
    else
    {
      /* #70 Decode element SignedInfo */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* SE(SignedInfo) */
      {
      #if (defined(EXI_DECODE_XMLSIG_SIGNED_INFO) && (EXI_DECODE_XMLSIG_SIGNED_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_XMLSIG_SignedInfo(DecWsPtr, &(structPtr->SignedInfo));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_SIGNED_INFO) && (EXI_DECODE_XMLSIG_SIGNED_INFO == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_XMLSIG_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #80 Decode element SignatureValue */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(SignatureValue) */
    {
      #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_VALUE) && (EXI_DECODE_XMLSIG_SIGNATURE_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_SignatureValue(DecWsPtr, &(structPtr->SignatureValue));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE_VALUE) && (EXI_DECODE_XMLSIG_SIGNATURE_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(SignatureValue) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode element KeyInfo */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(KeyInfo) */
    {
      #if (defined(EXI_DECODE_XMLSIG_KEY_INFO) && (EXI_DECODE_XMLSIG_KEY_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_XMLSIG_KeyInfo(DecWsPtr, &(structPtr->KeyInfo));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->KeyInfoFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_KEY_INFO) && (EXI_DECODE_XMLSIG_KEY_INFO == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(Object) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->ObjectFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->KeyInfoFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_XMLSIG_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    /* #100 Decode optional element Object with missing elements in front */
    if((0 == structPtr->KeyInfoFlag) && (1 == structPtr->ObjectFlag))
    {
      /* SE(Object) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_Object(DecWsPtr, &(structPtr->Object));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) */
    }
    /* #110 Decode optional element Object without missing elements in front */
    else if(1 == structPtr->KeyInfoFlag)
    {
      /* #120 Decode multi occurence element Object, decode first occurence */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* SE(Object) */
      {
      #if (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_Object(DecWsPtr, &(structPtr->Object));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->ObjectFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNATURE, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->ObjectFlag)
    {
      structPtr->Object->NextObjectPtr = (Exi_XMLSIG_ObjectType*)NULL_PTR;
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* check exiEventCode equals EE(Object) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
      {
        /* an error occured in a previous step -> abort to avoid not required loop */
        return;
      }
      lastObject = structPtr->Object;
      #if (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_XMLSIG_OBJECT - 1) > 0)
      /* #130 Decode subsequent occurences of Object */
      for(i=0; i<(EXI_MAXOCCURS_XMLSIG_OBJECT - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
      { /* PRQA S 3201 */ /* MD_MSR_14.1 */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
        if(0 == exiEventCode)/* SE(Object) */
        {
          Exi_Decode_XMLSIG_Object(DecWsPtr, &(lastObject->NextObjectPtr));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          lastObject = lastObject->NextObjectPtr;
        }
        else if(1 == exiEventCode)/* reached next Tag */
        {
          DecWsPtr->DecWs.EERequired = FALSE;
          break;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* check exiEventCode equals EE(Object) */
        if(0 != exiEventCode)
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      #endif /* #if ((EXI_MAXOCCURS_XMLSIG_OBJECT - 1) > 0) */
      #endif /* (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) */
      lastObject->NextObjectPtr = (Exi_XMLSIG_ObjectType*)NULL_PTR;
      exiEventCode = 0;
      #if (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_XMLSIG_OBJECT - 1) > 0)
      if((EXI_MAXOCCURS_XMLSIG_OBJECT - 1) == i)
      #endif /* #if ((EXI_MAXOCCURS_XMLSIG_OBJECT - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) */
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        if(1 == exiEventCode)
        {
          DecWsPtr->DecWs.EERequired = FALSE;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #140 If Signature was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->ObjectFlag)
      {
      #if (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_XMLSIG_OBJECT - 1) > 0)
        if(FALSE == DecWsPtr->DecWs.EERequired)
        {
          /* last element unbounded, EE(Signature) already found, EXI stream end reached */
        }
        else
        {
          /* unexpected state */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        }
      #endif /* #if ((EXI_MAXOCCURS_XMLSIG_OBJECT - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) */
      }
      else
      {
        /* EE(Signature) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_SIGNATURE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE) && (EXI_DECODE_XMLSIG_SIGNATURE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_SignatureValue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_SIGNATURE_VALUE) && (EXI_DECODE_XMLSIG_SIGNATURE_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_SignatureValue( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_SignatureValueType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_SignatureValueType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_SignatureValueType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_SignatureValueType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_ATTRIBUTE_ID, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */
    }
    else if( 1 == exiEventCode)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    if(1 == structPtr->IdFlag)
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    }
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode)
    {
      /* #40 Decode SignatureValue as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_SIGNATURE_VALUE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE_VALUE) && (EXI_DECODE_XMLSIG_SIGNATURE_VALUE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_SignedInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_SIGNED_INFO) && (EXI_DECODE_XMLSIG_SIGNED_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_SignedInfo( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_SignedInfoType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_SignedInfoType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_ReferenceType) lastReference;
  #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_SignedInfoType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_SignedInfoType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_SIGNED_INFO_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, TRUE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNED_INFO, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ID) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element CanonicalizationMethod with missing elements in front */
    if(0 == structPtr->IdFlag)
    {
      /* SE(CanonicalizationMethod) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD) && (EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_XMLSIG_CanonicalizationMethod(DecWsPtr, &(structPtr->CanonicalizationMethod));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNED_INFO, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD) && (EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD == STD_ON)) */
    }
    /* #60 Decode element CanonicalizationMethod without missing elements in front */
    else
    {
      /* #70 Decode element CanonicalizationMethod */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode) /* SE(CanonicalizationMethod) */
      {
      #if (defined(EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD) && (EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_XMLSIG_CanonicalizationMethod(DecWsPtr, &(structPtr->CanonicalizationMethod));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNED_INFO, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD) && (EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_XMLSIG_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #80 Decode element SignatureMethod */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(SignatureMethod) */
    {
      #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_METHOD) && (EXI_DECODE_XMLSIG_SIGNATURE_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_XMLSIG_SignatureMethod(DecWsPtr, &(structPtr->SignatureMethod));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNED_INFO, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE_METHOD) && (EXI_DECODE_XMLSIG_SIGNATURE_METHOD == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_XMLSIG_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #90 Decode multi occurence element Reference, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(Reference) */
    {
      #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_Reference(DecWsPtr, &(structPtr->Reference));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_SIGNED_INFO, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->Reference->NextReferencePtr = (Exi_XMLSIG_ReferenceType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(Reference) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastReference = structPtr->Reference;
    #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0)
    /* #100 Decode subsequent occurences of Reference */
    for(i=0; i<(EXI_MAXOCCURS_XMLSIG_REFERENCE - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode)/* SE(Reference) */
      {
        Exi_Decode_XMLSIG_Reference(DecWsPtr, &(lastReference->NextReferencePtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastReference = lastReference->NextReferencePtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(Reference) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0) */
    #endif /* (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) */
    lastReference->NextReferencePtr = (Exi_XMLSIG_ReferenceType*)NULL_PTR;
    exiEventCode = 0;
    #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0)
    if((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0) */
    #endif /* #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #110 If SignedInfo was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0)
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* last element unbounded, EE(SignedInfo) already found, EXI stream end reached */
      }
      else
      {
        /* unexpected state */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
      #endif /* #if ((EXI_MAXOCCURS_XMLSIG_REFERENCE - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) */
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_SIGNED_INFO, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_SIGNED_INFO) && (EXI_DECODE_XMLSIG_SIGNED_INFO == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_Transform
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_TRANSFORM) && (EXI_DECODE_XMLSIG_TRANSFORM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_Transform( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_TransformType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_TransformType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_TransformChoiceType) lastChoiceElement;
  #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE - 1) > 0) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_TransformType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_TransformType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_TRANSFORM_TYPE);
    /* #40 Decode attribute Algorithm */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* AT(Algorithm) */
    {
      #if (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_AttributeAlgorithm(DecWsPtr, &(structPtr->Algorithm));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_TRANSFORM, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* Start of Choice Element */
    /* #50 Allocate buffer for this element */
    structPtr->ChoiceElement = (Exi_XMLSIG_TransformChoiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_TransformChoiceType), TRUE);
    if (NULL_PTR == (void*)structPtr->ChoiceElement)
    {
      return;
    }
    /* #60 Decode TransformChoice element */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 3, TRUE, TRUE, TRUE, 1, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(GenericElement) */
    {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.GenericElement));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->GenericElementFlag = 1;
      structPtr->ChoiceElementFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_TRANSFORM, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
    }
    else if(1 == exiEventCode) /* SE(XPath) */
    {
      #if (defined(EXI_DECODE_STRING) && (EXI_DECODE_STRING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_string(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.XPath), EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->XPathFlag = 1;
      structPtr->ChoiceElementFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_TRANSFORM, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_STRING) && (EXI_DECODE_STRING == STD_ON)) */
    }
    else if(2 == exiEventCode)
    {
      structPtr->ChoiceElementFlag = 0;
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ChoiceElementFlag)
    {
      structPtr->ChoiceElement->NextChoiceElementPtr = (Exi_XMLSIG_TransformChoiceType*)NULL_PTR;
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* check exiEventCode equals EE(ChoiceElement) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
      {
        /* an error occured in a previous step -> abort to avoid not required loop */
        return;
      }
      lastChoiceElement = structPtr->ChoiceElement;
      #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE - 1) > 0)
      /* #70 Decode subsequent occurences of ChoiceElement */
      for(i=0; i<(EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
      { /* PRQA S 3201 */ /* MD_MSR_14.1 */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
        if(2 == exiEventCode)/* reached next Tag */
        {
          DecWsPtr->DecWs.EERequired = FALSE;
          break;
        }
        /* Start of Choice Element */
        /* #80 Allocate buffer for this element */
        lastChoiceElement->NextChoiceElementPtr = (Exi_XMLSIG_TransformChoiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_TransformChoiceType), TRUE);
        if (NULL_PTR == (void*)lastChoiceElement->NextChoiceElementPtr)
        {
          return;
        }
        if(0 == exiEventCode) /* SE(GenericElement) */
        {
        #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.GenericElement));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          lastChoiceElement->NextChoiceElementPtr->GenericElementFlag = 1;
          lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_TRANSFORM, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON) */
        }
        else if(1 == exiEventCode) /* SE(XPath) */
        {
        #if (defined(EXI_DECODE_STRING) && EXI_DECODE_STRING == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          Exi_Decode_string(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.XPath), EXI_SCHEMA_SET_XMLSIG_TYPE);
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          lastChoiceElement->NextChoiceElementPtr->XPathFlag = 1;
          lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_TRANSFORM, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_STRING) && EXI_DECODE_STRING == STD_ON) */
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
        if(FALSE == DecWsPtr->DecWs.EERequired)
        {
          /* EE(ChoiceElement) already decoded */
          DecWsPtr->DecWs.EERequired = TRUE;
          continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
        }
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* check exiEventCode equals EE(ChoiceElement) */
        if(0 != exiEventCode)
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      #endif /* #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE - 1) > 0) */
      lastChoiceElement->NextChoiceElementPtr = (Exi_XMLSIG_TransformChoiceType*)NULL_PTR;
      exiEventCode = 0;
      #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE - 1) > 0)
      if((EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE - 1) == i)
      #endif /* #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE - 1) > 0) */
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        if(2 == exiEventCode)
        {
          DecWsPtr->DecWs.EERequired = FALSE;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #90 If Transform was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->ChoiceElementFlag)
      {
      #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE - 1) > 0)
        if(FALSE == DecWsPtr->DecWs.EERequired)
        {
          /* last element unbounded, EE(Transform) already found, EXI stream end reached */
        }
        else
        {
          /* unexpected state */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        }
      #endif /* #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE - 1) > 0) */
      }
      else
      {
        /* EE(Transform) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_TRANSFORM, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_TRANSFORM) && (EXI_DECODE_XMLSIG_TRANSFORM == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_Transforms
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_TRANSFORMS) && (EXI_DECODE_XMLSIG_TRANSFORMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_Transforms( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_TransformsType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_TransformsType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_TransformType) lastTransform;
  #if (defined(EXI_DECODE_XMLSIG_TRANSFORM) && (EXI_DECODE_XMLSIG_TRANSFORM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORM - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORM - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_XMLSIG_TRANSFORM) && (EXI_DECODE_XMLSIG_TRANSFORM == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_TransformsType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_TransformsType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_TRANSFORMS_TYPE);
    /* #40 Decode multi occurence element Transform, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(Transform) */
    {
      #if (defined(EXI_DECODE_XMLSIG_TRANSFORM) && (EXI_DECODE_XMLSIG_TRANSFORM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_XMLSIG_Transform(DecWsPtr, &(structPtr->Transform));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_TRANSFORMS, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_TRANSFORM) && (EXI_DECODE_XMLSIG_TRANSFORM == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->Transform->NextTransformPtr = (Exi_XMLSIG_TransformType*)NULL_PTR;
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_XMLSIG_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastTransform = structPtr->Transform;
    #if (defined(EXI_DECODE_XMLSIG_TRANSFORM) && (EXI_DECODE_XMLSIG_TRANSFORM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORM - 1) > 0)
    /* #50 Decode subsequent occurences of Transform */
    for(i=0; i<(EXI_MAXOCCURS_XMLSIG_TRANSFORM - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode)/* SE(Transform) */
      {
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_XMLSIG_Transform(DecWsPtr, &(lastTransform->NextTransformPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastTransform = lastTransform->NextTransformPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(Transform) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(Transform) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORM - 1) > 0) */
    #endif /* (defined(EXI_DECODE_XMLSIG_TRANSFORM) && (EXI_DECODE_XMLSIG_TRANSFORM == STD_ON)) */
    lastTransform->NextTransformPtr = (Exi_XMLSIG_TransformType*)NULL_PTR;
    exiEventCode = 0;
    #if (defined(EXI_DECODE_XMLSIG_TRANSFORM) && (EXI_DECODE_XMLSIG_TRANSFORM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORM - 1) > 0)
    if((EXI_MAXOCCURS_XMLSIG_TRANSFORM - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORM - 1) > 0) */
    #endif /* #if (defined(EXI_DECODE_XMLSIG_TRANSFORM) && (EXI_DECODE_XMLSIG_TRANSFORM == STD_ON)) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #60 If Transforms was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      #if (defined(EXI_DECODE_XMLSIG_TRANSFORM) && (EXI_DECODE_XMLSIG_TRANSFORM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORM - 1) > 0)
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* last element unbounded, EE(Transforms) already found, EXI stream end reached */
      }
      else
      {
        /* unexpected state */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
      #endif /* #if ((EXI_MAXOCCURS_XMLSIG_TRANSFORM - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_XMLSIG_TRANSFORM) && (EXI_DECODE_XMLSIG_TRANSFORM == STD_ON)) */
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_TRANSFORMS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_TRANSFORMS) && (EXI_DECODE_XMLSIG_TRANSFORMS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_X509Data
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_X509DATA) && (EXI_DECODE_XMLSIG_X509DATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_X509Data( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_X509DataType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_X509DataType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_X509DataChoiceType) lastChoiceElement;
  #if ((EXI_MAXOCCURS_XMLSIG_X509DATACHOICE - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_XMLSIG_X509DATACHOICE - 1) > 0) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_X509DataType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_X509DataType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_XMLSIG_X509DATA_TYPE);
    /* Start of Choice Element */
    /* #40 Allocate buffer for this element */
    structPtr->ChoiceElement = (Exi_XMLSIG_X509DataChoiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_X509DataChoiceType), TRUE);
    if (NULL_PTR == (void*)structPtr->ChoiceElement)
    {
      return;
    }
    /* #50 Decode X509DataChoice element */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 6, 3, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(X509IssuerSerial) */
    {
      #if (defined(EXI_DECODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_DECODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_X509IssuerSerial(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.X509IssuerSerial));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->X509IssuerSerialFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_DECODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) */
    }
    else if(1 == exiEventCode) /* SE(X509SKI) */
    {
      #if (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_base64Binary(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.X509SKI), EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->X509SKIFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) */
    }
    else if(2 == exiEventCode) /* SE(X509SubjectName) */
    {
      #if (defined(EXI_DECODE_STRING) && (EXI_DECODE_STRING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_string(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.X509SubjectName), EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->X509SubjectNameFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_STRING) && (EXI_DECODE_STRING == STD_ON)) */
    }
    else if(3 == exiEventCode) /* SE(X509Certificate) */
    {
      #if (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_base64Binary(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.X509Certificate), EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->X509CertificateFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) */
    }
    else if(4 == exiEventCode) /* SE(X509CRL) */
    {
      #if (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_base64Binary(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.X509CRL), EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->X509CRLFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) */
    }
    else if(5 == exiEventCode) /* SE(GenericElement) */
    {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.GenericElement));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->GenericElementFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && (EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->ChoiceElement->NextChoiceElementPtr = (Exi_XMLSIG_X509DataChoiceType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(ChoiceElement) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastChoiceElement = structPtr->ChoiceElement;
    #if ((EXI_MAXOCCURS_XMLSIG_X509DATACHOICE - 1) > 0)
    /* #60 Decode subsequent occurences of ChoiceElement */
    for(i=0; i<(EXI_MAXOCCURS_XMLSIG_X509DATACHOICE - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 7, 3, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(6 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      /* Start of Choice Element */
      /* #70 Allocate buffer for this element */
      lastChoiceElement->NextChoiceElementPtr = (Exi_XMLSIG_X509DataChoiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_X509DataChoiceType), TRUE);
      if (NULL_PTR == (void*)lastChoiceElement->NextChoiceElementPtr)
      {
        return;
      }
      if(0 == exiEventCode) /* SE(X509IssuerSerial) */
      {
      #if (defined(EXI_DECODE_XMLSIG_X509ISSUER_SERIAL) && EXI_DECODE_XMLSIG_X509ISSUER_SERIAL == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_X509IssuerSerial(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.X509IssuerSerial));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->X509IssuerSerialFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_X509ISSUER_SERIAL) && EXI_DECODE_XMLSIG_X509ISSUER_SERIAL == STD_ON) */
      }
      else if(1 == exiEventCode) /* SE(X509SKI) */
      {
      #if (defined(EXI_DECODE_BASE64BINARY) && EXI_DECODE_BASE64BINARY == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_base64Binary(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.X509SKI), EXI_SCHEMA_SET_XMLSIG_TYPE);
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->X509SKIFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_BASE64BINARY) && EXI_DECODE_BASE64BINARY == STD_ON) */
      }
      else if(2 == exiEventCode) /* SE(X509SubjectName) */
      {
      #if (defined(EXI_DECODE_STRING) && EXI_DECODE_STRING == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_string(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.X509SubjectName), EXI_SCHEMA_SET_XMLSIG_TYPE);
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->X509SubjectNameFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_STRING) && EXI_DECODE_STRING == STD_ON) */
      }
      else if(3 == exiEventCode) /* SE(X509Certificate) */
      {
      #if (defined(EXI_DECODE_BASE64BINARY) && EXI_DECODE_BASE64BINARY == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_base64Binary(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.X509Certificate), EXI_SCHEMA_SET_XMLSIG_TYPE);
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->X509CertificateFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_BASE64BINARY) && EXI_DECODE_BASE64BINARY == STD_ON) */
      }
      else if(4 == exiEventCode) /* SE(X509CRL) */
      {
      #if (defined(EXI_DECODE_BASE64BINARY) && EXI_DECODE_BASE64BINARY == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_base64Binary(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.X509CRL), EXI_SCHEMA_SET_XMLSIG_TYPE);
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->X509CRLFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_BASE64BINARY) && EXI_DECODE_BASE64BINARY == STD_ON) */
      }
      else if(5 == exiEventCode) /* SE(GenericElement) */
      {
      #if (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_GenericElement(DecWsPtr, &(lastChoiceElement->NextChoiceElementPtr->ChoiceValue.GenericElement));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastChoiceElement->NextChoiceElementPtr->GenericElementFlag = 1;
        lastChoiceElement = lastChoiceElement->NextChoiceElementPtr;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_GENERIC_ELEMENT) && EXI_DECODE_XMLSIG_GENERIC_ELEMENT == STD_ON) */
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(ChoiceElement) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(ChoiceElement) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_XMLSIG_X509DATACHOICE - 1) > 0) */
    lastChoiceElement->NextChoiceElementPtr = (Exi_XMLSIG_X509DataChoiceType*)NULL_PTR;
    exiEventCode = 0;
    #if ((EXI_MAXOCCURS_XMLSIG_X509DATACHOICE - 1) > 0)
    if((EXI_MAXOCCURS_XMLSIG_X509DATACHOICE - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_XMLSIG_X509DATACHOICE - 1) > 0) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
      if(6 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #80 If X509Data was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      #if ((EXI_MAXOCCURS_XMLSIG_X509DATACHOICE - 1) > 0)
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* last element unbounded, EE(X509Data) already found, EXI stream end reached */
      }
      else
      {
        /* unexpected state */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
      #endif /* #if ((EXI_MAXOCCURS_XMLSIG_X509DATACHOICE - 1) > 0) */
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_X509DATA, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_X509DATA) && (EXI_DECODE_XMLSIG_X509DATA == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_XMLSIG_X509IssuerSerial
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_DECODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_XMLSIG_X509IssuerSerial( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_XMLSIG_X509IssuerSerialType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_XMLSIG_X509IssuerSerialType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_XMLSIG_X509IssuerSerialType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_XMLSIG_X509IssuerSerialType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element X509IssuerName */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(X509IssuerName) */
    {
      #if (defined(EXI_DECODE_STRING) && (EXI_DECODE_STRING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_string(DecWsPtr, &(structPtr->X509IssuerName), EXI_SCHEMA_SET_XMLSIG_TYPE);
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_XMLSIG_X509ISSUER_SERIAL, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_STRING) && (EXI_DECODE_STRING == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(X509IssuerName) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element X509SerialNumber */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    if(0 == exiEventCode) /* SE(X509SerialNumber) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
      if(0 == exiEventCode)
      {
        /* #50 Allocate buffer for this element */
        structPtr->X509SerialNumber = (Exi_BigIntType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_BigIntType), TRUE);
        if (NULL_PTR == (void*)structPtr->X509SerialNumber)
        {
          return;
        }
        Exi_VBSDecodeBigInt(&DecWsPtr->DecWs, structPtr->X509SerialNumber);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_XMLSIG_TYPE);
    /* check exiEventCode equals EE(X509SerialNumber) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_XMLSIG_X509ISSUER_SERIAL, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_DECODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_SchemaSet_XMLSIG
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_SCHEMA_SET_XMLSIG) && (EXI_DECODE_SCHEMA_SET_XMLSIG == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_SchemaSet_XMLSIG( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(void, AUTOMATIC, EXI_APPL_VAR) structPtr = NULL_PTR;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode all XMLSIG schema elements */
    exiEventCode = 0;
    /* #30 Decode the event code of the element */
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 5);
    /* #40 Switch event code */
    switch(exiEventCode)
    {
    case 0: /* SE(CanonicalizationMethod) */
      /* #50 Element CanonicalizationMethod */
      {
        /* #60 Decode element CanonicalizationMethod */
      #if (defined(EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD) && (EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_CanonicalizationMethod(DecWsPtr, (Exi_XMLSIG_CanonicalizationMethodType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD) && (EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD == STD_ON)) */
      }
    case 1: /* SE(DSAKeyValue) */
      /* #70 Element DSAKeyValue */
      {
        /* #80 Decode element DSAKeyValue */
      #if (defined(EXI_DECODE_XMLSIG_DSAKEY_VALUE) && (EXI_DECODE_XMLSIG_DSAKEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_DSAKeyValue(DecWsPtr, (Exi_XMLSIG_DSAKeyValueType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_DSAKEY_VALUE) && (EXI_DECODE_XMLSIG_DSAKEY_VALUE == STD_ON)) */
      }
    case 2: /* SE(DigestMethod) */
      /* #90 Element DigestMethod */
      {
        /* #100 Decode element DigestMethod */
      #if (defined(EXI_DECODE_XMLSIG_DIGEST_METHOD) && (EXI_DECODE_XMLSIG_DIGEST_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_DigestMethod(DecWsPtr, (Exi_XMLSIG_DigestMethodType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_DIGEST_METHOD) && (EXI_DECODE_XMLSIG_DIGEST_METHOD == STD_ON)) */
      }
    case 3: /* SE(DigestValue) */
      /* #110 Element DigestValue */
      {
        /* #120 Decode element DigestValue */
      #if (defined(EXI_DECODE_XMLSIG_DIGEST_VALUE) && (EXI_DECODE_XMLSIG_DIGEST_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_DigestValue(DecWsPtr, (Exi_XMLSIG_DigestValueType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_DIGEST_VALUE) && (EXI_DECODE_XMLSIG_DIGEST_VALUE == STD_ON)) */
      }
    case 4: /* SE(KeyInfo) */
      /* #130 Element KeyInfo */
      {
        /* #140 Decode element KeyInfo */
      #if (defined(EXI_DECODE_XMLSIG_KEY_INFO) && (EXI_DECODE_XMLSIG_KEY_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_KeyInfo(DecWsPtr, (Exi_XMLSIG_KeyInfoType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_KEY_INFO) && (EXI_DECODE_XMLSIG_KEY_INFO == STD_ON)) */
      }
    case 5: /* SE(KeyName) */
      /* #150 Element KeyName */
      {
        /* #160 Decode element KeyName */
      #if (defined(EXI_DECODE_XMLSIG_KEY_NAME) && (EXI_DECODE_XMLSIG_KEY_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_KeyName(DecWsPtr, (Exi_XMLSIG_KeyNameType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_KEY_NAME) && (EXI_DECODE_XMLSIG_KEY_NAME == STD_ON)) */
      }
    case 6: /* SE(KeyValue) */
      /* #170 Element KeyValue */
      {
        /* #180 Decode element KeyValue */
      #if (defined(EXI_DECODE_XMLSIG_KEY_VALUE) && (EXI_DECODE_XMLSIG_KEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_KeyValue(DecWsPtr, (Exi_XMLSIG_KeyValueType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_KEY_VALUE) && (EXI_DECODE_XMLSIG_KEY_VALUE == STD_ON)) */
      }
    case 7: /* SE(Manifest) */
      /* #190 Element Manifest */
      {
        /* #200 Decode element Manifest */
      #if (defined(EXI_DECODE_XMLSIG_MANIFEST) && (EXI_DECODE_XMLSIG_MANIFEST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_Manifest(DecWsPtr, (Exi_XMLSIG_ManifestType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_MANIFEST) && (EXI_DECODE_XMLSIG_MANIFEST == STD_ON)) */
      }
    case 8: /* SE(MgmtData) */
      /* #210 Element MgmtData */
      {
        /* #220 Decode element MgmtData */
      #if (defined(EXI_DECODE_XMLSIG_MGMT_DATA) && (EXI_DECODE_XMLSIG_MGMT_DATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_MgmtData(DecWsPtr, (Exi_XMLSIG_MgmtDataType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_MGMT_DATA) && (EXI_DECODE_XMLSIG_MGMT_DATA == STD_ON)) */
      }
    case 9: /* SE(Object) */
      /* #230 Element Object */
      {
        /* #240 Decode element Object */
      #if (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_Object(DecWsPtr, (Exi_XMLSIG_ObjectType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_OBJECT) && (EXI_DECODE_XMLSIG_OBJECT == STD_ON)) */
      }
    case 10: /* SE(PGPData) */
      /* #250 Element PGPData */
      {
        /* #260 Decode element PGPData */
      #if (defined(EXI_DECODE_XMLSIG_PGPDATA) && (EXI_DECODE_XMLSIG_PGPDATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_PGPData(DecWsPtr, (Exi_XMLSIG_PGPDataType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_PGPDATA) && (EXI_DECODE_XMLSIG_PGPDATA == STD_ON)) */
      }
    case 11: /* SE(RSAKeyValue) */
      /* #270 Element RSAKeyValue */
      {
        /* #280 Decode element RSAKeyValue */
      #if (defined(EXI_DECODE_XMLSIG_RSAKEY_VALUE) && (EXI_DECODE_XMLSIG_RSAKEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_RSAKeyValue(DecWsPtr, (Exi_XMLSIG_RSAKeyValueType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_RSAKEY_VALUE) && (EXI_DECODE_XMLSIG_RSAKEY_VALUE == STD_ON)) */
      }
    case 12: /* SE(Reference) */
      /* #290 Element Reference */
      {
        /* #300 Decode element Reference */
      #if (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_Reference(DecWsPtr, (Exi_XMLSIG_ReferenceType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_REFERENCE) && (EXI_DECODE_XMLSIG_REFERENCE == STD_ON)) */
      }
    case 13: /* SE(RetrievalMethod) */
      /* #310 Element RetrievalMethod */
      {
        /* #320 Decode element RetrievalMethod */
      #if (defined(EXI_DECODE_XMLSIG_RETRIEVAL_METHOD) && (EXI_DECODE_XMLSIG_RETRIEVAL_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_RetrievalMethod(DecWsPtr, (Exi_XMLSIG_RetrievalMethodType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_RETRIEVAL_METHOD) && (EXI_DECODE_XMLSIG_RETRIEVAL_METHOD == STD_ON)) */
      }
    case 14: /* SE(SPKIData) */
      /* #330 Element SPKIData */
      {
        /* #340 Decode element SPKIData */
      #if (defined(EXI_DECODE_XMLSIG_SPKIDATA) && (EXI_DECODE_XMLSIG_SPKIDATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_SPKIData(DecWsPtr, (Exi_XMLSIG_SPKIDataType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_SPKIDATA) && (EXI_DECODE_XMLSIG_SPKIDATA == STD_ON)) */
      }
    case 15: /* SE(Signature) */
      /* #350 Element Signature */
      {
        /* #360 Decode element Signature */
      #if (defined(EXI_DECODE_XMLSIG_SIGNATURE) && (EXI_DECODE_XMLSIG_SIGNATURE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_Signature(DecWsPtr, (Exi_XMLSIG_SignatureType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE) && (EXI_DECODE_XMLSIG_SIGNATURE == STD_ON)) */
      }
    case 16: /* SE(SignatureMethod) */
      /* #370 Element SignatureMethod */
      {
        /* #380 Decode element SignatureMethod */
      #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_METHOD) && (EXI_DECODE_XMLSIG_SIGNATURE_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_SignatureMethod(DecWsPtr, (Exi_XMLSIG_SignatureMethodType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE_METHOD) && (EXI_DECODE_XMLSIG_SIGNATURE_METHOD == STD_ON)) */
      }
    case 17: /* SE(SignatureProperties) */
      /* #390 Element SignatureProperties */
      {
        /* #400 Decode element SignatureProperties */
      #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTIES) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTIES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_SignatureProperties(DecWsPtr, (Exi_XMLSIG_SignaturePropertiesType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTIES) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTIES == STD_ON)) */
      }
    case 18: /* SE(SignatureProperty) */
      /* #410 Element SignatureProperty */
      {
        /* #420 Decode element SignatureProperty */
      #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_SignatureProperty(DecWsPtr, (Exi_XMLSIG_SignaturePropertyType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) */
      }
    case 19: /* SE(SignatureValue) */
      /* #430 Element SignatureValue */
      {
        /* #440 Decode element SignatureValue */
      #if (defined(EXI_DECODE_XMLSIG_SIGNATURE_VALUE) && (EXI_DECODE_XMLSIG_SIGNATURE_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_SignatureValue(DecWsPtr, (Exi_XMLSIG_SignatureValueType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE_VALUE) && (EXI_DECODE_XMLSIG_SIGNATURE_VALUE == STD_ON)) */
      }
    case 20: /* SE(SignedInfo) */
      /* #450 Element SignedInfo */
      {
        /* #460 Decode element SignedInfo */
      #if (defined(EXI_DECODE_XMLSIG_SIGNED_INFO) && (EXI_DECODE_XMLSIG_SIGNED_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_SignedInfo(DecWsPtr, (Exi_XMLSIG_SignedInfoType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_SIGNED_INFO) && (EXI_DECODE_XMLSIG_SIGNED_INFO == STD_ON)) */
      }
    case 21: /* SE(Transform) */
      /* #470 Element Transform */
      {
        /* #480 Decode element Transform */
      #if (defined(EXI_DECODE_XMLSIG_TRANSFORM) && (EXI_DECODE_XMLSIG_TRANSFORM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_Transform(DecWsPtr, (Exi_XMLSIG_TransformType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_TRANSFORM) && (EXI_DECODE_XMLSIG_TRANSFORM == STD_ON)) */
      }
    case 22: /* SE(Transforms) */
      /* #490 Element Transforms */
      {
        /* #500 Decode element Transforms */
      #if (defined(EXI_DECODE_XMLSIG_TRANSFORMS) && (EXI_DECODE_XMLSIG_TRANSFORMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_Transforms(DecWsPtr, (Exi_XMLSIG_TransformsType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_TRANSFORMS) && (EXI_DECODE_XMLSIG_TRANSFORMS == STD_ON)) */
      }
    case 23: /* SE(X509Data) */
      /* #510 Element X509Data */
      {
        /* #520 Decode element X509Data */
      #if (defined(EXI_DECODE_XMLSIG_X509DATA) && (EXI_DECODE_XMLSIG_X509DATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_XMLSIG_X509Data(DecWsPtr, (Exi_XMLSIG_X509DataType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_X509DATA) && (EXI_DECODE_XMLSIG_X509DATA == STD_ON)) */
      }
    default:
      /* #530 Unknown element */
      {
        /* #540 Set status code to error */
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_XMLSIG, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_SCHEMA_SET_XMLSIG) && (EXI_DECODE_SCHEMA_SET_XMLSIG == STD_ON)) */


#define EXI_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/* PRQA L:NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */

#endif /* (defined (EXI_ENABLE_DECODE_XMLSIG_MESSAGE_SET) && (EXI_ENABLE_DECODE_XMLSIG_MESSAGE_SET == STD_ON)) */

