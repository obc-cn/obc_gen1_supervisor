/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Exi.c
 *        \brief  Efficient XML Interchange main source file
 *
 *      \details  Vector static code implementation for the Efficient XML Interchange module.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file Exi.h.
 *********************************************************************************************************************/

/* PRQA S 0857 EOF */ /* MD_Exi_1.1_0857 */ /* [L] Number of macro definitions exceeds 1024 - program is non-conforming. */

#define EXI_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 EXI_C_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "Exi.h"
#include "Exi_Priv.h"
#include "Exi_BSEncoder.h"
#include "Exi_BSDecoder.h"
#include "Exi_SchemaEncoder.h"
#include "Exi_SchemaDecoder.h"
#if (EXI_ENABLE_PBUF_SUPPORT == STD_ON)
#include "IpBase.h"
#endif

/* PRQA L:EXI_C_IF_NESTING */ /* MD_MSR_1.1_828 */

/*lint -e438 */ /* Suppress ID438 because Config pointer only used in Post-Build Variant */
/*lint -e451 */ /* Suppress ID451 because MemMap.h cannot use a include guard */

/**********************************************************************************************************************
*  VERSION CHECK
*********************************************************************************************************************/
#if ( (EXI_SW_MAJOR_VERSION != 6u) || (EXI_SW_MINOR_VERSION != 0u) || (EXI_SW_PATCH_VERSION != 1u) )
  #error "Vendor specific version numbers of Exi.h and Exi.c are inconsistent"
#endif


/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

#if !defined (EXI_LOCAL)
# define EXI_LOCAL static
#endif /* !defined (EXI_LOCAL) */

#if !defined (EXI_LOCAL_INLINE)
# define EXI_LOCAL_INLINE LOCAL_INLINE
#endif /* !defined (EXI_LOCAL_INLINE) */

/**********************************************************************************************************************
*  LOCAL FUNCTION MACROS
**********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define EXI_START_SEC_CODE
#include "MemMap.h"  /* PRQA S 5087 */  /*  MD_MSR_19.1 */

#if EXI_ENABLE_PBUF_SUPPORT == STD_ON
/**********************************************************************************************************************
 *  Exi_CheckPBufConsistency
 *********************************************************************************************************************/
/*! \brief         Check if a PBuf is consistent.
 *  \details       Check if a PBuf is consistent regarding its length.
 *  \param[in]     PBufPtr            Pointer to the PBuf
 *  \return        EXI_E_OK:                      Finished successfully\n
 *                 EXI_E_NOT_OK:                  PBuf is set to NULL_PTR\n
 *                 EXI_E_INCONSISTEN_PBUF         PBuf length is not consistent
 *  \pre           -
 *  \context       TASK|ISR2
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *********************************************************************************************************************/
EXI_LOCAL FUNC(Exi_ReturnType, EXI_CODE) Exi_CheckPBufConsistency(
  P2CONST(IpBase_PbufType, AUTOMATIC, EXI_APPL_DATA) PBufPtr );
#endif /* EXI_ENABLE_PBUF_SUPPORT */

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Exi_InitMemory
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, EXI_CODE) Exi_InitMemory(void)
{
  /* #10 Function is empty */
}
/**********************************************************************************************************************
 *  Exi_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, EXI_CODE) Exi_Init(P2CONST(Exi_ConfigType, AUTOMATIC, EXI_CONST) Exi_ConfigPtr)
{
  /* #10 Function is empty */
  EXI_DUMMY_STATEMENT(Exi_ConfigPtr); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
}
#if (EXI_VERSION_INFO_API == STD_ON)
/**********************************************************************************************************************
 *  Exi_GetVersionInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, EXI_CODE) Exi_GetVersionInfo( P2VAR(Std_VersionInfoType, AUTOMATIC, EXI_APPL_DATA) VersionInfoPtr)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR;  /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
#if ( EXI_DEV_ERROR_DETECT == STD_ON )
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (VersionInfoPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
#endif /* (EXI_DEV_ERROR_DETECT == STD_ON) */
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Write version information to the output structure */
    VersionInfoPtr->vendorID = EXI_VENDOR_ID;
    VersionInfoPtr->moduleID = EXI_MODULE_ID;
    VersionInfoPtr->sw_major_version = EXI_SW_MAJOR_VERSION;
    VersionInfoPtr->sw_minor_version = EXI_SW_MINOR_VERSION;
    VersionInfoPtr->sw_patch_version = EXI_SW_PATCH_VERSION;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( EXI_DEV_ERROR_REPORT == STD_ON )
  if (errorId != EXI_E_NO_ERROR)
  {
    (void)Det_ReportError(EXI_MODULE_ID, EXI_INSTANCE_ID, EXI_API_ID_GET_VERSION_INFO, errorId);
  }
#else
  EXI_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif /* (EXI_DEV_ERROR_REPORT == STD_ON) */
}
#endif
  /* EXI_VERSION_INFO_API */

/**********************************************************************************************************************
 *  Exi_MainFunction
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, EXI_CODE) Exi_MainFunction(void)
{
  /* #10 Function is empty */
}

/**********************************************************************************************************************
 *  Exi_InitEncodeWorkspace()
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Exi_ReturnType, EXI_CODE) Exi_InitEncodeWorkspace(
  P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr,
  P2CONST(uint8, AUTOMATIC, EXI_APPL_DATA) InBufPtr,
#if EXI_ENABLE_PBUF_SUPPORT == STD_ON
  P2VAR(IpBase_PbufType, AUTOMATIC, EXI_APPL_VAR) OutPBufPtr,
#else
  P2VAR(uint8, AUTOMATIC, EXI_APPL_VAR) OutBufPtr,
  uint16 OutBufLen,
#endif
  uint16 OutBufOfs
#if (EXI_ENABLE_SIMPLE_TX_STREAMING_SUPPORT == STD_ON)
 ,uint32 StartWriteAtStreamPos
 ,boolean CalculateStreamLength
#endif
  )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR;  /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_ReturnType retVal;

  /* #10 Check plausibility of input parameters */
#if ( EXI_DEV_ERROR_DETECT == STD_ON )
  retVal = EXI_E_NOT_OK;
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (InBufPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
# if EXI_ENABLE_PBUF_SUPPORT == STD_ON
  else if (OutPBufPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (OutPBufPtr->payload == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (OutPBufPtr->totLen == 0)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else if (OutBufOfs >= OutPBufPtr->totLen)
  {
    errorId = EXI_E_INV_PARAM;
  }
# else
  else if (OutBufPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (OutBufLen == 0)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else if (OutBufOfs >= OutBufLen)
  {
    errorId = EXI_E_INV_PARAM;
  }
# endif
  else
#endif /* (EXI_DEV_ERROR_DETECT == STD_ON) */
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Initialize basic encoder workspace */
#if EXI_ENABLE_PBUF_SUPPORT == STD_ON
    retVal = Exi_CheckPBufConsistency(OutPBufPtr);
    if (retVal == EXI_E_OK)
    {
      retVal = Exi_VBSEncodeInitWorkspace(
                              &EncWsPtr->EncWs,
                              OutPBufPtr,
                              OutBufOfs
#if (EXI_ENABLE_SIMPLE_TX_STREAMING_SUPPORT == STD_ON)
                              ,StartWriteAtStreamPos
                              ,CalculateStreamLength
#endif
                              );
    }
#else
    retVal = Exi_VBSEncodeInitWorkspace(
                               &EncWsPtr->EncWs,
                               OutBufPtr,
                               OutBufLen,
                               OutBufOfs
#if (EXI_ENABLE_SIMPLE_TX_STREAMING_SUPPORT == STD_ON)
                              ,StartWriteAtStreamPos
                              ,CalculateStreamLength
#endif
                               );
#endif /* EXI_ENABLE_PBUF_SUPPORT */
    EncWsPtr->InputData.StoragePtr = InBufPtr;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( EXI_DEV_ERROR_REPORT == STD_ON )
  if (errorId != EXI_E_NO_ERROR)
  {
    (void)Det_ReportError(EXI_MODULE_ID, EXI_INSTANCE_ID, EXI_API_ID_INIT_ENCODE_WORKSPACE, errorId);
  }
#else
  EXI_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif /* (EXI_DEV_ERROR_REPORT == STD_ON) */

  return retVal;
} /* PRQA S 6060, 6080 */ /* MD_MSR_STPAR, MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Exi_InitDecodeWorkspace()
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Exi_ReturnType, EXI_CODE) Exi_InitDecodeWorkspace(
  P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr,
#if EXI_ENABLE_PBUF_SUPPORT == STD_ON
  P2CONST(IpBase_PbufType, AUTOMATIC, EXI_APPL_DATA) InPBufPtr,
#else
  P2CONST(uint8, AUTOMATIC, EXI_APPL_DATA) InBufPtr,
#endif
  P2VAR(uint8, AUTOMATIC, EXI_APPL_VAR) OutBufPtr,
#if EXI_ENABLE_PBUF_SUPPORT == STD_OFF
  uint16 InBufLen,
#endif
  uint16 OutBufLen,
  uint16 InBufOfs)
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR;  /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_ReturnType retVal;

  /* #10 Check plausibility of input parameters */
#if ( EXI_DEV_ERROR_DETECT == STD_ON )
  retVal = EXI_E_NOT_OK;
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (OutBufPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
#if EXI_ENABLE_PBUF_SUPPORT == STD_ON
  else if (InPBufPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (InPBufPtr->payload == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (InPBufPtr->totLen == 0)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else if (InBufOfs >= InPBufPtr->totLen)
  {
    errorId = EXI_E_INV_PARAM;
  }
#else
  else if (InBufPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (InBufLen == 0)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else if (InBufOfs >= InBufLen)
  {
    errorId = EXI_E_INV_PARAM;
  }
#endif
  else if (OutBufLen == 0)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
#endif /* (EXI_DEV_ERROR_DETECT == STD_ON) */
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Initialize basic decoder workspace */
#if EXI_ENABLE_PBUF_SUPPORT == STD_ON
    retVal = Exi_CheckPBufConsistency(InPBufPtr);
    if (retVal == EXI_E_OK)
    {
      retVal = Exi_VBSDecodeInitWorkspace(&DecWsPtr->DecWs, InPBufPtr, InBufOfs);
    }
#else
    retVal = Exi_VBSDecodeInitWorkspace(&DecWsPtr->DecWs, InBufPtr, InBufLen, InBufOfs);
#endif
    DecWsPtr->OutputData.StoragePtr = OutBufPtr;
    DecWsPtr->OutputData.StorageLen = OutBufLen;
    DecWsPtr->OutputData.StorageOffset = 0;
    DecWsPtr->OutputData.RootElementId = EXI_SCHEMA_UNKNOWN_ELEMENT_TYPE;
    DecWsPtr->OutputData.SchemaSetId = EXI_UNKNOWN_SCHEMA_SET_TYPE;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( EXI_DEV_ERROR_REPORT == STD_ON )
  if (errorId != EXI_E_NO_ERROR)
  {
    (void)Det_ReportError(EXI_MODULE_ID, EXI_INSTANCE_ID, EXI_API_ID_INIT_DECODE_WORKSPACE, errorId);
  }
#else
  EXI_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif /* (EXI_DEV_ERROR_REPORT == STD_ON) */

  return retVal;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Exi_Encode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Exi_ReturnType, EXI_CODE) Exi_Encode( P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR;  /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_ReturnType retVal = EXI_E_NOT_OK;

  /* #10 Check plausibility of input parameters */
#if ( EXI_DEV_ERROR_DETECT == STD_ON )
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (EncWsPtr->InputData.StoragePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
#if (EXI_ENABLE_PBUF_SUPPORT == STD_ON)
  else if (EncWsPtr->EncWs.PBufPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (EncWsPtr->EncWs.PBufPtr[EncWsPtr->EncWs.PBufIdx].len == 0)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else if (EncWsPtr->EncWs.BytePos >= EncWsPtr->EncWs.PBufPtr[EncWsPtr->EncWs.PBufIdx].len)
  {
    errorId = EXI_E_INV_PARAM;
  }
#else
  else if (EncWsPtr->EncWs.BufPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (EncWsPtr->EncWs.BufLen == 0)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else if (EncWsPtr->EncWs.BytePos >= EncWsPtr->EncWs.BufLen)
  {
    errorId = EXI_E_INV_PARAM;
  }
#endif
  else if (EncWsPtr->InputData.RootElementId >= EXI_SCHEMA_UNKNOWN_ELEMENT_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
#endif /* (EXI_DEV_ERROR_DETECT == STD_ON) */
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
#if ( EXI_DEV_ERROR_DETECT == STD_OFF ) /* Already checked in DET if DET is enabled */
    if(EXI_SCHEMA_UNKNOWN_ELEMENT_TYPE > EncWsPtr->InputData.RootElementId)
#endif
    {
      /* #20 Encode EXI header */
      Exi_VBSWriteHeader(&EncWsPtr->EncWs);
      retVal = EncWsPtr->EncWs.StatusCode;
      if(EXI_E_OK == retVal)
      {
        /* #30 Encode EXI schema root element */
        Exi_Encode_SchemaRoot(EncWsPtr);
        retVal = EncWsPtr->EncWs.StatusCode;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( EXI_DEV_ERROR_REPORT == STD_ON )
  if (errorId != EXI_E_NO_ERROR)
  {
    (void)Det_ReportError(EXI_MODULE_ID, EXI_INSTANCE_ID, EXI_API_ID_ENCODE, errorId);
  }
#else
  EXI_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif /* (EXI_DEV_ERROR_REPORT == STD_ON) */

  return retVal;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Exi_EncodeFragment
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Exi_ReturnType, EXI_CODE) Exi_EncodeFragment( P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr,
                                       Exi_NamespaceIdType Namespace )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR;  /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_ReturnType retVal = EXI_E_NOT_OK;

  /* #10 Check plausibility of input parameters */
#if ( EXI_DEV_ERROR_DETECT == STD_ON )
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (EncWsPtr->InputData.StoragePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
#if (EXI_ENABLE_PBUF_SUPPORT == STD_ON)
  else if (EncWsPtr->EncWs.PBufPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (EncWsPtr->EncWs.PBufPtr[EncWsPtr->EncWs.PBufIdx].len == 0)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else if (EncWsPtr->EncWs.BytePos >= EncWsPtr->EncWs.PBufPtr[EncWsPtr->EncWs.PBufIdx].len)
  {
    errorId = EXI_E_INV_PARAM;
  }
#else
  else if (EncWsPtr->EncWs.BufPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (EncWsPtr->EncWs.BufLen == 0)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else if (EncWsPtr->EncWs.BytePos >= EncWsPtr->EncWs.BufLen)
  {
    errorId = EXI_E_INV_PARAM;
  }
#endif
  else if (EncWsPtr->InputData.RootElementId >= EXI_SCHEMA_UNKNOWN_ELEMENT_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
#endif /* (EXI_DEV_ERROR_DETECT == STD_ON) */
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
#if ( EXI_DEV_ERROR_DETECT == STD_OFF ) /* Already checked in DET if DET is enabled */
    if(EXI_SCHEMA_UNKNOWN_ELEMENT_TYPE > EncWsPtr->InputData.RootElementId)
#endif
    {
      /* #20 Encode EXI header */
      Exi_VBSWriteHeader(&EncWsPtr->EncWs);
      retVal = EncWsPtr->EncWs.StatusCode;
      if(EXI_E_OK == retVal)
      {
#if (defined(EXI_ENCODE_SCHEMA_FRAGMENT) && (EXI_ENCODE_SCHEMA_FRAGMENT == STD_ON))
        /* #30 Encode EXI schema fragment */
        Exi_Encode_SchemaFragment(EncWsPtr, Namespace);
        retVal = EncWsPtr->EncWs.StatusCode;
#else
        EXI_DUMMY_STATEMENT(Namespace); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
        retVal = EXI_E_DISABLED_FEATURE;
#endif /* #if (defined(EXI_ENCODE_SCHEMA_FRAGMENT) && (EXI_ENCODE_SCHEMA_FRAGMENT == STD_ON)) */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( EXI_DEV_ERROR_REPORT == STD_ON )
  if (errorId != EXI_E_NO_ERROR)
  {
    (void)Det_ReportError(EXI_MODULE_ID, EXI_INSTANCE_ID, EXI_API_ID_ENCODE_FRAGMENT, errorId);
  }
#else
  EXI_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif /* (EXI_DEV_ERROR_REPORT == STD_ON) */

  return retVal;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Exi_FinalizeExiStream
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Exi_ReturnType, EXI_CODE) Exi_FinalizeExiStream( P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR;  /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_ReturnType retVal;

  /* #10 Check plausibility of input parameters */
#if ( EXI_DEV_ERROR_DETECT == STD_ON )
  retVal = EXI_E_NOT_OK;
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (EncWsPtr->InputData.StoragePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
# if (EXI_ENABLE_PBUF_SUPPORT == STD_ON)
  else if (EncWsPtr->EncWs.PBufPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (EncWsPtr->EncWs.PBufPtr[EncWsPtr->EncWs.PBufIdx].len == 0)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else if (EncWsPtr->EncWs.BytePos > EncWsPtr->EncWs.PBufPtr[EncWsPtr->EncWs.PBufIdx].len)
  {
    errorId = EXI_E_INV_PARAM;
  }
#  if (EXI_ENABLE_SIMPLE_TX_STREAMING_SUPPORT == STD_ON)
  else if ((EncWsPtr->EncWs.BitPos != 0) && (EncWsPtr->EncWs.BytePos > EncWsPtr->EncWs.PBufPtr[EncWsPtr->EncWs.PBufIdx].len))
  {
    errorId = EXI_E_INV_PARAM;
  }
#  else
  else if ((0 != EncWsPtr->EncWs.BitPos) && (EncWsPtr->EncWs.BytePos > EncWsPtr->EncWs.PBufPtr[EncWsPtr->EncWs.PBufIdx].len))
  {
    errorId = EXI_E_INV_PARAM;
  }
#  endif
# else
  else if (EncWsPtr->EncWs.BufPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (EncWsPtr->EncWs.BufLen == 0)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else if (EncWsPtr->EncWs.BytePos >= EncWsPtr->EncWs.BufLen)
  {
    errorId = EXI_E_INV_PARAM;
  }
#  if (EXI_ENABLE_SIMPLE_TX_STREAMING_SUPPORT == STD_ON)
  else if ((0 != EncWsPtr->EncWs.BitPos) && (EncWsPtr->EncWs.BytePos > EncWsPtr->EncWs.BufLen))
  {
    errorId = EXI_E_INV_PARAM;
  }
#  else
  else if ((0 != EncWsPtr->EncWs.BitPos) && (EncWsPtr->EncWs.BytePos >= EncWsPtr->EncWs.BufLen))
  {
    errorId = EXI_E_INV_PARAM;
  }
#  endif
# endif
  else
#endif /* (EXI_DEV_ERROR_DETECT == STD_ON) */
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Add padding to the end of the stream if required */
    if (0 != EncWsPtr->EncWs.BitPos)
    {
      EncWsPtr->EncWs.BitPos = 0;
#if (EXI_ENABLE_SIMPLE_TX_STREAMING_SUPPORT == STD_ON)
      if (TRUE == EncWsPtr->EncWs.WritingToBufferIsActive)
      {
        EncWsPtr->EncWs.BytePos++;
      }
      else
      {
        EncWsPtr->EncWs.OverallStreamBytePos++;
      }
#else
      EncWsPtr->EncWs.BytePos++;
#endif
    }

    /* #30 Calculate stream length */
#if (EXI_ENABLE_SIMPLE_TX_STREAMING_SUPPORT == STD_ON)
    /* Check if writing to the EXI stream is currently active -> this is last segment */
    if (TRUE == EncWsPtr->EncWs.WritingToBufferIsActive)
    {
      /* Writing to buffer is currently active -> Stream segment is shorter than buffer -> add segment length */
      EncWsPtr->EncWs.OverallStreamBytePos += (EncWsPtr->EncWs.PBufProcessedElementBytes + EncWsPtr->EncWs.BytePos) - EncWsPtr->EncWs.StartOffset;

      /* This is the last stream segment */
# if (EXI_ENABLE_PBUF_SUPPORT == STD_ON)
      EncWsPtr->EncWs.CurrentStreamSegmentLen = (EncWsPtr->EncWs.PBufProcessedElementBytes + EncWsPtr->EncWs.BytePos) - EncWsPtr->EncWs.StartOffset;
# else
      EncWsPtr->EncWs.CurrentStreamSegmentLen = EncWsPtr->EncWs.BytePos - EncWsPtr->EncWs.StartOffset;
# endif
      EncWsPtr->EncWs.TotalStreamLength = EncWsPtr->EncWs.OverallStreamBytePos;
      EncWsPtr->EncWs.StreamComplete = TRUE;
    }
    else
    {
      /* EncWsPtr->EncWs.CurrentStreamSegmentLen already written in Exi_VBSWriteBits() or nothing was written */
      EncWsPtr->EncWs.StreamComplete = FALSE;

      /* Check if the stream length should be calculated */
      if (TRUE == EncWsPtr->EncWs.CalculateStreamLength)
      {
        EncWsPtr->EncWs.TotalStreamLength = EncWsPtr->EncWs.OverallStreamBytePos;
      }
      else
      {
        EncWsPtr->EncWs.TotalStreamLength = 0;
      }
    }
#else
# if (EXI_ENABLE_PBUF_SUPPORT == STD_ON)
    EncWsPtr->EncWs.TotalStreamLength = EncWsPtr->EncWs.PBufProcessedElementBytes + EncWsPtr->EncWs.BytePos - EncWsPtr->EncWs.StartOffset;
# else
    EncWsPtr->EncWs.TotalStreamLength = EncWsPtr->EncWs.BytePos - EncWsPtr->EncWs.StartOffset;
# endif
#endif
    retVal = E_OK;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( EXI_DEV_ERROR_REPORT == STD_ON )
  if (errorId != EXI_E_NO_ERROR)
  {
    (void)Det_ReportError(EXI_MODULE_ID, EXI_INSTANCE_ID, EXI_API_ID_DECODE, errorId);
  }
#else
  EXI_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif /* (EXI_DEV_ERROR_REPORT == STD_ON) */

  return retVal;
} /* PRQA S 6010, 6030, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STMIF */


/**********************************************************************************************************************
 *  Exi_Decode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(Exi_ReturnType, EXI_CODE) Exi_Decode( P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR;  /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_ReturnType retVal;

  /* #10 Check plausibility of input parameters */
#if ( EXI_DEV_ERROR_DETECT == STD_ON )
  retVal = EXI_E_NOT_OK;
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (DecWsPtr->OutputData.StoragePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
#if (EXI_ENABLE_PBUF_SUPPORT == STD_ON)
  else if (DecWsPtr->DecWs.PBufPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (DecWsPtr->DecWs.PBufPtr[DecWsPtr->DecWs.PBufIdx].len == 0)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else if (DecWsPtr->DecWs.BytePos >= DecWsPtr->DecWs.PBufPtr[DecWsPtr->DecWs.PBufIdx].len)
  {
    errorId = EXI_E_INV_PARAM;
  }
#else
  else if (DecWsPtr->DecWs.BufPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else if (DecWsPtr->DecWs.BufLen == 0)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else if (DecWsPtr->DecWs.BytePos >= DecWsPtr->DecWs.BufLen)
  {
    errorId = EXI_E_INV_PARAM;
  }
#endif
  else if (DecWsPtr->OutputData.StorageLen == 0)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else if (DecWsPtr->OutputData.StorageOffset >= DecWsPtr->OutputData.StorageLen)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else if (DecWsPtr->OutputData.SchemaSetId >= EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
#endif /* (EXI_DEV_ERROR_DETECT == STD_ON) */
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Read EXI header */
    Exi_VBSReadHeader(&DecWsPtr->DecWs);
    retVal = DecWsPtr->DecWs.StatusCode;
    /* #30 If reading was OK, call the decode function of the specific schema set */
    if(EXI_E_OK == retVal)
    {
      if (EXI_UNKNOWN_SCHEMA_SET_TYPE > DecWsPtr->OutputData.SchemaSetId)
      {
        if(NULL_PTR != Exi_DecoderFcts[DecWsPtr->OutputData.SchemaSetId])
        {
          Exi_DecoderFcts[DecWsPtr->OutputData.SchemaSetId](DecWsPtr);
          retVal = DecWsPtr->DecWs.StatusCode;
        }
        else
        {
          retVal = EXI_E_NOT_OK;
        }
      }
      else
      {
        retVal = EXI_E_NOT_OK;
      }
    }
    /* #40 If no error did occur: Calculate the decoded EXI stream length, it may be checked in the calling module */
    if (EXI_E_OK == retVal)
    {
      if(0 != DecWsPtr->DecWs.BitPos)
      {
        DecWsPtr->DecWs.BitPos = 0;
        DecWsPtr->DecWs.BytePos++;
      }
#if (EXI_ENABLE_PBUF_SUPPORT == STD_ON)
      DecWsPtr->DecWs.TotalStreamLength = (DecWsPtr->DecWs.PBufProcessedElementBytes + DecWsPtr->DecWs.BytePos) - DecWsPtr->DecWs.StartOffset;
#else
      DecWsPtr->DecWs.TotalStreamLength = DecWsPtr->DecWs.BytePos - DecWsPtr->DecWs.StartOffset;
#endif
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
#if ( EXI_DEV_ERROR_REPORT == STD_ON )
  if (errorId != EXI_E_NO_ERROR)
  {
    (void)Det_ReportError(EXI_MODULE_ID, EXI_INSTANCE_ID, EXI_API_ID_DECODE, errorId);
  }
#else
  EXI_DUMMY_STATEMENT(errorId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint !e438 */
#endif /* (EXI_DEV_ERROR_REPORT == STD_ON) */

  return retVal;
} /* PRQA S 6010, 6030, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STMIF */

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 **********************************************************************************************************************/

#if EXI_ENABLE_PBUF_SUPPORT == STD_ON
/**********************************************************************************************************************
 *  Exi_CheckPBufConsistency
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
EXI_LOCAL FUNC(Exi_ReturnType, EXI_CODE) Exi_CheckPBufConsistency(
  P2CONST(IpBase_PbufType, AUTOMATIC, EXI_APPL_DATA) PBufPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  P2CONST(IpBase_PbufType, AUTOMATIC, EXI_APPL_DATA) currentPBufSegmentPtr = &PBufPtr[0];
  Exi_ReturnType retVal = EXI_E_NOT_OK;
  uint32 processedLength = 0;
  uint32 firstElementTotLen = currentPBufSegmentPtr->totLen;

  /* --------------------------------------------- Implementation -------------------------------------------------- */
  /* 'PBufPtr == NULL_PTR' is already checked in all calling API's */

  /* #10 Loop over all PBuf elements to check if PBuf is consistent */
  while (processedLength < firstElementTotLen)
  {
    retVal = EXI_E_INCONSISTEN_PBUF;
    /* #20 Segment size must not be set to 0 */
    if (currentPBufSegmentPtr->len == 0)
    {
    }
    /* #30 Payload pointer must not be set to NULL_PTR */
    else if (currentPBufSegmentPtr->payload == NULL_PTR)
    {
    }
    /* #40 Value of totLen must be the same for all segments */
    else if (firstElementTotLen != currentPBufSegmentPtr->totLen)
    {
    }
    /* #50 Size of all segements cannot be bigger than total length */
    else if ( currentPBufSegmentPtr->totLen < (processedLength + currentPBufSegmentPtr->len) )
    {
    }
    /* #60 Segment is OK */
    else
    {
      /* #70 Mark the current segment length as processed */
      processedLength += currentPBufSegmentPtr->len;
      /* #80 Check if switching to next segment is required */
      if (processedLength < firstElementTotLen)
      {
        /* #90 Increment the PBuf to the next element, its existence is ensured by adding all len elements up to the totLen element value */
        currentPBufSegmentPtr = &currentPBufSegmentPtr[1];
      }
      retVal = EXI_E_OK;
    }
    /* #100 Check if the current segment was OK */
    if (retVal != EXI_E_OK)
    {
      /* #110 If not: Report DET, end the loop and return error */
      Exi_CallInternalDetReportError(EXI_INSTANCE_ID, EXI_API_ID_V_INTERNAL_FUNCTION, EXI_E_INV_PARAM);
      break;
    }
  }

  return retVal;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */
#endif /* EXI_ENABLE_PBUF_SUPPORT */


#define EXI_STOP_SEC_CODE
#include "MemMap.h"  /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/* module specific MISRA deviations:
 MD_Exi_1.1_0857:
      Reason:     Number of macro definitions is huge but improves readability. (rule 0857)
      Risk:       Some compilers might have a problem with too many macro definitions.
      Prevention: Use only compilers supporting more than 1024 macro definitions.
 MD_Exi_1.1:
      Reason:     Nesting of control structures is exceeded because the generated state machine is based on XSD and EXI structure.
      Risk:       No risk.
      Prevention: Covered by code review.
 MD_Exi_1.1_0739_SwitchCase:
      Reason:     Switch case has more than 257 cases. This is required by underlaying schema.
      Risk:       Only compilers are supported that support more than 257 cases.
      Prevention: Covered by test runs with multiple compilers and Vector internal delivery process.
 MD_Exi_5.1:
      Reason:     Identifiers are derived from the XSD schema definition. Therefor some names do not differ within the first 31 characters.
      Risk:       No risk, this is generated code.
      Prevention: Not required.
 MD_Exi_5.6:
      Reason:     Elements with multiple occurences in the schema need to include a pointer to a structure from the same type. Therefor this typedef is required.
      Risk:       No risk, this is generated code.
      Prevention: Covered by code review.
 MD_Exi_5.6_0781:
      Reason:     Variable name 'errorId' is given by the Vector process, it is only used as local variable in the EXI module.
                  Usage in a struct is related to other modules.
      Risk:       No risk.
      Prevention: Covered by code review.
 MD_Exi_11.4:
      Reason:     EXI data is stored in the EXI workspace storage. Structures are placed inside this storage container and linked
                  using pointers to sub-structures. Casting to the required structure type is required here.
      Risk:       In all cases it has to be ensured, that the storage size is checked first and when placing structures inside
                  the EXI storage area the memory has to be cleared completely.
      Prevention: Covered by code review.
 MD_Exi_3305:
      Reason:     Structures are casted over an uint8 array.The uint8 Buffer has to be aligned by the EXI user and structure padding shall be
                  enabled in the compiler or optionally in the EXI configuration tool.
      Risk:       Miss-alignment exceptions.
      Prevention: Covered by code review of EXI user components (e.g. SCC).
 MD_Exi_13.6:
      Reason:     Control instruction modified inside the body of the loop to set the value to the current elements (multiple occurence element). This allows a max check
                  against the schema defined value.
      Risk:       If not followed by a break this could lead to an error.
      Prevention: Covered by code review.
 MD_Exi_13.7:
      Reason:     This condition is always true based on configuration.
      Risk:       No risk.
      Prevention: Covered by code review.
 MD_Exi_13.7_14.1_Enum:
      Reason:     This condition is always true. An enumeration value may not be out of its range, however it is possible. Since value may be read from bus systems,
                  there may communication errors and there for invalid values. The 'else' path of this 'if' statement is reported as unreachable.
      Risk:       No risk.
      Prevention: No prevention required, enumeration is just checked to have valied values only.
 MD_Exi_14.5:
      Reason:     Continue statement used when EE event already encoded.
      Risk:       No risk.
      Prevention: Covered by code review.
 MD_Exi_14.6_0771:
      Reason:     The break statement is used multiple times to prevent complex nesting of control statements. Unstructured code should be avoided.
      Risk:       No risk. If the break statement is removed an additional if-statement is required with an empty else block.
      Prevention: Covered by code review.
 MD_Exi_14.6_3333:
      Reason:     The break statement is used in an 'if' condition path, the else does contain a return. Due to code generation the content after the 'else'
                  path is used in multiple parts of the code that do not include this 'if' path.
      Risk:       No risk.
      Prevention: Covered by code review.
 MD_Exi_16.2:
      Reason:     Generic elements may include other generic elements. Therefor recursion is required for this elements.
      Risk:       No risk. Debugging code only. Schema deviation support shall be switched of in production code.
      Prevention: Covered by code review.
 MD_Exi_18.4:
      Reason:     XSD choice elements are represented as unions for optimization reason. Flags ensure that the application selects the correct type.
      Risk:       No risk, this is generated code.
      Prevention: Covered by code review.
 MD_Exi_19.11_3332:
      Reason:     QA-C MISRA checker does not recognize the pre-processor switch is checked for 'define' first and then for its value.
      Risk:       No Risk.
      Prevention: Covered by code review.
 MD_Exi_19.15_0883:
      Reason:     QA-C MISRA checker does not recognize the protection against multiple header include correctly.
      Risk:       No Risk.
      Prevention: Covered by code review.
 MD_Exi_21.1_0291:
      Reason:     Two unsigned integers are subtracted from each other 'BitCount - bitsRemain'. The function ensures
                  that bitsRemain is always smaller than or equal to BitCount.
      Risk:       Possible wrapp arround of the result.
      Prevention: Covered by code review.
 MD_Exi_21.1_3382:
      Reason:     Missing elements are signaled by their flag. In case an element is not present it is required to
                  decrement the event code by the amount of events of the missing element. The flags are previously
                  set according to the event code that is decremented here. There for the event code should never be
                  decremented below 0. Negative event codes are invalid, as well as there are no known schemata with
                  more than 2147483647 event codes. There for an event code below 0 would be threated as the same
                  error than an event code bigger 2147483647.
      Risk:       No risk, independent of the value (-1 or 4294967295) EXI will abort the decoding and report an error.
      Prevention: Covered by code review (make sure that there are no types in the schema with more than 2147483647
                  optional elements).
*/
/**********************************************************************************************************************
 *  END OF FILE: Exi.c
 *********************************************************************************************************************/
