/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Exi_SchemaEncoder.c
 *        \brief  Efficient XML Interchange general encoder source file
 *
 *      \details  Vector static code implementation for the Efficient XML Interchange sub-component general encoder.
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file Exi.h.
 *********************************************************************************************************************/
/* PRQA S 0857 EOF */ /* MD_Exi_1.1_0857 */ /* [L] Number of macro definitions exceeds 1024 - program is non-conforming. */

#define EXI_SCHEMA_ENCODER_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 EXI_SCHEMA_ENCODER_C_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "Exi_SchemaEncoder.h"
#include "Exi_BSEncoder.h"
/* PRQA L:EXI_SCHEMA_ENCODER_C_IF_NESTING */ /* MD_MSR_1.1_828 */
/**********************************************************************************************************************
*  VERSION CHECK
*********************************************************************************************************************/
#if ( (EXI_SW_MAJOR_VERSION != 6u) || (EXI_SW_MINOR_VERSION != 0u) || (EXI_SW_PATCH_VERSION != 1u) )
  #error "Vendor specific version numbers of Exi.h and Exi_SchemaEncoder.c are inconsistent"
#endif

/* PRQA S 0715 NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */ /* MD_Exi_1.1 */


#define EXI_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/**********************************************************************************************************************
 *  FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Exi_Encode_ID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ID) && (EXI_ENCODE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ID( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_IDType, AUTOMATIC, EXI_APPL_DATA) IDPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (IDPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (IDPtr->Length > sizeof(IDPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of ID */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode ID as string value */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &IDPtr->Buffer[0], IDPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ID) && (EXI_ENCODE_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_base64Binary
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_base64Binary( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_base64BinaryType, AUTOMATIC, EXI_APPL_DATA) base64BinaryPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (base64BinaryPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (base64BinaryPtr->Length > sizeof(base64BinaryPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of base64Binary */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode base64Binary as byte array */
    Exi_VBSEncodeBytes(&EncWsPtr->EncWs, &base64BinaryPtr->Buffer[0], base64BinaryPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_BASE64BINARY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_string
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_STRING) && (EXI_ENCODE_STRING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_string( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_stringType, AUTOMATIC, EXI_APPL_DATA) stringPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (stringPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (stringPtr->Length > sizeof(stringPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of string */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode string as string value */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &stringPtr->Buffer[0], stringPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_STRING, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_STRING) && (EXI_ENCODE_STRING == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_SchemaFragment
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_SCHEMA_FRAGMENT) && (EXI_ENCODE_SCHEMA_FRAGMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_SchemaFragment( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       Exi_NamespaceIdType Namespace )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EncWsPtr->InputData.RootElementId > EXI_SCHEMA_UNKNOWN_ELEMENT_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (Namespace > EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    #if (defined (EXI_ENCODE_SAP_SCHEMA_FRAGMENT) && (EXI_ENCODE_SAP_SCHEMA_FRAGMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #20 If schema SAP shall be encoded */
    if(EXI_SCHEMA_SET_SAP_TYPE == Namespace)
    {
      /* #30 Call schema specific encoder for SAP */
      Exi_Encode_SAP_SchemaFragment(EncWsPtr);
    }
    else
    #endif /* (defined (EXI_ENCODE_SAP_SCHEMA_FRAGMENT) && (EXI_ENCODE_SAP_SCHEMA_FRAGMENT == STD_ON)) */
    #if (defined (EXI_ENCODE_XMLSIG_SCHEMA_FRAGMENT) && (EXI_ENCODE_XMLSIG_SCHEMA_FRAGMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #40 If schema XMLSIG shall be encoded */
    if(EXI_SCHEMA_SET_XMLSIG_TYPE == Namespace)
    {
      /* #50 Call schema specific encoder for XMLSIG */
      Exi_Encode_XMLSIG_SchemaFragment(EncWsPtr);
    }
    else
    #endif /* (defined (EXI_ENCODE_XMLSIG_SCHEMA_FRAGMENT) && (EXI_ENCODE_XMLSIG_SCHEMA_FRAGMENT == STD_ON)) */
    #if (defined (EXI_ENCODE_DIN_SCHEMA_FRAGMENT) && (EXI_ENCODE_DIN_SCHEMA_FRAGMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #60 If schema DIN shall be encoded */
    if(EXI_SCHEMA_SET_DIN_TYPE == Namespace)
    {
      /* #70 Call schema specific encoder for DIN */
      Exi_Encode_DIN_SchemaFragment(EncWsPtr);
    }
    else
    #endif /* (defined (EXI_ENCODE_DIN_SCHEMA_FRAGMENT) && (EXI_ENCODE_DIN_SCHEMA_FRAGMENT == STD_ON)) */
    #if (defined (EXI_ENCODE_ISO_SCHEMA_FRAGMENT) && (EXI_ENCODE_ISO_SCHEMA_FRAGMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #80 If schema ISO shall be encoded */
    if(EXI_SCHEMA_SET_ISO_TYPE == Namespace)
    {
      /* #90 Call schema specific encoder for ISO */
      Exi_Encode_ISO_SchemaFragment(EncWsPtr);
    }
    else
    #endif /* (defined (EXI_ENCODE_ISO_SCHEMA_FRAGMENT) && (EXI_ENCODE_ISO_SCHEMA_FRAGMENT == STD_ON)) */
    #if (defined (EXI_ENCODE_ISO_ED2_DIS_SCHEMA_FRAGMENT) && (EXI_ENCODE_ISO_ED2_DIS_SCHEMA_FRAGMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #100 If schema ISO_ED2_DIS shall be encoded */
    if(EXI_SCHEMA_SET_ISO_ED2_DIS_TYPE == Namespace)
    {
      /* #110 Call schema specific encoder for ISO_ED2_DIS */
      Exi_Encode_ISO_ED2_DIS_SchemaFragment(EncWsPtr);
    }
    else
    #endif /* (defined (EXI_ENCODE_ISO_ED2_DIS_SCHEMA_FRAGMENT) && (EXI_ENCODE_ISO_ED2_DIS_SCHEMA_FRAGMENT == STD_ON)) */
    /* #120 Schema not supported */
    {
      /* #130 Set status code to error */
      EncWsPtr->EncWs.StatusCode = EXI_E_DISABLED_FEATURE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_SCHEMA_FRAGMENT) && (EXI_ENCODE_SCHEMA_FRAGMENT == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_SchemaRoot
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_SCHEMA_ROOT) && (EXI_ENCODE_SCHEMA_ROOT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_SchemaRoot( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  boolean encodeCalled = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EncWsPtr->InputData.RootElementId >= 278)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If root element ID is in range of schema DIN */
    if (
        (EncWsPtr->InputData.RootElementId <= EXI_DIN_WELDING_DETECTION_RES_TYPE)
       )
    {
      /* DIN */
      /* #30 Call schema specific root element encoder for schema DIN */
    #if (defined(EXI_ENCODE_DIN_SCHEMA_ROOT) && (EXI_ENCODE_DIN_SCHEMA_ROOT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_DIN_SchemaRoot(EncWsPtr);
      encodeCalled = TRUE;
    #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
    #endif
    }
    /* #40 If root element ID is in range of schema ISO */
    if (   (EncWsPtr->InputData.RootElementId >= EXI_ISO_AC_EVCHARGE_PARAMETER_TYPE)
        && (EncWsPtr->InputData.RootElementId <= EXI_ISO_WELDING_DETECTION_RES_TYPE)
       )
    {
      /* ISO */
      /* #50 Call schema specific root element encoder for schema ISO */
    #if (defined(EXI_ENCODE_ISO_SCHEMA_ROOT) && (EXI_ENCODE_ISO_SCHEMA_ROOT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_SchemaRoot(EncWsPtr);
      encodeCalled = TRUE;
    #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
    #endif
    }
    /* #60 If root element ID is in range of schema ISO_ED2_DIS */
    if (   (EncWsPtr->InputData.RootElementId >= EXI_ISO_ED2_DIS_ACBCREQ_CONTROL_MODE_TYPE)
        && (EncWsPtr->InputData.RootElementId <= EXI_ISO_ED2_DIS_WELDING_DETECTION_RES_TYPE)
       )
    {
      /* ISO_ED2_DIS */
      /* #70 Call schema specific root element encoder for schema ISO_ED2_DIS */
    #if (defined(EXI_ENCODE_ISO_ED2_DIS_SCHEMA_ROOT) && (EXI_ENCODE_ISO_ED2_DIS_SCHEMA_ROOT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_ED2_DIS_SchemaRoot(EncWsPtr);
      encodeCalled = TRUE;
    #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
    #endif
    }
    /* #80 If root element ID is in range of schema SAP */
    if (   (EncWsPtr->InputData.RootElementId >= EXI_SAP_SUPPORTED_APP_PROTOCOL_REQ_TYPE)
        && (EncWsPtr->InputData.RootElementId <= EXI_SAP_SUPPORTED_APP_PROTOCOL_RES_TYPE)
       )
    {
      /* SAP */
      /* #90 Call schema specific root element encoder for schema SAP */
    #if (defined(EXI_ENCODE_SAP_SCHEMA_ROOT) && (EXI_ENCODE_SAP_SCHEMA_ROOT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_SAP_SchemaRoot(EncWsPtr);
      encodeCalled = TRUE;
    #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
    #endif
    }
    /* #100 If root element ID is in range of schema XMLSIG */
    if (   (EncWsPtr->InputData.RootElementId >= EXI_XMLSIG_CANONICALIZATION_METHOD_TYPE)
    #if ( EXI_DEV_ERROR_DETECT == STD_OFF ) /* Already checked in DET if DET is enabled */
        && (EncWsPtr->InputData.RootElementId <= EXI_XMLSIG_X509DATA_TYPE)
    #endif
       )
    {
      /* XMLSIG */
      /* #110 Call schema specific root element encoder for schema XMLSIG */
    #if (defined(EXI_ENCODE_XMLSIG_SCHEMA_ROOT) && (EXI_ENCODE_XMLSIG_SCHEMA_ROOT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_SchemaRoot(EncWsPtr);
      encodeCalled = TRUE;
    #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
    #endif
    }
    /* #120 If no schema specific encode function was called */
    if ( (FALSE == encodeCalled) && (EXI_E_OK == EncWsPtr->EncWs.StatusCode) )
    {
      /* #130 Set status code to error */
      EncWsPtr->EncWs.StatusCode = EXI_E_INV_EVENT_CODE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_SCHEMA_ROOT) && (EXI_ENCODE_SCHEMA_ROOT == STD_ON)) */


#define EXI_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/* PRQA L:NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */

