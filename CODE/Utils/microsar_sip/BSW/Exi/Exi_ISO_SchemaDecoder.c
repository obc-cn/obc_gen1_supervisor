/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Exi_ISO_SchemaDecoder.c
 *        \brief  Efficient XML Interchange ISO decoder source file
 *
 *      \details  Vector static code implementation for the Efficient XML Interchange sub-component ISO decoder.
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file Exi.h.
 *********************************************************************************************************************/
/* PRQA S 0857 EOF */ /* MD_Exi_1.1_0857 */ /* [L] Number of macro definitions exceeds 1024 - program is non-conforming. */

#define EXI_ISO_SCHEMA_DECODER_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 EXI_ISO_SCHEMA_DECODER_C_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "Exi_ISO_SchemaDecoder.h"
#include "Exi_BSDecoder.h"
#include "Exi_Priv.h"
/* PRQA L:EXI_ISO_SCHEMA_DECODER_C_IF_NESTING */ /* MD_MSR_1.1_828 */
/**********************************************************************************************************************
*  VERSION CHECK
*********************************************************************************************************************/
#if ( (EXI_SW_MAJOR_VERSION != 6u) || (EXI_SW_MINOR_VERSION != 0u) || (EXI_SW_PATCH_VERSION != 1u) )
  #error "Vendor specific version numbers of Exi.h and Exi_ISO_SchemaDecoder.c are inconsistent"
#endif

#if (!defined (EXI_ENABLE_DECODE_ISO_MESSAGE_SET))
# if (defined (EXI_ENABLE_ISO_MESSAGE_SET))
#  define EXI_ENABLE_DECODE_ISO_MESSAGE_SET   EXI_ENABLE_ISO_MESSAGE_SET
# else
#  define EXI_ENABLE_DECODE_ISO_MESSAGE_SET   STD_OFF
# endif
#endif

#if (defined(EXI_ENABLE_DECODE_ISO_MESSAGE_SET) && (EXI_ENABLE_DECODE_ISO_MESSAGE_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */

/* PRQA S 0715 NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */ /* MD_Exi_1.1 */


#define EXI_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/**********************************************************************************************************************
 *  FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Exi_Decode_ISO_AC_EVChargeParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_AC_EVCHARGE_PARAMETER) && (EXI_DECODE_ISO_AC_EVCHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_AC_EVChargeParameter( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_AC_EVChargeParameterType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_AC_EVChargeParameterType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_AC_EVChargeParameterType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_AC_EVChargeParameterType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_AC_EVCHARGE_PARAMETER_TYPE);
    /* #40 Decode element DepartureTime */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(DepartureTime) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->DepartureTimeFlag = 1;
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->DepartureTime);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->DepartureTimeFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(DepartureTime) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #50 Decode element EAmount with missing elements in front */
    if(0 == structPtr->DepartureTimeFlag)
    {
      /* SE(EAmount) already decoded */
    #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EAmount));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_AC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #60 Decode element EAmount without missing elements in front */
    else
    {
      /* #70 Decode element EAmount */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(EAmount) */
      {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EAmount));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_AC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EAmount) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode element EVMaxVoltage */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVMaxVoltage) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVMaxVoltage));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_AC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVMaxVoltage) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode element EVMaxCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVMaxCurrent) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVMaxCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_AC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVMaxCurrent) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #100 Decode element EVMinCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVMinCurrent) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVMinCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_AC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVMinCurrent) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #110 If AC_EVChargeParameter was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(AC_EVChargeParameter) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_AC_EVCHARGE_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_AC_EVCHARGE_PARAMETER) && (EXI_DECODE_ISO_AC_EVCHARGE_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_AC_EVSEChargeParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_AC_EVSECHARGE_PARAMETER) && (EXI_DECODE_ISO_AC_EVSECHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_AC_EVSEChargeParameter( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_AC_EVSEChargeParameterType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_AC_EVSEChargeParameterType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_AC_EVSEChargeParameterType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_AC_EVSEChargeParameterType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_AC_EVSECHARGE_PARAMETER_TYPE);
    /* #40 Decode element AC_EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(AC_EVSEStatus) */
    {
      #if (defined(EXI_DECODE_ISO_AC_EVSESTATUS) && (EXI_DECODE_ISO_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_AC_EVSEStatus(DecWsPtr, &(structPtr->AC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_AC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_AC_EVSESTATUS) && (EXI_DECODE_ISO_AC_EVSESTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(AC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element EVSENominalVoltage */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSENominalVoltage) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSENominalVoltage));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_AC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSENominalVoltage) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVSEMaxCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMaxCurrent) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaxCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_AC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEMaxCurrent) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 If AC_EVSEChargeParameter was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(AC_EVSEChargeParameter) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_AC_EVSECHARGE_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_AC_EVSECHARGE_PARAMETER) && (EXI_DECODE_ISO_AC_EVSECHARGE_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_AC_EVSEStatus
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_AC_EVSESTATUS) && (EXI_DECODE_ISO_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_AC_EVSEStatus( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_AC_EVSEStatusType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_AC_EVSEStatusType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_AC_EVSEStatusType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_AC_EVSEStatusType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_AC_EVSESTATUS_TYPE);
    /* #40 Decode element NotificationMaxDelay */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(NotificationMaxDelay) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt16(&DecWsPtr->DecWs, &structPtr->NotificationMaxDelay);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(NotificationMaxDelay) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element EVSENotification */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSENotification) */
    {
      #if (defined(EXI_DECODE_ISO_EVSENOTIFICATION) && (EXI_DECODE_ISO_EVSENOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #60 Decode EVSENotification as enumeration value */
      Exi_Decode_ISO_EVSENotification(DecWsPtr, &structPtr->EVSENotification);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_AC_EVSESTATUS, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_EVSENOTIFICATION) && (EXI_DECODE_ISO_EVSENOTIFICATION == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSENotification) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element RCD */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(RCD) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->RCD);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(RCD) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 If AC_EVSEStatus was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(AC_EVSEStatus) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_AC_EVSESTATUS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_AC_EVSESTATUS) && (EXI_DECODE_ISO_AC_EVSESTATUS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_AttributeId
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_AttributeId( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_AttributeIdType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_AttributeIdType, AUTOMATIC, EXI_APPL_VAR) structPtr;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_AttributeIdType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_AttributeIdType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode AttributeId as string value */
    structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
    Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_ATTRIBUTE_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_AttributeName
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_ATTRIBUTE_NAME) && (EXI_DECODE_ISO_ATTRIBUTE_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_AttributeName( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_AttributeNameType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_AttributeNameType, AUTOMATIC, EXI_APPL_VAR) structPtr;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_AttributeNameType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_AttributeNameType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode AttributeName as string value */
    structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
    Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_ATTRIBUTE_NAME, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_ATTRIBUTE_NAME) && (EXI_DECODE_ISO_ATTRIBUTE_NAME == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_AuthorizationReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_AUTHORIZATION_REQ) && (EXI_DECODE_ISO_AUTHORIZATION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_AuthorizationReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_AuthorizationReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_AuthorizationReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_AuthorizationReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_AuthorizationReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_AUTHORIZATION_REQ_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, FALSE, TRUE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_AUTHORIZATION_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(GenChallenge) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->GenChallengeFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode optional element GenChallenge with missing elements in front */
    if((0 == structPtr->IdFlag) && (1 == structPtr->GenChallengeFlag))
    {
      /* SE(GenChallenge) already decoded */
    #if (defined(EXI_DECODE_ISO_GEN_CHALLENGE) && (EXI_DECODE_ISO_GEN_CHALLENGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_genChallenge(DecWsPtr, &(structPtr->GenChallenge));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_AUTHORIZATION_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_GEN_CHALLENGE) && (EXI_DECODE_ISO_GEN_CHALLENGE == STD_ON)) */
    }
    /* #60 Decode optional element GenChallenge without missing elements in front */
    else if(1 == structPtr->IdFlag)
    {
      /* #70 Decode element GenChallenge */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(GenChallenge) */
      {
      #if (defined(EXI_DECODE_ISO_GEN_CHALLENGE) && (EXI_DECODE_ISO_GEN_CHALLENGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_genChallenge(DecWsPtr, &(structPtr->GenChallenge));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->GenChallengeFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_AUTHORIZATION_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_GEN_CHALLENGE) && (EXI_DECODE_ISO_GEN_CHALLENGE == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->GenChallengeFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(GenChallenge) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #80 If AuthorizationReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->GenChallengeFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)/* EE(AuthorizationReq) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(AuthorizationReq) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_AUTHORIZATION_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_AUTHORIZATION_REQ) && (EXI_DECODE_ISO_AUTHORIZATION_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_AuthorizationRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_AUTHORIZATION_RES) && (EXI_DECODE_ISO_AUTHORIZATION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_AuthorizationRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_AuthorizationResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_AuthorizationResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_AuthorizationResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_AuthorizationResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_AUTHORIZATION_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_AUTHORIZATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVSEProcessing */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEProcessing) */
    {
      #if (defined(EXI_DECODE_ISO_EVSEPROCESSING) && (EXI_DECODE_ISO_EVSEPROCESSING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #70 Decode EVSEProcessing as enumeration value */
      Exi_Decode_ISO_EVSEProcessing(DecWsPtr, &structPtr->EVSEProcessing);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_AUTHORIZATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_EVSEPROCESSING) && (EXI_DECODE_ISO_EVSEPROCESSING == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEProcessing) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 If AuthorizationRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(AuthorizationRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_AUTHORIZATION_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_AUTHORIZATION_RES) && (EXI_DECODE_ISO_AUTHORIZATION_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_Body
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_BODY) && (EXI_DECODE_ISO_BODY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_Body( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_BodyType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_BodyType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_BodyType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_BodyType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode substitution group BodyElement */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 6);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 36, 6, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* #40 Swtich event code */
    switch(exiEventCode)
    {
    case 0: /* SE(AuthorizationReq) */
      /* #50 Element AuthorizationReq */
      {
        /* #60 Decode element AuthorizationReq */
      #if (defined(EXI_DECODE_ISO_AUTHORIZATION_REQ) && (EXI_DECODE_ISO_AUTHORIZATION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_AUTHORIZATION_REQ_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_AuthorizationReq(DecWsPtr, (Exi_ISO_AuthorizationReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_AUTHORIZATION_REQ) && (EXI_DECODE_ISO_AUTHORIZATION_REQ == STD_ON)) */
      }
    case 1: /* SE(AuthorizationRes) */
      /* #70 Element AuthorizationRes */
      {
        /* #80 Decode element AuthorizationRes */
      #if (defined(EXI_DECODE_ISO_AUTHORIZATION_RES) && (EXI_DECODE_ISO_AUTHORIZATION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_AUTHORIZATION_RES_TYPE;
        Exi_Decode_ISO_AuthorizationRes(DecWsPtr, (Exi_ISO_AuthorizationResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_AUTHORIZATION_RES) && (EXI_DECODE_ISO_AUTHORIZATION_RES == STD_ON)) */
      }
    /* case 2: SE(BodyElement) not required, element is abstract*/
    case 3: /* SE(CableCheckReq) */
      /* #90 Element CableCheckReq */
      {
        /* #100 Decode element CableCheckReq */
      #if (defined(EXI_DECODE_ISO_CABLE_CHECK_REQ) && (EXI_DECODE_ISO_CABLE_CHECK_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_CABLE_CHECK_REQ_TYPE;
        Exi_Decode_ISO_CableCheckReq(DecWsPtr, (Exi_ISO_CableCheckReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_CABLE_CHECK_REQ) && (EXI_DECODE_ISO_CABLE_CHECK_REQ == STD_ON)) */
      }
    case 4: /* SE(CableCheckRes) */
      /* #110 Element CableCheckRes */
      {
        /* #120 Decode element CableCheckRes */
      #if (defined(EXI_DECODE_ISO_CABLE_CHECK_RES) && (EXI_DECODE_ISO_CABLE_CHECK_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_CABLE_CHECK_RES_TYPE;
        Exi_Decode_ISO_CableCheckRes(DecWsPtr, (Exi_ISO_CableCheckResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_CABLE_CHECK_RES) && (EXI_DECODE_ISO_CABLE_CHECK_RES == STD_ON)) */
      }
    case 5: /* SE(CertificateInstallationReq) */
      /* #130 Element CertificateInstallationReq */
      {
        /* #140 Decode element CertificateInstallationReq */
      #if (defined(EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_REQ) && (EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_CERTIFICATE_INSTALLATION_REQ_TYPE;
        Exi_Decode_ISO_CertificateInstallationReq(DecWsPtr, (Exi_ISO_CertificateInstallationReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_REQ) && (EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_REQ == STD_ON)) */
      }
    case 6: /* SE(CertificateInstallationRes) */
      /* #150 Element CertificateInstallationRes */
      {
        /* #160 Decode element CertificateInstallationRes */
      #if (defined(EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_RES) && (EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_CERTIFICATE_INSTALLATION_RES_TYPE;
        Exi_Decode_ISO_CertificateInstallationRes(DecWsPtr, (Exi_ISO_CertificateInstallationResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_RES) && (EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_RES == STD_ON)) */
      }
    case 7: /* SE(CertificateUpdateReq) */
      /* #170 Element CertificateUpdateReq */
      {
        /* #180 Decode element CertificateUpdateReq */
      #if (defined(EXI_DECODE_ISO_CERTIFICATE_UPDATE_REQ) && (EXI_DECODE_ISO_CERTIFICATE_UPDATE_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_CERTIFICATE_UPDATE_REQ_TYPE;
        Exi_Decode_ISO_CertificateUpdateReq(DecWsPtr, (Exi_ISO_CertificateUpdateReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_UPDATE_REQ) && (EXI_DECODE_ISO_CERTIFICATE_UPDATE_REQ == STD_ON)) */
      }
    case 8: /* SE(CertificateUpdateRes) */
      /* #190 Element CertificateUpdateRes */
      {
        /* #200 Decode element CertificateUpdateRes */
      #if (defined(EXI_DECODE_ISO_CERTIFICATE_UPDATE_RES) && (EXI_DECODE_ISO_CERTIFICATE_UPDATE_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_CERTIFICATE_UPDATE_RES_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_CertificateUpdateRes(DecWsPtr, (Exi_ISO_CertificateUpdateResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_UPDATE_RES) && (EXI_DECODE_ISO_CERTIFICATE_UPDATE_RES == STD_ON)) */
      }
    case 9: /* SE(ChargeParameterDiscoveryReq) */
      /* #210 Element ChargeParameterDiscoveryReq */
      {
        /* #220 Decode element ChargeParameterDiscoveryReq */
      #if (defined(EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ) && (EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_CHARGE_PARAMETER_DISCOVERY_REQ_TYPE;
        Exi_Decode_ISO_ChargeParameterDiscoveryReq(DecWsPtr, (Exi_ISO_ChargeParameterDiscoveryReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ) && (EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ == STD_ON)) */
      }
    case 10: /* SE(ChargeParameterDiscoveryRes) */
      /* #230 Element ChargeParameterDiscoveryRes */
      {
        /* #240 Decode element ChargeParameterDiscoveryRes */
      #if (defined(EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES) && (EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_CHARGE_PARAMETER_DISCOVERY_RES_TYPE;
        Exi_Decode_ISO_ChargeParameterDiscoveryRes(DecWsPtr, (Exi_ISO_ChargeParameterDiscoveryResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES) && (EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES == STD_ON)) */
      }
    case 11: /* SE(ChargingStatusReq) */
      /* #250 Element ChargingStatusReq */
      {
        /* #260 Decode element ChargingStatusReq */
      #if (defined(EXI_DECODE_ISO_CHARGING_STATUS_REQ) && (EXI_DECODE_ISO_CHARGING_STATUS_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_CHARGING_STATUS_REQ_TYPE;
        structPtr->BodyElement = 0;
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_CHARGING_STATUS_REQ) && (EXI_DECODE_ISO_CHARGING_STATUS_REQ == STD_ON)) */
      }
    case 12: /* SE(ChargingStatusRes) */
      /* #270 Element ChargingStatusRes */
      {
        /* #280 Decode element ChargingStatusRes */
      #if (defined(EXI_DECODE_ISO_CHARGING_STATUS_RES) && (EXI_DECODE_ISO_CHARGING_STATUS_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_CHARGING_STATUS_RES_TYPE;
        Exi_Decode_ISO_ChargingStatusRes(DecWsPtr, (Exi_ISO_ChargingStatusResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_CHARGING_STATUS_RES) && (EXI_DECODE_ISO_CHARGING_STATUS_RES == STD_ON)) */
      }
    case 13: /* SE(CurrentDemandReq) */
      /* #290 Element CurrentDemandReq */
      {
        /* #300 Decode element CurrentDemandReq */
      #if (defined(EXI_DECODE_ISO_CURRENT_DEMAND_REQ) && (EXI_DECODE_ISO_CURRENT_DEMAND_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_CURRENT_DEMAND_REQ_TYPE;
        Exi_Decode_ISO_CurrentDemandReq(DecWsPtr, (Exi_ISO_CurrentDemandReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_CURRENT_DEMAND_REQ) && (EXI_DECODE_ISO_CURRENT_DEMAND_REQ == STD_ON)) */
      }
    case 14: /* SE(CurrentDemandRes) */
      /* #310 Element CurrentDemandRes */
      {
        /* #320 Decode element CurrentDemandRes */
      #if (defined(EXI_DECODE_ISO_CURRENT_DEMAND_RES) && (EXI_DECODE_ISO_CURRENT_DEMAND_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_CURRENT_DEMAND_RES_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_CurrentDemandRes(DecWsPtr, (Exi_ISO_CurrentDemandResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_CURRENT_DEMAND_RES) && (EXI_DECODE_ISO_CURRENT_DEMAND_RES == STD_ON)) */
      }
    case 15: /* SE(MeteringReceiptReq) */
      /* #330 Element MeteringReceiptReq */
      {
        /* #340 Decode element MeteringReceiptReq */
      #if (defined(EXI_DECODE_ISO_METERING_RECEIPT_REQ) && (EXI_DECODE_ISO_METERING_RECEIPT_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_METERING_RECEIPT_REQ_TYPE;
        Exi_Decode_ISO_MeteringReceiptReq(DecWsPtr, (Exi_ISO_MeteringReceiptReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_METERING_RECEIPT_REQ) && (EXI_DECODE_ISO_METERING_RECEIPT_REQ == STD_ON)) */
      }
    case 16: /* SE(MeteringReceiptRes) */
      /* #350 Element MeteringReceiptRes */
      {
        /* #360 Decode element MeteringReceiptRes */
      #if (defined(EXI_DECODE_ISO_METERING_RECEIPT_RES) && (EXI_DECODE_ISO_METERING_RECEIPT_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_METERING_RECEIPT_RES_TYPE;
        Exi_Decode_ISO_MeteringReceiptRes(DecWsPtr, (Exi_ISO_MeteringReceiptResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_METERING_RECEIPT_RES) && (EXI_DECODE_ISO_METERING_RECEIPT_RES == STD_ON)) */
      }
    case 17: /* SE(PaymentDetailsReq) */
      /* #370 Element PaymentDetailsReq */
      {
        /* #380 Decode element PaymentDetailsReq */
      #if (defined(EXI_DECODE_ISO_PAYMENT_DETAILS_REQ) && (EXI_DECODE_ISO_PAYMENT_DETAILS_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_PAYMENT_DETAILS_REQ_TYPE;
        Exi_Decode_ISO_PaymentDetailsReq(DecWsPtr, (Exi_ISO_PaymentDetailsReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PAYMENT_DETAILS_REQ) && (EXI_DECODE_ISO_PAYMENT_DETAILS_REQ == STD_ON)) */
      }
    case 18: /* SE(PaymentDetailsRes) */
      /* #390 Element PaymentDetailsRes */
      {
        /* #400 Decode element PaymentDetailsRes */
      #if (defined(EXI_DECODE_ISO_PAYMENT_DETAILS_RES) && (EXI_DECODE_ISO_PAYMENT_DETAILS_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_PAYMENT_DETAILS_RES_TYPE;
        Exi_Decode_ISO_PaymentDetailsRes(DecWsPtr, (Exi_ISO_PaymentDetailsResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PAYMENT_DETAILS_RES) && (EXI_DECODE_ISO_PAYMENT_DETAILS_RES == STD_ON)) */
      }
    case 19: /* SE(PaymentServiceSelectionReq) */
      /* #410 Element PaymentServiceSelectionReq */
      {
        /* #420 Decode element PaymentServiceSelectionReq */
      #if (defined(EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_REQ) && (EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_PAYMENT_SERVICE_SELECTION_REQ_TYPE;
        Exi_Decode_ISO_PaymentServiceSelectionReq(DecWsPtr, (Exi_ISO_PaymentServiceSelectionReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_REQ) && (EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_REQ == STD_ON)) */
      }
    case 20: /* SE(PaymentServiceSelectionRes) */
      /* #430 Element PaymentServiceSelectionRes */
      {
        /* #440 Decode element PaymentServiceSelectionRes */
      #if (defined(EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_RES) && (EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_PAYMENT_SERVICE_SELECTION_RES_TYPE;
        Exi_Decode_ISO_PaymentServiceSelectionRes(DecWsPtr, (Exi_ISO_PaymentServiceSelectionResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_RES) && (EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_RES == STD_ON)) */
      }
    case 21: /* SE(PowerDeliveryReq) */
      /* #450 Element PowerDeliveryReq */
      {
        /* #460 Decode element PowerDeliveryReq */
      #if (defined(EXI_DECODE_ISO_POWER_DELIVERY_REQ) && (EXI_DECODE_ISO_POWER_DELIVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_POWER_DELIVERY_REQ_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_PowerDeliveryReq(DecWsPtr, (Exi_ISO_PowerDeliveryReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_POWER_DELIVERY_REQ) && (EXI_DECODE_ISO_POWER_DELIVERY_REQ == STD_ON)) */
      }
    case 22: /* SE(PowerDeliveryRes) */
      /* #470 Element PowerDeliveryRes */
      {
        /* #480 Decode element PowerDeliveryRes */
      #if (defined(EXI_DECODE_ISO_POWER_DELIVERY_RES) && (EXI_DECODE_ISO_POWER_DELIVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_POWER_DELIVERY_RES_TYPE;
        Exi_Decode_ISO_PowerDeliveryRes(DecWsPtr, (Exi_ISO_PowerDeliveryResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_POWER_DELIVERY_RES) && (EXI_DECODE_ISO_POWER_DELIVERY_RES == STD_ON)) */
      }
    case 23: /* SE(PreChargeReq) */
      /* #490 Element PreChargeReq */
      {
        /* #500 Decode element PreChargeReq */
      #if (defined(EXI_DECODE_ISO_PRE_CHARGE_REQ) && (EXI_DECODE_ISO_PRE_CHARGE_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_PRE_CHARGE_REQ_TYPE;
        Exi_Decode_ISO_PreChargeReq(DecWsPtr, (Exi_ISO_PreChargeReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PRE_CHARGE_REQ) && (EXI_DECODE_ISO_PRE_CHARGE_REQ == STD_ON)) */
      }
    case 24: /* SE(PreChargeRes) */
      /* #510 Element PreChargeRes */
      {
        /* #520 Decode element PreChargeRes */
      #if (defined(EXI_DECODE_ISO_PRE_CHARGE_RES) && (EXI_DECODE_ISO_PRE_CHARGE_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_PRE_CHARGE_RES_TYPE;
        Exi_Decode_ISO_PreChargeRes(DecWsPtr, (Exi_ISO_PreChargeResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PRE_CHARGE_RES) && (EXI_DECODE_ISO_PRE_CHARGE_RES == STD_ON)) */
      }
    case 25: /* SE(ServiceDetailReq) */
      /* #530 Element ServiceDetailReq */
      {
        /* #540 Decode element ServiceDetailReq */
      #if (defined(EXI_DECODE_ISO_SERVICE_DETAIL_REQ) && (EXI_DECODE_ISO_SERVICE_DETAIL_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_SERVICE_DETAIL_REQ_TYPE;
        Exi_Decode_ISO_ServiceDetailReq(DecWsPtr, (Exi_ISO_ServiceDetailReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE_DETAIL_REQ) && (EXI_DECODE_ISO_SERVICE_DETAIL_REQ == STD_ON)) */
      }
    case 26: /* SE(ServiceDetailRes) */
      /* #550 Element ServiceDetailRes */
      {
        /* #560 Decode element ServiceDetailRes */
      #if (defined(EXI_DECODE_ISO_SERVICE_DETAIL_RES) && (EXI_DECODE_ISO_SERVICE_DETAIL_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_SERVICE_DETAIL_RES_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_ServiceDetailRes(DecWsPtr, (Exi_ISO_ServiceDetailResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE_DETAIL_RES) && (EXI_DECODE_ISO_SERVICE_DETAIL_RES == STD_ON)) */
      }
    case 27: /* SE(ServiceDiscoveryReq) */
      /* #570 Element ServiceDiscoveryReq */
      {
        /* #580 Decode element ServiceDiscoveryReq */
      #if (defined(EXI_DECODE_ISO_SERVICE_DISCOVERY_REQ) && (EXI_DECODE_ISO_SERVICE_DISCOVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_SERVICE_DISCOVERY_REQ_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_ServiceDiscoveryReq(DecWsPtr, (Exi_ISO_ServiceDiscoveryReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE_DISCOVERY_REQ) && (EXI_DECODE_ISO_SERVICE_DISCOVERY_REQ == STD_ON)) */
      }
    case 28: /* SE(ServiceDiscoveryRes) */
      /* #590 Element ServiceDiscoveryRes */
      {
        /* #600 Decode element ServiceDiscoveryRes */
      #if (defined(EXI_DECODE_ISO_SERVICE_DISCOVERY_RES) && (EXI_DECODE_ISO_SERVICE_DISCOVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_SERVICE_DISCOVERY_RES_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_ServiceDiscoveryRes(DecWsPtr, (Exi_ISO_ServiceDiscoveryResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE_DISCOVERY_RES) && (EXI_DECODE_ISO_SERVICE_DISCOVERY_RES == STD_ON)) */
      }
    case 29: /* SE(SessionSetupReq) */
      /* #610 Element SessionSetupReq */
      {
        /* #620 Decode element SessionSetupReq */
      #if (defined(EXI_DECODE_ISO_SESSION_SETUP_REQ) && (EXI_DECODE_ISO_SESSION_SETUP_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_SESSION_SETUP_REQ_TYPE;
        Exi_Decode_ISO_SessionSetupReq(DecWsPtr, (Exi_ISO_SessionSetupReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SESSION_SETUP_REQ) && (EXI_DECODE_ISO_SESSION_SETUP_REQ == STD_ON)) */
      }
    case 30: /* SE(SessionSetupRes) */
      /* #630 Element SessionSetupRes */
      {
        /* #640 Decode element SessionSetupRes */
      #if (defined(EXI_DECODE_ISO_SESSION_SETUP_RES) && (EXI_DECODE_ISO_SESSION_SETUP_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_SESSION_SETUP_RES_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_SessionSetupRes(DecWsPtr, (Exi_ISO_SessionSetupResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SESSION_SETUP_RES) && (EXI_DECODE_ISO_SESSION_SETUP_RES == STD_ON)) */
      }
    case 31: /* SE(SessionStopReq) */
      /* #650 Element SessionStopReq */
      {
        /* #660 Decode element SessionStopReq */
      #if (defined(EXI_DECODE_ISO_SESSION_STOP_REQ) && (EXI_DECODE_ISO_SESSION_STOP_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_SESSION_STOP_REQ_TYPE;
        Exi_Decode_ISO_SessionStopReq(DecWsPtr, (Exi_ISO_SessionStopReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SESSION_STOP_REQ) && (EXI_DECODE_ISO_SESSION_STOP_REQ == STD_ON)) */
      }
    case 32: /* SE(SessionStopRes) */
      /* #670 Element SessionStopRes */
      {
        /* #680 Decode element SessionStopRes */
      #if (defined(EXI_DECODE_ISO_SESSION_STOP_RES) && (EXI_DECODE_ISO_SESSION_STOP_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_SESSION_STOP_RES_TYPE;
        Exi_Decode_ISO_SessionStopRes(DecWsPtr, (Exi_ISO_SessionStopResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SESSION_STOP_RES) && (EXI_DECODE_ISO_SESSION_STOP_RES == STD_ON)) */
      }
    case 33: /* SE(WeldingDetectionReq) */
      /* #690 Element WeldingDetectionReq */
      {
        /* #700 Decode element WeldingDetectionReq */
      #if (defined(EXI_DECODE_ISO_WELDING_DETECTION_REQ) && (EXI_DECODE_ISO_WELDING_DETECTION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_WELDING_DETECTION_REQ_TYPE;
        Exi_Decode_ISO_WeldingDetectionReq(DecWsPtr, (Exi_ISO_WeldingDetectionReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_WELDING_DETECTION_REQ) && (EXI_DECODE_ISO_WELDING_DETECTION_REQ == STD_ON)) */
      }
    case 34: /* SE(WeldingDetectionRes) */
      /* #710 Element WeldingDetectionRes */
      {
        /* #720 Decode element WeldingDetectionRes */
      #if (defined(EXI_DECODE_ISO_WELDING_DETECTION_RES) && (EXI_DECODE_ISO_WELDING_DETECTION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_ISO_WELDING_DETECTION_RES_TYPE;
        Exi_Decode_ISO_WeldingDetectionRes(DecWsPtr, (Exi_ISO_WeldingDetectionResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_WELDING_DETECTION_RES) && (EXI_DECODE_ISO_WELDING_DETECTION_RES == STD_ON)) */
      }
    case 35: /* optional element not included */
      /* Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      break;
    default:
      /* #730 Unknown element */
      {
        {
          /* #740 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* End of Substitution Group */
    if(1 == structPtr->BodyElementFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_BODY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_BODY) && (EXI_DECODE_ISO_BODY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_CableCheckReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CABLE_CHECK_REQ) && (EXI_DECODE_ISO_CABLE_CHECK_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_CableCheckReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_CableCheckReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_CableCheckReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_CableCheckReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_CableCheckReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_CABLE_CHECK_REQ_TYPE);
    /* #40 Decode element DC_EVStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVStatus) */
    {
      #if (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_DC_EVStatus(DecWsPtr, &(structPtr->DC_EVStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CABLE_CHECK_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(DC_EVStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 If CableCheckReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(CableCheckReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CABLE_CHECK_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CABLE_CHECK_REQ) && (EXI_DECODE_ISO_CABLE_CHECK_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_CableCheckRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CABLE_CHECK_RES) && (EXI_DECODE_ISO_CABLE_CHECK_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_CableCheckRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_CableCheckResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_CableCheckResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_CableCheckResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_CableCheckResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_CABLE_CHECK_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CABLE_CHECK_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element DC_EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVSEStatus) */
    {
      #if (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_DC_EVSEStatus(DecWsPtr, &(structPtr->DC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CABLE_CHECK_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(DC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVSEProcessing */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEProcessing) */
    {
      #if (defined(EXI_DECODE_ISO_EVSEPROCESSING) && (EXI_DECODE_ISO_EVSEPROCESSING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #80 Decode EVSEProcessing as enumeration value */
      Exi_Decode_ISO_EVSEProcessing(DecWsPtr, &structPtr->EVSEProcessing);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CABLE_CHECK_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_EVSEPROCESSING) && (EXI_DECODE_ISO_EVSEPROCESSING == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEProcessing) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 If CableCheckRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(CableCheckRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CABLE_CHECK_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CABLE_CHECK_RES) && (EXI_DECODE_ISO_CABLE_CHECK_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_CertificateChain
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CERTIFICATE_CHAIN) && (EXI_DECODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_CertificateChain( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_CertificateChainType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_CertificateChainType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_CertificateChainType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_CertificateChainType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, TRUE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_CHAIN, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element Certificate with missing elements in front */
    if(0 == structPtr->IdFlag)
    {
      /* SE(Certificate) already decoded */
    #if (defined(EXI_DECODE_ISO_CERTIFICATE) && (EXI_DECODE_ISO_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_certificate(DecWsPtr, &(structPtr->Certificate));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_CHAIN, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE) && (EXI_DECODE_ISO_CERTIFICATE == STD_ON)) */
    }
    /* #50 Decode element Certificate without missing elements in front */
    else
    {
      /* #60 Decode element Certificate */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(Certificate) */
      {
      #if (defined(EXI_DECODE_ISO_CERTIFICATE) && (EXI_DECODE_ISO_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_certificate(DecWsPtr, &(structPtr->Certificate));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_CHAIN, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE) && (EXI_DECODE_ISO_CERTIFICATE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(Certificate) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element SubCertificates */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SubCertificates) */
    {
      #if (defined(EXI_DECODE_ISO_SUB_CERTIFICATES) && (EXI_DECODE_ISO_SUB_CERTIFICATES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_SubCertificates(DecWsPtr, &(structPtr->SubCertificates));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->SubCertificatesFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_CHAIN, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SUB_CERTIFICATES) && (EXI_DECODE_ISO_SUB_CERTIFICATES == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->SubCertificatesFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CERTIFICATE_CHAIN, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_CHAIN) && (EXI_DECODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_CertificateInstallationReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_REQ) && (EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_CertificateInstallationReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_CertificateInstallationReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_CertificateInstallationReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_CertificateInstallationReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_CertificateInstallationReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_CERTIFICATE_INSTALLATION_REQ_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_INSTALLATION_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element OEMProvisioningCert */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(OEMProvisioningCert) */
    {
      #if (defined(EXI_DECODE_ISO_CERTIFICATE) && (EXI_DECODE_ISO_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_certificate(DecWsPtr, &(structPtr->OEMProvisioningCert));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_INSTALLATION_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE) && (EXI_DECODE_ISO_CERTIFICATE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(OEMProvisioningCert) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element ListOfRootCertificateIDs */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ListOfRootCertificateIDs) */
    {
      #if (defined(EXI_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_ListOfRootCertificateIDs(DecWsPtr, &(structPtr->ListOfRootCertificateIDs));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_INSTALLATION_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #70 If CertificateInstallationReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(CertificateInstallationReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CERTIFICATE_INSTALLATION_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_REQ) && (EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_CertificateInstallationRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_RES) && (EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_CertificateInstallationRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_CertificateInstallationResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_CertificateInstallationResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_CertificateInstallationResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_CertificateInstallationResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_CERTIFICATE_INSTALLATION_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element SAProvisioningCertificateChain */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SAProvisioningCertificateChain) */
    {
      #if (defined(EXI_DECODE_ISO_CERTIFICATE_CHAIN) && (EXI_DECODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_CertificateChain(DecWsPtr, &(structPtr->SAProvisioningCertificateChain));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_CHAIN) && (EXI_DECODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #70 Decode element ContractSignatureCertChain */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ContractSignatureCertChain) */
    {
      #if (defined(EXI_DECODE_ISO_CERTIFICATE_CHAIN) && (EXI_DECODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_CertificateChain(DecWsPtr, &(structPtr->ContractSignatureCertChain));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_CHAIN) && (EXI_DECODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #80 Decode element ContractSignatureEncryptedPrivateKey */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ContractSignatureEncryptedPrivateKey) */
    {
      #if (defined(EXI_DECODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY) && (EXI_DECODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_ContractSignatureEncryptedPrivateKey(DecWsPtr, &(structPtr->ContractSignatureEncryptedPrivateKey));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY) && (EXI_DECODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ContractSignatureEncryptedPrivateKey) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode element DHpublickey */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(DHpublickey) */
    {
      #if (defined(EXI_DECODE_ISO_DIFFIE_HELLMAN_PUBLICKEY) && (EXI_DECODE_ISO_DIFFIE_HELLMAN_PUBLICKEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_DiffieHellmanPublickey(DecWsPtr, &(structPtr->DHpublickey));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_DIFFIE_HELLMAN_PUBLICKEY) && (EXI_DECODE_ISO_DIFFIE_HELLMAN_PUBLICKEY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(DHpublickey) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #100 Decode element eMAID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(eMAID) */
    {
      #if (defined(EXI_DECODE_ISO_EMAID) && (EXI_DECODE_ISO_EMAID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_EMAID(DecWsPtr, &(structPtr->eMAID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_EMAID) && (EXI_DECODE_ISO_EMAID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(eMAID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #110 If CertificateInstallationRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(CertificateInstallationRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CERTIFICATE_INSTALLATION_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_RES) && (EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_CertificateUpdateReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CERTIFICATE_UPDATE_REQ) && (EXI_DECODE_ISO_CERTIFICATE_UPDATE_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_CertificateUpdateReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_CertificateUpdateReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_CertificateUpdateReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_CertificateUpdateReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_CertificateUpdateReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_CERTIFICATE_UPDATE_REQ_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_UPDATE_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element ContractSignatureCertChain */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ContractSignatureCertChain) */
    {
      #if (defined(EXI_DECODE_ISO_CERTIFICATE_CHAIN) && (EXI_DECODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_CertificateChain(DecWsPtr, &(structPtr->ContractSignatureCertChain));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_UPDATE_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_CHAIN) && (EXI_DECODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #60 Decode element eMAID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(eMAID) */
    {
      #if (defined(EXI_DECODE_ISO_E_MAID) && (EXI_DECODE_ISO_E_MAID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_eMAID(DecWsPtr, &(structPtr->eMAID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_UPDATE_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_E_MAID) && (EXI_DECODE_ISO_E_MAID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(eMAID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element ListOfRootCertificateIDs */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ListOfRootCertificateIDs) */
    {
      #if (defined(EXI_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_ListOfRootCertificateIDs(DecWsPtr, &(structPtr->ListOfRootCertificateIDs));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_UPDATE_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #80 If CertificateUpdateReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(CertificateUpdateReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CERTIFICATE_UPDATE_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_UPDATE_REQ) && (EXI_DECODE_ISO_CERTIFICATE_UPDATE_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_CertificateUpdateRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CERTIFICATE_UPDATE_RES) && (EXI_DECODE_ISO_CERTIFICATE_UPDATE_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_CertificateUpdateRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_CertificateUpdateResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_CertificateUpdateResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_CertificateUpdateResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_CertificateUpdateResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_CERTIFICATE_UPDATE_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element SAProvisioningCertificateChain */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SAProvisioningCertificateChain) */
    {
      #if (defined(EXI_DECODE_ISO_CERTIFICATE_CHAIN) && (EXI_DECODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_CertificateChain(DecWsPtr, &(structPtr->SAProvisioningCertificateChain));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_CHAIN) && (EXI_DECODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #70 Decode element ContractSignatureCertChain */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ContractSignatureCertChain) */
    {
      #if (defined(EXI_DECODE_ISO_CERTIFICATE_CHAIN) && (EXI_DECODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_CertificateChain(DecWsPtr, &(structPtr->ContractSignatureCertChain));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_CHAIN) && (EXI_DECODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #80 Decode element ContractSignatureEncryptedPrivateKey */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ContractSignatureEncryptedPrivateKey) */
    {
      #if (defined(EXI_DECODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY) && (EXI_DECODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_ContractSignatureEncryptedPrivateKey(DecWsPtr, &(structPtr->ContractSignatureEncryptedPrivateKey));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY) && (EXI_DECODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ContractSignatureEncryptedPrivateKey) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode element DHpublickey */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(DHpublickey) */
    {
      #if (defined(EXI_DECODE_ISO_DIFFIE_HELLMAN_PUBLICKEY) && (EXI_DECODE_ISO_DIFFIE_HELLMAN_PUBLICKEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_DiffieHellmanPublickey(DecWsPtr, &(structPtr->DHpublickey));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_DIFFIE_HELLMAN_PUBLICKEY) && (EXI_DECODE_ISO_DIFFIE_HELLMAN_PUBLICKEY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(DHpublickey) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #100 Decode element eMAID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(eMAID) */
    {
      #if (defined(EXI_DECODE_ISO_EMAID) && (EXI_DECODE_ISO_EMAID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_EMAID(DecWsPtr, &(structPtr->eMAID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_EMAID) && (EXI_DECODE_ISO_EMAID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(eMAID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #110 Decode element RetryCounter */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(RetryCounter) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->RetryCounterFlag = 1;
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->RetryCounter);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->RetryCounterFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(RetryCounter) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #120 If CertificateUpdateRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->RetryCounterFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)/* EE(CertificateUpdateRes) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(CertificateUpdateRes) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CERTIFICATE_UPDATE_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_UPDATE_RES) && (EXI_DECODE_ISO_CERTIFICATE_UPDATE_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ChargeParameterDiscoveryReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ) && (EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ChargeParameterDiscoveryReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ChargeParameterDiscoveryReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ChargeParameterDiscoveryReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ChargeParameterDiscoveryReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ChargeParameterDiscoveryReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_CHARGE_PARAMETER_DISCOVERY_REQ_TYPE);
    /* #40 Decode element MaxEntriesSAScheduleTuple */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(MaxEntriesSAScheduleTuple) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->MaxEntriesSAScheduleTupleFlag = 1;
        Exi_VBSDecodeUInt16(&DecWsPtr->DecWs, &structPtr->MaxEntriesSAScheduleTuple);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->MaxEntriesSAScheduleTupleFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(MaxEntriesSAScheduleTuple) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #50 Decode element RequestedEnergyTransferMode with missing elements in front */
    if(0 == structPtr->MaxEntriesSAScheduleTupleFlag)
    {
      /* SE(RequestedEnergyTransferMode) already decoded */
    #if (defined(EXI_DECODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_DECODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* Decode enumeration value */
      Exi_Decode_ISO_EnergyTransferMode(DecWsPtr, &structPtr->RequestedEnergyTransferMode);
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_DECODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) */
    }
    /* #60 Decode element RequestedEnergyTransferMode without missing elements in front */
    else
    {
      /* #70 Decode element RequestedEnergyTransferMode */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(RequestedEnergyTransferMode) */
      {
      #if (defined(EXI_DECODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_DECODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* #80 Decode EnergyTransferMode as enumeration value */
        Exi_Decode_ISO_EnergyTransferMode(DecWsPtr, &structPtr->RequestedEnergyTransferMode);
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_DECODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(RequestedEnergyTransferMode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode substitution group EVChargeParameter */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* #100 Swtich event code */
    switch(exiEventCode)
    {
    case 0: /* SE(AC_EVChargeParameter) */
      /* #110 Element AC_EVChargeParameter */
      {
        /* #120 Decode element AC_EVChargeParameter */
      #if (defined(EXI_DECODE_ISO_AC_EVCHARGE_PARAMETER) && (EXI_DECODE_ISO_AC_EVCHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->EVChargeParameterElementId = EXI_ISO_AC_EVCHARGE_PARAMETER_TYPE;
        Exi_Decode_ISO_AC_EVChargeParameter(DecWsPtr, (Exi_ISO_AC_EVChargeParameterType**)&(structPtr->EVChargeParameter)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_AC_EVCHARGE_PARAMETER) && (EXI_DECODE_ISO_AC_EVCHARGE_PARAMETER == STD_ON)) */
      }
    case 1: /* SE(DC_EVChargeParameter) */
      /* #130 Element DC_EVChargeParameter */
      {
        /* #140 Decode element DC_EVChargeParameter */
      #if (defined(EXI_DECODE_ISO_DC_EVCHARGE_PARAMETER) && (EXI_DECODE_ISO_DC_EVCHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->EVChargeParameterElementId = EXI_ISO_DC_EVCHARGE_PARAMETER_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_DC_EVChargeParameter(DecWsPtr, (Exi_ISO_DC_EVChargeParameterType**)&(structPtr->EVChargeParameter)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVCHARGE_PARAMETER) && (EXI_DECODE_ISO_DC_EVCHARGE_PARAMETER == STD_ON)) */
      }
    /* case 2: SE(EVChargeParameter) not required, element is abstract*/
    default:
      /* #150 Unknown element */
      {
        {
          /* #160 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* End of Substitution Group */
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #170 If ChargeParameterDiscoveryReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(ChargeParameterDiscoveryReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ) && (EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ChargeParameterDiscoveryRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES) && (EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ChargeParameterDiscoveryRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ChargeParameterDiscoveryResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ChargeParameterDiscoveryResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ChargeParameterDiscoveryResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ChargeParameterDiscoveryResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_CHARGE_PARAMETER_DISCOVERY_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVSEProcessing */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEProcessing) */
    {
      #if (defined(EXI_DECODE_ISO_EVSEPROCESSING) && (EXI_DECODE_ISO_EVSEPROCESSING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #70 Decode EVSEProcessing as enumeration value */
      Exi_Decode_ISO_EVSEProcessing(DecWsPtr, &structPtr->EVSEProcessing);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_EVSEPROCESSING) && (EXI_DECODE_ISO_EVSEPROCESSING == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEProcessing) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode substitution group SASchedules */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 5, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* #90 Swtich event code */
    switch(exiEventCode)
    {
    case 0: /* SE(SAScheduleList) */
      /* #100 Element SAScheduleList */
      {
        /* #110 Decode element SAScheduleList */
      #if (defined(EXI_DECODE_ISO_SASCHEDULE_LIST) && (EXI_DECODE_ISO_SASCHEDULE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->SASchedulesElementId = EXI_ISO_SASCHEDULE_LIST_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_SAScheduleList(DecWsPtr, (Exi_ISO_SAScheduleListType**)&(structPtr->SASchedules)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->SASchedulesFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SASCHEDULE_LIST) && (EXI_DECODE_ISO_SASCHEDULE_LIST == STD_ON)) */
      }
    /* case 1: SE(SASchedules) not required, element is abstract*/
    default:
      /* #120 Unknown element */
      {
        /* #130 Check optional substitution members */
        if(exiEventCode < 5) /* optional element not included */
        {
          /* Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
          break; /* PRQA S 3333 */ /* MD_Exi_14.6_3333 */
        }
        else
        {
          /* #140 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* End of Substitution Group */
    if(1 == structPtr->SASchedulesFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    /* #150 Decode substitution group EVSEChargeParameter with missing elements in front */
    if(0 == structPtr->SASchedulesFlag)
    {
      /* Start of Substitution Group with missing elements in front */
      /* #160 Decrement event code by the amount of missing elements to get a zero based event code */
      exiEventCode -= 2u; /* Element: SASchedules */ /* PRQA S 3382 */ /* MD_Exi_21.1_3382 */
      /* #170 Switch event code */
      switch(exiEventCode)
      {
      case 0: /* SE(AC_EVSEChargeParameter) */
        /* #180 Element AC_EVSEChargeParameter */
        {
          /* #190 Decode element AC_EVSEChargeParameter */
        #if (defined(EXI_DECODE_ISO_AC_EVSECHARGE_PARAMETER) && (EXI_DECODE_ISO_AC_EVSECHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          structPtr->EVSEChargeParameterElementId = EXI_ISO_AC_EVSECHARGE_PARAMETER_TYPE;
          Exi_Decode_ISO_AC_EVSEChargeParameter(DecWsPtr, (Exi_ISO_AC_EVSEChargeParameterType**)&(structPtr->EVSEChargeParameter)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_ISO_AC_EVSECHARGE_PARAMETER) && (EXI_DECODE_ISO_AC_EVSECHARGE_PARAMETER == STD_ON)) */
        }
      case 1: /* SE(DC_EVSEChargeParameter) */
        /* #200 Element DC_EVSEChargeParameter */
        {
          /* #210 Decode element DC_EVSEChargeParameter */
        #if (defined(EXI_DECODE_ISO_DC_EVSECHARGE_PARAMETER) && (EXI_DECODE_ISO_DC_EVSECHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          structPtr->EVSEChargeParameterElementId = EXI_ISO_DC_EVSECHARGE_PARAMETER_TYPE;
          DecWsPtr->DecWs.EERequired = TRUE;
          Exi_Decode_ISO_DC_EVSEChargeParameter(DecWsPtr, (Exi_ISO_DC_EVSEChargeParameterType**)&(structPtr->EVSEChargeParameter)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_ISO_DC_EVSECHARGE_PARAMETER) && (EXI_DECODE_ISO_DC_EVSECHARGE_PARAMETER == STD_ON)) */
        }
      /* case 2: SE(EVSEChargeParameter) not required, element is abstract*/
      default:
        /* #220 Unknown substitution element */
        {
          /* #230 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* #240 Decode substitution group EVSEChargeParameter without missing elements in front */
    else
    {
      /* Start of Substitution Group without missing elements in front */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* #250 Swtich event code */
      switch(exiEventCode)
      {
      case 0: /* SE(AC_EVSEChargeParameter) */
        /* #260 Element AC_EVSEChargeParameter */
        {
          /* #270 Decode element AC_EVSEChargeParameter */
        #if (defined(EXI_DECODE_ISO_AC_EVSECHARGE_PARAMETER) && (EXI_DECODE_ISO_AC_EVSECHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          structPtr->EVSEChargeParameterElementId = EXI_ISO_AC_EVSECHARGE_PARAMETER_TYPE;
          Exi_Decode_ISO_AC_EVSEChargeParameter(DecWsPtr, (Exi_ISO_AC_EVSEChargeParameterType**)&(structPtr->EVSEChargeParameter)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_ISO_AC_EVSECHARGE_PARAMETER) && (EXI_DECODE_ISO_AC_EVSECHARGE_PARAMETER == STD_ON)) */
        }
      case 1: /* SE(DC_EVSEChargeParameter) */
        /* #280 Element DC_EVSEChargeParameter */
        {
          /* #290 Decode element DC_EVSEChargeParameter */
        #if (defined(EXI_DECODE_ISO_DC_EVSECHARGE_PARAMETER) && (EXI_DECODE_ISO_DC_EVSECHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          structPtr->EVSEChargeParameterElementId = EXI_ISO_DC_EVSECHARGE_PARAMETER_TYPE;
          DecWsPtr->DecWs.EERequired = TRUE;
          Exi_Decode_ISO_DC_EVSEChargeParameter(DecWsPtr, (Exi_ISO_DC_EVSEChargeParameterType**)&(structPtr->EVSEChargeParameter)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_ISO_DC_EVSECHARGE_PARAMETER) && (EXI_DECODE_ISO_DC_EVSECHARGE_PARAMETER == STD_ON)) */
        }
      /* case 2: SE(EVSEChargeParameter) not required, element is abstract*/
      default:
        /* #300 Unknown element */
        {
          {
            /* #310 Set status code to error */
            /* unsupported event code */
            Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
            return;
          }
        }
      }
    /* End of Substitution Group */
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #320 If ChargeParameterDiscoveryRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(ChargeParameterDiscoveryRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES) && (EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ChargeService
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CHARGE_SERVICE) && (EXI_DECODE_ISO_CHARGE_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ChargeService( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ChargeServiceType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ChargeServiceType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ChargeServiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ChargeServiceType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element ServiceID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ServiceID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt16(&DecWsPtr->DecWs, &structPtr->ServiceID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ServiceID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element ServiceName */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ServiceName) */
    {
      #if (defined(EXI_DECODE_ISO_SERVICE_NAME) && (EXI_DECODE_ISO_SERVICE_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_serviceName(DecWsPtr, &(structPtr->ServiceName));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ServiceNameFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_SERVICE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE_NAME) && (EXI_DECODE_ISO_SERVICE_NAME == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ServiceNameFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(ServiceName) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #50 Decode element ServiceCategory with missing elements in front */
    if(0 == structPtr->ServiceNameFlag)
    {
      /* SE(ServiceCategory) already decoded */
    #if (defined(EXI_DECODE_ISO_SERVICE_CATEGORY) && (EXI_DECODE_ISO_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* Decode enumeration value */
      Exi_Decode_ISO_serviceCategory(DecWsPtr, &structPtr->ServiceCategory);
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_SERVICE, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_SERVICE_CATEGORY) && (EXI_DECODE_ISO_SERVICE_CATEGORY == STD_ON)) */
    }
    /* #60 Decode element ServiceCategory without missing elements in front */
    else
    {
      /* #70 Decode element ServiceCategory */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(ServiceCategory) */
      {
      #if (defined(EXI_DECODE_ISO_SERVICE_CATEGORY) && (EXI_DECODE_ISO_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* #80 Decode serviceCategory as enumeration value */
        Exi_Decode_ISO_serviceCategory(DecWsPtr, &structPtr->ServiceCategory);
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_SERVICE, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE_CATEGORY) && (EXI_DECODE_ISO_SERVICE_CATEGORY == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ServiceCategory) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode element ServiceScope */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ServiceScope) */
    {
      #if (defined(EXI_DECODE_ISO_SERVICE_SCOPE) && (EXI_DECODE_ISO_SERVICE_SCOPE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_serviceScope(DecWsPtr, &(structPtr->ServiceScope));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ServiceScopeFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_SERVICE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE_SCOPE) && (EXI_DECODE_ISO_SERVICE_SCOPE == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ServiceScopeFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(ServiceScope) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #100 Decode element FreeService with missing elements in front */
    if(0 == structPtr->ServiceScopeFlag)
    {
      /* SE(FreeService) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->FreeService);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #110 Decode element FreeService without missing elements in front */
    else
    {
      /* #120 Decode element FreeService */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(FreeService) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)
        {
          Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->FreeService);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(FreeService) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #130 Decode element SupportedEnergyTransferMode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SupportedEnergyTransferMode) */
    {
      #if (defined(EXI_DECODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE) && (EXI_DECODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_SupportedEnergyTransferMode(DecWsPtr, &(structPtr->SupportedEnergyTransferMode));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGE_SERVICE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE) && (EXI_DECODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CHARGE_SERVICE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CHARGE_SERVICE) && (EXI_DECODE_ISO_CHARGE_SERVICE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ChargingProfile
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CHARGING_PROFILE) && (EXI_DECODE_ISO_CHARGING_PROFILE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ChargingProfile( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ChargingProfileType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ChargingProfileType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_ProfileEntryType) lastProfileEntry;
  #if (defined(EXI_DECODE_ISO_PROFILE_ENTRY) && (EXI_DECODE_ISO_PROFILE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  uint16_least i;
  #endif /* #if (defined(EXI_DECODE_ISO_PROFILE_ENTRY) && (EXI_DECODE_ISO_PROFILE_ENTRY == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ChargingProfileType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ChargingProfileType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element ProfileEntry, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ProfileEntry) */
    {
      #if (defined(EXI_DECODE_ISO_PROFILE_ENTRY) && (EXI_DECODE_ISO_PROFILE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_ProfileEntry(DecWsPtr, &(structPtr->ProfileEntry));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGING_PROFILE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PROFILE_ENTRY) && (EXI_DECODE_ISO_PROFILE_ENTRY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->ProfileEntry->NextProfileEntryPtr = (Exi_ISO_ProfileEntryType*)NULL_PTR;
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastProfileEntry = structPtr->ProfileEntry;
    #if (defined(EXI_DECODE_ISO_PROFILE_ENTRY) && (EXI_DECODE_ISO_PROFILE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #40 Decode subsequent occurences of ProfileEntry */
    for(i=0; i<23; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* SE(ProfileEntry) */
      {
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_ProfileEntry(DecWsPtr, &(lastProfileEntry->NextProfileEntryPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastProfileEntry = lastProfileEntry->NextProfileEntryPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(ProfileEntry) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(ProfileEntry) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* (defined(EXI_DECODE_ISO_PROFILE_ENTRY) && (EXI_DECODE_ISO_PROFILE_ENTRY == STD_ON)) */
    lastProfileEntry->NextProfileEntryPtr = (Exi_ISO_ProfileEntryType*)NULL_PTR;

  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CHARGING_PROFILE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CHARGING_PROFILE) && (EXI_DECODE_ISO_CHARGING_PROFILE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ChargingStatusRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CHARGING_STATUS_RES) && (EXI_DECODE_ISO_CHARGING_STATUS_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ChargingStatusRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ChargingStatusResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ChargingStatusResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ChargingStatusResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ChargingStatusResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_CHARGING_STATUS_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVSEID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEID) */
    {
      #if (defined(EXI_DECODE_ISO_EVSE_ID) && (EXI_DECODE_ISO_EVSE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_evseID(DecWsPtr, &(structPtr->EVSEID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_EVSE_ID) && (EXI_DECODE_ISO_EVSE_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element SAScheduleTupleID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SAScheduleTupleID) */
    {
      Exi_BitBufType value = 0;
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 8);
        structPtr->SAScheduleTupleID = (uint8)(value + 1);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(SAScheduleTupleID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode element EVSEMaxCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 4, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMaxCurrent) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaxCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVSEMaxCurrentFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(MeterInfo) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->MeterInfoFlag = 1;
    }
    else if(exiEventCode == 2) /* SE(ReceiptRequired) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->ReceiptRequiredFlag = 1;
    }
    else if(exiEventCode == 3)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVSEMaxCurrentFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EVSEMaxCurrent) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #90 Decode optional element MeterInfo with missing elements in front */
    if((0 == structPtr->EVSEMaxCurrentFlag) && (1 == structPtr->MeterInfoFlag))
    {
      /* SE(MeterInfo) already decoded */
    #if (defined(EXI_DECODE_ISO_METER_INFO) && (EXI_DECODE_ISO_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_MeterInfo(DecWsPtr, &(structPtr->MeterInfo));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_METER_INFO) && (EXI_DECODE_ISO_METER_INFO == STD_ON)) */
    }
    /* #100 Decode optional element MeterInfo without missing elements in front */
    else if(1 == structPtr->EVSEMaxCurrentFlag)
    {
      /* #110 Decode element MeterInfo */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(MeterInfo) */
      {
      #if (defined(EXI_DECODE_ISO_METER_INFO) && (EXI_DECODE_ISO_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_MeterInfo(DecWsPtr, &(structPtr->MeterInfo));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->MeterInfoFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_METER_INFO) && (EXI_DECODE_ISO_METER_INFO == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(ReceiptRequired) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->ReceiptRequiredFlag = 1;
      }
      else if(exiEventCode == 2)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->MeterInfoFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    /* #120 Decode optional element ReceiptRequired with missing elements in front */
    if((0 == structPtr->MeterInfoFlag) && (1 == structPtr->ReceiptRequiredFlag))
    {
      /* SE(ReceiptRequired) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ReceiptRequired);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #130 Decode optional element ReceiptRequired without missing elements in front */
    else if(1 == structPtr->MeterInfoFlag)
    {
      /* #140 Decode element ReceiptRequired */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(ReceiptRequired) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)
        {
          structPtr->ReceiptRequiredFlag = 1;
          Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ReceiptRequired);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->ReceiptRequiredFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(ReceiptRequired) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #150 Decode element AC_EVSEStatus with missing elements in front */
    if(0 == structPtr->ReceiptRequiredFlag)
    {
      /* SE(AC_EVSEStatus) already decoded */
    #if (defined(EXI_DECODE_ISO_AC_EVSESTATUS) && (EXI_DECODE_ISO_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_AC_EVSEStatus(DecWsPtr, &(structPtr->AC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_AC_EVSESTATUS) && (EXI_DECODE_ISO_AC_EVSESTATUS == STD_ON)) */
    }
    /* #160 Decode element AC_EVSEStatus without missing elements in front */
    else
    {
      /* #170 Decode element AC_EVSEStatus */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(AC_EVSEStatus) */
      {
      #if (defined(EXI_DECODE_ISO_AC_EVSESTATUS) && (EXI_DECODE_ISO_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_AC_EVSEStatus(DecWsPtr, &(structPtr->AC_EVSEStatus));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_AC_EVSESTATUS) && (EXI_DECODE_ISO_AC_EVSESTATUS == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(AC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #180 If ChargingStatusRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(ChargingStatusRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CHARGING_STATUS_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CHARGING_STATUS_RES) && (EXI_DECODE_ISO_CHARGING_STATUS_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ConsumptionCost
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CONSUMPTION_COST) && (EXI_DECODE_ISO_CONSUMPTION_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ConsumptionCost( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ConsumptionCostType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ConsumptionCostType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_CostType) lastCost;
  #if (defined(EXI_DECODE_ISO_COST) && (EXI_DECODE_ISO_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  uint16_least i;
  #endif /* #if (defined(EXI_DECODE_ISO_COST) && (EXI_DECODE_ISO_COST == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ConsumptionCostType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ConsumptionCostType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element startValue */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(startValue) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->startValue));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CONSUMPTION_COST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(startValue) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode multi occurence element Cost, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(Cost) */
    {
      #if (defined(EXI_DECODE_ISO_COST) && (EXI_DECODE_ISO_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_Cost(DecWsPtr, &(structPtr->Cost));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CONSUMPTION_COST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_COST) && (EXI_DECODE_ISO_COST == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->Cost->NextCostPtr = (Exi_ISO_CostType*)NULL_PTR;
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastCost = structPtr->Cost;
    #if (defined(EXI_DECODE_ISO_COST) && (EXI_DECODE_ISO_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #50 Decode subsequent occurences of Cost */
    for(i=0; i<2; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* SE(Cost) */
      {
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_Cost(DecWsPtr, &(lastCost->NextCostPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastCost = lastCost->NextCostPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(Cost) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(Cost) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* (defined(EXI_DECODE_ISO_COST) && (EXI_DECODE_ISO_COST == STD_ON)) */
    lastCost->NextCostPtr = (Exi_ISO_CostType*)NULL_PTR;

  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CONSUMPTION_COST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CONSUMPTION_COST) && (EXI_DECODE_ISO_CONSUMPTION_COST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ContractSignatureEncryptedPrivateKey
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY) && (EXI_DECODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ContractSignatureEncryptedPrivateKey( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ContractSignatureEncryptedPrivateKeyType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ContractSignatureEncryptedPrivateKeyType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ContractSignatureEncryptedPrivateKeyType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ContractSignatureEncryptedPrivateKeyType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_ATTRIBUTE_ID, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #40 Decode ContractSignatureEncryptedPrivateKey as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY) && (EXI_DECODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_Cost
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_COST) && (EXI_DECODE_ISO_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_Cost( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_CostType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_CostType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_CostType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_CostType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element costKind */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(costKind) */
    {
      #if (defined(EXI_DECODE_ISO_COST_KIND) && (EXI_DECODE_ISO_COST_KIND == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #40 Decode costKind as enumeration value */
      Exi_Decode_ISO_costKind(DecWsPtr, &structPtr->costKind);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_COST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_COST_KIND) && (EXI_DECODE_ISO_COST_KIND == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(costKind) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element amount */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(amount) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->amount);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(amount) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element amountMultiplier */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(amountMultiplier) */
    {
      Exi_BitBufType value = 0;
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->amountMultiplierFlag = 1;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 3);
        structPtr->amountMultiplier = (sint8)(value - 3);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->amountMultiplierFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(amountMultiplier) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_COST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_COST) && (EXI_DECODE_ISO_COST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_CurrentDemandReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CURRENT_DEMAND_REQ) && (EXI_DECODE_ISO_CURRENT_DEMAND_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_CurrentDemandReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_CurrentDemandReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_CurrentDemandReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_CurrentDemandReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_CurrentDemandReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_CURRENT_DEMAND_REQ_TYPE);
    /* #40 Decode element DC_EVStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVStatus) */
    {
      #if (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_DC_EVStatus(DecWsPtr, &(structPtr->DC_EVStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(DC_EVStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element EVTargetCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVTargetCurrent) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVTargetCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVTargetCurrent) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVMaximumVoltageLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 5, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVMaximumVoltageLimit) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumVoltageLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVMaximumVoltageLimitFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(EVMaximumCurrentLimit) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->EVMaximumCurrentLimitFlag = 1;
    }
    else if(exiEventCode == 2) /* SE(EVMaximumPowerLimit) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->EVMaximumPowerLimitFlag = 1;
    }
    else if(exiEventCode == 3) /* SE(BulkChargingComplete) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->BulkChargingCompleteFlag = 1;
    }
    else if(exiEventCode == 4)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVMaximumVoltageLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EVMaximumVoltageLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #70 Decode optional element EVMaximumCurrentLimit with missing elements in front */
    if((0 == structPtr->EVMaximumVoltageLimitFlag) && (1 == structPtr->EVMaximumCurrentLimitFlag))
    {
      /* SE(EVMaximumCurrentLimit) already decoded */
    #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumCurrentLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #80 Decode optional element EVMaximumCurrentLimit without missing elements in front */
    else if(1 == structPtr->EVMaximumVoltageLimitFlag)
    {
      /* #90 Decode element EVMaximumCurrentLimit */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 4, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(EVMaximumCurrentLimit) */
      {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumCurrentLimit));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->EVMaximumCurrentLimitFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(EVMaximumPowerLimit) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->EVMaximumPowerLimitFlag = 1;
      }
      else if(exiEventCode == 2) /* SE(BulkChargingComplete) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->BulkChargingCompleteFlag = 1;
      }
      else if(exiEventCode == 3)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->EVMaximumCurrentLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EVMaximumCurrentLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #100 Decode optional element EVMaximumPowerLimit with missing elements in front */
    if((0 == structPtr->EVMaximumCurrentLimitFlag) && (1 == structPtr->EVMaximumPowerLimitFlag))
    {
      /* SE(EVMaximumPowerLimit) already decoded */
    #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumPowerLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #110 Decode optional element EVMaximumPowerLimit without missing elements in front */
    else if(1 == structPtr->EVMaximumCurrentLimitFlag)
    {
      /* #120 Decode element EVMaximumPowerLimit */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(EVMaximumPowerLimit) */
      {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumPowerLimit));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->EVMaximumPowerLimitFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(BulkChargingComplete) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->BulkChargingCompleteFlag = 1;
      }
      else if(exiEventCode == 2)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->EVMaximumPowerLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EVMaximumPowerLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #130 Decode optional element BulkChargingComplete with missing elements in front */
    if((0 == structPtr->EVMaximumPowerLimitFlag) && (1 == structPtr->BulkChargingCompleteFlag))
    {
      /* SE(BulkChargingComplete) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->BulkChargingComplete);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #140 Decode optional element BulkChargingComplete without missing elements in front */
    else if(1 == structPtr->EVMaximumPowerLimitFlag)
    {
      /* #150 Decode element BulkChargingComplete */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(BulkChargingComplete) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)
        {
          structPtr->BulkChargingCompleteFlag = 1;
          Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->BulkChargingComplete);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->BulkChargingCompleteFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(BulkChargingComplete) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #160 Decode element ChargingComplete with missing elements in front */
    if(0 == structPtr->BulkChargingCompleteFlag)
    {
      /* SE(ChargingComplete) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ChargingComplete);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #170 Decode element ChargingComplete without missing elements in front */
    else
    {
      /* #180 Decode element ChargingComplete */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(ChargingComplete) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)
        {
          Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ChargingComplete);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ChargingComplete) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #190 Decode element RemainingTimeToFullSoC */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(RemainingTimeToFullSoC) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->RemainingTimeToFullSoC));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->RemainingTimeToFullSoCFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(RemainingTimeToBulkSoC) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->RemainingTimeToBulkSoCFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->RemainingTimeToFullSoCFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(RemainingTimeToFullSoC) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #200 Decode optional element RemainingTimeToBulkSoC with missing elements in front */
    if((0 == structPtr->RemainingTimeToFullSoCFlag) && (1 == structPtr->RemainingTimeToBulkSoCFlag))
    {
      /* SE(RemainingTimeToBulkSoC) already decoded */
    #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->RemainingTimeToBulkSoC));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #210 Decode optional element RemainingTimeToBulkSoC without missing elements in front */
    else if(1 == structPtr->RemainingTimeToFullSoCFlag)
    {
      /* #220 Decode element RemainingTimeToBulkSoC */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(RemainingTimeToBulkSoC) */
      {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->RemainingTimeToBulkSoC));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->RemainingTimeToBulkSoCFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->RemainingTimeToBulkSoCFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(RemainingTimeToBulkSoC) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #230 Decode element EVTargetVoltage with missing elements in front */
    if(0 == structPtr->RemainingTimeToBulkSoCFlag)
    {
      /* SE(EVTargetVoltage) already decoded */
    #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVTargetVoltage));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #240 Decode element EVTargetVoltage without missing elements in front */
    else
    {
      /* #250 Decode element EVTargetVoltage */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(EVTargetVoltage) */
      {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVTargetVoltage));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVTargetVoltage) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #260 If CurrentDemandReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(CurrentDemandReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CURRENT_DEMAND_REQ) && (EXI_DECODE_ISO_CURRENT_DEMAND_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_CurrentDemandRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CURRENT_DEMAND_RES) && (EXI_DECODE_ISO_CURRENT_DEMAND_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_CurrentDemandRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_CurrentDemandResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_CurrentDemandResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_CurrentDemandResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_CurrentDemandResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_CURRENT_DEMAND_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element DC_EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVSEStatus) */
    {
      #if (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_DC_EVSEStatus(DecWsPtr, &(structPtr->DC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(DC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVSEPresentVoltage */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEPresentVoltage) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEPresentVoltage));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEPresentVoltage) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode element EVSEPresentCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEPresentCurrent) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEPresentCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEPresentCurrent) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode element EVSECurrentLimitAchieved */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSECurrentLimitAchieved) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->EVSECurrentLimitAchieved);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSECurrentLimitAchieved) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #100 Decode element EVSEVoltageLimitAchieved */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEVoltageLimitAchieved) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->EVSEVoltageLimitAchieved);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEVoltageLimitAchieved) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #110 Decode element EVSEPowerLimitAchieved */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEPowerLimitAchieved) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->EVSEPowerLimitAchieved);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEPowerLimitAchieved) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #120 Decode element EVSEMaximumVoltageLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 4, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMaximumVoltageLimit) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumVoltageLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVSEMaximumVoltageLimitFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(EVSEMaximumCurrentLimit) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->EVSEMaximumCurrentLimitFlag = 1;
    }
    else if(exiEventCode == 2) /* SE(EVSEMaximumPowerLimit) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->EVSEMaximumPowerLimitFlag = 1;
    }
    else if(exiEventCode == 3)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVSEMaximumVoltageLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EVSEMaximumVoltageLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #130 Decode optional element EVSEMaximumCurrentLimit with missing elements in front */
    if((0 == structPtr->EVSEMaximumVoltageLimitFlag) && (1 == structPtr->EVSEMaximumCurrentLimitFlag))
    {
      /* SE(EVSEMaximumCurrentLimit) already decoded */
    #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumCurrentLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #140 Decode optional element EVSEMaximumCurrentLimit without missing elements in front */
    else if(1 == structPtr->EVSEMaximumVoltageLimitFlag)
    {
      /* #150 Decode element EVSEMaximumCurrentLimit */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(EVSEMaximumCurrentLimit) */
      {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumCurrentLimit));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->EVSEMaximumCurrentLimitFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(EVSEMaximumPowerLimit) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->EVSEMaximumPowerLimitFlag = 1;
      }
      else if(exiEventCode == 2)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->EVSEMaximumCurrentLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EVSEMaximumCurrentLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #160 Decode optional element EVSEMaximumPowerLimit with missing elements in front */
    if((0 == structPtr->EVSEMaximumCurrentLimitFlag) && (1 == structPtr->EVSEMaximumPowerLimitFlag))
    {
      /* SE(EVSEMaximumPowerLimit) already decoded */
    #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumPowerLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #170 Decode optional element EVSEMaximumPowerLimit without missing elements in front */
    else if(1 == structPtr->EVSEMaximumCurrentLimitFlag)
    {
      /* #180 Decode element EVSEMaximumPowerLimit */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(EVSEMaximumPowerLimit) */
      {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumPowerLimit));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->EVSEMaximumPowerLimitFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->EVSEMaximumPowerLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EVSEMaximumPowerLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #190 Decode element EVSEID with missing elements in front */
    if(0 == structPtr->EVSEMaximumPowerLimitFlag)
    {
      /* SE(EVSEID) already decoded */
    #if (defined(EXI_DECODE_ISO_EVSE_ID) && (EXI_DECODE_ISO_EVSE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_evseID(DecWsPtr, &(structPtr->EVSEID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_EVSE_ID) && (EXI_DECODE_ISO_EVSE_ID == STD_ON)) */
    }
    /* #200 Decode element EVSEID without missing elements in front */
    else
    {
      /* #210 Decode element EVSEID */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(EVSEID) */
      {
      #if (defined(EXI_DECODE_ISO_EVSE_ID) && (EXI_DECODE_ISO_EVSE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_evseID(DecWsPtr, &(structPtr->EVSEID));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_EVSE_ID) && (EXI_DECODE_ISO_EVSE_ID == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #220 Decode element SAScheduleTupleID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SAScheduleTupleID) */
    {
      Exi_BitBufType value = 0;
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 8);
        structPtr->SAScheduleTupleID = (uint8)(value + 1);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(SAScheduleTupleID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #230 Decode element MeterInfo */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(MeterInfo) */
    {
      #if (defined(EXI_DECODE_ISO_METER_INFO) && (EXI_DECODE_ISO_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_MeterInfo(DecWsPtr, &(structPtr->MeterInfo));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->MeterInfoFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_METER_INFO) && (EXI_DECODE_ISO_METER_INFO == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(ReceiptRequired) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->ReceiptRequiredFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->MeterInfoFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    /* #240 Decode optional element ReceiptRequired with missing elements in front */
    if((0 == structPtr->MeterInfoFlag) && (1 == structPtr->ReceiptRequiredFlag))
    {
      /* SE(ReceiptRequired) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ReceiptRequired);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #250 Decode optional element ReceiptRequired without missing elements in front */
    else if(1 == structPtr->MeterInfoFlag)
    {
      /* #260 Decode element ReceiptRequired */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(ReceiptRequired) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)
        {
          structPtr->ReceiptRequiredFlag = 1;
          Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ReceiptRequired);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->ReceiptRequiredFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(ReceiptRequired) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #270 If CurrentDemandRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->ReceiptRequiredFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)/* EE(CurrentDemandRes) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(CurrentDemandRes) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CURRENT_DEMAND_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CURRENT_DEMAND_RES) && (EXI_DECODE_ISO_CURRENT_DEMAND_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_DC_EVChargeParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_DC_EVCHARGE_PARAMETER) && (EXI_DECODE_ISO_DC_EVCHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_DC_EVChargeParameter( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_DC_EVChargeParameterType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_DC_EVChargeParameterType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_DC_EVChargeParameterType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_DC_EVChargeParameterType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_DC_EVCHARGE_PARAMETER_TYPE);
    /* #40 Decode element DepartureTime */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(DepartureTime) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->DepartureTimeFlag = 1;
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->DepartureTime);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->DepartureTimeFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(DepartureTime) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #50 Decode element DC_EVStatus with missing elements in front */
    if(0 == structPtr->DepartureTimeFlag)
    {
      /* SE(DC_EVStatus) already decoded */
    #if (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_DC_EVStatus(DecWsPtr, &(structPtr->DC_EVStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) */
    }
    /* #60 Decode element DC_EVStatus without missing elements in front */
    else
    {
      /* #70 Decode element DC_EVStatus */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(DC_EVStatus) */
      {
      #if (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_DC_EVStatus(DecWsPtr, &(structPtr->DC_EVStatus));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(DC_EVStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode element EVMaximumCurrentLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVMaximumCurrentLimit) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumCurrentLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVMaximumCurrentLimit) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode element EVMaximumPowerLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVMaximumPowerLimit) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumPowerLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVMaximumPowerLimitFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVMaximumPowerLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EVMaximumPowerLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #100 Decode element EVMaximumVoltageLimit with missing elements in front */
    if(0 == structPtr->EVMaximumPowerLimitFlag)
    {
      /* SE(EVMaximumVoltageLimit) already decoded */
    #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumVoltageLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #110 Decode element EVMaximumVoltageLimit without missing elements in front */
    else
    {
      /* #120 Decode element EVMaximumVoltageLimit */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(EVMaximumVoltageLimit) */
      {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumVoltageLimit));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVMaximumVoltageLimit) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #130 Decode element EVEnergyCapacity */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 5, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVEnergyCapacity) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVEnergyCapacity));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVEnergyCapacityFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(EVEnergyRequest) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->EVEnergyRequestFlag = 1;
    }
    else if(exiEventCode == 2) /* SE(FullSOC) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->FullSOCFlag = 1;
    }
    else if(exiEventCode == 3) /* SE(BulkSOC) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->BulkSOCFlag = 1;
    }
    else if(exiEventCode == 4)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVEnergyCapacityFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EVEnergyCapacity) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #140 Decode optional element EVEnergyRequest with missing elements in front */
    if((0 == structPtr->EVEnergyCapacityFlag) && (1 == structPtr->EVEnergyRequestFlag))
    {
      /* SE(EVEnergyRequest) already decoded */
    #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVEnergyRequest));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #150 Decode optional element EVEnergyRequest without missing elements in front */
    else if(1 == structPtr->EVEnergyCapacityFlag)
    {
      /* #160 Decode element EVEnergyRequest */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 4, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(EVEnergyRequest) */
      {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVEnergyRequest));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->EVEnergyRequestFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(FullSOC) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->FullSOCFlag = 1;
      }
      else if(exiEventCode == 2) /* SE(BulkSOC) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->BulkSOCFlag = 1;
      }
      else if(exiEventCode == 3)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->EVEnergyRequestFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EVEnergyRequest) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #170 Decode optional element FullSOC with missing elements in front */
    if((0 == structPtr->EVEnergyRequestFlag) && (1 == structPtr->FullSOCFlag))
    {
      /* SE(FullSOC) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_BitBufType value = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 7);
        structPtr->FullSOC = (sint8)(value);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #180 Decode optional element FullSOC without missing elements in front */
    else if(1 == structPtr->EVEnergyRequestFlag)
    {
      /* #190 Decode element FullSOC */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(FullSOC) */
      {
        Exi_BitBufType value = 0;
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)
        {
          structPtr->FullSOCFlag = 1;
          Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 7);
          structPtr->FullSOC = (sint8)(value);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else if(exiEventCode == 1) /* SE(BulkSOC) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->BulkSOCFlag = 1;
      }
      else if(exiEventCode == 2)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->FullSOCFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(FullSOC) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #200 Decode optional element BulkSOC with missing elements in front */
    if((0 == structPtr->FullSOCFlag) && (1 == structPtr->BulkSOCFlag))
    {
      /* SE(BulkSOC) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_BitBufType value = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 7);
        structPtr->BulkSOC = (sint8)(value);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #210 Decode optional element BulkSOC without missing elements in front */
    else if(1 == structPtr->FullSOCFlag)
    {
      /* #220 Decode element BulkSOC */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(BulkSOC) */
      {
        Exi_BitBufType value = 0;
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)
        {
          structPtr->BulkSOCFlag = 1;
          Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 7);
          structPtr->BulkSOC = (sint8)(value);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->BulkSOCFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(BulkSOC) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #230 If DC_EVChargeParameter was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->BulkSOCFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)/* EE(DC_EVChargeParameter) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(DC_EVChargeParameter) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_DC_EVCHARGE_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_DC_EVCHARGE_PARAMETER) && (EXI_DECODE_ISO_DC_EVCHARGE_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_DC_EVErrorCode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_DC_EVERROR_CODE) && (EXI_DECODE_ISO_DC_EVERROR_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_DC_EVErrorCode( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_DC_EVErrorCodeType, AUTOMATIC, EXI_APPL_VAR) DC_EVErrorCodePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DC_EVErrorCodePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value DC_EVErrorCode element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 4);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 12)
      {
        /* #40 Store value in output buffer */
        *DC_EVErrorCodePtr = (Exi_ISO_DC_EVErrorCodeType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_DC_EVERROR_CODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_DC_EVERROR_CODE) && (EXI_DECODE_ISO_DC_EVERROR_CODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_DC_EVPowerDeliveryParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_DC_EVPowerDeliveryParameter( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_DC_EVPowerDeliveryParameterType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_DC_EVPowerDeliveryParameterType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_DC_EVPowerDeliveryParameterType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_DC_EVPowerDeliveryParameterType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_DC_EVPOWER_DELIVERY_PARAMETER_TYPE);
    /* #40 Decode element DC_EVStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVStatus) */
    {
      #if (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_DC_EVStatus(DecWsPtr, &(structPtr->DC_EVStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(DC_EVStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element BulkChargingComplete */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(BulkChargingComplete) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->BulkChargingCompleteFlag = 1;
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->BulkChargingComplete);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->BulkChargingCompleteFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(BulkChargingComplete) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #60 Decode element ChargingComplete with missing elements in front */
    if(0 == structPtr->BulkChargingCompleteFlag)
    {
      /* SE(ChargingComplete) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ChargingComplete);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #70 Decode element ChargingComplete without missing elements in front */
    else
    {
      /* #80 Decode element ChargingComplete */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(ChargingComplete) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)
        {
          Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ChargingComplete);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ChargingComplete) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 If DC_EVPowerDeliveryParameter was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(DC_EVPowerDeliveryParameter) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_DC_EVSEChargeParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_DC_EVSECHARGE_PARAMETER) && (EXI_DECODE_ISO_DC_EVSECHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_DC_EVSEChargeParameter( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_DC_EVSEChargeParameterType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_DC_EVSEChargeParameterType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_DC_EVSEChargeParameterType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_DC_EVSEChargeParameterType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_DC_EVSECHARGE_PARAMETER_TYPE);
    /* #40 Decode element DC_EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVSEStatus) */
    {
      #if (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_DC_EVSEStatus(DecWsPtr, &(structPtr->DC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(DC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element EVSEMaximumCurrentLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMaximumCurrentLimit) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumCurrentLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEMaximumCurrentLimit) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVSEMaximumPowerLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMaximumPowerLimit) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumPowerLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEMaximumPowerLimit) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVSEMaximumVoltageLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMaximumVoltageLimit) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumVoltageLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEMaximumVoltageLimit) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode element EVSEMinimumCurrentLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMinimumCurrentLimit) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEMinimumCurrentLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEMinimumCurrentLimit) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode element EVSEMinimumVoltageLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMinimumVoltageLimit) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEMinimumVoltageLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEMinimumVoltageLimit) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #100 Decode element EVSECurrentRegulationTolerance */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSECurrentRegulationTolerance) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSECurrentRegulationTolerance));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVSECurrentRegulationToleranceFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVSECurrentRegulationToleranceFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EVSECurrentRegulationTolerance) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #110 Decode element EVSEPeakCurrentRipple with missing elements in front */
    if(0 == structPtr->EVSECurrentRegulationToleranceFlag)
    {
      /* SE(EVSEPeakCurrentRipple) already decoded */
    #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEPeakCurrentRipple));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #120 Decode element EVSEPeakCurrentRipple without missing elements in front */
    else
    {
      /* #130 Decode element EVSEPeakCurrentRipple */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(EVSEPeakCurrentRipple) */
      {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEPeakCurrentRipple));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEPeakCurrentRipple) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #140 Decode element EVSEEnergyToBeDelivered */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEEnergyToBeDelivered) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEEnergyToBeDelivered));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVSEEnergyToBeDeliveredFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVSEEnergyToBeDeliveredFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EVSEEnergyToBeDelivered) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #150 If DC_EVSEChargeParameter was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->EVSEEnergyToBeDeliveredFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)/* EE(DC_EVSEChargeParameter) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(DC_EVSEChargeParameter) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_DC_EVSECHARGE_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_DC_EVSECHARGE_PARAMETER) && (EXI_DECODE_ISO_DC_EVSECHARGE_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_DC_EVSEStatusCode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_DC_EVSESTATUS_CODE) && (EXI_DECODE_ISO_DC_EVSESTATUS_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_DC_EVSEStatusCode( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_DC_EVSEStatusCodeType, AUTOMATIC, EXI_APPL_VAR) DC_EVSEStatusCodePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DC_EVSEStatusCodePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value DC_EVSEStatusCode element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 4);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 12)
      {
        /* #40 Store value in output buffer */
        *DC_EVSEStatusCodePtr = (Exi_ISO_DC_EVSEStatusCodeType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_DC_EVSESTATUS_CODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_DC_EVSESTATUS_CODE) && (EXI_DECODE_ISO_DC_EVSESTATUS_CODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_DC_EVSEStatus
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_DC_EVSEStatus( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_DC_EVSEStatusType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_DC_EVSEStatusType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_DC_EVSEStatusType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_DC_EVSEStatusType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_DC_EVSESTATUS_TYPE);
    /* #40 Decode element NotificationMaxDelay */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(NotificationMaxDelay) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt16(&DecWsPtr->DecWs, &structPtr->NotificationMaxDelay);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(NotificationMaxDelay) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element EVSENotification */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSENotification) */
    {
      #if (defined(EXI_DECODE_ISO_EVSENOTIFICATION) && (EXI_DECODE_ISO_EVSENOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #60 Decode EVSENotification as enumeration value */
      Exi_Decode_ISO_EVSENotification(DecWsPtr, &structPtr->EVSENotification);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSESTATUS, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_EVSENOTIFICATION) && (EXI_DECODE_ISO_EVSENOTIFICATION == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSENotification) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVSEIsolationStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEIsolationStatus) */
    {
      #if (defined(EXI_DECODE_ISO_ISOLATION_LEVEL) && (EXI_DECODE_ISO_ISOLATION_LEVEL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #80 Decode isolationLevel as enumeration value */
      Exi_Decode_ISO_isolationLevel(DecWsPtr, &structPtr->EVSEIsolationStatus);
      structPtr->EVSEIsolationStatusFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSESTATUS, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_ISOLATION_LEVEL) && (EXI_DECODE_ISO_ISOLATION_LEVEL == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVSEIsolationStatusFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EVSEIsolationStatus) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #90 Decode element EVSEStatusCode with missing elements in front */
    if(0 == structPtr->EVSEIsolationStatusFlag)
    {
      /* SE(EVSEStatusCode) already decoded */
    #if (defined(EXI_DECODE_ISO_DC_EVSESTATUS_CODE) && (EXI_DECODE_ISO_DC_EVSESTATUS_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* Decode enumeration value */
      Exi_Decode_ISO_DC_EVSEStatusCode(DecWsPtr, &structPtr->EVSEStatusCode);
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSESTATUS, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_DC_EVSESTATUS_CODE) && (EXI_DECODE_ISO_DC_EVSESTATUS_CODE == STD_ON)) */
    }
    /* #100 Decode element EVSEStatusCode without missing elements in front */
    else
    {
      /* #110 Decode element EVSEStatusCode */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(EVSEStatusCode) */
      {
      #if (defined(EXI_DECODE_ISO_DC_EVSESTATUS_CODE) && (EXI_DECODE_ISO_DC_EVSESTATUS_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* #120 Decode DC_EVSEStatusCode as enumeration value */
        Exi_Decode_ISO_DC_EVSEStatusCode(DecWsPtr, &structPtr->EVSEStatusCode);
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSESTATUS, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVSESTATUS_CODE) && (EXI_DECODE_ISO_DC_EVSESTATUS_CODE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEStatusCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #130 If DC_EVSEStatus was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(DC_EVSEStatus) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_DC_EVSESTATUS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_DC_EVStatus
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_DC_EVStatus( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_DC_EVStatusType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_DC_EVStatusType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_DC_EVStatusType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_DC_EVStatusType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_DC_EVSTATUS_TYPE);
    /* #40 Decode element EVReady */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVReady) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->EVReady);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVReady) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element EVErrorCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVErrorCode) */
    {
      #if (defined(EXI_DECODE_ISO_DC_EVERROR_CODE) && (EXI_DECODE_ISO_DC_EVERROR_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #60 Decode DC_EVErrorCode as enumeration value */
      Exi_Decode_ISO_DC_EVErrorCode(DecWsPtr, &structPtr->EVErrorCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_DC_EVSTATUS, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVERROR_CODE) && (EXI_DECODE_ISO_DC_EVERROR_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVErrorCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVRESSSOC */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVRESSSOC) */
    {
      Exi_BitBufType value = 0;
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 7);
        structPtr->EVRESSSOC = (sint8)(value);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVRESSSOC) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 If DC_EVStatus was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(DC_EVStatus) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_DC_EVSTATUS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_DiffieHellmanPublickey
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_DIFFIE_HELLMAN_PUBLICKEY) && (EXI_DECODE_ISO_DIFFIE_HELLMAN_PUBLICKEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_DiffieHellmanPublickey( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_DiffieHellmanPublickeyType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_DiffieHellmanPublickeyType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_DiffieHellmanPublickeyType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_DiffieHellmanPublickeyType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_ATTRIBUTE_ID, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #40 Decode DiffieHellmanPublickey as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_DIFFIE_HELLMAN_PUBLICKEY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_DIFFIE_HELLMAN_PUBLICKEY) && (EXI_DECODE_ISO_DIFFIE_HELLMAN_PUBLICKEY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_EMAID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_EMAID) && (EXI_DECODE_ISO_EMAID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_EMAID( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_EMAIDType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_EMAIDType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_EMAIDType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_EMAIDType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_ATTRIBUTE_ID, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #40 Decode EMAID as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_EMAID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_EMAID) && (EXI_DECODE_ISO_EMAID == STD_ON)) */


/* Encode API for abstract type Exi_ISO_EVChargeParameterType not required */

/**********************************************************************************************************************
 *  Exi_Decode_ISO_EVSENotification
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_EVSENOTIFICATION) && (EXI_DECODE_ISO_EVSENOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_EVSENotification( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_EVSENotificationType, AUTOMATIC, EXI_APPL_VAR) EVSENotificationPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EVSENotificationPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value EVSENotification element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 3)
      {
        /* #40 Store value in output buffer */
        *EVSENotificationPtr = (Exi_ISO_EVSENotificationType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_EVSENOTIFICATION, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_EVSENOTIFICATION) && (EXI_DECODE_ISO_EVSENOTIFICATION == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_EVSEProcessing
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_EVSEPROCESSING) && (EXI_DECODE_ISO_EVSEPROCESSING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_EVSEProcessing( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_EVSEProcessingType, AUTOMATIC, EXI_APPL_VAR) EVSEProcessingPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EVSEProcessingPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value EVSEProcessing element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 3)
      {
        /* #40 Store value in output buffer */
        *EVSEProcessingPtr = (Exi_ISO_EVSEProcessingType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_EVSEPROCESSING, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_EVSEPROCESSING) && (EXI_DECODE_ISO_EVSEPROCESSING == STD_ON)) */


/* Encode API for abstract type Exi_ISO_EVSEStatusType not required */

/**********************************************************************************************************************
 *  Exi_Decode_ISO_EnergyTransferMode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_DECODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_EnergyTransferMode( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_EnergyTransferModeType, AUTOMATIC, EXI_APPL_VAR) EnergyTransferModePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EnergyTransferModePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value EnergyTransferMode element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 6)
      {
        /* #40 Store value in output buffer */
        *EnergyTransferModePtr = (Exi_ISO_EnergyTransferModeType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_ENERGY_TRANSFER_MODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_DECODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) */


/* Encode API for abstract type Exi_ISO_EntryType not required */

/**********************************************************************************************************************
 *  Exi_Decode_ISO_ListOfRootCertificateIDs
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ListOfRootCertificateIDs( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ListOfRootCertificateIDsType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ListOfRootCertificateIDsType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_X509IssuerSerialType) lastRootCertificateID;
  #if (defined(EXI_DECODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_DECODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  uint16_least i;
  #endif /* #if (defined(EXI_DECODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_DECODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ListOfRootCertificateIDsType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ListOfRootCertificateIDsType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element RootCertificateID, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(RootCertificateID) */
    {
      #if (defined(EXI_DECODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_DECODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_XMLSIG_X509IssuerSerial(DecWsPtr, &(structPtr->RootCertificateID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_DECODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->RootCertificateID->NextRootCertificateIDPtr = (Exi_XMLSIG_X509IssuerSerialType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(RootCertificateID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastRootCertificateID = structPtr->RootCertificateID;
    #if (defined(EXI_DECODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_DECODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #40 Decode subsequent occurences of RootCertificateID */
    for(i=0; i<19; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* SE(RootCertificateID) */
      {
        Exi_Decode_XMLSIG_X509IssuerSerial(DecWsPtr, &(lastRootCertificateID->NextRootCertificateIDPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastRootCertificateID = lastRootCertificateID->NextRootCertificateIDPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(RootCertificateID) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* (defined(EXI_DECODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_DECODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) */
    lastRootCertificateID->NextRootCertificateIDPtr = (Exi_XMLSIG_X509IssuerSerialType*)NULL_PTR;

  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_MessageHeader
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_MESSAGE_HEADER) && (EXI_DECODE_ISO_MESSAGE_HEADER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_MessageHeader( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_MessageHeaderType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_MessageHeaderType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_MessageHeaderType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_MessageHeaderType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element SessionID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SessionID) */
    {
      #if (defined(EXI_DECODE_ISO_SESSION_ID) && (EXI_DECODE_ISO_SESSION_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_sessionID(DecWsPtr, &(structPtr->SessionID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_MESSAGE_HEADER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SESSION_ID) && (EXI_DECODE_ISO_SESSION_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(SessionID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element Notification */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(Notification) */
    {
      #if (defined(EXI_DECODE_ISO_NOTIFICATION) && (EXI_DECODE_ISO_NOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_Notification(DecWsPtr, &(structPtr->Notification));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->NotificationFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_MESSAGE_HEADER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_NOTIFICATION) && (EXI_DECODE_ISO_NOTIFICATION == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(Signature) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->SignatureFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->NotificationFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    /* #50 Decode optional element Signature with missing elements in front */
    if((0 == structPtr->NotificationFlag) && (1 == structPtr->SignatureFlag))
    {
      /* SE(Signature) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_SIGNATURE) && (EXI_DECODE_XMLSIG_SIGNATURE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_XMLSIG_Signature(DecWsPtr, &(structPtr->Signature));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_MESSAGE_HEADER, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE) && (EXI_DECODE_XMLSIG_SIGNATURE == STD_ON)) */
    }
    /* #60 Decode optional element Signature without missing elements in front */
    else if(1 == structPtr->NotificationFlag)
    {
      /* #70 Decode element Signature */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(Signature) */
      {
      #if (defined(EXI_DECODE_XMLSIG_SIGNATURE) && (EXI_DECODE_XMLSIG_SIGNATURE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_XMLSIG_Signature(DecWsPtr, &(structPtr->Signature));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->SignatureFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_MESSAGE_HEADER, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE) && (EXI_DECODE_XMLSIG_SIGNATURE == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->SignatureFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_MESSAGE_HEADER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_MESSAGE_HEADER) && (EXI_DECODE_ISO_MESSAGE_HEADER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_MeterInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_METER_INFO) && (EXI_DECODE_ISO_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_MeterInfo( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_MeterInfoType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_MeterInfoType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_MeterInfoType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_MeterInfoType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element MeterID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(MeterID) */
    {
      #if (defined(EXI_DECODE_ISO_METER_ID) && (EXI_DECODE_ISO_METER_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_meterID(DecWsPtr, &(structPtr->MeterID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_METER_INFO, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_METER_ID) && (EXI_DECODE_ISO_METER_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(MeterID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element MeterReading */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 5, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(MeterReading) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->MeterReadingFlag = 1;
        Exi_VBSDecodeUInt64(&DecWsPtr->DecWs, &structPtr->MeterReading);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1) /* SE(SigMeterReading) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->SigMeterReadingFlag = 1;
    }
    else if(exiEventCode == 2) /* SE(MeterStatus) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->MeterStatusFlag = 1;
    }
    else if(exiEventCode == 3) /* SE(TMeter) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->TMeterFlag = 1;
    }
    else if(exiEventCode == 4)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->MeterReadingFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(MeterReading) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #50 Decode optional element SigMeterReading with missing elements in front */
    if((0 == structPtr->MeterReadingFlag) && (1 == structPtr->SigMeterReadingFlag))
    {
      /* SE(SigMeterReading) already decoded */
    #if (defined(EXI_DECODE_ISO_SIG_METER_READING) && (EXI_DECODE_ISO_SIG_METER_READING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_sigMeterReading(DecWsPtr, &(structPtr->SigMeterReading));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_METER_INFO, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_SIG_METER_READING) && (EXI_DECODE_ISO_SIG_METER_READING == STD_ON)) */
    }
    /* #60 Decode optional element SigMeterReading without missing elements in front */
    else if(1 == structPtr->MeterReadingFlag)
    {
      /* #70 Decode element SigMeterReading */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 4, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(SigMeterReading) */
      {
      #if (defined(EXI_DECODE_ISO_SIG_METER_READING) && (EXI_DECODE_ISO_SIG_METER_READING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_sigMeterReading(DecWsPtr, &(structPtr->SigMeterReading));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->SigMeterReadingFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_METER_INFO, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SIG_METER_READING) && (EXI_DECODE_ISO_SIG_METER_READING == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(MeterStatus) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->MeterStatusFlag = 1;
      }
      else if(exiEventCode == 2) /* SE(TMeter) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->TMeterFlag = 1;
      }
      else if(exiEventCode == 3)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->SigMeterReadingFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(SigMeterReading) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #80 Decode optional element MeterStatus with missing elements in front */
    if((0 == structPtr->SigMeterReadingFlag) && (1 == structPtr->MeterStatusFlag))
    {
      /* SE(MeterStatus) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->MeterStatus);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #90 Decode optional element MeterStatus without missing elements in front */
    else if(1 == structPtr->SigMeterReadingFlag)
    {
      /* #100 Decode element MeterStatus */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(MeterStatus) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)
        {
          structPtr->MeterStatusFlag = 1;
          Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->MeterStatus);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else if(exiEventCode == 1) /* SE(TMeter) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->TMeterFlag = 1;
      }
      else if(exiEventCode == 2)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->MeterStatusFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(MeterStatus) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #110 Decode optional element TMeter with missing elements in front */
    if((0 == structPtr->MeterStatusFlag) && (1 == structPtr->TMeterFlag))
    {
      /* SE(TMeter) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt64(&DecWsPtr->DecWs, &structPtr->TMeter);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #120 Decode optional element TMeter without missing elements in front */
    else if(1 == structPtr->MeterStatusFlag)
    {
      /* #130 Decode element TMeter */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(TMeter) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)
        {
          structPtr->TMeterFlag = 1;
          Exi_VBSDecodeInt64(&DecWsPtr->DecWs, &structPtr->TMeter);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->TMeterFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(TMeter) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_METER_INFO, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_METER_INFO) && (EXI_DECODE_ISO_METER_INFO == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_MeteringReceiptReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_METERING_RECEIPT_REQ) && (EXI_DECODE_ISO_METERING_RECEIPT_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_MeteringReceiptReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_MeteringReceiptReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_MeteringReceiptReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_MeteringReceiptReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_MeteringReceiptReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_METERING_RECEIPT_REQ_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, TRUE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_METERING_RECEIPT_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element SessionID with missing elements in front */
    if(0 == structPtr->IdFlag)
    {
      /* SE(SessionID) already decoded */
    #if (defined(EXI_DECODE_ISO_SESSION_ID) && (EXI_DECODE_ISO_SESSION_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_sessionID(DecWsPtr, &(structPtr->SessionID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_METERING_RECEIPT_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_SESSION_ID) && (EXI_DECODE_ISO_SESSION_ID == STD_ON)) */
    }
    /* #60 Decode element SessionID without missing elements in front */
    else
    {
      /* #70 Decode element SessionID */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(SessionID) */
      {
      #if (defined(EXI_DECODE_ISO_SESSION_ID) && (EXI_DECODE_ISO_SESSION_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_sessionID(DecWsPtr, &(structPtr->SessionID));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_METERING_RECEIPT_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SESSION_ID) && (EXI_DECODE_ISO_SESSION_ID == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(SessionID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode element SAScheduleTupleID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SAScheduleTupleID) */
    {
      Exi_BitBufType value = 0;
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->SAScheduleTupleIDFlag = 1;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 8);
        structPtr->SAScheduleTupleID = (uint8)(value + 1);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->SAScheduleTupleIDFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(SAScheduleTupleID) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #90 Decode element MeterInfo with missing elements in front */
    if(0 == structPtr->SAScheduleTupleIDFlag)
    {
      /* SE(MeterInfo) already decoded */
    #if (defined(EXI_DECODE_ISO_METER_INFO) && (EXI_DECODE_ISO_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_MeterInfo(DecWsPtr, &(structPtr->MeterInfo));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_METERING_RECEIPT_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_METER_INFO) && (EXI_DECODE_ISO_METER_INFO == STD_ON)) */
    }
    /* #100 Decode element MeterInfo without missing elements in front */
    else
    {
      /* #110 Decode element MeterInfo */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(MeterInfo) */
      {
      #if (defined(EXI_DECODE_ISO_METER_INFO) && (EXI_DECODE_ISO_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_MeterInfo(DecWsPtr, &(structPtr->MeterInfo));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_METERING_RECEIPT_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_METER_INFO) && (EXI_DECODE_ISO_METER_INFO == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #120 If MeteringReceiptReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(MeteringReceiptReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_METERING_RECEIPT_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_METERING_RECEIPT_REQ) && (EXI_DECODE_ISO_METERING_RECEIPT_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_MeteringReceiptRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_METERING_RECEIPT_RES) && (EXI_DECODE_ISO_METERING_RECEIPT_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_MeteringReceiptRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_MeteringReceiptResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_MeteringReceiptResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_MeteringReceiptResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_MeteringReceiptResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_METERING_RECEIPT_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_METERING_RECEIPT_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode substitution group EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* #70 Swtich event code */
    switch(exiEventCode)
    {
    case 0: /* SE(AC_EVSEStatus) */
      /* #80 Element AC_EVSEStatus */
      {
        /* #90 Decode element AC_EVSEStatus */
      #if (defined(EXI_DECODE_ISO_AC_EVSESTATUS) && (EXI_DECODE_ISO_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->EVSEStatusElementId = EXI_ISO_AC_EVSESTATUS_TYPE;
        Exi_Decode_ISO_AC_EVSEStatus(DecWsPtr, (Exi_ISO_AC_EVSEStatusType**)&(structPtr->EVSEStatus)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_METERING_RECEIPT_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_AC_EVSESTATUS) && (EXI_DECODE_ISO_AC_EVSESTATUS == STD_ON)) */
      }
    case 1: /* SE(DC_EVSEStatus) */
      /* #100 Element DC_EVSEStatus */
      {
        /* #110 Decode element DC_EVSEStatus */
      #if (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->EVSEStatusElementId = EXI_ISO_DC_EVSESTATUS_TYPE;
        Exi_Decode_ISO_DC_EVSEStatus(DecWsPtr, (Exi_ISO_DC_EVSEStatusType**)&(structPtr->EVSEStatus)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_METERING_RECEIPT_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) */
      }
    /* case 2: SE(EVSEStatus) not required, element is abstract*/
    default:
      /* #120 Unknown element */
      {
        {
          /* #130 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* End of Substitution Group */
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #140 If MeteringReceiptRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(MeteringReceiptRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_METERING_RECEIPT_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_METERING_RECEIPT_RES) && (EXI_DECODE_ISO_METERING_RECEIPT_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_Notification
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_NOTIFICATION) && (EXI_DECODE_ISO_NOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_Notification( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_NotificationType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_NotificationType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_NotificationType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_NotificationType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element FaultCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(FaultCode) */
    {
      #if (defined(EXI_DECODE_ISO_FAULT_CODE) && (EXI_DECODE_ISO_FAULT_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #40 Decode faultCode as enumeration value */
      Exi_Decode_ISO_faultCode(DecWsPtr, &structPtr->FaultCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_NOTIFICATION, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_FAULT_CODE) && (EXI_DECODE_ISO_FAULT_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(FaultCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element FaultMsg */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(FaultMsg) */
    {
      #if (defined(EXI_DECODE_ISO_FAULT_MSG) && (EXI_DECODE_ISO_FAULT_MSG == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_faultMsg(DecWsPtr, &(structPtr->FaultMsg));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->FaultMsgFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_NOTIFICATION, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_FAULT_MSG) && (EXI_DECODE_ISO_FAULT_MSG == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->FaultMsgFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(FaultMsg) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_NOTIFICATION, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_NOTIFICATION) && (EXI_DECODE_ISO_NOTIFICATION == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_PMaxScheduleEntry
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_PMaxScheduleEntry( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_PMaxScheduleEntryType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_PMaxScheduleEntryType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_PMaxScheduleEntryType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_PMaxScheduleEntryType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_PMAX_SCHEDULE_ENTRY_TYPE);
    /* #40 Decode substitution group TimeInterval */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* #50 Swtich event code */
    switch(exiEventCode)
    {
    case 0: /* SE(RelativeTimeInterval) */
      /* #60 Element RelativeTimeInterval */
      {
        /* #70 Decode element RelativeTimeInterval */
      #if (defined(EXI_DECODE_ISO_RELATIVE_TIME_INTERVAL) && (EXI_DECODE_ISO_RELATIVE_TIME_INTERVAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->TimeIntervalElementId = EXI_ISO_RELATIVE_TIME_INTERVAL_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_RelativeTimeInterval(DecWsPtr, (Exi_ISO_RelativeTimeIntervalType**)&(structPtr->TimeInterval)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PMAX_SCHEDULE_ENTRY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_RELATIVE_TIME_INTERVAL) && (EXI_DECODE_ISO_RELATIVE_TIME_INTERVAL == STD_ON)) */
      }
    /* case 1: SE(TimeInterval) not required, element is abstract*/
    default:
      /* #80 Unknown element */
      {
        {
          /* #90 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* End of Substitution Group */
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #100 Decode element PMax */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(PMax) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->PMax));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PMAX_SCHEDULE_ENTRY, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(PMax) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #110 If PMaxScheduleEntry was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(PMaxScheduleEntry) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_PMAX_SCHEDULE_ENTRY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_PMaxSchedule
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_PMAX_SCHEDULE) && (EXI_DECODE_ISO_PMAX_SCHEDULE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_PMaxSchedule( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_PMaxScheduleType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_PMaxScheduleType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_PMaxScheduleEntryType) lastPMaxScheduleEntry;
  #if (defined(EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  uint16_least i;
  #endif /* #if (defined(EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_PMaxScheduleType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_PMaxScheduleType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element PMaxScheduleEntry, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(PMaxScheduleEntry) */
    {
      #if (defined(EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PMaxScheduleEntry(DecWsPtr, &(structPtr->PMaxScheduleEntry));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PMAX_SCHEDULE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->PMaxScheduleEntry->NextPMaxScheduleEntryPtr = (Exi_ISO_PMaxScheduleEntryType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(PMaxScheduleEntry) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastPMaxScheduleEntry = structPtr->PMaxScheduleEntry;
    #if (defined(EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #40 Decode subsequent occurences of PMaxScheduleEntry */
    for(i=0; i<1023; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* SE(PMaxScheduleEntry) */
      {
        Exi_Decode_ISO_PMaxScheduleEntry(DecWsPtr, &(lastPMaxScheduleEntry->NextPMaxScheduleEntryPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastPMaxScheduleEntry = lastPMaxScheduleEntry->NextPMaxScheduleEntryPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(PMaxScheduleEntry) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* (defined(EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY == STD_ON)) */
    lastPMaxScheduleEntry->NextPMaxScheduleEntryPtr = (Exi_ISO_PMaxScheduleEntryType*)NULL_PTR;

  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_PMAX_SCHEDULE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_PMAX_SCHEDULE) && (EXI_DECODE_ISO_PMAX_SCHEDULE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ParameterSet
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_PARAMETER_SET) && (EXI_DECODE_ISO_PARAMETER_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ParameterSet( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ParameterSetType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ParameterSetType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_ParameterType) lastParameter;
  #if (defined(EXI_DECODE_ISO_PARAMETER) && (EXI_DECODE_ISO_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  uint16_least i;
  #endif /* #if (defined(EXI_DECODE_ISO_PARAMETER) && (EXI_DECODE_ISO_PARAMETER == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ParameterSetType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ParameterSetType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element ParameterSetID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ParameterSetID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->ParameterSetID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ParameterSetID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode multi occurence element Parameter, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(Parameter) */
    {
      #if (defined(EXI_DECODE_ISO_PARAMETER) && (EXI_DECODE_ISO_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_Parameter(DecWsPtr, &(structPtr->Parameter));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PARAMETER_SET, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PARAMETER) && (EXI_DECODE_ISO_PARAMETER == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->Parameter->NextParameterPtr = (Exi_ISO_ParameterType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(Parameter) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastParameter = structPtr->Parameter;
    #if (defined(EXI_DECODE_ISO_PARAMETER) && (EXI_DECODE_ISO_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #50 Decode subsequent occurences of Parameter */
    for(i=0; i<15; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* SE(Parameter) */
      {
        Exi_Decode_ISO_Parameter(DecWsPtr, &(lastParameter->NextParameterPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastParameter = lastParameter->NextParameterPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(Parameter) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* (defined(EXI_DECODE_ISO_PARAMETER) && (EXI_DECODE_ISO_PARAMETER == STD_ON)) */
    lastParameter->NextParameterPtr = (Exi_ISO_ParameterType*)NULL_PTR;

  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_PARAMETER_SET, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_PARAMETER_SET) && (EXI_DECODE_ISO_PARAMETER_SET == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_Parameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_PARAMETER) && (EXI_DECODE_ISO_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_Parameter( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ParameterType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ParameterType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ParameterType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ParameterType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode attribute Name */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* AT(Name) */
    {
      #if (defined(EXI_DECODE_ISO_ATTRIBUTE_NAME) && (EXI_DECODE_ISO_ATTRIBUTE_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_AttributeName(DecWsPtr, &(structPtr->Name));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_ATTRIBUTE_NAME) && (EXI_DECODE_ISO_ATTRIBUTE_NAME == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* Start of Choice Element */
    /* #40 Allocate buffer for this element */
    structPtr->ChoiceElement = (Exi_ISO_ParameterChoiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ParameterChoiceType), TRUE);
    if (NULL_PTR == (void*)structPtr->ChoiceElement)
    {
      return;
    }
    /* #50 Decode ParameterChoice element */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 6, 3, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(boolValue) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ChoiceElement->ChoiceValue.boolValue);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      structPtr->ChoiceElement->boolValueFlag = 1;
    }
    else if(1 == exiEventCode) /* SE(byteValue) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt8(&DecWsPtr->DecWs, &structPtr->ChoiceElement->ChoiceValue.byteValue);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      structPtr->ChoiceElement->byteValueFlag = 1;
    }
    else if(2 == exiEventCode) /* SE(shortValue) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->ChoiceElement->ChoiceValue.shortValue);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      structPtr->ChoiceElement->shortValueFlag = 1;
    }
    else if(3 == exiEventCode) /* SE(intValue) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt32(&DecWsPtr->DecWs, &structPtr->ChoiceElement->ChoiceValue.intValue);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      structPtr->ChoiceElement->intValueFlag = 1;
    }
    else if(4 == exiEventCode) /* SE(physicalValue) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.physicalValue));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->physicalValueFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(5 == exiEventCode) /* SE(stringValue) */
    {
      #if (defined(EXI_DECODE_STRING) && (EXI_DECODE_STRING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_string(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.stringValue), EXI_SCHEMA_SET_ISO_TYPE);
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->stringValueFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_STRING) && (EXI_DECODE_STRING == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ChoiceElement) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_PARAMETER) && (EXI_DECODE_ISO_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_PaymentDetailsReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_PAYMENT_DETAILS_REQ) && (EXI_DECODE_ISO_PAYMENT_DETAILS_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_PaymentDetailsReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_PaymentDetailsReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_PaymentDetailsReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_PaymentDetailsReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_PaymentDetailsReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_PAYMENT_DETAILS_REQ_TYPE);
    /* #40 Decode element eMAID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(eMAID) */
    {
      #if (defined(EXI_DECODE_ISO_E_MAID) && (EXI_DECODE_ISO_E_MAID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_eMAID(DecWsPtr, &(structPtr->eMAID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PAYMENT_DETAILS_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_E_MAID) && (EXI_DECODE_ISO_E_MAID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(eMAID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element ContractSignatureCertChain */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ContractSignatureCertChain) */
    {
      #if (defined(EXI_DECODE_ISO_CERTIFICATE_CHAIN) && (EXI_DECODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_CertificateChain(DecWsPtr, &(structPtr->ContractSignatureCertChain));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PAYMENT_DETAILS_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE_CHAIN) && (EXI_DECODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #60 If PaymentDetailsReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(PaymentDetailsReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_PAYMENT_DETAILS_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_PAYMENT_DETAILS_REQ) && (EXI_DECODE_ISO_PAYMENT_DETAILS_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_PaymentDetailsRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_PAYMENT_DETAILS_RES) && (EXI_DECODE_ISO_PAYMENT_DETAILS_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_PaymentDetailsRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_PaymentDetailsResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_PaymentDetailsResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_PaymentDetailsResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_PaymentDetailsResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_PAYMENT_DETAILS_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PAYMENT_DETAILS_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element GenChallenge */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(GenChallenge) */
    {
      #if (defined(EXI_DECODE_ISO_GEN_CHALLENGE) && (EXI_DECODE_ISO_GEN_CHALLENGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_genChallenge(DecWsPtr, &(structPtr->GenChallenge));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PAYMENT_DETAILS_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_GEN_CHALLENGE) && (EXI_DECODE_ISO_GEN_CHALLENGE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(GenChallenge) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVSETimeStamp */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSETimeStamp) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt64(&DecWsPtr->DecWs, &structPtr->EVSETimeStamp);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSETimeStamp) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 If PaymentDetailsRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(PaymentDetailsRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_PAYMENT_DETAILS_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_PAYMENT_DETAILS_RES) && (EXI_DECODE_ISO_PAYMENT_DETAILS_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_PaymentOptionList
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_PAYMENT_OPTION_LIST) && (EXI_DECODE_ISO_PAYMENT_OPTION_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_PaymentOptionList( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_PaymentOptionListType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_PaymentOptionListType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  uint16_least i;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_PaymentOptionListType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_PaymentOptionListType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element PaymentOption, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(PaymentOption) */
    {
      #if (defined(EXI_DECODE_ISO_PAYMENT_OPTION) && (EXI_DECODE_ISO_PAYMENT_OPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #40 Decode first element of paymentOption as a multi occurence enumeration value */
      Exi_Decode_ISO_paymentOption(DecWsPtr, &structPtr->PaymentOption[0]);
      structPtr->PaymentOptionCount = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PAYMENT_OPTION_LIST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PAYMENT_OPTION) && (EXI_DECODE_ISO_PAYMENT_OPTION == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(PaymentOption) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    /* #50 Decode subsequent occurences of PaymentOption */
    for(i=0; i<1; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* SE(PaymentOption) */
      {
      #if (defined(EXI_DECODE_ISO_PAYMENT_OPTION) && (EXI_DECODE_ISO_PAYMENT_OPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* Decode a subsequent element of a multi occurence enumeration value */
        Exi_Decode_ISO_paymentOption(DecWsPtr, &structPtr->PaymentOption[i + 1]);
        structPtr->PaymentOptionCount++;
      #endif /* (defined(EXI_DECODE_ISO_PAYMENT_OPTION) && (EXI_DECODE_ISO_PAYMENT_OPTION == STD_ON)) */
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(PaymentOption) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }


  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_PAYMENT_OPTION_LIST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_PAYMENT_OPTION_LIST) && (EXI_DECODE_ISO_PAYMENT_OPTION_LIST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_PaymentServiceSelectionReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_REQ) && (EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_PaymentServiceSelectionReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_PaymentServiceSelectionReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_PaymentServiceSelectionReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_PaymentServiceSelectionReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_PaymentServiceSelectionReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_PAYMENT_SERVICE_SELECTION_REQ_TYPE);
    /* #40 Decode element SelectedPaymentOption */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SelectedPaymentOption) */
    {
      #if (defined(EXI_DECODE_ISO_PAYMENT_OPTION) && (EXI_DECODE_ISO_PAYMENT_OPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode paymentOption as enumeration value */
      Exi_Decode_ISO_paymentOption(DecWsPtr, &structPtr->SelectedPaymentOption);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PAYMENT_SERVICE_SELECTION_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PAYMENT_OPTION) && (EXI_DECODE_ISO_PAYMENT_OPTION == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(SelectedPaymentOption) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element SelectedServiceList */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SelectedServiceList) */
    {
      #if (defined(EXI_DECODE_ISO_SELECTED_SERVICE_LIST) && (EXI_DECODE_ISO_SELECTED_SERVICE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_SelectedServiceList(DecWsPtr, &(structPtr->SelectedServiceList));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PAYMENT_SERVICE_SELECTION_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SELECTED_SERVICE_LIST) && (EXI_DECODE_ISO_SELECTED_SERVICE_LIST == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #70 If PaymentServiceSelectionReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(PaymentServiceSelectionReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_PAYMENT_SERVICE_SELECTION_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_REQ) && (EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_PaymentServiceSelectionRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_RES) && (EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_PaymentServiceSelectionRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_PaymentServiceSelectionResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_PaymentServiceSelectionResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_PaymentServiceSelectionResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_PaymentServiceSelectionResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_PAYMENT_SERVICE_SELECTION_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PAYMENT_SERVICE_SELECTION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 If PaymentServiceSelectionRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(PaymentServiceSelectionRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_PAYMENT_SERVICE_SELECTION_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_RES) && (EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_PhysicalValue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_PhysicalValue( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_PhysicalValueType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_PhysicalValueType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_PhysicalValueType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element Multiplier */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(Multiplier) */
    {
      Exi_BitBufType value = 0;
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 3);
        structPtr->Multiplier = (sint8)(value - 3);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(Multiplier) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element Unit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(Unit) */
    {
      #if (defined(EXI_DECODE_ISO_UNIT_SYMBOL) && (EXI_DECODE_ISO_UNIT_SYMBOL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode unitSymbol as enumeration value */
      Exi_Decode_ISO_unitSymbol(DecWsPtr, &structPtr->Unit);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PHYSICAL_VALUE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_UNIT_SYMBOL) && (EXI_DECODE_ISO_UNIT_SYMBOL == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(Unit) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element Value */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(Value) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->Value);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(Value) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_PHYSICAL_VALUE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_PowerDeliveryReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_POWER_DELIVERY_REQ) && (EXI_DECODE_ISO_POWER_DELIVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_PowerDeliveryReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_PowerDeliveryReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_PowerDeliveryReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_PowerDeliveryReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_PowerDeliveryReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_POWER_DELIVERY_REQ_TYPE);
    /* #40 Decode element ChargeProgress */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ChargeProgress) */
    {
      #if (defined(EXI_DECODE_ISO_CHARGE_PROGRESS) && (EXI_DECODE_ISO_CHARGE_PROGRESS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode chargeProgress as enumeration value */
      Exi_Decode_ISO_chargeProgress(DecWsPtr, &structPtr->ChargeProgress);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_POWER_DELIVERY_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_CHARGE_PROGRESS) && (EXI_DECODE_ISO_CHARGE_PROGRESS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ChargeProgress) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element SAScheduleTupleID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SAScheduleTupleID) */
    {
      Exi_BitBufType value = 0;
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 8);
        structPtr->SAScheduleTupleID = (uint8)(value + 1);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(SAScheduleTupleID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element ChargingProfile */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 4, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ChargingProfile) */
    {
      #if (defined(EXI_DECODE_ISO_CHARGING_PROFILE) && (EXI_DECODE_ISO_CHARGING_PROFILE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_ChargingProfile(DecWsPtr, &(structPtr->ChargingProfile));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChargingProfileFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_POWER_DELIVERY_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_CHARGING_PROFILE) && (EXI_DECODE_ISO_CHARGING_PROFILE == STD_ON)) */
    }
    else if(exiEventCode < 3) /* Substitution Group SE(EVPowerDeliveryParameter) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->EVPowerDeliveryParameterFlag = 1;
    }
    else if(exiEventCode == 3)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ChargingProfileFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    /* #80 Decode optional substitution group EVPowerDeliveryParameter with missing elements in front */
    if((0 == structPtr->ChargingProfileFlag) && (1 == structPtr->EVPowerDeliveryParameterFlag))
    {
      /* Start of Substitution Group with missing elements in front */
      /* #90 Decrement event code by the amount of missing elements to get a zero based event code */
      exiEventCode -= 1u; /* Element: ChargingProfile */ /* PRQA S 3382 */ /* MD_Exi_21.1_3382 */
      /* #100 Switch event code */
      switch(exiEventCode)
      {
      case 0: /* SE(DC_EVPowerDeliveryParameter) */
        /* #110 Element DC_EVPowerDeliveryParameter */
        {
          /* #120 Decode element DC_EVPowerDeliveryParameter */
        #if (defined(EXI_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          structPtr->EVPowerDeliveryParameterElementId = EXI_ISO_DC_EVPOWER_DELIVERY_PARAMETER_TYPE;
          Exi_Decode_ISO_DC_EVPowerDeliveryParameter(DecWsPtr, (Exi_ISO_DC_EVPowerDeliveryParameterType**)&(structPtr->EVPowerDeliveryParameter)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          structPtr->EVPowerDeliveryParameterFlag = 1;
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_POWER_DELIVERY_REQ, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) */
        }
      /* case 1: SE(EVPowerDeliveryParameter) not required, element is abstract*/
      default:
        /* #130 Unknown substitution element */
        {
          /* #140 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* #150 Decode optional substitution group EVPowerDeliveryParameter without missing elements in front */
    else if(1 == structPtr->ChargingProfileFlag)
    {
      /* Start of Substitution Group without missing elements in front */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* #160 Swtich event code */
      switch(exiEventCode)
      {
      case 0: /* SE(DC_EVPowerDeliveryParameter) */
        /* #170 Element DC_EVPowerDeliveryParameter */
        {
          /* #180 Decode element DC_EVPowerDeliveryParameter */
        #if (defined(EXI_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          structPtr->EVPowerDeliveryParameterElementId = EXI_ISO_DC_EVPOWER_DELIVERY_PARAMETER_TYPE;
          Exi_Decode_ISO_DC_EVPowerDeliveryParameter(DecWsPtr, (Exi_ISO_DC_EVPowerDeliveryParameterType**)&(structPtr->EVPowerDeliveryParameter)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          structPtr->EVPowerDeliveryParameterFlag = 1;
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_POWER_DELIVERY_REQ, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) */
        }
      /* case 1: SE(EVPowerDeliveryParameter) not required, element is abstract*/
      case 2: /* optional element not included */
        /* Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        break;
      default:
        /* #190 Unknown element */
        {
          {
            /* #200 Set status code to error */
            /* unsupported event code */
            Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
            return;
          }
        }
      }
    /* End of Substitution Group */
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->EVPowerDeliveryParameterFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #210 If PowerDeliveryReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->EVPowerDeliveryParameterFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)/* EE(PowerDeliveryReq) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(PowerDeliveryReq) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_POWER_DELIVERY_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_POWER_DELIVERY_REQ) && (EXI_DECODE_ISO_POWER_DELIVERY_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_PowerDeliveryRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_POWER_DELIVERY_RES) && (EXI_DECODE_ISO_POWER_DELIVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_PowerDeliveryRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_PowerDeliveryResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_PowerDeliveryResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_PowerDeliveryResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_PowerDeliveryResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_POWER_DELIVERY_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_POWER_DELIVERY_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode substitution group EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* #70 Swtich event code */
    switch(exiEventCode)
    {
    case 0: /* SE(AC_EVSEStatus) */
      /* #80 Element AC_EVSEStatus */
      {
        /* #90 Decode element AC_EVSEStatus */
      #if (defined(EXI_DECODE_ISO_AC_EVSESTATUS) && (EXI_DECODE_ISO_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->EVSEStatusElementId = EXI_ISO_AC_EVSESTATUS_TYPE;
        Exi_Decode_ISO_AC_EVSEStatus(DecWsPtr, (Exi_ISO_AC_EVSEStatusType**)&(structPtr->EVSEStatus)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_POWER_DELIVERY_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_AC_EVSESTATUS) && (EXI_DECODE_ISO_AC_EVSESTATUS == STD_ON)) */
      }
    case 1: /* SE(DC_EVSEStatus) */
      /* #100 Element DC_EVSEStatus */
      {
        /* #110 Decode element DC_EVSEStatus */
      #if (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->EVSEStatusElementId = EXI_ISO_DC_EVSESTATUS_TYPE;
        Exi_Decode_ISO_DC_EVSEStatus(DecWsPtr, (Exi_ISO_DC_EVSEStatusType**)&(structPtr->EVSEStatus)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_POWER_DELIVERY_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) */
      }
    /* case 2: SE(EVSEStatus) not required, element is abstract*/
    default:
      /* #120 Unknown element */
      {
        {
          /* #130 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* End of Substitution Group */
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #140 If PowerDeliveryRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(PowerDeliveryRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_POWER_DELIVERY_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_POWER_DELIVERY_RES) && (EXI_DECODE_ISO_POWER_DELIVERY_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_PreChargeReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_PRE_CHARGE_REQ) && (EXI_DECODE_ISO_PRE_CHARGE_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_PreChargeReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_PreChargeReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_PreChargeReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_PreChargeReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_PreChargeReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_PRE_CHARGE_REQ_TYPE);
    /* #40 Decode element DC_EVStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVStatus) */
    {
      #if (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_DC_EVStatus(DecWsPtr, &(structPtr->DC_EVStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PRE_CHARGE_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(DC_EVStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element EVTargetVoltage */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVTargetVoltage) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVTargetVoltage));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PRE_CHARGE_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVTargetVoltage) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVTargetCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVTargetCurrent) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVTargetCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PRE_CHARGE_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVTargetCurrent) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 If PreChargeReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(PreChargeReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_PRE_CHARGE_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_PRE_CHARGE_REQ) && (EXI_DECODE_ISO_PRE_CHARGE_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_PreChargeRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_PRE_CHARGE_RES) && (EXI_DECODE_ISO_PRE_CHARGE_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_PreChargeRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_PreChargeResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_PreChargeResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_PreChargeResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_PreChargeResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_PRE_CHARGE_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PRE_CHARGE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element DC_EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVSEStatus) */
    {
      #if (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_DC_EVSEStatus(DecWsPtr, &(structPtr->DC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PRE_CHARGE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(DC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVSEPresentVoltage */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEPresentVoltage) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEPresentVoltage));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PRE_CHARGE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEPresentVoltage) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 If PreChargeRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(PreChargeRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_PRE_CHARGE_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_PRE_CHARGE_RES) && (EXI_DECODE_ISO_PRE_CHARGE_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ProfileEntry
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_PROFILE_ENTRY) && (EXI_DECODE_ISO_PROFILE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ProfileEntry( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ProfileEntryType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ProfileEntryType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ProfileEntryType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ProfileEntryType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element ChargingProfileEntryStart */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ChargingProfileEntryStart) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->ChargingProfileEntryStart);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ChargingProfileEntryStart) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element ChargingProfileEntryMaxPower */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ChargingProfileEntryMaxPower) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->ChargingProfileEntryMaxPower));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_PROFILE_ENTRY, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ChargingProfileEntryMaxPower) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element ChargingProfileEntryMaxNumberOfPhasesInUse */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ChargingProfileEntryMaxNumberOfPhasesInUse) */
    {
      Exi_BitBufType value = 0;
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->ChargingProfileEntryMaxNumberOfPhasesInUseFlag = 1;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 2);
        structPtr->ChargingProfileEntryMaxNumberOfPhasesInUse = (sint8)(value + 1);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ChargingProfileEntryMaxNumberOfPhasesInUseFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(ChargingProfileEntryMaxNumberOfPhasesInUse) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_PROFILE_ENTRY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_PROFILE_ENTRY) && (EXI_DECODE_ISO_PROFILE_ENTRY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_RelativeTimeInterval
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_RELATIVE_TIME_INTERVAL) && (EXI_DECODE_ISO_RELATIVE_TIME_INTERVAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_RelativeTimeInterval( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_RelativeTimeIntervalType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_RelativeTimeIntervalType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_RelativeTimeIntervalType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_RelativeTimeIntervalType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_RELATIVE_TIME_INTERVAL_TYPE);
    /* #40 Decode element start */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(start) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->start);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(start) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element duration */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(duration) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->durationFlag = 1;
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->duration);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->durationFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(duration) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #60 If RelativeTimeInterval was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->durationFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)/* EE(RelativeTimeInterval) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(RelativeTimeInterval) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_RELATIVE_TIME_INTERVAL, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_RELATIVE_TIME_INTERVAL) && (EXI_DECODE_ISO_RELATIVE_TIME_INTERVAL == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_SAScheduleList
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SASCHEDULE_LIST) && (EXI_DECODE_ISO_SASCHEDULE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_SAScheduleList( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_SAScheduleListType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_SAScheduleListType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_SAScheduleTupleType) lastSAScheduleTuple;
  #if (defined(EXI_DECODE_ISO_SASCHEDULE_TUPLE) && (EXI_DECODE_ISO_SASCHEDULE_TUPLE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  uint16_least i;
  #endif /* #if (defined(EXI_DECODE_ISO_SASCHEDULE_TUPLE) && (EXI_DECODE_ISO_SASCHEDULE_TUPLE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_SAScheduleListType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_SAScheduleListType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_SASCHEDULE_LIST_TYPE);
    /* #40 Decode multi occurence element SAScheduleTuple, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SAScheduleTuple) */
    {
      #if (defined(EXI_DECODE_ISO_SASCHEDULE_TUPLE) && (EXI_DECODE_ISO_SASCHEDULE_TUPLE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_SAScheduleTuple(DecWsPtr, &(structPtr->SAScheduleTuple));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SASCHEDULE_LIST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SASCHEDULE_TUPLE) && (EXI_DECODE_ISO_SASCHEDULE_TUPLE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->SAScheduleTuple->NextSAScheduleTuplePtr = (Exi_ISO_SAScheduleTupleType*)NULL_PTR;
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastSAScheduleTuple = structPtr->SAScheduleTuple;
    #if (defined(EXI_DECODE_ISO_SASCHEDULE_TUPLE) && (EXI_DECODE_ISO_SASCHEDULE_TUPLE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #50 Decode subsequent occurences of SAScheduleTuple */
    for(i=0; i<2; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* SE(SAScheduleTuple) */
      {
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_SAScheduleTuple(DecWsPtr, &(lastSAScheduleTuple->NextSAScheduleTuplePtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastSAScheduleTuple = lastSAScheduleTuple->NextSAScheduleTuplePtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(SAScheduleTuple) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(SAScheduleTuple) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* (defined(EXI_DECODE_ISO_SASCHEDULE_TUPLE) && (EXI_DECODE_ISO_SASCHEDULE_TUPLE == STD_ON)) */
    lastSAScheduleTuple->NextSAScheduleTuplePtr = (Exi_ISO_SAScheduleTupleType*)NULL_PTR;

    /* #60 If SAScheduleList was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      #if (defined(EXI_DECODE_ISO_SASCHEDULE_TUPLE) && (EXI_DECODE_ISO_SASCHEDULE_TUPLE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      exiEventCode = 0;
      if(2 == i)
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      }
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(SAScheduleList) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      #endif /* #if (defined(EXI_DECODE_ISO_SASCHEDULE_TUPLE) && (EXI_DECODE_ISO_SASCHEDULE_TUPLE == STD_ON)) */
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SASCHEDULE_LIST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SASCHEDULE_LIST) && (EXI_DECODE_ISO_SASCHEDULE_LIST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_SAScheduleTuple
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SASCHEDULE_TUPLE) && (EXI_DECODE_ISO_SASCHEDULE_TUPLE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_SAScheduleTuple( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_SAScheduleTupleType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_SAScheduleTupleType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_SAScheduleTupleType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_SAScheduleTupleType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element SAScheduleTupleID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SAScheduleTupleID) */
    {
      Exi_BitBufType value = 0;
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 8);
        structPtr->SAScheduleTupleID = (uint8)(value + 1);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(SAScheduleTupleID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element PMaxSchedule */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(PMaxSchedule) */
    {
      #if (defined(EXI_DECODE_ISO_PMAX_SCHEDULE) && (EXI_DECODE_ISO_PMAX_SCHEDULE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_PMaxSchedule(DecWsPtr, &(structPtr->PMaxSchedule));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SASCHEDULE_TUPLE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PMAX_SCHEDULE) && (EXI_DECODE_ISO_PMAX_SCHEDULE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #50 Decode element SalesTariff */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SalesTariff) */
    {
      #if (defined(EXI_DECODE_ISO_SALES_TARIFF) && (EXI_DECODE_ISO_SALES_TARIFF == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_SalesTariff(DecWsPtr, &(structPtr->SalesTariff));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->SalesTariffFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SASCHEDULE_TUPLE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SALES_TARIFF) && (EXI_DECODE_ISO_SALES_TARIFF == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->SalesTariffFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SASCHEDULE_TUPLE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SASCHEDULE_TUPLE) && (EXI_DECODE_ISO_SASCHEDULE_TUPLE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_SalesTariffEntry
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SALES_TARIFF_ENTRY) && (EXI_DECODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_SalesTariffEntry( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_SalesTariffEntryType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_SalesTariffEntryType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_ConsumptionCostType) lastConsumptionCost;
  #if (defined(EXI_DECODE_ISO_CONSUMPTION_COST) && (EXI_DECODE_ISO_CONSUMPTION_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  uint16_least i = 0;
  #endif /* #if (defined(EXI_DECODE_ISO_CONSUMPTION_COST) && (EXI_DECODE_ISO_CONSUMPTION_COST == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_SalesTariffEntryType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_SalesTariffEntryType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_SALES_TARIFF_ENTRY_TYPE);
    /* #40 Decode substitution group TimeInterval */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* #50 Swtich event code */
    switch(exiEventCode)
    {
    case 0: /* SE(RelativeTimeInterval) */
      /* #60 Element RelativeTimeInterval */
      {
        /* #70 Decode element RelativeTimeInterval */
      #if (defined(EXI_DECODE_ISO_RELATIVE_TIME_INTERVAL) && (EXI_DECODE_ISO_RELATIVE_TIME_INTERVAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->TimeIntervalElementId = EXI_ISO_RELATIVE_TIME_INTERVAL_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_RelativeTimeInterval(DecWsPtr, (Exi_ISO_RelativeTimeIntervalType**)&(structPtr->TimeInterval)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SALES_TARIFF_ENTRY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_RELATIVE_TIME_INTERVAL) && (EXI_DECODE_ISO_RELATIVE_TIME_INTERVAL == STD_ON)) */
      }
    /* case 1: SE(TimeInterval) not required, element is abstract*/
    default:
      /* #80 Unknown element */
      {
        {
          /* #90 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* End of Substitution Group */
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #100 Decode element EPriceLevel */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EPriceLevel) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->EPriceLevelFlag = 1;
        Exi_VBSDecodeUInt8(&DecWsPtr->DecWs, &structPtr->EPriceLevel);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1) /* SE(ConsumptionCost) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->ConsumptionCostFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EPriceLevelFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EPriceLevel) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #110 Decode optional element ConsumptionCost with missing elements in front */
    if((0 == structPtr->EPriceLevelFlag) && (1 == structPtr->ConsumptionCostFlag))
    {
      /* SE(ConsumptionCost) already decoded */
    #if (defined(EXI_DECODE_ISO_CONSUMPTION_COST) && (EXI_DECODE_ISO_CONSUMPTION_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_ConsumptionCost(DecWsPtr, &(structPtr->ConsumptionCost));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SALES_TARIFF_ENTRY, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_CONSUMPTION_COST) && (EXI_DECODE_ISO_CONSUMPTION_COST == STD_ON)) */
    }
    /* #120 Decode optional element ConsumptionCost without missing elements in front */
    else if(1 == structPtr->EPriceLevelFlag)
    {
      /* #130 Decode multi occurence element ConsumptionCost, decode first occurence */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(ConsumptionCost) */
      {
      #if (defined(EXI_DECODE_ISO_CONSUMPTION_COST) && (EXI_DECODE_ISO_CONSUMPTION_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_ConsumptionCost(DecWsPtr, &(structPtr->ConsumptionCost));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->ConsumptionCostFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SALES_TARIFF_ENTRY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_CONSUMPTION_COST) && (EXI_DECODE_ISO_CONSUMPTION_COST == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->ConsumptionCostFlag)
    {
      structPtr->ConsumptionCost->NextConsumptionCostPtr = (Exi_ISO_ConsumptionCostType*)NULL_PTR;
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
      if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
      {
        /* an error occured in a previous step -> abort to avoid not required loop */
        return;
      }
      lastConsumptionCost = structPtr->ConsumptionCost;
      #if (defined(EXI_DECODE_ISO_CONSUMPTION_COST) && (EXI_DECODE_ISO_CONSUMPTION_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #140 Decode subsequent occurences of ConsumptionCost */
      for(i=0; i<2; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
      { /* PRQA S 3201 */ /* MD_MSR_14.1 */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)/* SE(ConsumptionCost) */
        {
          DecWsPtr->DecWs.EERequired = TRUE;
          Exi_Decode_ISO_ConsumptionCost(DecWsPtr, &(lastConsumptionCost->NextConsumptionCostPtr));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          lastConsumptionCost = lastConsumptionCost->NextConsumptionCostPtr;
        }
        else if(1 == exiEventCode)/* reached next Tag */
        {
          DecWsPtr->DecWs.EERequired = FALSE;
          break;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
        if(FALSE == DecWsPtr->DecWs.EERequired)
        {
          /* EE(ConsumptionCost) already decoded */
          DecWsPtr->DecWs.EERequired = TRUE;
          continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
        }
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* check exiEventCode equals EE(ConsumptionCost) */
        if(0 != exiEventCode)
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      #endif /* (defined(EXI_DECODE_ISO_CONSUMPTION_COST) && (EXI_DECODE_ISO_CONSUMPTION_COST == STD_ON)) */
      lastConsumptionCost->NextConsumptionCostPtr = (Exi_ISO_ConsumptionCostType*)NULL_PTR;

    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #150 If SalesTariffEntry was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->ConsumptionCostFlag)
      {
        exiEventCode = 0;
        if(1 == i)
        {
          Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        }
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)/* EE(SalesTariffEntry) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(SalesTariffEntry) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SALES_TARIFF_ENTRY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SALES_TARIFF_ENTRY) && (EXI_DECODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_SalesTariff
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SALES_TARIFF) && (EXI_DECODE_ISO_SALES_TARIFF == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_SalesTariff( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_SalesTariffType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_SalesTariffType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_SalesTariffEntryType) lastSalesTariffEntry;
  #if (defined(EXI_DECODE_ISO_SALES_TARIFF_ENTRY) && (EXI_DECODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  uint16_least i;
  #endif /* #if (defined(EXI_DECODE_ISO_SALES_TARIFF_ENTRY) && (EXI_DECODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_SalesTariffType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_SalesTariffType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, TRUE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SALES_TARIFF, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_ATTRIBUTE_ID) && (EXI_DECODE_ISO_ATTRIBUTE_ID == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element SalesTariffID with missing elements in front */
    if(0 == structPtr->IdFlag)
    {
      /* SE(SalesTariffID) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_BitBufType value = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 8);
        structPtr->SalesTariffID = (uint8)(value + 1);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #50 Decode element SalesTariffID without missing elements in front */
    else
    {
      /* #60 Decode element SalesTariffID */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(SalesTariffID) */
      {
        Exi_BitBufType value = 0;
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)
        {
          Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 8);
          structPtr->SalesTariffID = (uint8)(value + 1);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(SalesTariffID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element SalesTariffDescription */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 1, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SalesTariffDescription) */
    {
      #if (defined(EXI_DECODE_ISO_TARIFF_DESCRIPTION) && (EXI_DECODE_ISO_TARIFF_DESCRIPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_tariffDescription(DecWsPtr, &(structPtr->SalesTariffDescription));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->SalesTariffDescriptionFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SALES_TARIFF, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_TARIFF_DESCRIPTION) && (EXI_DECODE_ISO_TARIFF_DESCRIPTION == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(NumEPriceLevels) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->NumEPriceLevelsFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->SalesTariffDescriptionFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(SalesTariffDescription) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #80 Decode optional element NumEPriceLevels with missing elements in front */
    if((0 == structPtr->SalesTariffDescriptionFlag) && (1 == structPtr->NumEPriceLevelsFlag))
    {
      /* SE(NumEPriceLevels) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt8(&DecWsPtr->DecWs, &structPtr->NumEPriceLevels);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #90 Decode optional element NumEPriceLevels without missing elements in front */
    else if(1 == structPtr->SalesTariffDescriptionFlag)
    {
      /* #100 Decode element NumEPriceLevels */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 1, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(NumEPriceLevels) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)
        {
          structPtr->NumEPriceLevelsFlag = 1;
          Exi_VBSDecodeUInt8(&DecWsPtr->DecWs, &structPtr->NumEPriceLevels);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->NumEPriceLevelsFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(NumEPriceLevels) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #110 Decode element SalesTariffEntry with missing elements in front */
    if(0 == structPtr->NumEPriceLevelsFlag)
    {
      /* SE(SalesTariffEntry) already decoded */
    #if (defined(EXI_DECODE_ISO_SALES_TARIFF_ENTRY) && (EXI_DECODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_SalesTariffEntry(DecWsPtr, &(structPtr->SalesTariffEntry));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SALES_TARIFF, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_SALES_TARIFF_ENTRY) && (EXI_DECODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) */
    }
    /* #120 Decode element SalesTariffEntry without missing elements in front */
    else
    {
      /* #130 Decode multi occurence element SalesTariffEntry, decode first occurence */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(SalesTariffEntry) */
      {
      #if (defined(EXI_DECODE_ISO_SALES_TARIFF_ENTRY) && (EXI_DECODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_SalesTariffEntry(DecWsPtr, &(structPtr->SalesTariffEntry));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SALES_TARIFF, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SALES_TARIFF_ENTRY) && (EXI_DECODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    structPtr->SalesTariffEntry->NextSalesTariffEntryPtr = (Exi_ISO_SalesTariffEntryType*)NULL_PTR;
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastSalesTariffEntry = structPtr->SalesTariffEntry;
    #if (defined(EXI_DECODE_ISO_SALES_TARIFF_ENTRY) && (EXI_DECODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #140 Decode subsequent occurences of SalesTariffEntry */
    for(i=0; i<1023; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* SE(SalesTariffEntry) */
      {
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_SalesTariffEntry(DecWsPtr, &(lastSalesTariffEntry->NextSalesTariffEntryPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastSalesTariffEntry = lastSalesTariffEntry->NextSalesTariffEntryPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(SalesTariffEntry) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(SalesTariffEntry) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* (defined(EXI_DECODE_ISO_SALES_TARIFF_ENTRY) && (EXI_DECODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) */
    lastSalesTariffEntry->NextSalesTariffEntryPtr = (Exi_ISO_SalesTariffEntryType*)NULL_PTR;

  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SALES_TARIFF, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SALES_TARIFF) && (EXI_DECODE_ISO_SALES_TARIFF == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_SelectedServiceList
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SELECTED_SERVICE_LIST) && (EXI_DECODE_ISO_SELECTED_SERVICE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_SelectedServiceList( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_SelectedServiceListType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_SelectedServiceListType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_SelectedServiceType) lastSelectedService;
  #if (defined(EXI_DECODE_ISO_SELECTED_SERVICE) && (EXI_DECODE_ISO_SELECTED_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  uint16_least i;
  #endif /* #if (defined(EXI_DECODE_ISO_SELECTED_SERVICE) && (EXI_DECODE_ISO_SELECTED_SERVICE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_SelectedServiceListType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_SelectedServiceListType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element SelectedService, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(SelectedService) */
    {
      #if (defined(EXI_DECODE_ISO_SELECTED_SERVICE) && (EXI_DECODE_ISO_SELECTED_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_SelectedService(DecWsPtr, &(structPtr->SelectedService));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SELECTED_SERVICE_LIST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SELECTED_SERVICE) && (EXI_DECODE_ISO_SELECTED_SERVICE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->SelectedService->NextSelectedServicePtr = (Exi_ISO_SelectedServiceType*)NULL_PTR;
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastSelectedService = structPtr->SelectedService;
    #if (defined(EXI_DECODE_ISO_SELECTED_SERVICE) && (EXI_DECODE_ISO_SELECTED_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #40 Decode subsequent occurences of SelectedService */
    for(i=0; i<15; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* SE(SelectedService) */
      {
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_SelectedService(DecWsPtr, &(lastSelectedService->NextSelectedServicePtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastSelectedService = lastSelectedService->NextSelectedServicePtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(SelectedService) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(SelectedService) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* (defined(EXI_DECODE_ISO_SELECTED_SERVICE) && (EXI_DECODE_ISO_SELECTED_SERVICE == STD_ON)) */
    lastSelectedService->NextSelectedServicePtr = (Exi_ISO_SelectedServiceType*)NULL_PTR;

  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SELECTED_SERVICE_LIST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SELECTED_SERVICE_LIST) && (EXI_DECODE_ISO_SELECTED_SERVICE_LIST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_SelectedService
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SELECTED_SERVICE) && (EXI_DECODE_ISO_SELECTED_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_SelectedService( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_SelectedServiceType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_SelectedServiceType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_SelectedServiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_SelectedServiceType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element ServiceID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ServiceID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt16(&DecWsPtr->DecWs, &structPtr->ServiceID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ServiceID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element ParameterSetID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ParameterSetID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->ParameterSetIDFlag = 1;
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->ParameterSetID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ParameterSetIDFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(ParameterSetID) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SELECTED_SERVICE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SELECTED_SERVICE) && (EXI_DECODE_ISO_SELECTED_SERVICE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ServiceDetailReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SERVICE_DETAIL_REQ) && (EXI_DECODE_ISO_SERVICE_DETAIL_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ServiceDetailReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ServiceDetailReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ServiceDetailReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ServiceDetailReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ServiceDetailReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_SERVICE_DETAIL_REQ_TYPE);
    /* #40 Decode element ServiceID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ServiceID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt16(&DecWsPtr->DecWs, &structPtr->ServiceID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ServiceID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 If ServiceDetailReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(ServiceDetailReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SERVICE_DETAIL_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SERVICE_DETAIL_REQ) && (EXI_DECODE_ISO_SERVICE_DETAIL_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ServiceDetailRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SERVICE_DETAIL_RES) && (EXI_DECODE_ISO_SERVICE_DETAIL_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ServiceDetailRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ServiceDetailResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ServiceDetailResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ServiceDetailResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ServiceDetailResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_SERVICE_DETAIL_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE_DETAIL_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element ServiceID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ServiceID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt16(&DecWsPtr->DecWs, &structPtr->ServiceID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ServiceID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element ServiceParameterList */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ServiceParameterList) */
    {
      #if (defined(EXI_DECODE_ISO_SERVICE_PARAMETER_LIST) && (EXI_DECODE_ISO_SERVICE_PARAMETER_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_ServiceParameterList(DecWsPtr, &(structPtr->ServiceParameterList));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ServiceParameterListFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE_DETAIL_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE_PARAMETER_LIST) && (EXI_DECODE_ISO_SERVICE_PARAMETER_LIST == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ServiceParameterListFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #80 If ServiceDetailRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->ServiceParameterListFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)/* EE(ServiceDetailRes) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(ServiceDetailRes) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SERVICE_DETAIL_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SERVICE_DETAIL_RES) && (EXI_DECODE_ISO_SERVICE_DETAIL_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ServiceDiscoveryReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SERVICE_DISCOVERY_REQ) && (EXI_DECODE_ISO_SERVICE_DISCOVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ServiceDiscoveryReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ServiceDiscoveryReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ServiceDiscoveryReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ServiceDiscoveryReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ServiceDiscoveryReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_SERVICE_DISCOVERY_REQ_TYPE);
    /* #40 Decode element ServiceScope */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ServiceScope) */
    {
      #if (defined(EXI_DECODE_ISO_SERVICE_SCOPE) && (EXI_DECODE_ISO_SERVICE_SCOPE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_serviceScope(DecWsPtr, &(structPtr->ServiceScope));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ServiceScopeFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE_DISCOVERY_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE_SCOPE) && (EXI_DECODE_ISO_SERVICE_SCOPE == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(ServiceCategory) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->ServiceCategoryFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ServiceScopeFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(ServiceScope) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #50 Decode optional element ServiceCategory with missing elements in front */
    if((0 == structPtr->ServiceScopeFlag) && (1 == structPtr->ServiceCategoryFlag))
    {
      /* SE(ServiceCategory) already decoded */
    #if (defined(EXI_DECODE_ISO_SERVICE_CATEGORY) && (EXI_DECODE_ISO_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* Decode enumeration value */
      Exi_Decode_ISO_serviceCategory(DecWsPtr, &structPtr->ServiceCategory);
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE_DISCOVERY_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_SERVICE_CATEGORY) && (EXI_DECODE_ISO_SERVICE_CATEGORY == STD_ON)) */
    }
    /* #60 Decode optional element ServiceCategory without missing elements in front */
    else if(1 == structPtr->ServiceScopeFlag)
    {
      /* #70 Decode element ServiceCategory */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(ServiceCategory) */
      {
      #if (defined(EXI_DECODE_ISO_SERVICE_CATEGORY) && (EXI_DECODE_ISO_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* #80 Decode serviceCategory as enumeration value */
        Exi_Decode_ISO_serviceCategory(DecWsPtr, &structPtr->ServiceCategory);
        structPtr->ServiceCategoryFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE_DISCOVERY_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE_CATEGORY) && (EXI_DECODE_ISO_SERVICE_CATEGORY == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->ServiceCategoryFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(ServiceCategory) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #90 If ServiceDiscoveryReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->ServiceCategoryFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)/* EE(ServiceDiscoveryReq) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(ServiceDiscoveryReq) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SERVICE_DISCOVERY_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SERVICE_DISCOVERY_REQ) && (EXI_DECODE_ISO_SERVICE_DISCOVERY_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ServiceDiscoveryRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SERVICE_DISCOVERY_RES) && (EXI_DECODE_ISO_SERVICE_DISCOVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ServiceDiscoveryRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ServiceDiscoveryResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ServiceDiscoveryResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ServiceDiscoveryResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ServiceDiscoveryResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_SERVICE_DISCOVERY_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE_DISCOVERY_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element PaymentOptionList */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(PaymentOptionList) */
    {
      #if (defined(EXI_DECODE_ISO_PAYMENT_OPTION_LIST) && (EXI_DECODE_ISO_PAYMENT_OPTION_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_PaymentOptionList(DecWsPtr, &(structPtr->PaymentOptionList));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE_DISCOVERY_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PAYMENT_OPTION_LIST) && (EXI_DECODE_ISO_PAYMENT_OPTION_LIST == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #70 Decode element ChargeService */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ChargeService) */
    {
      #if (defined(EXI_DECODE_ISO_CHARGE_SERVICE) && (EXI_DECODE_ISO_CHARGE_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_ChargeService(DecWsPtr, &(structPtr->ChargeService));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE_DISCOVERY_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_CHARGE_SERVICE) && (EXI_DECODE_ISO_CHARGE_SERVICE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ChargeService) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode element ServiceList */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ServiceList) */
    {
      #if (defined(EXI_DECODE_ISO_SERVICE_LIST) && (EXI_DECODE_ISO_SERVICE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_ServiceList(DecWsPtr, &(structPtr->ServiceList));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ServiceListFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE_DISCOVERY_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE_LIST) && (EXI_DECODE_ISO_SERVICE_LIST == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ServiceListFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #90 If ServiceDiscoveryRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->ServiceListFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)/* EE(ServiceDiscoveryRes) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(ServiceDiscoveryRes) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SERVICE_DISCOVERY_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SERVICE_DISCOVERY_RES) && (EXI_DECODE_ISO_SERVICE_DISCOVERY_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ServiceList
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SERVICE_LIST) && (EXI_DECODE_ISO_SERVICE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ServiceList( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ServiceListType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ServiceListType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_ServiceType) lastService;
  #if (defined(EXI_DECODE_ISO_SERVICE) && (EXI_DECODE_ISO_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  uint16_least i;
  #endif /* #if (defined(EXI_DECODE_ISO_SERVICE) && (EXI_DECODE_ISO_SERVICE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ServiceListType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ServiceListType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element Service, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(Service) */
    {
      #if (defined(EXI_DECODE_ISO_SERVICE) && (EXI_DECODE_ISO_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_Service(DecWsPtr, &(structPtr->Service));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE_LIST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE) && (EXI_DECODE_ISO_SERVICE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->Service->NextServicePtr = (Exi_ISO_ServiceType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(Service) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastService = structPtr->Service;
    #if (defined(EXI_DECODE_ISO_SERVICE) && (EXI_DECODE_ISO_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #40 Decode subsequent occurences of Service */
    for(i=0; i<7; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* SE(Service) */
      {
        Exi_Decode_ISO_Service(DecWsPtr, &(lastService->NextServicePtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastService = lastService->NextServicePtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(Service) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* (defined(EXI_DECODE_ISO_SERVICE) && (EXI_DECODE_ISO_SERVICE == STD_ON)) */
    lastService->NextServicePtr = (Exi_ISO_ServiceType*)NULL_PTR;

  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SERVICE_LIST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SERVICE_LIST) && (EXI_DECODE_ISO_SERVICE_LIST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_ServiceParameterList
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SERVICE_PARAMETER_LIST) && (EXI_DECODE_ISO_SERVICE_PARAMETER_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_ServiceParameterList( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ServiceParameterListType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ServiceParameterListType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_ParameterSetType) lastParameterSet;
  #if (defined(EXI_DECODE_ISO_PARAMETER_SET) && (EXI_DECODE_ISO_PARAMETER_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  uint16_least i;
  #endif /* #if (defined(EXI_DECODE_ISO_PARAMETER_SET) && (EXI_DECODE_ISO_PARAMETER_SET == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ServiceParameterListType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ServiceParameterListType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element ParameterSet, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ParameterSet) */
    {
      #if (defined(EXI_DECODE_ISO_PARAMETER_SET) && (EXI_DECODE_ISO_PARAMETER_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_ParameterSet(DecWsPtr, &(structPtr->ParameterSet));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE_PARAMETER_LIST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PARAMETER_SET) && (EXI_DECODE_ISO_PARAMETER_SET == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->ParameterSet->NextParameterSetPtr = (Exi_ISO_ParameterSetType*)NULL_PTR;
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastParameterSet = structPtr->ParameterSet;
    #if (defined(EXI_DECODE_ISO_PARAMETER_SET) && (EXI_DECODE_ISO_PARAMETER_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #40 Decode subsequent occurences of ParameterSet */
    for(i=0; i<254; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* SE(ParameterSet) */
      {
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_ISO_ParameterSet(DecWsPtr, &(lastParameterSet->NextParameterSetPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastParameterSet = lastParameterSet->NextParameterSetPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(ParameterSet) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(ParameterSet) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* (defined(EXI_DECODE_ISO_PARAMETER_SET) && (EXI_DECODE_ISO_PARAMETER_SET == STD_ON)) */
    lastParameterSet->NextParameterSetPtr = (Exi_ISO_ParameterSetType*)NULL_PTR;

  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SERVICE_PARAMETER_LIST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SERVICE_PARAMETER_LIST) && (EXI_DECODE_ISO_SERVICE_PARAMETER_LIST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_Service
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SERVICE) && (EXI_DECODE_ISO_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_Service( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_ServiceType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_ServiceType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_ServiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_ServiceType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element ServiceID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ServiceID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt16(&DecWsPtr->DecWs, &structPtr->ServiceID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ServiceID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element ServiceName */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ServiceName) */
    {
      #if (defined(EXI_DECODE_ISO_SERVICE_NAME) && (EXI_DECODE_ISO_SERVICE_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_serviceName(DecWsPtr, &(structPtr->ServiceName));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ServiceNameFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE_NAME) && (EXI_DECODE_ISO_SERVICE_NAME == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ServiceNameFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(ServiceName) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #50 Decode element ServiceCategory with missing elements in front */
    if(0 == structPtr->ServiceNameFlag)
    {
      /* SE(ServiceCategory) already decoded */
    #if (defined(EXI_DECODE_ISO_SERVICE_CATEGORY) && (EXI_DECODE_ISO_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* Decode enumeration value */
      Exi_Decode_ISO_serviceCategory(DecWsPtr, &structPtr->ServiceCategory);
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_ISO_SERVICE_CATEGORY) && (EXI_DECODE_ISO_SERVICE_CATEGORY == STD_ON)) */
    }
    /* #60 Decode element ServiceCategory without missing elements in front */
    else
    {
      /* #70 Decode element ServiceCategory */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(ServiceCategory) */
      {
      #if (defined(EXI_DECODE_ISO_SERVICE_CATEGORY) && (EXI_DECODE_ISO_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* #80 Decode serviceCategory as enumeration value */
        Exi_Decode_ISO_serviceCategory(DecWsPtr, &structPtr->ServiceCategory);
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE_CATEGORY) && (EXI_DECODE_ISO_SERVICE_CATEGORY == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ServiceCategory) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode element ServiceScope */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ServiceScope) */
    {
      #if (defined(EXI_DECODE_ISO_SERVICE_SCOPE) && (EXI_DECODE_ISO_SERVICE_SCOPE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_serviceScope(DecWsPtr, &(structPtr->ServiceScope));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ServiceScopeFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SERVICE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_SERVICE_SCOPE) && (EXI_DECODE_ISO_SERVICE_SCOPE == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ServiceScopeFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(ServiceScope) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #100 Decode element FreeService with missing elements in front */
    if(0 == structPtr->ServiceScopeFlag)
    {
      /* SE(FreeService) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->FreeService);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #110 Decode element FreeService without missing elements in front */
    else
    {
      /* #120 Decode element FreeService */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode) /* SE(FreeService) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)
        {
          Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->FreeService);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(FreeService) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SERVICE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SERVICE) && (EXI_DECODE_ISO_SERVICE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_SessionSetupReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SESSION_SETUP_REQ) && (EXI_DECODE_ISO_SESSION_SETUP_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_SessionSetupReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_SessionSetupReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_SessionSetupReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_SessionSetupReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_SessionSetupReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_SESSION_SETUP_REQ_TYPE);
    /* #40 Decode element EVCCID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVCCID) */
    {
      #if (defined(EXI_DECODE_ISO_EVCC_ID) && (EXI_DECODE_ISO_EVCC_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_evccID(DecWsPtr, &(structPtr->EVCCID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SESSION_SETUP_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_EVCC_ID) && (EXI_DECODE_ISO_EVCC_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVCCID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 If SessionSetupReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(SessionSetupReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SESSION_SETUP_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SESSION_SETUP_REQ) && (EXI_DECODE_ISO_SESSION_SETUP_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_SessionSetupRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SESSION_SETUP_RES) && (EXI_DECODE_ISO_SESSION_SETUP_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_SessionSetupRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_SessionSetupResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_SessionSetupResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_SessionSetupResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_SessionSetupResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_SESSION_SETUP_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SESSION_SETUP_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVSEID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEID) */
    {
      #if (defined(EXI_DECODE_ISO_EVSE_ID) && (EXI_DECODE_ISO_EVSE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_evseID(DecWsPtr, &(structPtr->EVSEID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SESSION_SETUP_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_EVSE_ID) && (EXI_DECODE_ISO_EVSE_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVSETimeStamp */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSETimeStamp) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->EVSETimeStampFlag = 1;
        Exi_VBSDecodeInt64(&DecWsPtr->DecWs, &structPtr->EVSETimeStamp);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVSETimeStampFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      /* check exiEventCode equals EE(EVSETimeStamp) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #80 If SessionSetupRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->EVSETimeStampFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
        if(0 == exiEventCode)/* EE(SessionSetupRes) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(SessionSetupRes) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SESSION_SETUP_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SESSION_SETUP_RES) && (EXI_DECODE_ISO_SESSION_SETUP_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_SessionStopReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SESSION_STOP_REQ) && (EXI_DECODE_ISO_SESSION_STOP_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_SessionStopReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_SessionStopReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_SessionStopReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_SessionStopReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_SessionStopReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_SESSION_STOP_REQ_TYPE);
    /* #40 Decode element ChargingSession */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ChargingSession) */
    {
      #if (defined(EXI_DECODE_ISO_CHARGING_SESSION) && (EXI_DECODE_ISO_CHARGING_SESSION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode chargingSession as enumeration value */
      Exi_Decode_ISO_chargingSession(DecWsPtr, &structPtr->ChargingSession);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SESSION_STOP_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_CHARGING_SESSION) && (EXI_DECODE_ISO_CHARGING_SESSION == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ChargingSession) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 If SessionStopReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(SessionStopReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SESSION_STOP_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SESSION_STOP_REQ) && (EXI_DECODE_ISO_SESSION_STOP_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_SessionStopRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SESSION_STOP_RES) && (EXI_DECODE_ISO_SESSION_STOP_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_SessionStopRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_SessionStopResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_SessionStopResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_SessionStopResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_SessionStopResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_SESSION_STOP_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SESSION_STOP_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 If SessionStopRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(SessionStopRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SESSION_STOP_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SESSION_STOP_RES) && (EXI_DECODE_ISO_SESSION_STOP_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_SubCertificates
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SUB_CERTIFICATES) && (EXI_DECODE_ISO_SUB_CERTIFICATES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_SubCertificates( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_SubCertificatesType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_SubCertificatesType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_certificateType) lastCertificate;
  #if (defined(EXI_DECODE_ISO_CERTIFICATE) && (EXI_DECODE_ISO_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  uint16_least i;
  #endif /* #if (defined(EXI_DECODE_ISO_CERTIFICATE) && (EXI_DECODE_ISO_CERTIFICATE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_SubCertificatesType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_SubCertificatesType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element Certificate, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(Certificate) */
    {
      #if (defined(EXI_DECODE_ISO_CERTIFICATE) && (EXI_DECODE_ISO_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_certificate(DecWsPtr, &(structPtr->Certificate));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SUB_CERTIFICATES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE) && (EXI_DECODE_ISO_CERTIFICATE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->Certificate->NextCertificatePtr = (Exi_ISO_certificateType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(Certificate) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastCertificate = structPtr->Certificate;
    #if (defined(EXI_DECODE_ISO_CERTIFICATE) && (EXI_DECODE_ISO_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #40 Decode subsequent occurences of Certificate */
    for(i=0; i<3; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* SE(Certificate) */
      {
        Exi_Decode_ISO_certificate(DecWsPtr, &(lastCertificate->NextCertificatePtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastCertificate = lastCertificate->NextCertificatePtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(Certificate) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* (defined(EXI_DECODE_ISO_CERTIFICATE) && (EXI_DECODE_ISO_CERTIFICATE == STD_ON)) */
    lastCertificate->NextCertificatePtr = (Exi_ISO_certificateType*)NULL_PTR;

  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SUB_CERTIFICATES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SUB_CERTIFICATES) && (EXI_DECODE_ISO_SUB_CERTIFICATES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_SupportedEnergyTransferMode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE) && (EXI_DECODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_SupportedEnergyTransferMode( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_SupportedEnergyTransferModeType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_SupportedEnergyTransferModeType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  uint16_least i;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_SupportedEnergyTransferModeType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_SupportedEnergyTransferModeType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element EnergyTransferMode, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EnergyTransferMode) */
    {
      #if (defined(EXI_DECODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_DECODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #40 Decode first element of EnergyTransferMode as a multi occurence enumeration value */
      Exi_Decode_ISO_EnergyTransferMode(DecWsPtr, &structPtr->EnergyTransferMode[0]);
      structPtr->EnergyTransferModeCount = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_DECODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EnergyTransferMode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    /* #50 Decode subsequent occurences of EnergyTransferMode */
    for(i=0; i<5; i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* SE(EnergyTransferMode) */
      {
      #if (defined(EXI_DECODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_DECODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* Decode a subsequent element of a multi occurence enumeration value */
        Exi_Decode_ISO_EnergyTransferMode(DecWsPtr, &structPtr->EnergyTransferMode[i + 1]);
        structPtr->EnergyTransferModeCount++;
      #endif /* (defined(EXI_DECODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_DECODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) */
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(EnergyTransferMode) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }


  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE) && (EXI_DECODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_V2G_Message
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_V2G_MESSAGE) && (EXI_DECODE_ISO_V2G_MESSAGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_V2G_Message( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_V2G_MessageType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_V2G_MessageType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_V2G_MessageType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_V2G_MessageType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_V2G_MESSAGE_TYPE);
    /* #40 Decode element Header */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(Header) */
    {
      #if (defined(EXI_DECODE_ISO_MESSAGE_HEADER) && (EXI_DECODE_ISO_MESSAGE_HEADER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_MessageHeader(DecWsPtr, &(structPtr->Header));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_V2G_MESSAGE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_MESSAGE_HEADER) && (EXI_DECODE_ISO_MESSAGE_HEADER == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #50 Decode element Body */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(Body) */
    {
      #if (defined(EXI_DECODE_ISO_BODY) && (EXI_DECODE_ISO_BODY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_ISO_Body(DecWsPtr, &(structPtr->Body));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_V2G_MESSAGE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_BODY) && (EXI_DECODE_ISO_BODY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_ISO_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #60 If V2G_Message was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(V2G_Message) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_V2G_MESSAGE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_V2G_MESSAGE) && (EXI_DECODE_ISO_V2G_MESSAGE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_WeldingDetectionReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_WELDING_DETECTION_REQ) && (EXI_DECODE_ISO_WELDING_DETECTION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_WeldingDetectionReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_WeldingDetectionReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_WeldingDetectionReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_WeldingDetectionReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_WeldingDetectionReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_WELDING_DETECTION_REQ_TYPE);
    /* #40 Decode element DC_EVStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVStatus) */
    {
      #if (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_DC_EVStatus(DecWsPtr, &(structPtr->DC_EVStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_WELDING_DETECTION_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVSTATUS) && (EXI_DECODE_ISO_DC_EVSTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(DC_EVStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 If WeldingDetectionReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(WeldingDetectionReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_WELDING_DETECTION_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_WELDING_DETECTION_REQ) && (EXI_DECODE_ISO_WELDING_DETECTION_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_WeldingDetectionRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_WELDING_DETECTION_RES) && (EXI_DECODE_ISO_WELDING_DETECTION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_WeldingDetectionRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_WeldingDetectionResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_WeldingDetectionResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_WeldingDetectionResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_WeldingDetectionResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_ISO_WELDING_DETECTION_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_ISO_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_WELDING_DETECTION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element DC_EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVSEStatus) */
    {
      #if (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_DC_EVSEStatus(DecWsPtr, &(structPtr->DC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_WELDING_DETECTION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_DC_EVSESTATUS) && (EXI_DECODE_ISO_DC_EVSESTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(DC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVSEPresentVoltage */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode) /* SE(EVSEPresentVoltage) */
    {
      #if (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_ISO_PhysicalValue(DecWsPtr, &(structPtr->EVSEPresentVoltage));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_ISO_WELDING_DETECTION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_ISO_PHYSICAL_VALUE) && (EXI_DECODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    /* check exiEventCode equals EE(EVSEPresentVoltage) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 If WeldingDetectionRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_ISO_TYPE);
      if(0 == exiEventCode)/* EE(WeldingDetectionRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_WELDING_DETECTION_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_WELDING_DETECTION_RES) && (EXI_DECODE_ISO_WELDING_DETECTION_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_certificate
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CERTIFICATE) && (EXI_DECODE_ISO_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_certificate( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_certificateType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_certificateType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_certificateType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_certificateType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode certificate as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CERTIFICATE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CERTIFICATE) && (EXI_DECODE_ISO_CERTIFICATE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_chargeProgress
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CHARGE_PROGRESS) && (EXI_DECODE_ISO_CHARGE_PROGRESS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_chargeProgress( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_chargeProgressType, AUTOMATIC, EXI_APPL_VAR) chargeProgressPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (chargeProgressPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value chargeProgress element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 3)
      {
        /* #40 Store value in output buffer */
        *chargeProgressPtr = (Exi_ISO_chargeProgressType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CHARGE_PROGRESS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CHARGE_PROGRESS) && (EXI_DECODE_ISO_CHARGE_PROGRESS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_chargingSession
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_CHARGING_SESSION) && (EXI_DECODE_ISO_CHARGING_SESSION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_chargingSession( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_chargingSessionType, AUTOMATIC, EXI_APPL_VAR) chargingSessionPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (chargingSessionPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value chargingSession element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 2)
      {
        /* #40 Store value in output buffer */
        *chargingSessionPtr = (Exi_ISO_chargingSessionType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_CHARGING_SESSION, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_CHARGING_SESSION) && (EXI_DECODE_ISO_CHARGING_SESSION == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_costKind
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_COST_KIND) && (EXI_DECODE_ISO_COST_KIND == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_costKind( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_costKindType, AUTOMATIC, EXI_APPL_VAR) costKindPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (costKindPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value costKind element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 3)
      {
        /* #40 Store value in output buffer */
        *costKindPtr = (Exi_ISO_costKindType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_COST_KIND, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_COST_KIND) && (EXI_DECODE_ISO_COST_KIND == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_eMAID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_E_MAID) && (EXI_DECODE_ISO_E_MAID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_eMAID( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_eMAIDType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_eMAIDType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_eMAIDType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_eMAIDType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode eMAID as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_E_MAID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_E_MAID) && (EXI_DECODE_ISO_E_MAID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_evccID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_EVCC_ID) && (EXI_DECODE_ISO_EVCC_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_evccID( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_evccIDType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_evccIDType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_evccIDType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_evccIDType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode evccID as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_EVCC_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_EVCC_ID) && (EXI_DECODE_ISO_EVCC_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_evseID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_EVSE_ID) && (EXI_DECODE_ISO_EVSE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_evseID( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_evseIDType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_evseIDType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_evseIDType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_evseIDType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode evseID as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_EVSE_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_EVSE_ID) && (EXI_DECODE_ISO_EVSE_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_faultCode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_FAULT_CODE) && (EXI_DECODE_ISO_FAULT_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_faultCode( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_faultCodeType, AUTOMATIC, EXI_APPL_VAR) faultCodePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (faultCodePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value faultCode element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 3)
      {
        /* #40 Store value in output buffer */
        *faultCodePtr = (Exi_ISO_faultCodeType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_FAULT_CODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_FAULT_CODE) && (EXI_DECODE_ISO_FAULT_CODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_faultMsg
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_FAULT_MSG) && (EXI_DECODE_ISO_FAULT_MSG == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_faultMsg( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_faultMsgType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_faultMsgType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_faultMsgType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_faultMsgType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode faultMsg as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_FAULT_MSG, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_FAULT_MSG) && (EXI_DECODE_ISO_FAULT_MSG == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_genChallenge
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_GEN_CHALLENGE) && (EXI_DECODE_ISO_GEN_CHALLENGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_genChallenge( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_genChallengeType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_genChallengeType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_genChallengeType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_genChallengeType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode genChallenge as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_GEN_CHALLENGE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_GEN_CHALLENGE) && (EXI_DECODE_ISO_GEN_CHALLENGE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_isolationLevel
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_ISOLATION_LEVEL) && (EXI_DECODE_ISO_ISOLATION_LEVEL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_isolationLevel( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_isolationLevelType, AUTOMATIC, EXI_APPL_VAR) isolationLevelPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (isolationLevelPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value isolationLevel element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 5)
      {
        /* #40 Store value in output buffer */
        *isolationLevelPtr = (Exi_ISO_isolationLevelType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_ISOLATION_LEVEL, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_ISOLATION_LEVEL) && (EXI_DECODE_ISO_ISOLATION_LEVEL == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_meterID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_METER_ID) && (EXI_DECODE_ISO_METER_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_meterID( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_meterIDType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_meterIDType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_meterIDType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_meterIDType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode meterID as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_METER_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_METER_ID) && (EXI_DECODE_ISO_METER_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_paymentOption
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_PAYMENT_OPTION) && (EXI_DECODE_ISO_PAYMENT_OPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_paymentOption( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_paymentOptionType, AUTOMATIC, EXI_APPL_VAR) paymentOptionPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (paymentOptionPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value paymentOption element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 2)
      {
        /* #40 Store value in output buffer */
        *paymentOptionPtr = (Exi_ISO_paymentOptionType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_PAYMENT_OPTION, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_PAYMENT_OPTION) && (EXI_DECODE_ISO_PAYMENT_OPTION == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_responseCode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_responseCode( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_responseCodeType, AUTOMATIC, EXI_APPL_VAR) responseCodePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (responseCodePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value responseCode element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 5);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 26)
      {
        /* #40 Store value in output buffer */
        *responseCodePtr = (Exi_ISO_responseCodeType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_RESPONSE_CODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_RESPONSE_CODE) && (EXI_DECODE_ISO_RESPONSE_CODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_serviceCategory
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SERVICE_CATEGORY) && (EXI_DECODE_ISO_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_serviceCategory( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_serviceCategoryType, AUTOMATIC, EXI_APPL_VAR) serviceCategoryPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (serviceCategoryPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value serviceCategory element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 4)
      {
        /* #40 Store value in output buffer */
        *serviceCategoryPtr = (Exi_ISO_serviceCategoryType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SERVICE_CATEGORY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SERVICE_CATEGORY) && (EXI_DECODE_ISO_SERVICE_CATEGORY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_serviceName
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SERVICE_NAME) && (EXI_DECODE_ISO_SERVICE_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_serviceName( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_serviceNameType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_serviceNameType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_serviceNameType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_serviceNameType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode serviceName as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SERVICE_NAME, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SERVICE_NAME) && (EXI_DECODE_ISO_SERVICE_NAME == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_serviceScope
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SERVICE_SCOPE) && (EXI_DECODE_ISO_SERVICE_SCOPE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_serviceScope( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_serviceScopeType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_serviceScopeType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_serviceScopeType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_serviceScopeType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode serviceScope as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SERVICE_SCOPE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SERVICE_SCOPE) && (EXI_DECODE_ISO_SERVICE_SCOPE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_sessionID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SESSION_ID) && (EXI_DECODE_ISO_SESSION_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_sessionID( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_sessionIDType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_sessionIDType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_sessionIDType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_sessionIDType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode sessionID as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SESSION_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SESSION_ID) && (EXI_DECODE_ISO_SESSION_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_sigMeterReading
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_SIG_METER_READING) && (EXI_DECODE_ISO_SIG_METER_READING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_sigMeterReading( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_sigMeterReadingType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_sigMeterReadingType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_sigMeterReadingType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_sigMeterReadingType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode sigMeterReading as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_SIG_METER_READING, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_SIG_METER_READING) && (EXI_DECODE_ISO_SIG_METER_READING == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_tariffDescription
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_TARIFF_DESCRIPTION) && (EXI_DECODE_ISO_TARIFF_DESCRIPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_tariffDescription( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_tariffDescriptionType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_ISO_tariffDescriptionType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_ISO_tariffDescriptionType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_ISO_tariffDescriptionType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_ISO_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode tariffDescription as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_TARIFF_DESCRIPTION, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_TARIFF_DESCRIPTION) && (EXI_DECODE_ISO_TARIFF_DESCRIPTION == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_ISO_unitSymbol
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_ISO_UNIT_SYMBOL) && (EXI_DECODE_ISO_UNIT_SYMBOL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_ISO_unitSymbol( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_ISO_unitSymbolType, AUTOMATIC, EXI_APPL_VAR) unitSymbolPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (unitSymbolPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value unitSymbol element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 7)
      {
        /* #40 Store value in output buffer */
        *unitSymbolPtr = (Exi_ISO_unitSymbolType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_ISO_UNIT_SYMBOL, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_ISO_UNIT_SYMBOL) && (EXI_DECODE_ISO_UNIT_SYMBOL == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_SchemaSet_ISO
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_SCHEMA_SET_ISO) && (EXI_DECODE_SCHEMA_SET_ISO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_SchemaSet_ISO( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(void, AUTOMATIC, EXI_APPL_VAR) structPtr = NULL_PTR;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode all ISO schema elements */
    exiEventCode = 0;
    /* #30 Decode the event code of the element */
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 7);
    /* #40 Switch event code */
    switch(exiEventCode)
    {
    case 76: /* SE(V2G_Message) */
      /* #50 Element V2G_Message */
      {
        /* #60 Decode element V2G_Message */
      #if (defined(EXI_DECODE_ISO_V2G_MESSAGE) && (EXI_DECODE_ISO_V2G_MESSAGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_ISO_V2G_Message(DecWsPtr, (Exi_ISO_V2G_MessageType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_ISO, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_ISO_V2G_MESSAGE) && (EXI_DECODE_ISO_V2G_MESSAGE == STD_ON)) */
      }
    default:
      /* #70 Unknown element */
      {
        /* #80 Set status code to error */
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_ISO, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_SCHEMA_SET_ISO) && (EXI_DECODE_SCHEMA_SET_ISO == STD_ON)) */


#define EXI_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/* PRQA L:NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */

#endif /* (defined (EXI_ENABLE_DECODE_ISO_MESSAGE_SET) && (EXI_ENABLE_DECODE_ISO_MESSAGE_SET == STD_ON)) */

