/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* STARTSINGLE_OF_MULTIPLE */

/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*  \file     Can.c / Can_Irq.c / Can_Local.h
 *  \brief    Implementation of the CAN driver
 *  \details  see functional description below
 *
 *********************************************************************************************************************/
/* ***************************************************************************
|-----------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-----------------------------------------------------------------------------
| Initials     Name                      Company
| --------     ---------------------     -------------------------------------
| Ht           Heike Honert              Vector Informatik GmbH
| Pl           Georg Pfluegel            Vector Informatik GmbH
| Bir          Holger Birke              Vector Informatik GmbH
| Rna          Ruediger Naas             Vector Informatik GmbH
| Hzo          Han Zhao                  Vector Informatik GmbH
| Bns          Benjamin Schuetterle      Vector Informatik GmbH
|-----------------------------------------------------------------------------
|               R E V I S I O N   H I S T O R Y
|-----------------------------------------------------------------------------
| Date       Ver   Author  Description
| ---------  ---  ------  ----------------------------------------------------
| Refer to the module's header file.
|************************************************************************** */

/***************************************************************************/
/* Include files                                                           */
/***************************************************************************/
/* ECO_IGNORE_BLOCK_BEGIN */
#define C_DRV_INTERNAL
#define CAN_IRQ_SOURCE     /* testability */

#include "Can.h"
#if defined(V_OSTYPE_OSEK) /* COV_CAN_OS_USAGE */
# include "osek.h"
#endif
/* \trace SPEC-1408 */
#if defined(V_OSTYPE_AUTOSAR) /* COV_CAN_OS_USAGE */
# include "Os.h"
#endif


/* ECO_IGNORE_BLOCK_END */

/***************************************************************************/
/* Version Check                                                           */
/***************************************************************************/
/* \trace SPEC-20329 */
/* not the SW version but all file versions that represent the SW version are checked */
#if (CAN_COREVERSION           != 0x0900u) /* \trace SPEC-1699, SPEC-3837 */
# error "Header file are inconsistent!"
#endif
#if (CAN_RELEASE_COREVERSION   != 0x00u)
# error "Header file are inconsistent!"
#endif
#if( DRVCAN_TRICOREMULTICANASR_VERSION != 0x0503u)
# error "Source and Header file are inconsistent!"
#endif
#if( DRVCAN_TRICOREMULTICANASR_RELEASE_VERSION != 0x00u)
# error "Source and Header file are inconsistent!"
#endif

# if( CAN_GEN_TricoreMulticanAsr_VERSION != 0x0101u)
#  error "Generated Data are inconsistent!"
# endif

#if defined(CAN_ENABLE_USE_INIT_ROOT_POINTER)
# define CAN_START_SEC_VAR_INIT_UNSPECIFIED  /*-----------------------------*/
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
V_DEF_P2CONST (extern, Can_ConfigType, VAR_INIT, CONST_PBCFG) Can_ConfigDataPtr; /* PRQA S 3447,3451,3210 */ /* MD_Can_ExternalScope */
# define CAN_STOP_SEC_VAR_INIT_UNSPECIFIED  /*------------------------------*/
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#endif

/***************************************************************************/
/* Interrupt Service Routine                                               */
/***************************************************************************/
#define CAN_START_SEC_CODE  /*-----------------------------------------*/
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* \trace SPEC-1579, SPEC-1395, SPEC-1567 */
/* ISR functions */
#if defined( C_LL_INTERRUPT_ISR_ACTIVE )
/* **************************************************************************
| NAME:             CanIsr_0
| CALLED BY:        HLL, ASR: Interrupt
| PRECONDITIONS:
| INPUT PARAMETERS: none
| RETURN VALUES:    none
| DESCRIPTION:      Interrupt service functions according to the CAN controller
|                   interrupt structure
|
|  Attention:  <Name> has to be replaced with the name of the ISR.
|              Naming conventions: with Name = "", "Rx", "Tx", "RxTx", "Wakeup", "Status"
|  The name of the ISR will always have a channel index at the end. Even in single channel
|  systems.
************************************************************************** */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_0
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_0 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_0 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_0(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_0(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(0));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_0);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_0) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_1
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_1 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_1 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_1(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_1(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(1));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_1);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_1) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_2
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_2 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_2 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_2(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_2(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(2));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_2);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_2) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_3
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_3 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_3 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_3(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_3(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(3));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_3);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_3) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_4
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_4 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_4 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_4(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_4(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(4));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_4);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_4) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_5
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_5 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_5 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_5(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_5(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(5));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_5);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_5) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_6
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_6 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_6 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_6(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_6(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(6));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_6);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_6) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_7
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_7 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_7 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_7(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_7(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(7));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_7);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_7) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_8
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_8 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_8 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_8(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_8(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(8));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_8);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_8) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_9
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_9 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_9 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_9(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_9(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(9));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_9);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_9) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_10
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_10 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_10 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_10(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_10(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(10));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_10);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_10) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_11
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_11 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_11 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_11(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_11(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(11));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_11);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_11) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_12
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_12 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_12 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_12(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_12(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(12));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_12);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_12) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_13
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_13 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_13 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_13(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_13(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(13));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_13);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_13) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_14
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_14 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_14 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_14(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_14(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(14));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_14);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_14) */
/* CODE CATEGORY 1 END */
/* CODE CATEGORY 1 START */
/****************************************************************************
| NAME:             CanIsr_15
****************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
# if defined( kCanPhysToLogChannelIndex_15 )  /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
#  if defined( C_ENABLE_OSEK_OS_INTCAT2 )
ISR( CanIsr_15 )
#  else /* CAT1 or VOID */
#   if defined(C_ENABLE_ISRVOID)
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_15(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#   else /* CAT1 */
#    if defined( C_COMP_GNU_TRICORE_MULTICAN ) /* COV_CAN_DERIVATIVE_SWITCH */
V_DEF_FUNC(V_NONE, void, CODE) CanIsr_15(void) /* PRQA S 3408 */ /* MD_Can_ExternalScope */
#    endif
#   endif /* CAT1 */
#  endif /* CAT1 or VOID */
{
  /* #10 Call interrupt handler */
#  if defined( CAN_USE_PHYSTOLOG_MAPPING ) /* COV_CAN_EQUAL_CHANNEL_LAYOUT */
  CanInterrupt(Can_GetPhysToLogChannel(15));  /* call Interrupt handling with identity depended logical channel */
#  else
  CanInterrupt(kCanPhysToLogChannelIndex_15);     /* call Interrupt handling with logical channel */
#  endif

} /* END OF CanISR */
# endif /* (kCanPhysToLogChannelIndex_15) */
/* CODE CATEGORY 1 END */

#endif  /* Not a pure polling configuration */

#define CAN_STOP_SEC_CODE  /*------------------------------------------*/
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

/* Kernbauer Version: 1.14 Konfiguration: DrvCAN Erzeugungsgangnummer: 910 */

