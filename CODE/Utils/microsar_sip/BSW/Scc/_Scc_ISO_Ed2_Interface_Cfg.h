#if !defined(SCC_WPT_INTERFACE_CFG_H)
#define SCC_WPT_INTERFACE_CFG_H

/***********************************************************************************************************************
 *  Scc_Interface_Cfg.h
 **********************************************************************************************************************/

 /**********************************************************************************************************************
*  FILE REQUIRES USER MODIFICATIONS
*  Template Scope: whole file
*  -------------------------------------------------------------------------------------------------------------------
*  This file includes template code that must be completed and/or adapted during BSW integration.
*  The template code is incomplete and only intended for providing a signature and an empty implementation. It is
*  neither intended nor qualified for use in series production without applying suitable quality measures. The template
*  code must be completed as described in the instructions given within this file and/or in the Technical Reference.
*  The completed implementation must be tested with diligent care and must comply with all quality requirements which
*  are necessary according to the state of the art before its use.
*********************************************************************************************************************/

/* -------------------------- ISO ED2 CD2 Interface Config--------------------------------*/

/* StateM */
#define Scc_Get_StateM_SelectedVASList(DataPtr)                                     Appl_SccCbk_Get_StateM_SelectedVASList(DataPtr)
#define Scc_Get_StateM_EVProcessing(DataPtr)                                        Appl_SccCbk_Get_ISO_Ed2_DIS_EVProcessing(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_EVProcessing(DataPtr)                                   Scc_StateM_Set_ISO_Ed2_DIS_EVProcessing(DataPtr)
#define Scc_Set_StateM_EnergyTransferServiceIDFlags(Data)                           Appl_SccCbk_Set_StateM_EnergyTransferServiceIDFlags(Data)
#define Scc_Get_StateM_EnergyTransferServiceIDFlags(DataPtr)                        Appl_SccCbk_Get_StateM_EnergyTransferServiceIDFlags(DataPtr)
#define Scc_Set_StateM_VasIdFlags(Data)                                             Appl_SccCbk_Set_StateM_VasIdFlags(Data)
#define Scc_Get_StateM_RemainingContractCertificateChains(DataPtr)                  Appl_SccCbk_Get_StateM_RemainingContractCertificateChains(DataPtr)
/* General */
#define Scc_Set_ISO_Ed2_DIS_EVSEStatus(DataPtr, Flag)                               Appl_SccCbk_Set_ISO_Ed2_DIS_EVSEStatus(DataPtr, Flag)
#define Scc_Set_ISO_Ed2_DIS_ResponseCode(Data)                                      Appl_SccCbk_Set_ISO_Ed2_DIS_ResponseCode(Data)
#define Scc_Set_ISO_Ed2_DIS_EVSEProcessing(Data)                                    Scc_StateM_Set_ISO_Ed2_DIS_EVSEProcessing(Data)

/* SessionSetup */
#define Scc_Set_ISO_Ed2_DIS_SessionID(DataPtr)                                      Appl_SccCbk_Set_ISO_Ed2_DIS_SessionID(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_EVSEID(DataPtr)                                         Appl_SccCbk_Set_ISO_Ed2_DIS_EVSEID(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_EVSETimeStamp(Data, Flag)                               Appl_SccCbk_Set_ISO_Ed2_DIS_EVSETimeStamp(Data, Flag)

/* ServiceDiscovery */
#define Scc_Get_ISO_Ed2_DIS_SupportedServiceIDs(DataPtr, Flag)                      Appl_SccCbk_Get_ISO_Ed2_DIS_SupportedServiceIDs(DataPtr, Flag)
#define Scc_Set_ISO_Ed2_DIS_IdentificationOptionList(DataPtr)                       { Scc_StateM_Set_ISO_Ed2_DIS_IdentificationOptionList(DataPtr); Appl_SccCbk_Set_ISO_Ed2_DIS_IdentificationOptionList(DataPtr); }
#define Scc_Set_ISO_Ed2_DIS_EnergyTransferServiceList(DataPtr)                      { Scc_StateM_Set_ISO_Ed2_DIS_EnergyTransferServiceList(DataPtr); Appl_SccCbk_Set_ISO_Ed2_DIS_EnergyTransferServiceList(DataPtr); }
#define Scc_Set_ISO_Ed2_DIS_VASList(DataPtr, Flag)                                  Scc_StateM_Set_ISO_Ed2_DIS_VASList(DataPtr, Flag)

/* ServiceDetail */
#define Scc_Get_ISO_Ed2_DIS_ServiceID(DataPtr)                                      Scc_StateM_Get_ISO_Ed2_DIS_ServiceID(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_ServiceID(Data)                                         { Scc_StateM_Set_ISO_Ed2_DIS_ServiceID(Data); Appl_SccCbk_Set_ISO_Ed2_DIS_ServiceID(Data);  }
#define Scc_Set_ISO_Ed2_DIS_ServiceParameterList(DataPtr, Flag)                     { Scc_StateM_Set_ISO_Ed2_DIS_ServiceParameterList(DataPtr, Flag); Appl_SccCbk_Set_ISO_Ed2_DIS_ServiceParameterList(DataPtr, Flag);  }

/* PaymentServiceSelection */
#define Scc_Get_ISO_Ed2_DIS_SelectedIdentificationOption(DataPtr)                   Scc_StateM_Get_ISO_Ed2_DIS_SelectedIdentificationOption(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_SelectedEnergyTransferService(DataPtr)                  Scc_StateM_Get_ISO_Ed2_DIS_SelectedEnergyTransferService(DataPtr)
#define Scc_Get_StateM_SelectedEnergyTransferService(DataPtr)                       Appl_SccCbk_Get_ISO_Ed2_DIS_SelectedEnergyTransferService(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_SelectedVASListPtr(DataPtr, Flag)                       Scc_StateM_Get_ISO_Ed2_DIS_SelectedVASListPtr(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_SelectedVasIdFlags(DataPtr)                             Scc_StateM_Get_ISO_Ed2_DIS_SelectedVasIdFlags(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_SelectedVasList(DataPtr)                                Appl_SccCbk_Get_ISO_Ed2_DIS_SelectedVasList(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_SelectedControlMode(DataPtr)                            Appl_SccCbk_Get_ISO_Ed2_DIS_SelectedControlMode(DataPtr)
/* CertificateInstallation */
#define Scc_Get_ISO_Ed2_DIS_PnC_CurrentTime(DataPtr)                                Appl_SccCbk_Get_ISO_Ed2_DIS_PnC_CurrentTime(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_MaximumSupportedCertificateChains(DataPtr)              Appl_SccCbk_Get_ISO_Ed2_DIS_MaximumSupportedCertificateChains(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_RemainingContractCertificateChains(DataPtr)             Appl_SccCbk_Set_ISO_Ed2_DIS_RemainingContractCertificateChains(DataPtr)
/* FinePositioningSetup */
#define Scc_Get_ISO_Ed2_DIS_WPT_EVFinePositioningSetupParametersPtr(DataPtr, Flag)  Appl_SccCbk_Get_ISO_Ed2_DIS_LFA_EVFinePositioningSetupParametersPtr(DataPtr, Flag)
#define Scc_Set_ISO_Ed2_DIS_WPT_EVSEFinePositioningSetupParameters(DataPtr, Flag)   Appl_SccCbk_Set_ISO_Ed2_DIS_WPT_EVSEFinePositioningSetupParameters(DataPtr, Flag)

/* FinePositioning */
#define Scc_Get_ISO_Ed2_DIS_EVProcessing(DataPtr)                                   Scc_StateM_Get_ISO_Ed2_DIS_EVProcessing(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_WPT_EVFinePositioningParametersPtr(DataPtr, Flag)       Appl_SccCbk_Get_ISO_Ed2_DIS_LFA_EVFinePositioningParametersPtr(DataPtr, Flag)
#define Scc_Set_ISO_Ed2_DIS_WPT_EVSEFinePositioningParameters(DataPtr, Flag)        Appl_SccCbk_Set_ISO_Ed2_DIS_WPT_EVSEFinePositioningParameters(DataPtr, Flag)

/* PairingRes */
#define Scc_Get_ISO_Ed2_DIS_WPT_EVPairingParametersPtr(DataPtr, Flag)               Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVPairingParametersPtr(DataPtr, Flag)
#define Scc_Set_ISO_Ed2_DIS_WPT_EVSEPairingParameters(DataPtr, Flag)                Appl_SccCbk_Set_ISO_Ed2_DIS_WPT_EVSEPairingParameters(DataPtr, Flag)

/* Authorization */

/* ChargeParameterDiscovery */
#define Scc_Get_ISO_Ed2_DIS_MaxSupportingPoints(DataPtr, Flag)                      Appl_SccCbk_Get_ISO_Ed2_DIS_MaxSupportingPoints(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_DepartureTime(DataPtr, Flag)                            Appl_SccCbk_Get_ISO_Ed2_DIS_DepartureTime(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_ScheduleList(DataPtr, Flag)                             Appl_SccCbk_Get_ISO_Ed2_DIS_ScheduleList(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_StandByMode(DataPtr)                                    Appl_SccCbk_Get_ISO_Ed2_DIS_StandByMode(DataPtr)

/* AC */
#define Scc_Get_ISO_Ed2_DIS_AC_EVMaximumChargeCurrent(DataPtr, Flag)                Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVMaximumChargeCurrent(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_AC_EVMaximumChargePower(DataPtr, Flag)                  Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVMaximumChargePower(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_AC_EVMaximumEnergyRequest(DataPtr, Flag)                Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVMaximumEnergyRequest(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_AC_EVMaximumVoltage(DataPtr)                            Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVMaximumVoltage(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_AC_EVMinimumChargeCurrent(DataPtr, Flag)                Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVMinimumChargeCurrent(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_AC_EVMinimumEnergyRequest(DataPtr, Flag)                Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVMinimumEnergyRequest(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_AC_EVTargetEnergyRequest(DataPtr, Flag)                 Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVTargetEnergyRequest(DataPtr, Flag)

#define Scc_Set_ISO_Ed2_DIS_AC_EVSENominalVoltage(DataPtr)                          Appl_SccCbk_Set_ISO_Ed2_DIS_AC_EVSENominalVoltage(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_AC_EVSEMaximumChargeCurrent(DatatPtr)                   Appl_SccCbk_Set_ISO_Ed2_DIS_AC_EVSEMaximumChargeCurrent(DatatPtr)
#define Scc_Set_ISO_Ed2_DIS_AC_EVSENominalFrequency(DataPtr)                        Appl_SccCbk_Set_ISO_Ed2_DIS_AC_EVSENominalFrequency(DataPtr)

/* AC_BPT */
#define Scc_Get_ISO_Ed2_DIS_BPT_AC_EVMaximumDischargePower(DataPtr)                 Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_AC_EVMaximumDischargePower(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_BPT_AC_EVMinimumDischargePower(DataPtr)                 Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_AC_EVMinimumDischargePower(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_BPT_AC_EVMaximumDischargeCurrent(DataPtr)               Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_AC_EVMaximumDischargeCurrent(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_BPT_AC_EVMinimumDischargeCurrent(DataPtr)               Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_AC_EVMinimumDischargeCurrent(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_BPT_AC_EVSEMaximumDischargeCurrent(DataPtr)             Appl_SccCbk_Set_ISO_Ed2_DIS_BPT_AC_EVSEMaximumDischargeCurrent(DataPtr)

/* DC_BPT */
#define Scc_Get_ISO_Ed2_DIS_BPT_DC_EVMaximumDischargePower(DataPtr, Flag)           Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_DC_EVMaximumDischargePower(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_BPT_DC_EVMinimumDischargePower(DataPtr, Flag)           Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_DC_EVMinimumDischargePower(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_BPT_DC_EVMaximumDischargeCurrent(DataPtr)               Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_DC_EVMaximumDischargeCurrent(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_BPT_DC_EVMinimumDischargeCurrent(DataPtr)               Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_DC_EVMinimumDischargeCurrent(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_DC_EVMinimumVoltage(DataPtr)                            Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_DC_EVMinimumVoltage(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_DC_BidirectionalControlRes(DataPtr)                     Appl_SccCbk_Set_ISO_Ed2_DIS_BPT_DC_BidirectionalControlRes(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_BPT_DC_EVSEMaximumDischargePower(DataPtr)               Appl_SccCbk_Set_ISO_Ed2_DIS_BPT_DC_EVSEMaximumDischargePower(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_BPT_DC_EVSEMaximumDischargeCurrent(DataPtr)             Appl_SccCbk_Set_ISO_Ed2_DIS_BPT_DC_EVSEMaximumDischargeCurrent(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_BPT_DC_EVSEMinimumDischargeCurrent(DataPtr)             Appl_SccCbk_Set_ISO_Ed2_DIS_BPT_DC_EVSEMinimumDischargeCurrent(DataPtr)

/* DC */
#define Scc_Get_ISO_Ed2_DIS_DC_EVMaximumChargeCurrent(DataPtr, Flag)                Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVMaximumChargeCurrent(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_DC_EVMaximumChargePower(DataPtr, Flag)                  Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVMaximumChargePower(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_DC_EVMaximumEnergyRequest(DataPtr, Flag)                Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVMaximumEnergyRequest(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_DC_EVMaximumVoltage(DataPtr, Flag)                      Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVMaximumVoltage(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_DC_EVMinimumChargeCurrent(DataPtr)                      Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVMinimumChargeCurrent(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_DC_EVMinimumEnergyRequest(DataPtr, Flag)                Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVMinimumEnergyRequest(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_DC_EVMinimumChargePower(DataPtr, Flag)                  Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVMinimumChargePower(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_DC_EVTargetEnergyRequest(DataPtr, Flag)                 Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVTargetEnergyRequest(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_DC_TargetSOC(DataPtr, Flag)                             Appl_SccCbk_Get_ISO_Ed2_DIS_DC_TargetSOC(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_DC_BulkSOC(DataPtr, Flag)                               Appl_SccCbk_Get_ISO_Ed2_DIS_DC_BulkSOC(DataPtr, Flag)

#define Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumChargePower(DataPtr, Flag)                Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEMaximumChargePower(DataPtr, Flag)
#define Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumChargeCurrent(DatatPtr, Flag)             Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEMaximumChargeCurrent(DatatPtr, Flag)
#define Scc_Set_ISO_Ed2_DIS_DC_EVSEMinimumChargeCurrent(DatatPtr)                   Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEMinimumChargeCurrent(DatatPtr)
#define Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumVoltage(DataPtr, Flag)                    Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEMaximumVoltage(DataPtr, Flag)
#define Scc_Set_ISO_Ed2_DIS_DC_EVSEMinimumVoltage(DataPtr)                          Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEMinimumVoltage(DataPtr)

/* WPT */
#define Scc_Get_ISO_Ed2_DIS_WPT_EVMaximumEnergyRequest(DataPtr, Flag)               Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVMaximumEnergyRequest(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_WPT_EVMinimumEnergyRequest(DataPtr, Flag)               Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVMinimumEnergyRequest(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_WPT_EVTargetEnergyRequest(DataPtr, Flag)                Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVTargetEnergyRequest(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_WPT_EVMaximumPower(DataPtr, Flag)                       Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVMaximumPower(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_WPT_EVMinimumPower(DataPtr, Flag)                       Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVMinimumPower(DataPtr, Flag)

#define Scc_Set_ISO_Ed2_DIS_ScheduleList(DataPtr)                                   Appl_SccCbk_Set_ISO_Ed2_DIS_ScheduleList(DataPtr)

#define Scc_Set_ISO_Ed2_DIS_WPT_EVSEMaximumPower(DataPtr)                           Appl_SccCbk_Set_ISO_Ed2_DIS_WPT_EVSEMaximumPower(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_WPT_EVSEMinimumPower(DataPtr)                           Appl_SccCbk_Set_ISO_Ed2_DIS_WPT_EVSEMinimumPower(DataPtr)

/* InitialAlignmentCheck */
#define Scc_Get_ISO_Ed2_DIS_WPT_EVAlignmentCheckMechanismPtr(DataPtr, Flag)         Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVAlignmentCheckMechanismPtr(DataPtr, Flag)
#define Scc_Set_ISO_Ed2_DIS_WPT_EVSEAlignmentCheckMechanism(DataPtr, Flag)          Appl_SccCbk_Set_ISO_Ed2_DIS_WPT_EVSEAlignmentCheckMechanism(DataPtr, Flag)

/* Precharge */
#define Scc_Get_ISO_Ed2_DIS_DC_EVTargetVoltage(DataPtr)                             Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVTargetVoltage(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_DC_EVTargetCurrent(DataPtr)                             Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVTargetCurrent(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_DC_EVSEPresentVoltage(DataPtr)                          Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEPresentVoltage(DataPtr)

/* PowerDelivery */
#define Scc_Get_ISO_Ed2_DIS_ChargeProgress(DataPtr)                                 Scc_StateM_Get_ISO_Ed2_DIS_ChargeProgress(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_EVOperation(DataPtr, Flag)                              Appl_SccCbk_Get_ISO_Ed2_DIS_EVOperation(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_ScheduleTupleID(DataPtr, Flag)                          Appl_SccCbk_Get_ISO_Ed2_DIS_ScheduleTupleID(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_ChargingProfilePtr(DataPtr, Flag)                       Appl_SccCbk_Get_ISO_Ed2_DIS_ChargingProfilePtr(DataPtr, Flag)

/* CurrentDemand */
#define Scc_Get_ISO_Ed2_DIS_MeteringReceiptRequested(DataPtr, Flag)                 Appl_SccCbk_Get_ISO_Ed2_DIS_PnC_ReceiptRequired(DataPtr, Flag)
#define Scc_Set_ISO_Ed2_DIS_DC_EVSEPresentCurrent(DataPtr)                          Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEPresentCurrent(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_DC_EVSEPowerLimitAchieved(DataPtr)                      Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEPowerLimitAchieved(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_DC_EVSECurrentLimitAchieved(DataPtr)                    Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSECurrentLimitAchieved(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_DC_EVSEVoltageLimitAchieved(DataPtr)                    Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEVoltageLimitAchieved(DataPtr)

/* Charging Status */
//#define Scc_Get_ISO_Ed2_DIS_AC_EVTargetEnergyRequest(DataPtr, Flag)                 Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVTargetEnergyRequest(DataPtr, Flag)
//#define Scc_Get_ISO_Ed2_DIS_AC_EVMaximumEnergyRequest(DataPtr, Flag)                Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVMaximumEnergyRequest(DataPtr, Flag)
//#define Scc_Get_ISO_Ed2_DIS_AC_EVMinimumEnergyRequest(DataPtr, Flag)                Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVMinimumEnergyRequest(DataPtr, Flag)

#define Scc_Set_ISO_Ed2_DIS_EVSETargetFrequency(DataPtr, Flag)                      Appl_SccCbk_Set_ISO_Ed2_DIS_EVSETargetFrequency(DataPtr, Flag)
#define Scc_Set_ISO_Ed2_DIS_EVSETargetActivePower(DataPtr, Flag)                    Appl_SccCbk_Set_ISO_Ed2_DIS_EVSETargetActivePower(DataPtr, Flag)
#define Scc_Set_ISO_Ed2_DIS_EVSETargetReactivePower(DataPtr, Flag)                  Appl_SccCbk_Set_ISO_Ed2_DIS_EVSETargetReactivePower(DataPtr, Flag)

/* PowerDemand */
#define Scc_Get_ISO_Ed2_DIS_WPT_EVTargetEnergyRequest(DataPtr, Flag)                Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVTargetEnergyRequest(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_WPT_EVMaximumEnergyRequest(DataPtr, Flag)               Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVMaximumEnergyRequest(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_WPT_EVMinimumEnergyRequest(DataPtr, Flag)               Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVMinimumEnergyRequest(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_WPT_EVTargetPower(DataPtr)                              Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVTargetPower(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_WPT_EVReceivedDCPower(DataPtr)                          Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVReceivedDCPower(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_WPT_PowerDemandParametersPtr(DataPtr, Flag)             Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_PowerDemandParametersPtr(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_WPT_GACoilCurrent(DataPtr)                              Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_GACoilCurrent(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_WPT_GAFrequency(DataPtr)                                Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_GAFrequency(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_WPT_VAPowerReceived(DataPtr)                            Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_VAPowerReceived(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_WPT_VAVoltageToEV(DataPtr)                              Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_VAVoltageToEV(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_WPT_VACurrentToEV(DataPtr)                              Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_VACurrentToEV(DataPtr)

#define Scc_Set_ISO_Ed2_DIS_WPT_EVSEOutputPower(DataPtr)                            Appl_SccCbk_Set_ISO_Ed2_DIS_WPT_EVSEOutputPower(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_MeterInfo(DataPtr, Flag)                                Appl_SccCbk_Set_ISO_Ed2_DIS_MeterInfo(DataPtr, Flag)
#define Scc_Set_ISO_Ed2_DIS_WPT_PowerDemandParameters(DataPtr, Flag)                Appl_SccCbk_Set_ISO_Ed2_DIS_WPT_PowerDemandParameters(DataPtr, Flag)
#define Scc_Get_ISO_Ed2_DIS_DisplayParameters(DataPtr, Flag)                        Appl_SccCbk_Get_ISO_Ed2_DIS_DisplayParameters(DataPtr, Flag)
#define Scc_Set_ISO_Ed2_DIS_EVSEDCPrimaryPower(DataPtr)                             Appl_SccCbk_Set_ISO_Ed2_DIS_EVSEDCPrimaryPower(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_EVRequestedPower(DataPtr)                               Appl_SccCbk_Set_ISO_Ed2_DIS_EVRequestedPower(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_EVSEPowerLimit(DataPtr)                                 Appl_SccCbk_Set_ISO_Ed2_DIS_EVSEPowerLimit(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_ForeignObjectDetected(DataPtr)                          Appl_SccCbk_Set_ISO_Ed2_DIS_ForeignObjectDetected(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_LivingObjectDetected(DataPtr)                           Appl_SccCbk_Set_ISO_Ed2_DIS_LivingObjectDetected(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_GAInputPower(DataPtr)                                   Appl_SccCbk_Set_ISO_Ed2_DIS_GAInputPower(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_GACoilCurrent(DataPtr)                                  Appl_SccCbk_Set_ISO_Ed2_DIS_GACoilCurrent(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_GAFrequency(DataPtr)                                    Appl_SccCbk_Set_ISO_Ed2_DIS_GAFrequency(DataPtr)
#define Scc_Set_ISO_Ed2_DIS_PowerDemandParameters(DataPtr)                          Appl_SccCbk_Set_ISO_Ed2_DIS_PowerDemandParameters(DataPtr)

/* SessionStop */
#define Scc_Get_ISO_Ed2_DIS_ChargingSession(DataPtr)                                Appl_SccCbk_Get_ISO_Ed2_DIS_ChargingSession(DataPtr)

/* MeteringReceipt */
#define Scc_Get_ISO_Ed2_DIS_MeterInfo(DataPtr, Flag)                                Appl_SccCbk_Get_ISO_Ed2_DIS_MeterInfo(DataPtr, Flag)

#define Scc_Get_ISO_Ed2_DIS_Scheduled_ACBCReqControlMode(DataPtr)                   Appl_SccCbk_Get_ISO_Ed2_DIS_Scheduled_ACBCReqControlMode(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_Dynamic_ACBCReqControlMode(DataPtr)                     Appl_SccCbk_Get_ISO_Ed2_DIS_Dynamic_ACBCReqControlMode(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_Scheduled_DCBCReqControlMode(DataPtr)                   Appl_SccCbk_Get_ISO_Ed2_DIS_Scheduled_DCBCReqControlMode(DataPtr)
#define Scc_Get_ISO_Ed2_DIS_Dynamic_DCBCReqControlMode(DataPtr)                     Appl_SccCbk_Get_ISO_Ed2_DIS_Dynamic_DCBCReqControlMode(DataPtr)

#endif /* SCC_WPT_INTERFACE_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Scc_Cfg.h
 *********************************************************************************************************************/
