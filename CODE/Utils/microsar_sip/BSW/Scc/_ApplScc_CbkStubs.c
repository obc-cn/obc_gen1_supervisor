/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  _ApplScc_CbkStubs.c
 *        \brief  Smart Charging Communication Stub Code File
 *
 *      \details  Provides callback stubs for the parameter interface.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *********************************************************************************************************************/

#define SCC_CBK_STUBS_SOURCE

/**********************************************************************************************************************
*  FILE REQUIRES USER MODIFICATIONS
*  Template Scope: whole file
*  -------------------------------------------------------------------------------------------------------------------
*  This file includes template code that must be completed and/or adapted during BSW integration.
*  The template code is incomplete and only intended for providing a signature and an empty implementation. It is
*  neither intended nor qualified for use in series production without applying suitable quality measures. The template
*  code must be completed as described in the instructions given within this file and/or in the Technical Reference.
*  The completed implementation must be tested with diligent care and must comply with all quality requirements which
*  are necessary according to the state of the art before its use.
*********************************************************************************************************************/

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
/* PRQA S 0777,3206 EOF */ /* MD_MSR_Rule5.1,MD_Scc_CallbackStubs  */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "_ApplScc_CbkStubs.h"

/* Callbacks */

/* Rx Core */
void Appl_SccCbk_Get_Core_CyclicMsgTrigRx(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) CyclicMsgTrig)
{
  return;
}
void Appl_SccCbk_Get_Core_MsgTrig(P2VAR(Scc_MsgTrigType, AUTOMATIC, SCC_APPL_DATA) MsgTrig)
{
  return;
}
void Appl_SccCbk_Get_Core_SDPSecurityRx(P2VAR(Scc_SDPSecurityType, AUTOMATIC, SCC_APPL_DATA) SDPSecurity)
{
  return;
}
void Appl_SccCbk_Get_Core_ForceSAPSchema(P2VAR(Scc_ForceSAPSchemasType, AUTOMATIC, SCC_APPL_DATA) ForceSAPSchema)
{
  return;
}

/* Tx Core */
void Appl_SccCbk_Set_Core_CyclicMsgRcvd(boolean CyclicMsgRcvd)
{
  return;
}
void Appl_SccCbk_Set_Core_CyclicMsgTrigTx(boolean CyclicMsgTrig)
{
  return;
}
void Appl_SccCbk_Set_Core_IPAssigned(boolean IPAssigned)
{
  return;
}
void Appl_SccCbk_Set_Core_MsgState(Scc_MsgStateType MsgState)
{
  return;
}
void Appl_SccCbk_Set_Core_MsgStatus(Scc_MsgStatusType MsgStatus)
{
  return;
}
void Appl_SccCbk_Set_Core_SAPResponseCode(Exi_SAP_responseCodeType SAPResponseCode)
{
  return;
}
void Appl_SccCbk_Set_Core_SAPSchemaID(Scc_SAPSchemaIDType SAPSchemaID)
{
  return;
}
void Appl_SccCbk_Set_Core_SDPTransportProtocol(Scc_SDPTransportProtocolType SDPTransportProtocol)
{
  return;
}
void Appl_SccCbk_Set_Core_SDPSecurityTx(Scc_SDPSecurityType SDPSecurityTx)
{
  return;
}
void Appl_SccCbk_Set_Core_SECCIPAddress(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) SECCIPAddress)
{
  return;
}
void Appl_SccCbk_Set_Core_SECCPort(uint16 SECCPort)
{
  return;
}
void Appl_SccCbk_Set_Core_StackError(Scc_StackErrorType StackError)
{
  return;
}
void Appl_SccCbk_Set_Core_TrcvLinkState(boolean TrcvLinkState)
{
  return;
}
void Appl_SccCbk_Set_Core_TCPSocketState(Scc_TCPSocketStateType TCPSocketStateType)
{
  return;
}
void Appl_SccCbk_Set_Core_TLS_CertChain(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) TLS_CertChain, P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) validationResultPtr)
{
  return;
}
void Appl_SccCbk_Set_Core_TLS_UnknownCALeafCert(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) TLS_UnknownCALeafCert)
{
  return;
}
void Appl_SccCbk_Set_Core_V2GResponse(P2CONST(Scc_TxRxBufferPointerType, AUTOMATIC, SCC_APPL_DATA) V2GResponse)
{
  return;
}
void Appl_SccCbk_Set_Core_V2GRequest(P2CONST(Scc_TxRxBufferPointerType, AUTOMATIC, SCC_APPL_DATA) V2GRequest)
{
  return;
}

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )

/* Rx SLAC */
void Appl_SccCbk_Get_SLAC_QCAIdleTimer(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) QCAIdleTimer)
{
  return;
}
void Appl_SccCbk_Get_SLAC_StartMode(P2VAR(EthTrcv_30_Ar7000_Slac_StartModeType, AUTOMATIC, SCC_APPL_DATA) SLACStartMode)
{
  return;
}

/* Tx SLAC */
void Appl_SccCbk_Set_SLAC_AssociationStatus(uint8 AssociationStatus)
{
  return;
}
void Appl_SccCbk_Set_SLAC_ToggleRequest(uint8 ToogleNum)
{
  return;
}
void Appl_SccCbk_Set_SLAC_NMK(P2CONST(Scc_BufferPointerType, AUTOMATIC, SCC_APPL_DATA) NMKPtr)
{
  return;
}
void Appl_SccCbk_Set_SLAC_NID(P2CONST(Scc_BufferPointerType, AUTOMATIC, SCC_APPL_DATA) NIDPtr)
{
  return;
}

#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_STATE_MACHINE == STD_ON )

/* Rx StateM */
void Appl_SccCbk_Get_StateM_EnergyTransferServiceIDFlags(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) TxEnergyTransferServiceIDFlags)
{
  return;
}
void Appl_SccCbk_Get_StateM_ChargingControl(P2VAR(Scc_StateM_ChargingControlType, AUTOMATIC, SCC_APPL_DATA) ChargingControl)
{
  return;
}
void Appl_SccCbk_Get_StateM_EnergyTransferMode(P2VAR(Scc_StateM_EnergyTransferModeType, AUTOMATIC, SCC_APPL_DATA) EnergyTransferMode)
{
  return;
}
void Appl_SccCbk_Get_StateM_EVTimeStamp(P2VAR(uint32, AUTOMATIC, SCC_APPL_DATA) EVTimeStamp)
{
  return;
}
void Appl_SccCbk_Get_StateM_FlowControl(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) FlowControl)
{
  return;
}
void Appl_SccCbk_Get_StateM_FunctionControl(P2VAR(Scc_StateM_FunctionControlType, AUTOMATIC, SCC_APPL_DATA) FunctionControl)
{
  return;
}
void Appl_SccCbk_Get_StateM_ControlPilotState(P2VAR(Scc_StateM_ControlPilotStateType, AUTOMATIC, SCC_APPL_DATA) ControlPilotState)
{
  return;
}
void Appl_SccCbk_Get_StateM_PWMState(P2VAR(Scc_StateM_PWMStateType, AUTOMATIC, SCC_APPL_DATA) PWMState)
{
  return;
}
void Appl_SccCbk_Get_StateM_RemainingContractCertificateChains(P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) RemainingContractCertificateChains)
{
  return;
}

/* Tx StateM */
void Appl_SccCbk_Set_StateM_EnergyTransferServiceIDFlags(uint16 RxEnergyTransferServiceIDFlags)
{
  return;
}
void Appl_SccCbk_Set_StateM_VasIdFlags(uint8 VasIdFlags)
{
  return;
}
void Appl_SccCbk_Set_StateM_EnergyTransferModeFlags(uint8 EnergyTransferModeFlags)
{
  return;
}
void Appl_SccCbk_Set_StateM_InternetAvailable(boolean InternetAvailable)
{
  return;
}
void Appl_SccCbk_Set_StateM_StateMachineError(Scc_StateM_StateMachineErrorType StateMachineError)
{
  return;
}
void Appl_SccCbk_Set_StateM_StateMachineMessageState(Scc_StateM_MsgStateType StateMachineMessageState)
{
  return;
}
void Appl_SccCbk_Set_StateM_StateMachineStatus(Scc_StateM_StateMachineStatusType StateMachineStatus)
{
  return;
}

#endif /* SCC_ENABLE_STATE_MACHINE */

#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )

/* Rx ISO */
void Appl_SccCbk_Get_ISO_ChargeProgress(P2VAR(Exi_ISO_chargeProgressType, AUTOMATIC, SCC_APPL_DATA) ChargeProgress)
{
  return;
}
void Appl_SccCbk_Get_ISO_ChargingSession(P2VAR(Scc_ISO_ChargingSessionType, AUTOMATIC, SCC_APPL_DATA) ChargingSession)
{
  return;
}
#if ( defined SCC_ENABLE_INTERNAL_CHARGING_PROFILE_BUFFER ) && ( SCC_ENABLE_INTERNAL_CHARGING_PROFILE_BUFFER == STD_ON )
void Appl_SccCbk_Get_ISO_ChargingProfilePtr(P2VAR(Scc_BufferPointerType, AUTOMATIC, SCC_APPL_DATA) ChargingProfilePtr, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
#else
void Appl_SccCbk_Get_ISO_ChargingProfilePtr(P2VAR(Exi_ISO_ChargingProfileType*, AUTOMATIC, SCC_APPL_DATA) ChargingProfilePtr, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
#endif /* SCC_ENABLE_INTERNAL_CHARGING_PROFILE_BUFFER */
{
  return;
}
void Appl_SccCbk_Get_ISO_DepartureTime(P2VAR(uint32, AUTOMATIC, SCC_APPL_DATA) DepartureTime, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_MaxEntriesSAScheduleTuple(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) MaxEntriesSAScheduleTuple, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_RequestedEnergyTransferMode(P2VAR(Exi_ISO_EnergyTransferModeType, AUTOMATIC, SCC_APPL_DATA) RequestedEnergyTransferMode)
{
  return;
}
void Appl_SccCbk_Get_ISO_SAScheduleTupleID(P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) SAScheduleTupleID)
{
  return;
}
void Appl_SccCbk_Get_ISO_SkipSAScheduleListCheck(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) skipSAScheduleListCheck)
{
  return;
}
void Appl_SccCbk_Get_ISO_PnC_SkipSalesTariffSignatureCheck(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) skipSalesTariffSignatureCheck)
{
  return;
}
void Appl_SccCbk_Get_ISO_SelectedPaymentOption(P2VAR(Exi_ISO_paymentOptionType, AUTOMATIC, SCC_APPL_DATA) SelectedPaymentOption)
{
  return;
}
void Appl_SccCbk_Get_ISO_SelectedServiceListPtr(P2VAR(Exi_ISO_SelectedServiceListType, AUTOMATIC, SCC_APPL_DATA) SelectedServiceListPtr)
{
  return;
}
void Appl_SccCbk_Get_ISO_ServiceCategory(P2VAR(Exi_ISO_serviceCategoryType, AUTOMATIC, SCC_APPL_DATA) ServiceCategory, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_ServiceIDRx(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) ServiceIDRx)
{
  return;
}
void Appl_SccCbk_Get_ISO_ServiceScope(P2VAR(Exi_ISO_serviceScopeType, AUTOMATIC, SCC_APPL_DATA) ServiceScope, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_PnC_CurrentTime(P2VAR(uint32, AUTOMATIC, SCC_APPL_DATA) CurrentTime)
{
  return;
}

void Appl_SccCbk_Get_ISO_AC_EAmount(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EAmount)
{
  return;
}
void Appl_SccCbk_Get_ISO_AC_EVMaxVoltage(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaxVoltage)
{
  return;
}
void Appl_SccCbk_Get_ISO_AC_EVMaxCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaxCurrent)
{
  return;
}
void Appl_SccCbk_Get_ISO_AC_EVMinCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMinCurrent)
{
  return;
}

void Appl_SccCbk_Get_ISO_DC_EVErrorCode(P2VAR(Exi_ISO_DC_EVErrorCodeType, AUTOMATIC, SCC_APPL_DATA) EVErrorCode)
{
  return;
}


/* Tx ISO */
void Appl_SccCbk_Set_ISO_ChargeService(P2CONST(Exi_ISO_ChargeServiceType, AUTOMATIC, SCC_APPL_DATA) ChargeService)
{
  return;
}
void Appl_SccCbk_Set_ISO_EVSEID(P2CONST(Exi_ISO_evseIDType, AUTOMATIC, SCC_APPL_DATA) EVSEID)
{
  return;
}
void Appl_SccCbk_Set_ISO_EVSENotification(Exi_ISO_EVSENotificationType EVSENotification)
{
  return;
}
void Appl_SccCbk_Set_ISO_EVSEProcessing(Exi_ISO_EVSEProcessingType EVSEProcessing)
{
  return;
}
void Appl_SccCbk_Set_ISO_EVSETimeStamp(Exi_SInt64 EVSETimeStamp, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_MeterInfo(P2CONST(Exi_ISO_MeterInfoType, AUTOMATIC, SCC_APPL_DATA) MeterInfo, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_Notification(P2CONST(Exi_ISO_NotificationType,  AUTOMATIC, SCC_APPL_DATA) Notification, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_NotificationMaxDelay(uint16 NotificationMaxDelay)
{
  return;
}
void Appl_SccCbk_Set_ISO_PaymentOptionList(P2CONST(Exi_ISO_PaymentOptionListType, AUTOMATIC, SCC_APPL_DATA) PaymentOptionList)
{
  return;
}
void Appl_SccCbk_Set_ISO_ResponseCode(Exi_ISO_responseCodeType ResponseCode)
{
  return;
}
void Appl_SccCbk_Set_ISO_SAScheduleList(P2CONST(Exi_ISO_SAScheduleListType, AUTOMATIC, SCC_APPL_DATA) SAScheduleList)
{
  return;
}
void Appl_SccCbk_Set_ISO_ServiceIDTx(uint16 ServiceID)
{
  return;
}
void Appl_SccCbk_Set_ISO_ServiceList(P2CONST(Exi_ISO_ServiceListType, AUTOMATIC, SCC_APPL_DATA) ServiceList, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_ServiceParameterList(P2CONST(Exi_ISO_ServiceParameterListType, AUTOMATIC, SCC_APPL_DATA) ServiceParameterList, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_PnC_ReceiptRequired(boolean ReceiptRequired, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_PnC_RetryCounter(sint16 RetryCounter, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_SessionID(P2CONST(Exi_ISO_sessionIDType, AUTOMATIC, SCC_APPL_DATA) SessionID)
{
  return;
}

void Appl_SccCbk_Set_ISO_AC_EVSEMaxCurrent(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEMaxCurrent, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_AC_EVSENominalVoltage(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSENominalVoltage)
{
  return;
}
void Appl_SccCbk_Set_ISO_AC_RCD(boolean RCD)
{
  return;
}

void Appl_SccCbk_Set_ISO_DC_EVSEIsolationStatus(Exi_ISO_isolationLevelType EVSEIsolationStatus, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_DC_EVSEStatusCode(Exi_ISO_DC_EVSEStatusCodeType EVSEStatusCode)
{
  return;
}

#endif /* SCC_SCHEMA_ISO */

#if ( (( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )) || (( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )) )

/* Rx IsoDin */
void Appl_SccCbk_Get_IsoDin_DC_BulkChargingComplete(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) BulkChargingComplete, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_IsoDin_DC_BulkSOC(P2VAR(sint8, AUTOMATIC, SCC_APPL_DATA) BulkSOC, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_IsoDin_DC_ChargingComplete(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) ChargingComplete)
{
  return;
}
void Appl_SccCbk_Get_IsoDin_DC_EVEnergyCapacity(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVEnergyCapacity, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_IsoDin_DC_EVEnergyRequest(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVEnergyRequest, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}

void Appl_SccCbk_Get_IsoDin_DC_EVMaximumCurrentLimit(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumCurrentLimit, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_IsoDin_DC_EVMaximumPowerLimit(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumPowerLimit, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_IsoDin_DC_EVMaximumVoltageLimit(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumVoltageLimit, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_IsoDin_DC_EVPowerDeliveryParameterFlag(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) EVPowerDeliveryParameterFlag)
{
  return;
}
void Appl_SccCbk_Get_IsoDin_DC_EVReady(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) EVReady)
{
  return;
}
void Appl_SccCbk_Get_IsoDin_DC_EVRESSSOC(P2VAR(sint8, AUTOMATIC, SCC_APPL_DATA) EVRESSSOC)
{
  return;
}
void Appl_SccCbk_Get_IsoDin_DC_EVTargetCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVTargetCurrent)
{
  return;
}
void Appl_SccCbk_Get_IsoDin_DC_EVTargetVoltage(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVTargetVoltage)
{
  return;
}
void Appl_SccCbk_Get_IsoDin_DC_FullSOC(P2VAR(sint8, AUTOMATIC, SCC_APPL_DATA) FullSOC, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_IsoDin_DC_RemainingTimeToBulkSoC(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) RemainingTimeToBulkSOC, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_IsoDin_DC_RemainingTimeToFullSoC(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) RemainingTimeToFullSOC, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}


/* Tx IsoDin */
void Appl_SccCbk_Set_IsoDin_DC_EVSECurrentLimitAchieved(boolean EVSECurrentLimitAchieved)
{
  return;
}
void Appl_SccCbk_Set_IsoDin_DC_EVSECurrentRegulationTolerance(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSECurrentRegulationTolerance, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_IsoDin_DC_EVSEEnergyToBeDelivered(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEEnergyToBeDelivered, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_IsoDin_DC_EVSEMaximumCurrentLimit(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEMaximumCurrentLimit, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_IsoDin_DC_EVSEMaximumPowerLimit(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEMaximumPowerLimit, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_IsoDin_DC_EVSEMaximumVoltageLimit(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEMaximumVoltageLimit, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_IsoDin_DC_EVSEMinimumCurrentLimit(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEMinimumCurrentLimit)
{
  return;
}
void Appl_SccCbk_Set_IsoDin_DC_EVSEMinimumVoltageLimit(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEMinimumVoltageLimit)
{
  return;
}
void Appl_SccCbk_Set_IsoDin_DC_EVSEPeakCurrentRipple(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEPeakCurrentRipple)
{
  return;
}
void Appl_SccCbk_Set_IsoDin_DC_EVSEPowerLimitAchieved(boolean EVSEPowerLimitAchieved)
{
  return;
}
void Appl_SccCbk_Set_IsoDin_DC_EVSEPresentCurrent(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEPresentCurrent)
{
  return;
}
void Appl_SccCbk_Set_IsoDin_DC_EVSEPresentVoltage(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEPresentVoltage)
{
  return;
}
void Appl_SccCbk_Set_IsoDin_DC_EVSEVoltageLimitAchieved(boolean EVSEVoltageLimitAchieved)
{
  return;
}

#endif /* SCC_SCHEMA_ISO || SCC_SCHEMA_DIN */


#if (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))

void Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVTargetEnergyRequest(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVTargetEnergyRequest, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVMaximumEnergyRequest(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumEnergyRequest, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVMinimumEnergyRequest(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMinimumEnergyRequest, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVMaximumChargePower(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumChargePower, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVMaximumVoltage(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumVoltage)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVMaximumChargeCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumChargeCurrent, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_AC_EVMinimumChargeCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMinimumChargeCurrent, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_AC_EVMaximumDischargePower(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumDischargePower)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_AC_EVMinimumDischargePower(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMinimumDischargePower)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_AC_EVMaximumDischargeCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumDischargeCurrent)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_AC_EVMinimumDischargeCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMinimumDischargeCurrent)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_DC_EVMaximumDischargePower(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumDischargePower, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_DC_EVMinimumDischargePower(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMinimumDischargePower, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_DC_EVMaximumDischargeCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumDischargeCurrent)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_DC_EVMinimumDischargeCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMinimumDischargeCurrent)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_BPT_DC_EVMinimumVoltage(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMinimumVoltage)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_ChargingProfilePtr(P2VAR(Exi_ISO_ED2_DIS_EVPowerProfileType *, AUTOMATIC, SCC_APPL_DATA) ChargingProfile, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_ChargingSession(P2VAR(Exi_ISO_ED2_DIS_chargingSessionType, AUTOMATIC, SCC_APPL_DATA) ChargingSession)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_DepartureTime(P2VAR(uint32, AUTOMATIC, SCC_APPL_DATA) DepartureTime, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_ScheduleList(P2VAR(Exi_ISO_ED2_DIS_ScheduleListType, AUTOMATIC, SCC_APPL_DATA) ScheduleList, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_StandByMode(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) StandByMode)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_DisplayParameters(P2VAR(Exi_ISO_ED2_DIS_DisplayParametersType, AUTOMATIC, SCC_APPL_DATA) DisplayParameters, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_EVOperation(P2VAR(Exi_ISO_ED2_DIS_evOperationType, AUTOMATIC, SCC_APPL_DATA) EVOperation, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_EVProcessing(P2VAR(Exi_ISO_ED2_DIS_processingType, AUTOMATIC, SCC_APPL_DATA) EVProcessing)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_LFA_EVFinePositioningSetupParametersPtr(P2VAR(Exi_ISO_ED2_DIS_EVFinePositioningSetupParametersType*, AUTOMATIC, SCC_APPL_DATA) FinePositioningSetupReqPtrPtr, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_LFA_EVFinePositioningParametersPtr(P2VAR(Exi_ISO_ED2_DIS_BodyBaseType *, AUTOMATIC, SCC_APPL_DATA) EVFinePositioningParametersPtr, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_MaxSupportingPoints(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) MaxSupportingPoints, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_SelectedEnergyTransferService(P2VAR(Exi_ISO_ED2_DIS_SelectedServiceType, AUTOMATIC, SCC_VAR_NOINIT) SelectedEnergyTransferService)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_SelectedControlMode(P2VAR(Scc_ISO_Ed2_ControlModeType, AUTOMATIC, SCC_VAR_NOINIT) SelectedControlMode)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_SelectedVasList(P2VAR(Exi_ISO_ED2_DIS_SelectedServiceListType, AUTOMATIC, SCC_APPL_DATA) SelectedVasList)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_SupportedServiceIDs(P2VAR(Exi_ISO_ED2_DIS_ServiceIDListType, AUTOMATIC, SCC_APPL_DATA) SupportedServiceIDs, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVAlignmentCheckMechanismPtr(P2VAR(Exi_ISO_ED2_DIS_ACResAlignmentCheckMechanismType *, AUTOMATIC, SCC_APPL_DATA) AlignmentCheckMechanism, P2VAR(Exi_RootElementIdType, AUTOMATIC, SCC_APPL_DATA) ElementId)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVReceivedDCPower(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVReceivedDCPower)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVMaximumEnergyRequest(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumEnergyRequest, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVMaximumPower(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumPower, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVMinimumEnergyRequest(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMinimumEnergyRequest, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVMinimumPower(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMinimumPower, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVTargetEnergyRequest(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVTargetEnergyRequest, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVTargetPower(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVTargetPower)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_EVPairingParametersPtr(P2VAR(Exi_ISO_ED2_DIS_ParameterSetType*, AUTOMATIC, SCC_APPL_DATA) EVPairingParametersPtr, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_PowerDemandParametersPtr(P2VAR(Exi_ISO_ED2_DIS_ParameterSetType *, AUTOMATIC, SCC_APPL_DATA) PowerDemandParametersPtr, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_GACoilCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) GACoilCurrent)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_GAFrequency(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) GAFrequency)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_VAPowerReceived(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) VAPowerReceived)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_VAVoltageToEV(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) VAVoltageToEV)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_WPT_VACurrentToEV(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) VACurrentToEV)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_MeterInfo(Exi_ISO_ED2_DIS_MeterInfoType *DataPtr, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_EVSETargetFrequency(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSETargetFrequency, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_EVSETargetActivePower(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSETargetActivePower, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_EVSETargetReactivePower(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSETargetReactivePower, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_ScheduleTupleID(P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) ScheduleTupleID, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}

void Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVMaximumVoltage(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumVoltage, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVTargetEnergyRequest(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVTargetEnergyRequest, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVMaximumEnergyRequest(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumEnergyRequest, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVMinimumEnergyRequest(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMinimumEnergyRequest, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVMaximumChargePower(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumChargePower, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVMaximumChargeCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumChargeCurrent, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVMinimumChargeCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMinimumChargeCurrent)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVMinimumChargePower(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMinimumChargePower, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_DC_TargetSOC(P2VAR(sint8, AUTOMATIC, SCC_APPL_DATA) TargetSOC, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_DC_BulkSOC(P2VAR(sint8, AUTOMATIC, SCC_APPL_DATA) BulkSOC, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVTargetVoltage(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVTargetVoltage)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_DC_EVTargetCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVTargetCurrent)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEPresentVoltage(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEPresentVoltage)
{
  return;
}

void Appl_SccCbk_Get_ISO_Ed2_DIS_Scheduled_ACBCReqControlMode(P2VAR(Exi_ISO_ED2_DIS_Scheduled_ACBCReqControlModeType, AUTOMATIC, SCC_APPL_DATA) ScheduledControlMode)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_Dynamic_ACBCReqControlMode(P2VAR(Exi_ISO_ED2_DIS_Dynamic_ACBCReqControlModeType, AUTOMATIC, SCC_APPL_DATA) DynamicControlMode)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_Scheduled_DCBCReqControlMode(P2VAR(Exi_ISO_ED2_DIS_Scheduled_DCBCReqControlModeType, AUTOMATIC, SCC_APPL_DATA) ScheduledControlMode)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_Dynamic_DCBCReqControlMode(P2VAR(Exi_ISO_ED2_DIS_Dynamic_DCBCReqControlModeType, AUTOMATIC, SCC_APPL_DATA) DynamicControlMode)
{
  return;
}

void Appl_SccCbk_Set_ISO_Ed2_DIS_WPT_EVSEAlignmentCheckMechanism(P2CONST(Exi_ISO_ED2_DIS_ACResAlignmentCheckMechanismType, AUTOMATIC, SCC_APPL_DATA) AlignmentCheckMechanism, Exi_RootElementIdType ElementId)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_WPT_EVSEPairingParameters(P2CONST(Exi_ISO_ED2_DIS_ParameterSetType, AUTOMATIC, SCC_APPL_DATA) EVSEPairingParameters, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_WPT_EVSEFinePositioningParameters(P2CONST(Exi_ISO_ED2_DIS_EVSEFinePositioningParametersType, AUTOMATIC, SCC_APPL_DATA) EVSEFinePositioningParameters, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_WPT_EVSEFinePositioningSetupParameters(P2CONST(Exi_ISO_ED2_DIS_EVSEFinePositioningSetupParametersType, AUTOMATIC, SCC_APPL_DATA) EVSEFinePositioningSetupParameters, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_WPT_EVSEMaximumPower(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) WPT_EVSEMaximumPower)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_WPT_EVSEMinimumPower(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) WPT_EVSEMinimumPower)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_IdentificationOptionList(P2CONST(Exi_ISO_ED2_DIS_IdentificationOptionListType, AUTOMATIC, SCC_VAR_NOINIT) IdentificationOptionList)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_EnergyTransferServiceList(P2CONST(Exi_ISO_ED2_DIS_ServiceListType, AUTOMATIC, SCC_VAR_NOINIT) EnergyTransferServiceList)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_EVSEID(P2CONST(Exi_ISO_ED2_DIS_evseIDType, AUTOMATIC, SCC_APPL_DATA) EVSEID)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_SessionID(P2CONST(Exi_ISO_ED2_DIS_sessionIDType, AUTOMATIC, SCC_APPL_DATA) SessionID)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_EVSEStatus(P2CONST(Exi_ISO_ED2_DIS_EVSEStatusType, AUTOMATIC, SCC_APPL_DATA) EVSEStatus, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_ResponseCode(Exi_ISO_ED2_DIS_responseCodeType ResponseCode)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_ScheduleList(P2CONST(Exi_ISO_ED2_DIS_CPDResControlModeType, AUTOMATIC, SCC_APPL_DATA) ScheduleList)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_ServiceID(uint16 ServiceID)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_ServiceParameterList(P2CONST(Exi_ISO_ED2_DIS_ServiceParameterListType, AUTOMATIC, SCC_VAR_NOINIT) ServiceParameter, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_AC_EVSENominalVoltage(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) AC_EVSENominalVoltage)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_AC_EVSEMaximumChargeCurrent(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) AC_EVSEMaximumChargeCurrent)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_AC_EVSENominalFrequency(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) AC_EVSENominalFrequency)
{
  return;
}

void Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEMaximumChargePower(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVSEMaximumChargePower, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEMaximumChargeCurrent(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVSEMaximumChargeCurrent, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEMinimumChargeCurrent(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVSEMinimumChargeCurrent)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEMaximumVoltage(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVSEMaximumVoltage, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEMinimumVoltage(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVSEMinimumVoltage)
{
  return;
}

void Appl_SccCbk_Get_ISO_Ed2_DIS_PnC_ReceiptRequired(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) ReceiptRequired, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEPresentCurrent(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEPresentCurrent)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEPowerLimitAchieved(boolean EVSEPowerLimitAchieved)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSECurrentLimitAchieved(boolean EVSECurrentLimitAchieved)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_DC_EVSEVoltageLimitAchieved(boolean EVSEVoltageLimitAchieved)
{
  return;
}

void Appl_SccCbk_Set_ISO_Ed2_DIS_EVSEDCPrimaryPower(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEDCPrimaryPower)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_EVRequestedPower(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVRequestedPower)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_EVSEPowerLimit(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEPowerLimit)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_ForeignObjectDetected(boolean ForeignObjectDetected)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_LivingObjectDetected(boolean LivingObjectDetected)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_GAInputPower(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) GAInputPower)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_GACoilCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) GACoilCurrent)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_GAFrequency(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) GAFrequency)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_PowerDemandParameters(P2VAR(Exi_ISO_ED2_DIS_ParameterSetType, AUTOMATIC, SCC_APPL_DATA) PowerDemandParameters)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_BPT_DC_BidirectionalControlRes(P2CONST(Exi_ISO_ED2_DIS_DC_BidirectionalControlResType, AUTOMATIC, SCC_VAR_NOINIT) DC_BidirectionalControlRes)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_BPT_AC_EVSEMaximumDischargeCurrent(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) AC_EVSEMaximumDischargeCurrent)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_BPT_DC_EVSEMaximumDischargePower(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVSEMaximumDischargePower)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_BPT_DC_EVSEMaximumDischargeCurrent(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVSEMaximumDischargeCurrent)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_BPT_DC_EVSEMinimumDischargeCurrent(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVSEMinimumDischargeCurrent)
{
  return;
}
void Appl_SccCbk_Set_ISO_Ed2_DIS_RemainingContractCertificateChains(uint8 RemainingContractCertificateChains)
{
  return;
}
void Appl_SccCbk_Get_ISO_Ed2_DIS_MaximumSupportedCertificateChains(P2VAR(uint8, AUTOMATIC, SCC_VAR_NOINIT) MaximumSupportedCertificateChains)
{
  return;
}

#endif /* SCC_SCHEMA_ISO_ED2 */

#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )

/* Rx DIN */
void Appl_SccCbk_Get_DIN_ChargingProfilePtr(P2VAR(Exi_DIN_ChargingProfileType*, AUTOMATIC, SCC_APPL_DATA) ChargingProfilePtr, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_DIN_EVCabinConditioning(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) EVCabinConditioning, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_DIN_EVRESSConditioning(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) EVRESSConditioning, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
  return;
}
void Appl_SccCbk_Get_DIN_EVErrorCode(P2VAR(Exi_DIN_DC_EVErrorCodeType, AUTOMATIC, SCC_APPL_DATA) EVErrorCode)
{
  return;
}
void Appl_SccCbk_Get_DIN_RequestedEnergyTransferMode(P2VAR(Exi_DIN_EVRequestedEnergyTransferType, AUTOMATIC, SCC_APPL_DATA) RequestedEnergyTransferMode)
{
  return;
}

/* Tx DIN */
void Appl_SccCbk_Set_DIN_ChargeService(P2CONST(Exi_DIN_ServiceChargeType, AUTOMATIC, SCC_APPL_DATA) ChargeService)
{
  return;
}
void Appl_SccCbk_Set_DIN_DateTimeNow(Exi_SInt64 DateTimeNow, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_DIN_EVSEID(P2CONST(Exi_DIN_evseIDType, AUTOMATIC, SCC_APPL_DATA) EVSEID)
{
  return;
}
void Appl_SccCbk_Set_DIN_EVSEIsolationStatus(Exi_DIN_isolationLevelType EVSEIsolationStatus, Exi_BitType Flag)
{
  return;
}
void Appl_SccCbk_Set_DIN_EVSENotification(Exi_DIN_EVSENotificationType EVSENotification)
{
  return;
}
void Appl_SccCbk_Set_DIN_EVSEProcessing(Exi_DIN_EVSEProcessingType EVSEProcessing)
{
  return;
}
void Appl_SccCbk_Set_DIN_EVSEStatusCode(Exi_DIN_DC_EVSEStatusCodeType EVSEStatusCode)
{
  return;
}
void Appl_SccCbk_Set_DIN_NotificationMaxDelay(uint32 NotificationMaxDelay)
{
  return;
}
void Appl_SccCbk_Set_DIN_ResponseCode(Exi_DIN_responseCodeType ResponseCode)
{
  return;
}
void Appl_SccCbk_Set_DIN_SAScheduleList(P2CONST(Exi_DIN_SAScheduleListType, AUTOMATIC, SCC_APPL_DATA) SAScheduleList)
{
  return;
}
void Appl_SccCbk_Set_DIN_SessionID(P2CONST(Exi_DIN_sessionIDType, AUTOMATIC, SCC_APPL_DATA) SessionID)
{
  return;
}

#endif /* SCC_SCHEMA_DIN */


/**********************************************************************************************************************
*  MISRA
*********************************************************************************************************************/

/* Justification for module specific MISRA deviations:

  MD_Scc_CallbackStubs:
    Reason:     Dummy callbacks to enable compile and link.
    Risk:       None.
    Prevention: Covered by code review.

*/


/**********************************************************************************************************************
 *  END OF FILE: _ApplScc_CbkStubs.c
 *********************************************************************************************************************/
