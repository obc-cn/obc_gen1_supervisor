/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc_Priv.c
 *        \brief  Smart Charging Communication Source Code File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *********************************************************************************************************************/

#define SCC_PRIV_SOURCE

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
 /* PRQA S 0777 EOF */ /* MD_MSR_Rule5.1 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Scc.h"
#include "Scc_Priv.h"
#include "Scc_Cfg.h"
#include "Scc_Lcfg.h"
#include "Scc_Exi.h"
#include "Scc_Interface_Cfg.h"

#if ( SCC_DEV_ERROR_DETECT == STD_ON )
#include "Det.h"
#endif /* SCC_DEV_ERROR_DETECT */
#include "IpBase.h"
#include "TcpIp.h"
#include "TcpIp_IpV6.h"
#if ( SCC_ENABLE_TLS == STD_ON )
#include "Tls.h"
#endif /* SCC_ENABLE_TLS */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
# include "Csm.h"
#endif /* SCC_ENABLE_PNC_CHARGING */

#include "NvM.h"


/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined (SCC_LOCAL)
# define SCC_LOCAL static
#endif

#define SCC_CLIENT_RANDOM_AND_MASTER_SECRET_LEN          80u
#define SCC_EXTRA_BYTES_TO_BE_SENT_LEN                   2u

/**********************************************************************************************************************
 *  LOCAL / GLOBAL DATA
 *********************************************************************************************************************/
/* uint8 variables - no init */
#define SCC_START_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )

VAR(boolean, SCC_VAR_NOINIT)      Scc_SaPubKeyRcvd;
#endif /* SCC_ENABLE_PNC_CHARGING */

#define SCC_STOP_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* other variables - no init */
#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#if ( SCC_ENABLE_TLS == STD_ON )
SCC_LOCAL VAR(IpBase_BerWorkspaceType, SCC_VAR_NOINIT)    Scc_BerWs; /* PRQA S 3218 */ /* MD_Scc_3218 */
SCC_LOCAL VAR(IpBase_BerStackElementType, SCC_VAR_NOINIT) Scc_BerStack[SCC_BER_STACK_DEPTH]; /* PRQA S 3218 */ /* MD_Scc_3218 */
#endif /* SCC_ENABLE_TLS */

#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Scc_Priv_Init
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Priv_Init(void)
{
  /* ----- Implementation ----------------------------------------------- */
#if ( SCC_ENABLE_TLS == STD_ON )
  /* #10 Initialize the BER workspace. */
  IpBase_BerInitWorkspace(&Scc_BerWs, &Scc_BerStack[0], SCC_BER_STACK_DEPTH);
#endif /* SCC_ENABLE_TLS */

  return;
}

/**********************************************************************************************************************
 *  Scc_ReportError
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_ReportError(Scc_StackErrorType StackError)
{
  /* ----- Implementation ----------------------------------------------- */
  /* check if a StackError is already set or if no MsgTrig is active */
  if ( Scc_StackError_NoError == Scc_StackError )
  {
    /* #10 Set the StackError. */
    Scc_StackError = StackError;

    /* #20 Set the MsgStatus. */
    switch ( Scc_MsgTrig )
    {
#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
    case Scc_MsgTrig_SLAC:
      Scc_MsgStatus = Scc_MsgStatus_SLAC_Failed;
      break;
#endif /* SCC_ENABLE_SLAC_HANDLING */

    case Scc_MsgTrig_SECCDiscoveryProtocol:
      Scc_MsgStatus = Scc_MsgStatus_SECCDiscoveryProtocol_Failed;
      break;

    case Scc_MsgTrig_TransportLayer:
      Scc_MsgStatus = Scc_MsgStatus_TransportLayer_Failed;
      break;

    case Scc_MsgTrig_SupportedAppProtocol:
      Scc_MsgStatus = Scc_MsgStatus_SupportedAppProtocol_Failed;
      break;

    case Scc_MsgTrig_SessionSetup:
      Scc_MsgStatus = Scc_MsgStatus_SessionSetup_Failed;
      break;

    case Scc_MsgTrig_ServiceDiscovery:
      Scc_MsgStatus = Scc_MsgStatus_ServiceDiscovery_Failed;
      break;

    case Scc_MsgTrig_ReadContrCertChain:
      Scc_MsgStatus = Scc_MsgStatus_ReadContrCertChain_Failed;
      break;

    case Scc_MsgTrig_ServiceDetail:
      Scc_MsgStatus = Scc_MsgStatus_ServiceDetail_Failed;
      break;

    case Scc_MsgTrig_PaymentServiceSelection:
      Scc_MsgStatus = Scc_MsgStatus_PaymentServiceSelection_Failed;
      break;

    case Scc_MsgTrig_ReadRootCerts:
      Scc_MsgStatus = Scc_MsgStatus_ReadRootCerts_Failed;
      break;

    case Scc_MsgTrig_ReadProvCert:
      Scc_MsgStatus = Scc_MsgStatus_ReadProvCert_Failed;
      break;

    case Scc_MsgTrig_CertificateInstallation:
      Scc_MsgStatus = Scc_MsgStatus_CertificateInstallation_Failed;
      break;

    case Scc_MsgTrig_CertificateUpdate:
      Scc_MsgStatus = Scc_MsgStatus_CertificateUpdate_Failed;
      break;

    case Scc_MsgTrig_PaymentDetails:
      Scc_MsgStatus = Scc_MsgStatus_PaymentDetails_Failed;
      break;

    case Scc_MsgTrig_Authorization:
      Scc_MsgStatus = Scc_MsgStatus_Authorization_Failed;
      break;

    case Scc_MsgTrig_ChargeParameterDiscovery:
      Scc_MsgStatus = Scc_MsgStatus_ChargeParameterDiscovery_Failed;
      break;

    case Scc_MsgTrig_PowerDelivery:
      Scc_MsgStatus = Scc_MsgStatus_PowerDelivery_Failed;
      break;

    case Scc_MsgTrig_MeteringReceipt:
      Scc_MsgStatus = Scc_MsgStatus_MeteringReceipt_Failed;
      break;


#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )

    case Scc_MsgTrig_CableCheck:
      Scc_MsgStatus = Scc_MsgStatus_CableCheck_Failed;
      break;

    case Scc_MsgTrig_PreCharge:
      Scc_MsgStatus = Scc_MsgStatus_PreCharge_Failed;
      break;

    case Scc_MsgTrig_CurrentDemand:
      Scc_MsgStatus = Scc_MsgStatus_CurrentDemand_Failed;
      break;

    case Scc_MsgTrig_WeldingDetection:
      Scc_MsgStatus = Scc_MsgStatus_WeldingDetection_Failed;
      break;

#endif /* SCC_CHARGING_DC */


    case Scc_MsgTrig_SessionStop:
      Scc_MsgStatus = Scc_MsgStatus_SessionStop_Failed;
      break;

    default: /* case Scc_MsgTrig_StopCommunicationSession: */
      Scc_MsgStatus = Scc_MsgStatus_StopCommunicationSession_Failed;
      break;
    }

    /* #30 Report the MsgStatus and the StackError to the application. */
    Scc_Set_Core_MsgStatus(Scc_MsgStatus);
    Scc_Set_Core_StackError(Scc_StackError);
  }

  return;
} /* PRQA S 6010 */ /* MD_MSR_STPTH */ /* PRQA S 6030 */ /* MD_MSR_STCYC */

/**********************************************************************************************************************
*  Scc_ReportSuccessAndStatus
*********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_ReportSuccessAndStatus(Scc_MsgStatusType MsgStatus)
{
  /* ----- Implementation ----------------------------------------------- */
  /* check if a StackError is already set or if no MsgTrig is active */
  if (   ( Scc_StackError_NoError == Scc_StackError )
      && ( Scc_MsgTrig_None != Scc_MsgTrig ))
  {
    /* #10 Set the MsgStatus. */
    Scc_MsgStatus = MsgStatus;

    /* #20 Report the MsgStatus and the StackError to the application. */
    Scc_Set_Core_MsgStatus(Scc_MsgStatus);
  }

  return;
}

/**********************************************************************************************************************
*  Scc_ResetSessionID
*********************************************************************************************************************/
/*!
*
* Internal comment removed.
 *
 *
 *
 *
 *
*/
FUNC(Std_ReturnType, SCC_CODE) Scc_ResetSessionID(boolean ForceReset)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_MsgTrigType MsgTrig;
  boolean SccResetSessionID = FALSE;
  Std_ReturnType retVal = E_NOT_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 check if the session id shall not be overwritten during an active session. */
  if (FALSE == ForceReset)
  {
    /* get the current message trigger from the application */
    Scc_Get_Core_MsgTrig(&MsgTrig);
    /* check if a session is active */
    if ((Scc_MsgTrig_SessionSetup > MsgTrig)
      || (Scc_MsgTrig_SessionStop < MsgTrig))
    {
      SccResetSessionID = TRUE;
    }
  }

  /*#20 check if there is no V2G session active. */
  if ((TRUE == SccResetSessionID)
    || (TRUE == ForceReset))
  {
    /* #30 reset the SessionID. */
    Scc_SessionIDNvm[1] = 0x00;
    /* #40 set the length of the SessionID. */
    Scc_SessionIDNvm[0] = 0x01;
#if ((( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))) /* PRQA S 3332 */ /* MD_Scc_3332 */
    /* set the flag for the NvM to copy the new SessionID */
    (void) NvM_SetRamBlockStatus((NvM_BlockIdType)SCC_SESSION_ID_NVM_BLOCK, TRUE);
#endif /* SCC_SCHEMA_ISO, SCC_SCHEMA_ISO_ED2 */

    retVal = E_OK;
  }
  /* if a V2G session is active */
  else
  {
    /* operation not allowed */

  }

  return retVal;
}

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_NvmReadBlock
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_NvmReadBlock(NvM_BlockIdType NvMBlockID,
  P2VAR(Scc_NvMBlockReadStateType, AUTOMATIC, SCC_APPL_DATA) NvMBlockReadStatePtr, P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8       NvmReadState;
  boolean     ReadFinished = FALSE;
  boolean     ErrorOccurred = FALSE;
#if ( defined SCC_DEM_NVM_READ_ROOT_CERT_FAIL ) || ( defined SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL )
  uint8_least Counter;
#endif /* SCC_DEM_NVM_READ_ROOT_CERT_FAIL || SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 check if the NvM block has not been read yet */
  if ( Scc_NvMBlockReadState_NotSet == *NvMBlockReadStatePtr )
  {
    /* #20 get the current status of the NvM block */
    (void) NvM_GetErrorStatus(NvMBlockID, &NvmReadState);
    /* #30 check if the NvM block is currently not busy */
    if ( NVM_REQ_PENDING != NvmReadState )
    {
      /* #40 read the NvM block and check if an error occurred */
      /* PRQA S 0314 1 */ /* MD_Scc_Nvm_Generic */
      if ( (Std_ReturnType)E_OK != NvM_ReadBlock(NvMBlockID, (P2VAR(void,AUTOMATIC,SCC_APPL_DATA))&DataPtr[0]) )
      {
        /* report the error */
        *NvMBlockReadStatePtr = Scc_NvMBlockReadState_Error;
        ErrorOccurred = TRUE;
      }
      /* if the NvM block could be read successfully */
      else
      {
        *NvMBlockReadStatePtr = Scc_NvMBlockReadState_ReadRequested;
      }
    }
    /* if the NvM block is currently busy */
    else
    {
      /* wait */
    }
  }
  /* #50 check if the NvM block is currently being read */
  if ( Scc_NvMBlockReadState_ReadRequested == *NvMBlockReadStatePtr )
  {
    /* get the current status of the NvM block */
    (void) NvM_GetErrorStatus(NvMBlockID, &NvmReadState);

    /* check if block is fully read by NvM */
    if (   ( NVM_REQ_OK == NvmReadState )
        || ( NVM_REQ_RESTORED_FROM_ROM == NvmReadState ))
    {
      *NvMBlockReadStatePtr = Scc_NvMBlockReadState_ReadFinished;
      ReadFinished = TRUE;
    }
    /* if the status is still pending */
    else if ( NVM_REQ_PENDING == NvmReadState )
    {
      /* wait */
    }
    /* check if the NvM block is not set */
    else if ( NVM_REQ_NV_INVALIDATED == NvmReadState )
    {
      *NvMBlockReadStatePtr = Scc_NvMBlockReadState_Invalidated;
    }
    /* if an error occurred */
    else
    {
      /* report the error */
      *NvMBlockReadStatePtr = Scc_NvMBlockReadState_Error;
      ErrorOccurred = TRUE;
    }
  }

  /* #60 DemReport fail */
  if ( TRUE == ErrorOccurred )
  {
#if ( defined SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL )
    /* Contract Cert Chain Size */
    if ( Scc_GetNvMBlockIDContrCertChainSize(Scc_CertsWs.ChosenContrCertChainIdx) == NvMBlockID )
    {
      Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL);
    }
#endif /* SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL */
#if ( defined SCC_DEM_NVM_READ_CONTR_CERT_FAIL )
    /* Contract Certificate */
    if ( Scc_GetNvMBlockIDContrCert(Scc_CertsWs.ChosenContrCertChainIdx) == NvMBlockID )
    {
      Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_CONTR_CERT_FAIL);
    }
#endif /* SCC_DEM_NVM_READ_CONTR_CERT_FAIL */

#if ( defined SCC_DEM_NVM_READ_PROV_CERT_FAIL )
    /* Provisioning Certificate */
    if ( Scc_GetNvMBlockIDProvCert(Scc_CertsWs.ChosenContrCertChainIdx) == NvMBlockID )
    {
      Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_PROV_CERT_FAIL);
    }
#endif /* SCC_DEM_NVM_READ_PROV_CERT_FAIL */

    {
#if ( defined SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL )
      /* Contract Sub Certificates */
      for ( Counter = 0; Counter < Scc_CertsWs.ContrSubCertCnt; Counter++ )
      {
        if ( Scc_GetNvMBlockIDContrSubCerts(Scc_CertsWs.ChosenContrCertChainIdx, Counter) == NvMBlockID )
        {
          Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL);
          break;
        }
      }
#endif /* SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL */
#if ( defined SCC_DEM_NVM_READ_ROOT_CERT_FAIL )
      /* Root Certificates */
      for ( Counter = 0; Counter < Scc_CertsNvm.RootCertNvmCnt; Counter++)
      {
        if ( Scc_CertsNvm.RootCerts[Counter].NvmBlockId == NvMBlockID )
        {
          Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_ROOT_CERT_FAIL);
          break;
        }
      }
#endif /* SCC_DEM_NVM_READ_ROOT_CERT_FAIL */
    }
  }

  /* #70 DemReport success */
  else if ( TRUE == ReadFinished ) /* PRQA S 2004 */ /* MD_Scc_2004 */
  {
#if ( defined SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL )
    /* Contract Cert Chain Size */
    if ( Scc_GetNvMBlockIDContrCertChainSize(Scc_CertsWs.ChosenContrCertChainIdx) == NvMBlockID )
    {
      Scc_DemReportErrorStatusPassed(SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL);
    }
#endif /* SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL */
#if ( defined SCC_DEM_NVM_READ_CONTR_CERT_FAIL )
    /* Contract Certificate */
    if ( Scc_GetNvMBlockIDContrCert(Scc_CertsWs.ChosenContrCertChainIdx) == NvMBlockID )
    {
      Scc_DemReportErrorStatusPassed(SCC_DEM_NVM_READ_CONTR_CERT_FAIL);
    }
#endif /* SCC_DEM_NVM_READ_CONTR_CERT_FAIL */

#if ( defined SCC_DEM_NVM_READ_PROV_CERT_FAIL )
    /* Provisioning Certificate */
    if ( Scc_GetNvMBlockIDProvCert(Scc_CertsWs.ChosenContrCertChainIdx) == NvMBlockID )
    {
      Scc_DemReportErrorStatusPassed(SCC_DEM_NVM_READ_PROV_CERT_FAIL);
    }
#endif /* SCC_DEM_NVM_READ_PROV_CERT_FAIL */

    {
#if ( defined SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL )
      /* Contract Sub Certificates */
      for ( Counter = 0; Counter < Scc_CertsWs.ContrSubCertCnt; Counter++ )
      {
        if ( Scc_GetNvMBlockIDContrSubCerts(Scc_CertsWs.ChosenContrCertChainIdx, Counter) == NvMBlockID )
        {
          Scc_DemReportErrorStatusPassed(SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL);
          break;
        }
      }
#endif /* SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL */
#if ( defined SCC_DEM_NVM_READ_ROOT_CERT_FAIL )
      /* Root Certificates */
      for ( Counter = 0; Counter < Scc_CertsNvm.RootCertNvmCnt; Counter++)
      {
        if ( Scc_CertsNvm.RootCerts[Counter].NvmBlockId == NvMBlockID )
        {
          Scc_DemReportErrorStatusPassed(SCC_DEM_NVM_READ_ROOT_CERT_FAIL);
          break;
        }
      }
#endif /* SCC_DEM_NVM_READ_ROOT_CERT_FAIL */
    }
  }

  return;
} /* PRQA S 6010,6030 */ /* MD_MSR_STPTH, MD_MSR_STCYC */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_NvmReadContrCertChain
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_NvmReadContrCertChain(boolean LoadContrSubCerts)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint32 Counter;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Read contract certificate chain size. */
  if ( Scc_NvMBlockReadState_ReadFinished > Scc_CertsWs.ContrCertChainSizeReadState )
  {
    /* read the contract certificate chain size */ /* PRQA S 0310,3305 3 */ /* MD_Scc_0310_0314_0316_3305 */
    Scc_NvmReadBlock((NvM_BlockIdType)Scc_GetNvMBlockIDContrCertChainSize(Scc_CertsWs.ChosenContrCertChainIdx),
      (P2VAR(Scc_NvMBlockReadStateType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrCertChainSizeReadState,
      (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrCertChainSize);
    /* reset the processed contract sub certs */
    Scc_CertsWs.ContrSubCertsProcessedFlags = 0;

    /* check if it failed */
    if ( Scc_NvMBlockReadState_Error != Scc_CertsWs.ContrCertChainSizeReadState )
    {
      /* check if the block was invalidated */
      if ( Scc_NvMBlockReadState_Invalidated == Scc_CertsWs.ContrCertChainSizeReadState )
      {
        Scc_CertsWs.ContrCertChainSize = -1;
      }
      /* check if the block was read */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
      else if ( Scc_NvMBlockReadState_ReadFinished == Scc_CertsWs.ContrCertChainSizeReadState )
      {
        /* check if the ContrCertChainSize is invalid */
        if (   ( -1 > Scc_CertsWs.ContrCertChainSize )
            || ( Scc_CertsWs.ContrSubCertCnt < (uint8)Scc_CertsWs.ContrCertChainSize ))
        {
#if ( defined SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL )
          /* set DEM event, if enabled */
          Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL);
#endif /* SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL */

          Scc_CertsWs.ContrCertChainSizeReadState = Scc_NvMBlockReadState_Error;
        }
        else
        {
          Scc_CertsWs.ContrCertChainSizeReadState = Scc_NvMBlockReadState_Processed;
        }
      }
    }
  }
  else
  {
    if ( -1 < Scc_CertsWs.ContrCertChainSize )
    {
      /* #20 Read contract certificate */
      Scc_NvmReadBlock((NvM_BlockIdType)Scc_GetNvMBlockIDContrCert(Scc_CertsWs.ChosenContrCertChainIdx),
        (P2VAR(Scc_NvMBlockReadStateType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrCertReadState,
        (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrCert->Buffer[0]);
      /* check if the certificate is read */
      if ( Scc_NvMBlockReadState_ReadFinished == Scc_CertsWs.ContrCertReadState )
      {
        Std_ReturnType RetVal;

        /* set the next pointer to NULL */
        Scc_CertsWs.ContrCert->NextCertificatePtr = (P2VAR(Exi_ISO_certificateType, AUTOMATIC, SCC_VAR_NOINIT))NULL_PTR;
        /* set the length to the block size */
        Scc_CertsWs.ContrCert->Length = Scc_GetNvMBlockLenContrCert(Scc_CertsWs.ChosenContrCertChainIdx);
        /* get the size of the certificate */
        RetVal = Scc_GetCertSize(&Scc_CertsWs.ContrCert->Buffer[0], &Scc_CertsWs.ContrCert->Length);
        /* #30 get the eMAID */
        Scc_CertsWs.eMAID->Length = sizeof(Scc_CertsWs.eMAID->Buffer);
        RetVal |= Scc_GetCertDistinguishedNameObject(&Scc_CertsWs.ContrCert->Buffer[0],
          Scc_CertsWs.ContrCert->Length, &Scc_CertsWs.eMAID->Buffer[0],
          &Scc_CertsWs.eMAID->Length, Scc_BEROID_Subject_EMAID);
        /* if no error occurred, set the NvM state to processed */
        if ( (Std_ReturnType)E_OK == RetVal )
        {
          Scc_CertsWs.ContrCertReadState = Scc_NvMBlockReadState_Processed;
        }
        else
        {
#if ( defined SCC_DEM_NVM_READ_CONTR_CERT_FAIL )
          /* set DEM event, if enabled */
          Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_CONTR_CERT_FAIL);
#endif /* SCC_DEM_NVM_READ_CONTR_CERT_FAIL */

          Scc_CertsWs.ContrCertReadState = Scc_NvMBlockReadState_Error;
        }
      }
      /* check if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
      else if ( Scc_NvMBlockReadState_Error != Scc_CertsWs.ContrCertReadState )
      {
        /* #40 Read the SubCertificates of the ContractCertificate */
        if ( TRUE == LoadContrSubCerts )
        {
          /* if they exist read the contract sub certificates */
          for ( Counter = 0; Counter < (uint32)(Scc_CertsWs.ContrCertChainSize); Counter++ ) /*lint !e571 */
          {
            Scc_NvmReadBlock((NvM_BlockIdType)Scc_GetNvMBlockIDContrSubCerts(Scc_CertsWs.ChosenContrCertChainIdx, Counter),
              (P2VAR(Scc_NvMBlockReadStateType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrSubCertsReadStates[Counter],
              (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrSubCerts[Counter].Buffer[0]);
            /* check if the certificate is read */
            if ( Scc_NvMBlockReadState_ReadFinished == Scc_CertsWs.ContrSubCertsReadStates[Counter] )
            {
              /* set the length to the block size */
              Scc_CertsWs.ContrSubCerts[Counter].Length = Scc_GetNvMBlockLenContrSubCerts(
                Scc_CertsWs.ChosenContrCertChainIdx, Counter);
              /* get the size of the certificate and check if no error occurred */
              if ( (Std_ReturnType)E_OK == Scc_GetCertSize(&Scc_CertsWs.ContrSubCerts[Counter].Buffer[0],
                &Scc_CertsWs.ContrSubCerts[Counter].Length) )
              {
                /* check if this is the first contract sub certificate */
                if ( 0u < Counter )
                {
                  /* link the sub certificates */
                  Scc_CertsWs.ContrSubCerts[Counter-1u].NextCertificatePtr = &Scc_CertsWs.ContrSubCerts[Counter];
                }
                /* set the next pointer to NULL */
                Scc_CertsWs.ContrSubCerts[Counter].NextCertificatePtr =
                  (P2VAR(Exi_ISO_certificateType, AUTOMATIC, SCC_VAR_NOINIT))NULL_PTR;
                /* set the new nvm read state */
                Scc_CertsWs.ContrSubCertsReadStates[Counter] = Scc_NvMBlockReadState_Processed;
                /* update the status flag */
                Scc_CertsWs.ContrSubCertsProcessedFlags |= ( (uint32)0x01 << Counter );
              }
              /* if an error occurred */
              else
              {
#if ( defined SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL )
                /* set DEM event, if enabled */
                Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL);
#endif /* SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL */

                Scc_CertsWs.ContrSubCertsReadStates[Counter] = Scc_NvMBlockReadState_Error;
              }
            }
          }
        }
      }
    }
    else
    {
      /* #50 invalidate the contract certificate chain */
      Scc_CertsWs.ContrCertReadState = Scc_NvMBlockReadState_Invalidated;

      /* step through the sub certificates */
      for ( Counter = 0; Counter < Scc_CertsWs.ContrSubCertCnt; Counter++ )
      {
        Scc_CertsWs.ContrSubCertsReadStates[Counter] = Scc_NvMBlockReadState_Invalidated;
        Scc_CertsWs.ContrSubCertsProcessedFlags |= ( (uint32)0x01 << Counter );
      }
    }
  }
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_NvmReadProvCert
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */

FUNC(void, SCC_CODE) Scc_NvmReadProvCert(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Read Provisioning Certificate */
  Scc_NvmReadBlock((NvM_BlockIdType)Scc_GetNvMBlockIDProvCert(Scc_CertsWs.ChosenContrCertChainIdx),
    (P2VAR(Scc_NvMBlockReadStateType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ProvCertReadState,
    (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA)) &Scc_CertsWs.ProvCert->Buffer[0]);
  /* check if the certificate is read */
  if ( Scc_NvMBlockReadState_ReadFinished == Scc_CertsWs.ProvCertReadState )
  {
    Std_ReturnType RetVal;

    /* set the length to the block size */
    Scc_CertsWs.ProvCert->Length = Scc_GetNvMBlockLenProvCert(Scc_CertsWs.ChosenContrCertChainIdx);
    /* get the size of the provisioning certificate */
    RetVal = Scc_GetCertSize(&Scc_CertsWs.ProvCert->Buffer[0], &Scc_CertsWs.ProvCert->Length);
    /* #20 Get the ProvCertID */
    Scc_CertsWs.ProvCertIDLen = SCC_PROV_CERT_ID_MAX_LEN;
    RetVal |= Scc_GetCertDistinguishedNameObject(&Scc_CertsWs.ProvCert->Buffer[0], Scc_CertsWs.ProvCert->Length,
      &Scc_CertsWs.ProvCertID[0], &Scc_CertsWs.ProvCertIDLen, Scc_BEROID_Subject_CommonName);
    /* if no error occurred, set the nvm state to processed */
    if ( (Std_ReturnType)E_OK == RetVal )
    {
      Scc_CertsWs.ProvCertReadState = Scc_NvMBlockReadState_Processed;
    }
    else
    {
#if ( defined SCC_DEM_NVM_READ_PROV_CERT_FAIL )
      /* set DEM event, if enabled */
      Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_PROV_CERT_FAIL);
#endif /* SCC_DEM_NVM_READ_PROV_CERT_FAIL */

      Scc_CertsWs.ProvCertReadState = Scc_NvMBlockReadState_Error;
    }
  }
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_NvmReadRootCerts
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_NvmReadRootCerts(boolean ReadAll, uint8 RootCertIdx)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = SCC_DET_NO_ERROR;
  uint8_least Counter;
  uint8_least ForLoopLimit;

  /* ----- Development Error Checks ------------------------------------- */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of runtime parameters. */
  if ( Scc_CertsNvm.RootCertNvmCnt <= RootCertIdx )
  {
    errorId = SCC_DET_INV_PARAM;
  }
  else
#endif /* SCC_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */
    if ( FALSE == ReadAll )
    {
      /* #20 Read only on RootCertificate. */
      Counter = RootCertIdx;
      ForLoopLimit = RootCertIdx+1u;
    }
    else
    {
      /* #30 Read all RootCertificates. */
      Counter = 0;
      ForLoopLimit = Scc_CertsNvm.RootCertNvmCnt;
    }

    for ( ; Counter < ForLoopLimit; Counter++)
    {
      Scc_NvmReadBlock((NvM_BlockIdType)Scc_CertsNvm.RootCerts[Counter].NvmBlockId,
        (P2VAR(Scc_NvMBlockReadStateType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.RootCertsReadStates[Counter],
        (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsNvm.RootCerts[Counter].RootCert->Buffer[0]);
      /* check if the root certificate is read */
      if ( Scc_NvMBlockReadState_ReadFinished == Scc_CertsWs.RootCertsReadStates[Counter] )
      {
        /* set the maximum length to the block size */
        Scc_CertsNvm.RootCerts[Counter].RootCert->Length = Scc_CertsNvm.RootCerts[Counter].NvmBlockLen;
        /* get the size of the root certificate and check if no error occurred */
        if ( (Std_ReturnType)E_OK == Scc_GetCertSize(&Scc_CertsNvm.RootCerts[Counter].RootCert->Buffer[0],
          &Scc_CertsNvm.RootCerts[Counter].RootCert->Length) )
        {
          /* set the nvm state to processed */
          Scc_CertsWs.RootCertsReadStates[Counter] = Scc_NvMBlockReadState_Processed;
        }

        else
        {
          /* #40 Size could not be read, RootCertificate is invalid. */
#if ( defined SCC_DEM_NVM_READ_ROOT_CERT_FAIL )
          /* set DEM event, if enabled */
          Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_ROOT_CERT_FAIL);
#endif /* SCC_DEM_NVM_READ_ROOT_CERT_FAIL */

          /* set the state for this root cert to error */
          Scc_CertsWs.RootCertsReadStates[Counter] = Scc_NvMBlockReadState_Error;
        }
        /* processing of this root cert is finished */
        Scc_CertsWs.RootCertsProcessedFlags |= ( (uint32)0x01 << Counter );
      }
      /* check if the root certificate is not set or an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
      else if (   ( Scc_NvMBlockReadState_Invalidated == Scc_CertsWs.RootCertsReadStates[Counter] )
               || ( Scc_NvMBlockReadState_Error == Scc_CertsWs.RootCertsReadStates[Counter] ))
      {
        /* skip to the next one */
        Scc_CertsWs.RootCertsProcessedFlags |= ( (uint32)0x01 << Counter );
      }
    }
  }
  /* ----- Development Error Report --------------------------------------- */
#if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #50 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_NVM_READ_ROOT_CERTS, errorId);
  }
#else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* SCC_DEV_ERROR_REPORT */
  return;
}


/**********************************************************************************************************************
 *  Scc_ValidateKeyPair
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_ValidateKeyPair(P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) CertPtr, uint16 CertLen, uint32 ECDSASignJobId)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint16 PubKeyIdx;
  uint16 PubKeyLen;
  Std_ReturnType retVal = E_NOT_OK;
  uint8 Scc_CryptoBuf[SCC_CRYPTO_BUF_LEN] = {0};
  uint8 Scc_Sha256HashBuf[SCC_SHA256_DIGEST] = {0};

  /* ----- Implementation ----------------------------------------------- */
  uint32 SignatureLengthCsm = sizeof(Scc_CryptoBuf);
  Crypto_VerifyResultType verifyResult;

  /* #10 Get the PublicKey in the given certificate */
  if ( E_OK == Scc_GetIndexOfPublicKey(CertPtr, CertLen, &PubKeyIdx, &PubKeyLen) )
  {

    /* #20 Create the ECDSA signature using the private key */
    if( (Std_ReturnType)E_OK != Csm_SignatureGenerate(ECDSASignJobId,
      CRYPTO_OPERATIONMODE_SINGLECALL, &Scc_Sha256HashBuf[0],
      SCC_SHA256_DIGEST, &Scc_CryptoBuf[0], &SignatureLengthCsm))
    {
      retVal = E_NOT_OK;
    }

    /* #30 validate the ECDSA signature using the public key */
    /* PubKeyPtr[0] == 0x04 --> uncompressed point */
    else if (( PubKeyLen != ( SCC_P256R1_PUBLIC_KEY_LEN + 1u )) && ( CertPtr[PubKeyIdx] != 0x04u ))
    {
      retVal = E_NOT_OK;
    }
    else if( (Std_ReturnType)E_OK != Csm_KeyElementSet(Scc_ECDSAVerifyKeyId,
      CRYPTO_KE_CERTIFICATE_SUBJECT_PUBLIC_KEY, &CertPtr[PubKeyIdx+1u], (uint32)PubKeyLen-1u))
    {
      retVal = E_NOT_OK;
    }
    else if( (Std_ReturnType)E_OK != Csm_KeySetValid(Scc_ECDSAVerifyKeyId))
    {
      retVal = E_NOT_OK;
    }
    else if( (Std_ReturnType)E_OK != Csm_SignatureVerify(Scc_ECDSAVerifyJobId,
      CRYPTO_OPERATIONMODE_SINGLECALL, &Scc_Sha256HashBuf[0],
      SCC_SHA256_DIGEST, &Scc_CryptoBuf[0], SignatureLengthCsm, &verifyResult))
    {
      retVal = E_NOT_OK;
    }
    else if( CRYPTO_E_VER_OK != verifyResult )
    {
      retVal = E_NOT_OK;
    }
    else
    {
      retVal = E_OK;
    }
  }

  return retVal;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_DecryptPrivateKey
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_DecryptPrivateKey(uint32 ECDHExchangeKeyId,
                                                     uint32 ECDSASignCertKeyId,
                                                     P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) PublicKeyPtr,
                                                     P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) EncryptedPrivKeyPtr,
                                                     P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) InitVectorPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType        retVal = E_NOT_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* Set Key valid */
  if (E_OK != Csm_KeySetValid(ECDHExchangeKeyId))
  {
    Scc_ReportError(Scc_StackError_Crypto);
  }
  /* #10 Create SharedSecret (ECDH with curve p256r1) */
  else if (E_OK != Csm_KeyExchangeCalcSecret(ECDHExchangeKeyId,
           PublicKeyPtr,
           ((uint32)SCC_PUB_KEY_LEN - 1u)))
  {
    Scc_ReportError(Scc_StackError_Crypto);
  }
  else
  {
    /* #20 Create ConcatenateDataStructure for KDF NIST SP 800-56A */
    uint32 concatenateBufLen = (SCC_KDF_ITERATION_INFO_LEN + SCC_KDF_SHARED_SECRET_LEN + SCC_KDF_OTHER_INFO_LEN);
    uint8 concatenateBuf[SCC_KDF_ITERATION_INFO_LEN + SCC_KDF_SHARED_SECRET_LEN + SCC_KDF_OTHER_INFO_LEN];

    /* Set OtherInfo = (AlgorithmID || PartyUInfo(Transmitter) || PartyVInfo(Receiver)) */
    concatenateBuf[0] = 0x00u;
    concatenateBuf[1] = 0x00u;
    concatenateBuf[2] = 0x00u;
    concatenateBuf[3] = 0x01u; /* Iterations */
    /* index 4 to 35 (32Bytes): space for the SharedSecret */
    concatenateBuf[36] = 0x01u; /* AlgorithmID */
    concatenateBuf[37] = 0x55u; /* PartyUInfo(Transmitter) */
    concatenateBuf[38] = 0x56u; /* PartyVInfo(Receiver)) */

    /* #30 Set ConcatenateDataStructure to KeyElement */
    if (E_OK != Csm_KeyElementSet(Scc_KdfSHA256KeyId,
        CRYPTO_KE_KEYDERIVATION_SALT,
        &concatenateBuf[0],
        concatenateBufLen))
    {
      Scc_ReportError(Scc_StackError_Crypto);
    }
    /* #40 Copy first 32 bytes of SharedSecret to ConcatenateDataStructure with an offset of SCC_KDF_ITERATION_INFO_LEN */
    else if (E_OK != Csm_KeyElementCopyPartial(ECDHExchangeKeyId,
             CRYPTO_KE_KEYEXCHANGE_SHAREDVALUE,
             0,
             SCC_KDF_ITERATION_INFO_LEN,
             SCC_KDF_SHARED_SECRET_LEN, Scc_KdfSHA256KeyId,
             CRYPTO_KE_KEYDERIVATION_SALT))
    {
      Scc_ReportError(Scc_StackError_Crypto);
    }
    /* Set Key valid */
    else if (E_OK != Csm_KeySetValid(Scc_KdfSHA256KeyId))
    {
      Scc_ReportError(Scc_StackError_Crypto);
    }
    /* #50 Hash (SHA256) over the data strucutre (KDF) and create hash result (32 Byte) */
    /* Used Redirection: Input: Scc_KdfSHA256KeyId[CRYPTO_KE_KEYDERIVATION_SALT]
                         Output: Scc_AES128KeyId[CRYPTO_KE_KEYDERIVATION_SALT] */
    else if (E_OK != Csm_Hash(Scc_KdfSHA256JobId,
             CRYPTO_OPERATIONMODE_SINGLECALL,
             NULL_PTR,
             0,
             NULL_PTR,
             NULL_PTR))
    {
      Scc_ReportError(Scc_StackError_Crypto);
    }
    else
    {
      /* Set Key valid */
      if (E_OK != Csm_KeySetValid(Scc_AES128KeyId))
      {
        Scc_ReportError(Scc_StackError_Crypto);
      }
      /* #60 Copy first 16 bytes of the hash result to use as the AES-128 Key to decrypt the encrypted private key */
      else if (E_OK != Csm_KeyElementCopyPartial(Scc_AES128KeyId,
               CRYPTO_KE_KEYDERIVATION_SALT,
               0,
               0,
               SCC_AES128_KEY_LEN,
               Scc_AES128KeyId,
               CRYPTO_KE_CIPHER_KEY))
      {
        Scc_ReportError(Scc_StackError_Crypto);
      }
      /* #70 Set the Initialization Vector for the AES-Decrypt job */
      else if (E_OK != Csm_KeyElementSet(Scc_AES128KeyId,
               CRYPTO_KE_CIPHER_IV,
               &InitVectorPtr[0],
               SCC_IV_LEN))
      {
        Scc_ReportError(Scc_StackError_Crypto);
      }
      /* Set Key valid */
      else if (E_OK != Csm_KeySetValid(Scc_AES128KeyId))
      {
        Scc_ReportError(Scc_StackError_Crypto);
      }
      /* #80 Decrypt (AES-128-CBC no Padding) the private Key*/
      /* Used Redirection: Output: Scc_AES128KeyId[CRYPTO_KE_CIPHER_2NDKEY] */
      else if (E_OK != Csm_Decrypt(Scc_AES128JobId,
               CRYPTO_OPERATIONMODE_SINGLECALL,
               &EncryptedPrivKeyPtr[0],
               SCC_PRIV_KEY_LEN,
               NULL_PTR,
               NULL_PTR))
      {
        Scc_ReportError(Scc_StackError_Crypto);
      }
      /* Set Key valid */
      else if (E_OK != Csm_KeySetValid(Scc_AES128KeyId))
      {
        Scc_ReportError(Scc_StackError_Crypto);
      }
      /* #90 Copy private key to Signature KeyType */
      else if (E_OK != Csm_KeyElementCopy(Scc_AES128KeyId,
               CRYPTO_KE_CIPHER_2NDKEY,
               ECDSASignCertKeyId,
               CRYPTO_KE_SIGNATURE_KEY))
      {
        Scc_ReportError(Scc_StackError_Crypto);
      }
      /* Set Key valid */
      else if (E_OK != Csm_KeySetValid(ECDSASignCertKeyId))
      {
        Scc_ReportError(Scc_StackError_Crypto);
      }
      else
      {
        retVal = E_OK;
      }
    }
  }

  return retVal;
} /* PRQA S 6030,6050,6080 */ /* MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */


/**********************************************************************************************************************
 *  Scc_GetCertDistinguishedNameObject
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_GetCertDistinguishedNameObject(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr, uint16 CertLen,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) DataLenPtr, Scc_BERObjectIDsType ObjectID)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType        retVal = E_NOT_OK;
  IpBase_BerElementType BerEleDesc;
  uint8                 DistinguishedNameEleNr[6] = { 1, 1, 6, 1, 1, 1 };
  uint8                 BerDnObjId[3] = { 0x55, 0x04, 0x03 };
  uint8                 BerDnObjIdDC[10] = { 0x09, 0x92, 0x26, 0x89, 0x93, 0xf2, 0x2c, 0x64, 0x01, 0x19 };

  /* ----- Implementation ----------------------------------------------- */
  BerEleDesc.Idx = 0;

  /* #10 Get the Object identifier for the DistinguishedName element */
  switch ( ObjectID )
  {
  case Scc_BEROID_Issuer_Country:
  case Scc_BEROID_Subject_Country:
    BerDnObjId[2] = 0x06;
    break;

  case Scc_BEROID_Issuer_Organization:
  case Scc_BEROID_Subject_Organization:
    BerDnObjId[2] = 0x0A;
    break;

  case Scc_BEROID_Issuer_OrganizationUnit:
  case Scc_BEROID_Subject_OrganizationUnit:
    BerDnObjId[2] = 0x0B;
    break;

  case Scc_BEROID_Subject_DomainComponent:
    BerDnObjId[0] = 0x09;
    BerDnObjId[1] = 0x92;
    BerDnObjId[2] = 0x26;
    break;

  default:
  /* case Scc_BEROID_Issuer_CommonName:
     case Scc_BEROID_Subject_CommonName:
     case Scc_BEROID_Subject_EMAID:
     case Scc_BEROID_Subject_PCID   */
    BerDnObjId[2] = 0x03;
    break;
  }

  /* check if the DistinguishedName of the Issuer shall be returned */
  if ( Scc_BEROID_Subject_CommonName > ObjectID )
  {
    DistinguishedNameEleNr[2] = 0x04;
  }

  /* #20 Search object within subject/issuer. */
  while ( BerEleDesc.Idx < CertLen )
  {
    /* parse object identifier and check if an error occurred */
    if ( IPBASE_E_OK != IpBase_BerGetElement(&Scc_BerWs, &BerEleDesc, &DistinguishedNameEleNr[0],
      sizeof(DistinguishedNameEleNr), &CertPtr[0], CertLen) )
    {
      break; /* PRQA S 0771 */ /* MD_Scc_0771 */
    }

    /* #30 search object identifier in certificate. */
    if ( ( BerDnObjId[0] == CertPtr[BerEleDesc.ContentIdx] ) && \
         ( BerDnObjId[1] == CertPtr[BerEleDesc.ContentIdx+1u] ) && \
         ( BerDnObjId[2] == CertPtr[BerEleDesc.ContentIdx+2u] ))
    {
      uint16 Length; /* PRQA S 0781 */ /* MD_Scc_0781 */

      if ( Scc_BEROID_Subject_DomainComponent == ObjectID )
      {
        /* check if the object identifier does not match */
        if ( IPBASE_CMP_EQUAL != IpBase_StrCmpLen(&BerDnObjIdDC[0], &CertPtr[BerEleDesc.ContentIdx], sizeof(BerDnObjIdDC)) )
        {
          /* search for the next object identifier within the issuer */
          DistinguishedNameEleNr[3]++;
          continue; /* PRQA S 0770 */ /* MD_Scc_0770 */
        }
      }

      DistinguishedNameEleNr[5]++;
      if ( IPBASE_E_OK != IpBase_BerGetElement(&Scc_BerWs, &BerEleDesc, &DistinguishedNameEleNr[0],
        sizeof(DistinguishedNameEleNr), &CertPtr[0], CertLen) )
      {
        break; /* PRQA S 0771 */ /* MD_Scc_0771 */
      }

      /* set the length of the common name from the BER structure. */
      Length = (uint16)(BerEleDesc.EndIdx - BerEleDesc.ContentIdx);

      /* if the output buffer is bigger than the data, set the output buffer length */
      if ( Length < *DataLenPtr )
      {
        /* limit copy length to source */
        *DataLenPtr = Length;
      }

      /* check if it is not the EMAID */
      if ( Scc_BEROID_Subject_EMAID != ObjectID )
      {
        /* check if CN does not fit into the buffer */
        if ( Length > *DataLenPtr )
        {
          break; /* PRQA S 0771 */ /* MD_Scc_0771 */
        }

        /* #40 extract the object */
        /* PRQA S 0315 2 */ /* MD_MSR_VStdLibCopy */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) DataPtr,
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &CertPtr[BerEleDesc.ContentIdx], *DataLenPtr);
      }
      /* if it's an EMAID */
      else
      {
        uint16 CertIdx = 0;
        uint16 DataIdx = 0;

        /* #50 copy the EMAID */
        while (( DataIdx < *DataLenPtr ) && ( CertIdx < Length ))
        {
          /* do not copy the separators */
          if ( (uint8)'-' != CertPtr[BerEleDesc.ContentIdx+CertIdx] )
          {
            /* copy the data */
            DataPtr[DataIdx] = CertPtr[BerEleDesc.ContentIdx+CertIdx];
            /* increase the output buffer index */
            DataIdx++;
          }
          /* increase the input buffer index */
          CertIdx++;
        }
        /* set the new length, without the separators */
        *DataLenPtr = DataIdx;
      }

      retVal = E_OK;
      break; /* PRQA S 0771 */ /* MD_Scc_0771 */
    }
    /* if the object was not found */
    else
    {
      /* step to the next object identifier */
      DistinguishedNameEleNr[3]++;
    }
  }

  return retVal;
} /* PRQA S 6010,6030,6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STMIF */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_TLS == STD_ON )
/**********************************************************************************************************************
 *  Scc_SearchDomainComponentForValue
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_SearchDomainComponentForValue(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr, uint16 CertLen, P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) ValuePtr, uint8 ValueLen)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  IpBase_BerElementType BerEleDesc;
  uint8                 BerObjIdDC[10] = { 0x09, 0x92, 0x26, 0x89, 0x93, 0xf2, 0x2c, 0x64, 0x01, 0x19 };
  uint8                 BerIssuerEleNr[6] = { 1, 1, 6, 1, 1, 1 };

  /* ----- Implementation ----------------------------------------------- */
  BerEleDesc.Idx = 0;

  /* #10 search the domain component within the issuer */
  while ( BerEleDesc.Idx < CertLen )
  {
    /* #20 search for the object identifier and check if an error occurred */
    if ( IPBASE_E_OK != IpBase_BerGetElement(&Scc_BerWs, &BerEleDesc, &BerIssuerEleNr[0], sizeof(BerIssuerEleNr),
      &CertPtr[0], CertLen) )
    {
      /* certificate has invalid structure, stop searching */
      break; /* PRQA S 0771 */ /* MD_Scc_0771 */
    }
    /* #30 check if the length does not match */
    if ( ( BerEleDesc.EndIdx - BerEleDesc.ContentIdx ) != sizeof(BerObjIdDC) )
    {
      /* skip this element */
    }
    /* #40 check if the object identifier matches */
    else if ( IPBASE_CMP_EQUAL == IpBase_StrCmpLen(&BerObjIdDC[0], &CertPtr[BerEleDesc.ContentIdx], sizeof(BerObjIdDC)) )  /* PRQA S 2004 */ /* MD_Scc_2004 */
    {
      /* DomainComponent was found, switch from AttributeType to AttributeValue */
      BerIssuerEleNr[5]++;
      /* get the AttributeValue and check if an error occurred */
      if ( IPBASE_E_OK != IpBase_BerGetElement(&Scc_BerWs, &BerEleDesc, &BerIssuerEleNr[0], sizeof(BerIssuerEleNr),
        &CertPtr[0], CertLen) )
      {
        /* invalid encoding of attribute value */
        /* switch back to AttributeType and continue to search for other elements */
        BerIssuerEleNr[5]--;
      }
      /* #50 check if Domain Component in Certificate matches the provided value */
      else if ( ValueLen == (BerEleDesc.EndIdx - BerEleDesc.ContentIdx) )
      {
        if ( IPBASE_CMP_EQUAL == IpBase_StrCmpLen(&ValuePtr[0], &CertPtr[BerEleDesc.ContentIdx], ValueLen) )
        {
          retVal = E_OK;
          break; /* PRQA S 0771 */ /* MD_Scc_0771 */
        }
        else
        {
          /* this Domain Component entry was not the wanted one, search for another one */
          /* switch back to AttributeType and continue the search */
          BerIssuerEleNr[5]--;
        }
      }
      else
      {
        /* this Domain Component entry was not the wanted one, search for another one */
        /* switch back to AttributeType and continue the search */
        BerIssuerEleNr[5]--;
      }
    }

    /* search for the next object identifier within the issuer */
    BerIssuerEleNr[3]++;
  }

  return retVal;
}  /* PRQA S 6010,6080 */ /* MD_MSR_STPTH,MD_MSR_STMIF */
#endif /* SCC_ENABLE_TLS */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_GetCertSerialNumber
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_GetCertSerialNumber(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr, uint16 CertLen,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) DataLenPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType RetVal = E_NOT_OK;
  IpBase_ReturnType IpBaseRetVal;

  /* ----- Implementation ----------------------------------------------- */
  /* parameters to get the Serial Number */
  CONST(uint8, SCC_CONST) BerSerialNumEleNr[3] = { 1, 1, 2 };
  IpBase_BerElementType BerEleDesc;

  /* #10 Search the Serial Number in the given certificate */
  IpBaseRetVal = IpBase_BerGetElement(&Scc_BerWs, &BerEleDesc, &BerSerialNumEleNr[0], sizeof(BerSerialNumEleNr),
    &CertPtr[0], CertLen);
  /* check if the Serial was found */
  if ( IPBASE_E_OK == IpBaseRetVal )
  {
    /* check if the Serial Number is smaller than the output buffer */
    if ( ( BerEleDesc.EndIdx - BerEleDesc.ContentIdx ) < *DataLenPtr )
    {
      *DataLenPtr = (uint16)( BerEleDesc.EndIdx - BerEleDesc.ContentIdx );
    }
    /* #20 copy the Serial Number to the output buffer */
    IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) DataPtr,  /* PRQA S 0310,3305, 0315 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
      (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &CertPtr[BerEleDesc.ContentIdx], *DataLenPtr);

    RetVal = E_OK;
  }
  /* if no Serial was found */
  else if ( IPBASE_E_NOT_OK == IpBaseRetVal )
  {
    /* report an error with the certificates */
    Scc_ReportError(Scc_StackError_Crypto);
  }
  /* if another problem occurred */
  else
  {
    /* report a problem with IpBase */
    Scc_ReportError(Scc_StackError_IpBase);
  }

  return RetVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_GetCertIssuer
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */

FUNC(Std_ReturnType, SCC_CODE) Scc_GetCertIssuer(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr, uint16 CertLen,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) DataLenPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  uint16 lBufIdx = 0;
  uint16 RemOutBufLen;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 check if the output buffer is big enough for "CN=X,O=X", which is the smallest possible DN */
  if ( sizeof("CN=X,O=X") < *DataLenPtr )
  {
    /* #20 parse the CommonName (CN) into the output string */

    /* set the start tag */
    DataPtr[lBufIdx] = (uint8)'C';
    DataPtr[lBufIdx+1u] = (uint8)'N';
    DataPtr[lBufIdx+2u] = (uint8)'=';
    lBufIdx += 3u;
    RemOutBufLen = *DataLenPtr - lBufIdx;

    /* try to append the value */
    if ( (Std_ReturnType)E_OK == Scc_GetCertDistinguishedNameObject(CertPtr, CertLen, &DataPtr[lBufIdx], &RemOutBufLen,
      Scc_BEROID_Issuer_CommonName) )
    {
      /* adjust the lBufIdx */
      lBufIdx += RemOutBufLen;

      /* #30 parse the Organization (O) into the output string */

      /* add the comma */
      DataPtr[lBufIdx] = (uint8)',';
      lBufIdx++;

      /* set the start tag */
      DataPtr[lBufIdx] = (uint8)'O';
      DataPtr[lBufIdx+1u] = (uint8)'=';
      lBufIdx += 2u;
      RemOutBufLen = *DataLenPtr - lBufIdx;

      /* try to append the value */
      if ( (Std_ReturnType)E_OK == Scc_GetCertDistinguishedNameObject(CertPtr, CertLen, &DataPtr[lBufIdx], &RemOutBufLen,
        Scc_BEROID_Issuer_Organization) )
      {
        /* adjust the lBufIdx */
        lBufIdx += RemOutBufLen;

        /* #40 parse the OrganizationUnit (OU) into the output string */

        /* element is optional, therefore the 4 bytes may not be required */
        RemOutBufLen = *DataLenPtr - ( lBufIdx + 4u );

        /* try to append the value */
        if ( (Std_ReturnType)E_OK != Scc_GetCertDistinguishedNameObject(CertPtr, CertLen, &DataPtr[lBufIdx + 4u], &RemOutBufLen,
          Scc_BEROID_Issuer_OrganizationUnit) )
        {
          /* the Organization element is optional -> no error */
          *DataLenPtr = lBufIdx;
          retVal = E_OK;
        }
        else
        {
          /* add the comma */
          DataPtr[lBufIdx] = (uint8)',';
          lBufIdx++;

          /* set the start tag */
          DataPtr[lBufIdx] = (uint8)'O';
          DataPtr[lBufIdx+1u] = (uint8)'U';
          DataPtr[lBufIdx+2u] = (uint8)'=';
          lBufIdx += 3u;

          /* adjust the lBufIdx */
          lBufIdx += RemOutBufLen;

          /* #50 parse the Country (C) into the output string */

          RemOutBufLen = *DataLenPtr - ( lBufIdx + 3u );

          /* try to append the value */
          if ( (Std_ReturnType)E_OK == Scc_GetCertDistinguishedNameObject(CertPtr, CertLen, &DataPtr[lBufIdx + 3u], &RemOutBufLen,
            Scc_BEROID_Issuer_Country) )
          {
            /* add the comma */
            DataPtr[lBufIdx] = (uint8)',';
            lBufIdx++;

            /* set the start tag */
            DataPtr[lBufIdx] = (uint8)'C';
            DataPtr[lBufIdx+1u] = (uint8)'=';
            lBufIdx += 2u;

            /* adjust the lBufIdx */
            lBufIdx += RemOutBufLen;
          }

          *DataLenPtr = lBufIdx;
          retVal = E_OK;
        }
      }
    }

  }

  return retVal;
}/* PRQA S 6080 */ /* MD_MSR_STMIF */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_TLS == STD_ON )
/**********************************************************************************************************************
 *  Scc_GetCertSubjectRaw
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */

FUNC(Std_ReturnType, SCC_CODE) Scc_GetCertSubjectRaw(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr, uint16 CertLen,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) DataLenPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  IpBase_ReturnType IpBaseRetVal;

  /* parameters to get the Subject */
  CONST(uint8, SCC_CONST) BerSubjectEleNr[3] = { 1, 1, 6 };
  IpBase_BerElementType BerEleDesc;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 search the Subject in the given certificate */
  IpBaseRetVal = IpBase_BerGetElement(&Scc_BerWs, &BerEleDesc, &BerSubjectEleNr[0], sizeof(BerSubjectEleNr),
    &CertPtr[0], CertLen);
  /* check if the Subject was found */
  if ( IPBASE_E_OK == IpBaseRetVal )
  {
    /* #20 check if the Subject is smaller than the output buffer */
    if ( ( BerEleDesc.EndIdx - BerEleDesc.Idx ) < *DataLenPtr )
    {
      *DataLenPtr = (uint16)( BerEleDesc.EndIdx - BerEleDesc.Idx );
    }
    /* #30 copy the Subject to the output buffer */
    IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))DataPtr, /* PRQA S 0310, 3305, 0315 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
      (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&CertPtr[BerEleDesc.Idx], *DataLenPtr);

    retVal = (Std_ReturnType)E_OK;
  }
  else
  {
    /* subject not found */
  }

  return retVal;
}
#endif /* SCC_ENABLE_TLS */

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )

/***********************************************************************************************************************
 *  Scc_CheckCertTimeExpiration
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_CheckCertTimeExpiration(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr,
  uint32 CertLen, uint32 CurrentUnixTime)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType              retVal = E_NOT_OK;

  /* check validity (time) of received certificate */
  uint32                      TimeNotAfter;
  uint16                      ValidityEndOffs;
  uint16                      ValidityEndLen;
  CONST(uint8, SCC_CONST)       EleNrValidityEnd[4] = { 1, 1, 5, 2 };

  IpBase_ReturnType           ipBaseRetVal;
  IpBase_BerElementType       EleDesc;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get position of validity end time info from the certificate */
  ipBaseRetVal = IpBase_BerGetElement(&Scc_BerWs, &EleDesc, EleNrValidityEnd, 4, CertPtr, CertLen);
  if( IPBASE_E_OK == ipBaseRetVal )
  {
    /* validity end */
    ValidityEndOffs = (uint16)EleDesc.ContentIdx;
    ValidityEndLen = (uint16)(EleDesc.EndIdx - EleDesc.ContentIdx);


    /* #20 Read validity end time info from certificate */
    Scc_ReadCertTimeInfo( &(CertPtr[ValidityEndOffs]),  (uint8)(ValidityEndLen),  &TimeNotAfter );

    /* #30 compare with validity end */
    if( CurrentUnixTime <= TimeNotAfter )
    {
      retVal = E_OK;
    }
  }

  return retVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/***********************************************************************************************************************
 *  Scc_ReadCertTimeInfo
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_ReadCertTimeInfo(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr,
  uint8 Len, CONSTP2VAR(uint32, AUTOMATIC, SCC_APPL_DATA) Time32Ptr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType ipBaseRetVal = E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  Scc_TimeDTType TimeDT;
  uint32         TempInt;
  uint8          TempOffset;

  /* ----- Implementation ----------------------------------------------- */

  /* #10 copy date and time from certificate: */

  if(13u == Len)
  {
    /* UTCTime */
    ipBaseRetVal = IpBase_ConvString2Int(&DataPtr[0], 2, &TempInt);  /* cert.year starts in 1950 */
    if(TempInt < 50u)
    {
      /* years 2000 .. 2049 */
      TimeDT.Year = (uint8)TempInt + 100u;
    }
    else
    {
      /* years 1950 .. 1999 */
      /* nothing to do */
    }
    TempOffset = 2u;
  }
  else
  {
    /* (15 == Len) GeneralizedTime */
    ipBaseRetVal = IpBase_ConvString2Int(&DataPtr[0], 4, &TempInt);
    /* 4 digit year */
    TimeDT.Year = (uint8)(TempInt - 1900u);  /* This will fail starting in the year 2156 */
    TempOffset = 4;
  }

  ipBaseRetVal |= IpBase_ConvString2Int(&DataPtr[TempOffset +0u], 2, &TempInt);  /* cert.mon = 1..12 */ /* PRQA S 2985 */ /* MD_Scc_QAC_Mistaken */
  if ( (Std_ReturnType)E_OK == ipBaseRetVal )
  {
    TimeDT.Mon  = (uint8)TempInt - 1u;
  }

  ipBaseRetVal |= IpBase_ConvString2Int(&DataPtr[TempOffset +2u], 2, &TempInt);  /* cert.mday = 1..31 */
  if ( (Std_ReturnType)E_OK == ipBaseRetVal )
  {
    TimeDT.Mday = (uint8)TempInt;
  }

  ipBaseRetVal |= IpBase_ConvString2Int(&DataPtr[TempOffset +4u], 2, &TempInt);
  if ( (Std_ReturnType)E_OK == ipBaseRetVal )
  {
    TimeDT.Hour = (uint8)TempInt;
  }

  ipBaseRetVal |= IpBase_ConvString2Int(&DataPtr[TempOffset +6u], 2, &TempInt);
  if ( (Std_ReturnType)E_OK == ipBaseRetVal )
  {
    TimeDT.Min  = (uint8)TempInt;
  }

  ipBaseRetVal |= IpBase_ConvString2Int(&DataPtr[TempOffset +8u], 2, &TempInt);
  if ( (Std_ReturnType)E_OK == ipBaseRetVal )
  {
    TimeDT.Sec  = (uint8)TempInt;
  }

  if ( (Std_ReturnType)E_OK == ipBaseRetVal )
  {
    ipBaseRetVal = Scc_ConvertTimeDT2S(&TimeDT, Time32Ptr); /* Time32Ptr overrun in the year 2106 */ /*lint !e645 */
  }

  if ( (Std_ReturnType)E_OK != ipBaseRetVal )
  {
    /* Error converting time: assume 01.01.1970 */
    *Time32Ptr = (uint32)0u;
  }

  return;
} /* PRQA S 6010 */ /* MD_MSR_STPTH */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/***********************************************************************************************************************
 *  Scc_ConvertTimeDT2S
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_ConvertTimeDT2S(P2CONST(Scc_TimeDTType, AUTOMATIC, SCC_APPL_DATA) TimeDTPtr,
  P2VAR(uint32, AUTOMATIC, SCC_APPL_DATA) TimeSPtr)
{
  /* - Every 4th year is a leap year, every hundred years the leap year is skipped, every 400 years the leap year takes
       place.
     - Scc_TimeDTType.Year starts in 1970 */

  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  uint32         TotalSecs;
  uint8          NumOfLeapYears;  /* complete leap years since 1970 (not including current year) */
  uint16         DaysInYear;      /* completed days */
  const uint8    DaysInMonth[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  uint8          i;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Add days in current year. */
  DaysInYear = 0;
  for(i=0; i < TimeDTPtr->Mon; i++)
  {
    DaysInYear += DaysInMonth[i];
  }
  /* Day of month should not be 0 */
  if ( 0u < TimeDTPtr->Mday )
  {
    DaysInYear += ((uint16)TimeDTPtr->Mday -1u);
    /* Year has to be at least 100 (Year starts at 2000) */
    if ( 70u <= TimeDTPtr->Year )
    {
      /* #20 Check if this year is a leap year */
      if(0u == (((TimeDTPtr->Year -70u) +2u) % 4u))
      {
        /* this is a leap year */
        if(TimeDTPtr->Mon > 1u) /* March to December */
        {
          DaysInYear += 1u;  /* add a leap day */
        }
      }

      /* Get all leap years since 1970 (not including current year) */
      NumOfLeapYears = ((TimeDTPtr->Year -70u) +1u) / 4u;

      /* #30 Convert minutes, hours, days and years to seconds */
      /* add years, underflow check already done */
      TotalSecs = ((((((uint32)TimeDTPtr->Year) -70u) *365u) + (uint32)NumOfLeapYears) * 86400u);
      TotalSecs += ((uint32)DaysInYear * 86400u);       /* add days in this year */
      TotalSecs += ((uint32)TimeDTPtr->Hour * 3600u);   /* add hours */
      TotalSecs += ((uint32)TimeDTPtr->Min * 60u);      /* add minutes */
      TotalSecs += ((uint32)TimeDTPtr->Sec);            /* add seconds */

      *TimeSPtr = TotalSecs;

      retVal = E_OK;
    }
  }

  return retVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_TLS == STD_ON )
/**********************************************************************************************************************
 *  Scc_GetCertSize
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_GetCertSize(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr,
                                               P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) CertLenPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  CONST(uint8, SCC_CONST) BerEleLen[1] = { 1 };
  CONST(uint8, SCC_CONST) BerEleLenDepth = 1;
  IpBase_BerElementType BerEleDesc;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the certificate element and check if an error occurred */
  if( IPBASE_E_OK != IpBase_BerGetElement(&Scc_BerWs, &BerEleDesc, &BerEleLen[0], BerEleLenDepth,
    &CertPtr[0], *CertLenPtr) )
  {
    Scc_ReportError(Scc_StackError_IpBase);
  }
  else
  {
  /* #20 Get the size of the certificate */
    *CertLenPtr = (uint16)IpBase_BerSizeOfElement(BerEleDesc);
    retVal = E_OK;
  }

  return retVal;
}
#endif /* SCC_ENABLE_TLS */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_GetIndexOfPublicKey
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_GetIndexOfPublicKey(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr, uint16 CertLen,
  P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) PubKeyIdxPtr, P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) PubKeyLenPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  IpBase_ReturnType IpBaseRetVal;
  /* parameters to get the PublicKey */
  CONST(uint8, SCC_CONST) BerPubKeyEleNr[4] = { 1, 1, 7, 2 };
  IpBase_BerElementType BerEleDesc;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 search the PublicKey in the given certificate */
  IpBaseRetVal = IpBase_BerGetElement(&Scc_BerWs, &BerEleDesc, &BerPubKeyEleNr[0], sizeof(BerPubKeyEleNr),
    &CertPtr[0], CertLen);
  /* check if the PublicKey was found */
  if ( IPBASE_E_OK == IpBaseRetVal )
  {
    /* #20 Get position and length of the PublicKey */
    *PubKeyIdxPtr = (uint16)( BerEleDesc.ContentIdx + 1u ); /* skip the additional information */
    *PubKeyLenPtr = (uint16)( BerEleDesc.EndIdx - BerEleDesc.ContentIdx - 1u );
    retVal = E_OK;
  }
  /* if an issue with IpBase occurred */
  else
  {
    Scc_ReportError(Scc_StackError_IpBase);
  }

  return retVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
*  Scc_SwapContrCertChain
*********************************************************************************************************************/
/*!
*
* Internal comment removed.
 *
 *
*/
FUNC(Scc_ReturnType, SCC_CODE) Scc_SwapContrCertChain(uint8 NewContractCertificateChainIndex, boolean ForceSwap)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8_least Counter;
  Scc_ReturnType RetVal = Scc_ReturnType_NotOK;

  /* ----- Implementation ----------------------------------------------- */
  /* check if the selected chain is not the current one */
  if ((TRUE == ForceSwap)
    || ((NewContractCertificateChainIndex != Scc_CertsWs.ChosenContrCertChainIdx)
      && (Scc_State_Connected != Scc_State)))
  {
    /* check if the selected chain exists */
    if (NewContractCertificateChainIndex < Scc_CertsNvm.ContrCertChainNvmCnt)
    {
      /* #10 Swap ContractCertificate and reset read states. */
      /* change the index */
      Scc_CertsWs.ChosenContrCertChainIdx = NewContractCertificateChainIndex;
      /* set the new sub cert slot cnt */
      Scc_CertsWs.ContrSubCertCnt = Scc_CertsNvm.ContrCertChains[NewContractCertificateChainIndex].ContrSubCertCnt;

      /* reset the read states */
      Scc_CertsWs.ContrCertChainSizeReadState = Scc_NvMBlockReadState_NotSet;
      Scc_CertsWs.ContrCertReadState = Scc_NvMBlockReadState_NotSet;
      Scc_CertsWs.ProvCertReadState = Scc_NvMBlockReadState_NotSet;

      /* reset the read states of the sub certificates */
      Scc_CertsWs.ContrSubCertsProcessedFlags = 0;
      for (Counter = 0; Counter < Scc_CertsNvm.ContrCertChains[NewContractCertificateChainIndex].ContrSubCertCnt;
        Counter++)
      {
        Scc_CertsWs.ContrSubCertsReadStates[Counter] = Scc_NvMBlockReadState_NotSet;
      }

      RetVal = Scc_ReturnType_OK;
    }
  }
  /* check if the selected chain is already active */
  else if (NewContractCertificateChainIndex == Scc_CertsWs.ChosenContrCertChainIdx)
  {
    RetVal = Scc_ReturnType_OK;
  }
  else
  {
    /* a V2G session is active */
    RetVal = Scc_ReturnType_Busy;
  }

  return RetVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ValidateCertAgainstRootCert
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_ValidateCertAgainstRootCert(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr,
  P2CONST(uint16, AUTOMATIC, SCC_APPL_DATA) CertLengthPtr, P2CONST(uint32, AUTOMATIC, SCC_APPL_DATA) CurrentTime,
  uint8 RootCertIdx)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType           RetVal = E_NOT_OK;
  IpBase_ReturnType        IpBaseRetVal;
  IpBase_BerElementType    BerEleDesc;

  /* parameters to get the SignedInfo */
  CONST(uint8, SCC_CONST)    BerSignedInfoEleNr[2] = { 1, 1 };
  uint32                   SignedInfoPos;
  uint16                   SignedInfoLen;

  /* parameters to get the Signature */
  CONST(uint8, SCC_CONST)    BerSignatureEleNr[2] = { 1, 3 };
  uint32                   SignaturePos;
  uint8                    SignatureLen;

  uint16                   PubKeyIdx;
  uint16                   PubKeyLen;
  Crypto_VerifyResultType  verifyResult;
  uint8                    SignatureEncoded[SCC_P256R1_SIGNATURE_LEN];


  /* ----- Implementation ----------------------------------------------- */
  if ( (Std_ReturnType)E_OK == Scc_CheckCertTimeExpiration(CertPtr, *CertLengthPtr, *CurrentTime) )
  {
    /* get the SignedInfo from the certificate */
    IpBaseRetVal = IpBase_BerGetElement(&Scc_BerWs, &BerEleDesc, &BerSignedInfoEleNr[0], sizeof(BerSignedInfoEleNr),
      CertPtr, *CertLengthPtr);
    /* set the pointer and the length of the SignedInfo */
    SignedInfoPos = BerEleDesc.Idx;
    SignedInfoLen = (uint16)(BerEleDesc.EndIdx - BerEleDesc.Idx);

    /* get the Signature from the certificate */
    IpBaseRetVal |= IpBase_BerGetElement(&Scc_BerWs, &BerEleDesc, &BerSignatureEleNr[0], sizeof(BerSignatureEleNr),
      CertPtr, *CertLengthPtr);
    /* get the position and the length of the Signature */
    SignaturePos = BerEleDesc.ContentIdx + 1u;
    SignatureLen = (uint8)(BerEleDesc.EndIdx - BerEleDesc.ContentIdx - 1u);

    /* check if an error occurred during the BER operations */
    if ( IPBASE_E_OK != IpBaseRetVal )
    {
      /* retVal is already set to E_NOT_OK, nothing to do here */
    }
    else if ( (Std_ReturnType)E_OK != Scc_GetSignatureElemOfBerCert((&CertPtr[SignaturePos]),
      &SignatureLen, &SignatureEncoded[0], SCC_P256R1_SIGNATURE_LEN) )
    {
      Scc_ReportError(Scc_StackError_IpBase);
    }
    /* Get PublicKey of given root certificate */
    else if ( (Std_ReturnType)E_OK != Scc_GetIndexOfPublicKey(&Scc_CertsNvm.RootCerts[RootCertIdx].RootCert->Buffer[0],
      Scc_CertsNvm.RootCerts[RootCertIdx].RootCert->Length, &PubKeyIdx, &PubKeyLen) )
    {
      Scc_ReportError(Scc_StackError_IpBase);
    }
    /* PubKeyPtr[0] == 0x04 --> uncompressed point */
    else if ( (PubKeyLen != (SCC_P256R1_PUBLIC_KEY_LEN + 1u)) && (Scc_CertsNvm.RootCerts[RootCertIdx].RootCert->Buffer[PubKeyIdx] != 0x04u) )
    {
      Scc_ReportError(Scc_StackError_Crypto);
    }
    else if ( (Std_ReturnType)E_OK != Csm_KeyElementSet(Scc_ECDSAVerifyKeyId, CRYPTO_KE_CERTIFICATE_SUBJECT_PUBLIC_KEY,
      &Scc_CertsNvm.RootCerts[RootCertIdx].RootCert->Buffer[PubKeyIdx + 1u], (uint32)PubKeyLen - 1u) )
    {
      Scc_ReportError(Scc_StackError_Crypto);
    }
    else if ( (Std_ReturnType)E_OK != Csm_KeySetValid(Scc_ECDSAVerifyKeyId) )
    {
      Scc_ReportError(Scc_StackError_Crypto);
    }
    /* #10 Verify Signature of certificate with PublicKey of root certificate */
    else if ( (Std_ReturnType)E_OK != Csm_SignatureVerify(Scc_ECDSAVerifyJobId, CRYPTO_OPERATIONMODE_SINGLECALL,
      &CertPtr[SignedInfoPos], SignedInfoLen,
      &SignatureEncoded[0], 64, &verifyResult) )
    {
      Scc_ReportError(Scc_StackError_Crypto);
    }
    else if ( CRYPTO_E_VER_OK != verifyResult )
    {
      /* retVal is already set to E_NOT_OK, nothing to do here */
    }
    else
    {
      RetVal = E_OK;
    }
  }


  return RetVal;
} /* PRQA S 6050, 6010, 6080, 6030 */ /* MD_MSR_STCAL, MD_MSR_STPTH, MD_MSR_STMIF, MD_MSR_STCYC */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_GetCertsFromPKCS7
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Scc_ReturnType, SCC_CODE) Scc_GetCertsFromPKCS7(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertChainMsgPtr,
  uint16 CertChainLength, P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) NumberOfCertsPtr,
  P2VAR(Scc_CertificateType, AUTOMATIC, SCC_APPL_DATA) CertChainStructPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType               RetVal = Scc_ReturnType_OK;

  /* parameters for BER Decoder */
  IpBase_BerElementType        BerEleDesc;

  /* parameters to get the Certificate */
  uint8                        BerPKCS7EleNr[4] = { 1, 2, 1, 4 };
  uint8                        BerCertEleNr[2] = { 1, 1 };
  uint16                       BerOffset;
  uint8                        CertCount = 0;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get all Certificates from PKCS7 structure */
  if ( IPBASE_E_OK != IpBase_BerGetElement(&Scc_BerWs, &BerEleDesc, &BerPKCS7EleNr[0], sizeof(BerPKCS7EleNr),
    &CertChainMsgPtr[0], CertChainLength) )
  {
    RetVal = Scc_ReturnType_NotOK;
  }
  else
  {
    BerOffset = (uint16)BerEleDesc.Idx;
    for ( CertCount = 0; CertCount < *NumberOfCertsPtr; CertCount++ )
    {
      /* #20 Get position and length of certificate in PKCS7 structure */
      if ( IPBASE_E_OK != IpBase_BerGetElement(&Scc_BerWs, &BerEleDesc, &BerCertEleNr[0], sizeof(BerCertEleNr),
        &CertChainMsgPtr[BerOffset], CertChainLength) )
      {
        break;
      }
      else
      {
        /* #30 Set length and position chained list of certificates */
        if (0u != CertCount )
        {
          CertChainStructPtr[CertCount-1u].NextCertificatePtr = &CertChainStructPtr[CertCount];
        }
        CertChainStructPtr[CertCount].NextCertificatePtr = NULL_PTR;
        CertChainStructPtr[CertCount].Length = (uint16) (BerEleDesc.EndIdx - BerEleDesc.Idx);
        CertChainStructPtr[CertCount].Position = (uint16) (BerEleDesc.Idx + BerOffset);

        BerCertEleNr[1]++;
      }
    }
  }

  *NumberOfCertsPtr = CertCount;

  return RetVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_GetSignatureElemOfBerCert
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_GetSignatureElemOfBerCert(P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) SignaturePtr,
  P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) SignatureLenPtr, P2VAR(uint8, AUTOMATIC, SCC_VAR_NOINIT) SignatureEncodedPtr, uint8 SignatureEncodedLen)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8_least rLength;
  uint8_least sLength;

  P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) SignRPtr;
  P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) SignSPtr;

  Std_ReturnType RetVal = (Std_ReturnType)E_NOT_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* signature value is BER encoded */
  /* do some manual BER decoding */
  if((*SignatureLenPtr > 64u) &&
     (SignaturePtr[0] == 0x30u) && /* sequence */
     /* SignatureValuePtr[1] is the signature total length. this value is not evaluated here, only r and s length are taking into account later */
     (SignaturePtr[2] == 0x02u)) /* integer */
  {
    /* #10 read r length and check total length */
    rLength = SignaturePtr[3];
    if((*SignatureLenPtr > (4u + rLength + 2u)) &&
       (SignaturePtr[4u + rLength] == 0x02u)) /* integer */
    {
      sLength = SignaturePtr[5u + rLength];
      /* #20 get r and s value pointers, value first because rLength may be manipulated */
      if(0x21u == sLength)
      {
        /* zero byte will be removed in case first byte >= 0x80 */
        SignSPtr = &SignaturePtr[7u + rLength];
        sLength--;
      }
      else
      {
        SignSPtr = &SignaturePtr[6u + rLength];
      }
      if(0x21u == rLength)
      {
        /* zero byte will be removed in case first byte >= 0x80 */
        SignRPtr = &SignaturePtr[5];
        rLength--;
      }
      else
      {
        SignRPtr = &SignaturePtr[4];
      }

      if ( SignatureEncodedLen >= SCC_P256R1_SIGNATURE_LEN )
      {
        /* #30 Copy signature to buffer */
        /* PRQA S 0315 2 */ /* MD_MSR_VStdLibCopy */
        (void)IpBase_Copy( &SignatureEncodedPtr[0], SignRPtr, rLength);
        (void)IpBase_Copy( &SignatureEncodedPtr[rLength], SignSPtr, sLength);

        RetVal = (Std_ReturnType)E_OK;
      }
    }
  }

  return RetVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ValidateCertChain
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_ValidateCertChain(P2CONST(Exi_ISO_certificateType, AUTOMATIC, SCC_VAR_NOINIT) CertChainPtr,
  uint32 CurrentTime)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8                    errorId = SCC_DET_NO_ERROR;
  Std_ReturnType           retVal = E_NOT_OK;

  /* common parameters */
  IpBase_ReturnType        IpBaseRetVal;
  P2CONST(Exi_ISO_certificateType, AUTOMATIC, SCC_VAR_NOINIT) ExiCertPtr = CertChainPtr;

  /* parameters for BER */
  IpBase_BerElementType    BerEleDesc;

  /* parameters to get the certificate, which needs to be hashed */
  CONST(uint8, SCC_CONST)  BerSignedInfoEleNr[2] = { 1, 1 };
  uint32                   SignedInfoPos;
  uint16                   SignedInfoLen;

  /* parameters to get the Signature */
  CONST(uint8, SCC_CONST)  BerSignatureEleNr[2] = { 1, 3 };
  uint32                   SignaturePos;
  uint8                    SignatureLen;

  uint16                   PubKeyIdx;
  uint16                   PubKeyLen;
  Crypto_VerifyResultType  verifyResult;
  uint8                    SignatureEncoded[SCC_P256R1_SIGNATURE_LEN];

  /* ----- Development Error Checks ------------------------------------- */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of runtime parameters. */
  if ( CertChainPtr == NULL_PTR )
  {
    errorId = SCC_DET_INV_POINTER;
    retVal = E_NOT_OK;
  }
  else
#endif /* SCC_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Iterate over all certificates in the certificate chain. */
    while ( NULL_PTR != ExiCertPtr->NextCertificatePtr )
    {
      /* #30 Check the Expiration date of the current certificate. */
      if ( (Std_ReturnType)E_OK != Scc_CheckCertTimeExpiration(&ExiCertPtr->Buffer[0], ExiCertPtr->Length, CurrentTime) )
      {
        retVal = E_NOT_OK;
        break; /* PRQA S 0771 */ /* MD_Scc_0771 */
      }

      /* #40 Get the position and length of SignedInfo from certificate */
      IpBaseRetVal = IpBase_BerGetElement(&Scc_BerWs, &BerEleDesc, &BerSignedInfoEleNr[0], sizeof(BerSignedInfoEleNr),
        &ExiCertPtr->Buffer[0], ExiCertPtr->Length);
      /* get the pointer and the length of the SignedInfo */
      SignedInfoPos = BerEleDesc.Idx;
      SignedInfoLen = (uint16)(BerEleDesc.EndIdx - BerEleDesc.Idx);

      /* #50 Get the position and length of signature from certificate */
      IpBaseRetVal |= IpBase_BerGetElement(&Scc_BerWs, &BerEleDesc, &BerSignatureEleNr[0], sizeof(BerSignatureEleNr),
        &ExiCertPtr->Buffer[0], ExiCertPtr->Length);
      /* get the position and the length of the Signature */
      SignaturePos = BerEleDesc.ContentIdx + 1u;
      SignatureLen = (uint8)(BerEleDesc.EndIdx - BerEleDesc.ContentIdx - 1u);

      /* check if an error occurred during the BER operations */
      if ( IPBASE_E_OK != IpBaseRetVal )
      {
        /* report error to the application */
        Scc_ReportError(Scc_StackError_IpBase);
        retVal = E_NOT_OK;
        break; /* PRQA S 0771 */ /* MD_Scc_0771 */
      }

      if ( (Std_ReturnType)E_OK != Scc_GetSignatureElemOfBerCert(&ExiCertPtr->Buffer[SignaturePos],
        &SignatureLen, &SignatureEncoded[0], SCC_P256R1_SIGNATURE_LEN) )
      {
        retVal = E_NOT_OK;
        break; /* PRQA S 0771 */ /* MD_Scc_0771 */
      }
      /* #60 Get the PublicKey in the next given certificate and check if it was found */
      else if ( (Std_ReturnType)E_OK != Scc_GetIndexOfPublicKey(&ExiCertPtr->NextCertificatePtr->Buffer[0],
        ExiCertPtr->NextCertificatePtr->Length, &PubKeyIdx, &PubKeyLen) )
      {
        retVal = E_NOT_OK;
        break; /* PRQA S 0771 */ /* MD_Scc_0771 */
      }
      /* PubKeyPtr[0] == 0x04 --> uncompressed point */
      else if (( PubKeyLen != ( SCC_P256R1_PUBLIC_KEY_LEN + 1u )) && ( ExiCertPtr->NextCertificatePtr->Buffer[PubKeyIdx] != 0x04u ))
      {
        Scc_ReportError(Scc_StackError_Crypto);
        retVal = E_NOT_OK;
        break; /* PRQA S 0771 */ /* MD_Scc_0771 */
      }
      /* #70 Set PublicKey */
      else if( (Std_ReturnType)E_OK != Csm_KeyElementSet(Scc_ECDSAVerifyKeyId, CRYPTO_KE_CERTIFICATE_SUBJECT_PUBLIC_KEY,
        &ExiCertPtr->NextCertificatePtr->Buffer[PubKeyIdx+1u], PubKeyLen-1uL))
      {
        retVal = E_NOT_OK;
        break; /* PRQA S 0771 */ /* MD_Scc_0771 */
      }
      else if( (Std_ReturnType)E_OK != Csm_KeySetValid(Scc_ECDSAVerifyKeyId))
      {
        retVal = E_NOT_OK;
        break; /* PRQA S 0771 */ /* MD_Scc_0771 */
      }
      /* #80 Verify signature with publicKey of next certificate */
      else if( (Std_ReturnType)E_OK != Csm_SignatureVerify(Scc_ECDSAVerifyJobId, CRYPTO_OPERATIONMODE_SINGLECALL,
        &ExiCertPtr->Buffer[SignedInfoPos], SignedInfoLen,
        &SignatureEncoded[0], 64, &verifyResult))
      {
        retVal = E_NOT_OK;
        break; /* PRQA S 0771 */ /* MD_Scc_0771 */
      }
      else if( CRYPTO_E_VER_OK != verifyResult )
      {
        retVal = E_NOT_OK;
        break; /* PRQA S 0771 */ /* MD_Scc_0771 */
      }
      /* otherwise select the next certificate in the chain */
      else
      {
        ExiCertPtr = ExiCertPtr->NextCertificatePtr;
        retVal = E_OK;
      }
    }
  }

  /* ----- Development Error Report --------------------------------------- */
#if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #90 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_VALIDATE_CERT_CHAIN, errorId);
  }
#else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* SCC_DEV_ERROR_REPORT */

  return retVal;
} /* PRQA S 6030, 6050, 6080 */ /* MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
*  Scc_ValidateContractCertChain()
*********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_ValidateContractCertChain(uint32 CurrentTime)
{
  /* #10 Link the contract certificate chain. */
  Scc_CertsWs.ContrCert->NextCertificatePtr = &Scc_CertsWs.ContrSubCerts[0];

  /* #20 Validate the contract certificate chain. */
  return Scc_ValidateCertChain(Scc_CertsWs.ContrCert, CurrentTime);
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_XmlSecGetPublicKey
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_XmlSecGetPublicKey(P2VAR(uint8*, AUTOMATIC, SCC_VAR_NOINIT) PubKeyPtr,
  P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) PubKeyLen, XmlSecurity_SignatureAlgorithmType SigAlgorithm)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType RetVal = (Std_ReturnType)E_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 check for which algorithm the public key is needed and if one was received before */
  if (   ( XmlSecurity_SAT_ECDSA_SHA256 == SigAlgorithm )
      && ( TRUE == Scc_SaPubKeyRcvd ))
  {
    *PubKeyPtr = &Scc_CertsWs.SaCertPubKey[0];
    *PubKeyLen = Scc_CertsWs.SaCertPubKeyLen;
  }
  /* unsupported algorithm or public key not received yet */
  else
  {
    RetVal = (Std_ReturnType)E_NOT_OK;
  }

  return RetVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( ( defined SCC_ENABLE_CERTIFICATE_INSTALLATION ) && ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )) \
 || ( ( defined SCC_ENABLE_CERTIFICATE_UPDATE ) && ( SCC_ENABLE_CERTIFICATE_UPDATE == STD_ON ))
/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_ExtractRootCertificateIDs
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiTx_ISO_ExtractRootCertificateIDs(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  uint8_least    Counter;
  uint8_least    CertIDIdx = 0;
  uint16_least   i;
  uint16         Offset;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over all root certificates */
  for ( Counter = 0; Counter < Scc_CertsNvm.RootCertNvmCnt; Counter++ )
  {
    /* check if this root cert is valid and was processed */
    if ( Scc_NvMBlockReadState_Processed == Scc_CertsWs.RootCertsReadStates[Counter] )
    {
      /* link the RootCertIDs, not necessary for the first entry */
      if ( 0u < CertIDIdx )
      {
        Scc_CertsWs.RootCertIDs[CertIDIdx-1u].NextRootCertificateIDPtr = &Scc_CertsWs.RootCertIDs[CertIDIdx];
      }

      /* set the max length of the issuer field */
      Scc_CertsWs.RootCertIDs[CertIDIdx].X509IssuerName->Length =
        sizeof(Scc_CertsWs.RootCertIDs[CertIDIdx].X509IssuerName->Buffer);
      /* #20 get the issuer name of this root certificate */
      if (E_OK != Scc_GetCertIssuer(&Scc_CertsNvm.RootCerts[Counter].RootCert->Buffer[0],
        Scc_CertsNvm.RootCerts[Counter].RootCert->Length, &Scc_CertsWs.RootCertIDs[CertIDIdx].X509IssuerName->Buffer[0],
        &Scc_CertsWs.RootCertIDs[CertIDIdx].X509IssuerName->Length))
      {
        /* set the error */
        Scc_CertsWs.RootCertsReadStates[Counter] = Scc_NvMBlockReadState_Error;
#if ( defined SCC_DEM_NVM_READ_ROOT_CERT_FAIL )
        Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_ROOT_CERT_FAIL);
#endif /* SCC_DEM_NVM_READ_ROOT_CERT_FAIL */
        /* start over from the top, the index will not be incremented as the content will be overwritten with the next certificate */
        continue; /* PRQA S 0770 */ /* MD_Scc_0770 */
      }

      /* set the max length of the serial number field */
      Scc_CertsWs.RootCertIDs[CertIDIdx].X509SerialNumber->Length =
        sizeof(Scc_CertsWs.RootCertIDs[CertIDIdx].X509SerialNumber->Value);
      /* #30 get the serial number of this root certificate */
      if (E_OK != Scc_GetCertSerialNumber(&Scc_CertsNvm.RootCerts[Counter].RootCert->Buffer[0],
        Scc_CertsNvm.RootCerts[Counter].RootCert->Length,
        &Scc_CertsWs.RootCertIDs[CertIDIdx].X509SerialNumber->Value[0],
        &Scc_CertsWs.RootCertIDs[CertIDIdx].X509SerialNumber->Length))
      {
        /* set the error */
        Scc_CertsWs.RootCertsReadStates[Counter] = Scc_NvMBlockReadState_Error;
#if ( defined SCC_DEM_NVM_READ_ROOT_CERT_FAIL )
        Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_ROOT_CERT_FAIL);
#endif /* SCC_DEM_NVM_READ_ROOT_CERT_FAIL */
        /* start over from the top, the index will not be incremented as the content will be overwritten with the next certificate */
        continue; /* PRQA S 0770 */ /* MD_Scc_0770 */
      }
      /* #40 check if this integer is negative */
      if ( 0x80u == ( Scc_CertsWs.RootCertIDs[CertIDIdx].X509SerialNumber->Value[0] & 0x80u ))
      {
        Scc_CertsWs.RootCertIDs[CertIDIdx].X509SerialNumber->IsNegative = TRUE;
      }
      else
      {
        Scc_CertsWs.RootCertIDs[CertIDIdx].X509SerialNumber->IsNegative = FALSE;
      }
      /* the data has to be right aligned */
      /* calculate the offset */
      Offset = (uint16)sizeof(Scc_CertsWs.RootCertIDs[CertIDIdx].X509SerialNumber->Value)
               - Scc_CertsWs.RootCertIDs[CertIDIdx].X509SerialNumber->Length;
      /* check if the offset is not 0 */
      if ( 0u != Offset )
      {
        /* loop through all bytes */
        for ( i = Scc_CertsWs.RootCertIDs[CertIDIdx].X509SerialNumber->Length; i > 0u; i-- )
        {
          /* shift them to their correct position */
          Scc_CertsWs.RootCertIDs[CertIDIdx].X509SerialNumber->Value[(i-1u)+Offset] =
            Scc_CertsWs.RootCertIDs[CertIDIdx].X509SerialNumber->Value[i-1u];
        }
      }

      CertIDIdx++;
    }
  }

  /* check if no root certificates are available */
  if ( 0u != CertIDIdx )
  {
    /* mark the end of the list */
    Scc_CertsWs.RootCertIDs[CertIDIdx-1u].NextRootCertificateIDPtr =
      (P2VAR(Exi_XMLSIG_X509IssuerSerialType, AUTOMATIC, SCC_VAR_NOINIT))NULL_PTR;

    /* at least one root cert ID could be read */
    retVal = E_OK;
  }

  return retVal;
}  /* PRQA S 6010 */ /* MD_MSR_STPTH */
#endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION, SCC_ENABLE_CERTIFICATE_UPDATE */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_RemoveDashesFromEMAID
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiRx_ISO_RemoveDashesFromEMAID(
  P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) inEMAIDPtr, CONST(uint16, SCC_CONST)inEMAIDLen,
  P2VAR(uint8, AUTOMATIC, SCC_VAR_NOINIT) outEMAIDPtr, P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) outEMAIDLenPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType RetVal = E_OK;

  uint8_least inEMAIDIdx  = 0;
  uint8_least outEMAIDIdx = 0;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate through the whole EMAID and add EMAID ot buffer ignore '-' */
  while ( inEMAIDIdx < inEMAIDLen )
  {
    if ( (uint8)'-' != inEMAIDPtr[inEMAIDIdx] )
    {
      /* check if their is enough space for one more character in the destination EMAID */
      if ( outEMAIDIdx < *outEMAIDLenPtr )
      {
        outEMAIDPtr[outEMAIDIdx] = inEMAIDPtr[inEMAIDIdx];
        outEMAIDIdx++;
      }
      else
      {
        RetVal = E_NOT_OK;
        break;
      }
    }

    /* increase the index of the source EMAID */
    inEMAIDIdx++;
  }

  *outEMAIDLenPtr = (uint8)outEMAIDIdx;

  return RetVal;
}

#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_InitWorkspaceDecodeExiData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(Scc_ReturnType, SCC_CODE) Scc_InitWorkspaceDecodeExiData(P2VAR(uint8, AUTOMATIC, SCC_RTE_DATA) ExiPtr, uint16 ExiLength)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType RetVal = Scc_ReturnType_NotOK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 check if the pointer are not a null pointers */
  if ( NULL_PTR != ExiPtr )
  {
    /* Set DataPtr to Exi Struct Buffer */
    Scc_ExiStreamRxPBuf[0].payload = &ExiPtr[0];
    Scc_ExiStreamRxPBuf[0].totLen = ExiLength;
    Scc_ExiStreamRxPBuf[0].len = ExiLength;

    /* #20 Initialize the XmlSecurity workspace */
    Scc_ExiRx_ISO_InitXmlSecurityWorkspace();

    /* #30 initialize the exi workspace and check if the workspace initialization failed */
    if ( (Std_ReturnType)E_OK == Exi_InitDecodeWorkspace((P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, SCC_RTE_DATA))&Scc_Exi_DecWs,
      (P2VAR(IpBase_PbufType, AUTOMATIC, SCC_RTE_DATA))&Scc_ExiStreamRxPBuf[0], (P2VAR(uint8, AUTOMATIC, SCC_RTE_DATA))&Scc_Exi_StructBuf[0],
      (uint16)SCC_EXI_STRUCT_BUF_LEN, 0) )
    {
      /* set the decode information */
      Scc_Exi_DecWs.OutputData.SchemaSetId = EXI_SCHEMA_SET_ISO_TYPE;
      /* #40 decode the message and check if it passed */
      if ( (Std_ReturnType)E_OK == Exi_Decode(&Scc_Exi_DecWs) )
      {
        RetVal = Scc_ReturnType_OK;
      }
      /* #50 Otherwise set error */
      else
      {
        /* report the error */
        Scc_ReportError(Scc_StackError_Exi);
      }
    }
  }

  return RetVal;
}

#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_TLS == STD_ON )
/**********************************************************************************************************************
 *  Scc_Get_ClientRandom_MasterSecret()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_Get_ClientRandom_MasterSecret(
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataLen) /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
#if ((defined TLS_SUPPORT_MASTER_SECRET_ACCESS) && (TLS_SUPPORT_MASTER_SECRET_ACCESS == STD_ON))
  uint8          lenMasterSecret = SCC_MASTER_SECRET_LEN;
  uint8          lenClientRandom = SCC_CLIENT_RANDOM_LEN;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the data length is enough to write master secret and client random */
  if ( *DataLen >= (SCC_MASTER_SECRET_LEN + SCC_CLIENT_RANDOM_LEN) )
  {
    /* #20 Try to get the client random from Tls and check if it passed */
    if ( Tls_GetClientRandom(Scc_V2GSocket, &DataPtr[0], &lenClientRandom) == (Std_ReturnType)E_OK )
    {
      /* /MICROSAR/Tls/TlsGeneral/TlsSupportMasterSecretAccess should be set to TRUE to use the below API */
      /* #30 Try to get the master secret from Tls and check if it passed */
      if ( Tls_GetMasterSecret(Scc_V2GSocket, &DataPtr[lenClientRandom], &lenMasterSecret) == (Std_ReturnType)E_OK )
      {
        *DataLen = lenMasterSecret + lenClientRandom;
        retVal = E_OK;
      }
    }
  }
#else /* TLS_SUPPORT_MASTER_SECRET_ACCESS */
  SCC_DUMMY_STATEMENT(DataPtr);
  SCC_DUMMY_STATEMENT(DataLen);
#endif /* TLS_SUPPORT_MASTER_SECRET_ACCESS */

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_Send_ClientRandom_MasterSecret()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_Send_ClientRandom_MasterSecret(
  P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, CONST(uint8, SCC_CONST) DataLen)
{
  Std_ReturnType retVal = E_NOT_OK;
  uint8 ChangeParameterParam = 1;
  uint16 Port = TCPIP_PORT_ANY;

  /* #10 Check if the UDP socket is not already bound to SCC */
  if ( (Scc_SocketType)0xFFu == Scc_SDPSocket )
  {
    /* #20 Get the socket */
    if ( (Std_ReturnType)E_OK !=
      TcpIp_SccGetSocket(TCPIP_AF_INET6, TCPIP_IPPROTO_UDP, &Scc_SDPSocket) )
    {
      Scc_ReportError(Scc_StackError_TransportLayer);
    }
    /* #30 Set the tx confirmation list */
    else if ( (Std_ReturnType)E_OK !=
      TcpIp_ChangeParameter(Scc_SDPSocket, TCPIP_PARAMID_V_UDP_TXREQLISTSIZE, &ChangeParameterParam) )
    {
      Scc_ReportError(Scc_StackError_TransportLayer);
    }
    /* #40 Bind the socket */
    else if ( (Std_ReturnType)E_OK != TcpIp_Bind(Scc_SDPSocket, (TcpIp_LocalAddrIdType)SCC_IPV6_ADDRESS, &Port) ) /* PRQA S 2004 */ /* MD_Scc_2004 */
    {
      Scc_ReportError(Scc_StackError_TransportLayer);
    }
    else
    {
      /* Do nothing */
    }
  }

  /* #50 Check if the UDP socket is valid */
  if ( Scc_SDPSocket != (Scc_SocketType)0xFFu )
  {
    Scc_SockAddrIn6Type IpAddrPortDst;
    uint8 buff[SCC_CLIENT_RANDOM_AND_MASTER_SECRET_LEN + SCC_EXTRA_BYTES_TO_BE_SENT_LEN];
    uint8 totalLen = SCC_CLIENT_RANDOM_AND_MASTER_SECRET_LEN;

    /* #60 Check if number bytes to be sent are within limit */
    if ( DataLen <= SCC_EXTRA_BYTES_TO_BE_SENT_LEN )
    {
      /* copy the bytes to be sent before client random and master secret */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_3305, MD_Scc_0310_3305, MD_MSR_VStdLibCopy  */
      IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))buff,
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&DataPtr[0],
        DataLen);

      /* #70 Try to get the client random and master secret and check if it was successful */
      if ( Scc_Get_ClientRandom_MasterSecret(&buff[DataLen], &totalLen) == (Std_ReturnType)E_OK ) /* PRQA S 2996,2992 */ /* MD_Scc_2992_2996 */
      {
        totalLen += DataLen; /* Send some bytes before client random */ /* PRQA S 2880 */ /* MD_Scc_2742_2880_2995 */
        /* #80 create and initialize the socket address struct */
        IpAddrPortDst.domain = SCC_IPvX_IPV6;
        IpAddrPortDst.port = IPBASE_HTON16(SCC_SDP_SERVER_PORT);
        /* copy the IP address */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_3305, MD_Scc_0310_3305, MD_MSR_VStdLibCopy */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&IpAddrPortDst.addr[0],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&IpV6_AddrAllNodesLL, sizeof(IpV6_AddrAllNodesLL));

        /* #90 transmit SDP request and check if it failed */
        if ( (Std_ReturnType)E_OK != TcpIp_UdpTransmit(Scc_SDPSocket, (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA))&buff[0],  /* PRQA S 0310 2 */ /* MD_Scc_0310 */
          (P2VAR(Scc_SockAddrType, AUTOMATIC, SCC_APPL_DATA))&IpAddrPortDst, totalLen) )
        {
          Scc_ReportError(Scc_StackError_TransportLayer);
        }
        /* Otherwise return ok */
        else
        {
          retVal = E_OK;
        }
      }
    }
  }

  /* #100 Close UDP socket since it is no longer necessary */
  (void)TcpIp_Close(Scc_SDPSocket, FALSE);
  Scc_SDPSocket = (Scc_SocketType)0xFFu;

  return retVal;
} /* PRQA S 6050 */ /* MD_MSR_STCAL */
#endif /* ( SCC_ENABLE_TLS == STD_ON ) */

#if ( SCC_SCHEMA_ISO_ED2 != 0 )
/**********************************************************************************************************************
 *  Scc_Conv_Scc_2_ISOEd2_PhysicalValue
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Conv_Scc_2_ISOEd2_PhysicalValue(
  P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) scc_physicalValue,
  P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) iso_ed2_physicalValue)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Convert Scc_PhysicalValueType to Exi_ISO_ED2_DIS_PhysicalValueType */
  iso_ed2_physicalValue->Value = scc_physicalValue->Value;
  iso_ed2_physicalValue->Exponent = scc_physicalValue->Exponent;

  /* Reset scc_physicalValue */
  scc_physicalValue->Value = 0;
  scc_physicalValue->Exponent = 0;
}

/**********************************************************************************************************************
 *  Scc_Conv_ISOEd2_2_Scc_PhysicalValue
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Conv_ISOEd2_2_Scc_PhysicalValue(
  P2CONST(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) iso_ed2_physicalValue,
  P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) scc_physicalValue)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Convert Exi_ISO_ED2_DIS_PhysicalValueType to Scc_PhysicalValueType only if iso-Value is not a nullpointer */
  if ( iso_ed2_physicalValue != NULL_PTR )
  {
    scc_physicalValue->Value = iso_ed2_physicalValue->Value;
    scc_physicalValue->Exponent = iso_ed2_physicalValue->Exponent;
  }
  else
  {
    scc_physicalValue->Value = 0;
    scc_physicalValue->Exponent = 0;
  }
}
#endif /* SCC_SCHEMA_ISO_ED2 */

#if ( SCC_SCHEMA_ISO != 0 )
/**********************************************************************************************************************
 *  Scc_Conv_Scc2ISO_PhysicalValue
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Conv_Scc2ISO_PhysicalValue(
  P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) scc_physicalValue,
  P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) iso_physicalValue)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Convert Scc_PhysicalValueType to Exi_ISO_PhysicalValueType */
  iso_physicalValue->Value = scc_physicalValue->Value;
  iso_physicalValue->Multiplier = scc_physicalValue->Exponent;

  /* Reset scc_physicalValue */
  scc_physicalValue->Value = 0;
  scc_physicalValue->Exponent = 0;
}

/**********************************************************************************************************************
 *  Scc_Conv_ISO2Scc_PhysicalValue
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Conv_ISO2Scc_PhysicalValue(
  P2CONST(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) iso_physicalValue,
  P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) scc_physicalValue)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Convert Exi_ISO_PhysicalValueType to Scc_PhysicalValueType only if iso-Value is not a nullpointer */
  if ( iso_physicalValue != NULL_PTR )
  {
    scc_physicalValue->Value = iso_physicalValue->Value;
    scc_physicalValue->Exponent = iso_physicalValue->Multiplier;
  }
  else
  {
    scc_physicalValue->Value = 0;
    scc_physicalValue->Exponent = 0;
  }
}
#endif /* SCC_SCHEMA_ISO */

#if ( SCC_SCHEMA_DIN != 0 )
/**********************************************************************************************************************
 *  Scc_Conv_Scc2DIN_PhysicalValue
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Conv_Scc2DIN_PhysicalValue(
  P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) scc_physicalValue,
  P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) din_physicalValue)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Convert Scc_PhysicalValueType to Exi_DIN_PhysicalValueType */
  din_physicalValue->Value = scc_physicalValue->Value;
  din_physicalValue->Multiplier = scc_physicalValue->Exponent;

  /* Reset scc_physicalValue */
  scc_physicalValue->Value = 0;
  scc_physicalValue->Exponent = 0;
}

/**********************************************************************************************************************
 *  Scc_Conv_DIN2Scc_PhysicalValue
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Conv_DIN2Scc_PhysicalValue(
  P2CONST(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) din_physicalValue,
  P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) scc_physicalValue)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Convert Exi_DIN_PhysicalValueType to Scc_PhysicalValueType only if din-Value is not a nullpointer */
  if ( din_physicalValue != NULL_PTR )
  {
    scc_physicalValue->Value = din_physicalValue->Value;
    scc_physicalValue->Exponent = din_physicalValue->Multiplier;
  }
  else
  {
    scc_physicalValue->Value = 0;
    scc_physicalValue->Exponent = 0;
  }
}
#endif /* SCC_SCHEMA_DIN */


#define SCC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
/* PRQA L:NEST_STRUCTS */
/* PRQA L:RETURN_PATHS */

/**********************************************************************************************************************
 *  END OF FILE: Scc_Priv.c
 *********************************************************************************************************************/
