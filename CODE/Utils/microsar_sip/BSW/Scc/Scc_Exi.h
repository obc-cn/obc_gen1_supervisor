/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc_Exi.h
 *        \brief  Smart Charging Communication Header File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *********************************************************************************************************************/
#if !defined (SCC_EXI_H)
# define SCC_EXI_H

/**********************************************************************************************************************
 LOCAL MISRA / PCLINT JUSTIFICATION
 **********************************************************************************************************************/


/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

#include "Scc_Types.h"
#include "Scc_Cfg.h"

/**********************************************************************************************************************
 *  GLOBAL MACROS
 *********************************************************************************************************************/
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) /* PRQA S 3332 */ /* MD_Scc_3332 */
#define SCC_EXI_SAP_ISO_FDIS_PROTOCOL_NAMESPACE     "urn:iso:15118:2:2013:MsgDef"
#define SCC_EXI_SAP_ISO_FDIS_PROTOCOL_NAMESPACE_LEN 27u
#define SCC_EXI_SAP_ISO_FDIS_VERSION_NUMBER_MAJOR   2u
#define SCC_EXI_SAP_ISO_FDIS_VERSION_NUMBER_MINOR   0u
#endif /* SCC_SCHEMA_ISO */

#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) /* PRQA S 3332 */ /* MD_Scc_3332 */
#define SCC_EXI_SAP_ISO_ED2_DIS_PROTOCOL_NAMESPACE     "urn:iso:15118:2:2016:MsgDef"
#define SCC_EXI_SAP_ISO_ED2_DIS_PROTOCOL_NAMESPACE_LEN 27u
#define SCC_EXI_SAP_ISO_ED2_DIS_VERSION_NUMBER_MAJOR   3u
#define SCC_EXI_SAP_ISO_ED2_DIS_VERSION_NUMBER_MINOR   0u
#endif /* SCC_SCHEMA_ISO_ED2 */

#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) /* PRQA S 3332 */ /* MD_Scc_3332 */
#define SCC_EXI_SAP_DIN_PROTOCOL_NAMESPACE     "urn:din:70121:2012:MsgDef"
#define SCC_EXI_SAP_DIN_PROTOCOL_NAMESPACE_LEN 25u
#define SCC_EXI_SAP_DIN_VERSION_NUMBER_MAJOR   2u
#define SCC_EXI_SAP_DIN_VERSION_NUMBER_MINOR   0u
#endif /* SCC_SCHEMA_DIN */

#define Scc_Exi_StructBuf Scc_Exi_StructBufUnion.Buf8  /* Scc_Exi_StructBuf is also used for SECCDiscoveryProtocol */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
#define Scc_Exi_TempBuf   Scc_Exi_TempBufUnion.Buf8
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  GLOBAL VARIABLES
 *********************************************************************************************************************/
/* 8bit variables */
#define SCC_START_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#if ((( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))) /* PRQA S 3332 */ /* MD_Scc_3332 */
extern VAR(uint8, SCC_VAR_NOINIT) Scc_Exi_SAScheduleTupleID;
#endif /* SCC_SCHEMA_ISO, SCC_SCHEMA_ISO_ED2 */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
extern VAR(Scc_Exi_TempBufType, SCC_VAR_NOINIT)   Scc_Exi_TempBufUnion; /* PRQA S 0759 */ /* MD_MSR_Union */
#endif /* SCC_ENABLE_PNC_CHARGING */

#define SCC_STOP_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* 16bit variables */
#define SCC_START_SEC_VAR_NOINIT_16BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
extern VAR(uint16, SCC_VAR_NOINIT) Scc_Exi_TempBufPos;
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
extern VAR(uint16, SCC_VAR_NOINIT) Scc_ExiRx_DIN_ServiceID;
#endif /* SCC_SCHEMA_DIN */

#if (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )) /* PRQA S 3332 */ /* MD_Scc_3332 */
extern VAR(Scc_StateM_SupportedServiceIDType, SCC_VAR_NOINIT)      Scc_ExiTx_ISO_Ed2_DIS_SelectedEnergyTransferServiceID;
#endif

#define SCC_STOP_SEC_VAR_NOINIT_16BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* other variables */
#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* Scc_Exi_StructBufUnion can be an uint8 or uint32 value */
extern VAR(Scc_Exi_StructBufType, SCC_VAR_NOINIT) Scc_Exi_StructBufUnion; /* PRQA S 0759 */ /* MD_MSR_Union */

extern VAR(Exi_EncodeWorkspaceType, SCC_VAR_NOINIT) Scc_Exi_EncWs;
extern VAR(Exi_DecodeWorkspaceType, SCC_VAR_NOINIT) Scc_Exi_DecWs;
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
extern VAR(XmlSecurity_SigGenWorkspaceType, SCC_VAR_NOINIT) Scc_Exi_XmlSecSigGenWs;
extern VAR(XmlSecurity_SigValWorkspaceType, SCC_VAR_NOINIT) Scc_Exi_XmlSecSigValWs;
extern VAR(Scc_RefInSigInfoType, SCC_VAR_ZERO_INIT) Scc_Exi_RefInSigInfo; /* PRQA S 0759 */ /* MD_MSR_Union */
#endif /* SCC_ENABLE_PNC_CHARGING */


#if ((( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))) /* PRQA S 3332 */ /* MD_Scc_3332 */
extern VAR(Scc_ISO_PaymentOptionType, SCC_VAR_NOINIT)      Scc_Exi_PaymentOption;
extern VAR(Scc_ISO_ChargingSessionType, SCC_VAR_NOINIT)    Scc_Exi_ChargingSession;
#endif /* SCC_SCHEMA_ISO, SCC_SCHEMA_ISO_ED2 */

#if ( SCC_SCHEMA_ISO_ED2 != 0 )
/* Global switch between scheduled and dynamic mode */
extern VAR(Scc_ISO_Ed2_ControlModeType, SCC_VAR_NOINIT)    Scc_Exi_ControlMode;
#endif /* SCC_SCHEMA_ISO_ED2 */



#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Scc_Exi_Init
 *********************************************************************************************************************/
/*! \brief          Initializes global parameters.
 *  \details        Reset the position of the Scc_Exi_TempBuf.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \note           Is called in the Scc_Init()
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_Exi_Init(void);

/**********************************************************************************************************************
 *  Scc_Exi_StreamRequest
 *********************************************************************************************************************/
/*! \brief          Exi encode the request and copy the data into the PBuf
 *  \details        -
 *  \param[in]      BufPtr                pointer to the output buffer
 *  \param[in]      BufLengthPtr          Length of the output buffer
 *  \param[in]      PBufPtr               pointer to tx data
 *  \return         E_OK                  Exi encode was successful
 *  \return         E_NOT_OK              Exi encode was not successful
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_Exi_StreamRequest(
    P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr );

#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Init
 *********************************************************************************************************************/
/*! \brief          initializes global parameters
 *  \details        -
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_ExiTx_ISO_Init(void);

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_EncodeMessage
 *********************************************************************************************************************/
/*! \brief          Call back function from TcpIp to copy data into TcpIp buffer for ISO
 *  \details        This function is called from the TcpIp, encode the V2G message in the PBuf.
 *  \param[in]      BufPtr                pointer to the output buffer
 *  \param[in]      BufLengthPtr          Length of the output buffer
 *  \param[in]      PBufPtr               pointer to tx data
 *  \return         E_OK                  Exi encode was successful
 *  \return         E_NOT_OK              Exi encode was not successful or timeout happend
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiTx_ISO_EncodeMessage(
    P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr
);

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Init
 *********************************************************************************************************************/
/*! \brief          initializes global parameters for ISO Ed1
 *  \details        -
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_ExiRx_ISO_Init(void);

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_InitXmlSecurityWorkspace
 *********************************************************************************************************************/
/*! \brief          initializes XmlSecurity workspace for ISO Ed1.
 *  \details        -
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_ExiRx_ISO_InitXmlSecurityWorkspace(void);

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_DecodeMessage
 *********************************************************************************************************************/
/*! \brief          Decodes EXI streams
 *  \details        -
 *  \return         E_OK                  Exi decode was successful
 *  \return         E_NOT_OK              Exi decode was not successful
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiRx_ISO_DecodeMessage(void);
#endif /* SCC_SCHEMA_ISO */



#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_Init
 *********************************************************************************************************************/
/*! \brief          initializes global parameters
 *  \details        -
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_ExiTx_ISO_Ed2_Init(void);

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_EncodeMessage
 *********************************************************************************************************************/
/*! \brief          Call back function from TcpIp to copy data into TcpIp buffer for ISO Ed2
 *  \details        This function is called from the TcpIp, encode the V2G message in the PBuf.
 *  \param[in]      BufPtr                pointer to the output buffer
 *  \param[in]      BufLengthPtr          Length of the output buffer
 *  \param[in]      PBufPtr               pointer to tx data
 *  \return         E_OK                  Exi encode was successful
 *  \return         E_NOT_OK              Exi encode was not successful or timeout happend
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiTx_ISO_Ed2_EncodeMessage(
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
  P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr,
  P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr);

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_Init
 *********************************************************************************************************************/
/*! \brief          initializes global parameters for ISO Ed2
 *  \details        -
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_ExiRx_ISO_Ed2_Init(void);

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_InitXmlSecurityWorkspace
 *********************************************************************************************************************/
/*! \brief          initializes XmlSecurity workspace for ISO Ed2.
 *  \details        -
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_ExiRx_ISO_Ed2_InitXmlSecurityWorkspace(void);

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DecodeMessage
 *********************************************************************************************************************/
/*! \brief          Decodes EXI streams
 *  \details        -
 *  \return         E_OK                  Exi decode was successful
 *  \return         E_NOT_OK              Exi decode was not successful
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiRx_ISO_Ed2_DecodeMessage(void);
#endif /* SCC_SCHEMA_ISO_ED2 */



#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_Init
 *********************************************************************************************************************/
/*! \brief          initializes global parameters
 *  \details        -
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_ExiTx_DIN_Init(void);

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_EncodeMessage
 *********************************************************************************************************************/
/*! \brief          Call back function from TcpIp to copy data into TcpIp buffer for DIN
 *  \details        This function is called from the TcpIp, encode the V2G message in the PBuf.
 *  \param[in]      BufPtr                pointer to the output buffer
 *  \param[in]      BufLengthPtr          Length of the output buffer
 *  \param[in]      PBufPtr               pointer to tx data
 *  \return         E_OK                  Exi encode was successful
 *  \return         E_NOT_OK              Exi encode was not successful or timeout happend
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiTx_DIN_EncodeMessage(
    P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr
);

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_Init
 *********************************************************************************************************************/
/*! \brief          initializes global parameters for DIN
 *  \details        -
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_ExiRx_DIN_Init(void);

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_DecodeMessage
 *********************************************************************************************************************/
/*! \brief          Decodes EXI streams
 *  \details        -
 *  \return         E_OK                  Exi decode was successful
 *  \return         E_NOT_OK              Exi decode was not successful
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiRx_DIN_DecodeMessage(void);
#endif /* SCC_SCHEMA_DIN */



/**********************************************************************************************************************
 *  Scc_Exi_InitEncodingWorkspace
 *********************************************************************************************************************/
/*! \brief          initialize encoding workspace (Common - DIN, ISO Ed1 and ISO Ed2)
 *  \details        add data to the post buffer and transmit the encoded data
 *  \param[in]      BufPtr                pointer to the output buffer
 *  \param[in]      BufLengthPtr          Length of the output buffer
 *  \param[in]      PBufPtr               pointer to tx data
 *  \return         E_OK                  Exi encode was successful
 *  \return         E_NOT_OK              Exi encode was not successful
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_Exi_InitEncodingWorkspace(P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr);

/**********************************************************************************************************************
 *  Scc_Exi_InitDecodingWorkspace
 *********************************************************************************************************************/
/*! \brief          initialize dencoding workspace (Common - DIN, ISO Ed1 and ISO Ed2)
 *  \details        add data to the post buffer and transmit the encoded data
 *  \return         E_OK                  Exi initialize dencoding workspace was successful
 *  \return         E_NOT_OK              Exi initialize dencoding workspace was not successful
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_Exi_InitDecodingWorkspace(void);

/**********************************************************************************************************************
 *  Scc_Exi_EncodeExiStream
 *********************************************************************************************************************/
/*! \brief          Encode the message into Exi
 *  \details        -
 *  \param[in]      PayloadType           payload type to be sent
 *  \param[in]      PBufPtr               pointer to tx data
 *  \return         E_OK                  Exi encode was successful
 *  \return         E_NOT_OK              Exi encode was not successful
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_Exi_EncodeExiStream(uint16 PayloadType,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr);

/**********************************************************************************************************************
 *  Scc_Exi_EncodeSupportedAppProtocolReq
 *********************************************************************************************************************/
/*! \brief          Call back function from TcpIp to copy data into TcpIp buffer for supported app protocol request
 *  \details        -
 *  \param[in]      BufPtr                pointer to the output buffer
 *  \param[in]      BufLengthPtr          Length of the output buffer
 *  \param[in]      PBufPtr               pointer to tx data
 *  \return         E_OK                  Exi encode was successful
 *  \return         E_NOT_OK              Exi encode was not successful
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_Exi_EncodeSupportedAppProtocolReq(
    P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr
);

/**********************************************************************************************************************
 *  Scc_Exi_DecodeSupportedAppProtocolRes
 *********************************************************************************************************************/
/*! \brief          Decode SupportedAppProtocol Response and set the Schema (DIN, ISO Ed1 or ISO Ed2)
 *  \details        -
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_Exi_DecodeSupportedAppProtocolRes(void);

/**********************************************************************************************************************
 *  Scc_Exi_CopyPendingHeader
 *********************************************************************************************************************/
/*! \brief          Copy the header that is not yet sent
 *  \details        -
 *  \param[in]      PayloadType           payload type to be sent
 *  \param[in]      PBufPtr               pointer to tx data
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_Exi_CopyPendingHeader(uint16 PayloadType,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr);

/**********************************************************************************************************************
 *  Scc_Exi_WriteHeader
 *********************************************************************************************************************/
/*! \brief          Write the header that is not yet sent
 *  \details        -
 *  \param[in]      PayloadType           payload type to be sent
 *  \param[in]      PBufPtr               pointer to tx data
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_Exi_WriteHeader(uint16 PayloadType,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr);

/**********************************************************************************************************************
 *  Scc_Exi_SendSubsequentMessage
 *********************************************************************************************************************/
/*! \brief          Call back function from TcpIp to copy data into TcpIp buffer
 *  \details        -
 *  \param[in]      BufPtr                pointer to the output buffer
 *  \param[in]      BufLengthPtr          Length of the output buffer
 *  \param[in]      PayloadType           V2G Payload type from the V2GTP header
 *  \param[in]      PBufPtr               pointer to tx data
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_Exi_SendSubsequentMessage(
    P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr,
    uint16 PayloadType,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr);

#define SCC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* SCC_EXI_H */
/**********************************************************************************************************************
 *  END OF FILE: Scc_Exi.h
 *********************************************************************************************************************/
