/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc_ExiTx_ISO_Ed2.c
 *        \brief  Smart Charging Communication Source Code File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *********************************************************************************************************************/

#define SCC_EXITX_ISO_ED2_SOURCE

/**********************************************************************************************************************
   LOCAL MISRA / PCLINT JUSTIFICATION
**********************************************************************************************************************/
/* PRQA S 0777 EOF */ /* MD_MSR_Rule5.1 */
/* PRQA S 0779 EOF */ /* MD_MSR_Rule5.2 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Scc_Cfg.h"

#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) /* PRQA S 3332 */ /* MD_Scc_3332 */

#include "Scc_Exi.h"
#include "Scc.h"
#include "Scc_Lcfg.h"
#include "Scc_Priv.h"
#include "Scc_Interface_Cfg.h"
#include "Scc_ConfigParams_Cfg.h"

#if ( SCC_DEV_ERROR_DETECT == STD_ON )
#include "Det.h"
#endif /* SCC_DEV_ERROR_DETECT */
#include "EthIf.h"
#include "Exi.h"
#include "IpBase.h"
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
#include "XmlSecurity.h"
#endif /* SCC_ENABLE_PNC_CHARGING */
# include "Csm.h"

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined (SCC_LOCAL)
# define SCC_LOCAL static
#endif

/**********************************************************************************************************************
 *  LOCAL / GLOBAL DATA
 *********************************************************************************************************************/
/* 16bit variables */
#define SCC_START_SEC_VAR_NOINIT_16BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#if (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
VAR(Scc_StateM_SupportedServiceIDType, SCC_VAR_NOINIT)      Scc_ExiTx_ISO_Ed2_DIS_SelectedEnergyTransferServiceID;
#endif

#define SCC_STOP_SEC_VAR_NOINIT_16BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/* other variables */
#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

SCC_LOCAL P2VAR(Exi_ISO_ED2_DIS_V2G_MessageType, AUTOMATIC, SCC_VAR_NOINIT) Scc_ExiTx_ISO_Ed2_DIS_MsgPtr;

#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3453 MACROS_FUNCTION_LIKE */ /* MD_MSR_FctLikeMacro */

/* PRQA L:MACROS_FUNCTION_LIKE */
/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

 /**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request>
 *********************************************************************************************************************/
 /*! \brief         Set all parameters for ISO_Ed2 request message
 *  \details        Set all parameters for the body element of the request message.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_SessionSetupReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_ServiceDiscoveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_ServiceDetailReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_ServiceSelectionReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_IdentificationDetailsReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
#endif /* SCC_ENABLE_PNC_CHARGING */
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_AuthorizationReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_ChargeParameterDiscoveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_PowerDeliveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_MeteringReceiptReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_CertificateInstallationReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
#endif /* SCC_ENABLE_PNC_CHARGING */
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_SessionStopReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

/* AC */

/* DC */
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_CableCheckReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_PreChargeReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_CurrentDemandReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_ISO_Ed2_<ISO_Ed2-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_WeldingDetectionReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
#endif /* SCC_CHARGING_DC */

/* WPT */
#if ( defined SCC_CHARGING_DC_BPT ) && ( SCC_CHARGING_DC_BPT == STD_ON )
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_DC_BidirectionalControlReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
#endif /* SCC_CHARGING_DC_BPT */
/**********************************************************************************************************************
*  Scc_ExiTx_ISO_Ed2_WriteHeader
*********************************************************************************************************************/
/*! \brief          Write the ISO Ed2 header to the buffer
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
*********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_WriteHeader(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_Init
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_ExiTx_ISO_Ed2_Init(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize the message pointer */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr = (P2VAR(Exi_ISO_ED2_DIS_V2G_MessageType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_StructBuf[0]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */

}

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_EncodeMessage
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiTx_ISO_Ed2_EncodeMessage(
      P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr
)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  uint16         lBufIdx = 0;
  uint16         PayloadType = SCC_V2GTP_HDR_PAYLOAD_TYPE_EXI;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Since no data has been sent, this means that the initializations has to be done */
  if(0UL == Scc_TxDataSent)
  {
    /* initialize the exi workspace and check if it failed */
    if ( (Std_ReturnType)E_OK == Scc_Exi_InitEncodingWorkspace(PBufPtr) )
    {
      /* set the V2G_Message as root element */
      Scc_Exi_EncWs.InputData.RootElementId = EXI_ISO_ED2_DIS_V2G_MESSAGE_TYPE;
      /* initialize the header of the V2G messages */
      Scc_ExiTx_ISO_Ed2_WriteHeader(&lBufIdx);
      /* create the body */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
      Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body = (P2VAR(Exi_ISO_ED2_DIS_BodyType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[lBufIdx];
      lBufIdx += (uint16)sizeof(Exi_ISO_ED2_DIS_BodyType);
      /* set the common body parameters */
      Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement =
        (P2VAR(Exi_ISO_ED2_DIS_BodyBaseType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[lBufIdx]; /* PRQA S 0314 */ /* MD_Scc_0310_0314_0316_3305 */

      /* #20 create request message and set MessageTimeout */
      switch ( Scc_MsgTrig )
      {
        /* check if a session setup request shall be sent */
      case Scc_MsgTrig_SessionSetup:
        Scc_ExiTx_ISO_Ed2_DIS_SessionSetupReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_SessionSetupMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a service discovery request shall be sent */
      case Scc_MsgTrig_ServiceDiscovery:
        Scc_ExiTx_ISO_Ed2_DIS_ServiceDiscoveryReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_ServiceDiscoveryMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a service detail request shall be sent */
      case Scc_MsgTrig_ServiceDetail:
        Scc_ExiTx_ISO_Ed2_DIS_ServiceDetailReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_ServiceDetailMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a service payment selection request shall be sent */
      case Scc_MsgTrig_PaymentServiceSelection:
        Scc_ExiTx_ISO_Ed2_DIS_ServiceSelectionReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_PaymentServiceSelectionMessageTimeout;
        retVal = E_OK;
        break;

    #if ( SCC_ENABLE_PNC_CHARGING == STD_ON )

    #if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
        /* check if a certificate installation request shall be sent */
      case Scc_MsgTrig_CertificateInstallation:
        /* try to transmit the message and check if it was successful */
        if ( (Std_ReturnType)E_OK == Scc_ExiTx_ISO_Ed2_DIS_CertificateInstallationReq(&lBufIdx) )
        {
          PayloadType = (uint16)SCC_V2GTP_HDR_PAYLOAD_TYPE_CERTIFICATE_INSTALL;
          Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_CertificateInstallationMessageTimeout;
          retVal = E_OK;
        }
        break;
    #endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

        /* check if a Identification details request shall be sent */
      case Scc_MsgTrig_PaymentDetails:
        Scc_ExiTx_ISO_Ed2_DIS_IdentificationDetailsReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_PaymentDetailsMessageTimeout;
        retVal = E_OK;
        break;

    #endif /* SCC_ENABLE_PNC_CHARGING */

        /* check if a contract authentication request shall be sent */
      case Scc_MsgTrig_Authorization:
        /* try to transmit the message and check if it was successful */
        if ( (Std_ReturnType)E_OK == Scc_ExiTx_ISO_Ed2_DIS_AuthorizationReq(&lBufIdx) ) /* PRQA S 2991,2995 */ /* MD_Scc_VariantDependent */
        {
          Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_AuthorizationMessageTimeout;
          retVal = E_OK;
        }
        break;

        /* check if a charge parameter discovery request shall be sent */
      case Scc_MsgTrig_ChargeParameterDiscovery:
        Scc_ExiTx_ISO_Ed2_DIS_ChargeParameterDiscoveryReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_ChargeParameterDiscoveryMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a power delivery request shall be sent */
      case Scc_MsgTrig_PowerDelivery:
        Scc_ExiTx_ISO_Ed2_DIS_PowerDeliveryReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_PowerDeliveryMessageTimeout;
        retVal = E_OK;
        break;

    #if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
        /* check if a metering receipt request shall be sent */
      case Scc_MsgTrig_MeteringReceipt:
        /* try to transmit the message and check if it was successful */
        if ( (Std_ReturnType)E_OK == Scc_ExiTx_ISO_Ed2_DIS_MeteringReceiptReq(&lBufIdx) )
        {
          PayloadType = (uint16)SCC_V2GTP_HDR_PAYLOAD_TYPE_METERING_RECEIPT;
          Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_MeteringReceiptMessageTimeout;
          retVal = E_OK;
        }
        break;
    #endif /* SCC_ENABLE_PNC_CHARGING */

        /* check if a session stop request shall be sent */
      case Scc_MsgTrig_SessionStop:
        Scc_ExiTx_ISO_Ed2_DIS_SessionStopReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_SessionStopMessageTimeout;
        retVal = E_OK;
        break;


    #if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )

        /* check if a cable check request shall be sent */
      case Scc_MsgTrig_CableCheck:
        Scc_ExiTx_ISO_Ed2_DIS_CableCheckReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_CableCheckMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a pre charge request shall be sent */
      case Scc_MsgTrig_PreCharge:
        Scc_ExiTx_ISO_Ed2_DIS_PreChargeReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_PreChargeMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a current demand request shall be sent */
      case Scc_MsgTrig_CurrentDemand:
        Scc_ExiTx_ISO_Ed2_DIS_CurrentDemandReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_CurrentDemandMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a welding detection request shall be sent */
      case Scc_MsgTrig_WeldingDetection:
        Scc_ExiTx_ISO_Ed2_DIS_WeldingDetectionReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_WeldingDetectionMessageTimeout;
        retVal = E_OK;
        break;

    #endif /* SCC_CHARGING_DC */


#if ( defined SCC_CHARGING_DC_BPT ) && ( SCC_CHARGING_DC_BPT == STD_ON )
        /* check if AC_BidirectionalControl request shall be sent */
      case Scc_MsgTrig_DC_BidirectionalControl:
        Scc_ExiTx_ISO_Ed2_DIS_DC_BidirectionalControlReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_DC_BidirectionalControlTimeout;
        retVal = E_OK;
        break;
#endif /* SCC_CHARGING_DC_BPT */

      default:
        /* report the error */
        Scc_ReportError(Scc_StackError_InvalidTxParameter);
        retVal = E_NOT_OK;
        break;
      }

      /* #30 encode and send the request message */
      if ( E_OK == retVal )
      {
        retVal = E_NOT_OK;

        if(E_OK == Scc_Exi_EncodeExiStream(PayloadType, PBufPtr))
        {
          *BufLengthPtr = (uint16)Scc_TxDataSent;
          PBufPtr->totLen = *BufLengthPtr;
          PBufPtr->len    = *BufLengthPtr;

          /* provide sent data to application */
          {
            /* create and set the tx buffer pointer */
            Scc_TxRxBufferPointerType V2GRequest;
            V2GRequest.PbufPtr = PBufPtr;
            V2GRequest.FirstPart = TRUE;
            V2GRequest.StreamComplete = Scc_Exi_EncWs.EncWs.StreamComplete;
            /* provide the buffer to the application */
            Scc_Set_Core_V2GRequest(&V2GRequest);
          }

          retVal = E_OK;
        }
      }
    }
  }
  else
  {
    /* #40 send the pending request message */
    retVal = Scc_Exi_SendSubsequentMessage(BufPtr, BufLengthPtr, PayloadType, PBufPtr);
  }

  return retVal; /*lint !e438 */
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_SessionSetupReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_SessionSetupReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ED2_DIS_SessionSetupReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ED2_DIS_SessionSetupReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_SESSION_SETUP_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_SessionSetupReqType);

  /* EVCCID */ /* PRQA S 0310,3305 6 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVCCID = (P2VAR(Exi_ISO_ED2_DIS_evccIDType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_evccIDType);
  /* get the MAC address from EthIf */
  EthIf_GetPhysAddr((uint8)SCC_CTRL_IDX, &BodyPtr->EVCCID->Buffer[0]);
  BodyPtr->EVCCID->Length = 6;

  BodyPtr->SSEnergyTransferModeFlag = 0;
}

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_ServiceDiscoveryReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_ServiceDiscoveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ED2_DIS_ServiceDiscoveryReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ED2_DIS_ServiceDiscoveryReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  boolean Flag = 0; /* PRQA S 2982 */ /* MD_Scc_2982 */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_SERVICE_DISCOVERY_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_ServiceDiscoveryReqType);

  /* SupportedServiceIDs */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->SupportedServiceIDs = (P2VAR(Exi_ISO_ED2_DIS_ServiceIDListType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_ServiceIDListType);

  Scc_Get_ISO_Ed2_DIS_SupportedServiceIDs(BodyPtr->SupportedServiceIDs, &Flag);
  BodyPtr->SupportedServiceIDsFlag = (Exi_BitType)Flag;

}

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_ServiceDetailReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_ServiceDetailReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ED2_DIS_ServiceDetailReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ED2_DIS_ServiceDetailReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_SERVICE_DETAIL_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_ServiceDetailReqType);

  /* ServiceID */
  Scc_Get_ISO_Ed2_DIS_ServiceID(&BodyPtr->ServiceID);

  BodyPtr->SDlEnergyTransferModeFlag = 0;
}

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_ServiceSelectionReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_ServiceSelectionReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ED2_DIS_ServiceSelectionReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ED2_DIS_ServiceSelectionReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  boolean               Flag = 0; /* PRQA S 2982 */ /* MD_Scc_2982 */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_SERVICE_SELECTION_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_ServiceSelectionReqType);

  /* EVProcessing */
  Scc_Get_ISO_Ed2_DIS_EVProcessing(&BodyPtr->EVProcessing);

  /* SelectedIdentificationOption */
  Scc_Get_ISO_Ed2_DIS_SelectedIdentificationOption(&BodyPtr->SelectedIdentificationOption);
  /* save the IdentificationOption */
  Scc_Exi_PaymentOption = (Scc_ISO_PaymentOptionType)BodyPtr->SelectedIdentificationOption; /* [ISOVersionCast] */ /* PRQA S 4322 */ /* MD_Scc_Exi_Generic */

  /* SelectedEnergyTransferService */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->SelectedEnergyTransferService = (P2VAR(Exi_ISO_ED2_DIS_SelectedServiceType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_SelectedServiceType);

  Scc_Get_ISO_Ed2_DIS_SelectedEnergyTransferService(BodyPtr->SelectedEnergyTransferService);
  Scc_ExiTx_ISO_Ed2_DIS_SelectedEnergyTransferServiceID = (Scc_StateM_SupportedServiceIDType)BodyPtr->SelectedEnergyTransferService->ServiceID; /* PRQA S 4342 */ /* MD_Scc_Exi_Generic */

  /* Get the control mode to be transmitted */
  Scc_Get_ISO_Ed2_DIS_SelectedControlMode(&Scc_Exi_ControlMode);

  /* SelectedVASList */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->SelectedVASList = (P2VAR(Exi_ISO_ED2_DIS_SelectedServiceListType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_SelectedServiceListType);

  Scc_Get_ISO_Ed2_DIS_SelectedVASListPtr(BodyPtr->SelectedVASList, &Flag);
  BodyPtr->SelectedVASListFlag = (Exi_BitType)Flag;

}

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )

#if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_CertificateInstallationReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_CertificateInstallationReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ED2_DIS_CertificateInstallationReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ED2_DIS_CertificateInstallationReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  uint8                  IdBuffer[6] = "ID_CI";
  XmlSecurity_ReturnType XmlSecRetVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_CERTIFICATE_INSTALLATION_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_CertificateInstallationReqType);

  BodyPtr->OEMProvisioningCertificateChain = (P2VAR(Exi_ISO_ED2_DIS_CertificateChainType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_CertificateChainType);
  /* OEMProvisioningCert */
  BodyPtr->OEMProvisioningCertificateChain->Certificate = (Exi_ISO_ED2_DIS_certificateType*)Scc_CertsWs.ProvCert; /* [ISOVersionCast] */ /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->OEMProvisioningCertificateChain->SubCertificatesFlag = 0;
  BodyPtr->OEMProvisioningCertificateChain->IdFlag = 0; /* Check if Id is not used for the signature*/

  /* #20 Get the ListOfRootCertificateIDs */
  if ( (Std_ReturnType)E_OK == Scc_ExiTx_ISO_ExtractRootCertificateIDs() )
  {
    /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->ListOfRootCertificateIDs =
      (P2VAR(Exi_ISO_ED2_DIS_ListOfRootCertificateIDsType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_ListOfRootCertificateIDsType);
    BodyPtr->ListOfRootCertificateIDs->RootCertificateID = &Scc_CertsWs.RootCertIDs[0];

    /* create the attribute ID */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->Id = (P2VAR(Exi_ISO_ED2_DIS_AttributeIdType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_AttributeIdType);
    /* set the attribute ID */
    (void) IpBase_StrCpy(&BodyPtr->Id->Buffer[0], &IdBuffer[0]);
    BodyPtr->Id->Length = 5;

    /* Get the MaximumSupportedCertificateChains to be transmitted */
    Scc_Get_ISO_Ed2_DIS_MaximumSupportedCertificateChains(&BodyPtr->MaximumSupportedCertificateChains);
    BodyPtr->PrioritizedEMAIDs = (P2VAR(Exi_ISO_ED2_DIS_EMAIDListType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_EMAIDListType);
    BodyPtr->PrioritizedEMAIDs->EMAID = (P2VAR(Exi_ISO_ED2_DIS_emaIDType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305,0316 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_emaIDType);
    /* PRQA S 0315 2 */ /* MD_MSR_VStdLibCopy */
    IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->PrioritizedEMAIDs->EMAID->Buffer[0],
      (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.eMAID->Buffer[0],
      Scc_CertsWs.eMAID->Length);
    BodyPtr->PrioritizedEMAIDs->EMAID->Length = Scc_CertsWs.eMAID->Length;
    BodyPtr->PrioritizedEMAIDs->EMAID->NextEMAIDPtr = NULL_PTR;
    /* #30 initialize XmlSecurity workspace */
    XmlSecRetVal = XmlSecurity_InitSigGenWorkspace(&Scc_Exi_XmlSecSigGenWs,
      &Scc_Exi_StructBuf[*BufIdxPtr], (uint16)(SCC_EXI_STRUCT_BUF_LEN-*BufIdxPtr),
      XmlSecurity_SAT_ECDSA_SHA256, XmlSecurity_CAT_EXI);
    if ( XmlSec_RetVal_OK != XmlSecRetVal )
    {
      /* report error to application */
      Scc_ReportError(Scc_StackError_XmlSecurity);
    }
    else
    {
      /* set the pointer for the signature */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
      Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Header->Signature = (P2VAR(Exi_XMLSIG_SignatureType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
      Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Header->SignatureFlag = 1;

      /* #40 Add EXI references */
      /* PRQA S 0316 5 */ /* MD_Scc_0310_0314_0316_3305 */
      XmlSecRetVal = XmlSecurity_AddExiReference(&Scc_Exi_XmlSecSigGenWs,
        &Scc_Exi_TempBuf[0],
        (P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement,
        (P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->Id->Buffer[0],
        Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId,
        SCC_EXI_TEMP_BUF_LEN, BodyPtr->Id->Length,
        (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_ED2_DIS_TYPE,
        Scc_SHA256JobId);

      if ( XmlSec_RetVal_OK != XmlSecRetVal )
      {
        /* report error to application */
        Scc_ReportError(Scc_StackError_XmlSecurity);
      }
      else
      {
        /* #50 generate the signature */
        XmlSecRetVal = XmlSecurity_GenerateSignature(&Scc_Exi_XmlSecSigGenWs,
          &Scc_Exi_TempBuf[0],
          SCC_EXI_TEMP_BUF_LEN,
          Scc_ECDSASignProvCertKeyId,
          Scc_ECDSASignProvCertJobId);

        if ( XmlSec_RetVal_OK != XmlSecRetVal )
        {
          /* report error to application */
          Scc_ReportError(Scc_StackError_XmlSecurity);
        }
        else
        {
          retVal = E_OK;
        }
      }
    }
  }

  return retVal;
} /* PRQA S 6050 */ /* MD_MSR_STCAL */
#endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_IdentificationDetailsReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_IdentificationDetailsReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ED2_DIS_IdentificationDetailsReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ED2_DIS_IdentificationDetailsReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_IDENTIFICATION_DETAILS_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_IdentificationDetailsReqType);

  /* ContractSignatureCertChain -> Certificate */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->ContractSignatureCertChain =
    (P2VAR(Exi_ISO_ED2_DIS_CertificateChainType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_CertificateChainType);
  BodyPtr->ContractSignatureCertChain->Certificate =
    (Exi_ISO_ED2_DIS_certificateType*)Scc_CertsWs.ContrCert; /* [ISOVersionCast] */ /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->ContractSignatureCertChain->IdFlag = 0;

  /* ContractSignatureCertChain -> SubCertificates */
  if ( 0 < Scc_CertsWs.ContrCertChainSize )
  {
    /* create the SubCertificates element */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->ContractSignatureCertChain->SubCertificates =
      (P2VAR(Exi_ISO_ED2_DIS_SubCertificatesType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_SubCertificatesType);
    /* set the certificates pointer */
    BodyPtr->ContractSignatureCertChain->SubCertificates->Certificate =
      (Exi_ISO_ED2_DIS_certificateType*)&Scc_CertsWs.ContrSubCerts[0]; /* [ISOVersionCast] */ /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */

    /* set the sub certificate flag to true */
    BodyPtr->ContractSignatureCertChain->SubCertificatesFlag = 1;
  }
  else
  {
    BodyPtr->ContractSignatureCertChain->SubCertificatesFlag = 0;
  }

}

#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_AuthorizationReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_AuthorizationReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ED2_DIS_AuthorizationReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ED2_DIS_AuthorizationReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  Exi_ISO_ED2_DIS_PnC_AReqIdentificationModeType *reqAReqIdentificationModePtr;
  XmlSecurity_ReturnType XmlSecRetVal;
  uint8                  IdBuffer[6] = "ID_CA";
#endif /* SCC_ENABLE_PNC_CHARGING */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_AUTHORIZATION_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_AuthorizationReqType);

  /* GenChallenge */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  if (   ( SCC_ISO_PAYMENT_OPTION_TYPE_CONTRACT == Scc_Exi_PaymentOption )
      && ( Scc_MsgStatus_Authorization_OK != Scc_MsgStatus ))
  {
    BodyPtr->AReqIdentificationMode = (P2VAR(Exi_ISO_ED2_DIS_AReqIdentificationModeType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0314 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PnC_AReqIdentificationModeType);
    reqAReqIdentificationModePtr = (Exi_ISO_ED2_DIS_PnC_AReqIdentificationModeType *)BodyPtr->AReqIdentificationMode; /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->AReqIdentificationModeElementId = EXI_ISO_ED2_DIS_PN_C_AREQ_IDENTIFICATION_MODE_TYPE;
    BodyPtr->AReqIdentificationModeFlag = TRUE;

    /* set the GenChallenge */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
    reqAReqIdentificationModePtr->GenChallenge = (P2VAR(Exi_ISO_ED2_DIS_genChallengeType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[0];

    /* create the attribute ID */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    reqAReqIdentificationModePtr->Id = (P2VAR(Exi_ISO_ED2_DIS_AttributeIdType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_AttributeIdType);
    /* set the attribute ID */
    (void) IpBase_StrCpy(&reqAReqIdentificationModePtr->Id->Buffer[0], &IdBuffer[0]);
    reqAReqIdentificationModePtr->Id->Length = 5;

    /* #20 Initialize XmlSecurity workspace */
    XmlSecRetVal = XmlSecurity_InitSigGenWorkspace(&Scc_Exi_XmlSecSigGenWs,
      &Scc_Exi_StructBuf[*BufIdxPtr], (uint16)(SCC_EXI_STRUCT_BUF_LEN-*BufIdxPtr),
      XmlSecurity_SAT_ECDSA_SHA256, XmlSecurity_CAT_EXI);
    if ( XmlSec_RetVal_OK != XmlSecRetVal )
    {
      /* report the error to the application */
      Scc_ReportError(Scc_StackError_XmlSecurity);
    }
    else
    {
      /* set the pointer for the signature */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
      Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Header->Signature =
        (P2VAR(Exi_XMLSIG_SignatureType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Header->SignatureFlag = 1;

      /* #30 Add EXI references */
      XmlSecRetVal = XmlSecurity_AddExiReference(&Scc_Exi_XmlSecSigGenWs, &Scc_Exi_TempBuf[Scc_Exi_TempBufPos],
        (P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA))reqAReqIdentificationModePtr, /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
        (P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA))&reqAReqIdentificationModePtr->Id->Buffer[0],
        EXI_ISO_ED2_DIS_PN_C_AREQ_IDENTIFICATION_MODE_TYPE, (uint16)(SCC_EXI_TEMP_BUF_LEN-Scc_Exi_TempBufPos),
        reqAReqIdentificationModePtr->Id->Length, (uint8)EXI_SCHEMA_SET_ISO_ED2_DIS_TYPE, Scc_SHA256JobId);

      if ( XmlSec_RetVal_OK != XmlSecRetVal )
      {
        /* report the error to the application */
        Scc_ReportError(Scc_StackError_XmlSecurity);
      }
      else
      {
        /* #40 generate the signature */
        XmlSecRetVal = XmlSecurity_GenerateSignature(&Scc_Exi_XmlSecSigGenWs,
          &Scc_Exi_TempBuf[Scc_Exi_TempBufPos],
          (uint16)(SCC_EXI_TEMP_BUF_LEN-Scc_Exi_TempBufPos),
          Scc_ECDSASignCertKeyIds[Scc_CertsWs.ChosenContrCertChainIdx], Scc_ECDSASignCertJobIds[Scc_CertsWs.ChosenContrCertChainIdx]);

        if ( XmlSec_RetVal_OK != XmlSecRetVal )
        {
          /* report the error to the application */
          Scc_ReportError(Scc_StackError_XmlSecurity);
        }
        else
        {
          retVal = E_OK;
        }
      }
    }
  }
  else
#endif /* SCC_ENABLE_PNC_CHARGING */
  {
    BodyPtr->AReqIdentificationModeFlag = FALSE;

    retVal = E_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_ChargeParameterDiscoveryReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_ChargeParameterDiscoveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ED2_DIS_ChargeParameterDiscoveryReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ED2_DIS_ChargeParameterDiscoveryReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
  P2VAR(Exi_ISO_ED2_DIS_DC_CPDReqEnergyTransferModeType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVChargeParameterPtr;
#endif /* SCC_CHARGING_DC */
#if ( defined SCC_CHARGING_DC_BPT ) && ( SCC_CHARGING_DC_BPT == STD_ON )
  P2VAR(Exi_ISO_ED2_DIS_BPT_DC_CPDReqEnergyTransferModeType, AUTOMATIC, SCC_VAR_NOINIT) BPT_DC_EVChargeParameterPtr;
#endif /* SCC_CHARGING_DC_BPT */

  boolean               Flag = 0; /* PRQA S 2982 */ /* MD_Scc_2982 */
  Scc_PhysicalValueType scc_PhysicalValueTmp;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_CHARGE_PARAMETER_DISCOVERY_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_ChargeParameterDiscoveryReqType);

  if (Scc_Exi_ControlMode == SCC_ISO_ED2_CONTROL_MODE_TYPE_SCHEDULED)
  {
    Exi_ISO_ED2_DIS_Scheduled_CPDReqControlModeType *reqScheduled_CPDReqControlModePtr;

    BodyPtr->CPDReqControlMode = (P2VAR(Exi_ISO_ED2_DIS_CPDReqControlModeType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305,0314 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_Scheduled_CPDReqControlModeType);
    BodyPtr->CPDReqControlModeElementId = EXI_ISO_ED2_DIS_SCHEDULED_CPDREQ_CONTROL_MODE_TYPE;

    reqScheduled_CPDReqControlModePtr = (Exi_ISO_ED2_DIS_Scheduled_CPDReqControlModeType *)BodyPtr->CPDReqControlMode; /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */
    /* EVProcessing */
    Scc_Get_ISO_Ed2_DIS_EVProcessing(&reqScheduled_CPDReqControlModePtr->EVProcessing);
    /* DepartureTime */
    Scc_Get_ISO_Ed2_DIS_DepartureTime(&reqScheduled_CPDReqControlModePtr->DepartureTime, &Flag);
    reqScheduled_CPDReqControlModePtr->DepartureTimeFlag = Flag;
    /* MaxEntriesScheduleTuple */
    Scc_Get_ISO_Ed2_DIS_MaxSupportingPoints(&reqScheduled_CPDReqControlModePtr->MaxSupportingPoints, &Flag);

    reqScheduled_CPDReqControlModePtr->ScheduleList = (P2VAR(Exi_ISO_ED2_DIS_ScheduleListType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_ScheduleListType);
    /* ScheduleList */
    Scc_Get_ISO_Ed2_DIS_ScheduleList(reqScheduled_CPDReqControlModePtr->ScheduleList, &Flag);
    reqScheduled_CPDReqControlModePtr->ScheduleListFlag = Flag;
  }
  else
  {/* Scc_Exi_ControlMode == SCC_ISO_ED2_CONTROL_MODE_TYPE_DYNAMIC */
    BodyPtr->CPDReqControlMode = (P2VAR(Exi_ISO_ED2_DIS_CPDReqControlModeType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305,0314 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_Dynamic_CPDReqControlModeType);
    BodyPtr->CPDReqControlModeElementId = EXI_ISO_ED2_DIS_DYNAMIC_CPDREQ_CONTROL_MODE_TYPE;

    /* DepartureTime */
    Scc_Get_ISO_Ed2_DIS_DepartureTime(&((Exi_ISO_ED2_DIS_Dynamic_CPDReqControlModeType *)BodyPtr->CPDReqControlMode)->DepartureTime, &Flag); /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */
  }

  /* RequestedEnergyTransferMode */
  switch(Scc_ExiTx_ISO_Ed2_DIS_SelectedEnergyTransferServiceID)
  {
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
    case Scc_SID_DC_Charging:
    {
      BodyPtr->CPDReqEnergyTransferMode = (P2VAR(Exi_ISO_ED2_DIS_CPDReqEnergyTransferModeType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_DC_CPDReqEnergyTransferModeType);
      BodyPtr->CPDReqEnergyTransferModeElementId = EXI_ISO_ED2_DIS_DC_CPDREQ_ENERGY_TRANSFER_MODE_TYPE;

      DC_EVChargeParameterPtr = (P2VAR(Exi_ISO_ED2_DIS_DC_CPDReqEnergyTransferModeType, AUTOMATIC, SCC_APPL_DATA))(BodyPtr->CPDReqEnergyTransferMode); /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */

      /* DC_EVChargeParameter -> EVTargetEnergyRequest */
      DC_EVChargeParameterPtr->EVTargetEnergyRequest = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVTargetEnergyRequest(&scc_PhysicalValueTmp, &Flag);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterPtr->EVTargetEnergyRequest);
      DC_EVChargeParameterPtr->EVTargetEnergyRequestFlag = (Exi_BitType)Flag;

      /* DC_EVChargeParameter -> EVMaximumEnergyRequest */
      DC_EVChargeParameterPtr->EVMaximumEnergyRequest = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMaximumEnergyRequest(&scc_PhysicalValueTmp, &Flag);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterPtr->EVMaximumEnergyRequest);
      DC_EVChargeParameterPtr->EVMaximumEnergyRequestFlag = (Exi_BitType)Flag;

      /* DC_EVChargeParameter -> EVMinimumEenrgyRequest */
      DC_EVChargeParameterPtr->EVMinimumEnergyRequest = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMinimumEnergyRequest(&scc_PhysicalValueTmp, &Flag);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterPtr->EVMinimumEnergyRequest);
      DC_EVChargeParameterPtr->EVMinimumEnergyRequestFlag = (Exi_BitType)Flag;

      /* DC_EVChargeParameter -> EVMaximumChargePower */
      DC_EVChargeParameterPtr->EVMaximumChargePower = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMaximumChargePower(&scc_PhysicalValueTmp, &Flag);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterPtr->EVMaximumChargePower);
      DC_EVChargeParameterPtr->EVMaximumChargePowerFlag = (Exi_BitType)Flag;

      /* DC_EVChargeParameter -> EVMinimumChargePower */
      DC_EVChargeParameterPtr->EVMinimumChargePower = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMinimumChargePower(&scc_PhysicalValueTmp, &Flag);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterPtr->EVMinimumChargePower);
      DC_EVChargeParameterPtr->EVMinimumChargePowerFlag = (Exi_BitType)Flag;

      /* DC_EVChargeParameter -> EVMaximumChargeCurrent */
      DC_EVChargeParameterPtr->EVMaximumChargeCurrent = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMaximumChargeCurrent(&scc_PhysicalValueTmp, &Flag); /* Flag not used. */
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterPtr->EVMaximumChargeCurrent);

      /* DC_EVChargeParameter -> EVMinimumChargeCurrent */
      DC_EVChargeParameterPtr->EVMinimumChargeCurrent = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMinimumChargeCurrent(&scc_PhysicalValueTmp);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterPtr->EVMinimumChargeCurrent);

      /* DC_EVChargeParameter -> EVMaximumVoltage */
      DC_EVChargeParameterPtr->EVMaximumVoltage = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMaximumVoltage(&scc_PhysicalValueTmp, &Flag); /* Flag not used */
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterPtr->EVMaximumVoltage);

      /* DC_EVChargeParameter -> TargetSOC */
      Scc_Get_ISO_Ed2_DIS_DC_TargetSOC(&DC_EVChargeParameterPtr->TargetSOC, &Flag);
      DC_EVChargeParameterPtr->TargetSOCFlag = (Exi_BitType)Flag;

      /* DC_EVChargeParameter -> BulkSOC */
      Scc_Get_ISO_Ed2_DIS_DC_BulkSOC(&DC_EVChargeParameterPtr->BulkSOC, &Flag);
      DC_EVChargeParameterPtr->BulkSOCFlag = (Exi_BitType)Flag;

      break;
    }
#endif /* SCC_CHARGING_DC */
#if ( defined SCC_CHARGING_DC_BPT ) && ( SCC_CHARGING_DC_BPT == STD_ON )
    case Scc_SID_DC_BPT:
    {
      BodyPtr->CPDReqEnergyTransferMode = (P2VAR(Exi_ISO_ED2_DIS_CPDReqEnergyTransferModeType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_BPT_DC_CPDReqEnergyTransferModeType);
      BodyPtr->CPDReqEnergyTransferModeElementId = EXI_ISO_ED2_DIS_BPT_DC_CPDREQ_ENERGY_TRANSFER_MODE_TYPE;

      BPT_DC_EVChargeParameterPtr = (P2VAR(Exi_ISO_ED2_DIS_BPT_DC_CPDReqEnergyTransferModeType, AUTOMATIC, SCC_APPL_DATA))(BodyPtr->CPDReqEnergyTransferMode); /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */

      /* BPT_DC_EVChargeParameter -> EVTargetEnergyRequest */
      BPT_DC_EVChargeParameterPtr->EVTargetEnergyRequest = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVTargetEnergyRequest(&scc_PhysicalValueTmp, &Flag);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BPT_DC_EVChargeParameterPtr->EVTargetEnergyRequest);
      BPT_DC_EVChargeParameterPtr->EVTargetEnergyRequestFlag = (Exi_BitType)Flag;

      /* BPT_DC_EVChargeParameter -> EVMaximumEnergyRequest */
      BPT_DC_EVChargeParameterPtr->EVMaximumEnergyRequest = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMaximumEnergyRequest(&scc_PhysicalValueTmp, &Flag);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BPT_DC_EVChargeParameterPtr->EVMaximumEnergyRequest);
      BPT_DC_EVChargeParameterPtr->EVMaximumEnergyRequestFlag = (Exi_BitType)Flag;

      /* BPT_DC_EVChargeParameterPtr -> EVMinimumEnergyRequest */
      BPT_DC_EVChargeParameterPtr->EVMinimumEnergyRequest = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMinimumEnergyRequest(&scc_PhysicalValueTmp, &Flag);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BPT_DC_EVChargeParameterPtr->EVMinimumEnergyRequest);
      BPT_DC_EVChargeParameterPtr->EVMinimumEnergyRequestFlag = (Exi_BitType)Flag;

      /* BPT_DC_EVChargeParameter -> EVMaximumChargePower */
      BPT_DC_EVChargeParameterPtr->EVMaximumChargePower = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMaximumChargePower(&scc_PhysicalValueTmp, &Flag); /* Flag not used. */
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BPT_DC_EVChargeParameterPtr->EVMaximumChargePower);
      BPT_DC_EVChargeParameterPtr->EVMaximumChargePowerFlag = (Exi_BitType)Flag;

      /* BPT_DC_EVChargeParameter -> EVMinimumChargePower */
      BPT_DC_EVChargeParameterPtr->EVMinimumChargePower = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMinimumChargePower(&scc_PhysicalValueTmp, &Flag); /* Flag not used. */
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BPT_DC_EVChargeParameterPtr->EVMinimumChargePower);
      BPT_DC_EVChargeParameterPtr->EVMinimumChargePowerFlag = (Exi_BitType)Flag;

      /* BPT_DC_EVChargeParameter -> EVMaximumChargeCurrent */
      BPT_DC_EVChargeParameterPtr->EVMaximumChargeCurrent = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMaximumChargeCurrent(&scc_PhysicalValueTmp, &Flag); /* Flag not used. */
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BPT_DC_EVChargeParameterPtr->EVMaximumChargeCurrent);

      /* BPT_DC_EVChargeParameter -> EVMinimumChargeCurrent */
      BPT_DC_EVChargeParameterPtr->EVMinimumChargeCurrent = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMinimumChargeCurrent(&scc_PhysicalValueTmp);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BPT_DC_EVChargeParameterPtr->EVMinimumChargeCurrent);

      /* BPT_DC_EVChargeParameter -> EVMaximumVoltage */
      BPT_DC_EVChargeParameterPtr->EVMaximumVoltage = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMaximumVoltage(&scc_PhysicalValueTmp, &Flag);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BPT_DC_EVChargeParameterPtr->EVMaximumVoltage);

      /* BPT_DC_EVChargeParameterPtr -> TargetSOC */
      Scc_Get_ISO_Ed2_DIS_DC_TargetSOC(&BPT_DC_EVChargeParameterPtr->TargetSOC, &Flag);
      BPT_DC_EVChargeParameterPtr->TargetSOCFlag = (Exi_BitType)Flag;

      /* BPT_DC_EVChargeParameterPtr -> BulkSOC */
      Scc_Get_ISO_Ed2_DIS_DC_BulkSOC(&BPT_DC_EVChargeParameterPtr->BulkSOC, &Flag);
      BPT_DC_EVChargeParameterPtr->BulkSOCFlag = (Exi_BitType)Flag;

      /* BPT_DC_EVChargeParameter -> EVMaximumDischargePower */
      BPT_DC_EVChargeParameterPtr->EVMaximumDischargePower = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_BPT_DC_EVMaximumDischargePower(&scc_PhysicalValueTmp, &Flag);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BPT_DC_EVChargeParameterPtr->EVMaximumDischargePower);
      BPT_DC_EVChargeParameterPtr->EVMaximumDischargePowerFlag = (Exi_BitType)Flag;

      /* BPT_DC_EVChargeParameter -> EVMinimumDischargePower */
      BPT_DC_EVChargeParameterPtr->EVMinimumDischargePower = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_BPT_DC_EVMinimumDischargePower(&scc_PhysicalValueTmp, &Flag);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BPT_DC_EVChargeParameterPtr->EVMinimumDischargePower);
      BPT_DC_EVChargeParameterPtr->EVMinimumDischargePowerFlag = (Exi_BitType)Flag;

      /* BPT_DC_EVChargeParameter -> EVMaximumDischargeCurrent */
      BPT_DC_EVChargeParameterPtr->EVMaximumDischargeCurrent = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_BPT_DC_EVMaximumDischargeCurrent(&scc_PhysicalValueTmp);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BPT_DC_EVChargeParameterPtr->EVMaximumDischargeCurrent);

      /* BPT_DC_EVChargeParameter -> EVMinimumDischargeCurrent */
      BPT_DC_EVChargeParameterPtr->EVMinimumDischargeCurrent = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_BPT_DC_EVMinimumDischargeCurrent(&scc_PhysicalValueTmp);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BPT_DC_EVChargeParameterPtr->EVMinimumDischargeCurrent);

      /* BPT_DC_EVChargeParameter -> EVMinimumVoltage */
      BPT_DC_EVChargeParameterPtr->EVMinimumVoltage = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
      Scc_Get_ISO_Ed2_DIS_DC_EVMinimumVoltage(&scc_PhysicalValueTmp);
      Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BPT_DC_EVChargeParameterPtr->EVMinimumVoltage);

      break;
    }
#endif /* SCC_CHARGING_DC_BPT */

    default:
      Scc_ReportError(Scc_StackError_InvalidTxParameter);
      break;
  }

} /* PRQA S 6010,6030,6050 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL */

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_PowerDeliveryReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_PowerDeliveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2*/ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ED2_DIS_PowerDeliveryReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ED2_DIS_PowerDeliveryReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  boolean               Flag = 0;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_POWER_DELIVERY_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PowerDeliveryReqType);

  /* ChargeProgress */
  Scc_Get_ISO_Ed2_DIS_ChargeProgress(&BodyPtr->ChargeProgress);

  if (Scc_Exi_ControlMode == SCC_ISO_ED2_CONTROL_MODE_TYPE_SCHEDULED)
  {
    if ( (Scc_ExiTx_ISO_Ed2_DIS_SelectedEnergyTransferServiceID == Scc_SID_AC_BPT)
      || (Scc_ExiTx_ISO_Ed2_DIS_SelectedEnergyTransferServiceID == Scc_SID_DC_BPT) )
    {
      Exi_ISO_ED2_DIS_BPT_Scheduled_PDReqControlModeType *reqBPT_Scheduled_PDReqControlModePtr;

      BodyPtr->PDReqControlMode = (P2VAR(Exi_ISO_ED2_DIS_PDReqControlModeType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305,0314 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_BPT_Scheduled_PDReqControlModeType);
      BodyPtr->PDReqControlModeElementId = EXI_ISO_ED2_DIS_BPT_SCHEDULED_PDREQ_CONTROL_MODE_TYPE;
      BodyPtr->PDReqControlModeFlag = 1;

      reqBPT_Scheduled_PDReqControlModePtr = (Exi_ISO_ED2_DIS_BPT_Scheduled_PDReqControlModeType*)BodyPtr->PDReqControlMode; /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */
      /* ScheduleTupleID */
      Scc_Get_ISO_Ed2_DIS_ScheduleTupleID(&reqBPT_Scheduled_PDReqControlModePtr->ScheduleTupleID, &Flag);
      Scc_Exi_SAScheduleTupleID = reqBPT_Scheduled_PDReqControlModePtr->ScheduleTupleID;

      /* ChargingProfile */
      reqBPT_Scheduled_PDReqControlModePtr->EVPowerProfile = (P2VAR(Exi_ISO_ED2_DIS_EVPowerProfileType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_EVPowerProfileType);
      Scc_Get_ISO_Ed2_DIS_ChargingProfilePtr(&reqBPT_Scheduled_PDReqControlModePtr->EVPowerProfile, &Flag);
      /* set the flag */
      reqBPT_Scheduled_PDReqControlModePtr->EVPowerProfileFlag = (Exi_BitType)Flag;

      Scc_Get_ISO_Ed2_DIS_EVOperation(&reqBPT_Scheduled_PDReqControlModePtr->EVOperation, &Flag);
    }
    else
    {
      Exi_ISO_ED2_DIS_Scheduled_PDReqControlModeType *reqScheduled_PDReqControlModePtr;

      BodyPtr->PDReqControlMode = (P2VAR(Exi_ISO_ED2_DIS_PDReqControlModeType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305,0314 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_Scheduled_PDReqControlModeType);
      BodyPtr->PDReqControlModeElementId = EXI_ISO_ED2_DIS_SCHEDULED_PDREQ_CONTROL_MODE_TYPE;
      BodyPtr->PDReqControlModeFlag = 1;

      reqScheduled_PDReqControlModePtr = (Exi_ISO_ED2_DIS_Scheduled_PDReqControlModeType*)BodyPtr->PDReqControlMode; /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */
      /* ScheduleTupleID */
      Scc_Get_ISO_Ed2_DIS_ScheduleTupleID(&reqScheduled_PDReqControlModePtr->ScheduleTupleID, &Flag);
      Scc_Exi_SAScheduleTupleID = reqScheduled_PDReqControlModePtr->ScheduleTupleID;

      /* ChargingProfile */
      reqScheduled_PDReqControlModePtr->EVPowerProfile = (P2VAR(Exi_ISO_ED2_DIS_EVPowerProfileType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_EVPowerProfileType);

      Scc_Get_ISO_Ed2_DIS_ChargingProfilePtr(&reqScheduled_PDReqControlModePtr->EVPowerProfile, &Flag);
      /* set the flag */
      reqScheduled_PDReqControlModePtr->EVPowerProfileFlag = (Exi_BitType)Flag;
    }
  }
  else
  {
    BodyPtr->PDReqControlModeFlag = 0;
  }

} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_MeteringReceiptReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_MeteringReceiptReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ED2_DIS_MeteringReceiptReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ED2_DIS_MeteringReceiptReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  XmlSecurity_ReturnType XmlSecRetVal;
  uint8                  IdBuffer[6] = "ID_MR";

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_METERING_RECEIPT_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_MeteringReceiptReqType);

  /* SessionID */
  /* get the buffer for the SessionID */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->SessionID = (P2VAR(Exi_ISO_ED2_DIS_sessionIDType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_sessionIDType);
  /* copy the SessionID to the buffer */ /* PRQA S 0310, 3305, 0315 3 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
  IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->SessionID->Buffer[0],
    (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &Scc_SessionIDNvm[1], Scc_SessionIDNvm[0]);
  /* set the length of the SessionID */
  BodyPtr->SessionID->Length = Scc_SessionIDNvm[0];

  /* MeterInfo */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->MeterInfo = (P2VAR(Exi_ISO_ED2_DIS_MeterInfoType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[0];

  /* create the attribute ID */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->Id = (P2VAR(Exi_ISO_ED2_DIS_AttributeIdType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_AttributeIdType);
  /* set the attribute ID */
  (void) IpBase_StrCpy(&BodyPtr->Id->Buffer[0], &IdBuffer[0]);
  BodyPtr->Id->Length = 5;

  if (Scc_Exi_ControlMode == SCC_ISO_ED2_CONTROL_MODE_TYPE_SCHEDULED)
  {
    BodyPtr->MRReqControlMode = (P2VAR(Exi_ISO_ED2_DIS_MRReqControlModeType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305,0314 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_Scheduled_MRReqControlModeType);
    BodyPtr->MRReqControlModeElementId = EXI_ISO_ED2_DIS_SCHEDULED_MRREQ_CONTROL_MODE_TYPE;
    BodyPtr->MRReqControlModeFlag = 1;

    /* ScheduleTupleID */
    ((Exi_ISO_ED2_DIS_Scheduled_MRReqControlModeType *)BodyPtr->MRReqControlMode)->ScheduleTupleID = Scc_Exi_SAScheduleTupleID; /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */
  }
  /* Schedule tuple id should not be present in the Schema. To be removed when this is removed from the schema */
  BodyPtr->ScheduleTupleID = Scc_Exi_SAScheduleTupleID;
  if (Scc_Exi_ControlMode != SCC_ISO_ED2_CONTROL_MODE_TYPE_SCHEDULED)
  {
    /* Since it is not scheduled the scheduled tuple id is 0 which leads to a Exi encode error. To be removed when this is removed from the schema */
    BodyPtr->ScheduleTupleID = 1;
  }

  /* initialize workspace */
  XmlSecRetVal = XmlSecurity_InitSigGenWorkspace(&Scc_Exi_XmlSecSigGenWs,
    &Scc_Exi_StructBuf[*BufIdxPtr], (uint16)(SCC_EXI_STRUCT_BUF_LEN-*BufIdxPtr),
    XmlSecurity_SAT_ECDSA_SHA256, XmlSecurity_CAT_EXI);
  if ( XmlSec_RetVal_OK != XmlSecRetVal )
  {
    /* report the error to the application */
    Scc_ReportError(Scc_StackError_XmlSecurity);
  }
  else
  {
    /* set the pointer for the signature */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
    Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Header->Signature = (P2VAR(Exi_XMLSIG_SignatureType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Header->SignatureFlag = 1;

    /* add exi references */
    /* PRQA S 0316 5 */ /* MD_Scc_0310_0314_0316_3305 */
    XmlSecRetVal = XmlSecurity_AddExiReference(&Scc_Exi_XmlSecSigGenWs, &Scc_Exi_TempBuf[Scc_Exi_TempBufPos],
      (P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement,
      (P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->Id->Buffer[0], Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId,
      (uint16)(SCC_EXI_TEMP_BUF_LEN-Scc_Exi_TempBufPos), BodyPtr->Id->Length,
      (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_ED2_DIS_TYPE, Scc_SHA256JobId);

    if ( XmlSec_RetVal_OK != XmlSecRetVal )
    {
      /* report the error to the application */
      Scc_ReportError(Scc_StackError_XmlSecurity);
    }
    else
    {
      /* generate the signature(s) */
      XmlSecRetVal = XmlSecurity_GenerateSignature(&Scc_Exi_XmlSecSigGenWs,
        &Scc_Exi_TempBuf[Scc_Exi_TempBufPos],
        (uint16)(SCC_EXI_TEMP_BUF_LEN-Scc_Exi_TempBufPos),
        Scc_ECDSASignCertKeyIds[Scc_CertsWs.ChosenContrCertChainIdx], Scc_ECDSASignCertJobIds[Scc_CertsWs.ChosenContrCertChainIdx]);

      if ( XmlSec_RetVal_OK != XmlSecRetVal )
      {
        /* report the error to the application */
        Scc_ReportError(Scc_StackError_XmlSecurity);
      }
      else
      {
        retVal = E_OK;
      }
    }
  }

  return retVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_SessionStopReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_SessionStopReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ED2_DIS_SessionStopReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ED2_DIS_SessionStopReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_SESSION_STOP_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_SessionStopReqType);

  /* ChargingSession */
  Scc_Get_ISO_Ed2_DIS_ChargingSession(&BodyPtr->ChargingSession);
  Scc_Exi_ChargingSession = (Scc_ISO_ChargingSessionType)BodyPtr->ChargingSession; /* [ISOVersionCast] */ /* PRQA S 4322 */ /* MD_Scc_Exi_Generic */

}


#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_CableCheckReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_CableCheckReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr) /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_CABLE_CHECK_REQ_TYPE;
  /* *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_CableCheckReqType); */ /* uncomment if CableCheckReq has parameters */

  SCC_DUMMY_STATEMENT(BufIdxPtr); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint -e{438} */
}

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_PreChargeReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_PreChargeReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr) /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ED2_DIS_PreChargeReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ED2_DIS_PreChargeReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;
  Scc_PhysicalValueType scc_PhysicalValueTmp;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_PRE_CHARGE_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PreChargeReqType);

  /* EVTargetVoltage */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVTargetVoltage = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
  Scc_Get_ISO_Ed2_DIS_DC_EVTargetVoltage(&scc_PhysicalValueTmp);
  Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVTargetVoltage);

  /* EVTargetCurrent */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVTargetCurrent = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
  Scc_Get_ISO_Ed2_DIS_DC_EVTargetCurrent(&scc_PhysicalValueTmp);
  Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVTargetCurrent);
}

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_CurrentDemandReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_CurrentDemandReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr) /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ED2_DIS_CurrentDemandReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ED2_DIS_CurrentDemandReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  boolean Flag = 0; /* PRQA S 2982 */ /* MD_Scc_2982 */
  Scc_PhysicalValueType scc_PhysicalValueTmp;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_CURRENT_DEMAND_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_CurrentDemandReqType);

  /* DisplayParameters */ /* PRQA S 0310,3305,0314 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->DisplayParameters =
    (P2VAR(Exi_ISO_ED2_DIS_DisplayParametersType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_DisplayParametersType);
  Scc_Get_ISO_Ed2_DIS_DisplayParameters(BodyPtr->DisplayParameters, &Flag); /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->DisplayParametersFlag = Flag;

  if (SCC_ISO_PAYMENT_OPTION_TYPE_CONTRACT == Scc_Exi_PaymentOption)
  {
    /* MeteringReceiptRequested */ /* PRQA S 0310,3305,0314 2 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->CLReqIdentificationMode =
      (P2VAR(Exi_ISO_ED2_DIS_CLReqIdentificationModeType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PnC_CLReqIdentificationModeType);
    Scc_Get_ISO_Ed2_DIS_MeteringReceiptRequested(&((Exi_ISO_ED2_DIS_PnC_CLReqIdentificationModeType *)BodyPtr->CLReqIdentificationMode)->MeteringReceiptRequested, &Flag); /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->CLReqIdentificationModeFlag = 1;
    BodyPtr->CLReqIdentificationModeElementId = EXI_ISO_ED2_DIS_PN_C_CLREQ_IDENTIFICATION_MODE_TYPE;
  }
  else
  {
    BodyPtr->CLReqIdentificationModeFlag = 0;
  }

  if (Scc_Exi_ControlMode == SCC_ISO_ED2_CONTROL_MODE_TYPE_SCHEDULED)
  {
    Exi_ISO_ED2_DIS_Scheduled_CDReqControlModeType *reqScheduled_CDReqControlModePtr;

    BodyPtr->CDReqControlMode = (P2VAR(Exi_ISO_ED2_DIS_CDReqControlModeType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_Scheduled_CDReqControlModeType);
    BodyPtr->CDReqControlModeElementId = EXI_ISO_ED2_DIS_SCHEDULED_CDREQ_CONTROL_MODE_TYPE;
    reqScheduled_CDReqControlModePtr = (Exi_ISO_ED2_DIS_Scheduled_CDReqControlModeType *)BodyPtr->CDReqControlMode; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */

    /* EVTargetEnergyRequest */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    reqScheduled_CDReqControlModePtr->EVTargetEnergyRequest =
      (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    Scc_Get_ISO_Ed2_DIS_DC_EVTargetEnergyRequest(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, reqScheduled_CDReqControlModePtr->EVTargetEnergyRequest);
    reqScheduled_CDReqControlModePtr->EVTargetEnergyRequestFlag = Flag;

    /* EVMaximumEnergyRequest */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    reqScheduled_CDReqControlModePtr->EVMaximumEnergyRequest =
      (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    Scc_Get_ISO_Ed2_DIS_DC_EVMaximumEnergyRequest(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, reqScheduled_CDReqControlModePtr->EVMaximumEnergyRequest);
    reqScheduled_CDReqControlModePtr->EVMaximumEnergyRequestFlag = Flag;

    /* EVMinimumEnergyRequest */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    reqScheduled_CDReqControlModePtr->EVMinimumEnergyRequest =
      (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    Scc_Get_ISO_Ed2_DIS_DC_EVMinimumEnergyRequest(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, reqScheduled_CDReqControlModePtr->EVMinimumEnergyRequest);
    reqScheduled_CDReqControlModePtr->EVMinimumEnergyRequestFlag = Flag;

    /* EVTargetCurrent */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    reqScheduled_CDReqControlModePtr->EVTargetCurrent = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    Scc_Get_ISO_Ed2_DIS_DC_EVTargetCurrent(&scc_PhysicalValueTmp);
    Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, reqScheduled_CDReqControlModePtr->EVTargetCurrent);

    /* EVTargetVoltage */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    reqScheduled_CDReqControlModePtr->EVTargetVoltage = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    Scc_Get_ISO_Ed2_DIS_DC_EVTargetVoltage(&scc_PhysicalValueTmp);
    Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, reqScheduled_CDReqControlModePtr->EVTargetVoltage);

    /* EVMaximumChargeCurrent */
    reqScheduled_CDReqControlModePtr->EVMaximumChargeCurrent = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    Scc_Get_ISO_Ed2_DIS_DC_EVMaximumChargeCurrent(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, reqScheduled_CDReqControlModePtr->EVMaximumChargeCurrent);
    reqScheduled_CDReqControlModePtr->EVMaximumChargeCurrentFlag = (Exi_BitType)Flag;

    /* EVMaximumChargePower */
    reqScheduled_CDReqControlModePtr->EVMaximumChargePower = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    Scc_Get_ISO_Ed2_DIS_DC_EVMaximumChargePower(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, reqScheduled_CDReqControlModePtr->EVMaximumChargePower);
    reqScheduled_CDReqControlModePtr->EVMaximumChargePowerFlag = (Exi_BitType)Flag;

    /* EVMaximumVoltage */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    reqScheduled_CDReqControlModePtr->EVMaximumVoltage = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    Scc_Get_ISO_Ed2_DIS_DC_EVMaximumVoltage(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, reqScheduled_CDReqControlModePtr->EVMaximumVoltage);
    reqScheduled_CDReqControlModePtr->EVMaximumVoltageFlag = Flag;

    /* Standby */
    Scc_Get_ISO_Ed2_DIS_StandByMode(&reqScheduled_CDReqControlModePtr->Standby);
  }
  else
  {/* Scc_Exi_ControlMode == SCC_ISO_ED2_CONTROL_MODE_TYPE_DYNAMIC */
    Exi_ISO_ED2_DIS_Dynamic_CDReqControlModeType *reqDynamic_CDReqControlModePtr;

    BodyPtr->CDReqControlMode = (P2VAR(Exi_ISO_ED2_DIS_CDReqControlModeType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_Dynamic_CDReqControlModeType);
    BodyPtr->CDReqControlModeElementId = EXI_ISO_ED2_DIS_DYNAMIC_CDREQ_CONTROL_MODE_TYPE;
    reqDynamic_CDReqControlModePtr = (Exi_ISO_ED2_DIS_Dynamic_CDReqControlModeType *)BodyPtr->CDReqControlMode; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */

    /* EVTargetEnergyRequest */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    reqDynamic_CDReqControlModePtr->EVTargetEnergyRequest =
      (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    Scc_Get_ISO_Ed2_DIS_DC_EVTargetEnergyRequest(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, reqDynamic_CDReqControlModePtr->EVTargetEnergyRequest);

    /* EVMaximumEnergyRequest */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    reqDynamic_CDReqControlModePtr->EVMaximumEnergyRequest =
      (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    Scc_Get_ISO_Ed2_DIS_DC_EVMaximumEnergyRequest(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, reqDynamic_CDReqControlModePtr->EVMaximumEnergyRequest);

    /* EVMinimumEnergyRequest */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    reqDynamic_CDReqControlModePtr->EVMinimumEnergyRequest =
      (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    Scc_Get_ISO_Ed2_DIS_DC_EVMinimumEnergyRequest(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, reqDynamic_CDReqControlModePtr->EVMinimumEnergyRequest);


    /* EVMaximumChargeCurrent */
    reqDynamic_CDReqControlModePtr->EVMaximumChargeCurrent = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    Scc_Get_ISO_Ed2_DIS_DC_EVMaximumChargeCurrent(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, reqDynamic_CDReqControlModePtr->EVMaximumChargeCurrent);

    /* EVMaximumChargePower */
    reqDynamic_CDReqControlModePtr->EVMaximumChargePower = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    Scc_Get_ISO_Ed2_DIS_DC_EVMaximumChargePower(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, reqDynamic_CDReqControlModePtr->EVMaximumChargePower);

    /* EVMaximumVoltage */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    reqDynamic_CDReqControlModePtr->EVMaximumVoltage = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    Scc_Get_ISO_Ed2_DIS_DC_EVMaximumVoltage(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc_2_ISOEd2_PhysicalValue(&scc_PhysicalValueTmp, reqDynamic_CDReqControlModePtr->EVMaximumVoltage);
  }
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_DIS_WeldingDetectionReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_WeldingDetectionReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)  /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
{
  /* WeldingDetectionReq has no elements */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_WELDING_DETECTION_REQ_TYPE;
  /* *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_WeldingDetectionReqType); */
  SCC_DUMMY_STATEMENT(BufIdxPtr); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */

}

#endif /* SCC_CHARGING_DC */



#if ( defined SCC_CHARGING_DC_BPT ) && ( SCC_CHARGING_DC_BPT == STD_ON )
/**********************************************************************************************************************
*  Scc_ExiTx_ISO_Ed2_DIS_DC_BidirectionalControlReq
*********************************************************************************************************************/
/*!
*
* Internal comment removed.
 *
 *
*/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_DIS_DC_BidirectionalControlReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ED2_DIS_DC_BidirectionalControlReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ED2_DIS_DC_BidirectionalControlReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  boolean               Flag = FALSE;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId = EXI_ISO_ED2_DIS_DC_BIDIRECTIONAL_CONTROL_REQ_TYPE; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_DC_BidirectionalControlReqType);

  /* DisplayParameters */
  BodyPtr->DisplayParameters = (P2VAR(Exi_ISO_ED2_DIS_DisplayParametersType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_DisplayParametersType);
  Scc_Get_ISO_Ed2_DIS_DisplayParameters(BodyPtr->DisplayParameters, &Flag);
  BodyPtr->DisplayParametersFlag = (Exi_BitType)Flag;

  if ( Scc_Exi_PaymentOption == SCC_ISO_PAYMENT_OPTION_TYPE_CONTRACT )
  {
    /* MeteringReceiptRequested */ /* PRQA S 0310,3305,0314 2 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->CLReqIdentificationMode =
      (P2VAR(Exi_ISO_ED2_DIS_CLReqIdentificationModeType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PnC_CLReqIdentificationModeType);
    Scc_Get_ISO_Ed2_DIS_MeteringReceiptRequested(&((Exi_ISO_ED2_DIS_PnC_CLReqIdentificationModeType *)BodyPtr->CLReqIdentificationMode)->MeteringReceiptRequested, &Flag); /* PRQA S 3109,0316 */ /* MD_Scc_Empty_Statement, MD_Scc_0310_0314_0316_3305 */
    BodyPtr->CLReqIdentificationModeFlag = 1;
    BodyPtr->CLReqIdentificationModeElementId = EXI_ISO_ED2_DIS_PN_C_CLREQ_IDENTIFICATION_MODE_TYPE;
  }
  else
  {
    BodyPtr->CLReqIdentificationModeFlag = 0;
  }

  if ( Scc_Exi_ControlMode == SCC_ISO_ED2_CONTROL_MODE_TYPE_SCHEDULED )
  {
    Exi_ISO_ED2_DIS_Scheduled_DCBCReqControlModeType *reqScheduled_DCBCReqControlModePtr;

    BodyPtr->DCBCReqControlMode = (P2VAR(Exi_ISO_ED2_DIS_DCBCReqControlModeType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_Scheduled_DCBCReqControlModeType);
    BodyPtr->DCBCReqControlModeElementId = EXI_ISO_ED2_DIS_SCHEDULED_DCBCREQ_CONTROL_MODE_TYPE;

    reqScheduled_DCBCReqControlModePtr = (Exi_ISO_ED2_DIS_Scheduled_DCBCReqControlModeType *)BodyPtr->DCBCReqControlMode; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */

    reqScheduled_DCBCReqControlModePtr->EVTargetEnergyRequest = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqScheduled_DCBCReqControlModePtr->EVMaximumEnergyRequest = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqScheduled_DCBCReqControlModePtr->EVMinimumEnergyRequest = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqScheduled_DCBCReqControlModePtr->EVTargetCurrent= (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqScheduled_DCBCReqControlModePtr->EVTargetVoltage = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqScheduled_DCBCReqControlModePtr->EVMaximumChargeCurrent = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqScheduled_DCBCReqControlModePtr->EVMaximumChargePower = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqScheduled_DCBCReqControlModePtr->EVMaximumVoltage = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqScheduled_DCBCReqControlModePtr->EVMinimumVoltage = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqScheduled_DCBCReqControlModePtr->EVMaximumDischargePower = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqScheduled_DCBCReqControlModePtr->EVMaximumDischargeCurrent = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);

    Scc_Get_ISO_Ed2_DIS_Scheduled_DCBCReqControlMode(reqScheduled_DCBCReqControlModePtr);
  }
  else
  {/* Scc_Exi_ControlMode == SCC_ISO_ED2_CONTROL_MODE_TYPE_DYNAMIC */
    Exi_ISO_ED2_DIS_Dynamic_DCBCReqControlModeType *reqDynamic_DCBCReqControlModePtr;

    BodyPtr->DCBCReqControlMode = (P2VAR(Exi_ISO_ED2_DIS_DCBCReqControlModeType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_Dynamic_DCBCReqControlModeType);
    BodyPtr->DCBCReqControlModeElementId = EXI_ISO_ED2_DIS_DYNAMIC_DCBCREQ_CONTROL_MODE_TYPE;

    reqDynamic_DCBCReqControlModePtr = (Exi_ISO_ED2_DIS_Dynamic_DCBCReqControlModeType *)BodyPtr->DCBCReqControlMode; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */

    reqDynamic_DCBCReqControlModePtr->EVTargetEnergyRequest = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqDynamic_DCBCReqControlModePtr->EVMaximumEnergyRequest = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqDynamic_DCBCReqControlModePtr->EVMinimumEnergyRequest = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqDynamic_DCBCReqControlModePtr->EVMaximumChargeCurrent = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqDynamic_DCBCReqControlModePtr->EVMaximumChargePower = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqDynamic_DCBCReqControlModePtr->EVMaximumVoltage = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqDynamic_DCBCReqControlModePtr->EVMinimumVoltage = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqDynamic_DCBCReqControlModePtr->EVMaximumDischargePower = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);
    reqDynamic_DCBCReqControlModePtr->EVMaximumDischargeCurrent = (P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_PhysicalValueType);

    Scc_Get_ISO_Ed2_DIS_Dynamic_DCBCReqControlMode(reqDynamic_DCBCReqControlModePtr);
  }
}
#endif /* SCC_CHARGING_DC_BPT */

/**********************************************************************************************************************
 *  LOCAL HELPER FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Ed2_WriteHeader
 *********************************************************************************************************************/
 /*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_Ed2_WriteHeader(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Implementation ----------------------------------------------- */
  /* set the buffer for this new V2G message */
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_V2G_MessageType);
  /* #10 set the buffer of the V2G message header */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Header =
    (P2VAR(Exi_ISO_ED2_DIS_MessageHeaderType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_MessageHeaderType);
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Header->SignatureFlag = 0;
  /* set the buffer of the SessionID */
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Header->SessionID =
    (P2VAR(Exi_ISO_ED2_DIS_sessionIDType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ED2_DIS_sessionIDType);
  Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Header->SessionID->Length = Scc_SessionIDNvm[0];
  /* copy the SessionID to the buffer */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
  IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &Scc_ExiTx_ISO_Ed2_DIS_MsgPtr->Header->SessionID->Buffer[0],
    (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &Scc_SessionIDNvm[1], (uint32)Scc_SessionIDNvm[0]);

}

#define SCC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
/* PRQA L:NEST_STRUCTS */
/* PRQA L:RETURN_PATHS */

#endif /* SCC_SCHEMA_ISO_ED2 */

/**********************************************************************************************************************
 *  END OF FILE: Scc_ExiTx_ISO_Ed2_DIS_Ed2_CD.c
 *********************************************************************************************************************/
