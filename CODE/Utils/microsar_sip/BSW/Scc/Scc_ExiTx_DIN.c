/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc_ExiTx_DIN.c
 *        \brief  Smart Charging Communication Source Code File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *********************************************************************************************************************/

#define SCC_EXITX_DIN_SOURCE

/**********************************************************************************************************************
 LOCAL MISRA / PCLINT JUSTIFICATION
 **********************************************************************************************************************/
/* PRQA S 0777 EOF */ /* MD_MSR_Rule5.1 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Scc_Cfg.h"

#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )

#include "Scc_Exi.h"
#include "Scc.h"
#include "Scc_Lcfg.h"
#include "Scc_Priv.h"
#include "Scc_Interface_Cfg.h"
#include "Scc_ConfigParams_Cfg.h"

#if ( SCC_DEV_ERROR_DETECT == STD_ON )
#include "Det.h"
#endif /* SCC_DEV_ERROR_DETECT */
#include "EthIf.h"
#include "Exi.h"
#include "IpBase.h"

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined (SCC_LOCAL)
# define SCC_LOCAL static
#endif

/**********************************************************************************************************************
 *  LOCAL / GLOBAL DATA
 *********************************************************************************************************************/

/* other variables */
#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

SCC_LOCAL P2VAR(Exi_DIN_V2G_MessageType, AUTOMATIC, SCC_VAR_NOINIT) Scc_ExiTx_DIN_MsgPtr;

#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3453 MACROS_FUNCTION_LIKE */ /* MD_MSR_FctLikeMacro */

/* PRQA L:MACROS_FUNCTION_LIKE */
/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_<DIN-Request>
 *********************************************************************************************************************/
 /*! \brief         Set all parameters for DIN request message
 *  \details        Set all parameters for the body element of the request message.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/

/* Common */
/* see pattern Scc_ExiTx_DIN_<DIN-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_SessionSetupReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_DIN_<DIN-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_ServiceDiscoveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_DIN_<DIN-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_ServicePaymentSelectionReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_DIN_<DIN-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_ContractAuthenticationReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_DIN_<DIN-Request> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_DIN_ChargeParameterDiscoveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_DIN_<DIN-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_PowerDeliveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_DIN_<DIN-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_SessionStopReq(void);

/* DC */
/* see pattern Scc_ExiTx_DIN_<DIN-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_CableCheckReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_DIN_<DIN-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_PreChargeReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_DIN_<DIN-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_CurrentDemandReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
/* see pattern Scc_ExiTx_DIN_<DIN-Request> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_WeldingDetectionReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

/**********************************************************************************************************************
*  Scc_ExiTx_DIN_WriteHeader
*********************************************************************************************************************/
/*! \brief          Write the DIN header to the buffer
*  \details        -
*  \param[in]      BufIdxPtr                Pointer to the buffer index
*  \pre            -
*  \context        TASK|ISR2
*  \reentrant      TRUE
*  \synchronous    TRUE
*********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_WriteHeader(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

/**********************************************************************************************************************
*  Scc_ExiTx_DIN_WriteEVStatusDC
*********************************************************************************************************************/
/*! \brief          Write the EVStatus to the buffer
*  \details        -
*  \param[out]     EVStatePtrPtr            Pointer to the EV Status
*  \param[in]      BufIdxPtr                Pointer to the buffer index
*  \pre            -
*  \context        TASK|ISR2
*  \reentrant      TRUE
*  \synchronous    TRUE
*********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_WriteEVStatusDC(P2VAR(Exi_DIN_DC_EVStatusType, AUTOMATIC, SCC_VAR_NOINIT) *EVStatePtrPtr,
  P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_ExiTx_DIN_Init(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the message pointer to the Exi buffer. */
  Scc_ExiTx_DIN_MsgPtr = (P2VAR(Exi_DIN_V2G_MessageType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_StructBuf[0]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */

}

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_EncodeMessage
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiTx_DIN_EncodeMessage(
    P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr
)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  uint16         lBufIdx = 0;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Since no data has been sent, this means that the initializations has to be done */
  if(0u == Scc_TxDataSent)
  {
    /* #20 Initialize the exi workspace and check if it failed */
    if ( (Std_ReturnType)E_OK == Scc_Exi_InitEncodingWorkspace(PBufPtr) )
    {
      /* set the V2G_Message as root element */
      Scc_Exi_EncWs.InputData.RootElementId = EXI_DIN_V2G_MESSAGE_TYPE;
      /* initialize the header of the V2G messages */
      Scc_ExiTx_DIN_WriteHeader(&lBufIdx);
      /* create the body */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
      Scc_ExiTx_DIN_MsgPtr->Body = (P2VAR(Exi_DIN_BodyType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[lBufIdx];
      lBufIdx += (uint16)sizeof(Exi_DIN_BodyType);
      /* set the common body parameters */
      Scc_ExiTx_DIN_MsgPtr->Body->BodyElementFlag = 1;
      Scc_ExiTx_DIN_MsgPtr->Body->BodyElement = (P2VAR(Exi_DIN_BodyBaseType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[lBufIdx]; /* PRQA S 0314 */ /* MD_Scc_0310_0314_0316_3305 */

      /* #30 create request message and set MessageTimeout */
      switch ( Scc_MsgTrig )
      {
        /* check if a session setup request shall be sent */
      case Scc_MsgTrig_SessionSetup:
        Scc_ExiTx_DIN_SessionSetupReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_DIN_SessionSetupMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a service discovery request shall be sent */
      case Scc_MsgTrig_ServiceDiscovery:
        Scc_ExiTx_DIN_ServiceDiscoveryReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_DIN_ServiceDiscoveryMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a service payment selection request shall be sent */
      case Scc_MsgTrig_PaymentServiceSelection:
        Scc_ExiTx_DIN_ServicePaymentSelectionReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_DIN_ServicePaymentSelectionMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a contract authentication request shall be sent */
      case Scc_MsgTrig_Authorization:
        Scc_ExiTx_DIN_ContractAuthenticationReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_DIN_ContractAuthenticationMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a charge parameter discovery request shall be sent */
      case Scc_MsgTrig_ChargeParameterDiscovery:
        if ( Scc_ExiTx_DIN_ChargeParameterDiscoveryReq(&lBufIdx) == E_OK )
        {
          Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_DIN_ChargeParameterDiscoveryMessageTimeout;
          retVal = E_OK;
        }
        break;

        /* check if a power delivery request shall be sent */
      case Scc_MsgTrig_PowerDelivery:
        Scc_ExiTx_DIN_PowerDeliveryReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_DIN_PowerDeliveryMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a session stop request shall be sent */
      case Scc_MsgTrig_SessionStop:
        Scc_ExiTx_DIN_SessionStopReq();
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_DIN_SessionStopMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a cable check request shall be sent */
      case Scc_MsgTrig_CableCheck:
        Scc_ExiTx_DIN_CableCheckReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_DIN_CableCheckMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a pre charge request shall be sent */
      case Scc_MsgTrig_PreCharge:
        Scc_ExiTx_DIN_PreChargeReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_DIN_PreChargeMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a current demand request shall be sent */
      case Scc_MsgTrig_CurrentDemand:
        Scc_ExiTx_DIN_CurrentDemandReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_DIN_CurrentDemandMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a welding detection request shall be sent */
      case Scc_MsgTrig_WeldingDetection:
        Scc_ExiTx_DIN_WeldingDetectionReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_DIN_WeldingDetectionMessageTimeout;
        retVal = E_OK;
        break;

      default:
        /* report a V2G error of the current trigger */
        Scc_ReportError(Scc_StackError_InvalidTxParameter);
        retVal = E_NOT_OK;
        break;
      }

      /* #40 encode and send the request message */
      if ( E_OK == retVal )
      {
        retVal = E_NOT_OK;

        if(E_OK == Scc_Exi_EncodeExiStream(SCC_V2GTP_HDR_PAYLOAD_TYPE_EXI, PBufPtr))
        {
          *BufLengthPtr = (uint16)Scc_TxDataSent;
          PBufPtr->totLen = *BufLengthPtr;
          PBufPtr->len    = *BufLengthPtr;

          /* provide sent data to application */
          {
            /* create and set the tx buffer pointer */
            Scc_TxRxBufferPointerType V2GRequest;
            V2GRequest.PbufPtr = PBufPtr;
            V2GRequest.FirstPart = TRUE;
            V2GRequest.StreamComplete = Scc_Exi_EncWs.EncWs.StreamComplete;
            /* provide the buffer to the application */
            Scc_Set_Core_V2GRequest(&V2GRequest);
          } /*lint !e550 */

          retVal = E_OK;
        }
      }
    }
  }
  else
  {
    /* #50 send the pending request message */
    retVal = Scc_Exi_SendSubsequentMessage(BufPtr, BufLengthPtr, (uint16)SCC_V2GTP_HDR_PAYLOAD_TYPE_EXI, PBufPtr);
  }

  return retVal; /*lint !e438 */

} /* PRQA S 6010,6030,6050 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL */

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_SessionSetupReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_SessionSetupReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_DIN_SessionSetupReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_DIN_SessionSetupReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_DIN_MsgPtr->Body->BodyElementElementId = EXI_DIN_SESSION_SETUP_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_SessionSetupReqType);

  /* EVCCID */ /* PRQA S 0310,3305 6 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVCCID = (P2VAR(Exi_DIN_evccIDType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_evccIDType);
  /* get the MAC address from EthIf */
  EthIf_GetPhysAddr((uint8)SCC_CTRL_IDX, &BodyPtr->EVCCID->Buffer[0]);
  BodyPtr->EVCCID->Length = 6;

}

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_ServiceDiscoveryReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_ServiceDiscoveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_DIN_ServiceDiscoveryReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_DIN_ServiceDiscoveryReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_DIN_MsgPtr->Body->BodyElement;

  boolean Flag = 0; /* PRQA S 2982 */ /* MD_Scc_2982 */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_DIN_MsgPtr->Body->BodyElementElementId = EXI_DIN_SERVICE_DISCOVERY_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_ServiceDiscoveryReqType);

  /* ServiceScope */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->ServiceScopeFlag = 0;

  /* ServiceCategory */
  Scc_Get_DIN_ServiceCategory(&BodyPtr->ServiceCategory, &Flag);
  BodyPtr->ServiceCategoryFlag = Flag;

}

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_ServicePaymentSelectionReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_ServicePaymentSelectionReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_DIN_ServicePaymentSelectionReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_DIN_ServicePaymentSelectionReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_DIN_MsgPtr->Body->BodyElementElementId = EXI_DIN_SERVICE_PAYMENT_SELECTION_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_ServicePaymentSelectionReqType);

  /* SelectedPaymentOption */
  BodyPtr->SelectedPaymentOption = EXI_DIN_PAYMENT_OPTION_TYPE_EXTERNAL_PAYMENT;

  /* create a SelectedServiceList */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->SelectedServiceList =
    (P2VAR(Exi_DIN_SelectedServiceListType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_SelectedServiceListType);
  /* create a SelectedService */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->SelectedServiceList->SelectedService =
    (P2VAR(Exi_DIN_SelectedServiceType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_SelectedServiceType);
  /* set the SelectedService */
  BodyPtr->SelectedServiceList->SelectedService->ServiceID = Scc_ExiRx_DIN_ServiceID;
  BodyPtr->SelectedServiceList->SelectedService->ParameterSetIDFlag = 0;
  BodyPtr->SelectedServiceList->SelectedService->NextSelectedServicePtr =
    (P2VAR(Exi_DIN_SelectedServiceType, AUTOMATIC, SCC_APPL_DATA))NULL_PTR;

}

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_ContractAuthenticationReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_ContractAuthenticationReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_DIN_ContractAuthenticationReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_DIN_ContractAuthenticationReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_DIN_MsgPtr->Body->BodyElementElementId = EXI_DIN_CONTRACT_AUTHENTICATION_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_ContractAuthenticationReqType);

  /* GenChallenge */
  BodyPtr->IdFlag = FALSE;
  BodyPtr->GenChallengeFlag = FALSE;

}

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_ChargeParameterDiscoveryReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_DIN_ChargeParameterDiscoveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_DIN_ChargeParameterDiscoveryReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_DIN_ChargeParameterDiscoveryReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_DIN_MsgPtr->Body->BodyElement;
  P2VAR(Exi_DIN_DC_EVChargeParameterType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVChargeParameterTypePtr;

  boolean        Flag = 0; /* PRQA S 2982 */ /* MD_Scc_2982 */
  Std_ReturnType retVal = E_OK;
  Scc_PhysicalValueType scc_PhysicalValueTmp;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_DIN_MsgPtr->Body->BodyElementElementId = EXI_DIN_CHARGE_PARAMETER_DISCOVERY_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_ChargeParameterDiscoveryReqType);
  /* set the EVChargeParameter pointer */ /* PRQA S 0310,3305,0314 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVChargeParameter = (P2VAR(Exi_DIN_EVChargeParameterType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_DC_EVChargeParameterType);
  BodyPtr->EVChargeParameterElementId = EXI_DIN_DC_EVCHARGE_PARAMETER_TYPE;

  /* EVRequestedEnergyTransferType */
  Scc_Get_DIN_RequestedEnergyTransferMode(&BodyPtr->EVRequestedEnergyTransferType);

  /* get the pointer to the Exi_DIN_DC_EVChargeParameterType */ /* PRQA S 0316 1 */ /* MD_Scc_0310_0314_0316_3305 */
  DC_EVChargeParameterTypePtr = (P2VAR(Exi_DIN_DC_EVChargeParameterType, AUTOMATIC, SCC_APPL_DATA))(BodyPtr->EVChargeParameter);

  /* DC_EVChargeParameter -> DC_EVStatus*/
  Scc_ExiTx_DIN_WriteEVStatusDC(&DC_EVChargeParameterTypePtr->DC_EVStatus, BufIdxPtr);

  /* DC_EVChargeParameter -> EVMaximumCurrentLimit */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  DC_EVChargeParameterTypePtr->EVMaximumCurrentLimit =
    (P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PhysicalValueType);
  Scc_Get_DIN_EVMaximumCurrentLimit(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2DIN_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterTypePtr->EVMaximumCurrentLimit);
  DC_EVChargeParameterTypePtr->EVMaximumCurrentLimit->UnitFlag = 1;
  DC_EVChargeParameterTypePtr->EVMaximumCurrentLimit->Unit = EXI_DIN_UNIT_SYMBOL_TYPE_A;
  /* parameter is not optional for this message, check if it was set */
  if ( TRUE != Flag )
  {
    Scc_ReportError(Scc_StackError_InvalidTxParameter);
    retVal = E_NOT_OK;
  }

  /* DC_EVChargeParameter -> EVMaximumPowerLimit */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  DC_EVChargeParameterTypePtr->EVMaximumPowerLimit =
    (P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PhysicalValueType);
  Scc_Get_DIN_EVMaximumPowerLimit(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2DIN_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterTypePtr->EVMaximumPowerLimit);
  DC_EVChargeParameterTypePtr->EVMaximumPowerLimit->UnitFlag = 1;
  DC_EVChargeParameterTypePtr->EVMaximumPowerLimit->Unit = EXI_DIN_UNIT_SYMBOL_TYPE_W;
  DC_EVChargeParameterTypePtr->EVMaximumPowerLimitFlag = Flag;

  /* DC_EVChargeParameter -> EVMaximumVoltageLimit */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  DC_EVChargeParameterTypePtr->EVMaximumVoltageLimit =
    (P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PhysicalValueType);
  Scc_Get_DIN_EVMaximumVoltageLimit(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2DIN_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterTypePtr->EVMaximumVoltageLimit);
  DC_EVChargeParameterTypePtr->EVMaximumVoltageLimit->UnitFlag = 1;
  DC_EVChargeParameterTypePtr->EVMaximumVoltageLimit->Unit = EXI_DIN_UNIT_SYMBOL_TYPE_V;
  /* parameter is not optional for this message, check if it was set */
  if ( TRUE != Flag )
  {
    Scc_ReportError(Scc_StackError_InvalidTxParameter);
    retVal = E_NOT_OK;
  }

  /* EVEnergyCapacity */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  DC_EVChargeParameterTypePtr->EVEnergyCapacity =
    (P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PhysicalValueType);
  Scc_Get_DIN_EVEnergyCapacity(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2DIN_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterTypePtr->EVEnergyCapacity);
  DC_EVChargeParameterTypePtr->EVEnergyCapacity->UnitFlag = 1;
  DC_EVChargeParameterTypePtr->EVEnergyCapacity->Unit = EXI_DIN_UNIT_SYMBOL_TYPE_WH;
  DC_EVChargeParameterTypePtr->EVEnergyCapacityFlag = Flag;

  /* EVEnergyRequest */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  DC_EVChargeParameterTypePtr->EVEnergyRequest =
    (P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PhysicalValueType);
  Scc_Get_DIN_EVEnergyRequest(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2DIN_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterTypePtr->EVEnergyRequest);
  DC_EVChargeParameterTypePtr->EVEnergyRequest->UnitFlag = 1;
  DC_EVChargeParameterTypePtr->EVEnergyRequest->Unit = EXI_DIN_UNIT_SYMBOL_TYPE_WH;
  DC_EVChargeParameterTypePtr->EVEnergyRequestFlag = Flag;

  /* FullSOC */
  Scc_Get_DIN_FullSOC(&DC_EVChargeParameterTypePtr->FullSOC, &Flag);
  DC_EVChargeParameterTypePtr->FullSOCFlag = Flag;

  /* BulkSOC */
  Scc_Get_DIN_BulkSOC(&DC_EVChargeParameterTypePtr->BulkSOC, &Flag);
  DC_EVChargeParameterTypePtr->BulkSOCFlag = Flag;

  return retVal;

} /* PRQA S 6010,6050 */ /* MD_MSR_STPTH,MD_MSR_STCAL */

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_PowerDeliveryReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_PowerDeliveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_DIN_PowerDeliveryReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_DIN_PowerDeliveryReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_DIN_MsgPtr->Body->BodyElement;
  P2VAR(Exi_DIN_DC_EVPowerDeliveryParameterType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVPowerDeliveryParameter;

  boolean Flag = FALSE; /* PRQA S 2982 */ /* MD_Scc_2982 */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_DIN_MsgPtr->Body->BodyElementElementId = EXI_DIN_POWER_DELIVERY_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PowerDeliveryReqType);

  /* ReadyToChargeState */
  Scc_Get_DIN_ReadyToChargeState(&BodyPtr->ReadyToChargeState);

  /* ChargingProfile */
  Scc_Get_DIN_ChargingProfilePtr(&BodyPtr->ChargingProfile, &Flag);
  BodyPtr->ChargingProfileFlag = (Exi_BitType)Flag;

  /* check if the EVPowerDeliveryParameter shall be sent */
  Scc_Get_DIN_EVPowerDeliveryParameterFlag(&Flag);
  if ( TRUE == Flag )
  {
    /* get the pointer to the Exi_DIN_DC_EVPowerDeliveryParameterType */ /* PRQA S 0310,3305,0314 2 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->EVPowerDeliveryParameter =
      (P2VAR(Exi_DIN_EVChargeParameterType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_DIN_DC_EVPowerDeliveryParameterType);
    DC_EVPowerDeliveryParameter = /* PRQA S 0316 1 */ /* MD_Scc_0310_0314_0316_3305 */
      (P2VAR(Exi_DIN_DC_EVPowerDeliveryParameterType, AUTOMATIC, SCC_APPL_DATA))(BodyPtr->EVPowerDeliveryParameter);

    /* DC_EVPowerDeliveryParameter */
    BodyPtr->EVPowerDeliveryParameterElementId = EXI_DIN_DC_EVPOWER_DELIVERY_PARAMETER_TYPE;
    BodyPtr->EVPowerDeliveryParameterFlag = 1;

    /* DC_EVPowerDeliveryParameter -> DC_EVStatus */
    Scc_ExiTx_DIN_WriteEVStatusDC(&DC_EVPowerDeliveryParameter->DC_EVStatus, BufIdxPtr);

    /* DC_EVPowerDeliveryParameter -> BulkChargingComplete */
    Scc_Get_DIN_BulkChargingComplete(&DC_EVPowerDeliveryParameter->BulkChargingComplete, &Flag);
    DC_EVPowerDeliveryParameter->BulkChargingCompleteFlag = Flag;

    /* DC_EVPowerDeliveryParameter -> ChargingComplete */
    Scc_Get_DIN_ChargingComplete(&DC_EVPowerDeliveryParameter->ChargingComplete);
  }
  else
  {
    BodyPtr->EVPowerDeliveryParameterFlag = 0;
  }

} /* PRQA S 6050 */ /* MD_MSR_STCAL */

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_SessionStopReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_SessionStopReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_DIN_MsgPtr->Body->BodyElementElementId = EXI_DIN_SESSION_STOP_REQ_TYPE;
  /* *BufIdxPtr += sizeof(Exi_DIN_SessionStopReqType); */ /* change if SessionStopReq has elements */

}

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_CableCheckReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_CableCheckReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_DIN_CableCheckReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_DIN_CableCheckReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_DIN_MsgPtr->Body->BodyElementElementId = EXI_DIN_CABLE_CHECK_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_CableCheckReqType);

  /* DC_EVStatus */
  Scc_ExiTx_DIN_WriteEVStatusDC(&BodyPtr->DC_EVStatus, BufIdxPtr);

}

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_PreChargeReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_PreChargeReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_PhysicalValueType scc_PhysicalValueTmp;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_DIN_PreChargeReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_DIN_PreChargeReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_DIN_MsgPtr->Body->BodyElementElementId = EXI_DIN_PRE_CHARGE_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PreChargeReqType);

  /* DC_EVStatus */
  Scc_ExiTx_DIN_WriteEVStatusDC(&BodyPtr->DC_EVStatus, BufIdxPtr);

  /* EVTargetVoltage */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVTargetVoltage = (P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PhysicalValueType);
  Scc_Get_DIN_EVTargetVoltage(&scc_PhysicalValueTmp);
  Scc_Conv_Scc2DIN_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVTargetVoltage);
  BodyPtr->EVTargetVoltage->UnitFlag = 1;
  BodyPtr->EVTargetVoltage->Unit = EXI_DIN_UNIT_SYMBOL_TYPE_V;

  /* EVTargetCurrent */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVTargetCurrent = (P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PhysicalValueType);
  Scc_Get_DIN_EVTargetCurrent(&scc_PhysicalValueTmp);
  Scc_Conv_Scc2DIN_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVTargetCurrent);
  BodyPtr->EVTargetCurrent->UnitFlag = 1;
  BodyPtr->EVTargetCurrent->Unit = EXI_DIN_UNIT_SYMBOL_TYPE_A;

}

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_CurrentDemandReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_CurrentDemandReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_DIN_CurrentDemandReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_DIN_CurrentDemandReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_DIN_MsgPtr->Body->BodyElement;

  boolean Flag = FALSE; /* PRQA S 2982 */ /* MD_Scc_2982 */
  Scc_PhysicalValueType scc_PhysicalValueTmp;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_DIN_MsgPtr->Body->BodyElementElementId = EXI_DIN_CURRENT_DEMAND_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_CurrentDemandReqType);

  /* DC_EVStatus */
  Scc_ExiTx_DIN_WriteEVStatusDC(&BodyPtr->DC_EVStatus, BufIdxPtr);

  /* EVTargetCurrent */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVTargetCurrent = (P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PhysicalValueType);
  Scc_Get_DIN_EVTargetCurrent(&scc_PhysicalValueTmp);
  Scc_Conv_Scc2DIN_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVTargetCurrent);
  BodyPtr->EVTargetCurrent->UnitFlag = 1;
  BodyPtr->EVTargetCurrent->Unit = EXI_DIN_UNIT_SYMBOL_TYPE_A;

  /* EVMaximumVoltageLimit */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVMaximumVoltageLimit = (P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PhysicalValueType);
  Scc_Get_DIN_EVMaximumVoltageLimit(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2DIN_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVMaximumVoltageLimit);
  BodyPtr->EVMaximumVoltageLimit->UnitFlag = 1;
  BodyPtr->EVMaximumVoltageLimit->Unit = EXI_DIN_UNIT_SYMBOL_TYPE_V;
  BodyPtr->EVMaximumVoltageLimitFlag = Flag;

  /* EVMaximumCurrentLimit */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVMaximumCurrentLimit = (P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PhysicalValueType);
  Scc_Get_DIN_EVMaximumCurrentLimit(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2DIN_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVMaximumCurrentLimit);
  BodyPtr->EVMaximumCurrentLimit->UnitFlag = 1;
  BodyPtr->EVMaximumCurrentLimit->Unit = EXI_DIN_UNIT_SYMBOL_TYPE_A;
  BodyPtr->EVMaximumCurrentLimitFlag = Flag;

  /* EVMaximumPowerLimit */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVMaximumPowerLimit = (P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PhysicalValueType);
  Scc_Get_DIN_EVMaximumPowerLimit(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2DIN_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVMaximumPowerLimit);
  BodyPtr->EVMaximumPowerLimit->UnitFlag = 1;
  BodyPtr->EVMaximumPowerLimit->Unit = EXI_DIN_UNIT_SYMBOL_TYPE_W;
  BodyPtr->EVMaximumPowerLimitFlag = Flag;

  /* BulkChargingComplete */
  Scc_Get_DIN_BulkChargingComplete(&BodyPtr->BulkChargingComplete, &Flag);
  BodyPtr->BulkChargingCompleteFlag = Flag;

  /* ChargingComplete */
  Scc_Get_DIN_ChargingComplete(&BodyPtr->ChargingComplete);

  /* RemainingTimeToFullSoC */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->RemainingTimeToFullSoC = (P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PhysicalValueType);
  Scc_Get_DIN_RemainingTimeToFullSoC(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2DIN_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->RemainingTimeToFullSoC);
  BodyPtr->RemainingTimeToFullSoC->UnitFlag = 1;
  BodyPtr->RemainingTimeToFullSoC->Unit = EXI_DIN_UNIT_SYMBOL_TYPE_S;
  BodyPtr->RemainingTimeToFullSoCFlag = Flag;

  /* RemainingTimeToBulkSoC */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->RemainingTimeToBulkSoC = (P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PhysicalValueType);
  Scc_Get_DIN_RemainingTimeToBulkSoC(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2DIN_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->RemainingTimeToBulkSoC);
  BodyPtr->RemainingTimeToBulkSoC->UnitFlag = 1;
  BodyPtr->RemainingTimeToBulkSoC->Unit = EXI_DIN_UNIT_SYMBOL_TYPE_S;
  BodyPtr->RemainingTimeToBulkSoCFlag = Flag;

  /* EVTargetVoltage */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVTargetVoltage = (P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_PhysicalValueType);
  Scc_Get_DIN_EVTargetVoltage(&scc_PhysicalValueTmp);
  Scc_Conv_Scc2DIN_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVTargetVoltage);
  BodyPtr->EVTargetVoltage->UnitFlag = 1;
  BodyPtr->EVTargetVoltage->Unit = EXI_DIN_UNIT_SYMBOL_TYPE_V;

} /* PRQA S 6050 */ /* MD_MSR_STCAL */

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_WeldingDetectionReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_WeldingDetectionReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_DIN_WeldingDetectionReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_DIN_WeldingDetectionReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_DIN_MsgPtr->Body->BodyElementElementId = EXI_DIN_WELDING_DETECTION_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_WeldingDetectionReqType);

  /* DC_EVStatus */
  Scc_ExiTx_DIN_WriteEVStatusDC(&BodyPtr->DC_EVStatus, BufIdxPtr);

}

/**********************************************************************************************************************
 *  LOCAL HELPER FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_WriteHeader
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_WriteHeader(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the header in the buffer for this new V2G message */
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_V2G_MessageType);
  /* set the buffer of the V2G message header */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
  Scc_ExiTx_DIN_MsgPtr->Header = (P2VAR(Exi_DIN_MessageHeaderType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_MessageHeaderType);
  Scc_ExiTx_DIN_MsgPtr->Header->NotificationFlag = 0;
  Scc_ExiTx_DIN_MsgPtr->Header->SignatureFlag = 0;
  /* set the buffer of the SessionID */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
  Scc_ExiTx_DIN_MsgPtr->Header->SessionID = (P2VAR(Exi_DIN_sessionIDType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_sessionIDType);
  Scc_ExiTx_DIN_MsgPtr->Header->SessionID->Length = Scc_SessionIDNvm[0];
  /* copy the SessionID to the buffer */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
  IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &Scc_ExiTx_DIN_MsgPtr->Header->SessionID->Buffer[0],
    (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &Scc_SessionIDNvm[1], (uint32)Scc_SessionIDNvm[0]);

}

/**********************************************************************************************************************
 *  Scc_ExiTx_DIN_WriteEVStatusDC
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_DIN_WriteEVStatusDC(P2VAR(Exi_DIN_DC_EVStatusType, AUTOMATIC, SCC_VAR_NOINIT) *EVStatePtrPtr,
  P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean Flag = 0; /* PRQA S 2982 */ /* MD_Scc_2982 */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set EV Status DC for V2G message */
  /* create the root element */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
  *EVStatePtrPtr = (P2VAR(Exi_DIN_DC_EVStatusType, AUTOMATIC, SCC_VAR_NOINIT)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_DIN_DC_EVStatusType);

  /* EVReady */
  Scc_Get_DIN_EVReady(&(*EVStatePtrPtr)->EVReady);

  /* EVCabinConditioning */
  Scc_Get_DIN_EVCabinConditioning(&(*EVStatePtrPtr)->EVCabinConditioning, &Flag);
  (*EVStatePtrPtr)->EVCabinConditioningFlag = Flag;

  /* EVRESSConditioning */
  Scc_Get_DIN_EVRESSConditioning(&(*EVStatePtrPtr)->EVRESSConditioning, &Flag);
  (*EVStatePtrPtr)->EVRESSConditioningFlag = Flag;

  /* EVErrorCode */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
  Scc_Get_DIN_EVErrorCode(&(*EVStatePtrPtr)->EVErrorCode);

  /* EVRESSSOC */
  Scc_Get_DIN_EVRESSSOC(&(*EVStatePtrPtr)->EVRESSSOC);

}

#define SCC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
/* PRQA L:NEST_STRUCTS */
/* PRQA L:RETURN_PATHS */

#endif /* SCC_SCHEMA_DIN */

/**********************************************************************************************************************
 *  END OF FILE: Scc_ExiTx_DIN.c
 *********************************************************************************************************************/
