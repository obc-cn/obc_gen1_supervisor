############################################################################### 
# File Name  : Scc_rules.mak 
# Description: Rules makefile 
#------------------------------------------------------------------------------
# COPYRIGHT
#------------------------------------------------------------------------------
# Copyright (c) 2019 by Vector Informatik GmbH.  All rights reserved.
#------------------------------------------------------------------------------
# REVISION HISTORY
#------------------------------------------------------------------------------
# Version   Date        Author  Description
#------------------------------------------------------------------------------
# 1.00.00   2010-01-01  visalr  Created
# 1.00.00   2015-02-27  visefa  removed revision history for version 1.xx.xx
# 2.00.00   2015-02-27  visefa  removed revision history for version 2.xx.xx
# 4.00.00   2012-12-17  visefa  Update to DIS of ISO15118
# 4.01.00   2013-07-05  visefa  Scc_StateM_Vector added
# 5.00.00   2014-01-30  visefa  Added support for DIN70121
# 5.01.00   2014-02-14  visefa  Scc_Rte.c removed
# 5.01.01   2014-03-19  visefa  removed Scc_ExiZx_DIN.c for some OEM
# 5.02.00   2014-03-25  visefa  Added support for another OEM
# 5.02.01   2014-06-10  visefa  Added DIN files for some OEM
# 5.03.00   2014-06-25  visefa  Added files for customer specific schema
# 5.03.01   2014-07-28  visefa  Fixed filtering
# 6.00.00   2015-02-27  visefa  Added support for another OEM
# 6.00.01   2015-07-13  visefa  Fixed filter rules for one OEM
# 7.00.00   2016-02-03  visefa  Added new OEM, changed file names
# 8.00.00   2016-06-27  visefa  Updated file names for some OEM
# 8.01.00   2016-07-07  visefa  Added new OEM specific files
# 8.02.00   2017-11-08  visphh  Added new OEM specific files
# 9.00.00   2018-01-09  visphh  Removed WPT-DENSO, BWM, BRUSA
# 10.00.00  2018-08-08  visphh  Removed Scc_PBcfg.c
# 11.00.00  2018-09-11  visphh  Removed Daimler changed filter for ISO Ed2
# 12.00.00  2019-02-06  vircbl  Added support of component-based SIP structure
#------------------------------------------------------------------------------
# TemplateVersion = 1.02
###############################################################################


###############################################################
# REGISTRY
#

#e.g.: LIBRARIES_TO_BUILD      +=    $(LIB_OUPUT_PATH)\vendorx_canlib1.$(LIB_FILE_SUFFIX)
LIBRARIES_TO_BUILD      += Scc
Scc_FILES               = Scc$(BSW_SRC_DIR)\Scc.c Scc$(BSW_SRC_DIR)\Scc_Priv.c Scc$(BSW_SRC_DIR)\Scc_Exi.c Scc$(BSW_SRC_DIR)\Scc_ExiRx_ISO.c Scc$(BSW_SRC_DIR)\Scc_ExiTx_ISO.c Scc$(BSW_SRC_DIR)\Scc_StateM_Vector.c Scc$(BSW_SRC_DIR)\Scc_ExiRx_ISO_Ed2.c Scc$(BSW_SRC_DIR)\Scc_ExiTx_ISO_Ed2.c
Scc_FILES              += Scc$(BSW_SRC_DIR)\Scc_ExiRx_DIN.c Scc$(BSW_SRC_DIR)\Scc_ExiTx_DIN.c

# e.g.: CC_FILES_TO_BUILD       += drv\can_drv.c
ifeq ($(GET_IMPLEMENTION_FROM), LIB)
CC_FILES_TO_BUILD       += 
else
CC_FILES_TO_BUILD       += Scc$(BSW_SRC_DIR)\Scc.c Scc$(BSW_SRC_DIR)\Scc_Priv.c Scc$(BSW_SRC_DIR)\Scc_Exi.c Scc$(BSW_SRC_DIR)\Scc_ExiRx_ISO.c Scc$(BSW_SRC_DIR)\Scc_ExiTx_ISO.c Scc$(BSW_SRC_DIR)\Scc_StateM_Vector.c Scc$(BSW_SRC_DIR)\Scc_ExiRx_ISO_Ed2.c Scc$(BSW_SRC_DIR)\Scc_ExiTx_ISO_Ed2.c
CC_FILES_TO_BUILD       += Scc$(BSW_SRC_DIR)\Scc_ExiRx_DIN.c Scc$(BSW_SRC_DIR)\Scc_ExiTx_DIN.c
endif
CPP_FILES_TO_BUILD      +=
ASM_FILES_TO_BUILD      +=

#LIBRARIES_LINK_ONLY     += (not yet supported)
#OBJECTS_LINK_ONLY       += (not yet supported)

#-------------------------------------------------------------------------------------------------
#only define new dirs, OBJ, LIB, LOG were created automaticly 
#-------------------------------------------------------------------------------------------------
DIRECTORIES_TO_CREATE   +=

#DEPEND_GCC_OPTS         += (not yet supported)

# e.g.:  GENERATED_SOURCE_FILES += $(GENDATA_DIR)\drv_par.c
GENERATED_SOURCE_FILES  += $(GENDATA_DIR)\Scc_Lcfg.c

#e.g.: COMMON_SOURCE_FILES     += $(GENDATA_DIR)\v_par.c
COMMON_SOURCE_FILES     += 

#-------------------------------------------------------------------------------------------------
# <project>.dep & <projekt>.lnk & <project>.bin and.....
# all in err\ & obj\ & lst\ & lib\ & log\ will be deleted by clean-rule automaticly
# so in this clean-rule it is only necessary to define additional files which
# were not delete automaticly.
# e.g.: $(<PATH>)\can_test.c
#-------------------------------------------------------------------------------------------------
MAKE_CLEAN_RULES        +=
#MAKE_GENERATE_RULES     +=
#MAKE_COMPILER_RULES     +=
#MAKE_DEBUG_RULES        +=
#MAKE_CONFIG_RULES       +=
#MAKE_ADD_RULES          +=


###############################################################
# REQUIRED   (defined in BaseMake (global.Makefile.target.make...))
#
# SSC_ROOT		(required)
# PROJECT_ROOT	(required)
#
# LIB_OUTPUT_PATH	(optional)
# OBJ_OUTPUT_PATH	(optional)
#
# OBJ_FILE_SUFFIX	
# LIB_FILE_SUFFIX
#
###############################################################


###############################################################
# PROVIDE   this Section can be used to define own additional rules
#
# In vendorx_can_cfg.mak:
# Please configure the project file:
#CAN_CONFIG_FILE = $(PROJECT_ROOT)\source\network\can\my_can_config.cfg

#In vendorx_can_config :
#generate_can_config:
#$(SSC_ROOT)\core\com\can\tools\canconfiggen.exe -o $(CAN_CONFIG_FILE)


###############################################################
# SPECIFIC
#
# There are no rules defined for the Specific part of the 
# Rules-Makefile. Each author is free to create temporary 
# variables or to use other resources of GNU-MAKE
#
###############################################################

