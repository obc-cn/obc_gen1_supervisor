/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc_ExiTx_ISO.c
 *        \brief  Smart Charging Communication Source Code File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *********************************************************************************************************************/

#define SCC_EXITX_ISO_SOURCE

/**********************************************************************************************************************
 LOCAL MISRA / PCLINT JUSTIFICATION
 **********************************************************************************************************************/
/* PRQA S 0777 EOF */ /* MD_MSR_Rule5.1 */


/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Scc_Cfg.h"

#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )

#include "Scc_Exi.h"
#include "Scc.h"
#include "Scc_Lcfg.h"
#include "Scc_Priv.h"
#include "Scc_Interface_Cfg.h"
#include "Scc_ConfigParams_Cfg.h"

#if ( SCC_DEV_ERROR_DETECT == STD_ON )
#include "Det.h"
#endif /* SCC_DEV_ERROR_DETECT */
#include "EthIf.h"
#include "Exi.h"
#include "IpBase.h"
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
#include "XmlSecurity.h"
#include "Csm.h"
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined (SCC_LOCAL)
# define SCC_LOCAL static
#endif

/**********************************************************************************************************************
 *  LOCAL / GLOBAL DATA
 *********************************************************************************************************************/
/* other variables */
#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

SCC_LOCAL P2VAR(Exi_ISO_V2G_MessageType, AUTOMATIC, SCC_VAR_NOINIT) Scc_ExiTx_ISO_MsgPtr;


#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3453 MACROS_FUNCTION_LIKE */ /* MD_MSR_FctLikeMacro */

/* PRQA L:MACROS_FUNCTION_LIKE */
/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_SessionSetupReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_SessionSetupReq(P2VAR(uint16, AUTOMATIC, SCC_VAR_NOINIT) BufIdxPtr);

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_ServiceDiscoveryReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_ServiceDiscoveryReq(P2VAR(uint16, AUTOMATIC, SCC_VAR_NOINIT) BufIdxPtr);

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_ServiceDetailReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_ServiceDetailReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_PaymentServiceSelectionReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_PaymentServiceSelectionReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_PaymentDetailsReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_PaymentDetailsReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_AuthorizationReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_AuthorizationReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_ChargeParameterDiscoveryReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_ChargeParameterDiscoveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_PowerDeliveryReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_PowerDeliveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_MeteringReceiptReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_MeteringReceiptReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_CertificateInstallationReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_CertificateInstallationReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_CertificateUpdateReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_CertificateUpdateReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_SessionStopReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_SessionStopReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

/* AC */

/* DC */
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_CableCheckReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_CableCheckReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_PreChargeReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_PreChargeReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_CurrentDemandReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_CurrentDemandReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_WeldingDetectionReq
 *********************************************************************************************************************/
/*! \brief          Write the content of the V2G message to the buffer.
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_WeldingDetectionReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

#endif /* SCC_CHARGING_DC */

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_WriteHeader
 *********************************************************************************************************************/
/*! \brief          Write the ISO header to the buffer
 *  \details        -
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_WriteHeader(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);

#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_WriteEVStatusDC
 *********************************************************************************************************************/
/*! \brief          Write the EVStatus to the buffer
 *  \details        -
 *  \param[out]     EVStatePtrPtr            Pointer to the EV Status
 *  \param[in]      BufIdxPtr                Pointer to the buffer index
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_WriteEVStatusDC(P2VAR(Exi_ISO_DC_EVStatusType, AUTOMATIC, SCC_VAR_NOINIT) *EVStatePtrPtr,
  P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr);
#endif /* SCC_CHARGING_DC */

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_Init
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_ExiTx_ISO_Init(void)
{
  /* #10 initialize the message pointer */
  Scc_ExiTx_ISO_MsgPtr = (P2VAR(Exi_ISO_V2G_MessageType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_StructBuf[0]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */

}

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_EncodeMessage
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiTx_ISO_EncodeMessage(
    P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr
)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  uint16         lBufIdx = 0;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Since no data has been sent, this means that the initializations has to be done */
  if(0u == Scc_TxDataSent)
  {
    /* #20 Initialize the exi workspace and check if it failed */
    if ( (Std_ReturnType)E_OK == Scc_Exi_InitEncodingWorkspace(PBufPtr) )
    {
      /* set the V2G_Message as root element */
      Scc_Exi_EncWs.InputData.RootElementId = EXI_ISO_V2G_MESSAGE_TYPE;
      /* initialize the header of the V2G messages */
      Scc_ExiTx_ISO_WriteHeader(&lBufIdx);
      /* create the body */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
      Scc_ExiTx_ISO_MsgPtr->Body = (P2VAR(Exi_ISO_BodyType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[lBufIdx];
      lBufIdx += (uint16)sizeof(Exi_ISO_BodyType);
      /* set the common body parameters */
      Scc_ExiTx_ISO_MsgPtr->Body->BodyElementFlag = 1;
      Scc_ExiTx_ISO_MsgPtr->Body->BodyElement = (P2VAR(Exi_ISO_BodyBaseType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[lBufIdx]; /* PRQA S 0314 */ /* MD_Scc_0310_0314_0316_3305 */

      /* #30 create request message and set MessageTimeout */
      switch ( Scc_MsgTrig )
      {
        /* check if a session setup request shall be sent */
      case Scc_MsgTrig_SessionSetup:
        Scc_ExiTx_ISO_SessionSetupReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_SessionSetupMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a service discovery request shall be sent */
      case Scc_MsgTrig_ServiceDiscovery:
        Scc_ExiTx_ISO_ServiceDiscoveryReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_ServiceDiscoveryMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a service detail request shall be sent */
      case Scc_MsgTrig_ServiceDetail:
        Scc_ExiTx_ISO_ServiceDetailReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_ServiceDetailMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a service payment selection request shall be sent */
      case Scc_MsgTrig_PaymentServiceSelection:
        Scc_ExiTx_ISO_PaymentServiceSelectionReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_PaymentServiceSelectionMessageTimeout;
        retVal = E_OK;
        break;

  #if ( SCC_ENABLE_PNC_CHARGING == STD_ON )

  #if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
        /* check if a certificate installation request shall be sent */
      case Scc_MsgTrig_CertificateInstallation:
        if ( (Std_ReturnType)E_OK == Scc_ExiTx_ISO_CertificateInstallationReq(&lBufIdx) )
        {
          Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_CertificateInstallationMessageTimeout;
          retVal = E_OK;
        }
        break;
  #endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

  #if ( SCC_ENABLE_CERTIFICATE_UPDATE == STD_ON )
        /* check if a certificate update request shall be sent */
      case Scc_MsgTrig_CertificateUpdate:
        if ( (Std_ReturnType)E_OK == Scc_ExiTx_ISO_CertificateUpdateReq(&lBufIdx) )
        {
          Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_CertificateUpdateMessageTimeout;
          retVal = E_OK;
        }
        break;
  #endif /* SCC_ENABLE_CERTIFICATE_UPDATE */

        /* check if a payment details request shall be sent */
      case Scc_MsgTrig_PaymentDetails:
        Scc_ExiTx_ISO_PaymentDetailsReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_PaymentDetailsMessageTimeout;
        retVal = E_OK;
        break;

  #endif /* SCC_ENABLE_PNC_CHARGING */

        /* check if a contract authentication request shall be sent */
      case Scc_MsgTrig_Authorization:
        if ( (Std_ReturnType)E_OK == Scc_ExiTx_ISO_AuthorizationReq(&lBufIdx) )
        {
          Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_AuthorizationMessageTimeout;
          retVal = E_OK;
        }
        break;

        /* check if a charge parameter discovery request shall be sent */
      case Scc_MsgTrig_ChargeParameterDiscovery:
        if ( E_OK == Scc_ExiTx_ISO_ChargeParameterDiscoveryReq(&lBufIdx) )
        {
          Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_ChargeParameterDiscoveryMessageTimeout;
          retVal = E_OK;
        }
        break;

        /* check if a power delivery request shall be sent */
      case Scc_MsgTrig_PowerDelivery:
        Scc_ExiTx_ISO_PowerDeliveryReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_PowerDeliveryMessageTimeout;
        retVal = E_OK;
        break;

  #if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
        /* check if a metering receipt request shall be sent */
      case Scc_MsgTrig_MeteringReceipt:
        if ( (Std_ReturnType)E_OK == Scc_ExiTx_ISO_MeteringReceiptReq(&lBufIdx) )
        {
          Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_MeteringReceiptMessageTimeout;
          retVal = E_OK;
        }
        break;
  #endif /* SCC_ENABLE_PNC_CHARGING */

        /* check if a session stop request shall be sent */
      case Scc_MsgTrig_SessionStop:
        Scc_ExiTx_ISO_SessionStopReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_SessionStopMessageTimeout;
        retVal = E_OK;
        break;


  #if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )

        /* check if a cable check request shall be sent */
      case Scc_MsgTrig_CableCheck:
        Scc_ExiTx_ISO_CableCheckReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_CableCheckMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a pre charge request shall be sent */
      case Scc_MsgTrig_PreCharge:
        Scc_ExiTx_ISO_PreChargeReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_PreChargeMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a current demand request shall be sent */
      case Scc_MsgTrig_CurrentDemand:
        Scc_ExiTx_ISO_CurrentDemandReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_CurrentDemandMessageTimeout;
        retVal = E_OK;
        break;

        /* check if a welding detection request shall be sent */
      case Scc_MsgTrig_WeldingDetection:
        Scc_ExiTx_ISO_WeldingDetectionReq(&lBufIdx);
        Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_ISO_WeldingDetectionMessageTimeout;
        retVal = E_OK;
        break;

  #endif /* SCC_CHARGING_DC */

      default:
        /* report a V2G error of the current trigger */
        Scc_ReportError(Scc_StackError_InvalidTxParameter);
        retVal = E_NOT_OK;
        break;
      }

      /* #40 encode and send the request message */
      if ( retVal == E_OK )
      {
        retVal = E_NOT_OK;

        if(E_OK == Scc_Exi_EncodeExiStream(SCC_V2GTP_HDR_PAYLOAD_TYPE_EXI, PBufPtr))
        {
          *BufLengthPtr = (uint16)Scc_TxDataSent;
          PBufPtr->totLen = *BufLengthPtr;
          PBufPtr->len    = *BufLengthPtr;

          /* provide sent data to application */
          {
            /* create and set the tx buffer pointer */
            Scc_TxRxBufferPointerType V2GRequest;
            V2GRequest.PbufPtr = PBufPtr;
            V2GRequest.FirstPart = TRUE;
            V2GRequest.StreamComplete = Scc_Exi_EncWs.EncWs.StreamComplete;
            /* provide the buffer to the application */
            Scc_Set_Core_V2GRequest(&V2GRequest);
          } /*lint !e550 */

          retVal = E_OK;
        }
      }
    }
  }
  else
  {
    /* #50 send the pending request message */
    retVal = Scc_Exi_SendSubsequentMessage(BufPtr, BufLengthPtr, SCC_V2GTP_HDR_PAYLOAD_TYPE_EXI, PBufPtr);
  }

  return retVal; /*lint !e438 */
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_SessionSetupReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_SessionSetupReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_SessionSetupReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_SessionSetupReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_SESSION_SETUP_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_SessionSetupReqType);

  /* EVCCID */ /* PRQA S 0310,3305 6 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVCCID = (P2VAR(Exi_ISO_evccIDType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_evccIDType);
  /* get the MAC address from EthIf */
  EthIf_GetPhysAddr((uint8)SCC_CTRL_IDX, &BodyPtr->EVCCID->Buffer[0]);
  BodyPtr->EVCCID->Length = 6;

}

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_ServiceDiscoveryReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_ServiceDiscoveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ServiceDiscoveryReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ServiceDiscoveryReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;

  boolean Flag = 0; /* PRQA S 2982 */ /* MD_Scc_2982 */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_SERVICE_DISCOVERY_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ServiceDiscoveryReqType);

  /* ServiceScope */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->ServiceScope = (P2VAR(Exi_ISO_serviceScopeType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_serviceScopeType);
  Scc_Get_ISO_ServiceScope(BodyPtr->ServiceScope, &Flag);
  BodyPtr->ServiceScopeFlag = Flag;

  /* ServiceCategory */
  Scc_Get_ISO_ServiceCategory(&BodyPtr->ServiceCategory, &Flag);
  BodyPtr->ServiceCategoryFlag = Flag;

}

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_ServiceDetailReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_ServiceDetailReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ServiceDetailReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ServiceDetailReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_SERVICE_DETAIL_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ServiceDetailReqType);

  /* ServiceID */
  Scc_Get_ISO_ServiceIDRx(&BodyPtr->ServiceID);

}

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_PaymentServiceSelectionReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_PaymentServiceSelectionReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_PaymentServiceSelectionReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_PaymentServiceSelectionReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_PAYMENT_SERVICE_SELECTION_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_PaymentServiceSelectionReqType);

  /* SelectedPaymentOption */
  Scc_Get_ISO_SelectedPaymentOption(&BodyPtr->SelectedPaymentOption);
  /* save the PaymentOption */
  Scc_Exi_PaymentOption = (Scc_ISO_PaymentOptionType)BodyPtr->SelectedPaymentOption; /* PRQA S 4322 */ /* MD_Scc_Exi_Generic */

  /* SelectedServiceList */
  Scc_Get_ISO_SelectedServiceListPtr(&BodyPtr->SelectedServiceList);

}

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )

#if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_CertificateInstallationReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_CertificateInstallationReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_CertificateInstallationReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_CertificateInstallationReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;

  uint8                  IdBuffer[6] = "ID_CI";
  XmlSecurity_ReturnType XmlSecRetVal;


  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_CERTIFICATE_INSTALLATION_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_CertificateInstallationReqType);

  /* OEMProvisioningCert */
  BodyPtr->OEMProvisioningCert = Scc_CertsWs.ProvCert;

  /* #20 Get the ListOfRootCertificateIDs */
  if ( (Std_ReturnType)E_OK == Scc_ExiTx_ISO_ExtractRootCertificateIDs() )
  {
    /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->ListOfRootCertificateIDs =
      (P2VAR(Exi_ISO_ListOfRootCertificateIDsType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_ListOfRootCertificateIDsType);
    BodyPtr->ListOfRootCertificateIDs->RootCertificateID = &Scc_CertsWs.RootCertIDs[0];

    /* create the attribute ID */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->Id = (P2VAR(Exi_ISO_AttributeIdType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_AttributeIdType);
    /* set the attribute ID */
    (void) IpBase_StrCpy(&BodyPtr->Id->Buffer[0], &IdBuffer[0]);
    BodyPtr->Id->Length = 5;

    /* #30 initialize XmlSecurity workspace */
    XmlSecRetVal = XmlSecurity_InitSigGenWorkspace(&Scc_Exi_XmlSecSigGenWs,
      &Scc_Exi_StructBuf[*BufIdxPtr], (uint16)(SCC_EXI_STRUCT_BUF_LEN-*BufIdxPtr),
      XmlSecurity_SAT_ECDSA_SHA256, XmlSecurity_CAT_EXI);
    if ( XmlSec_RetVal_OK != XmlSecRetVal )
    {
      /* report error to application */
      Scc_ReportError(Scc_StackError_XmlSecurity);
    }
    else
    {
      /* set the pointer for the signature */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
      Scc_ExiTx_ISO_MsgPtr->Header->Signature = (P2VAR(Exi_XMLSIG_SignatureType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
      Scc_ExiTx_ISO_MsgPtr->Header->SignatureFlag = 1;

      /* #40 Add EXI references */
      /* PRQA S 0316 9 */ /* MD_Scc_0310_0314_0316_3305 */
      XmlSecRetVal = XmlSecurity_AddExiReference(&Scc_Exi_XmlSecSigGenWs,
        &Scc_Exi_TempBuf[0],
        (P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement,
        (P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->Id->Buffer[0],
        Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId,
        SCC_EXI_TEMP_BUF_LEN,
        BodyPtr->Id->Length,
        (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_TYPE,
        Scc_SHA256JobId);

      if ( XmlSec_RetVal_OK != XmlSecRetVal )
      {
        /* report error to application */
        Scc_ReportError(Scc_StackError_XmlSecurity);
      }
      else
      {
        /* #50 generate the signature */
        XmlSecRetVal = XmlSecurity_GenerateSignature(&Scc_Exi_XmlSecSigGenWs,
          &Scc_Exi_TempBuf[0],
          SCC_EXI_TEMP_BUF_LEN,
          Scc_ECDSASignProvCertKeyId,
          Scc_ECDSASignProvCertJobId);

        if ( XmlSec_RetVal_OK != XmlSecRetVal )
        {
          /* report error to application */
          Scc_ReportError(Scc_StackError_XmlSecurity);
        }
        else
        {
          retVal = E_OK;
        }
      }
    }
  }

  return retVal;
}
#endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

#if ( SCC_ENABLE_CERTIFICATE_UPDATE == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_CertificateUpdateReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_CertificateUpdateReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_CertificateUpdateReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_CertificateUpdateReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;

  uint8                  IdBuffer[6] = "ID_CU";
  XmlSecurity_ReturnType XmlSecRetVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_CERTIFICATE_UPDATE_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_CertificateUpdateReqType);

  /* ContractSignatureCertChain -> Certificate */
  if ( 0 <= Scc_CertsWs.ContrCertChainSize )
  {
    /* #20 Set the Contract Certificate */
    /* create the buffer for the contract certificate chain */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->ContractSignatureCertChain =
      (P2VAR(Exi_ISO_CertificateChainType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_CertificateChainType);
    /* set the Id attribute flag to false */
    BodyPtr->ContractSignatureCertChain->IdFlag = 0;
    /* set the contract certificate */
    BodyPtr->ContractSignatureCertChain->Certificate = Scc_CertsWs.ContrCert;

    /* ContractSignatureCertChain -> SubCertificates */
    if ( 0 < Scc_CertsWs.ContrCertChainSize )
    {
      /* create the SubCertificates element */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
      BodyPtr->ContractSignatureCertChain->SubCertificates =
        (P2VAR(Exi_ISO_SubCertificatesType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_SubCertificatesType);
      /* set the certificates pointer */
      BodyPtr->ContractSignatureCertChain->SubCertificates->Certificate = &Scc_CertsWs.ContrSubCerts[0];
      /* set the sub certificate flag to true */
      BodyPtr->ContractSignatureCertChain->SubCertificatesFlag = 1;
    }
    else
    {
      BodyPtr->ContractSignatureCertChain->SubCertificatesFlag = 0;
    }

    /* eMAID */
    BodyPtr->eMAID = Scc_CertsWs.eMAID;

    /* #30 get the ListOfRootCertificateIDs */
    if ( (Std_ReturnType)E_OK == Scc_ExiTx_ISO_ExtractRootCertificateIDs() )
    {
      /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
      BodyPtr->ListOfRootCertificateIDs =
        (P2VAR(Exi_ISO_ListOfRootCertificateIDsType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_ListOfRootCertificateIDsType);
      BodyPtr->ListOfRootCertificateIDs->RootCertificateID = &Scc_CertsWs.RootCertIDs[0];

      /* create the attribute ID */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
      BodyPtr->Id = (P2VAR(Exi_ISO_AttributeIdType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_AttributeIdType);
      /* set the attribute ID */
      (void) IpBase_StrCpy(&BodyPtr->Id->Buffer[0], &IdBuffer[0]);
      BodyPtr->Id->Length = 5;

      /* #40 initialize XmlSecurity workspace */
      XmlSecRetVal = XmlSecurity_InitSigGenWorkspace(&Scc_Exi_XmlSecSigGenWs,
        &Scc_Exi_StructBuf[*BufIdxPtr], (uint16)(SCC_EXI_STRUCT_BUF_LEN-*BufIdxPtr),
        XmlSecurity_SAT_ECDSA_SHA256, XmlSecurity_CAT_EXI);
      if ( XmlSec_RetVal_OK != XmlSecRetVal )
      {
        /* report error to application */
        Scc_ReportError(Scc_StackError_XmlSecurity);
      }
      else
      {
        /* set the pointer for the signature */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
        Scc_ExiTx_ISO_MsgPtr->Header->Signature = (P2VAR(Exi_XMLSIG_SignatureType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
        Scc_ExiTx_ISO_MsgPtr->Header->SignatureFlag = 1;

        /* #50 add EXI references */
        /* PRQA S 0316 9 */ /* MD_Scc_0310_0314_0316_3305 */
        XmlSecRetVal = XmlSecurity_AddExiReference(&Scc_Exi_XmlSecSigGenWs,
          &Scc_Exi_TempBuf[0],
          (P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement,
          (P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->Id->Buffer[0],
          Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId,
          SCC_EXI_TEMP_BUF_LEN,
           BodyPtr->Id->Length,
           (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_TYPE,
           Scc_SHA256JobId);

        if ( XmlSec_RetVal_OK != XmlSecRetVal )
        {
          /* report the error to the application */
          Scc_ReportError(Scc_StackError_XmlSecurity);
        }
        else
        {
          /* #60 generate the signature */
          XmlSecRetVal = XmlSecurity_GenerateSignature(&Scc_Exi_XmlSecSigGenWs,
            &Scc_Exi_TempBuf[0],
            SCC_EXI_TEMP_BUF_LEN,
            Scc_ECDSASignCertKeyIds[Scc_CertsWs.ChosenContrCertChainIdx],
            Scc_ECDSASignCertJobIds[Scc_CertsWs.ChosenContrCertChainIdx]);

          if ( XmlSec_RetVal_OK != XmlSecRetVal )
          {
            /* report the error to the application */
            Scc_ReportError(Scc_StackError_XmlSecurity);
          }
          else
          {
            retVal = E_OK;
          }
        }
      }
    }
  }
  /* no contract certificate available */
  else
  {
    Scc_ReportError(Scc_StackError_NvM);
  }

  return retVal;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */
#endif /* SCC_ENABLE_CERTIFICATE_UPDATE */

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_PaymentDetailsReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_PaymentDetailsReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_PaymentDetailsReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_PaymentDetailsReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_PAYMENT_DETAILS_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_PaymentDetailsReqType);

  /* eMAID */
  BodyPtr->eMAID = Scc_CertsWs.eMAID;

  /* ContractSignatureCertChain -> Certificate */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->ContractSignatureCertChain =
    (P2VAR(Exi_ISO_CertificateChainType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_CertificateChainType);
  BodyPtr->ContractSignatureCertChain->Certificate = Scc_CertsWs.ContrCert;
  BodyPtr->ContractSignatureCertChain->IdFlag = 0;

  /* ContractSignatureCertChain -> SubCertificates */
  if ( 0 < Scc_CertsWs.ContrCertChainSize )
  {
    /* create the SubCertificates element */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->ContractSignatureCertChain->SubCertificates =
      (P2VAR(Exi_ISO_SubCertificatesType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_SubCertificatesType);
    /* set the certificates pointer */
    BodyPtr->ContractSignatureCertChain->SubCertificates->Certificate = &Scc_CertsWs.ContrSubCerts[0];

    /* set the sub certificate flag to true */
    BodyPtr->ContractSignatureCertChain->SubCertificatesFlag = 1;
  }
  else
  {
    BodyPtr->ContractSignatureCertChain->SubCertificatesFlag = 0;
  }

}

#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_AuthorizationReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_AuthorizationReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_AuthorizationReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_AuthorizationReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  XmlSecurity_ReturnType XmlSecRetVal;
  uint8                  IdBuffer[6] = "ID_CA";
#endif /* SCC_ENABLE_PNC_CHARGING */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_AUTHORIZATION_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_AuthorizationReqType);

  /* GenChallenge */

  if (   ( SCC_ISO_PAYMENT_OPTION_TYPE_CONTRACT == Scc_Exi_PaymentOption )
      && ( Scc_MsgStatus_Authorization_OK != Scc_MsgStatus ))
  {
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
    /* set the GenChallenge */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->GenChallenge = (P2VAR(Exi_ISO_genChallengeType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[0];
    BodyPtr->GenChallengeFlag = TRUE;

    /* create the attribute ID */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->Id = (P2VAR(Exi_ISO_AttributeIdType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_AttributeIdType);
    /* set the attribute ID */
    (void) IpBase_StrCpy(&BodyPtr->Id->Buffer[0], &IdBuffer[0]);
    BodyPtr->Id->Length = 5;
    BodyPtr->IdFlag = TRUE;

    /* #20 Initialize XmlSecurity workspace */
    XmlSecRetVal = XmlSecurity_InitSigGenWorkspace(&Scc_Exi_XmlSecSigGenWs,
      &Scc_Exi_StructBuf[*BufIdxPtr], (uint16)(SCC_EXI_STRUCT_BUF_LEN-*BufIdxPtr),
      XmlSecurity_SAT_ECDSA_SHA256, XmlSecurity_CAT_EXI);
    if ( XmlSec_RetVal_OK != XmlSecRetVal )
    {
      /* report the error to the application */
      Scc_ReportError(Scc_StackError_XmlSecurity);
    }
    else
    {
      /* set the pointer for the signature */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
      Scc_ExiTx_ISO_MsgPtr->Header->Signature = (P2VAR(Exi_XMLSIG_SignatureType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
      Scc_ExiTx_ISO_MsgPtr->Header->SignatureFlag = 1;

      /* #30 Add EXI references */
      /* PRQA S 0316 9 */ /* MD_Scc_0310_0314_0316_3305 */
      XmlSecRetVal = XmlSecurity_AddExiReference(&Scc_Exi_XmlSecSigGenWs,
        &Scc_Exi_TempBuf[Scc_Exi_TempBufPos],
        (P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement,
        (P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->Id->Buffer[0],
        Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId,
        (uint16)(SCC_EXI_TEMP_BUF_LEN-Scc_Exi_TempBufPos),
        BodyPtr->Id->Length,
        (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_TYPE,
        Scc_SHA256JobId);

      if ( XmlSec_RetVal_OK != XmlSecRetVal )
      {
        /* report the error to the application */
        Scc_ReportError(Scc_StackError_XmlSecurity);
      }
      else
      {
        /* #40 generate the signature */
      XmlSecRetVal = XmlSecurity_GenerateSignature(&Scc_Exi_XmlSecSigGenWs,
        &Scc_Exi_TempBuf[Scc_Exi_TempBufPos],
        (uint16)(SCC_EXI_TEMP_BUF_LEN-Scc_Exi_TempBufPos),
        Scc_ECDSASignCertKeyIds[Scc_CertsWs.ChosenContrCertChainIdx],
        Scc_ECDSASignCertJobIds[Scc_CertsWs.ChosenContrCertChainIdx]);

        if ( XmlSec_RetVal_OK != XmlSecRetVal )
        {
          /* report the error to the application */
          Scc_ReportError(Scc_StackError_XmlSecurity);
        }
        else
        {
          retVal = E_OK;
        }
      }
    }
#endif /* SCC_ENABLE_PNC_CHARGING */
  }
  else
  {
    BodyPtr->IdFlag = FALSE;
    BodyPtr->GenChallengeFlag = FALSE;
    retVal = E_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_ChargeParameterDiscoveryReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_ChargeParameterDiscoveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_ChargeParameterDiscoveryReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_ChargeParameterDiscoveryReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
  P2VAR(Exi_ISO_DC_EVChargeParameterType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVChargeParameterTypePtr;
#endif /* SCC_CHARGING_DC */

  boolean        Flag = 0; /* PRQA S 2982 */ /* MD_Scc_2982 */
  Std_ReturnType retVal = E_OK;
  Scc_PhysicalValueType scc_PhysicalValueTmp;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_CHARGE_PARAMETER_DISCOVERY_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_ChargeParameterDiscoveryReqType);

  /* MaxEntriesSAScheduleTuple */
  Scc_Get_ISO_MaxEntriesSAScheduleTuple(&BodyPtr->MaxEntriesSAScheduleTuple, &Flag);
  BodyPtr->MaxEntriesSAScheduleTupleFlag = Flag;

  /* RequestedEnergyTransferMode */
  Scc_Get_ISO_RequestedEnergyTransferMode(&BodyPtr->RequestedEnergyTransferMode);


#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
  {
    /* #30 set the EVChargeParameter pointer for DC Charging */
    BodyPtr->EVChargeParameter =
      (P2VAR(Exi_ISO_EVChargeParameterType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_DC_EVChargeParameterType);
    BodyPtr->EVChargeParameterElementId =
      EXI_ISO_DC_EVCHARGE_PARAMETER_TYPE;

    /* get the pointer to the Exi_ISO_AC_EVChargeParameterType */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
    DC_EVChargeParameterTypePtr = (P2VAR(Exi_ISO_DC_EVChargeParameterType, AUTOMATIC, SCC_APPL_DATA))(BodyPtr->EVChargeParameter);
    /* DC_EVChargeParameter -> DepartureTime */
    Scc_Get_ISO_DepartureTime(&DC_EVChargeParameterTypePtr->DepartureTime, &Flag);
    DC_EVChargeParameterTypePtr->DepartureTimeFlag = Flag;

    /* DC_EVChargeParameter -> DC_EVStatus*/
    Scc_ExiTx_ISO_WriteEVStatusDC(&DC_EVChargeParameterTypePtr->DC_EVStatus, BufIdxPtr);

    /* DC_EVChargeParameter -> EVMaximumCurrentLimit */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    DC_EVChargeParameterTypePtr->EVMaximumCurrentLimit =
      (P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_PhysicalValueType);
    DC_EVChargeParameterTypePtr->EVMaximumCurrentLimit->Unit = EXI_ISO_UNIT_SYMBOL_TYPE_A;
    Scc_Get_ISO_DC_EVMaximumCurrentLimit(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc2ISO_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterTypePtr->EVMaximumCurrentLimit);
    /* parameter is not optional for this message, check if it was set */
    if ( TRUE != Flag )
    {
      Scc_ReportError(Scc_StackError_InvalidTxParameter);
      retVal = E_NOT_OK;
    }

    /* DC_EVChargeParameter -> EVMaximumPowerLimit */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    DC_EVChargeParameterTypePtr->EVMaximumPowerLimit =
      (P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_PhysicalValueType);
    DC_EVChargeParameterTypePtr->EVMaximumPowerLimit->Unit = EXI_ISO_UNIT_SYMBOL_TYPE_W;
    Scc_Get_ISO_DC_EVMaximumPowerLimit(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc2ISO_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterTypePtr->EVMaximumPowerLimit);
    DC_EVChargeParameterTypePtr->EVMaximumPowerLimitFlag = Flag;

    /* DC_EVChargeParameter -> EVMaximumVoltageLimit */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    DC_EVChargeParameterTypePtr->EVMaximumVoltageLimit =
      (P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_PhysicalValueType);
    DC_EVChargeParameterTypePtr->EVMaximumVoltageLimit->Unit = EXI_ISO_UNIT_SYMBOL_TYPE_V;
    Scc_Get_ISO_DC_EVMaximumVoltageLimit(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc2ISO_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterTypePtr->EVMaximumVoltageLimit);
    /* parameter is not optional for this message, check if it was set */
    if ( TRUE != Flag )
    {
      Scc_ReportError(Scc_StackError_InvalidTxParameter);
      retVal = E_NOT_OK;
    }

    /* EVEnergyCapacity */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    DC_EVChargeParameterTypePtr->EVEnergyCapacity =
      (P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_PhysicalValueType);
    DC_EVChargeParameterTypePtr->EVEnergyCapacity->Unit = EXI_ISO_UNIT_SYMBOL_TYPE_WH;
    Scc_Get_ISO_DC_EVEnergyCapacity(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc2ISO_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterTypePtr->EVEnergyCapacity);
    DC_EVChargeParameterTypePtr->EVEnergyCapacityFlag = Flag;

    /* EVEnergyRequest */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    DC_EVChargeParameterTypePtr->EVEnergyRequest =
      (P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += (uint16)sizeof(Exi_ISO_PhysicalValueType);
    DC_EVChargeParameterTypePtr->EVEnergyRequest->Unit = EXI_ISO_UNIT_SYMBOL_TYPE_WH;
    Scc_Get_ISO_DC_EVEnergyRequest(&scc_PhysicalValueTmp, &Flag);
    Scc_Conv_Scc2ISO_PhysicalValue(&scc_PhysicalValueTmp, DC_EVChargeParameterTypePtr->EVEnergyRequest);
    DC_EVChargeParameterTypePtr->EVEnergyRequestFlag = Flag;

    /* FullSOC */
    Scc_Get_ISO_DC_FullSOC(&DC_EVChargeParameterTypePtr->FullSOC, &Flag);
    DC_EVChargeParameterTypePtr->FullSOCFlag = Flag;

    /* BulkSOC */
    Scc_Get_ISO_DC_BulkSOC(&DC_EVChargeParameterTypePtr->BulkSOC, &Flag);
    DC_EVChargeParameterTypePtr->BulkSOCFlag = Flag;

  }
#endif /* SCC_CHARGING_DC */

  return retVal;

} /* PRQA S 6010,6030,6050 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL */

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_PowerDeliveryReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_PowerDeliveryReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_PowerDeliveryReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_PowerDeliveryReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
  P2VAR(Exi_ISO_DC_EVPowerDeliveryParameterType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVPowerDeliveryParameter;
#endif /* SCC_CHARGING_DC */

  boolean Flag = 0;
#if ( defined SCC_ENABLE_INTERNAL_CHARGING_PROFILE_BUFFER ) && ( SCC_ENABLE_INTERNAL_CHARGING_PROFILE_BUFFER == STD_ON )
  Scc_BufferPointerType ChargingProfileBufPtr;
#endif /* SCC_ENABLE_INTERNAL_CHARGING_PROFILE_BUFFER */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_POWER_DELIVERY_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_PowerDeliveryReqType);

  /* ChargeProgress */
  Scc_Get_ISO_ChargeProgress(&BodyPtr->ChargeProgress);

  /* SAScheduleTupleID */
  Scc_Get_ISO_SAScheduleTupleID(&BodyPtr->SAScheduleTupleID);
  Scc_Exi_SAScheduleTupleID = BodyPtr->SAScheduleTupleID;

  /* ChargingProfile */
#if ( defined SCC_ENABLE_INTERNAL_CHARGING_PROFILE_BUFFER ) && ( SCC_ENABLE_INTERNAL_CHARGING_PROFILE_BUFFER == STD_ON )
    /* set the buffer pointer */
  ChargingProfileBufPtr.BufferPtr = &Scc_Exi_StructBuf[*BufIdxPtr];

  /* Set the BufferLen to the maximum length that can be send for the ChargingProfile */
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
  ChargingProfileBufPtr.BufferLen =
    ((uint16)SCC_EXI_STRUCT_BUF_LEN - *BufIdxPtr - (uint16)sizeof(Exi_ISO_DC_EVPowerDeliveryParameterType) - (uint16)sizeof(Exi_ISO_DC_EVStatusType) );
#else
  ChargingProfileBufPtr.BufferLen = (uint16)( SCC_EXI_STRUCT_BUF_LEN - *BufIdxPtr );
#endif /* SCC_CHARGING_DC */

    /* get the charging profile */
  Scc_Get_ISO_ChargingProfilePtr(&ChargingProfileBufPtr, &Flag);
    /* check if the charging profile shall be used */
  if ( 1u == Flag )
  {
    /* set the ChargingProfile of the application */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
    BodyPtr->ChargingProfile = (P2VAR(Exi_ISO_ChargingProfileType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    *BufIdxPtr += ChargingProfileBufPtr.BufferLen;
    if ( *BufIdxPtr > ( SCC_EXI_STRUCT_BUF_LEN - sizeof(Exi_ISO_DC_EVPowerDeliveryParameterType) - sizeof(Exi_ISO_DC_EVStatusType) ))
    {
      /* The ChargingProfile is larger than the internal buffer of Scc_Exi_StructBuf */
#if(SCC_DEV_ERROR_REPORT == STD_ON)
      ( void )Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_POWER_DELIVERY_REQ, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
    }
  }
#else
    /* get the charging profile */
  Scc_Get_ISO_ChargingProfilePtr(&BodyPtr->ChargingProfile, &Flag);
#endif /* SCC_ENABLE_INTERNAL_CHARGING_PROFILE_BUFFER */
  /* set the flag of the exi struct */
  BodyPtr->ChargingProfileFlag = (Exi_BitType)Flag;

  /* set the flag for the EVPowerDeliveryParameter element to 0 */
  BodyPtr->EVPowerDeliveryParameterFlag = 0;

#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
  {
    /* check if the EVPowerDeliveryParameter shall be sent */
    Scc_Get_ISO_DC_EVPowerDeliveryParameterFlag(&Flag);
    if ( TRUE == Flag )
    {
      /* get the pointer to the Exi_ISO_DC_EVPowerDeliveryParameterType */ /* PRQA S 0314 2 */ /* MD_Scc_Exi_Generic */
      BodyPtr->EVPowerDeliveryParameter =
        (P2VAR(Exi_ISO_EVPowerDeliveryParameterType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
      *BufIdxPtr += (uint16)sizeof(Exi_ISO_DC_EVPowerDeliveryParameterType);
      /* PRQA S 0316 2 */ /* MD_Scc_Exi_Generic */
      DC_EVPowerDeliveryParameter =
        (P2VAR(Exi_ISO_DC_EVPowerDeliveryParameterType, AUTOMATIC, SCC_APPL_DATA))(BodyPtr->EVPowerDeliveryParameter);

      /* DC_EVPowerDeliveryParameter */
      BodyPtr->EVPowerDeliveryParameterElementId = EXI_ISO_DC_EVPOWER_DELIVERY_PARAMETER_TYPE;
      BodyPtr->EVPowerDeliveryParameterFlag = 1;

      /* DC_EVPowerDeliveryParameter -> DC_EVStatus */
      Scc_ExiTx_ISO_WriteEVStatusDC(&DC_EVPowerDeliveryParameter->DC_EVStatus, BufIdxPtr);

      /* DC_EVPowerDeliveryParameter -> BulkChargingComplete */
      Scc_Get_ISO_DC_BulkChargingComplete(&DC_EVPowerDeliveryParameter->BulkChargingComplete, &Flag);
      DC_EVPowerDeliveryParameter->BulkChargingCompleteFlag = Flag;

      /* DC_EVPowerDeliveryParameter -> ChargingComplete */
      Scc_Get_ISO_DC_ChargingComplete(&DC_EVPowerDeliveryParameter->ChargingComplete);
    }
  }
#endif /* SCC_CHARGING_DC */

} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_MeteringReceiptReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiTx_ISO_MeteringReceiptReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_MeteringReceiptReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_MeteringReceiptReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;

  XmlSecurity_ReturnType XmlSecRetVal;
  uint8                  IdBuffer[6] = "ID_MR";

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_METERING_RECEIPT_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_MeteringReceiptReqType);

  /* SessionID */
  /* get the buffer for the SessionID */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305*/
  BodyPtr->SessionID = (P2VAR(Exi_ISO_sessionIDType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_sessionIDType);
  /* copy the SessionID to the buffer */ /* PRQA S 0310,3305,0315 3 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
  IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->SessionID->Buffer[0],
    (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &Scc_SessionIDNvm[1], Scc_SessionIDNvm[0]);
  /* set the length of the SessionID */
  BodyPtr->SessionID->Length = Scc_SessionIDNvm[0];

  /* SAScheduleTupleID */
  BodyPtr->SAScheduleTupleID = Scc_Exi_SAScheduleTupleID;
  BodyPtr->SAScheduleTupleIDFlag = 1;

  /* MeterInfo */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->MeterInfo = (P2VAR(Exi_ISO_MeterInfoType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[0];

  /* create the attribute ID */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->Id = (P2VAR(Exi_ISO_AttributeIdType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_AttributeIdType);
  /* set the attribute ID */
  (void) IpBase_StrCpy(&BodyPtr->Id->Buffer[0], &IdBuffer[0]);
  BodyPtr->Id->Length = 5;
  BodyPtr->IdFlag = 1;

  /* #20 Initialize XmlSecurity workspace */
  XmlSecRetVal = XmlSecurity_InitSigGenWorkspace(&Scc_Exi_XmlSecSigGenWs,
    &Scc_Exi_StructBuf[*BufIdxPtr], (uint16)(SCC_EXI_STRUCT_BUF_LEN-*BufIdxPtr),
    XmlSecurity_SAT_ECDSA_SHA256, XmlSecurity_CAT_EXI);
  if ( XmlSec_RetVal_OK != XmlSecRetVal )
  {
    /* report the error to the application */
    Scc_ReportError(Scc_StackError_XmlSecurity);
  }
  else
  {
    /* set the pointer for the signature */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
    Scc_ExiTx_ISO_MsgPtr->Header->Signature = (P2VAR(Exi_XMLSIG_SignatureType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[*BufIdxPtr];
    Scc_ExiTx_ISO_MsgPtr->Header->SignatureFlag = 1;

    /* #30 add EXI references */
    /* PRQA S 0316 9 */ /* MD_Scc_0310_0314_0316_3305 */
    XmlSecRetVal = XmlSecurity_AddExiReference(&Scc_Exi_XmlSecSigGenWs,
      &Scc_Exi_TempBuf[Scc_Exi_TempBufPos],
      (P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement,
      (P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->Id->Buffer[0],
      Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId,
      (uint16)(SCC_EXI_TEMP_BUF_LEN-Scc_Exi_TempBufPos),
      BodyPtr->Id->Length,
      (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_TYPE,
      Scc_SHA256JobId);

    if ( XmlSec_RetVal_OK != XmlSecRetVal )
    {
      /* report the error to the application */
      Scc_ReportError(Scc_StackError_XmlSecurity);
    }
    else
    {
      /* #40 generate the signature */
      XmlSecRetVal = XmlSecurity_GenerateSignature(&Scc_Exi_XmlSecSigGenWs,
        &Scc_Exi_TempBuf[Scc_Exi_TempBufPos],
        (uint16)(SCC_EXI_TEMP_BUF_LEN-Scc_Exi_TempBufPos),
        Scc_ECDSASignCertKeyIds[Scc_CertsWs.ChosenContrCertChainIdx],
        Scc_ECDSASignCertJobIds[Scc_CertsWs.ChosenContrCertChainIdx]);

      if ( XmlSec_RetVal_OK != XmlSecRetVal )
      {
        /* report the error to the application */
        Scc_ReportError(Scc_StackError_XmlSecurity);
      }
      else
      {
        retVal = E_OK;
      }
    }
  }

  return retVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_SessionStopReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_SessionStopReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_SessionStopReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_SessionStopReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_SESSION_STOP_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_SessionStopReqType);

  /* ChargingSession */
  Scc_Get_ISO_ChargingSession((P2VAR(Scc_ISO_ChargingSessionType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_ChargingSession);
  BodyPtr->ChargingSession = (Exi_ISO_chargingSessionType)Scc_Exi_ChargingSession; /* PRQA S 4322 */ /* MD_Scc_Exi_Generic */
}


#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_CableCheckReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_CableCheckReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_CableCheckReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_CableCheckReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_CABLE_CHECK_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_CableCheckReqType);

  /* DC_EVStatus */
  Scc_ExiTx_ISO_WriteEVStatusDC(&BodyPtr->DC_EVStatus, BufIdxPtr);

}

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_PreChargeReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_PreChargeReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_PhysicalValueType scc_PhysicalValueTmp;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_PreChargeReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_PreChargeReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_PRE_CHARGE_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_PreChargeReqType);

  /* DC_EVStatus */
  Scc_ExiTx_ISO_WriteEVStatusDC(&BodyPtr->DC_EVStatus, BufIdxPtr);

  /* EVTargetVoltage */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVTargetVoltage = (P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_PhysicalValueType);
  BodyPtr->EVTargetVoltage->Unit = EXI_ISO_UNIT_SYMBOL_TYPE_V;
  Scc_Get_ISO_DC_EVTargetVoltage(&scc_PhysicalValueTmp);
  Scc_Conv_Scc2ISO_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVTargetVoltage);

  /* EVTargetCurrent */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVTargetCurrent = (P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_PhysicalValueType);
  BodyPtr->EVTargetCurrent->Unit = EXI_ISO_UNIT_SYMBOL_TYPE_A;
  Scc_Get_ISO_DC_EVTargetCurrent(&scc_PhysicalValueTmp);
  Scc_Conv_Scc2ISO_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVTargetCurrent);

}

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_CurrentDemandReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_CurrentDemandReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_CurrentDemandReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_CurrentDemandReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;

  boolean Flag = 0; /* PRQA S 2982 */ /* MD_Scc_2982 */
  Scc_PhysicalValueType scc_PhysicalValueTmp;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_CURRENT_DEMAND_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_CurrentDemandReqType);

  /* DC_EVStatus */
  Scc_ExiTx_ISO_WriteEVStatusDC(&BodyPtr->DC_EVStatus, BufIdxPtr);

  /* EVTargetCurrent */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVTargetCurrent = (P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_PhysicalValueType);
  BodyPtr->EVTargetCurrent->Unit = EXI_ISO_UNIT_SYMBOL_TYPE_A;
  Scc_Get_ISO_DC_EVTargetCurrent(&scc_PhysicalValueTmp);
  Scc_Conv_Scc2ISO_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVTargetCurrent);

  /* EVMaximumVoltageLimit */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVMaximumVoltageLimit = (P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_PhysicalValueType);
  BodyPtr->EVMaximumVoltageLimit->Unit = EXI_ISO_UNIT_SYMBOL_TYPE_V;
  Scc_Get_ISO_DC_EVMaximumVoltageLimit(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2ISO_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVMaximumVoltageLimit);
  BodyPtr->EVMaximumVoltageLimitFlag = Flag;

  /* EVMaximumCurrentLimit */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVMaximumCurrentLimit = (P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_PhysicalValueType);
  BodyPtr->EVMaximumCurrentLimit->Unit = EXI_ISO_UNIT_SYMBOL_TYPE_A;
  Scc_Get_ISO_DC_EVMaximumCurrentLimit(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2ISO_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVMaximumCurrentLimit);
  BodyPtr->EVMaximumCurrentLimitFlag = Flag;

  /* EVMaximumPowerLimit */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVMaximumPowerLimit = (P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_PhysicalValueType);
  BodyPtr->EVMaximumPowerLimit->Unit = EXI_ISO_UNIT_SYMBOL_TYPE_W;
  Scc_Get_ISO_DC_EVMaximumPowerLimit(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2ISO_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVMaximumPowerLimit);
  BodyPtr->EVMaximumPowerLimitFlag = Flag;

  /* BulkChargingComplete */
  Scc_Get_ISO_DC_BulkChargingComplete(&BodyPtr->BulkChargingComplete, &Flag);
  BodyPtr->BulkChargingCompleteFlag = Flag;

  /* ChargingComplete */
  Scc_Get_ISO_DC_ChargingComplete(&BodyPtr->ChargingComplete);

  /* RemainingTimeToFullSoC */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->RemainingTimeToFullSoC = (P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_PhysicalValueType);
  BodyPtr->RemainingTimeToFullSoC->Unit = EXI_ISO_UNIT_SYMBOL_TYPE_S;
  Scc_Get_ISO_DC_RemainingTimeToFullSoC(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2ISO_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->RemainingTimeToFullSoC);
  BodyPtr->RemainingTimeToFullSoCFlag = Flag;

  /* RemainingTimeToBulkSoC */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->RemainingTimeToBulkSoC = (P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_PhysicalValueType);
  BodyPtr->RemainingTimeToBulkSoC->Unit = EXI_ISO_UNIT_SYMBOL_TYPE_S;
  Scc_Get_ISO_DC_RemainingTimeToBulkSoC(&scc_PhysicalValueTmp, &Flag);
  Scc_Conv_Scc2ISO_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->RemainingTimeToBulkSoC);
  BodyPtr->RemainingTimeToBulkSoCFlag = Flag;

  /* EVTargetVoltage */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  BodyPtr->EVTargetVoltage = (P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_PhysicalValueType);
  BodyPtr->EVTargetVoltage->Unit = EXI_ISO_UNIT_SYMBOL_TYPE_V;
  Scc_Get_ISO_DC_EVTargetVoltage(&scc_PhysicalValueTmp);
  Scc_Conv_Scc2ISO_PhysicalValue(&scc_PhysicalValueTmp, BodyPtr->EVTargetVoltage);

} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_WeldingDetectionReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_WeldingDetectionReq(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_ISO_WeldingDetectionReqType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2VAR(Exi_ISO_WeldingDetectionReqType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiTx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Fill the EXI structure with parameter values */
  Scc_ExiTx_ISO_MsgPtr->Body->BodyElementElementId = EXI_ISO_WELDING_DETECTION_REQ_TYPE;
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_WeldingDetectionReqType);

  /* DC_EVStatus */
  Scc_ExiTx_ISO_WriteEVStatusDC(&BodyPtr->DC_EVStatus, BufIdxPtr);

}

#endif /* SCC_CHARGING_DC */

/**********************************************************************************************************************
 *  LOCAL HELPER FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_WriteHeader
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_WriteHeader(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 set the buffer for this new V2G message */
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_V2G_MessageType);
  /* set the buffer of the V2G message header */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
  Scc_ExiTx_ISO_MsgPtr->Header = (P2VAR(Exi_ISO_MessageHeaderType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_MessageHeaderType);
  Scc_ExiTx_ISO_MsgPtr->Header->NotificationFlag = 0;
  Scc_ExiTx_ISO_MsgPtr->Header->SignatureFlag = 0;
  /* set the buffer of the SessionID */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
  Scc_ExiTx_ISO_MsgPtr->Header->SessionID = (P2VAR(Exi_ISO_sessionIDType, AUTOMATIC, SCC_APPL_DATA)) &Scc_Exi_StructBuf[*BufIdxPtr];
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_sessionIDType);
  Scc_ExiTx_ISO_MsgPtr->Header->SessionID->Length = Scc_SessionIDNvm[0];
  /* copy the SessionID to the buffer */ /* PRQA S 0310, 3305, 0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
  IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &Scc_ExiTx_ISO_MsgPtr->Header->SessionID->Buffer[0],
    (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &Scc_SessionIDNvm[1], (uint32)Scc_SessionIDNvm[0]);

}

#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_WriteEVStatusDC
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiTx_ISO_WriteEVStatusDC(P2VAR(Exi_ISO_DC_EVStatusType, AUTOMATIC, SCC_VAR_NOINIT) *EVStatePtrPtr,
  P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufIdxPtr)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 create the root element */
  *EVStatePtrPtr = (P2VAR(Exi_ISO_DC_EVStatusType, AUTOMATIC, SCC_VAR_NOINIT)) &Scc_Exi_StructBuf[*BufIdxPtr]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
  *BufIdxPtr += (uint16)sizeof(Exi_ISO_DC_EVStatusType);

  /* EVReady */
  Scc_Get_ISO_DC_EVReady(&(*EVStatePtrPtr)->EVReady);

  /* EVErrorCode */
  Scc_Get_ISO_DC_EVErrorCode(&(*EVStatePtrPtr)->EVErrorCode);

  /* EVRESSSOC */
  Scc_Get_ISO_DC_EVRESSSOC(&(*EVStatePtrPtr)->EVRESSSOC);

  return;
}
#endif /* SCC_CHARGING_DC */

#define SCC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
/* PRQA L:NEST_STRUCTS */
/* PRQA L:RETURN_PATHS */

#endif /* SCC_SCHEMA_ISO */

/**********************************************************************************************************************
 *  END OF FILE: Scc_ExiTx_ISO.c
 *********************************************************************************************************************/
