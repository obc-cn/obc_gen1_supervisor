/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc_Priv.h
 *        \brief  Smart Charging Communication Header File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *********************************************************************************************************************/
#if !defined (SCC_PRIV_H)
#define SCC_PRIV_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Scc_Types.h"
#include "Scc_Cfg.h"

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
#include "NvM.h"
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* V2G constants */
#define SCC_V2GTP_HDR_LEN       8u
#define SCC_EXI_HDR_LEN         1u
#define SCC_THREE_PHASES        3u
#define SCC_ONE_PHASE           1u
#define SCC_SDP_SERVER_PORT     15118U

#define SCC_V2GTP_HDR_PAYLOAD_TYPE_SDP_PAIR_POSITION_REQ     36866UL
#define SCC_V2GTP_HDR_PAYLOAD_TYPE_SDP_PAIR_POSITION_RES     36867UL

#define SCC_V2GTP_HDR_PAYLOAD_TYPE_RENEGOTIATION             32770UL
#define SCC_V2GTP_HDR_PAYLOAD_TYPE_ACD_SYSTEM_STATUS         32771UL
#define SCC_V2GTP_HDR_PAYLOAD_TYPE_METERING_RECEIPT          32772UL
#define SCC_V2GTP_HDR_PAYLOAD_TYPE_CERTIFICATE_INSTALL       32773UL

/* Crypto constants */
#define SCC_CRYPTO_BUF_LEN                   80u
#define SCC_BER_STACK_DEPTH                   6u
#define SCC_DOM_COM_TAG_LEN                   3u
#define SCC_SHA256_DIGEST                    32u
#define SCC_P256R1_PUBLIC_KEY_LEN            64u
#define SCC_P256R1_SIGNATURE_LEN             64u
#define SCC_AES128_KEY_LEN                   16u
#define SCC_KDF_OTHER_INFO_LEN                3u
#define SCC_KDF_SHARED_SECRET_LEN            32u
#define SCC_KDF_ITERATION_INFO_LEN            4u
#define SCC_MASTER_SECRET_LEN                48u
#define SCC_CLIENT_RANDOM_LEN                32u

/* Integer constants */
#define SCC_SINT8_MAX_POS_NUM  0x7Fu       /*            127 */
#define SCC_SINT8_MAX_NEG_NUM  0x80u       /* (-)        128 */
#define SCC_SINT16_MAX_POS_NUM 0x7FFFu     /*          32767 */
#define SCC_SINT16_MAX_NEG_NUM 0x8000u     /* (-)      32768 */
#define SCC_SINT32_MAX_POS_NUM 0x7FFFFFFFu /*     2147483647 */
#define SCC_SINT32_MAX_NEG_NUM 0x80000000u /* (-) 2147483648 */

#define SCC_UINT8_MAX_NUM  0xFFu
#define SCC_UINT16_MAX_NUM 0xFFFFu
#define SCC_UINT32_MAX_NUM 0xFFFFFFFFu

#define SCC_SECONDS_PER_DAY 0x15180u /* 86,400 seconds per day */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3453 MACROS_FUNCTION_LIKE */ /* MD_MSR_FctLikeMacro */
/* PRQA S 3458 MACROS_BRACES */ /* MD_Scc_20.4 */

#if ( defined SCC_CONFIG_VARIANT ) && ( SCC_CONFIG_VARIANT == 1u )

/* --- NvM Block Identifier & Length --- */
/* Block Identifier */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
#define Scc_GetNvMBlockIDContrCertChainSize(Chain)   Scc_CertsNvm.ContrCertChains[(Chain)].ContrCertChainSizeBlockId
#define Scc_GetNvMBlockIDContrCert(Chain)            Scc_CertsNvm.ContrCertChains[(Chain)].ContrCertBlockId
#define Scc_GetNvMBlockIDContrCertPrivKey(Chain)     Scc_CertsNvm.ContrCertChains[(Chain)].ContrCertPrivKeyBlockId
#define Scc_GetNvMBlockIDContrSubCerts(Chain, Idx)   Scc_CertsNvm.ContrCertChains[(Chain)].ContrSubCert[(Idx)].ContrSubCertBlockId
#define Scc_GetNvMBlockIDProvCert(Chain)             Scc_CertsNvm.ContrCertChains[(Chain)].ProvCertBlockId
#define Scc_GetNvMBlockIDProvCertPrivKey(Chain)      Scc_CertsNvm.ContrCertChains[(Chain)].ProvCertPrivKeyBlockId
#endif /* SCC_ENABLE_PNC_CHARGING */
/* Block Length */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
#define Scc_GetNvMBlockLenContrCertChainSize(Chain)  Scc_CertsNvm.ContrCertChains[(Chain)].ContrCertChainSizeBlockLen
#define Scc_GetNvMBlockLenContrCert(Chain)           Scc_CertsNvm.ContrCertChains[(Chain)].ContrCertBlockLen
#define Scc_GetNvMBlockLenContrCertPrivKey(Chain)    Scc_CertsNvm.ContrCertChains[(Chain)].ContrCertPrivKeyBlockLen
#define Scc_GetNvMBlockLenContrSubCerts(Chain, Idx)  Scc_CertsNvm.ContrCertChains[(Chain)].ContrSubCert[(Idx)].ContrSubCertBlockLen
#define Scc_GetNvMBlockLenProvCert(Chain)            Scc_CertsNvm.ContrCertChains[(Chain)].ProvCertBlockLen
#define Scc_GetNvMBlockLenProvCertPrivKey(Chain)     Scc_CertsNvm.ContrCertChains[(Chain)].ProvCertPrivKeyBlockLen
#endif /* SCC_ENABLE_PNC_CHARGING */

#endif /* SCC_CONFIG_VARIANT */

/* PRQA L:MACROS_BRACES */
/* PRQA L:MACROS_FUNCTION_LIKE */
/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/* other variables */
#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Scc_MsgTrigType, SCC_VAR_NOINIT)     Scc_MsgTrig;
extern VAR(Scc_MsgStatusType, SCC_VAR_NOINIT)   Scc_MsgStatus;
extern VAR(Scc_StackErrorType, SCC_VAR_NOINIT)  Scc_StackError;

extern VAR(Scc_PbufType, SCC_VAR_NOINIT)        Scc_ExiStreamRxPBuf[SCC_EXI_RX_STREAM_PBUF_ELEMENTS];
extern VAR(Scc_SockAddrIn6Type, SCC_VAR_NOINIT) Scc_ServerSockAddr;

extern VAR(TcpIp_SocketIdType, SCC_VAR_NOINIT)  Scc_SDPSocket;
extern VAR(TcpIp_SocketIdType, SCC_VAR_NOINIT)  Scc_V2GSocket;

extern Std_ReturnType (*Scc_ExiTx_Xyz_EncodeMessageFctPtr)(
    P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr
);
extern Std_ReturnType (*Scc_ExiRx_Xyz_DecodeMessageFctPtr)(void);

#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* uint8 variables - no init */
#define SCC_START_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, SCC_VAR_NOINIT) Scc_TxStreamingActive;

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
extern VAR(boolean, SCC_VAR_NOINIT) Scc_SaPubKeyRcvd;
#endif /* SCC_ENABLE_PNC_CHARGING */

#define SCC_STOP_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* uint16 variables */
#define SCC_START_SEC_VAR_NOINIT_16BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(uint16, SCC_VAR_NOINIT) Scc_TimeoutCnt;

#define SCC_STOP_SEC_VAR_NOINIT_16BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* 32bit variables */
#define SCC_START_SEC_VAR_NOINIT_32BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(uint32, SCC_VAR_NOINIT) Scc_TxDataSent;

#define SCC_STOP_SEC_VAR_NOINIT_32BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Scc_StateType, SCC_VAR_NOINIT) Scc_State;

#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Scc_Priv_Init()
 *********************************************************************************************************************/
/*! \brief          Initializes BER encoding workspace for PnC.
 *  \details        -
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_Priv_Init(void);

/**********************************************************************************************************************
 *  Scc_ReportError()
 *********************************************************************************************************************/
/*! \brief          Set the stack error, determine the current message status and report the error to the application.
 *  \details        -
 *  \param[in]      StackError            stack error that has to be reported.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-133184
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_ReportError(Scc_StackErrorType StackError);

/**********************************************************************************************************************
 *  Scc_ReportSuccessAndStatus()
 *********************************************************************************************************************/
/*! \brief          Set the message status and report the status to the application.
 *  \details        -
 *  \param[in]      MsgStatus             message status of the V2G message.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_ReportSuccessAndStatus(Scc_MsgStatusType MsgStatus);

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_NvmReadBlock()
 *********************************************************************************************************************/
/*! \brief          Read the NvM block.
 *  \details        -
 *  \param[in]      NvMBlockID            ID of the NvM Block.
 *  \param[in,out]  NvMBlockReadStatePtr  Returns the value of the NvM Read Block status.
 *  \param[in]      DataPtr               Choose check for CPS or CPO.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_NvmReadBlock(NvM_BlockIdType NvMBlockID,
  P2VAR(Scc_NvMBlockReadStateType, AUTOMATIC, SCC_APPL_DATA) NvMBlockReadStatePtr, P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr);

/**********************************************************************************************************************
 *  Scc_CheckCertTimeExpiration()
 *********************************************************************************************************************/
/*! \brief          Checks if the certificate is already expired.
 *  \details        -
 *  \param[in]      CertPtr               Pointer to the certificate.
 *  \param[in]      CertLen               Length of the certificate.
 *  \param[in]      CurrentUnixTime       Current time in Unix format.
 *  \return         E_OK                  Certificate is not yet expired.
 *  \return         E_NOT_OK              Certificate is expired.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_CheckCertTimeExpiration(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr,
  uint32 CertLen, uint32 CurrentUnixTime);

/**********************************************************************************************************************
 *  Scc_GetCertSerialNumber()
 *********************************************************************************************************************/
/*! \brief          Get serial number from certificate.
 *  \details        -
 *  \param[in]      CertPtr               Pointer to the certificate.
 *  \param[in]      CertLen               Length of the certificate.
 *  \param[out]     DataPtr               Pointer to the output buffer.
 *  \param[in,out]  DataLenPtr            Length of the output buffer.
 *  \return         E_OK                  Certificate BER-decoding and data copying was successful.
 *  \return         E_NOT_OK              Certificate BER-decoding or data copying failed.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_GetCertSerialNumber(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr, uint16 CertLen,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) DataLenPtr);

/**********************************************************************************************************************
 *  Scc_GetCertIssuer()
 *********************************************************************************************************************/
/*! \brief          Get issuer from certificate.
 *  \details        -
 *  \param[in]      CertPtr               Pointer to the certificate.
 *  \param[in]      CertLen               Length of the certificate.
 *  \param[out]     DataPtr               Pointer to the output buffer.
 *  \param[in,out]  DataLenPtr            Length of the output buffer.
 *  \return         E_OK                  Certificate BER-decoding and data copying was successful.
 *  \return         E_NOT_OK              Certificate BER-decoding or data copying failed.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_GetCertIssuer(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr, uint16 CertLen,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) DataLenPtr);

/**********************************************************************************************************************
 *  Scc_GetIndexOfPublicKey()
 *********************************************************************************************************************/
/*! \brief          Get PublicKey from certificate.
 *  \details        -
 *  \param[in]      CertPtr               Pointer to the certificate.
 *  \param[in]      CertLen               Length of the certificate.
 *  \param[out]     DataPtr               Pointer to the output buffer.
 *  \param[in,out]  DataLenPtr            Length of the output buffer.
 *  \return         E_OK                  Certificate BER-decoding and data copying was successful.
 *  \return         E_NOT_OK              Certificate BER-decoding or data copying failed.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_GetIndexOfPublicKey(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr, uint16 CertLen,
  P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) PubKeyIdxPtr, P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) PubKeyLenPtr);

/**********************************************************************************************************************
 *  Scc_ValidateCertAgainstRootCert()
 *********************************************************************************************************************/
/*! \brief          Validates the certificate against the root certificate and the expiration time of the certificate
 *  \details        -
 *  \param[in]      CertPtr               Pointer to the certificate.
 *  \param[in]      CertLengthPtr         Length of the certificate.
 *  \param[in]      CurrentTime           Current Time.
 *  \param[in]      RootCertIdx           The root certificate that shall be used for validation.
 *  \return         E_OK                  Certificate is signed with the Root certificate.
 *  \return         E_NOT_OK              Invalid certificate. Certificate is not signed with the Root certificate.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ValidateCertAgainstRootCert(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr,
  P2CONST(uint16, AUTOMATIC, SCC_APPL_DATA) CertLengthPtr, P2CONST(uint32, AUTOMATIC, SCC_APPL_DATA) CurrentTime,
  uint8 RootCertIdx);

/**********************************************************************************************************************
 *  Scc_GetCertsFromPKCS7()
 *********************************************************************************************************************/
/*! \brief          Gets the certificate positions and length out of a DER decoded PKCS#7 structure.
 *  \details        -
 *  \param[in]      CertChainMsgPtr       Pointer to the PKCS#7 structure.
 *  \param[in]      CertChainLength       Length of the PKCS#7 structure.
 *  \param[in,out]  NumberOfCerts         (Maximum) Number of certificates in the PKCS#7 structure.
 *  \param[in,out]  CertChainStructPtr    Pointer the concatenated list of type Scc_CertificateType
 *  \return         E_OK                  PKCS#7 structure was correct decoded.
 *  \return         E_NOT_OK              Decoding of PKCS#7 failed.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Scc_ReturnType, SCC_CODE) Scc_GetCertsFromPKCS7(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertChainMsgPtr,
  uint16 CertChainLength, P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) NumberOfCertsPtr,
  P2VAR(Scc_CertificateType, AUTOMATIC, SCC_APPL_DATA) CertChainStructPtr);

/**********************************************************************************************************************
 *  Scc_GetSignatureElemOfBerCert()
 *********************************************************************************************************************/
/*! \brief          BER encode the signature from the certificate.
 *  \details        -
 *  \param[in]      SignaturePtr          Pointer to signature in the certificate.
 *  \param[in]      SignatureLenPtr       Length of the signature in the certificate.
 *  \param[out]     SignatureEncodedPtr   Output buffer to write the BER encoded signature.
 *  \param[in]      SignatureEncodedLen   Length of the output Buffer (always 64 Byte for ECDSA with p256r1 curve).
 *  \return         E_OK                  BER encoding was successful.
 *  \return         E_NOT_OK              BER encoding failed.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_GetSignatureElemOfBerCert(P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) SignaturePtr,
  P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) SignatureLenPtr, P2VAR(uint8, AUTOMATIC, SCC_VAR_NOINIT) SignatureEncodedPtr, uint8 SignatureEncodedLen);

/**********************************************************************************************************************
 *  Scc_NvmReadRootCerts()
 *********************************************************************************************************************/
/*! \brief          Read the root certificates from the NvM.
 *  \details        -
 *  \param[in]      ReadAll               If FALSE, only the Root Cert specified by RootCertIdx will be read.
 *  \param[in]      RootCertIdx           Index of the Root Cert that shall be read, only evaluated if ReadAll is FALSE.
 *  \return         E_OK                  Root Certificate was read successfully from NVM.
 *  \return         E_NOT_OK              Reading the Root Certificate from NvM failed.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_NvmReadRootCerts(boolean ReadAll, uint8 RootCertIdx);

/**********************************************************************************************************************
 *  Scc_NvmReadProvCert()
 *********************************************************************************************************************/
/*! \brief          Read the provisioning certificate from the NvM.
 *  \details        -
 *  \return         E_OK                  Provisioning certificate was read successfully from NVM.
 *  \return         E_NOT_OK              Reading the provisioning certificate from NvM failed.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_NvmReadProvCert(void);

/**********************************************************************************************************************
 *  Scc_NvmReadContrCertChain()
 *********************************************************************************************************************/
/*! \brief          Read the contract certificate from the NvM.
 *  \details        -
 *  \param[in]      LoadContrSubCerts     Check if also the sub certificates should be read from NvM.
 *  \return         E_OK                  Contract certificate was read successfully from NVM.
 *  \return         E_NOT_OK              Reading the contract certificate from NvM failed.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_NvmReadContrCertChain(boolean LoadContrSubCerts);

/**********************************************************************************************************************
 *  Scc_ValidateKeyPair()
 *********************************************************************************************************************/
/*! \brief          Validate the private key against the public key from the certificate.
 *  \details        -
 *  \param[in]      CertPtr               Pointer to the certificate.
 *  \param[in]      CertLen               Length of the certificate.
 *  \param[in]      ECDSASignJobId        CSM Key Job of the ECDSA sign job to sign the message.
 *  \return         E_OK                  Private Key is the correct key to the public key in the certificate.
 *  \return         E_NOT_OK              Private Key is not the  correct key to the public key in the certificate.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ValidateKeyPair(P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) CertPtr, uint16 CertLen,
                                                   uint32 ECDSASignJobId);

/**********************************************************************************************************************
 *  Scc_DecryptPrivateKey()
 *********************************************************************************************************************/
/*! \brief          Decrypt the PrivateKey during CertificateInstallation and CertificateUpdate.
 *  \details        -
 *  \param[in]      ECDHExchangeKeyId     Temporary exchange key Id.
 *  \param[in]      ECDSASignCertKeyId    CSM Key Id of the private key to store the key Id.
 *  \param[in]      PublicKeyPtr          Pointer to PublicKey of the SAProvisioningCertificate.
 *  \param[in]      EncryptedPrivKeyPtr   Pointer to the encrypted PrivateKey.
 *  \param[in]      InitVectorPtr         Pointer to the Initialization Vector.
 *  \return         E_OK                  PrivateKey was succesfull decrypted.
 *  \return         E_NOT_OK              Decryption of PrivateKey failed.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_DecryptPrivateKey(uint32 ECDHExchangeKeyId,
                                                     uint32 ECDSASignCertKeyId,
                                                     P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) PublicKeyPtr,
                                                     P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) EncryptedPrivKeyPtr,
                                                     P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) InitVectorPtr);

/**********************************************************************************************************************
 *  Scc_XmlSecGetPublicKey()
 *********************************************************************************************************************/
/*! \brief          Callback for XmlSecurity to get the public key.
 *  \details        -
 *  \param[in]      PubKeyPtr             Pointer to the public key.
 *  \param[in]      PubKeyLen             Length to the public key.
 *  \param[in]      SigAlgorithm          Pointer to the certificate.
 *  \return         E_OK                  Set the Public Key.
 *  \return         E_NOT_OK              Not correct signature algorithm or public key not received.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_XmlSecGetPublicKey(P2VAR(uint8*, AUTOMATIC, SCC_VAR_NOINIT) PubKeyPtr,
  P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) PubKeyLen, XmlSecurity_SignatureAlgorithmType SigAlgorithm);

/**********************************************************************************************************************
 *  Scc_ReadCertTimeInfo()
 *********************************************************************************************************************/
/*! \brief          Read the validity end time info from certificate.
 *  \details        -
 *  \param[in]      DataPtr               Pointer to validity end time info from certificate.
 *  \param[in]      Len                   Length of validity end time info from certificate.
 *  \param[out]     Time32Ptr             Time value of validity end time info from certificate.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_ReadCertTimeInfo(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr,
  uint8 Len, CONSTP2VAR(uint32, AUTOMATIC, SCC_APPL_DATA) Time32Ptr);

/**********************************************************************************************************************
 *  Scc_ConvertTimeDT2S()
 *********************************************************************************************************************/
/*! \brief          Convert time (minutes, hours, days, month, years) to seconds.
 *  \details        -
 *  \param[in]      TimeDTPtr             Pointer time (minutes, hours, days, month, years) value.
 *  \param[out]     TimeSPtr              Pointer converted seconds value.
 *  \return         E_OK                  Conversion was successful.
 *  \return         E_NOT_OK              Not correct conversion of the time to seconds.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ConvertTimeDT2S(P2CONST(Scc_TimeDTType, AUTOMATIC, SCC_APPL_DATA) TimeDTPtr,
  P2VAR(uint32, AUTOMATIC, SCC_APPL_DATA) TimeSPtr);

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_CertificateInstallationRes()
 *********************************************************************************************************************/
/*! \brief          Process the CertificateInstallation response.
 *  \details        -
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-133195
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiRx_ISO_CertificateInstallationRes(void);

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_RemoveDashesFromEMAID()
 *********************************************************************************************************************/
/*! \brief          Remove dashes from EMAID.
 *  \details        -
 *  \param[in]      inEMAIDPtr            Pointer the EMAID with dashes.
 *  \param[in]      inEMAIDLen            Length of the the EMAID with dashes.
 *  \param[out]     outEMAIDPtr           Pointer the EMAID without dashes.
 *  \param[out]     outEMAIDPtr           Length of the the EMAID without dashes.
 *  \return         E_OK                  Conversion was successful.
 *  \return         E_NOT_OK              Output buffer to small.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiRx_ISO_RemoveDashesFromEMAID(
  P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) inEMAIDPtr, CONST(uint16, SCC_CONST)inEMAIDLen,
  P2VAR(uint8, AUTOMATIC, SCC_VAR_NOINIT) outEMAIDPtr, P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) outEMAIDLenPtr);

/**********************************************************************************************************************
*  Scc_InitWorkspaceDecodeExiData()
*********************************************************************************************************************/
/*! \brief          Initializes the Scc Exi workspace and decodes the given Exi stream.
 *  \details        -
 *  \param[in]      ExiPtr                Pointer to the Exi stream.
 *  \param[in]      ExiLength             Length of the Exi stream.
 *  \return         E_OK                  Decoding of Exi was successful and workspace could be initialized.
 *  \return         E_NOT_OK              Decoding of Exi leands to error or workspace could not be initialized.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Scc_ReturnType, SCC_CODE) Scc_InitWorkspaceDecodeExiData(P2VAR(uint8, AUTOMATIC, SCC_RTE_DATA) ExiPtr,
  uint16 ExiLength);

#if (   ( ( defined SCC_ENABLE_CERTIFICATE_INSTALLATION ) && ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )) \
     || ( ( defined SCC_ENABLE_CERTIFICATE_UPDATE ) && ( SCC_ENABLE_CERTIFICATE_UPDATE == STD_ON )))
/**********************************************************************************************************************
 *  Scc_ExiTx_ISO_ExtractRootCertificateIDs()
 *********************************************************************************************************************/
/*! \brief          extract the IDs of all root certificates.
 *  \details        -
 *  \return         E_OK                  extraction was successful
 *  \return         E_NOT_OK              IpBase decode error.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiTx_ISO_ExtractRootCertificateIDs(void);
#endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION, SCC_ENABLE_CERTIFICATE_UPDATE */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_TLS == STD_ON )
/**********************************************************************************************************************
*  Scc_Get_ClientRandom_MasterSecret()
*********************************************************************************************************************/
/*! \brief          Fetches the Client random and Master secret used by Tls.
 *  \details        -
 *  \param[out]     DataPtr               Pointer to buffer to store the data.
 *  \param[in,out]  DataLen               in: Length of the DataPtr. out: Number of bytes written into DataPtr.
 *  \return         E_OK                  Getting Client random and Master secret was successful.
 *  \return         E_NOT_OK              Error while getting Client random and Master secret.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_Get_ClientRandom_MasterSecret(
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataLen);

/**********************************************************************************************************************
 *  Scc_Send_ClientRandom_MasterSecret()
 *********************************************************************************************************************/
/*! \brief          Transmits Client random and Master secret over UDP.
 *  \details        -
 *  \param[in]      DataPtr           Pointer to buffer which needs to be sent before master secret and client random.
 *  \param[in]      DataLen           Length of the DataPtr.
 *  \return         E_OK              Sending Client random and Master secret was successful.
 *  \return         E_NOT_OK          Error while sending Client random and Master secret.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_Send_ClientRandom_MasterSecret(
  P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, CONST(uint8, SCC_CONST) DataLen);
#endif /* ( SCC_ENABLE_TLS == STD_ON ) */

#if ( SCC_SCHEMA_ISO_ED2 != 0 )
/**********************************************************************************************************************
 *  Scc_Conv_Scc_2_ISOEd2_PhysicalValue()
 *********************************************************************************************************************/
/*! \brief          Converts a Scc_PhysicalValueType to Exi_ISO_ED2_DIS_PhysicalValueType.
 *  \details        -
 *  \param[in]      scc_physicalValue             Pointer to the Scc-physicalvalue.
 *  \param[out]     iso_ed2_physicalValue         Pointer to the converted physicalvalue.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_Conv_Scc_2_ISOEd2_PhysicalValue(
  P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) scc_physicalValue,
  P2VAR(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) iso_ed2_physicalValue);

/**********************************************************************************************************************
 *  Scc_Conv_ISOEd2_2_Scc_PhysicalValue()
 *********************************************************************************************************************/
/*! \brief          Converts a Exi_ISO_ED2_DIS_PhysicalValueType to Scc_PhysicalValueType.
 *  \details        -
 *  \param[in]      iso_ed2_physicalValue         Pointer to the Schema-physicalvalue.
 *  \param[out]     scc_physicalValue             Pointer to the converted Scc-physicalvalue.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_Conv_ISOEd2_2_Scc_PhysicalValue(
  P2CONST(Exi_ISO_ED2_DIS_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) iso_ed2_physicalValue,
  P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) scc_physicalValue);
#endif /* SCC_SCHEMA_ISO_ED2 */

#if ( SCC_SCHEMA_ISO != 0 )
/**********************************************************************************************************************
 *  Scc_Conv_Scc2ISO_PhysicalValue()
 *********************************************************************************************************************/
/*! \brief          Converts a Scc_PhysicalValueType to Exi_ISO_PhysicalValueType.
 *  \details        -
 *  \param[in]      scc_physicalValue             Pointer to the Scc-physicalvalue.
 *  \param[out]     iso_physicalValue             Pointer to the converted physicalvalue.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_Conv_Scc2ISO_PhysicalValue(
  P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) scc_physicalValue,
  P2VAR(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) iso_physicalValue);

/**********************************************************************************************************************
 *  Scc_Conv_ISO2Scc_PhysicalValue()
 *********************************************************************************************************************/
/*! \brief          Converts a Exi_ISO_PhysicalValueType to Scc_PhysicalValueType.
 *  \details        -
 *  \param[in]      iso_physicalValue             Pointer to the Schema-physicalvalue.
 *  \param[out]     scc_physicalValue             Pointer to the converted Scc-physicalvalue.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_Conv_ISO2Scc_PhysicalValue(
  P2CONST(Exi_ISO_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) iso_physicalValue,
  P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) scc_physicalValue);
#endif /* SCC_SCHEMA_ISO */

#if ( SCC_SCHEMA_DIN != 0 )
/**********************************************************************************************************************
 *  Scc_Conv_Scc2DIN_PhysicalValue()
 *********************************************************************************************************************/
/*! \brief          Converts a Scc_PhysicalValueType to Exi_DIN_PhysicalValueType.
 *  \details        -
 *  \param[in]      scc_physicalValue             Pointer to the Scc-physicalvalue.
 *  \param[out]     din_physicalValue             Pointer to the converted physicalvalue.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_Conv_Scc2DIN_PhysicalValue(
  P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) scc_physicalValue,
  P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) din_physicalValue);

/**********************************************************************************************************************
 *  Scc_Conv_DIN2Scc_PhysicalValue()
 *********************************************************************************************************************/
/*! \brief          Converts a Exi_DIN_PhysicalValueType to Scc_PhysicalValueType.
 *  \details        -
 *  \param[in]      din_physicalValue             Pointer to the Schema-physicalvalue.
 *  \param[out]     scc_physicalValue             Pointer to the converted Scc-physicalvalue.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_Conv_DIN2Scc_PhysicalValue(
  P2CONST(Exi_DIN_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) din_physicalValue,
  P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_VAR_NOINIT) scc_physicalValue);
#endif /* SCC_SCHEMA_DIN */

#define SCC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* SCC_PRIV_H */
/**********************************************************************************************************************
 *  END OF FILE: Scc_Priv.h
 *********************************************************************************************************************/
