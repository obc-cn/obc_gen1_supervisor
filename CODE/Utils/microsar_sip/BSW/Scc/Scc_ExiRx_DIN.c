/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc_ExiRx_DIN.c
 *        \brief  Smart Charging Communication Source Code File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *********************************************************************************************************************/

#define SCC_EXIRX_DIN_SOURCE

/**********************************************************************************************************************
 LOCAL MISRA / PCLINT JUSTIFICATION
 **********************************************************************************************************************/
/* PRQA S 0777 EOF */ /* MD_MSR_Rule5.1 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

#include "Scc_Cfg.h"

#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )

#include "Scc_Exi.h"
#include "Scc.h"
#include "Scc_Priv.h"
#include "Scc_Lcfg.h"
#include "Scc_Interface_Cfg.h"

#if ( SCC_DEV_ERROR_DETECT == STD_ON )
#include "Det.h"
#endif /* SCC_DEV_ERROR_DETECT */
#include "Exi.h"
#include "IpBase.h"

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined (SCC_LOCAL)
# define SCC_LOCAL static
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3453 MACROS_FUNCTION_LIKE */ /* MD_MSR_FctLikeMacro */

/* PRQA L:MACROS_FUNCTION_LIKE */
/**********************************************************************************************************************
 *  LOCAL / GLOBAL DATA
 *********************************************************************************************************************/

/* 16bit variables */
#define SCC_START_SEC_VAR_NOINIT_16BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

VAR(uint16, SCC_VAR_NOINIT) Scc_ExiRx_DIN_ServiceID;

#define SCC_STOP_SEC_VAR_NOINIT_16BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* other variables */
#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
SCC_LOCAL P2VAR(Exi_DIN_V2G_MessageType, AUTOMATIC, SCC_VAR_NOINIT) Scc_ExiRx_DIN_MsgPtr;

#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_<DIN-Response>
 *********************************************************************************************************************/
 /*! \brief         Get all parameters of DIN response message
 *  \details        Validates parameter and reports them to the application.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
/* Common */
/* see pattern Scc_ExiRx_DIN_<DIN-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_SessionSetupRes(void);
/* see pattern Scc_ExiRx_DIN_<DIN-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_ServiceDiscoveryRes(void);
/* see pattern Scc_ExiRx_DIN_<DIN-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_ServicePaymentSelectionRes(void);
/* see pattern Scc_ExiRx_DIN_<DIN-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_ContractAuthenticationRes(void);
/* see pattern Scc_ExiRx_DIN_<DIN-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_ChargeParameterDiscoveryRes(void);
/* see pattern Scc_ExiRx_DIN_<DIN-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_PowerDeliveryRes(void);
/* see pattern Scc_ExiRx_DIN_<DIN-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_SessionStopRes(void);

/* DC */
/* see pattern Scc_ExiRx_DIN_<DIN-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_CableCheckRes(void);
/* see pattern Scc_ExiRx_DIN_<DIN-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_PreChargeRes(void);
/* see pattern Scc_ExiRx_DIN_<DIN-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_CurrentDemandRes(void);
/* see pattern Scc_ExiRx_DIN_<DIN-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_WeldingDetectionRes(void);

/**********************************************************************************************************************
*  Scc_ExiRx_DIN_ReadEVSEStatusDC
*********************************************************************************************************************/
/*! \brief          reports the EVSEStatus to the application.
 *  \details        reports EVSEIsolationStatus, EVSEStatusCode, EVSENotification and NotificationMaxDelay to the application.
 *  \param[in]      EVSEStatusPtr             Pointer to the EVSEstatus
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
*********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiRx_DIN_ReadEVSEStatusDC(P2CONST(Exi_DIN_DC_EVSEStatusType, AUTOMATIC, SCC_VAR_NOINIT) EVSEStatusPtr);

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_Init
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_ExiRx_DIN_Init(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the message pointer to the Exi buffer. */
  Scc_ExiRx_DIN_MsgPtr = (P2VAR(Exi_DIN_V2G_MessageType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_StructBuf[0]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */

}

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_DecodeMessage
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiRx_DIN_DecodeMessage(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize the Exi workspace and check if the workspace initialization failed */
  if ( (Std_ReturnType)E_OK == Scc_Exi_InitDecodingWorkspace() )
  {
    /* Set the decode information */
    Scc_Exi_DecWs.OutputData.SchemaSetId = EXI_SCHEMA_SET_DIN_TYPE;
    /* #20 decode the message */
    if ( (Std_ReturnType)E_OK == Exi_Decode(&Scc_Exi_DecWs) )
    {
#if ( defined SCC_DEM_EXI )
      /* report status to DEM */
      Scc_DemReportErrorStatusPassed(SCC_DEM_EXI);
#endif /* SCC_DEM_EXI */

      /* #30 check the SessionID starting from ServiceDiscovery and stop with SessionStop */
      /* V2G-DC-649 requires SessionStop after SessionSetupRes with ResponseCode Failed.
         SessionID is not set in this condition */
      if  (( Scc_MsgTrig_SessionSetup < Scc_MsgTrig )
        && ( Scc_MsgStatus != Scc_MsgStatus_SessionSetup_Failed ))
      {
        /* compare the length of the stored and received SessionID */
        if ( Scc_ExiRx_DIN_MsgPtr->Header->SessionID->Length == Scc_SessionIDNvm[0] )
        {
          /* if the length is the same, check if the content is the same, too */
          if ( IPBASE_CMP_EQUAL != IpBase_StrCmpLen(&Scc_ExiRx_DIN_MsgPtr->Header->SessionID->Buffer[0],
            &Scc_SessionIDNvm[1], Scc_SessionIDNvm[0] ))
          {
            /* SessionID is not equal, report the error */
            Scc_ReportError(Scc_StackError_InvalidRxParameter);
          }
          else
          {
            retVal = E_OK;
          }
        }
        /* length is not the same */
        else
        {
          /* report the error */
          Scc_ReportError(Scc_StackError_InvalidRxParameter);
          retVal = E_NOT_OK;
        }
        /* the Notification is currently ignored */
      }
      else
      {
        retVal = E_OK;
      }

      if ( retVal == E_OK )
      {
        retVal = E_NOT_OK;

        /* #40 handle an incoming response message */
        switch ( Scc_MsgTrig )
        {
          /* handle an incoming session setup response message */
        case Scc_MsgTrig_SessionSetup:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_DIN_SessionSetupRes() )
          {
            Scc_State = Scc_State_Connected;
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_SessionSetup_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming service discovery response message */
        case Scc_MsgTrig_ServiceDiscovery:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_DIN_ServiceDiscoveryRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_ServiceDiscovery_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming service payment selection response message */
        case Scc_MsgTrig_PaymentServiceSelection:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_DIN_ServicePaymentSelectionRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_PaymentServiceSelection_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming contract authentication response message */
        case Scc_MsgTrig_Authorization:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_DIN_ContractAuthenticationRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_Authorization_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming charge parameter discovery response message */
        case Scc_MsgTrig_ChargeParameterDiscovery:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_DIN_ChargeParameterDiscoveryRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_ChargeParameterDiscovery_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming power delivery response message */
        case Scc_MsgTrig_PowerDelivery:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_DIN_PowerDeliveryRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_PowerDelivery_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming session stop response message */
        case Scc_MsgTrig_SessionStop:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_DIN_SessionStopRes() )
          {
            Scc_State = Scc_State_Disconnected;
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_SessionStop_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming cable check response message */
        case Scc_MsgTrig_CableCheck:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_DIN_CableCheckRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_CableCheck_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming pre charge response message */
        case Scc_MsgTrig_PreCharge:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_DIN_PreChargeRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_PreCharge_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming current demand response message */
        case Scc_MsgTrig_CurrentDemand:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_DIN_CurrentDemandRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_CurrentDemand_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming welding detection response message */
        case Scc_MsgTrig_WeldingDetection:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_DIN_WeldingDetectionRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_WeldingDetection_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

        default:
          /* invalid state */
          break;
        }
      }


    }
    else
    {
      /* report the error */
      Scc_ReportError(Scc_StackError_Exi);

  #if ( defined SCC_DEM_EXI )
      /* report status to DEM */
      Scc_DemReportErrorStatusFailed(SCC_DEM_EXI);
  #endif /* SCC_DEM_EXI */

      retVal = E_NOT_OK;
    }
  }

  return retVal;
} /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_SessionSetupRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_SessionSetupRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_DIN_SessionSetupResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_DIN_SessionSetupResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 check if the message is not the one that was expected */
  if ( EXI_DIN_SESSION_SETUP_RES_TYPE != Scc_ExiRx_DIN_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }

  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 check if the response is negative */
    if ( EXI_DIN_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application */

      /* SessionID */
      /* get the new length of the SessionID */
      Scc_SessionIDNvm[0] = (uint8) Scc_ExiRx_DIN_MsgPtr->Header->SessionID->Length;
      /* copy the SessionID */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
      IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &Scc_SessionIDNvm[1],
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &Scc_ExiRx_DIN_MsgPtr->Header->SessionID->Buffer[0], Scc_SessionIDNvm[0]);

      /* SessionID */
      Scc_Set_DIN_SessionID(Scc_ExiRx_DIN_MsgPtr->Header->SessionID);

      /* EVSEID */
      Scc_Set_DIN_EVSEID(BodyPtr->EVSEID);

      /* DateTimeNow */
      Scc_Set_DIN_DateTimeNow(BodyPtr->DateTimeNow, BodyPtr->DateTimeNowFlag);

      /* ResponseCode */
      Scc_Set_DIN_ResponseCode(EXI_DIN_RESPONSE_CODE_TYPE_OK_NEW_SESSION_ESTABLISHED);

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_ServiceDiscoveryRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_ServiceDiscoveryRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_DIN_ServiceDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_DIN_ServiceDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_DIN_MsgPtr->Body->BodyElement;

  uint8_least Counter;
  boolean     ExternalPaymentFound = FALSE;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_DIN_SERVICE_DISCOVERY_RES_TYPE != Scc_ExiRx_DIN_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_DIN_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* PaymentOptionList */
      for ( Counter = 0; Counter < BodyPtr->PaymentOptions->PaymentOptionCount; Counter++ )
      {
        /* check if this PaymentOption element contains ExternalPayment */
        if ( EXI_DIN_PAYMENT_OPTION_TYPE_EXTERNAL_PAYMENT == BodyPtr->PaymentOptions->PaymentOption[Counter] )
        {
          /* set the flag */
          ExternalPaymentFound = TRUE;
        }
      }
      /* check if ExternalPayment was not found */
      if ( TRUE != ExternalPaymentFound )
      {
        /* report the error */
        Scc_ReportError(Scc_StackError_InvalidRxParameter);
      }
      else
      {

        /* ChargeService */
        Scc_Set_DIN_ChargeService(BodyPtr->ChargeService);
        /* save the ServiceID */
        Scc_ExiRx_DIN_ServiceID = BodyPtr->ChargeService->ServiceTag->ServiceID;

        /* ResponseCode */
        Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);

        retVal = E_OK;
      }
    }
  }
  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_ServicePaymentSelectionRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_ServicePaymentSelectionRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_DIN_ServicePaymentSelectionResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_DIN_ServicePaymentSelectionResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_DIN_SERVICE_PAYMENT_SELECTION_RES_TYPE != Scc_ExiRx_DIN_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
      /* report status to DEM */
      Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_DIN_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* ResponseCode */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_ContractAuthenticationRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_ContractAuthenticationRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_DIN_ContractAuthenticationResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_DIN_ContractAuthenticationResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_DIN_CONTRACT_AUTHENTICATION_RES_TYPE != Scc_ExiRx_DIN_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_DIN_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* EVSEProcessing */
      Scc_Set_DIN_EVSEProcessing(BodyPtr->EVSEProcessing);

      /* ResponseCode */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_ChargeParameterDiscoveryRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_ChargeParameterDiscoveryRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  Scc_PhysicalValueType scc_PhysicalValueTmp;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_DIN_ChargeParameterDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_DIN_ChargeParameterDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_DIN_MsgPtr->Body->BodyElement;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_DIN_DC_EVSEChargeParameterType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVSEChargeParameterPtr =
    (P2CONST(Exi_DIN_DC_EVSEChargeParameterType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->EVSEChargeParameter;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_DIN_CHARGE_PARAMETER_DISCOVERY_RES_TYPE != Scc_ExiRx_DIN_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* check if the EVSEChargeParameter has the correct type */
    if ( EXI_DIN_DC_EVSECHARGE_PARAMETER_TYPE != BodyPtr->EVSEChargeParameterElementId )
    {
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
    }
    else
    {
      /* #20 Check if the response is negative. */
      if ( EXI_DIN_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
      {
        /* report the negative response code as stack error */
        Scc_ReportError(Scc_StackError_NegativeResponseCode);
        /* provide the response code to the application */
        Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);
      }
      else
      {
        /* #30 Report the parameter from the response to the application. */
        /* EVSEProcessing */
        Scc_Set_DIN_EVSEProcessing(BodyPtr->EVSEProcessing);

        /* only update the values when EVSEProcessing is set to Finished */
        if ( EXI_DIN_EVSEPROCESSING_TYPE_FINISHED == BodyPtr->EVSEProcessing )
        {
          /* provide the SAScheduleList to the application */
          Scc_Set_DIN_SAScheduleList((P2CONST(Exi_DIN_SAScheduleListType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->SASchedules); /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */
        }

      #if ( SCC_ENABLE_ONGOING_CALLBACKS ) && ( SCC_ENABLE_ONGOING_CALLBACKS == STD_ON )
      #else
        /* only update the values when EVSEProcessing is set to Finished */
        if ( EXI_DIN_EVSEPROCESSING_TYPE_FINISHED == BodyPtr->EVSEProcessing )
      #endif
        {
          /* DC_EVSEChargeParameter -> DC_EVSEStatus */
          Scc_ExiRx_DIN_ReadEVSEStatusDC(DC_EVSEChargeParameterPtr->DC_EVSEStatus);

          /* DC_EVSEChargeParameter -> EVSEMaximumCurrentLimit */
          Scc_Conv_DIN2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMaximumCurrentLimit, &scc_PhysicalValueTmp);
          Scc_Set_DIN_EVSEMaximumCurrentLimit(&scc_PhysicalValueTmp, 1);

          /* DC_EVSEChargeParameter -> EVSEMaximumPowerLimit */
          Scc_Conv_DIN2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMaximumPowerLimit, &scc_PhysicalValueTmp);
          Scc_Set_DIN_EVSEMaximumPowerLimit(&scc_PhysicalValueTmp,
            DC_EVSEChargeParameterPtr->EVSEMaximumPowerLimitFlag);

          /* DC_EVSEChargeParameter -> EVSEMaximumVoltageLimit */
          Scc_Conv_DIN2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMaximumVoltageLimit, &scc_PhysicalValueTmp);
          Scc_Set_DIN_EVSEMaximumVoltageLimit(&scc_PhysicalValueTmp, 1);

          /* DC_EVSEChargeParameter -> EVSEMinimumCurrentLimit */
          Scc_Conv_DIN2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMinimumCurrentLimit, &scc_PhysicalValueTmp);
          Scc_Set_DIN_EVSEMinimumCurrentLimit(&scc_PhysicalValueTmp);

          /* DC_EVSEChargeParameter -> EVSEMinimumVoltageLimit */
          Scc_Conv_DIN2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMinimumVoltageLimit, &scc_PhysicalValueTmp);
          Scc_Set_DIN_EVSEMinimumVoltageLimit(&scc_PhysicalValueTmp);

          /* DC_EVSEChargeParameter -> EVSECurrentRegulationTolerance */
          Scc_Conv_DIN2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSECurrentRegulationTolerance, &scc_PhysicalValueTmp);
          Scc_Set_DIN_EVSECurrentRegulationTolerance(&scc_PhysicalValueTmp,
            DC_EVSEChargeParameterPtr->EVSECurrentRegulationToleranceFlag);

          /* DC_EVSEChargeParameter -> EVSEPeakCurrentRipple */
          Scc_Conv_DIN2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEPeakCurrentRipple, &scc_PhysicalValueTmp);
          Scc_Set_DIN_EVSEPeakCurrentRipple(&scc_PhysicalValueTmp);

          /* DC_EVSEChargeParameter -> EVSEEnergyToBeDelivered */
          Scc_Conv_DIN2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEEnergyToBeDelivered, &scc_PhysicalValueTmp);
          Scc_Set_DIN_EVSEEnergyToBeDelivered(&scc_PhysicalValueTmp,
            DC_EVSEChargeParameterPtr->EVSEEnergyToBeDeliveredFlag);
        }

        retVal = E_OK;

        /* #40 only further verify the message when EVSEProcessing is set to Finished */
        if ( EXI_DIN_EVSEPROCESSING_TYPE_FINISHED == BodyPtr->EVSEProcessing )
        {
          /* check if the parameter was sent by the EVSE */
          if ( 0u == DC_EVSEChargeParameterPtr->EVSEMaximumPowerLimitFlag )
          {
#if ( defined SCC_IGNORE_EVSE_MAXIMUM_POWER_LIMIT ) && ( SCC_IGNORE_EVSE_MAXIMUM_POWER_LIMIT == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
#else
            Scc_ReportError(Scc_StackError_InvalidRxParameter);
            retVal = E_NOT_OK;
#endif /* SCC_IGNORE_EVSE_MAXIMUM_POWER_LIMIT */
          }
        }

        if ( retVal == E_OK )
        {
          /* ResponseCode */
          Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);
        }
      }
    }
  }

  return retVal;
} /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_PowerDeliveryRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_PowerDeliveryRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_DIN_PowerDeliveryResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_DIN_PowerDeliveryResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_DIN_POWER_DELIVERY_RES_TYPE != Scc_ExiRx_DIN_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_DIN_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* check if the EVSEStatus contains an DC_EVSEStatus */
      if ( EXI_DIN_DC_EVSESTATUS_TYPE != BodyPtr->EVSEStatusElementId )
      {
        Scc_ReportError(Scc_StackError_InvalidRxParameter);
      }
      else
      {
        /* DC_EVSEStatus */
        Scc_ExiRx_DIN_ReadEVSEStatusDC((P2VAR(Exi_DIN_DC_EVSEStatusType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->EVSEStatus); /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */

        /* ResponseCode */
        Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);

        retVal = E_OK;
      }
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_SessionStopRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_SessionStopRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_DIN_SessionStopResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_DIN_SessionStopResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_DIN_SESSION_STOP_RES_TYPE != Scc_ExiRx_DIN_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_DIN_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */
      /* ResponseCode */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_CableCheckRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_CableCheckRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_DIN_CableCheckResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_DIN_CableCheckResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_DIN_CABLE_CHECK_RES_TYPE != Scc_ExiRx_DIN_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_DIN_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* EVSEProcessing */
      Scc_Set_DIN_EVSEProcessing(BodyPtr->EVSEProcessing);

    #if ( SCC_ENABLE_ONGOING_CALLBACKS ) && ( SCC_ENABLE_ONGOING_CALLBACKS == STD_ON )
    #else
      /* only update the values when EVSEProcessing is set to Finished */
      if ( EXI_DIN_EVSEPROCESSING_TYPE_FINISHED == BodyPtr->EVSEProcessing )
    #endif
      {
        /* DC_EVSEStatus */
        Scc_ExiRx_DIN_ReadEVSEStatusDC((P2VAR(Exi_DIN_DC_EVSEStatusType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->DC_EVSEStatus);
      }

      /* ResponseCode */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_PreChargeRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_PreChargeRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  Scc_PhysicalValueType scc_PhysicalValueTmp;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_DIN_PreChargeResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_DIN_PreChargeResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_DIN_PRE_CHARGE_RES_TYPE != Scc_ExiRx_DIN_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_DIN_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* DC_EVSEStatus */
      Scc_ExiRx_DIN_ReadEVSEStatusDC((P2VAR(Exi_DIN_DC_EVSEStatusType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->DC_EVSEStatus);

      /* EVSEPresentVoltage */
      Scc_Conv_DIN2Scc_PhysicalValue(BodyPtr->EVSEPresentVoltage, &scc_PhysicalValueTmp);
      Scc_Set_DIN_EVSEPresentVoltage(&scc_PhysicalValueTmp);

      /* ResponseCode */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_CurrentDemandRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_CurrentDemandRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  Scc_PhysicalValueType scc_PhysicalValueTmp;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_DIN_CurrentDemandResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_DIN_CurrentDemandResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_DIN_CURRENT_DEMAND_RES_TYPE != Scc_ExiRx_DIN_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_DIN_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* DC_EVSEStatus */
      Scc_ExiRx_DIN_ReadEVSEStatusDC((P2VAR(Exi_DIN_DC_EVSEStatusType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->DC_EVSEStatus);

      /* EVSEPresentVoltage */
      Scc_Conv_DIN2Scc_PhysicalValue(BodyPtr->EVSEPresentVoltage, &scc_PhysicalValueTmp);
      Scc_Set_DIN_EVSEPresentVoltage(&scc_PhysicalValueTmp);

      /* EVSEPresentCurrent */
      Scc_Conv_DIN2Scc_PhysicalValue(BodyPtr->EVSEPresentCurrent, &scc_PhysicalValueTmp);
      Scc_Set_DIN_EVSEPresentCurrent(&scc_PhysicalValueTmp);

      /* EVSECurrentLimitAchieved */
      Scc_Set_DIN_EVSECurrentLimitAchieved(BodyPtr->EVSECurrentLimitAchieved);

      /* EVSEVoltageLimitAchieved */
      Scc_Set_DIN_EVSEVoltageLimitAchieved(BodyPtr->EVSEVoltageLimitAchieved);

      /* EVSEPowerLimitAchieved */
      Scc_Set_DIN_EVSEPowerLimitAchieved(BodyPtr->EVSEPowerLimitAchieved);

      /* EVSEMaximumVoltageLimit */
      Scc_Conv_DIN2Scc_PhysicalValue(BodyPtr->EVSEMaximumVoltageLimit, &scc_PhysicalValueTmp);
      Scc_Set_DIN_EVSEMaximumVoltageLimit(&scc_PhysicalValueTmp,
        BodyPtr->EVSEMaximumVoltageLimitFlag);

      /* EVSEMaximumCurrentLimit */
      Scc_Conv_DIN2Scc_PhysicalValue(BodyPtr->EVSEMaximumCurrentLimit, &scc_PhysicalValueTmp);
      Scc_Set_DIN_EVSEMaximumCurrentLimit(&scc_PhysicalValueTmp, BodyPtr->EVSEMaximumCurrentLimitFlag);

      /* EVSEMaximumPowerLimit */
      Scc_Conv_DIN2Scc_PhysicalValue(BodyPtr->EVSEMaximumPowerLimit, &scc_PhysicalValueTmp);
      Scc_Set_DIN_EVSEMaximumPowerLimit(&scc_PhysicalValueTmp, BodyPtr->EVSEMaximumPowerLimitFlag);

      /* ResponseCode */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
} /* PRQA S 6050 */ /* MD_MSR_STCAL */

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_WeldingDetectionRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_DIN_WeldingDetectionRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  Scc_PhysicalValueType scc_PhysicalValueTmp;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_DIN_WeldingDetectionResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_DIN_WeldingDetectionResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_DIN_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_DIN_WELDING_DETECTION_RES_TYPE != Scc_ExiRx_DIN_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_DIN_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* DC_EVSEStatus */
      Scc_ExiRx_DIN_ReadEVSEStatusDC((P2VAR(Exi_DIN_DC_EVSEStatusType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->DC_EVSEStatus);

      /* EVSEPresentVoltage */
      Scc_Conv_DIN2Scc_PhysicalValue(BodyPtr->EVSEPresentVoltage, &scc_PhysicalValueTmp);
      Scc_Set_DIN_EVSEPresentVoltage(&scc_PhysicalValueTmp);

      /* ResponseCode */
      Scc_Set_DIN_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_DIN_ReadEVSEStatusDC
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiRx_DIN_ReadEVSEStatusDC(P2CONST(Exi_DIN_DC_EVSEStatusType, AUTOMATIC, SCC_VAR_NOINIT) EVSEStatusPtr)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Report the parameter from the response to the application. */
  /* EVSEIsolationStatus */
  Scc_Set_DIN_EVSEIsolationStatus(EVSEStatusPtr->EVSEIsolationStatus, EVSEStatusPtr->EVSEIsolationStatusFlag);
  /* EVSEStatusCode */
  Scc_Set_DIN_EVSEStatusCode(EVSEStatusPtr->EVSEStatusCode);
  /* NotificationMaxDelay */
  Scc_Set_DIN_NotificationMaxDelay(EVSEStatusPtr->NotificationMaxDelay);
  /* EVSENotification */
  Scc_Set_DIN_EVSENotification(EVSEStatusPtr->EVSENotification);

}

#define SCC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
/* PRQA L:NEST_STRUCTS */
/* PRQA L:RETURN_PATHS */

#endif /* SCC_SCHEMA_DIN */

/**********************************************************************************************************************
 *  END OF FILE: Scc_ExiRx_DIN.c
 *********************************************************************************************************************/
