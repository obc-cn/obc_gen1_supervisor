/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc_Cbk.h
 *        \brief  Smart Charging Communication Header File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *********************************************************************************************************************/
#if !defined (SCC_CBK_H)
# define SCC_CBK_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Scc_Types.h"
#include "TcpIp_Types.h"
#include "EthIf_Types.h"

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "MemMap.h"

/**********************************************************************************************************************
 *  Scc_Cbk_TL_RxIndication
 *********************************************************************************************************************/
/*! \brief       receive indication
 *  \details     -
 *  \param[in]   SockHnd               socket handle
 *  \param[in]   SourcePtr             source network address and port
 *  \param[in]   DataPtr               pointer to the received data
 *  \param[in]   DataLen               length of to the received data
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_Cbk_TL_RxIndication(
  Scc_SocketType SockHnd,
  P2VAR(Scc_SockAddrType, AUTOMATIC, SCC_APPL_DATA) SourcePtr,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr,
  uint16 DataLen);

/**********************************************************************************************************************
 *  Scc_Cbk_CopyTxData
 *********************************************************************************************************************/
/*! \brief       receive indication
 *  \details      -
 *  \param[in]   SocketId              socket handle
 *  \param[in]   BufPtr                pointer to the output buffer
 *  \param[in]   BufLengthPtr             Length of the output buffer
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
FUNC(BufReq_ReturnType, SCC_CODE) Scc_Cbk_CopyTxData(
    TcpIp_SocketIdType SocketId,
    P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr
);

/**********************************************************************************************************************
 *  Scc_Cbk_TL_TCPConnected
 *********************************************************************************************************************/
/*! \brief       TCP connected
 *  \details     -
 *  \param[in]   SockHnd            socket handle
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   FALSE
 *  \synchronous TRUE
 **********************************************************************************************************************/
extern FUNC(void, SCC_CODE) Scc_Cbk_TL_TCPConnected(Scc_SocketType SockHnd);

/**********************************************************************************************************************
 *  Scc_Cbk_TL_TCPEvent
 *********************************************************************************************************************/
/*! \brief       TCP event handling
 *  \details     -
 *  \param[in]   SockHnd            socket handle
 *  \param[in]   Event              socket event
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   FALSE
 *  \synchronous TRUE
 **********************************************************************************************************************/
extern FUNC(void, SCC_CODE) Scc_Cbk_TL_TCPEvent(Scc_SocketType SockHnd, IpBase_TcpIpEventType Event);

/**********************************************************************************************************************
 *  Scc_Cbk_TLS_CertChain
 *********************************************************************************************************************/
/*! \brief          TLS validation result for a server certificate chain
 *  \details        -
 *  \param[in]      SockHnd                socket handle
 *  \param[in,out]  validationResultPtr    Validation result (VALIDATION_OK, VALIDATION_UNKNOWN_CA)
 *  \param[in]      certChainPtr           pointer to start of the certificate chain
 *  \param[in]      certChainLen           length of the certificate chain
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 **********************************************************************************************************************/
extern FUNC(void, SCC_CODE) Scc_Cbk_TLS_CertChain(
  Scc_SocketType                            SockHnd,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA)    validationResultPtr,
  P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA)  certChainPtr,
  uint32                                    certChainLen );

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_Cbk_SLAC_FirmwareDownloadComplete
 *********************************************************************************************************************/
/*! \brief       callback that is called after the firmware download was processed and the firmware was started
 *  \details     -
 *  \param[in]   DownloadSuccessful           indicates if Firmware download was successful
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern FUNC(void, SCC_CODE) Scc_Cbk_SLAC_FirmwareDownloadComplete(boolean DownloadSuccessful);
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_Cbk_SLAC_FirmwareDownloadStart
 *********************************************************************************************************************/
/*! \brief       callback that is called after the firmware download was started
 *  \details     -
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern FUNC(void, SCC_CODE) Scc_Cbk_SLAC_FirmwareDownloadStart(void);
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_Cbk_SLAC_AssociationStatus
 *********************************************************************************************************************/
/*! \brief       callback function for ongoing association errors and status information
 *  \details     -
 *  \param[in]   AssociationStatus      the association status reported to the application
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern FUNC(void, SCC_CODE) Scc_Cbk_SLAC_AssociationStatus(uint8 AssociationStatus);
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_Cbk_SLAC_DLinkReady
 *********************************************************************************************************************/
/*! \brief       D-LINK READY indication informs higher layers about a change of PLC link status
 *  \details     -
 *  \param[in]   DLinkReady      the power line link state after SLAC was finished
 *  \param[in]   NMKPtr          the network membership key (NMK) that was established during the SLAC session
 *  \param[in]   NIDPtr          the network identifier (NID) that was established during the SLAC session
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern FUNC(void, SCC_CODE) Scc_Cbk_SLAC_DLinkReady(EthTrcv_LinkStateType DLinkReady,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) NMKPtr, P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) NIDPtr);
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_Cbk_SLAC_GetRandomizedDataBuffer
 *********************************************************************************************************************/
/*! \brief       the callback is used by the SLAC component to gain a random byte array
 *  \details     -
 *  \param[out]  RandomDataPtr      the buffer that must be randomized
 *  \param[in]   RandomDataLen      the length of the buffer
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern FUNC(void, SCC_CODE) Scc_Cbk_SLAC_GetRandomizedDataBuffer(P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) RandomDataPtr, uint16 RandomDataLen);
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_Cbk_SLAC_GetValidateToggles
 *********************************************************************************************************************/
/*! \brief       callback function to generate a random number of validation toggles
 *  \details     -
 *  \return      [1,2,3]      random number of validation toggles
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern FUNC(uint8, SCC_CODE) Scc_Cbk_SLAC_GetValidateToggles(void);
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_Cbk_SLAC_ToggleRequest
 *********************************************************************************************************************/
/*! \brief       callback to request the toggle process during the validation phase
 *  \details     -
 *  \param[in]   ToggleNum      number of BCB-Toggles the application must generate on the Control Pilot
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern FUNC(void, SCC_CODE) Scc_Cbk_SLAC_ToggleRequest(uint8 ToggleNum);
#endif /* SCC_ENABLE_SLAC_HANDLING */

/**********************************************************************************************************************
 *  Scc_Cbk_Eth_TransceiverLinkStateChange
 **********************************************************************************************************************/
/*! \brief        Called by lower layer (e.g. EthIf) to indicate a change of the transceiver link state
 *  \details      -
 *  \param[in]    CtrlIdx         index of the ethernet controller
 *  \param[in]    TrcvLinkState   new state of the tranceiver
 *  \pre          -
 *  \context      TASK|ISR2
 *  \reentrant    FALSE
 *  \synchronous  TRUE
 **********************************************************************************************************************/
extern FUNC(void, SCC_CODE) Scc_Cbk_Eth_TransceiverLinkStateChange(
  uint8 CtrlIdx,
  EthTrcv_LinkStateType TrcvLinkState);

/**********************************************************************************************************************
 *  Scc_Cbk_IP_AddressAssignmentChange
 *********************************************************************************************************************/
/*! \brief       IP address assignment change callback
 *  \details     -
 *  \param[in]   LocalAddrId           ID of the local address
 *  \param[in]   State                 state of the local address
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   FALSE
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern FUNC(void, SCC_CODE) Scc_Cbk_IP_AddressAssignmentChange(TcpIp_LocalAddrIdType LocalAddrId,
  TcpIp_IpAddrStateType State);

#define SCC_STOP_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "MemMap.h"

#endif  /* SCC_CBK_H */
/**********************************************************************************************************************
 *  END OF FILE: Scc_Cbk.h
 *********************************************************************************************************************/
