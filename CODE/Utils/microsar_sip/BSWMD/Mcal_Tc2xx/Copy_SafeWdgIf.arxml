<?xml version='1.0'?>
<AUTOSAR xmlns="http://autosar.org/schema/r4.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://autosar.org/schema/r4.0 AUTOSAR_4-0-3.xsd">
  <AR-PACKAGES>
    <AR-PACKAGE UUID="93d9a111-9d4c-47b6-98cb-fe5aec24c8b0">
      <SHORT-NAME>AURIX</SHORT-NAME>
      <ELEMENTS>
        <ECUC-MODULE-DEF UUID="03eaa8c5-137e-402e-823d-8d7776c1de4d">
          <SHORT-NAME>SafeWdgIf</SHORT-NAME>
          <SUPPORTED-CONFIG-VARIANTS>
            <SUPPORTED-CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</SUPPORTED-CONFIG-VARIANT>
            <SUPPORTED-CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</SUPPORTED-CONFIG-VARIANT>
          </SUPPORTED-CONFIG-VARIANTS>
          <CONTAINERS>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ccda81dc-46cf-4171-858b-7c947884702f">
              <SHORT-NAME>SafeWdgIfConfigSet</SHORT-NAME>
              <DESC>
                <L-2 L="FOR-ALL">This is the base container that contains the post-build selectable configuration parameters</L-2>
              </DESC>
              <MULTIPLE-CONFIGURATION-CONTAINER>true</MULTIPLE-CONFIGURATION-CONTAINER>
              <PARAMETERS>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ed1-5962-93b2-24d30b1fa49e">
                  <SHORT-NAME>SafeWdgIfIntWdgConfigIndex</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This contains the index of the internal safety watchdog configuration to be used for initialisation.
This parameter will be considered only if IntWdgOnly or IntCnRWdg_ExtTLFWindowWdg combination is selected.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>255</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ee1-5962-93b2-24d21b1fa49e">
                  <SHORT-NAME>SafeWdgIfExtWdgCicConfigIndex</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This contains the index of the external CIC watchdog configuration to be used for initialization. 
This parameter will be considered only if ExtCICWdg is selected.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>255</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="98ce938e-6ee1-5962-93b2-24d21b1fa49a">
                  <SHORT-NAME>SafeWdgIfExtWdgTlfConfigIndex</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This contains the index of the external TLF watchdog configuration to be used for initialization. 
This parameter will be considered only if ExtTLFWdg or or IntCnRWdg_ExtTLFWindowWdg is selected.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>255</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
              </PARAMETERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="a868aef7-06f0-5e10-9482-3b9f6ca77cb2">
              <SHORT-NAME>SafeWdgIfGeneral</SHORT-NAME>
              <DESC>
                <L-2 L="EN">General configuration parameters for SafeWdg interface module.</L-2>
              </DESC>
              <PARAMETERS>
                <ECUC-ENUMERATION-PARAM-DEF UUID="999ccf11-8b4f-4719-abde-2f2d65e8614a">
                  <SHORT-NAME>SafeWdgIfWdgCombination</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">This parameter selects the type of watchdog combination used.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>SWDG_INT_WDG_ONLY</DEFAULT-VALUE>
                  <LITERALS>
                    <ECUC-ENUMERATION-LITERAL-DEF>
                      <SHORT-NAME>SWDG_INT_WDG_ONLY</SHORT-NAME>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                    </ECUC-ENUMERATION-LITERAL-DEF>
                    <ECUC-ENUMERATION-LITERAL-DEF>
                      <SHORT-NAME>SWDG_INT_CNR_WDG_EXT_TLF_WINDOW_WDG</SHORT-NAME>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                    </ECUC-ENUMERATION-LITERAL-DEF>
                    <ECUC-ENUMERATION-LITERAL-DEF>
                      <SHORT-NAME>SWDG_EXT_CIC_WDG_ONLY</SHORT-NAME>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                    </ECUC-ENUMERATION-LITERAL-DEF>
                    <ECUC-ENUMERATION-LITERAL-DEF>
                      <SHORT-NAME>SWDG_EXT_TLF_WDG_ONLY</SHORT-NAME>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                    </ECUC-ENUMERATION-LITERAL-DEF>
                  </LITERALS>
                </ECUC-ENUMERATION-PARAM-DEF>
              </PARAMETERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
          </CONTAINERS>
        </ECUC-MODULE-DEF>
      </ELEMENTS>
    </AR-PACKAGE>
  </AR-PACKAGES>
</AUTOSAR>
