<?xml version='1.0'?>
<AUTOSAR xmlns="http://autosar.org/schema/r4.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://autosar.org/schema/r4.0 AUTOSAR_4-0-3.xsd">
  <AR-PACKAGES>
    <AR-PACKAGE UUID="93d9a111-9d4c-47b6-98cb-fe5aec24c8b0">
      <SHORT-NAME>AURIX</SHORT-NAME>
      <ELEMENTS>
        <ECUC-MODULE-DEF UUID="03eaa8c5-137e-402e-823d-8d7776c1de4d">
          <SHORT-NAME>TstHandler</SHORT-NAME>
          <SUPPORTED-CONFIG-VARIANTS>
            <SUPPORTED-CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</SUPPORTED-CONFIG-VARIANT>
            <SUPPORTED-CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</SUPPORTED-CONFIG-VARIANT>
          </SUPPORTED-CONFIG-VARIANTS>
          <CONTAINERS>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="3ea7abca-6ba4-4d14-befd-8bb9350fe86f">
              <SHORT-NAME>SlGeneral</SHORT-NAME>
              <DESC>
                <L-2 L="EN">General configuration (parameters) of the Test Handler driver software module.</L-2>
              </DESC>
              <PARAMETERS>
                <ECUC-INTEGER-PARAM-DEF UUID="6e2cdc5f-16e8-4669-b7d8-531a888e7acc">
                  <SHORT-NAME>SlMasterCoreId</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Id of the core that is used as master core.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>0</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
              </PARAMETERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ccda81dc-46cf-4171-858b-7c947884702f">
              <SHORT-NAME>SlConfigSet</SHORT-NAME>
              <DESC>
                <L-2 L="FOR-ALL">This is the base container that contains the post-build selectable configuration parameters</L-2>
              </DESC>
              <MULTIPLE-CONFIGURATION-CONTAINER>true</MULTIPLE-CONFIGURATION-CONTAINER>
              <PARAMETERS>
                <ECUC-INTEGER-PARAM-DEF UUID="6e2cdc5f-96e8-4669-b7d8-531a888e7acc">
                  <SHORT-NAME>SlCoreId</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">Core id for which this configuration needs to be used.</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>INFINEON_AURIX</ORIGIN>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                  <MAX>0</MAX>
                  <MIN>0</MIN>
                </ECUC-INTEGER-PARAM-DEF>
              </PARAMETERS>
              <SUB-CONTAINERS>
                <ECUC-PARAM-CONF-CONTAINER-DEF UUID="97e6fd09-f2da-4844-b2e0-ecc3dc70e5a3">
                  <SHORT-NAME>SlCPUxTestList</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This container contains the information about Test index, Test name and parameter set index </L-2>
                  </DESC>
                  <LOWER-MULTIPLICITY>1</LOWER-MULTIPLICITY>
                  <UPPER-MULTIPLICITY>200</UPPER-MULTIPLICITY>
                  <PARAMETERS>
                    <ECUC-INTEGER-PARAM-DEF UUID="6e2cdc5f-16e8-4669-b7d9-531a888e7acc">
                      <SHORT-NAME>SlTestIndex</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This parameter defines the test identifier which indicates combination of MTL Test and Parameter Set Index.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>0</DEFAULT-VALUE>
                      <MAX>255</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-ENUMERATION-PARAM-DEF UUID="abdb45c9-db94-4d8b-9921-a360a38f90b1">
                      <SHORT-NAME>SlTestName</SHORT-NAME>
                      <DESC>
                        <L-2 L="EN">This parameter defines the name of MTl test.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>SfrTst_SfrCmpTst</DEFAULT-VALUE>
                      <LITERALS>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>SfrTst_SfrCmpTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>SfrTst_SfrCrcTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>CpuBusMpuLfmTst_LfmTest</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>ClkmTst_ClkmTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>VltmTst_VltmTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>TrapTst_TrapTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>SriTst_SriTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>CpuMpuTst_CpuMpuTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>RegAccProtTst_RegAccProtTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>PmuEccEdcTst_EccEdcTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>WdgTst_WdgCpuTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>WdgTst_WdgSafetyTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>CpuTst_CpuSbstETst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>SramEccTst_SramEccTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>LockStepTst_LockStepTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>SpbTst_PeripheralRegAccProtTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>SpbTst_TimeoutTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>PflashMonTst_PflashMonTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>SffTst_SffTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>PhlSramTst_PhlSramTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>DmaTst_CRCTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>DmaTst_TimestampTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>DmaTst_SafeLinkedListTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>IomTst_IomTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>IRTst_IRTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>SmuTst_IrqTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>SmuTst_RtTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>SmuTst_NmiTst</SHORT-NAME>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                      </LITERALS>
                    </ECUC-ENUMERATION-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="6e2cdc5f-16e8-4669-b7d8-531a888eaacc">
                      <SHORT-NAME>SlParamSetIndex</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This parameter defines the parameter set index of the chosen MTL test Set Index.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>0</DEFAULT-VALUE>
                      <MAX>255</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                    <ECUC-INTEGER-PARAM-DEF UUID="6e2cdc5f-16e8-4669-b7d8-631a888e7acc">
                      <SHORT-NAME>SlSmuAlarm</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This parameter defines the alarm index within the alarm group 5 (ALM5). This parameter is only used
only by Runtime Test and elsewhere is ignored.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DEFAULT-VALUE>0</DEFAULT-VALUE>
                      <MAX>3</MAX>
                      <MIN>0</MIN>
                    </ECUC-INTEGER-PARAM-DEF>
                  </PARAMETERS>
                </ECUC-PARAM-CONF-CONTAINER-DEF>
                <ECUC-PARAM-CONF-CONTAINER-DEF UUID="97e6fd09-f2da-4844-b2e0-ecc3dc70e5a1">
                  <SHORT-NAME>SlEarlyPrerun</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This is the SlEarlyPrerun container information.</L-2>
                  </DESC>
                  <SUB-CONTAINERS>
                    <ECUC-PARAM-CONF-CONTAINER-DEF UUID="97e6fd09-f2da-4864-b2e0-ecc3dc70e5a8">
                      <SHORT-NAME>SlTestGroupEarlyPrerun</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This container contains SlTestGroupEarlyPrerun SlTestGroupIndex and  SlTestEntry list</L-2>
                      </DESC>
                      <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                      <UPPER-MULTIPLICITY>100</UPPER-MULTIPLICITY>
                      <PARAMETERS>
                        <ECUC-INTEGER-PARAM-DEF UUID="7e2cdc5f-16e8-4669-b7d8-631a888e7acc">
                          <SHORT-NAME>SlTestGroupIndex</SHORT-NAME>
                          <DESC>
                            <L-2 L="FOR-ALL">This parameter defines the early pre-run test group index.</L-2>
                          </DESC>
                          <IMPLEMENTATION-CONFIG-CLASSES>
                            <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                              <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                            </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                            <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                              <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                            </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          </IMPLEMENTATION-CONFIG-CLASSES>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                          <DEFAULT-VALUE>0</DEFAULT-VALUE>
                          <MAX>99</MAX>
                          <MIN>0</MIN>
                        </ECUC-INTEGER-PARAM-DEF>
                      </PARAMETERS>
                      <SUB-CONTAINERS>
                        <ECUC-PARAM-CONF-CONTAINER-DEF UUID="97e6fd08-f2da-4844-b2e0-ecc3dc70e5a0">
                          <SHORT-NAME>SlTestEntry</SHORT-NAME>
                          <DESC>
                            <L-2 L="FOR-ALL">This container contains references to tests from SlCPUxTestList.</L-2>
                          </DESC>
                          <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                          <UPPER-MULTIPLICITY>5</UPPER-MULTIPLICITY>
                          <REFERENCES>
                            <ECUC-REFERENCE-DEF UUID="4e8f054e-d56f-4410-b256-6dc3e70169d6">
                              <SHORT-NAME>Test</SHORT-NAME>
                              <DESC>
                                <L-2 L="EN">Parameter set to be used for the chosen test.</L-2>
                              </DESC>
                              <IMPLEMENTATION-CONFIG-CLASSES>
                                <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                  <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                                  <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                                </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                  <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                                  <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                                </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              </IMPLEMENTATION-CONFIG-CLASSES>
                              <ORIGIN>INFINEON_AURIX</ORIGIN>
                              <DESTINATION-REF DEST="ECUC-PARAM-CONF-CONTAINER-DEF">/AURIX/TstHandler/SlConfigSet/SlCPUxTestList</DESTINATION-REF>
                            </ECUC-REFERENCE-DEF>
                          </REFERENCES>
                        </ECUC-PARAM-CONF-CONTAINER-DEF>
                      </SUB-CONTAINERS>
                    </ECUC-PARAM-CONF-CONTAINER-DEF>
                  </SUB-CONTAINERS>
                </ECUC-PARAM-CONF-CONTAINER-DEF>
                <ECUC-PARAM-CONF-CONTAINER-DEF UUID="97e6fd09-f2da-4844-b2e0-ecc3dc70e5a9">
                  <SHORT-NAME>SlPrerun</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This  container contains the information about SlPrerun SlTestGroupPrerun, SlTestGroupIndex and SlTestEntry</L-2>
                  </DESC>
                  <REFERENCES>
                    <ECUC-CHOICE-REFERENCE-DEF UUID="4e8f054e-d56f-4410-b256-6dc3e70169d9">
                      <SHORT-NAME>SlSmuConfigPrerunRef</SHORT-NAME>
                      <DESC>
                        <L-2 L="EN">Reference to TestLib function.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DESTINATION-REFS>
                        <DESTINATION-REF DEST="ECUC-PARAM-CONF-CONTAINER-DEF">/AURIX/Smu/SmuConfigSet</DESTINATION-REF>
                      </DESTINATION-REFS>
                    </ECUC-CHOICE-REFERENCE-DEF>
                  </REFERENCES>
                  <SUB-CONTAINERS>
                    <ECUC-PARAM-CONF-CONTAINER-DEF UUID="97e6fd09-f2da-4844-b2e0-ecc3dc70e5a6">
                      <SHORT-NAME>SlTestGroupPrerun</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This container containes the information about SlTestGroupIndex, Index to identify a group of tests to run during pre-run phase.</L-2>
                      </DESC>
                      <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                      <UPPER-MULTIPLICITY>100</UPPER-MULTIPLICITY>
                      <PARAMETERS>
                        <ECUC-INTEGER-PARAM-DEF UUID="9e2cdc5f-16e8-4669-b7d8-531a808e7acc">
                          <SHORT-NAME>SlTestGroupIndex</SHORT-NAME>
                          <DESC>
                            <L-2 L="FOR-ALL">This parameter defines the pre-run test group index.</L-2>
                          </DESC>
                          <IMPLEMENTATION-CONFIG-CLASSES>
                            <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                              <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                            </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                            <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                              <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                            </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          </IMPLEMENTATION-CONFIG-CLASSES>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                          <DEFAULT-VALUE>0</DEFAULT-VALUE>
                          <MAX>99</MAX>
                          <MIN>0</MIN>
                        </ECUC-INTEGER-PARAM-DEF>
                      </PARAMETERS>
                      <SUB-CONTAINERS>
                        <ECUC-PARAM-CONF-CONTAINER-DEF UUID="91e6fd09-f2da-4841-b2e0-ecc3dc70e9a3">
                          <SHORT-NAME>SlTestEntry</SHORT-NAME>
                          <DESC>
                            <L-2 L="FOR-ALL">This container contains references to tests from SlCPUxTestList.</L-2>
                          </DESC>
                          <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                          <UPPER-MULTIPLICITY>10</UPPER-MULTIPLICITY>
                          <REFERENCES>
                            <ECUC-REFERENCE-DEF UUID="4e8f054e-d56f-4410-b256-6dc3e70169d0">
                              <SHORT-NAME>Test</SHORT-NAME>
                              <DESC>
                                <L-2 L="EN">Parameter set to be used for the chosen test.</L-2>
                              </DESC>
                              <IMPLEMENTATION-CONFIG-CLASSES>
                                <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                  <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                                  <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                                </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                  <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                                  <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                                </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              </IMPLEMENTATION-CONFIG-CLASSES>
                              <ORIGIN>INFINEON_AURIX</ORIGIN>
                              <DESTINATION-REF DEST="ECUC-PARAM-CONF-CONTAINER-DEF">/AURIX/TstHandler/SlConfigSet/SlCPUxTestList</DESTINATION-REF>
                            </ECUC-REFERENCE-DEF>
                          </REFERENCES>
                        </ECUC-PARAM-CONF-CONTAINER-DEF>
                      </SUB-CONTAINERS>
                    </ECUC-PARAM-CONF-CONTAINER-DEF>
                  </SUB-CONTAINERS>
                </ECUC-PARAM-CONF-CONTAINER-DEF>
                <ECUC-PARAM-CONF-CONTAINER-DEF UUID="97e6fd09-f2da-4844-b2e0-ecc3dc70e5a4">
                  <SHORT-NAME>SlRuntime</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This container contains the information about SlRuntime information</L-2>
                  </DESC>
                  <REFERENCES>
                    <ECUC-CHOICE-REFERENCE-DEF UUID="4e8f054e-d56f-4410-b256-6dc3e80169d6">
                      <SHORT-NAME>SlSmuConfigRuntimeRef</SHORT-NAME>
                      <DESC>
                        <L-2 L="EN">Reference to TestLib function.</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>INFINEON_AURIX</ORIGIN>
                      <DESTINATION-REFS>
                        <DESTINATION-REF DEST="ECUC-PARAM-CONF-CONTAINER-DEF">/AURIX/Smu/SmuConfigSet</DESTINATION-REF>
                      </DESTINATION-REFS>
                    </ECUC-CHOICE-REFERENCE-DEF>
                  </REFERENCES>
                  <SUB-CONTAINERS>
                    <ECUC-PARAM-CONF-CONTAINER-DEF UUID="99e6fd09-f2da-4844-b2e0-ecc3dc70e5a3">
                      <SHORT-NAME>SlTestGroupRuntime</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This container contains the information about SlTestGroupRuntime information</L-2>
                      </DESC>
                      <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                      <UPPER-MULTIPLICITY>100</UPPER-MULTIPLICITY>
                      <PARAMETERS>
                        <ECUC-INTEGER-PARAM-DEF UUID="6e2cdc5f-26e8-4669-b7d8-531a888e7acc">
                          <SHORT-NAME>SlTestGroupIndex</SHORT-NAME>
                          <DESC>
                            <L-2 L="FOR-ALL">This parameter defines the run-time test group index.</L-2>
                          </DESC>
                          <IMPLEMENTATION-CONFIG-CLASSES>
                            <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                              <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                            </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                            <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                              <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                            </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          </IMPLEMENTATION-CONFIG-CLASSES>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                          <DEFAULT-VALUE>0</DEFAULT-VALUE>
                          <MAX>99</MAX>
                          <MIN>0</MIN>
                        </ECUC-INTEGER-PARAM-DEF>
                      </PARAMETERS>
                      <SUB-CONTAINERS>
                        <ECUC-PARAM-CONF-CONTAINER-DEF UUID="97e6fd07-f2da-4844-b2e0-ecc3dc70e5a9">
                          <SHORT-NAME>SlTestEntry</SHORT-NAME>
                          <DESC>
                            <L-2 L="FOR-ALL">This container contains references to tests from SlCPUxTestList.</L-2>
                          </DESC>
                          <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                          <UPPER-MULTIPLICITY>5</UPPER-MULTIPLICITY>
                          <REFERENCES>
                            <ECUC-REFERENCE-DEF UUID="4e8f054e-d56f-4410-b256-6dc3e70169d8">
                              <SHORT-NAME>Test</SHORT-NAME>
                              <DESC>
                                <L-2 L="EN">Parameter set to be used for the chosen test.</L-2>
                              </DESC>
                              <IMPLEMENTATION-CONFIG-CLASSES>
                                <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                  <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                                  <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                                </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                  <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                                  <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                                </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              </IMPLEMENTATION-CONFIG-CLASSES>
                              <ORIGIN>INFINEON_AURIX</ORIGIN>
                              <DESTINATION-REF DEST="ECUC-PARAM-CONF-CONTAINER-DEF">/AURIX/TstHandler/SlConfigSet/SlCPUxTestList</DESTINATION-REF>
                            </ECUC-REFERENCE-DEF>
                          </REFERENCES>
                        </ECUC-PARAM-CONF-CONTAINER-DEF>
                      </SUB-CONTAINERS>
                    </ECUC-PARAM-CONF-CONTAINER-DEF>
                  </SUB-CONTAINERS>
                </ECUC-PARAM-CONF-CONTAINER-DEF>
                <ECUC-PARAM-CONF-CONTAINER-DEF UUID="97e6fd09-f2da-4844-b2e0-ecc3dc70e5a8">
                  <SHORT-NAME>SlPostrun</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">This container contains the information about SlTestGroupPostrun.</L-2>
                  </DESC>
                  <SUB-CONTAINERS>
                    <ECUC-PARAM-CONF-CONTAINER-DEF UUID="97e6fd09-f2da-4844-b2e0-ecc3dc70e5a5">
                      <SHORT-NAME>SlTestGroupPostrun</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">This container contains the information about SlTestGroupPostrun </L-2>
                      </DESC>
                      <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                      <UPPER-MULTIPLICITY>100</UPPER-MULTIPLICITY>
                      <PARAMETERS>
                        <ECUC-INTEGER-PARAM-DEF UUID="6e2cdc5f-16e8-4669-b7d8-521a888e7acc">
                          <SHORT-NAME>SlTestGroupIndex</SHORT-NAME>
                          <DESC>
                            <L-2 L="FOR-ALL">This parameter defines the post-run test group index.</L-2>
                          </DESC>
                          <IMPLEMENTATION-CONFIG-CLASSES>
                            <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                              <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                            </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                            <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                              <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                            </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          </IMPLEMENTATION-CONFIG-CLASSES>
                          <ORIGIN>INFINEON_AURIX</ORIGIN>
                          <DEFAULT-VALUE>0</DEFAULT-VALUE>
                          <MAX>99</MAX>
                          <MIN>0</MIN>
                        </ECUC-INTEGER-PARAM-DEF>
                      </PARAMETERS>
                      <SUB-CONTAINERS>
                        <ECUC-PARAM-CONF-CONTAINER-DEF UUID="97e6fd09-f2da-4844-b2e0-ecc3dc70e8a6">
                          <SHORT-NAME>SlTestEntry</SHORT-NAME>
                          <DESC>
                            <L-2 L="FOR-ALL">This container contains references to tests from SlCPUxTestList.</L-2>
                          </DESC>
                          <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                          <UPPER-MULTIPLICITY>5</UPPER-MULTIPLICITY>
                          <REFERENCES>
                            <ECUC-REFERENCE-DEF UUID="4e8f054e-d56f-4410-b256-6dc3e70169d7">
                              <SHORT-NAME>Test</SHORT-NAME>
                              <DESC>
                                <L-2 L="EN">Parameter set to be used for the chosen test.</L-2>
                              </DESC>
                              <IMPLEMENTATION-CONFIG-CLASSES>
                                <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                  <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                                  <CONFIG-VARIANT>VARIANT-POST-BUILD-LOADABLE</CONFIG-VARIANT>
                                </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                                  <CONFIG-CLASS>POST-BUILD</CONFIG-CLASS>
                                  <CONFIG-VARIANT>VARIANT-POST-BUILD-SELECTABLE</CONFIG-VARIANT>
                                </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                              </IMPLEMENTATION-CONFIG-CLASSES>
                              <ORIGIN>INFINEON_AURIX</ORIGIN>
                              <DESTINATION-REF DEST="ECUC-PARAM-CONF-CONTAINER-DEF">/AURIX/TstHandler/SlConfigSet/SlCPUxTestList</DESTINATION-REF>
                            </ECUC-REFERENCE-DEF>
                          </REFERENCES>
                        </ECUC-PARAM-CONF-CONTAINER-DEF>
                      </SUB-CONTAINERS>
                    </ECUC-PARAM-CONF-CONTAINER-DEF>
                  </SUB-CONTAINERS>
                </ECUC-PARAM-CONF-CONTAINER-DEF>
              </SUB-CONTAINERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
          </CONTAINERS>
        </ECUC-MODULE-DEF>
      </ELEMENTS>
    </AR-PACKAGE>
  </AR-PACKAGES>
</AUTOSAR>
