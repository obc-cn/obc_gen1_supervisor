
using Communication;
using DiagnosticsCommunicationStack;
using HardwareAbstractionLayer;
using Middleware;
using SecurityAccessStack;
using TestEnvironment;



ulong MONO  =  1U;
ulong TRI   =  2U;
double sqrt3 = 1.73205080757;
double sqrt2 = 1.41421356237;


//Set mode
ulong MODE  =  TRI;
double AC_in = 220.0;

Console.WriteLine("Disable the internal communication fault using the external CAN (DID: 0xFD11)");



CANUserConfiguration cuc = (CANUserConfiguration)MTFConfigurationUtils.CommunicationUserConfigurations["Int_CAN_simulated"]["OBC_DCDC"];

CAN_Manager canManager = (CAN_Manager)cuc.CommunicationManager;
CAN_DDBB canDatabase = (CAN_DDBB)cuc.CommunicationDatabase;


// Get CAN signal object
CAN_Signal PFC_SupReq = (CAN_Signal)canDatabase.Signals["SUP_RequestPFCStatus"];
CAN_Signal DCHV_SupReq = (CAN_Signal)canDatabase.Signals["SUP_RequestStatusDCHV"];

CAN_Signal PFC_Status = (CAN_Signal)canDatabase.Signals["PFC_PFCStatus"];
CAN_Signal DCHV_status = (CAN_Signal)canDatabase.Signals["DCHV_DCHVStatus"];

CAN_Signal DClink_Req =  (CAN_Signal)canDatabase.Signals["SUP_CommandVDCLink_V"];
CAN_Signal DClink_State =  (CAN_Signal)canDatabase.Signals["PFC_Vdclink_V"];


((CAN_Signal)canDatabase.Signals["PFC_VPH12_RMS_V"]).RealValue = 0.0;
((CAN_Signal)canDatabase.Signals["PFC_VPH23_RMS_V"]).RealValue = 0.0;
((CAN_Signal)canDatabase.Signals["PFC_VPH31_RMS_V"]).RealValue = 0.0;

PFC_Status.RawValue = 0x00;
DCHV_status.RawValue = 0x00;
System.Threading.Thread.Sleep(3000);


if(MODE==TRI)
{

    ((CAN_Signal)canDatabase.Signals["PFC_VPH12_RMS_V"]).RealValue = AC_in*sqrt3;
    ((CAN_Signal)canDatabase.Signals["PFC_VPH23_RMS_V"]).RealValue = AC_in*sqrt3;
    ((CAN_Signal)canDatabase.Signals["PFC_VPH31_RMS_V"]).RealValue = AC_in*sqrt3;
    
    ((CAN_Signal)canDatabase.Signals["PFC_VPH12_Peak_V"]).RealValue =  AC_in*sqrt3*sqrt2;
    ((CAN_Signal)canDatabase.Signals["PFC_VPH23_Peak_V"]).RealValue =  AC_in*sqrt3*sqrt2;
    ((CAN_Signal)canDatabase.Signals["PFC_VPH31_Peak_V"]).RealValue =  AC_in*sqrt3*sqrt2;
    
    Console.WriteLine("Set charge allow now.");
    System.Threading.Thread.Sleep(1000);
    MODE = 0;
}
else if(MODE==MONO)
{


    ((CAN_Signal)canDatabase.Signals["PFC_VPH12_RMS_V"]).RealValue = 0.0;
    ((CAN_Signal)canDatabase.Signals["PFC_VPH23_RMS_V"]).RealValue = 0.0;
    ((CAN_Signal)canDatabase.Signals["PFC_VPH31_RMS_V"]).RealValue = 0.0;
    
    ((CAN_Signal)canDatabase.Signals["PFC_VPH12_Peak_V"]).RealValue =  0.0;
    ((CAN_Signal)canDatabase.Signals["PFC_VPH23_Peak_V"]).RealValue = 0.0;
    ((CAN_Signal)canDatabase.Signals["PFC_VPH31_Peak_V"]).RealValue = 0.0;


    Console.WriteLine("Set charge allow now.");
    System.Threading.Thread.Sleep(5000);

    ((CAN_Signal)canDatabase.Signals["PFC_VPH12_RMS_V"]).RealValue = AC_in;
    ((CAN_Signal)canDatabase.Signals["PFC_VPH23_RMS_V"]).RealValue = 0.0;
    ((CAN_Signal)canDatabase.Signals["PFC_VPH31_RMS_V"]).RealValue = AC_in;
    
    ((CAN_Signal)canDatabase.Signals["PFC_VPH12_Peak_V"]).RealValue =  AC_in*sqrt2;
    ((CAN_Signal)canDatabase.Signals["PFC_VPH23_Peak_V"]).RealValue = 0.0;
    ((CAN_Signal)canDatabase.Signals["PFC_VPH31_Peak_V"]).RealValue =  AC_in*sqrt2;
    
    MODE = 0;

}


 while(true)
{
   

    if(PFC_SupReq.RawValue<=0x02) PFC_Status.RawValue = PFC_SupReq.RawValue;
    else PFC_Status.RawValue = 0x00;
    
    if(DCHV_SupReq.RawValue<=0x01) DCHV_status.RawValue = DCHV_SupReq.RawValue;
    else DCHV_status.RawValue = 0x00;
    
    if(PFC_SupReq.RawValue==0x01 || PFC_SupReq.RawValue<=0x02) DClink_State.RealValue = DClink_Req.RealValue;
    else DClink_State.RealValue = ((CAN_Signal)canDatabase.Signals["PFC_VPH12_Peak_V"]).RealValue;
    
    
    ((CAN_Signal)canDatabase.Signals["DCHV_ADC_VOUT"]).RealValue = ((CAN_Signal)canDatabase.Signals["DCDC_VoltageReference"]).RealValue;
    ((CAN_Signal)canDatabase.Signals["DCHV_ADC_IOUT"]).RealValue = ((CAN_Signal)canDatabase.Signals["DCDC_CurrentReference"]).RealValue;
    
    ((CAN_Signal)canDatabase.Signals["DCLV_Input_Voltage"]).RealValue = ((CAN_Signal)canDatabase.Signals["DCHV_ADC_VOUT"]).RealValue;
    
    
    System.Threading.Thread.Sleep(10);
    
}
