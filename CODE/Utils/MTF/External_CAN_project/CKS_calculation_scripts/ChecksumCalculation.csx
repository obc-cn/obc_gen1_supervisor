using Communication;

// Don't change header of class or methods.
// Don't add, delete or modify usings of this file.
// Don't change the name of ChecksumCalculation class or CalculateChecksum method or method parameters.
// Only complete the body of CalculateChecksum method.
private class ChecksumCalculation : Base_FrameMessage.IChecksumCalculation
{
	// Method for calculate checksum. Inside frameMessage object you can extract all parameters that you need for calculation
	public byte CalculateChecksum(Base_FrameMessage frameMessage)
    {
		byte checksum = 0;
		uint id = frameMessage.ID;
		byte length = frameMessage.Length;
		List<byte> rawData = frameMessage.RawData;
	
	
        // Type your code for checksum calculation here
		int index = 0;
		
		checksum = 3;
		
		for(index=0;index<frameMessage.Length;index++)
		{
			checksum = (byte)((byte)((byte)(rawData[index] & (byte)0x0F)+(byte)(rawData[index]>>4 & (byte)0x0F))+checksum);
			checksum = (byte)(checksum&(byte)0x0F);
	    }
		checksum = (byte)((byte)0x0F-checksum);

        return checksum;
    }
	
	// Type here if you need do additional methods for help to checksum calculation
	// ...
}
