/*@!Encoding:1252*/
/* ============================================================ */
/* ============= OBC_DCDC can node manual control ============= */
/* ============================================================ */
/*                                                              */
/* General:                                                     */
/*    - OBC_DCDC (CAN node in charge to manage the Vehicle      */
/*      charging functions)                                     */
/*    - Frames/Signals based on spec:                           */
/*      "01552_18_03671_eCMP_OBC_DCDC_CAN_Messaging_V3.0.xlsx"  */
/*                                                              */
/* Description:                                                 */
/*    - Simulates the OBC_DCDC periodic frames' TX when active  */
/*    - All relevant TX signals from OBC_DCDC node are tunnable */
/*      by Panel_syssim                                         */
/*                                                              */



/* ------------------------------------------------------------ */
/* --------------- INTERNAL VARIABLES Definition ---------------*/
/* ------------------------------------------------------------ */
variables
{
  msTimer timer100ms;
  msTimer timer10ms; 
  
  message OBC1 frameOBC1;
  message OBC2 frameOBC2;
  message DC2 frameDC2;
  
  long myWord;
  char string[100];
}



/* ------------------------------------------------------------ */
/* ------------------- FUNCTIONS Definition ------------------- */
/* ------------------------------------------------------------ */
void CalculateE2Efields(byte payload[], byte chk_Qindex, byte pc_Qindex, byte chk_ini)
{
  byte i;
  byte sum;
  byte chk, pc;
  
  // Caculate Process Counter value
  // - Current pc
  if(pc_Qindex%2 == 0)
  {
    pc = (payload.byte(pc_Qindex/2) & 0xF0) >> 4;
  }
  else
  {
    pc = payload.byte(pc_Qindex/2) & 0x0F;
  }
  // - Get new pc needed
  if(pc < 15)
  {
    pc = pc + 1;
  }
  else
  {
    pc = 0;
  }
  
  // - Write new pc into the correct nibble
  if(pc_Qindex%2 == 0)
  {
    payload.byte(pc_Qindex/2) = ((pc & 0x0F) << 4) | (payload.byte(pc_Qindex/2) & 0x0F);
  }
  else
  {
    payload.byte(pc_Qindex/2) = (payload.byte(pc_Qindex/2) & 0xF0) | (pc & 0x0F);
  }
  
  // Caculate Checksum value
  //  - Sum of all payload nibbles + chk_ini (except the Checksum field)
  sum = chk_ini;
  for(i = 0; i < elCount(payload); i++)
  {
    if(i != (chk_Qindex/2))
    {
      sum = sum + ((payload.byte(i) >> 4) & 0x0F);
      sum = sum + payload.byte(i) & 0x0F;
    }
    else if((chk_Qindex%2) == 0)
    {
      sum = sum + payload.byte(i) & 0x0F;
    }
    else
    {
      sum = sum + ((payload.byte(i) >> 4) & 0x0F);
    }
  }
  // - Get new chk needed
  chk = 0x0F - (sum & 0x0F);
    
  // - Write new chk into the correct nibble
  if(chk_Qindex%2 == 0)
  {
    payload.byte(chk_Qindex/2) = ((chk & 0x0F) << 4) | (payload.byte(chk_Qindex/2) & 0x0F);
  }else
  {
    payload.byte(chk_Qindex/2) = (payload.byte(chk_Qindex/2) & 0xF0) | (chk & 0x0F);
  }
}

void CheckE2Efields(byte payload[], byte pc_Qindex, byte chk_ini, byte pc_prev, byte chk_err, byte pc_err)
{
  byte i;
  byte sum;
  byte pc;

  // Check Checksum value
  //  - Sum of all payload nibbles + chk_ini (including the Checksum field)
  sum = chk_ini;
  for(i = 0; i < elCount(payload); i++)
  {
    sum = sum + payload.byte(i);
  }
  // - Crosscheck chk
  if((sum & 0x0F) == 0x0F)
  {
    chk_err = 0;  //OK
  }
  else
  {
    chk_err = 1;  //NOK
  }
  
  // Check Process Counter value
  // - Current pc
  if(pc_Qindex%2 == 0)
  {
    pc = (payload.byte(pc_Qindex/2) & 0xF0) >> 4;
  }
  else
  {
    pc = payload.byte(pc_Qindex/2) & 0x0F;
  }
  // - Crosscheck pc
  if(pc != pc_prev )
  {
    pc_err = 0;   //OK
  }
  else
  {
    pc_err = 1;   //NOK
  }
}



/* ------------------------------------------------------------ */
/* ---------------------- EVENT Handlers ---------------------- */
/* ------------------------------------------------------------ */
/* Executed just after CANoe starts */
on start
{
  // Init internal variables
  myWord = 0;
  string[0] = '\n';

  // Stop periodic frames' TX
  cancelTimer(timer100ms);
  cancelTimer(timer10ms);
}



/* Executed every 100ms: Sending Frames ... */
on timer timer100ms
{
  byte i;
  byte data[8];
  
  // Update E2E fields for OBC1
  for(i = 0; i < 8 ; i++)
  {
    data[i] = frameOBC1.byte(i);
  } 
  CalculateE2Efields(data, 15, 14, 0x00);
  for(i = 0; i < 8 ; i++)
  {
    frameOBC1.byte(i) = data[i];
  }
  
  // Send OBC1
  output(frameOBC1);
  
  // Update E2E fields for OBC2
  for(i = 0; i < 8 ; i++)
  {
    data[i] = frameOBC2.byte(i);
  }
  CalculateE2Efields(data, 13, 12, 0x0F);
  for(i = 0; i < 8 ; i++)
  {
    frameOBC2.byte(i) = data[i];
  }
  
  // Send OBC2
  output(frameOBC2);
}

/* Executed every 10ms: Sending Frames ... */
on timer timer10ms
{
  // Update Checksum and RollingCounter for DC2
  // Not required
  
  // Send DC2 frame
  output(frameDC2);
}



/*  Executed when 'Panel_syssim::OBC_DCDC_enable' var changes */
on sysvar Panel_syssim::OBC_DCDC_enable
{
  // Start/Stop sending frames

  if(@Panel_syssim::OBC_DCDC_enable == 1)
  {
    // Debug info
    snprintf(string, elcount(string), "[OBC_DCDC] ---- OBC_DCDC manual control simulation started!", myWord);
    write(string);

    // Start sending frames
    setTimerCyclic(timer100ms, 100);
    setTimerCyclic(timer10ms, 10);
  }
  else
  {
    // Debug info
    snprintf(string, elcount(string), "[OBC_DCDC] ---- OBC_DCDC manual control simulation stopped!", myWord);
    write(string);

    // Stop sending frames
    cancelTimer(timer100ms);    
    cancelTimer(timer10ms);
  }
}

/*  Executed when 'Panel_syssim::OBC_DCDC_defaults' var changes */
on sysvar Panel_syssim::OBC_DCDC_defaults
{
  // Reset all frames with its defaults values

  if(@Panel_syssim::OBC_DCDC_defaults == 1)
  {
    // Debug info
    snprintf(string, elcount(string), "[OBC_DCDC] Reset frames with its defaults values", myWord);
    write(string);

    // Reseting values
    @Panel_syssim::OBC_DCDC_OBC_HighVoltConnectionAllowed = 0;
    @Panel_syssim::OBC_DCDC_OBC_ChargingConnectionConfirmation = 0;
    @Panel_syssim::OBC_DCDC_OBC_CP_connection_Status = 0;
    @Panel_syssim::OBC_DCDC_OBC_PlugVoltDetection = 0;
    @Panel_syssim::OBC_DCDC_OBC_OBCStartSt = 0;
    @Panel_syssim::OBC_DCDC_OBC_ChargingMode = 0;
    @Panel_syssim::OBC_DCDC_DCDC_OBCMainContactorReq = 0;
    @Panel_syssim::OBC_DCDC_DCDC_OBCQuickChargeContactorReq = 0;
  }
}

/*  Executed when 'Panel_syssim::OBC_DCDC_xxx' var changes */
on sysvar Panel_syssim::OBC_DCDC_OBC_HighVoltConnectionAllowed
{
  frameOBC1.OBC_HighVoltConnectionAllowed = @Panel_syssim::OBC_DCDC_OBC_HighVoltConnectionAllowed;
}
on sysvar Panel_syssim::OBC_DCDC_OBC_ChargingConnectionConfirmation
{
  frameOBC2.OBC_ChargingConnectionConfirmati = @Panel_syssim::OBC_DCDC_OBC_ChargingConnectionConfirmation;
}
on sysvar Panel_syssim::OBC_DCDC_OBC_CP_connection_Status
{
  frameOBC1.OBC_CP_connection_Status = @Panel_syssim::OBC_DCDC_OBC_CP_connection_Status;
}
on sysvar Panel_syssim::OBC_DCDC_OBC_PlugVoltDetection
{
  frameOBC2.OBC_PlugVoltDetection = @Panel_syssim::OBC_DCDC_OBC_PlugVoltDetection;
}
on sysvar Panel_syssim::OBC_DCDC_OBC_OBCStartSt
{
  frameOBC2.OBC_OBCStartSt = @Panel_syssim::OBC_DCDC_OBC_OBCStartSt;
}
on sysvar Panel_syssim::OBC_DCDC_OBC_ChargingMode
{
  frameOBC1.OBC_ChargingMode = @Panel_syssim::OBC_DCDC_OBC_ChargingMode;
}
on sysvar Panel_syssim::OBC_DCDC_DCDC_OBCMainContactorReq
{
  frameDC2.DCDC_OBCMainContactorReq = @Panel_syssim::OBC_DCDC_DCDC_OBCMainContactorReq;
}
on sysvar Panel_syssim::OBC_DCDC_DCDC_OBCQuickChargeContactorReq
{
  frameDC2.DCDC_OBCQuickChargeContactorReq = @Panel_syssim::OBC_DCDC_DCDC_OBCQuickChargeContactorReq;
}



/*  Executed when any signal from 'frameOBC1' variable changes */
