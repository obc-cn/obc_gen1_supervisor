/*@!Encoding:1252*/
/* ============================================================ */
/* ================= TBMU can node simulation ================= */
/* ============================================================ */
/*                                                              */
/* General:                                                     */
/*    - TBMU (CAN node in charge to manage the Battery Monitor  */
/*      System communications)                                  */
/*    - Frames/Signals based on spec:                           */
/*      "01552_18_03671_eCMP_OBC_DCDC_CAN_Messaging_V3.0.xlsx"  */
/*    - Node behavior based on diagram:                         */
/*      "sequence_diagram_eCMP_v1.pdf"                          */
/*                                                              */
/* INPUTS:                                                      */
/*    A1) Frames RX (from OBC_DCDC to TBMU)                     */
/*        + OBC2: Periodic - 100ms                              */
/*        + DC2: Periodic - 10ms                                */
/*                                                              */
/*        .Note: there are more Frames RX, but they             */
/*        are not relevant for this simulation                  */
/*                                                              */
/*    B1) Signals RX (from OBC_DCDC to TBMU)                    */
/*        + [Frame OBC2] OBC_OBCStartSt: OBC allows starting    */
/*          a charge process.                                   */
/*            -Value=0 -> OBC not ready                         */
/*            -Value=1 -> OBC ready [AC: CC connected and       */
/*               power confirmed through CP] [DC: CC connected  */
/*               and power confirmed through HLC]               */
/*        + [Frame DC2] DCDC_OBCQuickChargeContactorReq: Inlet  */
/*          Quick Charge Connector Open/Close request           */
/*            - Value=0 -> Request to Open the Contactors       */
/*            - Value=1 -> Request to Close the Contactors      */
/*            - Value=2 -> No request                           */
/*                                                              */
/*        .Note: there are more Signals RX, but they            */
/*        are not relevant for this simulation                  */
/*                                                              */
/*    B2) Signals RX (from E_VCU to TBMU)                       */
/*        -simulated by CANoe internal vars-                    */
/*        + [E_VCU_sigsim NameSpace] VCU_BattMainConnectorCmd:  */
/*          Command for openning/closing the Battery Main       */
/*          Connector                                           */
/*            - Value=0 -> Open                                 */
/*            - Value=1 -> Close                                */
/*                                                              */
/*        + [E_VCU_sigsim NameSpace] VCU_AccPowerConsumption:   */
/*          Power consumption of the battery, used as input for */
/*          calculating the charging current and voltage        */
/*            -Values=0W-11000W                                 */
/*                                                              */
/*                                                              */
/* OUTPUTS                                                      */
/*    A1) Frames TX (from TBMU to OBC_DCDC)                     */
/*        + BMS1: Periodic - 20ms                               */
/*        + BMS9: Periodic - 20ms                               */
/*        + BMS6: Periodic - 100ms                              */
/*                                                              */
/*        .Note: there are more Frames RX, but they             */
/*        are not relevant for this simulation:                 */
/*                                                              */
/*          - BMS5: Periodic - 100ms                            */
/*          - BMS8: Periodic - 100ms                            */
/*          - BMS3: Periodic - 20ms                             */
/*                                                              */
/*    B1) Signals TX (from TBMU to OBC_DCDC)                    */
/*      + [Frame BMS1] BMS_SOC: Current Battery state of charge */
/*            -Values=0%-100%                                   */
/*      + [Frame BMS1] BMS_Voltage: Current Battery voltage     */
/*            -Values=0V-500V                                   */
/*      + [Frame BMS1] BMS_MainConnectorState: Battery Main     */
/*        Connector state                                       */
/*            -Value=0 -> contactors opened                     */
/*            -Value=1 -> precharge                             */
/*            -Value=2 -> contactors closed                     */
/*            -Value=3 -> Invalid                               */
/*      + [Frame BMS1] BMS_QuickChargeConnectorState: Inlet     */
/*        Quick Charge Connector State                          */
/*            -Value=0 -> contactors opened                     */
/*            -Value=1 -> contactors closed                     */
/*      + [Frame BMS6] BMS_HighestChargeVoltageAllow: Battery   */
/*        highest charge voltage allowed                        */
/*            -Values=0V-500V                                   */
/*      + [Frame BMS6] BMS_HighestChargeCurrentAllow: Battery   */
/*        highest charge current allowed                        */
/*            -Values=0A-200A                                   */
/*      + [Frame BMS6] BMS_OnBoardChargerEnable: Battery ready  */
/*        to be charged                                         */
/*            -Value=0 -> charging forbidden                    */
/*            -Value=1 -> charging allow                        */
/*      + [Frame BMS6] BMS_SlowChargeSt: Slow charging status   */
/*            -Value=0 -> not in charging                       */
/*            -Value=1 -> charging                              */
/*            -Value=2 -> charging fault                        */
/*            -Value=3 -> charging finished                     */
/*      + [Frame BMS6] BMS_FastChargeSt: Fast charging status   */
/*            -Value=0 -> not in charging                       */
/*            -Value=1 -> charging                              */
/*            -Value=2 -> charging fault                        */
/*            -Value=3 -> charging finished                     */
/*      + [Frame BMS9] BMS_DCRelayVoltage: Inlet Quick Charge   */
/*        Relay voltage                                         */
/*            -Values=0V-500V                                   */
/*                                                              */
/*        .Note: there are more Signals TX, but they            */
/*        are not relevant for this simulation                  */
/*                                                              */
/*          - [Frame BMS1] BMS_RelayOpenReq: Request opening    */
/*            the Battery Main Connector relay                  */
/*              -Value=0 -> BMS no request                      */
/*              -Value=1 -> BMS request to open main contactor  */
/*                                                              */
/*          - [Frame BMS5] BMS_Fault: Battery fault status      */
/*            -Value=000000000000b -> No fault                  */
/*            -Value=01XXXXXXXXXXb -> The first level fault     */
/*            -Value=10XXXXXXXXXXb -> The second level fault    */
/*            -Value=11XXXXXXXXXX -> The third level fault      */
/*              ('x' is "don't care" character)                 */
/*                                                              */
/*          - [Frame BMS8] BMS_CC2_connection_Status: Proximily */
/*            line status �?                                    */
/*              -Value=0 -> Invalid value                       */
/*              -Value=1 -> CC2 invalid not connected           */
/*              -Value=2 -> CC2 valid full connected            */
/*              -Value=3 -> CC2 invalid half connected          */
/*                                                              */
/*          - [Frame BMS3] BMS_AuxBattVolt: Auxiliary Battery   */
/*            Voltage                                           */
/*              -Values=0V-24V                                  */
/*                                                              */
/*    B2) Signals TX (from TBMU to E_VCU)                       */
/*        -simulated by CANoe internal vars-                    */
/*        + [TBMU_sigsim NameSpace]                             */
/*          BMS_HighVoltConnectionAllowed: Informs E_VCU        */
/*          about BSM awake and ready to manage the HV net      */
/*            - Value=0 -> Not allow                            */
/*            - Value=1 -> Allow                                */
/*                                                              */
/*        + [TBMU_sigsim NameSpace] BMS_ChargeConnectorReq:     */
/*          Request closing the Battery Main Connector relay    */
/*              -Value=0 -> BMS no request                      */
/*              -Value=1 -> BMS request to close main contactor */
/*                                                              */



/* ------------------------------------------------------------ */
/* --------------- INTERNAL VARIABLES Definition ---------------*/
/* ------------------------------------------------------------ */
variables
{
  msTimer timer20ms_PC;
  msTimer timer20ms_EVSE_SD;
  
  msTimer timer20ms;
  msTimer timer100ms;
  
  message BMS1 frameBMS1;
  message BMS9 frameBMS9;
  message BMS6 frameBMS6; 
 
  long myWord;
  char string[100];
  byte OBC2_last_pc;
  
  int Charge_SOC_init;
  float Charge_TimeDiff;
  float Charge_SOC_diff;
  float Charge_SOC_accum;
  
  float Test;
}



/* ------------------------------------------------------------ */
/* ------------------- FUNCTIONS Definition ------------------- */
/* ------------------------------------------------------------ */
void CalculateE2Efields(byte payload[], byte chk_Qindex, byte pc_Qindex, byte chk_ini)
{
  byte i;
  byte sum;
  byte chk, pc;
  
  // Caculate Process Counter value
  // - Current pc
  if(pc_Qindex%2 == 0)
  {
    pc = (payload.byte(pc_Qindex/2) & 0xF0) >> 4;
  }
  else
  {
    pc = payload.byte(pc_Qindex/2) & 0x0F;
  }
  // - Get new pc needed
  if(pc < 15)
  {
    pc = pc + 1;
  }
  else
  {
    pc = 0;
  }
  
  // - Write new pc into the correct nibble
  if(pc_Qindex%2 == 0)
  {
    payload.byte(pc_Qindex/2) = ((pc & 0x0F) << 4) | (payload.byte(pc_Qindex/2) & 0x0F);
  }
  else
  {
    payload.byte(pc_Qindex/2) = (payload.byte(pc_Qindex/2) & 0xF0) | (pc & 0x0F);
  }
  
  // Caculate Checksum value
  //  - Sum of all payload nibbles + chk_ini (except the Checksum field)
  sum = chk_ini;
  for(i = 0; i < elCount(payload); i++)
  {
    if(i != (chk_Qindex/2))
    {
      sum = sum + ((payload.byte(i) >> 4) & 0x0F);
      sum = sum + payload.byte(i) & 0x0F;
    }
    else if((chk_Qindex%2) == 0)
    {
      sum = sum + payload.byte(i) & 0x0F;
    }
    else
    {
      sum = sum + ((payload.byte(i) >> 4) & 0x0F);
    }
  }
  // - Get new chk needed
  chk = 0x0F - (sum & 0x0F);
    
  // - Write new chk into the correct nibble
  if(chk_Qindex%2 == 0)
  {
    payload.byte(chk_Qindex/2) = ((chk & 0x0F) << 4) | (payload.byte(chk_Qindex/2) & 0x0F);
  }else
  {
    payload.byte(chk_Qindex/2) = (payload.byte(chk_Qindex/2) & 0xF0) | (chk & 0x0F);
  }
}

void CheckE2Efields(byte payload[], byte pc_Qindex, byte chk_ini, byte pc_prev, byte chk_err, byte pc_err)
{
  byte i;
  byte sum;
  byte pc;

  // Check Checksum value
  //  - Sum of all payload nibbles + chk_ini (including the Checksum field)
  sum = chk_ini;
  for(i = 0; i < elCount(payload); i++)
  {
    sum = sum + payload.byte(i);
  }
  // - Crosscheck chk
  if((sum & 0x0F) == 0x0F)
  {
    chk_err = 0;  //OK
  }
  else
  {
    chk_err = 1;  //NOK
  }
  
  // Check Process Counter value
  // - Current pc
  if(pc_Qindex%2 == 0)
  {
    pc = (payload.byte(pc_Qindex/2) & 0xF0) >> 4;
  }
  else
  {
    pc = payload.byte(pc_Qindex/2) & 0x0F;
  }
  // - Crosscheck pc
  if(pc != pc_prev )
  {
    pc_err = 0;   //OK
  }
  else
  {
    pc_err = 1;   //NOK
  }
}



/* ------------------------------------------------------------ */
/* ---------------------- EVENT Handlers ---------------------- */
/* ------------------------------------------------------------ */
/* Executed just after CANoe starts */
on start{
  // Init internal variables
  myWord = 0;
  string[0] = '\n';
  OBC2_last_pc = 16;
  
  // Init Frames to be TX
  // Battery Initial state (TODO: to be configured from Panel_syssim)
  frameBMS1.BMS_SOC = (@Panel_syssim::TBMU_init_SOC)*10;
  frameBMS1.BMS_Voltage = @Panel_syssim::TBMU_init_BatVoltage;
  frameBMS6.BMS_HighestChargeCurrentAllow = 30*10;
  frameBMS6.BMS_HighestChargeVoltageAllow = (11000/30)*10;
  
  // Battery Main Connector Initial state (contactors openned)
  frameBMS1.BMS_MainConnectorState = 0;

  // Inlet Quick Charge Connector state (connector openned)
  frameBMS1.BMS_QuickChargeConnectorState = 0;

  // Inlet Quick Charge Connector voltage
  frameBMS9.BMS_DCRelayVoltage = 0;
}



/* Executed every 100ms: Sending Frames BM5 and BM6 */
on timer timer100ms
{
  // Debug info
  snprintf(string, elcount(string), "[TBMU_sim] Send Frames \"BMS6\"");
  write(string);

  // Send BMS6 frame
  output(frameBMS6);
}

/* Executed every 20ms: Sending Frames BMS1 and BM9 */
on timer timer20ms
{
  // Debug info
  snprintf(string, elcount(string), "[TBMU_sim] Send Frames \"BMS1\" and \"BMS9\"");
  write(string);

  // Send BMS1 frame
  output(frameBMS1);
  
  // Send BMS9 frame
  output(frameBMS9);
}

/* Executed when 'OBC2' frame is received: Check E2E fields */
on message OBC2
{
  byte i;
  byte data[8];
  
  // Load payload into an array
  for(i = 0; i < 8 ; i++)
  {
    data[i] = this.byte(i);
  } 
  
  // Check E2E fields
  CheckE2Efields(data, 12, 0x0F, OBC2_last_pc, @Panel_syssim::TBMU_OBC2_chk_err, @Panel_syssim::TBMU_OBC2_pc_err);
  
  // Store new process counter received
  OBC2_last_pc = getSignal(OBC_RollingCounter3A2);
}

/* [STEP 4_dc] Executed every 20ms: PreCharge process simulation */
on timer timer20ms_PC
{
  // Execute the PreCharge process simulation
  // and
  // Update signal accordingly:
  //     1. BMS_DC_RELAY_VOLTAGE (from TBMU to OBC_DCDC)  
  //
  // Simulation strategy:
  //     TMBU sends periodically 'BMS_Voltage' and 'BMS_DC_RELAY_VOLTAGE' signals.
  //     The 'BMS_Voltage' refers to the current Battery's voltage (~400V) and
  //     the 'BMS_DC_RELAY_VOLTAGE' refers to the current Inlet Quick Charge Connector
  //     voltage.
  //
  //    PreCharge process is being executed by the EVSE when it is increasing
  //    its output's voltage little by little (i.e. the voltage reported by
  //    'BMS_DC_RELAY_VOLTAGE' signal)
  //
  //     In order to simulate a precharge process, there has been defined
  //      a configurable parameter which can be selected by Panel_syssim:
  //         - 'Panel_syssim::TBMU_pc_speed': var for defining the precharge speed (in V/s)
  //
  // Then,
  //     1. While PreCharge process is ongoing:
  //         - BMS_Voltage = fixed value (~400V)
  //         - BMS_DC_RELAY_VOLTAGE = [from 0 to ~BMS_Voltage]
  //
  //     2. PreCharge process will be stopped (completed) when below condition
  //     is accomplished:
  //        - BMS_DC_RELAY_VOLTAGE = BMS_Voltage
  //
  //
  
  if(frameBMS9.BMS_DCRelayVoltage < frameBMS1.BMS_Voltage)
  {
    frameBMS9.BMS_DCRelayVoltage = frameBMS9.BMS_DCRelayVoltage + (@Panel_syssim::TBMU_pc_speed/50);
  }
  else
  {
    // Debug info
    snprintf(string, elcount(string), "[TBMU_sim] PreCharge process simulation completed");
    write(string);
    
    cancelTimer(timer20ms_PC);  
  }
}

/* [STEP 9_dc] Executed every 100ms: EVSE Shutdown process simulation */
on timer timer20ms_EVSE_SD
{
  // Execute the EVSE shutdown process simulation
  // and
  // Update signal accordingly:
  //     1. BMS_DC_RELAY_VOLTAGE (from TBMU to OBC_DCDC)  
  //
  // Simulation strategy:
  //     TMBU sends periodically 'BMS_DC_RELAY_VOLTAGE' signal.
  //     The 'BMS_DC_RELAY_VOLTAGE' refers to the current Inlet Quick Charge Connector
  //     voltage.
  //
  //    EVSE shutdown process is being executed by the EVSE when it is decreasing
  //    its output's voltage little by little (i.e. the voltage reported by
  //    'BMS_DC_RELAY_VOLTAGE' signal)
  //
  // Then,
  //     1. While EVSE process is ongoing:
  //         - BMS_DC_RELAY_VOLTAGE = [from X to 0]
  //
  //     2. EVSE process will be stopped (completed) when below condition
  //     is accomplished:
  //        - BMS_DC_RELAY_VOLTAGE = 0
  //
  
  if(frameBMS9.BMS_DCRelayVoltage > 0)
  {
    if(frameBMS9.BMS_DCRelayVoltage >= (@Panel_syssim::TBMU_pc_speed/50))
    {
      frameBMS9.BMS_DCRelayVoltage = frameBMS9.BMS_DCRelayVoltage - (@Panel_syssim::TBMU_pc_speed/50);
    }
    else
    {
      // Debug info
      snprintf(string, elcount(string), "[TBMU_sim] EVSE shutown process simulation completed");
      write(string);
      
      frameBMS9.BMS_DCRelayVoltage = 0;
    }
  }
  else
  {
    cancelTimer(timer20ms_EVSE_SD);   
  }
}



/* [STEP 0] Executed when 'Panel_syssim::TBMU_awake' var changes */
on sysvar Panel_syssim::TBMU_awake
{ 
  // Start/Stop sending frames
  // and
  // Update simulated signals:
  //     1. BMS_HighVoltConnectionAllowed (from TBMU to E_VCU)
  //
  // Simulation strategy:
  //     TBMU node will be transition from awake to sleep mode
  //     and viceversa by switching on/off a switch button from
  //     Panel_syssim.
  //
  //    Then,
  //      1. TBMU informs about BMS awake and ready to manage
  //      the HV network after switch button from Panel_syssim to ON.
  //          - BMS_HighVoltConnectionAllowed = 1
  //      2. TBMU informs about BSM not ready to manage
  //      the HV network after switch button from Panel_syssim to OFF.
  //          - BMS_HighVoltConnectionAllowed = 0
  //      NOTE: Moreover, when TBMU goes to sleep some signals are reseted
  //
  // TODO: improve CAPL model in order to include the TBMU awake/sleep 
  //       transition simulation according to Main/Partial WakeUp
  //       requests received over E_CAN bus
  //
  
  if(@Panel_syssim::TBMU_awake == 1)
  {    
    // Debug info
    snprintf(string, elcount(string), " [TBMU_sim] ---- TBMU simulation started! (TBMU node awake)", myWord);
    write(string);

    // Reset signals' values
    // - Battery Initial state
    frameBMS1.BMS_SOC = (@Panel_syssim::TBMU_init_SOC)*10;
    frameBMS1.BMS_Voltage = @Panel_syssim::TBMU_init_BatVoltage;
    frameBMS6.BMS_HighestChargeCurrentAllow = 30*10;
    frameBMS6.BMS_HighestChargeVoltageAllow = (11000/30)*10;

    // - Battery Main Connector Initial state (contactors openned)
    frameBMS1.BMS_MainConnectorState = 0;

    // - Inlet Quick Charge Connector state (connector openned)
    frameBMS1.BMS_QuickChargeConnectorState = 0;

    // - Inlet Quick Charge Connector voltage
    frameBMS9.BMS_DCRelayVoltage = 0;

    // Reset (initialize) internal variables
    Charge_SOC_init = frameBMS1.BMS_SOC;
    Charge_SOC_diff = 0;
    Charge_SOC_accum = 0;
    Charge_TimeDiff = 0;
    
    // Update simulated signals
    @TBMU_sigsim::BMS_HighVoltConnectionAllowed = 1;

    // Start sending frames
    setTimerCyclic(timer100ms, 100);
    setTimerCyclic(timer20ms, 20);
  }
  else
  {
    // Debug info
    snprintf(string, elcount(string), "[TBMU_sim] ---- TBMU simulation stopped! (TBMU node asleep)", myWord);
    write(string);
    
    // Stop sending frames
    cancelTimer(timer100ms);
    cancelTimer(timer20ms);
    
    // Stop PreCharg process simulation (if ongoing)
    cancelTimer(timer20ms_PC);

    // Update simulated signal
    @TBMU_sigsim::BMS_HighVoltConnectionAllowed = 0;
    
    // Reset (initialize) some signals when TBMU node goes to sleep
    frameBMS1.BMS_MainConnectorState = 0;
  }
}
  
/* [STEP 1] Executed when 'OBC_OBCStartSt' signal value changes */
on signal OBC_OBCStartSt
{
  // Update simulated signals:
  //     1. BMS_ChargeConnectorReq (from TBMU to E_VCU)
  //
  // Simulation strategy:
  //     OBC_DCDC will update the 'OBC_OBCStartSt' signal according 
  //     to below reqs:
  //     a) PSA_OBC_AcChg_14, PSA_eCMP-EPS-CDC-Charger-7
  //         IF the charge cable is plugged 
  //         into the vehicle inlet and available power is
  //         confirmed through CP and CC (), OBC shall send
  //         OBC_OBCStartSt = 1, ELSE OBC shall send OBC_OBCStartSt = 0
  //
  //     b) PSA_OBC_AcChg_19
  //         Charging shall stopped and OBC shall send OBC_OBCStartSt=0:
  //         stop charging in t2 (200ms, adjustable) when: there is fault
  //         in CAN communication(OBC_CommunicationSt=1) or first level
  //         fault (non-recoverable faults) has happened in OBC
  //
  //    Then,
  //      1. TBMU requests to close the Battery Main Connector
  //      relay just after 'OBC_OBCStartSt=OBC ready' is received
  //          - BMS_ChargeConnectorReq = 1
  //      2. TBMU requests to close the Battery Main Connector
  //      relay just after 'OBC_OBCStartSt=OBC not ready' is received
  //          - BMS_ChargeConnectorReq = 0
  //

  if(getSignal(OBC_OBCStartSt))
  {
    // Debug info
    snprintf(string, elcount(string), "[TBMU_sim] OBC_DCDC is ready (OBCStartSt=READY)!!!");
    write(string);
    
    @TBMU_sigsim::BMS_ChargeConnectorReq = 1;
  }
  else
  {
    // Debug info
    snprintf(string, elcount(string), "[TBMU_sim] OBC_DCDC is NOT ready (OBCStartSt=NOT_READY)");
    write(string);
    
    @TBMU_sigsim::BMS_ChargeConnectorReq = 0;
  }
}

/* [STEP 2] Executed when 'E_VCU_sigsim::VCU_BattMainConnectorCmd' simulated signal changes */
on sysvar E_VCU_sigsim::VCU_BattMainConnectorCmd
{
  // Start PreCharge process simulation (if needed)
  // and
  // Update signals:
  //     1. BMS_MainConnectorState (from TBMU to OBC_DCDC)
  //
  // Simulation strategy:
  //     E_VCU is in charge to determine in which moment the Battery Main 
  //     Connector shall be openned/closed after TBMU/OBC_DCDC requests
  //     received.
  //
  // Then,
  //     1. TBMU closes the Battery Main Connector
  //     and
  //     update the 'BMS_MainConnectorState=2 (contactors closed)' signal just
  //     after 'VCU_BattMainConnectorCmd=Close' simulated signal is received
  //     and
  //     Start PreCharge process simulation if 'OBC_ChargingMode=3 (Euro fast charging)'([STEP 4_dc])
  //
  //     2. Specs do not define in which moment the Battery Main Connector
  //     shall be opened by TBMU after charging process finishes.
  //     The E_VCU sends 'VCU_BattMainConnectorCmd=Open' simulated signal when
  //     charging shutdown phase shall be started, but PSA do not specify in which
  //     moment, the TBMU shall open the relay and update
  //     'BMS_MainConnectorState=contactors opened' signal.
  //     (It is out of our scope, so 'BMS_MainConnectorState=0 (contactors opened)'
  //     signal will be always reseted when TBMU node goes to sleep)
  //    

  if(@E_VCU_sigsim::VCU_BattMainConnectorCmd == 1)
  {    
    frameBMS1.BMS_MainConnectorState = 2;
    
    if(getSignal(OBC_ChargingMode) == 3)
    {           
      // Debug info
      snprintf(string, elcount(string), "[TBMU_sim] PreCharge process simulation ongoing");
      write(string);
        
      setTimerCyclic(timer20ms_PC, 20);
    }
  }
}

/* [STEP 3 & 7] Executed when 'VCU_ModeEPSRequest' signal value changes */
on signal VCU_ModeEPSRequest
{
  // Update signals:
  //     1. BMS_OnBoardChargerEnable (from TBMU to E_VCU)
  //     2. BMS_SlowChargeSt (from TBMU to E_VCU)
  //     3. BMS_FastChargeSt (from TBMU to E_VCU)  
  //     4. BMS_HighestChargeCurrentAllow  (from TBMU to E_VCU)  
  //
  // Simulation strategy:
  //     E_VCU is the master for starting/stopping a charging process 
  //     depending of the VCU_ModeEPSRequest signal value sent:
  //         a) VCU_ModeEPSRequest = 3 (PI Charge) -> start charging process (or ongoing)
  //         b) VCU_ModeEPSRequest != 3 (PI Discharge) -> stop charging process 
  //
  //    Then,
  //      1. TBMU requests starting (or keep ongoing) a charging process
  //      if some preconditions are met and 'VCU_ModeEPSRequest=3'
  //      is received:
  //        IF PRECONDITION ['BMS_HighVoltConnectionAllowed=1'], THEN:
  //          - BMS_OnBoardChargerEnable = 1 (charging allow)
  //          - Update internal variable (Charge_SOC_init)
  //          IF PRECONDITION ['OBC_ChargingMode=3 (Euro fast charging)'], THEN:
  //            - BMS_SlowChargeSt = 0
  //            - BMS_FastChargeSt = 1
  //          ELSE:
  //            - BMS_SlowChargeSt = 1
  //            - BMS_FastChargeSt = 0     
  //        ELSE:
  //          do nothing
  //
  //      2. TBMU requests stopping a charging process is some
  //      preconditions are met and 'VCU_ModeEPSRequest!=3'
  //      is received:
  //        IF PRECONDITION ['BMS_HighVoltConnectionAllowed=1'], THEN:
  //          - BMS_OnBoardChargerEnable = 0 (charging forbidden)
  //          - BMS_SlowChargeSt = 0
  //          - BMS_FastChargeSt = 0 
  //          - BMS_HighestChargeCurrentAllow = 0
  //          - Start EVSE shutdown process simulation ([STEP 9_dc])
  //        ELSE:
  //          do nothing
  //

  if(getSignal(VCU_ModeEPSRequest) == 3)
  {
    if(@TBMU_sigsim::BMS_HighVoltConnectionAllowed)
    {
      frameBMS6.BMS_OnBoardChargerEnable = 1;
        
      Charge_SOC_init = frameBMS1.BMS_SOC;
      Charge_SOC_diff = 0;
      Charge_TimeDiff = 0;

      if(getSignal(OBC_ChargingMode) == 3)
      {
        frameBMS6.BMS_SlowChargeSt = 0;
        frameBMS6.BMS_FastChargeSt = 1;
      }
      else
      {
        frameBMS6.BMS_SlowChargeSt = 1;
        frameBMS6.BMS_FastChargeSt = 0;
      }
    }
  }
  else if(getSignal(VCU_ModeEPSRequest) != 3)
  {
    if(@TBMU_sigsim::BMS_HighVoltConnectionAllowed)
    {
      frameBMS6.BMS_OnBoardChargerEnable = 0;
      frameBMS6.BMS_SlowChargeSt = 0;
      frameBMS6.BMS_FastChargeSt = 0;
      frameBMS6.BMS_HighestChargeCurrentAllow = 0;
             
      // Debug info
      snprintf(string, elcount(string), "[TBMU_sim] EVSE shutown process simulation ongoing");
      write(string);
      
      setTimerCyclic(timer20ms_EVSE_SD, 20);
    }
  }
}

/* [STEP 5_dc & 8_dc] Executed when 'DCDC_OBCQuickChargeContactorReq' signal value changes */
on signal DCDC_OBCQuickChargeContactorReq
{

  // Close/Open the Inlet Quick Charge Connector
  // and
  // Update signal:
  //     1. BMS_QuickChargeConnectorState (from TBMU to E_VCU)  
  //
  // Simulation strategy:
  //     OBC_DCDC publishes 'DCDC_OBCQuickChargeContactorReq' signal in order to 
  //     request Closing/Openning the Inlet Quick Charge Connector.
  //
  //    Then,
  //      1. TBMU closes the Inlet Quick Charge Connector when
  //      'DCDC_OBCQuickChargeContactorReq=1 (Request to Close the Contactors)'
  //      signal is received, and udpdate its state accordingly:
  //          - BMS_QuickChargeConnectorState = 1 (contactors closed)
  //
  //      2. TBMU opens the Inlet Quick Charge Connector when
  //      'DCDC_OBCQuickChargeContactorReq=0 (Request to Open the Contactors)'
  //      signal is received, and udpdate its state accordingly:
  //          - BMS_QuickChargeConnectorState = 0 (contactors opened)
  //

  if(getSignal(DCDC_OBCQuickChargeContactorReq) == 1)
  {
    frameBMS1.BMS_QuickChargeConnectorState = 1;
  }
  else if(getSignal(DCDC_OBCQuickChargeContactorReq) == 0)
  {
    frameBMS1.BMS_QuickChargeConnectorState = 0;
  }
}

/* [STEP 6] Executed when 'E_VCU_sigsim::VCU_AccPowerConsumption' simulated signal is written */
on sysvar_update E_VCU_sigsim::VCU_AccPowerConsumption
{
  // Execute Charging process simulation (if needed)
  // and
  // Update signals:
  //     1. BMS_HighestChargeVoltageAllow (from TBMU to OBC_DCDC)
  //     2. BMS_HighestChargeCurrentAllow (from TBMU to OBC_DCDC)
  //     3. BMS_SOC (from TBMU to OBC_DCDC) 
  //
  // Simulation strategy:
  //     E_VCU provides the Battery Power Consumption in real time. 
  //     Taken it as input, the TBMU calculates the charging current/
  //     voltage and the Battery's State Of Charge.
  //
  //     In order to simulate a battery charge process, there have been
  //     defined 2 configurable parameters which can be selected by Panel_syssim:
  //         - 'Panel_syssim::TBMU_chg_batprofile': var for defining the charging
  //           profile selected (0: Voltage Constant, 1: Current Constant, 2: Mixed)
  //         - 'Panel_syssim::TBMU_chg_batcapacity': var for defining the battery
  //           total capacity (in Ah)
  //
  //     It is assumed that charging process is on going when 
  //     'VCU_AccPowerConsumption' var is NOT zero.
  //     It is assumed that charging process is completed/stopped when
  //     'VCU_AccPowerConsumption' var is zero.
  //
  //    Then,
  //      1. Every time the 'VCU_AccPowerConsumption!=0' var is received, one step
  //      of the Charging process simulation is executed. And the related signals are
  //      updated
  //
  //      2. Every time the 'VCU_AccPowerConsumption!=0' var is received, no charging
  //      process simulation is executed and the related signals are not updated
  //

  if(@E_VCU_sigsim::VCU_AccPowerConsumption != 0)
  {
    if(frameBMS1.BMS_SOC < (100*10))
    {
      // Charging Battery profile 1
      // (Current Constant = 25A) - - - - - - - - - - - - - - - - - - - - - -
      if(@Panel_syssim::TBMU_chg_batprofile == 1)
      {
        frameBMS6.BMS_HighestChargeCurrentAllow = 25*10;
        frameBMS6.BMS_HighestChargeVoltageAllow = (@E_VCU_sigsim::VCU_AccPowerConsumption/25)*10;
      }
        
      // Charging Battery profile 2
      // (Mixed = 15% Voltage Constant / 75% Current Constant) - - - - - - - -
      else if(@Panel_syssim::TBMU_chg_batprofile == 2)
      {
        if((frameBMS1.BMS_SOC - Charge_SOC_init) < 0.85/(100*10 - Charge_SOC_init))
        {
          frameBMS6.BMS_HighestChargeVoltageAllow = 400*10;
          frameBMS6.BMS_HighestChargeCurrentAllow = (@E_VCU_sigsim::VCU_AccPowerConsumption/400)*10; 
        }
        else
        {
          frameBMS6.BMS_HighestChargeCurrentAllow = 25*10;
          frameBMS6.BMS_HighestChargeVoltageAllow = (@E_VCU_sigsim::VCU_AccPowerConsumption/25)*10; 
        } 
      }
        
      // Charging Battery profile 0 -Default-
      // (Voltage Constant = 400V) - - - - - - - - - - - - - - - - - - - - - -
      else
      {
        frameBMS6.BMS_HighestChargeVoltageAllow = 400*10;
        frameBMS6.BMS_HighestChargeCurrentAllow = (@E_VCU_sigsim::VCU_AccPowerConsumption/400)*10; 
      } 

      // SOC = SOC_prev + ((Current_A(step) * Time_h(step)) / BatteryCapacity(Ah))
      // NOTE: for simulation porpoises, the Time_h(step) is being multiplied by 8000.
      //       (i.e. 1 step of 500ms means 1h of charge aprox.)
      if(Charge_TimeDiff != 0){
        
        Test = (((timeNow() - Charge_TimeDiff)/100000.0)/3600)*(8000);
        
        Charge_SOC_diff = (((frameBMS6.BMS_HighestChargeCurrentAllow*0.1 * (((timeNow() - Charge_TimeDiff)/100000.0)/3600)*(8000)) / 
                           (@Panel_syssim::TBMU_chg_batcapacity))*10);
        
        Charge_SOC_accum = Charge_SOC_accum + Charge_SOC_diff;
        
        if((Charge_SOC_accum*10 + Charge_SOC_init) >= 100*10)
        {
          frameBMS1.BMS_SOC = 100*10;
        }
        else
        {
          frameBMS1.BMS_SOC = Charge_SOC_init + Charge_SOC_accum*10;
        }
      } 
    }
    else
    {     
      frameBMS1.BMS_SOC = 100*10;
      frameBMS6.BMS_HighestChargeVoltageAllow = 600*10;
      frameBMS6.BMS_HighestChargeCurrentAllow = 0*10;
    } 
  }
  else
  {    
    // Reset SOC internal variables
    Charge_SOC_init = frameBMS1.BMS_SOC;
    Charge_SOC_diff = 0;
    Charge_SOC_accum = 0; 
  }
  
  Charge_TimeDiff = timeNow();
}

