// KeyGeneration.cpp : Defines the entry point for the DLL application.
//

#include <windows.h>
#include "KeyGenAlgoInterfaceEx.h"



BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
                      )
{
    return TRUE;
}



KEYGENALGO_API VKeyGenResultEx GenerateKeyEx(
      const unsigned char*  iSeedArray,     /* Array for the seed [in] */
      unsigned int          iSeedArraySize, /* Length of the array for the seed [in] */
      const unsigned int    iSecurityLevel, /* Security level [in] */
      const char*           iVariant,       /* Name of the active variant [in] */
      unsigned char*        ioKeyArray,     /* Array for the key [in, out] */
      unsigned int          iKeyArraySize,  /* Maximum length of the array for the key [in] */
      unsigned int&         oSize           /* Length of the key [out] */
      )
{
    if (iSeedArraySize>iKeyArraySize)
    {
        return KGRE_BufferToSmall;
    }

    //Copy Seed from parameter to integer
    //Note: the byte order in the seed array is equal to de byte order in the bus message
    signed int SEED1SEED4 = 0x0000;
    signed int SEED2SEED3 = 0x0000;
    SEED1SEED4 = (signed int)(signed short)((iSeedArray[0]<<8) | iSeedArray[3]);
    SEED2SEED3 = (signed int)(signed short)((iSeedArray[1]<<8) | iSeedArray[2]);

    //being calculate key from seed ==============================================
    const signed int CLEF = 0x2017;   //To be defined by PSA
    signed int KEY1KEY2 = 0x0000;
    signed int KEY3KEY4 = 0x0000;

    signed int F1_CLEF = 0x0000;
    signed int F2_SEED1SEED4 = 0x0000;
    signed int F1_SEED2SEED3 = 0x0000;
    signed int F2_KEY1KEY2 = 0x0000;

    signed int K, R, NI, NY;

    //for security access with Services 0x27 01 -> 0x27 02
    if(iSecurityLevel == 0x01)
    {
        //
        //PSA security algorithm - Generic Part
        //
        // F1(E1), E1 is 16bit signed integer
        //  K=E1/178
        //  R=E1-(K*178)
        //  NI=(-63*K)+(R*170)
        //  if NI<0 then NI=NI+30323
        //
        // F2(E2), E2 is 16bit signed integer
        //  K=E2/177
        //  R=E2-(K*177)
        //  NY=-K-K+(R*171)
        //  if NY<0 then NY=NY+30269
        //
        //PSA security algorithm - Specific Part
        //
        // KEY1 KEY2 = F1(CLEF) or F2(SEED1 SEED4)
        // KEY3 KEY4 = F1(SEED2 SEED3) or F2(KEY1 KEY2)
        //

        K = CLEF/178;
        R = CLEF - (K*178);
        NI = (-63*K) + (R*170);
        if(NI < 0)
        {
            NI = NI + 30323;
        }
        F1_CLEF = NI;

        K = SEED1SEED4/177;
        R = SEED1SEED4 - (K*177);
        NY = - K - K + (R*171);
        if(NY<0)
        {
            NY = NY + 30269;
        }
        F2_SEED1SEED4 = NY;

        KEY1KEY2 = F1_CLEF | F2_SEED1SEED4;


        K = SEED2SEED3/178;
        R = SEED2SEED3 - (K*178);
        NI = (-63*K) + (R*170);
        if(NI < 0)
        {
            NI = NI + 30323;
        }
        F1_SEED2SEED3 = NI;

        K = KEY1KEY2/177;
        R = KEY1KEY2 - (K*177);
        NY = - K - K + (R*171);
        if(NY<0)
        {
            NY = NY + 30269;
        }
        F2_KEY1KEY2 = NY;

        KEY3KEY4 = F1_SEED2SEED3 | F2_KEY1KEY2;
    }
    //end calculate key from seed ================================================

    //Copy Key to the output buffer
    //Note: The first byte of the key array will be the first key byte of the bus message
    ioKeyArray[3] = KEY3KEY4 & 0xff;
    ioKeyArray[2] = (KEY3KEY4>>8) & 0xff;
    ioKeyArray[1] = KEY1KEY2 & 0xff;
    ioKeyArray[0] = (KEY1KEY2>>8) & 0xff;
    //setting length of key
    oSize = 4;

  return KGRE_Ok;
}


