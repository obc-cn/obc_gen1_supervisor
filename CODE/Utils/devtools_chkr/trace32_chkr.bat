@echo off


echo *********************************************************
echo **** trace32 checker ************************************
echo *********************************************************
echo *


rem *                                                        *
rem * Read devtools' paths from the local machine            *
rem * (defined at devtools.path.defines file)                *
rem *                                                        *
for /f "tokens=2 delims==" %%i in ('%WINDIR%\system32\findstr.exe /b /c:"DEVTOOLS_TRACE32_PATH" %~dp0\..\devtools.path.defines') do (
    set trace32_path=%%i
)
echo * Reading devtools' paths from "devtools.path.defines" file...
echo *    - trace32_path: %trace32_path%
echo *


rem *                                                        *
rem * Check for required devtools properly installed in      *
rem * local machine                                          *
rem *                                                        *
set trace32_st=NOK
%WINDIR%\system32\findstr.exe "R.2018.09.000104140 R.2020.02.000121039" "%trace32_path%\version.t32" >nul 2>&1
if %ERRORLEVEL% == 0 (
    set trace32_st=OK
)
echo * Checking trace32 tool properly installed... [%trace32_st%]
echo *


rem *                                                        *
rem * Generate output results (0: OK, !0: NOK )              *
rem *                                                        *
if %trace32_st% == OK (
    exit /b 0
)
exit /b 1

