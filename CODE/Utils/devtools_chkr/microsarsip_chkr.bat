@echo off


echo *********************************************************
echo **** microsar_sip checker *******************************
echo *********************************************************
echo *


rem *                                                        *
rem * Check for required devtools properly installed in      *
rem * local machine                                          *
rem *                                                        *
set microsarsip_st1=NOK
%WINDIR%\system32\fc.exe %~dp0\..\microsar_sip\SipLicense.lic "%~dp0\..\..\Source\BaseSW\microsar\SipLicense.lic" >nul 2>&1
if %ERRORLEVEL% == 0 (
    set microsarsip_st1=OK
)

set microsarsip_st2=NOK
%WINDIR%\system32\fc.exe %~dp0\..\microsar_sip\SipDeliveryVersion.txt "%~dp0\..\..\Source\BaseSW\microsar\SipDeliveryVersion.txt" >nul 2>&1
if %ERRORLEVEL% == 0 (
    set microsarsip_st2=OK
)

echo * Checking microsar_sip tool properly integrated into repo (sip license chk)... [%microsarsip_st1%]
echo * Checking microsar_sip tool properly integrated into repo (sip version chk)... [%microsarsip_st2%]
echo *

rem *                                                        *
rem * Generate output results (0: OK, !0: NOK )              *
rem *                                                        *
if %microsarsip_st1% == OK (
    if %microsarsip_st2% == OK (
        exit /b 0
    )
)
exit /b 1

