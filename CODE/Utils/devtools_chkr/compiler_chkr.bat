@echo off


echo *********************************************************
echo **** compiler checker ***********************************
echo *********************************************************
echo *


rem *                                                        *
rem * Read devtools' paths from the local machine            *
rem * (defined at devtools.path.defines file)                *
rem *                                                        *
for /f "tokens=2 delims==" %%i in ('%WINDIR%\system32\findstr.exe /b /c:"DEVTOOLS_COMPILER_PATH" %~dp0\..\devtools.path.defines') do (
    set compiler_path=%%i
)
echo * Reading devtools' paths from "devtools.path.defines" file...
echo *    - compiler_path: %compiler_path%
echo *


rem *                                                        *
rem * Check for required devtools properly installed in      *
rem * local machine                                          *
rem *                                                        *
set compiler_st=NOK
%WINDIR%\system32\findstr.exe /l "4.9.3.0" "%compiler_path%\toolchain_info.htct" >nul 2>&1
if %ERRORLEVEL% == 0 (
    set compiler_st=OK
)
echo * Checking compiler tool properly installed... [%compiler_st%]
echo *



rem *                                                        *
rem * Read devtools' paths from the local machine            *
rem * (defined at devtools.path.defines file)                *
rem *                                                        *
for /f "tokens=2 delims==" %%i in ('%WINDIR%\system32\findstr.exe /b /c:"DEVTOOLS_COMPILER_SAFETLIB_PATH" %~dp0\..\devtools.path.defines') do (
    set compiler_safetlib_path=%%i
)
echo * Reading devtools' paths from "devtools.path.defines" file...
echo *    - compiler_safetlib_path: %compiler_safetlib_path%
echo *


rem *                                                        *
rem * Check for required devtools properly installed in      *
rem * local machine                                          *
rem *                                                        *
set compiler_st=NOK
%WINDIR%\system32\findstr.exe /l "4.6.3.0" "%compiler_safetlib_path%\toolchain_info.htct" >nul 2>&1
if %ERRORLEVEL% == 0 (
    set compiler_st=OK
)
echo * Checking compiler (safetlib) tool properly installed... [%compiler_st%]
echo *



rem *                                                        *
rem * Generate output results (0: OK, !0: NOK )              *
rem *                                                        *
if %compiler_st% == OK (
    exit /b 0
)
exit /b 1

