@echo off


echo *********************************************************
echo **** developer checker **********************************
echo *********************************************************
echo *


rem *                                                        *
rem * Read devtools' paths from the local machine            *
rem * (defined at devtools.path.defines file)                *
rem *                                                        *
for /f "tokens=2 delims==" %%i in ('%WINDIR%\system32\findstr.exe /b /c:"DEVTOOLS_DAVINCIDEV_PATH" %~dp0\..\devtools.path.defines') do (
    set davinci_dev_path=%%i
)
echo * Reading devtools' paths from "devtools.path.defines" file...
echo *    - davinci_dev_path: %davinci_dev_path%
echo *


rem *                                                        *
rem * Check for required devtools properly installed in      *
rem * local machine                                          *
rem *                                                        *
set davinci_dev_st=NOK
%WINDIR%\system32\find.exe "DaVinci Developer 4.4 Installation Guide" "%davinci_dev_path%\Docs\ReadmeInstall.htm" >nul 2>&1
if %ERRORLEVEL% == 0 (
    set davinci_dev_st=OK
)
echo * Checking davinci developer tool properly installed... [%davinci_dev_st%]
echo *


rem *                                                        *
rem * Generate output results (0: OK, !0: NOK )              *
rem *                                                        *
if %davinci_dev_st% == OK (
    exit /b 0
)
exit /b 1

