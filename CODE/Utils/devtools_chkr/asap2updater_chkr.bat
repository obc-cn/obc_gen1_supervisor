@echo off


echo *********************************************************
echo **** asap2updater checker *******************************
echo *********************************************************
echo *


rem *                                                        *
rem * Read devtools' paths from the local machine            *
rem * (defined at devtools.path.defines file)                *
rem *                                                        *
for /f "tokens=2 delims==" %%i in ('%WINDIR%\system32\findstr.exe /b /c:"DEVTOOLS_ASAP2UPDATER_PATH" %~dp0\..\devtools.path.defines') do (
    set asap2updater_path=%%i
)
echo * Reading devtools' paths from "devtools.path.defines" file...
echo *    - asap2updater: %asap2updater_path%
echo *


rem *                                                        *
rem * Check for required devtools properly installed in      *
rem * local machine                                          *
rem *                                                        *
set asap2updater=NOK
call "%asap2updater_path%\ASAP2Updater.exe" > %~dp0\tmp.txt
%WINDIR%\system32\findstr.exe /l " 13.0.20" "%~dp0\tmp.txt" >nul 2>&1
if %ERRORLEVEL% == 0 (
    set asap2updater=OK
)
del %~dp0\tmp.txt >nul
echo * Checking a2lupdater tool properly installed... [%asap2updater%]
echo *


rem *                                                        *
rem * Generate output results (0: OK, !0: NOK )              *
rem *                                                        *
if %asap2updater% == OK (
    exit /b 0
)
exit /b 1

