@echo off


if "%1"=="" goto DEFAULTS
if %1 == all goto DEFAULTS

if %1 == compiler (
    call compiler_chkr.bat
    if not %ERRORLEVEL% == 0 (
        set error=1
    )
    goto END
)
if %1 == developer (
    call developer_chkr.bat
    if not %ERRORLEVEL% == 0 (
        set error=1
    )
    goto END
)
if %1 == microsarsip (
    call microsarsip_chkr.bat
    if not %ERRORLEVEL% == 0 (
        set error=1
    )
    goto END
)
if %1 == trace32 (
    call trace32_chkr.bat
    if not %ERRORLEVEL% == 0 (
        set error=1
    )
    goto END
)
if %1 == asap2updater (
    call asap2updater_chkr.bat
    if not %ERRORLEVEL% == 0 (
        set error=1
    )
    goto END
)
echo Input(s) argument(s) invalid
exit /b 1



:DEFAULTS
set error=0
call compiler_chkr.bat
if not %ERRORLEVEL% == 0 (
    set error=1
)
call developer_chkr.bat
if not %ERRORLEVEL% == 0 (
    set error=1
)
call microsarsip_chkr.bat
if not %ERRORLEVEL% == 0 (
    set error=1
)
call trace32_chkr.bat
if not %ERRORLEVEL% == 0 (
    set error=1
)
call asap2updater_chkr.bat
if not %ERRORLEVEL% == 0 (
    set error=1
)



:END
exit /b error

