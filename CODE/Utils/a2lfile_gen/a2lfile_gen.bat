@echo off


rem echo *********************************************************
rem echo **** a2lfile_gen generator ******************************
rem echo *********************************************************
rem echo *


rem * Read input arguments                                   *
rem * (if no input aguments, jump to error)                  *
rem *                                                        *
if %1.==. goto NO_ARGS
set A2LFileOut=%1
set A2LFileOut=%A2LFileOut:/=\%

echo * A2L file to be generated at:
echo *    - %1
echo *


rem *                                                        *
rem * Generate A2L file from last ELF binary file and        *
rem * Rte.a2l partial database file available                *
rem *                                                        *
rem echo *

:: Checking devtools OK
call %~dp0\..\devtools_chkr\asap2updater_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_ASAP2

:: Read asap2updater allocation path
for /f "tokens=2 delims==" %%i in ('%WINDIR%\system32\findstr.exe /b /c:"DEVTOOLS_ASAP2UPDATER_PATH" %~dp0\..\devtools.path.defines') do (
    set asap2_path=%%i
)

:: Launch asap2updater tool
echo * Launch ASAP2Updater generator...
echo *
call "%asap2_path%\ASAP2Updater.exe" -I %~dp0\master_a2l\Master.a2l -O %A2LFileOut% -A %~dp0\..\..\Source\out\OBCP11.elf -L %~dp0\a2l.log -T "%~dp0\support\cfg\Updater.ini"
echo *

:: Move 'mapconv.tmp' file
echo * Move 'mapconv.tmp' file (ASCII representation of the Map file)....
echo *
if exist %~dp0\..\..\Source\out\mapconv.tmp Copy %~dp0\..\..\Source\out\mapconv.tmp %~dp0\mapconv.tmp >nul
if exist %~dp0\..\..\Source\out\mapconv.tmp del %~dp0\..\..\Source\out\mapconv.tmp >nul
echo *

rem *                                                        *
rem * Generate output results (0: OK, !0: NOK )              *
rem *                                                        *
echo * OK: A2L file generated succesfully! :)
echo *
exit /b 0

:NO_ARGS
echo * ERROR: No valid inputs selected
echo *
exit /b 1

:END_ERR_ASAP2
echo * ERR: ASAP2Updater tool required not found
echo *
echo * Press any key for finishing...
set /p dummy=
echo *
exit /b %ERRORLEVEL%

