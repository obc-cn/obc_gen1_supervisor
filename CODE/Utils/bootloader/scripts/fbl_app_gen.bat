@echo off
rem This script merges fbl + app and sets a valid ECU status


rem Checks compatible FBL version to be flashed...
rem Read HW_VERSION compiled
for /f "tokens=3 delims=	,	" %%i in ('%WINDIR%\system32\findstr.exe /b /c:"#define		APP_HW_VERSION" %~dp0\..\..\..\Source\AppSW\AppExternals.h') do (
    set hw_version=%%i
)

rem Selects compatible FBL version to merge
rem !!! Please, update this script for any new HW VERSION defined
rem NOTES:
rem     - FBL V1_1 and V1_2 are fully compatibles
set fbl_version=UNKNOWN
if %hw_version% == APP_SAMPLES_D2_1_NO_XCP set fbl_version=V1_2
if %hw_version% == APP_SAMPLES_D2_1 set fbl_version=V1_2
if %hw_version% == APP_SAMPLES_D1_2 set fbl_version=V1
if %hw_version% == APP_SAMPLES_D1_1 set fbl_version=V0
if %hw_version% == APP_SAMPLES_D1_1_NO_LED set fbl_version=V0
if %hw_version% == APP_SAMPLES_D1_NO_PLC set fbl_version=V0
if %hw_version% == APP_SAMPLES_D1 set fbl_version=V0
if %hw_version% == APP_SAMPLES_C1 set fbl_version=V0
if %fbl_version% == UNKNOWN goto :END_ERR_HWVERSION

set fbl_hex=%~dp0\..\bin\OBCP11_FlashBootloader_%fbl_version%.hex

rem Selects APP binary to merge
set SUP_4ULP_srec=%~dp0\..\..\..\Source\out\OBCP11_SUP_4ULP.srec

rem Add offset that was removed to generate 4ULP file and convert to hex
%~dp0\..\..\hex_view\hexview.exe /s /g /MT:%SUP_4ULP_srec%;0xA0000000 /XI -o app.hex

rem Patch FBL ECU status
%~dp0\..\..\hex_view\hexview.exe /s /g %fbl_hex% /FR:0xA001C000,1 /FP:F0 /XI:16 -o fbl_patched.hex

rem Merge FBL and APP
%~dp0\..\..\hex_view\hexview.exe /s /g /MO:fbl_patched.hex+app.hex /XI -o %~dp0\..\..\..\Source\out\OBCP11_FBL_APP.hex

rem Remove letfovers
if exist app.hex del app.hex >nul
if exist fbl_patched.hex del fbl_patched.hex >nul

