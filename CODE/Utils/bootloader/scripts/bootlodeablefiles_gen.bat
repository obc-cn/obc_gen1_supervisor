@echo off


rem echo *********************************************************
rem echo **** bootlodeablefiles_gen generator ********************
rem echo *********************************************************
rem echo *


rem * Read input arguments                                   *
rem * (if no input aguments, jump to error)                  *
rem *                                                        *
if %1.==. goto NO_ARGS
set AppFile=%1
set AppFile=%AppFile:/=\%
set CalFile=%2
set CalFile=%CalFile:/=\%
set CalFile4Trace=%3
set CalFile4Trace=%CalFile4Trace:/=\%
set GlobalFile=%4
set GlobalFile=%GlobalFile:/=\%

rem echo * Bootloadable files to be generated at:
rem echo *    - %2
rem echo *    - %3
rem echo *


rem *                                                        *
rem * Generate Bootlodeable files from last SREC file        *
rem * available                                              *
rem *                                                        *
rem echo *

:: Read HW_VERSION being compiled
rem echo * Checking APP_HW_VERSION flag being compiled...
for /f "tokens=3 delims=	,	" %%i in ('%WINDIR%\system32\findstr.exe /b /c:"#define		APP_HW_VERSION" %~dp0\..\..\..\Source\AppSW\AppExternals.h') do (
    set hw_version=%%i
)
rem echo *    - APP_HW_VERSION: %hw_version%
rem echo * 

:: Selecting te correct mem_flag to apply
set extendedmem_flag=UNKNOWN
if %hw_version% == APP_SAMPLES_D2_1_NO_XCP set extendedmem_flag=APP_EXTENDEDMEM
if %hw_version% == APP_SAMPLES_D2_1 set extendedmem_flag=APP_EXTENDEDMEM
if %hw_version% == APP_SAMPLES_D1_2 set extendedmem_flag=APP_REDUCEDDMEM
if %hw_version% == APP_SAMPLES_D1_1 set extendedmem_flag=APP_REDUCEDDMEM
if %hw_version% == APP_SAMPLES_D1_1_NO_LED set extendedmem_flag=APP_REDUCEDDMEM
if %hw_version% == APP_SAMPLES_D1_NO_PLC set extendedmem_flag=APP_REDUCEDDMEM
if %hw_version% == APP_SAMPLES_D1 set extendedmem_flag=APP_REDUCEDDMEM
if %hw_version% == APP_SAMPLES_C1 set extendedmem_flag=APP_REDUCEDDMEM
if %extendedmem_flag% == UNKNOWN goto :END_ERR_HWVERSION

:: Defines the address range where the Application and Calibration segments shall be allocated:
::     - Application: [0xA002000, 0x17FFFD]
::     - Application checksum: [0x17FFFE, 0x17FFFF]
::     - Calibration: [0xA001C060, 0xA001FFFE].
::       NOTE: first 2 bytes from Calibration segment are reserved for PSA defined signals,
::             ECU_STATUS (0xA001C000) and
::             INFRACT_ATT (0xA001C001)
::             bytes managed by FBL (outside the controlled zone by checksum).
::             Hence, the first 0x60 bytes will not be considered for 'calibration data'.
::       NOTE: first 2 bytes from 'calibration data' are reserved for MAHLE defined signal,
::             CALIBRATION_ID ([0xA001C060,0xA001C061])
::             which is used internally by Application in order to crosscheck the
::             calibration file compatibility with the current Application flashed.
::     - Calibration checksum: [0xA001FFFE, 0xA001FFFF]
set App_start4b=0xA0020000
set App_start3b=0x020000
if %extendedmem_flag% == APP_EXTENDEDMEM set App_length=0x1DFFFE
if %extendedmem_flag% == APP_REDUCEDDMEM set App_length=0x15FFFE
set Cal_start4b=0xA001C060
set Cal_start3b=0x01C060
set Cal_length=0x03F9E


:: Generate some auxiliar variables
set /a App_and_Chk_length="%App_length%+2"
set /a Cal_and_Chk_length="%Cal_length%+2"


:: Step0. Backup the App binary generated by the linker
if exist %AppFile% Copy %AppFile% AppFile_org.srec >nul

:: Step1. App: Remove RAM area
:: - Data Scratch-Pad SRAM: [0x70100000, 0x70101FFF]
:: - Program Scratch-Pad SRAM: [0x70000000, 0x7001BFFF]
if exist %AppFile% %~dp0\..\..\hex_view\hexview.exe /s /g AppFile_org.srec /CR:0x70100000-0x70101FFF:0x70000000-0x7001BFFF /XS -o AppFile_step1.srec

:: Step2. App: Remove CALIBRATION area
if exist %AppFile% %~dp0\..\..\hex_view\hexview.exe /s /g AppFile_step1.srec /CR:%Cal_start4b%,%Cal_and_Chk_length% /XS -o AppFile_step2.srec

:: Step3. App: Fill all gaps
if exist %AppFile% %~dp0\..\..\hex_view\hexview.exe /s /g AppFile_step2.srec /FR:%App_start4b%,%App_length% /FP:00 /XS -o AppFile_step3.srec

:: Step4. App: Cut the first byte out of the address
if exist %AppFile% %~dp0\..\..\hex_view\hexview.exe /s /g /MT:AppFile_step3.srec;-0xA0000000 /XS -o AppFile_step4.srec

:: Step5. App: Create a single temp global binary and generate the Checksum for the application (3byte addresses)
if exist %AppFile% %~dp0\..\..\hex_view\hexview.exe /s /g AppFile_step4.srec /AR:%App_start3b%,%App_length% /AL /FA /FP:00 /FR:%App_start3b%,%App_length% /CS5:@append;%App_start3b%,%App_length% /XS -o AppFile_prepared.srec


:: Step6. Delete the last Cal binary available
if exist %CalFile% del %CalFile% >nul


:: TODO: Step7-10 shall be replaced by a CAL file generator script based on CalibrationTable.csv doc =============================
::       It will generate the user-defined calibration data + the correlated CALIBRATION_ID value.

:: Step7. Cal: Remove APPLICATION area
%~dp0\..\..\hex_view\hexview.exe /s /g AppFile_step1.srec /CR:%App_start4b%,%App_and_Chk_length% /XS -o CalFile_step2.srec

:: Step8. Cal: Fill all gaps
if exist %AppFile% %~dp0\..\..\hex_view\hexview.exe /s /g CalFile_step2.srec /FR:%Cal_start4b%,%Cal_length% /FP:00 /XS -o CalFile_step3.srec

:: Step9. Cal: Generate calibration file for Trace32 use case only (4byte addresses)
if exist %AppFile% Copy CalFile_step3.srec %CalFile4Trace% >nul

:: Step10. Cal: Cut the first byte out of the address
if exist %AppFile% %~dp0\..\..\hex_view\hexview.exe /s /g /MT:CalFile_step3.srec;-0xA0000000 /XS -o CalFile_step4.srec

:: Step10. Cal: Generate the Checksum for the calibration data (3byte addresses)
if exist %AppFile% %~dp0\..\..\hex_view\hexview.exe /s /g CalFile_step4.srec /AR:%Cal_start3b%,%Cal_length% /AL /FA /FP:00 /FR:%Cal_start3b%,%Cal_length% /CS5:@append;%Cal_start3b%,%Cal_length% /XS -o %CalFile%

:: TODO ==========================================================================================================================


:: Step11. App+Cal: Append the Calibration data to the global binary (3byte addresses)
if exist %AppFile% %~dp0\..\..\hex_view\hexview.exe /s /g AppFile_prepared.srec /MT:%CalFile% /XS -o %GlobalFile%


:: Erase temporary files
if exist AppFile_org.srec del AppFile_org.srec >nul
if exist AppFile_step1.srec del AppFile_step1.srec >nul
if exist AppFile_step2.srec del AppFile_step2.srec >nul
if exist AppFile_step3.srec del AppFile_step3.srec >nul
if exist AppFile_step4.srec del AppFile_step4.srec >nul
if exist AppFile_prepared.srec del AppFile_prepared.srec >nul
if exist CalFile_step2.srec del CalFile_step2.srec >nul
if exist CalFile_step3.srec del CalFile_step3.srec >nul
if exist CalFile_step4.srec del CalFile_step4.srec >nul


rem *                                                        *
rem * Generate output results (0: OK, !0: NOK )              *
rem *                                                        *
rem echo * OK: Download files generated succesfully! :)
rem echo *
exit /b 0

:NO_ARGS
rem echo * ERROR: No valid inputs selected
rem echo *
exit /b 1

:END_ERR_HWVERSION
echo * ERR: HW Version setted not found
echo *      Hint: please, update StartT32.bat for defining the HW Version needed
echo *
exit /b 1

:END_ERR
rem echo * ERR: BootloadX files generation failed
rem echo *
exit /b %ERRORLEVEL%
