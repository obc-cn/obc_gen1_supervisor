@echo off


rem echo *********************************************************
rem echo **** buildinfo generator ********************************
rem echo *********************************************************
rem echo *


rem * Read input arguments                                   *
rem * (if no input aguments, jump to error)                  *
rem *                                                        *
if %1.==. goto NO_ARGS
set OutputFile=%1
rem echo * BuildInfo file to be generated at:
rem echo *    - %1
rem echo *

rem Read git allocation path
for /f "tokens=2 delims==" %%i in ('%WINDIR%\system32\findstr.exe /b /c:"DEVTOOLS_GIT_PATH" %~dp0\..\devtools.path.defines') do (
    set git_path=%%i
)

rem Recursively expands variable of git allocation path
for %%i in (%git_path%) do for /f %%a in ('echo %%%%i%%') do set git_path=%%a
set git_path=%git_path:~1,-1%

rem *                                                        *
rem * Generate BuildInfo report from the current repo status *
rem *     - Date and Time                                    *
rem *     - User Name                                        *
rem *     - Git Branch                                       *
rem *     - Git Status                                       *
rem *                                                        *
rem echo * Generating info...
rem echo *

echo Build Information > %OutputFile%
echo ================= >> %OutputFile%
echo. >> %OutputFile%

echo date >> %OutputFile%
echo ---- >> %OutputFile%
echo %date% >> %OutputFile%
echo %time% >> %OutputFile%
echo. >> %OutputFile%

echo user >> %OutputFile%
echo ---- >> %OutputFile%
echo %username% >> %OutputFile%
echo. >> %OutputFile%

echo git branch >> %OutputFile%
echo ---------- >> %OutputFile%
%git_path%\git.exe symbolic-ref --short HEAD >> %OutputFile%
echo : >> %OutputFile%
%git_path%\git.exe rev-parse HEAD >> %OutputFile%
echo. >> %OutputFile%

echo local changes >> %OutputFile%
echo ------------- >> %OutputFile%
%git_path%\git.exe status >> %OutputFile% 2>nul
echo. >> %OutputFile%


rem *                                                        *
rem * Generate output results (0: OK, !0: NOK )              *
rem *                                                        *
rem echo * OK: BuildInfo report generated succesfully! :)
rem echo *
exit /b 0


:NO_ARGS
rem echo * ERROR: No output path selected
rem echo *
exit /b 1
