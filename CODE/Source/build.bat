@echo off


echo *********************************************************
echo ******************* build for OBCP11 ********************
echo *********************************************************
echo *


rem *                                                        *
rem * Read input arguments                                   *
rem * (if no input aguments, print MENU)                     *
rem *                                                        *
if %1.==. goto PRINT_MENU
set Opt=%1
goto SELECTION


rem *                                                        *
rem * Print MENU                                             *
rem *                                                        *
:PRINT_MENU
echo *             ----- MENU -----
echo *
echo *    [1] Build Target
echo *     '-- [a] With Multithread enabled
echo *    [2] Re-build Target
echo *     '-- [b] With Multithread enabled
echo *    [3] Build Target (No SafeTlib)
echo *     '-- [c] With Multithread enabled
echo *    [4] Remake Dependencies Tree
echo *     '-- [d] With Multithread enabled
echo *
echo *    [5] Generate A2L file (CANoe Option .XCP
echo *                           license required)
echo *
echo *    [6] Check for devtools required
echo *
echo *    ----
echo *    [0] Exit
echo *
echo * Please, select an option:
set /p Opt=
goto SELECTION


rem *                                                        *
rem * Select the option to be run                            *
rem *                                                        *
:SELECTION
if %Opt% == 1 goto BUILD_TARGET
if %Opt% == a goto BUILD_TARGET_MT
if %Opt% == 2 goto REBUILD_TARGET
if %Opt% == b goto REBUILD_TARGET_MT
if %Opt% == 3 goto BUILD_TARGET_NO_SFTLIB
if %Opt% == c goto BUILD_TARGET_NO_SFTLIB_MT
if %Opt% == 4 goto REMAKE_DEPENDTREE
if %Opt% == d goto REMAKE_DEPENDTREE_MT
if %Opt% == 5 goto GENERATE_A2L
if %Opt% == 6 goto CHECK_DEVTOOLS
if %Opt% == 0 goto :EXIT
goto END_ERR_OP


rem *                                                        *
rem * Execute the option selected                            *
rem *                                                        *
:BUILD_TARGET
echo * Option %Opt% selected!
echo *
rem Checking devtools OK
call %~dp0\..\Utils\devtools_chkr\compiler_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_COMP
call %~dp0\..\Utils\devtools_chkr\microsarsip_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_SIP
rem 1. Launch make environment (for SafeTlib only)
call %~dp0\mak\Build_SafeTlib_AKKAIntegration_TC23x
rem 2. Launch make environment for Vector SIP & APP
call %~dp0\mak\m
if not %ERRORLEVEL% == 0 goto END_NOK
goto END_OK

:BUILD_TARGET_MT
echo * Option %Opt% selected!
echo *
rem Checking devtools OK
call %~dp0\..\Utils\devtools_chkr\compiler_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_COMP
call %~dp0\..\Utils\devtools_chkr\microsarsip_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_SIP
rem 1. Launch make environment (for SafeTlib only)
call %~dp0\mak\Build_SafeTlib_AKKAIntegration_TC23x
rem 2. Launch make environment for Vector SIP & APP
call %~dp0\mak\m -j4
if not %ERRORLEVEL% == 0 goto END_NOK
goto END_OK

:REBUILD_TARGET
echo * Option %Opt% selected!
echo *
rem Checking devtools OK
call %~dp0\..\Utils\devtools_chkr\compiler_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_COMP
call %~dp0\..\Utils\devtools_chkr\microsarsip_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_SIP
rem 1. Launch make environment (for SafeTlib only)
call %~dp0\mak\Build_SafeTlib_AKKAIntegration_TC23x clean
call %~dp0\mak\Build_SafeTlib_AKKAIntegration_TC23x
rem 2. Launch make environment for Vector SIP & APP
call %~dp0\mak\m rebuild
if not %ERRORLEVEL% == 0 goto END_NOK
goto END_OK

:REBUILD_TARGET_MT
echo * Option %Opt% selected!
echo *
rem Checking devtools OK
call %~dp0\..\Utils\devtools_chkr\compiler_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_COMP
call %~dp0\..\Utils\devtools_chkr\microsarsip_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_SIP
rem 1. Launch make environment (for SafeTlib only)
call %~dp0\mak\Build_SafeTlib_AKKAIntegration_TC23x clean
call %~dp0\mak\Build_SafeTlib_AKKAIntegration_TC23x
rem 2. Launch make environment for Vector SIP & APP
call %~dp0\mak\m -j rebuild
if not %ERRORLEVEL% == 0 goto END_NOK
goto END_OK

:BUILD_TARGET_NO_SFTLIB
echo * Option %Opt% selected!
echo *
rem Checking devtools OK
call %~dp0\..\Utils\devtools_chkr\compiler_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_COMP
call %~dp0\..\Utils\devtools_chkr\microsarsip_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_SIP
rem NO Launch make environment for SafeTlib only
rem Launch make environment for Vector SIP & APP
call %~dp0\mak\m
if not %ERRORLEVEL% == 0 goto END_NOK
goto END_OK

:BUILD_TARGET_NO_SFTLIB_MT
echo * Option %Opt% selected!
echo *
rem Checking devtools OK
call %~dp0\..\Utils\devtools_chkr\compiler_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_COMP
call %~dp0\..\Utils\devtools_chkr\microsarsip_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_SIP
rem NO Launch make environment for SafeTlib only
rem Launch make environment for Vector SIP & APP
call %~dp0\mak\m -j4
if not %ERRORLEVEL% == 0 goto END_NOK
goto END_OK

:REMAKE_DEPENDTREE
echo * Option %Opt% selected!
echo *
rem Checking devtools OK
call %~dp0\..\Utils\devtools_chkr\compiler_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_COMP
call %~dp0\..\Utils\devtools_chkr\microsarsip_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_SIP
rem Launch make environment
call %~dp0\mak\m depend
if not %ERRORLEVEL% == 0 goto END_NOK
goto END_OK

:REMAKE_DEPENDTREE_MT
echo * Option %Opt% selected!
echo *
rem Checking devtools OK
call %~dp0\..\Utils\devtools_chkr\compiler_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_COMP
call %~dp0\..\Utils\devtools_chkr\microsarsip_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_SIP
rem Launch make environment
call %~dp0\mak\m -j4 depend
if not %ERRORLEVEL% == 0 goto END_NOK
goto END_OK

:GENERATE_A2L
rem Launch a2lfile_gen script
call ..\Utils\a2lfile_gen\a2lfile_gen.bat .\out\OBCP11.a2l
if not %ERRORLEVEL% == 0 goto :END_NOK
goto END_EXIT

:CHECK_DEVTOOLS
echo * Option %Opt% selected!
echo *
rem Checking devtools OK
call %~dp0\..\Utils\devtools_chkr\compiler_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_COMP
call %~dp0\..\Utils\devtools_chkr\microsarsip_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_SIP
call %~dp0\..\Utils\devtools_chkr\developer_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_DDEV
call %~dp0\..\Utils\devtools_chkr\trace32_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_T32
call %~dp0\..\Utils\devtools_chkr\asap2updater_chkr.bat
if not %ERRORLEVEL% == 0 goto :END_ERR_ASAP2
goto END_EXIT

:EXIT
echo * Option %Opt% selected!
echo *
rem Do nothing
goto END_EXIT


rem *                                                        *
rem * End batch script                                       *
rem *                                                        *
:END_OK
echo *
echo * OK: Process success! :)
echo *
exit /b %ERRORLEVEL%

:END_NOK
echo *
echo * ERR: Process failed
echo *
exit /b %ERRORLEVEL%

:END_ERR_OP
echo * ERR: No valid option selected
echo *
exit /b %ERRORLEVEL%

:END_ERR_COMP
echo * ERR: Compiler tool required not found
echo *
exit /b %ERRORLEVEL%

:END_ERR_SIP
echo * ERR: Microsar SIP required not found
echo *
exit /b %ERRORLEVEL%

:END_ERR_DDEV
echo * ERR: DaVinci DEV tool required not found
echo *
exit /b %ERRORLEVEL%

:END_ERR_T32
echo * ERR: Trace32 tool required not found
echo *
exit /b %ERRORLEVEL%

:END_ERR_ASAP2
echo * ERR: ASAP2Updater tool required not found
echo *
exit /b %ERRORLEVEL%


:END_EXIT
echo * OK: No more actions required from my side.
echo * Have a nice day! :)
echo *
exit /b %ERRORLEVEL%

