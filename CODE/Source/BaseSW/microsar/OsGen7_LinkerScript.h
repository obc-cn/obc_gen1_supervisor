/*
 * tricoreIROM.x -- TriBoard-TC27xx/AppKit-TC27xx full linker script
 *   (using internal Flash for code and internal RAM for data)
 *
 * Copyright (C) 1982-2014 HighTec EDV-Systeme GmbH.
 *
 * 2018-08-06 virrro: Updated to support actual OS implementation
 *                    Removed derivative check - suitable for TC29x and TC27x
 * 2018-09-03 virrro: Added section lmu_sram
 */

/* do not replace tricore in the OUTPUT_ARCH below */
#undef tricore


OUTPUT_FORMAT("elf32-tricore")
OUTPUT_ARCH(tricore)
ENTRY(_START)

#ifndef ConfigSC1NonAsrMaster
# define CORE0_IS_ASR
#endif

#define OS_LINK_GAP_BETWEEN_MPU_REGIONS 16


/* Linker script's tags header file (auto-generated) */
#include "LinkerScriptFlags.h"



/*__TRICORE_DERIVATE_MEMORY_MAP__ = 0x2900;*/

/* the symbol __TRICORE_DERIVATE_NAME__ will be defined in the crt0_tc27x.c and is
 * tested here to confirm that this memory map and the startup file will fit together
 */
/*_. = ASSERT ((__TRICORE_DERIVATE_MEMORY_MAP__ == __TRICORE_DERIVATE_NAME__), "Using wrong Memory Map. This Map is for TC29XX");*/

/* minimal heap size (C library stuff) */
__HEAP_SIZE = DEFINED (__HEAP_SIZE) ? __HEAP_SIZE : 16K;

/* SafeTlib symbols needed (from _mcal_pjt.ld) */
__TC0_PCON2_VALUE     = 0x00080008 ;
__TC0_DCON2_VALUE     = 0x00B80000 ;
__TC1_PCON2_VALUE     = 0x00000000 ;
__TC1_DCON2_VALUE     = 0x00000000 ;
__TC2_PCON2_VALUE     = 0x00000000 ;
__TC2_DCON2_VALUE     = 0x00000000 ;

__INTTAB_START_ADDR        = 0x800F0000 ;


MEMORY
{

/* internal flash memories */
  /*[Enrique.Bueno] int_flash0_x memory moved in order to integrate VECTOR Bootloader.
                        -int_flash0_fbl added at 0xa0000000 (96K).
                        -int_flash0_logzone added at 0xa0018000 (16K).
                        -int_flash0_calflags added at 0xa001c000 (2).
                        -int_flash0_cal added at 0xa001c060 (16K).
                        -int_flash0_app moved from 0x80000000 to 0xa0020000 (1408K).
                        -int_flash0_trap moved from 0x80100000 to 0xa0170000 (32K).
                        -int_flash0_int moved from 0x80110000 to 0xa0178000 (32K).*/
  int_flash0_fbl (rx): org = 0xa0000000, len = 96K          /* Reserved (Flash Bootloader) */
  int_flash0_logzone (r): org = 0xa0018000, len = 16K       /* Reserved (Log Zone) */
  int_flash0_calflags (r): org = 0xa001c000, len = 2        /* Reserved (EcuStatus & Att_Intrp flags) */
  int_flash0_cal (r): org = 0xa001c060, len = 16K - 0x300   /* Calibration (only read access) */
  int_flash0_ecc_patterns (rx!p): org = 0xa001fd00, len = 0x300  /* ECC/EDC test paterns from SafeTlib (shall be excluded from the APP CRC calculation that
                                                                    checks the integrity of the program image in flash) */
#if defined(APP_STANDARDDMEM)
  int_flash0_app (rx): org = 0xa0020000, len = 1368K        /* Application */
  int_flash0_tvt (rx): org = 0xa0176300, len = 512          /* TVT test code to be executed in RAM from SafeTlib */
  int_flash0_ecc (rx): org = 0xa0176500, len = 5K           /* ECC test code to be executed in RAM from SafeTlib */ /*TODO: why 4k size is not enough?*/
  int_flash0_trap (rx): org = 0xa0178000, len = 16K         /* Application traps */
  int_flash0_int (rx): org = 0xa017c000, len = 16K          /* Application interrupts */
#else
  int_flash0_app (rx): org = 0xa0020000, len = 1880K        /* Application */
  int_flash0_tvt (rx): org = 0xa01f6300, len = 512          /* TVT test code to be executed in RAM from SafeTlib */
  int_flash0_ecc (rx): org = 0xa01f6500, len = 5K           /* ECC test code to be executed in RAM from SafeTlib */ /*TODO: why 4k size is not enough?*/
  int_flash0_trap (rx): org = 0xa01f8000, len = 16K         /* Application traps */
  int_flash0_int (rx): org = 0xa01fc000, len = 16K          /* Application interrupts */
#endif

/* ED (emulation device) memory */
  ed_mem (rx):  org = 0x9f000000, len = 512K

/* internal code ram */
  int_psprcpu0 (rxc0): org = 0xc0000000, len = 8K
  int_psprcpu1 (rxc1): org = 0xc0000000, len = 8K
  int_psprcpu2 (rxc2): org = 0xc0000000, len = 8K

/* internal data ram */
  int_dsprcpu0 (wc0!x): org = 0xd0000000, len = 184K
  int_dsprcpu1 (wc1!x): org = 0xd0000000, len = 184K
  int_dsprcpu2 (wc2!x): org = 0xd0000000, len = 184K

/* global view on internal memories */
  glb_int_psprcpu0 (rx): org = 0x70100000, len = 8K
  glb_int_psprcpu1 (rx): org = 0x60100000, len = 8K
  glb_int_psprcpu2 (rx): org = 0x50100000, len = 8K
  glb_int_dsprcpu0 (w!x): org = 0x70000000, len = 184K
  glb_int_dsprcpu1 (w!x): org = 0x60000000, len = 184K
  glb_int_dsprcpu2 (w!x): org = 0x50000000, len = 184K

/* global LMU RAM */
/* using the non cachable address enable shared accesses */
  lmu_sram (wx): org = 0xB0000000, len = 32K

/* local view on GTM/MCS data and code memory */
  int_mcs0_code (rx): org = 0x0000, len = 4K
  int_mcs0_data (w):  org = 0x1000, len = 2K

/* TriCore view on GTM/MCS data and code memory */
  glb_mcs0_code (rx): org = 0xf0138000, len = 4K
  glb_mcs0_data (w):  org = 0xf0139000, len = 2K
}

/* map local memory address to the corresponding global address */
REGION_MAP("CPU0", ORIGIN(int_dsprcpu0),  LENGTH(int_dsprcpu0),  ORIGIN(glb_int_dsprcpu0))
REGION_MAP("CPU1", ORIGIN(int_dsprcpu1),  LENGTH(int_dsprcpu1),  ORIGIN(glb_int_dsprcpu1))
REGION_MAP("CPU2", ORIGIN(int_dsprcpu2),  LENGTH(int_dsprcpu2),  ORIGIN(glb_int_dsprcpu2))
REGION_MAP("CPU0", ORIGIN(int_psprcpu0),  LENGTH(int_psprcpu0),  ORIGIN(glb_int_psprcpu0))
REGION_MAP("CPU1", ORIGIN(int_psprcpu1),  LENGTH(int_psprcpu1),  ORIGIN(glb_int_psprcpu1))
REGION_MAP("CPU2", ORIGIN(int_psprcpu2),  LENGTH(int_psprcpu2),  ORIGIN(glb_int_psprcpu2))

SECTIONS
{
  /*
   * Calibration data (PFLASH)
   */
  .calibration_data :
  {
    *(CALIBRATION.ID)       /* Calibration ID */
    *(CALIBRATION.CALPRM)   /* Calibration parameters (Cal. Component global) */
    *(CALIBRATION.CDATA)    /* Calibration parameters (SW-C local) */
  } > int_flash0_cal =0x00 /* PMU_PFLASH0: Program Flash Memory (APP_PFLASH0) */

  /*
   * Application reset vector (PFLASH)
   */
  .appl_vect :
  {
    *(APPLVECT)
  } > int_flash0_app =0x00 /* PMU_PFLASH0: Program Flash Memory (APP_PFLASH0) */

  /*
   * Startup code (PFLASH)
   */
  .startup_code : FLAGS(axl)
  {
    PROVIDE(__startup_code_start = .);

    /*PROTECTED REGION ID(Protection: iROM .startup_code.begin) ENABLED START*/
    /*Protection-Area for your own LDF-Code*/
    /*PROTECTED REGION END*/

    *(.startup_code) /*Startup code for AURIX*/
    *(.startup_code*)

    /*PROTECTED REGION ID(Protection: iROM .startup_code) ENABLED START*/
    /*Protection-Area for your own LDF-Code*/
    /*PROTECTED REGION END*/

    PROVIDE(__startup_code_end = .);
    . = ALIGN(4);
    KEEP (*(.text.STARTUP))
  } > int_flash0_app /* PMU_PFLASH0: Program Flash Memory (PFLASH0) */

  .startup_bmhd :
  {
    /* boot header */
    KEEP (*(.bmhd.STARTUP))
    . = ALIGN(8);
  } > int_flash0_app

  /*
   * Main Function (PFLASH)
   */
 .mainfnc :
  {
    . = ALIGN(4);
    *(.mainfnc)
  } > int_flash0_app  

  /*
   * Trap and Interrupt vector tables (PFLASH)
   */
  .trap_table  : FLAGS(axl)
  {
    . = ALIGN(32);
    __BTV_BASE_CORE0 = .;
	#if defined(CORE0_IS_ASR)
     #define OS_LINK_EXCVEC_CODE
	 #include "Os_Link_Core0.ld"
	#endif
    . = ALIGN(32);
    __BTV_BASE_CORE1 = .;
	#if defined(CORE1_IS_ASR)
     #define OS_LINK_EXCVEC_CODE
	 #include "Os_Link_Core1.ld"
	#endif
    . = ALIGN(32);
    __BTV_BASE_CORE2 = .;
	#if defined(CORE2_IS_ASR)
     #define OS_LINK_EXCVEC_CODE
	 #include "Os_Link_Core2.ld"
	#endif
  } > int_flash0_trap =0x00

  .interrupt_table : FLAGS(axl)
  {
    . = ALIGN(32);
    __BIV_BASE_CORE0 = .;
	#if defined(CORE0_IS_ASR)
     #define OS_LINK_INTVEC_CODE
	 #include "Os_Link_Core0.ld"
	#endif
    . = ALIGN(32);
    __BIV_BASE_CORE1 = .;
	#if defined(CORE1_IS_ASR)
     #define OS_LINK_INTVEC_CODE
	 #include "Os_Link_Core1.ld"
	#endif
    . = ALIGN(32);
    __BIV_BASE_CORE2 = .;
	#if defined(CORE2_IS_ASR)
     #define OS_LINK_INTVEC_CODE
	 #include "Os_Link_Core2.ld"
	#endif
  } > int_flash0_int =0x00

  /*
   * Code sections for Application (PFLASH)
   */
  .normal_code : FLAGS(axl)
  {
    *(*.text)
    *(*.text.*)
    #if defined(CORE0_IS_ASR)
  	 #define OS_LINK_CALLOUT_CODE
      #include "Os_Link_Core0.ld"
    #endif
    
    #if defined(CORE1_IS_ASR)
  	 #define OS_LINK_CALLOUT_CODE
      #include "Os_Link_Core1.ld"
    #endif
    
    #if defined(CORE2_IS_ASR)
  	 #define OS_LINK_CALLOUT_CODE
      #include "Os_Link_Core2.ld"
    #endif
    
    #define OS_LINK_CODE
    #define OS_LINK_CALLOUT_CODE
     #include "Os_Link.ld"
  } > int_flash0_app

  /* 
   * Ram-Code sections for Application (PFLASH -> SRAM)
   */
  .ram_code   : FLAGS(axl)
  {
    *(*.cramtext)
  } > glb_int_dsprcpu0 AT> int_flash0_app

  /*
   * Constant sections for SafeTlib package (PFLASH)
   */
  .MTL_PMU_ECC_EDC_TST_PF0 :  ALIGN(32) 
  {
    /*576 bytes need to be reserved for this section*/
    PROVIDE(LOC_START_PF0_PATTERNS = .);
    KEEP(*(.rodata.MTL_PMU_ECC_EDC_TST_PF0))
    . = ALIGN(32);
  } > int_flash0_ecc_patterns =0x00

  /*
   * Constant sections for Application (PFLASH)
   */
  .normal_constants : FLAGS(arl)
  {
    *(*.rodata)
    *(*.rodata.*)
	  #if defined(CORE0_IS_ASR)
     #define OS_LINK_CONST
     #include "Os_Link_Core0.ld"
	 #endif
	 #if defined(CORE1_IS_ASR)
	 #define OS_LINK_CONST
     #include "Os_Link_Core1.ld"
	 #endif
	 #if defined(CORE2_IS_ASR)
	  #define OS_LINK_CONST
      #include "Os_Link_Core2.ld"
	 #endif
  } > int_flash0_app


  /* 
   * Ram-Code sections for SafeTlib package (PFLASH -> SRAM)
   */
  /* Added for copytable implementation of PmuEccEdcTst Trap vector table that is executed from RAM */
  .PMUECCEDCTST_TVT_RAMCODE :
  {
    LOC_START_PmuEccEdcTst_tvt_RAM_RUNCODE = ABSOLUTE (.);
    *(.PMUECCEDCTST_TVT_RAMCODE*)
    . = ALIGN(256);
    LOC_END_PmuEccEdcTst_tvt_RAM_RUNCODE = ABSOLUTE (.);
    LOC_START_PmuEccEdcTst_tvt_PFLASHCODE = LOADADDR (.PMUECCEDCTST_TVT_RAMCODE);
    LOC_END_PmuEccEdcTst_tvt_PFLASHCODE = LOC_START_PmuEccEdcTst_tvt_PFLASHCODE + SIZEOF(.PMUECCEDCTST_TVT_RAMCODE);
  } > glb_int_psprcpu0 AT> int_flash0_tvt

  /* Added for copytable implementation of PmuEccEdcTst, PFlashMonTst and SlFlsErrPtrn functions that are executed from RAM */
  .PFLASH_MON_ECCEDC_RAMCODE :
  {
    LOC_START_PflashMon_EccEdc_RAM_RUNCODE = ABSOLUTE (.);
    *(.PFLASH_MON_ECCEDC_RAMCODE*)
    . = ALIGN(4);
    LOC_END_PflashMon_EccEdc_RAM_RUNCODE = ABSOLUTE (.);
    LOC_START_PflashMon_EccEdc_PFLASHCODE = LOADADDR (.PFLASH_MON_ECCEDC_RAMCODE);
    LOC_END_PflashMon_EccEdc_PFLASHCODE = LOC_START_PflashMon_EccEdc_PFLASHCODE + SIZEOF(.PFLASH_MON_ECCEDC_RAMCODE);
  } > glb_int_psprcpu0 AT> int_flash0_ecc


  /*
   * Reset Reasons Diag Data (SRAM)
   */
  .reset_reasons_data :
  {
    LOC_START_ResetReasons_DiagData_NOINIT_RAM = ABSOLUTE (.);
    *(RESETDATA)
    LOC_STOP_ResetReasons_DiagData_NOINIT_RAM = ABSOLUTE (.);
  } > glb_int_psprcpu0

  /*
   * Flash Bootloader Magic Number (SRAM)
   */
  /* Fixed location @0x70101000
     (only written before jump to FBL) */
  .fbl_magic 0x70101000 :
  {
    *(FBLSTARTMAGIC)
  } > glb_int_psprcpu0


  /*
   * ASIL Non-Initializated data (SRAM)
   */
  .asil_bss   : FLAGS(aw)
  {
    //*(*.bss*)
    . = ALIGN(8);
    _ASIL_Core0_VAR_BSS_ALL_START = ABSOLUTE(.);
    *(*.ASIL_SEC_VAR_NOINIT*)
    *(.data.DEFAULT_RAM_UNSPECIFIED)
    *(.data.DEFAULT_RAM_32BIT)
    *(.data.DEFAULT_RAM_32BYTE)
    *(.data.DEFAULT_RAM_8BIT)
    #define OS_LINK_VAR_APP_OSAPPLICATION_ASILB_FAR_CACHE_NOINIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_ASILB_NEAR_CACHE_NOINIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_ASILB_FAR_NOCACHE_NOINIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_ASILB_NEAR_NOCACHE_NOINIT
    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE_NOINIT
    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE_NOINIT
    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE_NOINIT
    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE_NOINIT
    #include "Os_Link_Core0.ld"
    . = ALIGN(8);
    _ASIL_Core0_VAR_BSS_ALL_END = ABSOLUTE(.);

  } > glb_int_dsprcpu0

  
  /*
   * ASIL Initializated data (SRAM > PFLASH)
   */
  .asil_initdata  : FLAGS(aw)
  {
    //*(*.bss*)
    . = ALIGN(8);
    _ASIL_Core0_VAR_INIT_ALL_START = ABSOLUTE(.);
    *(*.ASIL_SEC_VAR_INIT*)
    *(.data.DEFAULT_RAM_UNSPECIFIED_NONZERO_INIT)
    *(.data.DEFAULT_RAM_32BIT_NONZERO_INIT)
    *(.data.DEFAULT_RAM_NONZERO_INIT_8BIT)
    *(.data.DEFAULT_RAM_UNSPECIFIED_NONZERO_INIT)
    #define OS_LINK_VAR_APP_OSAPPLICATION_ASILB_FAR_CACHE_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_ASILB_NEAR_CACHE_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_ASILB_FAR_NOCACHE_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_ASILB_NEAR_NOCACHE_INIT
    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE_INIT
    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE_INIT
    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE_INIT
    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE_INIT
    #include "Os_Link_Core0.ld"
    . = ALIGN(8);
    _ASIL_Core0_VAR_INIT_ALL_END = ABSOLUTE(.);

  } > glb_int_dsprcpu0 AT> int_flash0_app

  .asil_zero_initdata   : FLAGS(aw)
  {
    //*(*.bss*)
    . = ALIGN(8);
    _ASIL_Core0_VAR_ZERO_INIT_ALL_START = ABSOLUTE(.);
    *(*.ASIL_SEC_VAR_ZERO_INIT*)
    #define OS_LINK_VAR_APP_OSAPPLICATION_ASILB_FAR_CACHE_ZERO_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_ASILB_NEAR_CACHE_ZERO_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_ASILB_FAR_NOCACHE_ZERO_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_ASILB_NEAR_NOCACHE_ZERO_INIT
    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_CACHE_ZERO_INIT
    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_CACHE_ZERO_INIT
    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_FAR_NOCACHE_ZERO_INIT
    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0_NEAR_NOCACHE_ZERO_INIT
    #include "Os_Link_Core0.ld"
    . = ALIGN(8);
    _ASIL_Core0_VAR_ZERO_INIT_ALL_END = ABSOLUTE(.);

  } > glb_int_dsprcpu0 AT> int_flash0_app


  /*
   * QM Non-Initializated data (SRAM)
   */
  .qm_bss  : FLAGS(aw)
  {
    //*(*.bss*)
    . = ALIGN(8);
    _QM_Core0_VAR_BSS_ALL_START = ABSOLUTE(.);
    *(*.QM_SEC_VAR_NOINIT*)
    #define OS_LINK_VAR_APP_OSAPPLICATION_BSW_QM_FAR_CACHE_NOINIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_BSW_QM_NEAR_CACHE_NOINIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_BSW_QM_FAR_NOCACHE_NOINIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_BSW_QM_NEAR_NOCACHE_NOINIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_QM_FAR_CACHE_NOINIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_QM_NEAR_CACHE_NOINIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_QM_FAR_NOCACHE_NOINIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_QM_NEAR_NOCACHE_NOINIT
    #include "Os_Link_Core0.ld"
    . = ALIGN(8);
    _QM_Core0_VAR_BSS_ALL_END = ABSOLUTE(.);

  } > glb_int_dsprcpu0

  
  /*
   * QM Initializated data (SRAM > PFLASH)
   */
  .qm_initdata  : FLAGS(aw)
  {
    //*(*.bss*)
    . = ALIGN(8);
    _QM_Core0_VAR_INIT_ALL_START = ABSOLUTE(.);
    *(*.QM_SEC_VAR_INIT*)
    #define OS_LINK_VAR_APP_OSAPPLICATION_BSW_QM_FAR_CACHE_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_BSW_QM_NEAR_CACHE_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_BSW_QM_FAR_NOCACHE_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_BSW_QM_NEAR_NOCACHE_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_QM_FAR_CACHE_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_QM_NEAR_CACHE_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_QM_FAR_NOCACHE_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_QM_NEAR_NOCACHE_INIT
    #include "Os_Link_Core0.ld"
    . = ALIGN(8);
    _QM_Core0_VAR_INIT_ALL_END = ABSOLUTE(.);

  } > glb_int_dsprcpu0 AT> int_flash0_app

  .qm_zero_initdata   : FLAGS(aw)
  {
    //*(*.bss*)
    . = ALIGN(8);
    _QM_Core0_VAR_ZERO_INIT_ALL_START = ABSOLUTE(.);
    *(*.QM_SEC_VAR_ZERO_INIT*)
    #define OS_LINK_VAR_APP_OSAPPLICATION_BSW_QM_FAR_CACHE_ZERO_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_BSW_QM_NEAR_CACHE_ZERO_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_BSW_QM_FAR_NOCACHE_ZERO_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_BSW_QM_NEAR_NOCACHE_ZERO_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_QM_FAR_CACHE_ZERO_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_QM_NEAR_CACHE_ZERO_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_QM_FAR_NOCACHE_ZERO_INIT
    #define OS_LINK_VAR_APP_OSAPPLICATION_QM_NEAR_NOCACHE_ZERO_INIT
    #include "Os_Link_Core0.ld"
    . = ALIGN(8);
    _QM_Core0_VAR_ZERO_INIT_ALL_END = ABSOLUTE(.);

  } > glb_int_dsprcpu0 AT> int_flash0_app


  /*
   * AutosarOS data & Ram sections (SRAM)
   */
#ifdef CORE0_IS_ASR
  .normal_bss_core0   : FLAGS(aw)
  {
    #define OS_LINK_KERNEL_STACKS
    #include "Os_Link_Core0_Stacks.ld"
    osEndOf_Core0Stack = .;

    #define OS_LINK_VAR_KERNEL_FAR_CACHE_NOINIT
    #define OS_LINK_VAR_KERNEL_FAR_NOCACHE_NOINIT
    #include "Os_Link_Core0.ld"

    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE0
    #define OS_LINK_VAR_APP_OSAPPLICATION_TRUSTED_CORE0
    #define OS_LINK_VAR_APP_OSAPPLICATION_NONTRUSTED_CORE0
    #include "Os_Link_Core0.ld"
  } > glb_int_dsprcpu0

  osEndOf_Core0StackInAlias = (osEndOf_Core0Stack & ~ 0xF0000000) | 0xD0000000;

  .global_vars : FLAGS(aw)
  {
    #define OS_LINK_VAR_GLOBALSHARED
    #include "Os_Link.ld"
  } > glb_int_dsprcpu0

  .normal_bss_core0_aws   : FLAGS(aws)
  {
    #define OS_LINK_VAR_KERNEL_NEAR_CACHE_NOINIT
    #include "Os_Link_Core0.ld"

  } > glb_int_dsprcpu0
#endif // CORE0_IS_ASR

#ifdef CORE1_IS_ASR
  .normal_bss_core1   : FLAGS(aw)
  {
    #define OS_LINK_KERNEL_STACKS
    #include "Os_Link_Core1_Stacks.ld"
    osEndOf_Core1Stack = .;

    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE1
    #define OS_LINK_VAR_APP_OSAPPLICATION_TRUSTED_CORE1
    #define OS_LINK_VAR_APP_OSAPPLICATION_NONTRUSTED_CORE1
    #include "Os_Link_Core1.ld"

    #define OS_LINK_VAR_KERNEL_FAR_CACHE_NOINIT
    #define OS_LINK_VAR_KERNEL_FAR_NOCACHE_NOINIT
    #include "Os_Link_Core1.ld"

  } > glb_int_dsprcpu1
  osEndOf_Core1StackInAlias = (osEndOf_Core1Stack & ~ 0xF0000000) | 0xD0000000;

  .normal_bss_core1_aws   : FLAGS(aws)
    {
      #define OS_LINK_VAR_KERNEL_NEAR_CACHE_NOINIT
      #include "Os_Link_Core1.ld"

    } > glb_int_dsprcpu1

#endif //CORE1_IS_ASR

#ifdef CORE2_IS_ASR
  .normal_bss_core2   : FLAGS(aw)
  {
    #define OS_LINK_KERNEL_STACKS
    #include "Os_Link_Core2_Stacks.ld"
    osEndOf_Core2Stack = .;

    #define OS_LINK_VAR_APP_SYSTEMAPPLICATION_OSCORE2
    #define OS_LINK_VAR_APP_OSAPPLICATION_TRUSTED_CORE2
    #define OS_LINK_VAR_APP_OSAPPLICATION_NONTRUSTED_CORE2
    #include "Os_Link_Core2.ld"

    #define OS_LINK_VAR_KERNEL_FAR_CACHE_NOINIT
    #define OS_LINK_VAR_KERNEL_FAR_NOCACHE_NOINIT
    #include "Os_Link_Core2.ld"

  } > glb_int_dsprcpu2
  osEndOf_Core2StackInAlias = (osEndOf_Core2Stack & ~ 0xF0000000) | 0xD0000000;

  .normal_bss_core2_aws   : FLAGS(aws)
    {
      #define OS_LINK_VAR_KERNEL_NEAR_CACHE_NOINIT
      #include "Os_Link_Core2.ld"

    } > glb_int_dsprcpu2

#endif //CORE2_IS_ASR

  .normal_bss   : FLAGS(aw)
  {
    #define OS_LINK_VAR_KERNEL_FAR_NOINIT
    #define OS_LINK_VAR_KERNEL_NEAR_NOINIT
    #include "Os_Link.ld"

    *(*.bss*)
    _ASIL_Core0_BEFORE_ASIL_SECTION = ABSOLUTE(.);
    . += 16;
  } > glb_int_dsprcpu0

  .initdata  :  FLAGS(awl)
  {
    *(*.data.a4)
    . = ALIGN(8);
    *(*.data.a2)
    . = ALIGN(8);
    *(*.data.a1)
    . = ALIGN(8);
  } > glb_int_dsprcpu0 AT> int_flash0_app


  /*
   * Define CSAs
   */
  .CPU0.csa : ALIGN(64) FLAGS(aw)
  {
    . = . + 0x14000;
    __CSA_BEGIN_CPU0_ = .;
    . +=  25600;
    __CSA_END_CPU0_ = .;
  } > int_dsprcpu0
  __CSA_SIZE_CPU0_ = __CSA_END_CPU0_ - __CSA_BEGIN_CPU0_;

  .CPU1.csa : ALIGN(64) FLAGS(aw)
  {
    . = . + 0x14000;
    __CSA_BEGIN_CPU1_ = .;
    . +=  8192;
    __CSA_END_CPU1_ = .;
  } > int_dsprcpu1
  __CSA_SIZE_CPU1_ = __CSA_END_CPU1_ - __CSA_BEGIN_CPU1_;

  .CPU2.csa : ALIGN(64) FLAGS(aw)
  {
    . = . + 0x14000;
    __CSA_BEGIN_CPU2_ = .;
    . +=  8192;
    __CSA_END_CPU2_ = .;
  } > int_dsprcpu2
  __CSA_SIZE_CPU2_ = __CSA_END_CPU2_ - __CSA_BEGIN_CPU2_;


  /*
   * Copy Tables (PFLASH)
   */
  .clear_sec : ALIGN(8) FLAGS(arl)
  {
    PROVIDE(__clear_table = .);
    /*
    LONG(0 + ADDR(.zero_data_core0));          LONG(SIZEOF(.zero_data_core0));
    LONG(0 + ADDR(.zero_data_core1));          LONG(SIZEOF(.zero_data_core1));
    LONG(0 + ADDR(.zero_data_core2));          LONG(SIZEOF(.zero_data_core2));
    */
    LONG(0 + ADDR(.normal_bss ));              LONG(SIZEOF(.normal_bss ));
    LONG(0 + ADDR(.normal_bss_core0 ));        LONG(SIZEOF(.normal_bss_core0 ));
    LONG(0 + ADDR(.normal_bss_core0_aws ));    LONG(SIZEOF(.normal_bss_core0_aws ));
    LONG(0 + ADDR(.qm_bss ));                  LONG(SIZEOF(.qm_bss ));
    LONG(0 + ADDR(.qm_initdata ));             LONG(SIZEOF(.qm_initdata ));
    LONG(0 + ADDR(.qm_zero_initdata));         LONG(SIZEOF(.qm_zero_initdata));
    LONG(0 + ADDR(.asil_bss ));                LONG(SIZEOF(.asil_bss ));
    LONG(0 + ADDR(.asil_initdata));            LONG(SIZEOF(.asil_initdata));
    LONG(0 + ADDR(.asil_zero_initdata));       LONG(SIZEOF(.asil_zero_initdata));
    LONG(-1);                                  LONG(-1);
  } > int_flash0_app

  .copy_sec  : ALIGN(8) FLAGS(arl)
  {
    PROVIDE(__copy_table = .);
    /*
    LONG(LOADADDR(.normal_data));         LONG(0 + ADDR(.normal_data));         LONG(SIZEOF(.normal_data));
    */
    LONG(LOADADDR(.ram_code)); LONG(0 + ADDR(.ram_code)); LONG(SIZEOF(.ram_code));
    LONG(LOADADDR(.PMUECCEDCTST_TVT_RAMCODE)); LONG(0 + ADDR(.PMUECCEDCTST_TVT_RAMCODE)); LONG(SIZEOF(.PMUECCEDCTST_TVT_RAMCODE));
    LONG(LOADADDR(.PFLASH_MON_ECCEDC_RAMCODE)); LONG(0 + ADDR(.PFLASH_MON_ECCEDC_RAMCODE)); LONG(SIZEOF(.PFLASH_MON_ECCEDC_RAMCODE));
    LONG(LOADADDR(.asil_initdata)); LONG(0 + ADDR(.asil_initdata)); LONG(SIZEOF(.asil_initdata));
    LONG(LOADADDR(.asil_zero_initdata)); LONG(0 + ADDR(.asil_zero_initdata)); LONG(SIZEOF(.asil_zero_initdata));
    LONG(LOADADDR(.qm_initdata)); LONG(0 + ADDR(.qm_initdata)); LONG(SIZEOF(.qm_initdata));
    LONG(LOADADDR(.qm_zero_initdata)); LONG(0 + ADDR(.qm_zero_initdata)); LONG(SIZEOF(.qm_zero_initdata));
    LONG(LOADADDR(.initdata)); LONG(0 + ADDR(.initdata)); LONG(SIZEOF(.initdata));
    LONG(-1);                             LONG(-1);                             LONG(-1);
  } > int_flash0_app

}

