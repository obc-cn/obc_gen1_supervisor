/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  XmlSecurity_Types.h
 *        \brief  Xml Security type definition header File
 *
 *      \details  Types definition header of the Xml Security for ISO 15118 Smart Charge Communication
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the XmlSecurity module. >> XmlSecurity.h
 *********************************************************************************************************************/
#if !defined (XMLSECURITY_TYPES_H)
# define XMLSECURITY_TYPES_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
# include "Std_Types.h"
# include "XmlSecurity_Cfg.h"
# include "Exi_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

# define XMLSECURITY_SHA256_DIGEST           32u
# define XMLSECURITY_P256R1_ECDSA_SIG_LEN    64u
# define XMLSECURITY_P256R1_PUBLIC_KEY_LEN   64u

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

typedef uint8   XmlSecurity_ConfigType;

typedef enum XmlSecurity_canonicalizationAlgorithmType
{
  XmlSecurity_CAT_Invalid,
  XmlSecurity_CAT_EXI
} XmlSecurity_CanonicalizationAlgorithmType;

typedef enum XmlSecurity_signatureAlgorithmType
{
  XmlSecurity_SAT_Invalid,
  XmlSecurity_SAT_ECDSA_SHA256
} XmlSecurity_SignatureAlgorithmType;

typedef enum XmlSecurity_digestMethodType
{
  XmlSecurity_DMT_Invalid,
  XmlSecurity_DMT_SHA256
} XmlSecurity_DigestMethodType;

typedef enum XmlSecurity_stateType
{
  XmlSec_State_Uninitialized = 0U,
  XmlSec_State_Initialized = 1U
} XmlSecurity_StateType;

typedef enum XmlSecurity_returnType
{
  /* general errors */
  XmlSec_RetVal_OK,                        /* E_OK */
  XmlSec_RetVal_Pending,                   /* operation is pending */
  XmlSec_RetVal_Busy,                      /* internal buffer locked */
  XmlSec_RetVal_NotOK,                     /* undefined error */
  XmlSec_RetVal_TargetBufferTooSmall,      /* target buffer to small */
  XmlSec_RetVal_AlgorithmNotSupported,     /* required algorithm not supported */
  XmlSec_RetVal_TransformationError,       /* error execution the required transformation */
  /* errors during signature generation */
  XmlSec_RetVal_NoReferenceDetected,       /* before signing there needs to exist at least on digest value in the reference section */
  XmlSec_RetVal_DigestGenerationError,     /* error generating the digest value */
  XmlSec_RetVal_SignatureGenerationError,  /* error generating the signature value */
  XmlSec_RetVal_ExiBufferTooSmall,         /* target EXI buffer to small */
  /* errors during signature validation */
  XmlSec_RetVal_InvalidReference,          /* error in reference validation */
  XmlSec_RetVal_SignatureValidationError,  /* error validating the signature value */
  XmlSec_RetVal_NoPublicKey                /* failed to get public key information */
} XmlSecurity_ReturnType;

typedef struct
{
  P2CONST(uint8, AUTOMATIC, XMLSECURITY_APPL_DATA) CertPtr;
  uint16 CertLen;
} XmlSecurity_CertType;

/* Signature generation workspace type */
typedef struct
{
  Exi_EncodeWorkspaceType                       ExiEncWs;
  XmlSecurity_CanonicalizationAlgorithmType     CanonicalizationAlgorithm;
  XmlSecurity_SignatureAlgorithmType            SignatureAlgorithm;
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) StoragePtr;    /* location where the signature will be stored */
  uint16                                        StorageLen;    /* available storage length in bytes */
  uint16                                        StorageOffset; /* offset in storage memory in bytes */
  uint8                                         SHA256Buf[XMLSECURITY_SHA256_DIGEST];
} XmlSecurity_SigGenWorkspaceType;

typedef P2FUNC(Std_ReturnType, XMLSECURITY_CBK_CODE, XmlSecurity_ExiURIDereferenceFctType)(
  P2VAR(uint8*, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiStructPtr,
  P2VAR(Exi_RootElementIdType, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiRootElementId,
  P2VAR(Exi_NamespaceIdType, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiNamespaceId,
  P2CONST(uint8, AUTOMATIC, XMLSECURITY_APPL_DATA) URIPtr,
  uint16 URILength);

typedef P2FUNC(Std_ReturnType, XMLSECURITY_CBK_CODE, XmlSecurity_GetPublicKeyFctType)(
  P2VAR(uint8*, AUTOMATIC, XMLSECURITY_APPL_VAR) PubKeyPtr,
  P2VAR(uint16, AUTOMATIC, XMLSECURITY_APPL_VAR) PubKeyLen,
  XmlSecurity_SignatureAlgorithmType SigAlgorithm);

/* Signature validation workspace type */
typedef struct
{
  Exi_EncodeWorkspaceType                 ExiEncWs;
  XmlSecurity_GetPublicKeyFctType         GetPublicKeyFct;
  XmlSecurity_ExiURIDereferenceFctType    ExiURIDereferenceFct;
  uint8                                   SHA256Buf[XMLSECURITY_SHA256_DIGEST];
} XmlSecurity_SigValWorkspaceType;

#endif /* XMLSECURITY_TYPES_H */

/**********************************************************************************************************************
 *  END OF FILE: XmlSecurity_Types.h
 *********************************************************************************************************************/
