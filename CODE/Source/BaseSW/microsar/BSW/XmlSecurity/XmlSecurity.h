/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  XmlSecurity.h
 *        \brief  Xml Security Header File
 *
 *      \details  Header file of the Xml Security for ISO 15118 Smart Charge Communication.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  -------------------------------------------------------------------------------------------------------------------
 *  Daniel Dausend                visdd         Vector Informatik GmbH
 *  Fabian Eisele                 visefa        Vector Informatik GmbH
 *  Phanuel Hieber                visphh        Vector Informatik GmbH
 *  Danny Bogner                  visdyb        Vector Informatik GmbH
 *  -------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  01.00.00  2013-01-16  visdd                 Created
 *  01.00.01  2013-07-11  visdd   ESCAN00068932 Adding a Reference fails in case the buffer limit is reached
 *  01.00.02  2013-10-01  visdd   ESCAN00069386 SHA512 returns no error but validates the signature
 *  02.00.00  2013-11-14  visdd   ESCAN00071961 Support new EXI types introduced with Exi module version 2.00.00
 *                                ESCAN00069258 AR4-450: Usage of section PBCFG in PB files
 *  02.01.00  2014-04-25  visefa  ESCAN00073494 'Exi_InitEncodeWorkspace' : too few arguments for call
 *                                ESCAN00074502 Temp Buffer has now to be provided by the user
 *                                ESCAN00074503 Crypto Workspace has now to be provided by the user
 *  02.02.00  2014-07-23  visefa  ESCAN00077307 SHA256 Workspace has now to be provided by the user
 *            2014-07-23  visefa  ESCAN00077322 KeyInfo element support can be disabled
 *            2014-08-13  visefa  ESCAN00077817 EXI Reference Validation and Signature Validation are now consecutively
 *  02.03.00  2015-02-19  visefa  ESCAN00081353 Support EXI Tx Streaming
 *  02.03.01  2015-09-11  visefa  ESCAN00085163 Compiler warning (dcc:1522): statement not reached
 *  04.00.00  2017-01-18  visefa  ESCAN00093608 XmlSec_ReturnType is now enumeration instead of uint8 using defines
 *            2017-06-08  visphh  ESCAN00095474 Add CSM (ASR4.3) Support
 *  04.01.00  2017-09-15  visphh  STORYC-2512   Removed SHA256 WorkSpace from XmlSecurity functions
 *  05.00.00  2017-11-16  visphh  ESCAN00097435 XmlSecurity needs Crypto_Types.h if CSM is enabled
 *            2017-11-20  visphh  ESCAN00098109 Missing memory mapping
 *  05.00.01  2018-01-24  visphh  ESCAN00098091 Invalid DigestValue is ignored in the SignedInfo-Element
              2018-01-24  visphh  STORYC-4021   Removed Key Info Support
              2018-01-24  visphh  STORYC-4029   Removed ASR3 Support
 *  06.00.00  2018-02-20  visphh  ESCAN00098450 PublicKey length and first byte of PublicKey is not checked correct
              2018-02-20  visphh  ESCAN00098771 XmlSecurity_AddExiReference overwrites last reference
              2018-02-20  visphh  STORYC-3887   Code Refactoring and CDD (Proc3.0)
              2018-05-15  visphh  STORYC-4020   Remove Crypto Legacy support. Support only CSM (ASR4.3)
 *  06.00.01  2019-03-22  visdyb  ESCAN00102539 Exi streaming: EXI returns EOS
 *  07.00.00  2019-05-02  visdyb  STORYC-8294   Migration to MISRA2012
 *********************************************************************************************************************/
#if !defined (XMLSECURITY_H)
# define XMLSECURITY_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
# include "XmlSecurity_Types.h"
# include "XmlSecurity_Lcfg.h"
# include "Exi.h"
# include "Exi_SchemaTypes.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* Vendor and module identification */
# define XMLSECURITY_VENDOR_ID                       (30u)
# define XMLSECURITY_MODULE_ID                       (255u)
# define XMLSECURITY_INSTANCE_ID_DET                 (113u)

/* AUTOSAR Software specification version information */
# define XMLSECURITY_AR_RELEASE_MAJOR_VERSION        (4u)
# define XMLSECURITY_AR_RELEASE_MINOR_VERSION        (3u)
# define XMLSECURITY_AR_RELEASE_REVISION_VERSION     (0u)

/* ----- Component version information (decimal version of ALM implementation package) ----- */
# define XMLSECURITY_SW_MAJOR_VERSION                (7u)
# define XMLSECURITY_SW_MINOR_VERSION                (0u)
# define XMLSECURITY_SW_PATCH_VERSION                (0u)

/* ----- API service IDs ----- */
/*!< Service ID: XmlSecurity_Init() */
# define XMLSECURITY_API_ID_INIT                     (0x01u)
/*!< Service ID: XmlSecurity_GetVersionInfo() */
# define XMLSECURITY_API_ID_GET_VERSION_INFO         (0x02u)
/*!< Service ID: XmlSecurity_InitSigGenWorkspace() */
# define XMLSECURITY_API_ID_INIT_SIG_GEN_WORKSPACE   (0x03u)
/*!< Service ID: XmlSecurity_InitSigValWorkspace() */
# define XMLSECURITY_API_ID_INIT_SIG_VAL_WORKSPACE   (0x04u)
/*!< Service ID: XmlSecurity_AddExiReference() */
# define XMLSECURITY_API_ID_ADD_EXI_REFERENCE        (0x05u)
/*!< Service ID: XmlSecurity_GenerateSignature() */
# define XMLSECURITY_API_ID_GENERATE_SIGNATURE       (0x06u)
/*!< Service ID: XmlSecurity_ValidateExiSignature() */
# define XMLSECURITY_API_ID_VALIDATE_SIGNATURE       (0x07u)

/* ----- Error codes ----- */
/*!< used to check if no error occurred - use a value unequal to any error code */
# define XMLSECURITY_E_NO_ERROR                      (0x00u)
/*!< Error code: API service used with invalid pointer parameter (NULL) */
# define XMLSECURITY_E_PARAM_POINTER                 (0x01u)
/*!< Error code: API service used with invalid pointer length (0) */
# define XMLSECURITY_E_PARAM_POINTER_SIZE            (0x02u)
/*!< Error code: API service used with invalid signature algorithm (!= ECDSA_SHA256) */
# define XMLSECURITY_E_PARAM_SIGNATURE_ALGO          (0x03u)
/*!< Error code: API service used with invalid canonicalization algorithm (!= EXI) */
# define XMLSECURITY_E_PARAM_CANONINCALIZATION_ALGO  (0x04u)

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
# define XMLSECURITY_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /*  MD_MSR_MemMap */
/***********************************************************************************************************************
 *  XmlSecurity_InitMemory
 **********************************************************************************************************************/
/*! \brief       Power-up memory initialization
 *  \details     -
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
***********************************************************************************************************************/
FUNC(void, XMLSECURITY_CODE) XmlSecurity_InitMemory(void);

/***********************************************************************************************************************
 *  XmlSecurity_Init
 **********************************************************************************************************************/
/*! \brief       Initializes component
 *  \details     -
 *  \pre         -
 *  \context     TASK
 *  \reentrant   FALSE
 *  \synchronous TRUE
***********************************************************************************************************************/
FUNC(void, XMLSECURITY_CODE) XmlSecurity_Init(void);

# if (XMLSECURITY_VERSION_INFO_API == STD_ON)
/***********************************************************************************************************************
 *  XmlSecurity_GetVersionInfo
 **********************************************************************************************************************/
/*! \brief       Returns the version information
 *  \details     Returns the version information, vendor ID and AUTOSAR module ID of the component.
 *  \param[out]  VersionInfoPtr    Pointer to where to store the version information. Parameter must not be NULL.
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *  \trace       CREQ-152433
***********************************************************************************************************************/
FUNC(void, XMLSECURITY_CODE) XmlSecurity_GetVersionInfo(
  P2VAR(Std_VersionInfoType, AUTOMATIC, XMLSECURITY_APPL_VAR) VersionInfoPtr);
# endif
  /* XMLSECURITY_VERSION_INFO_API */

/***********************************************************************************************************************
 *  XmlSecurity_InitSigGenWorkspace
 **********************************************************************************************************************/
/*! \brief         Initializes the XmlSecurity signature generation workspace.
 *  \details       Prepare a user provided signature generation workspace for further usage.
 *  \param[in]     SigGenWsPtr                Pointer to the signature generation workspace to be initialized.
 *  \param[in]     SignatureBufPtr            Target buffer where to store the generated signature.
 *  \param[in]     SignatureBufLen            Available target buffer length.
 *  \param[in]     SignatureAlgorithm         Signature algorithm identifier.
 *  \param[in]     CanonicalizationAlgorithm  Canonicalization algorithm identifier.
 *  \return        XmlSec_RetVal_OK                     Workspace successfully prepared
 *  \return        XmlSec_RetVal_NotOK                  Failure during workspace preparation
 *  \return        XmlSec_RetVal_TargetBufferTooSmall   Signature will not fit into the target buffer
 *  \return        XmlSec_RetVal_AlgorithmNotSupported  Siganture or hash algorthm is not supported
 *  \pre           -
 *  \context       TASK|ISR2
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *  \note          Only reentrant if different workspaces are used.
 *  \trace         CREQ-152427
***********************************************************************************************************************/
FUNC(XmlSecurity_ReturnType, XMLSECURITY_CODE) XmlSecurity_InitSigGenWorkspace(
  P2VAR(XmlSecurity_SigGenWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) SigGenWsPtr,
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) SignatureBufPtr,
  uint16 SignatureBufLen,
  XmlSecurity_SignatureAlgorithmType SignatureAlgorithm,
  XmlSecurity_CanonicalizationAlgorithmType CanonicalizationAlgorithm);

/***********************************************************************************************************************
 *  XmlSecurity_InitSigValWorkspace
 **********************************************************************************************************************/
/*! \brief         Initializes the XmlSecurity signature validation workspace.
 *  \details       Prepare a user provided signature validation workspace for further usage.
 *  \param[in]     SigValWsPtr             Pointer to the signature validation workspace to be initialized.
 *  \param[in]     ExiURIDereferenceFct    Pointer to the URI de-referencing function.
 *  \param[in]     GetPublicKeyFct         Pointer to the function for public key information.
 *  \return        E_OK                    Workspace successfully prepared.
 *  \return        E_NOT_OK                Failure during workspace preparation.
 *  \pre           -
 *  \context       TASK|ISR2
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *  \note          Only reentrant if different workspaces are used.
 *  \trace         CREQ-152430
***********************************************************************************************************************/
FUNC(Std_ReturnType, XMLSECURITY_CODE) XmlSecurity_InitSigValWorkspace(
  P2VAR(XmlSecurity_SigValWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) SigValWsPtr,
  XmlSecurity_ExiURIDereferenceFctType ExiURIDereferenceFct,
  XmlSecurity_GetPublicKeyFctType GetPublicKeyFct);

/***********************************************************************************************************************
 *  XmlSecurity_AddExiReference
 **********************************************************************************************************************/
/*! \brief         Adds a reference that should be signed.
 *  \details       Adding a reference that should be signed. Adding more references is done by calling this function
 *                 multiple time.\n
 *                 This method uses EXI as transformation algorithm to determine the required binary data as input for
 *                 the digest algorithm. It is expected that the workspace uses the EXI struct representation to store
 *                 the complete signature. The complete reference is placed inside this EXI struct. This means that the
 *                 application does not need to add any further information.
 *  \param[in]     SigGenWsPtr               Pointer to the signature generation workspace.
 *  \param[in]     ExiStreamBufPtr           Pointer to the Outputbuffer (Pbuf) of the EXI stream
 *  \param[in]     ReferenceStruct           Pointer to the data that should be added to the signature.
 *  \param[in]     ReferenceURI              Pointer to the URI identifier. Only same document references supported.
                                             Leading '#' is not required here.
 *  \param[in]     ReferenceElementId        EXI root element ID of the given data element. This is required for EXI
                                             transformation.
 *  \param[in]     ExiStreamBufLen           Length of the EXI stream buffer.
 *  \param[in]     ReferenceURILength        Length of the URI identifier.
 *  \param[in]     Namespace                 Namespace identifier used for the EXI fragment grammar.
 *  \param[in]     CsmSHA256JobId            CsmJobId for the SHA256-job Csm/CsmJobs/CsmJob/CsmJobId.
 *  \return        XmlSec_RetVal_OK:                     Finished successfully, the reference element was added to the
 *                                                       signature struct.
 *  \return        XmlSec_RetVal_AlgorithmNotSupported:  The selected canonicalization or signature algorithm is not
 *                                                       supported.
 *  \return        XmlSec_RetVal_TargetBufferTooSmall:   Not enough memory available to store the resulting struct.
 *  \return        XmlSec_RetVal_Busy:                   Local buffer busy. Call again later.
 *  \return        XmlSec_RetVal_TransformationError:    EXI transformation error.
 *  \return        XmlSec_RetVal_DigestGenerationError:  Error generating the digest value.
 *  \return        XmlSec_RetVal_NotOK:                  Unspecified Error.
 *  \pre           Signature generation workspace must be initialized (XmlSecurity_InitSigGenWorkspace).\n
 *                 There need to be enough memory available inside the signature generation workspace to store
 *                 the reference struct.
 *  \context       TASK|ISR2
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *  \note          Only EXI transformation and SHA256 as digest algorithm is supported. Only reentrant if different workspaces are used.
 *  \trace         CREQ-152428
 *  \trace         CREQ-190286
***********************************************************************************************************************/
FUNC(XmlSecurity_ReturnType, XMLSECURITY_CODE) XmlSecurity_AddExiReference(
  P2VAR(XmlSecurity_SigGenWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) SigGenWsPtr,
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiStreamBufPtr,
  P2CONST(uint8, AUTOMATIC, XMLSECURITY_APPL_DATA) ReferenceStruct,
  P2CONST(uint8, AUTOMATIC, XMLSECURITY_APPL_DATA) ReferenceURI,
  Exi_RootElementIdType ReferenceElementId,
  uint16 ExiStreamBufLen,
  uint16 ReferenceURILength,
  Exi_NamespaceIdType Namespace,
  uint32 CsmSHA256JobId);

/***********************************************************************************************************************
 *  XmlSecurity_GenerateSignature
 **********************************************************************************************************************/
/*! \brief         Generates the XML signature value.
 *  \details       Generate the XML signature value for the previously added references. To generate the signature value
 *                 the user must provide a valid private key.
 *  \param[in]     SigGenWsPtr                         Pointer to the signature generation workspace.
 *  \param[in]     ExiStreamBufPtr                     Pointer to the EXI stream buffer.
 *  \param[in]     ExiStreamBufLen                     Length of the EXI stream buffer.
 *  \param[in]     CsmPrivateKeyId                     CsmKeyId to the PrivateKey CSM Csm/CsmKeys/CsmKey/CsmKeyId.
 *  \param[in]     CsmECDSASignJobId                   CsmJobId to the ECDSA-Sign-job Csm/CsmJobs/CsmJob/Csm-.
 *  \return        XmlSec_RetVal_OK:                   Finished successfully. The signature is ready for transmission.
 *  \return        XmlSec_RetVal_AlgorithmNotSupported:     The selected canonicalization or signature algorithm is
 *                                                          not supported.
 *  \return        XmlSec_RetVal_Busy:                      Local buffer busy. Call again later.
 *  \return        XmlSec_RetVal_TransformationError:       EXI transformation error.
 *  \return        XmlSec_RetVal_NoReferenceDetected:       No reference found. A signature does not make sense without
 *                                                          a reference.
 *  \return        XmlSec_RetVal_SignatureGenerationError:  Error generating the signature value.
 *  \return        XmlSec_RetVal_NotOK:                     Unspecified Error.
 *  \pre           Signature generation workspace must be initialized (XmlSecurity_InitSigGenWorkspace).\n
 *                 One or more references must be added to the reference section using XmlSecurity_AddExiReference.
 *  \context       TASK|ISR2
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *  \note          Only reentrant if different workspaces are used.
***********************************************************************************************************************/
FUNC(XmlSecurity_ReturnType, XMLSECURITY_CODE) XmlSecurity_GenerateSignature(
  P2VAR(XmlSecurity_SigGenWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) SigGenWsPtr,
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiStreamBufPtr,
  uint16 ExiStreamBufLen,
  uint32 CsmPrivateKeyId,
  uint32 CsmECDSASignJobId);

/***********************************************************************************************************************
 *  XmlSecurity_ValidateExiSignature
 **********************************************************************************************************************/
/*! \brief         Validates the XML signature value.
 *  \details       Validate that the signature for the references is valid by using a user provided public key. Only if
 *                 canonicalization of the SignedInfo element was successful and all reference elements are valid the
 *                 received signature value is validated.
 *  \param[in]     SigValWsPtr                           Pointer to the signature validation workspace.
 *  \param[in]     SignaturePtr                          Pointer to the root of the signatures EXI struct element.
 *  \param[in]     ExiStreamBufPtr                       Pointer to the EXI stream buffer.
 *  \param[in]     ExiStreamBufLen                       Length of the EXI stream buffer.
 *  \param[in]     CsmECDSAVerifyJobId                   CsmJobId to the ECDSA-Verify-Job Csm/CsmJobs/CsmJob/CsmJobId.
 *  \param[in]     CsmPublicKeyId                        CsmKeyId to the PublicKey CSM Csm/CsmKeys/CsmKey/CsmKeyId.
 *  \param[in]     CsmSHA256JobId                        CsmJobId for the SHA256-job Csm/CsmJobs/CsmJob/CsmJobId.
 *  \return        XmlSec_RetVal_OK:                     Finished successfully. The received signature is valid.
 *  \return        XmlSec_RetVal_AlgorithmNotSupported:       The received KeyInfo, canonicalization or
 *                                                            signature algorithm is not supported or no keying material
 *                                                            is available to validate the signature.
 *  \return        XmlSec_RetVal_Busy:                        Local buffer busy. Call again later.
 *  \return        XmlSec_RetVal_TransformationError:         EXI transformation error.
 *  \return        XmlSec_RetVal_InvalidReference:            Reference validation failed for at least one reference.
 *  \return        XmlSec_RetVal_CertificateValidationError:  Certificate validation failed. Received certificate is
 *                                                            untrusted.
 *  \return        XmlSec_RetVal_UnknownIssuerSerial:         No certificate available for the received IssuerSerial.
 *  \return        XmlSec_RetVal_NoPublicKey:                 No public key information available to validate the
 *                                                            signature.
 *  \return        XmlSec_RetVal_DigestGenerationError:       Error generating the digest value.
 *  \return        XmlSec_RetVal_SignatureValidationError:    Signature value invalid.
 *  \return        XmlSec_RetVal_NotOK:                       Unspecified Error.
 *  \pre           Signature validation workspace must be initialized (XmlSecurity_InitSigValWorkspace).\n
 *                 The public key must be the right counter-part of the used private key used during signature
 *                 generation.
 *  \context       TASK|ISR2
 *  \reentrant     TRUE
 *  \synchronous   TRUE
 *  \note          Only reentrant if different workspaces are used.
***********************************************************************************************************************/
FUNC(XmlSecurity_ReturnType, XMLSECURITY_CODE) XmlSecurity_ValidateExiSignature(
  P2VAR(XmlSecurity_SigValWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) SigValWsPtr,
  P2CONST(Exi_XMLSIG_SignatureType, AUTOMATIC, XMLSECURITY_APPL_DATA) SignaturePtr,
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiStreamBufPtr,
  uint16 ExiStreamBufLen,
  uint32 CsmECDSAVerifyJobId,
  uint32 CsmPublicKeyId,
  uint32 CsmSHA256JobId);

# define XMLSECURITY_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /*  MD_MSR_MemMap */


#endif /* XMLSECURITY_H */

/**********************************************************************************************************************
 *  END OF FILE: XmlSecurity.h
 *********************************************************************************************************************/
