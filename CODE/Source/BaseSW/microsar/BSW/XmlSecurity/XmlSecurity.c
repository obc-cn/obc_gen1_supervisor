/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  XmlSecurity.c
 *        \brief  Xml Security Source Code File
 *
 *      \details  Implementation of the Xml Security for ISO 15118 Smart Charge Communication.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the XmlSecurity module. >> XmlSecurity.h
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/
#define XMLSECURITY_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "XmlSecurity.h"
#include "Exi_SchemaTypes.h"
#include "Csm.h"
#include "IpBase.h"

#if (XMLSECURITY_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/
/* Check the version of XmlSecurity header file */
#if (  (XMLSECURITY_SW_MAJOR_VERSION != 0x07u) \
    || (XMLSECURITY_SW_MINOR_VERSION != 0x00u) \
    || (XMLSECURITY_SW_PATCH_VERSION != 0x00u) )
# error "Vendor specific version numbers of XmlSecurity.c and XmlSecurity.h are inconsistent"
#endif

/**********************************************************************************************************************
 *  MISRA & PClint
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 **********************************************************************************************************************/
#if !defined (XMLSECURITY_LOCAL)                                                                                        /* COV_XMLSECURITY_COMPATIBILITY */
# define XMLSECURITY_LOCAL static
#endif

#if !defined (XMLSECURITY_LOCAL_INLINE)                                                                                 /* COV_XMLSECURITY_COMPATIBILITY */
# define XMLSECURITY_LOCAL_INLINE LOCAL_INLINE
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 **********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 **********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA
 **********************************************************************************************************************/
#define XMLSECURITY_START_SEC_CONST_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

XMLSECURITY_LOCAL CONST(uint8, XMLSECURITY_CONST) XmlSecurity_ExiTransformUri[XMLSECURITY_EXI_TRANSFORM_URI_LEN] =
  { XMLSECURITY_EXI_TRANSFORM_URI };
XMLSECURITY_LOCAL CONST(uint8, XMLSECURITY_CONST) XmlSecurity_EcdsaSha256Uri[XMLSECURITY_ECDSA_SHA256_URI_LEN] =
  { XMLSECURITY_ECDSA_SHA256_URI };
XMLSECURITY_LOCAL CONST(uint8, XMLSECURITY_CONST) XmlSecurity_Sha256Uri[XMLSECURITY_SHA256_URI_LEN] =
  { XMLSECURITY_SHA256_URI };

#define XMLSECURITY_STOP_SEC_CONST_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GLOBAL DATA
 **********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/
#define XMLSECURITY_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  XmlSecurity_CreateCanonicalizedHash
 **********************************************************************************************************************/
/*! \brief          This method is called whenever a EXI transformation on some data is required. The input data will be
 *                  encoded using EXI in schema-informed mode. This means, only EXI schema-conform data can be processed
 *                  correctly.
 *  \details        -
 *  \param[in]      DataStructPtr             Pointer to the EXI data struct that should be EXI encoded
 *  \param[in]      DataElementId             EXI root element ID.
 *  \param[in]      Namespace                 EXI Namespace.
 *  \param[in, out] ExiStreamBufPtr           Pointer to the EXI stream buffer.
 *  \param[in]      ExiStreamBufLen           Length of the EXI stream buffer.
 *  \param[in, out] ExiEncWsPtr               Pointer to EXI Workspace.
 *  \param[in, out] SHA256BufPtr              Pointer to the SHA256 Buffer.
 *  \param[in]      CsmSHA256JobId            Job Id of the SHA256 Job of the CSM.
 *  \return         XmlSec_RetVal_OK:         EXI stream generated successfully.
 *                  XmlSec_RetVal_TransformationError:  EXI stream could not be generated.
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
XMLSECURITY_LOCAL FUNC(XmlSecurity_ReturnType, XMLSECURITY_CODE) XmlSecurity_CreateCanonicalizedHash(
  P2CONST(uint8, AUTOMATIC, XMLSECURITY_APPL_DATA) DataStructPtr,
  Exi_RootElementIdType DataElementId,
  Exi_NamespaceIdType Namespace,
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiStreamBufPtr,
  uint16 ExiStreamBufLen,
  P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiEncWsPtr,
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) SHA256BufPtr,
  uint32 CsmSHA256JobId);

/**********************************************************************************************************************
*  XmlSecurity_GetExiStreamingData
**********************************************************************************************************************/
/*! \brief          This method is called whenever a EXI transformation on some data is required. The input data will be
 *                  encoded using EXI in schema-informed mode. This means, only EXI schema-conform data can be processed
 *                  correctly.
 *  \details        If the provided temporary buffer is not big enough, only partial data will be transformed.
 *                  Multiple successive calls with adjusted CurrentStreamPosition can be used to fetch all data.
 *                  The StreamComplete flag of the ExiEncsWsPtr->EncWs shall be checked to determine if the stream is
 *                  complete or if it is partial data.
 *  \param[in]      DataStructPtr             Pointer to the EXI data struct that should be EXI encoded.
 *  \param[in, out] TempBuffer                Temporary buffer on which the Exi module is working.
 *  \param[in, out] ExiEncWsPtr               Pointer to EXI Workspace.
 *  \param[in]      CurrentStreamPosition     The offset where exi encoding should start.
 *  \param[in]      DataElementId             EXI root element ID.
 *  \param[in]      Namespace                 EXI Namespace.
 *
 *  \return         XmlSec_RetVal_OK:         EXI stream generated successfully.
 *                  XmlSec_RetVal_TransformationError:  EXI stream could not be generated.
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
XMLSECURITY_LOCAL FUNC(XmlSecurity_ReturnType, XMLSECURITY_CODE) XmlSecurity_GetExiStreamingData(
  P2CONST(uint8, AUTOMATIC, XMLSECURITY_APPL_DATA) DataStructPtr,
  P2VAR(IpBase_PbufType, AUTOMATIC, XMLSECURITY_APPL_VAR) TempBuffer,
  P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiEncWsPtr,
  uint32 CurrentStreamPosition,
  Exi_RootElementIdType DataElementId,
  Exi_NamespaceIdType Namespace);


/**********************************************************************************************************************
 *  XmlSecurity_ValidateExiReferences
 **********************************************************************************************************************/
/*! \brief       This method is called to validate the references included inside a SignedInfo element. It will call the
 *               required user callback functions to de-reference the found URIs and check the validity of the digest
 *               value.
 *  \details     -
 *  \param[in]   SigValWsPtr                       Pointer to the signature validation workspace.
 *  \param[in]   SignedInfoPtr                     Pointer to the SignedInfo element.
 *  \param[in]   BufPtr                            Pointer to the temporary buffer that can be used to store intermediate
 *                                                 data.
 *  \param[in]   BufLen                            Temporary buffer length.
 *  \param[in]   CsmSHA256JobId                    Job Id of the SHA256 Job of the CSM.
 *  \return      XmlSec_RetVal_OK:                 All references are valid.\n
 *               XmlSec_RetVal_InvalidReference:  At least one reference is invalid.\n
 *  \pre         none
 *  \pre         The XmlSecurity has to be initialized with a call of XmlSecurity_Init.\n
 *               Further the signature validation workspace has to be initialized using the XmlSecurity_InitSigValWorkspace
 *               API in a previous step.\n
 *  \context     TASK|ISR2
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
XMLSECURITY_LOCAL FUNC(XmlSecurity_ReturnType, XMLSECURITY_CODE) XmlSecurity_ValidateExiReferences(
  P2VAR(XmlSecurity_SigValWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) SigValWsPtr,
  P2CONST(Exi_XMLSIG_SignedInfoType, AUTOMATIC, XMLSECURITY_APPL_DATA) SignedInfoPtr,
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) BufPtr,
  uint16 BufLen,
  uint32 CsmSHA256JobId);

/**********************************************************************************************************************
 *  XmlSecurity_GetNextReferenceElement
 **********************************************************************************************************************/
/*! \brief       This method finds the last reference by searching for a null pointer and then adds a reference to the
 *               next pointer and fills the value
 *  \details     -
 *  \param[in]   SigGenWsPtr               Pointer to the signature generation workspace
 *  \param[in]   CurrentReferenceElement   Pointer to reference element where next reference has to be added
 *  \return      E_OK:     Next reference added successfully.
 *               E_NOT_OK: Next reference could not be added.
 *  \pre         -
 *  \context     TASK|ISR2
 *  \reentrant   TRUE
 *  \synchronous TRUE
 *********************************************************************************************************************/
XMLSECURITY_LOCAL FUNC(Std_ReturnType, XMLSECURITY_CODE) XmlSecurity_GetNextReferenceElement(
  P2VAR(XmlSecurity_SigGenWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) SigGenWsPtr,
  P2VAR(Exi_XMLSIG_ReferenceType*, AUTOMATIC, XMLSECURITY_APPL_VAR) CurrentReferenceElement);

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  XmlSecurity_InitMemory
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, XMLSECURITY_CODE) XmlSecurity_InitMemory(void)
{
  /* #10 Do nothing*/
}

/***********************************************************************************************************************
 *  XmlSecurity_Init
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
FUNC(void, XMLSECURITY_CODE) XmlSecurity_Init(void)
{
  /* #10 Do nothing */
}

#if (XMLSECURITY_VERSION_INFO_API == STD_ON)
/***********************************************************************************************************************
 *  XmlSecurity_GetVersionInfo
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, XMLSECURITY_CODE) XmlSecurity_GetVersionInfo(
  P2VAR(Std_VersionInfoType, AUTOMATIC, XMLSECURITY_APPL_VAR) VersionInfoPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorID = XMLSECURITY_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if (XMLSECURITY_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check if parameter is a NullPointer */
  if (VersionInfoPtr == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else
# endif /* XMLSECURITY_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Copy version information */
    VersionInfoPtr->vendorID = (uint16)XMLSECURITY_VENDOR_ID;
    VersionInfoPtr->moduleID = (uint16)XMLSECURITY_MODULE_ID;
    VersionInfoPtr->sw_major_version = (uint8)XMLSECURITY_SW_MAJOR_VERSION;
    VersionInfoPtr->sw_minor_version = (uint8)XMLSECURITY_SW_MINOR_VERSION;
    VersionInfoPtr->sw_patch_version = (uint8)XMLSECURITY_SW_PATCH_VERSION;
  }

# if (XMLSECURITY_DEV_ERROR_REPORT == STD_ON)
  if (errorID != XMLSECURITY_E_NO_ERROR)
  {
    (void)Det_ReportError(XMLSECURITY_MODULE_ID, XMLSECURITY_INSTANCE_ID_DET, XMLSECURITY_API_ID_GET_VERSION_INFO, errorID);
  }
# else
  XMLSECURITY_DUMMY_STATEMENT(errorID); /* PRQA S 3112, 2983 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
# endif /* XMLSECURITY_DEV_ERROR_REPORT */

  return;
}
#endif /* XMLSECURITY_VERSION_INFO_API == STD_ON */

/***********************************************************************************************************************
 *  XmlSecurity_InitSigGenWorkspace
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(XmlSecurity_ReturnType, XMLSECURITY_CODE) XmlSecurity_InitSigGenWorkspace(
  P2VAR(XmlSecurity_SigGenWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) SigGenWsPtr,
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) SignatureBufPtr,
  uint16 SignatureBufLen,
  XmlSecurity_SignatureAlgorithmType SignatureAlgorithm,
  XmlSecurity_CanonicalizationAlgorithmType CanonicalizationAlgorithm)
{
  /* ----- Local Variables ---------------------------------------------- */
  XmlSecurity_ReturnType retValue = XmlSec_RetVal_NotOK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = XMLSECURITY_E_NO_ERROR;
  P2VAR(Exi_XMLSIG_SignatureType, AUTOMATIC, XMLSECURITY_APPL_VAR) signaturePtr;
  /* store size of all required struct elements */
  const uint16 minimumStorageRequired = (uint16)(sizeof(Exi_XMLSIG_SignatureType) +
                                          sizeof(Exi_XMLSIG_SignedInfoType) +
                                          sizeof(Exi_XMLSIG_SignatureValueType) +
                                          sizeof(Exi_XMLSIG_CanonicalizationMethodType) +
                                          (4UL * sizeof(Exi_XMLSIG_AttributeAlgorithmType)) +
                                          sizeof(Exi_XMLSIG_SignatureMethodType) +
                                          sizeof(Exi_XMLSIG_ReferenceType) +
                                          sizeof(Exi_XMLSIG_AttributeURIType) +
                                          sizeof(Exi_XMLSIG_TransformsType) +
                                          sizeof(Exi_XMLSIG_TransformType) +
                                          sizeof(Exi_XMLSIG_DigestMethodType) +
                                          sizeof(Exi_XMLSIG_DigestValueType));

  /* ----- Development Error Checks ------------------------------------- */
#if (XMLSECURITY_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of input parameters. */
  if (SigGenWsPtr == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (SignatureBufPtr == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (SignatureBufLen == 0U)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER_SIZE;
  }
  else if (SignatureAlgorithm != XmlSecurity_SAT_ECDSA_SHA256)
  {
    errorID = XMLSECURITY_E_PARAM_SIGNATURE_ALGO;
    retValue = XmlSec_RetVal_AlgorithmNotSupported;
  }
  else if (CanonicalizationAlgorithm != XmlSecurity_CAT_EXI)
  {
    errorID = XMLSECURITY_E_PARAM_CANONINCALIZATION_ALGO;
    retValue = XmlSec_RetVal_AlgorithmNotSupported;
  }
  else
#endif /* XMLSECURITY_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    SigGenWsPtr->StoragePtr = SignatureBufPtr;
    SigGenWsPtr->StorageLen = SignatureBufLen;
    SigGenWsPtr->StorageOffset = 0;
    SigGenWsPtr->SignatureAlgorithm = XmlSecurity_SAT_Invalid;
    SigGenWsPtr->CanonicalizationAlgorithm = XmlSecurity_CAT_Invalid;

    /* #20 check if there is enough storage available to hold
     * at least one signature struct with minimum required elements
     */
    if ( SigGenWsPtr->StorageLen < minimumStorageRequired )
    {
      /* the minimum size to store a <Signature> element is not available */
      retValue = XmlSec_RetVal_TargetBufferTooSmall;
    }
    else
    {
      /* #30 Set variables and calculate storage offset. */
      IpBase_Fill((P2VAR(IpBase_CopyDataType, AUTOMATIC, XMLSECURITY_APPL_VAR))&SigGenWsPtr->StoragePtr[0], 0x00, minimumStorageRequired); /* PRQA S 0315 */ /* MD_MSR_VStdLibCopy */
      /* create all required data elements */ /* PRQA S 0310, 3305 47 */ /* MD_XmlSecurity_11.4, MD_XmlSecurity_3305 */
      signaturePtr =
          (P2VAR(Exi_XMLSIG_SignatureType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[0]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(*signaturePtr);
      signaturePtr->SignatureValue =
          (P2VAR(Exi_XMLSIG_SignatureValueType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_SignatureValueType);
      signaturePtr->SignedInfo =
          (P2VAR(Exi_XMLSIG_SignedInfoType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_SignedInfoType);
      signaturePtr->SignedInfo->CanonicalizationMethod =
          (P2VAR(Exi_XMLSIG_CanonicalizationMethodType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_CanonicalizationMethodType);
      signaturePtr->SignedInfo->CanonicalizationMethod->Algorithm =
          (P2VAR(Exi_XMLSIG_AttributeAlgorithmType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_AttributeAlgorithmType);
      signaturePtr->SignedInfo->SignatureMethod =
          (P2VAR(Exi_XMLSIG_SignatureMethodType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_SignatureMethodType);
      signaturePtr->SignedInfo->SignatureMethod->Algorithm =
          (P2VAR(Exi_XMLSIG_AttributeAlgorithmType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_AttributeAlgorithmType);
      signaturePtr->SignedInfo->Reference =
          (P2VAR(Exi_XMLSIG_ReferenceType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_ReferenceType);
      signaturePtr->SignedInfo->Reference->URI =
          (P2VAR(Exi_XMLSIG_AttributeURIType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_AttributeURIType);
      signaturePtr->SignedInfo->Reference->URIFlag = 1;
      signaturePtr->SignedInfo->Reference->Transforms =
          (P2VAR(Exi_XMLSIG_TransformsType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_TransformsType);
      signaturePtr->SignedInfo->Reference->TransformsFlag = 1;
      signaturePtr->SignedInfo->Reference->Transforms->Transform =
          (P2VAR(Exi_XMLSIG_TransformType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_TransformType);
      signaturePtr->SignedInfo->Reference->Transforms->Transform->Algorithm =
          (P2VAR(Exi_XMLSIG_AttributeAlgorithmType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_AttributeAlgorithmType);
      signaturePtr->SignedInfo->Reference->DigestMethod =
          (P2VAR(Exi_XMLSIG_DigestMethodType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_DigestMethodType);
      signaturePtr->SignedInfo->Reference->DigestMethod->Algorithm =
          (P2VAR(Exi_XMLSIG_AttributeAlgorithmType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_AttributeAlgorithmType);
      signaturePtr->SignedInfo->Reference->DigestValue =
          (P2VAR(Exi_XMLSIG_DigestValueType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_DigestValueType);

      /* set signature canonicalization, only Exi supported so far */
      signaturePtr->SignedInfo->CanonicalizationMethod->Algorithm->Length = sizeof(XmlSecurity_ExiTransformUri) - 1U;
      /* PRQA S 0315 1 */ /* MD_MSR_VStdLibCopy */
      IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, XMLSECURITY_APPL_VAR))&signaturePtr->SignedInfo->CanonicalizationMethod->Algorithm->Buffer[0],
                  (P2CONST(IpBase_CopyDataType, AUTOMATIC, XMLSECURITY_APPL_DATA))&XmlSecurity_ExiTransformUri[0],
                  signaturePtr->SignedInfo->CanonicalizationMethod->Algorithm->Length);
      SigGenWsPtr->SignatureAlgorithm = SignatureAlgorithm;
      /* set signature method, only ecdh-sha256 supported so far */
      signaturePtr->SignedInfo->SignatureMethod->Algorithm->Length = sizeof(XmlSecurity_EcdsaSha256Uri) - 1U;
      /* PRQA S 0315 1 */ /* MD_MSR_VStdLibCopy */
      IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, XMLSECURITY_APPL_VAR))&signaturePtr->SignedInfo->SignatureMethod->Algorithm->Buffer[0],
                  (P2CONST(IpBase_CopyDataType, AUTOMATIC, XMLSECURITY_APPL_DATA))&XmlSecurity_EcdsaSha256Uri[0],
                  signaturePtr->SignedInfo->SignatureMethod->Algorithm->Length);
      SigGenWsPtr->CanonicalizationAlgorithm = CanonicalizationAlgorithm;

      retValue = XmlSec_RetVal_OK;
    }
  }

#if (XMLSECURITY_DEV_ERROR_REPORT == STD_ON)
  if (errorID != XMLSECURITY_E_NO_ERROR)
  {
    (void)Det_ReportError(XMLSECURITY_MODULE_ID, XMLSECURITY_INSTANCE_ID_DET, XMLSECURITY_API_ID_INIT_SIG_GEN_WORKSPACE,
                                                                                                               errorID);
  }
#else
  XMLSECURITY_DUMMY_STATEMENT(errorID); /* PRQA S 3112, 2983 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* XMLSECURITY_DEV_ERROR_REPORT */

  return retValue;
} /* PRQA S 6060, 6010, 6080 */ /* MD_MSR_STPAR, MD_MSR_STPTH, MD_MSR_STMIF */

/***********************************************************************************************************************
 *  XmlSecurity_InitSigValWorkspace
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, XMLSECURITY_CODE) XmlSecurity_InitSigValWorkspace(
  P2VAR(XmlSecurity_SigValWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) SigValWsPtr,
  XmlSecurity_ExiURIDereferenceFctType ExiURIDereferenceFct,
  XmlSecurity_GetPublicKeyFctType GetPublicKeyFct)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retValue = E_NOT_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = XMLSECURITY_E_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
#if (XMLSECURITY_DEV_ERROR_DETECT == STD_ON)
  /* #10 Check plausibility of input parameters. */
  if (SigValWsPtr == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (ExiURIDereferenceFct == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (GetPublicKeyFct == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else
#endif /* XMLSECURITY_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Initialize the workspace variables */
    SigValWsPtr->ExiURIDereferenceFct = ExiURIDereferenceFct;
    SigValWsPtr->GetPublicKeyFct = GetPublicKeyFct;
    retValue = E_OK;
  }

#if (XMLSECURITY_DEV_ERROR_REPORT == STD_ON)
  if (errorID != XMLSECURITY_E_NO_ERROR)
  {
    (void)Det_ReportError(XMLSECURITY_MODULE_ID, XMLSECURITY_INSTANCE_ID_DET, XMLSECURITY_API_ID_INIT_SIG_VAL_WORKSPACE,
                                                                                                               errorID);
  }
#else
  XMLSECURITY_DUMMY_STATEMENT(errorID); /* PRQA S 3112, 2983 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* XMLSECURITY_DEV_ERROR_REPORT */

  return retValue;
} /* PRQA S 6060 */ /* MD_MSR_STPAR */

/***********************************************************************************************************************
 *  XmlSecurity_AddExiReference
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(XmlSecurity_ReturnType, XMLSECURITY_CODE) XmlSecurity_AddExiReference(
  P2VAR(XmlSecurity_SigGenWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) SigGenWsPtr,
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiStreamBufPtr,
  P2CONST(uint8, AUTOMATIC, XMLSECURITY_APPL_DATA) ReferenceStruct,
  P2CONST(uint8, AUTOMATIC, XMLSECURITY_APPL_DATA) ReferenceURI,
  Exi_RootElementIdType ReferenceElementId,
  uint16 ExiStreamBufLen,
  uint16 ReferenceURILength,
  Exi_NamespaceIdType Namespace,
  uint32 CsmSHA256JobId)
{
  /* ----- Local Variables ---------------------------------------------- */
  XmlSecurity_ReturnType retValue = XmlSec_RetVal_NotOK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = XMLSECURITY_E_NO_ERROR;
  P2VAR(Exi_XMLSIG_SignatureType, AUTOMATIC, XMLSECURITY_APPL_VAR) signaturePtr;
  P2VAR(Exi_XMLSIG_ReferenceType, AUTOMATIC, XMLSECURITY_APPL_VAR) referencePtr;

  XmlSecurity_ReturnType ret;

    /* ----- Development Error Checks ------------------------------------- */
#if (XMLSECURITY_DEV_ERROR_DETECT == STD_ON)
  if (SigGenWsPtr == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (SigGenWsPtr->StoragePtr == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (SigGenWsPtr->CanonicalizationAlgorithm == XmlSecurity_CAT_Invalid)
  {
    errorID = XMLSECURITY_E_PARAM_CANONINCALIZATION_ALGO;
  }
  else if (SigGenWsPtr->SignatureAlgorithm == XmlSecurity_SAT_Invalid)
  {
    errorID = XMLSECURITY_E_PARAM_SIGNATURE_ALGO;
  }
  else if (ReferenceStruct == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (ReferenceURI == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (ExiStreamBufPtr == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (ExiStreamBufLen == 0U)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER_SIZE;
  }
  else if (ReferenceURILength == 0U)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER_SIZE;
  }
  else
#endif /* XMLSECURITY_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* PRQA S 0310, 3305 1 */ /* MD_XmlSecurity_11.4, MD_XmlSecurity_3305 */
    signaturePtr = (P2VAR(Exi_XMLSIG_SignatureType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[0]);
    referencePtr = signaturePtr->SignedInfo->Reference;
    /* if this is not the first reference, digest method algorithm length will not equal 0 */
    /* in this case, follow the linked list and add a new reference element at it's end */
    /* ReferencePtr will point to the empty reference object */
    /* #10 Check if digest method length is not 0 and next reference element could be got was unsuccessful
     *       Return error */
    /* PRQA S 3415 2 */ /* MD_XmlSecurity_12.4 */
    if ((referencePtr->DigestMethod->Algorithm->Length != 0U) &&
             (XmlSecurity_GetNextReferenceElement(SigGenWsPtr, &referencePtr) != E_OK))
    {
      retValue = XmlSec_RetVal_TargetBufferTooSmall;
    }
    /* ESCAN00068932 */
    /* #20 Otherwise check if buffer does not have enough space to accomodate Uri
     *       Return error */
    else if (sizeof(referencePtr->URI->Buffer) < ( ReferenceURILength + 1UL ))
    {
      /* It is not possible to store the URI inside the Exi buffer struct. */
      retValue = XmlSec_RetVal_ExiBufferTooSmall;
    }
    /* if an empty reference element is available, ReferencePtr point to that empty element */
    /* #30 Otherwise do the following
     *       Check if creating of canonicalized hash is other than ok
     *         Set transformation error
     *       Otherwise set variables and set return as ok */
    else
    {
      /* add reference URI, '#' added because only same document references are supported */
      referencePtr->URI->Length = ReferenceURILength + 1U;
      referencePtr->URI->Buffer[0] = (uint8)'#';

      /* PRQA S 0315 1 */ /* MD_MSR_VStdLibCopy */
      IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, XMLSECURITY_APPL_VAR))&referencePtr->URI->Buffer[1],
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, XMLSECURITY_APPL_DATA))&ReferenceURI[0], ReferenceURILength);
      /* add transform and digest method algorithm attribute */
      /* Note: TransformsFlag already set during initialization */
      referencePtr->Transforms->Transform->Algorithm->Length = sizeof(XmlSecurity_ExiTransformUri) - 1U;
      /* PRQA S 0315 1 */ /* MD_MSR_VStdLibCopy */
      IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, XMLSECURITY_APPL_VAR))&referencePtr->Transforms->Transform->Algorithm->Buffer[0],
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, XMLSECURITY_APPL_DATA))&XmlSecurity_ExiTransformUri[0],
        referencePtr->Transforms->Transform->Algorithm->Length);

      /* execute transformation algorithm, only Exi supported so far */
      ret = XmlSecurity_CreateCanonicalizedHash(ReferenceStruct, ReferenceElementId, Namespace, &ExiStreamBufPtr[0],
                                                ExiStreamBufLen, &SigGenWsPtr->ExiEncWs,
                                                &referencePtr->DigestValue->Buffer[0], CsmSHA256JobId);
      if (ret != XmlSec_RetVal_OK)
      {
        retValue = XmlSec_RetVal_TransformationError;
      }
      else
      {
        /* digest generation finished successfully */
        referencePtr->DigestValue->Length = 32;
        /* set algorithm identifier */
        referencePtr->DigestMethod->Algorithm->Length = sizeof(XmlSecurity_Sha256Uri) - 1U;
        /* PRQA S 0315 1 */ /* MD_MSR_VStdLibCopy */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, XMLSECURITY_APPL_VAR))&referencePtr->DigestMethod->Algorithm->Buffer[0],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, XMLSECURITY_APPL_DATA))&XmlSecurity_Sha256Uri[0],
          referencePtr->DigestMethod->Algorithm->Length);
        retValue = XmlSec_RetVal_OK;
      }
    }
  }

#if (XMLSECURITY_DEV_ERROR_REPORT == STD_ON)
  if (errorID != XMLSECURITY_E_NO_ERROR)
  {
    (void)Det_ReportError(XMLSECURITY_MODULE_ID, XMLSECURITY_INSTANCE_ID_DET, XMLSECURITY_API_ID_ADD_EXI_REFERENCE,
                                                                                                               errorID);
  }
#else
  XMLSECURITY_DUMMY_STATEMENT(errorID); /* PRQA S 3112, 2983 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* XMLSECURITY_DEV_ERROR_REPORT */
  return retValue;
} /* PRQA S 6060, 6010, 6030, 6080 */ /* MD_MSR_STPAR, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STMIF */
/* PRQA L:NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */ /* MD_XmlSecurity_1.1 */

/***********************************************************************************************************************
 *  XmlSecurity_GenerateSignature
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(XmlSecurity_ReturnType, XMLSECURITY_CODE) XmlSecurity_GenerateSignature(
  P2VAR(XmlSecurity_SigGenWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) SigGenWsPtr,
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiStreamBufPtr,
  uint16 ExiStreamBufLen,
  uint32 CsmPrivateKeyId,
  uint32 CsmECDSASignJobId)
{
  /* ----- Local Variables ---------------------------------------------- */
  XmlSecurity_ReturnType retValue = XmlSec_RetVal_NotOK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = XMLSECURITY_E_NO_ERROR;
  P2VAR(Exi_XMLSIG_SignatureType, AUTOMATIC, XMLSECURITY_APPL_VAR) signaturePtr;

  /* ----- Development Error Checks ------------------------------------- */
#if (XMLSECURITY_DEV_ERROR_DETECT == STD_ON)
  if (SigGenWsPtr == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (SigGenWsPtr->StoragePtr == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (SigGenWsPtr->CanonicalizationAlgorithm != XmlSecurity_CAT_EXI)
  {
    errorID = XMLSECURITY_E_PARAM_CANONINCALIZATION_ALGO;
    retValue = XmlSec_RetVal_AlgorithmNotSupported;
  }
  else if (SigGenWsPtr->SignatureAlgorithm != XmlSecurity_SAT_ECDSA_SHA256)
  {
    errorID = XMLSECURITY_E_PARAM_SIGNATURE_ALGO;
    retValue = XmlSec_RetVal_AlgorithmNotSupported;
  }
  else if (ExiStreamBufPtr == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (ExiStreamBufLen == 0U)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER_SIZE;
  }
  else
#endif /* XMLSECURITY_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* PRQA S 0310, 3305 1 */ /* MD_XmlSecurity_11.4, MD_XmlSecurity_3305 */
    signaturePtr = (P2VAR(Exi_XMLSIG_SignatureType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[0]);

    /* check if there exists at least one digest value in the reference section of the signature */
    /* #10 Check if digest value length is 0
     *      Set error */
    if ( signaturePtr->SignedInfo->Reference->DigestValue->Length == 0U )
    {
      /* No digest value available.
       * There shall exist at least one reference with a digest value inside the <SignedInfo> element
       */
      retValue = XmlSec_RetVal_NoReferenceDetected;
    }
    /* #20 Otherwise check if key is invalid
     *       Set error */
    else if (Csm_KeySetValid(CsmPrivateKeyId) != E_OK )
    {
      retValue = XmlSec_RetVal_SignatureGenerationError;
    }
    /* #30 Otherwise check if the CSM job is initialized unsuccessful
     *      Set error */
    else if ( Csm_SignatureGenerate(CsmECDSASignJobId, CRYPTO_OPERATIONMODE_START, NULL_PTR, 0, NULL_PTR, NULL_PTR)
                != (Std_ReturnType)E_OK )
    {
      retValue = XmlSec_RetVal_SignatureGenerationError;
    }
    /* #40 Otherwise perform the signature generation */
    else
    {
      /* ----- Local Variables needed to generate the signature ----------- */
      uint32 signatureLength = sizeof(signaturePtr->SignatureValue->Buffer);
      uint32 CurrentStreamPosition = 0;
      IpBase_PbufType pBuf;

      /* convert the linear buffer to a PBuf */
      pBuf.payload = ExiStreamBufPtr;
      pBuf.totLen = ExiStreamBufLen;
      pBuf.len = ExiStreamBufLen;

      /* Try to generate Exi-encoded data.
       * Due to possible restrictions based on configured buffer size
       * partially data handling may be needed. Therefore, the largest
       * possible data chunk is transformed in each iteration. If buffer
       * is big enough only one iteration have to be performed.
       */
      /* #50 iterate over all Exi streaming data
       *      Get Exi streaming data
       *        Update CSM */
      do
      {
        /* Fetch exi streaming data. ExiEncWsPtr->EncWs.StreamComplete can be
         * used to determine if it is partial or complete data */
        /* PRQA S 0310 2 */ /* MD_XmlSecurity_11.4 */
        if ( XmlSecurity_GetExiStreamingData(
                (P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR))(signaturePtr->SignedInfo),
                &pBuf, &SigGenWsPtr->ExiEncWs, CurrentStreamPosition,
                EXI_XMLSIG_SIGNED_INFO_TYPE, (uint8)EXI_SCHEMA_SET_XMLSIG_TYPE
              ) != XmlSec_RetVal_OK )
        {
          retValue = XmlSec_RetVal_TransformationError;
        }
        /* Update the CSM with the new data */
        else if ( Csm_SignatureGenerate(CsmECDSASignJobId, CRYPTO_OPERATIONMODE_UPDATE,
                    &ExiStreamBufPtr[0], SigGenWsPtr->ExiEncWs.EncWs.CurrentStreamSegmentLen,
                    NULL_PTR, NULL_PTR) != (Std_ReturnType)E_OK )
        {
          retValue = XmlSec_RetVal_SignatureGenerationError;
        }
        else
        {
          retValue = XmlSec_RetVal_OK;
          /* update the current position in the exi stream */
          CurrentStreamPosition += SigGenWsPtr->ExiEncWs.EncWs.CurrentStreamSegmentLen;
        }
      } while ((SigGenWsPtr->ExiEncWs.EncWs.StreamComplete == FALSE) && (retValue == XmlSec_RetVal_OK));

      /* #60 if no error
       *         generate the signature */
      if ( retValue == XmlSec_RetVal_OK )
      {
        /* The complete Exi-encoded data was already added to the csm in the preceding loop.
         * Now the signature generation shall be finished.
         */
        if ( Csm_SignatureGenerate(CsmECDSASignJobId, CRYPTO_OPERATIONMODE_FINISH,
                &ExiStreamBufPtr[0], 0, &signaturePtr->SignatureValue->Buffer[0], &signatureLength
              ) != (Std_ReturnType)E_OK )
        {
          retValue = XmlSec_RetVal_SignatureGenerationError;
        }
        else
        {
          /* store length of signature value encoded data */
          signaturePtr->SignatureValue->Length = (uint16)signatureLength;
        }
      }
    }
  }

#if (XMLSECURITY_DEV_ERROR_REPORT == STD_ON)
  if (errorID != XMLSECURITY_E_NO_ERROR)
  {
    (void)Det_ReportError(XMLSECURITY_MODULE_ID, XMLSECURITY_INSTANCE_ID_DET, XMLSECURITY_API_ID_GENERATE_SIGNATURE,
                                                                                                               errorID);
  }
#else
  XMLSECURITY_DUMMY_STATEMENT(errorID); /* PRQA S 3112, 2983 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* XMLSECURITY_DEV_ERROR_REPORT */

  return retValue;
} /* PRQA S 6010, 6030, 6060, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STPAR, MD_MSR_STMIF */
  /* PRQA L:NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */ /* MD_XmlSecurity_1.1 */

/***********************************************************************************************************************
 *  XmlSecurity_ValidateExiSignature
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(XmlSecurity_ReturnType, XMLSECURITY_CODE) XmlSecurity_ValidateExiSignature(
  P2VAR(XmlSecurity_SigValWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) SigValWsPtr,
  P2CONST(Exi_XMLSIG_SignatureType, AUTOMATIC, XMLSECURITY_APPL_DATA) SignaturePtr,
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiStreamBufPtr,
  uint16 ExiStreamBufLen,
  uint32 CsmECDSAVerifyJobId,
  uint32 CsmPublicKeyId,
  uint32 CsmSHA256JobId)
{
  /* ----- Local Variables ---------------------------------------------- */
  XmlSecurity_ReturnType retValue = XmlSec_RetVal_NotOK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8 errorID = XMLSECURITY_E_NO_ERROR;
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) pubKeyPtr =
                                          (P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR))NULL_PTR;
  uint16 pubKeyLen = 0u;
  uint32 signatureLengthCsm = XMLSECURITY_P256R1_ECDSA_SIG_LEN;
  Crypto_VerifyResultType verifyResult;

  /* ----- Development Error Checks ------------------------------------- */
#if (XMLSECURITY_DEV_ERROR_DETECT == STD_ON)
  if (SigValWsPtr == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (SigValWsPtr->ExiURIDereferenceFct == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (SigValWsPtr->GetPublicKeyFct == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
    retValue = XmlSec_RetVal_NoPublicKey;
  }
  else if (SignaturePtr == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (ExiStreamBufPtr == NULL_PTR)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER;
  }
  else if (ExiStreamBufLen == 0U)
  {
    errorID = XMLSECURITY_E_PARAM_POINTER_SIZE;
  }
  else
#endif /* XMLSECURITY_DEV_ERROR_DETECT == STD_ON */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* start of core validation */
    /* core validation includes two main steps: reference validation and signature validation */

    /* start of refernce validation */
    /* refernce validation:
    - canonicalize the <SignedInfo> element (application should see what is signed)
    - for each <Reference> being validated, perform these steps:
    1. Dereference the URI and get the data stream to be digested using the specified transforms
    2. Generate the hash value using the hash function specified in the <DigestMethod> element
    3. Compare the computed digest value against the <DigestValue> element content. If these values do not match,
    reference validation fails.
    */
    /* #10  Check if canonicalization algorithm length is not as expected
     *        Set error */
    if (SignaturePtr->SignedInfo->CanonicalizationMethod->Algorithm->Length !=
        (uint16)(sizeof(XmlSecurity_ExiTransformUri) - 1U ))
    {
      retValue = XmlSec_RetVal_AlgorithmNotSupported;
    }
    /* #20  Otherwise check if canonicalization algorithm buffer and Exi transform Uri are not equal
     *        Set error */
    else if(IpBase_StrCmpLen(&SignaturePtr->SignedInfo->CanonicalizationMethod->Algorithm->Buffer[0u],
                             &XmlSecurity_ExiTransformUri[0u],
                             SignaturePtr->SignedInfo->CanonicalizationMethod->Algorithm->Length) != IPBASE_CMP_EQUAL)
    {
      retValue = XmlSec_RetVal_AlgorithmNotSupported;
    }
    /* validate all references included in this <SignedInfo> element,
    * this will only return E_OK if all digest values match
    */
    /* #30  Otherwise check if Exi references are not valid
     *        Set error */
    else if (XmlSecurity_ValidateExiReferences(SigValWsPtr, SignaturePtr->SignedInfo,
                &ExiStreamBufPtr[0], ExiStreamBufLen, CsmSHA256JobId) != XmlSec_RetVal_OK)
    {
      retValue = XmlSec_RetVal_InvalidReference;
    }
    /* start of signature validation
    * signature validation:
    *  1. Retrieve the verification key from a <KeyInfo> element or application-specific key source
    *  2. Determin the signature algorithm being used and validate the signature over the canonical form
    *     of the <SignedInfo> element against the <SignatureValue> element.
    */

    /* buffer is still locked, do not forget to unlock the buffer! */
    /* the buffer was locked during reference validation and holds the canonicalized value of signed info,
     * size stored in BufLen
     */
    /* #40  Otherwise check if signature method algorithm length is not as expected
     *        Set error */
    else if (SignaturePtr->SignedInfo->SignatureMethod->Algorithm->Length != (sizeof(XmlSecurity_EcdsaSha256Uri) - 1u))
    {
      retValue = XmlSec_RetVal_AlgorithmNotSupported;
    }
    /* #50  Otherwise check if signature method algorithm buffer and sha256 Uri are not equal
     *        Set error */
    else if(IpBase_StrCmpLen(&SignaturePtr->SignedInfo->SignatureMethod->Algorithm->Buffer[0u],
                             (P2CONST(uint8, AUTOMATIC, XMLSECURITY_APPL_DATA))&XmlSecurity_EcdsaSha256Uri[0u],
                             SignaturePtr->SignedInfo->SignatureMethod->Algorithm->Length) != IPBASE_CMP_EQUAL)
    {
      retValue = XmlSec_RetVal_AlgorithmNotSupported;
    }
    /* this is a ECDSA signature with SHA26 */
    /* get public key */
    /* #60  Otherwise check if public key could not be got
     *        Set error */
    else if (SigValWsPtr->GetPublicKeyFct(&pubKeyPtr, &pubKeyLen, XmlSecurity_SAT_ECDSA_SHA256) != E_OK)
    {
      retValue = XmlSec_RetVal_NoPublicKey;
    }
    /* #70  Otherwise check if public key pointer is null
     *        Set error */
    else if (pubKeyPtr == NULL_PTR)
    {
      retValue = XmlSec_RetVal_NoPublicKey;
    }
    /* #80  Otherwise check if public key length is 0
     *        Set error */
    else if (pubKeyLen == 0u)
    {
      retValue = XmlSec_RetVal_NoPublicKey;
    }
    /* #90 Otherwise check if public key length is not as expected
     *        Set error */
    else if (pubKeyLen != (XMLSECURITY_P256R1_PUBLIC_KEY_LEN + 1u))
    {
      retValue = XmlSec_RetVal_NoPublicKey;
    }
    /* pubKeyPtr[0] == 0x04 --> uncompressed point */
    /* #100 Otherwise check if public key pointer contains no uncompressed point
     *        Set error */
    else if (pubKeyPtr[0] != (uint8)0x04)
    {
      retValue = XmlSec_RetVal_NoPublicKey;
    }
    /* #110 Otherwise check if key element set is not ok
     *        Set error */
    else if (Csm_KeyElementSet(CsmPublicKeyId, CRYPTO_KE_CERTIFICATE_SUBJECT_PUBLIC_KEY, &pubKeyPtr[1],
              (pubKeyLen - 1uL)) != E_OK)
    {
      retValue = XmlSec_RetVal_NoPublicKey;
    }
    /* #120 Otherwise check if key set is invalid
     *        Set error */
    else if ( Csm_KeySetValid(CsmPublicKeyId) != E_OK )
    {
      retValue = XmlSec_RetVal_NoPublicKey;
    }
    /* #130 Otherwise check if the CSM job is initialized unsuccessful
    *      Set error */
    else if ( Csm_SignatureVerify(CsmECDSAVerifyJobId, CRYPTO_OPERATIONMODE_START, NULL_PTR, 0, NULL_PTR, 0, NULL_PTR)
                != (Std_ReturnType)E_OK )
    {
      retValue = XmlSec_RetVal_SignatureValidationError;
    }
    /* execute canonicalization algorithm, only Exi supported so far */
    /* #140 Otherwise perform the signature verification */
    else
    {
      /* ----- Local Variables needed to validate the signature ----------- */
      uint32 CurrentStreamPosition = 0;
      IpBase_PbufType pBuf;

      /* convert the linear buffer to a PBuf */
      pBuf.payload = ExiStreamBufPtr;
      pBuf.totLen = ExiStreamBufLen;
      pBuf.len = ExiStreamBufLen;

      /* Try to generate Exi-encoded data.
       * Due to possible restrictions based on configured buffer size
       * partially data handling may be needed. Therefore, the largest
       * possible data chunk is transformed in each iteration. If buffer
       * is big enough only one iteration have to be performed.
       */
      /* #150 iterate over all Exi streaming data
       *      Get Exi streaming data
       *        Update CSM */
      do
      {
        /* Fetch exi streaming data. ExiEncWsPtr->EncWs.StreamComplete can be
         * used to determine if it is partial or complete data */
        /* PRQA S 0310 1 */ /* MD_XmlSecurity_11.4 */
        if ( XmlSecurity_GetExiStreamingData((P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR))(SignaturePtr->SignedInfo),
                &pBuf, &SigValWsPtr->ExiEncWs, CurrentStreamPosition,
                EXI_XMLSIG_SIGNED_INFO_TYPE, (uint8)EXI_SCHEMA_SET_XMLSIG_TYPE
             ) != XmlSec_RetVal_OK )
        {
          retValue = XmlSec_RetVal_TransformationError;
        }
        /* Update the csm with the new data */
        else if ( Csm_SignatureVerify(CsmECDSAVerifyJobId, CRYPTO_OPERATIONMODE_UPDATE,
                    &ExiStreamBufPtr[0], SigValWsPtr->ExiEncWs.EncWs.CurrentStreamSegmentLen,
                    NULL_PTR, 0, NULL_PTR ) != (Std_ReturnType)E_OK )
        {
          retValue = XmlSec_RetVal_SignatureValidationError;
        }
        else
        {
          retValue = XmlSec_RetVal_OK;
          /* update the current position in the exi stream */
          CurrentStreamPosition += SigValWsPtr->ExiEncWs.EncWs.CurrentStreamSegmentLen;
        }
      } while ((SigValWsPtr->ExiEncWs.EncWs.StreamComplete == FALSE ) && (retValue == XmlSec_RetVal_OK));

      /* #160 if no error
      *          verify signature
      */
      if (retValue == XmlSec_RetVal_OK)
      {
        /* The complete Exi-encoded data was already added to the csm in the preceding loop.
        * Now the signature verification shall be finished.
        */
        if ( Csm_SignatureVerify(CsmECDSAVerifyJobId, CRYPTO_OPERATIONMODE_FINISH,
                &ExiStreamBufPtr[0], 0, &SignaturePtr->SignatureValue->Buffer[0], signatureLengthCsm, &verifyResult
              ) != (Std_ReturnType)E_OK )

        {
          retValue = XmlSec_RetVal_SignatureValidationError;
        }
        else if ( verifyResult == CRYPTO_E_VER_NOT_OK )
        {
          retValue = XmlSec_RetVal_SignatureValidationError;
        }
        else
        {
          retValue = XmlSec_RetVal_OK;
        }
      }
    }
  }

#if (XMLSECURITY_DEV_ERROR_REPORT == STD_ON)
  if (errorID != XMLSECURITY_E_NO_ERROR)
  {
    (void)Det_ReportError(XMLSECURITY_MODULE_ID, XMLSECURITY_INSTANCE_ID_DET, XMLSECURITY_API_ID_VALIDATE_SIGNATURE,
                                                                                                               errorID);
  }
#else
  XMLSECURITY_DUMMY_STATEMENT(errorID); /* PRQA S 3112, 2983 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* XMLSECURITY_DEV_ERROR_REPORT */

  return retValue;
} /* PRQA S 6010, 6030, 6080, 6050, 6060 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STMIF, MD_MSR_STCAL, MD_MSR_STPAR */
/* PRQA L:NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */ /* MD_MSR_1.1_715 */

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 **********************************************************************************************************************/


/**********************************************************************************************************************
 *  XmlSecurity_CreateCanonicalizedHash
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
XMLSECURITY_LOCAL FUNC(XmlSecurity_ReturnType, XMLSECURITY_CODE) XmlSecurity_CreateCanonicalizedHash(
  P2CONST(uint8, AUTOMATIC, XMLSECURITY_APPL_DATA) DataStructPtr,
  Exi_RootElementIdType DataElementId, Exi_NamespaceIdType Namespace,
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiStreamBufPtr, uint16 ExiStreamBufLen,
  P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiEncWsPtr,
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) SHA256BufPtr,
  uint32 CsmSHA256JobId)
{
  /* ----- Local Variables ---------------------------------------------- */
  XmlSecurity_ReturnType   xmlSecRetVal;
  uint32                   xmlSecurity_resultLengthPtr = 32;
  uint32 CurrentStreamPosition = 0;
  IpBase_PbufType pBuf;

  /* ----- Implementation ----------------------------------------------- */

  /* convert the linear buffer to a PBuf */
  pBuf.payload = ExiStreamBufPtr;
  pBuf.totLen = ExiStreamBufLen;
  pBuf.len = ExiStreamBufLen;
  /* #10 Check if CSM job initialization is unsuccessful
   *      Set error */
  if ( Csm_Hash(CsmSHA256JobId, CRYPTO_OPERATIONMODE_START, NULL_PTR, 0, NULL_PTR, NULL_PTR) != (Std_ReturnType)E_OK )
  {
    xmlSecRetVal = XmlSec_RetVal_NotOK;
  }
  /* #20 Otherwise generate the canonicalized hash */
  else
  {
    /* #30 iterate over all Exi streaming data
    *      Get Exi streaming data
    *        Update CSM */
    do
    {
      /* Fetch exi streaming data. ExiEncWsPtr->EncWs.StreamComplete can be
       * used to determine if it is partial or complete data */
      if (  XmlSecurity_GetExiStreamingData(DataStructPtr, &pBuf, ExiEncWsPtr,
              CurrentStreamPosition, DataElementId, Namespace) != XmlSec_RetVal_OK )
      {
        xmlSecRetVal = XmlSec_RetVal_TransformationError;
      }
      /* Update CSM with new exi data */
      else if ( Csm_Hash(CsmSHA256JobId, CRYPTO_OPERATIONMODE_UPDATE,
                  ExiStreamBufPtr, ExiEncWsPtr->EncWs.CurrentStreamSegmentLen,
                  NULL_PTR, NULL_PTR) != (Std_ReturnType)E_OK )
      {
        xmlSecRetVal = XmlSec_RetVal_DigestGenerationError;
      }
      else
      {
        xmlSecRetVal = XmlSec_RetVal_OK;
        /* update the current position in the exi stream */
        CurrentStreamPosition += ExiEncWsPtr->EncWs.CurrentStreamSegmentLen;
      }
    } while ((ExiEncWsPtr->EncWs.StreamComplete == FALSE) && (xmlSecRetVal == XmlSec_RetVal_OK));

    /* #40 If no error
     *      Finish CSM job and generate hash */
    if (xmlSecRetVal == XmlSec_RetVal_OK)
    {
      /* Data has been added during the preceding loop
       * now the hash calculation should be finished
       */
      if ( Csm_Hash(CsmSHA256JobId, CRYPTO_OPERATIONMODE_FINISH,
              ExiStreamBufPtr, 0, SHA256BufPtr, &xmlSecurity_resultLengthPtr
           ) != (Std_ReturnType)E_OK )
      {
        xmlSecRetVal = XmlSec_RetVal_DigestGenerationError;
      }
    }
  }

  return xmlSecRetVal;
} /* PRQA S 6060, 6010, 6030, 6080 */ /* MD_MSR_STPAR, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STMIF */

/**********************************************************************************************************************
 *  XmlSecurity_GetExiStreamingData
 **********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 */
XMLSECURITY_LOCAL FUNC(XmlSecurity_ReturnType, XMLSECURITY_CODE) XmlSecurity_GetExiStreamingData(
  P2CONST(uint8, AUTOMATIC, XMLSECURITY_APPL_DATA) DataStructPtr,
  P2VAR(IpBase_PbufType, AUTOMATIC, XMLSECURITY_APPL_VAR) TempBuffer,
  P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) ExiEncWsPtr,
  uint32 CurrentStreamPosition,
  Exi_RootElementIdType DataElementId,
  Exi_NamespaceIdType Namespace
  )
{
  /* ----- Local Variables ---------------------------------------------- */
  XmlSecurity_ReturnType xmlSecRetVal = XmlSec_RetVal_NotOK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize the EXI encoding workspace */
  /* Initialization has to be performed based on the current stream position.
   * This have to be done in every iteration (for each transformation) based on
   * the information provided by the Exi TechRef.
   */
  if ( Exi_InitEncodeWorkspace(ExiEncWsPtr, DataStructPtr, TempBuffer, 0, CurrentStreamPosition, FALSE)
          != EXI_E_OK )
  {
    xmlSecRetVal = XmlSec_RetVal_TransformationError;
  }
  else
  {
    /* #20 Perform Exi encoding */
    Exi_ReturnType ExiRetVal;
    /* set the input data type and start encoding */
    ExiEncWsPtr->InputData.RootElementId = DataElementId;
    ExiRetVal = Exi_EncodeFragment(ExiEncWsPtr, Namespace);
    /* EXI_E_EOS may occur if the provided buffer is too small.
    * Multiple iterations have to be perfomed to solve this problem.
    */
    if ( (ExiRetVal != EXI_E_OK) && (ExiRetVal != EXI_E_EOS) )
    {
      xmlSecRetVal = XmlSec_RetVal_TransformationError;
    }
    /* #30 Finalize the current Exi stream element */
    else if ( Exi_FinalizeExiStream(ExiEncWsPtr) != EXI_E_OK )
    {
      xmlSecRetVal = XmlSec_RetVal_TransformationError;
    }
    else
    {
      xmlSecRetVal = XmlSec_RetVal_OK;
    }
  }

  return xmlSecRetVal;
}/* PRQA S 6060 */ /* MD_MSR_STPAR */


/**********************************************************************************************************************
 *  XmlSecurity_ValidateExiReferences
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
XMLSECURITY_LOCAL FUNC(XmlSecurity_ReturnType, XMLSECURITY_CODE) XmlSecurity_ValidateExiReferences(
  P2VAR(XmlSecurity_SigValWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) SigValWsPtr,
  P2CONST(Exi_XMLSIG_SignedInfoType, AUTOMATIC, XMLSECURITY_APPL_DATA) SignedInfoPtr,
  P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) BufPtr,
  uint16 BufLen,
  uint32 CsmSHA256JobId)
{
  /* ----- Local Variables ---------------------------------------------- */
  XmlSecurity_ReturnType retValue = XmlSec_RetVal_InvalidReference;
  P2VAR(Exi_XMLSIG_ReferenceType, AUTOMATIC, XMLSECURITY_APPL_VAR) refPtr = SignedInfoPtr->Reference;
  uint8_least referenceNum;

  /* ----- Implementation ----------------------------------------------- */
  /* loop all references */
  /* #10 Iterate for number of references */
  for (referenceNum = 0U; referenceNum < EXI_MAXOCCURS_XMLSIG_REFERENCE; referenceNum++)
  {
    /* #20 Check if reference pointer is null
     *       Break the loop */
    if (refPtr == NULL_PTR)
    {
      break;
    }
    /* set return value to invalid, set to valid if this reference is valid */
    retValue = XmlSec_RetVal_InvalidReference;
    /* only references using the URI attribute are supported */
    /* #30 Check if Uri flag is true and ExiURIDereferenceFct is not null
     *       Call user and check if Uri could be resolved
     *         Check if transformation flag is set and variables are as expected
     *           Check if creation of canonicalized hash was successful
     *             Check if digest value length is as expected and sha256 and digest value buffer are equal
     *               Set return value to ok
     *             Otherwise set return value to invalid reference */
    if((refPtr->URIFlag == 1U) && (SigValWsPtr->ExiURIDereferenceFct != NULL_PTR))
    {
      P2VAR(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR) exiStructPtr;
      Exi_RootElementIdType exiRootElementId;
      Exi_NamespaceIdType exiNamespaceId;
      /* call user to resolve the URI */
      if(SigValWsPtr->ExiURIDereferenceFct(&exiStructPtr, &exiRootElementId, &exiNamespaceId,
                                           &refPtr->URI->Buffer[0], refPtr->URI->Length) == E_OK)
      {
        /* URI found and corresponding EXI struct available at exiStructPtr */
        /* next steps are canonicalize and hash the reference,
         * then compare this computed value to the reference digest value
         */
        /* PRQA S 3415 6 */ /* MD_XmlSecurity_12.4 */
        if (   ( 1uL == refPtr->TransformsFlag ) /* transformation required */
            && ( (sizeof(XmlSecurity_ExiTransformUri) - 1uL) == refPtr->Transforms->Transform->Algorithm->Length ) /* only Exi transformation supported */
            && ( IPBASE_CMP_EQUAL == IpBase_StrCmpLen(&refPtr->Transforms->Transform->Algorithm->Buffer[0], &XmlSecurity_ExiTransformUri[0], refPtr->Transforms->Transform->Algorithm->Length) )
            && ( NULL_PTR == refPtr->Transforms->Transform->NextTransformPtr) /* only 1 transformation supported */
            && ( (sizeof(XmlSecurity_Sha256Uri) - 1uL) == refPtr->DigestMethod->Algorithm->Length) /* only SHA256 hash supported */
            && ( IPBASE_CMP_EQUAL == IpBase_StrCmpLen(&refPtr->DigestMethod->Algorithm->Buffer[0], &XmlSecurity_Sha256Uri[0], refPtr->DigestMethod->Algorithm->Length) ) )
        {
          /* only valid combination is EXI and SHA256 for Transform and Digest algorithms */
          retValue = XmlSecurity_CreateCanonicalizedHash(exiStructPtr, exiRootElementId, exiNamespaceId,
            BufPtr, BufLen, &SigValWsPtr->ExiEncWs, &SigValWsPtr->SHA256Buf[0], CsmSHA256JobId);
          if (retValue == XmlSec_RetVal_OK)
          {
            /* digest generation finished successfully */
            /* because we are using EXI as transform algorithm,
             * value in struct is not base64 encoded (EXI represents base64 as binary data)
             *  -> no base64 decoding required
             */
            /* PRQA S 3415 3 */ /* MD_XmlSecurity_12.4 */
            if ((refPtr->DigestValue->Length == 32U) &&
                (IpBase_StrCmpLen(&SigValWsPtr->SHA256Buf[0],
                                  (P2CONST(uint8, AUTOMATIC, XMLSECURITY_APPL_VAR))(&refPtr->DigestValue->Buffer[0]),
                                  32) == IPBASE_CMP_EQUAL)) /* PRQA S 3415 */ /* MD_XmlSecurity_12.4 */
            {
              /* digest values are equal -> this reference is valid */
              retValue = XmlSec_RetVal_OK;
            }
            else
            {
              /* digest values are not equal -> this reference is invalid */
              retValue = XmlSec_RetVal_InvalidReference;
            }
          }
        }
      }
    }
    /* only if this reference is valid the retValue will be XmlSec_RetVal_OK */
    /* #40 Check if return value is not ok
     *       Break the loop */
    if(retValue != XmlSec_RetVal_OK)
    {
      /* all references need to be valid, this one is invalid -> signature is invalid */
      break; /* PRQA S 0771 */ /* MD_XmlSecurity_14.6 */
    }
    /* no error detected so far -> go to next reference */
    /* #50 Assign the reference pointer to next reference pointer */
    refPtr = refPtr->NextReferencePtr;
  }

  return retValue;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */

/**********************************************************************************************************************
 *  XmlSecurity_GetNextReferenceElement
 **********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
XMLSECURITY_LOCAL FUNC(Std_ReturnType, XMLSECURITY_CODE) XmlSecurity_GetNextReferenceElement(
  P2VAR(XmlSecurity_SigGenWorkspaceType, AUTOMATIC, XMLSECURITY_APPL_VAR) SigGenWsPtr,
  P2VAR(Exi_XMLSIG_ReferenceType*, AUTOMATIC, XMLSECURITY_APPL_VAR) CurrentReferenceElement)
{
  Std_ReturnType retValue = E_NOT_OK;
  uint8_least referenceNum;
  P2VAR(Exi_XMLSIG_ReferenceType, AUTOMATIC, XMLSECURITY_APPL_VAR) referencePtr = *CurrentReferenceElement;

  /* store size of all required struct elements */
  CONST(uint16, AUTOMATIC) minimumStorageRequired = sizeof(Exi_XMLSIG_ReferenceType) +
                                                      sizeof(Exi_XMLSIG_AttributeURIType) +
                                                      sizeof(Exi_XMLSIG_TransformsType) +
                                                      sizeof(Exi_XMLSIG_TransformType) +
                                                      sizeof(Exi_XMLSIG_AttributeAlgorithmType) +
                                                      sizeof(Exi_XMLSIG_DigestMethodType) +
                                                      sizeof(Exi_XMLSIG_AttributeAlgorithmType) +
                                                      sizeof(Exi_XMLSIG_DigestValueType);

  /* check if there is enough storage available inside the <SignedInfo> element to add one more reference struct */
  /* #10 Check if there is enough storage available inside the <SignedInfo> element to add one more reference struct */
  if ((SigGenWsPtr->StorageLen - SigGenWsPtr->StorageOffset) >= minimumStorageRequired)
  {
    /* References are stored in a linked list, search last list element */
    /* #20 Iterate for number of references
     *       Check if the next reference pointer is null
     *         Break the loop
     *       Assign the referencec pointer to next reference pointer */
    for (referenceNum = 0U; referenceNum < (EXI_MAXOCCURS_XMLSIG_REFERENCE - 1U); referenceNum++ )
    {
      if ( referencePtr->NextReferencePtr == NULL_PTR )
      {
        break;
      }
      referencePtr = referencePtr->NextReferencePtr;
    }

    /*
       Ceck if there will be more references added than configured in the EXI Module:
         'EXI_MAXOCCURS_XMLSIG_REFERENCE-1' because this function is not called for the first reference.
         '<' because the referneceNum variable is null based.
    */
    /* #30 Check if reference number is less than maximum
     *       Set variables to required values and return value to ok */
    if (referenceNum < (EXI_MAXOCCURS_XMLSIG_REFERENCE - 1U))
    {
      /* add new element to the list and clear memory */
      /* PRQA S 0310, 3305 29 */ /* MD_XmlSecurity_11.4, MD_XmlSecurity_3305 */
      referencePtr->NextReferencePtr =
                                      (Exi_XMLSIG_ReferenceType*)(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      referencePtr = referencePtr->NextReferencePtr;
      IpBase_Fill((P2VAR(IpBase_CopyDataType, AUTOMATIC, XMLSECURITY_APPL_VAR))referencePtr, 0x00, minimumStorageRequired); /* PRQA S 0315 */ /* MD_MSR_VStdLibCopy */
      /* create all required data elements and add pointers to the struct */
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_ReferenceType);
      referencePtr->URI =
        (P2VAR(Exi_XMLSIG_AttributeURIType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_AttributeURIType);
      referencePtr->URIFlag = 1;
      referencePtr->Transforms =
          (P2VAR(Exi_XMLSIG_TransformsType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_TransformsType);
      referencePtr->TransformsFlag = 1;
      referencePtr->Transforms->Transform =
          (P2VAR(Exi_XMLSIG_TransformType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_TransformType);
      referencePtr->Transforms->Transform->Algorithm =
          (P2VAR(Exi_XMLSIG_AttributeAlgorithmType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_AttributeAlgorithmType);
      referencePtr->DigestMethod =
          (P2VAR(Exi_XMLSIG_DigestMethodType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_DigestMethodType);
      referencePtr->DigestMethod->Algorithm =
          (P2VAR(Exi_XMLSIG_AttributeAlgorithmType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_AttributeAlgorithmType);
      referencePtr->DigestValue =
          (P2VAR(Exi_XMLSIG_DigestValueType, AUTOMATIC, XMLSECURITY_APPL_VAR))(&SigGenWsPtr->StoragePtr[SigGenWsPtr->StorageOffset]);
      SigGenWsPtr->StorageOffset += (uint16)sizeof(Exi_XMLSIG_DigestValueType);

      /* Update current reference element */
      *CurrentReferenceElement = referencePtr;
      retValue = E_OK;
    }
  }

  return retValue;
}

#define XMLSECURITY_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* module specific MISRA deviations:
  MD_XmlSecurity_11.4:
      Reason:     EXI data is stored in the EXI workspace storage. Structures are placed inside this storage container and linked
                  using pointers to sub-structures. Casting to the required structure type is required here.
      Risk:       In all cases it has to be ensured, that the storage size is checked first and when placing structures inside
                  the EXI storage area the memory has to be cleared completely.
      Prevention: Covered by code review.

  MD_XmlSecurity_12.4:
     Reason:     Subsequent function calls and comparisons are not needed any more if a preceding comparison failed.
                 (rule 3415)
     Risk:       none
     Prevention: Covered by code review.

  MD_XmlSecurity_14.6:
      Reason:     break for loop if Reference Pointer is a null pointer and if an error occurs.
      Risk:       No risk.
      Prevention: Covered by code review.

  MD_XmlSecurity_3305:
      Reason:     StoragePtr points to a generic data buffer to store the signature data.
      Risk:       There is no risk.
      Prevention: Covered by code review.

  MD_XmlSecurity_3325:
      Reason:     The Det check use.config parameters. Therefore the expression is not constant
      Risk:       There is no risk.
      Prevention: Covered by code review.

  MD_XmlSecurity_3332:
    Reason:     It is checked if the macro is defined.
    Risk:       None.
    Prevention: Already correctly handled, but not detected by PRQA.
*/

/* START_COVERAGE_JUSTIFICATION

\ID COV_XMLSECURITY_COMPATIBILITY
\ACCEPT   TX
\ACCEPT   XF
\REASON   [COV_MSR_COMPATIBILITY]

END_COVERAGE_JUSTIFICATION */

/**********************************************************************************************************************
 *  END OF FILE: XmlSecurity.c
 *********************************************************************************************************************/
