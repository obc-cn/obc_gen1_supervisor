/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 * 
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Exi_SchemaTypes.h
 *        \brief  Efficient XML Interchange general types header file
 *
 *      \details  Vector static code header file for the Efficient XML Interchange sub-component general types.
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file Exi.h.
 *********************************************************************************************************************/
/* PRQA S 0857 EOF */ /* MD_Exi_1.1_0857 */ /* [L] Number of macro definitions exceeds 1024 - program is non-conforming. */

#if !defined (EXI_SCHEMA_TYPES_H) /* PRQA S 0883 */ /* MD_Exi_19.15_0883 */
# define EXI_SCHEMA_TYPES_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 EXI_SCHEMA_TYPES_H_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "Exi.h"
#include "Exi_Types.h"
/* PRQA L:EXI_SCHEMA_TYPES_H_IF_NESTING */ /* MD_MSR_1.1_828 */

#include "Exi_SchemaGeneralTypes.h"
#include "Exi_SAP_SchemaTypes.h"
#include "Exi_XMLSIG_SchemaTypes.h"
#include "Exi_DIN_SchemaTypes.h"
#include "Exi_ISO_SchemaTypes.h"
#include "Exi_ISO_ED2_DIS_SchemaTypes.h"


#endif
  /* EXI_SCHEMA_TYPES_H */
