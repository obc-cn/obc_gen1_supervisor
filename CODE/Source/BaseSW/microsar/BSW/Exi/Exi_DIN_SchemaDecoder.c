/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Exi_DIN_SchemaDecoder.c
 *        \brief  Efficient XML Interchange DIN decoder source file
 *
 *      \details  Vector static code implementation for the Efficient XML Interchange sub-component DIN decoder.
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file Exi.h.
 *********************************************************************************************************************/
/* PRQA S 0857 EOF */ /* MD_Exi_1.1_0857 */ /* [L] Number of macro definitions exceeds 1024 - program is non-conforming. */

#define EXI_DIN_SCHEMA_DECODER_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 EXI_DIN_SCHEMA_DECODER_C_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "Exi_DIN_SchemaDecoder.h"
#include "Exi_BSDecoder.h"
#include "Exi_Priv.h"
/* PRQA L:EXI_DIN_SCHEMA_DECODER_C_IF_NESTING */ /* MD_MSR_1.1_828 */
/**********************************************************************************************************************
*  VERSION CHECK
*********************************************************************************************************************/
#if ( (EXI_SW_MAJOR_VERSION != 6u) || (EXI_SW_MINOR_VERSION != 0u) || (EXI_SW_PATCH_VERSION != 1u) )
  #error "Vendor specific version numbers of Exi.h and Exi_DIN_SchemaDecoder.c are inconsistent"
#endif

#if (!defined (EXI_ENABLE_DECODE_DIN_MESSAGE_SET))
# if (defined (EXI_ENABLE_DIN_MESSAGE_SET))
#  define EXI_ENABLE_DECODE_DIN_MESSAGE_SET   EXI_ENABLE_DIN_MESSAGE_SET
# else
#  define EXI_ENABLE_DECODE_DIN_MESSAGE_SET   STD_OFF
# endif
#endif

#if (defined(EXI_ENABLE_DECODE_DIN_MESSAGE_SET) && (EXI_ENABLE_DECODE_DIN_MESSAGE_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */

/* PRQA S 0715 NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */ /* MD_Exi_1.1 */


#define EXI_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/**********************************************************************************************************************
 *  FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Exi_Decode_DIN_AC_EVChargeParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_AC_EVCHARGE_PARAMETER) && (EXI_DECODE_DIN_AC_EVCHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_AC_EVChargeParameter( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_AC_EVChargeParameterType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_AC_EVChargeParameterType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_AC_EVChargeParameterType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_AC_EVChargeParameterType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_AC_EVCHARGE_PARAMETER_TYPE);
    /* #40 Decode element DepartureTime */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DepartureTime) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->DepartureTime);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DepartureTime) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element EAmount */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EAmount) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EAmount));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_AC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EAmount) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVMaxVoltage */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVMaxVoltage) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVMaxVoltage));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_AC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVMaxVoltage) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVMaxCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVMaxCurrent) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVMaxCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_AC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVMaxCurrent) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode element EVMinCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVMinCurrent) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVMinCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_AC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVMinCurrent) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 If AC_EVChargeParameter was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(AC_EVChargeParameter) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_AC_EVCHARGE_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_AC_EVCHARGE_PARAMETER) && (EXI_DECODE_DIN_AC_EVCHARGE_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_AC_EVSEChargeParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_AC_EVSECHARGE_PARAMETER) && (EXI_DECODE_DIN_AC_EVSECHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_AC_EVSEChargeParameter( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_AC_EVSEChargeParameterType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_AC_EVSEChargeParameterType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_AC_EVSEChargeParameterType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_AC_EVSEChargeParameterType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_AC_EVSECHARGE_PARAMETER_TYPE);
    /* #40 Decode element AC_EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(AC_EVSEStatus) */
    {
      #if (defined(EXI_DECODE_DIN_AC_EVSESTATUS) && (EXI_DECODE_DIN_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_AC_EVSEStatus(DecWsPtr, &(structPtr->AC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_AC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_AC_EVSESTATUS) && (EXI_DECODE_DIN_AC_EVSESTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(AC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element EVSEMaxVoltage */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMaxVoltage) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaxVoltage));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_AC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEMaxVoltage) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVSEMaxCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMaxCurrent) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaxCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_AC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEMaxCurrent) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVSEMinCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMinCurrent) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMinCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_AC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEMinCurrent) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 If AC_EVSEChargeParameter was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(AC_EVSEChargeParameter) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_AC_EVSECHARGE_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_AC_EVSECHARGE_PARAMETER) && (EXI_DECODE_DIN_AC_EVSECHARGE_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_AC_EVSEStatus
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_AC_EVSESTATUS) && (EXI_DECODE_DIN_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_AC_EVSEStatus( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_AC_EVSEStatusType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_AC_EVSEStatusType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_AC_EVSEStatusType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_AC_EVSEStatusType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_AC_EVSESTATUS_TYPE);
    /* #40 Decode element PowerSwitchClosed */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(PowerSwitchClosed) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->PowerSwitchClosed);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(PowerSwitchClosed) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element RCD */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(RCD) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->RCD);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(RCD) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element NotificationMaxDelay */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(NotificationMaxDelay) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->NotificationMaxDelay);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(NotificationMaxDelay) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVSENotification */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSENotification) */
    {
      #if (defined(EXI_DECODE_DIN_EVSENOTIFICATION) && (EXI_DECODE_DIN_EVSENOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #80 Decode EVSENotification as enumeration value */
      Exi_Decode_DIN_EVSENotification(DecWsPtr, &structPtr->EVSENotification);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_AC_EVSESTATUS, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_EVSENOTIFICATION) && (EXI_DECODE_DIN_EVSENOTIFICATION == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSENotification) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 If AC_EVSEStatus was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(AC_EVSEStatus) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_AC_EVSESTATUS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_AC_EVSESTATUS) && (EXI_DECODE_DIN_AC_EVSESTATUS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_AttributeId
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_AttributeId( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_AttributeIdType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_AttributeIdType, AUTOMATIC, EXI_APPL_VAR) structPtr;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_AttributeIdType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_AttributeIdType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode AttributeId as string value */
    structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
    Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_ATTRIBUTE_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_AttributeName
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_ATTRIBUTE_NAME) && (EXI_DECODE_DIN_ATTRIBUTE_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_AttributeName( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_AttributeNameType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_AttributeNameType, AUTOMATIC, EXI_APPL_VAR) structPtr;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_AttributeNameType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_AttributeNameType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode AttributeName as string value */
    structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
    Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_ATTRIBUTE_NAME, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_ATTRIBUTE_NAME) && (EXI_DECODE_DIN_ATTRIBUTE_NAME == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_AttributeValue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_ATTRIBUTE_VALUE) && (EXI_DECODE_DIN_ATTRIBUTE_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_AttributeValue( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_AttributeValueType, AUTOMATIC, EXI_APPL_VAR) AttributeValuePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AttributeValuePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value AttributeValue attribute (start content not required) */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* #30 If value is in its allowed range */
    if (exiEventCode < 6)
    {
      /* #40 Store value in output buffer */
      *AttributeValuePtr = (Exi_DIN_AttributeValueType)exiEventCode;
    }
    /* #50 Enumeration is out of its allowed range */
    else
    {
      /* #60 Set status code to error */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_ATTRIBUTE_VALUE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_ATTRIBUTE_VALUE) && (EXI_DECODE_DIN_ATTRIBUTE_VALUE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_Body
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_BODY) && (EXI_DECODE_DIN_BODY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_Body( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_BodyType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_BodyType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_BodyType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_BodyType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode substitution group BodyElement */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 6);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 36, 6, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* #40 Swtich event code */
    switch(exiEventCode)
    {
    /* case 0: SE(BodyElement) not required, element is abstract*/
    case 1: /* SE(CableCheckReq) */
      /* #50 Element CableCheckReq */
      {
        /* #60 Decode element CableCheckReq */
      #if (defined(EXI_DECODE_DIN_CABLE_CHECK_REQ) && (EXI_DECODE_DIN_CABLE_CHECK_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_CABLE_CHECK_REQ_TYPE;
        Exi_Decode_DIN_CableCheckReq(DecWsPtr, (Exi_DIN_CableCheckReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CABLE_CHECK_REQ) && (EXI_DECODE_DIN_CABLE_CHECK_REQ == STD_ON)) */
      }
    case 2: /* SE(CableCheckRes) */
      /* #70 Element CableCheckRes */
      {
        /* #80 Decode element CableCheckRes */
      #if (defined(EXI_DECODE_DIN_CABLE_CHECK_RES) && (EXI_DECODE_DIN_CABLE_CHECK_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_CABLE_CHECK_RES_TYPE;
        Exi_Decode_DIN_CableCheckRes(DecWsPtr, (Exi_DIN_CableCheckResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CABLE_CHECK_RES) && (EXI_DECODE_DIN_CABLE_CHECK_RES == STD_ON)) */
      }
    case 3: /* SE(CertificateInstallationReq) */
      /* #90 Element CertificateInstallationReq */
      {
        /* #100 Decode element CertificateInstallationReq */
      #if (defined(EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ) && (EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_CERTIFICATE_INSTALLATION_REQ_TYPE;
        Exi_Decode_DIN_CertificateInstallationReq(DecWsPtr, (Exi_DIN_CertificateInstallationReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ) && (EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ == STD_ON)) */
      }
    case 4: /* SE(CertificateInstallationRes) */
      /* #110 Element CertificateInstallationRes */
      {
        /* #120 Decode element CertificateInstallationRes */
      #if (defined(EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_RES) && (EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_CERTIFICATE_INSTALLATION_RES_TYPE;
        Exi_Decode_DIN_CertificateInstallationRes(DecWsPtr, (Exi_DIN_CertificateInstallationResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_RES) && (EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_RES == STD_ON)) */
      }
    case 5: /* SE(CertificateUpdateReq) */
      /* #130 Element CertificateUpdateReq */
      {
        /* #140 Decode element CertificateUpdateReq */
      #if (defined(EXI_DECODE_DIN_CERTIFICATE_UPDATE_REQ) && (EXI_DECODE_DIN_CERTIFICATE_UPDATE_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_CERTIFICATE_UPDATE_REQ_TYPE;
        Exi_Decode_DIN_CertificateUpdateReq(DecWsPtr, (Exi_DIN_CertificateUpdateReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CERTIFICATE_UPDATE_REQ) && (EXI_DECODE_DIN_CERTIFICATE_UPDATE_REQ == STD_ON)) */
      }
    case 6: /* SE(CertificateUpdateRes) */
      /* #150 Element CertificateUpdateRes */
      {
        /* #160 Decode element CertificateUpdateRes */
      #if (defined(EXI_DECODE_DIN_CERTIFICATE_UPDATE_RES) && (EXI_DECODE_DIN_CERTIFICATE_UPDATE_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_CERTIFICATE_UPDATE_RES_TYPE;
        Exi_Decode_DIN_CertificateUpdateRes(DecWsPtr, (Exi_DIN_CertificateUpdateResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CERTIFICATE_UPDATE_RES) && (EXI_DECODE_DIN_CERTIFICATE_UPDATE_RES == STD_ON)) */
      }
    case 7: /* SE(ChargeParameterDiscoveryReq) */
      /* #170 Element ChargeParameterDiscoveryReq */
      {
        /* #180 Decode element ChargeParameterDiscoveryReq */
      #if (defined(EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_REQ) && (EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_CHARGE_PARAMETER_DISCOVERY_REQ_TYPE;
        Exi_Decode_DIN_ChargeParameterDiscoveryReq(DecWsPtr, (Exi_DIN_ChargeParameterDiscoveryReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_REQ) && (EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_REQ == STD_ON)) */
      }
    case 8: /* SE(ChargeParameterDiscoveryRes) */
      /* #190 Element ChargeParameterDiscoveryRes */
      {
        /* #200 Decode element ChargeParameterDiscoveryRes */
      #if (defined(EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES) && (EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_CHARGE_PARAMETER_DISCOVERY_RES_TYPE;
        Exi_Decode_DIN_ChargeParameterDiscoveryRes(DecWsPtr, (Exi_DIN_ChargeParameterDiscoveryResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES) && (EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES == STD_ON)) */
      }
    case 9: /* SE(ChargingStatusReq) */
      /* #210 Element ChargingStatusReq */
      {
        /* #220 Decode element ChargingStatusReq */
      #if (defined(EXI_DECODE_DIN_CHARGING_STATUS_REQ) && (EXI_DECODE_DIN_CHARGING_STATUS_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_CHARGING_STATUS_REQ_TYPE;
        structPtr->BodyElement = 0;
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CHARGING_STATUS_REQ) && (EXI_DECODE_DIN_CHARGING_STATUS_REQ == STD_ON)) */
      }
    case 10: /* SE(ChargingStatusRes) */
      /* #230 Element ChargingStatusRes */
      {
        /* #240 Decode element ChargingStatusRes */
      #if (defined(EXI_DECODE_DIN_CHARGING_STATUS_RES) && (EXI_DECODE_DIN_CHARGING_STATUS_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_CHARGING_STATUS_RES_TYPE;
        Exi_Decode_DIN_ChargingStatusRes(DecWsPtr, (Exi_DIN_ChargingStatusResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CHARGING_STATUS_RES) && (EXI_DECODE_DIN_CHARGING_STATUS_RES == STD_ON)) */
      }
    case 11: /* SE(ContractAuthenticationReq) */
      /* #250 Element ContractAuthenticationReq */
      {
        /* #260 Decode element ContractAuthenticationReq */
      #if (defined(EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_REQ) && (EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_CONTRACT_AUTHENTICATION_REQ_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_ContractAuthenticationReq(DecWsPtr, (Exi_DIN_ContractAuthenticationReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_REQ) && (EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_REQ == STD_ON)) */
      }
    case 12: /* SE(ContractAuthenticationRes) */
      /* #270 Element ContractAuthenticationRes */
      {
        /* #280 Decode element ContractAuthenticationRes */
      #if (defined(EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_RES) && (EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_CONTRACT_AUTHENTICATION_RES_TYPE;
        Exi_Decode_DIN_ContractAuthenticationRes(DecWsPtr, (Exi_DIN_ContractAuthenticationResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_RES) && (EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_RES == STD_ON)) */
      }
    case 13: /* SE(CurrentDemandReq) */
      /* #290 Element CurrentDemandReq */
      {
        /* #300 Decode element CurrentDemandReq */
      #if (defined(EXI_DECODE_DIN_CURRENT_DEMAND_REQ) && (EXI_DECODE_DIN_CURRENT_DEMAND_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_CURRENT_DEMAND_REQ_TYPE;
        Exi_Decode_DIN_CurrentDemandReq(DecWsPtr, (Exi_DIN_CurrentDemandReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CURRENT_DEMAND_REQ) && (EXI_DECODE_DIN_CURRENT_DEMAND_REQ == STD_ON)) */
      }
    case 14: /* SE(CurrentDemandRes) */
      /* #310 Element CurrentDemandRes */
      {
        /* #320 Decode element CurrentDemandRes */
      #if (defined(EXI_DECODE_DIN_CURRENT_DEMAND_RES) && (EXI_DECODE_DIN_CURRENT_DEMAND_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_CURRENT_DEMAND_RES_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_CurrentDemandRes(DecWsPtr, (Exi_DIN_CurrentDemandResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CURRENT_DEMAND_RES) && (EXI_DECODE_DIN_CURRENT_DEMAND_RES == STD_ON)) */
      }
    case 15: /* SE(MeteringReceiptReq) */
      /* #330 Element MeteringReceiptReq */
      {
        /* #340 Decode element MeteringReceiptReq */
      #if (defined(EXI_DECODE_DIN_METERING_RECEIPT_REQ) && (EXI_DECODE_DIN_METERING_RECEIPT_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_METERING_RECEIPT_REQ_TYPE;
        Exi_Decode_DIN_MeteringReceiptReq(DecWsPtr, (Exi_DIN_MeteringReceiptReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_METERING_RECEIPT_REQ) && (EXI_DECODE_DIN_METERING_RECEIPT_REQ == STD_ON)) */
      }
    case 16: /* SE(MeteringReceiptRes) */
      /* #350 Element MeteringReceiptRes */
      {
        /* #360 Decode element MeteringReceiptRes */
      #if (defined(EXI_DECODE_DIN_METERING_RECEIPT_RES) && (EXI_DECODE_DIN_METERING_RECEIPT_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_METERING_RECEIPT_RES_TYPE;
        Exi_Decode_DIN_MeteringReceiptRes(DecWsPtr, (Exi_DIN_MeteringReceiptResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_METERING_RECEIPT_RES) && (EXI_DECODE_DIN_METERING_RECEIPT_RES == STD_ON)) */
      }
    case 17: /* SE(PaymentDetailsReq) */
      /* #370 Element PaymentDetailsReq */
      {
        /* #380 Decode element PaymentDetailsReq */
      #if (defined(EXI_DECODE_DIN_PAYMENT_DETAILS_REQ) && (EXI_DECODE_DIN_PAYMENT_DETAILS_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_PAYMENT_DETAILS_REQ_TYPE;
        Exi_Decode_DIN_PaymentDetailsReq(DecWsPtr, (Exi_DIN_PaymentDetailsReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_PAYMENT_DETAILS_REQ) && (EXI_DECODE_DIN_PAYMENT_DETAILS_REQ == STD_ON)) */
      }
    case 18: /* SE(PaymentDetailsRes) */
      /* #390 Element PaymentDetailsRes */
      {
        /* #400 Decode element PaymentDetailsRes */
      #if (defined(EXI_DECODE_DIN_PAYMENT_DETAILS_RES) && (EXI_DECODE_DIN_PAYMENT_DETAILS_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_PAYMENT_DETAILS_RES_TYPE;
        Exi_Decode_DIN_PaymentDetailsRes(DecWsPtr, (Exi_DIN_PaymentDetailsResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_PAYMENT_DETAILS_RES) && (EXI_DECODE_DIN_PAYMENT_DETAILS_RES == STD_ON)) */
      }
    case 19: /* SE(PowerDeliveryReq) */
      /* #410 Element PowerDeliveryReq */
      {
        /* #420 Decode element PowerDeliveryReq */
      #if (defined(EXI_DECODE_DIN_POWER_DELIVERY_REQ) && (EXI_DECODE_DIN_POWER_DELIVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_POWER_DELIVERY_REQ_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_PowerDeliveryReq(DecWsPtr, (Exi_DIN_PowerDeliveryReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_POWER_DELIVERY_REQ) && (EXI_DECODE_DIN_POWER_DELIVERY_REQ == STD_ON)) */
      }
    case 20: /* SE(PowerDeliveryRes) */
      /* #430 Element PowerDeliveryRes */
      {
        /* #440 Decode element PowerDeliveryRes */
      #if (defined(EXI_DECODE_DIN_POWER_DELIVERY_RES) && (EXI_DECODE_DIN_POWER_DELIVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_POWER_DELIVERY_RES_TYPE;
        Exi_Decode_DIN_PowerDeliveryRes(DecWsPtr, (Exi_DIN_PowerDeliveryResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_POWER_DELIVERY_RES) && (EXI_DECODE_DIN_POWER_DELIVERY_RES == STD_ON)) */
      }
    case 21: /* SE(PreChargeReq) */
      /* #450 Element PreChargeReq */
      {
        /* #460 Decode element PreChargeReq */
      #if (defined(EXI_DECODE_DIN_PRE_CHARGE_REQ) && (EXI_DECODE_DIN_PRE_CHARGE_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_PRE_CHARGE_REQ_TYPE;
        Exi_Decode_DIN_PreChargeReq(DecWsPtr, (Exi_DIN_PreChargeReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_PRE_CHARGE_REQ) && (EXI_DECODE_DIN_PRE_CHARGE_REQ == STD_ON)) */
      }
    case 22: /* SE(PreChargeRes) */
      /* #470 Element PreChargeRes */
      {
        /* #480 Decode element PreChargeRes */
      #if (defined(EXI_DECODE_DIN_PRE_CHARGE_RES) && (EXI_DECODE_DIN_PRE_CHARGE_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_PRE_CHARGE_RES_TYPE;
        Exi_Decode_DIN_PreChargeRes(DecWsPtr, (Exi_DIN_PreChargeResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_PRE_CHARGE_RES) && (EXI_DECODE_DIN_PRE_CHARGE_RES == STD_ON)) */
      }
    case 23: /* SE(ServiceDetailReq) */
      /* #490 Element ServiceDetailReq */
      {
        /* #500 Decode element ServiceDetailReq */
      #if (defined(EXI_DECODE_DIN_SERVICE_DETAIL_REQ) && (EXI_DECODE_DIN_SERVICE_DETAIL_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_SERVICE_DETAIL_REQ_TYPE;
        Exi_Decode_DIN_ServiceDetailReq(DecWsPtr, (Exi_DIN_ServiceDetailReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_DETAIL_REQ) && (EXI_DECODE_DIN_SERVICE_DETAIL_REQ == STD_ON)) */
      }
    case 24: /* SE(ServiceDetailRes) */
      /* #510 Element ServiceDetailRes */
      {
        /* #520 Decode element ServiceDetailRes */
      #if (defined(EXI_DECODE_DIN_SERVICE_DETAIL_RES) && (EXI_DECODE_DIN_SERVICE_DETAIL_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_SERVICE_DETAIL_RES_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_ServiceDetailRes(DecWsPtr, (Exi_DIN_ServiceDetailResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_DETAIL_RES) && (EXI_DECODE_DIN_SERVICE_DETAIL_RES == STD_ON)) */
      }
    case 25: /* SE(ServiceDiscoveryReq) */
      /* #530 Element ServiceDiscoveryReq */
      {
        /* #540 Decode element ServiceDiscoveryReq */
      #if (defined(EXI_DECODE_DIN_SERVICE_DISCOVERY_REQ) && (EXI_DECODE_DIN_SERVICE_DISCOVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_SERVICE_DISCOVERY_REQ_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_ServiceDiscoveryReq(DecWsPtr, (Exi_DIN_ServiceDiscoveryReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_DISCOVERY_REQ) && (EXI_DECODE_DIN_SERVICE_DISCOVERY_REQ == STD_ON)) */
      }
    case 26: /* SE(ServiceDiscoveryRes) */
      /* #550 Element ServiceDiscoveryRes */
      {
        /* #560 Decode element ServiceDiscoveryRes */
      #if (defined(EXI_DECODE_DIN_SERVICE_DISCOVERY_RES) && (EXI_DECODE_DIN_SERVICE_DISCOVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_SERVICE_DISCOVERY_RES_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_ServiceDiscoveryRes(DecWsPtr, (Exi_DIN_ServiceDiscoveryResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_DISCOVERY_RES) && (EXI_DECODE_DIN_SERVICE_DISCOVERY_RES == STD_ON)) */
      }
    case 27: /* SE(ServicePaymentSelectionReq) */
      /* #570 Element ServicePaymentSelectionReq */
      {
        /* #580 Decode element ServicePaymentSelectionReq */
      #if (defined(EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_REQ) && (EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_SERVICE_PAYMENT_SELECTION_REQ_TYPE;
        Exi_Decode_DIN_ServicePaymentSelectionReq(DecWsPtr, (Exi_DIN_ServicePaymentSelectionReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_REQ) && (EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_REQ == STD_ON)) */
      }
    case 28: /* SE(ServicePaymentSelectionRes) */
      /* #590 Element ServicePaymentSelectionRes */
      {
        /* #600 Decode element ServicePaymentSelectionRes */
      #if (defined(EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_RES) && (EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_SERVICE_PAYMENT_SELECTION_RES_TYPE;
        Exi_Decode_DIN_ServicePaymentSelectionRes(DecWsPtr, (Exi_DIN_ServicePaymentSelectionResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_RES) && (EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_RES == STD_ON)) */
      }
    case 29: /* SE(SessionSetupReq) */
      /* #610 Element SessionSetupReq */
      {
        /* #620 Decode element SessionSetupReq */
      #if (defined(EXI_DECODE_DIN_SESSION_SETUP_REQ) && (EXI_DECODE_DIN_SESSION_SETUP_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_SESSION_SETUP_REQ_TYPE;
        Exi_Decode_DIN_SessionSetupReq(DecWsPtr, (Exi_DIN_SessionSetupReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SESSION_SETUP_REQ) && (EXI_DECODE_DIN_SESSION_SETUP_REQ == STD_ON)) */
      }
    case 30: /* SE(SessionSetupRes) */
      /* #630 Element SessionSetupRes */
      {
        /* #640 Decode element SessionSetupRes */
      #if (defined(EXI_DECODE_DIN_SESSION_SETUP_RES) && (EXI_DECODE_DIN_SESSION_SETUP_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_SESSION_SETUP_RES_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_SessionSetupRes(DecWsPtr, (Exi_DIN_SessionSetupResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SESSION_SETUP_RES) && (EXI_DECODE_DIN_SESSION_SETUP_RES == STD_ON)) */
      }
    case 31: /* SE(SessionStopReq) */
      /* #650 Element SessionStopReq */
      {
        /* #660 Decode element SessionStopReq */
      #if (defined(EXI_DECODE_DIN_SESSION_STOP_REQ) && (EXI_DECODE_DIN_SESSION_STOP_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_SESSION_STOP_REQ_TYPE;
        structPtr->BodyElement = 0;
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SESSION_STOP_REQ) && (EXI_DECODE_DIN_SESSION_STOP_REQ == STD_ON)) */
      }
    case 32: /* SE(SessionStopRes) */
      /* #670 Element SessionStopRes */
      {
        /* #680 Decode element SessionStopRes */
      #if (defined(EXI_DECODE_DIN_SESSION_STOP_RES) && (EXI_DECODE_DIN_SESSION_STOP_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_SESSION_STOP_RES_TYPE;
        Exi_Decode_DIN_SessionStopRes(DecWsPtr, (Exi_DIN_SessionStopResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SESSION_STOP_RES) && (EXI_DECODE_DIN_SESSION_STOP_RES == STD_ON)) */
      }
    case 33: /* SE(WeldingDetectionReq) */
      /* #690 Element WeldingDetectionReq */
      {
        /* #700 Decode element WeldingDetectionReq */
      #if (defined(EXI_DECODE_DIN_WELDING_DETECTION_REQ) && (EXI_DECODE_DIN_WELDING_DETECTION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_WELDING_DETECTION_REQ_TYPE;
        Exi_Decode_DIN_WeldingDetectionReq(DecWsPtr, (Exi_DIN_WeldingDetectionReqType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_WELDING_DETECTION_REQ) && (EXI_DECODE_DIN_WELDING_DETECTION_REQ == STD_ON)) */
      }
    case 34: /* SE(WeldingDetectionRes) */
      /* #710 Element WeldingDetectionRes */
      {
        /* #720 Decode element WeldingDetectionRes */
      #if (defined(EXI_DECODE_DIN_WELDING_DETECTION_RES) && (EXI_DECODE_DIN_WELDING_DETECTION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->BodyElementElementId = EXI_DIN_WELDING_DETECTION_RES_TYPE;
        Exi_Decode_DIN_WeldingDetectionRes(DecWsPtr, (Exi_DIN_WeldingDetectionResType**)&(structPtr->BodyElement)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->BodyElementFlag = 1;
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_BODY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_WELDING_DETECTION_RES) && (EXI_DECODE_DIN_WELDING_DETECTION_RES == STD_ON)) */
      }
    case 35: /* optional element not included */
      /* Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      break;
    default:
      /* #730 Unknown element */
      {
        {
          /* #740 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* End of Substitution Group */
    if(1 == structPtr->BodyElementFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_BODY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_BODY) && (EXI_DECODE_DIN_BODY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_CableCheckReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CABLE_CHECK_REQ) && (EXI_DECODE_DIN_CABLE_CHECK_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_CableCheckReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_CableCheckReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_CableCheckReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_CableCheckReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_CableCheckReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_CABLE_CHECK_REQ_TYPE);
    /* #40 Decode element DC_EVStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVStatus) */
    {
      #if (defined(EXI_DECODE_DIN_DC_EVSTATUS) && (EXI_DECODE_DIN_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_DC_EVStatus(DecWsPtr, &(structPtr->DC_EVStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CABLE_CHECK_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVSTATUS) && (EXI_DECODE_DIN_DC_EVSTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DC_EVStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 If CableCheckReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(CableCheckReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CABLE_CHECK_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CABLE_CHECK_REQ) && (EXI_DECODE_DIN_CABLE_CHECK_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_CableCheckRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CABLE_CHECK_RES) && (EXI_DECODE_DIN_CABLE_CHECK_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_CableCheckRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_CableCheckResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_CableCheckResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_CableCheckResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_CableCheckResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_CABLE_CHECK_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CABLE_CHECK_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element DC_EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVSEStatus) */
    {
      #if (defined(EXI_DECODE_DIN_DC_EVSESTATUS) && (EXI_DECODE_DIN_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_DC_EVSEStatus(DecWsPtr, &(structPtr->DC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CABLE_CHECK_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVSESTATUS) && (EXI_DECODE_DIN_DC_EVSESTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVSEProcessing */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEProcessing) */
    {
      #if (defined(EXI_DECODE_DIN_EVSEPROCESSING) && (EXI_DECODE_DIN_EVSEPROCESSING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #80 Decode EVSEProcessing as enumeration value */
      Exi_Decode_DIN_EVSEProcessing(DecWsPtr, &structPtr->EVSEProcessing);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CABLE_CHECK_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_EVSEPROCESSING) && (EXI_DECODE_DIN_EVSEPROCESSING == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEProcessing) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 If CableCheckRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(CableCheckRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CABLE_CHECK_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CABLE_CHECK_RES) && (EXI_DECODE_DIN_CABLE_CHECK_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_CertificateChain
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CERTIFICATE_CHAIN) && (EXI_DECODE_DIN_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_CertificateChain( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_CertificateChainType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_CertificateChainType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_CertificateChainType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_CertificateChainType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element Certificate */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(Certificate) */
    {
      #if (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_certificate(DecWsPtr, &(structPtr->Certificate));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_CHAIN, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(Certificate) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element SubCertificates */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(SubCertificates) */
    {
      #if (defined(EXI_DECODE_DIN_SUB_CERTIFICATES) && (EXI_DECODE_DIN_SUB_CERTIFICATES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_SubCertificates(DecWsPtr, &(structPtr->SubCertificates));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->SubCertificatesFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_CHAIN, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SUB_CERTIFICATES) && (EXI_DECODE_DIN_SUB_CERTIFICATES == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->SubCertificatesFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CERTIFICATE_CHAIN, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CERTIFICATE_CHAIN) && (EXI_DECODE_DIN_CERTIFICATE_CHAIN == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_CertificateInstallationReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ) && (EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_CertificateInstallationReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_CertificateInstallationReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_CertificateInstallationReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_CertificateInstallationReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_CertificateInstallationReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_CERTIFICATE_INSTALLATION_REQ_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, TRUE, TRUE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element OEMProvisioningCert with missing elements in front */
    if(0 == structPtr->IdFlag)
    {
      /* SE(OEMProvisioningCert) already decoded */
    #if (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_certificate(DecWsPtr, &(structPtr->OEMProvisioningCert));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) */
    }
    /* #60 Decode element OEMProvisioningCert without missing elements in front */
    else
    {
      /* #70 Decode element OEMProvisioningCert */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(OEMProvisioningCert) */
      {
      #if (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_certificate(DecWsPtr, &(structPtr->OEMProvisioningCert));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(OEMProvisioningCert) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode element ListOfRootCertificateIDs */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ListOfRootCertificateIDs) */
    {
      #if (defined(EXI_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_ListOfRootCertificateIDs(DecWsPtr, &(structPtr->ListOfRootCertificateIDs));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #90 Decode element DHParams */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DHParams) */
    {
      #if (defined(EXI_DECODE_DIN_D_HPARAMS) && (EXI_DECODE_DIN_D_HPARAMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_dHParams(DecWsPtr, &(structPtr->DHParams));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_D_HPARAMS) && (EXI_DECODE_DIN_D_HPARAMS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DHParams) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #100 If CertificateInstallationReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(CertificateInstallationReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ) && (EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_CertificateInstallationRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_RES) && (EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_CertificateInstallationRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_CertificateInstallationResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_CertificateInstallationResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_CertificateInstallationResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_CertificateInstallationResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_CERTIFICATE_INSTALLATION_RES_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, TRUE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #60 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element ContractSignatureCertChain */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ContractSignatureCertChain) */
    {
      #if (defined(EXI_DECODE_DIN_CERTIFICATE_CHAIN) && (EXI_DECODE_DIN_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_CertificateChain(DecWsPtr, &(structPtr->ContractSignatureCertChain));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_CERTIFICATE_CHAIN) && (EXI_DECODE_DIN_CERTIFICATE_CHAIN == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #80 Decode element ContractSignatureEncryptedPrivateKey */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ContractSignatureEncryptedPrivateKey) */
    {
      #if (defined(EXI_DECODE_DIN_PRIVATE_KEY) && (EXI_DECODE_DIN_PRIVATE_KEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_privateKey(DecWsPtr, &(structPtr->ContractSignatureEncryptedPrivateKey));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PRIVATE_KEY) && (EXI_DECODE_DIN_PRIVATE_KEY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ContractSignatureEncryptedPrivateKey) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode element DHParams */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DHParams) */
    {
      #if (defined(EXI_DECODE_DIN_D_HPARAMS) && (EXI_DECODE_DIN_D_HPARAMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_dHParams(DecWsPtr, &(structPtr->DHParams));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_D_HPARAMS) && (EXI_DECODE_DIN_D_HPARAMS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DHParams) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #100 Decode element ContractID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ContractID) */
    {
      #if (defined(EXI_DECODE_DIN_CONTRACT_ID) && (EXI_DECODE_DIN_CONTRACT_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_contractID(DecWsPtr, &(structPtr->ContractID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_CONTRACT_ID) && (EXI_DECODE_DIN_CONTRACT_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ContractID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #110 If CertificateInstallationRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(CertificateInstallationRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CERTIFICATE_INSTALLATION_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_RES) && (EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_CertificateUpdateReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CERTIFICATE_UPDATE_REQ) && (EXI_DECODE_DIN_CERTIFICATE_UPDATE_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_CertificateUpdateReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_CertificateUpdateReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_CertificateUpdateReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_CertificateUpdateReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_CertificateUpdateReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_CERTIFICATE_UPDATE_REQ_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, TRUE, TRUE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_UPDATE_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element ContractSignatureCertChain with missing elements in front */
    if(0 == structPtr->IdFlag)
    {
      /* SE(ContractSignatureCertChain) already decoded */
    #if (defined(EXI_DECODE_DIN_CERTIFICATE_CHAIN) && (EXI_DECODE_DIN_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_CertificateChain(DecWsPtr, &(structPtr->ContractSignatureCertChain));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_UPDATE_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_CERTIFICATE_CHAIN) && (EXI_DECODE_DIN_CERTIFICATE_CHAIN == STD_ON)) */
    }
    /* #60 Decode element ContractSignatureCertChain without missing elements in front */
    else
    {
      /* #70 Decode element ContractSignatureCertChain */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(ContractSignatureCertChain) */
      {
      #if (defined(EXI_DECODE_DIN_CERTIFICATE_CHAIN) && (EXI_DECODE_DIN_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_CertificateChain(DecWsPtr, &(structPtr->ContractSignatureCertChain));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_UPDATE_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_CERTIFICATE_CHAIN) && (EXI_DECODE_DIN_CERTIFICATE_CHAIN == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #80 Decode element ContractID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ContractID) */
    {
      #if (defined(EXI_DECODE_DIN_CONTRACT_ID) && (EXI_DECODE_DIN_CONTRACT_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_contractID(DecWsPtr, &(structPtr->ContractID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_UPDATE_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_CONTRACT_ID) && (EXI_DECODE_DIN_CONTRACT_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ContractID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode element ListOfRootCertificateIDs */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ListOfRootCertificateIDs) */
    {
      #if (defined(EXI_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_ListOfRootCertificateIDs(DecWsPtr, &(structPtr->ListOfRootCertificateIDs));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_UPDATE_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #100 Decode element DHParams */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DHParams) */
    {
      #if (defined(EXI_DECODE_DIN_D_HPARAMS) && (EXI_DECODE_DIN_D_HPARAMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_dHParams(DecWsPtr, &(structPtr->DHParams));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_UPDATE_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_D_HPARAMS) && (EXI_DECODE_DIN_D_HPARAMS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DHParams) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #110 If CertificateUpdateReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(CertificateUpdateReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CERTIFICATE_UPDATE_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CERTIFICATE_UPDATE_REQ) && (EXI_DECODE_DIN_CERTIFICATE_UPDATE_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_CertificateUpdateRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CERTIFICATE_UPDATE_RES) && (EXI_DECODE_DIN_CERTIFICATE_UPDATE_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_CertificateUpdateRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_CertificateUpdateResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_CertificateUpdateResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_CertificateUpdateResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_CertificateUpdateResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_CERTIFICATE_UPDATE_RES_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, TRUE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #60 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element ContractSignatureCertChain */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ContractSignatureCertChain) */
    {
      #if (defined(EXI_DECODE_DIN_CERTIFICATE_CHAIN) && (EXI_DECODE_DIN_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_CertificateChain(DecWsPtr, &(structPtr->ContractSignatureCertChain));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_CERTIFICATE_CHAIN) && (EXI_DECODE_DIN_CERTIFICATE_CHAIN == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #80 Decode element ContractSignatureEncryptedPrivateKey */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ContractSignatureEncryptedPrivateKey) */
    {
      #if (defined(EXI_DECODE_DIN_PRIVATE_KEY) && (EXI_DECODE_DIN_PRIVATE_KEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_privateKey(DecWsPtr, &(structPtr->ContractSignatureEncryptedPrivateKey));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PRIVATE_KEY) && (EXI_DECODE_DIN_PRIVATE_KEY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ContractSignatureEncryptedPrivateKey) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode element DHParams */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DHParams) */
    {
      #if (defined(EXI_DECODE_DIN_D_HPARAMS) && (EXI_DECODE_DIN_D_HPARAMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_dHParams(DecWsPtr, &(structPtr->DHParams));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_D_HPARAMS) && (EXI_DECODE_DIN_D_HPARAMS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DHParams) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #100 Decode element ContractID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ContractID) */
    {
      #if (defined(EXI_DECODE_DIN_CONTRACT_ID) && (EXI_DECODE_DIN_CONTRACT_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_contractID(DecWsPtr, &(structPtr->ContractID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_CONTRACT_ID) && (EXI_DECODE_DIN_CONTRACT_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ContractID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #110 Decode element RetryCounter */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(RetryCounter) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->RetryCounter);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(RetryCounter) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #120 If CertificateUpdateRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(CertificateUpdateRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CERTIFICATE_UPDATE_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CERTIFICATE_UPDATE_RES) && (EXI_DECODE_DIN_CERTIFICATE_UPDATE_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ChargeParameterDiscoveryReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_REQ) && (EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ChargeParameterDiscoveryReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ChargeParameterDiscoveryReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ChargeParameterDiscoveryReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ChargeParameterDiscoveryReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ChargeParameterDiscoveryReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_CHARGE_PARAMETER_DISCOVERY_REQ_TYPE);
    /* #40 Decode element EVRequestedEnergyTransferType */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVRequestedEnergyTransferType) */
    {
      #if (defined(EXI_DECODE_DIN_EVREQUESTED_ENERGY_TRANSFER) && (EXI_DECODE_DIN_EVREQUESTED_ENERGY_TRANSFER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode EVRequestedEnergyTransfer as enumeration value */
      Exi_Decode_DIN_EVRequestedEnergyTransfer(DecWsPtr, &structPtr->EVRequestedEnergyTransferType);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_EVREQUESTED_ENERGY_TRANSFER) && (EXI_DECODE_DIN_EVREQUESTED_ENERGY_TRANSFER == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVRequestedEnergyTransferType) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode substitution group EVChargeParameter */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* #70 Swtich event code */
    switch(exiEventCode)
    {
    case 0: /* SE(AC_EVChargeParameter) */
      /* #80 Element AC_EVChargeParameter */
      {
        /* #90 Decode element AC_EVChargeParameter */
      #if (defined(EXI_DECODE_DIN_AC_EVCHARGE_PARAMETER) && (EXI_DECODE_DIN_AC_EVCHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->EVChargeParameterElementId = EXI_DIN_AC_EVCHARGE_PARAMETER_TYPE;
        Exi_Decode_DIN_AC_EVChargeParameter(DecWsPtr, (Exi_DIN_AC_EVChargeParameterType**)&(structPtr->EVChargeParameter)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_AC_EVCHARGE_PARAMETER) && (EXI_DECODE_DIN_AC_EVCHARGE_PARAMETER == STD_ON)) */
      }
    case 1: /* SE(DC_EVChargeParameter) */
      /* #100 Element DC_EVChargeParameter */
      {
        /* #110 Decode element DC_EVChargeParameter */
      #if (defined(EXI_DECODE_DIN_DC_EVCHARGE_PARAMETER) && (EXI_DECODE_DIN_DC_EVCHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->EVChargeParameterElementId = EXI_DIN_DC_EVCHARGE_PARAMETER_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_DC_EVChargeParameter(DecWsPtr, (Exi_DIN_DC_EVChargeParameterType**)&(structPtr->EVChargeParameter)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVCHARGE_PARAMETER) && (EXI_DECODE_DIN_DC_EVCHARGE_PARAMETER == STD_ON)) */
      }
    /* case 2: SE(EVChargeParameter) not required, element is abstract*/
    default:
      /* #120 Unknown element */
      {
        {
          /* #130 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* End of Substitution Group */
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #140 If ChargeParameterDiscoveryReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(ChargeParameterDiscoveryReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_REQ) && (EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ChargeParameterDiscoveryRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES) && (EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ChargeParameterDiscoveryRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ChargeParameterDiscoveryResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ChargeParameterDiscoveryResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ChargeParameterDiscoveryResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ChargeParameterDiscoveryResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_CHARGE_PARAMETER_DISCOVERY_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVSEProcessing */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEProcessing) */
    {
      #if (defined(EXI_DECODE_DIN_EVSEPROCESSING) && (EXI_DECODE_DIN_EVSEPROCESSING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #70 Decode EVSEProcessing as enumeration value */
      Exi_Decode_DIN_EVSEProcessing(DecWsPtr, &structPtr->EVSEProcessing);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_EVSEPROCESSING) && (EXI_DECODE_DIN_EVSEPROCESSING == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEProcessing) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode substitution group SASchedules */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* #90 Swtich event code */
    switch(exiEventCode)
    {
    case 0: /* SE(SAScheduleList) */
      /* #100 Element SAScheduleList */
      {
        /* #110 Decode element SAScheduleList */
      #if (defined(EXI_DECODE_DIN_SASCHEDULE_LIST) && (EXI_DECODE_DIN_SASCHEDULE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->SASchedulesElementId = EXI_DIN_SASCHEDULE_LIST_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_SAScheduleList(DecWsPtr, (Exi_DIN_SAScheduleListType**)&(structPtr->SASchedules)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SASCHEDULE_LIST) && (EXI_DECODE_DIN_SASCHEDULE_LIST == STD_ON)) */
      }
    /* case 1: SE(SASchedules) not required, element is abstract*/
    default:
      /* #120 Unknown element */
      {
        {
          /* #130 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* End of Substitution Group */
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #140 Decode substitution group EVSEChargeParameter */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* #150 Swtich event code */
    switch(exiEventCode)
    {
    case 0: /* SE(AC_EVSEChargeParameter) */
      /* #160 Element AC_EVSEChargeParameter */
      {
        /* #170 Decode element AC_EVSEChargeParameter */
      #if (defined(EXI_DECODE_DIN_AC_EVSECHARGE_PARAMETER) && (EXI_DECODE_DIN_AC_EVSECHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->EVSEChargeParameterElementId = EXI_DIN_AC_EVSECHARGE_PARAMETER_TYPE;
        Exi_Decode_DIN_AC_EVSEChargeParameter(DecWsPtr, (Exi_DIN_AC_EVSEChargeParameterType**)&(structPtr->EVSEChargeParameter)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_AC_EVSECHARGE_PARAMETER) && (EXI_DECODE_DIN_AC_EVSECHARGE_PARAMETER == STD_ON)) */
      }
    case 1: /* SE(DC_EVSEChargeParameter) */
      /* #180 Element DC_EVSEChargeParameter */
      {
        /* #190 Decode element DC_EVSEChargeParameter */
      #if (defined(EXI_DECODE_DIN_DC_EVSECHARGE_PARAMETER) && (EXI_DECODE_DIN_DC_EVSECHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->EVSEChargeParameterElementId = EXI_DIN_DC_EVSECHARGE_PARAMETER_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_DC_EVSEChargeParameter(DecWsPtr, (Exi_DIN_DC_EVSEChargeParameterType**)&(structPtr->EVSEChargeParameter)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVSECHARGE_PARAMETER) && (EXI_DECODE_DIN_DC_EVSECHARGE_PARAMETER == STD_ON)) */
      }
    /* case 2: SE(EVSEChargeParameter) not required, element is abstract*/
    default:
      /* #200 Unknown element */
      {
        {
          /* #210 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* End of Substitution Group */
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #220 If ChargeParameterDiscoveryRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(ChargeParameterDiscoveryRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES) && (EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ChargingProfile
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CHARGING_PROFILE) && (EXI_DECODE_DIN_CHARGING_PROFILE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ChargingProfile( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ChargingProfileType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ChargingProfileType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_DIN_ProfileEntryType) lastProfileEntry;
  #if (defined(EXI_DECODE_DIN_PROFILE_ENTRY) && (EXI_DECODE_DIN_PROFILE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_DIN_PROFILEENTRY - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_DIN_PROFILEENTRY - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_DIN_PROFILE_ENTRY) && (EXI_DECODE_DIN_PROFILE_ENTRY == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ChargingProfileType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ChargingProfileType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element SAScheduleTupleID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(SAScheduleTupleID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->SAScheduleTupleID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(SAScheduleTupleID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode multi occurence element ProfileEntry, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ProfileEntry) */
    {
      #if (defined(EXI_DECODE_DIN_PROFILE_ENTRY) && (EXI_DECODE_DIN_PROFILE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_ProfileEntry(DecWsPtr, &(structPtr->ProfileEntry));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGING_PROFILE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PROFILE_ENTRY) && (EXI_DECODE_DIN_PROFILE_ENTRY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->ProfileEntry->NextProfileEntryPtr = (Exi_DIN_ProfileEntryType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ProfileEntry) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastProfileEntry = structPtr->ProfileEntry;
    #if (defined(EXI_DECODE_DIN_PROFILE_ENTRY) && (EXI_DECODE_DIN_PROFILE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_PROFILEENTRY - 1) > 0)
    /* #50 Decode subsequent occurences of ProfileEntry */
    for(i=0; i<(EXI_MAXOCCURS_DIN_PROFILEENTRY - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* SE(ProfileEntry) */
      {
        Exi_Decode_DIN_ProfileEntry(DecWsPtr, &(lastProfileEntry->NextProfileEntryPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastProfileEntry = lastProfileEntry->NextProfileEntryPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(ProfileEntry) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_DIN_PROFILEENTRY - 1) > 0) */
    #endif /* (defined(EXI_DECODE_DIN_PROFILE_ENTRY) && (EXI_DECODE_DIN_PROFILE_ENTRY == STD_ON)) */
    lastProfileEntry->NextProfileEntryPtr = (Exi_DIN_ProfileEntryType*)NULL_PTR;
    exiEventCode = 0;
    #if (defined(EXI_DECODE_DIN_PROFILE_ENTRY) && (EXI_DECODE_DIN_PROFILE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_PROFILEENTRY - 1) > 0)
    if((EXI_MAXOCCURS_DIN_PROFILEENTRY - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_DIN_PROFILEENTRY - 1) > 0) */
    #endif /* #if (defined(EXI_DECODE_DIN_PROFILE_ENTRY) && (EXI_DECODE_DIN_PROFILE_ENTRY == STD_ON)) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CHARGING_PROFILE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CHARGING_PROFILE) && (EXI_DECODE_DIN_CHARGING_PROFILE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ChargingStatusRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CHARGING_STATUS_RES) && (EXI_DECODE_DIN_CHARGING_STATUS_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ChargingStatusRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ChargingStatusResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ChargingStatusResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ChargingStatusResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ChargingStatusResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_CHARGING_STATUS_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVSEID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEID) */
    {
      #if (defined(EXI_DECODE_DIN_EVSE_ID) && (EXI_DECODE_DIN_EVSE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_evseID(DecWsPtr, &(structPtr->EVSEID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_EVSE_ID) && (EXI_DECODE_DIN_EVSE_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element SAScheduleTupleID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(SAScheduleTupleID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->SAScheduleTupleID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(SAScheduleTupleID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode element EVSEMaxCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMaxCurrent) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaxCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVSEMaxCurrentFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(MeterInfo) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->MeterInfoFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVSEMaxCurrentFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVSEMaxCurrent) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #90 Decode optional element MeterInfo with missing elements in front */
    if((0 == structPtr->EVSEMaxCurrentFlag) && (1 == structPtr->MeterInfoFlag))
    {
      /* SE(MeterInfo) already decoded */
    #if (defined(EXI_DECODE_DIN_METER_INFO) && (EXI_DECODE_DIN_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_MeterInfo(DecWsPtr, &(structPtr->MeterInfo));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_METER_INFO) && (EXI_DECODE_DIN_METER_INFO == STD_ON)) */
    }
    /* #100 Decode optional element MeterInfo without missing elements in front */
    else if(1 == structPtr->EVSEMaxCurrentFlag)
    {
      /* #110 Decode element MeterInfo */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(MeterInfo) */
      {
      #if (defined(EXI_DECODE_DIN_METER_INFO) && (EXI_DECODE_DIN_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_MeterInfo(DecWsPtr, &(structPtr->MeterInfo));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->MeterInfoFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_METER_INFO) && (EXI_DECODE_DIN_METER_INFO == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->MeterInfoFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    /* #120 Decode element ReceiptRequired with missing elements in front */
    if(0 == structPtr->MeterInfoFlag)
    {
      /* SE(ReceiptRequired) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ReceiptRequired);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #130 Decode element ReceiptRequired without missing elements in front */
    else
    {
      /* #140 Decode element ReceiptRequired */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(ReceiptRequired) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)
        {
          Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ReceiptRequired);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ReceiptRequired) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #150 Decode element AC_EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(AC_EVSEStatus) */
    {
      #if (defined(EXI_DECODE_DIN_AC_EVSESTATUS) && (EXI_DECODE_DIN_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_AC_EVSEStatus(DecWsPtr, &(structPtr->AC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_AC_EVSESTATUS) && (EXI_DECODE_DIN_AC_EVSESTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(AC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #160 If ChargingStatusRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(ChargingStatusRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CHARGING_STATUS_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CHARGING_STATUS_RES) && (EXI_DECODE_DIN_CHARGING_STATUS_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ConsumptionCost
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CONSUMPTION_COST) && (EXI_DECODE_DIN_CONSUMPTION_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ConsumptionCost( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ConsumptionCostType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ConsumptionCostType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_DIN_CostType) lastCost;
  #if (defined(EXI_DECODE_DIN_COST) && (EXI_DECODE_DIN_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_DIN_COST - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_DIN_COST - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_DIN_COST) && (EXI_DECODE_DIN_COST == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ConsumptionCostType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ConsumptionCostType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element startValue */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(startValue) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->startValue);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(startValue) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode multi occurence element Cost, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(Cost) */
    {
      #if (defined(EXI_DECODE_DIN_COST) && (EXI_DECODE_DIN_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_Cost(DecWsPtr, &(structPtr->Cost));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->CostFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CONSUMPTION_COST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_COST) && (EXI_DECODE_DIN_COST == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->CostFlag)
    {
      structPtr->Cost->NextCostPtr = (Exi_DIN_CostType*)NULL_PTR;
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
      if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
      {
        /* an error occured in a previous step -> abort to avoid not required loop */
        return;
      }
      lastCost = structPtr->Cost;
      #if (defined(EXI_DECODE_DIN_COST) && (EXI_DECODE_DIN_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_DIN_COST - 1) > 0)
      /* #50 Decode subsequent occurences of Cost */
      for(i=0; i<(EXI_MAXOCCURS_DIN_COST - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
      { /* PRQA S 3201 */ /* MD_MSR_14.1 */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)/* SE(Cost) */
        {
          DecWsPtr->DecWs.EERequired = TRUE;
          Exi_Decode_DIN_Cost(DecWsPtr, &(lastCost->NextCostPtr));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          lastCost = lastCost->NextCostPtr;
        }
        else if(1 == exiEventCode)/* reached next Tag */
        {
          DecWsPtr->DecWs.EERequired = FALSE;
          break;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
        if(FALSE == DecWsPtr->DecWs.EERequired)
        {
          /* EE(Cost) already decoded */
          DecWsPtr->DecWs.EERequired = TRUE;
          continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
        }
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* check exiEventCode equals EE(Cost) */
        if(0 != exiEventCode)
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      #endif /* #if ((EXI_MAXOCCURS_DIN_COST - 1) > 0) */
      #endif /* (defined(EXI_DECODE_DIN_COST) && (EXI_DECODE_DIN_COST == STD_ON)) */
      lastCost->NextCostPtr = (Exi_DIN_CostType*)NULL_PTR;
      exiEventCode = 0;
      #if (defined(EXI_DECODE_DIN_COST) && (EXI_DECODE_DIN_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_DIN_COST - 1) > 0)
      if((EXI_MAXOCCURS_DIN_COST - 1) == i)
      #endif /* #if ((EXI_MAXOCCURS_DIN_COST - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_DIN_COST) && (EXI_DECODE_DIN_COST == STD_ON)) */
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        if(1 == exiEventCode)
        {
          DecWsPtr->DecWs.EERequired = FALSE;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CONSUMPTION_COST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CONSUMPTION_COST) && (EXI_DECODE_DIN_CONSUMPTION_COST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ContractAuthenticationReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_REQ) && (EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ContractAuthenticationReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ContractAuthenticationReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ContractAuthenticationReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ContractAuthenticationReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ContractAuthenticationReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_CONTRACT_AUTHENTICATION_REQ_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, FALSE, TRUE, TRUE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CONTRACT_AUTHENTICATION_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(GenChallenge) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->GenChallengeFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode optional element GenChallenge with missing elements in front */
    if((0 == structPtr->IdFlag) && (1 == structPtr->GenChallengeFlag))
    {
      /* SE(GenChallenge) already decoded */
    #if (defined(EXI_DECODE_DIN_GEN_CHALLENGE) && (EXI_DECODE_DIN_GEN_CHALLENGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_genChallenge(DecWsPtr, &(structPtr->GenChallenge));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CONTRACT_AUTHENTICATION_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_GEN_CHALLENGE) && (EXI_DECODE_DIN_GEN_CHALLENGE == STD_ON)) */
    }
    /* #60 Decode optional element GenChallenge without missing elements in front */
    else if(1 == structPtr->IdFlag)
    {
      /* #70 Decode element GenChallenge */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, TRUE, 1, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(GenChallenge) */
      {
      #if (defined(EXI_DECODE_DIN_GEN_CHALLENGE) && (EXI_DECODE_DIN_GEN_CHALLENGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_genChallenge(DecWsPtr, &(structPtr->GenChallenge));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->GenChallengeFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CONTRACT_AUTHENTICATION_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_GEN_CHALLENGE) && (EXI_DECODE_DIN_GEN_CHALLENGE == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->GenChallengeFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(GenChallenge) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #80 If ContractAuthenticationReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->GenChallengeFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)/* EE(ContractAuthenticationReq) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(ContractAuthenticationReq) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CONTRACT_AUTHENTICATION_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_REQ) && (EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ContractAuthenticationRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_RES) && (EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ContractAuthenticationRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ContractAuthenticationResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ContractAuthenticationResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ContractAuthenticationResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ContractAuthenticationResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_CONTRACT_AUTHENTICATION_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CONTRACT_AUTHENTICATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVSEProcessing */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEProcessing) */
    {
      #if (defined(EXI_DECODE_DIN_EVSEPROCESSING) && (EXI_DECODE_DIN_EVSEPROCESSING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #70 Decode EVSEProcessing as enumeration value */
      Exi_Decode_DIN_EVSEProcessing(DecWsPtr, &structPtr->EVSEProcessing);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CONTRACT_AUTHENTICATION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_EVSEPROCESSING) && (EXI_DECODE_DIN_EVSEPROCESSING == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEProcessing) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 If ContractAuthenticationRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(ContractAuthenticationRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CONTRACT_AUTHENTICATION_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_RES) && (EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_Cost
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_COST) && (EXI_DECODE_DIN_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_Cost( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_CostType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_CostType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_CostType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_CostType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element costKind */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(costKind) */
    {
      #if (defined(EXI_DECODE_DIN_COST_KIND) && (EXI_DECODE_DIN_COST_KIND == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #40 Decode costKind as enumeration value */
      Exi_Decode_DIN_costKind(DecWsPtr, &structPtr->costKind);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_COST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_COST_KIND) && (EXI_DECODE_DIN_COST_KIND == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(costKind) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element amount */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(amount) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->amount);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(amount) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element amountMultiplier */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(amountMultiplier) */
    {
      Exi_BitBufType value = 0;
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->amountMultiplierFlag = 1;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 3);
        structPtr->amountMultiplier = (sint8)(value - 3);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->amountMultiplierFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(amountMultiplier) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_COST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_COST) && (EXI_DECODE_DIN_COST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_CurrentDemandReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CURRENT_DEMAND_REQ) && (EXI_DECODE_DIN_CURRENT_DEMAND_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_CurrentDemandReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_CurrentDemandReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_CurrentDemandReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_CurrentDemandReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_CurrentDemandReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_CURRENT_DEMAND_REQ_TYPE);
    /* #40 Decode element DC_EVStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVStatus) */
    {
      #if (defined(EXI_DECODE_DIN_DC_EVSTATUS) && (EXI_DECODE_DIN_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_DC_EVStatus(DecWsPtr, &(structPtr->DC_EVStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVSTATUS) && (EXI_DECODE_DIN_DC_EVSTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DC_EVStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element EVTargetCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVTargetCurrent) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVTargetCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVTargetCurrent) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVMaximumVoltageLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 5, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVMaximumVoltageLimit) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumVoltageLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVMaximumVoltageLimitFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(EVMaximumCurrentLimit) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->EVMaximumCurrentLimitFlag = 1;
    }
    else if(exiEventCode == 2) /* SE(EVMaximumPowerLimit) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->EVMaximumPowerLimitFlag = 1;
    }
    else if(exiEventCode == 3) /* SE(BulkChargingComplete) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->BulkChargingCompleteFlag = 1;
    }
    else if(exiEventCode == 4)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVMaximumVoltageLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVMaximumVoltageLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #70 Decode optional element EVMaximumCurrentLimit with missing elements in front */
    if((0 == structPtr->EVMaximumVoltageLimitFlag) && (1 == structPtr->EVMaximumCurrentLimitFlag))
    {
      /* SE(EVMaximumCurrentLimit) already decoded */
    #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumCurrentLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #80 Decode optional element EVMaximumCurrentLimit without missing elements in front */
    else if(1 == structPtr->EVMaximumVoltageLimitFlag)
    {
      /* #90 Decode element EVMaximumCurrentLimit */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 4, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(EVMaximumCurrentLimit) */
      {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumCurrentLimit));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->EVMaximumCurrentLimitFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(EVMaximumPowerLimit) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->EVMaximumPowerLimitFlag = 1;
      }
      else if(exiEventCode == 2) /* SE(BulkChargingComplete) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->BulkChargingCompleteFlag = 1;
      }
      else if(exiEventCode == 3)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->EVMaximumCurrentLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVMaximumCurrentLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #100 Decode optional element EVMaximumPowerLimit with missing elements in front */
    if((0 == structPtr->EVMaximumCurrentLimitFlag) && (1 == structPtr->EVMaximumPowerLimitFlag))
    {
      /* SE(EVMaximumPowerLimit) already decoded */
    #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumPowerLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #110 Decode optional element EVMaximumPowerLimit without missing elements in front */
    else if(1 == structPtr->EVMaximumCurrentLimitFlag)
    {
      /* #120 Decode element EVMaximumPowerLimit */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(EVMaximumPowerLimit) */
      {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumPowerLimit));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->EVMaximumPowerLimitFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(BulkChargingComplete) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->BulkChargingCompleteFlag = 1;
      }
      else if(exiEventCode == 2)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->EVMaximumPowerLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVMaximumPowerLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #130 Decode optional element BulkChargingComplete with missing elements in front */
    if((0 == structPtr->EVMaximumPowerLimitFlag) && (1 == structPtr->BulkChargingCompleteFlag))
    {
      /* SE(BulkChargingComplete) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->BulkChargingComplete);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #140 Decode optional element BulkChargingComplete without missing elements in front */
    else if(1 == structPtr->EVMaximumPowerLimitFlag)
    {
      /* #150 Decode element BulkChargingComplete */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(BulkChargingComplete) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)
        {
          structPtr->BulkChargingCompleteFlag = 1;
          Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->BulkChargingComplete);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->BulkChargingCompleteFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(BulkChargingComplete) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #160 Decode element ChargingComplete with missing elements in front */
    if(0 == structPtr->BulkChargingCompleteFlag)
    {
      /* SE(ChargingComplete) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ChargingComplete);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #170 Decode element ChargingComplete without missing elements in front */
    else
    {
      /* #180 Decode element ChargingComplete */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(ChargingComplete) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)
        {
          Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ChargingComplete);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ChargingComplete) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #190 Decode element RemainingTimeToFullSoC */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(RemainingTimeToFullSoC) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->RemainingTimeToFullSoC));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->RemainingTimeToFullSoCFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(RemainingTimeToBulkSoC) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->RemainingTimeToBulkSoCFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->RemainingTimeToFullSoCFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(RemainingTimeToFullSoC) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #200 Decode optional element RemainingTimeToBulkSoC with missing elements in front */
    if((0 == structPtr->RemainingTimeToFullSoCFlag) && (1 == structPtr->RemainingTimeToBulkSoCFlag))
    {
      /* SE(RemainingTimeToBulkSoC) already decoded */
    #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->RemainingTimeToBulkSoC));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #210 Decode optional element RemainingTimeToBulkSoC without missing elements in front */
    else if(1 == structPtr->RemainingTimeToFullSoCFlag)
    {
      /* #220 Decode element RemainingTimeToBulkSoC */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(RemainingTimeToBulkSoC) */
      {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->RemainingTimeToBulkSoC));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->RemainingTimeToBulkSoCFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->RemainingTimeToBulkSoCFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(RemainingTimeToBulkSoC) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #230 Decode element EVTargetVoltage with missing elements in front */
    if(0 == structPtr->RemainingTimeToBulkSoCFlag)
    {
      /* SE(EVTargetVoltage) already decoded */
    #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVTargetVoltage));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #240 Decode element EVTargetVoltage without missing elements in front */
    else
    {
      /* #250 Decode element EVTargetVoltage */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(EVTargetVoltage) */
      {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVTargetVoltage));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVTargetVoltage) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #260 If CurrentDemandReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(CurrentDemandReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CURRENT_DEMAND_REQ) && (EXI_DECODE_DIN_CURRENT_DEMAND_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_CurrentDemandRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CURRENT_DEMAND_RES) && (EXI_DECODE_DIN_CURRENT_DEMAND_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_CurrentDemandRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_CurrentDemandResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_CurrentDemandResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_CurrentDemandResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_CurrentDemandResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_CURRENT_DEMAND_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element DC_EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVSEStatus) */
    {
      #if (defined(EXI_DECODE_DIN_DC_EVSESTATUS) && (EXI_DECODE_DIN_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_DC_EVSEStatus(DecWsPtr, &(structPtr->DC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVSESTATUS) && (EXI_DECODE_DIN_DC_EVSESTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVSEPresentVoltage */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEPresentVoltage) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEPresentVoltage));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEPresentVoltage) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode element EVSEPresentCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEPresentCurrent) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEPresentCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEPresentCurrent) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode element EVSECurrentLimitAchieved */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSECurrentLimitAchieved) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->EVSECurrentLimitAchieved);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSECurrentLimitAchieved) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #100 Decode element EVSEVoltageLimitAchieved */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEVoltageLimitAchieved) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->EVSEVoltageLimitAchieved);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEVoltageLimitAchieved) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #110 Decode element EVSEPowerLimitAchieved */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEPowerLimitAchieved) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->EVSEPowerLimitAchieved);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEPowerLimitAchieved) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #120 Decode element EVSEMaximumVoltageLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 4, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMaximumVoltageLimit) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumVoltageLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVSEMaximumVoltageLimitFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(EVSEMaximumCurrentLimit) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->EVSEMaximumCurrentLimitFlag = 1;
    }
    else if(exiEventCode == 2) /* SE(EVSEMaximumPowerLimit) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->EVSEMaximumPowerLimitFlag = 1;
    }
    else if(exiEventCode == 3)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVSEMaximumVoltageLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVSEMaximumVoltageLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #130 Decode optional element EVSEMaximumCurrentLimit with missing elements in front */
    if((0 == structPtr->EVSEMaximumVoltageLimitFlag) && (1 == structPtr->EVSEMaximumCurrentLimitFlag))
    {
      /* SE(EVSEMaximumCurrentLimit) already decoded */
    #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumCurrentLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #140 Decode optional element EVSEMaximumCurrentLimit without missing elements in front */
    else if(1 == structPtr->EVSEMaximumVoltageLimitFlag)
    {
      /* #150 Decode element EVSEMaximumCurrentLimit */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(EVSEMaximumCurrentLimit) */
      {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumCurrentLimit));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->EVSEMaximumCurrentLimitFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(EVSEMaximumPowerLimit) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->EVSEMaximumPowerLimitFlag = 1;
      }
      else if(exiEventCode == 2)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->EVSEMaximumCurrentLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVSEMaximumCurrentLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #160 Decode optional element EVSEMaximumPowerLimit with missing elements in front */
    if((0 == structPtr->EVSEMaximumCurrentLimitFlag) && (1 == structPtr->EVSEMaximumPowerLimitFlag))
    {
      /* SE(EVSEMaximumPowerLimit) already decoded */
    #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumPowerLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #170 Decode optional element EVSEMaximumPowerLimit without missing elements in front */
    else if(1 == structPtr->EVSEMaximumCurrentLimitFlag)
    {
      /* #180 Decode element EVSEMaximumPowerLimit */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(EVSEMaximumPowerLimit) */
      {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumPowerLimit));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->EVSEMaximumPowerLimitFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->EVSEMaximumPowerLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVSEMaximumPowerLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #190 If CurrentDemandRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->EVSEMaximumPowerLimitFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)/* EE(CurrentDemandRes) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(CurrentDemandRes) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CURRENT_DEMAND_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CURRENT_DEMAND_RES) && (EXI_DECODE_DIN_CURRENT_DEMAND_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_DC_EVChargeParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_DC_EVCHARGE_PARAMETER) && (EXI_DECODE_DIN_DC_EVCHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_DC_EVChargeParameter( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_DC_EVChargeParameterType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_DC_EVChargeParameterType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_DC_EVChargeParameterType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_DC_EVChargeParameterType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_DC_EVCHARGE_PARAMETER_TYPE);
    /* #40 Decode element DC_EVStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVStatus) */
    {
      #if (defined(EXI_DECODE_DIN_DC_EVSTATUS) && (EXI_DECODE_DIN_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_DC_EVStatus(DecWsPtr, &(structPtr->DC_EVStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVSTATUS) && (EXI_DECODE_DIN_DC_EVSTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DC_EVStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element EVMaximumCurrentLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVMaximumCurrentLimit) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumCurrentLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVMaximumCurrentLimit) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVMaximumPowerLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVMaximumPowerLimit) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumPowerLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVMaximumPowerLimitFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVMaximumPowerLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVMaximumPowerLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #70 Decode element EVMaximumVoltageLimit with missing elements in front */
    if(0 == structPtr->EVMaximumPowerLimitFlag)
    {
      /* SE(EVMaximumVoltageLimit) already decoded */
    #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumVoltageLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #80 Decode element EVMaximumVoltageLimit without missing elements in front */
    else
    {
      /* #90 Decode element EVMaximumVoltageLimit */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(EVMaximumVoltageLimit) */
      {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVMaximumVoltageLimit));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVMaximumVoltageLimit) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #100 Decode element EVEnergyCapacity */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 5, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVEnergyCapacity) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVEnergyCapacity));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVEnergyCapacityFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(EVEnergyRequest) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->EVEnergyRequestFlag = 1;
    }
    else if(exiEventCode == 2) /* SE(FullSOC) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->FullSOCFlag = 1;
    }
    else if(exiEventCode == 3) /* SE(BulkSOC) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->BulkSOCFlag = 1;
    }
    else if(exiEventCode == 4)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVEnergyCapacityFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVEnergyCapacity) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #110 Decode optional element EVEnergyRequest with missing elements in front */
    if((0 == structPtr->EVEnergyCapacityFlag) && (1 == structPtr->EVEnergyRequestFlag))
    {
      /* SE(EVEnergyRequest) already decoded */
    #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVEnergyRequest));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #120 Decode optional element EVEnergyRequest without missing elements in front */
    else if(1 == structPtr->EVEnergyCapacityFlag)
    {
      /* #130 Decode element EVEnergyRequest */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 4, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(EVEnergyRequest) */
      {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVEnergyRequest));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->EVEnergyRequestFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(FullSOC) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->FullSOCFlag = 1;
      }
      else if(exiEventCode == 2) /* SE(BulkSOC) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->BulkSOCFlag = 1;
      }
      else if(exiEventCode == 3)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->EVEnergyRequestFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVEnergyRequest) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #140 Decode optional element FullSOC with missing elements in front */
    if((0 == structPtr->EVEnergyRequestFlag) && (1 == structPtr->FullSOCFlag))
    {
      /* SE(FullSOC) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_BitBufType value = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 7);
        structPtr->FullSOC = (sint8)(value);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #150 Decode optional element FullSOC without missing elements in front */
    else if(1 == structPtr->EVEnergyRequestFlag)
    {
      /* #160 Decode element FullSOC */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(FullSOC) */
      {
        Exi_BitBufType value = 0;
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)
        {
          structPtr->FullSOCFlag = 1;
          Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 7);
          structPtr->FullSOC = (sint8)(value);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else if(exiEventCode == 1) /* SE(BulkSOC) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->BulkSOCFlag = 1;
      }
      else if(exiEventCode == 2)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->FullSOCFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(FullSOC) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #170 Decode optional element BulkSOC with missing elements in front */
    if((0 == structPtr->FullSOCFlag) && (1 == structPtr->BulkSOCFlag))
    {
      /* SE(BulkSOC) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_BitBufType value = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 7);
        structPtr->BulkSOC = (sint8)(value);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #180 Decode optional element BulkSOC without missing elements in front */
    else if(1 == structPtr->FullSOCFlag)
    {
      /* #190 Decode element BulkSOC */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(BulkSOC) */
      {
        Exi_BitBufType value = 0;
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)
        {
          structPtr->BulkSOCFlag = 1;
          Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 7);
          structPtr->BulkSOC = (sint8)(value);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->BulkSOCFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(BulkSOC) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #200 If DC_EVChargeParameter was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->BulkSOCFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)/* EE(DC_EVChargeParameter) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(DC_EVChargeParameter) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_DC_EVCHARGE_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_DC_EVCHARGE_PARAMETER) && (EXI_DECODE_DIN_DC_EVCHARGE_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_DC_EVErrorCode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_DC_EVERROR_CODE) && (EXI_DECODE_DIN_DC_EVERROR_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_DC_EVErrorCode( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_DC_EVErrorCodeType, AUTOMATIC, EXI_APPL_VAR) DC_EVErrorCodePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DC_EVErrorCodePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value DC_EVErrorCode element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 4);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 12)
      {
        /* #40 Store value in output buffer */
        *DC_EVErrorCodePtr = (Exi_DIN_DC_EVErrorCodeType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_DC_EVERROR_CODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_DC_EVERROR_CODE) && (EXI_DECODE_DIN_DC_EVERROR_CODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_DC_EVPowerDeliveryParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_DC_EVPowerDeliveryParameter( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_DC_EVPowerDeliveryParameterType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_DC_EVPowerDeliveryParameterType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_DC_EVPowerDeliveryParameterType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_DC_EVPowerDeliveryParameterType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_DC_EVPOWER_DELIVERY_PARAMETER_TYPE);
    /* #40 Decode element DC_EVStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVStatus) */
    {
      #if (defined(EXI_DECODE_DIN_DC_EVSTATUS) && (EXI_DECODE_DIN_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_DC_EVStatus(DecWsPtr, &(structPtr->DC_EVStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVSTATUS) && (EXI_DECODE_DIN_DC_EVSTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DC_EVStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element BulkChargingComplete */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(BulkChargingComplete) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->BulkChargingCompleteFlag = 1;
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->BulkChargingComplete);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->BulkChargingCompleteFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(BulkChargingComplete) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #60 Decode element ChargingComplete with missing elements in front */
    if(0 == structPtr->BulkChargingCompleteFlag)
    {
      /* SE(ChargingComplete) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ChargingComplete);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #70 Decode element ChargingComplete without missing elements in front */
    else
    {
      /* #80 Decode element ChargingComplete */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(ChargingComplete) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)
        {
          Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ChargingComplete);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ChargingComplete) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 If DC_EVPowerDeliveryParameter was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(DC_EVPowerDeliveryParameter) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_DC_EVSEChargeParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_DC_EVSECHARGE_PARAMETER) && (EXI_DECODE_DIN_DC_EVSECHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_DC_EVSEChargeParameter( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_DC_EVSEChargeParameterType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_DC_EVSEChargeParameterType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_DC_EVSEChargeParameterType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_DC_EVSEChargeParameterType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_DC_EVSECHARGE_PARAMETER_TYPE);
    /* #40 Decode element DC_EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVSEStatus) */
    {
      #if (defined(EXI_DECODE_DIN_DC_EVSESTATUS) && (EXI_DECODE_DIN_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_DC_EVSEStatus(DecWsPtr, &(structPtr->DC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVSESTATUS) && (EXI_DECODE_DIN_DC_EVSESTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element EVSEMaximumCurrentLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMaximumCurrentLimit) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumCurrentLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEMaximumCurrentLimit) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVSEMaximumPowerLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMaximumPowerLimit) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumPowerLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVSEMaximumPowerLimitFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVSEMaximumPowerLimitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVSEMaximumPowerLimit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #70 Decode element EVSEMaximumVoltageLimit with missing elements in front */
    if(0 == structPtr->EVSEMaximumPowerLimitFlag)
    {
      /* SE(EVSEMaximumVoltageLimit) already decoded */
    #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumVoltageLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #80 Decode element EVSEMaximumVoltageLimit without missing elements in front */
    else
    {
      /* #90 Decode element EVSEMaximumVoltageLimit */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(EVSEMaximumVoltageLimit) */
      {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMaximumVoltageLimit));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEMaximumVoltageLimit) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #100 Decode element EVSEMinimumCurrentLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMinimumCurrentLimit) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMinimumCurrentLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEMinimumCurrentLimit) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #110 Decode element EVSEMinimumVoltageLimit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEMinimumVoltageLimit) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEMinimumVoltageLimit));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEMinimumVoltageLimit) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #120 Decode element EVSECurrentRegulationTolerance */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSECurrentRegulationTolerance) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSECurrentRegulationTolerance));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVSECurrentRegulationToleranceFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVSECurrentRegulationToleranceFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVSECurrentRegulationTolerance) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #130 Decode element EVSEPeakCurrentRipple with missing elements in front */
    if(0 == structPtr->EVSECurrentRegulationToleranceFlag)
    {
      /* SE(EVSEPeakCurrentRipple) already decoded */
    #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEPeakCurrentRipple));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    /* #140 Decode element EVSEPeakCurrentRipple without missing elements in front */
    else
    {
      /* #150 Decode element EVSEPeakCurrentRipple */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(EVSEPeakCurrentRipple) */
      {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEPeakCurrentRipple));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEPeakCurrentRipple) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #160 Decode element EVSEEnergyToBeDelivered */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEEnergyToBeDelivered) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEEnergyToBeDelivered));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->EVSEEnergyToBeDeliveredFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVSEEnergyToBeDeliveredFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVSEEnergyToBeDelivered) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #170 If DC_EVSEChargeParameter was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->EVSEEnergyToBeDeliveredFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)/* EE(DC_EVSEChargeParameter) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(DC_EVSEChargeParameter) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_DC_EVSECHARGE_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_DC_EVSECHARGE_PARAMETER) && (EXI_DECODE_DIN_DC_EVSECHARGE_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_DC_EVSEStatusCode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_DC_EVSESTATUS_CODE) && (EXI_DECODE_DIN_DC_EVSESTATUS_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_DC_EVSEStatusCode( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_DC_EVSEStatusCodeType, AUTOMATIC, EXI_APPL_VAR) DC_EVSEStatusCodePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DC_EVSEStatusCodePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value DC_EVSEStatusCode element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 4);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 12)
      {
        /* #40 Store value in output buffer */
        *DC_EVSEStatusCodePtr = (Exi_DIN_DC_EVSEStatusCodeType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_DC_EVSESTATUS_CODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_DC_EVSESTATUS_CODE) && (EXI_DECODE_DIN_DC_EVSESTATUS_CODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_DC_EVSEStatus
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_DC_EVSESTATUS) && (EXI_DECODE_DIN_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_DC_EVSEStatus( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_DC_EVSEStatusType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_DC_EVSEStatusType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_DC_EVSEStatusType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_DC_EVSEStatusType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_DC_EVSESTATUS_TYPE);
    /* #40 Decode element EVSEIsolationStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEIsolationStatus) */
    {
      #if (defined(EXI_DECODE_DIN_ISOLATION_LEVEL) && (EXI_DECODE_DIN_ISOLATION_LEVEL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode isolationLevel as enumeration value */
      Exi_Decode_DIN_isolationLevel(DecWsPtr, &structPtr->EVSEIsolationStatus);
      structPtr->EVSEIsolationStatusFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSESTATUS, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_ISOLATION_LEVEL) && (EXI_DECODE_DIN_ISOLATION_LEVEL == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVSEIsolationStatusFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVSEIsolationStatus) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #60 Decode element EVSEStatusCode with missing elements in front */
    if(0 == structPtr->EVSEIsolationStatusFlag)
    {
      /* SE(EVSEStatusCode) already decoded */
    #if (defined(EXI_DECODE_DIN_DC_EVSESTATUS_CODE) && (EXI_DECODE_DIN_DC_EVSESTATUS_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* Decode enumeration value */
      Exi_Decode_DIN_DC_EVSEStatusCode(DecWsPtr, &structPtr->EVSEStatusCode);
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSESTATUS, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_DC_EVSESTATUS_CODE) && (EXI_DECODE_DIN_DC_EVSESTATUS_CODE == STD_ON)) */
    }
    /* #70 Decode element EVSEStatusCode without missing elements in front */
    else
    {
      /* #80 Decode element EVSEStatusCode */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(EVSEStatusCode) */
      {
      #if (defined(EXI_DECODE_DIN_DC_EVSESTATUS_CODE) && (EXI_DECODE_DIN_DC_EVSESTATUS_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* #90 Decode DC_EVSEStatusCode as enumeration value */
        Exi_Decode_DIN_DC_EVSEStatusCode(DecWsPtr, &structPtr->EVSEStatusCode);
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSESTATUS, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVSESTATUS_CODE) && (EXI_DECODE_DIN_DC_EVSESTATUS_CODE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEStatusCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #100 Decode element NotificationMaxDelay */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(NotificationMaxDelay) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->NotificationMaxDelay);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(NotificationMaxDelay) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #110 Decode element EVSENotification */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSENotification) */
    {
      #if (defined(EXI_DECODE_DIN_EVSENOTIFICATION) && (EXI_DECODE_DIN_EVSENOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #120 Decode EVSENotification as enumeration value */
      Exi_Decode_DIN_EVSENotification(DecWsPtr, &structPtr->EVSENotification);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSESTATUS, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_EVSENOTIFICATION) && (EXI_DECODE_DIN_EVSENOTIFICATION == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSENotification) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #130 If DC_EVSEStatus was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(DC_EVSEStatus) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_DC_EVSESTATUS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_DC_EVSESTATUS) && (EXI_DECODE_DIN_DC_EVSESTATUS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_DC_EVStatus
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_DC_EVSTATUS) && (EXI_DECODE_DIN_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_DC_EVStatus( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_DC_EVStatusType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_DC_EVStatusType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_DC_EVStatusType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_DC_EVStatusType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_DC_EVSTATUS_TYPE);
    /* #40 Decode element EVReady */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVReady) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->EVReady);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVReady) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element EVCabinConditioning */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVCabinConditioning) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->EVCabinConditioningFlag = 1;
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->EVCabinConditioning);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1) /* SE(EVRESSConditioning) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->EVRESSConditioningFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->EVCabinConditioningFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVCabinConditioning) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #60 Decode optional element EVRESSConditioning with missing elements in front */
    if((0 == structPtr->EVCabinConditioningFlag) && (1 == structPtr->EVRESSConditioningFlag))
    {
      /* SE(EVRESSConditioning) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->EVRESSConditioning);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #70 Decode optional element EVRESSConditioning without missing elements in front */
    else if(1 == structPtr->EVCabinConditioningFlag)
    {
      /* #80 Decode element EVRESSConditioning */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(EVRESSConditioning) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)
        {
          structPtr->EVRESSConditioningFlag = 1;
          Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->EVRESSConditioning);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->EVRESSConditioningFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(EVRESSConditioning) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #90 Decode element EVErrorCode with missing elements in front */
    if(0 == structPtr->EVRESSConditioningFlag)
    {
      /* SE(EVErrorCode) already decoded */
    #if (defined(EXI_DECODE_DIN_DC_EVERROR_CODE) && (EXI_DECODE_DIN_DC_EVERROR_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* Decode enumeration value */
      Exi_Decode_DIN_DC_EVErrorCode(DecWsPtr, &structPtr->EVErrorCode);
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSTATUS, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_DC_EVERROR_CODE) && (EXI_DECODE_DIN_DC_EVERROR_CODE == STD_ON)) */
    }
    /* #100 Decode element EVErrorCode without missing elements in front */
    else
    {
      /* #110 Decode element EVErrorCode */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(EVErrorCode) */
      {
      #if (defined(EXI_DECODE_DIN_DC_EVERROR_CODE) && (EXI_DECODE_DIN_DC_EVERROR_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* #120 Decode DC_EVErrorCode as enumeration value */
        Exi_Decode_DIN_DC_EVErrorCode(DecWsPtr, &structPtr->EVErrorCode);
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_DC_EVSTATUS, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVERROR_CODE) && (EXI_DECODE_DIN_DC_EVERROR_CODE == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVErrorCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #130 Decode element EVRESSSOC */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVRESSSOC) */
    {
      Exi_BitBufType value = 0;
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 7);
        structPtr->EVRESSSOC = (sint8)(value);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVRESSSOC) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #140 If DC_EVStatus was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(DC_EVStatus) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_DC_EVSTATUS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_DC_EVSTATUS) && (EXI_DECODE_DIN_DC_EVSTATUS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_EVRequestedEnergyTransfer
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_EVREQUESTED_ENERGY_TRANSFER) && (EXI_DECODE_DIN_EVREQUESTED_ENERGY_TRANSFER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_EVRequestedEnergyTransfer( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_EVRequestedEnergyTransferType, AUTOMATIC, EXI_APPL_VAR) EVRequestedEnergyTransferPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EVRequestedEnergyTransferPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value EVRequestedEnergyTransfer element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 6)
      {
        /* #40 Store value in output buffer */
        *EVRequestedEnergyTransferPtr = (Exi_DIN_EVRequestedEnergyTransferType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_EVREQUESTED_ENERGY_TRANSFER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_EVREQUESTED_ENERGY_TRANSFER) && (EXI_DECODE_DIN_EVREQUESTED_ENERGY_TRANSFER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_EVSENotification
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_EVSENOTIFICATION) && (EXI_DECODE_DIN_EVSENOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_EVSENotification( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_EVSENotificationType, AUTOMATIC, EXI_APPL_VAR) EVSENotificationPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EVSENotificationPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value EVSENotification element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 3)
      {
        /* #40 Store value in output buffer */
        *EVSENotificationPtr = (Exi_DIN_EVSENotificationType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_EVSENOTIFICATION, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_EVSENOTIFICATION) && (EXI_DECODE_DIN_EVSENOTIFICATION == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_EVSEProcessing
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_EVSEPROCESSING) && (EXI_DECODE_DIN_EVSEPROCESSING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_EVSEProcessing( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_EVSEProcessingType, AUTOMATIC, EXI_APPL_VAR) EVSEProcessingPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EVSEProcessingPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value EVSEProcessing element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 2)
      {
        /* #40 Store value in output buffer */
        *EVSEProcessingPtr = (Exi_DIN_EVSEProcessingType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_EVSEPROCESSING, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_EVSEPROCESSING) && (EXI_DECODE_DIN_EVSEPROCESSING == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_EVSESupportedEnergyTransfer
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_EVSESUPPORTED_ENERGY_TRANSFER) && (EXI_DECODE_DIN_EVSESUPPORTED_ENERGY_TRANSFER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_EVSESupportedEnergyTransfer( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_EVSESupportedEnergyTransferType, AUTOMATIC, EXI_APPL_VAR) EVSESupportedEnergyTransferPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EVSESupportedEnergyTransferPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value EVSESupportedEnergyTransfer element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 4);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 10)
      {
        /* #40 Store value in output buffer */
        *EVSESupportedEnergyTransferPtr = (Exi_DIN_EVSESupportedEnergyTransferType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_EVSESUPPORTED_ENERGY_TRANSFER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_EVSESUPPORTED_ENERGY_TRANSFER) && (EXI_DECODE_DIN_EVSESUPPORTED_ENERGY_TRANSFER == STD_ON)) */


/* Encode API for abstract type Exi_DIN_EntryType not required */

/**********************************************************************************************************************
 *  Exi_Decode_DIN_ListOfRootCertificateIDs
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ListOfRootCertificateIDs( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ListOfRootCertificateIDsType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ListOfRootCertificateIDsType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_DIN_rootCertificateIDType) lastRootCertificateID;
  #if (defined(EXI_DECODE_DIN_ROOT_CERTIFICATE_ID) && (EXI_DECODE_DIN_ROOT_CERTIFICATE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_DIN_ROOTCERTIFICATEID - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_DIN_ROOTCERTIFICATEID - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_DIN_ROOT_CERTIFICATE_ID) && (EXI_DECODE_DIN_ROOT_CERTIFICATE_ID == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ListOfRootCertificateIDsType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ListOfRootCertificateIDsType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element RootCertificateID, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(RootCertificateID) */
    {
      #if (defined(EXI_DECODE_DIN_ROOT_CERTIFICATE_ID) && (EXI_DECODE_DIN_ROOT_CERTIFICATE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_rootCertificateID(DecWsPtr, &(structPtr->RootCertificateID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_ROOT_CERTIFICATE_ID) && (EXI_DECODE_DIN_ROOT_CERTIFICATE_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->RootCertificateID->NextRootCertificateIDPtr = (Exi_DIN_rootCertificateIDType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(RootCertificateID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastRootCertificateID = structPtr->RootCertificateID;
    #if (defined(EXI_DECODE_DIN_ROOT_CERTIFICATE_ID) && (EXI_DECODE_DIN_ROOT_CERTIFICATE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_ROOTCERTIFICATEID - 1) > 0)
    /* #40 Decode subsequent occurences of RootCertificateID */
    for(i=0; i<(EXI_MAXOCCURS_DIN_ROOTCERTIFICATEID - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* SE(RootCertificateID) */
      {
        Exi_Decode_DIN_rootCertificateID(DecWsPtr, &(lastRootCertificateID->NextRootCertificateIDPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastRootCertificateID = lastRootCertificateID->NextRootCertificateIDPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(RootCertificateID) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_DIN_ROOTCERTIFICATEID - 1) > 0) */
    #endif /* (defined(EXI_DECODE_DIN_ROOT_CERTIFICATE_ID) && (EXI_DECODE_DIN_ROOT_CERTIFICATE_ID == STD_ON)) */
    lastRootCertificateID->NextRootCertificateIDPtr = (Exi_DIN_rootCertificateIDType*)NULL_PTR;
    exiEventCode = 0;
    #if (defined(EXI_DECODE_DIN_ROOT_CERTIFICATE_ID) && (EXI_DECODE_DIN_ROOT_CERTIFICATE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_ROOTCERTIFICATEID - 1) > 0)
    if((EXI_MAXOCCURS_DIN_ROOTCERTIFICATEID - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_DIN_ROOTCERTIFICATEID - 1) > 0) */
    #endif /* #if (defined(EXI_DECODE_DIN_ROOT_CERTIFICATE_ID) && (EXI_DECODE_DIN_ROOT_CERTIFICATE_ID == STD_ON)) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_MessageHeader
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_MESSAGE_HEADER) && (EXI_DECODE_DIN_MESSAGE_HEADER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_MessageHeader( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_MessageHeaderType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_MessageHeaderType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_MessageHeaderType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_MessageHeaderType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element SessionID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(SessionID) */
    {
      #if (defined(EXI_DECODE_DIN_SESSION_ID) && (EXI_DECODE_DIN_SESSION_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_sessionID(DecWsPtr, &(structPtr->SessionID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_MESSAGE_HEADER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SESSION_ID) && (EXI_DECODE_DIN_SESSION_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(SessionID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element Notification */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(Notification) */
    {
      #if (defined(EXI_DECODE_DIN_NOTIFICATION) && (EXI_DECODE_DIN_NOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_Notification(DecWsPtr, &(structPtr->Notification));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->NotificationFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_MESSAGE_HEADER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_NOTIFICATION) && (EXI_DECODE_DIN_NOTIFICATION == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(Signature) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->SignatureFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->NotificationFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    /* #50 Decode optional element Signature with missing elements in front */
    if((0 == structPtr->NotificationFlag) && (1 == structPtr->SignatureFlag))
    {
      /* SE(Signature) already decoded */
    #if (defined(EXI_DECODE_XMLSIG_SIGNATURE) && (EXI_DECODE_XMLSIG_SIGNATURE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_XMLSIG_Signature(DecWsPtr, &(structPtr->Signature));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_MESSAGE_HEADER, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE) && (EXI_DECODE_XMLSIG_SIGNATURE == STD_ON)) */
    }
    /* #60 Decode optional element Signature without missing elements in front */
    else if(1 == structPtr->NotificationFlag)
    {
      /* #70 Decode element Signature */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(Signature) */
      {
      #if (defined(EXI_DECODE_XMLSIG_SIGNATURE) && (EXI_DECODE_XMLSIG_SIGNATURE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_XMLSIG_Signature(DecWsPtr, &(structPtr->Signature));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->SignatureFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_MESSAGE_HEADER, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_XMLSIG_SIGNATURE) && (EXI_DECODE_XMLSIG_SIGNATURE == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->SignatureFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_MESSAGE_HEADER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_MESSAGE_HEADER) && (EXI_DECODE_DIN_MESSAGE_HEADER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_MeterInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_METER_INFO) && (EXI_DECODE_DIN_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_MeterInfo( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_MeterInfoType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_MeterInfoType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_MeterInfoType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_MeterInfoType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element MeterID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(MeterID) */
    {
      #if (defined(EXI_DECODE_DIN_METER_ID) && (EXI_DECODE_DIN_METER_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_meterID(DecWsPtr, &(structPtr->MeterID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_METER_INFO, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_METER_ID) && (EXI_DECODE_DIN_METER_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(MeterID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element MeterReading */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 5, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(MeterReading) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->MeterReading));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->MeterReadingFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_METER_INFO, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(SigMeterReading) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->SigMeterReadingFlag = 1;
    }
    else if(exiEventCode == 2) /* SE(MeterStatus) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->MeterStatusFlag = 1;
    }
    else if(exiEventCode == 3) /* SE(TMeter) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->TMeterFlag = 1;
    }
    else if(exiEventCode == 4)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->MeterReadingFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(MeterReading) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #50 Decode optional element SigMeterReading with missing elements in front */
    if((0 == structPtr->MeterReadingFlag) && (1 == structPtr->SigMeterReadingFlag))
    {
      /* SE(SigMeterReading) already decoded */
    #if (defined(EXI_DECODE_DIN_SIG_METER_READING) && (EXI_DECODE_DIN_SIG_METER_READING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_sigMeterReading(DecWsPtr, &(structPtr->SigMeterReading));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_METER_INFO, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_SIG_METER_READING) && (EXI_DECODE_DIN_SIG_METER_READING == STD_ON)) */
    }
    /* #60 Decode optional element SigMeterReading without missing elements in front */
    else if(1 == structPtr->MeterReadingFlag)
    {
      /* #70 Decode element SigMeterReading */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 4, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(SigMeterReading) */
      {
      #if (defined(EXI_DECODE_DIN_SIG_METER_READING) && (EXI_DECODE_DIN_SIG_METER_READING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_sigMeterReading(DecWsPtr, &(structPtr->SigMeterReading));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        structPtr->SigMeterReadingFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_METER_INFO, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SIG_METER_READING) && (EXI_DECODE_DIN_SIG_METER_READING == STD_ON)) */
      }
      else if(exiEventCode == 1) /* SE(MeterStatus) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->MeterStatusFlag = 1;
      }
      else if(exiEventCode == 2) /* SE(TMeter) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->TMeterFlag = 1;
      }
      else if(exiEventCode == 3)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->SigMeterReadingFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(SigMeterReading) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #80 Decode optional element MeterStatus with missing elements in front */
    if((0 == structPtr->SigMeterReadingFlag) && (1 == structPtr->MeterStatusFlag))
    {
      /* SE(MeterStatus) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->MeterStatus);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #90 Decode optional element MeterStatus without missing elements in front */
    else if(1 == structPtr->SigMeterReadingFlag)
    {
      /* #100 Decode element MeterStatus */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(MeterStatus) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)
        {
          structPtr->MeterStatusFlag = 1;
          Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->MeterStatus);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else if(exiEventCode == 1) /* SE(TMeter) */
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        structPtr->TMeterFlag = 1;
      }
      else if(exiEventCode == 2)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->MeterStatusFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(MeterStatus) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #110 Decode optional element TMeter with missing elements in front */
    if((0 == structPtr->MeterStatusFlag) && (1 == structPtr->TMeterFlag))
    {
      /* SE(TMeter) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt64(&DecWsPtr->DecWs, &structPtr->TMeter);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #120 Decode optional element TMeter without missing elements in front */
    else if(1 == structPtr->MeterStatusFlag)
    {
      /* #130 Decode element TMeter */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(TMeter) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)
        {
          structPtr->TMeterFlag = 1;
          Exi_VBSDecodeInt64(&DecWsPtr->DecWs, &structPtr->TMeter);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->TMeterFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(TMeter) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_METER_INFO, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_METER_INFO) && (EXI_DECODE_DIN_METER_INFO == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_MeteringReceiptReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_METERING_RECEIPT_REQ) && (EXI_DECODE_DIN_METERING_RECEIPT_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_MeteringReceiptReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_MeteringReceiptReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_MeteringReceiptReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_MeteringReceiptReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_MeteringReceiptReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_METERING_RECEIPT_REQ_TYPE);
    /* #40 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, TRUE, TRUE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->IdFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_METERING_RECEIPT_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element SessionID with missing elements in front */
    if(0 == structPtr->IdFlag)
    {
      /* SE(SessionID) already decoded */
    #if (defined(EXI_DECODE_DIN_SESSION_ID) && (EXI_DECODE_DIN_SESSION_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_sessionID(DecWsPtr, &(structPtr->SessionID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_METERING_RECEIPT_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_SESSION_ID) && (EXI_DECODE_DIN_SESSION_ID == STD_ON)) */
    }
    /* #60 Decode element SessionID without missing elements in front */
    else
    {
      /* #70 Decode element SessionID */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(SessionID) */
      {
      #if (defined(EXI_DECODE_DIN_SESSION_ID) && (EXI_DECODE_DIN_SESSION_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_sessionID(DecWsPtr, &(structPtr->SessionID));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_METERING_RECEIPT_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SESSION_ID) && (EXI_DECODE_DIN_SESSION_ID == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(SessionID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode element SAScheduleTupleID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(SAScheduleTupleID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->SAScheduleTupleIDFlag = 1;
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->SAScheduleTupleID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->SAScheduleTupleIDFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(SAScheduleTupleID) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #90 Decode element MeterInfo with missing elements in front */
    if(0 == structPtr->SAScheduleTupleIDFlag)
    {
      /* SE(MeterInfo) already decoded */
    #if (defined(EXI_DECODE_DIN_METER_INFO) && (EXI_DECODE_DIN_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_MeterInfo(DecWsPtr, &(structPtr->MeterInfo));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_METERING_RECEIPT_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_METER_INFO) && (EXI_DECODE_DIN_METER_INFO == STD_ON)) */
    }
    /* #100 Decode element MeterInfo without missing elements in front */
    else
    {
      /* #110 Decode element MeterInfo */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(MeterInfo) */
      {
      #if (defined(EXI_DECODE_DIN_METER_INFO) && (EXI_DECODE_DIN_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_MeterInfo(DecWsPtr, &(structPtr->MeterInfo));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_METERING_RECEIPT_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_METER_INFO) && (EXI_DECODE_DIN_METER_INFO == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #120 If MeteringReceiptReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(MeteringReceiptReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_METERING_RECEIPT_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_METERING_RECEIPT_REQ) && (EXI_DECODE_DIN_METERING_RECEIPT_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_MeteringReceiptRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_METERING_RECEIPT_RES) && (EXI_DECODE_DIN_METERING_RECEIPT_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_MeteringReceiptRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_MeteringReceiptResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_MeteringReceiptResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_MeteringReceiptResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_MeteringReceiptResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_METERING_RECEIPT_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_METERING_RECEIPT_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element AC_EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(AC_EVSEStatus) */
    {
      #if (defined(EXI_DECODE_DIN_AC_EVSESTATUS) && (EXI_DECODE_DIN_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_AC_EVSEStatus(DecWsPtr, &(structPtr->AC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_METERING_RECEIPT_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_AC_EVSESTATUS) && (EXI_DECODE_DIN_AC_EVSESTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(AC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 If MeteringReceiptRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(MeteringReceiptRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_METERING_RECEIPT_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_METERING_RECEIPT_RES) && (EXI_DECODE_DIN_METERING_RECEIPT_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_Notification
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_NOTIFICATION) && (EXI_DECODE_DIN_NOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_Notification( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_NotificationType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_NotificationType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_NotificationType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_NotificationType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element FaultCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(FaultCode) */
    {
      #if (defined(EXI_DECODE_DIN_FAULT_CODE) && (EXI_DECODE_DIN_FAULT_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #40 Decode faultCode as enumeration value */
      Exi_Decode_DIN_faultCode(DecWsPtr, &structPtr->FaultCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_NOTIFICATION, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_FAULT_CODE) && (EXI_DECODE_DIN_FAULT_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(FaultCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element FaultMsg */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(FaultMsg) */
    {
      #if (defined(EXI_DECODE_DIN_FAULT_MSG) && (EXI_DECODE_DIN_FAULT_MSG == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_faultMsg(DecWsPtr, &(structPtr->FaultMsg));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->FaultMsgFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_NOTIFICATION, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_FAULT_MSG) && (EXI_DECODE_DIN_FAULT_MSG == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->FaultMsgFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(FaultMsg) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_NOTIFICATION, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_NOTIFICATION) && (EXI_DECODE_DIN_NOTIFICATION == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_PMaxScheduleEntry
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_PMaxScheduleEntry( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_PMaxScheduleEntryType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_PMaxScheduleEntryType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_PMaxScheduleEntryType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_PMaxScheduleEntryType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_PMAX_SCHEDULE_ENTRY_TYPE);
    /* #40 Decode substitution group TimeInterval */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* #50 Swtich event code */
    switch(exiEventCode)
    {
    case 0: /* SE(RelativeTimeInterval) */
      /* #60 Element RelativeTimeInterval */
      {
        /* #70 Decode element RelativeTimeInterval */
      #if (defined(EXI_DECODE_DIN_RELATIVE_TIME_INTERVAL) && (EXI_DECODE_DIN_RELATIVE_TIME_INTERVAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->TimeIntervalElementId = EXI_DIN_RELATIVE_TIME_INTERVAL_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_RelativeTimeInterval(DecWsPtr, (Exi_DIN_RelativeTimeIntervalType**)&(structPtr->TimeInterval)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PMAX_SCHEDULE_ENTRY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_RELATIVE_TIME_INTERVAL) && (EXI_DECODE_DIN_RELATIVE_TIME_INTERVAL == STD_ON)) */
      }
    /* case 1: SE(TimeInterval) not required, element is abstract*/
    default:
      /* #80 Unknown element */
      {
        {
          /* #90 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* End of Substitution Group */
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #100 Decode element PMax */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(PMax) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->PMax);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(PMax) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #110 If PMaxScheduleEntry was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(PMaxScheduleEntry) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_PMAX_SCHEDULE_ENTRY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_PMaxSchedule
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_PMAX_SCHEDULE) && (EXI_DECODE_DIN_PMAX_SCHEDULE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_PMaxSchedule( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_PMaxScheduleType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_PMaxScheduleType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_DIN_PMaxScheduleEntryType) lastPMaxScheduleEntry;
  #if (defined(EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_DIN_PMAXSCHEDULEENTRY - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_DIN_PMAXSCHEDULEENTRY - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_PMaxScheduleType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_PMaxScheduleType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element PMaxScheduleID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(PMaxScheduleID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->PMaxScheduleID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(PMaxScheduleID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode multi occurence element PMaxScheduleEntry, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(PMaxScheduleEntry) */
    {
      #if (defined(EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PMaxScheduleEntry(DecWsPtr, &(structPtr->PMaxScheduleEntry));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PMAX_SCHEDULE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->PMaxScheduleEntry->NextPMaxScheduleEntryPtr = (Exi_DIN_PMaxScheduleEntryType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(PMaxScheduleEntry) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastPMaxScheduleEntry = structPtr->PMaxScheduleEntry;
    #if (defined(EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_PMAXSCHEDULEENTRY - 1) > 0)
    /* #50 Decode subsequent occurences of PMaxScheduleEntry */
    for(i=0; i<(EXI_MAXOCCURS_DIN_PMAXSCHEDULEENTRY - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* SE(PMaxScheduleEntry) */
      {
        Exi_Decode_DIN_PMaxScheduleEntry(DecWsPtr, &(lastPMaxScheduleEntry->NextPMaxScheduleEntryPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastPMaxScheduleEntry = lastPMaxScheduleEntry->NextPMaxScheduleEntryPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(PMaxScheduleEntry) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_DIN_PMAXSCHEDULEENTRY - 1) > 0) */
    #endif /* (defined(EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY == STD_ON)) */
    lastPMaxScheduleEntry->NextPMaxScheduleEntryPtr = (Exi_DIN_PMaxScheduleEntryType*)NULL_PTR;
    exiEventCode = 0;
    #if (defined(EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_PMAXSCHEDULEENTRY - 1) > 0)
    if((EXI_MAXOCCURS_DIN_PMAXSCHEDULEENTRY - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_DIN_PMAXSCHEDULEENTRY - 1) > 0) */
    #endif /* #if (defined(EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY) && (EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY == STD_ON)) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_PMAX_SCHEDULE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_PMAX_SCHEDULE) && (EXI_DECODE_DIN_PMAX_SCHEDULE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ParameterSet
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_PARAMETER_SET) && (EXI_DECODE_DIN_PARAMETER_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ParameterSet( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ParameterSetType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ParameterSetType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_DIN_ParameterType) lastParameter;
  #if (defined(EXI_DECODE_DIN_PARAMETER) && (EXI_DECODE_DIN_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_DIN_PARAMETER - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_DIN_PARAMETER - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_DIN_PARAMETER) && (EXI_DECODE_DIN_PARAMETER == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ParameterSetType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ParameterSetType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element ParameterSetID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ParameterSetID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->ParameterSetID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ParameterSetID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode multi occurence element Parameter, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(Parameter) */
    {
      #if (defined(EXI_DECODE_DIN_PARAMETER) && (EXI_DECODE_DIN_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_Parameter(DecWsPtr, &(structPtr->Parameter));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PARAMETER_SET, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PARAMETER) && (EXI_DECODE_DIN_PARAMETER == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->Parameter->NextParameterPtr = (Exi_DIN_ParameterType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(Parameter) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastParameter = structPtr->Parameter;
    #if (defined(EXI_DECODE_DIN_PARAMETER) && (EXI_DECODE_DIN_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_PARAMETER - 1) > 0)
    /* #50 Decode subsequent occurences of Parameter */
    for(i=0; i<(EXI_MAXOCCURS_DIN_PARAMETER - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* SE(Parameter) */
      {
        Exi_Decode_DIN_Parameter(DecWsPtr, &(lastParameter->NextParameterPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastParameter = lastParameter->NextParameterPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(Parameter) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_DIN_PARAMETER - 1) > 0) */
    #endif /* (defined(EXI_DECODE_DIN_PARAMETER) && (EXI_DECODE_DIN_PARAMETER == STD_ON)) */
    lastParameter->NextParameterPtr = (Exi_DIN_ParameterType*)NULL_PTR;
    exiEventCode = 0;
    #if (defined(EXI_DECODE_DIN_PARAMETER) && (EXI_DECODE_DIN_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_PARAMETER - 1) > 0)
    if((EXI_MAXOCCURS_DIN_PARAMETER - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_DIN_PARAMETER - 1) > 0) */
    #endif /* #if (defined(EXI_DECODE_DIN_PARAMETER) && (EXI_DECODE_DIN_PARAMETER == STD_ON)) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_PARAMETER_SET, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_PARAMETER_SET) && (EXI_DECODE_DIN_PARAMETER_SET == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_Parameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_PARAMETER) && (EXI_DECODE_DIN_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_Parameter( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ParameterType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ParameterType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ParameterType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ParameterType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode attribute Name */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, TRUE, 2, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* AT(Name) */
    {
      #if (defined(EXI_DECODE_DIN_ATTRIBUTE_NAME) && (EXI_DECODE_DIN_ATTRIBUTE_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_AttributeName(DecWsPtr, &(structPtr->Name));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_ATTRIBUTE_NAME) && (EXI_DECODE_DIN_ATTRIBUTE_NAME == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode attribute ValueType */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, TRUE, 2, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* AT(ValueType) */
    {
      #if (defined(EXI_DECODE_DIN_ATTRIBUTE_VALUE) && (EXI_DECODE_DIN_ATTRIBUTE_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode AttributeValue as enumeration value */
      Exi_Decode_DIN_AttributeValue(DecWsPtr, &structPtr->ValueType);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_ATTRIBUTE_VALUE) && (EXI_DECODE_DIN_ATTRIBUTE_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* Start of Choice Element */
    /* #60 Allocate buffer for this element */
    structPtr->ChoiceElement = (Exi_DIN_ParameterChoiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ParameterChoiceType), TRUE);
    if (NULL_PTR == (void*)structPtr->ChoiceElement)
    {
      return;
    }
    /* #70 Decode ParameterChoice element */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 6, 3, TRUE, FALSE, TRUE, 2, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(boolValue) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ChoiceElement->ChoiceValue.boolValue);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      structPtr->ChoiceElement->boolValueFlag = 1;
    }
    else if(1 == exiEventCode) /* SE(byteValue) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt8(&DecWsPtr->DecWs, &structPtr->ChoiceElement->ChoiceValue.byteValue);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      structPtr->ChoiceElement->byteValueFlag = 1;
    }
    else if(2 == exiEventCode) /* SE(shortValue) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->ChoiceElement->ChoiceValue.shortValue);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      structPtr->ChoiceElement->shortValueFlag = 1;
    }
    else if(3 == exiEventCode) /* SE(intValue) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt32(&DecWsPtr->DecWs, &structPtr->ChoiceElement->ChoiceValue.intValue);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      structPtr->ChoiceElement->intValueFlag = 1;
    }
    else if(4 == exiEventCode) /* SE(physicalValue) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.physicalValue));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->physicalValueFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else if(5 == exiEventCode) /* SE(stringValue) */
    {
      #if (defined(EXI_DECODE_STRING) && (EXI_DECODE_STRING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_string(DecWsPtr, &(structPtr->ChoiceElement->ChoiceValue.stringValue), EXI_SCHEMA_SET_DIN_TYPE);
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChoiceElement->stringValueFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PARAMETER, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_STRING) && (EXI_DECODE_STRING == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ChoiceElement) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_PARAMETER) && (EXI_DECODE_DIN_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_PaymentDetailsReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_PAYMENT_DETAILS_REQ) && (EXI_DECODE_DIN_PAYMENT_DETAILS_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_PaymentDetailsReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_PaymentDetailsReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_PaymentDetailsReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_PaymentDetailsReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_PaymentDetailsReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_PAYMENT_DETAILS_REQ_TYPE);
    /* #40 Decode element ContractID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ContractID) */
    {
      #if (defined(EXI_DECODE_DIN_CONTRACT_ID) && (EXI_DECODE_DIN_CONTRACT_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_contractID(DecWsPtr, &(structPtr->ContractID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PAYMENT_DETAILS_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_CONTRACT_ID) && (EXI_DECODE_DIN_CONTRACT_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ContractID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element ContractSignatureCertChain */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ContractSignatureCertChain) */
    {
      #if (defined(EXI_DECODE_DIN_CERTIFICATE_CHAIN) && (EXI_DECODE_DIN_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_CertificateChain(DecWsPtr, &(structPtr->ContractSignatureCertChain));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PAYMENT_DETAILS_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_CERTIFICATE_CHAIN) && (EXI_DECODE_DIN_CERTIFICATE_CHAIN == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #60 If PaymentDetailsReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(PaymentDetailsReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_PAYMENT_DETAILS_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_PAYMENT_DETAILS_REQ) && (EXI_DECODE_DIN_PAYMENT_DETAILS_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_PaymentDetailsRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_PAYMENT_DETAILS_RES) && (EXI_DECODE_DIN_PAYMENT_DETAILS_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_PaymentDetailsRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_PaymentDetailsResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_PaymentDetailsResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_PaymentDetailsResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_PaymentDetailsResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_PAYMENT_DETAILS_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PAYMENT_DETAILS_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element GenChallenge */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(GenChallenge) */
    {
      #if (defined(EXI_DECODE_DIN_GEN_CHALLENGE) && (EXI_DECODE_DIN_GEN_CHALLENGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_genChallenge(DecWsPtr, &(structPtr->GenChallenge));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PAYMENT_DETAILS_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_GEN_CHALLENGE) && (EXI_DECODE_DIN_GEN_CHALLENGE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(GenChallenge) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element DateTimeNow */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DateTimeNow) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt64(&DecWsPtr->DecWs, &structPtr->DateTimeNow);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DateTimeNow) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 If PaymentDetailsRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(PaymentDetailsRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_PAYMENT_DETAILS_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_PAYMENT_DETAILS_RES) && (EXI_DECODE_DIN_PAYMENT_DETAILS_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_PaymentOptions
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_PAYMENT_OPTIONS) && (EXI_DECODE_DIN_PAYMENT_OPTIONS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_PaymentOptions( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_PaymentOptionsType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_PaymentOptionsType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  #if ((EXI_MAXOCCURS_DIN_PAYMENTOPTION - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_DIN_PAYMENTOPTION - 1) > 0) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_PaymentOptionsType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_PaymentOptionsType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element PaymentOption, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(PaymentOption) */
    {
      #if (defined(EXI_DECODE_DIN_PAYMENT_OPTION) && (EXI_DECODE_DIN_PAYMENT_OPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #40 Decode first element of paymentOption as a multi occurence enumeration value */
      Exi_Decode_DIN_paymentOption(DecWsPtr, &structPtr->PaymentOption[0]);
      structPtr->PaymentOptionCount = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PAYMENT_OPTIONS, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PAYMENT_OPTION) && (EXI_DECODE_DIN_PAYMENT_OPTION == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(PaymentOption) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    /* #50 Decode subsequent occurences of PaymentOption */
    #if ((EXI_MAXOCCURS_DIN_PAYMENTOPTION - 1) > 0)
    for(i=0; i<(EXI_MAXOCCURS_DIN_PAYMENTOPTION - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* SE(PaymentOption) */
      {
      #if (defined(EXI_DECODE_DIN_PAYMENT_OPTION) && (EXI_DECODE_DIN_PAYMENT_OPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* Decode a subsequent element of a multi occurence enumeration value */
        Exi_Decode_DIN_paymentOption(DecWsPtr, &structPtr->PaymentOption[i + 1]);
        structPtr->PaymentOptionCount++;
      #endif /* (defined(EXI_DECODE_DIN_PAYMENT_OPTION) && (EXI_DECODE_DIN_PAYMENT_OPTION == STD_ON)) */
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(PaymentOption) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_DIN_PAYMENTOPTION - 1) > 0) */
    exiEventCode = 0;
    #if ((EXI_MAXOCCURS_DIN_PAYMENTOPTION - 1) > 0)
    if((EXI_MAXOCCURS_DIN_PAYMENTOPTION - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_DIN_PAYMENTOPTION - 1) > 0) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_PAYMENT_OPTIONS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_PAYMENT_OPTIONS) && (EXI_DECODE_DIN_PAYMENT_OPTIONS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_PhysicalValue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_PhysicalValue( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_PhysicalValueType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_PhysicalValueType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_PhysicalValueType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_PhysicalValueType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element Multiplier */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(Multiplier) */
    {
      Exi_BitBufType value = 0;
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &value, 3);
        structPtr->Multiplier = (sint8)(value - 3);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(Multiplier) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element Unit */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(Unit) */
    {
      #if (defined(EXI_DECODE_DIN_UNIT_SYMBOL) && (EXI_DECODE_DIN_UNIT_SYMBOL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode unitSymbol as enumeration value */
      Exi_Decode_DIN_unitSymbol(DecWsPtr, &structPtr->Unit);
      structPtr->UnitFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PHYSICAL_VALUE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_UNIT_SYMBOL) && (EXI_DECODE_DIN_UNIT_SYMBOL == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->UnitFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(Unit) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #60 Decode element Value with missing elements in front */
    if(0 == structPtr->UnitFlag)
    {
      /* SE(Value) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->Value);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #70 Decode element Value without missing elements in front */
    else
    {
      /* #80 Decode element Value */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(Value) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)
        {
          Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->Value);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(Value) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_PHYSICAL_VALUE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_PowerDeliveryReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_POWER_DELIVERY_REQ) && (EXI_DECODE_DIN_POWER_DELIVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_PowerDeliveryReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_PowerDeliveryReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_PowerDeliveryReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_PowerDeliveryReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_PowerDeliveryReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_POWER_DELIVERY_REQ_TYPE);
    /* #40 Decode element ReadyToChargeState */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ReadyToChargeState) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->ReadyToChargeState);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ReadyToChargeState) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element ChargingProfile */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 3);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 4, 3, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ChargingProfile) */
    {
      #if (defined(EXI_DECODE_DIN_CHARGING_PROFILE) && (EXI_DECODE_DIN_CHARGING_PROFILE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_ChargingProfile(DecWsPtr, &(structPtr->ChargingProfile));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ChargingProfileFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_POWER_DELIVERY_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_CHARGING_PROFILE) && (EXI_DECODE_DIN_CHARGING_PROFILE == STD_ON)) */
    }
    else if(exiEventCode < 3) /* Substitution Group SE(EVPowerDeliveryParameter) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->EVPowerDeliveryParameterFlag = 1;
    }
    else if(exiEventCode == 3)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ChargingProfileFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    /* #60 Decode optional substitution group EVPowerDeliveryParameter with missing elements in front */
    if((0 == structPtr->ChargingProfileFlag) && (1 == structPtr->EVPowerDeliveryParameterFlag))
    {
      /* Start of Substitution Group with missing elements in front */
      /* #70 Decrement event code by the amount of missing elements to get a zero based event code */
      exiEventCode -= 1u; /* Element: ChargingProfile */ /* PRQA S 3382 */ /* MD_Exi_21.1_3382 */
      /* #80 Switch event code */
      switch(exiEventCode)
      {
      case 0: /* SE(DC_EVPowerDeliveryParameter) */
        /* #90 Element DC_EVPowerDeliveryParameter */
        {
          /* #100 Decode element DC_EVPowerDeliveryParameter */
        #if (defined(EXI_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          structPtr->EVPowerDeliveryParameterElementId = EXI_DIN_DC_EVPOWER_DELIVERY_PARAMETER_TYPE;
          Exi_Decode_DIN_DC_EVPowerDeliveryParameter(DecWsPtr, (Exi_DIN_DC_EVPowerDeliveryParameterType**)&(structPtr->EVPowerDeliveryParameter)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          structPtr->EVPowerDeliveryParameterFlag = 1;
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_POWER_DELIVERY_REQ, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) */
        }
      /* case 1: SE(EVPowerDeliveryParameter) not required, element is abstract*/
      default:
        /* #110 Unknown substitution element */
        {
          /* #120 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* #130 Decode optional substitution group EVPowerDeliveryParameter without missing elements in front */
    else if(1 == structPtr->ChargingProfileFlag)
    {
      /* Start of Substitution Group without missing elements in front */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* #140 Swtich event code */
      switch(exiEventCode)
      {
      case 0: /* SE(DC_EVPowerDeliveryParameter) */
        /* #150 Element DC_EVPowerDeliveryParameter */
        {
          /* #160 Decode element DC_EVPowerDeliveryParameter */
        #if (defined(EXI_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          structPtr->EVPowerDeliveryParameterElementId = EXI_DIN_DC_EVPOWER_DELIVERY_PARAMETER_TYPE;
          Exi_Decode_DIN_DC_EVPowerDeliveryParameter(DecWsPtr, (Exi_DIN_DC_EVPowerDeliveryParameterType**)&(structPtr->EVPowerDeliveryParameter)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          structPtr->EVPowerDeliveryParameterFlag = 1;
          break;
        #else
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_POWER_DELIVERY_REQ, EXI_E_INV_PARAM);
          return;
        #endif /* (defined(EXI_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) */
        }
      /* case 1: SE(EVPowerDeliveryParameter) not required, element is abstract*/
      case 2: /* optional element not included */
        /* Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
        break;
      default:
        /* #170 Unknown element */
        {
          {
            /* #180 Set status code to error */
            /* unsupported event code */
            Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
            return;
          }
        }
      }
    /* End of Substitution Group */
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->EVPowerDeliveryParameterFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #190 If PowerDeliveryReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->EVPowerDeliveryParameterFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)/* EE(PowerDeliveryReq) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(PowerDeliveryReq) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_POWER_DELIVERY_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_POWER_DELIVERY_REQ) && (EXI_DECODE_DIN_POWER_DELIVERY_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_PowerDeliveryRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_POWER_DELIVERY_RES) && (EXI_DECODE_DIN_POWER_DELIVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_PowerDeliveryRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_PowerDeliveryResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_PowerDeliveryResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_PowerDeliveryResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_PowerDeliveryResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_POWER_DELIVERY_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_POWER_DELIVERY_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode substitution group EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* #70 Swtich event code */
    switch(exiEventCode)
    {
    case 0: /* SE(AC_EVSEStatus) */
      /* #80 Element AC_EVSEStatus */
      {
        /* #90 Decode element AC_EVSEStatus */
      #if (defined(EXI_DECODE_DIN_AC_EVSESTATUS) && (EXI_DECODE_DIN_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->EVSEStatusElementId = EXI_DIN_AC_EVSESTATUS_TYPE;
        Exi_Decode_DIN_AC_EVSEStatus(DecWsPtr, (Exi_DIN_AC_EVSEStatusType**)&(structPtr->EVSEStatus)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_POWER_DELIVERY_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_AC_EVSESTATUS) && (EXI_DECODE_DIN_AC_EVSESTATUS == STD_ON)) */
      }
    case 1: /* SE(DC_EVSEStatus) */
      /* #100 Element DC_EVSEStatus */
      {
        /* #110 Decode element DC_EVSEStatus */
      #if (defined(EXI_DECODE_DIN_DC_EVSESTATUS) && (EXI_DECODE_DIN_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->EVSEStatusElementId = EXI_DIN_DC_EVSESTATUS_TYPE;
        Exi_Decode_DIN_DC_EVSEStatus(DecWsPtr, (Exi_DIN_DC_EVSEStatusType**)&(structPtr->EVSEStatus)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_POWER_DELIVERY_RES, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVSESTATUS) && (EXI_DECODE_DIN_DC_EVSESTATUS == STD_ON)) */
      }
    /* case 2: SE(EVSEStatus) not required, element is abstract*/
    default:
      /* #120 Unknown element */
      {
        {
          /* #130 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* End of Substitution Group */
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #140 If PowerDeliveryRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(PowerDeliveryRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_POWER_DELIVERY_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_POWER_DELIVERY_RES) && (EXI_DECODE_DIN_POWER_DELIVERY_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_PreChargeReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_PRE_CHARGE_REQ) && (EXI_DECODE_DIN_PRE_CHARGE_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_PreChargeReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_PreChargeReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_PreChargeReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_PreChargeReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_PreChargeReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_PRE_CHARGE_REQ_TYPE);
    /* #40 Decode element DC_EVStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVStatus) */
    {
      #if (defined(EXI_DECODE_DIN_DC_EVSTATUS) && (EXI_DECODE_DIN_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_DC_EVStatus(DecWsPtr, &(structPtr->DC_EVStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PRE_CHARGE_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVSTATUS) && (EXI_DECODE_DIN_DC_EVSTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DC_EVStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element EVTargetVoltage */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVTargetVoltage) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVTargetVoltage));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PRE_CHARGE_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVTargetVoltage) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVTargetCurrent */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVTargetCurrent) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVTargetCurrent));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PRE_CHARGE_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVTargetCurrent) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 If PreChargeReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(PreChargeReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_PRE_CHARGE_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_PRE_CHARGE_REQ) && (EXI_DECODE_DIN_PRE_CHARGE_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_PreChargeRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_PRE_CHARGE_RES) && (EXI_DECODE_DIN_PRE_CHARGE_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_PreChargeRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_PreChargeResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_PreChargeResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_PreChargeResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_PreChargeResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_PRE_CHARGE_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PRE_CHARGE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element DC_EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVSEStatus) */
    {
      #if (defined(EXI_DECODE_DIN_DC_EVSESTATUS) && (EXI_DECODE_DIN_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_DC_EVSEStatus(DecWsPtr, &(structPtr->DC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PRE_CHARGE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVSESTATUS) && (EXI_DECODE_DIN_DC_EVSESTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVSEPresentVoltage */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEPresentVoltage) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEPresentVoltage));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_PRE_CHARGE_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEPresentVoltage) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 If PreChargeRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(PreChargeRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_PRE_CHARGE_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_PRE_CHARGE_RES) && (EXI_DECODE_DIN_PRE_CHARGE_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ProfileEntry
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_PROFILE_ENTRY) && (EXI_DECODE_DIN_PROFILE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ProfileEntry( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ProfileEntryType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ProfileEntryType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ProfileEntryType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ProfileEntryType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element ChargingProfileEntryStart */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ChargingProfileEntryStart) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->ChargingProfileEntryStart);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ChargingProfileEntryStart) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element ChargingProfileEntryMaxPower */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ChargingProfileEntryMaxPower) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->ChargingProfileEntryMaxPower);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ChargingProfileEntryMaxPower) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_PROFILE_ENTRY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_PROFILE_ENTRY) && (EXI_DECODE_DIN_PROFILE_ENTRY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_RelativeTimeInterval
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_RELATIVE_TIME_INTERVAL) && (EXI_DECODE_DIN_RELATIVE_TIME_INTERVAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_RelativeTimeInterval( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_RelativeTimeIntervalType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_RelativeTimeIntervalType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_RelativeTimeIntervalType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_RelativeTimeIntervalType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_RELATIVE_TIME_INTERVAL_TYPE);
    /* #40 Decode element start */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(start) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->start);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(start) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element duration */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(duration) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->durationFlag = 1;
        Exi_VBSDecodeUInt32(&DecWsPtr->DecWs, &structPtr->duration);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->durationFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(duration) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #60 If RelativeTimeInterval was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->durationFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)/* EE(RelativeTimeInterval) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(RelativeTimeInterval) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_RELATIVE_TIME_INTERVAL, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_RELATIVE_TIME_INTERVAL) && (EXI_DECODE_DIN_RELATIVE_TIME_INTERVAL == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_SAScheduleList
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SASCHEDULE_LIST) && (EXI_DECODE_DIN_SASCHEDULE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_SAScheduleList( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_SAScheduleListType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_SAScheduleListType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_DIN_SAScheduleTupleType) lastSAScheduleTuple;
  #if (defined(EXI_DECODE_DIN_SASCHEDULE_TUPLE) && (EXI_DECODE_DIN_SASCHEDULE_TUPLE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_DIN_SASCHEDULETUPLE - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_DIN_SASCHEDULETUPLE - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_DIN_SASCHEDULE_TUPLE) && (EXI_DECODE_DIN_SASCHEDULE_TUPLE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_SAScheduleListType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_SAScheduleListType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_SASCHEDULE_LIST_TYPE);
    /* #40 Decode multi occurence element SAScheduleTuple, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(SAScheduleTuple) */
    {
      #if (defined(EXI_DECODE_DIN_SASCHEDULE_TUPLE) && (EXI_DECODE_DIN_SASCHEDULE_TUPLE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_SAScheduleTuple(DecWsPtr, &(structPtr->SAScheduleTuple));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SASCHEDULE_LIST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SASCHEDULE_TUPLE) && (EXI_DECODE_DIN_SASCHEDULE_TUPLE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->SAScheduleTuple->NextSAScheduleTuplePtr = (Exi_DIN_SAScheduleTupleType*)NULL_PTR;
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastSAScheduleTuple = structPtr->SAScheduleTuple;
    #if (defined(EXI_DECODE_DIN_SASCHEDULE_TUPLE) && (EXI_DECODE_DIN_SASCHEDULE_TUPLE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_SASCHEDULETUPLE - 1) > 0)
    /* #50 Decode subsequent occurences of SAScheduleTuple */
    for(i=0; i<(EXI_MAXOCCURS_DIN_SASCHEDULETUPLE - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* SE(SAScheduleTuple) */
      {
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_SAScheduleTuple(DecWsPtr, &(lastSAScheduleTuple->NextSAScheduleTuplePtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastSAScheduleTuple = lastSAScheduleTuple->NextSAScheduleTuplePtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(SAScheduleTuple) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(SAScheduleTuple) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_DIN_SASCHEDULETUPLE - 1) > 0) */
    #endif /* (defined(EXI_DECODE_DIN_SASCHEDULE_TUPLE) && (EXI_DECODE_DIN_SASCHEDULE_TUPLE == STD_ON)) */
    lastSAScheduleTuple->NextSAScheduleTuplePtr = (Exi_DIN_SAScheduleTupleType*)NULL_PTR;
    exiEventCode = 0;
    #if (defined(EXI_DECODE_DIN_SASCHEDULE_TUPLE) && (EXI_DECODE_DIN_SASCHEDULE_TUPLE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_SASCHEDULETUPLE - 1) > 0)
    if((EXI_MAXOCCURS_DIN_SASCHEDULETUPLE - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_DIN_SASCHEDULETUPLE - 1) > 0) */
    #endif /* #if (defined(EXI_DECODE_DIN_SASCHEDULE_TUPLE) && (EXI_DECODE_DIN_SASCHEDULE_TUPLE == STD_ON)) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #60 If SAScheduleList was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      #if (defined(EXI_DECODE_DIN_SASCHEDULE_TUPLE) && (EXI_DECODE_DIN_SASCHEDULE_TUPLE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_DIN_SASCHEDULETUPLE - 1) > 0)
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* last element unbounded, EE(SAScheduleList) already found, EXI stream end reached */
      }
      else
      {
        /* unexpected state */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
      #endif /* #if ((EXI_MAXOCCURS_DIN_SASCHEDULETUPLE - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_DIN_SASCHEDULE_TUPLE) && (EXI_DECODE_DIN_SASCHEDULE_TUPLE == STD_ON)) */
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SASCHEDULE_LIST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SASCHEDULE_LIST) && (EXI_DECODE_DIN_SASCHEDULE_LIST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_SAScheduleTuple
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SASCHEDULE_TUPLE) && (EXI_DECODE_DIN_SASCHEDULE_TUPLE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_SAScheduleTuple( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_SAScheduleTupleType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_SAScheduleTupleType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_SAScheduleTupleType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_SAScheduleTupleType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element SAScheduleTupleID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(SAScheduleTupleID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->SAScheduleTupleID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(SAScheduleTupleID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element PMaxSchedule */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(PMaxSchedule) */
    {
      #if (defined(EXI_DECODE_DIN_PMAX_SCHEDULE) && (EXI_DECODE_DIN_PMAX_SCHEDULE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_PMaxSchedule(DecWsPtr, &(structPtr->PMaxSchedule));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SASCHEDULE_TUPLE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PMAX_SCHEDULE) && (EXI_DECODE_DIN_PMAX_SCHEDULE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #50 Decode element SalesTariff */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(SalesTariff) */
    {
      #if (defined(EXI_DECODE_DIN_SALES_TARIFF) && (EXI_DECODE_DIN_SALES_TARIFF == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_SalesTariff(DecWsPtr, &(structPtr->SalesTariff));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->SalesTariffFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SASCHEDULE_TUPLE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SALES_TARIFF) && (EXI_DECODE_DIN_SALES_TARIFF == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->SalesTariffFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SASCHEDULE_TUPLE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SASCHEDULE_TUPLE) && (EXI_DECODE_DIN_SASCHEDULE_TUPLE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_SalesTariffEntry
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SALES_TARIFF_ENTRY) && (EXI_DECODE_DIN_SALES_TARIFF_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_SalesTariffEntry( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_SalesTariffEntryType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_SalesTariffEntryType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;
  EXI_P2VAR_IN_FUNCTION(Exi_DIN_ConsumptionCostType) lastConsumptionCost;
  #if (defined(EXI_DECODE_DIN_CONSUMPTION_COST) && (EXI_DECODE_DIN_CONSUMPTION_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_DIN_CONSUMPTIONCOST - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_DIN_CONSUMPTIONCOST - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_DIN_CONSUMPTION_COST) && (EXI_DECODE_DIN_CONSUMPTION_COST == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_SalesTariffEntryType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_SalesTariffEntryType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_SALES_TARIFF_ENTRY_TYPE);
    /* #40 Decode substitution group TimeInterval */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* #50 Swtich event code */
    switch(exiEventCode)
    {
    case 0: /* SE(RelativeTimeInterval) */
      /* #60 Element RelativeTimeInterval */
      {
        /* #70 Decode element RelativeTimeInterval */
      #if (defined(EXI_DECODE_DIN_RELATIVE_TIME_INTERVAL) && (EXI_DECODE_DIN_RELATIVE_TIME_INTERVAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        structPtr->TimeIntervalElementId = EXI_DIN_RELATIVE_TIME_INTERVAL_TYPE;
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_RelativeTimeInterval(DecWsPtr, (Exi_DIN_RelativeTimeIntervalType**)&(structPtr->TimeInterval)); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SALES_TARIFF_ENTRY, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_RELATIVE_TIME_INTERVAL) && (EXI_DECODE_DIN_RELATIVE_TIME_INTERVAL == STD_ON)) */
      }
    /* case 1: SE(TimeInterval) not required, element is abstract*/
    default:
      /* #80 Unknown element */
      {
        {
          /* #90 Set status code to error */
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    /* End of Substitution Group */
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #100 Decode element EPriceLevel */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EPriceLevel) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt8(&DecWsPtr->DecWs, &structPtr->EPriceLevel);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EPriceLevel) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #110 Decode multi occurence element ConsumptionCost, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ConsumptionCost) */
    {
      #if (defined(EXI_DECODE_DIN_CONSUMPTION_COST) && (EXI_DECODE_DIN_CONSUMPTION_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_ConsumptionCost(DecWsPtr, &(structPtr->ConsumptionCost));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ConsumptionCostFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SALES_TARIFF_ENTRY, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_CONSUMPTION_COST) && (EXI_DECODE_DIN_CONSUMPTION_COST == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ConsumptionCostFlag)
    {
      structPtr->ConsumptionCost->NextConsumptionCostPtr = (Exi_DIN_ConsumptionCostType*)NULL_PTR;
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
      if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
      {
        /* an error occured in a previous step -> abort to avoid not required loop */
        return;
      }
      lastConsumptionCost = structPtr->ConsumptionCost;
      #if (defined(EXI_DECODE_DIN_CONSUMPTION_COST) && (EXI_DECODE_DIN_CONSUMPTION_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_DIN_CONSUMPTIONCOST - 1) > 0)
      /* #120 Decode subsequent occurences of ConsumptionCost */
      for(i=0; i<(EXI_MAXOCCURS_DIN_CONSUMPTIONCOST - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
      { /* PRQA S 3201 */ /* MD_MSR_14.1 */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)/* SE(ConsumptionCost) */
        {
          DecWsPtr->DecWs.EERequired = TRUE;
          Exi_Decode_DIN_ConsumptionCost(DecWsPtr, &(lastConsumptionCost->NextConsumptionCostPtr));
          /* Check for errors in subfunctions */
          if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
          {
            return;
          }
          lastConsumptionCost = lastConsumptionCost->NextConsumptionCostPtr;
        }
        else if(1 == exiEventCode)/* reached next Tag */
        {
          DecWsPtr->DecWs.EERequired = FALSE;
          break;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
        if(FALSE == DecWsPtr->DecWs.EERequired)
        {
          /* EE(ConsumptionCost) already decoded */
          DecWsPtr->DecWs.EERequired = TRUE;
          continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
        }
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* check exiEventCode equals EE(ConsumptionCost) */
        if(0 != exiEventCode)
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      #endif /* #if ((EXI_MAXOCCURS_DIN_CONSUMPTIONCOST - 1) > 0) */
      #endif /* (defined(EXI_DECODE_DIN_CONSUMPTION_COST) && (EXI_DECODE_DIN_CONSUMPTION_COST == STD_ON)) */
      lastConsumptionCost->NextConsumptionCostPtr = (Exi_DIN_ConsumptionCostType*)NULL_PTR;
      exiEventCode = 0;
      #if (defined(EXI_DECODE_DIN_CONSUMPTION_COST) && (EXI_DECODE_DIN_CONSUMPTION_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_DIN_CONSUMPTIONCOST - 1) > 0)
      if((EXI_MAXOCCURS_DIN_CONSUMPTIONCOST - 1) == i)
      #endif /* #if ((EXI_MAXOCCURS_DIN_CONSUMPTIONCOST - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_DIN_CONSUMPTION_COST) && (EXI_DECODE_DIN_CONSUMPTION_COST == STD_ON)) */
      {
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
        if(1 == exiEventCode)
        {
          DecWsPtr->DecWs.EERequired = FALSE;
        }
        else
        {
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #130 If SalesTariffEntry was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->ConsumptionCostFlag)
      {
      #if (defined(EXI_DECODE_DIN_CONSUMPTION_COST) && (EXI_DECODE_DIN_CONSUMPTION_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      #if ((EXI_MAXOCCURS_DIN_CONSUMPTIONCOST - 1) > 0)
        if(FALSE == DecWsPtr->DecWs.EERequired)
        {
          /* last element unbounded, EE(SalesTariffEntry) already found, EXI stream end reached */
        }
        else
        {
          /* unexpected state */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        }
      #endif /* #if ((EXI_MAXOCCURS_DIN_CONSUMPTIONCOST - 1) > 0) */
      #endif /* #if (defined(EXI_DECODE_DIN_CONSUMPTION_COST) && (EXI_DECODE_DIN_CONSUMPTION_COST == STD_ON)) */
      }
      else
      {
        /* EE(SalesTariffEntry) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SALES_TARIFF_ENTRY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SALES_TARIFF_ENTRY) && (EXI_DECODE_DIN_SALES_TARIFF_ENTRY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_SalesTariff
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SALES_TARIFF) && (EXI_DECODE_DIN_SALES_TARIFF == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_SalesTariff( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_SalesTariffType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_SalesTariffType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_DIN_SalesTariffEntryType) lastSalesTariffEntry;
  #if (defined(EXI_DECODE_DIN_SALES_TARIFF_ENTRY) && (EXI_DECODE_DIN_SALES_TARIFF_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_DIN_SALESTARIFFENTRY - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_DIN_SALESTARIFFENTRY - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_DIN_SALES_TARIFF_ENTRY) && (EXI_DECODE_DIN_SALES_TARIFF_ENTRY == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_SalesTariffType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_SalesTariffType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode attribute Id */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, TRUE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* AT(Id) */
    {
      #if (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_AttributeId(DecWsPtr, &(structPtr->Id));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SALES_TARIFF, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_ATTRIBUTE_ID) && (EXI_DECODE_DIN_ATTRIBUTE_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element SalesTariffID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(SalesTariffID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->SalesTariffID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(SalesTariffID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 Decode element SalesTariffDescription */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(SalesTariffDescription) */
    {
      #if (defined(EXI_DECODE_DIN_TARIFF_DESCRIPTION) && (EXI_DECODE_DIN_TARIFF_DESCRIPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_tariffDescription(DecWsPtr, &(structPtr->SalesTariffDescription));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->SalesTariffDescriptionFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SALES_TARIFF, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_TARIFF_DESCRIPTION) && (EXI_DECODE_DIN_TARIFF_DESCRIPTION == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->SalesTariffDescriptionFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(SalesTariffDescription) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #60 Decode element NumEPriceLevels with missing elements in front */
    if(0 == structPtr->SalesTariffDescriptionFlag)
    {
      /* SE(NumEPriceLevels) already decoded */
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt8(&DecWsPtr->DecWs, &structPtr->NumEPriceLevels);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #70 Decode element NumEPriceLevels without missing elements in front */
    else
    {
      /* #80 Decode element NumEPriceLevels */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(NumEPriceLevels) */
      {
        /* read start content */
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)
        {
          Exi_VBSDecodeUInt8(&DecWsPtr->DecWs, &structPtr->NumEPriceLevels);
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(NumEPriceLevels) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode multi occurence element SalesTariffEntry, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 1, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(SalesTariffEntry) */
    {
      #if (defined(EXI_DECODE_DIN_SALES_TARIFF_ENTRY) && (EXI_DECODE_DIN_SALES_TARIFF_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_SalesTariffEntry(DecWsPtr, &(structPtr->SalesTariffEntry));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SALES_TARIFF, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SALES_TARIFF_ENTRY) && (EXI_DECODE_DIN_SALES_TARIFF_ENTRY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->SalesTariffEntry->NextSalesTariffEntryPtr = (Exi_DIN_SalesTariffEntryType*)NULL_PTR;
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastSalesTariffEntry = structPtr->SalesTariffEntry;
    #if (defined(EXI_DECODE_DIN_SALES_TARIFF_ENTRY) && (EXI_DECODE_DIN_SALES_TARIFF_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_SALESTARIFFENTRY - 1) > 0)
    /* #100 Decode subsequent occurences of SalesTariffEntry */
    for(i=0; i<(EXI_MAXOCCURS_DIN_SALESTARIFFENTRY - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* SE(SalesTariffEntry) */
      {
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_SalesTariffEntry(DecWsPtr, &(lastSalesTariffEntry->NextSalesTariffEntryPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastSalesTariffEntry = lastSalesTariffEntry->NextSalesTariffEntryPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(SalesTariffEntry) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(SalesTariffEntry) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_DIN_SALESTARIFFENTRY - 1) > 0) */
    #endif /* (defined(EXI_DECODE_DIN_SALES_TARIFF_ENTRY) && (EXI_DECODE_DIN_SALES_TARIFF_ENTRY == STD_ON)) */
    lastSalesTariffEntry->NextSalesTariffEntryPtr = (Exi_DIN_SalesTariffEntryType*)NULL_PTR;
    exiEventCode = 0;
    #if (defined(EXI_DECODE_DIN_SALES_TARIFF_ENTRY) && (EXI_DECODE_DIN_SALES_TARIFF_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_SALESTARIFFENTRY - 1) > 0)
    if((EXI_MAXOCCURS_DIN_SALESTARIFFENTRY - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_DIN_SALESTARIFFENTRY - 1) > 0) */
    #endif /* #if (defined(EXI_DECODE_DIN_SALES_TARIFF_ENTRY) && (EXI_DECODE_DIN_SALES_TARIFF_ENTRY == STD_ON)) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SALES_TARIFF, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SALES_TARIFF) && (EXI_DECODE_DIN_SALES_TARIFF == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_SelectedServiceList
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SELECTED_SERVICE_LIST) && (EXI_DECODE_DIN_SELECTED_SERVICE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_SelectedServiceList( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_SelectedServiceListType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_SelectedServiceListType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_DIN_SelectedServiceType) lastSelectedService;
  #if (defined(EXI_DECODE_DIN_SELECTED_SERVICE) && (EXI_DECODE_DIN_SELECTED_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_DIN_SELECTEDSERVICE - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_DIN_SELECTEDSERVICE - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_DIN_SELECTED_SERVICE) && (EXI_DECODE_DIN_SELECTED_SERVICE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_SelectedServiceListType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_SelectedServiceListType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element SelectedService, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(SelectedService) */
    {
      #if (defined(EXI_DECODE_DIN_SELECTED_SERVICE) && (EXI_DECODE_DIN_SELECTED_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_SelectedService(DecWsPtr, &(structPtr->SelectedService));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SELECTED_SERVICE_LIST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SELECTED_SERVICE) && (EXI_DECODE_DIN_SELECTED_SERVICE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->SelectedService->NextSelectedServicePtr = (Exi_DIN_SelectedServiceType*)NULL_PTR;
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastSelectedService = structPtr->SelectedService;
    #if (defined(EXI_DECODE_DIN_SELECTED_SERVICE) && (EXI_DECODE_DIN_SELECTED_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_SELECTEDSERVICE - 1) > 0)
    /* #40 Decode subsequent occurences of SelectedService */
    for(i=0; i<(EXI_MAXOCCURS_DIN_SELECTEDSERVICE - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* SE(SelectedService) */
      {
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_SelectedService(DecWsPtr, &(lastSelectedService->NextSelectedServicePtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastSelectedService = lastSelectedService->NextSelectedServicePtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(SelectedService) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(SelectedService) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_DIN_SELECTEDSERVICE - 1) > 0) */
    #endif /* (defined(EXI_DECODE_DIN_SELECTED_SERVICE) && (EXI_DECODE_DIN_SELECTED_SERVICE == STD_ON)) */
    lastSelectedService->NextSelectedServicePtr = (Exi_DIN_SelectedServiceType*)NULL_PTR;
    exiEventCode = 0;
    #if (defined(EXI_DECODE_DIN_SELECTED_SERVICE) && (EXI_DECODE_DIN_SELECTED_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_SELECTEDSERVICE - 1) > 0)
    if((EXI_MAXOCCURS_DIN_SELECTEDSERVICE - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_DIN_SELECTEDSERVICE - 1) > 0) */
    #endif /* #if (defined(EXI_DECODE_DIN_SELECTED_SERVICE) && (EXI_DECODE_DIN_SELECTED_SERVICE == STD_ON)) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SELECTED_SERVICE_LIST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SELECTED_SERVICE_LIST) && (EXI_DECODE_DIN_SELECTED_SERVICE_LIST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_SelectedService
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SELECTED_SERVICE) && (EXI_DECODE_DIN_SELECTED_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_SelectedService( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_SelectedServiceType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_SelectedServiceType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_SelectedServiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_SelectedServiceType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element ServiceID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ServiceID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt16(&DecWsPtr->DecWs, &structPtr->ServiceID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ServiceID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element ParameterSetID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ParameterSetID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->ParameterSetIDFlag = 1;
        Exi_VBSDecodeInt16(&DecWsPtr->DecWs, &structPtr->ParameterSetID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ParameterSetIDFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(ParameterSetID) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SELECTED_SERVICE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SELECTED_SERVICE) && (EXI_DECODE_DIN_SELECTED_SERVICE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ServiceCharge
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SERVICE_CHARGE) && (EXI_DECODE_DIN_SERVICE_CHARGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ServiceCharge( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ServiceChargeType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ServiceChargeType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ServiceChargeType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ServiceChargeType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_SERVICE_CHARGE_TYPE);
    /* #40 Decode element ServiceTag */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ServiceTag) */
    {
      #if (defined(EXI_DECODE_DIN_SERVICE_TAG) && (EXI_DECODE_DIN_SERVICE_TAG == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_ServiceTag(DecWsPtr, &(structPtr->ServiceTag));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_CHARGE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_TAG) && (EXI_DECODE_DIN_SERVICE_TAG == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #50 Decode element FreeService */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(FreeService) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->FreeService);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(FreeService) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EnergyTransferType */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EnergyTransferType) */
    {
      #if (defined(EXI_DECODE_DIN_EVSESUPPORTED_ENERGY_TRANSFER) && (EXI_DECODE_DIN_EVSESUPPORTED_ENERGY_TRANSFER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #70 Decode EVSESupportedEnergyTransfer as enumeration value */
      Exi_Decode_DIN_EVSESupportedEnergyTransfer(DecWsPtr, &structPtr->EnergyTransferType);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_CHARGE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_EVSESUPPORTED_ENERGY_TRANSFER) && (EXI_DECODE_DIN_EVSESUPPORTED_ENERGY_TRANSFER == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EnergyTransferType) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 If ServiceCharge was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(ServiceCharge) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SERVICE_CHARGE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SERVICE_CHARGE) && (EXI_DECODE_DIN_SERVICE_CHARGE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ServiceDetailReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SERVICE_DETAIL_REQ) && (EXI_DECODE_DIN_SERVICE_DETAIL_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ServiceDetailReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ServiceDetailReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ServiceDetailReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ServiceDetailReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ServiceDetailReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_SERVICE_DETAIL_REQ_TYPE);
    /* #40 Decode element ServiceID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ServiceID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt16(&DecWsPtr->DecWs, &structPtr->ServiceID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ServiceID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 If ServiceDetailReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(ServiceDetailReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SERVICE_DETAIL_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SERVICE_DETAIL_REQ) && (EXI_DECODE_DIN_SERVICE_DETAIL_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ServiceDetailRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SERVICE_DETAIL_RES) && (EXI_DECODE_DIN_SERVICE_DETAIL_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ServiceDetailRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ServiceDetailResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ServiceDetailResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ServiceDetailResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ServiceDetailResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_SERVICE_DETAIL_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_DETAIL_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element ServiceID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ServiceID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt16(&DecWsPtr->DecWs, &structPtr->ServiceID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ServiceID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element ServiceParameterList */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ServiceParameterList) */
    {
      #if (defined(EXI_DECODE_DIN_SERVICE_PARAMETER_LIST) && (EXI_DECODE_DIN_SERVICE_PARAMETER_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_ServiceParameterList(DecWsPtr, &(structPtr->ServiceParameterList));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ServiceParameterListFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_DETAIL_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_PARAMETER_LIST) && (EXI_DECODE_DIN_SERVICE_PARAMETER_LIST == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ServiceParameterListFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #80 If ServiceDetailRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->ServiceParameterListFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)/* EE(ServiceDetailRes) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(ServiceDetailRes) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SERVICE_DETAIL_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SERVICE_DETAIL_RES) && (EXI_DECODE_DIN_SERVICE_DETAIL_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ServiceDiscoveryReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SERVICE_DISCOVERY_REQ) && (EXI_DECODE_DIN_SERVICE_DISCOVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ServiceDiscoveryReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ServiceDiscoveryReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ServiceDiscoveryReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ServiceDiscoveryReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ServiceDiscoveryReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_SERVICE_DISCOVERY_REQ_TYPE);
    /* #40 Decode element ServiceScope */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 3, 2, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ServiceScope) */
    {
      #if (defined(EXI_DECODE_DIN_SERVICE_SCOPE) && (EXI_DECODE_DIN_SERVICE_SCOPE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_serviceScope(DecWsPtr, &(structPtr->ServiceScope));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ServiceScopeFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_DISCOVERY_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_SCOPE) && (EXI_DECODE_DIN_SERVICE_SCOPE == STD_ON)) */
    }
    else if(exiEventCode == 1) /* SE(ServiceCategory) */
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      structPtr->ServiceCategoryFlag = 1;
    }
    else if(exiEventCode == 2)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ServiceScopeFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(ServiceScope) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #50 Decode optional element ServiceCategory with missing elements in front */
    if((0 == structPtr->ServiceScopeFlag) && (1 == structPtr->ServiceCategoryFlag))
    {
      /* SE(ServiceCategory) already decoded */
    #if (defined(EXI_DECODE_DIN_SERVICE_CATEGORY) && (EXI_DECODE_DIN_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* Decode enumeration value */
      Exi_Decode_DIN_serviceCategory(DecWsPtr, &structPtr->ServiceCategory);
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_DISCOVERY_REQ, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_SERVICE_CATEGORY) && (EXI_DECODE_DIN_SERVICE_CATEGORY == STD_ON)) */
    }
    /* #60 Decode optional element ServiceCategory without missing elements in front */
    else if(1 == structPtr->ServiceScopeFlag)
    {
      /* #70 Decode element ServiceCategory */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(ServiceCategory) */
      {
      #if (defined(EXI_DECODE_DIN_SERVICE_CATEGORY) && (EXI_DECODE_DIN_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* #80 Decode serviceCategory as enumeration value */
        Exi_Decode_DIN_serviceCategory(DecWsPtr, &structPtr->ServiceCategory);
        structPtr->ServiceCategoryFlag = 1;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_DISCOVERY_REQ, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_CATEGORY) && (EXI_DECODE_DIN_SERVICE_CATEGORY == STD_ON)) */
      }
      else if(exiEventCode == 1)
      {
        /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* nothing to do */
    }
    if(1 == structPtr->ServiceCategoryFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(ServiceCategory) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #90 If ServiceDiscoveryReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->ServiceCategoryFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)/* EE(ServiceDiscoveryReq) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(ServiceDiscoveryReq) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SERVICE_DISCOVERY_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SERVICE_DISCOVERY_REQ) && (EXI_DECODE_DIN_SERVICE_DISCOVERY_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ServiceDiscoveryRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SERVICE_DISCOVERY_RES) && (EXI_DECODE_DIN_SERVICE_DISCOVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ServiceDiscoveryRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ServiceDiscoveryResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ServiceDiscoveryResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ServiceDiscoveryResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ServiceDiscoveryResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_SERVICE_DISCOVERY_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_DISCOVERY_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element PaymentOptions */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(PaymentOptions) */
    {
      #if (defined(EXI_DECODE_DIN_PAYMENT_OPTIONS) && (EXI_DECODE_DIN_PAYMENT_OPTIONS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_PaymentOptions(DecWsPtr, &(structPtr->PaymentOptions));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_DISCOVERY_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PAYMENT_OPTIONS) && (EXI_DECODE_DIN_PAYMENT_OPTIONS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #70 Decode element ChargeService */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ChargeService) */
    {
      #if (defined(EXI_DECODE_DIN_SERVICE_CHARGE) && (EXI_DECODE_DIN_SERVICE_CHARGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_ServiceCharge(DecWsPtr, &(structPtr->ChargeService));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_DISCOVERY_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_CHARGE) && (EXI_DECODE_DIN_SERVICE_CHARGE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ChargeService) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 Decode element ServiceList */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ServiceList) */
    {
      #if (defined(EXI_DECODE_DIN_SERVICE_TAG_LIST) && (EXI_DECODE_DIN_SERVICE_TAG_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_ServiceTagList(DecWsPtr, &(structPtr->ServiceList));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ServiceListFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_DISCOVERY_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_TAG_LIST) && (EXI_DECODE_DIN_SERVICE_TAG_LIST == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ServiceListFlag)
    {
      if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
      {
        /* Error, invalid event code for EE */
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #90 If ServiceDiscoveryRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->ServiceListFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)/* EE(ServiceDiscoveryRes) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(ServiceDiscoveryRes) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SERVICE_DISCOVERY_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SERVICE_DISCOVERY_RES) && (EXI_DECODE_DIN_SERVICE_DISCOVERY_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ServiceParameterList
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SERVICE_PARAMETER_LIST) && (EXI_DECODE_DIN_SERVICE_PARAMETER_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ServiceParameterList( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ServiceParameterListType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ServiceParameterListType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_DIN_ParameterSetType) lastParameterSet;
  #if (defined(EXI_DECODE_DIN_PARAMETER_SET) && (EXI_DECODE_DIN_PARAMETER_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_DIN_PARAMETERSET - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_DIN_PARAMETERSET - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_DIN_PARAMETER_SET) && (EXI_DECODE_DIN_PARAMETER_SET == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ServiceParameterListType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ServiceParameterListType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element ParameterSet, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ParameterSet) */
    {
      #if (defined(EXI_DECODE_DIN_PARAMETER_SET) && (EXI_DECODE_DIN_PARAMETER_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_ParameterSet(DecWsPtr, &(structPtr->ParameterSet));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_PARAMETER_LIST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PARAMETER_SET) && (EXI_DECODE_DIN_PARAMETER_SET == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->ParameterSet->NextParameterSetPtr = (Exi_DIN_ParameterSetType*)NULL_PTR;
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastParameterSet = structPtr->ParameterSet;
    #if (defined(EXI_DECODE_DIN_PARAMETER_SET) && (EXI_DECODE_DIN_PARAMETER_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_PARAMETERSET - 1) > 0)
    /* #40 Decode subsequent occurences of ParameterSet */
    for(i=0; i<(EXI_MAXOCCURS_DIN_PARAMETERSET - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* SE(ParameterSet) */
      {
        DecWsPtr->DecWs.EERequired = TRUE;
        Exi_Decode_DIN_ParameterSet(DecWsPtr, &(lastParameterSet->NextParameterSetPtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastParameterSet = lastParameterSet->NextParameterSetPtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      if(FALSE == DecWsPtr->DecWs.EERequired)
      {
        /* EE(ParameterSet) already decoded */
        DecWsPtr->DecWs.EERequired = TRUE;
        continue; /* PRQA S 0770 */ /* MD_Exi_14.5 */
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(ParameterSet) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_DIN_PARAMETERSET - 1) > 0) */
    #endif /* (defined(EXI_DECODE_DIN_PARAMETER_SET) && (EXI_DECODE_DIN_PARAMETER_SET == STD_ON)) */
    lastParameterSet->NextParameterSetPtr = (Exi_DIN_ParameterSetType*)NULL_PTR;
    exiEventCode = 0;
    #if (defined(EXI_DECODE_DIN_PARAMETER_SET) && (EXI_DECODE_DIN_PARAMETER_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_PARAMETERSET - 1) > 0)
    if((EXI_MAXOCCURS_DIN_PARAMETERSET - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_DIN_PARAMETERSET - 1) > 0) */
    #endif /* #if (defined(EXI_DECODE_DIN_PARAMETER_SET) && (EXI_DECODE_DIN_PARAMETER_SET == STD_ON)) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SERVICE_PARAMETER_LIST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SERVICE_PARAMETER_LIST) && (EXI_DECODE_DIN_SERVICE_PARAMETER_LIST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ServicePaymentSelectionReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_REQ) && (EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ServicePaymentSelectionReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ServicePaymentSelectionReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ServicePaymentSelectionReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ServicePaymentSelectionReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ServicePaymentSelectionReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_SERVICE_PAYMENT_SELECTION_REQ_TYPE);
    /* #40 Decode element SelectedPaymentOption */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(SelectedPaymentOption) */
    {
      #if (defined(EXI_DECODE_DIN_PAYMENT_OPTION) && (EXI_DECODE_DIN_PAYMENT_OPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode paymentOption as enumeration value */
      Exi_Decode_DIN_paymentOption(DecWsPtr, &structPtr->SelectedPaymentOption);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_PAYMENT_SELECTION_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PAYMENT_OPTION) && (EXI_DECODE_DIN_PAYMENT_OPTION == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(SelectedPaymentOption) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element SelectedServiceList */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(SelectedServiceList) */
    {
      #if (defined(EXI_DECODE_DIN_SELECTED_SERVICE_LIST) && (EXI_DECODE_DIN_SELECTED_SERVICE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_SelectedServiceList(DecWsPtr, &(structPtr->SelectedServiceList));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_PAYMENT_SELECTION_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SELECTED_SERVICE_LIST) && (EXI_DECODE_DIN_SELECTED_SERVICE_LIST == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #70 If ServicePaymentSelectionReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(ServicePaymentSelectionReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SERVICE_PAYMENT_SELECTION_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_REQ) && (EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ServicePaymentSelectionRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_RES) && (EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ServicePaymentSelectionRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ServicePaymentSelectionResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ServicePaymentSelectionResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ServicePaymentSelectionResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ServicePaymentSelectionResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_SERVICE_PAYMENT_SELECTION_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_PAYMENT_SELECTION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 If ServicePaymentSelectionRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(ServicePaymentSelectionRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SERVICE_PAYMENT_SELECTION_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_RES) && (EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ServiceTagList
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SERVICE_TAG_LIST) && (EXI_DECODE_DIN_SERVICE_TAG_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ServiceTagList( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ServiceTagListType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ServiceTagListType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_DIN_ServiceType) lastService;
  #if (defined(EXI_DECODE_DIN_SERVICE) && (EXI_DECODE_DIN_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_DIN_SERVICE - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_DIN_SERVICE - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_DIN_SERVICE) && (EXI_DECODE_DIN_SERVICE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ServiceTagListType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ServiceTagListType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element Service, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(Service) */
    {
      #if (defined(EXI_DECODE_DIN_SERVICE) && (EXI_DECODE_DIN_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_Service(DecWsPtr, &(structPtr->Service));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_TAG_LIST, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE) && (EXI_DECODE_DIN_SERVICE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->Service->NextServicePtr = (Exi_DIN_ServiceType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(Service) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastService = structPtr->Service;
    #if (defined(EXI_DECODE_DIN_SERVICE) && (EXI_DECODE_DIN_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_SERVICE - 1) > 0)
    /* #40 Decode subsequent occurences of Service */
    for(i=0; i<(EXI_MAXOCCURS_DIN_SERVICE - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* SE(Service) */
      {
        Exi_Decode_DIN_Service(DecWsPtr, &(lastService->NextServicePtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastService = lastService->NextServicePtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(Service) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_DIN_SERVICE - 1) > 0) */
    #endif /* (defined(EXI_DECODE_DIN_SERVICE) && (EXI_DECODE_DIN_SERVICE == STD_ON)) */
    lastService->NextServicePtr = (Exi_DIN_ServiceType*)NULL_PTR;
    exiEventCode = 0;
    #if (defined(EXI_DECODE_DIN_SERVICE) && (EXI_DECODE_DIN_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_SERVICE - 1) > 0)
    if((EXI_MAXOCCURS_DIN_SERVICE - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_DIN_SERVICE - 1) > 0) */
    #endif /* #if (defined(EXI_DECODE_DIN_SERVICE) && (EXI_DECODE_DIN_SERVICE == STD_ON)) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SERVICE_TAG_LIST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SERVICE_TAG_LIST) && (EXI_DECODE_DIN_SERVICE_TAG_LIST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_ServiceTag
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SERVICE_TAG) && (EXI_DECODE_DIN_SERVICE_TAG == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_ServiceTag( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ServiceTagType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ServiceTagType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ServiceTagType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ServiceTagType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element ServiceID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ServiceID) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeUInt16(&DecWsPtr->DecWs, &structPtr->ServiceID);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ServiceID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #40 Decode element ServiceName */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ServiceName) */
    {
      #if (defined(EXI_DECODE_DIN_SERVICE_NAME) && (EXI_DECODE_DIN_SERVICE_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_serviceName(DecWsPtr, &(structPtr->ServiceName));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ServiceNameFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_TAG, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_NAME) && (EXI_DECODE_DIN_SERVICE_NAME == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ServiceNameFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(ServiceName) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    /* #50 Decode element ServiceCategory with missing elements in front */
    if(0 == structPtr->ServiceNameFlag)
    {
      /* SE(ServiceCategory) already decoded */
    #if (defined(EXI_DECODE_DIN_SERVICE_CATEGORY) && (EXI_DECODE_DIN_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* Decode enumeration value */
      Exi_Decode_DIN_serviceCategory(DecWsPtr, &structPtr->ServiceCategory);
    #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_TAG, EXI_E_INV_PARAM);
      return;
    #endif /* (defined(EXI_DECODE_DIN_SERVICE_CATEGORY) && (EXI_DECODE_DIN_SERVICE_CATEGORY == STD_ON)) */
    }
    /* #60 Decode element ServiceCategory without missing elements in front */
    else
    {
      /* #70 Decode element ServiceCategory */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode) /* SE(ServiceCategory) */
      {
      #if (defined(EXI_DECODE_DIN_SERVICE_CATEGORY) && (EXI_DECODE_DIN_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* #80 Decode serviceCategory as enumeration value */
        Exi_Decode_DIN_serviceCategory(DecWsPtr, &structPtr->ServiceCategory);
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_TAG, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_CATEGORY) && (EXI_DECODE_DIN_SERVICE_CATEGORY == STD_ON)) */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ServiceCategory) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #90 Decode element ServiceScope */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ServiceScope) */
    {
      #if (defined(EXI_DECODE_DIN_SERVICE_SCOPE) && (EXI_DECODE_DIN_SERVICE_SCOPE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_serviceScope(DecWsPtr, &(structPtr->ServiceScope));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      structPtr->ServiceScopeFlag = 1;
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE_TAG, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_SCOPE) && (EXI_DECODE_DIN_SERVICE_SCOPE == STD_ON)) */
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->ServiceScopeFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(ServiceScope) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SERVICE_TAG, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SERVICE_TAG) && (EXI_DECODE_DIN_SERVICE_TAG == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_Service
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SERVICE) && (EXI_DECODE_DIN_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_Service( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_ServiceType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_ServiceType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_ServiceType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_ServiceType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode element ServiceTag */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ServiceTag) */
    {
      #if (defined(EXI_DECODE_DIN_SERVICE_TAG) && (EXI_DECODE_DIN_SERVICE_TAG == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_ServiceTag(DecWsPtr, &(structPtr->ServiceTag));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SERVICE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_SERVICE_TAG) && (EXI_DECODE_DIN_SERVICE_TAG == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #40 Decode element FreeService */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(FreeService) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        Exi_VBSDecodeBool(&DecWsPtr->DecWs, &structPtr->FreeService);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(FreeService) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SERVICE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SERVICE) && (EXI_DECODE_DIN_SERVICE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_SessionSetupReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SESSION_SETUP_REQ) && (EXI_DECODE_DIN_SESSION_SETUP_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_SessionSetupReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_SessionSetupReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_SessionSetupReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_SessionSetupReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_SessionSetupReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_SESSION_SETUP_REQ_TYPE);
    /* #40 Decode element EVCCID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVCCID) */
    {
      #if (defined(EXI_DECODE_DIN_EVCC_ID) && (EXI_DECODE_DIN_EVCC_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_evccID(DecWsPtr, &(structPtr->EVCCID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SESSION_SETUP_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_EVCC_ID) && (EXI_DECODE_DIN_EVCC_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVCCID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 If SessionSetupReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(SessionSetupReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SESSION_SETUP_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SESSION_SETUP_REQ) && (EXI_DECODE_DIN_SESSION_SETUP_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_SessionSetupRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SESSION_SETUP_RES) && (EXI_DECODE_DIN_SESSION_SETUP_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_SessionSetupRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_SessionSetupResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_SessionSetupResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_SessionSetupResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_SessionSetupResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_SESSION_SETUP_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SESSION_SETUP_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element EVSEID */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEID) */
    {
      #if (defined(EXI_DECODE_DIN_EVSE_ID) && (EXI_DECODE_DIN_EVSE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_evseID(DecWsPtr, &(structPtr->EVSEID));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SESSION_SETUP_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_EVSE_ID) && (EXI_DECODE_DIN_EVSE_ID == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEID) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element DateTimeNow */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, TRUE, TRUE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DateTimeNow) */
    {
      /* read start content */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, TRUE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)
      {
        structPtr->DateTimeNowFlag = 1;
        Exi_VBSDecodeInt64(&DecWsPtr->DecWs, &structPtr->DateTimeNow);
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else if(exiEventCode == 1)
    {
      /* Optional element not included. Setting Flag to 0 is not requiered because of clear memory call at the beginning of this function */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(1 == structPtr->DateTimeNowFlag)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      /* check exiEventCode equals EE(DateTimeNow) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    else
    {
      /* EE encoding for calling function not required */
      DecWsPtr->DecWs.EERequired = FALSE;
    }
    /* #80 If SessionSetupRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      if(1 == structPtr->DateTimeNowFlag)
      {
        exiEventCode = 0;
        Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
        /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
        Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
        if(0 == exiEventCode)/* EE(SessionSetupRes) */
        {
          /* EXI stream end reached */
        }
        else
        {
          /* unsupported event code */
          Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      }
      else
      {
        /* EE(SessionSetupRes) already found, EXI stream end reached */
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SESSION_SETUP_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SESSION_SETUP_RES) && (EXI_DECODE_DIN_SESSION_SETUP_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_SessionStopRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SESSION_STOP_RES) && (EXI_DECODE_DIN_SESSION_STOP_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_SessionStopRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_SessionStopResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_SessionStopResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_SessionStopResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_SessionStopResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_SESSION_STOP_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SESSION_STOP_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 If SessionStopRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(SessionStopRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SESSION_STOP_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SESSION_STOP_RES) && (EXI_DECODE_DIN_SESSION_STOP_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_SubCertificates
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SUB_CERTIFICATES) && (EXI_DECODE_DIN_SUB_CERTIFICATES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_SubCertificates( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_SubCertificatesType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_SubCertificatesType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  EXI_P2VAR_IN_FUNCTION(Exi_DIN_certificateType) lastCertificate;
  #if (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  #if ((EXI_MAXOCCURS_DIN_CERTIFICATE - 1) > 0)
  uint16_least i;
  #endif /* #if ((EXI_MAXOCCURS_DIN_CERTIFICATE - 1) > 0) */
  #endif /* #if (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_SubCertificatesType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_SubCertificatesType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 Decode multi occurence element Certificate, decode first occurence */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(Certificate) */
    {
      #if (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_certificate(DecWsPtr, &(structPtr->Certificate));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_SUB_CERTIFICATES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    structPtr->Certificate->NextCertificatePtr = (Exi_DIN_certificateType*)NULL_PTR;
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(Certificate) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(EXI_E_OK != DecWsPtr->DecWs.StatusCode)
    {
      /* an error occured in a previous step -> abort to avoid not required loop */
      return;
    }
    lastCertificate = structPtr->Certificate;
    #if (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_CERTIFICATE - 1) > 0)
    /* #40 Decode subsequent occurences of Certificate */
    for(i=0; i<(EXI_MAXOCCURS_DIN_CERTIFICATE - 1); i++) /* PRQA S 3356,3359 */ /* MD_Exi_13.7 */
    { /* PRQA S 3201 */ /* MD_MSR_14.1 */
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 2, 2, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* SE(Certificate) */
      {
        Exi_Decode_DIN_certificate(DecWsPtr, &(lastCertificate->NextCertificatePtr));
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        lastCertificate = lastCertificate->NextCertificatePtr;
      }
      else if(1 == exiEventCode)/* reached next Tag */
      {
        DecWsPtr->DecWs.EERequired = FALSE;
        break;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* check exiEventCode equals EE(Certificate) */
      if(0 != exiEventCode)
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
    #endif /* #if ((EXI_MAXOCCURS_DIN_CERTIFICATE - 1) > 0) */
    #endif /* (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) */
    lastCertificate->NextCertificatePtr = (Exi_DIN_certificateType*)NULL_PTR;
    exiEventCode = 0;
    #if (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    #if ((EXI_MAXOCCURS_DIN_CERTIFICATE - 1) > 0)
    if((EXI_MAXOCCURS_DIN_CERTIFICATE - 1) == i)
    #endif /* #if ((EXI_MAXOCCURS_DIN_CERTIFICATE - 1) > 0) */
    #endif /* #if (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) */
    {
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      if(1 == exiEventCode)
      {
        DecWsPtr->DecWs.EERequired = FALSE;
      }
      else
      {
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SUB_CERTIFICATES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SUB_CERTIFICATES) && (EXI_DECODE_DIN_SUB_CERTIFICATES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_V2G_Message
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_V2G_MESSAGE) && (EXI_DECODE_DIN_V2G_MESSAGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_V2G_Message( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_V2G_MessageType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_V2G_MessageType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_V2G_MessageType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_V2G_MessageType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_V2G_MESSAGE_TYPE);
    /* #40 Decode element Header */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(Header) */
    {
      #if (defined(EXI_DECODE_DIN_MESSAGE_HEADER) && (EXI_DECODE_DIN_MESSAGE_HEADER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_MessageHeader(DecWsPtr, &(structPtr->Header));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_V2G_MESSAGE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_MESSAGE_HEADER) && (EXI_DECODE_DIN_MESSAGE_HEADER == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #50 Decode element Body */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(Body) */
    {
      #if (defined(EXI_DECODE_DIN_BODY) && (EXI_DECODE_DIN_BODY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      DecWsPtr->DecWs.EERequired = TRUE;
      Exi_Decode_DIN_Body(DecWsPtr, &(structPtr->Body));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_V2G_MESSAGE, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_BODY) && (EXI_DECODE_DIN_BODY == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    if(E_NOT_OK == Exi_VBSDecodeCheckAndDecodeEE(DecWsPtr, 1, 1, EXI_SCHEMA_SET_DIN_TYPE))
    {
      /* Error, invalid event code for EE */
      return;
    }
    /* #60 If V2G_Message was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(V2G_Message) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_V2G_MESSAGE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_V2G_MESSAGE) && (EXI_DECODE_DIN_V2G_MESSAGE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_WeldingDetectionReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_WELDING_DETECTION_REQ) && (EXI_DECODE_DIN_WELDING_DETECTION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_WeldingDetectionReq( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_WeldingDetectionReqType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_WeldingDetectionReqType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_WeldingDetectionReqType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_WeldingDetectionReqType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_WELDING_DETECTION_REQ_TYPE);
    /* #40 Decode element DC_EVStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVStatus) */
    {
      #if (defined(EXI_DECODE_DIN_DC_EVSTATUS) && (EXI_DECODE_DIN_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_DC_EVStatus(DecWsPtr, &(structPtr->DC_EVStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_WELDING_DETECTION_REQ, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVSTATUS) && (EXI_DECODE_DIN_DC_EVSTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DC_EVStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #50 If WeldingDetectionReq was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(WeldingDetectionReq) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_WELDING_DETECTION_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_WELDING_DETECTION_REQ) && (EXI_DECODE_DIN_WELDING_DETECTION_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_WeldingDetectionRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_WELDING_DETECTION_RES) && (EXI_DECODE_DIN_WELDING_DETECTION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_WeldingDetectionRes( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_WeldingDetectionResType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_WeldingDetectionResType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;
  boolean isFirstElement = FALSE;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_WeldingDetectionResType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_WeldingDetectionResType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    /* #30 If this is the first element in the stream, set the root element ID */
    Exi_VBSDecodeCheckForFirstElement(&DecWsPtr->OutputData, &isFirstElement, EXI_DIN_WELDING_DETECTION_RES_TYPE);
    /* #40 Decode element ResponseCode */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(ResponseCode) */
    {
      #if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #50 Decode responseCode as enumeration value */
      Exi_Decode_DIN_responseCode(DecWsPtr, &structPtr->ResponseCode);
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_WELDING_DETECTION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(ResponseCode) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #60 Decode element DC_EVSEStatus */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(DC_EVSEStatus) */
    {
      #if (defined(EXI_DECODE_DIN_DC_EVSESTATUS) && (EXI_DECODE_DIN_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_DC_EVSEStatus(DecWsPtr, &(structPtr->DC_EVSEStatus));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_WELDING_DETECTION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_DC_EVSESTATUS) && (EXI_DECODE_DIN_DC_EVSESTATUS == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(DC_EVSEStatus) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #70 Decode element EVSEPresentVoltage */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode) /* SE(EVSEPresentVoltage) */
    {
      #if (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Decode_DIN_PhysicalValue(DecWsPtr, &(structPtr->EVSEPresentVoltage));
      /* Check for errors in subfunctions */
      if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
      {
        return;
      }
      #else
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_DECODE_DIN_WELDING_DETECTION_RES, EXI_E_INV_PARAM);
      return;
      #endif /* (defined(EXI_DECODE_DIN_PHYSICAL_VALUE) && (EXI_DECODE_DIN_PHYSICAL_VALUE == STD_ON)) */
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    /* check exiEventCode equals EE(EVSEPresentVoltage) */
    if(0 != exiEventCode)
    {
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* #80 If WeldingDetectionRes was the first element, decode end element tag */
    if(TRUE == isFirstElement)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
      Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, FALSE, FALSE, FALSE, 0, EXI_SCHEMA_SET_DIN_TYPE);
      if(0 == exiEventCode)/* EE(WeldingDetectionRes) */
      {
      /* EXI stream end reached */
      }
      else
      {
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_WELDING_DETECTION_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_WELDING_DETECTION_RES) && (EXI_DECODE_DIN_WELDING_DETECTION_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_certificate
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_certificate( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_certificateType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_certificateType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_certificateType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_certificateType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode certificate as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CERTIFICATE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CERTIFICATE) && (EXI_DECODE_DIN_CERTIFICATE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_contractID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_CONTRACT_ID) && (EXI_DECODE_DIN_CONTRACT_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_contractID( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_contractIDType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_contractIDType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_contractIDType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_contractIDType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode contractID as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_CONTRACT_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_CONTRACT_ID) && (EXI_DECODE_DIN_CONTRACT_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_costKind
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_COST_KIND) && (EXI_DECODE_DIN_COST_KIND == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_costKind( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_costKindType, AUTOMATIC, EXI_APPL_VAR) costKindPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (costKindPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value costKind element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 3)
      {
        /* #40 Store value in output buffer */
        *costKindPtr = (Exi_DIN_costKindType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_COST_KIND, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_COST_KIND) && (EXI_DECODE_DIN_COST_KIND == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_dHParams
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_D_HPARAMS) && (EXI_DECODE_DIN_D_HPARAMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_dHParams( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_dHParamsType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_dHParamsType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_dHParamsType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_dHParamsType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode dHParams as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_D_HPARAMS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_D_HPARAMS) && (EXI_DECODE_DIN_D_HPARAMS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_evccID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_EVCC_ID) && (EXI_DECODE_DIN_EVCC_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_evccID( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_evccIDType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_evccIDType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_evccIDType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_evccIDType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode evccID as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_EVCC_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_EVCC_ID) && (EXI_DECODE_DIN_EVCC_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_evseID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_EVSE_ID) && (EXI_DECODE_DIN_EVSE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_evseID( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_evseIDType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_evseIDType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_evseIDType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_evseIDType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode evseID as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_EVSE_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_EVSE_ID) && (EXI_DECODE_DIN_EVSE_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_faultCode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_FAULT_CODE) && (EXI_DECODE_DIN_FAULT_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_faultCode( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_faultCodeType, AUTOMATIC, EXI_APPL_VAR) faultCodePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (faultCodePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value faultCode element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 3)
      {
        /* #40 Store value in output buffer */
        *faultCodePtr = (Exi_DIN_faultCodeType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_FAULT_CODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_FAULT_CODE) && (EXI_DECODE_DIN_FAULT_CODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_faultMsg
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_FAULT_MSG) && (EXI_DECODE_DIN_FAULT_MSG == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_faultMsg( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_faultMsgType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_faultMsgType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_faultMsgType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_faultMsgType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode faultMsg as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_FAULT_MSG, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_FAULT_MSG) && (EXI_DECODE_DIN_FAULT_MSG == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_genChallenge
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_GEN_CHALLENGE) && (EXI_DECODE_DIN_GEN_CHALLENGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_genChallenge( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_genChallengeType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_genChallengeType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_genChallengeType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_genChallengeType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode genChallenge as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_GEN_CHALLENGE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_GEN_CHALLENGE) && (EXI_DECODE_DIN_GEN_CHALLENGE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_isolationLevel
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_ISOLATION_LEVEL) && (EXI_DECODE_DIN_ISOLATION_LEVEL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_isolationLevel( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_isolationLevelType, AUTOMATIC, EXI_APPL_VAR) isolationLevelPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (isolationLevelPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value isolationLevel element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 4)
      {
        /* #40 Store value in output buffer */
        *isolationLevelPtr = (Exi_DIN_isolationLevelType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_ISOLATION_LEVEL, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_ISOLATION_LEVEL) && (EXI_DECODE_DIN_ISOLATION_LEVEL == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_meterID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_METER_ID) && (EXI_DECODE_DIN_METER_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_meterID( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_meterIDType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_meterIDType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_meterIDType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_meterIDType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode meterID as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_METER_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_METER_ID) && (EXI_DECODE_DIN_METER_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_paymentOption
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_PAYMENT_OPTION) && (EXI_DECODE_DIN_PAYMENT_OPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_paymentOption( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_paymentOptionType, AUTOMATIC, EXI_APPL_VAR) paymentOptionPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (paymentOptionPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value paymentOption element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 2)
      {
        /* #40 Store value in output buffer */
        *paymentOptionPtr = (Exi_DIN_paymentOptionType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_PAYMENT_OPTION, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_PAYMENT_OPTION) && (EXI_DECODE_DIN_PAYMENT_OPTION == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_privateKey
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_PRIVATE_KEY) && (EXI_DECODE_DIN_PRIVATE_KEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_privateKey( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_privateKeyType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_privateKeyType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_privateKeyType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_privateKeyType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode privateKey as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_PRIVATE_KEY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_PRIVATE_KEY) && (EXI_DECODE_DIN_PRIVATE_KEY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_responseCode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_responseCode( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_responseCodeType, AUTOMATIC, EXI_APPL_VAR) responseCodePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (responseCodePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value responseCode element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 5);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 23)
      {
        /* #40 Store value in output buffer */
        *responseCodePtr = (Exi_DIN_responseCodeType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_RESPONSE_CODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_RESPONSE_CODE) && (EXI_DECODE_DIN_RESPONSE_CODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_rootCertificateID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_ROOT_CERTIFICATE_ID) && (EXI_DECODE_DIN_ROOT_CERTIFICATE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_rootCertificateID( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_rootCertificateIDType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_rootCertificateIDType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_rootCertificateIDType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_rootCertificateIDType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode rootCertificateID as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_ROOT_CERTIFICATE_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_ROOT_CERTIFICATE_ID) && (EXI_DECODE_DIN_ROOT_CERTIFICATE_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_serviceCategory
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SERVICE_CATEGORY) && (EXI_DECODE_DIN_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_serviceCategory( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_serviceCategoryType, AUTOMATIC, EXI_APPL_VAR) serviceCategoryPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (serviceCategoryPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value serviceCategory element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 2);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 4)
      {
        /* #40 Store value in output buffer */
        *serviceCategoryPtr = (Exi_DIN_serviceCategoryType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SERVICE_CATEGORY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SERVICE_CATEGORY) && (EXI_DECODE_DIN_SERVICE_CATEGORY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_serviceName
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SERVICE_NAME) && (EXI_DECODE_DIN_SERVICE_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_serviceName( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_serviceNameType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_serviceNameType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_serviceNameType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_serviceNameType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode serviceName as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SERVICE_NAME, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SERVICE_NAME) && (EXI_DECODE_DIN_SERVICE_NAME == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_serviceScope
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SERVICE_SCOPE) && (EXI_DECODE_DIN_SERVICE_SCOPE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_serviceScope( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_serviceScopeType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_serviceScopeType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_serviceScopeType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_serviceScopeType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode serviceScope as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SERVICE_SCOPE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SERVICE_SCOPE) && (EXI_DECODE_DIN_SERVICE_SCOPE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_sessionID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SESSION_ID) && (EXI_DECODE_DIN_SESSION_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_sessionID( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_sessionIDType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_sessionIDType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_sessionIDType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_sessionIDType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode sessionID as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SESSION_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SESSION_ID) && (EXI_DECODE_DIN_SESSION_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_sigMeterReading
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_SIG_METER_READING) && (EXI_DECODE_DIN_SIG_METER_READING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_sigMeterReading( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_sigMeterReadingType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_sigMeterReadingType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_sigMeterReadingType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_sigMeterReadingType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode sigMeterReading as byte array */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeBytes(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_SIG_METER_READING, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_SIG_METER_READING) && (EXI_DECODE_DIN_SIG_METER_READING == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_tariffDescription
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_TARIFF_DESCRIPTION) && (EXI_DECODE_DIN_TARIFF_DESCRIPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_tariffDescription( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_tariffDescriptionType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(Exi_DIN_tariffDescriptionType, AUTOMATIC, EXI_APPL_VAR) structPtr;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (elementPtrPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Allocate buffer for this element */
    structPtr = (Exi_DIN_tariffDescriptionType*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(Exi_DIN_tariffDescriptionType), TRUE);
    if (NULL_PTR == (void*)structPtr)
    {
      return;
    }
    else
    {
      *elementPtrPtr = structPtr;
    }
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    /* In case of a deviation this call will update the exiEventCode, it will contain the next event code. This should be the event code of the correct element */
    Exi_VBSDecodeCheckSchemaDeviation(DecWsPtr, &exiEventCode, 1, 1, TRUE, FALSE, TRUE, 0, EXI_SCHEMA_SET_DIN_TYPE);
    if(0 == exiEventCode)
    {
      /* #30 Decode tariffDescription as string value */
      structPtr->Length = (uint16)(sizeof(structPtr->Buffer));
      Exi_VBSDecodeStringValue(&DecWsPtr->DecWs, &structPtr->Buffer[0], &structPtr->Length);
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_TARIFF_DESCRIPTION, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_TARIFF_DESCRIPTION) && (EXI_DECODE_DIN_TARIFF_DESCRIPTION == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_DIN_unitSymbol
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_DIN_UNIT_SYMBOL) && (EXI_DECODE_DIN_UNIT_SYMBOL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_DIN_unitSymbol( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_DIN_unitSymbolType, AUTOMATIC, EXI_APPL_VAR) unitSymbolPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (unitSymbolPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode enumeration value unitSymbol element, decode and check start content event */
    exiEventCode = 0;
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 1);
    if(0 == exiEventCode)
    {
      exiEventCode = 0;
      Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 4);
      /* #30 If value is in its allowed range */
      if (exiEventCode < 10)
      {
        /* #40 Store value in output buffer */
        *unitSymbolPtr = (Exi_DIN_unitSymbolType)exiEventCode;
      }
      /* #50 Enumeration is out of its allowed range */
      else
      {
        /* #60 Set status code to error */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    else
    {
      /* unsupported event code */
      Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_DIN_UNIT_SYMBOL, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_DIN_UNIT_SYMBOL) && (EXI_DECODE_DIN_UNIT_SYMBOL == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_SchemaSet_DIN
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_DECODE_SCHEMA_SET_DIN) && (EXI_DECODE_SCHEMA_SET_DIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Decode_SchemaSet_DIN( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  P2VAR(void, AUTOMATIC, EXI_APPL_VAR) structPtr = NULL_PTR;
  Exi_BitBufType exiEventCode;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DecWsPtr->OutputData.SchemaSetId == EXI_UNKNOWN_SCHEMA_SET_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Decode all DIN schema elements */
    exiEventCode = 0;
    /* #30 Decode the event code of the element */
    Exi_VBSDecodeUIntN(&DecWsPtr->DecWs, &exiEventCode, 7);
    /* #40 Switch event code */
    switch(exiEventCode)
    {
    case 3: /* SE(BodyElement) */
      /* #50 Element BodyElement */
      {
        /* #60 Decode element BodyElement */
      #if (defined(EXI_DECODE_DIN_BODY_ELEMENT) && (EXI_DECODE_DIN_BODY_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* Empty element, make sure that user is informed, allocate a void pointer and set it to NULL_PTR */
        DecWsPtr->OutputData.RootElementId = EXI_DIN_BODY_ELEMENT_TYPE;
        structPtr = (void*)Exi_VBSDecodeCheckAndAllocateBuffer(DecWsPtr, sizeof(structPtr), TRUE);
        if (NULL_PTR != structPtr)
        {
          structPtr = NULL_PTR;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_DIN, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_BODY_ELEMENT) && (EXI_DECODE_DIN_BODY_ELEMENT == STD_ON)) */
      }
    case 77: /* SE(V2G_Message) */
      /* #70 Element V2G_Message */
      {
        /* #80 Decode element V2G_Message */
      #if (defined(EXI_DECODE_DIN_V2G_MESSAGE) && (EXI_DECODE_DIN_V2G_MESSAGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Decode_DIN_V2G_Message(DecWsPtr, (Exi_DIN_V2G_MessageType**)&structPtr); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* Check for errors in subfunctions */
        if (DecWsPtr->DecWs.StatusCode != EXI_E_OK)
        {
          return;
        }
        break;
      #else
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_DIN, EXI_E_INV_PARAM);
        return;
      #endif /* (defined(EXI_DECODE_DIN_V2G_MESSAGE) && (EXI_DECODE_DIN_V2G_MESSAGE == STD_ON)) */
      }
    default:
      /* #90 Unknown element */
      {
        /* #100 Set status code to error */
        /* unsupported event code */
        Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSDecodeSetStatusCode(&DecWsPtr->DecWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_DECODE_SCHEMA_SET_DIN, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_DECODE_SCHEMA_SET_DIN) && (EXI_DECODE_SCHEMA_SET_DIN == STD_ON)) */


#define EXI_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/* PRQA L:NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */

#endif /* (defined (EXI_ENABLE_DECODE_DIN_MESSAGE_SET) && (EXI_ENABLE_DECODE_DIN_MESSAGE_SET == STD_ON)) */

