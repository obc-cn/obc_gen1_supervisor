/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 * 
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Exi_SchemaDecoder.h
 *        \brief  Efficient XML Interchange general decoder header file
 *
 *      \details  Vector static code header file for the Efficient XML Interchange sub-component general decoder.
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file Exi.h.
 *********************************************************************************************************************/
/* PRQA S 0857 EOF */ /* MD_Exi_1.1_0857 */ /* [L] Number of macro definitions exceeds 1024 - program is non-conforming. */

#if !defined (EXI_SCHEMA_DECODER_H) /* PRQA S 0883 */ /* MD_Exi_19.15_0883 */
# define EXI_SCHEMA_DECODER_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 EXI_SCHEMA_DECODER_H_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "Exi.h"
#include "Exi_Priv.h"
#include "Exi_SchemaTypes.h"
#include "Exi_SAP_SchemaDecoder.h"
#include "Exi_XMLSIG_SchemaDecoder.h"
#include "Exi_DIN_SchemaDecoder.h"
#include "Exi_ISO_SchemaDecoder.h"
#include "Exi_ISO_ED2_DIS_SchemaDecoder.h"
/* PRQA L:EXI_SCHEMA_DECODER_H_IF_NESTING */ /* MD_MSR_1.1_828 */

/* PRQA S 0777 IDENTIFIER_NAMES */ /* MD_Exi_5.1 */
/* EXI internal API Ids */
#define EXI_API_ID_DECODE_ID 0x01U
#define EXI_API_ID_DECODE_BASE64BINARY 0x02U
#define EXI_API_ID_DECODE_STRING 0x03U

/* Decoding default switches */
#ifndef EXI_DECODE_ID
#define EXI_DECODE_ID STD_OFF
#endif
#ifndef EXI_DECODE_BASE64BINARY
#define EXI_DECODE_BASE64BINARY STD_OFF
#endif
#ifndef EXI_DECODE_STRING
#define EXI_DECODE_STRING STD_OFF
#endif


#define EXI_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

extern CONST(Exi_DecoderFctType, EXI_CODE)Exi_DecoderFcts[5];

extern CONST(uint8, EXI_CONST) Exi_UriPartitionSize[5];

#define EXI_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */



#define EXI_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/**********************************************************************************************************************
 *  FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Exi_Decode_ID
 *********************************************************************************************************************/
/*! \brief         Decodes an EXI stream
 *  \details       Decodes an EXI stream representing an Exi_IDType object
 *  \param[in,out] DecWsPtr                    pointer to EXI decoding workspace
 *  \param[out]    elementPtrPtr               pointer to the decoded element
 *  \param[in]     schemaSet                   current schema set of this function call
 *  \pre           The provided workspace must be initialized
 *  \context       TASK (due to run time)
 *  \reentrant     TRUE (for different workspaces)
 *  \synchronous   TRUE
 *  \config        EXI_DECODE_ID
 *********************************************************************************************************************/
#if (defined(EXI_DECODE_ID) && (EXI_DECODE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
extern FUNC(void, EXI_CODE) Exi_Decode_ID( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_IDType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr, \
                                       uint8 schemaSet );
#endif /* (defined(EXI_DECODE_ID) && (EXI_DECODE_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_base64Binary
 *********************************************************************************************************************/
/*! \brief         Decodes an EXI stream
 *  \details       Decodes an EXI stream representing an Exi_base64BinaryType object
 *  \param[in,out] DecWsPtr                    pointer to EXI decoding workspace
 *  \param[out]    elementPtrPtr               pointer to the decoded element
 *  \param[in]     schemaSet                   current schema set of this function call
 *  \pre           The provided workspace must be initialized
 *  \context       TASK (due to run time)
 *  \reentrant     TRUE (for different workspaces)
 *  \synchronous   TRUE
 *  \config        EXI_DECODE_BASE64BINARY
 *********************************************************************************************************************/
#if (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
extern FUNC(void, EXI_CODE) Exi_Decode_base64Binary( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_base64BinaryType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr, \
                                       uint8 schemaSet );
#endif /* (defined(EXI_DECODE_BASE64BINARY) && (EXI_DECODE_BASE64BINARY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Decode_string
 *********************************************************************************************************************/
/*! \brief         Decodes an EXI stream
 *  \details       Decodes an EXI stream representing an Exi_stringType object
 *  \param[in,out] DecWsPtr                    pointer to EXI decoding workspace
 *  \param[out]    elementPtrPtr               pointer to the decoded element
 *  \param[in]     schemaSet                   current schema set of this function call
 *  \pre           The provided workspace must be initialized
 *  \context       TASK (due to run time)
 *  \reentrant     TRUE (for different workspaces)
 *  \synchronous   TRUE
 *  \config        EXI_DECODE_STRING
 *********************************************************************************************************************/
#if (defined(EXI_DECODE_STRING) && (EXI_DECODE_STRING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
extern FUNC(void, EXI_CODE) Exi_Decode_string( \
                                       P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) DecWsPtr, \
                                       P2VAR(Exi_stringType*, AUTOMATIC, EXI_APPL_VAR) elementPtrPtr, \
                                       uint8 schemaSet );
#endif /* (defined(EXI_DECODE_STRING) && (EXI_DECODE_STRING == STD_ON)) */


#define EXI_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/* PRQA L:IDENTIFIER_NAMES */

#endif
  /* EXI_SCHEMA_DECODER_H */
