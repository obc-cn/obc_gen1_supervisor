/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Exi_ISO_SchemaEncoder.c
 *        \brief  Efficient XML Interchange ISO encoder source file
 *
 *      \details  Vector static code implementation for the Efficient XML Interchange sub-component ISO encoder.
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file Exi.h.
 *********************************************************************************************************************/
/* PRQA S 0857 EOF */ /* MD_Exi_1.1_0857 */ /* [L] Number of macro definitions exceeds 1024 - program is non-conforming. */

#define EXI_ISO_SCHEMA_ENCODER_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 EXI_ISO_SCHEMA_ENCODER_C_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "Exi_ISO_SchemaEncoder.h"
#include "Exi_BSEncoder.h"
/* PRQA L:EXI_ISO_SCHEMA_ENCODER_C_IF_NESTING */ /* MD_MSR_1.1_828 */
/**********************************************************************************************************************
*  VERSION CHECK
*********************************************************************************************************************/
#if ( (EXI_SW_MAJOR_VERSION != 6u) || (EXI_SW_MINOR_VERSION != 0u) || (EXI_SW_PATCH_VERSION != 1u) )
  #error "Vendor specific version numbers of Exi.h and Exi_ISO_SchemaEncoder.c are inconsistent"
#endif

#if (!defined (EXI_ENABLE_ENCODE_ISO_MESSAGE_SET))
# if (defined (EXI_ENABLE_ISO_MESSAGE_SET))
#  define EXI_ENABLE_ENCODE_ISO_MESSAGE_SET   EXI_ENABLE_ISO_MESSAGE_SET
# else
#  define EXI_ENABLE_ENCODE_ISO_MESSAGE_SET   STD_OFF
# endif
#endif

#if (defined (EXI_ENABLE_ENCODE_ISO_MESSAGE_SET) && (EXI_ENABLE_ENCODE_ISO_MESSAGE_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */

/* PRQA S 0715 NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */ /* MD_Exi_1.1 */


#define EXI_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/**********************************************************************************************************************
 *  FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Exi_Encode_ISO_AC_EVChargeParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_AC_EVCHARGE_PARAMETER) && (EXI_ENCODE_ISO_AC_EVCHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_AC_EVChargeParameter( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_AC_EVChargeParameterType, AUTOMATIC, EXI_APPL_DATA) AC_EVChargeParameterPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AC_EVChargeParameterPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element DepartureTime is included */
    if(1 == AC_EVChargeParameterPtr->DepartureTimeFlag)
    {
      /* #30 Encode element DepartureTime */
      /* SE(DepartureTime) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeUInt(&EncWsPtr->EncWs, (Exi_BitBufType)(AC_EVChargeParameterPtr->DepartureTime));
      /* EE(DepartureTime) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #40 Encode element EAmount */
    /* SE(EAmount) */
    if(0 == AC_EVChargeParameterPtr->DepartureTimeFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (AC_EVChargeParameterPtr->EAmount));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EAmount) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #50 Encode element EVMaxVoltage */
    /* SE(EVMaxVoltage) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (AC_EVChargeParameterPtr->EVMaxVoltage));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVMaxVoltage) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #60 Encode element EVMaxCurrent */
    /* SE(EVMaxCurrent) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (AC_EVChargeParameterPtr->EVMaxCurrent));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVMaxCurrent) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #70 Encode element EVMinCurrent */
    /* SE(EVMinCurrent) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (AC_EVChargeParameterPtr->EVMinCurrent));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVMinCurrent) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AC_EVCHARGE_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_AC_EVCHARGE_PARAMETER) && (EXI_ENCODE_ISO_AC_EVCHARGE_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_AC_EVSEChargeParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_AC_EVSECHARGE_PARAMETER) && (EXI_ENCODE_ISO_AC_EVSECHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_AC_EVSEChargeParameter( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_AC_EVSEChargeParameterType, AUTOMATIC, EXI_APPL_DATA) AC_EVSEChargeParameterPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AC_EVSEChargeParameterPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element AC_EVSEStatus */
    /* SE(AC_EVSEStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_AC_EVSESTATUS) && (EXI_ENCODE_ISO_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_AC_EVSEStatus(EncWsPtr, (AC_EVSEChargeParameterPtr->AC_EVSEStatus));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_AC_EVSESTATUS) && (EXI_ENCODE_ISO_AC_EVSESTATUS == STD_ON)) */
    /* EE(AC_EVSEStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element EVSENominalVoltage */
    /* SE(EVSENominalVoltage) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (AC_EVSEChargeParameterPtr->EVSENominalVoltage));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVSENominalVoltage) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element EVSEMaxCurrent */
    /* SE(EVSEMaxCurrent) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (AC_EVSEChargeParameterPtr->EVSEMaxCurrent));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVSEMaxCurrent) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AC_EVSECHARGE_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_AC_EVSECHARGE_PARAMETER) && (EXI_ENCODE_ISO_AC_EVSECHARGE_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_AC_EVSEStatus
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_AC_EVSESTATUS) && (EXI_ENCODE_ISO_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_AC_EVSEStatus( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_AC_EVSEStatusType, AUTOMATIC, EXI_APPL_DATA) AC_EVSEStatusPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AC_EVSEStatusPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element NotificationMaxDelay */
    /* SE(NotificationMaxDelay) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUInt(&EncWsPtr->EncWs, (Exi_BitBufType)(AC_EVSEStatusPtr->NotificationMaxDelay));
    /* EE(NotificationMaxDelay) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element EVSENotification */
    /* SE(EVSENotification) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_EVSENOTIFICATION) && (EXI_ENCODE_ISO_EVSENOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_EVSENotification(EncWsPtr, &(AC_EVSEStatusPtr->EVSENotification));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AC_EVSESTATUS, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_EVSENOTIFICATION) && (EXI_ENCODE_ISO_EVSENOTIFICATION == STD_ON)) */
    /* EE(EVSENotification) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element RCD */
    /* SE(RCD) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeBool(&EncWsPtr->EncWs, (AC_EVSEStatusPtr->RCD));
    /* EE(RCD) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AC_EVSESTATUS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_AC_EVSESTATUS) && (EXI_ENCODE_ISO_AC_EVSESTATUS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_AttributeId
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_AttributeId( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_AttributeIdType, AUTOMATIC, EXI_APPL_DATA) AttributeIdPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AttributeIdPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (AttributeIdPtr->Length > sizeof(AttributeIdPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &AttributeIdPtr->Buffer[0], AttributeIdPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_ATTRIBUTE_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_AttributeName
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_ATTRIBUTE_NAME) && (EXI_ENCODE_ISO_ATTRIBUTE_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_AttributeName( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_AttributeNameType, AUTOMATIC, EXI_APPL_DATA) AttributeNamePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AttributeNamePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (AttributeNamePtr->Length > sizeof(AttributeNamePtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &AttributeNamePtr->Buffer[0], AttributeNamePtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_ATTRIBUTE_NAME, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_ATTRIBUTE_NAME) && (EXI_ENCODE_ISO_ATTRIBUTE_NAME == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_AuthorizationReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_AUTHORIZATION_REQ) && (EXI_ENCODE_ISO_AUTHORIZATION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_AuthorizationReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_AuthorizationReqType, AUTOMATIC, EXI_APPL_DATA) AuthorizationReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AuthorizationReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element Id is included */
    if ( (1 == AuthorizationReqPtr->IdFlag) && (NULL_PTR != AuthorizationReqPtr->Id) )
    {
      /* #30 Encode attribute Id */
      /* AT(Id) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_AttributeId(EncWsPtr, (AuthorizationReqPtr->Id));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AUTHORIZATION_REQ, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) */

    }
    /* #40 If optional element GenChallenge is included */
    if ( (1 == AuthorizationReqPtr->GenChallengeFlag) && (NULL_PTR != AuthorizationReqPtr->GenChallenge) )
    {
      /* #50 Encode element GenChallenge */
      /* SE(GenChallenge) */
      if(0 == AuthorizationReqPtr->IdFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      #if (defined(EXI_ENCODE_ISO_GEN_CHALLENGE) && (EXI_ENCODE_ISO_GEN_CHALLENGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_genChallenge(EncWsPtr, (AuthorizationReqPtr->GenChallenge));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AUTHORIZATION_REQ, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_GEN_CHALLENGE) && (EXI_ENCODE_ISO_GEN_CHALLENGE == STD_ON)) */
      /* EE(GenChallenge) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #60 Optional element GenChallenge is not included */
    else
    {
      /* EE(AuthorizationReq) */
      /* #70 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      if(0 == AuthorizationReqPtr->IdFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AUTHORIZATION_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_AUTHORIZATION_REQ) && (EXI_ENCODE_ISO_AUTHORIZATION_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_AuthorizationRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_AUTHORIZATION_RES) && (EXI_ENCODE_ISO_AUTHORIZATION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_AuthorizationRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_AuthorizationResType, AUTOMATIC, EXI_APPL_DATA) AuthorizationResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AuthorizationResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(AuthorizationResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AUTHORIZATION_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element EVSEProcessing */
    /* SE(EVSEProcessing) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_EVSEPROCESSING) && (EXI_ENCODE_ISO_EVSEPROCESSING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_EVSEProcessing(EncWsPtr, &(AuthorizationResPtr->EVSEProcessing));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AUTHORIZATION_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_EVSEPROCESSING) && (EXI_ENCODE_ISO_EVSEPROCESSING == STD_ON)) */
    /* EE(EVSEProcessing) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_AUTHORIZATION_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_AUTHORIZATION_RES) && (EXI_ENCODE_ISO_AUTHORIZATION_RES == STD_ON)) */


/* Encode API for abstract type Exi_ISO_BodyBaseType not required */

/**********************************************************************************************************************
 *  Exi_Encode_ISO_Body
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_BODY) && (EXI_ENCODE_ISO_BODY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_Body( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_BodyType, AUTOMATIC, EXI_APPL_DATA) BodyPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (BodyPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element BodyElement is included */
    if(1 == BodyPtr->BodyElementFlag)
    {
      /* #30 Start of Substitution Group BodyElement */
      /* #40 Switch BodyElementElementId */
      switch(BodyPtr->BodyElementElementId)
      {
      case EXI_ISO_AUTHORIZATION_REQ_TYPE:
        /* #50 Substitution element AuthorizationReq */
        {
          /* #60 Encode element AuthorizationReq */
        #if (defined(EXI_ENCODE_ISO_AUTHORIZATION_REQ) && (EXI_ENCODE_ISO_AUTHORIZATION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(AuthorizationReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 6);
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_AuthorizationReq(EncWsPtr, (P2CONST(Exi_ISO_AuthorizationReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(AuthorizationReq) */
          /* Check EE encoding for AuthorizationReq */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_AUTHORIZATION_REQ) && (EXI_ENCODE_ISO_AUTHORIZATION_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_AUTHORIZATION_RES_TYPE:
        /* #70 Substitution element AuthorizationRes */
        {
          /* #80 Encode element AuthorizationRes */
        #if (defined(EXI_ENCODE_ISO_AUTHORIZATION_RES) && (EXI_ENCODE_ISO_AUTHORIZATION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(AuthorizationRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_AuthorizationRes(EncWsPtr, (P2CONST(Exi_ISO_AuthorizationResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(AuthorizationRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_AUTHORIZATION_RES) && (EXI_ENCODE_ISO_AUTHORIZATION_RES == STD_ON)) */
          break;
        }
      /* case EXI_ISO_BODY_ELEMENT_TYPE: Substitution element BodyElement not required, element is abstract*/
      case EXI_ISO_CABLE_CHECK_REQ_TYPE:
        /* #90 Substitution element CableCheckReq */
        {
          /* #100 Encode element CableCheckReq */
        #if (defined(EXI_ENCODE_ISO_CABLE_CHECK_REQ) && (EXI_ENCODE_ISO_CABLE_CHECK_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(CableCheckReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_CableCheckReq(EncWsPtr, (P2CONST(Exi_ISO_CableCheckReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(CableCheckReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_CABLE_CHECK_REQ) && (EXI_ENCODE_ISO_CABLE_CHECK_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_CABLE_CHECK_RES_TYPE:
        /* #110 Substitution element CableCheckRes */
        {
          /* #120 Encode element CableCheckRes */
        #if (defined(EXI_ENCODE_ISO_CABLE_CHECK_RES) && (EXI_ENCODE_ISO_CABLE_CHECK_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(CableCheckRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 4, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_CableCheckRes(EncWsPtr, (P2CONST(Exi_ISO_CableCheckResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(CableCheckRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_CABLE_CHECK_RES) && (EXI_ENCODE_ISO_CABLE_CHECK_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_CERTIFICATE_INSTALLATION_REQ_TYPE:
        /* #130 Substitution element CertificateInstallationReq */
        {
          /* #140 Encode element CertificateInstallationReq */
        #if (defined(EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ) && (EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(CertificateInstallationReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 5, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_CertificateInstallationReq(EncWsPtr, (P2CONST(Exi_ISO_CertificateInstallationReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(CertificateInstallationReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ) && (EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_CERTIFICATE_INSTALLATION_RES_TYPE:
        /* #150 Substitution element CertificateInstallationRes */
        {
          /* #160 Encode element CertificateInstallationRes */
        #if (defined(EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES) && (EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(CertificateInstallationRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 6, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_CertificateInstallationRes(EncWsPtr, (P2CONST(Exi_ISO_CertificateInstallationResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(CertificateInstallationRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES) && (EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_CERTIFICATE_UPDATE_REQ_TYPE:
        /* #170 Substitution element CertificateUpdateReq */
        {
          /* #180 Encode element CertificateUpdateReq */
        #if (defined(EXI_ENCODE_ISO_CERTIFICATE_UPDATE_REQ) && (EXI_ENCODE_ISO_CERTIFICATE_UPDATE_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(CertificateUpdateReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 7, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_CertificateUpdateReq(EncWsPtr, (P2CONST(Exi_ISO_CertificateUpdateReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(CertificateUpdateReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_UPDATE_REQ) && (EXI_ENCODE_ISO_CERTIFICATE_UPDATE_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_CERTIFICATE_UPDATE_RES_TYPE:
        /* #190 Substitution element CertificateUpdateRes */
        {
          /* #200 Encode element CertificateUpdateRes */
        #if (defined(EXI_ENCODE_ISO_CERTIFICATE_UPDATE_RES) && (EXI_ENCODE_ISO_CERTIFICATE_UPDATE_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(CertificateUpdateRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 8, 6);
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_CertificateUpdateRes(EncWsPtr, (P2CONST(Exi_ISO_CertificateUpdateResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(CertificateUpdateRes) */
          /* Check EE encoding for CertificateUpdateRes */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_UPDATE_RES) && (EXI_ENCODE_ISO_CERTIFICATE_UPDATE_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_CHARGE_PARAMETER_DISCOVERY_REQ_TYPE:
        /* #210 Substitution element ChargeParameterDiscoveryReq */
        {
          /* #220 Encode element ChargeParameterDiscoveryReq */
        #if (defined(EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ) && (EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(ChargeParameterDiscoveryReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 9, 6);
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_ChargeParameterDiscoveryReq(EncWsPtr, (P2CONST(Exi_ISO_ChargeParameterDiscoveryReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(ChargeParameterDiscoveryReq) */
          /* Check EE encoding for ChargeParameterDiscoveryReq */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ) && (EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_CHARGE_PARAMETER_DISCOVERY_RES_TYPE:
        /* #230 Substitution element ChargeParameterDiscoveryRes */
        {
          /* #240 Encode element ChargeParameterDiscoveryRes */
        #if (defined(EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES) && (EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(ChargeParameterDiscoveryRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 10, 6);
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_ChargeParameterDiscoveryRes(EncWsPtr, (P2CONST(Exi_ISO_ChargeParameterDiscoveryResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(ChargeParameterDiscoveryRes) */
          /* Check EE encoding for ChargeParameterDiscoveryRes */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES) && (EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_CHARGING_STATUS_REQ_TYPE:
        /* #250 Substitution element ChargingStatusReq */
        {
          /* #260 Encode element ChargingStatusReq */
        #if (defined(EXI_ENCODE_ISO_CHARGING_STATUS_REQ) && (EXI_ENCODE_ISO_CHARGING_STATUS_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(ChargingStatusReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 11, 6);
          /* EE(ChargingStatusReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_CHARGING_STATUS_REQ) && (EXI_ENCODE_ISO_CHARGING_STATUS_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_CHARGING_STATUS_RES_TYPE:
        /* #270 Substitution element ChargingStatusRes */
        {
          /* #280 Encode element ChargingStatusRes */
        #if (defined(EXI_ENCODE_ISO_CHARGING_STATUS_RES) && (EXI_ENCODE_ISO_CHARGING_STATUS_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(ChargingStatusRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 12, 6);
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_ChargingStatusRes(EncWsPtr, (P2CONST(Exi_ISO_ChargingStatusResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(ChargingStatusRes) */
          /* Check EE encoding for ChargingStatusRes */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_CHARGING_STATUS_RES) && (EXI_ENCODE_ISO_CHARGING_STATUS_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_CURRENT_DEMAND_REQ_TYPE:
        /* #290 Substitution element CurrentDemandReq */
        {
          /* #300 Encode element CurrentDemandReq */
        #if (defined(EXI_ENCODE_ISO_CURRENT_DEMAND_REQ) && (EXI_ENCODE_ISO_CURRENT_DEMAND_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(CurrentDemandReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 13, 6);
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_CurrentDemandReq(EncWsPtr, (P2CONST(Exi_ISO_CurrentDemandReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(CurrentDemandReq) */
          /* Check EE encoding for CurrentDemandReq */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_CURRENT_DEMAND_REQ) && (EXI_ENCODE_ISO_CURRENT_DEMAND_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_CURRENT_DEMAND_RES_TYPE:
        /* #310 Substitution element CurrentDemandRes */
        {
          /* #320 Encode element CurrentDemandRes */
        #if (defined(EXI_ENCODE_ISO_CURRENT_DEMAND_RES) && (EXI_ENCODE_ISO_CURRENT_DEMAND_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(CurrentDemandRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 14, 6);
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_CurrentDemandRes(EncWsPtr, (P2CONST(Exi_ISO_CurrentDemandResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(CurrentDemandRes) */
          /* Check EE encoding for CurrentDemandRes */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_CURRENT_DEMAND_RES) && (EXI_ENCODE_ISO_CURRENT_DEMAND_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_METERING_RECEIPT_REQ_TYPE:
        /* #330 Substitution element MeteringReceiptReq */
        {
          /* #340 Encode element MeteringReceiptReq */
        #if (defined(EXI_ENCODE_ISO_METERING_RECEIPT_REQ) && (EXI_ENCODE_ISO_METERING_RECEIPT_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(MeteringReceiptReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 15, 6);
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_MeteringReceiptReq(EncWsPtr, (P2CONST(Exi_ISO_MeteringReceiptReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(MeteringReceiptReq) */
          /* Check EE encoding for MeteringReceiptReq */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_METERING_RECEIPT_REQ) && (EXI_ENCODE_ISO_METERING_RECEIPT_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_METERING_RECEIPT_RES_TYPE:
        /* #350 Substitution element MeteringReceiptRes */
        {
          /* #360 Encode element MeteringReceiptRes */
        #if (defined(EXI_ENCODE_ISO_METERING_RECEIPT_RES) && (EXI_ENCODE_ISO_METERING_RECEIPT_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(MeteringReceiptRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 16, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_MeteringReceiptRes(EncWsPtr, (P2CONST(Exi_ISO_MeteringReceiptResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(MeteringReceiptRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_METERING_RECEIPT_RES) && (EXI_ENCODE_ISO_METERING_RECEIPT_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_PAYMENT_DETAILS_REQ_TYPE:
        /* #370 Substitution element PaymentDetailsReq */
        {
          /* #380 Encode element PaymentDetailsReq */
        #if (defined(EXI_ENCODE_ISO_PAYMENT_DETAILS_REQ) && (EXI_ENCODE_ISO_PAYMENT_DETAILS_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(PaymentDetailsReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 17, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_PaymentDetailsReq(EncWsPtr, (P2CONST(Exi_ISO_PaymentDetailsReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(PaymentDetailsReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_PAYMENT_DETAILS_REQ) && (EXI_ENCODE_ISO_PAYMENT_DETAILS_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_PAYMENT_DETAILS_RES_TYPE:
        /* #390 Substitution element PaymentDetailsRes */
        {
          /* #400 Encode element PaymentDetailsRes */
        #if (defined(EXI_ENCODE_ISO_PAYMENT_DETAILS_RES) && (EXI_ENCODE_ISO_PAYMENT_DETAILS_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(PaymentDetailsRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 18, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_PaymentDetailsRes(EncWsPtr, (P2CONST(Exi_ISO_PaymentDetailsResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(PaymentDetailsRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_PAYMENT_DETAILS_RES) && (EXI_ENCODE_ISO_PAYMENT_DETAILS_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_PAYMENT_SERVICE_SELECTION_REQ_TYPE:
        /* #410 Substitution element PaymentServiceSelectionReq */
        {
          /* #420 Encode element PaymentServiceSelectionReq */
        #if (defined(EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_REQ) && (EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(PaymentServiceSelectionReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 19, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_PaymentServiceSelectionReq(EncWsPtr, (P2CONST(Exi_ISO_PaymentServiceSelectionReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(PaymentServiceSelectionReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_REQ) && (EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_PAYMENT_SERVICE_SELECTION_RES_TYPE:
        /* #430 Substitution element PaymentServiceSelectionRes */
        {
          /* #440 Encode element PaymentServiceSelectionRes */
        #if (defined(EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_RES) && (EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(PaymentServiceSelectionRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 20, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_PaymentServiceSelectionRes(EncWsPtr, (P2CONST(Exi_ISO_PaymentServiceSelectionResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(PaymentServiceSelectionRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_RES) && (EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_POWER_DELIVERY_REQ_TYPE:
        /* #450 Substitution element PowerDeliveryReq */
        {
          /* #460 Encode element PowerDeliveryReq */
        #if (defined(EXI_ENCODE_ISO_POWER_DELIVERY_REQ) && (EXI_ENCODE_ISO_POWER_DELIVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(PowerDeliveryReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 21, 6);
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_PowerDeliveryReq(EncWsPtr, (P2CONST(Exi_ISO_PowerDeliveryReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(PowerDeliveryReq) */
          /* Check EE encoding for PowerDeliveryReq */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_POWER_DELIVERY_REQ) && (EXI_ENCODE_ISO_POWER_DELIVERY_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_POWER_DELIVERY_RES_TYPE:
        /* #470 Substitution element PowerDeliveryRes */
        {
          /* #480 Encode element PowerDeliveryRes */
        #if (defined(EXI_ENCODE_ISO_POWER_DELIVERY_RES) && (EXI_ENCODE_ISO_POWER_DELIVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(PowerDeliveryRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 22, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_PowerDeliveryRes(EncWsPtr, (P2CONST(Exi_ISO_PowerDeliveryResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(PowerDeliveryRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_POWER_DELIVERY_RES) && (EXI_ENCODE_ISO_POWER_DELIVERY_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_PRE_CHARGE_REQ_TYPE:
        /* #490 Substitution element PreChargeReq */
        {
          /* #500 Encode element PreChargeReq */
        #if (defined(EXI_ENCODE_ISO_PRE_CHARGE_REQ) && (EXI_ENCODE_ISO_PRE_CHARGE_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(PreChargeReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 23, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_PreChargeReq(EncWsPtr, (P2CONST(Exi_ISO_PreChargeReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(PreChargeReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_PRE_CHARGE_REQ) && (EXI_ENCODE_ISO_PRE_CHARGE_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_PRE_CHARGE_RES_TYPE:
        /* #510 Substitution element PreChargeRes */
        {
          /* #520 Encode element PreChargeRes */
        #if (defined(EXI_ENCODE_ISO_PRE_CHARGE_RES) && (EXI_ENCODE_ISO_PRE_CHARGE_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(PreChargeRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 24, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_PreChargeRes(EncWsPtr, (P2CONST(Exi_ISO_PreChargeResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(PreChargeRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_PRE_CHARGE_RES) && (EXI_ENCODE_ISO_PRE_CHARGE_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_SERVICE_DETAIL_REQ_TYPE:
        /* #530 Substitution element ServiceDetailReq */
        {
          /* #540 Encode element ServiceDetailReq */
        #if (defined(EXI_ENCODE_ISO_SERVICE_DETAIL_REQ) && (EXI_ENCODE_ISO_SERVICE_DETAIL_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(ServiceDetailReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 25, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_ServiceDetailReq(EncWsPtr, (P2CONST(Exi_ISO_ServiceDetailReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(ServiceDetailReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_SERVICE_DETAIL_REQ) && (EXI_ENCODE_ISO_SERVICE_DETAIL_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_SERVICE_DETAIL_RES_TYPE:
        /* #550 Substitution element ServiceDetailRes */
        {
          /* #560 Encode element ServiceDetailRes */
        #if (defined(EXI_ENCODE_ISO_SERVICE_DETAIL_RES) && (EXI_ENCODE_ISO_SERVICE_DETAIL_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(ServiceDetailRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 26, 6);
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_ServiceDetailRes(EncWsPtr, (P2CONST(Exi_ISO_ServiceDetailResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(ServiceDetailRes) */
          /* Check EE encoding for ServiceDetailRes */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_SERVICE_DETAIL_RES) && (EXI_ENCODE_ISO_SERVICE_DETAIL_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_SERVICE_DISCOVERY_REQ_TYPE:
        /* #570 Substitution element ServiceDiscoveryReq */
        {
          /* #580 Encode element ServiceDiscoveryReq */
        #if (defined(EXI_ENCODE_ISO_SERVICE_DISCOVERY_REQ) && (EXI_ENCODE_ISO_SERVICE_DISCOVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(ServiceDiscoveryReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 27, 6);
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_ServiceDiscoveryReq(EncWsPtr, (P2CONST(Exi_ISO_ServiceDiscoveryReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(ServiceDiscoveryReq) */
          /* Check EE encoding for ServiceDiscoveryReq */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_SERVICE_DISCOVERY_REQ) && (EXI_ENCODE_ISO_SERVICE_DISCOVERY_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_SERVICE_DISCOVERY_RES_TYPE:
        /* #590 Substitution element ServiceDiscoveryRes */
        {
          /* #600 Encode element ServiceDiscoveryRes */
        #if (defined(EXI_ENCODE_ISO_SERVICE_DISCOVERY_RES) && (EXI_ENCODE_ISO_SERVICE_DISCOVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(ServiceDiscoveryRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 28, 6);
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_ServiceDiscoveryRes(EncWsPtr, (P2CONST(Exi_ISO_ServiceDiscoveryResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(ServiceDiscoveryRes) */
          /* Check EE encoding for ServiceDiscoveryRes */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_SERVICE_DISCOVERY_RES) && (EXI_ENCODE_ISO_SERVICE_DISCOVERY_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_SESSION_SETUP_REQ_TYPE:
        /* #610 Substitution element SessionSetupReq */
        {
          /* #620 Encode element SessionSetupReq */
        #if (defined(EXI_ENCODE_ISO_SESSION_SETUP_REQ) && (EXI_ENCODE_ISO_SESSION_SETUP_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(SessionSetupReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 29, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_SessionSetupReq(EncWsPtr, (P2CONST(Exi_ISO_SessionSetupReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(SessionSetupReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_SESSION_SETUP_REQ) && (EXI_ENCODE_ISO_SESSION_SETUP_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_SESSION_SETUP_RES_TYPE:
        /* #630 Substitution element SessionSetupRes */
        {
          /* #640 Encode element SessionSetupRes */
        #if (defined(EXI_ENCODE_ISO_SESSION_SETUP_RES) && (EXI_ENCODE_ISO_SESSION_SETUP_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(SessionSetupRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 30, 6);
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_SessionSetupRes(EncWsPtr, (P2CONST(Exi_ISO_SessionSetupResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(SessionSetupRes) */
          /* Check EE encoding for SessionSetupRes */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_SESSION_SETUP_RES) && (EXI_ENCODE_ISO_SESSION_SETUP_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_SESSION_STOP_REQ_TYPE:
        /* #650 Substitution element SessionStopReq */
        {
          /* #660 Encode element SessionStopReq */
        #if (defined(EXI_ENCODE_ISO_SESSION_STOP_REQ) && (EXI_ENCODE_ISO_SESSION_STOP_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(SessionStopReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 31, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_SessionStopReq(EncWsPtr, (P2CONST(Exi_ISO_SessionStopReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(SessionStopReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_SESSION_STOP_REQ) && (EXI_ENCODE_ISO_SESSION_STOP_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_SESSION_STOP_RES_TYPE:
        /* #670 Substitution element SessionStopRes */
        {
          /* #680 Encode element SessionStopRes */
        #if (defined(EXI_ENCODE_ISO_SESSION_STOP_RES) && (EXI_ENCODE_ISO_SESSION_STOP_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(SessionStopRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 32, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_SessionStopRes(EncWsPtr, (P2CONST(Exi_ISO_SessionStopResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(SessionStopRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_SESSION_STOP_RES) && (EXI_ENCODE_ISO_SESSION_STOP_RES == STD_ON)) */
          break;
        }
      case EXI_ISO_WELDING_DETECTION_REQ_TYPE:
        /* #690 Substitution element WeldingDetectionReq */
        {
          /* #700 Encode element WeldingDetectionReq */
        #if (defined(EXI_ENCODE_ISO_WELDING_DETECTION_REQ) && (EXI_ENCODE_ISO_WELDING_DETECTION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(WeldingDetectionReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 33, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_WeldingDetectionReq(EncWsPtr, (P2CONST(Exi_ISO_WeldingDetectionReqType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(WeldingDetectionReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_WELDING_DETECTION_REQ) && (EXI_ENCODE_ISO_WELDING_DETECTION_REQ == STD_ON)) */
          break;
        }
      case EXI_ISO_WELDING_DETECTION_RES_TYPE:
        /* #710 Substitution element WeldingDetectionRes */
        {
          /* #720 Encode element WeldingDetectionRes */
        #if (defined(EXI_ENCODE_ISO_WELDING_DETECTION_RES) && (EXI_ENCODE_ISO_WELDING_DETECTION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(WeldingDetectionRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 34, 6);
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_WeldingDetectionRes(EncWsPtr, (P2CONST(Exi_ISO_WeldingDetectionResType, AUTOMATIC, EXI_APPL_DATA))BodyPtr->BodyElement); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(WeldingDetectionRes) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_WELDING_DETECTION_RES) && (EXI_ENCODE_ISO_WELDING_DETECTION_RES == STD_ON)) */
          break;
        }
      default:
        /* #730 Default path */
        {
          /* Substitution Element not supported */
          /* #740 Set status code to error */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      } /* switch(BodyPtr->BodyElementElementId) */
      /* End of Substitution Group */
    }
    /* #750 Optional element BodyElement is not included */
    else
    {
      /* EE(Body) */
      /* #760 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 35, 6);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_BODY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_BODY) && (EXI_ENCODE_ISO_BODY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_CableCheckReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CABLE_CHECK_REQ) && (EXI_ENCODE_ISO_CABLE_CHECK_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_CableCheckReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_CableCheckReqType, AUTOMATIC, EXI_APPL_DATA) CableCheckReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (CableCheckReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element DC_EVStatus */
    /* SE(DC_EVStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_DC_EVSTATUS) && (EXI_ENCODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DC_EVStatus(EncWsPtr, (CableCheckReqPtr->DC_EVStatus));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CABLE_CHECK_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DC_EVSTATUS) && (EXI_ENCODE_ISO_DC_EVSTATUS == STD_ON)) */
    /* EE(DC_EVStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CABLE_CHECK_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CABLE_CHECK_REQ) && (EXI_ENCODE_ISO_CABLE_CHECK_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_CableCheckRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CABLE_CHECK_RES) && (EXI_ENCODE_ISO_CABLE_CHECK_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_CableCheckRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_CableCheckResType, AUTOMATIC, EXI_APPL_DATA) CableCheckResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (CableCheckResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(CableCheckResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CABLE_CHECK_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element DC_EVSEStatus */
    /* SE(DC_EVSEStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DC_EVSEStatus(EncWsPtr, (CableCheckResPtr->DC_EVSEStatus));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CABLE_CHECK_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) */
    /* EE(DC_EVSEStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element EVSEProcessing */
    /* SE(EVSEProcessing) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_EVSEPROCESSING) && (EXI_ENCODE_ISO_EVSEPROCESSING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_EVSEProcessing(EncWsPtr, &(CableCheckResPtr->EVSEProcessing));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CABLE_CHECK_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_EVSEPROCESSING) && (EXI_ENCODE_ISO_EVSEPROCESSING == STD_ON)) */
    /* EE(EVSEProcessing) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CABLE_CHECK_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CABLE_CHECK_RES) && (EXI_ENCODE_ISO_CABLE_CHECK_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_CertificateChain
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_CertificateChain( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_CertificateChainType, AUTOMATIC, EXI_APPL_DATA) CertificateChainPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (CertificateChainPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element Id is included */
    if ( (1 == CertificateChainPtr->IdFlag) && (NULL_PTR != CertificateChainPtr->Id) )
    {
      /* #30 Encode attribute Id */
      /* AT(Id) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_AttributeId(EncWsPtr, (CertificateChainPtr->Id));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_CHAIN, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) */

    }
    /* #40 Encode element Certificate */
    /* SE(Certificate) */
    if(0 == CertificateChainPtr->IdFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_ISO_CERTIFICATE) && (EXI_ENCODE_ISO_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_certificate(EncWsPtr, (CertificateChainPtr->Certificate));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_CHAIN, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE) && (EXI_ENCODE_ISO_CERTIFICATE == STD_ON)) */
    /* EE(Certificate) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #50 If optional element SubCertificates is included */
    if ( (1 == CertificateChainPtr->SubCertificatesFlag) && (NULL_PTR != CertificateChainPtr->SubCertificates) )
    {
      /* #60 Encode element SubCertificates */
      /* SE(SubCertificates) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_ISO_SUB_CERTIFICATES) && (EXI_ENCODE_ISO_SUB_CERTIFICATES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_SubCertificates(EncWsPtr, (CertificateChainPtr->SubCertificates));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_CHAIN, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_SUB_CERTIFICATES) && (EXI_ENCODE_ISO_SUB_CERTIFICATES == STD_ON)) */
      /* EE(SubCertificates) */
      /* Check EE encoding for SubCertificates */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    /* #70 Optional element SubCertificates is not included */
    else
    {
      /* EE(CertificateChain) */
      /* #80 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_CHAIN, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_CertificateInstallationReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ) && (EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_CertificateInstallationReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_CertificateInstallationReqType, AUTOMATIC, EXI_APPL_DATA) CertificateInstallationReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (CertificateInstallationReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode attribute Id */
    /* AT(Id) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_AttributeId(EncWsPtr, (CertificateInstallationReqPtr->Id));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) */

    /* #30 Encode element OEMProvisioningCert */
    /* SE(OEMProvisioningCert) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_CERTIFICATE) && (EXI_ENCODE_ISO_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_certificate(EncWsPtr, (CertificateInstallationReqPtr->OEMProvisioningCert));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE) && (EXI_ENCODE_ISO_CERTIFICATE == STD_ON)) */
    /* EE(OEMProvisioningCert) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element ListOfRootCertificateIDs */
    /* SE(ListOfRootCertificateIDs) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_ListOfRootCertificateIDs(EncWsPtr, (CertificateInstallationReqPtr->ListOfRootCertificateIDs));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) */
    /* EE(ListOfRootCertificateIDs) */
    /* Check EE encoding for ListOfRootCertificateIDs */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ) && (EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_CertificateInstallationRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES) && (EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_CertificateInstallationRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_CertificateInstallationResType, AUTOMATIC, EXI_APPL_DATA) CertificateInstallationResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (CertificateInstallationResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(CertificateInstallationResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element SAProvisioningCertificateChain */
    /* SE(SAProvisioningCertificateChain) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_CertificateChain(EncWsPtr, (CertificateInstallationResPtr->SAProvisioningCertificateChain));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */
    /* EE(SAProvisioningCertificateChain) */
    /* Check EE encoding for SAProvisioningCertificateChain */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element ContractSignatureCertChain */
    /* SE(ContractSignatureCertChain) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_CertificateChain(EncWsPtr, (CertificateInstallationResPtr->ContractSignatureCertChain));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */
    /* EE(ContractSignatureCertChain) */
    /* Check EE encoding for ContractSignatureCertChain */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    /* #50 Encode element ContractSignatureEncryptedPrivateKey */
    /* SE(ContractSignatureEncryptedPrivateKey) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY) && (EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_ContractSignatureEncryptedPrivateKey(EncWsPtr, (CertificateInstallationResPtr->ContractSignatureEncryptedPrivateKey));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY) && (EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY == STD_ON)) */
    /* EE(ContractSignatureEncryptedPrivateKey) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #60 Encode element DHpublickey */
    /* SE(DHpublickey) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY) && (EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DiffieHellmanPublickey(EncWsPtr, (CertificateInstallationResPtr->DHpublickey));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY) && (EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY == STD_ON)) */
    /* EE(DHpublickey) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #70 Encode element eMAID */
    /* SE(eMAID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_EMAID) && (EXI_ENCODE_ISO_EMAID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_EMAID(EncWsPtr, (CertificateInstallationResPtr->eMAID));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_EMAID) && (EXI_ENCODE_ISO_EMAID == STD_ON)) */
    /* EE(eMAID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES) && (EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_CertificateUpdateReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CERTIFICATE_UPDATE_REQ) && (EXI_ENCODE_ISO_CERTIFICATE_UPDATE_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_CertificateUpdateReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_CertificateUpdateReqType, AUTOMATIC, EXI_APPL_DATA) CertificateUpdateReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (CertificateUpdateReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode attribute Id */
    /* AT(Id) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_AttributeId(EncWsPtr, (CertificateUpdateReqPtr->Id));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_UPDATE_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) */

    /* #30 Encode element ContractSignatureCertChain */
    /* SE(ContractSignatureCertChain) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_CertificateChain(EncWsPtr, (CertificateUpdateReqPtr->ContractSignatureCertChain));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_UPDATE_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */
    /* EE(ContractSignatureCertChain) */
    /* Check EE encoding for ContractSignatureCertChain */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element eMAID */
    /* SE(eMAID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_E_MAID) && (EXI_ENCODE_ISO_E_MAID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_eMAID(EncWsPtr, (CertificateUpdateReqPtr->eMAID));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_UPDATE_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_E_MAID) && (EXI_ENCODE_ISO_E_MAID == STD_ON)) */
    /* EE(eMAID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #50 Encode element ListOfRootCertificateIDs */
    /* SE(ListOfRootCertificateIDs) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_ListOfRootCertificateIDs(EncWsPtr, (CertificateUpdateReqPtr->ListOfRootCertificateIDs));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_UPDATE_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) */
    /* EE(ListOfRootCertificateIDs) */
    /* Check EE encoding for ListOfRootCertificateIDs */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_UPDATE_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_UPDATE_REQ) && (EXI_ENCODE_ISO_CERTIFICATE_UPDATE_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_CertificateUpdateRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CERTIFICATE_UPDATE_RES) && (EXI_ENCODE_ISO_CERTIFICATE_UPDATE_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_CertificateUpdateRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_CertificateUpdateResType, AUTOMATIC, EXI_APPL_DATA) CertificateUpdateResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (CertificateUpdateResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(CertificateUpdateResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element SAProvisioningCertificateChain */
    /* SE(SAProvisioningCertificateChain) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_CertificateChain(EncWsPtr, (CertificateUpdateResPtr->SAProvisioningCertificateChain));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */
    /* EE(SAProvisioningCertificateChain) */
    /* Check EE encoding for SAProvisioningCertificateChain */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element ContractSignatureCertChain */
    /* SE(ContractSignatureCertChain) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_CertificateChain(EncWsPtr, (CertificateUpdateResPtr->ContractSignatureCertChain));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */
    /* EE(ContractSignatureCertChain) */
    /* Check EE encoding for ContractSignatureCertChain */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    /* #50 Encode element ContractSignatureEncryptedPrivateKey */
    /* SE(ContractSignatureEncryptedPrivateKey) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY) && (EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_ContractSignatureEncryptedPrivateKey(EncWsPtr, (CertificateUpdateResPtr->ContractSignatureEncryptedPrivateKey));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY) && (EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY == STD_ON)) */
    /* EE(ContractSignatureEncryptedPrivateKey) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #60 Encode element DHpublickey */
    /* SE(DHpublickey) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY) && (EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DiffieHellmanPublickey(EncWsPtr, (CertificateUpdateResPtr->DHpublickey));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY) && (EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY == STD_ON)) */
    /* EE(DHpublickey) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #70 Encode element eMAID */
    /* SE(eMAID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_EMAID) && (EXI_ENCODE_ISO_EMAID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_EMAID(EncWsPtr, (CertificateUpdateResPtr->eMAID));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_UPDATE_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_EMAID) && (EXI_ENCODE_ISO_EMAID == STD_ON)) */
    /* EE(eMAID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #80 If optional element RetryCounter is included */
    if(1 == CertificateUpdateResPtr->RetryCounterFlag)
    {
      /* #90 Encode element RetryCounter */
      /* SE(RetryCounter) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeInt(&EncWsPtr->EncWs, (sint32)(CertificateUpdateResPtr->RetryCounter));
      /* EE(RetryCounter) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #100 Optional element RetryCounter is not included */
    else
    {
      /* EE(CertificateUpdateRes) */
      /* #110 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE_UPDATE_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_UPDATE_RES) && (EXI_ENCODE_ISO_CERTIFICATE_UPDATE_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ChargeParameterDiscoveryReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ) && (EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ChargeParameterDiscoveryReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ChargeParameterDiscoveryReqType, AUTOMATIC, EXI_APPL_DATA) ChargeParameterDiscoveryReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ChargeParameterDiscoveryReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element MaxEntriesSAScheduleTuple is included */
    if(1 == ChargeParameterDiscoveryReqPtr->MaxEntriesSAScheduleTupleFlag)
    {
      /* #30 Encode element MaxEntriesSAScheduleTuple */
      /* SE(MaxEntriesSAScheduleTuple) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeUInt(&EncWsPtr->EncWs, (Exi_BitBufType)(ChargeParameterDiscoveryReqPtr->MaxEntriesSAScheduleTuple));
      /* EE(MaxEntriesSAScheduleTuple) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #40 Encode element RequestedEnergyTransferMode */
    /* SE(RequestedEnergyTransferMode) */
    if(0 == ChargeParameterDiscoveryReqPtr->MaxEntriesSAScheduleTupleFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_ENCODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_EnergyTransferMode(EncWsPtr, &(ChargeParameterDiscoveryReqPtr->RequestedEnergyTransferMode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_ENCODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) */
    /* EE(RequestedEnergyTransferMode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #50 Start of Substitution Group EVChargeParameter */
    /* #60 Switch EVChargeParameterElementId */
    switch(ChargeParameterDiscoveryReqPtr->EVChargeParameterElementId)
    {
    case EXI_ISO_AC_EVCHARGE_PARAMETER_TYPE:
      /* #70 Substitution element AC_EVChargeParameter */
      {
        /* #80 Encode element AC_EVChargeParameter */
      #if (defined(EXI_ENCODE_ISO_AC_EVCHARGE_PARAMETER) && (EXI_ENCODE_ISO_AC_EVCHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* SE(AC_EVChargeParameter) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
        /* NULL_PTR check is done in the called API */
        Exi_Encode_ISO_AC_EVChargeParameter(EncWsPtr, (P2CONST(Exi_ISO_AC_EVChargeParameterType, AUTOMATIC, EXI_APPL_DATA))ChargeParameterDiscoveryReqPtr->EVChargeParameter); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* EE(AC_EVChargeParameter) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_AC_EVCHARGE_PARAMETER) && (EXI_ENCODE_ISO_AC_EVCHARGE_PARAMETER == STD_ON)) */
        break;
      }
    case EXI_ISO_DC_EVCHARGE_PARAMETER_TYPE:
      /* #90 Substitution element DC_EVChargeParameter */
      {
        /* #100 Encode element DC_EVChargeParameter */
      #if (defined(EXI_ENCODE_ISO_DC_EVCHARGE_PARAMETER) && (EXI_ENCODE_ISO_DC_EVCHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* SE(DC_EVChargeParameter) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
        EncWsPtr->EncWs.EERequired = TRUE;
        /* NULL_PTR check is done in the called API */
        Exi_Encode_ISO_DC_EVChargeParameter(EncWsPtr, (P2CONST(Exi_ISO_DC_EVChargeParameterType, AUTOMATIC, EXI_APPL_DATA))ChargeParameterDiscoveryReqPtr->EVChargeParameter); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* EE(DC_EVChargeParameter) */
        /* Check EE encoding for DC_EVChargeParameter */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_DC_EVCHARGE_PARAMETER) && (EXI_ENCODE_ISO_DC_EVCHARGE_PARAMETER == STD_ON)) */
        break;
      }
    /* case EXI_ISO_EVCHARGE_PARAMETER_TYPE: Substitution element EVChargeParameter not required, element is abstract*/
    default:
      /* #110 Default path */
      {
        /* Substitution Element not supported */
        /* #120 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    } /* switch(ChargeParameterDiscoveryReqPtr->EVChargeParameterElementId) */
    /* End of Substitution Group */
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ) && (EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ChargeParameterDiscoveryRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES) && (EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ChargeParameterDiscoveryRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ChargeParameterDiscoveryResType, AUTOMATIC, EXI_APPL_DATA) ChargeParameterDiscoveryResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ChargeParameterDiscoveryResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(ChargeParameterDiscoveryResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element EVSEProcessing */
    /* SE(EVSEProcessing) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_EVSEPROCESSING) && (EXI_ENCODE_ISO_EVSEPROCESSING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_EVSEProcessing(EncWsPtr, &(ChargeParameterDiscoveryResPtr->EVSEProcessing));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_EVSEPROCESSING) && (EXI_ENCODE_ISO_EVSEPROCESSING == STD_ON)) */
    /* EE(EVSEProcessing) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 If optional element SASchedules is included */
    if(1 == ChargeParameterDiscoveryResPtr->SASchedulesFlag)
    {
      /* #50 Start of Substitution Group SASchedules */
      /* #60 Switch SASchedulesElementId */
      switch(ChargeParameterDiscoveryResPtr->SASchedulesElementId)
      {
      case EXI_ISO_SASCHEDULE_LIST_TYPE:
        /* #70 Substitution element SAScheduleList */
        {
          /* #80 Encode element SAScheduleList */
        #if (defined(EXI_ENCODE_ISO_SASCHEDULE_LIST) && (EXI_ENCODE_ISO_SASCHEDULE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(SAScheduleList) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_SAScheduleList(EncWsPtr, (P2CONST(Exi_ISO_SAScheduleListType, AUTOMATIC, EXI_APPL_DATA))ChargeParameterDiscoveryResPtr->SASchedules); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(SAScheduleList) */
          /* Check EE encoding for SAScheduleList */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_SASCHEDULE_LIST) && (EXI_ENCODE_ISO_SASCHEDULE_LIST == STD_ON)) */
          break;
        }
      /* case EXI_ISO_SASCHEDULES_TYPE: Substitution element SASchedules not required, element is abstract*/
      default:
        /* #90 Default path */
        {
          /* Substitution Element not supported */
          /* #100 Set status code to error */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      } /* switch(ChargeParameterDiscoveryResPtr->SASchedulesElementId) */
      /* End of Substitution Group */
    }
    /* #110 Start of Substitution Group EVSEChargeParameter */
    /* #120 Switch EVSEChargeParameterElementId */
    switch(ChargeParameterDiscoveryResPtr->EVSEChargeParameterElementId)
    {
    case EXI_ISO_AC_EVSECHARGE_PARAMETER_TYPE:
      /* #130 Substitution element AC_EVSEChargeParameter */
      {
        /* #140 Encode element AC_EVSEChargeParameter */
      #if (defined(EXI_ENCODE_ISO_AC_EVSECHARGE_PARAMETER) && (EXI_ENCODE_ISO_AC_EVSECHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* SE(AC_EVSEChargeParameter) */
        if(0 == ChargeParameterDiscoveryResPtr->SASchedulesFlag)
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
        }
        /* NULL_PTR check is done in the called API */
        Exi_Encode_ISO_AC_EVSEChargeParameter(EncWsPtr, (P2CONST(Exi_ISO_AC_EVSEChargeParameterType, AUTOMATIC, EXI_APPL_DATA))ChargeParameterDiscoveryResPtr->EVSEChargeParameter); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* EE(AC_EVSEChargeParameter) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_AC_EVSECHARGE_PARAMETER) && (EXI_ENCODE_ISO_AC_EVSECHARGE_PARAMETER == STD_ON)) */
        break;
      }
    case EXI_ISO_DC_EVSECHARGE_PARAMETER_TYPE:
      /* #150 Substitution element DC_EVSEChargeParameter */
      {
        /* #160 Encode element DC_EVSEChargeParameter */
      #if (defined(EXI_ENCODE_ISO_DC_EVSECHARGE_PARAMETER) && (EXI_ENCODE_ISO_DC_EVSECHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* SE(DC_EVSEChargeParameter) */
        if(0 == ChargeParameterDiscoveryResPtr->SASchedulesFlag)
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
        }
        EncWsPtr->EncWs.EERequired = TRUE;
        /* NULL_PTR check is done in the called API */
        Exi_Encode_ISO_DC_EVSEChargeParameter(EncWsPtr, (P2CONST(Exi_ISO_DC_EVSEChargeParameterType, AUTOMATIC, EXI_APPL_DATA))ChargeParameterDiscoveryResPtr->EVSEChargeParameter); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* EE(DC_EVSEChargeParameter) */
        /* Check EE encoding for DC_EVSEChargeParameter */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_DC_EVSECHARGE_PARAMETER) && (EXI_ENCODE_ISO_DC_EVSECHARGE_PARAMETER == STD_ON)) */
        break;
      }
    /* case EXI_ISO_EVSECHARGE_PARAMETER_TYPE: Substitution element EVSEChargeParameter not required, element is abstract*/
    default:
      /* #170 Default path */
      {
        /* Substitution Element not supported */
        /* #180 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    } /* switch(ChargeParameterDiscoveryResPtr->EVSEChargeParameterElementId) */
    /* End of Substitution Group */
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES) && (EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ChargeService
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CHARGE_SERVICE) && (EXI_ENCODE_ISO_CHARGE_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ChargeService( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ChargeServiceType, AUTOMATIC, EXI_APPL_DATA) ChargeServicePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ChargeServicePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ServiceID */
    /* SE(ServiceID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUInt(&EncWsPtr->EncWs, (Exi_BitBufType)(ChargeServicePtr->ServiceID));
    /* EE(ServiceID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If optional element ServiceName is included */
    if ( (1 == ChargeServicePtr->ServiceNameFlag) && (NULL_PTR != ChargeServicePtr->ServiceName) )
    {
      /* #40 Encode element ServiceName */
      /* SE(ServiceName) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_SERVICE_NAME) && (EXI_ENCODE_ISO_SERVICE_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_serviceName(EncWsPtr, (ChargeServicePtr->ServiceName));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_SERVICE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_SERVICE_NAME) && (EXI_ENCODE_ISO_SERVICE_NAME == STD_ON)) */
      /* EE(ServiceName) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #50 Encode element ServiceCategory */
    /* SE(ServiceCategory) */
    if(0 == ChargeServicePtr->ServiceNameFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_ISO_SERVICE_CATEGORY) && (EXI_ENCODE_ISO_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_serviceCategory(EncWsPtr, &(ChargeServicePtr->ServiceCategory));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_SERVICE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_SERVICE_CATEGORY) && (EXI_ENCODE_ISO_SERVICE_CATEGORY == STD_ON)) */
    /* EE(ServiceCategory) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #60 If optional element ServiceScope is included */
    if ( (1 == ChargeServicePtr->ServiceScopeFlag) && (NULL_PTR != ChargeServicePtr->ServiceScope) )
    {
      /* #70 Encode element ServiceScope */
      /* SE(ServiceScope) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_SERVICE_SCOPE) && (EXI_ENCODE_ISO_SERVICE_SCOPE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_serviceScope(EncWsPtr, (ChargeServicePtr->ServiceScope));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_SERVICE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_SERVICE_SCOPE) && (EXI_ENCODE_ISO_SERVICE_SCOPE == STD_ON)) */
      /* EE(ServiceScope) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #80 Encode element FreeService */
    /* SE(FreeService) */
    if(0 == ChargeServicePtr->ServiceScopeFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeBool(&EncWsPtr->EncWs, (ChargeServicePtr->FreeService));
    /* EE(FreeService) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #90 Encode element SupportedEnergyTransferMode */
    /* SE(SupportedEnergyTransferMode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE) && (EXI_ENCODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_SupportedEnergyTransferMode(EncWsPtr, (ChargeServicePtr->SupportedEnergyTransferMode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_SERVICE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE) && (EXI_ENCODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE == STD_ON)) */
    /* EE(SupportedEnergyTransferMode) */
    /* Check EE encoding for SupportedEnergyTransferMode */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_SERVICE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CHARGE_SERVICE) && (EXI_ENCODE_ISO_CHARGE_SERVICE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ChargingProfile
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CHARGING_PROFILE) && (EXI_ENCODE_ISO_CHARGING_PROFILE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ChargingProfile( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ChargingProfileType, AUTOMATIC, EXI_APPL_DATA) ChargingProfilePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_ISO_PROFILE_ENTRY) && (EXI_ENCODE_ISO_PROFILE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_ProfileEntryType) nextPtr;
  uint8_least i;
  #endif /* #if (defined(EXI_ENCODE_ISO_PROFILE_ENTRY) && (EXI_ENCODE_ISO_PROFILE_ENTRY == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ChargingProfilePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    #if (defined(EXI_ENCODE_ISO_PROFILE_ENTRY) && (EXI_ENCODE_ISO_PROFILE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #20 Initialize next pointer with the first ProfileEntry element */
    nextPtr = (Exi_ISO_ProfileEntryType*)ChargingProfilePtr->ProfileEntry;
    /* #30 Loop over all ProfileEntry elements */
    for(i=0; i<24; i++)
    {
      /* #40 Encode element ProfileEntry */
      /* SE(ProfileEntry) */
      if(0 == i)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }

      EncWsPtr->EncWs.EERequired = TRUE;
      Exi_Encode_ISO_ProfileEntry(EncWsPtr, nextPtr);
      /* EE(ProfileEntry) */
      /* Check EE encoding for ProfileEntry */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_ISO_ProfileEntryType*)(nextPtr->NextProfileEntryPtr);
      /* #50 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* i holds the number of encoded Exi_ISO_ProfileEntryType elements */
        i++; /* PRQA S 2469 */  /*  MD_Exi_13.6 */
        /* #60 End the loop */
        break;
      }
    }
    /* #70 If maximum possible number of ChargingProfile's was encoded */
    if(i == 24)
    {
      /* #80 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #90 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG,EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    /* #100 If element list does not have maximum length */
    if( i < 24)
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGING_PROFILE, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_ISO_PROFILE_ENTRY) && (EXI_ENCODE_ISO_PROFILE_ENTRY == STD_ON)) */
    {
      /* EE(ChargingProfile) */
      /* #110 Encode end element tag is requrired */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGING_PROFILE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CHARGING_PROFILE) && (EXI_ENCODE_ISO_CHARGING_PROFILE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ChargingStatusRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CHARGING_STATUS_RES) && (EXI_ENCODE_ISO_CHARGING_STATUS_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ChargingStatusRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ChargingStatusResType, AUTOMATIC, EXI_APPL_DATA) ChargingStatusResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ChargingStatusResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if ((ChargingStatusResPtr->SAScheduleTupleID < 1))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(ChargingStatusResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element EVSEID */
    /* SE(EVSEID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_EVSE_ID) && (EXI_ENCODE_ISO_EVSE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_evseID(EncWsPtr, (ChargingStatusResPtr->EVSEID));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_EVSE_ID) && (EXI_ENCODE_ISO_EVSE_ID == STD_ON)) */
    /* EE(EVSEID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element SAScheduleTupleID */
    /* SE(SAScheduleTupleID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(ChargingStatusResPtr->SAScheduleTupleID - 1), 8);
    /* EE(SAScheduleTupleID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #50 If optional element EVSEMaxCurrent is included */
    if ( (1 == ChargingStatusResPtr->EVSEMaxCurrentFlag) && (NULL_PTR != ChargingStatusResPtr->EVSEMaxCurrent) )
    {
      /* #60 Encode element EVSEMaxCurrent */
      /* SE(EVSEMaxCurrent) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (ChargingStatusResPtr->EVSEMaxCurrent));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(EVSEMaxCurrent) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #70 If optional element MeterInfo is included */
    if ( (1 == ChargingStatusResPtr->MeterInfoFlag) && (NULL_PTR != ChargingStatusResPtr->MeterInfo) )
    {
      /* #80 Encode element MeterInfo */
      /* SE(MeterInfo) */
      if(0 == ChargingStatusResPtr->EVSEMaxCurrentFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_ISO_METER_INFO) && (EXI_ENCODE_ISO_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_MeterInfo(EncWsPtr, (ChargingStatusResPtr->MeterInfo));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_METER_INFO) && (EXI_ENCODE_ISO_METER_INFO == STD_ON)) */
      /* EE(MeterInfo) */
      /* Check EE encoding for MeterInfo */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    /* #90 If optional element ReceiptRequired is included */
    if(1 == ChargingStatusResPtr->ReceiptRequiredFlag)
    {
      /* #100 Encode element ReceiptRequired */
      /* SE(ReceiptRequired) */
      if(0 == ChargingStatusResPtr->MeterInfoFlag)
      {
        if(0 == ChargingStatusResPtr->EVSEMaxCurrentFlag)
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeBool(&EncWsPtr->EncWs, (ChargingStatusResPtr->ReceiptRequired));
      /* EE(ReceiptRequired) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #110 Encode element AC_EVSEStatus */
    /* SE(AC_EVSEStatus) */
    if(0 == ChargingStatusResPtr->ReceiptRequiredFlag)
    {
      if(0 == ChargingStatusResPtr->MeterInfoFlag)
      {
        if(0 == ChargingStatusResPtr->EVSEMaxCurrentFlag)
        {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_ISO_AC_EVSESTATUS) && (EXI_ENCODE_ISO_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_AC_EVSEStatus(EncWsPtr, (ChargingStatusResPtr->AC_EVSEStatus));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGING_STATUS_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_AC_EVSESTATUS) && (EXI_ENCODE_ISO_AC_EVSESTATUS == STD_ON)) */
    /* EE(AC_EVSEStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGING_STATUS_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CHARGING_STATUS_RES) && (EXI_ENCODE_ISO_CHARGING_STATUS_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ConsumptionCost
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CONSUMPTION_COST) && (EXI_ENCODE_ISO_CONSUMPTION_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ConsumptionCost( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ConsumptionCostType, AUTOMATIC, EXI_APPL_DATA) ConsumptionCostPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_ISO_COST) && (EXI_ENCODE_ISO_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_CostType) nextPtr;
  uint8_least i;
  #endif /* #if (defined(EXI_ENCODE_ISO_COST) && (EXI_ENCODE_ISO_COST == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ConsumptionCostPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element startValue */
    /* SE(startValue) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (ConsumptionCostPtr->startValue));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CONSUMPTION_COST, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(startValue) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_COST) && (EXI_ENCODE_ISO_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #30 Initialize next pointer with the first Cost element */
    nextPtr = (Exi_ISO_CostType*)ConsumptionCostPtr->Cost;
    /* #40 Loop over all Cost elements */
    for(i=0; i<3; i++)
    {
      /* #50 Encode element Cost */
      /* SE(Cost) */
      if(0 == i)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }

      EncWsPtr->EncWs.EERequired = TRUE;
      Exi_Encode_ISO_Cost(EncWsPtr, nextPtr);
      /* EE(Cost) */
      /* Check EE encoding for Cost */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_ISO_CostType*)(nextPtr->NextCostPtr);
      /* #60 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* i holds the number of encoded Exi_ISO_CostType elements */
        i++; /* PRQA S 2469 */  /*  MD_Exi_13.6 */
        /* #70 End the loop */
        break;
      }
    }
    /* #80 If maximum possible number of ConsumptionCost's was encoded */
    if(i == 3)
    {
      /* #90 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #100 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG,EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    /* #110 If element list does not have maximum length */
    if( i < 3)
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CONSUMPTION_COST, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_ISO_COST) && (EXI_ENCODE_ISO_COST == STD_ON)) */
    {
      /* EE(ConsumptionCost) */
      /* #120 Encode end element tag is requrired */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CONSUMPTION_COST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CONSUMPTION_COST) && (EXI_ENCODE_ISO_CONSUMPTION_COST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ContractSignatureEncryptedPrivateKey
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY) && (EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ContractSignatureEncryptedPrivateKey( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ContractSignatureEncryptedPrivateKeyType, AUTOMATIC, EXI_APPL_DATA) ContractSignatureEncryptedPrivateKeyPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ContractSignatureEncryptedPrivateKeyPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (ContractSignatureEncryptedPrivateKeyPtr->Length > sizeof(ContractSignatureEncryptedPrivateKeyPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode attribute Id */
    /* AT(Id) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_AttributeId(EncWsPtr, (ContractSignatureEncryptedPrivateKeyPtr->Id));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) */

    /* #30 Start content of ContractSignatureEncryptedPrivateKey */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode ContractSignatureEncryptedPrivateKey as byte array */
    Exi_VBSEncodeBytes(&EncWsPtr->EncWs, &ContractSignatureEncryptedPrivateKeyPtr->Buffer[0], ContractSignatureEncryptedPrivateKeyPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY) && (EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_Cost
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_COST) && (EXI_ENCODE_ISO_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_Cost( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_CostType, AUTOMATIC, EXI_APPL_DATA) CostPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (CostPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if ((CostPtr->amountMultiplierFlag == 1) && ((CostPtr->amountMultiplier < -3) || (CostPtr->amountMultiplier > 3)))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element costKind */
    /* SE(costKind) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_COST_KIND) && (EXI_ENCODE_ISO_COST_KIND == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_costKind(EncWsPtr, &(CostPtr->costKind));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_COST, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_COST_KIND) && (EXI_ENCODE_ISO_COST_KIND == STD_ON)) */
    /* EE(costKind) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element amount */
    /* SE(amount) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUInt(&EncWsPtr->EncWs, (Exi_BitBufType)(CostPtr->amount));
    /* EE(amount) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 If optional element amountMultiplier is included */
    if(1 == CostPtr->amountMultiplierFlag)
    {
      /* #50 Encode element amountMultiplier */
      /* SE(amountMultiplier) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(CostPtr->amountMultiplier + 3), 3); /*lint !e571 */ /* Signed to unsigned cast, value is limited by the schema, lowest possible value is 0 or an offset is added to set it to 0 */
      /* EE(amountMultiplier) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #60 Optional element amountMultiplier is not included */
    else
    {
      /* EE(Cost) */
      /* #70 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_COST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_COST) && (EXI_ENCODE_ISO_COST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_CurrentDemandReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CURRENT_DEMAND_REQ) && (EXI_ENCODE_ISO_CURRENT_DEMAND_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_CurrentDemandReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_CurrentDemandReqType, AUTOMATIC, EXI_APPL_DATA) CurrentDemandReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (CurrentDemandReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element DC_EVStatus */
    /* SE(DC_EVStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_DC_EVSTATUS) && (EXI_ENCODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DC_EVStatus(EncWsPtr, (CurrentDemandReqPtr->DC_EVStatus));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DC_EVSTATUS) && (EXI_ENCODE_ISO_DC_EVSTATUS == STD_ON)) */
    /* EE(DC_EVStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element EVTargetCurrent */
    /* SE(EVTargetCurrent) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (CurrentDemandReqPtr->EVTargetCurrent));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVTargetCurrent) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 If optional element EVMaximumVoltageLimit is included */
    if ( (1 == CurrentDemandReqPtr->EVMaximumVoltageLimitFlag) && (NULL_PTR != CurrentDemandReqPtr->EVMaximumVoltageLimit) )
    {
      /* #50 Encode element EVMaximumVoltageLimit */
      /* SE(EVMaximumVoltageLimit) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (CurrentDemandReqPtr->EVMaximumVoltageLimit));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(EVMaximumVoltageLimit) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #60 If optional element EVMaximumCurrentLimit is included */
    if ( (1 == CurrentDemandReqPtr->EVMaximumCurrentLimitFlag) && (NULL_PTR != CurrentDemandReqPtr->EVMaximumCurrentLimit) )
    {
      /* #70 Encode element EVMaximumCurrentLimit */
      /* SE(EVMaximumCurrentLimit) */
      if(0 == CurrentDemandReqPtr->EVMaximumVoltageLimitFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      }
      #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (CurrentDemandReqPtr->EVMaximumCurrentLimit));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(EVMaximumCurrentLimit) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #80 If optional element EVMaximumPowerLimit is included */
    if ( (1 == CurrentDemandReqPtr->EVMaximumPowerLimitFlag) && (NULL_PTR != CurrentDemandReqPtr->EVMaximumPowerLimit) )
    {
      /* #90 Encode element EVMaximumPowerLimit */
      /* SE(EVMaximumPowerLimit) */
      if(0 == CurrentDemandReqPtr->EVMaximumCurrentLimitFlag)
      {
        if(0 == CurrentDemandReqPtr->EVMaximumVoltageLimitFlag)
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (CurrentDemandReqPtr->EVMaximumPowerLimit));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(EVMaximumPowerLimit) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #100 If optional element BulkChargingComplete is included */
    if(1 == CurrentDemandReqPtr->BulkChargingCompleteFlag)
    {
      /* #110 Encode element BulkChargingComplete */
      /* SE(BulkChargingComplete) */
      if(0 == CurrentDemandReqPtr->EVMaximumPowerLimitFlag)
      {
        if(0 == CurrentDemandReqPtr->EVMaximumCurrentLimitFlag)
        {
          if(0 == CurrentDemandReqPtr->EVMaximumVoltageLimitFlag)
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
          }
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
          }
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeBool(&EncWsPtr->EncWs, (CurrentDemandReqPtr->BulkChargingComplete));
      /* EE(BulkChargingComplete) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #120 Encode element ChargingComplete */
    /* SE(ChargingComplete) */
    if(0 == CurrentDemandReqPtr->BulkChargingCompleteFlag)
    {
      if(0 == CurrentDemandReqPtr->EVMaximumPowerLimitFlag)
      {
        if(0 == CurrentDemandReqPtr->EVMaximumCurrentLimitFlag)
        {
          if(0 == CurrentDemandReqPtr->EVMaximumVoltageLimitFlag)
          {
              Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 4, 3);
          }
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
          }
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeBool(&EncWsPtr->EncWs, (CurrentDemandReqPtr->ChargingComplete));
    /* EE(ChargingComplete) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #130 If optional element RemainingTimeToFullSoC is included */
    if ( (1 == CurrentDemandReqPtr->RemainingTimeToFullSoCFlag) && (NULL_PTR != CurrentDemandReqPtr->RemainingTimeToFullSoC) )
    {
      /* #140 Encode element RemainingTimeToFullSoC */
      /* SE(RemainingTimeToFullSoC) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (CurrentDemandReqPtr->RemainingTimeToFullSoC));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(RemainingTimeToFullSoC) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #150 If optional element RemainingTimeToBulkSoC is included */
    if ( (1 == CurrentDemandReqPtr->RemainingTimeToBulkSoCFlag) && (NULL_PTR != CurrentDemandReqPtr->RemainingTimeToBulkSoC) )
    {
      /* #160 Encode element RemainingTimeToBulkSoC */
      /* SE(RemainingTimeToBulkSoC) */
      if(0 == CurrentDemandReqPtr->RemainingTimeToFullSoCFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (CurrentDemandReqPtr->RemainingTimeToBulkSoC));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(RemainingTimeToBulkSoC) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #170 Encode element EVTargetVoltage */
    /* SE(EVTargetVoltage) */
    if(0 == CurrentDemandReqPtr->RemainingTimeToBulkSoCFlag)
    {
      if(0 == CurrentDemandReqPtr->RemainingTimeToFullSoCFlag)
      {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (CurrentDemandReqPtr->EVTargetVoltage));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVTargetVoltage) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CURRENT_DEMAND_REQ) && (EXI_ENCODE_ISO_CURRENT_DEMAND_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_CurrentDemandRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CURRENT_DEMAND_RES) && (EXI_ENCODE_ISO_CURRENT_DEMAND_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_CurrentDemandRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_CurrentDemandResType, AUTOMATIC, EXI_APPL_DATA) CurrentDemandResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (CurrentDemandResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if ((CurrentDemandResPtr->SAScheduleTupleID < 1))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(CurrentDemandResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element DC_EVSEStatus */
    /* SE(DC_EVSEStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DC_EVSEStatus(EncWsPtr, (CurrentDemandResPtr->DC_EVSEStatus));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) */
    /* EE(DC_EVSEStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element EVSEPresentVoltage */
    /* SE(EVSEPresentVoltage) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (CurrentDemandResPtr->EVSEPresentVoltage));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVSEPresentVoltage) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #50 Encode element EVSEPresentCurrent */
    /* SE(EVSEPresentCurrent) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (CurrentDemandResPtr->EVSEPresentCurrent));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVSEPresentCurrent) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #60 Encode element EVSECurrentLimitAchieved */
    /* SE(EVSECurrentLimitAchieved) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeBool(&EncWsPtr->EncWs, (CurrentDemandResPtr->EVSECurrentLimitAchieved));
    /* EE(EVSECurrentLimitAchieved) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #70 Encode element EVSEVoltageLimitAchieved */
    /* SE(EVSEVoltageLimitAchieved) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeBool(&EncWsPtr->EncWs, (CurrentDemandResPtr->EVSEVoltageLimitAchieved));
    /* EE(EVSEVoltageLimitAchieved) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #80 Encode element EVSEPowerLimitAchieved */
    /* SE(EVSEPowerLimitAchieved) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeBool(&EncWsPtr->EncWs, (CurrentDemandResPtr->EVSEPowerLimitAchieved));
    /* EE(EVSEPowerLimitAchieved) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #90 If optional element EVSEMaximumVoltageLimit is included */
    if ( (1 == CurrentDemandResPtr->EVSEMaximumVoltageLimitFlag) && (NULL_PTR != CurrentDemandResPtr->EVSEMaximumVoltageLimit) )
    {
      /* #100 Encode element EVSEMaximumVoltageLimit */
      /* SE(EVSEMaximumVoltageLimit) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (CurrentDemandResPtr->EVSEMaximumVoltageLimit));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(EVSEMaximumVoltageLimit) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #110 If optional element EVSEMaximumCurrentLimit is included */
    if ( (1 == CurrentDemandResPtr->EVSEMaximumCurrentLimitFlag) && (NULL_PTR != CurrentDemandResPtr->EVSEMaximumCurrentLimit) )
    {
      /* #120 Encode element EVSEMaximumCurrentLimit */
      /* SE(EVSEMaximumCurrentLimit) */
      if(0 == CurrentDemandResPtr->EVSEMaximumVoltageLimitFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (CurrentDemandResPtr->EVSEMaximumCurrentLimit));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(EVSEMaximumCurrentLimit) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #130 If optional element EVSEMaximumPowerLimit is included */
    if ( (1 == CurrentDemandResPtr->EVSEMaximumPowerLimitFlag) && (NULL_PTR != CurrentDemandResPtr->EVSEMaximumPowerLimit) )
    {
      /* #140 Encode element EVSEMaximumPowerLimit */
      /* SE(EVSEMaximumPowerLimit) */
      if(0 == CurrentDemandResPtr->EVSEMaximumCurrentLimitFlag)
      {
        if(0 == CurrentDemandResPtr->EVSEMaximumVoltageLimitFlag)
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (CurrentDemandResPtr->EVSEMaximumPowerLimit));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(EVSEMaximumPowerLimit) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #150 Encode element EVSEID */
    /* SE(EVSEID) */
    if(0 == CurrentDemandResPtr->EVSEMaximumPowerLimitFlag)
    {
      if(0 == CurrentDemandResPtr->EVSEMaximumCurrentLimitFlag)
      {
        if(0 == CurrentDemandResPtr->EVSEMaximumVoltageLimitFlag)
        {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_ISO_EVSE_ID) && (EXI_ENCODE_ISO_EVSE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_evseID(EncWsPtr, (CurrentDemandResPtr->EVSEID));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_EVSE_ID) && (EXI_ENCODE_ISO_EVSE_ID == STD_ON)) */
    /* EE(EVSEID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #160 Encode element SAScheduleTupleID */
    /* SE(SAScheduleTupleID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(CurrentDemandResPtr->SAScheduleTupleID - 1), 8);
    /* EE(SAScheduleTupleID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #170 If optional element MeterInfo is included */
    if ( (1 == CurrentDemandResPtr->MeterInfoFlag) && (NULL_PTR != CurrentDemandResPtr->MeterInfo) )
    {
      /* #180 Encode element MeterInfo */
      /* SE(MeterInfo) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_ISO_METER_INFO) && (EXI_ENCODE_ISO_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_MeterInfo(EncWsPtr, (CurrentDemandResPtr->MeterInfo));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_RES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_METER_INFO) && (EXI_ENCODE_ISO_METER_INFO == STD_ON)) */
      /* EE(MeterInfo) */
      /* Check EE encoding for MeterInfo */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    /* #190 If optional element ReceiptRequired is included */
    if(1 == CurrentDemandResPtr->ReceiptRequiredFlag)
    {
      /* #200 Encode element ReceiptRequired */
      /* SE(ReceiptRequired) */
      if(0 == CurrentDemandResPtr->MeterInfoFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeBool(&EncWsPtr->EncWs, (CurrentDemandResPtr->ReceiptRequired));
      /* EE(ReceiptRequired) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #210 Optional element ReceiptRequired is not included */
    else
    {
      /* EE(CurrentDemandRes) */
      /* #220 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      if(0 == CurrentDemandResPtr->MeterInfoFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CURRENT_DEMAND_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CURRENT_DEMAND_RES) && (EXI_ENCODE_ISO_CURRENT_DEMAND_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_DC_EVChargeParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_DC_EVCHARGE_PARAMETER) && (EXI_ENCODE_ISO_DC_EVCHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_DC_EVChargeParameter( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_DC_EVChargeParameterType, AUTOMATIC, EXI_APPL_DATA) DC_EVChargeParameterPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DC_EVChargeParameterPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if ((DC_EVChargeParameterPtr->FullSOCFlag == 1) && ((DC_EVChargeParameterPtr->FullSOC < 0) || (DC_EVChargeParameterPtr->FullSOC > 100)))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  if ((DC_EVChargeParameterPtr->BulkSOCFlag == 1) && ((DC_EVChargeParameterPtr->BulkSOC < 0) || (DC_EVChargeParameterPtr->BulkSOC > 100)))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element DepartureTime is included */
    if(1 == DC_EVChargeParameterPtr->DepartureTimeFlag)
    {
      /* #30 Encode element DepartureTime */
      /* SE(DepartureTime) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeUInt(&EncWsPtr->EncWs, (Exi_BitBufType)(DC_EVChargeParameterPtr->DepartureTime));
      /* EE(DepartureTime) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #40 Encode element DC_EVStatus */
    /* SE(DC_EVStatus) */
    if(0 == DC_EVChargeParameterPtr->DepartureTimeFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_ISO_DC_EVSTATUS) && (EXI_ENCODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DC_EVStatus(EncWsPtr, (DC_EVChargeParameterPtr->DC_EVStatus));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DC_EVSTATUS) && (EXI_ENCODE_ISO_DC_EVSTATUS == STD_ON)) */
    /* EE(DC_EVStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #50 Encode element EVMaximumCurrentLimit */
    /* SE(EVMaximumCurrentLimit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (DC_EVChargeParameterPtr->EVMaximumCurrentLimit));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVMaximumCurrentLimit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #60 If optional element EVMaximumPowerLimit is included */
    if ( (1 == DC_EVChargeParameterPtr->EVMaximumPowerLimitFlag) && (NULL_PTR != DC_EVChargeParameterPtr->EVMaximumPowerLimit) )
    {
      /* #70 Encode element EVMaximumPowerLimit */
      /* SE(EVMaximumPowerLimit) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (DC_EVChargeParameterPtr->EVMaximumPowerLimit));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(EVMaximumPowerLimit) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #80 Encode element EVMaximumVoltageLimit */
    /* SE(EVMaximumVoltageLimit) */
    if(0 == DC_EVChargeParameterPtr->EVMaximumPowerLimitFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (DC_EVChargeParameterPtr->EVMaximumVoltageLimit));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVMaximumVoltageLimit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #90 If optional element EVEnergyCapacity is included */
    if ( (1 == DC_EVChargeParameterPtr->EVEnergyCapacityFlag) && (NULL_PTR != DC_EVChargeParameterPtr->EVEnergyCapacity) )
    {
      /* #100 Encode element EVEnergyCapacity */
      /* SE(EVEnergyCapacity) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (DC_EVChargeParameterPtr->EVEnergyCapacity));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(EVEnergyCapacity) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #110 If optional element EVEnergyRequest is included */
    if ( (1 == DC_EVChargeParameterPtr->EVEnergyRequestFlag) && (NULL_PTR != DC_EVChargeParameterPtr->EVEnergyRequest) )
    {
      /* #120 Encode element EVEnergyRequest */
      /* SE(EVEnergyRequest) */
      if(0 == DC_EVChargeParameterPtr->EVEnergyCapacityFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      }
      #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (DC_EVChargeParameterPtr->EVEnergyRequest));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVCHARGE_PARAMETER, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(EVEnergyRequest) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #130 If optional element FullSOC is included */
    if(1 == DC_EVChargeParameterPtr->FullSOCFlag)
    {
      /* #140 Encode element FullSOC */
      /* SE(FullSOC) */
      if(0 == DC_EVChargeParameterPtr->EVEnergyRequestFlag)
      {
        if(0 == DC_EVChargeParameterPtr->EVEnergyCapacityFlag)
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(DC_EVChargeParameterPtr->FullSOC), 7); /*lint !e571 */ /* Signed to unsigned cast, value is limited by the schema, lowest possible value is 0 or an offset is added to set it to 0 */
      /* EE(FullSOC) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #150 If optional element BulkSOC is included */
    if(1 == DC_EVChargeParameterPtr->BulkSOCFlag)
    {
      /* #160 Encode element BulkSOC */
      /* SE(BulkSOC) */
      if(0 == DC_EVChargeParameterPtr->FullSOCFlag)
      {
        if(0 == DC_EVChargeParameterPtr->EVEnergyRequestFlag)
        {
          if(0 == DC_EVChargeParameterPtr->EVEnergyCapacityFlag)
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
          }
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
          }
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(DC_EVChargeParameterPtr->BulkSOC), 7); /*lint !e571 */ /* Signed to unsigned cast, value is limited by the schema, lowest possible value is 0 or an offset is added to set it to 0 */
      /* EE(BulkSOC) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #170 Optional element BulkSOC is not included */
    else
    {
      /* EE(DC_EVChargeParameter) */
      /* #180 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      if(0 == DC_EVChargeParameterPtr->FullSOCFlag)
      {
        if(0 == DC_EVChargeParameterPtr->EVEnergyRequestFlag)
        {
          if(0 == DC_EVChargeParameterPtr->EVEnergyCapacityFlag)
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 4, 3);
          }
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
          }
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVCHARGE_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_DC_EVCHARGE_PARAMETER) && (EXI_ENCODE_ISO_DC_EVCHARGE_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_DC_EVErrorCode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_DC_EVERROR_CODE) && (EXI_ENCODE_ISO_DC_EVERROR_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_DC_EVErrorCode( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_DC_EVErrorCodeType, AUTOMATIC, EXI_APPL_DATA) DC_EVErrorCodePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DC_EVErrorCodePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode enumeration value DC_EVErrorCode as element */
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If enumeration value is in range */
    if (*DC_EVErrorCodePtr <= EXI_ISO_DC_EVERROR_CODE_TYPE_NO_DATA) /* PRQA S 3355, 3358 */ /* MD_Exi_13.7_14.1_Enum */ /*lint !e685 */
    {
      /* #40 Encode value */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (uint8)*DC_EVErrorCodePtr, 4);
    }
    /* #50 Enumeration is out of its allowed range */
    else
    { /* PRQA S 3201 */ /* MD_Exi_13.7_14.1_Enum */
      /* #60 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVERROR_CODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_DC_EVERROR_CODE) && (EXI_ENCODE_ISO_DC_EVERROR_CODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_DC_EVPowerDeliveryParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_ENCODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_DC_EVPowerDeliveryParameter( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_DC_EVPowerDeliveryParameterType, AUTOMATIC, EXI_APPL_DATA) DC_EVPowerDeliveryParameterPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DC_EVPowerDeliveryParameterPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element DC_EVStatus */
    /* SE(DC_EVStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_DC_EVSTATUS) && (EXI_ENCODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DC_EVStatus(EncWsPtr, (DC_EVPowerDeliveryParameterPtr->DC_EVStatus));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DC_EVSTATUS) && (EXI_ENCODE_ISO_DC_EVSTATUS == STD_ON)) */
    /* EE(DC_EVStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If optional element BulkChargingComplete is included */
    if(1 == DC_EVPowerDeliveryParameterPtr->BulkChargingCompleteFlag)
    {
      /* #40 Encode element BulkChargingComplete */
      /* SE(BulkChargingComplete) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeBool(&EncWsPtr->EncWs, (DC_EVPowerDeliveryParameterPtr->BulkChargingComplete));
      /* EE(BulkChargingComplete) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #50 Encode element ChargingComplete */
    /* SE(ChargingComplete) */
    if(0 == DC_EVPowerDeliveryParameterPtr->BulkChargingCompleteFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeBool(&EncWsPtr->EncWs, (DC_EVPowerDeliveryParameterPtr->ChargingComplete));
    /* EE(ChargingComplete) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_ENCODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_DC_EVSEChargeParameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_DC_EVSECHARGE_PARAMETER) && (EXI_ENCODE_ISO_DC_EVSECHARGE_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_DC_EVSEChargeParameter( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_DC_EVSEChargeParameterType, AUTOMATIC, EXI_APPL_DATA) DC_EVSEChargeParameterPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DC_EVSEChargeParameterPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element DC_EVSEStatus */
    /* SE(DC_EVSEStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DC_EVSEStatus(EncWsPtr, (DC_EVSEChargeParameterPtr->DC_EVSEStatus));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) */
    /* EE(DC_EVSEStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element EVSEMaximumCurrentLimit */
    /* SE(EVSEMaximumCurrentLimit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (DC_EVSEChargeParameterPtr->EVSEMaximumCurrentLimit));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVSEMaximumCurrentLimit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element EVSEMaximumPowerLimit */
    /* SE(EVSEMaximumPowerLimit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (DC_EVSEChargeParameterPtr->EVSEMaximumPowerLimit));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVSEMaximumPowerLimit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #50 Encode element EVSEMaximumVoltageLimit */
    /* SE(EVSEMaximumVoltageLimit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (DC_EVSEChargeParameterPtr->EVSEMaximumVoltageLimit));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVSEMaximumVoltageLimit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #60 Encode element EVSEMinimumCurrentLimit */
    /* SE(EVSEMinimumCurrentLimit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (DC_EVSEChargeParameterPtr->EVSEMinimumCurrentLimit));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVSEMinimumCurrentLimit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #70 Encode element EVSEMinimumVoltageLimit */
    /* SE(EVSEMinimumVoltageLimit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (DC_EVSEChargeParameterPtr->EVSEMinimumVoltageLimit));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVSEMinimumVoltageLimit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #80 If optional element EVSECurrentRegulationTolerance is included */
    if ( (1 == DC_EVSEChargeParameterPtr->EVSECurrentRegulationToleranceFlag) && (NULL_PTR != DC_EVSEChargeParameterPtr->EVSECurrentRegulationTolerance) )
    {
      /* #90 Encode element EVSECurrentRegulationTolerance */
      /* SE(EVSECurrentRegulationTolerance) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (DC_EVSEChargeParameterPtr->EVSECurrentRegulationTolerance));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(EVSECurrentRegulationTolerance) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #100 Encode element EVSEPeakCurrentRipple */
    /* SE(EVSEPeakCurrentRipple) */
    if(0 == DC_EVSEChargeParameterPtr->EVSECurrentRegulationToleranceFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (DC_EVSEChargeParameterPtr->EVSEPeakCurrentRipple));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVSEPeakCurrentRipple) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #110 If optional element EVSEEnergyToBeDelivered is included */
    if ( (1 == DC_EVSEChargeParameterPtr->EVSEEnergyToBeDeliveredFlag) && (NULL_PTR != DC_EVSEChargeParameterPtr->EVSEEnergyToBeDelivered) )
    {
      /* #120 Encode element EVSEEnergyToBeDelivered */
      /* SE(EVSEEnergyToBeDelivered) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (DC_EVSEChargeParameterPtr->EVSEEnergyToBeDelivered));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSECHARGE_PARAMETER, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(EVSEEnergyToBeDelivered) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #130 Optional element EVSEEnergyToBeDelivered is not included */
    else
    {
      /* EE(DC_EVSEChargeParameter) */
      /* #140 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSECHARGE_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_DC_EVSECHARGE_PARAMETER) && (EXI_ENCODE_ISO_DC_EVSECHARGE_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_DC_EVSEStatusCode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_DC_EVSESTATUS_CODE) && (EXI_ENCODE_ISO_DC_EVSESTATUS_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_DC_EVSEStatusCode( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_DC_EVSEStatusCodeType, AUTOMATIC, EXI_APPL_DATA) DC_EVSEStatusCodePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DC_EVSEStatusCodePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode enumeration value DC_EVSEStatusCode as element */
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If enumeration value is in range */
    if (*DC_EVSEStatusCodePtr <= EXI_ISO_DC_EVSESTATUS_CODE_TYPE_RESERVED_C) /* PRQA S 3355, 3358 */ /* MD_Exi_13.7_14.1_Enum */ /*lint !e685 */
    {
      /* #40 Encode value */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (uint8)*DC_EVSEStatusCodePtr, 4);
    }
    /* #50 Enumeration is out of its allowed range */
    else
    { /* PRQA S 3201 */ /* MD_Exi_13.7_14.1_Enum */
      /* #60 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSESTATUS_CODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_DC_EVSESTATUS_CODE) && (EXI_ENCODE_ISO_DC_EVSESTATUS_CODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_DC_EVSEStatus
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_DC_EVSEStatus( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_DC_EVSEStatusType, AUTOMATIC, EXI_APPL_DATA) DC_EVSEStatusPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DC_EVSEStatusPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element NotificationMaxDelay */
    /* SE(NotificationMaxDelay) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUInt(&EncWsPtr->EncWs, (Exi_BitBufType)(DC_EVSEStatusPtr->NotificationMaxDelay));
    /* EE(NotificationMaxDelay) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element EVSENotification */
    /* SE(EVSENotification) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_EVSENOTIFICATION) && (EXI_ENCODE_ISO_EVSENOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_EVSENotification(EncWsPtr, &(DC_EVSEStatusPtr->EVSENotification));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSESTATUS, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_EVSENOTIFICATION) && (EXI_ENCODE_ISO_EVSENOTIFICATION == STD_ON)) */
    /* EE(EVSENotification) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 If optional element EVSEIsolationStatus is included */
    if(1 == DC_EVSEStatusPtr->EVSEIsolationStatusFlag)
    {
      /* #50 Encode element EVSEIsolationStatus */
      /* SE(EVSEIsolationStatus) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_ISOLATION_LEVEL) && (EXI_ENCODE_ISO_ISOLATION_LEVEL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_isolationLevel(EncWsPtr, &(DC_EVSEStatusPtr->EVSEIsolationStatus));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSESTATUS, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_ISOLATION_LEVEL) && (EXI_ENCODE_ISO_ISOLATION_LEVEL == STD_ON)) */
      /* EE(EVSEIsolationStatus) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #60 Encode element EVSEStatusCode */
    /* SE(EVSEStatusCode) */
    if(0 == DC_EVSEStatusPtr->EVSEIsolationStatusFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_ISO_DC_EVSESTATUS_CODE) && (EXI_ENCODE_ISO_DC_EVSESTATUS_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DC_EVSEStatusCode(EncWsPtr, &(DC_EVSEStatusPtr->EVSEStatusCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSESTATUS, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DC_EVSESTATUS_CODE) && (EXI_ENCODE_ISO_DC_EVSESTATUS_CODE == STD_ON)) */
    /* EE(EVSEStatusCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSESTATUS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_DC_EVStatus
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_DC_EVSTATUS) && (EXI_ENCODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_DC_EVStatus( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_DC_EVStatusType, AUTOMATIC, EXI_APPL_DATA) DC_EVStatusPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DC_EVStatusPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if ((DC_EVStatusPtr->EVRESSSOC < 0) || (DC_EVStatusPtr->EVRESSSOC > 100))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element EVReady */
    /* SE(EVReady) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeBool(&EncWsPtr->EncWs, (DC_EVStatusPtr->EVReady));
    /* EE(EVReady) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element EVErrorCode */
    /* SE(EVErrorCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_DC_EVERROR_CODE) && (EXI_ENCODE_ISO_DC_EVERROR_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DC_EVErrorCode(EncWsPtr, &(DC_EVStatusPtr->EVErrorCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSTATUS, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DC_EVERROR_CODE) && (EXI_ENCODE_ISO_DC_EVERROR_CODE == STD_ON)) */
    /* EE(EVErrorCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element EVRESSSOC */
    /* SE(EVRESSSOC) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(DC_EVStatusPtr->EVRESSSOC), 7); /*lint !e571 */ /* Signed to unsigned cast, value is limited by the schema, lowest possible value is 0 or an offset is added to set it to 0 */
    /* EE(EVRESSSOC) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DC_EVSTATUS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_DC_EVSTATUS) && (EXI_ENCODE_ISO_DC_EVSTATUS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_DiffieHellmanPublickey
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY) && (EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_DiffieHellmanPublickey( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_DiffieHellmanPublickeyType, AUTOMATIC, EXI_APPL_DATA) DiffieHellmanPublickeyPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DiffieHellmanPublickeyPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (DiffieHellmanPublickeyPtr->Length > sizeof(DiffieHellmanPublickeyPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode attribute Id */
    /* AT(Id) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_AttributeId(EncWsPtr, (DiffieHellmanPublickeyPtr->Id));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) */

    /* #30 Start content of DiffieHellmanPublickey */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode DiffieHellmanPublickey as byte array */
    Exi_VBSEncodeBytes(&EncWsPtr->EncWs, &DiffieHellmanPublickeyPtr->Buffer[0], DiffieHellmanPublickeyPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY) && (EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_EMAID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_EMAID) && (EXI_ENCODE_ISO_EMAID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_EMAID( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_EMAIDType, AUTOMATIC, EXI_APPL_DATA) EMAIDPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EMAIDPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (EMAIDPtr->Length > sizeof(EMAIDPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode attribute Id */
    /* AT(Id) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_AttributeId(EncWsPtr, (EMAIDPtr->Id));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_EMAID, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) */

    /* #30 Start content of EMAID */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode EMAID as string value */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &EMAIDPtr->Buffer[0], EMAIDPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_EMAID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_EMAID) && (EXI_ENCODE_ISO_EMAID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_EMAID_ElementFragment
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_EMAID_ELEMENT_FRAGMENT) && (EXI_ENCODE_ISO_EMAID_ELEMENT_FRAGMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_EMAID_ElementFragment( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_EMAIDType, AUTOMATIC, EXI_APPL_DATA) EMAIDPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EMAIDPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (EMAIDPtr->Length > sizeof(EMAIDPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode attribute Id */
    /* AT(Id) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 4, 9);
    #if (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_AttributeId(EncWsPtr, (EMAIDPtr->Id));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_EMAID, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) */

    /* CH[untyped value] */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 256, 9);
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &EMAIDPtr->Buffer[0], EMAIDPtr->Length);
    /* EE */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 244, 8);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_EMAID_ELEMENT_FRAGMENT, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_EMAID_ELEMENT_FRAGMENT) && (EXI_ENCODE_ISO_EMAID_ELEMENT_FRAGMENT == STD_ON)) */


/* Encode API for abstract type Exi_ISO_EVChargeParameterType not required */

/* Encode API for abstract type Exi_ISO_EVPowerDeliveryParameterType not required */

/* Encode API for abstract type Exi_ISO_EVSEChargeParameterType not required */

/**********************************************************************************************************************
 *  Exi_Encode_ISO_EVSENotification
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_EVSENOTIFICATION) && (EXI_ENCODE_ISO_EVSENOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_EVSENotification( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_EVSENotificationType, AUTOMATIC, EXI_APPL_DATA) EVSENotificationPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EVSENotificationPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode enumeration value EVSENotification as element */
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If enumeration value is in range */
    if (*EVSENotificationPtr <= EXI_ISO_EVSENOTIFICATION_TYPE_RE_NEGOTIATION) /* PRQA S 3355, 3358 */ /* MD_Exi_13.7_14.1_Enum */ /*lint !e685 */
    {
      /* #40 Encode value */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (uint8)*EVSENotificationPtr, 2);
    }
    /* #50 Enumeration is out of its allowed range */
    else
    { /* PRQA S 3201 */ /* MD_Exi_13.7_14.1_Enum */
      /* #60 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_EVSENOTIFICATION, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_EVSENOTIFICATION) && (EXI_ENCODE_ISO_EVSENOTIFICATION == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_EVSEProcessing
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_EVSEPROCESSING) && (EXI_ENCODE_ISO_EVSEPROCESSING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_EVSEProcessing( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_EVSEProcessingType, AUTOMATIC, EXI_APPL_DATA) EVSEProcessingPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EVSEProcessingPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode enumeration value EVSEProcessing as element */
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If enumeration value is in range */
    if (*EVSEProcessingPtr <= EXI_ISO_EVSEPROCESSING_TYPE_ONGOING_WAITING_FOR_CUSTOMER_INTERACTION) /* PRQA S 3355, 3358 */ /* MD_Exi_13.7_14.1_Enum */ /*lint !e685 */
    {
      /* #40 Encode value */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (uint8)*EVSEProcessingPtr, 2);
    }
    /* #50 Enumeration is out of its allowed range */
    else
    { /* PRQA S 3201 */ /* MD_Exi_13.7_14.1_Enum */
      /* #60 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_EVSEPROCESSING, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_EVSEPROCESSING) && (EXI_ENCODE_ISO_EVSEPROCESSING == STD_ON)) */


/* Encode API for abstract type Exi_ISO_EVSEStatusType not required */

/* Encode API for abstract type Exi_ISO_EVStatusType not required */

/**********************************************************************************************************************
 *  Exi_Encode_ISO_EnergyTransferMode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_ENCODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_EnergyTransferMode( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_EnergyTransferModeType, AUTOMATIC, EXI_APPL_DATA) EnergyTransferModePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EnergyTransferModePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode enumeration value EnergyTransferMode as element */
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If enumeration value is in range */
    if (*EnergyTransferModePtr <= EXI_ISO_ENERGY_TRANSFER_MODE_TYPE_DC_UNIQUE) /* PRQA S 3355, 3358 */ /* MD_Exi_13.7_14.1_Enum */ /*lint !e685 */
    {
      /* #40 Encode value */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (uint8)*EnergyTransferModePtr, 3);
    }
    /* #50 Enumeration is out of its allowed range */
    else
    { /* PRQA S 3201 */ /* MD_Exi_13.7_14.1_Enum */
      /* #60 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_ENERGY_TRANSFER_MODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_ENCODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) */


/* Encode API for abstract type Exi_ISO_EntryType not required */

/* Encode API for abstract type Exi_ISO_IntervalType not required */

/**********************************************************************************************************************
 *  Exi_Encode_ISO_ListOfRootCertificateIDs
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ListOfRootCertificateIDs( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ListOfRootCertificateIDsType, AUTOMATIC, EXI_APPL_DATA) ListOfRootCertificateIDsPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_X509IssuerSerialType) nextPtr;
  uint8_least i;
  #endif /* #if (defined(EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ListOfRootCertificateIDsPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    #if (defined(EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #20 Initialize next pointer with the first RootCertificateID element */
    nextPtr = (Exi_XMLSIG_X509IssuerSerialType*)ListOfRootCertificateIDsPtr->RootCertificateID;
    /* #30 Loop over all RootCertificateID elements */
    for(i=0; i<20; i++)
    {
      /* #40 Encode element RootCertificateID */
      /* SE(RootCertificateID) */
      if(0 == i)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }

      Exi_Encode_XMLSIG_X509IssuerSerial(EncWsPtr, nextPtr);
      /* EE(RootCertificateID) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_XMLSIG_X509IssuerSerialType*)(nextPtr->NextRootCertificateIDPtr);
      /* #50 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* i holds the number of encoded Exi_XMLSIG_X509IssuerSerialType elements */
        i++; /* PRQA S 2469 */  /*  MD_Exi_13.6 */
        /* #60 End the loop */
        break;
      }
    }
    /* #70 If maximum possible number of ListOfRootCertificateIDs's was encoded */
    if(i == 20)
    {
      /* #80 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #90 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG,EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    /* #100 If element list does not have maximum length */
    if( i < 20)
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) */
    {
      /* EE(ListOfRootCertificateIDs) */
      /* #110 Encode end element tag is requrired */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS) && (EXI_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_MessageHeader
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_MESSAGE_HEADER) && (EXI_ENCODE_ISO_MESSAGE_HEADER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_MessageHeader( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_MessageHeaderType, AUTOMATIC, EXI_APPL_DATA) MessageHeaderPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (MessageHeaderPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element SessionID */
    /* SE(SessionID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_SESSION_ID) && (EXI_ENCODE_ISO_SESSION_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_sessionID(EncWsPtr, (MessageHeaderPtr->SessionID));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_MESSAGE_HEADER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_SESSION_ID) && (EXI_ENCODE_ISO_SESSION_ID == STD_ON)) */
    /* EE(SessionID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If optional element Notification is included */
    if ( (1 == MessageHeaderPtr->NotificationFlag) && (NULL_PTR != MessageHeaderPtr->Notification) )
    {
      /* #40 Encode element Notification */
      /* SE(Notification) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_ISO_NOTIFICATION) && (EXI_ENCODE_ISO_NOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_Notification(EncWsPtr, (MessageHeaderPtr->Notification));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_MESSAGE_HEADER, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_NOTIFICATION) && (EXI_ENCODE_ISO_NOTIFICATION == STD_ON)) */
      /* EE(Notification) */
      /* Check EE encoding for Notification */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    /* #50 If optional element Signature is included */
    if ( (1 == MessageHeaderPtr->SignatureFlag) && (NULL_PTR != MessageHeaderPtr->Signature) )
    {
      /* #60 Encode element Signature */
      /* SE(Signature) */
      if(0 == MessageHeaderPtr->NotificationFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE) && (EXI_ENCODE_XMLSIG_SIGNATURE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_Signature(EncWsPtr, (MessageHeaderPtr->Signature));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_MESSAGE_HEADER, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_SIGNATURE) && (EXI_ENCODE_XMLSIG_SIGNATURE == STD_ON)) */
      /* EE(Signature) */
      /* Check EE encoding for Signature */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    /* #70 Optional element Signature is not included */
    else
    {
      /* EE(MessageHeader) */
      /* #80 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      if(0 == MessageHeaderPtr->NotificationFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_MESSAGE_HEADER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_MESSAGE_HEADER) && (EXI_ENCODE_ISO_MESSAGE_HEADER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_MeterInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_METER_INFO) && (EXI_ENCODE_ISO_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_MeterInfo( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_MeterInfoType, AUTOMATIC, EXI_APPL_DATA) MeterInfoPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (MeterInfoPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element MeterID */
    /* SE(MeterID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_METER_ID) && (EXI_ENCODE_ISO_METER_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_meterID(EncWsPtr, (MeterInfoPtr->MeterID));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_METER_INFO, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_METER_ID) && (EXI_ENCODE_ISO_METER_ID == STD_ON)) */
    /* EE(MeterID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If optional element MeterReading is included */
    if(1 == MeterInfoPtr->MeterReadingFlag)
    {
      /* #40 Encode element MeterReading */
      /* SE(MeterReading) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeUInt64(&EncWsPtr->EncWs, (MeterInfoPtr->MeterReading));
      /* EE(MeterReading) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #50 If optional element SigMeterReading is included */
    if ( (1 == MeterInfoPtr->SigMeterReadingFlag) && (NULL_PTR != MeterInfoPtr->SigMeterReading) )
    {
      /* #60 Encode element SigMeterReading */
      /* SE(SigMeterReading) */
      if(0 == MeterInfoPtr->MeterReadingFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      }
      #if (defined(EXI_ENCODE_ISO_SIG_METER_READING) && (EXI_ENCODE_ISO_SIG_METER_READING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_sigMeterReading(EncWsPtr, (MeterInfoPtr->SigMeterReading));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_METER_INFO, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_SIG_METER_READING) && (EXI_ENCODE_ISO_SIG_METER_READING == STD_ON)) */
      /* EE(SigMeterReading) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #70 If optional element MeterStatus is included */
    if(1 == MeterInfoPtr->MeterStatusFlag)
    {
      /* #80 Encode element MeterStatus */
      /* SE(MeterStatus) */
      if(0 == MeterInfoPtr->SigMeterReadingFlag)
      {
        if(0 == MeterInfoPtr->MeterReadingFlag)
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeInt(&EncWsPtr->EncWs, (sint32)(MeterInfoPtr->MeterStatus));
      /* EE(MeterStatus) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #90 If optional element TMeter is included */
    if(1 == MeterInfoPtr->TMeterFlag)
    {
      /* #100 Encode element TMeter */
      /* SE(TMeter) */
      if(0 == MeterInfoPtr->MeterStatusFlag)
      {
        if(0 == MeterInfoPtr->SigMeterReadingFlag)
        {
          if(0 == MeterInfoPtr->MeterReadingFlag)
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
          }
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
          }
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeInt64(&EncWsPtr->EncWs, (MeterInfoPtr->TMeter));
      /* EE(TMeter) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #110 Optional element TMeter is not included */
    else
    {
      /* EE(MeterInfo) */
      /* #120 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      if(0 == MeterInfoPtr->MeterStatusFlag)
      {
        if(0 == MeterInfoPtr->SigMeterReadingFlag)
        {
          if(0 == MeterInfoPtr->MeterReadingFlag)
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 4, 3);
          }
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
          }
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_METER_INFO, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_METER_INFO) && (EXI_ENCODE_ISO_METER_INFO == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_MeteringReceiptReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_METERING_RECEIPT_REQ) && (EXI_ENCODE_ISO_METERING_RECEIPT_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_MeteringReceiptReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_MeteringReceiptReqType, AUTOMATIC, EXI_APPL_DATA) MeteringReceiptReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (MeteringReceiptReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if ((MeteringReceiptReqPtr->SAScheduleTupleIDFlag == 1) && (MeteringReceiptReqPtr->SAScheduleTupleID < 1))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element Id is included */
    if ( (1 == MeteringReceiptReqPtr->IdFlag) && (NULL_PTR != MeteringReceiptReqPtr->Id) )
    {
      /* #30 Encode attribute Id */
      /* AT(Id) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_AttributeId(EncWsPtr, (MeteringReceiptReqPtr->Id));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_METERING_RECEIPT_REQ, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) */

    }
    /* #40 Encode element SessionID */
    /* SE(SessionID) */
    if(0 == MeteringReceiptReqPtr->IdFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_ISO_SESSION_ID) && (EXI_ENCODE_ISO_SESSION_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_sessionID(EncWsPtr, (MeteringReceiptReqPtr->SessionID));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_METERING_RECEIPT_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_SESSION_ID) && (EXI_ENCODE_ISO_SESSION_ID == STD_ON)) */
    /* EE(SessionID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #50 If optional element SAScheduleTupleID is included */
    if(1 == MeteringReceiptReqPtr->SAScheduleTupleIDFlag)
    {
      /* #60 Encode element SAScheduleTupleID */
      /* SE(SAScheduleTupleID) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(MeteringReceiptReqPtr->SAScheduleTupleID - 1), 8);
      /* EE(SAScheduleTupleID) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #70 Encode element MeterInfo */
    /* SE(MeterInfo) */
    if(0 == MeteringReceiptReqPtr->SAScheduleTupleIDFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_METER_INFO) && (EXI_ENCODE_ISO_METER_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_MeterInfo(EncWsPtr, (MeteringReceiptReqPtr->MeterInfo));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_METERING_RECEIPT_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_METER_INFO) && (EXI_ENCODE_ISO_METER_INFO == STD_ON)) */
    /* EE(MeterInfo) */
    /* Check EE encoding for MeterInfo */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_METERING_RECEIPT_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_METERING_RECEIPT_REQ) && (EXI_ENCODE_ISO_METERING_RECEIPT_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_MeteringReceiptRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_METERING_RECEIPT_RES) && (EXI_ENCODE_ISO_METERING_RECEIPT_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_MeteringReceiptRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_MeteringReceiptResType, AUTOMATIC, EXI_APPL_DATA) MeteringReceiptResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (MeteringReceiptResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(MeteringReceiptResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_METERING_RECEIPT_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Start of Substitution Group EVSEStatus */
    /* #40 Switch EVSEStatusElementId */
    switch(MeteringReceiptResPtr->EVSEStatusElementId)
    {
    case EXI_ISO_AC_EVSESTATUS_TYPE:
      /* #50 Substitution element AC_EVSEStatus */
      {
        /* #60 Encode element AC_EVSEStatus */
      #if (defined(EXI_ENCODE_ISO_AC_EVSESTATUS) && (EXI_ENCODE_ISO_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* SE(AC_EVSEStatus) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
        /* NULL_PTR check is done in the called API */
        Exi_Encode_ISO_AC_EVSEStatus(EncWsPtr, (P2CONST(Exi_ISO_AC_EVSEStatusType, AUTOMATIC, EXI_APPL_DATA))MeteringReceiptResPtr->EVSEStatus); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* EE(AC_EVSEStatus) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_METERING_RECEIPT_RES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_AC_EVSESTATUS) && (EXI_ENCODE_ISO_AC_EVSESTATUS == STD_ON)) */
        break;
      }
    case EXI_ISO_DC_EVSESTATUS_TYPE:
      /* #70 Substitution element DC_EVSEStatus */
      {
        /* #80 Encode element DC_EVSEStatus */
      #if (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* SE(DC_EVSEStatus) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
        EncWsPtr->EncWs.EERequired = TRUE;
        /* NULL_PTR check is done in the called API */
        Exi_Encode_ISO_DC_EVSEStatus(EncWsPtr, (P2CONST(Exi_ISO_DC_EVSEStatusType, AUTOMATIC, EXI_APPL_DATA))MeteringReceiptResPtr->EVSEStatus); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* EE(DC_EVSEStatus) */
        /* Check EE encoding for DC_EVSEStatus */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_METERING_RECEIPT_RES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) */
        break;
      }
    /* case EXI_ISO_EVSESTATUS_TYPE: Substitution element EVSEStatus not required, element is abstract*/
    default:
      /* #90 Default path */
      {
        /* Substitution Element not supported */
        /* #100 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    } /* switch(MeteringReceiptResPtr->EVSEStatusElementId) */
    /* End of Substitution Group */
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_METERING_RECEIPT_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_METERING_RECEIPT_RES) && (EXI_ENCODE_ISO_METERING_RECEIPT_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_Notification
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_NOTIFICATION) && (EXI_ENCODE_ISO_NOTIFICATION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_Notification( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_NotificationType, AUTOMATIC, EXI_APPL_DATA) NotificationPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (NotificationPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element FaultCode */
    /* SE(FaultCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_FAULT_CODE) && (EXI_ENCODE_ISO_FAULT_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_faultCode(EncWsPtr, &(NotificationPtr->FaultCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_NOTIFICATION, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_FAULT_CODE) && (EXI_ENCODE_ISO_FAULT_CODE == STD_ON)) */
    /* EE(FaultCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If optional element FaultMsg is included */
    if ( (1 == NotificationPtr->FaultMsgFlag) && (NULL_PTR != NotificationPtr->FaultMsg) )
    {
      /* #40 Encode element FaultMsg */
      /* SE(FaultMsg) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_FAULT_MSG) && (EXI_ENCODE_ISO_FAULT_MSG == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_faultMsg(EncWsPtr, (NotificationPtr->FaultMsg));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_NOTIFICATION, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_FAULT_MSG) && (EXI_ENCODE_ISO_FAULT_MSG == STD_ON)) */
      /* EE(FaultMsg) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #50 Optional element FaultMsg is not included */
    else
    {
      /* EE(Notification) */
      /* #60 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_NOTIFICATION, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_NOTIFICATION) && (EXI_ENCODE_ISO_NOTIFICATION == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_PMaxScheduleEntry
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_PMAX_SCHEDULE_ENTRY) && (EXI_ENCODE_ISO_PMAX_SCHEDULE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_PMaxScheduleEntry( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_PMaxScheduleEntryType, AUTOMATIC, EXI_APPL_DATA) PMaxScheduleEntryPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PMaxScheduleEntryPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start of Substitution Group TimeInterval */
    /* #30 Switch TimeIntervalElementId */
    switch(PMaxScheduleEntryPtr->TimeIntervalElementId)
    {
    case EXI_ISO_RELATIVE_TIME_INTERVAL_TYPE:
      /* #40 Substitution element RelativeTimeInterval */
      {
        /* #50 Encode element RelativeTimeInterval */
      #if (defined(EXI_ENCODE_ISO_RELATIVE_TIME_INTERVAL) && (EXI_ENCODE_ISO_RELATIVE_TIME_INTERVAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* SE(RelativeTimeInterval) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
        EncWsPtr->EncWs.EERequired = TRUE;
        /* NULL_PTR check is done in the called API */
        Exi_Encode_ISO_RelativeTimeInterval(EncWsPtr, (P2CONST(Exi_ISO_RelativeTimeIntervalType, AUTOMATIC, EXI_APPL_DATA))PMaxScheduleEntryPtr->TimeInterval); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* EE(RelativeTimeInterval) */
        /* Check EE encoding for RelativeTimeInterval */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PMAX_SCHEDULE_ENTRY, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_RELATIVE_TIME_INTERVAL) && (EXI_ENCODE_ISO_RELATIVE_TIME_INTERVAL == STD_ON)) */
        break;
      }
    /* case EXI_ISO_TIME_INTERVAL_TYPE: Substitution element TimeInterval not required, element is abstract*/
    default:
      /* #60 Default path */
      {
        /* Substitution Element not supported */
        /* #70 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    } /* switch(PMaxScheduleEntryPtr->TimeIntervalElementId) */
    /* End of Substitution Group */
    /* #80 Encode element PMax */
    /* SE(PMax) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (PMaxScheduleEntryPtr->PMax));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PMAX_SCHEDULE_ENTRY, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(PMax) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PMAX_SCHEDULE_ENTRY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_PMAX_SCHEDULE_ENTRY) && (EXI_ENCODE_ISO_PMAX_SCHEDULE_ENTRY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_PMaxSchedule
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_PMAX_SCHEDULE) && (EXI_ENCODE_ISO_PMAX_SCHEDULE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_PMaxSchedule( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_PMaxScheduleType, AUTOMATIC, EXI_APPL_DATA) PMaxSchedulePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_ISO_PMAX_SCHEDULE_ENTRY) && (EXI_ENCODE_ISO_PMAX_SCHEDULE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_PMaxScheduleEntryType) nextPtr;
  uint16_least i;
  #endif /* #if (defined(EXI_ENCODE_ISO_PMAX_SCHEDULE_ENTRY) && (EXI_ENCODE_ISO_PMAX_SCHEDULE_ENTRY == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PMaxSchedulePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    #if (defined(EXI_ENCODE_ISO_PMAX_SCHEDULE_ENTRY) && (EXI_ENCODE_ISO_PMAX_SCHEDULE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #20 Initialize next pointer with the first PMaxScheduleEntry element */
    nextPtr = (Exi_ISO_PMaxScheduleEntryType*)PMaxSchedulePtr->PMaxScheduleEntry;
    /* #30 Loop over all PMaxScheduleEntry elements */
    for(i=0; i<1024; i++)
    {
      /* #40 Encode element PMaxScheduleEntry */
      /* SE(PMaxScheduleEntry) */
      if(0 == i)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }

      Exi_Encode_ISO_PMaxScheduleEntry(EncWsPtr, nextPtr);
      /* EE(PMaxScheduleEntry) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_ISO_PMaxScheduleEntryType*)(nextPtr->NextPMaxScheduleEntryPtr);
      /* #50 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* i holds the number of encoded Exi_ISO_PMaxScheduleEntryType elements */
        i++; /* PRQA S 2469 */  /*  MD_Exi_13.6 */
        /* #60 End the loop */
        break;
      }
    }
    /* #70 If maximum possible number of PMaxSchedule's was encoded */
    if(i == 1024)
    {
      /* #80 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #90 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG,EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    /* #100 If element list does not have maximum length */
    if( i < 1024)
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PMAX_SCHEDULE, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_ISO_PMAX_SCHEDULE_ENTRY) && (EXI_ENCODE_ISO_PMAX_SCHEDULE_ENTRY == STD_ON)) */
    {
      /* EE(PMaxSchedule) */
      /* #110 Encode end element tag is requrired */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PMAX_SCHEDULE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_PMAX_SCHEDULE) && (EXI_ENCODE_ISO_PMAX_SCHEDULE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ParameterSet
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_PARAMETER_SET) && (EXI_ENCODE_ISO_PARAMETER_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ParameterSet( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ParameterSetType, AUTOMATIC, EXI_APPL_DATA) ParameterSetPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_ISO_PARAMETER) && (EXI_ENCODE_ISO_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_ParameterType) nextPtr;
  uint8_least i;
  #endif /* #if (defined(EXI_ENCODE_ISO_PARAMETER) && (EXI_ENCODE_ISO_PARAMETER == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ParameterSetPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ParameterSetID */
    /* SE(ParameterSetID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeInt(&EncWsPtr->EncWs, (sint32)(ParameterSetPtr->ParameterSetID));
    /* EE(ParameterSetID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PARAMETER) && (EXI_ENCODE_ISO_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #30 Initialize next pointer with the first Parameter element */
    nextPtr = (Exi_ISO_ParameterType*)ParameterSetPtr->Parameter;
    /* #40 Loop over all Parameter elements */
    for(i=0; i<16; i++)
    {
      /* #50 Encode element Parameter */
      /* SE(Parameter) */
      if(0 == i)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }

      Exi_Encode_ISO_Parameter(EncWsPtr, nextPtr);
      /* EE(Parameter) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_ISO_ParameterType*)(nextPtr->NextParameterPtr);
      /* #60 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* i holds the number of encoded Exi_ISO_ParameterType elements */
        i++; /* PRQA S 2469 */  /*  MD_Exi_13.6 */
        /* #70 End the loop */
        break;
      }
    }
    /* #80 If maximum possible number of ParameterSet's was encoded */
    if(i == 16)
    {
      /* #90 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #100 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG,EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    /* #110 If element list does not have maximum length */
    if( i < 16)
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PARAMETER_SET, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_ISO_PARAMETER) && (EXI_ENCODE_ISO_PARAMETER == STD_ON)) */
    {
      /* EE(ParameterSet) */
      /* #120 Encode end element tag is requrired */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PARAMETER_SET, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_PARAMETER_SET) && (EXI_ENCODE_ISO_PARAMETER_SET == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_Parameter
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_PARAMETER) && (EXI_ENCODE_ISO_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_Parameter( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ParameterType, AUTOMATIC, EXI_APPL_DATA) ParameterPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ParameterPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (ParameterPtr->ChoiceElement == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode attribute Name */
    /* AT(Name) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_ATTRIBUTE_NAME) && (EXI_ENCODE_ISO_ATTRIBUTE_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_AttributeName(EncWsPtr, (ParameterPtr->Name));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_ATTRIBUTE_NAME) && (EXI_ENCODE_ISO_ATTRIBUTE_NAME == STD_ON)) */

    /* #30 Start of choice element Parameter */
    /* #40 If not exact one choice element flag is set */
    if (1 != (  ParameterPtr->ChoiceElement->boolValueFlag
              + ParameterPtr->ChoiceElement->byteValueFlag
              + ParameterPtr->ChoiceElement->shortValueFlag
              + ParameterPtr->ChoiceElement->intValueFlag
              + ParameterPtr->ChoiceElement->physicalValueFlag
              + ParameterPtr->ChoiceElement->stringValueFlag) )
    {
      /* #50 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_CHOICE_SELECTION, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_ENCODE_ISO_PARAMETER, EXI_E_INV_PARAM);
      return;
    }
    /* #60 If choice element is boolValue */
    else if(1 == ParameterPtr->ChoiceElement->boolValueFlag)
    {
      /* #70 Encode boolValue element */
      /* SE(boolValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeBool(&EncWsPtr->EncWs, (ParameterPtr->ChoiceElement->ChoiceValue.boolValue));
      /* EE(boolValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #80 If choice element is byteValue */
    else if(1 == ParameterPtr->ChoiceElement->byteValueFlag)
    {
      /* #90 Encode byteValue element */
      /* SE(byteValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(ParameterPtr->ChoiceElement->ChoiceValue.byteValue + 128), 8); /*lint !e571 */ /* Signed to unsigned cast, value is limited by the schema, lowest possible value is 0 or an offset is added to set it to 0 */
      /* EE(byteValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #100 If choice element is shortValue */
    else if(1 == ParameterPtr->ChoiceElement->shortValueFlag)
    {
      /* #110 Encode shortValue element */
      /* SE(shortValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeInt(&EncWsPtr->EncWs, (sint32)(ParameterPtr->ChoiceElement->ChoiceValue.shortValue));
      /* EE(shortValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #120 If choice element is intValue */
    else if(1 == ParameterPtr->ChoiceElement->intValueFlag)
    {
      /* #130 Encode intValue element */
      /* SE(intValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeInt(&EncWsPtr->EncWs, (sint32)(ParameterPtr->ChoiceElement->ChoiceValue.intValue));
      /* EE(intValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #140 If choice element is physicalValue */
    else if(1 == ParameterPtr->ChoiceElement->physicalValueFlag)
    {
      /* #150 Encode physicalValue element */
      /* SE(physicalValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 4, 3);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_PhysicalValue(EncWsPtr, (ParameterPtr->ChoiceElement->ChoiceValue.physicalValue));
    #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
      /* EE(physicalValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #160 If choice element is stringValue */
    else if(1 == ParameterPtr->ChoiceElement->stringValueFlag)
    {
      /* #170 Encode stringValue element */
      /* SE(stringValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 5, 3);
    #if (defined(EXI_ENCODE_STRING) && (EXI_ENCODE_STRING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_string(EncWsPtr, (ParameterPtr->ChoiceElement->ChoiceValue.stringValue));
    #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PARAMETER, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_STRING) && (EXI_ENCODE_STRING == STD_ON)) */
      /* EE(stringValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    else
    {
      /* Choice Element not supported */
      /* #180 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* End of Choice Element */
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PARAMETER, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_PARAMETER) && (EXI_ENCODE_ISO_PARAMETER == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_PaymentDetailsReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_PAYMENT_DETAILS_REQ) && (EXI_ENCODE_ISO_PAYMENT_DETAILS_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_PaymentDetailsReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_PaymentDetailsReqType, AUTOMATIC, EXI_APPL_DATA) PaymentDetailsReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PaymentDetailsReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element eMAID */
    /* SE(eMAID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_E_MAID) && (EXI_ENCODE_ISO_E_MAID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_eMAID(EncWsPtr, (PaymentDetailsReqPtr->eMAID));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PAYMENT_DETAILS_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_E_MAID) && (EXI_ENCODE_ISO_E_MAID == STD_ON)) */
    /* EE(eMAID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element ContractSignatureCertChain */
    /* SE(ContractSignatureCertChain) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_CertificateChain(EncWsPtr, (PaymentDetailsReqPtr->ContractSignatureCertChain));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PAYMENT_DETAILS_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */
    /* EE(ContractSignatureCertChain) */
    /* Check EE encoding for ContractSignatureCertChain */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PAYMENT_DETAILS_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_PAYMENT_DETAILS_REQ) && (EXI_ENCODE_ISO_PAYMENT_DETAILS_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_PaymentDetailsRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_PAYMENT_DETAILS_RES) && (EXI_ENCODE_ISO_PAYMENT_DETAILS_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_PaymentDetailsRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_PaymentDetailsResType, AUTOMATIC, EXI_APPL_DATA) PaymentDetailsResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PaymentDetailsResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(PaymentDetailsResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PAYMENT_DETAILS_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element GenChallenge */
    /* SE(GenChallenge) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_GEN_CHALLENGE) && (EXI_ENCODE_ISO_GEN_CHALLENGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_genChallenge(EncWsPtr, (PaymentDetailsResPtr->GenChallenge));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PAYMENT_DETAILS_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_GEN_CHALLENGE) && (EXI_ENCODE_ISO_GEN_CHALLENGE == STD_ON)) */
    /* EE(GenChallenge) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element EVSETimeStamp */
    /* SE(EVSETimeStamp) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeInt64(&EncWsPtr->EncWs, (PaymentDetailsResPtr->EVSETimeStamp));
    /* EE(EVSETimeStamp) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PAYMENT_DETAILS_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_PAYMENT_DETAILS_RES) && (EXI_ENCODE_ISO_PAYMENT_DETAILS_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_PaymentOptionList
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_PAYMENT_OPTION_LIST) && (EXI_ENCODE_ISO_PAYMENT_OPTION_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_PaymentOptionList( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_PaymentOptionListType, AUTOMATIC, EXI_APPL_DATA) PaymentOptionListPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  uint8_least i;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PaymentOptionListPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Loop over all PaymentOption elements */
    for(i=0; i<PaymentOptionListPtr->PaymentOptionCount; i++)
    {
      /* #30 Encode element PaymentOption */
      /* SE(PaymentOption) */
      if(0 == i)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }

      #if (defined(EXI_ENCODE_ISO_PAYMENT_OPTION) && (EXI_ENCODE_ISO_PAYMENT_OPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_paymentOption(EncWsPtr, &(PaymentOptionListPtr->PaymentOption[i]));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PAYMENT_OPTION_LIST, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_PAYMENT_OPTION) && (EXI_ENCODE_ISO_PAYMENT_OPTION == STD_ON)) */
      /* EE(PaymentOption) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #40 If element list does not have maximum length */
    if( i < 2)
    {
      /* EE(PaymentOptionList) */
      /* #50 Encode end element tag is requrired */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PAYMENT_OPTION_LIST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_PAYMENT_OPTION_LIST) && (EXI_ENCODE_ISO_PAYMENT_OPTION_LIST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_PaymentServiceSelectionReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_REQ) && (EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_PaymentServiceSelectionReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_PaymentServiceSelectionReqType, AUTOMATIC, EXI_APPL_DATA) PaymentServiceSelectionReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PaymentServiceSelectionReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element SelectedPaymentOption */
    /* SE(SelectedPaymentOption) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PAYMENT_OPTION) && (EXI_ENCODE_ISO_PAYMENT_OPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_paymentOption(EncWsPtr, &(PaymentServiceSelectionReqPtr->SelectedPaymentOption));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PAYMENT_OPTION) && (EXI_ENCODE_ISO_PAYMENT_OPTION == STD_ON)) */
    /* EE(SelectedPaymentOption) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element SelectedServiceList */
    /* SE(SelectedServiceList) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_SELECTED_SERVICE_LIST) && (EXI_ENCODE_ISO_SELECTED_SERVICE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_SelectedServiceList(EncWsPtr, (PaymentServiceSelectionReqPtr->SelectedServiceList));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_SELECTED_SERVICE_LIST) && (EXI_ENCODE_ISO_SELECTED_SERVICE_LIST == STD_ON)) */
    /* EE(SelectedServiceList) */
    /* Check EE encoding for SelectedServiceList */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_REQ) && (EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_PaymentServiceSelectionRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_RES) && (EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_PaymentServiceSelectionRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_PaymentServiceSelectionResType, AUTOMATIC, EXI_APPL_DATA) PaymentServiceSelectionResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PaymentServiceSelectionResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(PaymentServiceSelectionResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_RES) && (EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_PhysicalValue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_PhysicalValue( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_PhysicalValueType, AUTOMATIC, EXI_APPL_DATA) PhysicalValuePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PhysicalValuePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if ((PhysicalValuePtr->Multiplier < -3) || (PhysicalValuePtr->Multiplier > 3))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element Multiplier */
    /* SE(Multiplier) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(PhysicalValuePtr->Multiplier + 3), 3); /*lint !e571 */ /* Signed to unsigned cast, value is limited by the schema, lowest possible value is 0 or an offset is added to set it to 0 */
    /* EE(Multiplier) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element Unit */
    /* SE(Unit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_UNIT_SYMBOL) && (EXI_ENCODE_ISO_UNIT_SYMBOL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_unitSymbol(EncWsPtr, &(PhysicalValuePtr->Unit));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PHYSICAL_VALUE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_UNIT_SYMBOL) && (EXI_ENCODE_ISO_UNIT_SYMBOL == STD_ON)) */
    /* EE(Unit) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element Value */
    /* SE(Value) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeInt(&EncWsPtr->EncWs, (sint32)(PhysicalValuePtr->Value));
    /* EE(Value) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PHYSICAL_VALUE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_PowerDeliveryReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_POWER_DELIVERY_REQ) && (EXI_ENCODE_ISO_POWER_DELIVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_PowerDeliveryReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_PowerDeliveryReqType, AUTOMATIC, EXI_APPL_DATA) PowerDeliveryReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PowerDeliveryReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if ((PowerDeliveryReqPtr->SAScheduleTupleID < 1))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ChargeProgress */
    /* SE(ChargeProgress) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_CHARGE_PROGRESS) && (EXI_ENCODE_ISO_CHARGE_PROGRESS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_chargeProgress(EncWsPtr, &(PowerDeliveryReqPtr->ChargeProgress));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_POWER_DELIVERY_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_CHARGE_PROGRESS) && (EXI_ENCODE_ISO_CHARGE_PROGRESS == STD_ON)) */
    /* EE(ChargeProgress) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element SAScheduleTupleID */
    /* SE(SAScheduleTupleID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(PowerDeliveryReqPtr->SAScheduleTupleID - 1), 8);
    /* EE(SAScheduleTupleID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 If optional element ChargingProfile is included */
    if ( (1 == PowerDeliveryReqPtr->ChargingProfileFlag) && (NULL_PTR != PowerDeliveryReqPtr->ChargingProfile) )
    {
      /* #50 Encode element ChargingProfile */
      /* SE(ChargingProfile) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_ISO_CHARGING_PROFILE) && (EXI_ENCODE_ISO_CHARGING_PROFILE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_ChargingProfile(EncWsPtr, (PowerDeliveryReqPtr->ChargingProfile));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_POWER_DELIVERY_REQ, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_CHARGING_PROFILE) && (EXI_ENCODE_ISO_CHARGING_PROFILE == STD_ON)) */
      /* EE(ChargingProfile) */
      /* Check EE encoding for ChargingProfile */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    /* #60 If optional element EVPowerDeliveryParameter is included */
    if(1 == PowerDeliveryReqPtr->EVPowerDeliveryParameterFlag)
    {
      /* #70 Start of Substitution Group EVPowerDeliveryParameter */
      /* #80 Switch EVPowerDeliveryParameterElementId */
      switch(PowerDeliveryReqPtr->EVPowerDeliveryParameterElementId)
      {
      case EXI_ISO_DC_EVPOWER_DELIVERY_PARAMETER_TYPE:
        /* #90 Substitution element DC_EVPowerDeliveryParameter */
        {
          /* #100 Encode element DC_EVPowerDeliveryParameter */
        #if (defined(EXI_ENCODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_ENCODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          /* SE(DC_EVPowerDeliveryParameter) */
          if(0 == PowerDeliveryReqPtr->ChargingProfileFlag)
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
          }
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
          }
          EncWsPtr->EncWs.EERequired = TRUE;
          /* NULL_PTR check is done in the called API */
          Exi_Encode_ISO_DC_EVPowerDeliveryParameter(EncWsPtr, (P2CONST(Exi_ISO_DC_EVPowerDeliveryParameterType, AUTOMATIC, EXI_APPL_DATA))PowerDeliveryReqPtr->EVPowerDeliveryParameter); /* PRQA S 0310 */ /* MD_Exi_11.4 */
          /* EE(DC_EVPowerDeliveryParameter) */
          /* Check EE encoding for DC_EVPowerDeliveryParameter */
          Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_POWER_DELIVERY_REQ, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER) && (EXI_ENCODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER == STD_ON)) */
          break;
        }
      /* case EXI_ISO_EVPOWER_DELIVERY_PARAMETER_TYPE: Substitution element EVPowerDeliveryParameter not required, element is abstract*/
      default:
        /* #110 Default path */
        {
          /* Substitution Element not supported */
          /* #120 Set status code to error */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
      } /* switch(PowerDeliveryReqPtr->EVPowerDeliveryParameterElementId) */
      /* End of Substitution Group */
    }
    /* #130 Optional element EVPowerDeliveryParameter is not included */
    else
    {
      /* EE(PowerDeliveryReq) */
      /* #140 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      if(0 == PowerDeliveryReqPtr->ChargingProfileFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_POWER_DELIVERY_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_POWER_DELIVERY_REQ) && (EXI_ENCODE_ISO_POWER_DELIVERY_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_PowerDeliveryRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_POWER_DELIVERY_RES) && (EXI_ENCODE_ISO_POWER_DELIVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_PowerDeliveryRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_PowerDeliveryResType, AUTOMATIC, EXI_APPL_DATA) PowerDeliveryResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PowerDeliveryResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(PowerDeliveryResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_POWER_DELIVERY_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Start of Substitution Group EVSEStatus */
    /* #40 Switch EVSEStatusElementId */
    switch(PowerDeliveryResPtr->EVSEStatusElementId)
    {
    case EXI_ISO_AC_EVSESTATUS_TYPE:
      /* #50 Substitution element AC_EVSEStatus */
      {
        /* #60 Encode element AC_EVSEStatus */
      #if (defined(EXI_ENCODE_ISO_AC_EVSESTATUS) && (EXI_ENCODE_ISO_AC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* SE(AC_EVSEStatus) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
        /* NULL_PTR check is done in the called API */
        Exi_Encode_ISO_AC_EVSEStatus(EncWsPtr, (P2CONST(Exi_ISO_AC_EVSEStatusType, AUTOMATIC, EXI_APPL_DATA))PowerDeliveryResPtr->EVSEStatus); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* EE(AC_EVSEStatus) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_POWER_DELIVERY_RES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_AC_EVSESTATUS) && (EXI_ENCODE_ISO_AC_EVSESTATUS == STD_ON)) */
        break;
      }
    case EXI_ISO_DC_EVSESTATUS_TYPE:
      /* #70 Substitution element DC_EVSEStatus */
      {
        /* #80 Encode element DC_EVSEStatus */
      #if (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* SE(DC_EVSEStatus) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
        EncWsPtr->EncWs.EERequired = TRUE;
        /* NULL_PTR check is done in the called API */
        Exi_Encode_ISO_DC_EVSEStatus(EncWsPtr, (P2CONST(Exi_ISO_DC_EVSEStatusType, AUTOMATIC, EXI_APPL_DATA))PowerDeliveryResPtr->EVSEStatus); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* EE(DC_EVSEStatus) */
        /* Check EE encoding for DC_EVSEStatus */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_POWER_DELIVERY_RES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) */
        break;
      }
    /* case EXI_ISO_EVSESTATUS_TYPE: Substitution element EVSEStatus not required, element is abstract*/
    default:
      /* #90 Default path */
      {
        /* Substitution Element not supported */
        /* #100 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    } /* switch(PowerDeliveryResPtr->EVSEStatusElementId) */
    /* End of Substitution Group */
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_POWER_DELIVERY_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_POWER_DELIVERY_RES) && (EXI_ENCODE_ISO_POWER_DELIVERY_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_PreChargeReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_PRE_CHARGE_REQ) && (EXI_ENCODE_ISO_PRE_CHARGE_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_PreChargeReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_PreChargeReqType, AUTOMATIC, EXI_APPL_DATA) PreChargeReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PreChargeReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element DC_EVStatus */
    /* SE(DC_EVStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_DC_EVSTATUS) && (EXI_ENCODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DC_EVStatus(EncWsPtr, (PreChargeReqPtr->DC_EVStatus));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PRE_CHARGE_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DC_EVSTATUS) && (EXI_ENCODE_ISO_DC_EVSTATUS == STD_ON)) */
    /* EE(DC_EVStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element EVTargetVoltage */
    /* SE(EVTargetVoltage) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (PreChargeReqPtr->EVTargetVoltage));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PRE_CHARGE_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVTargetVoltage) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element EVTargetCurrent */
    /* SE(EVTargetCurrent) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (PreChargeReqPtr->EVTargetCurrent));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PRE_CHARGE_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVTargetCurrent) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PRE_CHARGE_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_PRE_CHARGE_REQ) && (EXI_ENCODE_ISO_PRE_CHARGE_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_PreChargeRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_PRE_CHARGE_RES) && (EXI_ENCODE_ISO_PRE_CHARGE_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_PreChargeRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_PreChargeResType, AUTOMATIC, EXI_APPL_DATA) PreChargeResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PreChargeResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(PreChargeResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PRE_CHARGE_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element DC_EVSEStatus */
    /* SE(DC_EVSEStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DC_EVSEStatus(EncWsPtr, (PreChargeResPtr->DC_EVSEStatus));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PRE_CHARGE_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) */
    /* EE(DC_EVSEStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element EVSEPresentVoltage */
    /* SE(EVSEPresentVoltage) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (PreChargeResPtr->EVSEPresentVoltage));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PRE_CHARGE_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVSEPresentVoltage) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PRE_CHARGE_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_PRE_CHARGE_RES) && (EXI_ENCODE_ISO_PRE_CHARGE_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ProfileEntry
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_PROFILE_ENTRY) && (EXI_ENCODE_ISO_PROFILE_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ProfileEntry( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ProfileEntryType, AUTOMATIC, EXI_APPL_DATA) ProfileEntryPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ProfileEntryPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if ((ProfileEntryPtr->ChargingProfileEntryMaxNumberOfPhasesInUseFlag == 1) && ((ProfileEntryPtr->ChargingProfileEntryMaxNumberOfPhasesInUse < 1) || (ProfileEntryPtr->ChargingProfileEntryMaxNumberOfPhasesInUse > 3)))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ChargingProfileEntryStart */
    /* SE(ChargingProfileEntryStart) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUInt(&EncWsPtr->EncWs, (Exi_BitBufType)(ProfileEntryPtr->ChargingProfileEntryStart));
    /* EE(ChargingProfileEntryStart) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element ChargingProfileEntryMaxPower */
    /* SE(ChargingProfileEntryMaxPower) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (ProfileEntryPtr->ChargingProfileEntryMaxPower));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PROFILE_ENTRY, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(ChargingProfileEntryMaxPower) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 If optional element ChargingProfileEntryMaxNumberOfPhasesInUse is included */
    if(1 == ProfileEntryPtr->ChargingProfileEntryMaxNumberOfPhasesInUseFlag)
    {
      /* #50 Encode element ChargingProfileEntryMaxNumberOfPhasesInUse */
      /* SE(ChargingProfileEntryMaxNumberOfPhasesInUse) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(ProfileEntryPtr->ChargingProfileEntryMaxNumberOfPhasesInUse - 1), 2); /*lint !e571 */ /* Signed to unsigned cast, value is limited by the schema, lowest possible value is 0 or an offset is added to set it to 0 */
      /* EE(ChargingProfileEntryMaxNumberOfPhasesInUse) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #60 Optional element ChargingProfileEntryMaxNumberOfPhasesInUse is not included */
    else
    {
      /* EE(ProfileEntry) */
      /* #70 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PROFILE_ENTRY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_PROFILE_ENTRY) && (EXI_ENCODE_ISO_PROFILE_ENTRY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_RelativeTimeInterval
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_RELATIVE_TIME_INTERVAL) && (EXI_ENCODE_ISO_RELATIVE_TIME_INTERVAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_RelativeTimeInterval( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_RelativeTimeIntervalType, AUTOMATIC, EXI_APPL_DATA) RelativeTimeIntervalPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (RelativeTimeIntervalPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element start */
    /* SE(start) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUInt(&EncWsPtr->EncWs, (Exi_BitBufType)(RelativeTimeIntervalPtr->start));
    /* EE(start) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If optional element duration is included */
    if(1 == RelativeTimeIntervalPtr->durationFlag)
    {
      /* #40 Encode element duration */
      /* SE(duration) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeUInt(&EncWsPtr->EncWs, (Exi_BitBufType)(RelativeTimeIntervalPtr->duration));
      /* EE(duration) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #50 Optional element duration is not included */
    else
    {
      /* EE(RelativeTimeInterval) */
      /* #60 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_RELATIVE_TIME_INTERVAL, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_RELATIVE_TIME_INTERVAL) && (EXI_ENCODE_ISO_RELATIVE_TIME_INTERVAL == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_SAScheduleList
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SASCHEDULE_LIST) && (EXI_ENCODE_ISO_SASCHEDULE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_SAScheduleList( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_SAScheduleListType, AUTOMATIC, EXI_APPL_DATA) SAScheduleListPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_ISO_SASCHEDULE_TUPLE) && (EXI_ENCODE_ISO_SASCHEDULE_TUPLE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_SAScheduleTupleType) nextPtr;
  uint8_least i;
  #endif /* #if (defined(EXI_ENCODE_ISO_SASCHEDULE_TUPLE) && (EXI_ENCODE_ISO_SASCHEDULE_TUPLE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SAScheduleListPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    #if (defined(EXI_ENCODE_ISO_SASCHEDULE_TUPLE) && (EXI_ENCODE_ISO_SASCHEDULE_TUPLE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #20 Initialize next pointer with the first SAScheduleTuple element */
    nextPtr = (Exi_ISO_SAScheduleTupleType*)SAScheduleListPtr->SAScheduleTuple;
    /* #30 Loop over all SAScheduleTuple elements */
    for(i=0; i<3; i++)
    {
      /* #40 Encode element SAScheduleTuple */
      /* SE(SAScheduleTuple) */
      if(0 == i)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }

      EncWsPtr->EncWs.EERequired = TRUE;
      Exi_Encode_ISO_SAScheduleTuple(EncWsPtr, nextPtr);
      /* EE(SAScheduleTuple) */
      /* Check EE encoding for SAScheduleTuple */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_ISO_SAScheduleTupleType*)(nextPtr->NextSAScheduleTuplePtr);
      /* #50 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* i holds the number of encoded Exi_ISO_SAScheduleTupleType elements */
        i++; /* PRQA S 2469 */  /*  MD_Exi_13.6 */
        /* #60 End the loop */
        break;
      }
    }
    /* #70 If maximum possible number of SAScheduleList's was encoded */
    if(i == 3)
    {
      /* #80 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #90 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG,EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    /* #100 If element list does not have maximum length */
    if( i < 3)
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SASCHEDULE_LIST, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_ISO_SASCHEDULE_TUPLE) && (EXI_ENCODE_ISO_SASCHEDULE_TUPLE == STD_ON)) */
    {
      /* EE(SAScheduleList) */
      /* #110 Encode end element tag is requrired */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SASCHEDULE_LIST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SASCHEDULE_LIST) && (EXI_ENCODE_ISO_SASCHEDULE_LIST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_SAScheduleTuple
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SASCHEDULE_TUPLE) && (EXI_ENCODE_ISO_SASCHEDULE_TUPLE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_SAScheduleTuple( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_SAScheduleTupleType, AUTOMATIC, EXI_APPL_DATA) SAScheduleTuplePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SAScheduleTuplePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if ((SAScheduleTuplePtr->SAScheduleTupleID < 1))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element SAScheduleTupleID */
    /* SE(SAScheduleTupleID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(SAScheduleTuplePtr->SAScheduleTupleID - 1), 8);
    /* EE(SAScheduleTupleID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element PMaxSchedule */
    /* SE(PMaxSchedule) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_PMAX_SCHEDULE) && (EXI_ENCODE_ISO_PMAX_SCHEDULE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PMaxSchedule(EncWsPtr, (SAScheduleTuplePtr->PMaxSchedule));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SASCHEDULE_TUPLE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PMAX_SCHEDULE) && (EXI_ENCODE_ISO_PMAX_SCHEDULE == STD_ON)) */
    /* EE(PMaxSchedule) */
    /* Check EE encoding for PMaxSchedule */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    /* #40 If optional element SalesTariff is included */
    if ( (1 == SAScheduleTuplePtr->SalesTariffFlag) && (NULL_PTR != SAScheduleTuplePtr->SalesTariff) )
    {
      /* #50 Encode element SalesTariff */
      /* SE(SalesTariff) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_ISO_SALES_TARIFF) && (EXI_ENCODE_ISO_SALES_TARIFF == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_SalesTariff(EncWsPtr, (SAScheduleTuplePtr->SalesTariff));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SASCHEDULE_TUPLE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_SALES_TARIFF) && (EXI_ENCODE_ISO_SALES_TARIFF == STD_ON)) */
      /* EE(SalesTariff) */
      /* Check EE encoding for SalesTariff */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    /* #60 Optional element SalesTariff is not included */
    else
    {
      /* EE(SAScheduleTuple) */
      /* #70 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SASCHEDULE_TUPLE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SASCHEDULE_TUPLE) && (EXI_ENCODE_ISO_SASCHEDULE_TUPLE == STD_ON)) */


/* Encode API for abstract type Exi_ISO_SASchedulesType not required */

/**********************************************************************************************************************
 *  Exi_Encode_ISO_SalesTariffEntry
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SALES_TARIFF_ENTRY) && (EXI_ENCODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_SalesTariffEntry( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_SalesTariffEntryType, AUTOMATIC, EXI_APPL_DATA) SalesTariffEntryPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_ISO_CONSUMPTION_COST) && (EXI_ENCODE_ISO_CONSUMPTION_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_ConsumptionCostType) nextPtr;
  uint8_least i;
  #endif /* #if (defined(EXI_ENCODE_ISO_CONSUMPTION_COST) && (EXI_ENCODE_ISO_CONSUMPTION_COST == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SalesTariffEntryPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start of Substitution Group TimeInterval */
    /* #30 Switch TimeIntervalElementId */
    switch(SalesTariffEntryPtr->TimeIntervalElementId)
    {
    case EXI_ISO_RELATIVE_TIME_INTERVAL_TYPE:
      /* #40 Substitution element RelativeTimeInterval */
      {
        /* #50 Encode element RelativeTimeInterval */
      #if (defined(EXI_ENCODE_ISO_RELATIVE_TIME_INTERVAL) && (EXI_ENCODE_ISO_RELATIVE_TIME_INTERVAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* SE(RelativeTimeInterval) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
        EncWsPtr->EncWs.EERequired = TRUE;
        /* NULL_PTR check is done in the called API */
        Exi_Encode_ISO_RelativeTimeInterval(EncWsPtr, (P2CONST(Exi_ISO_RelativeTimeIntervalType, AUTOMATIC, EXI_APPL_DATA))SalesTariffEntryPtr->TimeInterval); /* PRQA S 0310 */ /* MD_Exi_11.4 */
        /* EE(RelativeTimeInterval) */
        /* Check EE encoding for RelativeTimeInterval */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SALES_TARIFF_ENTRY, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_RELATIVE_TIME_INTERVAL) && (EXI_ENCODE_ISO_RELATIVE_TIME_INTERVAL == STD_ON)) */
        break;
      }
    /* case EXI_ISO_TIME_INTERVAL_TYPE: Substitution element TimeInterval not required, element is abstract*/
    default:
      /* #60 Default path */
      {
        /* Substitution Element not supported */
        /* #70 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
    } /* switch(SalesTariffEntryPtr->TimeIntervalElementId) */
    /* End of Substitution Group */
    /* #80 If optional element EPriceLevel is included */
    if(1 == SalesTariffEntryPtr->EPriceLevelFlag)
    {
      /* #90 Encode element EPriceLevel */
      /* SE(EPriceLevel) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(SalesTariffEntryPtr->EPriceLevel), 8);
      /* EE(EPriceLevel) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #100 If optional element ConsumptionCost is included */
    if ( (1 == SalesTariffEntryPtr->ConsumptionCostFlag) && (NULL_PTR != SalesTariffEntryPtr->ConsumptionCost) )
    {
      #if (defined(EXI_ENCODE_ISO_CONSUMPTION_COST) && (EXI_ENCODE_ISO_CONSUMPTION_COST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #110 Initialize next pointer with the first ConsumptionCost element */
      nextPtr = (Exi_ISO_ConsumptionCostType*)SalesTariffEntryPtr->ConsumptionCost;
      /* #120 Loop over all ConsumptionCost elements */
      for(i=0; i<3; i++)
      {
        /* #130 Encode element ConsumptionCost */
        /* SE(ConsumptionCost) */
        if(0 == i)
        {
          if(0 == SalesTariffEntryPtr->EPriceLevelFlag)
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
          }
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
          }
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
        }
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_ISO_ConsumptionCost(EncWsPtr, nextPtr);
        /* EE(ConsumptionCost) */
        /* Check EE encoding for ConsumptionCost */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
        nextPtr = (Exi_ISO_ConsumptionCostType*)(nextPtr->NextConsumptionCostPtr);
        /* #140 If this is the last element to encode */
        if(NULL_PTR == nextPtr)
        {
          /* i holds the number of encoded Exi_ISO_ConsumptionCostType elements */
          i++; /* PRQA S 2469 */  /*  MD_Exi_13.6 */
          /* #150 End the loop */
          break;
        }
      }
      /* #160 If maximum possible number of SalesTariffEntry's was encoded */
      if(i == 3)
      {
        /* #170 If there are more elements in the list */
        if (nextPtr != NULL_PTR)
        {
          /* #180 Set status code to error */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        }
      }
      /* EE(SalesTariffEntry) */
      /* #190 If element list does not have maximum length */
      if(i < 3)
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SALES_TARIFF_ENTRY, EXI_E_INV_PARAM);
      #endif /* #if (defined(EXI_ENCODE_ISO_CONSUMPTION_COST) && (EXI_ENCODE_ISO_CONSUMPTION_COST == STD_ON)) */
      {
        /* #200 Encode end element tag is requrired */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
        EncWsPtr->EncWs.EERequired = FALSE;
      }
    }
    /* #210 Optional element ConsumptionCost is not included */
    else
    {
      /* EE(SalesTariffEntry) */
      /* #220 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      if(0 == SalesTariffEntryPtr->EPriceLevelFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SALES_TARIFF_ENTRY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SALES_TARIFF_ENTRY) && (EXI_ENCODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_SalesTariff
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SALES_TARIFF) && (EXI_ENCODE_ISO_SALES_TARIFF == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_SalesTariff( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_SalesTariffType, AUTOMATIC, EXI_APPL_DATA) SalesTariffPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_ISO_SALES_TARIFF_ENTRY) && (EXI_ENCODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_SalesTariffEntryType) nextPtr;
  uint16_least i;
  #endif /* #if (defined(EXI_ENCODE_ISO_SALES_TARIFF_ENTRY) && (EXI_ENCODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SalesTariffPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if ((SalesTariffPtr->SalesTariffID < 1))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element Id is included */
    if ( (1 == SalesTariffPtr->IdFlag) && (NULL_PTR != SalesTariffPtr->Id) )
    {
      /* #30 Encode attribute Id */
      /* AT(Id) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_AttributeId(EncWsPtr, (SalesTariffPtr->Id));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SALES_TARIFF, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_ATTRIBUTE_ID) && (EXI_ENCODE_ISO_ATTRIBUTE_ID == STD_ON)) */

    }
    /* #40 Encode element SalesTariffID */
    /* SE(SalesTariffID) */
    if(0 == SalesTariffPtr->IdFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(SalesTariffPtr->SalesTariffID - 1), 8);
    /* EE(SalesTariffID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #50 If optional element SalesTariffDescription is included */
    if ( (1 == SalesTariffPtr->SalesTariffDescriptionFlag) && (NULL_PTR != SalesTariffPtr->SalesTariffDescription) )
    {
      /* #60 Encode element SalesTariffDescription */
      /* SE(SalesTariffDescription) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_TARIFF_DESCRIPTION) && (EXI_ENCODE_ISO_TARIFF_DESCRIPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_tariffDescription(EncWsPtr, (SalesTariffPtr->SalesTariffDescription));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SALES_TARIFF, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_TARIFF_DESCRIPTION) && (EXI_ENCODE_ISO_TARIFF_DESCRIPTION == STD_ON)) */
      /* EE(SalesTariffDescription) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #70 If optional element NumEPriceLevels is included */
    if(1 == SalesTariffPtr->NumEPriceLevelsFlag)
    {
      /* #80 Encode element NumEPriceLevels */
      /* SE(NumEPriceLevels) */
      if(0 == SalesTariffPtr->SalesTariffDescriptionFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (Exi_BitBufType)(SalesTariffPtr->NumEPriceLevels), 8);
      /* EE(NumEPriceLevels) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_ISO_SALES_TARIFF_ENTRY) && (EXI_ENCODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #90 Initialize next pointer with the first SalesTariffEntry element */
    nextPtr = (Exi_ISO_SalesTariffEntryType*)SalesTariffPtr->SalesTariffEntry;
    /* #100 Loop over all SalesTariffEntry elements */
    for(i=0; i<1024; i++)
    {
      /* #110 Encode element SalesTariffEntry */
      /* SE(SalesTariffEntry) */
      if(0 == i)
      {
        if(0 == SalesTariffPtr->NumEPriceLevelsFlag)
        {
          if(0 == SalesTariffPtr->SalesTariffDescriptionFlag)
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
          }
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
          }
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      EncWsPtr->EncWs.EERequired = TRUE;
      Exi_Encode_ISO_SalesTariffEntry(EncWsPtr, nextPtr);
      /* EE(SalesTariffEntry) */
      /* Check EE encoding for SalesTariffEntry */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_ISO_SalesTariffEntryType*)(nextPtr->NextSalesTariffEntryPtr);
      /* #120 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* i holds the number of encoded Exi_ISO_SalesTariffEntryType elements */
        i++; /* PRQA S 2469 */  /*  MD_Exi_13.6 */
        /* #130 End the loop */
        break;
      }
    }
    /* #140 If maximum possible number of SalesTariff's was encoded */
    if(i == 1024)
    {
      /* #150 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #160 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG,EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    /* #170 If element list does not have maximum length */
    if( i < 1024)
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SALES_TARIFF, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_ISO_SALES_TARIFF_ENTRY) && (EXI_ENCODE_ISO_SALES_TARIFF_ENTRY == STD_ON)) */
    {
      /* EE(SalesTariff) */
      /* #180 Encode end element tag is requrired */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SALES_TARIFF, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SALES_TARIFF) && (EXI_ENCODE_ISO_SALES_TARIFF == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_SelectedServiceList
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SELECTED_SERVICE_LIST) && (EXI_ENCODE_ISO_SELECTED_SERVICE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_SelectedServiceList( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_SelectedServiceListType, AUTOMATIC, EXI_APPL_DATA) SelectedServiceListPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_ISO_SELECTED_SERVICE) && (EXI_ENCODE_ISO_SELECTED_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_SelectedServiceType) nextPtr;
  uint8_least i;
  #endif /* #if (defined(EXI_ENCODE_ISO_SELECTED_SERVICE) && (EXI_ENCODE_ISO_SELECTED_SERVICE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SelectedServiceListPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    #if (defined(EXI_ENCODE_ISO_SELECTED_SERVICE) && (EXI_ENCODE_ISO_SELECTED_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #20 Initialize next pointer with the first SelectedService element */
    nextPtr = (Exi_ISO_SelectedServiceType*)SelectedServiceListPtr->SelectedService;
    /* #30 Loop over all SelectedService elements */
    for(i=0; i<16; i++)
    {
      /* #40 Encode element SelectedService */
      /* SE(SelectedService) */
      if(0 == i)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }

      EncWsPtr->EncWs.EERequired = TRUE;
      Exi_Encode_ISO_SelectedService(EncWsPtr, nextPtr);
      /* EE(SelectedService) */
      /* Check EE encoding for SelectedService */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_ISO_SelectedServiceType*)(nextPtr->NextSelectedServicePtr);
      /* #50 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* i holds the number of encoded Exi_ISO_SelectedServiceType elements */
        i++; /* PRQA S 2469 */  /*  MD_Exi_13.6 */
        /* #60 End the loop */
        break;
      }
    }
    /* #70 If maximum possible number of SelectedServiceList's was encoded */
    if(i == 16)
    {
      /* #80 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #90 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG,EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    /* #100 If element list does not have maximum length */
    if( i < 16)
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SELECTED_SERVICE_LIST, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_ISO_SELECTED_SERVICE) && (EXI_ENCODE_ISO_SELECTED_SERVICE == STD_ON)) */
    {
      /* EE(SelectedServiceList) */
      /* #110 Encode end element tag is requrired */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SELECTED_SERVICE_LIST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SELECTED_SERVICE_LIST) && (EXI_ENCODE_ISO_SELECTED_SERVICE_LIST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_SelectedService
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SELECTED_SERVICE) && (EXI_ENCODE_ISO_SELECTED_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_SelectedService( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_SelectedServiceType, AUTOMATIC, EXI_APPL_DATA) SelectedServicePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SelectedServicePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ServiceID */
    /* SE(ServiceID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUInt(&EncWsPtr->EncWs, (Exi_BitBufType)(SelectedServicePtr->ServiceID));
    /* EE(ServiceID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If optional element ParameterSetID is included */
    if(1 == SelectedServicePtr->ParameterSetIDFlag)
    {
      /* #40 Encode element ParameterSetID */
      /* SE(ParameterSetID) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeInt(&EncWsPtr->EncWs, (sint32)(SelectedServicePtr->ParameterSetID));
      /* EE(ParameterSetID) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #50 Optional element ParameterSetID is not included */
    else
    {
      /* EE(SelectedService) */
      /* #60 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SELECTED_SERVICE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SELECTED_SERVICE) && (EXI_ENCODE_ISO_SELECTED_SERVICE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ServiceDetailReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SERVICE_DETAIL_REQ) && (EXI_ENCODE_ISO_SERVICE_DETAIL_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ServiceDetailReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ServiceDetailReqType, AUTOMATIC, EXI_APPL_DATA) ServiceDetailReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ServiceDetailReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ServiceID */
    /* SE(ServiceID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUInt(&EncWsPtr->EncWs, (Exi_BitBufType)(ServiceDetailReqPtr->ServiceID));
    /* EE(ServiceID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_DETAIL_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SERVICE_DETAIL_REQ) && (EXI_ENCODE_ISO_SERVICE_DETAIL_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ServiceDetailRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SERVICE_DETAIL_RES) && (EXI_ENCODE_ISO_SERVICE_DETAIL_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ServiceDetailRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ServiceDetailResType, AUTOMATIC, EXI_APPL_DATA) ServiceDetailResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ServiceDetailResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(ServiceDetailResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_DETAIL_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element ServiceID */
    /* SE(ServiceID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUInt(&EncWsPtr->EncWs, (Exi_BitBufType)(ServiceDetailResPtr->ServiceID));
    /* EE(ServiceID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 If optional element ServiceParameterList is included */
    if ( (1 == ServiceDetailResPtr->ServiceParameterListFlag) && (NULL_PTR != ServiceDetailResPtr->ServiceParameterList) )
    {
      /* #50 Encode element ServiceParameterList */
      /* SE(ServiceParameterList) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_ISO_SERVICE_PARAMETER_LIST) && (EXI_ENCODE_ISO_SERVICE_PARAMETER_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_ServiceParameterList(EncWsPtr, (ServiceDetailResPtr->ServiceParameterList));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_DETAIL_RES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_SERVICE_PARAMETER_LIST) && (EXI_ENCODE_ISO_SERVICE_PARAMETER_LIST == STD_ON)) */
      /* EE(ServiceParameterList) */
      /* Check EE encoding for ServiceParameterList */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    /* #60 Optional element ServiceParameterList is not included */
    else
    {
      /* EE(ServiceDetailRes) */
      /* #70 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_DETAIL_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SERVICE_DETAIL_RES) && (EXI_ENCODE_ISO_SERVICE_DETAIL_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ServiceDiscoveryReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SERVICE_DISCOVERY_REQ) && (EXI_ENCODE_ISO_SERVICE_DISCOVERY_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ServiceDiscoveryReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ServiceDiscoveryReqType, AUTOMATIC, EXI_APPL_DATA) ServiceDiscoveryReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ServiceDiscoveryReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element ServiceScope is included */
    if ( (1 == ServiceDiscoveryReqPtr->ServiceScopeFlag) && (NULL_PTR != ServiceDiscoveryReqPtr->ServiceScope) )
    {
      /* #30 Encode element ServiceScope */
      /* SE(ServiceScope) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_SERVICE_SCOPE) && (EXI_ENCODE_ISO_SERVICE_SCOPE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_serviceScope(EncWsPtr, (ServiceDiscoveryReqPtr->ServiceScope));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_DISCOVERY_REQ, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_SERVICE_SCOPE) && (EXI_ENCODE_ISO_SERVICE_SCOPE == STD_ON)) */
      /* EE(ServiceScope) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #40 If optional element ServiceCategory is included */
    if(1 == ServiceDiscoveryReqPtr->ServiceCategoryFlag)
    {
      /* #50 Encode element ServiceCategory */
      /* SE(ServiceCategory) */
      if(0 == ServiceDiscoveryReqPtr->ServiceScopeFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      #if (defined(EXI_ENCODE_ISO_SERVICE_CATEGORY) && (EXI_ENCODE_ISO_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_serviceCategory(EncWsPtr, &(ServiceDiscoveryReqPtr->ServiceCategory));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_DISCOVERY_REQ, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_SERVICE_CATEGORY) && (EXI_ENCODE_ISO_SERVICE_CATEGORY == STD_ON)) */
      /* EE(ServiceCategory) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #60 Optional element ServiceCategory is not included */
    else
    {
      /* EE(ServiceDiscoveryReq) */
      /* #70 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      if(0 == ServiceDiscoveryReqPtr->ServiceScopeFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_DISCOVERY_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SERVICE_DISCOVERY_REQ) && (EXI_ENCODE_ISO_SERVICE_DISCOVERY_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ServiceDiscoveryRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SERVICE_DISCOVERY_RES) && (EXI_ENCODE_ISO_SERVICE_DISCOVERY_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ServiceDiscoveryRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ServiceDiscoveryResType, AUTOMATIC, EXI_APPL_DATA) ServiceDiscoveryResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ServiceDiscoveryResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(ServiceDiscoveryResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_DISCOVERY_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element PaymentOptionList */
    /* SE(PaymentOptionList) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_PAYMENT_OPTION_LIST) && (EXI_ENCODE_ISO_PAYMENT_OPTION_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PaymentOptionList(EncWsPtr, (ServiceDiscoveryResPtr->PaymentOptionList));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_DISCOVERY_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PAYMENT_OPTION_LIST) && (EXI_ENCODE_ISO_PAYMENT_OPTION_LIST == STD_ON)) */
    /* EE(PaymentOptionList) */
    /* Check EE encoding for PaymentOptionList */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element ChargeService */
    /* SE(ChargeService) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_CHARGE_SERVICE) && (EXI_ENCODE_ISO_CHARGE_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_ChargeService(EncWsPtr, (ServiceDiscoveryResPtr->ChargeService));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_DISCOVERY_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_CHARGE_SERVICE) && (EXI_ENCODE_ISO_CHARGE_SERVICE == STD_ON)) */
    /* EE(ChargeService) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #50 If optional element ServiceList is included */
    if ( (1 == ServiceDiscoveryResPtr->ServiceListFlag) && (NULL_PTR != ServiceDiscoveryResPtr->ServiceList) )
    {
      /* #60 Encode element ServiceList */
      /* SE(ServiceList) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_ISO_SERVICE_LIST) && (EXI_ENCODE_ISO_SERVICE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_ServiceList(EncWsPtr, (ServiceDiscoveryResPtr->ServiceList));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_DISCOVERY_RES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_SERVICE_LIST) && (EXI_ENCODE_ISO_SERVICE_LIST == STD_ON)) */
      /* EE(ServiceList) */
      /* Check EE encoding for ServiceList */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    /* #70 Optional element ServiceList is not included */
    else
    {
      /* EE(ServiceDiscoveryRes) */
      /* #80 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_DISCOVERY_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SERVICE_DISCOVERY_RES) && (EXI_ENCODE_ISO_SERVICE_DISCOVERY_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ServiceList
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SERVICE_LIST) && (EXI_ENCODE_ISO_SERVICE_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ServiceList( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ServiceListType, AUTOMATIC, EXI_APPL_DATA) ServiceListPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_ISO_SERVICE) && (EXI_ENCODE_ISO_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_ServiceType) nextPtr;
  uint8_least i;
  #endif /* #if (defined(EXI_ENCODE_ISO_SERVICE) && (EXI_ENCODE_ISO_SERVICE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ServiceListPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    #if (defined(EXI_ENCODE_ISO_SERVICE) && (EXI_ENCODE_ISO_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #20 Initialize next pointer with the first Service element */
    nextPtr = (Exi_ISO_ServiceType*)ServiceListPtr->Service;
    /* #30 Loop over all Service elements */
    for(i=0; i<8; i++)
    {
      /* #40 Encode element Service */
      /* SE(Service) */
      if(0 == i)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }

      Exi_Encode_ISO_Service(EncWsPtr, nextPtr);
      /* EE(Service) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_ISO_ServiceType*)(nextPtr->NextServicePtr);
      /* #50 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* i holds the number of encoded Exi_ISO_ServiceType elements */
        i++; /* PRQA S 2469 */  /*  MD_Exi_13.6 */
        /* #60 End the loop */
        break;
      }
    }
    /* #70 If maximum possible number of ServiceList's was encoded */
    if(i == 8)
    {
      /* #80 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #90 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG,EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    /* #100 If element list does not have maximum length */
    if( i < 8)
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_LIST, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_ISO_SERVICE) && (EXI_ENCODE_ISO_SERVICE == STD_ON)) */
    {
      /* EE(ServiceList) */
      /* #110 Encode end element tag is requrired */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_LIST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SERVICE_LIST) && (EXI_ENCODE_ISO_SERVICE_LIST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_ServiceParameterList
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SERVICE_PARAMETER_LIST) && (EXI_ENCODE_ISO_SERVICE_PARAMETER_LIST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_ServiceParameterList( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ServiceParameterListType, AUTOMATIC, EXI_APPL_DATA) ServiceParameterListPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_ISO_PARAMETER_SET) && (EXI_ENCODE_ISO_PARAMETER_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_ParameterSetType) nextPtr;
  uint8_least i;
  #endif /* #if (defined(EXI_ENCODE_ISO_PARAMETER_SET) && (EXI_ENCODE_ISO_PARAMETER_SET == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ServiceParameterListPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    #if (defined(EXI_ENCODE_ISO_PARAMETER_SET) && (EXI_ENCODE_ISO_PARAMETER_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #20 Initialize next pointer with the first ParameterSet element */
    nextPtr = (Exi_ISO_ParameterSetType*)ServiceParameterListPtr->ParameterSet;
    /* #30 Loop over all ParameterSet elements */
    for(i=0; i<255; i++)
    {
      /* #40 Encode element ParameterSet */
      /* SE(ParameterSet) */
      if(0 == i)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }

      EncWsPtr->EncWs.EERequired = TRUE;
      Exi_Encode_ISO_ParameterSet(EncWsPtr, nextPtr);
      /* EE(ParameterSet) */
      /* Check EE encoding for ParameterSet */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_ISO_ParameterSetType*)(nextPtr->NextParameterSetPtr);
      /* #50 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* i holds the number of encoded Exi_ISO_ParameterSetType elements */
        i++; /* PRQA S 2469 */  /*  MD_Exi_13.6 */
        /* #60 End the loop */
        break;
      }
    }
    /* #70 If maximum possible number of ServiceParameterList's was encoded */
    if(i == 255)
    {
      /* #80 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #90 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG,EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    /* #100 If element list does not have maximum length */
    if( i < 255)
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_PARAMETER_LIST, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_ISO_PARAMETER_SET) && (EXI_ENCODE_ISO_PARAMETER_SET == STD_ON)) */
    {
      /* EE(ServiceParameterList) */
      /* #110 Encode end element tag is requrired */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_PARAMETER_LIST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SERVICE_PARAMETER_LIST) && (EXI_ENCODE_ISO_SERVICE_PARAMETER_LIST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_Service
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SERVICE) && (EXI_ENCODE_ISO_SERVICE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_Service( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_ServiceType, AUTOMATIC, EXI_APPL_DATA) ServicePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ServicePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ServiceID */
    /* SE(ServiceID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeUInt(&EncWsPtr->EncWs, (Exi_BitBufType)(ServicePtr->ServiceID));
    /* EE(ServiceID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If optional element ServiceName is included */
    if ( (1 == ServicePtr->ServiceNameFlag) && (NULL_PTR != ServicePtr->ServiceName) )
    {
      /* #40 Encode element ServiceName */
      /* SE(ServiceName) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_SERVICE_NAME) && (EXI_ENCODE_ISO_SERVICE_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_serviceName(EncWsPtr, (ServicePtr->ServiceName));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_SERVICE_NAME) && (EXI_ENCODE_ISO_SERVICE_NAME == STD_ON)) */
      /* EE(ServiceName) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #50 Encode element ServiceCategory */
    /* SE(ServiceCategory) */
    if(0 == ServicePtr->ServiceNameFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_ISO_SERVICE_CATEGORY) && (EXI_ENCODE_ISO_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_serviceCategory(EncWsPtr, &(ServicePtr->ServiceCategory));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_SERVICE_CATEGORY) && (EXI_ENCODE_ISO_SERVICE_CATEGORY == STD_ON)) */
    /* EE(ServiceCategory) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #60 If optional element ServiceScope is included */
    if ( (1 == ServicePtr->ServiceScopeFlag) && (NULL_PTR != ServicePtr->ServiceScope) )
    {
      /* #70 Encode element ServiceScope */
      /* SE(ServiceScope) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_ISO_SERVICE_SCOPE) && (EXI_ENCODE_ISO_SERVICE_SCOPE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_serviceScope(EncWsPtr, (ServicePtr->ServiceScope));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_SERVICE_SCOPE) && (EXI_ENCODE_ISO_SERVICE_SCOPE == STD_ON)) */
      /* EE(ServiceScope) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #80 Encode element FreeService */
    /* SE(FreeService) */
    if(0 == ServicePtr->ServiceScopeFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeBool(&EncWsPtr->EncWs, (ServicePtr->FreeService));
    /* EE(FreeService) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SERVICE) && (EXI_ENCODE_ISO_SERVICE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_SessionSetupReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SESSION_SETUP_REQ) && (EXI_ENCODE_ISO_SESSION_SETUP_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_SessionSetupReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_SessionSetupReqType, AUTOMATIC, EXI_APPL_DATA) SessionSetupReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SessionSetupReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element EVCCID */
    /* SE(EVCCID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_EVCC_ID) && (EXI_ENCODE_ISO_EVCC_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_evccID(EncWsPtr, (SessionSetupReqPtr->EVCCID));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SESSION_SETUP_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_EVCC_ID) && (EXI_ENCODE_ISO_EVCC_ID == STD_ON)) */
    /* EE(EVCCID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SESSION_SETUP_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SESSION_SETUP_REQ) && (EXI_ENCODE_ISO_SESSION_SETUP_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_SessionSetupRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SESSION_SETUP_RES) && (EXI_ENCODE_ISO_SESSION_SETUP_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_SessionSetupRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_SessionSetupResType, AUTOMATIC, EXI_APPL_DATA) SessionSetupResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SessionSetupResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(SessionSetupResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SESSION_SETUP_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element EVSEID */
    /* SE(EVSEID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_EVSE_ID) && (EXI_ENCODE_ISO_EVSE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_evseID(EncWsPtr, (SessionSetupResPtr->EVSEID));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SESSION_SETUP_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_EVSE_ID) && (EXI_ENCODE_ISO_EVSE_ID == STD_ON)) */
    /* EE(EVSEID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 If optional element EVSETimeStamp is included */
    if(1 == SessionSetupResPtr->EVSETimeStampFlag)
    {
      /* #50 Encode element EVSETimeStamp */
      /* SE(EVSETimeStamp) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeInt64(&EncWsPtr->EncWs, (SessionSetupResPtr->EVSETimeStamp));
      /* EE(EVSETimeStamp) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #60 Optional element EVSETimeStamp is not included */
    else
    {
      /* EE(SessionSetupRes) */
      /* #70 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SESSION_SETUP_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SESSION_SETUP_RES) && (EXI_ENCODE_ISO_SESSION_SETUP_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_SessionStopReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SESSION_STOP_REQ) && (EXI_ENCODE_ISO_SESSION_STOP_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_SessionStopReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_SessionStopReqType, AUTOMATIC, EXI_APPL_DATA) SessionStopReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SessionStopReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ChargingSession */
    /* SE(ChargingSession) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_CHARGING_SESSION) && (EXI_ENCODE_ISO_CHARGING_SESSION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_chargingSession(EncWsPtr, &(SessionStopReqPtr->ChargingSession));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SESSION_STOP_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_CHARGING_SESSION) && (EXI_ENCODE_ISO_CHARGING_SESSION == STD_ON)) */
    /* EE(ChargingSession) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SESSION_STOP_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SESSION_STOP_REQ) && (EXI_ENCODE_ISO_SESSION_STOP_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_SessionStopRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SESSION_STOP_RES) && (EXI_ENCODE_ISO_SESSION_STOP_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_SessionStopRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_SessionStopResType, AUTOMATIC, EXI_APPL_DATA) SessionStopResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SessionStopResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(SessionStopResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SESSION_STOP_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SESSION_STOP_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SESSION_STOP_RES) && (EXI_ENCODE_ISO_SESSION_STOP_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_SubCertificates
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SUB_CERTIFICATES) && (EXI_ENCODE_ISO_SUB_CERTIFICATES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_SubCertificates( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_SubCertificatesType, AUTOMATIC, EXI_APPL_DATA) SubCertificatesPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_ISO_CERTIFICATE) && (EXI_ENCODE_ISO_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_ISO_certificateType) nextPtr;
  uint8_least i;
  #endif /* #if (defined(EXI_ENCODE_ISO_CERTIFICATE) && (EXI_ENCODE_ISO_CERTIFICATE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SubCertificatesPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    #if (defined(EXI_ENCODE_ISO_CERTIFICATE) && (EXI_ENCODE_ISO_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #20 Initialize next pointer with the first Certificate element */
    nextPtr = (Exi_ISO_certificateType*)SubCertificatesPtr->Certificate;
    /* #30 Loop over all Certificate elements */
    for(i=0; i<4; i++)
    {
      /* #40 Encode element Certificate */
      /* SE(Certificate) */
      if(0 == i)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }

      Exi_Encode_ISO_certificate(EncWsPtr, nextPtr);
      /* EE(Certificate) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_ISO_certificateType*)(nextPtr->NextCertificatePtr);
      /* #50 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* i holds the number of encoded Exi_ISO_certificateType elements */
        i++; /* PRQA S 2469 */  /*  MD_Exi_13.6 */
        /* #60 End the loop */
        break;
      }
    }
    /* #70 If maximum possible number of SubCertificates's was encoded */
    if(i == 4)
    {
      /* #80 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #90 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG,EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    /* #100 If element list does not have maximum length */
    if( i < 4)
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SUB_CERTIFICATES, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_ISO_CERTIFICATE) && (EXI_ENCODE_ISO_CERTIFICATE == STD_ON)) */
    {
      /* EE(SubCertificates) */
      /* #110 Encode end element tag is requrired */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SUB_CERTIFICATES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SUB_CERTIFICATES) && (EXI_ENCODE_ISO_SUB_CERTIFICATES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_SupportedEnergyTransferMode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE) && (EXI_ENCODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_SupportedEnergyTransferMode( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_SupportedEnergyTransferModeType, AUTOMATIC, EXI_APPL_DATA) SupportedEnergyTransferModePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  uint8_least i;

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SupportedEnergyTransferModePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Loop over all EnergyTransferMode elements */
    for(i=0; i<SupportedEnergyTransferModePtr->EnergyTransferModeCount; i++)
    {
      /* #30 Encode element EnergyTransferMode */
      /* SE(EnergyTransferMode) */
      if(0 == i)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }

      #if (defined(EXI_ENCODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_ENCODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_ISO_EnergyTransferMode(EncWsPtr, &(SupportedEnergyTransferModePtr->EnergyTransferMode[i]));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_ENERGY_TRANSFER_MODE) && (EXI_ENCODE_ISO_ENERGY_TRANSFER_MODE == STD_ON)) */
      /* EE(EnergyTransferMode) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #40 If element list does not have maximum length */
    if( i < 6)
    {
      /* EE(SupportedEnergyTransferMode) */
      /* #50 Encode end element tag is requrired */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE) && (EXI_ENCODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_V2G_Message
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_V2G_MESSAGE) && (EXI_ENCODE_ISO_V2G_MESSAGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_V2G_Message( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_V2G_MessageType, AUTOMATIC, EXI_APPL_DATA) V2G_MessagePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (V2G_MessagePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element Header */
    /* SE(Header) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_MESSAGE_HEADER) && (EXI_ENCODE_ISO_MESSAGE_HEADER == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_MessageHeader(EncWsPtr, (V2G_MessagePtr->Header));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_V2G_MESSAGE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_MESSAGE_HEADER) && (EXI_ENCODE_ISO_MESSAGE_HEADER == STD_ON)) */
    /* EE(Header) */
    /* Check EE encoding for Header */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element Body */
    /* SE(Body) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_ISO_BODY) && (EXI_ENCODE_ISO_BODY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_Body(EncWsPtr, (V2G_MessagePtr->Body));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_V2G_MESSAGE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_BODY) && (EXI_ENCODE_ISO_BODY == STD_ON)) */
    /* EE(Body) */
    /* Check EE encoding for Body */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_V2G_MESSAGE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_V2G_MESSAGE) && (EXI_ENCODE_ISO_V2G_MESSAGE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_WeldingDetectionReq
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_WELDING_DETECTION_REQ) && (EXI_ENCODE_ISO_WELDING_DETECTION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_WeldingDetectionReq( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_WeldingDetectionReqType, AUTOMATIC, EXI_APPL_DATA) WeldingDetectionReqPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (WeldingDetectionReqPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element DC_EVStatus */
    /* SE(DC_EVStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_DC_EVSTATUS) && (EXI_ENCODE_ISO_DC_EVSTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DC_EVStatus(EncWsPtr, (WeldingDetectionReqPtr->DC_EVStatus));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_WELDING_DETECTION_REQ, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DC_EVSTATUS) && (EXI_ENCODE_ISO_DC_EVSTATUS == STD_ON)) */
    /* EE(DC_EVStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_WELDING_DETECTION_REQ, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_WELDING_DETECTION_REQ) && (EXI_ENCODE_ISO_WELDING_DETECTION_REQ == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_WeldingDetectionRes
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_WELDING_DETECTION_RES) && (EXI_ENCODE_ISO_WELDING_DETECTION_RES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_WeldingDetectionRes( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_WeldingDetectionResType, AUTOMATIC, EXI_APPL_DATA) WeldingDetectionResPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (WeldingDetectionResPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element ResponseCode */
    /* SE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_responseCode(EncWsPtr, &(WeldingDetectionResPtr->ResponseCode));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_WELDING_DETECTION_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */
    /* EE(ResponseCode) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element DC_EVSEStatus */
    /* SE(DC_EVSEStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_DC_EVSEStatus(EncWsPtr, (WeldingDetectionResPtr->DC_EVSEStatus));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_WELDING_DETECTION_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_DC_EVSESTATUS) && (EXI_ENCODE_ISO_DC_EVSESTATUS == STD_ON)) */
    /* EE(DC_EVSEStatus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 Encode element EVSEPresentVoltage */
    /* SE(EVSEPresentVoltage) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_ISO_PhysicalValue(EncWsPtr, (WeldingDetectionResPtr->EVSEPresentVoltage));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_WELDING_DETECTION_RES, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_ISO_PHYSICAL_VALUE) && (EXI_ENCODE_ISO_PHYSICAL_VALUE == STD_ON)) */
    /* EE(EVSEPresentVoltage) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_WELDING_DETECTION_RES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_WELDING_DETECTION_RES) && (EXI_ENCODE_ISO_WELDING_DETECTION_RES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_certificate
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CERTIFICATE) && (EXI_ENCODE_ISO_CERTIFICATE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_certificate( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_certificateType, AUTOMATIC, EXI_APPL_DATA) certificatePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (certificatePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (certificatePtr->Length > sizeof(certificatePtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of certificate */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode certificate as byte array */
    Exi_VBSEncodeBytes(&EncWsPtr->EncWs, &certificatePtr->Buffer[0], certificatePtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CERTIFICATE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE) && (EXI_ENCODE_ISO_CERTIFICATE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_chargeProgress
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CHARGE_PROGRESS) && (EXI_ENCODE_ISO_CHARGE_PROGRESS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_chargeProgress( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_chargeProgressType, AUTOMATIC, EXI_APPL_DATA) chargeProgressPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (chargeProgressPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode enumeration value chargeProgress as element */
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If enumeration value is in range */
    if (*chargeProgressPtr <= EXI_ISO_CHARGE_PROGRESS_TYPE_RENEGOTIATE) /* PRQA S 3355, 3358 */ /* MD_Exi_13.7_14.1_Enum */ /*lint !e685 */
    {
      /* #40 Encode value */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (uint8)*chargeProgressPtr, 2);
    }
    /* #50 Enumeration is out of its allowed range */
    else
    { /* PRQA S 3201 */ /* MD_Exi_13.7_14.1_Enum */
      /* #60 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGE_PROGRESS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CHARGE_PROGRESS) && (EXI_ENCODE_ISO_CHARGE_PROGRESS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_chargingSession
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_CHARGING_SESSION) && (EXI_ENCODE_ISO_CHARGING_SESSION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_chargingSession( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_chargingSessionType, AUTOMATIC, EXI_APPL_DATA) chargingSessionPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (chargingSessionPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode enumeration value chargingSession as element */
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If enumeration value is in range */
    if (*chargingSessionPtr <= EXI_ISO_CHARGING_SESSION_TYPE_PAUSE) /* PRQA S 3355, 3358 */ /* MD_Exi_13.7_14.1_Enum */ /*lint !e685 */
    {
      /* #40 Encode value */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (uint8)*chargingSessionPtr, 1);
    }
    /* #50 Enumeration is out of its allowed range */
    else
    { /* PRQA S 3201 */ /* MD_Exi_13.7_14.1_Enum */
      /* #60 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_CHARGING_SESSION, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_CHARGING_SESSION) && (EXI_ENCODE_ISO_CHARGING_SESSION == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_costKind
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_COST_KIND) && (EXI_ENCODE_ISO_COST_KIND == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_costKind( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_costKindType, AUTOMATIC, EXI_APPL_DATA) costKindPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (costKindPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode enumeration value costKind as element */
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If enumeration value is in range */
    if (*costKindPtr <= EXI_ISO_COST_KIND_TYPE_CARBON_DIOXIDE_EMISSION) /* PRQA S 3355, 3358 */ /* MD_Exi_13.7_14.1_Enum */ /*lint !e685 */
    {
      /* #40 Encode value */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (uint8)*costKindPtr, 2);
    }
    /* #50 Enumeration is out of its allowed range */
    else
    { /* PRQA S 3201 */ /* MD_Exi_13.7_14.1_Enum */
      /* #60 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_COST_KIND, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_COST_KIND) && (EXI_ENCODE_ISO_COST_KIND == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_eMAID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_E_MAID) && (EXI_ENCODE_ISO_E_MAID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_eMAID( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_eMAIDType, AUTOMATIC, EXI_APPL_DATA) eMAIDPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (eMAIDPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (eMAIDPtr->Length > sizeof(eMAIDPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of eMAID */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode eMAID as string value */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &eMAIDPtr->Buffer[0], eMAIDPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_E_MAID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_E_MAID) && (EXI_ENCODE_ISO_E_MAID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_eMAID_ElementFragment
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_E_MAID_ELEMENT_FRAGMENT) && (EXI_ENCODE_ISO_E_MAID_ELEMENT_FRAGMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_eMAID_ElementFragment( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_eMAIDType, AUTOMATIC, EXI_APPL_DATA) eMAIDPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (eMAIDPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (eMAIDPtr->Length > sizeof(eMAIDPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* CH[untyped value] */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 256, 9);
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &eMAIDPtr->Buffer[0], eMAIDPtr->Length);
    /* EE */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 244, 8);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_E_MAID_ELEMENT_FRAGMENT, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_E_MAID_ELEMENT_FRAGMENT) && (EXI_ENCODE_ISO_E_MAID_ELEMENT_FRAGMENT == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_evccID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_EVCC_ID) && (EXI_ENCODE_ISO_EVCC_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_evccID( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_evccIDType, AUTOMATIC, EXI_APPL_DATA) evccIDPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (evccIDPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (evccIDPtr->Length > sizeof(evccIDPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of evccID */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode evccID as byte array */
    Exi_VBSEncodeBytes(&EncWsPtr->EncWs, &evccIDPtr->Buffer[0], evccIDPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_EVCC_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_EVCC_ID) && (EXI_ENCODE_ISO_EVCC_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_evseID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_EVSE_ID) && (EXI_ENCODE_ISO_EVSE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_evseID( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_evseIDType, AUTOMATIC, EXI_APPL_DATA) evseIDPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (evseIDPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (evseIDPtr->Length > sizeof(evseIDPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of evseID */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode evseID as string value */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &evseIDPtr->Buffer[0], evseIDPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_EVSE_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_EVSE_ID) && (EXI_ENCODE_ISO_EVSE_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_faultCode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_FAULT_CODE) && (EXI_ENCODE_ISO_FAULT_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_faultCode( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_faultCodeType, AUTOMATIC, EXI_APPL_DATA) faultCodePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (faultCodePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode enumeration value faultCode as element */
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If enumeration value is in range */
    if (*faultCodePtr <= EXI_ISO_FAULT_CODE_TYPE_UNKNOWN_ERROR) /* PRQA S 3355, 3358 */ /* MD_Exi_13.7_14.1_Enum */ /*lint !e685 */
    {
      /* #40 Encode value */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (uint8)*faultCodePtr, 2);
    }
    /* #50 Enumeration is out of its allowed range */
    else
    { /* PRQA S 3201 */ /* MD_Exi_13.7_14.1_Enum */
      /* #60 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_FAULT_CODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_FAULT_CODE) && (EXI_ENCODE_ISO_FAULT_CODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_faultMsg
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_FAULT_MSG) && (EXI_ENCODE_ISO_FAULT_MSG == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_faultMsg( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_faultMsgType, AUTOMATIC, EXI_APPL_DATA) faultMsgPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (faultMsgPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (faultMsgPtr->Length > sizeof(faultMsgPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of faultMsg */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode faultMsg as string value */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &faultMsgPtr->Buffer[0], faultMsgPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_FAULT_MSG, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_FAULT_MSG) && (EXI_ENCODE_ISO_FAULT_MSG == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_genChallenge
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_GEN_CHALLENGE) && (EXI_ENCODE_ISO_GEN_CHALLENGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_genChallenge( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_genChallengeType, AUTOMATIC, EXI_APPL_DATA) genChallengePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (genChallengePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (genChallengePtr->Length > sizeof(genChallengePtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of genChallenge */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode genChallenge as byte array */
    Exi_VBSEncodeBytes(&EncWsPtr->EncWs, &genChallengePtr->Buffer[0], genChallengePtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_GEN_CHALLENGE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_GEN_CHALLENGE) && (EXI_ENCODE_ISO_GEN_CHALLENGE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_isolationLevel
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_ISOLATION_LEVEL) && (EXI_ENCODE_ISO_ISOLATION_LEVEL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_isolationLevel( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_isolationLevelType, AUTOMATIC, EXI_APPL_DATA) isolationLevelPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (isolationLevelPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode enumeration value isolationLevel as element */
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If enumeration value is in range */
    if (*isolationLevelPtr <= EXI_ISO_ISOLATION_LEVEL_TYPE_NO_IMD) /* PRQA S 3355, 3358 */ /* MD_Exi_13.7_14.1_Enum */ /*lint !e685 */
    {
      /* #40 Encode value */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (uint8)*isolationLevelPtr, 3);
    }
    /* #50 Enumeration is out of its allowed range */
    else
    { /* PRQA S 3201 */ /* MD_Exi_13.7_14.1_Enum */
      /* #60 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_ISOLATION_LEVEL, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_ISOLATION_LEVEL) && (EXI_ENCODE_ISO_ISOLATION_LEVEL == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_meterID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_METER_ID) && (EXI_ENCODE_ISO_METER_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_meterID( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_meterIDType, AUTOMATIC, EXI_APPL_DATA) meterIDPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (meterIDPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (meterIDPtr->Length > sizeof(meterIDPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of meterID */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode meterID as string value */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &meterIDPtr->Buffer[0], meterIDPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_METER_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_METER_ID) && (EXI_ENCODE_ISO_METER_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_paymentOption
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_PAYMENT_OPTION) && (EXI_ENCODE_ISO_PAYMENT_OPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_paymentOption( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_paymentOptionType, AUTOMATIC, EXI_APPL_DATA) paymentOptionPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (paymentOptionPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode enumeration value paymentOption as element */
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If enumeration value is in range */
    if (*paymentOptionPtr <= EXI_ISO_PAYMENT_OPTION_TYPE_EXTERNAL_PAYMENT) /* PRQA S 3355, 3358 */ /* MD_Exi_13.7_14.1_Enum */ /*lint !e685 */
    {
      /* #40 Encode value */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (uint8)*paymentOptionPtr, 1);
    }
    /* #50 Enumeration is out of its allowed range */
    else
    { /* PRQA S 3201 */ /* MD_Exi_13.7_14.1_Enum */
      /* #60 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_PAYMENT_OPTION, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_PAYMENT_OPTION) && (EXI_ENCODE_ISO_PAYMENT_OPTION == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_responseCode
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_responseCode( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_responseCodeType, AUTOMATIC, EXI_APPL_DATA) responseCodePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (responseCodePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode enumeration value responseCode as element */
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If enumeration value is in range */
    if (*responseCodePtr <= EXI_ISO_RESPONSE_CODE_TYPE_FAILED_CERTIFICATE_REVOKED) /* PRQA S 3355, 3358 */ /* MD_Exi_13.7_14.1_Enum */ /*lint !e685 */
    {
      /* #40 Encode value */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (uint8)*responseCodePtr, 5);
    }
    /* #50 Enumeration is out of its allowed range */
    else
    { /* PRQA S 3201 */ /* MD_Exi_13.7_14.1_Enum */
      /* #60 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_RESPONSE_CODE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_RESPONSE_CODE) && (EXI_ENCODE_ISO_RESPONSE_CODE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_serviceCategory
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SERVICE_CATEGORY) && (EXI_ENCODE_ISO_SERVICE_CATEGORY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_serviceCategory( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_serviceCategoryType, AUTOMATIC, EXI_APPL_DATA) serviceCategoryPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (serviceCategoryPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode enumeration value serviceCategory as element */
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If enumeration value is in range */
    if (*serviceCategoryPtr <= EXI_ISO_SERVICE_CATEGORY_TYPE_OTHER_CUSTOM) /* PRQA S 3355, 3358 */ /* MD_Exi_13.7_14.1_Enum */ /*lint !e685 */
    {
      /* #40 Encode value */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (uint8)*serviceCategoryPtr, 2);
    }
    /* #50 Enumeration is out of its allowed range */
    else
    { /* PRQA S 3201 */ /* MD_Exi_13.7_14.1_Enum */
      /* #60 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_CATEGORY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SERVICE_CATEGORY) && (EXI_ENCODE_ISO_SERVICE_CATEGORY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_serviceName
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SERVICE_NAME) && (EXI_ENCODE_ISO_SERVICE_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_serviceName( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_serviceNameType, AUTOMATIC, EXI_APPL_DATA) serviceNamePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (serviceNamePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (serviceNamePtr->Length > sizeof(serviceNamePtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of serviceName */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode serviceName as string value */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &serviceNamePtr->Buffer[0], serviceNamePtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_NAME, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SERVICE_NAME) && (EXI_ENCODE_ISO_SERVICE_NAME == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_serviceScope
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SERVICE_SCOPE) && (EXI_ENCODE_ISO_SERVICE_SCOPE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_serviceScope( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_serviceScopeType, AUTOMATIC, EXI_APPL_DATA) serviceScopePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (serviceScopePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (serviceScopePtr->Length > sizeof(serviceScopePtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of serviceScope */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode serviceScope as string value */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &serviceScopePtr->Buffer[0], serviceScopePtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SERVICE_SCOPE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SERVICE_SCOPE) && (EXI_ENCODE_ISO_SERVICE_SCOPE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_sessionID
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SESSION_ID) && (EXI_ENCODE_ISO_SESSION_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_sessionID( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_sessionIDType, AUTOMATIC, EXI_APPL_DATA) sessionIDPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (sessionIDPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (sessionIDPtr->Length > sizeof(sessionIDPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of sessionID */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode sessionID as byte array */
    Exi_VBSEncodeBytes(&EncWsPtr->EncWs, &sessionIDPtr->Buffer[0], sessionIDPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SESSION_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SESSION_ID) && (EXI_ENCODE_ISO_SESSION_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_sigMeterReading
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SIG_METER_READING) && (EXI_ENCODE_ISO_SIG_METER_READING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_sigMeterReading( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_sigMeterReadingType, AUTOMATIC, EXI_APPL_DATA) sigMeterReadingPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (sigMeterReadingPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (sigMeterReadingPtr->Length > sizeof(sigMeterReadingPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of sigMeterReading */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode sigMeterReading as byte array */
    Exi_VBSEncodeBytes(&EncWsPtr->EncWs, &sigMeterReadingPtr->Buffer[0], sigMeterReadingPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SIG_METER_READING, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SIG_METER_READING) && (EXI_ENCODE_ISO_SIG_METER_READING == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_tariffDescription
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_TARIFF_DESCRIPTION) && (EXI_ENCODE_ISO_TARIFF_DESCRIPTION == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_tariffDescription( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_tariffDescriptionType, AUTOMATIC, EXI_APPL_DATA) tariffDescriptionPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (tariffDescriptionPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (tariffDescriptionPtr->Length > sizeof(tariffDescriptionPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of tariffDescription */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode tariffDescription as string value */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &tariffDescriptionPtr->Buffer[0], tariffDescriptionPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_TARIFF_DESCRIPTION, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_TARIFF_DESCRIPTION) && (EXI_ENCODE_ISO_TARIFF_DESCRIPTION == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_unitSymbol
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_UNIT_SYMBOL) && (EXI_ENCODE_ISO_UNIT_SYMBOL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_unitSymbol( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_ISO_unitSymbolType, AUTOMATIC, EXI_APPL_DATA) unitSymbolPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (unitSymbolPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode enumeration value unitSymbol as element */
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If enumeration value is in range */
    if (*unitSymbolPtr <= EXI_ISO_UNIT_SYMBOL_TYPE_WH) /* PRQA S 3355, 3358 */ /* MD_Exi_13.7_14.1_Enum */ /*lint !e685 */
    {
      /* #40 Encode value */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, (uint8)*unitSymbolPtr, 3);
    }
    /* #50 Enumeration is out of its allowed range */
    else
    { /* PRQA S 3201 */ /* MD_Exi_13.7_14.1_Enum */
      /* #60 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_ENUM_OUT_OF_RANGE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_UNIT_SYMBOL, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_UNIT_SYMBOL) && (EXI_ENCODE_ISO_UNIT_SYMBOL == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_SchemaFragment
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SCHEMA_FRAGMENT) && (EXI_ENCODE_ISO_SCHEMA_FRAGMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_SchemaFragment( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EncWsPtr->InputData.RootElementId > EXI_SCHEMA_UNKNOWN_ELEMENT_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode all fragments of schema ISO */
    /* #30 Switch root element ID */
    switch(EncWsPtr->InputData.RootElementId)
    {
    /* Fragment AC_EVChargeParameter urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment AC_EVSEChargeParameter urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment AC_EVSEStatus urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment AC_EVSEStatus urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    case EXI_ISO_AUTHORIZATION_REQ_TYPE:
      /* #40 Fragment AuthorizationReq urn:iso:15118:2:2013:MsgBody */
      {
      #if (defined(EXI_ENCODE_ISO_AUTHORIZATION_REQ) && (EXI_ENCODE_ISO_AUTHORIZATION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_ISO_AuthorizationReqType, AUTOMATIC, EXI_APPL_DATA) AuthorizationReqPtr = (P2CONST(Exi_ISO_AuthorizationReqType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #50 Encode fragment AuthorizationReq */
        /* SE(AuthorizationReq) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 4, 8);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_ISO_AuthorizationReq(EncWsPtr, AuthorizationReqPtr);
        if(TRUE == EncWsPtr->EncWs.EERequired)
        {
          /* EE(AuthorizationReq) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 244, 8);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_AUTHORIZATION_REQ) && (EXI_ENCODE_ISO_AUTHORIZATION_REQ == STD_ON)) */
        break;
      }
    /* Fragment AuthorizationRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment Body urn:iso:15118:2:2013:MsgDef not required, it does not have an 'Id' attribute */
    /* Fragment BodyElement urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment BulkChargingComplete urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment BulkChargingComplete urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    /* Fragment BulkSOC urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment CableCheckReq urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment CableCheckRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment CanonicalizationMethod http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment Certificate urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    case EXI_ISO_CERTIFICATE_INSTALLATION_REQ_TYPE:
      /* #60 Fragment CertificateInstallationReq urn:iso:15118:2:2013:MsgBody */
      {
      #if (defined(EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ) && (EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_ISO_CertificateInstallationReqType, AUTOMATIC, EXI_APPL_DATA) CertificateInstallationReqPtr = (P2CONST(Exi_ISO_CertificateInstallationReqType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #70 Encode fragment CertificateInstallationReq */
        /* SE(CertificateInstallationReq) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 15, 8);
        Exi_Encode_ISO_CertificateInstallationReq(EncWsPtr, CertificateInstallationReqPtr);
        /* EE(CertificateInstallationReq) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 244, 8);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ) && (EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ == STD_ON)) */
        break;
      }
    /* Fragment CertificateInstallationRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    case EXI_ISO_CERTIFICATE_UPDATE_REQ_TYPE:
      /* #80 Fragment CertificateUpdateReq urn:iso:15118:2:2013:MsgBody */
      {
      #if (defined(EXI_ENCODE_ISO_CERTIFICATE_UPDATE_REQ) && (EXI_ENCODE_ISO_CERTIFICATE_UPDATE_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_ISO_CertificateUpdateReqType, AUTOMATIC, EXI_APPL_DATA) CertificateUpdateReqPtr = (P2CONST(Exi_ISO_CertificateUpdateReqType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #90 Encode fragment CertificateUpdateReq */
        /* SE(CertificateUpdateReq) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 17, 8);
        Exi_Encode_ISO_CertificateUpdateReq(EncWsPtr, CertificateUpdateReqPtr);
        /* EE(CertificateUpdateReq) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 244, 8);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_UPDATE_REQ) && (EXI_ENCODE_ISO_CERTIFICATE_UPDATE_REQ == STD_ON)) */
        break;
      }
    /* Fragment CertificateUpdateRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ChargeParameterDiscoveryReq urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ChargeParameterDiscoveryRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ChargeProgress urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ChargeService urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ChargingComplete urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ChargingComplete urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    /* Fragment ChargingProfile urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ChargingProfileEntryMaxNumberOfPhasesInUse urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment ChargingProfileEntryMaxPower urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment ChargingProfileEntryStart urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment ChargingSession urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ChargingStatusReq urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ChargingStatusRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ConsumptionCost urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    case EXI_ISO_CONTRACT_SIGNATURE_CERT_CHAIN_TYPE:
      /* #100 Fragment ContractSignatureCertChain urn:iso:15118:2:2013:MsgBody */
      {
      #if (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_ISO_CertificateChainType, AUTOMATIC, EXI_APPL_DATA) ContractSignatureCertChainPtr = (P2CONST(Exi_ISO_CertificateChainType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #110 Encode fragment ContractSignatureCertChain */
        /* SE(ContractSignatureCertChain) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 33, 8);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_ISO_CertificateChain(EncWsPtr, ContractSignatureCertChainPtr);
        if(TRUE == EncWsPtr->EncWs.EERequired)
        {
          /* EE(ContractSignatureCertChain) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 244, 8);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */
        break;
      }
    case EXI_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY_TYPE:
      /* #120 Fragment ContractSignatureEncryptedPrivateKey urn:iso:15118:2:2013:MsgBody */
      {
      #if (defined(EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY) && (EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_ISO_ContractSignatureEncryptedPrivateKeyType, AUTOMATIC, EXI_APPL_DATA) ContractSignatureEncryptedPrivateKeyPtr = (P2CONST(Exi_ISO_ContractSignatureEncryptedPrivateKeyType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #130 Encode fragment ContractSignatureEncryptedPrivateKey */
        /* SE(ContractSignatureEncryptedPrivateKey) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 34, 8);
        Exi_Encode_ISO_ContractSignatureEncryptedPrivateKey(EncWsPtr, ContractSignatureEncryptedPrivateKeyPtr);
        /* EE(ContractSignatureEncryptedPrivateKey) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 244, 8);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY) && (EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY == STD_ON)) */
        break;
      }
    /* Fragment Cost urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment CurrentDemandReq urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment CurrentDemandRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment DC_EVChargeParameter urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment DC_EVPowerDeliveryParameter urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment DC_EVSEChargeParameter urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment DC_EVSEStatus urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment DC_EVSEStatus urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    /* Fragment DC_EVStatus urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment DC_EVStatus urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    case EXI_ISO_DHPUBLICKEY_TYPE:
      /* #140 Fragment DHpublickey urn:iso:15118:2:2013:MsgBody */
      {
      #if (defined(EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY) && (EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_ISO_DiffieHellmanPublickeyType, AUTOMATIC, EXI_APPL_DATA) DHpublickeyPtr = (P2CONST(Exi_ISO_DiffieHellmanPublickeyType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #150 Encode fragment DHpublickey */
        /* SE(DHpublickey) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 45, 8);
        Exi_Encode_ISO_DiffieHellmanPublickey(EncWsPtr, DHpublickeyPtr);
        /* EE(DHpublickey) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 244, 8);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY) && (EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY == STD_ON)) */
        break;
      }
    /* Fragment DSAKeyValue http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment DepartureTime urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment DigestMethod http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment DigestValue http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment EAmount urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EPriceLevel urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVCCID urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVChargeParameter urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVEnergyCapacity urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVEnergyRequest urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVErrorCode urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVMaxCurrent urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVMaxVoltage urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVMaximumCurrentLimit urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVMaximumCurrentLimit urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    /* Fragment EVMaximumPowerLimit urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVMaximumPowerLimit urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    /* Fragment EVMaximumVoltageLimit urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVMaximumVoltageLimit urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    /* Fragment EVMinCurrent urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVPowerDeliveryParameter urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVRESSSOC urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVReady urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVSEChargeParameter urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVSECurrentLimitAchieved urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVSECurrentRegulationTolerance urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVSEEnergyToBeDelivered urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVSEID urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVSEIsolationStatus urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVSEMaxCurrent urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVSEMaxCurrent urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    /* Fragment EVSEMaximumCurrentLimit urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVSEMaximumCurrentLimit urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    /* Fragment EVSEMaximumPowerLimit urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVSEMaximumPowerLimit urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    /* Fragment EVSEMaximumVoltageLimit urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVSEMaximumVoltageLimit urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    /* Fragment EVSEMinimumCurrentLimit urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVSEMinimumVoltageLimit urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVSENominalVoltage urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVSENotification urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVSEPeakCurrentRipple urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVSEPowerLimitAchieved urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVSEPresentCurrent urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVSEPresentVoltage urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVSEProcessing urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVSEStatus urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVSEStatusCode urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVSETimeStamp urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVSEVoltageLimitAchieved urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVStatus urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment EVTargetCurrent urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EVTargetVoltage urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment EnergyTransferMode urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment Entry urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment Exponent http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment FaultCode urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment FaultMsg urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment FreeService urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment FullSOC urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment G http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment GenChallenge urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment HMACOutputLength http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment Header urn:iso:15118:2:2013:MsgDef not required, it does not have an 'Id' attribute */
    /* Fragment J http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment KeyInfo http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment KeyName http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment KeyValue http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment ListOfRootCertificateIDs urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment Manifest http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment MaxEntriesSAScheduleTuple urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment MeterID urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment MeterInfo urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment MeterReading urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment MeterStatus urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    case EXI_ISO_METERING_RECEIPT_REQ_TYPE:
      /* #160 Fragment MeteringReceiptReq urn:iso:15118:2:2013:MsgBody */
      {
      #if (defined(EXI_ENCODE_ISO_METERING_RECEIPT_REQ) && (EXI_ENCODE_ISO_METERING_RECEIPT_REQ == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_ISO_MeteringReceiptReqType, AUTOMATIC, EXI_APPL_DATA) MeteringReceiptReqPtr = (P2CONST(Exi_ISO_MeteringReceiptReqType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #170 Encode fragment MeteringReceiptReq */
        /* SE(MeteringReceiptReq) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 121, 8);
        Exi_Encode_ISO_MeteringReceiptReq(EncWsPtr, MeteringReceiptReqPtr);
        /* EE(MeteringReceiptReq) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 244, 8);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_METERING_RECEIPT_REQ) && (EXI_ENCODE_ISO_METERING_RECEIPT_REQ == STD_ON)) */
        break;
      }
    /* Fragment MeteringReceiptRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment MgmtData http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment Modulus http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment Multiplier urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment Notification urn:iso:15118:2:2013:MsgHeader not required, it does not have an 'Id' attribute */
    /* Fragment NotificationMaxDelay urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment NumEPriceLevels urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment OEMProvisioningCert urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment Object http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment P http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment PGPData http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment PGPKeyID http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment PGPKeyPacket http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment PMax urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment PMaxSchedule urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment PMaxScheduleEntry urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment Parameter urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment ParameterSet urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment ParameterSetID urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment PaymentDetailsReq urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment PaymentDetailsRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment PaymentOption urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment PaymentOptionList urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment PaymentServiceSelectionReq urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment PaymentServiceSelectionRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment PgenCounter http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment PowerDeliveryReq urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment PowerDeliveryRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment PreChargeReq urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment PreChargeRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ProfileEntry urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment Q http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment RCD urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment RSAKeyValue http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment ReceiptRequired urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment Reference http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment RelativeTimeInterval urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment RemainingTimeToBulkSoC urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment RemainingTimeToFullSoC urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment RequestedEnergyTransferMode urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ResponseCode urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment RetrievalMethod http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment RetryCounter urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment RootCertificateID urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    case EXI_ISO_SAPROVISIONING_CERTIFICATE_CHAIN_TYPE:
      /* #180 Fragment SAProvisioningCertificateChain urn:iso:15118:2:2013:MsgBody */
      {
      #if (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_ISO_CertificateChainType, AUTOMATIC, EXI_APPL_DATA) SAProvisioningCertificateChainPtr = (P2CONST(Exi_ISO_CertificateChainType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #190 Encode fragment SAProvisioningCertificateChain */
        /* SE(SAProvisioningCertificateChain) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 166, 8);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_ISO_CertificateChain(EncWsPtr, SAProvisioningCertificateChainPtr);
        if(TRUE == EncWsPtr->EncWs.EERequired)
        {
          /* EE(SAProvisioningCertificateChain) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 244, 8);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_CERTIFICATE_CHAIN) && (EXI_ENCODE_ISO_CERTIFICATE_CHAIN == STD_ON)) */
        break;
      }
    /* Fragment SAScheduleList urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment SAScheduleTuple urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment SAScheduleTupleID urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment SAScheduleTupleID urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    /* Fragment SASchedules urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment SPKIData http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment SPKISexp http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    case EXI_ISO_SALES_TARIFF_TYPE:
      /* #200 Fragment SalesTariff urn:iso:15118:2:2013:MsgDataTypes */
      {
      #if (defined(EXI_ENCODE_ISO_SALES_TARIFF) && (EXI_ENCODE_ISO_SALES_TARIFF == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_ISO_SalesTariffType, AUTOMATIC, EXI_APPL_DATA) SalesTariffPtr = (P2CONST(Exi_ISO_SalesTariffType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #210 Encode fragment SalesTariff */
        /* SE(SalesTariff) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 174, 8);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_ISO_SalesTariff(EncWsPtr, SalesTariffPtr);
        if(TRUE == EncWsPtr->EncWs.EERequired)
        {
          /* EE(SalesTariff) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 244, 8);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_SALES_TARIFF) && (EXI_ENCODE_ISO_SALES_TARIFF == STD_ON)) */
        break;
      }
    /* Fragment SalesTariffDescription urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment SalesTariffEntry urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment SalesTariffID urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment Seed http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment SelectedPaymentOption urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment SelectedService urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment SelectedServiceList urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment Service urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment ServiceCategory urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ServiceCategory urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    /* Fragment ServiceDetailReq urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ServiceDetailRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ServiceDiscoveryReq urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ServiceDiscoveryRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ServiceID urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ServiceID urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    /* Fragment ServiceList urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ServiceName urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment ServiceParameterList urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ServiceScope urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment ServiceScope urn:iso:15118:2:2013:MsgDataTypes already implemented or not required */
    /* Fragment SessionID urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment SessionID urn:iso:15118:2:2013:MsgHeader already implemented or not required */
    /* Fragment SessionSetupReq urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment SessionSetupRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment SessionStopReq urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment SessionStopRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment SigMeterReading urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment Signature http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment SignatureMethod http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment SignatureProperties http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment SignatureProperty http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment SignatureValue http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment SignedInfo http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment SubCertificates urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment SupportedEnergyTransferMode urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment TMeter urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment TimeInterval urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment Transform http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment Transforms http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment Unit urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment V2G_Message urn:iso:15118:2:2013:MsgDef not required, it does not have an 'Id' attribute */
    /* Fragment Value urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment WeldingDetectionReq urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment WeldingDetectionRes urn:iso:15118:2:2013:MsgBody not required, it does not have an 'Id' attribute */
    /* Fragment X509CRL http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment X509Certificate http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment X509Data http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment X509IssuerName http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment X509IssuerSerial http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment X509SKI http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment X509SerialNumber http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment X509SubjectName http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment XPath http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment Y http://www.w3.org/2000/09/xmldsig# not required, it does not directly belong to this schema */
    /* Fragment amount urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment amountMultiplier urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment boolValue urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment byteValue urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment costKind urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment duration urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    case EXI_ISO_E_MAID_TYPE:
      /* #220 Fragment eMAID urn:iso:15118:2:2013:MsgBody */
      {
      #if (defined(EXI_ENCODE_ISO_E_MAID) && (EXI_ENCODE_ISO_E_MAID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* #230 Encode fragment eMAID */
        /* SE(eMAID) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 236, 8);
        /* Schema-informed Element Fragment Grammar required */
      #if (defined(EXI_ENCODE_ISO_E_MAID_ELEMENT_FRAGMENT) && (EXI_ENCODE_ISO_E_MAID_ELEMENT_FRAGMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_ISO_eMAID_ElementFragment(EncWsPtr, (P2CONST(Exi_ISO_eMAIDType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr)); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
      #else
        /* not supported in this configuration */
        Exi_CallInternalDetReportError(EXI_ENCODER_INSTANCE_ID, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
        /* Encode as EE */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 255, 9);
      #endif /* (defined(EXI_ENCODE_ISO_E_MAID_ELEMENT_FRAGMENT) && (EXI_ENCODE_ISO_E_MAID_ELEMENT_FRAGMENT == STD_ON)) */
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 244, 8);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_E_MAID) && (EXI_ENCODE_ISO_E_MAID == STD_ON)) */
        break;
      }
    case EXI_ISO_EMAID_TYPE:
      /* #240 Fragment eMAID urn:iso:15118:2:2013:MsgBody */
      {
      #if (defined(EXI_ENCODE_ISO_EMAID) && (EXI_ENCODE_ISO_EMAID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        /* #250 Encode fragment eMAID */
        /* SE(eMAID) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 236, 8);
        /* Schema-informed Element Fragment Grammar required */
      #if (defined(EXI_ENCODE_ISO_EMAID_ELEMENT_FRAGMENT) && (EXI_ENCODE_ISO_EMAID_ELEMENT_FRAGMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_ISO_EMAID_ElementFragment(EncWsPtr, (P2CONST(Exi_ISO_EMAIDType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr)); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
      #else
        /* not supported in this configuration */
        Exi_CallInternalDetReportError(EXI_ENCODER_INSTANCE_ID, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
        /* Encode as EE */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 255, 9);
      #endif /* (defined(EXI_ENCODE_ISO_EMAID_ELEMENT_FRAGMENT) && (EXI_ENCODE_ISO_EMAID_ELEMENT_FRAGMENT == STD_ON)) */
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 244, 8);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_ISO_EMAID) && (EXI_ENCODE_ISO_EMAID == STD_ON)) */
        break;
      }
    /* Fragment intValue urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment physicalValue urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment shortValue urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment start urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment startValue urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    /* Fragment stringValue urn:iso:15118:2:2013:MsgDataTypes not required, it does not have an 'Id' attribute */
    default:
      /* #260 Unknown fragment */
      {
        /* #270 Set status code to error */
        EncWsPtr->EncWs.StatusCode = EXI_E_ELEMENT_NOT_AVAILABLE;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SCHEMA_FRAGMENT, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SCHEMA_FRAGMENT) && (EXI_ENCODE_ISO_SCHEMA_FRAGMENT == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_ISO_SchemaRoot
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_ISO_SCHEMA_ROOT) && (EXI_ENCODE_ISO_SCHEMA_ROOT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_ISO_SchemaRoot( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EncWsPtr->InputData.RootElementId > EXI_ISO_WELDING_DETECTION_RES_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Swtich RootElementId */
    switch(EncWsPtr->InputData.RootElementId)
    {
    case EXI_ISO_V2G_MESSAGE_TYPE: /* 76 */
      /* #30 Element V2G_Message */
      {
      #if (defined(EXI_ENCODE_ISO_V2G_MESSAGE) && (EXI_ENCODE_ISO_V2G_MESSAGE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_ISO_V2G_MessageType, AUTOMATIC, EXI_APPL_DATA) V2G_MessagePtr = (P2CONST(Exi_ISO_V2G_MessageType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #40 If supported: Encode element V2G_Message */
        /* SE(V2G_Message) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 76, 7);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_ISO_V2G_Message(EncWsPtr, V2G_MessagePtr);
        /* EE(V2G_Message) */
        /* Check EE encoding for V2G_Message */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    default:
      /* #50 Default path */
      {
        /* #60 Set status code to error */
        EncWsPtr->EncWs.StatusCode = EXI_E_INV_EVENT_CODE;
        break;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_ISO_SCHEMA_ROOT, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_ISO_SCHEMA_ROOT) && (EXI_ENCODE_ISO_SCHEMA_ROOT == STD_ON)) */


#define EXI_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/* PRQA L:NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */
#endif /* (defined(EXI_ENABLE_ENCODE_ISO_MESSAGE_SET) && (EXI_ENABLE_ENCODE_ISO_MESSAGE_SET == STD_ON)) */

