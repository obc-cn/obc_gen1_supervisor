/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Exi_XMLSIG_SchemaEncoder.c
 *        \brief  Efficient XML Interchange XMLSIG encoder source file
 *
 *      \details  Vector static code implementation for the Efficient XML Interchange sub-component XMLSIG encoder.
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file Exi.h.
 *********************************************************************************************************************/
/* PRQA S 0857 EOF */ /* MD_Exi_1.1_0857 */ /* [L] Number of macro definitions exceeds 1024 - program is non-conforming. */

#define EXI_XMLSIG_SCHEMA_ENCODER_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0828 EXI_XMLSIG_SCHEMA_ENCODER_C_IF_NESTING */ /* MD_MSR_1.1_828 */
#include "Exi_XMLSIG_SchemaEncoder.h"
#include "Exi_BSEncoder.h"
/* PRQA L:EXI_XMLSIG_SCHEMA_ENCODER_C_IF_NESTING */ /* MD_MSR_1.1_828 */
/**********************************************************************************************************************
*  VERSION CHECK
*********************************************************************************************************************/
#if ( (EXI_SW_MAJOR_VERSION != 6u) || (EXI_SW_MINOR_VERSION != 0u) || (EXI_SW_PATCH_VERSION != 1u) )
  #error "Vendor specific version numbers of Exi.h and Exi_XMLSIG_SchemaEncoder.c are inconsistent"
#endif

#if (!defined (EXI_ENABLE_ENCODE_XMLSIG_MESSAGE_SET))
# if (defined (EXI_ENABLE_XMLSIG_MESSAGE_SET))
#  define EXI_ENABLE_ENCODE_XMLSIG_MESSAGE_SET   EXI_ENABLE_XMLSIG_MESSAGE_SET
# else
#  define EXI_ENABLE_ENCODE_XMLSIG_MESSAGE_SET   STD_OFF
# endif
#endif

#if (defined (EXI_ENABLE_ENCODE_XMLSIG_MESSAGE_SET) && (EXI_ENABLE_ENCODE_XMLSIG_MESSAGE_SET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */

/* PRQA S 0715 NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */ /* MD_Exi_1.1 */


#define EXI_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/**********************************************************************************************************************
 *  FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_AttributeAlgorithm
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_AttributeAlgorithm( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_AttributeAlgorithmType, AUTOMATIC, EXI_APPL_DATA) AttributeAlgorithmPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AttributeAlgorithmPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (AttributeAlgorithmPtr->Length > sizeof(AttributeAlgorithmPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &AttributeAlgorithmPtr->Buffer[0], AttributeAlgorithmPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_AttributeEncoding
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ENCODING) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ENCODING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_AttributeEncoding( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_AttributeEncodingType, AUTOMATIC, EXI_APPL_DATA) AttributeEncodingPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AttributeEncodingPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (AttributeEncodingPtr->Length > sizeof(AttributeEncodingPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &AttributeEncodingPtr->Buffer[0], AttributeEncodingPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_ATTRIBUTE_ENCODING, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ENCODING) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ENCODING == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_AttributeId
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_AttributeId( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_AttributeIdType, AUTOMATIC, EXI_APPL_DATA) AttributeIdPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AttributeIdPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (AttributeIdPtr->Length > sizeof(AttributeIdPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &AttributeIdPtr->Buffer[0], AttributeIdPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_ATTRIBUTE_ID, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_AttributeMime
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_MIME) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_MIME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_AttributeMime( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_AttributeMimeType, AUTOMATIC, EXI_APPL_DATA) AttributeMimePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AttributeMimePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (AttributeMimePtr->Length > sizeof(AttributeMimePtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &AttributeMimePtr->Buffer[0], AttributeMimePtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_ATTRIBUTE_MIME, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_MIME) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_MIME == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_AttributeTarget
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_TARGET) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_TARGET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_AttributeTarget( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_AttributeTargetType, AUTOMATIC, EXI_APPL_DATA) AttributeTargetPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AttributeTargetPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (AttributeTargetPtr->Length > sizeof(AttributeTargetPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &AttributeTargetPtr->Buffer[0], AttributeTargetPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_ATTRIBUTE_TARGET, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_TARGET) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_TARGET == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_Attribute
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE) && (EXI_ENCODE_XMLSIG_ATTRIBUTE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_Attribute( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_AttributeType, AUTOMATIC, EXI_APPL_DATA) AttributePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AttributePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (AttributePtr->Length > sizeof(AttributePtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &AttributePtr->Buffer[0], AttributePtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_ATTRIBUTE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE) && (EXI_ENCODE_XMLSIG_ATTRIBUTE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_AttributeURI
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_URI) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_AttributeURI( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_AttributeURIType, AUTOMATIC, EXI_APPL_DATA) AttributeURIPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (AttributeURIPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (AttributeURIPtr->Length > sizeof(AttributeURIPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &AttributeURIPtr->Buffer[0], AttributeURIPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_ATTRIBUTE_URI, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_URI) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_CanonicalizationMethod
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_CANONICALIZATION_METHOD) && (EXI_ENCODE_XMLSIG_CANONICALIZATION_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_CanonicalizationMethod( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_CanonicalizationMethodType, AUTOMATIC, EXI_APPL_DATA) CanonicalizationMethodPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_GenericElementType) nextPtr;
  #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
  uint16_least i;
  #endif /*#if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
  #endif /* #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (CanonicalizationMethodPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode attribute Algorithm */
    /* AT(Algorithm) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_AttributeAlgorithm(EncWsPtr, (CanonicalizationMethodPtr->Algorithm));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_CANONICALIZATION_METHOD, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) */

    /* #30 If optional element GenericElement is included */
    if ( (1 == CanonicalizationMethodPtr->GenericElementFlag) && (NULL_PTR != CanonicalizationMethodPtr->GenericElement) )
    {
      #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #40 Initialize next pointer with the first GenericElement element */
      nextPtr = (Exi_XMLSIG_GenericElementType*)CanonicalizationMethodPtr->GenericElement;
      /* #50 Loop over all GenericElement elements */
      #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
      for(i=0; i<EXI_MAXOCCURS_GENERICELEMENT; i++)
      #endif /* #if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
      {
        /* #60 Encode element GenericElement */
        /* SE(GenericElement) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
        Exi_Encode_XMLSIG_GenericElement(EncWsPtr, nextPtr);
        /* EE(GenericElement) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        nextPtr = (Exi_XMLSIG_GenericElementType*)(nextPtr->NextGenericElementPtr);
        #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
        /* #70 If this is the last element to encode */
        if(NULL_PTR == nextPtr)
        {
          /* #80 End the loop */
          break;
        }
        #endif /* #if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
      }
      /* #90 If maximum possible number of GenericElement's was encoded */
      #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
      if(i == EXI_MAXOCCURS_GENERICELEMENT)
      #endif /*#if (EXI_MAXOCCURS_GENERICELEMENT > 1)*/
      {
        /* #100 If there are more elements in the list */
        if (nextPtr != NULL_PTR)
        {
          /* #110 Set status code to error */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        }
      }
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_CANONICALIZATION_METHOD, EXI_E_INV_PARAM);
      #endif /* #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      /* EE(CanonicalizationMethod) */
      /* Max Occurs is unbounded */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
    /* #120 Optional element GenericElement is not included */
    else
    {
      /* EE(CanonicalizationMethod) */
      /* #130 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_CANONICALIZATION_METHOD, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_CANONICALIZATION_METHOD) && (EXI_ENCODE_XMLSIG_CANONICALIZATION_METHOD == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_CryptoBinary
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_CryptoBinary( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_CryptoBinaryType, AUTOMATIC, EXI_APPL_DATA) CryptoBinaryPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (CryptoBinaryPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (CryptoBinaryPtr->Length > sizeof(CryptoBinaryPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of CryptoBinary */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode CryptoBinary as byte array */
    Exi_VBSEncodeBytes(&EncWsPtr->EncWs, &CryptoBinaryPtr->Buffer[0], CryptoBinaryPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_CRYPTO_BINARY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_DSAKeyValue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_DSAKEY_VALUE) && (EXI_ENCODE_XMLSIG_DSAKEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_DSAKeyValue( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_DSAKeyValueType, AUTOMATIC, EXI_APPL_DATA) DSAKeyValuePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DSAKeyValuePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element P */
    /* SE(P) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_CryptoBinary(EncWsPtr, (DSAKeyValuePtr->P));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    /* EE(P) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element Q */
    /* SE(Q) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_CryptoBinary(EncWsPtr, (DSAKeyValuePtr->Q));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    /* EE(Q) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #40 If optional element G is included */
    if ( (1 == DSAKeyValuePtr->GFlag) && (NULL_PTR != DSAKeyValuePtr->G) )
    {
      /* #50 Encode element G */
      /* SE(G) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_CryptoBinary(EncWsPtr, (DSAKeyValuePtr->G));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
      /* EE(G) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #60 Encode element Y */
    /* SE(Y) */
    if(0 == DSAKeyValuePtr->GFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_CryptoBinary(EncWsPtr, (DSAKeyValuePtr->Y));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    /* EE(Y) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #70 If optional element J is included */
    if ( (1 == DSAKeyValuePtr->JFlag) && (NULL_PTR != DSAKeyValuePtr->J) )
    {
      /* #80 Encode element J */
      /* SE(J) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_CryptoBinary(EncWsPtr, (DSAKeyValuePtr->J));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
      /* EE(J) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #90 Encode element Seed */
    /* SE(Seed) */
    if(0 == DSAKeyValuePtr->JFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_CryptoBinary(EncWsPtr, (DSAKeyValuePtr->Seed));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    /* EE(Seed) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #100 Encode element PgenCounter */
    /* SE(PgenCounter) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_CryptoBinary(EncWsPtr, (DSAKeyValuePtr->PgenCounter));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_DSAKEY_VALUE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    /* EE(PgenCounter) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_DSAKEY_VALUE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_DSAKEY_VALUE) && (EXI_ENCODE_XMLSIG_DSAKEY_VALUE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_DigestMethod
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_DIGEST_METHOD) && (EXI_ENCODE_XMLSIG_DIGEST_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_DigestMethod( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_DigestMethodType, AUTOMATIC, EXI_APPL_DATA) DigestMethodPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_GenericElementType) nextPtr;
  #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
  uint16_least i;
  #endif /*#if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
  #endif /* #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DigestMethodPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode attribute Algorithm */
    /* AT(Algorithm) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_AttributeAlgorithm(EncWsPtr, (DigestMethodPtr->Algorithm));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_DIGEST_METHOD, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) */

    /* #30 If optional element GenericElement is included */
    if ( (1 == DigestMethodPtr->GenericElementFlag) && (NULL_PTR != DigestMethodPtr->GenericElement) )
    {
      #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #40 Initialize next pointer with the first GenericElement element */
      nextPtr = (Exi_XMLSIG_GenericElementType*)DigestMethodPtr->GenericElement;
      /* #50 Loop over all GenericElement elements */
      #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
      for(i=0; i<EXI_MAXOCCURS_GENERICELEMENT; i++)
      #endif /* #if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
      {
        /* #60 Encode element GenericElement */
        /* SE(GenericElement) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
        Exi_Encode_XMLSIG_GenericElement(EncWsPtr, nextPtr);
        /* EE(GenericElement) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        nextPtr = (Exi_XMLSIG_GenericElementType*)(nextPtr->NextGenericElementPtr);
        #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
        /* #70 If this is the last element to encode */
        if(NULL_PTR == nextPtr)
        {
          /* #80 End the loop */
          break;
        }
        #endif /* #if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
      }
      /* #90 If maximum possible number of GenericElement's was encoded */
      #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
      if(i == EXI_MAXOCCURS_GENERICELEMENT)
      #endif /*#if (EXI_MAXOCCURS_GENERICELEMENT > 1)*/
      {
        /* #100 If there are more elements in the list */
        if (nextPtr != NULL_PTR)
        {
          /* #110 Set status code to error */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        }
      }
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_DIGEST_METHOD, EXI_E_INV_PARAM);
      #endif /* #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      /* EE(DigestMethod) */
      /* Max Occurs is unbounded */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
    /* #120 Optional element GenericElement is not included */
    else
    {
      /* EE(DigestMethod) */
      /* #130 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_DIGEST_METHOD, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_DIGEST_METHOD) && (EXI_ENCODE_XMLSIG_DIGEST_METHOD == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_DigestValue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_DIGEST_VALUE) && (EXI_ENCODE_XMLSIG_DIGEST_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_DigestValue( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_DigestValueType, AUTOMATIC, EXI_APPL_DATA) DigestValuePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (DigestValuePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (DigestValuePtr->Length > sizeof(DigestValuePtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of DigestValue */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode DigestValue as byte array */
    Exi_VBSEncodeBytes(&EncWsPtr->EncWs, &DigestValuePtr->Buffer[0], DigestValuePtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_DIGEST_VALUE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_DIGEST_VALUE) && (EXI_ENCODE_XMLSIG_DIGEST_VALUE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_GenericElement
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_GenericElement( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_GenericElementType, AUTOMATIC, EXI_APPL_DATA) GenericElementPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (GenericElementPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_GENERIC_ELEMENT, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_KeyInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_KEY_INFO) && (EXI_ENCODE_XMLSIG_KEY_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_KeyInfo( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_KeyInfoType, AUTOMATIC, EXI_APPL_DATA) KeyInfoPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_KeyInfoChoiceType) nextPtr;
  #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
  uint16_least i;
  #endif /*#if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (KeyInfoPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (KeyInfoPtr->ChoiceElement == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element Id is included */
    if ( (1 == KeyInfoPtr->IdFlag) && (NULL_PTR != KeyInfoPtr->Id) )
    {
      /* #30 Encode attribute Id */
      /* AT(Id) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 4);
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_AttributeId(EncWsPtr, (KeyInfoPtr->Id));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */

    }
    /* #40 Initialize next pointer with the first ChoiceElement element */
    nextPtr = (Exi_XMLSIG_KeyInfoChoiceType*)KeyInfoPtr->ChoiceElement;
    /* #50 Loop over all ChoiceElement elements */
    #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
    for(i=0; i<EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE; i++)
    #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
    {
      /* #60 Start of choice element KeyInfo */
      /* #70 If not exact one choice element flag is set */
      if (1 != (  nextPtr->KeyNameFlag
                + nextPtr->KeyValueFlag
                + nextPtr->RetrievalMethodFlag
                + nextPtr->X509DataFlag
                + nextPtr->PGPDataFlag
                + nextPtr->SPKIDataFlag
                + nextPtr->MgmtDataFlag
                + nextPtr->GenericElementFlag) )
      {
        /* #80 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_CHOICE_SELECTION, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_ENCODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
        return;
      }
      /* #90 If choice element is KeyName */
      else if(1 == nextPtr->KeyNameFlag)
      {
        /* #100 Encode KeyName element */
        /* SE(KeyName) */
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        if((0 == i) && (0 == KeyInfoPtr->IdFlag))
        #else
        if(0 == KeyInfoPtr->IdFlag)
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 4);
        }
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 4);
        }
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
      #if (defined(EXI_ENCODE_XMLSIG_KEY_NAME) && (EXI_ENCODE_XMLSIG_KEY_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_XMLSIG_KeyName(EncWsPtr, (nextPtr->ChoiceValue.KeyName));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_KEY_NAME) && (EXI_ENCODE_XMLSIG_KEY_NAME == STD_ON)) */
        /* EE(KeyName) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      /* #110 If choice element is KeyValue */
      else if(1 == nextPtr->KeyValueFlag)
      {
        /* #120 Encode KeyValue element */
        /* SE(KeyValue) */
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        if((0 == i) && (0 == KeyInfoPtr->IdFlag))
        #else
        if(0 == KeyInfoPtr->IdFlag)
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 4);
        }
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 4);
        }
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
      #if (defined(EXI_ENCODE_XMLSIG_KEY_VALUE) && (EXI_ENCODE_XMLSIG_KEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_XMLSIG_KeyValue(EncWsPtr, (nextPtr->ChoiceValue.KeyValue));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_KEY_VALUE) && (EXI_ENCODE_XMLSIG_KEY_VALUE == STD_ON)) */
        /* EE(KeyValue) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      /* #130 If choice element is RetrievalMethod */
      else if(1 == nextPtr->RetrievalMethodFlag)
      {
        /* #140 Encode RetrievalMethod element */
        /* SE(RetrievalMethod) */
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        if((0 == i) && (0 == KeyInfoPtr->IdFlag))
        #else
        if(0 == KeyInfoPtr->IdFlag)
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 4);
        }
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 4);
        }
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
        EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_XMLSIG_RETRIEVAL_METHOD) && (EXI_ENCODE_XMLSIG_RETRIEVAL_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_XMLSIG_RetrievalMethod(EncWsPtr, (nextPtr->ChoiceValue.RetrievalMethod));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_RETRIEVAL_METHOD) && (EXI_ENCODE_XMLSIG_RETRIEVAL_METHOD == STD_ON)) */
        /* EE(RetrievalMethod) */
        /* Check EE encoding */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      }
      /* #150 If choice element is X509Data */
      else if(1 == nextPtr->X509DataFlag)
      {
        /* #160 Encode X509Data element */
        /* SE(X509Data) */
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        if((0 == i) && (0 == KeyInfoPtr->IdFlag))
        #else
        if(0 == KeyInfoPtr->IdFlag)
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 4, 4);
        }
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 4);
        }
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
        EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_XMLSIG_X509DATA) && (EXI_ENCODE_XMLSIG_X509DATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_XMLSIG_X509Data(EncWsPtr, (nextPtr->ChoiceValue.X509Data));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_X509DATA) && (EXI_ENCODE_XMLSIG_X509DATA == STD_ON)) */
        /* EE(X509Data) */
        /* Check EE encoding */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      }
      /* #170 If choice element is PGPData */
      else if(1 == nextPtr->PGPDataFlag)
      {
        /* #180 Encode PGPData element */
        /* SE(PGPData) */
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        if((0 == i) && (0 == KeyInfoPtr->IdFlag))
        #else
        if(0 == KeyInfoPtr->IdFlag)
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 5, 4);
        }
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 4, 4);
        }
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
      #if (defined(EXI_ENCODE_XMLSIG_PGPDATA) && (EXI_ENCODE_XMLSIG_PGPDATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_XMLSIG_PGPData(EncWsPtr, (nextPtr->ChoiceValue.PGPData));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_PGPDATA) && (EXI_ENCODE_XMLSIG_PGPDATA == STD_ON)) */
        /* EE(PGPData) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      /* #190 If choice element is SPKIData */
      else if(1 == nextPtr->SPKIDataFlag)
      {
        /* #200 Encode SPKIData element */
        /* SE(SPKIData) */
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        if((0 == i) && (0 == KeyInfoPtr->IdFlag))
        #else
        if(0 == KeyInfoPtr->IdFlag)
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 6, 4);
        }
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 5, 4);
        }
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
        EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_XMLSIG_SPKIDATA) && (EXI_ENCODE_XMLSIG_SPKIDATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_XMLSIG_SPKIData(EncWsPtr, (nextPtr->ChoiceValue.SPKIData));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_SPKIDATA) && (EXI_ENCODE_XMLSIG_SPKIDATA == STD_ON)) */
        /* EE(SPKIData) */
        /* Check EE encoding */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      }
      /* #210 If choice element is MgmtData */
      else if(1 == nextPtr->MgmtDataFlag)
      {
        /* #220 Encode MgmtData element */
        /* SE(MgmtData) */
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        if((0 == i) && (0 == KeyInfoPtr->IdFlag))
        #else
        if(0 == KeyInfoPtr->IdFlag)
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 7, 4);
        }
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 6, 4);
        }
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
      #if (defined(EXI_ENCODE_XMLSIG_MGMT_DATA) && (EXI_ENCODE_XMLSIG_MGMT_DATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_XMLSIG_MgmtData(EncWsPtr, (nextPtr->ChoiceValue.MgmtData));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_MGMT_DATA) && (EXI_ENCODE_XMLSIG_MGMT_DATA == STD_ON)) */
        /* EE(MgmtData) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      /* #230 If choice element is GenericElement */
      else if(1 == nextPtr->GenericElementFlag)
      {
        /* #240 Encode GenericElement element */
        /* SE(GenericElement) */
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        if((0 == i) && (0 == KeyInfoPtr->IdFlag))
        #else
        if(0 == KeyInfoPtr->IdFlag)
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 8, 4);
        }
        #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 7, 4);
        }
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
      #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_XMLSIG_GenericElement(EncWsPtr, (nextPtr->ChoiceValue.GenericElement));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_INFO, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
        /* EE(GenericElement) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        /* Choice Element not supported */
        /* #250 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      /* End of Choice Element */
      nextPtr = (Exi_XMLSIG_KeyInfoChoiceType*)(nextPtr->NextChoiceElementPtr);
      #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
      /* #260 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* #270 End the loop */
        break;
      }
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1) */
    }
    /* #280 If maximum possible number of KeyInfo's was encoded */
    #if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)
    if(i == EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE)
    #endif /*#if (EXI_MAXOCCURS_XMLSIG_KEYINFOCHOICE > 1)*/
    {
      /* #290 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #300 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    /* EE(KeyInfo) */
    /* Max Occurs is unbounded */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 8, 4);
    EncWsPtr->EncWs.EERequired = FALSE;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_INFO, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_KEY_INFO) && (EXI_ENCODE_XMLSIG_KEY_INFO == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_KeyName
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_KEY_NAME) && (EXI_ENCODE_XMLSIG_KEY_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_KeyName( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_KeyNameType, AUTOMATIC, EXI_APPL_DATA) KeyNamePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (KeyNamePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (KeyNamePtr->Length > sizeof(KeyNamePtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of KeyName */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode KeyName as string value */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &KeyNamePtr->Buffer[0], KeyNamePtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_NAME, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_KEY_NAME) && (EXI_ENCODE_XMLSIG_KEY_NAME == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_KeyValue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_KEY_VALUE) && (EXI_ENCODE_XMLSIG_KEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_KeyValue( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_KeyValueType, AUTOMATIC, EXI_APPL_DATA) KeyValuePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (KeyValuePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (KeyValuePtr->ChoiceElement == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start of choice element KeyValue */
    /* #30 If not exact one choice element flag is set */
    if (1 != (  KeyValuePtr->ChoiceElement->DSAKeyValueFlag
              + KeyValuePtr->ChoiceElement->RSAKeyValueFlag
              + KeyValuePtr->ChoiceElement->GenericElementFlag) )
    {
      /* #40 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_CHOICE_SELECTION, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_ENCODE_XMLSIG_KEY_VALUE, EXI_E_INV_PARAM);
      return;
    }
    /* #50 If choice element is DSAKeyValue */
    else if(1 == KeyValuePtr->ChoiceElement->DSAKeyValueFlag)
    {
      /* #60 Encode DSAKeyValue element */
      /* SE(DSAKeyValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_XMLSIG_DSAKEY_VALUE) && (EXI_ENCODE_XMLSIG_DSAKEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_DSAKeyValue(EncWsPtr, (KeyValuePtr->ChoiceElement->ChoiceValue.DSAKeyValue));
    #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_VALUE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_DSAKEY_VALUE) && (EXI_ENCODE_XMLSIG_DSAKEY_VALUE == STD_ON)) */
      /* EE(DSAKeyValue) */
      /* Check EE encoding */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    /* #70 If choice element is RSAKeyValue */
    else if(1 == KeyValuePtr->ChoiceElement->RSAKeyValueFlag)
    {
      /* #80 Encode RSAKeyValue element */
      /* SE(RSAKeyValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    #if (defined(EXI_ENCODE_XMLSIG_RSAKEY_VALUE) && (EXI_ENCODE_XMLSIG_RSAKEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_RSAKeyValue(EncWsPtr, (KeyValuePtr->ChoiceElement->ChoiceValue.RSAKeyValue));
    #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_VALUE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_RSAKEY_VALUE) && (EXI_ENCODE_XMLSIG_RSAKEY_VALUE == STD_ON)) */
      /* EE(RSAKeyValue) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #90 If choice element is GenericElement */
    else if(1 == KeyValuePtr->ChoiceElement->GenericElementFlag)
    {
      /* #100 Encode GenericElement element */
      /* SE(GenericElement) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
    #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_GenericElement(EncWsPtr, (KeyValuePtr->ChoiceElement->ChoiceValue.GenericElement));
    #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_VALUE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      /* EE(GenericElement) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    else
    {
      /* Choice Element not supported */
      /* #110 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* End of Choice Element */
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_KEY_VALUE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_KEY_VALUE) && (EXI_ENCODE_XMLSIG_KEY_VALUE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_Manifest
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_MANIFEST) && (EXI_ENCODE_XMLSIG_MANIFEST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_Manifest( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_ManifestType, AUTOMATIC, EXI_APPL_DATA) ManifestPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_XMLSIG_REFERENCE) && (EXI_ENCODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_ReferenceType) nextPtr;
  #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1)
  uint16_least i;
  #endif /*#if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1) */
  #endif /* #if (defined(EXI_ENCODE_XMLSIG_REFERENCE) && (EXI_ENCODE_XMLSIG_REFERENCE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ManifestPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element Id is included */
    if ( (1 == ManifestPtr->IdFlag) && (NULL_PTR != ManifestPtr->Id) )
    {
      /* #30 Encode attribute Id */
      /* AT(Id) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_AttributeId(EncWsPtr, (ManifestPtr->Id));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_MANIFEST, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */

    }
    #if (defined(EXI_ENCODE_XMLSIG_REFERENCE) && (EXI_ENCODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #40 Initialize next pointer with the first Reference element */
    nextPtr = (Exi_XMLSIG_ReferenceType*)ManifestPtr->Reference;
    /* #50 Loop over all Reference elements */
    #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1)
    for(i=0; i<EXI_MAXOCCURS_XMLSIG_REFERENCE; i++)
    #endif /* #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1) */
    {
      /* #60 Encode element Reference */
      /* SE(Reference) */
      #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1)
      if(0 == i)
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1) */
      {
        if(0 == ManifestPtr->IdFlag)
        {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
      }
      #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1)
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1) */
      Exi_Encode_XMLSIG_Reference(EncWsPtr, nextPtr);
      /* EE(Reference) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_XMLSIG_ReferenceType*)(nextPtr->NextReferencePtr);
      #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1)
      /* #70 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* #80 End the loop */
        break;
      }
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1) */
    }
    /* #90 If maximum possible number of Reference's was encoded */
    #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1)
    if(i == EXI_MAXOCCURS_XMLSIG_REFERENCE)
    #endif /*#if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1)*/
    {
      /* #100 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #110 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_MANIFEST, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_XMLSIG_REFERENCE) && (EXI_ENCODE_XMLSIG_REFERENCE == STD_ON)) */
    /* EE(Manifest) */
    /* Max Occurs is unbounded */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    EncWsPtr->EncWs.EERequired = FALSE;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_MANIFEST, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_MANIFEST) && (EXI_ENCODE_XMLSIG_MANIFEST == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_MgmtData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_MGMT_DATA) && (EXI_ENCODE_XMLSIG_MGMT_DATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_MgmtData( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_MgmtDataType, AUTOMATIC, EXI_APPL_DATA) MgmtDataPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (MgmtDataPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (MgmtDataPtr->Length > sizeof(MgmtDataPtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start content of MgmtData */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode MgmtData as string value */
    Exi_VBSEncodeStringValue(&EncWsPtr->EncWs, &MgmtDataPtr->Buffer[0], MgmtDataPtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_MGMT_DATA, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_MGMT_DATA) && (EXI_ENCODE_XMLSIG_MGMT_DATA == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_Object
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_OBJECT) && (EXI_ENCODE_XMLSIG_OBJECT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_Object( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_ObjectType, AUTOMATIC, EXI_APPL_DATA) ObjectPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ObjectPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element Encoding is included */
    if ( (1 == ObjectPtr->EncodingFlag) && (NULL_PTR != ObjectPtr->Encoding) )
    {
      /* #30 Encode attribute Encoding */
      /* AT(Encoding) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ENCODING) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ENCODING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_AttributeEncoding(EncWsPtr, (ObjectPtr->Encoding));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_OBJECT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ENCODING) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ENCODING == STD_ON)) */

    }
    /* #40 If optional element Id is included */
    if ( (1 == ObjectPtr->IdFlag) && (NULL_PTR != ObjectPtr->Id) )
    {
      /* #50 Encode attribute Id */
      /* AT(Id) */
      if(0 == ObjectPtr->EncodingFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      }
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_AttributeId(EncWsPtr, (ObjectPtr->Id));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_OBJECT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */

    }
    /* #60 If optional element MimeType is included */
    if ( (1 == ObjectPtr->MimeTypeFlag) && (NULL_PTR != ObjectPtr->MimeType) )
    {
      /* #70 Encode attribute MimeType */
      /* AT(MimeType) */
      if(0 == ObjectPtr->IdFlag)
      {
        if(0 == ObjectPtr->EncodingFlag)
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_MIME) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_MIME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_AttributeMime(EncWsPtr, (ObjectPtr->MimeType));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_OBJECT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_MIME) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_MIME == STD_ON)) */

    }
    /* #80 Encode element GenericElement */
    /* SE(GenericElement) */
    if(0 == ObjectPtr->MimeTypeFlag)
    {
      if(0 == ObjectPtr->IdFlag)
      {
        if(0 == ObjectPtr->EncodingFlag)
        {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_GenericElement(EncWsPtr, (ObjectPtr->GenericElement));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_OBJECT, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
    /* EE(GenericElement) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_OBJECT, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_OBJECT) && (EXI_ENCODE_XMLSIG_OBJECT == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_PGPDataChoiceSeq0
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ0) && (EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ0 == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_PGPDataChoiceSeq0( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_PGPDataChoiceSeq0Type, AUTOMATIC, EXI_APPL_DATA) PGPDataChoiceSeq0Ptr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_GenericElementType) nextPtr;
  #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
  uint16_least i;
  #endif /*#if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
  #endif /* #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PGPDataChoiceSeq0Ptr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element PGPKeyID */
    /* SE(PGPKeyID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_base64Binary(EncWsPtr, (PGPDataChoiceSeq0Ptr->PGPKeyID));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ0, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) */
    /* EE(PGPKeyID) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If optional element PGPKeyPacket is included */
    if ( (1 == PGPDataChoiceSeq0Ptr->PGPKeyPacketFlag) && (NULL_PTR != PGPDataChoiceSeq0Ptr->PGPKeyPacket) )
    {
      /* #40 Encode element PGPKeyPacket */
      /* SE(PGPKeyPacket) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_base64Binary(EncWsPtr, (PGPDataChoiceSeq0Ptr->PGPKeyPacket));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ0, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) */
      /* EE(PGPKeyPacket) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #50 If optional element GenericElement is included */
    if ( (1 == PGPDataChoiceSeq0Ptr->GenericElementFlag) && (NULL_PTR != PGPDataChoiceSeq0Ptr->GenericElement) )
    {
      #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #60 Initialize next pointer with the first GenericElement element */
      nextPtr = (Exi_XMLSIG_GenericElementType*)PGPDataChoiceSeq0Ptr->GenericElement;
      /* #70 Loop over all GenericElement elements */
      #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
      for(i=0; i<EXI_MAXOCCURS_GENERICELEMENT; i++)
      #endif /* #if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
      {
        /* #80 Encode element GenericElement */
        /* SE(GenericElement) */
        #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
        if(0 == i)
        #endif /* #if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
        {
          if(0 == PGPDataChoiceSeq0Ptr->PGPKeyPacketFlag)
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
          }
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
          }
        }
        #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
        }
        #endif /* #if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
        Exi_Encode_XMLSIG_GenericElement(EncWsPtr, nextPtr);
        /* EE(GenericElement) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        nextPtr = (Exi_XMLSIG_GenericElementType*)(nextPtr->NextGenericElementPtr);
        #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
        /* #90 If this is the last element to encode */
        if(NULL_PTR == nextPtr)
        {
          /* #100 End the loop */
          break;
        }
        #endif /* #if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
      }
      /* #110 If maximum possible number of GenericElement's was encoded */
      #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
      if(i == EXI_MAXOCCURS_GENERICELEMENT)
      #endif /*#if (EXI_MAXOCCURS_GENERICELEMENT > 1)*/
      {
        /* #120 If there are more elements in the list */
        if (nextPtr != NULL_PTR)
        {
          /* #130 Set status code to error */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        }
      }
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ0, EXI_E_INV_PARAM);
      #endif /* #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      /* EE(PGPDataChoiceSeq0) */
      /* Max Occurs is unbounded */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
    /* #140 Optional element GenericElement is not included */
    else
    {
      /* EE(PGPDataChoiceSeq0) */
      /* #150 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      if(0 == PGPDataChoiceSeq0Ptr->PGPKeyPacketFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ0, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ0) && (EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ0 == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_PGPDataChoiceSeq1
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ1) && (EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ1 == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_PGPDataChoiceSeq1( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_PGPDataChoiceSeq1Type, AUTOMATIC, EXI_APPL_DATA) PGPDataChoiceSeq1Ptr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_GenericElementType) nextPtr;
  #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
  uint16_least i;
  #endif /*#if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
  #endif /* #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PGPDataChoiceSeq1Ptr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element PGPKeyPacket */
    /* SE(PGPKeyPacket) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_base64Binary(EncWsPtr, (PGPDataChoiceSeq1Ptr->PGPKeyPacket));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ1, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) */
    /* EE(PGPKeyPacket) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If optional element GenericElement is included */
    if ( (1 == PGPDataChoiceSeq1Ptr->GenericElementFlag) && (NULL_PTR != PGPDataChoiceSeq1Ptr->GenericElement) )
    {
      #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #40 Initialize next pointer with the first GenericElement element */
      nextPtr = (Exi_XMLSIG_GenericElementType*)PGPDataChoiceSeq1Ptr->GenericElement;
      /* #50 Loop over all GenericElement elements */
      #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
      for(i=0; i<EXI_MAXOCCURS_GENERICELEMENT; i++)
      #endif /* #if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
      {
        /* #60 Encode element GenericElement */
        /* SE(GenericElement) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
        Exi_Encode_XMLSIG_GenericElement(EncWsPtr, nextPtr);
        /* EE(GenericElement) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        nextPtr = (Exi_XMLSIG_GenericElementType*)(nextPtr->NextGenericElementPtr);
        #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
        /* #70 If this is the last element to encode */
        if(NULL_PTR == nextPtr)
        {
          /* #80 End the loop */
          break;
        }
        #endif /* #if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
      }
      /* #90 If maximum possible number of GenericElement's was encoded */
      #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
      if(i == EXI_MAXOCCURS_GENERICELEMENT)
      #endif /*#if (EXI_MAXOCCURS_GENERICELEMENT > 1)*/
      {
        /* #100 If there are more elements in the list */
        if (nextPtr != NULL_PTR)
        {
          /* #110 Set status code to error */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        }
      }
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ1, EXI_E_INV_PARAM);
      #endif /* #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      /* EE(PGPDataChoiceSeq1) */
      /* Max Occurs is unbounded */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
    /* #120 Optional element GenericElement is not included */
    else
    {
      /* EE(PGPDataChoiceSeq1) */
      /* #130 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ1, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ1) && (EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ1 == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_PGPData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_PGPDATA) && (EXI_ENCODE_XMLSIG_PGPDATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_PGPData( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_PGPDataType, AUTOMATIC, EXI_APPL_DATA) PGPDataPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (PGPDataPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (PGPDataPtr->ChoiceElement == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Start of choice element PGPData */
    /* #30 If not exact one choice element flag is set */
    if (1 != (  PGPDataPtr->ChoiceElement->ChoiceSequence0Flag
              + PGPDataPtr->ChoiceElement->ChoiceSequence1Flag) )
    {
      /* #40 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_CHOICE_SELECTION, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_ENCODE_XMLSIG_PGPDATA, EXI_E_INV_PARAM);
      return;
    }
    /* #50 If choice element is ChoiceSequence0 */
    else if(1 == PGPDataPtr->ChoiceElement->ChoiceSequence0Flag)
    {
      /* #60 Encode ChoiceSequence0 element */
      /* SE(ChoiceSequence0) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ0) && (EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ0 == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_PGPDataChoiceSeq0(EncWsPtr, (PGPDataPtr->ChoiceElement->ChoiceValue.ChoiceSequence0));
    #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_PGPDATA, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ0) && (EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ0 == STD_ON)) */
      /* EE(ChoiceSequence0) */
      /* Check EE encoding */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    /* #70 If choice element is ChoiceSequence1 */
    else if(1 == PGPDataPtr->ChoiceElement->ChoiceSequence1Flag)
    {
      /* #80 Encode ChoiceSequence1 element */
      /* SE(ChoiceSequence1) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ1) && (EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ1 == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_PGPDataChoiceSeq1(EncWsPtr, (PGPDataPtr->ChoiceElement->ChoiceValue.ChoiceSequence1));
    #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_PGPDATA, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ1) && (EXI_ENCODE_XMLSIG_PGPDATA_CHOICE_SEQ1 == STD_ON)) */
      /* EE(ChoiceSequence1) */
      /* Check EE encoding */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    else
    {
      /* Choice Element not supported */
      /* #90 Set status code to error */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      return;
    }
    /* End of Choice Element */
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_PGPDATA, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_PGPDATA) && (EXI_ENCODE_XMLSIG_PGPDATA == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_RSAKeyValue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_RSAKEY_VALUE) && (EXI_ENCODE_XMLSIG_RSAKEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_RSAKeyValue( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_RSAKeyValueType, AUTOMATIC, EXI_APPL_DATA) RSAKeyValuePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (RSAKeyValuePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element Modulus */
    /* SE(Modulus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_CryptoBinary(EncWsPtr, (RSAKeyValuePtr->Modulus));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_RSAKEY_VALUE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    /* EE(Modulus) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element Exponent */
    /* SE(Exponent) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_CryptoBinary(EncWsPtr, (RSAKeyValuePtr->Exponent));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_RSAKEY_VALUE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_CRYPTO_BINARY) && (EXI_ENCODE_XMLSIG_CRYPTO_BINARY == STD_ON)) */
    /* EE(Exponent) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_RSAKEY_VALUE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_RSAKEY_VALUE) && (EXI_ENCODE_XMLSIG_RSAKEY_VALUE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_Reference
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_REFERENCE) && (EXI_ENCODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_Reference( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_ReferenceType, AUTOMATIC, EXI_APPL_DATA) ReferencePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (ReferencePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element Id is included */
    if ( (1 == ReferencePtr->IdFlag) && (NULL_PTR != ReferencePtr->Id) )
    {
      /* #30 Encode attribute Id */
      /* AT(Id) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_AttributeId(EncWsPtr, (ReferencePtr->Id));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */

    }
    /* #40 If optional element Type is included */
    if ( (1 == ReferencePtr->TypeFlag) && (NULL_PTR != ReferencePtr->Type) )
    {
      /* #50 Encode attribute Type */
      /* AT(Type) */
      if(0 == ReferencePtr->IdFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      }
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE) && (EXI_ENCODE_XMLSIG_ATTRIBUTE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_Attribute(EncWsPtr, (ReferencePtr->Type));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE) && (EXI_ENCODE_XMLSIG_ATTRIBUTE == STD_ON)) */

    }
    /* #60 If optional element URI is included */
    if ( (1 == ReferencePtr->URIFlag) && (NULL_PTR != ReferencePtr->URI) )
    {
      /* #70 Encode attribute URI */
      /* AT(URI) */
      if(0 == ReferencePtr->TypeFlag)
      {
        if(0 == ReferencePtr->IdFlag)
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_URI) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_AttributeURI(EncWsPtr, (ReferencePtr->URI));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_URI) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) */

    }
    /* #80 If optional element Transforms is included */
    if ( (1 == ReferencePtr->TransformsFlag) && (NULL_PTR != ReferencePtr->Transforms) )
    {
      /* #90 Encode element Transforms */
      /* SE(Transforms) */
      if(0 == ReferencePtr->URIFlag)
      {
        if(0 == ReferencePtr->TypeFlag)
        {
          if(0 == ReferencePtr->IdFlag)
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
          }
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
          }
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_XMLSIG_TRANSFORMS) && (EXI_ENCODE_XMLSIG_TRANSFORMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_Transforms(EncWsPtr, (ReferencePtr->Transforms));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_TRANSFORMS) && (EXI_ENCODE_XMLSIG_TRANSFORMS == STD_ON)) */
      /* EE(Transforms) */
      /* Check EE encoding for Transforms */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    /* #100 Encode element DigestMethod */
    /* SE(DigestMethod) */
    if(0 == ReferencePtr->TransformsFlag)
    {
      if(0 == ReferencePtr->URIFlag)
      {
        if(0 == ReferencePtr->TypeFlag)
        {
          if(0 == ReferencePtr->IdFlag)
          {
              Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 4, 3);
          }
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
          }
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_XMLSIG_DIGEST_METHOD) && (EXI_ENCODE_XMLSIG_DIGEST_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_DigestMethod(EncWsPtr, (ReferencePtr->DigestMethod));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_DIGEST_METHOD) && (EXI_ENCODE_XMLSIG_DIGEST_METHOD == STD_ON)) */
    /* EE(DigestMethod) */
    /* Check EE encoding for DigestMethod */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    /* #110 Encode element DigestValue */
    /* SE(DigestValue) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_XMLSIG_DIGEST_VALUE) && (EXI_ENCODE_XMLSIG_DIGEST_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_DigestValue(EncWsPtr, (ReferencePtr->DigestValue));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_REFERENCE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_DIGEST_VALUE) && (EXI_ENCODE_XMLSIG_DIGEST_VALUE == STD_ON)) */
    /* EE(DigestValue) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_REFERENCE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_REFERENCE) && (EXI_ENCODE_XMLSIG_REFERENCE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_RetrievalMethod
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_RETRIEVAL_METHOD) && (EXI_ENCODE_XMLSIG_RETRIEVAL_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_RetrievalMethod( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_RetrievalMethodType, AUTOMATIC, EXI_APPL_DATA) RetrievalMethodPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (RetrievalMethodPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element Type is included */
    if ( (1 == RetrievalMethodPtr->TypeFlag) && (NULL_PTR != RetrievalMethodPtr->Type) )
    {
      /* #30 Encode attribute Type */
      /* AT(Type) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE) && (EXI_ENCODE_XMLSIG_ATTRIBUTE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_Attribute(EncWsPtr, (RetrievalMethodPtr->Type));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_RETRIEVAL_METHOD, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE) && (EXI_ENCODE_XMLSIG_ATTRIBUTE == STD_ON)) */

    }
    /* #40 If optional element URI is included */
    if ( (1 == RetrievalMethodPtr->URIFlag) && (NULL_PTR != RetrievalMethodPtr->URI) )
    {
      /* #50 Encode attribute URI */
      /* AT(URI) */
      if(0 == RetrievalMethodPtr->TypeFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_URI) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_AttributeURI(EncWsPtr, (RetrievalMethodPtr->URI));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_RETRIEVAL_METHOD, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_URI) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_URI == STD_ON)) */

    }
    /* #60 If optional element Transforms is included */
    if ( (1 == RetrievalMethodPtr->TransformsFlag) && (NULL_PTR != RetrievalMethodPtr->Transforms) )
    {
      /* #70 Encode element Transforms */
      /* SE(Transforms) */
      if(0 == RetrievalMethodPtr->URIFlag)
      {
        if(0 == RetrievalMethodPtr->TypeFlag)
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_XMLSIG_TRANSFORMS) && (EXI_ENCODE_XMLSIG_TRANSFORMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_Transforms(EncWsPtr, (RetrievalMethodPtr->Transforms));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_RETRIEVAL_METHOD, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_TRANSFORMS) && (EXI_ENCODE_XMLSIG_TRANSFORMS == STD_ON)) */
      /* EE(Transforms) */
      /* Check EE encoding for Transforms */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    /* #80 Optional element Transforms is not included */
    else
    {
      /* EE(RetrievalMethod) */
      /* #90 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      if(0 == RetrievalMethodPtr->URIFlag)
      {
        if(0 == RetrievalMethodPtr->TypeFlag)
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
        }
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_RETRIEVAL_METHOD, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_RETRIEVAL_METHOD) && (EXI_ENCODE_XMLSIG_RETRIEVAL_METHOD == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_SPKIData
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_SPKIDATA) && (EXI_ENCODE_XMLSIG_SPKIDATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_SPKIData( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_SPKIDataType, AUTOMATIC, EXI_APPL_DATA) SPKIDataPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SPKIDataPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element SPKISexp */
    /* SE(SPKISexp) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_base64Binary(EncWsPtr, (SPKIDataPtr->SPKISexp));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SPKIDATA, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) */
    /* EE(SPKISexp) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 If optional element GenericElement is included */
    if ( (1 == SPKIDataPtr->GenericElementFlag) && (NULL_PTR != SPKIDataPtr->GenericElement) )
    {
      /* #40 Encode element GenericElement */
      /* SE(GenericElement) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_GenericElement(EncWsPtr, (SPKIDataPtr->GenericElement));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SPKIDATA, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      /* EE(GenericElement) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #50 Optional element GenericElement is not included */
    else
    {
      /* EE(SPKIData) */
      /* #60 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SPKIDATA, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_SPKIDATA) && (EXI_ENCODE_XMLSIG_SPKIDATA == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_SignatureMethod
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_METHOD) && (EXI_ENCODE_XMLSIG_SIGNATURE_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_SignatureMethod( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_SignatureMethodType, AUTOMATIC, EXI_APPL_DATA) SignatureMethodPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_GenericElementType) nextPtr;
  #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
  uint16_least i;
  #endif /*#if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
  #endif /* #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SignatureMethodPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode attribute Algorithm */
    /* AT(Algorithm) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_AttributeAlgorithm(EncWsPtr, (SignatureMethodPtr->Algorithm));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE_METHOD, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) */

    /* #30 If optional element HMACOutputLength is included */
    if(1 == SignatureMethodPtr->HMACOutputLengthFlag)
    {
      /* #40 Encode element HMACOutputLength */
      /* SE(HMACOutputLength) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      Exi_VBSEncodeBigInt(&EncWsPtr->EncWs, (SignatureMethodPtr->HMACOutputLength));
      /* EE(HMACOutputLength) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #50 If optional element GenericElement is included */
    if ( (1 == SignatureMethodPtr->GenericElementFlag) && (NULL_PTR != SignatureMethodPtr->GenericElement) )
    {
      #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #60 Initialize next pointer with the first GenericElement element */
      nextPtr = (Exi_XMLSIG_GenericElementType*)SignatureMethodPtr->GenericElement;
      /* #70 Loop over all GenericElement elements */
      #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
      for(i=0; i<EXI_MAXOCCURS_GENERICELEMENT; i++)
      #endif /* #if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
      {
        /* #80 Encode element GenericElement */
        /* SE(GenericElement) */
        #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
        if(0 == i)
        #endif /* #if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
        {
          if(0 == SignatureMethodPtr->HMACOutputLengthFlag)
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
          }
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
          }
        }
        #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
        }
        #endif /* #if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
        Exi_Encode_XMLSIG_GenericElement(EncWsPtr, nextPtr);
        /* EE(GenericElement) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        nextPtr = (Exi_XMLSIG_GenericElementType*)(nextPtr->NextGenericElementPtr);
        #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
        /* #90 If this is the last element to encode */
        if(NULL_PTR == nextPtr)
        {
          /* #100 End the loop */
          break;
        }
        #endif /* #if (EXI_MAXOCCURS_GENERICELEMENT > 1) */
      }
      /* #110 If maximum possible number of GenericElement's was encoded */
      #if (EXI_MAXOCCURS_GENERICELEMENT > 1)
      if(i == EXI_MAXOCCURS_GENERICELEMENT)
      #endif /*#if (EXI_MAXOCCURS_GENERICELEMENT > 1)*/
      {
        /* #120 If there are more elements in the list */
        if (nextPtr != NULL_PTR)
        {
          /* #130 Set status code to error */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        }
      }
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE_METHOD, EXI_E_INV_PARAM);
      #endif /* #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
      /* EE(SignatureMethod) */
      /* Max Occurs is unbounded */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
    /* #140 Optional element GenericElement is not included */
    else
    {
      /* EE(SignatureMethod) */
      /* #150 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      if(0 == SignatureMethodPtr->HMACOutputLengthFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE_METHOD, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_SIGNATURE_METHOD) && (EXI_ENCODE_XMLSIG_SIGNATURE_METHOD == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_SignatureProperties
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTIES) && (EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTIES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_SignatureProperties( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_SignaturePropertiesType, AUTOMATIC, EXI_APPL_DATA) SignaturePropertiesPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_SignaturePropertyType) nextPtr;
  #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY > 1)
  uint16_least i;
  #endif /*#if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY > 1) */
  #endif /* #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SignaturePropertiesPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element Id is included */
    if ( (1 == SignaturePropertiesPtr->IdFlag) && (NULL_PTR != SignaturePropertiesPtr->Id) )
    {
      /* #30 Encode attribute Id */
      /* AT(Id) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_AttributeId(EncWsPtr, (SignaturePropertiesPtr->Id));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE_PROPERTIES, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */

    }
    #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #40 Initialize next pointer with the first SignatureProperty element */
    nextPtr = (Exi_XMLSIG_SignaturePropertyType*)SignaturePropertiesPtr->SignatureProperty;
    /* #50 Loop over all SignatureProperty elements */
    #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY > 1)
    for(i=0; i<EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY; i++)
    #endif /* #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY > 1) */
    {
      /* #60 Encode element SignatureProperty */
      /* SE(SignatureProperty) */
      #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY > 1)
      if(0 == i)
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY > 1) */
      {
        if(0 == SignaturePropertiesPtr->IdFlag)
        {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
        }
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
      }
      #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY > 1)
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY > 1) */
      EncWsPtr->EncWs.EERequired = TRUE;
      Exi_Encode_XMLSIG_SignatureProperty(EncWsPtr, nextPtr);
      /* EE(SignatureProperty) */
      /* Check EE encoding for SignatureProperty */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_XMLSIG_SignaturePropertyType*)(nextPtr->NextSignaturePropertyPtr);
      #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY > 1)
      /* #70 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* #80 End the loop */
        break;
      }
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY > 1) */
    }
    /* #90 If maximum possible number of SignatureProperty's was encoded */
    #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY > 1)
    if(i == EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY)
    #endif /*#if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTY > 1)*/
    {
      /* #100 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #110 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE_PROPERTIES, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) */
    /* EE(SignatureProperties) */
    /* Max Occurs is unbounded */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    EncWsPtr->EncWs.EERequired = FALSE;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE_PROPERTIES, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTIES) && (EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTIES == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_SignatureProperty
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_SignatureProperty( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_SignaturePropertyType, AUTOMATIC, EXI_APPL_DATA) SignaturePropertyPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_SignaturePropertyChoiceType) nextPtr;
  #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE > 1)
  uint16_least i;
  #endif /*#if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE > 1) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SignaturePropertyPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (SignaturePropertyPtr->ChoiceElement == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element Id is included */
    if ( (1 == SignaturePropertyPtr->IdFlag) && (NULL_PTR != SignaturePropertyPtr->Id) )
    {
      /* #30 Encode attribute Id */
      /* AT(Id) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_AttributeId(EncWsPtr, (SignaturePropertyPtr->Id));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE_PROPERTY, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */

    }
    /* #40 Encode attribute Target */
    /* AT(Target) */
    if(0 == SignaturePropertyPtr->IdFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_TARGET) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_TARGET == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_AttributeTarget(EncWsPtr, (SignaturePropertyPtr->Target));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE_PROPERTY, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_TARGET) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_TARGET == STD_ON)) */

    /* #50 Initialize next pointer with the first ChoiceElement element */
    nextPtr = (Exi_XMLSIG_SignaturePropertyChoiceType*)SignaturePropertyPtr->ChoiceElement;
    /* #60 Loop over all ChoiceElement elements */
    #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE > 1)
    for(i=0; i<EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE; i++)
    #endif /* #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE > 1) */
    {
      /* #70 Start of choice element SignatureProperty */
      /* #80 If not exact one choice element flag is set */
      if (1 != (  nextPtr->GenericElementFlag) )
      {
        /* #90 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_CHOICE_SELECTION, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE_PROPERTY, EXI_E_INV_PARAM);
        return;
      }
      /* #100 If choice element is GenericElement */
      else if(1 == nextPtr->GenericElementFlag)
      {
        /* #110 Encode GenericElement element */
        /* SE(GenericElement) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_XMLSIG_GenericElement(EncWsPtr, (nextPtr->ChoiceValue.GenericElement));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE_PROPERTY, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
        /* EE(GenericElement) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        /* Choice Element not supported */
        /* #120 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      /* End of Choice Element */
      nextPtr = (Exi_XMLSIG_SignaturePropertyChoiceType*)(nextPtr->NextChoiceElementPtr);
      #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE > 1)
      /* #130 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* #140 End the loop */
        break;
      }
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE > 1) */
    }
    /* #150 If maximum possible number of SignatureProperty's was encoded */
    #if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE > 1)
    if(i == EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE)
    #endif /*#if (EXI_MAXOCCURS_XMLSIG_SIGNATUREPROPERTYCHOICE > 1)*/
    {
      /* #160 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #170 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    /* EE(SignatureProperty) */
    /* Max Occurs is unbounded */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    EncWsPtr->EncWs.EERequired = FALSE;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE_PROPERTY, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_Signature
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_SIGNATURE) && (EXI_ENCODE_XMLSIG_SIGNATURE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_Signature( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_SignatureType, AUTOMATIC, EXI_APPL_DATA) SignaturePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_XMLSIG_OBJECT) && (EXI_ENCODE_XMLSIG_OBJECT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_ObjectType) nextPtr;
  #if (EXI_MAXOCCURS_XMLSIG_OBJECT > 1)
  uint16_least i;
  #endif /*#if (EXI_MAXOCCURS_XMLSIG_OBJECT > 1) */
  #endif /* #if (defined(EXI_ENCODE_XMLSIG_OBJECT) && (EXI_ENCODE_XMLSIG_OBJECT == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SignaturePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element Id is included */
    if ( (1 == SignaturePtr->IdFlag) && (NULL_PTR != SignaturePtr->Id) )
    {
      /* #30 Encode attribute Id */
      /* AT(Id) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_AttributeId(EncWsPtr, (SignaturePtr->Id));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */

    }
    /* #40 Encode element SignedInfo */
    /* SE(SignedInfo) */
    if(0 == SignaturePtr->IdFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_XMLSIG_SIGNED_INFO) && (EXI_ENCODE_XMLSIG_SIGNED_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_SignedInfo(EncWsPtr, (SignaturePtr->SignedInfo));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_SIGNED_INFO) && (EXI_ENCODE_XMLSIG_SIGNED_INFO == STD_ON)) */
    /* EE(SignedInfo) */
    /* Check EE encoding for SignedInfo */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    /* #50 Encode element SignatureValue */
    /* SE(SignatureValue) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_VALUE) && (EXI_ENCODE_XMLSIG_SIGNATURE_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_SignatureValue(EncWsPtr, (SignaturePtr->SignatureValue));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_SIGNATURE_VALUE) && (EXI_ENCODE_XMLSIG_SIGNATURE_VALUE == STD_ON)) */
    /* EE(SignatureValue) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #60 If optional element KeyInfo is included */
    if ( (1 == SignaturePtr->KeyInfoFlag) && (NULL_PTR != SignaturePtr->KeyInfo) )
    {
      /* #70 Encode element KeyInfo */
      /* SE(KeyInfo) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      EncWsPtr->EncWs.EERequired = TRUE;
      #if (defined(EXI_ENCODE_XMLSIG_KEY_INFO) && (EXI_ENCODE_XMLSIG_KEY_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_KeyInfo(EncWsPtr, (SignaturePtr->KeyInfo));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_KEY_INFO) && (EXI_ENCODE_XMLSIG_KEY_INFO == STD_ON)) */
      /* EE(KeyInfo) */
      /* Check EE encoding for KeyInfo */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    }
    /* #80 If optional element Object is included */
    if ( (1 == SignaturePtr->ObjectFlag) && (NULL_PTR != SignaturePtr->Object) )
    {
      #if (defined(EXI_ENCODE_XMLSIG_OBJECT) && (EXI_ENCODE_XMLSIG_OBJECT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      /* #90 Initialize next pointer with the first Object element */
      nextPtr = (Exi_XMLSIG_ObjectType*)SignaturePtr->Object;
      /* #100 Loop over all Object elements */
      #if (EXI_MAXOCCURS_XMLSIG_OBJECT > 1)
      for(i=0; i<EXI_MAXOCCURS_XMLSIG_OBJECT; i++)
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_OBJECT > 1) */
      {
        /* #110 Encode element Object */
        /* SE(Object) */
        #if (EXI_MAXOCCURS_XMLSIG_OBJECT > 1)
        if(0 == i)
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_OBJECT > 1) */
        {
          if(0 == SignaturePtr->KeyInfoFlag)
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
          }
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
          }
        }
        #if (EXI_MAXOCCURS_XMLSIG_OBJECT > 1)
        else
        {
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
        }
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_OBJECT > 1) */
        Exi_Encode_XMLSIG_Object(EncWsPtr, nextPtr);
        /* EE(Object) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        nextPtr = (Exi_XMLSIG_ObjectType*)(nextPtr->NextObjectPtr);
        #if (EXI_MAXOCCURS_XMLSIG_OBJECT > 1)
        /* #120 If this is the last element to encode */
        if(NULL_PTR == nextPtr)
        {
          /* #130 End the loop */
          break;
        }
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_OBJECT > 1) */
      }
      /* #140 If maximum possible number of Object's was encoded */
      #if (EXI_MAXOCCURS_XMLSIG_OBJECT > 1)
      if(i == EXI_MAXOCCURS_XMLSIG_OBJECT)
      #endif /*#if (EXI_MAXOCCURS_XMLSIG_OBJECT > 1)*/
      {
        /* #150 If there are more elements in the list */
        if (nextPtr != NULL_PTR)
        {
          /* #160 Set status code to error */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        }
      }
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE, EXI_E_INV_PARAM);
      #endif /* #if (defined(EXI_ENCODE_XMLSIG_OBJECT) && (EXI_ENCODE_XMLSIG_OBJECT == STD_ON)) */
      /* EE(Signature) */
      /* Max Occurs is unbounded */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
    /* #170 Optional element Object is not included */
    else
    {
      /* EE(Signature) */
      /* #180 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      if(0 == SignaturePtr->KeyInfoFlag)
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
      }
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_SIGNATURE) && (EXI_ENCODE_XMLSIG_SIGNATURE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_SignatureValue
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_VALUE) && (EXI_ENCODE_XMLSIG_SIGNATURE_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_SignatureValue( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_SignatureValueType, AUTOMATIC, EXI_APPL_DATA) SignatureValuePtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SignatureValuePtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (SignatureValuePtr->Length > sizeof(SignatureValuePtr->Buffer))
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional attribute Id is included */
    if(1 == SignatureValuePtr->IdFlag)
    {
      /* #30 Encode attribute Id */
      /* AT(Id) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_AttributeId(EncWsPtr, (SignatureValuePtr->Id));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE_VALUE, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */

      /* #40 Start content of SignatureValue */
      /* start content */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    /* #50 Optional attribute Id is not included */
    else
    {
      /* #60 Start content of SignatureValue */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    /* #70 Encode SignatureValue as byte array */
    Exi_VBSEncodeBytes(&EncWsPtr->EncWs, &SignatureValuePtr->Buffer[0], SignatureValuePtr->Length);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNATURE_VALUE, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_SIGNATURE_VALUE) && (EXI_ENCODE_XMLSIG_SIGNATURE_VALUE == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_SignedInfo
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_SIGNED_INFO) && (EXI_ENCODE_XMLSIG_SIGNED_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_SignedInfo( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_SignedInfoType, AUTOMATIC, EXI_APPL_DATA) SignedInfoPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_XMLSIG_REFERENCE) && (EXI_ENCODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_ReferenceType) nextPtr;
  #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1)
  uint16_least i;
  #endif /*#if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1) */
  #endif /* #if (defined(EXI_ENCODE_XMLSIG_REFERENCE) && (EXI_ENCODE_XMLSIG_REFERENCE == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (SignedInfoPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 If optional element Id is included */
    if ( (1 == SignedInfoPtr->IdFlag) && (NULL_PTR != SignedInfoPtr->Id) )
    {
      /* #30 Encode attribute Id */
      /* AT(Id) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
      Exi_Encode_XMLSIG_AttributeId(EncWsPtr, (SignedInfoPtr->Id));
      #else
      /* not supported in this configuration */
      Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNED_INFO, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ID) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ID == STD_ON)) */

    }
    /* #40 Encode element CanonicalizationMethod */
    /* SE(CanonicalizationMethod) */
    if(0 == SignedInfoPtr->IdFlag)
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    }
    else
    {
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    }
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_XMLSIG_CANONICALIZATION_METHOD) && (EXI_ENCODE_XMLSIG_CANONICALIZATION_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_CanonicalizationMethod(EncWsPtr, (SignedInfoPtr->CanonicalizationMethod));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNED_INFO, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_CANONICALIZATION_METHOD) && (EXI_ENCODE_XMLSIG_CANONICALIZATION_METHOD == STD_ON)) */
    /* EE(CanonicalizationMethod) */
    /* Check EE encoding for CanonicalizationMethod */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    /* #50 Encode element SignatureMethod */
    /* SE(SignatureMethod) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    EncWsPtr->EncWs.EERequired = TRUE;
    #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_METHOD) && (EXI_ENCODE_XMLSIG_SIGNATURE_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_SignatureMethod(EncWsPtr, (SignedInfoPtr->SignatureMethod));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNED_INFO, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_SIGNATURE_METHOD) && (EXI_ENCODE_XMLSIG_SIGNATURE_METHOD == STD_ON)) */
    /* EE(SignatureMethod) */
    /* Check EE encoding for SignatureMethod */
    Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_XMLSIG_REFERENCE) && (EXI_ENCODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #60 Initialize next pointer with the first Reference element */
    nextPtr = (Exi_XMLSIG_ReferenceType*)SignedInfoPtr->Reference;
    /* #70 Loop over all Reference elements */
    #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1)
    for(i=0; i<EXI_MAXOCCURS_XMLSIG_REFERENCE; i++)
    #endif /* #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1) */
    {
      /* #80 Encode element Reference */
      /* SE(Reference) */
      #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1)
      if(0 == i)
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1) */
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1)
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1) */
      Exi_Encode_XMLSIG_Reference(EncWsPtr, nextPtr);
      /* EE(Reference) */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_XMLSIG_ReferenceType*)(nextPtr->NextReferencePtr);
      #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1)
      /* #90 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* #100 End the loop */
        break;
      }
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1) */
    }
    /* #110 If maximum possible number of Reference's was encoded */
    #if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1)
    if(i == EXI_MAXOCCURS_XMLSIG_REFERENCE)
    #endif /*#if (EXI_MAXOCCURS_XMLSIG_REFERENCE > 1)*/
    {
      /* #120 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #130 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNED_INFO, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_XMLSIG_REFERENCE) && (EXI_ENCODE_XMLSIG_REFERENCE == STD_ON)) */
    /* EE(SignedInfo) */
    /* Max Occurs is unbounded */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    EncWsPtr->EncWs.EERequired = FALSE;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SIGNED_INFO, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_SIGNED_INFO) && (EXI_ENCODE_XMLSIG_SIGNED_INFO == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_Transform
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_TRANSFORM) && (EXI_ENCODE_XMLSIG_TRANSFORM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_Transform( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_TransformType, AUTOMATIC, EXI_APPL_DATA) TransformPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_TransformChoiceType) nextPtr;
  #if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1)
  uint16_least i;
  #endif /*#if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (TransformPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode attribute Algorithm */
    /* AT(Algorithm) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_XMLSIG_AttributeAlgorithm(EncWsPtr, (TransformPtr->Algorithm));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_TRANSFORM, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM) && (EXI_ENCODE_XMLSIG_ATTRIBUTE_ALGORITHM == STD_ON)) */

    /* #30 If optional element ChoiceElement is included */
    if ( (1 == TransformPtr->ChoiceElementFlag) && (NULL_PTR != TransformPtr->ChoiceElement) )
    {
      /* #40 Initialize next pointer with the first ChoiceElement element */
      nextPtr = (Exi_XMLSIG_TransformChoiceType*)TransformPtr->ChoiceElement;
      /* #50 Loop over all ChoiceElement elements */
      #if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1)
      for(i=0; i<EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE; i++)
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1) */
      {
        /* #60 Start of choice element Transform */
        /* #70 If not exact one choice element flag is set */
        if (1 != (  nextPtr->GenericElementFlag
                  + nextPtr->XPathFlag) )
        {
          /* #80 Set status code to error */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_CHOICE_SELECTION, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_ENCODE_XMLSIG_TRANSFORM, EXI_E_INV_PARAM);
          return;
        }
        /* #90 If choice element is GenericElement */
        else if(1 == nextPtr->GenericElementFlag)
        {
          /* #100 Encode GenericElement element */
          /* SE(GenericElement) */
          #if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1)
          if(i == 0)
          #endif /* #if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1) */
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
          }
          #if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1)
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
          }
          #endif /* #if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1) */
        #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          Exi_Encode_XMLSIG_GenericElement(EncWsPtr, (nextPtr->ChoiceValue.GenericElement));
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_TRANSFORM, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
          /* EE(GenericElement) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
        /* #110 If choice element is XPath */
        else if(1 == nextPtr->XPathFlag)
        {
          /* #120 Encode XPath element */
          /* SE(XPath) */
          #if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1)
          if(i == 0)
          #endif /* #if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1) */
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
          }
          #if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1)
          else
          {
            Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
          }
          #endif /* #if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1) */
        #if (defined(EXI_ENCODE_STRING) && (EXI_ENCODE_STRING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
          Exi_Encode_string(EncWsPtr, (nextPtr->ChoiceValue.XPath));
        #else
          /* not supported in this configuration */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_TRANSFORM, EXI_E_INV_PARAM);
        #endif /* (defined(EXI_ENCODE_STRING) && (EXI_ENCODE_STRING == STD_ON)) */
          /* EE(XPath) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
        else
        {
          /* Choice Element not supported */
          /* #130 Set status code to error */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
          return;
        }
        /* End of Choice Element */
        nextPtr = (Exi_XMLSIG_TransformChoiceType*)(nextPtr->NextChoiceElementPtr);
        #if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1)
        /* #140 If this is the last element to encode */
        if(NULL_PTR == nextPtr)
        {
          /* #150 End the loop */
          break;
        }
        #endif /* #if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1) */
      }
      /* #160 If maximum possible number of Transform's was encoded */
      #if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1)
      if(i == EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE)
      #endif /*#if (EXI_MAXOCCURS_XMLSIG_TRANSFORMCHOICE > 1)*/
      {
        /* #170 If there are more elements in the list */
        if (nextPtr != NULL_PTR)
        {
          /* #180 Set status code to error */
          Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        }
      }
      /* EE(Transform) */
      /* Max Occurs is unbounded */
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 2);
      EncWsPtr->EncWs.EERequired = FALSE;
    }
    /* #190 Optional element ChoiceElement is not included */
    else
    {
      /* EE(Transform) */
      /* #200 Encode end element tag */
      EncWsPtr->EncWs.EERequired = FALSE;
      Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_TRANSFORM, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_TRANSFORM) && (EXI_ENCODE_XMLSIG_TRANSFORM == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_Transforms
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_TRANSFORMS) && (EXI_ENCODE_XMLSIG_TRANSFORMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_Transforms( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_TransformsType, AUTOMATIC, EXI_APPL_DATA) TransformsPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  #if (defined(EXI_ENCODE_XMLSIG_TRANSFORM) && (EXI_ENCODE_XMLSIG_TRANSFORM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_TransformType) nextPtr;
  #if (EXI_MAXOCCURS_XMLSIG_TRANSFORM > 1)
  uint16_least i;
  #endif /*#if (EXI_MAXOCCURS_XMLSIG_TRANSFORM > 1) */
  #endif /* #if (defined(EXI_ENCODE_XMLSIG_TRANSFORM) && (EXI_ENCODE_XMLSIG_TRANSFORM == STD_ON)) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (TransformsPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    #if (defined(EXI_ENCODE_XMLSIG_TRANSFORM) && (EXI_ENCODE_XMLSIG_TRANSFORM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    /* #20 Initialize next pointer with the first Transform element */
    nextPtr = (Exi_XMLSIG_TransformType*)TransformsPtr->Transform;
    /* #30 Loop over all Transform elements */
    #if (EXI_MAXOCCURS_XMLSIG_TRANSFORM > 1)
    for(i=0; i<EXI_MAXOCCURS_XMLSIG_TRANSFORM; i++)
    #endif /* #if (EXI_MAXOCCURS_XMLSIG_TRANSFORM > 1) */
    {
      /* #40 Encode element Transform */
      /* SE(Transform) */
      #if (EXI_MAXOCCURS_XMLSIG_TRANSFORM > 1)
      if(0 == i)
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_TRANSFORM > 1) */
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      #if (EXI_MAXOCCURS_XMLSIG_TRANSFORM > 1)
      else
      {
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 2);
      }
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_TRANSFORM > 1) */
      EncWsPtr->EncWs.EERequired = TRUE;
      Exi_Encode_XMLSIG_Transform(EncWsPtr, nextPtr);
      /* EE(Transform) */
      /* Check EE encoding for Transform */
      Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      nextPtr = (Exi_XMLSIG_TransformType*)(nextPtr->NextTransformPtr);
      #if (EXI_MAXOCCURS_XMLSIG_TRANSFORM > 1)
      /* #50 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* #60 End the loop */
        break;
      }
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_TRANSFORM > 1) */
    }
    /* #70 If maximum possible number of Transform's was encoded */
    #if (EXI_MAXOCCURS_XMLSIG_TRANSFORM > 1)
    if(i == EXI_MAXOCCURS_XMLSIG_TRANSFORM)
    #endif /*#if (EXI_MAXOCCURS_XMLSIG_TRANSFORM > 1)*/
    {
      /* #80 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #90 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_TRANSFORMS, EXI_E_INV_PARAM);
    #endif /* #if (defined(EXI_ENCODE_XMLSIG_TRANSFORM) && (EXI_ENCODE_XMLSIG_TRANSFORM == STD_ON)) */
    /* EE(Transforms) */
    /* Max Occurs is unbounded */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 2);
    EncWsPtr->EncWs.EERequired = FALSE;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_TRANSFORMS, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_TRANSFORMS) && (EXI_ENCODE_XMLSIG_TRANSFORMS == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_X509Data
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_X509DATA) && (EXI_ENCODE_XMLSIG_X509DATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_X509Data( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_X509DataType, AUTOMATIC, EXI_APPL_DATA) X509DataPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */
  EXI_P2VAR_IN_FUNCTION(Exi_XMLSIG_X509DataChoiceType) nextPtr;
  #if (EXI_MAXOCCURS_XMLSIG_X509DATACHOICE > 1)
  uint16_least i;
  #endif /*#if (EXI_MAXOCCURS_XMLSIG_X509DATACHOICE > 1) */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (X509DataPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  if (X509DataPtr->ChoiceElement == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Initialize next pointer with the first ChoiceElement element */
    nextPtr = (Exi_XMLSIG_X509DataChoiceType*)X509DataPtr->ChoiceElement;
    /* #30 Loop over all ChoiceElement elements */
    #if (EXI_MAXOCCURS_XMLSIG_X509DATACHOICE > 1)
    for(i=0; i<EXI_MAXOCCURS_XMLSIG_X509DATACHOICE; i++)
    #endif /* #if (EXI_MAXOCCURS_XMLSIG_X509DATACHOICE > 1) */
    {
      /* #40 Start of choice element X509Data */
      /* #50 If not exact one choice element flag is set */
      if (1 != (  nextPtr->X509IssuerSerialFlag
                + nextPtr->X509SKIFlag
                + nextPtr->X509SubjectNameFlag
                + nextPtr->X509CertificateFlag
                + nextPtr->X509CRLFlag
                + nextPtr->GenericElementFlag) )
      {
        /* #60 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_CHOICE_SELECTION, EXI_BS_SSC_CALL_DET_ON_CHANGE, EXI_API_ID_ENCODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
        return;
      }
      /* #70 If choice element is X509IssuerSerial */
      else if(1 == nextPtr->X509IssuerSerialFlag)
      {
        /* #80 Encode X509IssuerSerial element */
        /* SE(X509IssuerSerial) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 3);
      #if (defined(EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_XMLSIG_X509IssuerSerial(EncWsPtr, (nextPtr->ChoiceValue.X509IssuerSerial));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) */
        /* EE(X509IssuerSerial) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      /* #90 If choice element is X509SKI */
      else if(1 == nextPtr->X509SKIFlag)
      {
        /* #100 Encode X509SKI element */
        /* SE(X509SKI) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 3);
      #if (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_base64Binary(EncWsPtr, (nextPtr->ChoiceValue.X509SKI));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) */
        /* EE(X509SKI) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      /* #110 If choice element is X509SubjectName */
      else if(1 == nextPtr->X509SubjectNameFlag)
      {
        /* #120 Encode X509SubjectName element */
        /* SE(X509SubjectName) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 3);
      #if (defined(EXI_ENCODE_STRING) && (EXI_ENCODE_STRING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_string(EncWsPtr, (nextPtr->ChoiceValue.X509SubjectName));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_STRING) && (EXI_ENCODE_STRING == STD_ON)) */
        /* EE(X509SubjectName) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      /* #130 If choice element is X509Certificate */
      else if(1 == nextPtr->X509CertificateFlag)
      {
        /* #140 Encode X509Certificate element */
        /* SE(X509Certificate) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 3);
      #if (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_base64Binary(EncWsPtr, (nextPtr->ChoiceValue.X509Certificate));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) */
        /* EE(X509Certificate) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      /* #150 If choice element is X509CRL */
      else if(1 == nextPtr->X509CRLFlag)
      {
        /* #160 Encode X509CRL element */
        /* SE(X509CRL) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 4, 3);
      #if (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_base64Binary(EncWsPtr, (nextPtr->ChoiceValue.X509CRL));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_BASE64BINARY) && (EXI_ENCODE_BASE64BINARY == STD_ON)) */
        /* EE(X509CRL) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      /* #170 If choice element is GenericElement */
      else if(1 == nextPtr->GenericElementFlag)
      {
        /* #180 Encode GenericElement element */
        /* SE(GenericElement) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 5, 3);
      #if (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        Exi_Encode_XMLSIG_GenericElement(EncWsPtr, (nextPtr->ChoiceValue.GenericElement));
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_X509DATA, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_GENERIC_ELEMENT) && (EXI_ENCODE_XMLSIG_GENERIC_ELEMENT == STD_ON)) */
        /* EE(GenericElement) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      }
      else
      {
        /* Choice Element not supported */
        /* #190 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_INV_EVENT_CODE, EXI_BS_SSC_NO_DET_CALL, 0, 0);
        return;
      }
      /* End of Choice Element */
      nextPtr = (Exi_XMLSIG_X509DataChoiceType*)(nextPtr->NextChoiceElementPtr);
      #if (EXI_MAXOCCURS_XMLSIG_X509DATACHOICE > 1)
      /* #200 If this is the last element to encode */
      if(NULL_PTR == nextPtr)
      {
        /* #210 End the loop */
        break;
      }
      #endif /* #if (EXI_MAXOCCURS_XMLSIG_X509DATACHOICE > 1) */
    }
    /* #220 If maximum possible number of X509Data's was encoded */
    #if (EXI_MAXOCCURS_XMLSIG_X509DATACHOICE > 1)
    if(i == EXI_MAXOCCURS_XMLSIG_X509DATACHOICE)
    #endif /*#if (EXI_MAXOCCURS_XMLSIG_X509DATACHOICE > 1)*/
    {
      /* #230 If there are more elements in the list */
      if (nextPtr != NULL_PTR)
      {
        /* #240 Set status code to error */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_LIST_TO_LONG, EXI_BS_SSC_NO_DET_CALL, 0, 0);
      }
    }
    /* EE(X509Data) */
    /* Max Occurs is unbounded */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 6, 3);
    EncWsPtr->EncWs.EERequired = FALSE;
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_X509DATA, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_X509DATA) && (EXI_ENCODE_XMLSIG_X509DATA == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_X509IssuerSerial
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_X509IssuerSerial( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr, \
                                       P2CONST(Exi_XMLSIG_X509IssuerSerialType, AUTOMATIC, EXI_APPL_DATA) X509IssuerSerialPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (X509IssuerSerialPtr == NULL_PTR)
  {
    errorId = EXI_E_INV_POINTER;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode element X509IssuerName */
    /* SE(X509IssuerName) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    #if (defined(EXI_ENCODE_STRING) && (EXI_ENCODE_STRING == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
    Exi_Encode_string(EncWsPtr, (X509IssuerSerialPtr->X509IssuerName));
    #else
    /* not supported in this configuration */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_X509ISSUER_SERIAL, EXI_E_INV_PARAM);
    #endif /* (defined(EXI_ENCODE_STRING) && (EXI_ENCODE_STRING == STD_ON)) */
    /* EE(X509IssuerName) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* #30 Encode element X509SerialNumber */
    /* SE(X509SerialNumber) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    /* start content */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
    Exi_VBSEncodeBigInt(&EncWsPtr->EncWs, (X509IssuerSerialPtr->X509SerialNumber));
    /* EE(X509SerialNumber) */
    Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_X509ISSUER_SERIAL, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL) && (EXI_ENCODE_XMLSIG_X509ISSUER_SERIAL == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_SchemaFragment
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_SCHEMA_FRAGMENT) && (EXI_ENCODE_XMLSIG_SCHEMA_FRAGMENT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_SchemaFragment( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EncWsPtr->InputData.RootElementId > EXI_SCHEMA_UNKNOWN_ELEMENT_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Encode all fragments of schema XMLSIG */
    /* #30 Switch root element ID */
    switch(EncWsPtr->InputData.RootElementId)
    {
    /* Fragment CanonicalizationMethod http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment DSAKeyValue http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment DigestMethod http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment DigestValue http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment Exponent http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment G http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment HMACOutputLength http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment J http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    case EXI_XMLSIG_KEY_INFO_TYPE:
      /* #40 Fragment KeyInfo http://www.w3.org/2000/09/xmldsig# */
      {
      #if (defined(EXI_ENCODE_XMLSIG_KEY_INFO) && (EXI_ENCODE_XMLSIG_KEY_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_KeyInfoType, AUTOMATIC, EXI_APPL_DATA) KeyInfoPtr = (P2CONST(Exi_XMLSIG_KeyInfoType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #50 Encode fragment KeyInfo */
        /* SE(KeyInfo) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 8, 6);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_KeyInfo(EncWsPtr, KeyInfoPtr);
        if(TRUE == EncWsPtr->EncWs.EERequired)
        {
          /* EE(KeyInfo) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 46, 6);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_KEY_INFO) && (EXI_ENCODE_XMLSIG_KEY_INFO == STD_ON)) */
        break;
      }
    /* Fragment KeyName http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment KeyValue http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    case EXI_XMLSIG_MANIFEST_TYPE:
      /* #60 Fragment Manifest http://www.w3.org/2000/09/xmldsig# */
      {
      #if (defined(EXI_ENCODE_XMLSIG_MANIFEST) && (EXI_ENCODE_XMLSIG_MANIFEST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_ManifestType, AUTOMATIC, EXI_APPL_DATA) ManifestPtr = (P2CONST(Exi_XMLSIG_ManifestType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #70 Encode fragment Manifest */
        /* SE(Manifest) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 11, 6);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_Manifest(EncWsPtr, ManifestPtr);
        if(TRUE == EncWsPtr->EncWs.EERequired)
        {
          /* EE(Manifest) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 46, 6);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_MANIFEST) && (EXI_ENCODE_XMLSIG_MANIFEST == STD_ON)) */
        break;
      }
    /* Fragment MgmtData http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment Modulus http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    case EXI_XMLSIG_OBJECT_TYPE:
      /* #80 Fragment Object http://www.w3.org/2000/09/xmldsig# */
      {
      #if (defined(EXI_ENCODE_XMLSIG_OBJECT) && (EXI_ENCODE_XMLSIG_OBJECT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_ObjectType, AUTOMATIC, EXI_APPL_DATA) ObjectPtr = (P2CONST(Exi_XMLSIG_ObjectType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #90 Encode fragment Object */
        /* SE(Object) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 14, 6);
        Exi_Encode_XMLSIG_Object(EncWsPtr, ObjectPtr);
        /* EE(Object) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 46, 6);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_OBJECT) && (EXI_ENCODE_XMLSIG_OBJECT == STD_ON)) */
        break;
      }
    /* Fragment P http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment PGPData http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment PGPKeyID http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment PGPKeyPacket http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment PgenCounter http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment Q http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment RSAKeyValue http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    case EXI_XMLSIG_REFERENCE_TYPE:
      /* #100 Fragment Reference http://www.w3.org/2000/09/xmldsig# */
      {
      #if (defined(EXI_ENCODE_XMLSIG_REFERENCE) && (EXI_ENCODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_ReferenceType, AUTOMATIC, EXI_APPL_DATA) ReferencePtr = (P2CONST(Exi_XMLSIG_ReferenceType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #110 Encode fragment Reference */
        /* SE(Reference) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 22, 6);
        Exi_Encode_XMLSIG_Reference(EncWsPtr, ReferencePtr);
        /* EE(Reference) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 46, 6);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_REFERENCE) && (EXI_ENCODE_XMLSIG_REFERENCE == STD_ON)) */
        break;
      }
    /* Fragment RetrievalMethod http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment SPKIData http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment SPKISexp http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment Seed http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    case EXI_XMLSIG_SIGNATURE_TYPE:
      /* #120 Fragment Signature http://www.w3.org/2000/09/xmldsig# */
      {
      #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE) && (EXI_ENCODE_XMLSIG_SIGNATURE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_SignatureType, AUTOMATIC, EXI_APPL_DATA) SignaturePtr = (P2CONST(Exi_XMLSIG_SignatureType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #130 Encode fragment Signature */
        /* SE(Signature) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 27, 6);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_Signature(EncWsPtr, SignaturePtr);
        if(TRUE == EncWsPtr->EncWs.EERequired)
        {
          /* EE(Signature) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 46, 6);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_SIGNATURE) && (EXI_ENCODE_XMLSIG_SIGNATURE == STD_ON)) */
        break;
      }
    /* Fragment SignatureMethod http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    case EXI_XMLSIG_SIGNATURE_PROPERTIES_TYPE:
      /* #140 Fragment SignatureProperties http://www.w3.org/2000/09/xmldsig# */
      {
      #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTIES) && (EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTIES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_SignaturePropertiesType, AUTOMATIC, EXI_APPL_DATA) SignaturePropertiesPtr = (P2CONST(Exi_XMLSIG_SignaturePropertiesType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #150 Encode fragment SignatureProperties */
        /* SE(SignatureProperties) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 29, 6);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_SignatureProperties(EncWsPtr, SignaturePropertiesPtr);
        if(TRUE == EncWsPtr->EncWs.EERequired)
        {
          /* EE(SignatureProperties) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 46, 6);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTIES) && (EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTIES == STD_ON)) */
        break;
      }
    case EXI_XMLSIG_SIGNATURE_PROPERTY_TYPE:
      /* #160 Fragment SignatureProperty http://www.w3.org/2000/09/xmldsig# */
      {
      #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_SignaturePropertyType, AUTOMATIC, EXI_APPL_DATA) SignaturePropertyPtr = (P2CONST(Exi_XMLSIG_SignaturePropertyType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #170 Encode fragment SignatureProperty */
        /* SE(SignatureProperty) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 30, 6);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_SignatureProperty(EncWsPtr, SignaturePropertyPtr);
        if(TRUE == EncWsPtr->EncWs.EERequired)
        {
          /* EE(SignatureProperty) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 46, 6);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) */
        break;
      }
    case EXI_XMLSIG_SIGNATURE_VALUE_TYPE:
      /* #180 Fragment SignatureValue http://www.w3.org/2000/09/xmldsig# */
      {
      #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_VALUE) && (EXI_ENCODE_XMLSIG_SIGNATURE_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_SignatureValueType, AUTOMATIC, EXI_APPL_DATA) SignatureValuePtr = (P2CONST(Exi_XMLSIG_SignatureValueType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #190 Encode fragment SignatureValue */
        /* SE(SignatureValue) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 31, 6);
        Exi_Encode_XMLSIG_SignatureValue(EncWsPtr, SignatureValuePtr);
        /* EE(SignatureValue) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 46, 6);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_SIGNATURE_VALUE) && (EXI_ENCODE_XMLSIG_SIGNATURE_VALUE == STD_ON)) */
        break;
      }
    case EXI_XMLSIG_SIGNED_INFO_TYPE:
      /* #200 Fragment SignedInfo http://www.w3.org/2000/09/xmldsig# */
      {
      #if (defined(EXI_ENCODE_XMLSIG_SIGNED_INFO) && (EXI_ENCODE_XMLSIG_SIGNED_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_SignedInfoType, AUTOMATIC, EXI_APPL_DATA) SignedInfoPtr = (P2CONST(Exi_XMLSIG_SignedInfoType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #210 Encode fragment SignedInfo */
        /* SE(SignedInfo) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 32, 6);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_SignedInfo(EncWsPtr, SignedInfoPtr);
        if(TRUE == EncWsPtr->EncWs.EERequired)
        {
          /* EE(SignedInfo) */
          Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
        }
        /* ED */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 46, 6);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_FRAGMENT, EXI_E_INV_PARAM);
      #endif /* (defined(EXI_ENCODE_XMLSIG_SIGNED_INFO) && (EXI_ENCODE_XMLSIG_SIGNED_INFO == STD_ON)) */
        break;
      }
    /* Fragment Transform http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment Transforms http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment X509CRL http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment X509Certificate http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment X509Data http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment X509IssuerName http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment X509IssuerSerial http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment X509SKI http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment X509SerialNumber http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment X509SubjectName http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment XPath http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    /* Fragment Y http://www.w3.org/2000/09/xmldsig# not required, it does not have an 'Id' attribute */
    default:
      /* #220 Unknown fragment */
      {
        /* #230 Set status code to error */
        EncWsPtr->EncWs.StatusCode = EXI_E_ELEMENT_NOT_AVAILABLE;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SCHEMA_FRAGMENT, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_SCHEMA_FRAGMENT) && (EXI_ENCODE_XMLSIG_SCHEMA_FRAGMENT == STD_ON)) */


/**********************************************************************************************************************
 *  Exi_Encode_XMLSIG_SchemaRoot
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
#if (defined(EXI_ENCODE_XMLSIG_SCHEMA_ROOT) && (EXI_ENCODE_XMLSIG_SCHEMA_ROOT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
FUNC(void, EXI_CODE) Exi_Encode_XMLSIG_SchemaRoot( \
                                       P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, EXI_APPL_VAR) EncWsPtr )
{
  /* ------------------------------------------- Local Variables --------------------------------------------------- */
  Exi_DetErrorType errorId = EXI_E_NO_ERROR; /* PRQA S 0781 */ /*  MD_Exi_5.6_0781 */

  /* #10 Check plausibility of input parameters */
  /* ------------------------------------------ Development Error Checks ------------------------------------------- */
  if (EncWsPtr->EncWs.StatusCode != EXI_E_OK)
  {
    /* Nothing to do, error was already set and if required a DET was already reported */
  }
  else
  if (EncWsPtr->InputData.RootElementId > EXI_XMLSIG_X509DATA_TYPE)
  {
    errorId = EXI_E_INV_PARAM;
  }
  else
  {
    /* --------------------------------------------- Implementation -------------------------------------------------- */
    /* #20 Swtich RootElementId */
    switch(EncWsPtr->InputData.RootElementId)
    {
    case EXI_XMLSIG_CANONICALIZATION_METHOD_TYPE: /* 0 */
      /* #30 Element CanonicalizationMethod */
      {
      #if (defined(EXI_ENCODE_XMLSIG_CANONICALIZATION_METHOD) && (EXI_ENCODE_XMLSIG_CANONICALIZATION_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_CanonicalizationMethodType, AUTOMATIC, EXI_APPL_DATA) CanonicalizationMethodPtr = (P2CONST(Exi_XMLSIG_CanonicalizationMethodType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #40 If supported: Encode element CanonicalizationMethod */
        /* SE(CanonicalizationMethod) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 5);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_CanonicalizationMethod(EncWsPtr, CanonicalizationMethodPtr);
        /* EE(CanonicalizationMethod) */
        /* Check EE encoding for CanonicalizationMethod */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_DSAKEY_VALUE_TYPE: /* 1 */
      /* #50 Element DSAKeyValue */
      {
      #if (defined(EXI_ENCODE_XMLSIG_DSAKEY_VALUE) && (EXI_ENCODE_XMLSIG_DSAKEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_DSAKeyValueType, AUTOMATIC, EXI_APPL_DATA) DSAKeyValuePtr = (P2CONST(Exi_XMLSIG_DSAKeyValueType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #60 If supported: Encode element DSAKeyValue */
        /* SE(DSAKeyValue) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 1, 5);
        Exi_Encode_XMLSIG_DSAKeyValue(EncWsPtr, DSAKeyValuePtr);
        /* EE(DSAKeyValue) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_DIGEST_METHOD_TYPE: /* 2 */
      /* #70 Element DigestMethod */
      {
      #if (defined(EXI_ENCODE_XMLSIG_DIGEST_METHOD) && (EXI_ENCODE_XMLSIG_DIGEST_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_DigestMethodType, AUTOMATIC, EXI_APPL_DATA) DigestMethodPtr = (P2CONST(Exi_XMLSIG_DigestMethodType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #80 If supported: Encode element DigestMethod */
        /* SE(DigestMethod) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 2, 5);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_DigestMethod(EncWsPtr, DigestMethodPtr);
        /* EE(DigestMethod) */
        /* Check EE encoding for DigestMethod */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_DIGEST_VALUE_TYPE: /* 3 */
      /* #90 Element DigestValue */
      {
      #if (defined(EXI_ENCODE_XMLSIG_DIGEST_VALUE) && (EXI_ENCODE_XMLSIG_DIGEST_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_DigestValueType, AUTOMATIC, EXI_APPL_DATA) DigestValuePtr = (P2CONST(Exi_XMLSIG_DigestValueType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #100 If supported: Encode element DigestValue */
        /* SE(DigestValue) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 3, 5);
        Exi_Encode_XMLSIG_DigestValue(EncWsPtr, DigestValuePtr);
        /* EE(DigestValue) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_KEY_INFO_TYPE: /* 4 */
      /* #110 Element KeyInfo */
      {
      #if (defined(EXI_ENCODE_XMLSIG_KEY_INFO) && (EXI_ENCODE_XMLSIG_KEY_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_KeyInfoType, AUTOMATIC, EXI_APPL_DATA) KeyInfoPtr = (P2CONST(Exi_XMLSIG_KeyInfoType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #120 If supported: Encode element KeyInfo */
        /* SE(KeyInfo) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 4, 5);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_KeyInfo(EncWsPtr, KeyInfoPtr);
        /* EE(KeyInfo) */
        /* Check EE encoding for KeyInfo */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_KEY_NAME_TYPE: /* 5 */
      /* #130 Element KeyName */
      {
      #if (defined(EXI_ENCODE_XMLSIG_KEY_NAME) && (EXI_ENCODE_XMLSIG_KEY_NAME == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_KeyNameType, AUTOMATIC, EXI_APPL_DATA) KeyNamePtr = (P2CONST(Exi_XMLSIG_KeyNameType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #140 If supported: Encode element KeyName */
        /* SE(KeyName) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 5, 5);
        Exi_Encode_XMLSIG_KeyName(EncWsPtr, KeyNamePtr);
        /* EE(KeyName) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_KEY_VALUE_TYPE: /* 6 */
      /* #150 Element KeyValue */
      {
      #if (defined(EXI_ENCODE_XMLSIG_KEY_VALUE) && (EXI_ENCODE_XMLSIG_KEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_KeyValueType, AUTOMATIC, EXI_APPL_DATA) KeyValuePtr = (P2CONST(Exi_XMLSIG_KeyValueType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #160 If supported: Encode element KeyValue */
        /* SE(KeyValue) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 6, 5);
        Exi_Encode_XMLSIG_KeyValue(EncWsPtr, KeyValuePtr);
        /* EE(KeyValue) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_MANIFEST_TYPE: /* 7 */
      /* #170 Element Manifest */
      {
      #if (defined(EXI_ENCODE_XMLSIG_MANIFEST) && (EXI_ENCODE_XMLSIG_MANIFEST == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_ManifestType, AUTOMATIC, EXI_APPL_DATA) ManifestPtr = (P2CONST(Exi_XMLSIG_ManifestType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #180 If supported: Encode element Manifest */
        /* SE(Manifest) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 7, 5);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_Manifest(EncWsPtr, ManifestPtr);
        /* EE(Manifest) */
        /* Check EE encoding for Manifest */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_MGMT_DATA_TYPE: /* 8 */
      /* #190 Element MgmtData */
      {
      #if (defined(EXI_ENCODE_XMLSIG_MGMT_DATA) && (EXI_ENCODE_XMLSIG_MGMT_DATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_MgmtDataType, AUTOMATIC, EXI_APPL_DATA) MgmtDataPtr = (P2CONST(Exi_XMLSIG_MgmtDataType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #200 If supported: Encode element MgmtData */
        /* SE(MgmtData) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 8, 5);
        Exi_Encode_XMLSIG_MgmtData(EncWsPtr, MgmtDataPtr);
        /* EE(MgmtData) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_OBJECT_TYPE: /* 9 */
      /* #210 Element Object */
      {
      #if (defined(EXI_ENCODE_XMLSIG_OBJECT) && (EXI_ENCODE_XMLSIG_OBJECT == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_ObjectType, AUTOMATIC, EXI_APPL_DATA) ObjectPtr = (P2CONST(Exi_XMLSIG_ObjectType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #220 If supported: Encode element Object */
        /* SE(Object) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 9, 5);
        Exi_Encode_XMLSIG_Object(EncWsPtr, ObjectPtr);
        /* EE(Object) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_PGPDATA_TYPE: /* 10 */
      /* #230 Element PGPData */
      {
      #if (defined(EXI_ENCODE_XMLSIG_PGPDATA) && (EXI_ENCODE_XMLSIG_PGPDATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_PGPDataType, AUTOMATIC, EXI_APPL_DATA) PGPDataPtr = (P2CONST(Exi_XMLSIG_PGPDataType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #240 If supported: Encode element PGPData */
        /* SE(PGPData) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 10, 5);
        Exi_Encode_XMLSIG_PGPData(EncWsPtr, PGPDataPtr);
        /* EE(PGPData) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_RSAKEY_VALUE_TYPE: /* 11 */
      /* #250 Element RSAKeyValue */
      {
      #if (defined(EXI_ENCODE_XMLSIG_RSAKEY_VALUE) && (EXI_ENCODE_XMLSIG_RSAKEY_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_RSAKeyValueType, AUTOMATIC, EXI_APPL_DATA) RSAKeyValuePtr = (P2CONST(Exi_XMLSIG_RSAKeyValueType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #260 If supported: Encode element RSAKeyValue */
        /* SE(RSAKeyValue) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 11, 5);
        Exi_Encode_XMLSIG_RSAKeyValue(EncWsPtr, RSAKeyValuePtr);
        /* EE(RSAKeyValue) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_REFERENCE_TYPE: /* 12 */
      /* #270 Element Reference */
      {
      #if (defined(EXI_ENCODE_XMLSIG_REFERENCE) && (EXI_ENCODE_XMLSIG_REFERENCE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_ReferenceType, AUTOMATIC, EXI_APPL_DATA) ReferencePtr = (P2CONST(Exi_XMLSIG_ReferenceType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #280 If supported: Encode element Reference */
        /* SE(Reference) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 12, 5);
        Exi_Encode_XMLSIG_Reference(EncWsPtr, ReferencePtr);
        /* EE(Reference) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_RETRIEVAL_METHOD_TYPE: /* 13 */
      /* #290 Element RetrievalMethod */
      {
      #if (defined(EXI_ENCODE_XMLSIG_RETRIEVAL_METHOD) && (EXI_ENCODE_XMLSIG_RETRIEVAL_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_RetrievalMethodType, AUTOMATIC, EXI_APPL_DATA) RetrievalMethodPtr = (P2CONST(Exi_XMLSIG_RetrievalMethodType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #300 If supported: Encode element RetrievalMethod */
        /* SE(RetrievalMethod) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 13, 5);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_RetrievalMethod(EncWsPtr, RetrievalMethodPtr);
        /* EE(RetrievalMethod) */
        /* Check EE encoding for RetrievalMethod */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_SPKIDATA_TYPE: /* 14 */
      /* #310 Element SPKIData */
      {
      #if (defined(EXI_ENCODE_XMLSIG_SPKIDATA) && (EXI_ENCODE_XMLSIG_SPKIDATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_SPKIDataType, AUTOMATIC, EXI_APPL_DATA) SPKIDataPtr = (P2CONST(Exi_XMLSIG_SPKIDataType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #320 If supported: Encode element SPKIData */
        /* SE(SPKIData) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 14, 5);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_SPKIData(EncWsPtr, SPKIDataPtr);
        /* EE(SPKIData) */
        /* Check EE encoding for SPKIData */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_SIGNATURE_METHOD_TYPE: /* 16 */
      /* #330 Element SignatureMethod */
      {
      #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_METHOD) && (EXI_ENCODE_XMLSIG_SIGNATURE_METHOD == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_SignatureMethodType, AUTOMATIC, EXI_APPL_DATA) SignatureMethodPtr = (P2CONST(Exi_XMLSIG_SignatureMethodType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #340 If supported: Encode element SignatureMethod */
        /* SE(SignatureMethod) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 16, 5);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_SignatureMethod(EncWsPtr, SignatureMethodPtr);
        /* EE(SignatureMethod) */
        /* Check EE encoding for SignatureMethod */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_SIGNATURE_PROPERTIES_TYPE: /* 17 */
      /* #350 Element SignatureProperties */
      {
      #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTIES) && (EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTIES == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_SignaturePropertiesType, AUTOMATIC, EXI_APPL_DATA) SignaturePropertiesPtr = (P2CONST(Exi_XMLSIG_SignaturePropertiesType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #360 If supported: Encode element SignatureProperties */
        /* SE(SignatureProperties) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 17, 5);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_SignatureProperties(EncWsPtr, SignaturePropertiesPtr);
        /* EE(SignatureProperties) */
        /* Check EE encoding for SignatureProperties */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_SIGNATURE_PROPERTY_TYPE: /* 18 */
      /* #370 Element SignatureProperty */
      {
      #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY) && (EXI_ENCODE_XMLSIG_SIGNATURE_PROPERTY == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_SignaturePropertyType, AUTOMATIC, EXI_APPL_DATA) SignaturePropertyPtr = (P2CONST(Exi_XMLSIG_SignaturePropertyType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #380 If supported: Encode element SignatureProperty */
        /* SE(SignatureProperty) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 18, 5);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_SignatureProperty(EncWsPtr, SignaturePropertyPtr);
        /* EE(SignatureProperty) */
        /* Check EE encoding for SignatureProperty */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_SIGNATURE_TYPE: /* 15 */
      /* #390 Element Signature */
      {
      #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE) && (EXI_ENCODE_XMLSIG_SIGNATURE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_SignatureType, AUTOMATIC, EXI_APPL_DATA) SignaturePtr = (P2CONST(Exi_XMLSIG_SignatureType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #400 If supported: Encode element Signature */
        /* SE(Signature) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 15, 5);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_Signature(EncWsPtr, SignaturePtr);
        /* EE(Signature) */
        /* Check EE encoding for Signature */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_SIGNATURE_VALUE_TYPE: /* 19 */
      /* #410 Element SignatureValue */
      {
      #if (defined(EXI_ENCODE_XMLSIG_SIGNATURE_VALUE) && (EXI_ENCODE_XMLSIG_SIGNATURE_VALUE == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_SignatureValueType, AUTOMATIC, EXI_APPL_DATA) SignatureValuePtr = (P2CONST(Exi_XMLSIG_SignatureValueType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #420 If supported: Encode element SignatureValue */
        /* SE(SignatureValue) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 19, 5);
        Exi_Encode_XMLSIG_SignatureValue(EncWsPtr, SignatureValuePtr);
        /* EE(SignatureValue) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_SIGNED_INFO_TYPE: /* 20 */
      /* #430 Element SignedInfo */
      {
      #if (defined(EXI_ENCODE_XMLSIG_SIGNED_INFO) && (EXI_ENCODE_XMLSIG_SIGNED_INFO == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_SignedInfoType, AUTOMATIC, EXI_APPL_DATA) SignedInfoPtr = (P2CONST(Exi_XMLSIG_SignedInfoType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #440 If supported: Encode element SignedInfo */
        /* SE(SignedInfo) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 20, 5);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_SignedInfo(EncWsPtr, SignedInfoPtr);
        /* EE(SignedInfo) */
        /* Check EE encoding for SignedInfo */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_TRANSFORM_TYPE: /* 21 */
      /* #450 Element Transform */
      {
      #if (defined(EXI_ENCODE_XMLSIG_TRANSFORM) && (EXI_ENCODE_XMLSIG_TRANSFORM == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_TransformType, AUTOMATIC, EXI_APPL_DATA) TransformPtr = (P2CONST(Exi_XMLSIG_TransformType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #460 If supported: Encode element Transform */
        /* SE(Transform) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 21, 5);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_Transform(EncWsPtr, TransformPtr);
        /* EE(Transform) */
        /* Check EE encoding for Transform */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_TRANSFORMS_TYPE: /* 22 */
      /* #470 Element Transforms */
      {
      #if (defined(EXI_ENCODE_XMLSIG_TRANSFORMS) && (EXI_ENCODE_XMLSIG_TRANSFORMS == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_TransformsType, AUTOMATIC, EXI_APPL_DATA) TransformsPtr = (P2CONST(Exi_XMLSIG_TransformsType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #480 If supported: Encode element Transforms */
        /* SE(Transforms) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 22, 5);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_Transforms(EncWsPtr, TransformsPtr);
        /* EE(Transforms) */
        /* Check EE encoding for Transforms */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    case EXI_XMLSIG_X509DATA_TYPE: /* 23 */
      /* #490 Element X509Data */
      {
      #if (defined(EXI_ENCODE_XMLSIG_X509DATA) && (EXI_ENCODE_XMLSIG_X509DATA == STD_ON)) /* PRQA S 3332 */ /*  MD_Exi_19.11_3332 */
        P2CONST(Exi_XMLSIG_X509DataType, AUTOMATIC, EXI_APPL_DATA) X509DataPtr = (P2CONST(Exi_XMLSIG_X509DataType, AUTOMATIC, EXI_APPL_DATA))(EncWsPtr->InputData.StoragePtr); /* PRQA S 0310, 3305 */ /* MD_Exi_11.4, MD_Exi_3305 */
        /* #500 If supported: Encode element X509Data */
        /* SE(X509Data) */
        Exi_VBSEncodeUIntN(&EncWsPtr->EncWs, 23, 5);
        EncWsPtr->EncWs.EERequired = TRUE;
        Exi_Encode_XMLSIG_X509Data(EncWsPtr, X509DataPtr);
        /* EE(X509Data) */
        /* Check EE encoding for X509Data */
        Exi_VBSEncodeCheckAndEncodeEE(&EncWsPtr->EncWs, 0, 1);
      #else
        /* not supported in this configuration */
        Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_DISABLED_FEATURE, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_SCHEMA_ROOT, EXI_E_INV_PARAM);
      #endif
        break;
      }
    default:
      /* #510 Default path */
      {
        /* #520 Set status code to error */
        EncWsPtr->EncWs.StatusCode = EXI_E_INV_EVENT_CODE;
        break;
      }
    }
  }
  /* ------------------------------------------ Development Error Report ------------------------------------------- */
  if (errorId != EXI_E_NO_ERROR)
  {
    /* Call SetStatusCode API to ensure that in case of a DET error the status code is set to failed and stream processing is aborted */
    Exi_VBSEncodeSetStatusCode(&EncWsPtr->EncWs, EXI_E_PARAMETER_CHECK_FAILED, EXI_BS_SSC_CALL_DET_ALLWAYS, EXI_API_ID_ENCODE_XMLSIG_SCHEMA_ROOT, errorId);
  }
} /* PRQA S 2006, 6010, 6030, 6050, 6060, 6080 */ /* MD_MSR_14.7, MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STPAR, MD_MSR_STMIF */
#endif /* (defined(EXI_ENCODE_XMLSIG_SCHEMA_ROOT) && (EXI_ENCODE_XMLSIG_SCHEMA_ROOT == STD_ON)) */


#define EXI_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */  /*  MD_MSR_19.1 */

/* PRQA L:NESTING_OF_CONTROL_STRUCTURES_EXCEEDED */
#endif /* (defined(EXI_ENABLE_ENCODE_XMLSIG_MESSAGE_SET) && (EXI_ENABLE_ENCODE_XMLSIG_MESSAGE_SET == STD_ON)) */

