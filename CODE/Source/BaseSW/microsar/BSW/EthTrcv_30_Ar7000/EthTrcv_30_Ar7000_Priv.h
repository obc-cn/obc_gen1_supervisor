/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  EthTrcv_30_Ar7000_Priv.h
 *        \brief  Ethernet transceiver driver QCA 7005 private header file
 *
 *      \details  Vector static code private header file for the Ethernet transceiver driver QCA 7005 module. This 
 *                header file contains module internal declarations.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's main header file EthTrcv_30_Ar7000.h.
 *********************************************************************************************************************/

#if !defined (ETHTRCV_30_AR7000_PRIV_H)
# define ETHTRCV_30_AR7000_PRIV_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#if (ETHTRCV_30_AR7000_DEM_ERROR_DETECT == STD_ON)
#include "Dem.h"
#endif /* ETHTRCV_30_AR7000_DEM_ERROR_DETECT */
#if (ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON)
#include "Det.h"
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_DETECT */
#include "EthTrcv_30_Ar7000_Trcv.h"
#include "EthTrcv_30_Ar7000_PrivateCfg.h"
#include "SchM_EthTrcv_30_Ar7000.h"

/**********************************************************************************************************************
 *  GLOBAL CONSISTENCY CHECKS
 *********************************************************************************************************************/
#if (ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL != 1)
#error "AR7000 transceiver driver does only support 1 transceiver instance"
#endif

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined(ETHTRCV_30_AR7000_NMK_SIZE_BYTE)
#define ETHTRCV_30_AR7000_NMK_SIZE_BYTE 16u
#endif
#if !defined(ETHTRCV_30_AR7000_NID_SIZE_BYTE)
#define ETHTRCV_30_AR7000_NID_SIZE_BYTE 7u
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/
#if defined ETHTRCV_MMTYPE_TRACE
/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MMTypeTxTrace
 **********************************************************************************************************************/
/*!
 * \brief       Test function to trace the transmitted MME frames.
 * \details     -
 * \param[in]   MMType          MM type that shall be added to the trace
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   FALSE
 * \synchronous TRUE
 * \config      ETHTRCV_MMTYPE_TRACE
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MMTypeTxTrace(uint16 MMType);
#endif /* ETHTRCV_MMTYPE_TRACE */

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_VFrameHeaderGeneric
 **********************************************************************************************************************/
/*!
 * \brief       Initialze a buffer for generic MME frame transmission.
 * \details     Does set generic MME header information to a given buffer and intializes the required length with 0x00
 * \param[in]     TrcvIdx         Transceiver index
 * \param[in]     MMType          MM type to write to the buffer
 * \param[in,out] EthBufPtr       Pointer to the buffer that shall be initialized
 * \param[in]     ZeroInitLenByte Overall length of the MME frame
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      -
 */
FUNC(void, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_VFrameHeaderGeneric(
    uint8                                                                                               TrcvIdx,
    uint16                                                                                              MMType,
    P2VAR(EthTrcv_30_Ar7000_EthernetBufferType, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) EthBufPtr,
    uint16                                                                                              ZeroInitLenByte);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_CmSetKeyReq
 **********************************************************************************************************************/
/*!
 * \brief       Sends a CM_SET_KEY.REQ to the QCA7005.
 * \details     Does send a CM_SET_KEY.REQ to the local QCA7005 to set up a new NMK and NID in the QCA7005's PIB file
 * \param[in]     TrcvIdx         Transceiver index
 * \param[in]     NidPtr          Pointer to the NID to set
 * \param[in]     NmkPtr          Pointer to the NMK to set
 * \return      E_OK       success
 * \return      E_NOT_OK   Transmission not successful
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      -
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_CmSetKeyReq(
    uint8 TrcvIdx,
    P2CONST(uint8, ETHTRCV_30_AR7000_APPL_CONST, ETHTRCV_30_AR7000_APPL_CONST) NidPtr,
    P2CONST(uint8, ETHTRCV_30_AR7000_APPL_CONST, ETHTRCV_30_AR7000_APPL_CONST) NmkPtr);

/**********************************************************************************************************************
 *  EthTrcv_30_Ar7000_MME_GetLinkStatus
 **********************************************************************************************************************/
/*!
 * \brief       Sends a VS_PL_LNK_STATUS.REQ to the QCA7005.
 * \details     Does send a VS_PL_LNK_STATUS.REQ to the local QCA7005 to retriev the current link state
 * \param[in]     TrcvIdx         Transceiver index
 * \return      E_OK       success
 * \return      E_NOT_OK   Transmission not successful
 * \pre         -
 * \context     TASK|ISR2
 * \reentrant   TRUE
 * \synchronous TRUE
 * \config      -
 */
FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_MME_GetLinkStatus(
    uint8 TrcvIdx);

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 **********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
typedef uint8  EthTrcv_30_Ar7000_Slac_ValidationResultType;
typedef uint16 SlacFlagsType;
/* Slac mode flags */
#define ETHTRCV_30_AR7000_SLAC_FLAG_INITIALIZED                   0x0001u
#define ETHTRCV_30_AR7000_SLAC_FLAG_SET_CCO                       0x0002u
#define ETHTRCV_30_AR7000_SLAC_FLAG_SET_CAP3_PRIO                 0x0004u
#define ETHTRCV_30_AR7000_SLAC_FLAG_PARAM_REQUEST                 0x0008u
#define ETHTRCV_30_AR7000_SLAC_FLAG_SOUNDING                      0x0010u
#define ETHTRCV_30_AR7000_SLAC_FLAG_VALIDATING1                   0x0020u
#define ETHTRCV_30_AR7000_SLAC_FLAG_VALIDATING2                   0x0040u
#define ETHTRCV_30_AR7000_SLAC_FLAG_CONFIRMING                    0x0080u
#define ETHTRCV_30_AR7000_SLAC_FLAG_JOINING                       0x0100u

#define ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_START                 0x0200u
#define ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_LOCAL                 0x0400u
#define ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_REMOTE                0x0800u
#define ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_EXCHANGED             0x0E00u
#define ETHTRCV_30_AR7000_SLAC_FLAG_AMP_MAP_SET                   0x1000u

#define ETHTRCV_30_AR7000_SLAC_FLAG_WAIT_FOR_LINK                 0x2000u
#define ETHTRCV_30_AR7000_SLAC_FLAG_LINK_VALID                    0x4000u
#define ETHTRCV_30_AR7000_SLAC_FLAG_CON_PARAMS_VALID              0x8000u

#define ETHTRCV_30_AR7000_SLAC_FLAG_TERMINATE_MASK                0xC000u
#define ETHTRCV_30_AR7000_SLAC_FLAG_MASK                          0x7FFFu

typedef struct
{
  /* General */
  SlacFlagsType SlacFlags;
  EthTrcv_30_Ar7000_Slac_StartModeType SlacMode;
  uint8 SlacRunId [8];
#if ((defined ETHTRCV_30_AR7000_SLAC_RESP_TYPE) && (1u == ETHTRCV_30_AR7000_SLAC_RESP_TYPE))
  EthTrcv_PhysAddrType SlacFwdSta;
#endif

  /* Parameter exchange */
  uint8 SlacParamCnfRecvd;

  /* Sounding */
  uint16 SlacTimeOut;
  uint8 SlacCntSounds;
  uint8 SlacCntStartAtten;
  uint8 SlacNumSounds;

  /* Timeouts */
  uint16 SlacCurProcTimeout;
  uint8 SlacReqRetries;
  uint8 SlacMainFctCnt;

  /* Attenuation profiles and validation */
  uint8 SlacNumProfiles;
  uint8 SlacAttnCharIndRecvd;
  uint8 SlacCurProfileValidation;
  uint8 SlacCurToggleNum;
  P2VAR(
    EthTrcv_30_Ar7000_Slac_AttenCharProfileType,
    ETHTRCV_30_AR7000_VAR_NOINIT,
    ETHTRCV_30_AR7000_VAR_NOINIT)
    SlacProfilesSorted [ETHTRCV_30_AR7000_SLAC_MAX_ATTEN_CHAR_PROFILES];
  EthTrcv_30_Ar7000_Slac_ValidationResultType SlacValidationResult;

  /* Connection parameters */
  uint8 SlacNid [ETHTRCV_30_AR7000_NID_SIZE_BYTE];
  uint8 SlacNmk [ETHTRCV_30_AR7000_NMK_SIZE_BYTE];
  EthTrcv_PhysAddrType SlacPhysAddr;

  /* Amp map exchange */
#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE) && (STD_ON == ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE))
   uint16 SlacAmpMapRxEntries;
   uint16 SlacAmpMapTimeout;
   /* 4 bits are used per carrier (2 carriers per byte).
    * Ensure that amp map array is big enough and one byte divisible ((...+1)/2) */
   uint8 SlacCurAmpMap[(ETHTRCV_30_AR7000_SLAC_AMP_MAP_ENTRIES + 1) / 2];
   uint8 SlacAmpMapRespRetries;
   boolean SlacAmpMapQcaRspRcvd;
   boolean SlacAmpMapRemoteReqRcvd;
   boolean SlacAmpMapLocalRspRcvd;
#endif
  /* ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE */

} EthTrcv_30_Ar7000_Slac_MgmtType;
#endif
  /* ETHTRCV_30_AR7000_ENABLE_SLAC */

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 **********************************************************************************************************************/
#define ETHTRCV_30_AR7000_START_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#if ( ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON )
extern VAR(EthTrcv_StateType, ETHTRCV_30_AR7000_VAR_ZERO_INIT) EthTrcv_30_Ar7000_State;
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_DETECT == STD_ON */

#define ETHTRCV_30_AR7000_STOP_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#define ETHTRCV_30_AR7000_START_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#if defined ( ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC )
extern VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_FwReadyForSlacLastResult;
#endif /* ETHTRCV_30_AR7000_APPL_CBK_FW_READY_FOR_SLAC */

#if ( (STD_ON==ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP) )
extern VAR(uint8, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_WillResetAfterPibUpload;
#endif /* ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD && ETHTRCV_30_AR7000_CALC_NMK_LOCALLY */

#define ETHTRCV_30_AR7000_STOP_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#if ((defined ETHTRCV_30_AR7000_ENABLE_SLAC) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_SLAC))
#define ETHTRCV_30_AR7000_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

extern VAR(EthTrcv_30_Ar7000_Slac_MgmtType, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_Slac_MgmtStruct;

#define ETHTRCV_30_AR7000_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#endif
 /* ETHTRCV_30_AR7000_ENABLE_SLAC */

#define ETHTRCV_30_AR7000_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

extern CONST(uint8, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_AtherosODA[6];

#define ETHTRCV_30_AR7000_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *  GLOBAL DEFINES
 **********************************************************************************************************************/
/* only one instance is supported */
#define ETHTRCV_30_AR7000_INSTANCE_ID                0U
/* only one transceiver is supported */
#define ETHTRCV_30_AR7000_MAX_CFGS_TOTAL             1U
/* value for pre-compile time config variant */
#define ETHTRCV_30_AR7000_CONFIG_VARIANT_PRECOMPILE  1U
/* value for the transmission priority */
#define ETHTRCV_30_AR7000_TX_PRIO                    0U

/**********************************************************************************************************************
 *  GLOBAL MACROS
 **********************************************************************************************************************/
#define ETHTRCV_30_AR7000_BEGIN_CRITICAL_SECTION() SchM_Enter_EthTrcv_30_Ar7000_ETHTRCV_30_AR7000_EXCLUSIVE_AREA_0()
#define ETHTRCV_30_AR7000_END_CRITICAL_SECTION()   SchM_Exit_EthTrcv_30_Ar7000_ETHTRCV_30_AR7000_EXCLUSIVE_AREA_0()

/* Diagnostic Event Manager */
/* PRQA S 3453 1 */ /* MD_MSR_19.7 */
#define ETHTRCV_30_AR7000_DEM_REPORT_ERROR_STATUS(ErrId)   ((void) Dem_ReportErrorStatus((ErrId), DEM_EVENT_STATUS_FAILED))

/* Development Error Tracer */
#if !defined(ETHTRCV_30_AR7000_DEV_ERROR_REPORT)
#define ETHTRCV_30_AR7000_DEV_ERROR_REPORT  ETHTRCV_30_AR7000_DEV_ERROR_DETECT
#endif
/* PRQA S 3453 5 */ /* MD_MSR_19.7 */
#if ( ETHTRCV_30_AR7000_DEV_ERROR_REPORT == STD_ON )
#define ETHTRCV_30_AR7000_SET_ERROR_ID(errorId, value) (errorId) = (value)
#else
#define ETHTRCV_30_AR7000_SET_ERROR_ID(errorId, value) ETHTRCV_30_AR7000_DUMMY_STATEMENT(errorId)
#endif /* ETHTRCV_30_AR7000_DEV_ERROR_REPORT */

/* PRQA S 3453 2 */ /* MD_MSR_19.7 */
#define ETHTRCV_30_AR7000_HTOLIEN32(Data)  IPBASE_LE2HE32(Data)
#define ETHTRCV_30_AR7000_LIENTOH32(Data)  IPBASE_LE2HE32(Data)

/* PRQA S 3453 4 */ /* MD_MSR_19.7 */
#define ETHTRCV_30_AR7000_U8PTRDATA_TO_VAL32(U8DataPtr, Idx)  (uint32)(   (uint32)(U8DataPtr)[(Idx)]                           | \
                                                                        (((uint32)(U8DataPtr)[(Idx)+1u] <<  8u) & 0x0000FF00u) | \
                                                                        (((uint32)(U8DataPtr)[(Idx)+2u] << 16u) & 0x00FF0000u) | \
                                                                        (((uint32)(U8DataPtr)[(Idx)+3u] << 24u) & 0xFF000000u)  )

/* PRQA S 3453 2 */ /* MD_MSR_19.7 */
#define ETHTRCV_30_AR7000_U8PTRDATA_TO_VAL16(U8DataPtr, Idx)  (uint16)(   (uint32)(U8DataPtr)[(Idx)]                           | \
                                                                        (((uint32)(U8DataPtr)[(Idx)+1u] <<  8u) & 0x0000FF00u)  )

/* Configuration access (depending on configuration variant)*/
#define EthTrcv_30_Ar7000_VCfgGetEthIfCtrlIdx()               (ETHTRCV_30_AR7000_ETHIF_CTRL)
#define EthTrcv_30_Ar7000_VCfgGetEthCtrlIdx()                 (ETHTRCV_30_AR7000_HW_IDX)
#if ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS) && (ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS == STD_ON))
/* PRQA S 3453 1 */ /* MD_MSR_19.7 */
#define EthTrcv_30_Ar7000_VCfgGetPhysAddrBlockId(TrcvIdx)   (EthTrcv_30_Ar7000_PhysAddrBlockIds[TrcvIdx])
#define EthTrcv_30_Ar7000_VCfgGetPhysAddr()                 (EthTrcv_30_Ar7000_PhysAddr)
#endif /* ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS */

#if ((defined ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK) && (STD_ON == ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK))
#define EthTrcv_30_Ar7000_VCfgGetPhysAddr()                 (ETHTRCV_30_AR7000_APPL_CBK_GET_PHYS_ADDR())
#endif /* ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK */

#if ((defined ETHTRCV_30_AR7000_ENABLE_WRITE_DAK) && (ETHTRCV_30_AR7000_ENABLE_WRITE_DAK == STD_ON))
#define EthTrcv_30_Ar7000_VCfgGetDak()                      (EthTrcv_30_Ar7000_Dak)
/* PRQA S 3453 1 */ /* MD_MSR_19.7 */
#define EthTrcv_30_Ar7000_VCfgGetDakBlockId(TrcvIdx)        (EthTrcv_30_Ar7000_DakBlockIds[TrcvIdx])
#endif /* ETHTRCV_30_AR7000_ENABLE_WRITE_DAK */

#if ((defined ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP) && (ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP == STD_ON))
#define EthTrcv_30_Ar7000_Slac_VCfgGetAmpMapBlockId()       (EthTrcv_30_Ar7000_Slac_AmpMapBlockId)
#endif /* ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP */

#if ((defined ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION) && (ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION == STD_ON))
#define EthTrcv_30_Ar7000_Slac_VCfgGetCalibBlockId()        (EthTrcv_30_Ar7000_Slac_CalibBlockId)
#endif /* ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION */

#endif
  /* ETHTRCV_30_AR7000_PRIV_H */
/**********************************************************************************************************************
 *  END OF FILE: EthTrcv_30_Ar7000_Priv.h
 *********************************************************************************************************************/

