/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actBNModDiv2.c
 *        \brief  A basic (unsigned) integer and module arithmetic used for elliptic curve point arithmetic.
 *
 *      \details Currently the actClib version is used.
 *               This file is part of the embedded systems library actCLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#define ACTBNMODDIV2_SOURCE

#include "actBigNum.h"
#include "actUtilities.h"
#include "actWatchdog.h"

#if (VSECPRIM_ACTBNMODDIV2_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTION:
 **  void actBNModDiv2(actBNDIGIT* a,
 **                    const actBNRING* Ring)
 **
 **  Modular Division by 2 (a /= 2 mod m, m=Ring->m).
 **
 ** input:                                              (length in digits)
 ** - a:      ring element to be divided                    m_length
 ** - Ring:   the underlying ring (see assumptions)
 ** - Ring->m:        the modulus m                         m_length
 ** - Ring->m_length: equals to m_length
 **
 ** output:
 ** - a:      the result a / 2 mod m                        m_length
 **
 ** assumes:
 ** - The actBNRING structure parameter 'Field' holds all necessary
 **   information. It has to be initialized as far as listed above.
 **   Please have a look at the actBNRING definition; an example for
 **   a complete initialization is the actECDSAVerify(..) implementation
 **   in actIECDSA.c.
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actBNModDiv2(
   VSECPRIM_P2VAR_PARA(actBNDIGIT) a, VSECPRIM_P2CONST_PARA(actBNRING) Ring,
   VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void))
{
  actLengthType length = Ring->m_length;
  actU8 first_carry = 0;

  actL2trigger(watchdog); /* PRQA S 1338, 2983, 3112  */ /* MD_MSR_DummyStmt */ /*lint -e438 */ /* SBSW_VSECPRIM_FUNCTION_CALL_WATCHDOG */

  /* if a is odd, do a+=modulus */
  if (actBNIsOdd(a))
  {
    first_carry = (actU8) actBNAdd(a, Ring->m, a, length);
  }

  /* shift a to the right by 1 bit */
  actBNDiv2(a, first_carry, length);
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTBNMODDIV2_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actBNModDiv2.c
 *********************************************************************************************************************/
