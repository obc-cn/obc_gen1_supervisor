/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  ESLib_RSA_OAEP_Dec_SHA1.c
 *        \brief Crypto library - PKCS #1 RSA OAEP (Decryption)
 *
 *      \details RSA decryption using RSA OAEP encoding scheme according to PKCS #1 v2.2
 *               Currently the actClib version is used.
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/
#define ESLIB_RSA_OAEP_DEC_SHA1_SOURCE

/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/** \file
 *  \brief        Crypto library - PKCS #1 RSA OAEP (Encryption/Decryption)
 *
 *  \description  This file is part of the embedded systems library cvActLib/ES.
 *                RSA decryption using RSA OAEP encoding scheme according to PKCS #1 v2.2
 *  -------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \par Copyright
 *  \verbatim
 *  Copyright (c) 2016 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 */
/*********************************************************************************************************************/

/***********************************************************************************************************************
 *  INCLUDES
 **********************************************************************************************************************/

#include "ESLib.h"
#include "ESLib_types.h"

#include "actIRSA.h"
#include "actUtilities.h"
#include "actISHA.h"

#if (VSECPRIM_RSA_OAEP_SHA1_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/***********************************************************************************************************************
 *  DEFINES
 **********************************************************************************************************************/

/** Hash initialization function */
# define ESL_RSA_OAEP_INIT_HASH_FCT(workspace)                        (void)actSHAInit(&((workspace)->wsSHA1))
/** Hash update function */
# define ESL_RSA_OAEP_UPDATE_HASH_FCT(workspace, inputSize, input)    actSHAUpdate(&((workspace)->wsSHA1), (VSECPRIM_P2CONST_PARA(actU8))(input), (actLengthType)(inputSize), (workspace)->header.watchdog)
/** Hash finalization function */
# define ESL_RSA_OAEP_FINALIZE_HASH_FCT(workspace, messageDigest)     (void)actSHAFinalize(&((workspace)->wsSHA1), (VSECPRIM_P2VAR_PARA(actU8))(messageDigest),  (workspace)->header.watchdog)

/** Size of message digest (hash) */
# define ESL_RSA_OAEP_SIZEOF_HASH                                     ESL_SIZEOF_SHA1_DIGEST

/** Offset of buffer length in work space */
# define ESL_WS_RSA_OAEP_ED_BUFFERLENGTH                              ESL_WS_RSA_OAEP_ED_SHA1_BUFFERLENGTH
/** Offset of message buffer in work space */
# define ESL_WS_RSA_OAEP_ED_BUFFER                                    ESL_WS_RSA_OAEP_ED_SHA1_BUFFER
/** Offset of DB in work space */
# define ESL_WS_RSA_OAEP_ED_DB_BUFFER(ByteSizeOfBuffer)               ESL_WS_RSA_OAEP_ED_SHA1_DB_BUFFER(ByteSizeOfBuffer)
/** Offset of hash work space embedded in work space */
# define ESL_WS_RSA_OAEP_ED_WS_HASH(ByteSizeOfBuffer)                 ESL_WS_RSA_OAEP_ED_SHA1_WS_SHA1(ByteSizeOfBuffer)
/** Offset of RSA work space embedded in work space */
# define ESL_WS_RSA_OAEP_ED_WS_RSA_PRIM(ByteSizeOfBuffer)             ESL_WS_RSA_OAEP_ED_SHA1_WS_RSA_PRIM(ByteSizeOfBuffer)
/** Total work space overhead */
# define ESL_SIZEOF_WS_RSA_OAEP_ED_OVERHEAD(ByteSizeOfBuffer)         ESL_SIZEOF_WS_RSA_OAEP_ED_SHA1_OVERHEAD(ByteSizeOfBuffer)

/***********************************************************************************************************************
 *  TYPEDEFS
 **********************************************************************************************************************/

/** Hash operation work space */
typedef eslt_WorkSpaceSHA1 eslt_WorkSpaceHash;

/***********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 **********************************************************************************************************************/
# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/***********************************************************************************************************************
 *  esl_initDecryptRSASHA1_OAEP
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptRSASHA1_OAEP(
                                              VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPdec) workSpace,
                                              eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
                                              eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent)
{
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader) wsHeader;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash) wsHash;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec_prim) wsPRIM;
  eslt_ErrorCode returnValue;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* Get underlying work space pointers */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte))workSpace;
    wsHeader = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader))wsRAW;
    wsHash = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash))(wsRAW + ESL_WS_RSA_OAEP_ED_WS_HASH(keyPairModuleSize));
    wsPRIM = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec_prim))(wsRAW + ESL_WS_RSA_OAEP_ED_WS_RSA_PRIM(keyPairModuleSize));

    if (wsHeader->size < (ESL_SIZEOF_WS_RSA_OAEP_ED_OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)))
    {
      returnValue = ESL_ERC_WS_TOO_SMALL;
    }
    else
    {
      wsHash->header.size = (eslt_Length)(sizeof(eslt_WorkSpaceHash) - sizeof(eslt_WorkSpaceHeader));
      wsHash->header.watchdog = wsHeader->watchdog;

      wsPRIM->header.size = (eslt_Length)(wsHeader->size - (ESL_SIZEOF_WS_RSA_OAEP_ED_OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)));
      wsPRIM->header.watchdog = wsHeader->watchdog;
      returnValue = esl_initDecryptRSA_prim(wsPRIM, keyPairModuleSize, keyPairModule, privateKeyExponentSize, privateKeyExponent);
    }

    if (ESL_ERC_NO_ERROR == returnValue)
    {
      wsHeader->status = (ESL_WST_ALGO_RSA | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);
      /* Store key pair module size / message buffer length in work space */
      wsRAW[ESL_WS_RSA_OAEP_ED_BUFFERLENGTH] = (eslt_Byte)(keyPairModuleSize >> 8u);
      wsRAW[ESL_WS_RSA_OAEP_ED_BUFFERLENGTH + 1u] = (eslt_Byte)(keyPairModuleSize);
    }
  }

  return returnValue;
}

/***********************************************************************************************************************
 *  esl_decodeRSASHA1_OAEP
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_decodeRSASHA1_OAEP(
                                  VSECPRIM_P2VAR_PARA(eslt_WorkSpace) workSpace, eslt_Length encodedMessageSize,
                                  eslt_Length labelSize, VSECPRIM_P2CONST_PARA(eslt_Byte) label,
                                  VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message)
{
  eslt_Length keyPairModuleSize;
  eslt_Length index;
  eslt_Byte combinedValue;
  eslt_Length dummy;
  eslt_Length psLength;
  eslt_Length dbLength;
  eslt_Length messageLength;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader) wsHeader;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec_prim) wsPRIM;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash) wsHash;
  VSECPRIM_P2VAR_PARA(eslt_Byte) encodedMessage;
  VSECPRIM_P2VAR_PARA(eslt_Byte) db;
  VSECPRIM_P2VAR_PARA(eslt_Byte) maskedDb;
  VSECPRIM_P2VAR_PARA(eslt_Byte) maskedSeed;
  VSECPRIM_P2VAR_PARA(eslt_Byte) ps;
  eslt_ErrorCode returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* Get underlying workspace pointers */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte))workSpace;
    wsHeader = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader))wsRAW;
    keyPairModuleSize = (eslt_Length)(((eslt_Length)wsRAW[ESL_WS_RSA_OAEP_ED_BUFFERLENGTH] << 8) + (wsRAW[ESL_WS_RSA_OAEP_ED_BUFFERLENGTH + 1]));
    wsPRIM = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec_prim))(wsRAW + ESL_WS_RSA_OAEP_ED_WS_RSA_PRIM(keyPairModuleSize));
    /* General memory layout in workspace:
     | EM                                           | DB                            | Temp  | -    |
     | 0x00 | mSeed | maskedDB                      | lHash | PS   | 0x01 | M       |       | -    | */
    encodedMessage = wsRAW + ESL_WS_RSA_OAEP_ED_BUFFER;
    db = wsRAW + ESL_WS_RSA_OAEP_ED_DB_BUFFER(keyPairModuleSize);
    wsHash = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash))(wsRAW + ESL_WS_RSA_OAEP_ED_WS_HASH(keyPairModuleSize));

    /* 7.1.2, Step 1.c: Length check */
    if (encodedMessageSize < (2u * (ESL_RSA_OAEP_SIZEOF_HASH + 1u)))
    {
      returnValue = ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE;
    }

    /* 7.1.2, Step 3: EME-OAEP encoding decoding */
    if (ESL_ERC_NO_ERROR == returnValue)
    {
      /* Length of maskedDB / DB */
      dbLength = encodedMessageSize - (ESL_RSA_OAEP_SIZEOF_HASH + 1u);
      /* Points to maskedDb in EM */
      maskedDb = &encodedMessage[ESL_RSA_OAEP_SIZEOF_HASH + 1u];
      /* Points to maskedSeed in EM */
      maskedSeed = &encodedMessage[1u];

      /* 7.1.2, Step 3.c: Generate seedMask using mask generation function (MGF) with maskedDB as input
       Memory contents:
       | EM                                           | DB                            | Temp  | -    |
       | Y    | mSeed | maskedDB                      | seedM | -    | -    | -       | hash  | -    | */
      returnValue = esl_generateMaskMGF1RSASHA1_PSS(wsHash, &db[dbLength], dbLength, maskedDb, ESL_RSA_OAEP_SIZEOF_HASH, db);

      if (ESL_ERC_NO_ERROR == returnValue)
      {
        /* 7.1.2, Step 3.d: Apply mask to maskedSeed using XOR to get original seed
         Memory contents:
         | EM                                           | DB                            | Temp  | -    |
         | Y    | seed  | maskedDB                      | seedM | -    | -    | -       | -     | -    | */
        actXOR(maskedSeed, db, ESL_RSA_OAEP_SIZEOF_HASH); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */

        /* 7.1.2, Step 3.e: Generate dbMask using mask generation function (MGF) with seed as input
         Memory contents:
         | EM                                           | DB                            | Temp  | -    |
         | Y    | seed  | maskedDB                      | dbMask                        | hash  | -    | */
        returnValue = esl_generateMaskMGF1RSASHA1_PSS(wsHash, &db[dbLength], ESL_RSA_OAEP_SIZEOF_HASH, maskedSeed, dbLength, db);
      }

      if (ESL_ERC_NO_ERROR == returnValue)
      {
        /* 7.1.2, Step 3.f: Apply mask to maskedDB using XOR to get original DB
         Memory contents:
         | EM                                           | DB                            | Temp  | -    |
         | Y    | seed  | maskedDB                      | lHash'| PS   | ?    | M       | -     | -    | */
        actXOR(db, maskedDb, dbLength); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */

        /* 7.1.2, Step 3.a: Calculate hash lHash from label L
         Step 3.c: Place lHash at beginning of DB
         Memory contents:
         | EM                                           | DB                            | Temp  | -    |
         | Y    | lHash | maskedDB                      | lHash'| PS   | ?    | M       | -     | -    | */

        /* Initialize hash calculation */
        ESL_RSA_OAEP_INIT_HASH_FCT(wsHash);

        if (labelSize > 0u)
        {
          if (actOK != ESL_RSA_OAEP_UPDATE_HASH_FCT(wsHash, labelSize, label))
          {
            returnValue = ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW;
          }
        }

        if (ESL_ERC_NO_ERROR == returnValue)
        {
          ESL_RSA_OAEP_FINALIZE_HASH_FCT(wsHash, maskedSeed);
        }
      }

      if (ESL_ERC_NO_ERROR == returnValue)
      {
        /* Points to PS in DB */
        ps = &db[ESL_RSA_OAEP_SIZEOF_HASH];

        /* 7.1.2, Step 3.g: Evaluate length of PS, by checking octets of DB after lHash' which are zero
         Iterate over complete potential PS to prevent timing analysis */
        psLength = 0u;
        dummy = 0u;
        combinedValue = 0x00u;

        /* Iterate over complete potential PS to prevent timing analysis */
        for (index = 0u; index < (dbLength - ESL_RSA_OAEP_SIZEOF_HASH); index++)
        {
          combinedValue |= ps[index];

          /* All previous values were zero? */
          if (0x00u == combinedValue)
          {
            /* Increment actual length of PS */
            psLength++;
          }
          else
          {
            /* Increment dummy value instead */
            dummy++;
          }
        }

        /* 7.1.2, Step 3.g: Verify that an octet value 0x01 separates PS and M */
        if (0x01u != ps[psLength])
        {
          returnValue = ESL_ERC_RSA_ENCODING_INVALID;
        }
        /* 7.1.2, Step 3.g: Verify that Y has a value of zero */
        else if (0x00u != encodedMessage[0u])
        {
          returnValue = ESL_ERC_RSA_ENCODING_INVALID;
        }
        /* 7.1.2, Step 3.g: Verify lHash and lHash' are equal */
        else if (TRUE != actMemcmp(db, maskedSeed, ESL_RSA_OAEP_SIZEOF_HASH)) /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
        {
          returnValue = ESL_ERC_RSA_ENCODING_INVALID;
        }
        else
        {
          /* 7.1.2, Step 4: Output message M
          Memory contents:
          | EM                                           | DB                            | Temp  | -    |
          | 0x00 | lHash | maskedDB                      | lHash'| PS   | 0x01 | M       | -     | -    | */
          messageLength = dbLength - (psLength + ESL_RSA_OAEP_SIZEOF_HASH + 1u);

          if (*messageSize < messageLength)
          {
            returnValue = ESL_ERC_OUTPUT_SIZE_TOO_SHORT;
          }
          else
          {
            *messageSize = messageLength;
            actMemcpy(message, &ps[psLength + 1u], messageLength); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
          }
        }
      }
    }
  }

  return returnValue;
}

/***********************************************************************************************************************
 *  esl_decryptRSASHA1_OAEP_Label
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSASHA1_OAEP_Label(
                                  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPdec) workSpace,
                                  eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
                                  eslt_Length labelSize, VSECPRIM_P2CONST_PARA(eslt_Byte) label,
                                  VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message)
{
  eslt_ErrorCode returnValue;
  eslt_Length keyPairModuleSize;
  eslt_Length encodedMessageSize;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader) wsHeader;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec_prim) wsPRIM;
  VSECPRIM_P2VAR_PARA(eslt_Byte) encodedMessage;

  returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* Get underlying workspace pointers */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte))workSpace;
    wsHeader = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader))wsRAW;
    keyPairModuleSize = (eslt_Length)(((eslt_Length)wsRAW[ESL_WS_RSA_OAEP_ED_BUFFERLENGTH] << 8) + (wsRAW[ESL_WS_RSA_OAEP_ED_BUFFERLENGTH + 1]));
    wsPRIM = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec_prim))(wsRAW + ESL_WS_RSA_OAEP_ED_WS_RSA_PRIM(keyPairModuleSize));
    /* General memory layout in workspace:
     | EM                                           | DB                            | Temp  | -    |
     | 0x00 | mSeed | maskedDB                      | lHash | PS   | 0x01 | M       |       | -    | */
    encodedMessage = wsRAW + ESL_WS_RSA_OAEP_ED_BUFFER;
    encodedMessageSize = keyPairModuleSize;

    /* 7.1.2, Step 2: RSA decryption */
    returnValue = esl_decryptRSA_prim(wsPRIM, cipherSize, cipher, &encodedMessageSize, encodedMessage);

    if (ESL_ERC_NO_ERROR == returnValue)
    {
      /* 7.1.2, Step 3: EME-OAEP encoding decoding */
      returnValue = esl_decodeRSASHA1_OAEP((VSECPRIM_P2VAR_PARA(eslt_WorkSpace))workSpace, encodedMessageSize, labelSize, label, messageSize, message);
    }
  }

  return returnValue;
}

/***********************************************************************************************************************
 *  esl_decryptRSASHA1_OAEP
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSASHA1_OAEP(
                                    VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPdec) workSpace,
                                    eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
                                    VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message)
{
  return esl_decryptRSASHA1_OAEP_Label(workSpace, cipherSize, cipher, 0u, 0u, messageSize, message);
}

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_RSA_OAEP_SHA1_ENABLED == STD_ON) */
/**********************************************************************************************************************
 *  END OF FILE: ESLib_RSA_OAEP_Dec_SHA1.c
 *********************************************************************************************************************/
