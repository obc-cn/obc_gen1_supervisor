/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file ESLib_RSAprim_enc.c
 *        \brief RSA encrypt primitive
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#define ESIB_RSA_PRIM_ENC_SOURCE

#include "ESLib.h"
#include "ESLib_types.h"

/* actCLib includes */
#include "actIRSA.h"

#if (VSECPRIM_RSA_CIPHER_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/
# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 * esl_initEncryptRSA_prim
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptRSA_prim(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAenc_prim) workSpace,
                                                      eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule, eslt_Length publicKeyExponentSize, eslt_pRomByte publicKeyExponent)
{
  /* Init actCLib RSA public key initialization */
  {
    actRETURNCODE result = actRSAInitPublicKeyOperation(keyPairModule, (int)keyPairModuleSize, publicKeyExponent, (int)publicKeyExponentSize,
                                                        workSpace->wsRSA, (int)(workSpace->header.size), workSpace->header.watchdog);
    if (result != actOK)
    {
      if (result == actEXCEPTION_NULL)
      {
        return ESL_ERC_PARAMETER_INVALID;
      }
      else if (result == actEXCEPTION_MEMORY)
      {
        return ESL_ERC_WS_TOO_SMALL;
      }
      else if (result == actRSA_PARAM_OUT_OF_RANGE)
      {
        return ESL_ERC_PARAMETER_INVALID;
      }
      else if (result == actEXCEPTION_PUBKEY)
      {
        return ESL_ERC_RSA_PUBKEY_INVALID;
      }
    }
  }

  /* Set workSpace state */
  workSpace->header.status = (ESL_WST_ALGO_RSA | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);

  return ESL_ERC_NO_ERROR;
}

/****************************************************************************
 * esl_encryptRSA_prim
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptRSA_prim(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAenc_prim) workSpace,
                                                  eslt_Length messageSize, VSECPRIM_P2CONST_PARA(eslt_Byte) message, VSECPRIM_P2VAR_PARA(eslt_Length) cipherSize, VSECPRIM_P2VAR_PARA(eslt_Byte) cipher)
{
  /* Check workSpace */
  if ((workSpace->header.status & ESL_WST_M_ALGO) != ESL_WST_ALGO_RSA)
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  if (!(workSpace->header.status & ESL_WST_M_RUNNING))
  {
    return ESL_ERC_WS_STATE_INVALID;
  }

  /* Call actCLib RSA public key operation */
  {
    int cipherLen = *cipherSize;
    actRETURNCODE result = actRSAPublicKeyOperation(message, (int)messageSize, cipher, &cipherLen,
                                                    workSpace->wsRSA, workSpace->header.watchdog);
    if (result != actOK)
    {
      if (result == actEXCEPTION_NULL)
      {
        return ESL_ERC_PARAMETER_INVALID;
      }
      else if (result == actEXCEPTION_MEMORY)
      {
        return ESL_ERC_OUTPUT_SIZE_TOO_SHORT;
      }
      else if (result == actRSA_PARAM_OUT_OF_RANGE)
      {
        return ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE;
      }
    }
    *cipherSize = (eslt_Length) (cipherLen & 0xFFFFu);

  }

  return ESL_ERC_NO_ERROR;
}

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_RSA_CIPHER_ENABLED == STD_ON) */
/********************************************************************************************************************** 
 *  END OF FILE: ESLib_RSAprim_enc.c 
 *********************************************************************************************************************/
