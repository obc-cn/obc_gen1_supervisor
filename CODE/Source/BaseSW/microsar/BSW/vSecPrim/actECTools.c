/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actECTools.c
 *        \brief  ECC domain parameter initialization
 *
 *      \details This file is part of the embedded systems library cvActLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

/****************************************************************************
 **
 ** This file contains: The ECC domain parameters are initialized by
 **                     the ECInit module.
 **
 ** constants:
 **
 ** globals:
 **
 ** functions:
 **   actECInit
 **
 ***************************************************************************/

#include "actITypes.h"
#include "actECTools.h"
#include "actUtilities.h"
#include "actBigNum.h"
#include "actECPoint.h"
#include "actECLengthInfo.h"

#if (VSECPRIM_ACTECTOOLS_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Constants
 ***************************************************************************/

#define actTAG_SEQUENCE                                               (0x30)
#define actTAG_INTEGER                                                (0x02)
#define actTAG_OCTETSTRING                                            (0x04)

/****************************************************************************
 ** Globals
 ***************************************************************************/

#define VSECPRIM_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

VSECPRIM_ROM(VSECPRIM_LOCAL, actU8) PRIME_OID_TLV[] = { 0x06u, 0x07u, 0x2au, 0x86u, 0x48u, 0xceu, 0x3du, 0x01u, 0x01u };

#define VSECPRIM_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 ** Local functions
 ***************************************************************************/

#define checkPrimeOID(domain)                                         \
                                                                      actMemcmp_ROM ((domain), PRIME_OID_TLV, sizeof (PRIME_OID_TLV))

/*---------------------------------------------------------------------------
 -- This implementation shouldn't be considered "good coding style".
 -- It just circumvents pitfalls induced by some braindead compiler
 -- (namely Microchip PIC18).
 --
 --   static int checkPrimeOID (actPROMU8 domain) {
 --      return ! (*domain++ == 0x06 &&
 --                *domain++ == 0x07 &&
 --                *domain++ == 0x2A &&
 --                *domain++ == 0x86 &&
 --                *domain++ == 0x48 &&
 --                *domain++ == 0xCE &&
 --                *domain++ == 0x3D &&
 --                *domain++ == 0x01 &&
 --                *domain++ == 0x01;)
 --   }
 --
 --------------------------------------------------------------------------*/

/* TLV object macro to parse Tag-Length */
VSECPRIM_LOCAL_FUNC(int) actParseTL(actPROMU8 input, VSECPRIM_P2VAR_PARA(int) value_length)
{
   if (ACT_ROM_R_BYT(input[1]) < 0x80)
   {
     if (value_length != 0)
     {
        *value_length = ACT_ROM_R_BYT(input[1]);
     }
     return 2;
   }
   else if (ACT_ROM_R_BYT(input[1]) == 0x81)
   {
     if (value_length != 0)
     {
        *value_length = ACT_ROM_R_BYT(input[2]);
     }
     return 3;
   }
   else if (ACT_ROM_R_BYT(input[1]) == 0x82)
   {
     if (value_length != 0)
     {
        *value_length = ((unsigned)ACT_ROM_R_BYT(input[2]) << 8) | ACT_ROM_R_BYT(input[3]);
     }
     return 4;
   }
   return 0;
}

/* TLV object macro to parse a field element octetstring */
VSECPRIM_LOCAL_FUNC(int) actParseTLVElement(
  actPROMU8 input, int element_length, VSECPRIM_P2VAR_PARA( actPROMU8 ) addr)
{
   int tmp = actParseTL(input, (VSECPRIM_P2VAR_PARA(int))NULL_PTR);
   if (addr != 0)
   {
     *addr = input + tmp;
   }
   return tmp + element_length;
}

/* TLV object macro to parse an integer */
VSECPRIM_LOCAL_FUNC(int) actParseTLVFieldPrime(
  actPROMU8 input, VSECPRIM_P2VAR_PARA( actPROMU8 ) addr, VSECPRIM_P2VAR_PARA(int) length)
{
   int tmp = actParseTL(input, length);
   while (ACT_ROM_R_BYT(input[tmp]) == 0)
   {
      ++tmp;
      (*length)--;
   }

   if (addr != 0)
   {
     *addr = input + tmp;
   }
   return tmp + (*length);
}

/* TLV object macro to parse a point */
VSECPRIM_LOCAL_FUNC(int) actParseTLVPoint(actPROMU8 input, int x_y_length, VSECPRIM_P2VAR_PARA(int) tag)
{
   int tmp = 0;
   tmp = actParseTL(input, (VSECPRIM_P2VAR_PARA(int))NULL_PTR);

   if (tag != 0)
   {
     *tag = ACT_ROM_R_BYT(input[tmp]);
   }

   switch (ACT_ROM_R_BYT(input[tmp]))
   {
     case 0x02:
     case 0x03:
     {
        tmp += x_y_length + 1;
        break;
     }
     case 0x04:
     case 0x06:
     case 0x07:
     {
       tmp += 2 * x_y_length + 1;
        break;
     }
     default:
     {
       tmp = 0;
        break;
     }
   }
   return tmp;
}

/****************************************************************************
 ** Global Functions
 ***************************************************************************/

/****************************************************************************
 **
 ** FUNCTION:
 **  actECParseDomain
 **
 **  Function to parse domain TLV object.
 **
 ** input:
 ** - domain:     domain parameter
 ** - p_bytes:    |
 ** - p_addr:     |
 ** - a_addr:     |  buffers to return extracted addresses or length values
 ** - n_bytes:    |
 ** - n_addr:     |
 **
 ** output:
 ** - p_bytes:    |
 ** - p_addr:     |
 ** - a_addr:     |  the extracted addresses or length values
 ** - n_bytes:    |
 ** - n_addr:     |
 ** - returns:    actEXCEPTION_DOMAIN       domain decoding error
 **               actOK                     else
 **
 ** assumes:
 **
 ** uses:
 **
 ***************************************************************************/

VSECPRIM_FUNC(actRETURNCODE) actECParseDomain(
   actPROMU8 domain,
   VSECPRIM_P2VAR_PARA( int ) p_bytes,
   VSECPRIM_P2VAR_PARA( actPROMU8 ) p_addr,
   VSECPRIM_P2VAR_PARA( actPROMU8 ) a_addr,
   VSECPRIM_P2VAR_PARA( int ) n_bytes,
   VSECPRIM_P2VAR_PARA( actPROMU8 ) n_addr)
{
   /* skip TAG sequence */
   if (ACT_ROM_BYT(domain) != actTAG_SEQUENCE)
   {
      return actEXCEPTION_DOMAIN;
   }
   else
   {
      domain += actParseTL(domain, (VSECPRIM_P2VAR_PARA(int))NULL_PTR);
   }

   /* skip version */
   if ((ACT_ROM_BYT(domain) != actTAG_INTEGER) || (ACT_ROM_BYT(domain + 1) != 0x01) || (ACT_ROM_BYT(domain + 2) != 0x01))
   {
      return actEXCEPTION_DOMAIN;
   }
   else
   {
      domain += 3;
   }

   /* skip TAG sequence */
   if (ACT_ROM_BYT(domain) != actTAG_SEQUENCE)
   {
      return actEXCEPTION_DOMAIN;
   }
   else
   {
      domain += actParseTL(domain, (VSECPRIM_P2VAR_PARA(int))NULL_PTR);
   }

   /* skip prime OID */
   if (checkPrimeOID(domain) == FALSE) /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
   {
      return actEXCEPTION_DOMAIN;
   }
   else
   {
      domain += sizeof(PRIME_OID_TLV);
   }

   /* parse prime p */
   if (ACT_ROM_BYT(domain) != actTAG_INTEGER)
   {
      return actEXCEPTION_DOMAIN;
   }
   else
   {
      domain += actParseTLVFieldPrime(domain, p_addr, p_bytes);
   }

   /* skip TAG sequence */
   if (ACT_ROM_BYT(domain) != actTAG_SEQUENCE)
   {
      return actEXCEPTION_DOMAIN;
   }
   else
   {
      domain += actParseTL(domain, (VSECPRIM_P2VAR_PARA(int))NULL_PTR);
   }

   /* parse coeff a */
   if (ACT_ROM_BYT(domain) != actTAG_OCTETSTRING)
   {
      return actEXCEPTION_DOMAIN;
   }
   else
   {
      domain += actParseTLVElement(domain, *p_bytes, a_addr);
   }

   /* skip coeff b */
   if (ACT_ROM_BYT(domain) != actTAG_OCTETSTRING)
   {
      return actEXCEPTION_DOMAIN;
   }
   else
   {
      domain += actParseTLVElement(domain, *p_bytes, (VSECPRIM_P2VAR_PARA(actPROMU8))NULL_PTR);
   }

   /* skip basepoint */
   if (ACT_ROM_BYT(domain) != actTAG_OCTETSTRING)
   {
      return actEXCEPTION_DOMAIN;
   }
   else
   {
      int length = actParseTLVPoint(domain, *p_bytes, (VSECPRIM_P2VAR_PARA(int))NULL_PTR);
      if (length == 0)
      {
         return actEXCEPTION_DOMAIN;
      }
      else
      {
         domain += length;
      }
   }

   /* parse prime n */
   if (ACT_ROM_BYT(domain) != actTAG_INTEGER)
   {
      return actEXCEPTION_DOMAIN;
   }
   else
   {
      actParseTLVFieldPrime(domain, n_addr, n_bytes);
   }

   return actOK;
}

/****************************************************************************
 **
 ** FUNCTION:
 **  actECInit
 **
 **  This function initializes the ECC workspace.
 **
 ** input:
 ** - domain:      domain parameter
 ** - domain_ext:  domain parameter extensions (Montgomery constants etc.)
 ** - speedup_ext: (optional) precomputations (for ECDSA-Sign, -GenKey)
 ** - algo_flag:   actEC_ALGO_FLAG_*
 ** - wksp:        workspace
 ** - wksp_len:    length of workspace in bytes
 **
 ** output:
 ** - wksp:       initialized workspace
 ** - returns:    actEXCEPTION_NULL         if an input parameter is NULL
 **               actEXCEPTION_DOMAIN       domain decoding error
 **               actEXCEPTION_DOMAIN_EXT   domain_ext decoding error
 **               actEXCEPTION_SPEEDUP_EXT  speedup_ext decoding error
 **               actEXCEPTION_MEMORY       wksp_len to small
 **               actOK                     else
 **
 ** assumes:
 **
 ** uses:
 **
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECInit(
   actPROMU8 domain, actPROMU8 domain_ext, actPROMU8 speedup_ext, int algo_flag,
   VSECPRIM_P2VAR_PARA(actU8) wksp, int wksp_len)
{
   VSECPRIM_P2VAR_PARA(actECCURVE) Curve =
      (VSECPRIM_P2VAR_PARA(actECCURVE))wksp;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) wksp_ptr =
      (VSECPRIM_P2VAR_PARA(actBNDIGIT))(wksp + sizeof(actECCURVE));
   VSECPRIM_P2VAR_PARA(actBNRING) p_field = &Curve->p_field;
   VSECPRIM_P2VAR_PARA(actBNRING) n_field = &Curve->n_field;
   int i, p_bytes, n_bytes, p_length, n_length;

   /* any null pointers ? */
   if ((!wksp) || (!domain) || (!domain_ext))
   {
      return actEXCEPTION_NULL;
   }
   /* parse domain parameter TLV object, the structure is:

      TLV(SEQUENCE) {
         TLV(INTEGER(version)),
         TLV(SEQUENCE) {
            TLV(OBJECT_IDENTIFIER(prime OID)),
            TLV(INTEGER(p))
         },
         TLV(SEQUENCE) {
            TLV(OCTETSTRING(a)),
            TLV(OCTETSTRING(b))
         },
         TLV(OCTETSTRING(G)),
         TLV(INTEGER(n)),
         TLV(INTEGER(h))
      }
   */
   {
      actRETURNCODE result;
      actPROMU8 p_addr, a_addr, n_addr;
      int max_length;

      result = actECParseDomain(domain, &p_bytes, &p_addr, &a_addr, &n_bytes, &n_addr);
      if (result != actOK)
      {
        return result;
      }
      p_length = actBNGetDigitLength(p_bytes);
      n_length = actBNGetDigitLength(n_bytes);
      max_length = actMax(p_length, n_length);

      /* check workspace length */
      switch (algo_flag)
      {
         case actEC_ALGO_FLAG_KG:
         {
            max_length = actECKgWksp(p_length, n_length);
            break;
         }
         case actEC_ALGO_FLAG_SP:
         {
            max_length = actECDSASpWksp(p_length, n_length);
            break;
         }
         case actEC_ALGO_FLAG_VP:
         {
            max_length = actECDSAVpWksp(p_length, n_length);
            break;
         }
         case actEC_ALGO_FLAG_DH_PRIM:
         {
            max_length = actECDHWksp(p_length, n_length);
            break;
         }
         case actEC_ALGO_FLAG_DH:
         {
            max_length = actECDHWksp(p_length, n_length) + p_length;
            break;
         }
         case actEC_ALGO_FLAG_BDKA:
         {
             max_length = actECBDWksp(p_length, n_length);
             break;
         }
         default:
         {
            max_length = 0;
            break;
         }
      }
      if ((wksp_len - (int)sizeof(actECCURVE)) < (max_length * (int)actBN_BYTES_PER_DIGIT))
      {
         return actEXCEPTION_MEMORY;
      }
      max_length = actMax(p_length, n_length);

      /* assign temporary bignum workspace for p_field and n_field */
      p_field->m   = wksp_ptr;              wksp_ptr += p_length;
      p_field->RR  = wksp_ptr;              wksp_ptr += p_length;
      n_field->m   = wksp_ptr;              wksp_ptr += n_length;
      n_field->RR  = wksp_ptr;              wksp_ptr += n_length;

      /* assign temporary bignum workspace used for point operations etc. */
      for (i = 0; i < (int)actEC_BIGNUM_TMP; ++i)
      {
         Curve->t[i] = wksp_ptr;            wksp_ptr += (max_length + 1);
      }

      /* prime and order field initialization */

      /* initialize prime and order size values */
      p_field->m_length = p_length;
      p_field->m_byte_length = p_bytes;
      n_field->m_length = n_length;
      n_field->m_byte_length = n_bytes;

      /* init prime p */
      actBNSetOctetStringROM(p_field->m, p_length, (actPROMU8)p_addr, p_bytes);

      /* check if a = p - 3 */
      {
         actBNDIGIT* p_minus_3 = Curve->t[0];
         actBNDIGIT* a = Curve->t[1];

         actBNSetOctetStringROM(a, p_length, (actPROMU8)a_addr, p_bytes);
         actBNSetZero(p_minus_3, p_length);
         p_minus_3[0] = 3;
         actBNSub(p_field->m, p_minus_3, p_minus_3, p_length);
         if (actBNCompare(a, p_minus_3, p_length) == actCOMPARE_EQUAL)
         {
            Curve->a_equals_p_minus_3 = 1;
         }
         else
         {
            Curve->a_equals_p_minus_3 = 0;
         }
      }

      /* init prime n */
      actBNSetOctetStringROM(n_field->m, n_length, (actPROMU8)n_addr, n_bytes);
   }

   /* parse domain parameter extensions TLV object, the structure is:

         TLV(SEQUENCE) {
            TLV(SEQUENCE) {
               TLV(OCTETSTRING(p_RR)),
               TLV(OCTETSTRING(p_bar)),
               TLV(SEQUENCE) {
                  TLV(OCTETSTRING(a_R)),
                  TLV(OCTETSTRING(b_R))
               },
               TLV(OCTETSTRING(G_R)),
            }
            TLV(SEQUENCE) {
               TLV(OCTETSTRING(n_RR)),
               TLV(OCTETSTRING(n_bar)),
            }
         }
   */
   {
      /* skip sequence TAGs */
      if (ACT_ROM_BYT(domain_ext) != actTAG_SEQUENCE)
      {
         return actEXCEPTION_DOMAIN_EXT;
      }
      else
      {
         domain_ext += actParseTL(domain_ext, (VSECPRIM_P2VAR_PARA(int))NULL_PTR);
         if (ACT_ROM_BYT(domain_ext) != actTAG_SEQUENCE)
         {
            return actEXCEPTION_DOMAIN_EXT;
         }
         else
         {
            domain_ext += actParseTL(domain_ext, (VSECPRIM_P2VAR_PARA(int))NULL_PTR);
         }
      }

      /* parse p_RR */
      if (ACT_ROM_BYT(domain_ext) != actTAG_OCTETSTRING)
      {
         return actEXCEPTION_DOMAIN_EXT;
      }
      else
      {
         domain_ext += actParseTLVElement(domain_ext, p_bytes, (VSECPRIM_P2VAR_PARA(actPROMU8))NULL_PTR);
         actBNSetOctetStringROM(p_field->RR, p_length, domain_ext - p_bytes, p_bytes);
      }

      /* parse p_bar */
      if (ACT_ROM_BYT(domain_ext) != actTAG_OCTETSTRING)
      {
         return actEXCEPTION_DOMAIN_EXT;
      }
      else
      {
         domain_ext += actParseTLVElement(domain_ext, actBN_BYTES_PER_DIGIT, (VSECPRIM_P2VAR_PARA(actPROMU8))NULL_PTR);
         actBNSetOctetStringROM(&(p_field->m_bar), 1, domain_ext - actBN_BYTES_PER_DIGIT, actBN_BYTES_PER_DIGIT);
      }

      /* skip sequence TAG */
      if (ACT_ROM_BYT(domain_ext) != actTAG_SEQUENCE)
      {
         return actEXCEPTION_DOMAIN_EXT;
      }
      else
      {
         domain_ext += actParseTL(domain_ext, (VSECPRIM_P2VAR_PARA(int))NULL_PTR);
      }

      /* parse a_R */
      if (ACT_ROM_BYT(domain_ext) != actTAG_OCTETSTRING)
      {
         return actEXCEPTION_DOMAIN_EXT;
      }
      else
      {
         domain_ext += actParseTLVElement(domain_ext, p_bytes, (VSECPRIM_P2VAR_PARA(actPROMU8))NULL_PTR);
         Curve->a_R = domain_ext - p_bytes;
      }

      /* parse b_R */
      if (ACT_ROM_BYT(domain_ext) != actTAG_OCTETSTRING)
      {
         return actEXCEPTION_DOMAIN_EXT;
      }
      else
      {
         domain_ext += actParseTLVElement(domain_ext, p_bytes, (VSECPRIM_P2VAR_PARA(actPROMU8))NULL_PTR);
         Curve->b_R = domain_ext - p_bytes;
      }

      /* parse G_R */
      if (ACT_ROM_BYT(domain_ext) != actTAG_OCTETSTRING)
      {
         return actEXCEPTION_DOMAIN_EXT;
      }
      else
      {
         int tag, length = actParseTLVPoint(domain_ext, p_bytes, &tag);
         if ((length == 0) || (tag < 0x04))
         {
            return actEXCEPTION_DOMAIN_EXT;
         }
         else
         {
            domain_ext += length;
            Curve->G_R = domain_ext - (2 * p_bytes);
         }
      }

      /* skip sequence TAG */
      if (ACT_ROM_BYT(domain_ext) != actTAG_SEQUENCE)
      {
         return actEXCEPTION_DOMAIN_EXT;
      }
      else
      {
         domain_ext += actParseTL(domain_ext, (VSECPRIM_P2VAR_PARA(int))NULL_PTR);
      }

      /* parse n_RR */
      if (ACT_ROM_BYT(domain_ext) != actTAG_OCTETSTRING)
      {
         return actEXCEPTION_DOMAIN_EXT;
      }
      else
      {
         domain_ext += actParseTLVElement(domain_ext, n_bytes, (VSECPRIM_P2VAR_PARA(actPROMU8))NULL_PTR);
         actBNSetOctetStringROM(n_field->RR, n_length, domain_ext - n_bytes, n_bytes);
      }

      /* parse n_bar */
      if (ACT_ROM_BYT(domain_ext) != actTAG_OCTETSTRING)
      {
         return actEXCEPTION_DOMAIN_EXT;
      }
      else
      {
         domain_ext += actParseTLVElement(domain_ext, actBN_BYTES_PER_DIGIT, (VSECPRIM_P2VAR_PARA(actPROMU8))NULL_PTR);
         actBNSetOctetStringROM(&(n_field->m_bar), 1, domain_ext - actBN_BYTES_PER_DIGIT, actBN_BYTES_PER_DIGIT);
      }
   }

   /*
      parse precomputation TLV object, the structure is:

         TLV(SEQUENCE) {
            TLV(OCTETSTRING(groups)),
            TLV(SEQUENCE) {
               TLV(OCTETSTRING(2*G_R)),
               ...
               [2^groups - 1 precomputed points and correction point D_R]
               ...
               TLV(OCTETSTRING(D_R))
            },
         }
   */

   if (speedup_ext != 0)
   {
      int num_of_points;

      /* skip sequence TAG */
      if (ACT_ROM_BYT(speedup_ext) != actTAG_SEQUENCE)
      {
         return actEXCEPTION_SPEEDUP_EXT;
      }
      else
      {
         speedup_ext += actParseTL(speedup_ext, (VSECPRIM_P2VAR_PARA(int))NULL_PTR);
      }

      /* parse groups parameter */
      if (ACT_ROM_BYT(speedup_ext) != actTAG_OCTETSTRING)
      {
         return actEXCEPTION_SPEEDUP_EXT;
      }
      else
      {
         speedup_ext += actParseTLVElement(speedup_ext, 1, (VSECPRIM_P2VAR_PARA(actPROMU8))NULL_PTR);
         Curve->groups = ACT_ROM_BYT(speedup_ext-1);
         if (Curve->groups < 2)
         {
           return actEXCEPTION_SPEEDUP_EXT;
         }
      }

      /* skip sequence TAG */
      if (ACT_ROM_BYT(speedup_ext) != actTAG_SEQUENCE)
      {
         return actEXCEPTION_SPEEDUP_EXT;
      }
      else
      {
         speedup_ext += actParseTL(speedup_ext, (VSECPRIM_P2VAR_PARA(int))NULL_PTR);
      }

      /* parse points */
      num_of_points = (1 << Curve->groups);
      for (i = 0; i < num_of_points; ++i)
      {
         if (ACT_ROM_BYT(speedup_ext) != actTAG_OCTETSTRING)
         {
            return actEXCEPTION_SPEEDUP_EXT;
         }
         else
         {
            int tag, length = actParseTLVPoint(speedup_ext, p_bytes, &tag);
            if ((length == 0) || (tag < 0x04))
            {
               return actEXCEPTION_SPEEDUP_EXT;
            }
            else
            {
              if (i == 0)
              {
                Curve->prec_first = speedup_ext;
              }
              speedup_ext += length;
            }
         }
      }
   }

   /*
      additional settings
   */

#if (actBN_MONT_MUL_VERSION==1)
   /* initialize prime and order structure switch */
   p_field->prime_structure = 0u;
   n_field->prime_structure = 0u;
#endif

   /* initialize window size for modular exponentiation */
#if (actBN_MOD_EXP_WINDOW_SIZE==0)   /* variable k-bit window algorithm */
   p_field->window_size = actEC_MOD_EXP_WINDOW_SIZE;
   n_field->window_size = actEC_MOD_EXP_WINDOW_SIZE;
#endif

   return actOK;
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTECTOOLS_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actECTools.c
 *********************************************************************************************************************/
