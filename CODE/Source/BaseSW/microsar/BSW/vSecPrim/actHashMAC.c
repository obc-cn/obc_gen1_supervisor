/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actHashMAC.c
 *        \brief  Interface for SHA-1 based Hash MAC algorithm.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library actCLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/
#define ACTHASHMAC_SOURCE

#include "actIHashMAC.h"
#include "actUtilities.h"

#if (VSECPRIM_ACTHASHMAC_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTION:
 ** actRETURNCODE actHashMACInit(actHASHMACSTRUCT* info),
 **                              const actU8* key,
 **                              int key_length,
 **                              void (*watchdog) (void))
 **
 **  This function initializes the HashMAC algorithm.
 **
 ** input:
 ** - info:       pointer to context structure
 ** - key:        MAC key
 ** - key_length: length of key in bytes
 ** - watchdog:   pointer to watchdog reset function
 **
 ** output:
 ** - info:       initialized context structure
 ** - returns:    actEXCEPTION_LENGTH    if key_len < 1
 **               actOK                  else
 **
 ** assumes:
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actHashMACInit(
   VSECPRIM_P2VAR_PARA(actHASHMACSTRUCT) info,
   VSECPRIM_P2CONST_PARA(actU8) key, actLengthType key_length,
   VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void))
{
  VSECPRIM_P2VAR_PARA(actSHASTRUCT) sha = &info->sha; /* PRQA S 0781 */ /* MD_VSECPRIM_5.6 */
  VSECPRIM_P2VAR_PARA(actU8) tmp_block = info->sha.buffer;
  actRETURNCODE retVal = actOK;
  actU8 i;


  /* store key */
  if (key_length < 1u)
  {
    retVal = actEXCEPTION_LENGTH;
  }
  else if (key_length <= actHASH_BLOCK_SIZE_SHA)
  {
    /* copy key to key_buf */
    actMemcpy(info->key_buf, key, (actLengthType)key_length); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
    info->key_length = key_length;
  }
  else
  {
    /* use SHA-1(key) */
    (void)actSHAInit(sha);
    (void)actSHAUpdate(sha, key, key_length, watchdog);
    (void)actSHAFinalize(sha, info->key_buf, watchdog);
    info->key_length = actHASH_SIZE_SHA;
  }

  if (retVal == actOK)
  {
    /* ipad */
    actMemcpy(tmp_block, info->key_buf, (actLengthType)(info->key_length)); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
    actMemset(&tmp_block[info->key_length], 0, (actLengthType)(actHASH_BLOCK_SIZE_SHA - info->key_length)); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
    for (i = 0; i < actHASH_BLOCK_SIZE_SHA; ++i)
    {
      tmp_block[i] ^= (actU8) (0x36);
    }

    (void)actSHAInit(sha);
    (void)actSHAUpdate(sha, tmp_block, actHASH_BLOCK_SIZE_SHA, watchdog);
  }

  return retVal;
}

/****************************************************************************
 **
 ** FUNCTION:
 ** actRETURNCODE actHashMACUpdate(actHASHMACSTRUCT* info,
 **                   const actU8* dataIN,
 **                   int length,
 **                   void (*watchdog) (void)))
 **
 **  This function hashes the given data and can be called arbitrary
 **  often between an initialize and finalize of the HashMAC algorithm.
 **  Uses any data already in the actSHASTRUCT structure and leaves
 **  any partial data block there.
 **
 ** input:
 ** - info:       pointer to context structure
 ** - dataIN:     pointer to data to be hashed
 ** - length:     length of data in bytes
 ** - watchdog:   pointer to watchdog reset function
 **
 ** output:
 ** - info:       actualized context structure
 ** - returns:    actEXCEPTION_LENGTH   total input more than 2^64 - 1 bit
 **               actOK                 else
 **
 ** assumes:
 ** - actHashMACInit() is called once before calling this function
 ** - dataIN != NULL is a valid pointer
 ** - length >= 0
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actHashMACUpdate(
   VSECPRIM_P2VAR_PARA(actHASHMACSTRUCT) info,
   VSECPRIM_P2CONST_PARA(actU8) dataIN, actLengthType length,
   VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void))
{
  return actSHAUpdate(&info->sha, dataIN, length, watchdog);
}

/****************************************************************************
 **
 ** FUNCTION:
 ** actRETURNCODE actHashMACFinalize(actHASHMACSTRUCT* info,
 **                     actU8 hash[actHASH_SIZE_SHA]),
 **                     void (*watchdog) (void))
 **
 **  This function finalizes the HashMAC algorithm and
 **  delivers the hash value.
 **
 ** input:
 ** - info:       pointer to context structure
 ** - hash:       byte array to contain the hash value
 ** - watchdog:   pointer to watchdog reset function
 **
 ** output:
 ** - info:       finalized context structure
 ** - hash:       the final hash value,
 **                  (big endian of length actHASH_SIZE_SHA)
 ** - returns:    actOK allways
 **
 ** assumes:
 ** - actHashMACInit() is called once before calling this function
 **
 ** uses:
 ** - actHASH_SIZE_SHA
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actHashMACFinalize(
   VSECPRIM_P2VAR_PARA(actHASHMACSTRUCT) info, VSECPRIM_P2VAR_PARA(actU8) hash,
   VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void))
{
  VSECPRIM_P2VAR_PARA(actSHASTRUCT) sha = &info->sha; /* PRQA S 0781 */ /* MD_VSECPRIM_5.6 */
  VSECPRIM_P2VAR_PARA(actU8) tmp_block = info->sha.buffer;
  actU8 i;

  (void)actSHAFinalize(sha, hash, watchdog);

  /* opad */
  actMemcpy(tmp_block, info->key_buf, (actLengthType)(info->key_length)); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
  actMemset(&tmp_block[info->key_length], 0u, (actLengthType)(actHASH_BLOCK_SIZE_SHA - info->key_length)); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
  for (i = 0u; i < actHASH_BLOCK_SIZE_SHA; ++i)
  {
    tmp_block[i] ^= (actU8) (0x5c);
  }

  (void)actSHAInit(sha);
  (void)actSHAUpdate(sha, tmp_block, actHASH_BLOCK_SIZE_SHA, watchdog);
  (void)actSHAUpdate(sha, hash, actHASH_SIZE_SHA, watchdog);
  (void)actSHAFinalize(sha, hash, watchdog);

  return actOK;
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTHASHMAC_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actHashMAC.c
 *********************************************************************************************************************/
