/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file ESLib_RSA_V15_Dec.c
 *        \brief RSA V1.5 (Decryption) implementation.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/
#define ESLIB_RSA_V15_DEC_SOURCE

#include "ESLib.h"
#include "ESLib_types.h"
#include "ESLib_ASN_1.h"

#include "actIRSA.h"
#include "actUtilities.h"

#if (VSECPRIM_RSA_V15_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Local Functions
 ***************************************************************************/
# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTION:
 **  eslt_ErrorCode
 **  esl_decryptInternal(     eslt_WorkSpaceRSAdec *workSpace,
 **                            eslt_Length           cipherSize,
 **                      const eslt_Byte            *cipher,
 **                            eslt_Length          *messageSize,
 **                            eslt_Byte            *message
 **                            eslt_Byte             blockType)
 **
 ** input:
 ** - workSpace
 ** - cipherSize
 ** - cipher
 ** - blockType
 **
 ** output:
 ** - messageSize
 ** - message
 ** - errorCode:
 **      ESL_ERC_NO_ERRO
 **      ESL_ERC_WS_STATE_INVALID
 **      ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 **      ESL_ERC_RSA_CODE_OUT_OF_RANGE
 **      ESL_ERC_RSA_ENCODING_INVALID
 **      ESL_ERC_OUTPUT_SIZE_TOO_SHORT
 **
 ** assumes:
 ** - keyExponent  < keyPairModule
 ** - Module is a prime
 ***************************************************************************/
VSECPRIM_LOCAL_FUNC(eslt_ErrorCode) esl_decryptInternal(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec) workSpace,
                                                         eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
                                                         VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message, eslt_Byte blockType)
{

  eslt_ErrorCode returnValue;
  eslt_Length keyPairModuleSize;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader) wsHeaderV15;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec_prim) wsPRIM;
  VSECPRIM_P2VAR_PARA(eslt_Byte) messageBuf;
  eslt_Length i;

  returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* get underlying workspace pointer */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte)) workSpace;
    wsHeaderV15 = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader)) wsRAW;
    keyPairModuleSize = (eslt_Length) (((eslt_Length) wsRAW[ESL_WS_RSA_V15_ED_OFFSET_BUFFERLENGTH] << 8) + (wsRAW[ESL_WS_RSA_V15_ED_OFFSET_BUFFERLENGTH + 1]));
    wsPRIM = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec_prim)) (wsRAW + ESL_WS_RSA_V15_ED_OFFSET_WS_RSA_PRIM(keyPairModuleSize));
    messageBuf = wsRAW + ESL_WS_RSA_V15_ED_OFFSET_BUFFER;

    if (ESL_WST_ALGO_RSA != (wsHeaderV15->status & ESL_WST_M_ALGO))
    {
      returnValue = ESL_ERC_WS_STATE_INVALID;
    }
    else if (0 == (wsHeaderV15->status & ESL_WST_M_RUNNING))
    {
      returnValue = ESL_ERC_WS_STATE_INVALID;
    }
    else
    {
      i = keyPairModuleSize;
      returnValue = esl_decryptRSA_prim(wsPRIM, cipherSize, cipher, &i, messageBuf);
      if (ESL_ERC_NO_ERROR == returnValue)
      {
        if ((i != keyPairModuleSize) || (ESL_ERC_NO_ERROR != esl_verifyPaddingRSAEM_V15(messageBuf, &i, blockType)))
        {
          returnValue = ESL_ERC_RSA_ENCODING_INVALID;
        }
        else
        {
          *messageSize = (eslt_Length) (keyPairModuleSize - i);
          actMemcpy(message, messageBuf + (keyPairModuleSize - *messageSize), (unsigned int)*messageSize); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
        }
      }
    }
  }
  return returnValue;
}

/****************************************************************************
 ** Global Functions
 ***************************************************************************/

/****************************************************************************
 * esl_initDecryptRSA_V15
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptRSA_V15(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec) workSpace,
                                                     eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule, eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent)
{

  eslt_ErrorCode returnValue;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader) wsHeaderV15;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec_prim) wsPRIM;

  returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* get underlying workspace pointer */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte)) workSpace;
    wsHeaderV15 = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader)) wsRAW;
    wsPRIM = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec_prim)) (wsRAW + ESL_WS_RSA_V15_ED_OFFSET_WS_RSA_PRIM(keyPairModuleSize));

    if (wsHeaderV15->size < (ESL_SIZEOF_WS_RSA_V15_ED_V15OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)))
    {
      returnValue = ESL_ERC_WS_TOO_SMALL;
    }
    else
    {
      wsPRIM->header.size = (eslt_Length) (wsHeaderV15->size - (ESL_SIZEOF_WS_RSA_V15_ED_V15OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)));
      wsPRIM->header.watchdog = wsHeaderV15->watchdog;
      returnValue = esl_initDecryptRSA_prim(wsPRIM, keyPairModuleSize, keyPairModule, privateKeyExponentSize, privateKeyExponent);
    }
    if (ESL_ERC_NO_ERROR == returnValue)
    {
      wsHeaderV15->status = (ESL_WST_ALGO_RSA | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);
      /* store message buffer length in workspace */
      wsRAW[ESL_WS_RSA_V15_ED_OFFSET_BUFFERLENGTH] = (eslt_Byte) (keyPairModuleSize >> 8);
      wsRAW[ESL_WS_RSA_V15_ED_OFFSET_BUFFERLENGTH + 1] = (eslt_Byte) (keyPairModuleSize);
    }
  }
  return returnValue;
}

/****************************************************************************
 * esl_decryptPubRSA_V15
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptPubRSA_V15(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec) workSpace,
                                                    eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
                                                    VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message)
{
  /* Cipher was encrypted using private key */
  return esl_decryptInternal(workSpace, cipherSize, cipher, messageSize, message, ASN1_PADDING_BLOCK_TYPE_PRIVATE);
}

/****************************************************************************
 * esl_decryptPrivRSA_V15
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptPrivRSA_V15(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec) workSpace,
                                                     eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
                                                     VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message)
{
  /* Cipher was encrypted using public key */
  return esl_decryptInternal(workSpace, cipherSize, cipher, messageSize, message, ASN1_PADDING_BLOCK_TYPE_PUBLIC);
}

/****************************************************************************
 * esl_decryptRSA_V15
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSA_V15(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec) workSpace,
                                                 eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher, VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message)
{
  /* Default uses private key for decryption */
  return esl_decryptPrivRSA_V15(workSpace, cipherSize, cipher, messageSize, message);
}

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_RSA_V15_ENABLED == STD_ON) */
/********************************************************************************************************************** 
 *  END OF FILE: ESLib_RSA_V15_Dec.c 
 *********************************************************************************************************************/
