/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actECPoint.c
 *        \brief  Implementation file for actECPoint.h
 *
 *      \details This file is part of the embedded systems library cvActLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

/****************************************************************************
 **
 ** This file contains: Implementation file for actECPoint.h
 **
 ** constants:
 **
 ** types:
 **
 ** macros:
 **
 ** functions:
 **   (local) actECPCopy
 **   actECPToMont
 **   actECPFromMont
 **   actECPGetAffineXYFromMont
 **   actECPDouble
 **   actECPAdd
 **   actECPSimMult
 **
 ***************************************************************************/

#include "actECPoint.h"


#if (VSECPRIM_ACTECPOINT_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */


#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Local functions
 ***************************************************************************/

/****************************************************************************
 **
 ** FUNCTIO:
 **  actECPCopy
 **
 ** This macro copies Q2 into Q1.
 **
 ** input:                                              (length in digits)
 ** - Q1:       destination point                           3*p_length
 ** - Q2:       source point                            (3/2)*p_length
 ** - Curve: the underlying curve
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->RR:       constant for Montgomery     p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 **
 ** output:
 ** - Q1:       Q1 equal to Q2                              3*p_length
 **
 ** assumes:
 ** - Q1 is projective (!Q1->is_affine)
 **
 ** remark:
 **
 ***************************************************************************/
VSECPRIM_LOCAL_FUNC(void) actECPCopy(
   VSECPRIM_P2VAR_PARA(actECPOINT) Q1, VSECPRIM_P2CONST_PARA(actECPOINT) Q2,
   VSECPRIM_P2VAR_PARA(actECCURVE) Curve)
{
   actLengthType p_length = Curve->p_field.m_length;

   actBNCopy(Q1->x, Q2->x, p_length);
   actBNCopy(Q1->y, Q2->y, p_length);
   if (Q2->is_affine)
   {
      actBNSetZero(Q1->z, p_length); (Q1->z)[0] = (actBNDIGIT)1;  /* Q1->z = 1 */
      actBNMontMulCopy(Q1->z, Curve->p_field.RR, Curve->t[0], &Curve->p_field, VSECPRIM_FUNC_NULL_PTR);  /* Q1->z = R */
   }
   else
   {
      actBNCopy(Q1->z, Q2->z, p_length);
   }
   Q1->is_infinity = Q2->is_infinity;
}


/****************************************************************************
 ** Global Functions
 ***************************************************************************/
 
/****************************************************************************
 **
 ** FUNCTION:
 **   actECPAssign
 **
 **   Assign point Q2 to point Q1 (Q1 = Q2).
 **   The resulting point (Q1) will be in projective Montgomery representation.
 **
 ** input:                                              (length in digits)
 ** - Q1:       destination point                           3*p_length
 ** - Q2:       source point                            (3/2)*p_length
 ** - Curve: the underlying curve
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->RR:       constant for Montgomery     p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curve ID
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 **
 ** output:
 ** - Q1:       Q1 = Q2                                     3*p_length
 **
 ** assumes:
 ** - Q2 is in Montgomery representation (affine OR projective).
 ** - Q1 is in projective Montgomery representation.
 **
 ** remark:
 **
 ***************************************************************************/
VSECPRIM_FUNC( void ) actECPAssign(VSECPRIM_P2VAR_PARA( actECPOINT ) Q1, 
                                          VSECPRIM_P2CONST_PARA( actECPOINT ) Q2,
                                          VSECPRIM_P2VAR_PARA( actECCURVE ) Curve )
{
  VSECPRIM_P2VAR_PARA(actBNRING) p = &Curve->p_field;

    Q1->is_affine   = 0;
    Q1->is_infinity = Q2->is_infinity;

    actBNCopy (Q1->x, Q2->x, p->m_length);
    actBNCopy (Q1->y, Q2->y, p->m_length);

   if ( Q2->is_affine )
   {
      actBNSetOne       (Q1->z, p->m_length);                   /* Q1->Z = 1 */
      actBNMontMulCopy  (Q1->z, p->RR, Curve->t[0], p, VSECPRIM_FUNC_NULL_PTR);   /* Q1->Z = R */
   }
   else
   {
     actBNCopy( Q1->z, Q2->z, p->m_length);
   }
}


/****************************************************************************
 **
 ** FUNCTION:
 **  void actECPToMont(actECPOINT *Q,
 **                    actECCURVE *Curve)
 **
 **  This function converts a point from normal to Montgomery
 **  Representation:   (X, Y, Z) -> (XR, YR, ZR)
 **
 ** input:                                              (length in digits)
 ** - Q:     the point (X, Y, Z) to be converted            3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->RR:       constant for Montgomery     p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 **
 ** output:
 ** - Q:     the converted point (XR, YR, ZR)               3*p_length
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for a complete initialization is the
 **   actECDSAVerify(..) implementation in actIECDSA.c.
 **
 ** remark:
 ** - Q->is_affine is not touched by this routine
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actECPToMont(VSECPRIM_P2VAR_PARA(actECPOINT) Q, VSECPRIM_P2VAR_PARA(actECCURVE) Curve)
{
   VSECPRIM_P2VAR_PARA(actBNRING)  p_field  = &Curve->p_field;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) RR       = p_field->RR;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) tmp      = Curve->t[0];

   /* X -> XR */
   actBNMontMulCopy(Q->x, RR, tmp, p_field, VSECPRIM_FUNC_NULL_PTR);
   /* Y -> YR */
   actBNMontMulCopy(Q->y, RR, tmp, p_field, VSECPRIM_FUNC_NULL_PTR);
   /* Z -> ZR */
   if (!Q->is_affine) { actBNMontMulCopy(Q->z, RR, tmp, p_field, VSECPRIM_FUNC_NULL_PTR); }
}

/****************************************************************************
 **
 ** FUNCTION:
 **  actECPToAffineFromMont
 **
 **  Calculates the affine representation of the actPoint Q, where Q is in
 **  Montgomery Representation (XR, YR, ZR)
 **
 ** input:                                              (length in digits)
 ** - Q:     the projective point (XR, YR, ZR) in           3*p_length
 **          Montgomery representation
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->RR:       constant for Montgomery     p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** -        p_field->window_size for modular exponentiation
 ** - Curve->t[0..(i-1)]:  i temporary BigNum variables,   i*(p_length+1)
 **                        where i = actEC_BIGNUM_TMP
 ** - flag:  Output control flag; if zero the point will be checked
 **          to be on curve and transformed from Montgomery to normal
 **          representation (used in protocol functions, e.g. ECDSASign).
 **
 ** output:
 ** - Q:  flag==0:   the validated affine point (X, Y)      2*p_length
 **                  [validation means ]
 **       flag!=0:   the affine point (XR, YR) in Montgomery
 **                  Representation                         2*p_length
 ** - returns: 0  if !flag and Q is not on curve (internal error/attack)
 **            1  else
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for a complete initialization is the
 **   actECDSAVerify(..) implementation in actIECDSA.c.
 ** - Q != Infinity is in Montgomery Representation (XR, YR, ZR)
 **
 ***************************************************************************/
VSECPRIM_FUNC(int) actECPToAffineFromMont(
   VSECPRIM_P2VAR_PARA(actECPOINT) Q, VSECPRIM_P2VAR_PARA(actECCURVE) Curve, int flag)
{
   VSECPRIM_P2VAR_PARA(actBNRING) p_field  = &Curve->p_field;
   actLengthType p_length = p_field->m_length;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) t0    = Curve->t[0];
   VSECPRIM_P2VAR_PARA(actBNDIGIT) t1    = Curve->t[1];
   VSECPRIM_P2VAR_PARA(actBNDIGIT) Z_inv = Curve->t[actEC_BIGNUM_TMP - 1];

   /* calculate x-coordinate */

   /* Z_inv = ZR^-1 */
   if ((actBNIsZero(Q->z, p_length) == TRUE) || (Q->is_infinity)) { return 0; }
   actBNFieldInversion(Q->z, Z_inv, p_field, Curve->t);
   /* t0 = ZR^-2 */
   actBNMontMul(Z_inv, Z_inv, t0, p_field, VSECPRIM_FUNC_NULL_PTR);
   /* XR = XR*ZR^-2 */
   actBNMontMulCopy(Q->x, t0, t1, p_field, VSECPRIM_FUNC_NULL_PTR);

   /* calculate y-coordinate */

   /* t1 = ZR^-3 */
   actBNMontMul(Z_inv, t0, t1, p_field, VSECPRIM_FUNC_NULL_PTR);
   /* YR = YR*ZR^-3 */
   actBNMontMulCopy(Q->y, t1, t0, p_field, VSECPRIM_FUNC_NULL_PTR);

   if (!flag)
   {
      /* check point */
      if (!actECPointIsOnCurve(Q, Curve)) { return 0; }
      /* transform from Montgomery */
      actBNSetZero(t1, p_length); t1[0] = (actBNDIGIT)1;  /* t1 = 1 */
      actBNMontMulCopy(Q->x, t1, t0, p_field, VSECPRIM_FUNC_NULL_PTR);
      actBNMontMulCopy(Q->y, t1, t0, p_field, VSECPRIM_FUNC_NULL_PTR);
   }
   else
   {
      /* set affine flag, the point may be used in further calculations */
      Q->is_affine = 1;
   }

   return 1;
}


/****************************************************************************
 **
 ** FUNCTION:
 **   actECPointIsOnCurve
 **
 **  This function checks if a point QR is on curve, where QR is in affine
 **  Montgomery Representation (XR, YR, R)
 **
 ** input:
 ** - QR:     the (affine) point to be checked              3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_byte_length: length of p in byte
 ** -        p_field->RR:       constant for Montgomery     p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 ** - Curve->t[1]:   temporary BigNum variable              p_length+1
 ** - Curve->t[2]:   temporary BigNum variable              p_length+1
 ** - Curve->t[3]:   temporary BigNum variable              p_length+1
 ** - Curve->a_R:    coefficient a of curve equation        p_bytes
 ** - Curve->b_R:    coefficient b of curve equation        p_bytes
 **
 ** output:
 ** - returns:    0     if the point is not on curve
 **               1     else
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for the usage of this function is the
 **   actECDSAVerify(.) implementation in actIECDSA.c, which is basically
 **   a call of actECDSAvp(.) with all previous initializations.
 ** - QR != Infinity is in affine Montgomery Representation (XR, YR, R)
 ** - curve equation coefficient a is equal to p-3 (is the case for
 **   supported curves of the ECC module)
 **
 ** uses:
 **
 ***************************************************************************/
VSECPRIM_FUNC(int) actECPointIsOnCurve(
   VSECPRIM_P2VAR_PARA(actECPOINT) Q, VSECPRIM_P2VAR_PARA(actECCURVE) Curve)
{
   if (Q->is_infinity)
   {
      return 0;
   }
   else
   {
      VSECPRIM_P2VAR_PARA(actBNRING) p_field = &Curve->p_field;
      actLengthType p_length = p_field->m_length;
      actLengthType p_bytes  = p_field->m_byte_length;
      VSECPRIM_P2VAR_PARA(actBNDIGIT) a_R    = Curve->t[0];
      VSECPRIM_P2VAR_PARA(actBNDIGIT) b_R    = Curve->t[1];
      VSECPRIM_P2VAR_PARA(actBNDIGIT) tmp1   = Curve->t[2];
      VSECPRIM_P2VAR_PARA(actBNDIGIT) tmp2   = Curve->t[3];

      /*
         We have to check:
            y^2 = x^3 + ax + b = x(x^2 + a) + b
      */

      /* a_R = a * R mod p */
      actBNSetOctetStringROM(a_R, p_length, Curve->a_R, p_bytes);
      /* b_R = b * R mod p */
      actBNSetOctetStringROM(b_R, p_length, Curve->b_R, p_bytes);
      /* tmp1 = Q_X^2 * R mod p */
      actBNMontMul(Q->x, Q->x, tmp1, p_field, VSECPRIM_FUNC_NULL_PTR);
      /* tmp1 = (Q_X^2 + a) * R mod p */
      actBNModAdd(tmp1, a_R, tmp1, p_field, VSECPRIM_FUNC_NULL_PTR);
      /* tmp2 = (Q_X^2 + a) * Q_X * R mod p */
      actBNMontMul(tmp1, Q->x, tmp2, p_field, VSECPRIM_FUNC_NULL_PTR);
      /* tmp1 = ((Q_X^2 + a) * Q_X + b) * R mod p */
      actBNModAdd(tmp2, b_R, tmp1, p_field, VSECPRIM_FUNC_NULL_PTR);

      /* tmp2 = Q_Y^2 * R mod p */
      actBNMontMul(Q->y, Q->y, tmp2, p_field, VSECPRIM_FUNC_NULL_PTR);

      /* compare tmp1 and tmp2 */
      if (actBNCompare(tmp1, tmp2, p_length) != actCOMPARE_EQUAL)
      {
         return 0;
      }
   }

   return 1;
}


/****************************************************************************
 **
 ** FUNCTION:
 **  void actECPDouble(actECPOINT *Q,
 **                    actECCURVE *Curve)
 **
 **  This routine doubles the projective point Q (Q *= 2).
 **  The in- and output point(s) is(are) in Montgomery Representation.
 **  Implementation Reference: IEEE 1363 / D 13, Algorithm A.10.4
 **
 **  Projective Doubling:    2 * (X_1, Y_1, Z_1) = (X_2, Y_2, Z_2) ,
 **  where                   M   = 3*(X_1^2) + A*(Z_1^4)
 **                          Z_2 = 2*Y_1 * Z_1
 **                          S   = 4*X_1 * (Y_1^2)
 **                          X_2 = (M^2) - 2*S
 **                          T   = 8*(Y_1^4)
 **                          Y_2 = M * (S - X_2) - T
 **
 ** input:                                              (length in digits)
 ** - Q:     the point (X_1*R, Y_1*R, Z_1*R)                3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 ** - Curve->t[1]:   temporary BigNum variable              p_length+1
 ** - Curve->t[2]:   temporary BigNum variable              p_length+1
 **
 ** output:
 ** - Q:     the point 2Q = (X_2*R, Y_2*R, Z_2*R)           3*p_length
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for a complete initialization is the
 **   actECDSAVerify(..) implementation in actIECDSA.c.
 ** - Q is in Montgomery Representation (XR, YR, ZR)
 ** - curve equation coefficient a is equal to p-3 (is the case for
 **   supported curves of the ECC module)
 **
 ** remark:
 ** - Q->is_affine is not touched by this routine
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actECPDouble(VSECPRIM_P2VAR_PARA(actECPOINT) Q, VSECPRIM_P2VAR_PARA(actECCURVE) Curve)
{
   VSECPRIM_P2VAR_PARA(actBNRING)  p_field = &Curve->p_field;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) result  = Curve->t[0];
   actLengthType p_length = p_field->m_length;

   /* t1 = X_1, t2 = Y_1, t3 = Z_1 */
   VSECPRIM_P2VAR_PARA(actBNDIGIT) t1 = Q->x;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) t2 = Q->y;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) t3 = Q->z;

   if (Q->is_infinity)
   {
      /* t3==0, Q==Infinity */
      return;
   }
   else if (actBNIsZero(t2, p_length) == TRUE)
   {
      /* t2==0, set Q=Infinity and return */
      Q->is_infinity = 1;
   }
   else
   {
      /* need variables t4 and t5 */
      VSECPRIM_P2VAR_PARA(actBNDIGIT) t4 = Curve->t[1];
      VSECPRIM_P2VAR_PARA(actBNDIGIT) t5 = Curve->t[2];

      if (Curve->a_equals_p_minus_3 != 0)
      {
         /* t4 = t3^2 */
         actBNMontMul(t3, t3, t4, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t5 = t1 - t4 */
         actBNModSub(t1, t4, t5, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t4 = t1 + t4 */
         actBNModAdd(t1, t4, t4, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t5 = t4 * t5 */
         actBNMontMul(t4, t5, result, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t4 = 3*t5 */
         actBNModAdd(result, result, t4, p_field, VSECPRIM_FUNC_NULL_PTR);
         actBNModAdd(t4, result, t4, p_field, VSECPRIM_FUNC_NULL_PTR);                /* = M */
      }
      else
      {
         /* t5 = t3^4 * A */
         actBNSetOctetStringROM(t4, p_length, Curve->a_R, Curve->p_field.m_byte_length); /* t4 = A */
         if (actBNIsZero(t4, p_length) == FALSE)
         {
            actBNMontMul(t3, t3, t5, p_field, VSECPRIM_FUNC_NULL_PTR);
            actBNMontMul(t5, t5, result, p_field, VSECPRIM_FUNC_NULL_PTR); /* result = t3^4 */
            actBNMontMul(result, t4, t5, p_field, VSECPRIM_FUNC_NULL_PTR); /* t5 = t3^4 * A */
         }
         else
         {
            actBNSetZero(t5, p_length); /* t5 = 0 if A==0 */
         }
         /* t4 = t1^2 */
         actBNMontMul(t1, t1, result, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t4 = 3*t4 */
         actBNModAdd(result, result, t4, p_field, VSECPRIM_FUNC_NULL_PTR);
         actBNModAdd(t4, result, t4, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t4 = t4 + t5 */
         actBNModAdd(t4, t5, t4, p_field, VSECPRIM_FUNC_NULL_PTR);
      }

      /* t3 = t2 * t3 */
      actBNMontMul(t2, t3, result, p_field, VSECPRIM_FUNC_NULL_PTR);
      /* t3 = 2*t3 */
      actBNModAdd(result, result, t3, p_field, VSECPRIM_FUNC_NULL_PTR); /* = Z_2 */
      /* t2 = t2^2 */
      actBNMontMulCopy(t2, t2, result, p_field, VSECPRIM_FUNC_NULL_PTR);
      /* t5 = t1 * t2 */
      actBNMontMul(t1, t2, t5, p_field, VSECPRIM_FUNC_NULL_PTR);
      /* t5 = 4*t5 */
      actBNModAdd(t5, t5, t5, p_field, VSECPRIM_FUNC_NULL_PTR);
      actBNModAdd(t5, t5, t5, p_field, VSECPRIM_FUNC_NULL_PTR); /* = S */
      /* t1 = t4^2 */
      actBNMontMul(t4, t4, result, p_field, VSECPRIM_FUNC_NULL_PTR);
      /* t1 = t1 - 2*t5 */
      actBNModSub(result, t5, t1, p_field, VSECPRIM_FUNC_NULL_PTR);
      actBNModSub(t1, t5, t1, p_field, VSECPRIM_FUNC_NULL_PTR); /* = X_2 */
      /* t2 = t2^2 */
      actBNMontMul(t2, t2, result, p_field, VSECPRIM_FUNC_NULL_PTR);
      /* t2 = 8*t2 */
      actBNModAdd(result, result, t2, p_field, VSECPRIM_FUNC_NULL_PTR);
      actBNModAdd(t2, t2, t2, p_field, VSECPRIM_FUNC_NULL_PTR);
      actBNModAdd(t2, t2, t2, p_field, VSECPRIM_FUNC_NULL_PTR); /* = T */
      /* t5 = t5 - t1 */
      actBNModSub(t5, t1, t5, p_field, VSECPRIM_FUNC_NULL_PTR);
      /* t5 = t5 * t4 */
      actBNMontMul(t5, t4, result, p_field, VSECPRIM_FUNC_NULL_PTR);
      /* t2 = t5 - t2 */
      actBNModSub(result, t2, t2, p_field, VSECPRIM_FUNC_NULL_PTR); /* = Y_2 */
   }
}


/****************************************************************************
 **
 ** FUNCTION:
 **  void actECPAdd(actECPOINT *Q1,
 **                 const actECPOINT *Q2,
 **                 actECCURVE *Curve)
 **
 **  This routine adds the affine point Q2 to Q1 (Q1 += Q2).
 **  The in- and output point(s) is(are) in Montgomery Representation.
 **  Implementation Reference: IEEE 1363 / D 13, Algorithm A.10.4
 **
 **  Projective Addition:    (X_0, Y_0, Z_0) + (X_1, Y_1, Z_1) = (X_2, Y_2, Z_2),
 **  where                   U_0 = X_0 * (Z_1^2)
 **                          S_0 = Y_0 * (Z_1^3)
 **                          U_1 = X_1 * (Z_0^2)
 **                          S_1 = Y_1 * (Z_0^3)
 **                          W   = U_0 - U_1
 **                          R   = S_0 - S_1
 **                          T   = U_0 + U_1
 **                          M   = S_0 + S_1
 **                          Z_2 = Z_0 * Z_1 * W
 **                          X_2 = (R^2) - T * (W^2)
 **                          V   = T * (W^2) - 2*X_2
 **                        2*Y_2 = V * R - M * (W^3)
 **
 ** input:                                              (length in digits)
 ** - Q1:    the point (X_0*R, Y_0*R, Z_0*R)                3*p_length
 ** - Q2:    the point (X_1*R, Y_1*R, Z_1*R)                3*p_length
 ** - Q2->is_affine has to be 0 if Q2 is not in affine representation
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 ** - Curve->t[1]:   temporary BigNum variable              p_length+1
 ** - Curve->t[2]:   temporary BigNum variable              p_length+1
 ** - Curve->t[3]:   temporary BigNum variable              p_length+1
 **
 ** output:
 ** - Q1:    the point Q1+Q2 = (X_2*R, Y_2*R, Z_2*R)        3*p_length
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for a complete initialization is the
 **   actECDSAVerify(..) implementation in actIECDSA.c.
 ** - Q1 is in Montgomery Representation (XR, YR, ZR)
 ** - Q2 != 0 is in Montgomery Representation (XR, YR, ZR)
 **
 ** remark:
 ** - Q1->is_affine is not touched by this routine
 ** - Q2->is_affine is not touched by this routine
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actECPAdd(
   VSECPRIM_P2VAR_PARA(actECPOINT) Q1, VSECPRIM_P2CONST_PARA(actECPOINT) Q2,
   VSECPRIM_P2VAR_PARA(actECCURVE) Curve)
{
   VSECPRIM_P2VAR_PARA(actBNRING)  p_field = &Curve->p_field;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) result  = Curve->t[0];
   actLengthType p_length = p_field->m_length;

   /* t1 = X_0, t2 = Y_0, t3 = Z_0 */
   VSECPRIM_P2VAR_PARA(actBNDIGIT) t1 = Q1->x;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) t2 = Q1->y;
   VSECPRIM_P2VAR_PARA(actBNDIGIT) t3 = Q1->z;

   if (Q2->is_infinity)
   {
      /* Q1 + Infinity = Q1, return */
      return;
   }
   else if (Q1->is_infinity)
   {
      /* Infinity + Q2 = Q2, set Q1=Q2 and return */
      actECPCopy(Q1, Q2, Curve);
   }
   else
   {
      /* need variables t4, t5, t6 and t7 */
      VSECPRIM_P2VAR_PARA(actBNDIGIT) t4 = Curve->t[1];
      VSECPRIM_P2VAR_PARA(actBNDIGIT) t5 = Curve->t[2];
      VSECPRIM_P2VAR_PARA(actBNDIGIT) t7 = Curve->t[3];
      VSECPRIM_P2VAR_PARA(actBNDIGIT) t6 = 0;

      /* t4 = X_1, t5 = Y_1 */
      actBNCopy(t4, Q2->x, p_length);
      actBNCopy(t5, Q2->y, p_length);

      if (!Q2->is_affine)
      {
         /* t6 = Z_1 */
         t6 = Q2->z;
         /* t7 = t6^2 */
         actBNMontMul(t6, t6, t7, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t1 = t1 * t7 */
         actBNMontMulCopy(t1, t7, result, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t7 = t7 * t6 */
         actBNMontMul(t7, t6, result, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t2 = t2 * t7 */
         actBNMontMulCopy(t2, result, t7, p_field, VSECPRIM_FUNC_NULL_PTR);
      }

      /* t7 = t3^2 */
      actBNMontMul(t3, t3, t7, p_field, VSECPRIM_FUNC_NULL_PTR);
      /* t4 = t4 * t7 */
      actBNMontMul(t4, t7, result, p_field, VSECPRIM_FUNC_NULL_PTR); /* = U_1 */
      /* t4 = t1 - t4 */
      actBNModSub(t1, result, t4, p_field, VSECPRIM_FUNC_NULL_PTR);  /* = W */
      /* t7 = t7 * t3 */
      actBNMontMulCopy(t7, t3, result, p_field, VSECPRIM_FUNC_NULL_PTR);
      /* t5 = t5 * t7 */
      actBNMontMul(t5, t7, result, p_field, VSECPRIM_FUNC_NULL_PTR); /* = S_1 */
      /* t5 = t2 - t5 */
      actBNModSub(t2, result, t5, p_field, VSECPRIM_FUNC_NULL_PTR);  /* = R */

      if (actBNIsZero(t4, p_length) == TRUE)
      {
         /* t4==0 */
         if (actBNIsZero(t5, p_length) == TRUE)
         {
            /* t5==0 means Q1==Q2, recover Q1 from Q2, double Q1 and return */
            actECPCopy(Q1, Q2, Curve);
            actECPDouble(Q1, Curve);
         }
         else
         {
            /* set Q1=Infinity and return */
            Q1->is_infinity = 1;
         }
      }
      else
      {
         /* t1 = 2*t1 - t4 */
         actBNModAdd(t1, t1, t1, p_field, VSECPRIM_FUNC_NULL_PTR);
         actBNModSub(t1, t4, t1, p_field, VSECPRIM_FUNC_NULL_PTR); /* = T */
         /* t2 = 2*t2 - t5 */
         actBNModAdd(t2, t2, t2, p_field, VSECPRIM_FUNC_NULL_PTR);
         actBNModSub(t2, t5, t2, p_field, VSECPRIM_FUNC_NULL_PTR); /* = M */

         if (!Q2->is_affine)
         {
            /* t3 = t3 * t6 */
            actBNMontMulCopy(t3, t6, result, p_field, VSECPRIM_FUNC_NULL_PTR);
         }

         /* t3 = t3 * t4 */
         actBNMontMulCopy(t3, t4, result, p_field, VSECPRIM_FUNC_NULL_PTR); /* = Z_2 */
         /* t7 = t4^2 */
         actBNMontMul(t4, t4, t7, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t4 = t4 * t7 */
         actBNMontMulCopy(t4, t7, result, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t7 = t7 * t1 */
         actBNMontMulCopy(t7, t1, result, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t1 = t5^2 */
         actBNMontMul(t5, t5, result, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t1 = t1 - t7 */
         actBNModSub(result, t7, t1, p_field, VSECPRIM_FUNC_NULL_PTR); /* = X_2 */
         /* t7 = t7 - 2*t1 */
         actBNModSub(t7, t1, t7, p_field, VSECPRIM_FUNC_NULL_PTR);
         actBNModSub(t7, t1, t7, p_field, VSECPRIM_FUNC_NULL_PTR); /* = V */
         /* t5 = t5 * t7 */
         actBNMontMulCopy(t5, t7, result, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t4 = t4 * t2 */
         actBNMontMul(t4, t2, result, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t2 = t5 - t4 */
         actBNModSub(t5, result, t2, p_field, VSECPRIM_FUNC_NULL_PTR);
         /* t2 = t2 / 2 */
         actBNModDiv2(t2, p_field, VSECPRIM_FUNC_NULL_PTR); /* = Y_2 */
      }
   }
}


/****************************************************************************
 **
 ** FUNCTION:
 **  void actECPSub(actECPOINT *Q1,
 **                 const actECPOINT *Q2,
 **                 actECCURVE *Curve)
 **
 **  This routine subtracts the point Q2 from Q1 (Q1 -= Q2).
 **  The in- and output points are in Montgomery Representation.
 **
 **  Subtraction is achieved by< negating the Y coordinate (Q2.y) and then adding the points.
 **  Q2.y is restored after addition (just negated once more).
 **
 ** input:                                              (length in digits)
 ** - Q1:    the point (X_0*R, Y_0*R, Z_0*R)                3*p_length
 ** - Q2:    the point (X_1*R, Y_1*R, Z_1*R)                3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - more info under -> actECPAdd
 **
 ** output:
 ** - Q1:    the point Q1-Q2 = (X_2*R, Y_2*R, Z_2*R)        3*p_length
 **
 ** assumes:
 ** - see actECPAdd
 **
 ** remark:
 ** - Q1->is_affine is not touched by this routine
 ** - Q2->is_affine is not touched by this routine
 **
 ***************************************************************************/
VSECPRIM_FUNC (void) actECPSub (VSECPRIM_P2VAR_PARA( actECPOINT ) Q1,
                                VSECPRIM_P2CONST_PARA( actECPOINT ) Q2,
                                VSECPRIM_P2VAR_PARA( actECCURVE ) Curve)
{
    VSECPRIM_P2VAR_PARA (actBNRING) p = &Curve->p_field;

    actBNModSub (p->m, Q2->y, Q2->y, p,     NULL_PTR);
    actECPAdd   (Q1,   Q2,           Curve);
    actBNModSub (p->m, Q2->y, Q2->y, p, NULL_PTR);
}


/****************************************************************************
 **
 ** FUNCTION:
 **  actECPMult
 **
 **  This routine calculates R = k*Q (scalar multiplication)
 **  The in- and output point(s) is(are) in Montgomery Representation.
 **
 ** input:                                              (length in digits)
 ** - Q:     the point to be multiplied with k              3*p_length
 ** - k:     scalar                                         n_length
 ** - R:     the point to store the result                  3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->n_field: the field GF(n):
 ** -        n_field->m_length: equals to n_length
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 ** - Curve->t[1]:   temporary BigNum variable              p_length+1
 ** - Curve->t[2]:   temporary BigNum variable              p_length+1
 ** - Curve->t[3]:   temporary BigNum variable              p_length+1
 **
 ** output:
 ** - R:    the point k*Q in Montgomery                     3*p_length
 **          Representation (XR, YR, ZR)
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for a complete initialization is the
 **   actECDSAVerify(..) implementation in actIECDSA.c.
 ** - Q is in affine Montgomery Representation (XR, YR, R)
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actECPMult(
   VSECPRIM_P2CONST_PARA(actECPOINT) Q, VSECPRIM_P2CONST_PARA(actBNDIGIT) k,
   VSECPRIM_P2VAR_PARA(actECPOINT) R, VSECPRIM_P2VAR_PARA(actECCURVE) Curve)
{
   actLengthType i = actBNGetBitLength(Curve->n_field.m, Curve->n_field.m_length);

   /* R = Infinity */
   R->is_affine   = 0;
   R->is_infinity = 1;

   /* Double-And-Add loop */
   for (; i > 0; --i)
   {
      /* R *= 2 */
      actECPDouble(R, Curve);
      /* test bit i of k */
      if (actBNGetBit(k, i - 1))
      {
         /* R += Q */
         actECPAdd(R, Q, Curve);
      }
   }
}

/****************************************************************************
**
** FUNCTION:
**  actECPMultShort
**
**  This routine calculates R = k*Q (scalar multiplication)
**  The in- and output point(s) is(are) in Montgomery Representation.
**
** input:                                              (length in digits)
** - Q:     the point to be multiplied with k              3*p_length
** - k:     scalar                                         n_length
** - R:     the point to store the result                  3*p_length
** - Curve: the underlying curve (see assumptions)
** - Curve->p_field: the underlying field GF(p):
** -        p_field->m:        the modulus p               p_length
** -        p_field->m_length: equals to p_length
** -        p_field->m_bar:    constant for Montgomery     1
** -        p_field->prime_structure==curveid
** - Curve->n_field: the field GF(n):
** -        n_field->m_length: equals to n_length
** - Curve->t[0]:   temporary BigNum variable              p_length+1
** - Curve->t[1]:   temporary BigNum variable              p_length+1
** - Curve->t[2]:   temporary BigNum variable              p_length+1
** - Curve->t[3]:   temporary BigNum variable              p_length+1
**
** output:
** - R:    the point k*Q in Montgomery                     3*p_length
**          Representation (XR, YR, ZR)
**
** assumes:
** - The actECCURVE structure parameter 'Curve' holds all necessary
**   information and the workspace. It has to be initialized as far as
**   listed above. Please have a look at the actECCURVE definition in
**   actECPoint.h; an example for a complete initialization is the
**   actECDSAVerify(..) implementation in actIECDSA.c.
** - Q is in affine Montgomery Representation (XR, YR, R)
**
***************************************************************************/
VSECPRIM_FUNC( void ) actECPMultShort (VSECPRIM_P2CONST_PARA( actECPOINT ) Q,
                                        const actU32      k,
                                        VSECPRIM_P2VAR_PARA( actECPOINT ) R,
                                        VSECPRIM_P2VAR_PARA( actECCURVE ) curve)
{
   actU32   mask = 0x80000000;

   /* R = Infinity */
   R->is_affine   = 0;
   R->is_infinity = 1;

   /* Double-And-Add loop */
   while (0 != mask) {
      /* R *= 2 */
      actECPDouble (R, curve);

      /* test bit i of k */
      if (mask & k) {
         /* R += Q */
         actECPAdd (R, Q, curve);
      }

      mask >>= 1;
   }
}

/****************************************************************************
 **
 ** FUNCTION:
 **  void actECPMultG(const actBNDIGIT *k,
 **                   actECPOINT *Prec,
 **                   actECPOINT *R,
 **                   actECCURVE *Curve)
 **
 **  This routine calculates R = k*G (scalar multiplication of basepoint).
 **  The in- and output point(s) is(are) in Montgomery Representation.
 **
 ** input:                                              (length in digits)
 ** - k:     scalar                                         n_length
 ** - Prec:  temporary point to store a precomputed point   3*p_length
 ** - R:     the point to store the result                  3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_byte_length: length of p in byte
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->n_field: the field GF(n):
 ** -        n_field->m_length: equals to n_length
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 ** - Curve->t[1]:   temporary BigNum variable              p_length+1
 ** - Curve->t[2]:   temporary BigNum variable              p_length+1
 ** - Curve->t[3]:   temporary BigNum variable              p_length+1
 ** - Curve->prec_info:   precomputation information
 **
 ** output:
 ** - R:    the point k*G in Montgomery                     3*p_length
 **          Representation (XR, YR, ZR)
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for a complete initialization is the
 **   actECDSAVerify(..) implementation in actIECDSA.c.
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actECPMultG(
   VSECPRIM_P2CONST_PARA(actBNDIGIT) k, VSECPRIM_P2VAR_PARA(actECPOINT) Prec,
   VSECPRIM_P2VAR_PARA(actECPOINT) R, VSECPRIM_P2VAR_PARA(actECCURVE) Curve)
{
   VSECPRIM_P2VAR_PARA(actBNRING) p_field = &Curve->p_field;
   actLengthType p_length = p_field->m_length;
   actLengthType p_bytes = p_field->m_byte_length;
   actLengthType n_bits = actBNGetBitLength(Curve->n_field.m, Curve->n_field.m_length);
   actLengthType prec_offset = (p_bytes >= 64) ? 4 : 3;
   actLengthType prec_length = prec_offset + (2 * p_bytes);
   actLengthType i, j, group_bits, index, bit_pos;
   actPROMU8 prec_ptr;

   /* calculate group_bits */
   for (i = n_bits; (i % Curve->groups) != 0; ++i) {}
   group_bits = i / Curve->groups;

   /* R = Infinity */
   R->is_affine   = 0;
   R->is_infinity = 1;

   /* 'Prec' will be a precomputed affine point in Montgomery representation */
   Prec->is_affine   = 1;
   Prec->is_infinity = 0;

   /* Double-And-Add loop */
   for (i = group_bits; i > 0; --i)
   {
      /* R *= 2 */
      actECPDouble(R, Curve);
      /* calculate index of precomputed point */
      index = 0;
      for (j = Curve->groups; j > 0; --j)
      {
         bit_pos = ((j - 1u) * group_bits) + (i - 1u); /* bit position */
         if (bit_pos < n_bits)
         {
            index += ((unsigned) actBNGetBit(k, bit_pos) << (j - 1u));
         }
      }
      /* initialize precomputed point */
      if (index == 0)
      {
         /* set pointer to x-coordinate of basepoint */
         prec_ptr = Curve->G_R;
      }
      else
      {
         /* set pointer to x-coordinate of the i-th precomputed point (i=index) */
         prec_ptr = Curve->prec_first + (index - 1) * prec_length + prec_offset;
      }
      actBNSetOctetStringROM(Prec->x, p_length, prec_ptr, p_bytes);
      actBNSetOctetStringROM(Prec->y, p_length, prec_ptr + p_bytes, p_bytes);
      /* R += Prec */
      actECPAdd(R, Prec, Curve);
   }

   /* initialize correction point */
   prec_ptr = Curve->prec_first + ((1 << Curve->groups) - 1) * prec_length + prec_offset;
   actBNSetOctetStringROM(Prec->x, p_length, prec_ptr, p_bytes);
   actBNSetOctetStringROM(Prec->y, p_length, prec_ptr + p_bytes, p_bytes);
   /* R += Prec */
   actECPAdd(R, Prec, Curve);
}


/****************************************************************************
 **
 ** FUNCTION:
 **  void actECPSimMult(const actECPOINT *Q1,
 **                     const actECPOINT *Q2,
 **                     const actBNDIGIT *k1,
 **                     const actBNDIGIT *k2,
 **                     actECPOINT *Q12,
 **                     actECPOINT *R,
 **                     actECCURVE *Curve)
 **
 **  This routine calculates R = k1*Q1 + k2*Q2 (two scalar multipli-
 **  cations incl. addition).
 **  The in- and output point(s) is(are) in Montgomery Representation.
 **  Implementation Reference: MOV'97, algo 14.88
 **
 ** input:                                              (length in digits)
 ** - Q1:    the point to be multiplied with k1             3*p_length
 ** - Q2:    the point to be multiplied with k2             3*p_length
 ** - k1:    first scalar                                   n_length
 ** - k1:    second scalar                                  n_length
 ** - Q12:   the point to store Q1 + Q2                     3*p_length
 ** - R:     the point to store the result                  3*p_length
 ** - Curve: the underlying curve (see assumptions)
 ** - Curve->p_field: the underlying field GF(p):
 ** -        p_field->m:        the modulus p               p_length
 ** -        p_field->m_length: equals to p_length
 ** -        p_field->m_bar:    constant for Montgomery     1
 ** -        p_field->prime_structure==curveid
 ** - Curve->n_field: the field GF(n):
 ** -        n_field->m_length: equals to n_length
 ** - Curve->t[0]:   temporary BigNum variable              p_length+1
 ** - Curve->t[1]:   temporary BigNum variable              p_length+1
 ** - Curve->t[2]:   temporary BigNum variable              p_length+1
 ** - Curve->t[3]:   temporary BigNum variable              p_length+1
 **
 ** output:
 ** - R:     the point k1*Q1 + k2*Q2 in Montgomery          3*p_length
 **          Representation (XR, YR, ZR)
 **
 ** assumes:
 ** - The actECCURVE structure parameter 'Curve' holds all necessary
 **   information and the workspace. It has to be initialized as far as
 **   listed above. Please have a look at the actECCURVE definition in
 **   actECPoint.h; an example for a complete initialization is the
 **   actECDSAVerify(..) implementation in actIECDSA.c.
 ** - Q1 is in affine Montgomery Representation (XR, YR, R)
 ** - Q2 is in affine Montgomery Representation (XR, YR, R)
 **
 ***************************************************************************/
VSECPRIM_FUNC(void) actECPSimMult(
   VSECPRIM_P2CONST_PARA(actECPOINT) Q1, VSECPRIM_P2CONST_PARA(actECPOINT) Q2,
   VSECPRIM_P2CONST_PARA(actBNDIGIT) k1, VSECPRIM_P2CONST_PARA(actBNDIGIT) k2,
   VSECPRIM_P2VAR_PARA(actECPOINT) Q12, VSECPRIM_P2VAR_PARA(actECPOINT) R,
   VSECPRIM_P2VAR_PARA(actECCURVE) Curve)
{
   actLengthType i = actBNGetBitLength(Curve->n_field.m, Curve->n_field.m_length);
   actLengthType tmpI;
   actU8 k1_bit, k2_bit;

   /* R = Infinity */
   R->is_affine   = 0;
   R->is_infinity = 1;

   /* Q12 = Q1 + Q2 */
   Q12->is_affine = 0;
   actECPCopy(Q12, Q1, Curve);
   actECPAdd(Q12, Q2, Curve);
#if (VSECPRIM_ACTBNEUCLID_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
   actECPToAffineFromMont(Q12, Curve, 1);
#endif

   /* Double-And-Add loop */
   for (; i > 0; --i)
   {
      tmpI = i - 1;
      /* R *= 2 */
      actECPDouble(R, Curve);
      /* test bit i of k1 and k2 */
      k1_bit = (actU8)actBNGetBit(k1, tmpI);
      k2_bit = (actU8)actBNGetBit(k2, tmpI);
      if (k1_bit)
      {
         if (k2_bit)
         {
            /* R += (Q1+Q2) */
            actECPAdd(R, Q12, Curve);
         }
         else
         {
            /* R += Q1 */
            actECPAdd(R, Q1, Curve);
         }
      }
      else
      {
         if (k2_bit)
         {
            /* R += Q2 */
            actECPAdd(R, Q2, Curve);
         }
      }
   }
}


#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#endif /* (VSECPRIM_ACTECPOINT_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actECPoint.c
 *********************************************************************************************************************/
