/****************************************************************************
 ** Main author: Ubr                     Creation date: 03/18/05
 ** Author: sst                          JustDate: 03/18/05
 ** Workfile: actKDF2.c                 Revision: 1107
 ** NoKeywords:
 **
 **
 ** \copyright(cv cryptovision GmbH, 1999 - 2005                         )
 **
 ** \version(1.0 (beta)                                                 )
 ***************************************************************************/

/****************************************************************************
 **
 **     Part of the actCLibrary
 **
 **     Layer: Core Module
 **
 ***************************************************************************/

/****************************************************************************
 **
 ** This file contains: KDF2 implementation file.
 **                     (Reference is PKCS#5)
 **
 ** constants:
 **
 ** types:
 **
 ** macros:
 **
 ** functions:
 **   actKDF2Init
 **   actKDF2Derive
 **
 ***************************************************************************/

#include "actIKDF2.h"
#include "actUtilities.h"

#if (VSECPRIM_ACTKDF2_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Global Prototypes
 ***************************************************************************/

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/****************************************************************************
 **
 ** FUNCTION:
 ** actRETURNCODE actKDF2Init(actKDF2STRUCT* info, int iteration_count)
 **
 **  This function initializes the KDF2 algorithm.
 **
 ** input:
 ** - info:             pointer to context structure
 ** - iteration_count:  number of iterations used for the key derivation
 **
 ** output:
 ** - info:       initialized context structure
 ** - returns:    actEXCEPTION_LENGTH   iteration_count <= 0
 **               actOK                 else
 **
 ** assumes:
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actKDF2Init(
   VSECPRIM_P2VAR_PARA(actKDF2STRUCT) info, int iteration_count)
{
   if (iteration_count <= 0)
   {
      return actEXCEPTION_LENGTH;
   }

   info->iteration_count = iteration_count;

   return actOK;
}


/****************************************************************************
 **
 ** FUNCTION:
 **  actRETURNCODE actKDF2Derive(actKDF2STRUCT* info,
 **                              const actU8* passwd,
 **                              int passwd_length,
 **                              const actU8* salt,
 **                              int salt_length,
 **                              actU8* key,
 **                              int key_length)
 **
 **  This function derives a key of the desired length from the input
 **  password and the (optional) salt.
 **
 ** input:
 ** - info:          pointer to context structure
 ** - passwd:        pointer to the password
 ** - passwd_length: length of passwd in bytes
 ** - salt:          pointer to the optional salt (if NULL, no salt is used)
 ** - salt_length:   length of salt in bytes (if zero, no salt is used)
 ** - key:           pointer to the output key buffer
 ** - key_length:    length of output key buffer in bytes
 **
 ** output:
 ** - info:       actualized context structure
 ** - key:        the derived key
 ** - returns:    actEXCEPTION_NULL     passwd or key is NULL
 **               actEXCEPTION_LENGTH   an input length value is negative
 **               actOK                 else
 **
 ** assumes:
 ** - actKDF2Init() is called once before calling this function
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actKDF2Derive(
   VSECPRIM_P2VAR_PARA(actKDF2STRUCT) info,
   VSECPRIM_P2CONST_PARA(actU8) passwd, int passwd_length,
   VSECPRIM_P2CONST_PARA(actU8) salt, int salt_length,
   VSECPRIM_P2VAR_PARA(actU8) key, int key_length)
{
   /*
      Implementation and notation reference is PKCS#5, PBKDF2,
      as underlying pseudorandom function HashMAC-SHA-1 is used.
   */

   VSECPRIM_P2VAR_PARA(actHASHMACSTRUCT) hmac = &info->hmac;
   VSECPRIM_P2VAR_PARA(actU8) U_buf = info->U_buf;
   VSECPRIM_P2VAR_PARA(actU8) F_buf = info->F_buf;
   actU8 INT[4];
   int c = info->iteration_count;
   int l = (key_length+actHASH_SIZE_SHA-1) / actHASH_SIZE_SHA;
   int r = key_length - (l-1) * actHASH_SIZE_SHA;
   int i, j;

   /* check params */
   if ((!key) || (!passwd))
   {
      return actEXCEPTION_NULL;
   }
   if ((passwd_length<0) || (salt_length<0) || (key_length<0))
   {
      return actEXCEPTION_LENGTH;
   }

   /* derive key */
   for (i = 1; i <= l; ++i)
   {
      /* clear F_buf */
      actMemset(F_buf, 0, actHASH_SIZE_SHA); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
      /* U_buf = U(1) = HMAC(passwd, salt||INT(i)) */
      actU32toBE(INT, (actU32)i);
      actHashMACInit(hmac, passwd, passwd_length, VSECPRIM_FUNC_NULL_PTR);
      if (salt != 0)
      {
        actHashMACUpdate(hmac, salt, salt_length, VSECPRIM_FUNC_NULL_PTR);
      }
      actHashMACUpdate(hmac, INT, sizeof(INT), VSECPRIM_FUNC_NULL_PTR);
      actHashMACFinalize(hmac, U_buf, VSECPRIM_FUNC_NULL_PTR);
      for (j = 2; j <= c; ++j)
      {
         /* F_buf = F_buf ^ U(j-1) */
         actXOR(F_buf, U_buf, actHASH_SIZE_SHA); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
         /* U_buf = U(j) = HMAC(passwd, U(j-1)) */
         actHashMACInit(hmac, passwd, passwd_length, VSECPRIM_FUNC_NULL_PTR);
         actHashMACUpdate(hmac, U_buf, actHASH_SIZE_SHA, VSECPRIM_FUNC_NULL_PTR);
         actHashMACFinalize(hmac, U_buf, VSECPRIM_FUNC_NULL_PTR);
      }
      /* F_buf = F_buf ^ U(c) */
      actXOR(F_buf, U_buf, actHASH_SIZE_SHA); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
      /* copy block T_i in F_buf to output key */
      if (i != l)
      {
         actMemcpy(key, F_buf, actHASH_SIZE_SHA); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
         key += actHASH_SIZE_SHA;
      }
      else
      {
         actMemcpy(key, F_buf, (unsigned int)r); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
      }
   }

   return actOK;
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTKDF2_ENABLED == STD_ON) */
