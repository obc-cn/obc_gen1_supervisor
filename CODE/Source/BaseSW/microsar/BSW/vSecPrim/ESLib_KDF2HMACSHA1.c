 /**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
 /**        \file  ESLib_KDF2HMACSHA1.c
 *        \brief  KDF2 key derivation function implementation.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

 /**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#define ESLIB_KDF2HMACSHA1_SOURCE

#include "ESLib.h"
#include "ESLib_types.h"

/* actCLib includes */
#include "actIKDF2.h"

#if (VSECPRIM_PKCS5_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/

/*---------------------------------------------------------------------------
  iterationCount:
      this specifies how often the underlying pseudo random function is applied
      (cfg. PKCS# 5 V2.0)
  errorCode:
      ESL_ERC_...
       - workSpace too small
               ESL_ERC_KDF_ITERATION_COUNT_OUT_OF_RANGE   iterationCount==0 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_initKDF2HMACSHA1(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceKDF2HMACSHA1) workSpace, const eslt_Length iterationCount)
{
  /* Any NULLs ? */
  if (!workSpace)
  {
    return ESL_ERC_PARAMETER_INVALID;
  }

  /* Check workSpace */
  if (workSpace->header.size < ESL_MAXSIZEOF_WS_KDF2HMACSHA1)
  {
    return ESL_ERC_WS_TOO_SMALL;
  }

  /* Init actCLib KDF2 */
  if (iterationCount == 0)
  {
    return ESL_ERC_KDF_ITERATION_COUNT_OUT_OF_RANGE;
  }
  else
  {
    actKDF2Init((VSECPRIM_P2VAR_PARA(actKDF2STRUCT)) workSpace->wsKDF2HMACSHA1, (int)iterationCount);
  }

  /* Set workSpace state */
  workSpace->header.status = (ESL_WST_ALGO_KDF2HMACSHA1 | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);

  return ESL_ERC_NO_ERROR;
}

/*---------------------------------------------------------------------------
  secretLength:
      defines the bytelength of the secret for password derivation
  secret:
      points to the secret that schall be used for derivation of the key
  infoLength:
      defines the bytelength of additional inforation for password derivation
  info:
      points to the additional information for password derivation
  keyLength:
      the bytelength of the key needed to be derived
  outputKey:
      here the derivation function will store the key
      this pointer chall point to keyLength byte of memory
  errorCode:
      ESL_ERC_... 
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_deriveKeyKDF2HMACSHA1(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceKDF2HMACSHA1) workSpace,
                                                        const eslt_Length secretLength, VSECPRIM_P2CONST_PARA(eslt_Byte) secret,
                                                        const eslt_Length infoLength, VSECPRIM_P2CONST_PARA(eslt_Byte) info, const eslt_Length keyLength, VSECPRIM_P2VAR_PARA(eslt_Byte) key)
{
  /* Any NULLs ? */
  if ((!workSpace) || (!secret) || (!key))
  {
    return ESL_ERC_PARAMETER_INVALID;
  }

  /* Check workSpace */
  if ((workSpace->header.status & ESL_WST_M_ALGO) != ESL_WST_ALGO_KDF2HMACSHA1)
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  if (!(workSpace->header.status & ESL_WST_M_RUNNING))
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  if (workSpace->header.size < ESL_MAXSIZEOF_WS_KDF2HMACSHA1)
  {
    return ESL_ERC_WS_TOO_SMALL;
  }

  /* Derive key with actCLib KDF2 */
  actKDF2Derive((VSECPRIM_P2VAR_PARA(actKDF2STRUCT)) workSpace->wsKDF2HMACSHA1, secret, (int)secretLength, info, (int)infoLength, key, (int)keyLength);

  return ESL_ERC_NO_ERROR;
}

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_PKCS5_ENABLED == STD_ON) */
/**********************************************************************************************************************
 *  END OF FILE: ESLib_KDF2HMACSHA1.c
 *********************************************************************************************************************/
