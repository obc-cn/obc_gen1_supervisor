 /**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
 /**        \file  ESLib_GMAC.c
 *        \brief  GMAC implementation.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

 /**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#define ESLIB_GMAC_SOURCE

#include "ESLib.h"
#include "ESLib_types.h"
#include "ESLib_Helper.h"

/* actCLib includes */
#include "actIGCM.h"
#include "actUtilities.h"

#if (VSECPRIM_GMAC_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 * esl_initGMAC
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_initGMAC(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGMAC) workSpace,
                                           const eslt_Length keyLength, VSECPRIM_P2CONST_PARA(eslt_Byte) key, const eslt_Length ivLength, VSECPRIM_P2CONST_PARA(eslt_Byte) iv)
{
  /* any NULLs ? */
  if ((!workSpace) || (!key))
  {
    return ESL_ERC_PARAMETER_INVALID;
  }

  /* check workSpace */
  if (workSpace->header.size < ESL_MAXSIZEOF_WS_GMAC)
  {
    return ESL_ERC_WS_TOO_SMALL;
  }

  /* check keyLength */
  if ((keyLength != ESL_SIZEOF_AES128_KEY) && (keyLength != ESL_SIZEOF_AES192_KEY) && (keyLength != ESL_SIZEOF_AES256_KEY))
  {
    return ESL_ERC_GCM_INVALID_KEY_LENGTH;
  }

  /* check ivLength */
  if (1 > ivLength)
  {
    return ESL_ERC_INVALID_IV_LENGTH;
  }

  /* init actCLib GCM */
  actGCMInit((VSECPRIM_P2VAR_PARA(actGCMSTRUCT)) workSpace->wsGMAC, key, (int)keyLength, iv, (int)ivLength, workSpace->header.watchdog);

  /* set workSpace state */
  workSpace->header.status = (ESL_WST_ALGO_GMAC | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);

  return ESL_ERC_NO_ERROR;
}

/****************************************************************************
 * esl_updateGMAC
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_updateGMAC(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGMAC) workSpace, const eslt_Length inputLength, VSECPRIM_P2CONST_PARA(eslt_Byte) input)
{
  /* any NULLs ? */
  if ((!workSpace))
  {
    return ESL_ERC_PARAMETER_INVALID;
  }

  /* check workSpace */
  if ((workSpace->header.status & ESL_WST_M_ALGO) != ESL_WST_ALGO_GMAC)
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  if (!(workSpace->header.status & ESL_WST_M_RUNNING))
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  else
  {
    if (workSpace->header.size < ESL_MAXSIZEOF_WS_GMAC)
    {
      return ESL_ERC_WS_TOO_SMALL;
    }
  }

  /* update actCLib GCM */
  {
    actRETURNCODE result = actGCMTransform((VSECPRIM_P2VAR_PARA(actGCMSTRUCT)) workSpace->wsGMAC, input, (int)inputLength, NULL_PTR, NULL_PTR, 0, workSpace->header.watchdog);
    if (result == actEXCEPTION_LENGTH)
    {
      return ESL_ERC_GCM_TOTAL_LENGTH_OVERFLOW;
    }
  }

  return ESL_ERC_NO_ERROR;
}

/****************************************************************************
 * esl_finalizeGMAC
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeGMAC(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGMAC) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) tag)
{
  /* any NULLs ? */
  if ((!workSpace))
  {
    return ESL_ERC_PARAMETER_INVALID;
  }

  /* check workSpace */
  if ((workSpace->header.status & ESL_WST_M_ALGO) != ESL_WST_ALGO_GMAC)
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  if (!(workSpace->header.status & ESL_WST_M_RUNNING))
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  else
  {
    if (workSpace->header.size < ESL_MAXSIZEOF_WS_GMAC)
    {
      return ESL_ERC_WS_TOO_SMALL;
    }
  }

  /* finalize actCLib GMAC */
  actGMACFinalize((VSECPRIM_P2VAR_PARA(actGCMSTRUCT)) workSpace->wsGMAC, tag, workSpace->header.watchdog);

  /* Reset and clear workspace */
  esl_ResetAndClearWorkspace(&workSpace->header, &workSpace->wsGMAC); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */ /* SBSW_VSECPRIM_CALL_FUNCTION */

  return ESL_ERC_NO_ERROR;
}

/****************************************************************************
 * esl_verifyGMAC
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_verifyGMAC(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGMAC) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) tag)
{
  eslt_Byte tmpTag[ESL_SIZEOF_GCM_TAG];

  /* any NULLs ? */
  if ((!workSpace))
  {
    return ESL_ERC_PARAMETER_INVALID;
  }

  /* check workSpace */
  if ((workSpace->header.status & ESL_WST_M_ALGO) != ESL_WST_ALGO_GMAC)
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  if (!(workSpace->header.status & ESL_WST_M_RUNNING))
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  else
  {
    if (workSpace->header.size < ESL_MAXSIZEOF_WS_GMAC)
    {
      return ESL_ERC_WS_TOO_SMALL;
    }
  }

  /* finalize actCLib GCM and compare provided tag with computed one */
  {
    actGMACFinalize((VSECPRIM_P2VAR_PARA(actGCMSTRUCT)) workSpace->wsGMAC, tmpTag, workSpace->header.watchdog);
    if (actMemcmp(tmpTag, tag, ESL_SIZEOF_GCM_TAG) != TRUE) /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
      return ESL_ERC_GCM_TAG_VERIFICATION_FAILED;
  }

  /* Reset and clear workspace */
  esl_ResetAndClearWorkspace(&workSpace->header, &workSpace->wsGMAC); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */ /* SBSW_VSECPRIM_CALL_FUNCTION */

  return ESL_ERC_NO_ERROR;
}

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_GMAC_ENABLED == STD_ON) */
/**********************************************************************************************************************
 *  END OF FILE: ESLib_GMAC.c
 *********************************************************************************************************************/
