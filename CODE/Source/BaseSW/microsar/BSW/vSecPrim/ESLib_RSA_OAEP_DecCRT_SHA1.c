/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  ESLib_RSA_OAEP_DecCRT_SHA1.c
 *        \brief  Crypto library - PKCS #1 RSA OAEP (Decryption)
 *
 *      \details RSA decryption using RSA OAEP encoding scheme according to PKCS #1 v2.2
 *               Currently the actClib version is used.
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/
#define ESLIB_RSA_OAEP_DEC_CRT_SHA1_SOURCE

/***********************************************************************************************************************
 *  INCLUDES
 **********************************************************************************************************************/

#include "ESLib.h"
#include "ESLib_types.h"

#include "actIRSA.h"
#include "actUtilities.h"
#include "actISHA.h"

#if (VSECPRIM_RSA_OAEP_SHA1_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/***********************************************************************************************************************
 *  DEFINES
 **********************************************************************************************************************/

/** Hash initialization function */
# define ESL_RSA_OAEP_INIT_HASH_FCT(workspace)                        actSHAInit(&((workspace)->wsSHA1))
/** Hash update function */
# define ESL_RSA_OAEP_UPDATE_HASH_FCT(workspace, inputSize, input)    actSHAUpdate(&((workspace)->wsSHA1), (VSECPRIM_P2CONST_PARA(actU8))(input), (actLengthType)(inputSize), (workspace)->header.watchdog)
/** Hash finalization function */
# define ESL_RSA_OAEP_FINALIZE_HASH_FCT(workspace, messageDigest)     actSHAFinalize(&((workspace)->wsSHA1), (VSECPRIM_P2VAR_PARA(actU8))(messageDigest),  (workspace)->header.watchdog)

/** Size of message digest (hash) */
# define ESL_RSA_OAEP_SIZEOF_HASH                                     ESL_SIZEOF_SHA1_DIGEST

/** Offset of buffer length in work space */
# define ESL_WS_RSA_OAEP_ED_BUFFERLENGTH                              ESL_WS_RSA_OAEP_ED_SHA1_BUFFERLENGTH
/** Offset of message buffer in work space */
# define ESL_WS_RSA_OAEP_ED_BUFFER                                    ESL_WS_RSA_OAEP_ED_SHA1_BUFFER
/** Offset of DB in work space */
# define ESL_WS_RSA_OAEP_ED_DB_BUFFER(ByteSizeOfBuffer)               ESL_WS_RSA_OAEP_ED_SHA1_DB_BUFFER(ByteSizeOfBuffer)
/** Offset of hash work space embedded in work space */
# define ESL_WS_RSA_OAEP_ED_WS_HASH(ByteSizeOfBuffer)                 ESL_WS_RSA_OAEP_ED_SHA1_WS_SHA1(ByteSizeOfBuffer)
/** Offset of RSA work space embedded in work space */
# define ESL_WS_RSA_OAEP_ED_WS_RSA_PRIM(ByteSizeOfBuffer)             ESL_WS_RSA_OAEP_ED_SHA1_WS_RSA_PRIM(ByteSizeOfBuffer)
/** Total work space overhead */
# define ESL_SIZEOF_WS_RSA_OAEP_ED_OVERHEAD(ByteSizeOfBuffer)         ESL_SIZEOF_WS_RSA_OAEP_ED_SHA1_OVERHEAD(ByteSizeOfBuffer)

/***********************************************************************************************************************
 *  TYPEDEFS
 **********************************************************************************************************************/

/** Hash operation work space */
typedef eslt_WorkSpaceSHA1 eslt_WorkSpaceHash;

/***********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 **********************************************************************************************************************/
# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/***********************************************************************************************************************
 *  esl_initDecryptRSACRTSHA1_OAEP
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptRSACRTSHA1_OAEP(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTOAEPdec) workSpace,
                                                             eslt_Length keyPairPrimePSize, eslt_pRomByte keyPairPrimeP,
                                                             eslt_Length keyPairPrimeQSize, eslt_pRomByte keyPairPrimeQ,
                                                             eslt_Length privateKeyExponentDPSize, eslt_pRomByte privateKeyExponentDP,
                                                             eslt_Length privateKeyExponentDQSize, eslt_pRomByte privateKeyExponentDQ,
                                                             eslt_Length privateKeyInverseQISize, eslt_pRomByte privateKeyInverseQI)
{
  eslt_ErrorCode returnValue;
  eslt_Length keyPairModuleSize;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader) wsHeader;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash) wsHash;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTdec_prim) wsPRIM;

  returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* Get underlying work space pointers */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte)) workSpace;
    wsHeader = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader)) wsRAW;
    keyPairModuleSize = (eslt_Length) (keyPairPrimePSize + keyPairPrimeQSize);
    wsHash = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash)) (wsRAW + ESL_WS_RSA_OAEP_ED_WS_HASH(keyPairModuleSize));
    wsPRIM = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTdec_prim)) (wsRAW + ESL_WS_RSA_OAEP_ED_WS_RSA_PRIM(keyPairModuleSize));

    if (wsHeader->size < (ESL_SIZEOF_WS_RSA_OAEP_ED_OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)))
    {
      returnValue = ESL_ERC_WS_TOO_SMALL;
    }
    else
    {
      wsHash->header.size = (eslt_Length) (sizeof(eslt_WorkSpaceHash) - sizeof(eslt_WorkSpaceHeader));
      wsHash->header.watchdog = wsHeader->watchdog;

      wsPRIM->header.size = (eslt_Length) (wsHeader->size - (ESL_SIZEOF_WS_RSA_OAEP_ED_OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)));
      wsPRIM->header.watchdog = wsHeader->watchdog;
      returnValue = esl_initDecryptRSACRT_prim(wsPRIM,
                                               keyPairPrimePSize, keyPairPrimeP,
                                               keyPairPrimeQSize, keyPairPrimeQ,
                                               privateKeyExponentDPSize, privateKeyExponentDP, privateKeyExponentDQSize, privateKeyExponentDQ, privateKeyInverseQISize, privateKeyInverseQI);
    }

    if (ESL_ERC_NO_ERROR == returnValue)
    {
      wsHeader->status = (ESL_WST_ALGO_RSA | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);
      /* Store key pair module size / message buffer length in work space */
      wsRAW[ESL_WS_RSA_OAEP_ED_BUFFERLENGTH] = (eslt_Byte) (keyPairModuleSize >> 8u);
      wsRAW[ESL_WS_RSA_OAEP_ED_BUFFERLENGTH + 1u] = (eslt_Byte) (keyPairModuleSize);
    }
  }

  return returnValue;
}

/***********************************************************************************************************************
 *  esl_decryptRSACRTSHA1_OAEP_Label
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSACRTSHA1_OAEP_Label(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTOAEPdec) workSpace,
                                                               eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
                                                               eslt_Length labelSize, VSECPRIM_P2CONST_PARA(eslt_Byte) label,
                                                               VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message)
{
  eslt_ErrorCode returnValue;
  eslt_Length keyPairModuleSize;
  eslt_Length encodedMessageSize;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader) wsHeader;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTdec_prim) wsPRIM;
  VSECPRIM_P2VAR_PARA(eslt_Byte) encodedMessage;

  returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* Get underlying workspace pointers */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte)) workSpace;
    wsHeader = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader)) wsRAW;
    keyPairModuleSize = (eslt_Length) (((eslt_Length) wsRAW[ESL_WS_RSA_OAEP_ED_BUFFERLENGTH] << 8) + (wsRAW[ESL_WS_RSA_OAEP_ED_BUFFERLENGTH + 1]));
    wsPRIM = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTdec_prim)) (wsRAW + ESL_WS_RSA_OAEP_ED_WS_RSA_PRIM(keyPairModuleSize));
    /* General memory layout in workspace:
       | EM                                           | DB                            | Temp  | -    |
       | 0x00 | mSeed | maskedDB                      | lHash | PS   | 0x01 | M       |       | -    | */
    encodedMessage = wsRAW + ESL_WS_RSA_OAEP_ED_BUFFER;
    encodedMessageSize = keyPairModuleSize;

    /* 7.1.2, Step 2: RSA decryption */
    returnValue = esl_decryptRSACRT_prim(wsPRIM, cipherSize, cipher, &encodedMessageSize, encodedMessage);

    if (ESL_ERC_NO_ERROR == returnValue)
    {
      /* 7.1.2, Step 3: EME-OAEP encoding decoding */
      returnValue = esl_decodeRSASHA1_OAEP((VSECPRIM_P2VAR_PARA(eslt_WorkSpace)) workSpace, encodedMessageSize, labelSize, label, messageSize, message);
    }
  }

  return returnValue;
}

/***********************************************************************************************************************
 *  esl_decryptRSACRTSHA1_OAEP
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSACRTSHA1_OAEP(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTOAEPdec) workSpace,
                                                         eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
                                                         VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message)
{
  return esl_decryptRSACRTSHA1_OAEP_Label(workSpace, cipherSize, cipher, 0u, 0u, messageSize, message);
}

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_RSA_OAEP_SHA1_ENABLED == STD_ON) */
/********************************************************************************************************************** 
 *  END OF FILE: ESLib_RSA_OAEP_DecCRT_SHA1.c 
 *********************************************************************************************************************/
