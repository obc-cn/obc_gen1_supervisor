/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2018 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actIAEAD7359.c
 *        \brief  AEAD7359 implementation.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library actCLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#define ACTIAEAD7359_SOURCE

/****************************************************************************
 **
 ** AEAD_ChaCha20_Poly1305 (RFC 7359) core and interface implementation.
 **
 ** constants:
 **
 ** types:
 **
 ** macros:
 **
 ** functions:
 **   actAEAD7359Init
 **   actAEAD7359Update
 **   actAEAD7359SwitchToData
 **   actAEAD7359Finalze
 **
 ***************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
 
# include "actIAEAD7359.h"
# include "actUtilities.h"
# include "actWatchdog.h"

 /* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

#if (VSECPRIM_ACTAEAD7359_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

#define SIZEOF_POLY1305_KEY                                          (32)

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


VSECPRIM_FUNC(void) actAEAD7359Init(VSECPRIM_P2VAR_PARA(actAEAD7359Context) ctx, VSECPRIM_P2CONST_PARA(actU8) key, VSECPRIM_P2CONST_PARA(actU8) nonce, const actU32 mode, VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void))
{
  ctx->mode = DIRECTION_ENCRYPT == mode ? DIRECTION_ENCRYPT : DIRECTION_DECRYPT;
  ctx->mode |= PHASE_AAD;

  actChaCha20Init(&(ctx->cc20), key, nonce, 0, watchdog);
  actChaCha20NextBlock(&(ctx->cc20), watchdog);
  actPoly1305Init(&(ctx->p130), (VSECPRIM_P2CONST_PARA(actU8))(ctx->cc20.cipher));
  actChaCha20EatBlock(&(ctx->cc20));

  ctx->aadLenHI = 0;  /* PRQA S 2983 */ /* MD_VSECPRIM_RESDUNDANT_ASSIGNMENT */
  ctx->aadLenLO = 0;  /* PRQA S 2983 */ /* MD_VSECPRIM_RESDUNDANT_ASSIGNMENT */
  ctx->dataLenHI = 0;  /* PRQA S 2983 */ /* MD_VSECPRIM_RESDUNDANT_ASSIGNMENT */
  ctx->dataLenLO = 0;   /* PRQA S 2983 */ /* MD_VSECPRIM_RESDUNDANT_ASSIGNMENT */

  actL1trigger(watchdog); /* PRQA S 1338, 2983, 3112  */ /* MD_MSR_DummyStmt */ /*lint -e438 */  /* SBSW_VSECPRIM_FUNCTION_CALL_WATCHDOG */
}


VSECPRIM_FUNC(actRETURNCODE) actAEAD7359Update(VSECPRIM_P2VAR_PARA(actAEAD7359Context) ctx, VSECPRIM_P2CONST_PARA(actU8) in, const int iLen, VSECPRIM_P2VAR_PARA(actU8) out, VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void))
{
  actU32 lo, hi;

  switch (ctx->mode)
  {
  case PHASE_AAD | DIRECTION_ENCRYPT:
  case PHASE_AAD | DIRECTION_DECRYPT:

    /* update & validate AAD counter: */
    /* max: 2^64 -1 bytes (RFC 7539) */
    /* -> FFFFFFFFFFFFFFFF */
    hi = ctx->aadLenHI;
    lo = ctx->aadLenLO + iLen;    /* increment AAD counter */
    if ((lo < ctx->aadLenLO)      /* LSN overflow? */
      && (0 == ++hi))             /* MSN overflow (2^64 -1 bytes)? */
      return actEXCEPTION_LENGTH; /* overflow! */

    ctx->aadLenHI = hi;
    ctx->aadLenLO = lo;

    /* update authenticator */
    actPoly1305Update(&(ctx->p130), in, iLen, watchdog);

    break;


  case PHASE_DATA | DIRECTION_ENCRYPT:
  case PHASE_DATA | DIRECTION_DECRYPT:

    /* update & validate data counter: */
    /* max: 2^32 -1  blocks (RFC 7539) */
    /* -> (2^32 -1) *64 bytes */
    /* -> 3F.FFFF.FFC0 */
    hi = ctx->dataLenHI;
    lo = ctx->dataLenLO + iLen;  /* increment data counter */
    if (lo < ctx->dataLenLO)     /* overflow? */
      hi++;

    if ((hi >  0x0000003F)
      || ((hi == 0x0000003F) && (lo > 0xFFFFFFC0)))
      return actEXCEPTION_LENGTH; /* overflow! */

    ctx->dataLenHI = hi;
    ctx->dataLenLO = lo;


    /* encrypt / decrypt "in" -> "out" */
    actChaCha20Cipher(&(ctx->cc20), in, iLen, out, watchdog);

    /* update authenticator */
    if (DIRECTION_ENCRYPT == (ctx->mode & DIRECTION))
      actPoly1305Update(&(ctx->p130), out, iLen, watchdog); /* ENcrypt:  "out" is cipher */
    else
      actPoly1305Update(&(ctx->p130), in, iLen, watchdog); /* DEcrypt:  "in"  is cipher */

    break;


  default:
    return actEXCEPTION_MODE;
  }

  return actOK;
}


VSECPRIM_FUNC(void) actAEAD7359SwitchToData(VSECPRIM_P2VAR_PARA(actAEAD7359Context) ctx)
{
  actPoly1305HandleRemains(&(ctx->p130), 1, NULL_PTR);

  ctx->mode &= DIRECTION;
  ctx->mode |= PHASE_DATA;
}


VSECPRIM_FUNC(void) actAEAD7359Finalize(VSECPRIM_P2VAR_PARA(actAEAD7359Context) ctx, VSECPRIM_P2VAR_PARA(actU8) tag, VSECPRIM_P2FUNC(VSECPRIM_NONE, void, watchdog)(void))
{
  actPoly1305HandleRemains(&(ctx->p130), 1, NULL_PTR);

  actU32toLE(&(ctx->data[0]), ctx->aadLenLO);
  actU32toLE(&(ctx->data[4]), ctx->aadLenHI);
  actU32toLE(&(ctx->data[8]), ctx->dataLenLO);
  actU32toLE(&(ctx->data[12]), ctx->dataLenHI);
  actPoly1305Update(&(ctx->p130), ctx->data, sizeof(ctx->data), watchdog);
  actPoly1305Finalize(&(ctx->p130), tag, watchdog);
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTAEAD7359_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actIAES.c
 *********************************************************************************************************************/
