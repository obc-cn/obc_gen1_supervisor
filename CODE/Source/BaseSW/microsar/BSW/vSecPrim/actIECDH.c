/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actIECDH.c
 *        \brief  Implementation file for actIECDH.h
 *
 *      \details This file is part of the embedded systems library cvActLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

/****************************************************************************
 **
 ** This file contains: Implementation file for actIECDH.h
 **
 ** constants:
 **
 ** types:
 **
 ** global variables:
 **
 ** macros:
 **
 ** functions:
 **   actECDHGetSecret
 **
 ***************************************************************************/

#include "actIECDH.h"
#include "actIECKey.h"
#include "actIKDF2.h"
#include "actECTools.h"
#include "actECDH.h"
#include "actECPoint.h"
#include "actECLengthInfo.h"
#include "actUtilities.h"
#include "actConfig.h"

#if (VSECPRIM_ACTIECDH_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/
#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTION:
 **   actECDSAGetSecretCompLength
 **
 **  This function returns the secret component length in bytes
 **  for the desired curve.
 **
 ** input:
 ** - domain:      domain parameter
 **
 ** output:
 ** - returns:    length of each secret component (x, y) in bytes
 **                 (0, if domain not valid)
 **
 ** assumes:
 **
 ** uses:
 **
 ***************************************************************************/
VSECPRIM_FUNC(int) actECDSAGetSecretCompLength(actPROMU8 domain)
{
  return actECGetPublicKeyCompLength(domain);
}

/****************************************************************************
 **
 ** FUNCTION:
 **  actECDHInitGetSecret
 **
 **  This function initializes the ECC workspace.
 **
 ** input:
 ** - domain:      domain parameter
 ** - domain_ext:  domain parameter extensions (Montgomery constants etc.)
 ** - wksp:        workspace
 ** - wksp_len:    length of workspace in bytes
 **
 ** output:
 ** - wksp:       initialized workspace
 ** - returns:    actEXCEPTION_NULL         if an input parameter is NULL
 **               actEXCEPTION_DOMAIN       domain decoding error
 **               actEXCEPTION_DOMAIN_EXT   domain_ext decoding error
 **               actEXCEPTION_MEMORY       wksp_len to small
 **               actOK                     else
 **
 ** assumes:
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDHInitGetSecret(actPROMU8 domain, actPROMU8 domain_ext, VSECPRIM_P2VAR_PARA(actU8) wksp, int wksp_len)
{
  return actECInit(domain, domain_ext, (VSECPRIM_P2CONST_PARA(actU8)) NULL_PTR, actEC_ALGO_FLAG_DH_PRIM, wksp, wksp_len);
}

/****************************************************************************
 **
 ** FUNCTION:
 **   actECDHGetSecret
 **
 **  This function calculates the common secret d1 * Q2 (= d1 * d2 * G)
 **
 ** input:
 ** - privatekey:  the private key d1 of the key pair (d1, Q1)
 ** - publickey_x: x-coordinate of the public key Q2 of the key pair (d2, Q2)
 ** - publickey_y: y-coordinate of the public key Q2 of the key pair (d2, Q2)
 ** - secret_x:    buffer to store x-coordinate of the secret d1 * Q2
 ** - secret_y:    buffer to store y-coordinate of the secret d1 * Q2
 **                  (optional secret_y maybe NULL)
 ** - wksp:        initialized workspace
 **
 ** output:
 ** - secret_x:    x-coordinate of the secret d1 * Q2
 ** - secret_y:    y-coordinate of the secret d1 * Q2
 ** - returns:    actEXCEPTION_NULL       if an input parameter is NULL
 **               actEXCEPTION_PRIVKEY    if (d==0||d>=n)
 **               actEXCEPTION_PUBKEY     if Q2 is invalid
 **               actEXCEPTION_UNKNOWN    internal error (result point
 **                                       not on curve)
 **               actOK                   else
 **
 ** assumes:
 ** - wksp has been initialized with actECDHInitGetSecret
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDHGetSecret(VSECPRIM_P2CONST_PARA(actU8) privatekey,
                                              VSECPRIM_P2CONST_PARA(actU8) publickey_x, VSECPRIM_P2CONST_PARA(actU8) publickey_y,
                                              VSECPRIM_P2VAR_PARA(actU8) secret_x, VSECPRIM_P2VAR_PARA(actU8) secret_y, VSECPRIM_P2VAR_PARA(actU8) wksp)
{
  actRETURNCODE returncode;
  VSECPRIM_P2VAR_PARA(actECCURVE) Curve = (VSECPRIM_P2VAR_PARA(actECCURVE)) wksp;
  VSECPRIM_P2VAR_PARA(actBNDIGIT) wksp_ptr = (VSECPRIM_P2VAR_PARA(actBNDIGIT)) (wksp + sizeof(actECCURVE));
  int p_length = Curve->p_field.m_length;
  int p_bytes = Curve->p_field.m_byte_length;
  int n_length = Curve->n_field.m_length;
  int n_bytes = Curve->n_field.m_byte_length;
  actECPOINT Q, point;
  VSECPRIM_P2VAR_PARA(actBNDIGIT) d;

  /* any null pointers ? */
  if ((!privatekey) || (!publickey_x) || (!publickey_y) || (!secret_x))
  {
    return actEXCEPTION_NULL;
  }

  /* assign temporary bignum workspace */
  wksp_ptr += actECBasicWksp(p_length, n_length); /* this workspace has been used by actECInit() */
  /* d, Q */ /* adjust workspace pointer */
  d = wksp_ptr;
  wksp_ptr += n_length;
  Q.x = wksp_ptr;
  wksp_ptr += p_length;
  Q.y = wksp_ptr;
  wksp_ptr += p_length;
  point.x = wksp_ptr;
  wksp_ptr += p_length;
  point.y = wksp_ptr;
  wksp_ptr += p_length;
  point.z = wksp_ptr;

  /* initialization of d */
  actBNSetOctetString(d, n_length, privatekey, n_bytes);

  /* check 0 < d < n before continue */
  if (actBNIsZero(d, n_length) == TRUE)
  {
    return actEXCEPTION_PRIVKEY;
  }
  if (actBNCompare(d, Curve->n_field.m, n_length) >= actCOMPARE_EQUAL)
  {
    return actEXCEPTION_PRIVKEY;
  }

  /* initialization of Q */
  actBNSetOctetString(Q.x, p_length, publickey_x, p_bytes);
  actBNSetOctetString(Q.y, p_length, publickey_y, p_bytes);
  Q.is_affine = 1;
  Q.is_infinity = 0;

  /*
     Call ECDHp and extract secret
   */

  /* calculate d*Q */
  returncode = actECDHp(d, &Q, &point, Curve);

  /* if ok, write secret to output */
  if (returncode == actOK)
  {
    /* secret_x = x-coordinate of d*Q */
    actBNOctetString(secret_x, p_bytes, point.x, p_length);
    /* secret_y = x-coordinate of d*Q */
    if (secret_y != 0)
    {
      actBNOctetString(secret_y, p_bytes, point.y, p_length);
    }
  }

  /* wipe local d */
  actBNSetZero(d, n_length);

  return returncode;
}

/****************************************************************************
 **
 ** FUNCTION:
 **  actECDHInitKeyDerive
 **
 **  This function initializes the ECC workspace.
 **
 ** input:
 ** - domain:      domain parameter
 ** - domain_ext:  domain parameter extensions (Montgomery constants etc.)
 ** - wksp:        workspace
 ** - wksp_len:    length of workspace in bytes
 **
 ** output:
 ** - wksp:       initialized workspace
 ** - returns:    actEXCEPTION_NULL         if an input parameter is NULL
 **               actEXCEPTION_DOMAIN       domain decoding error
 **               actEXCEPTION_DOMAIN_EXT   domain_ext decoding error
 **               actEXCEPTION_MEMORY       wksp_len to small
 **               actOK                     else
 **
 ** assumes:
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDHInitKeyDerive(actPROMU8 domain, actPROMU8 domain_ext, VSECPRIM_P2VAR_PARA(actU8) wksp, int wksp_len)
{
  return actECInit(domain, domain_ext, (VSECPRIM_P2CONST_PARA(actU8)) NULL_PTR, actEC_ALGO_FLAG_DH, wksp, wksp_len);
}

/****************************************************************************
 **
 ** FUNCTION:
 **   actECDHKeyDerive
 **
 **  This function calculates the common secret d1 * Q2 (= d1 * d2 * G)
 **  and derives the output key from the x-coordinate of the secret with
 **  the KDF2HMACSHA1 derivation function of the library.
 **
 **
 ** input:
 ** - privatekey:  the private key d1 of the key pair (d1, Q1)
 ** - publickey_x: x-coordinate of the public key Q2 of the key pair (d2, Q2)
 ** - publickey_y: y-coordinate of the public key Q2 of the key pair (d2, Q2)
 ** - iteration_count:  number of iterations used for the key derivation,
 **                     if zero, actKDF2_DEFAULT_ITERATION_COUNT is used.
 ** - salt:        pointer to the optional salt (if NULL, no salt is used)
 ** - salt_length: length of salt in bytes
 ** - key:         pointer to the output key buffer
 ** - key_length:  length of output key buffer in bytes
 ** - wksp:        initialized workspace
 **
 ** output:
 ** - key:         the derived key of length key_length
 ** - returns:    actEXCEPTION_NULL       if an input parameter is NULL
 **               actEXCEPTION_PRIVKEY    if (d==0||d>=n)
 **               actEXCEPTION_PUBKEY     if Q2 is invalid
 **               actEXCEPTION_UNKNOWN    internal error (result point
 **                                       not on curve)
 **               actOK                   else
 **
 ** assumes:
 ** - wksp has been initialized with actECDHInitGetSecret
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actECDHKeyDerive(VSECPRIM_P2CONST_PARA(actU8) privatekey, VSECPRIM_P2CONST_PARA(actU8) publickey_x,
                                              VSECPRIM_P2CONST_PARA(actU8) publickey_y, int iteration_count,
                                              VSECPRIM_P2CONST_PARA(actU8) salt, int salt_length, VSECPRIM_P2VAR_PARA(actU8) key, int key_length, VSECPRIM_P2VAR_PARA(actU8) wksp)
{
  actRETURNCODE returncode;
  int p_length = ((VSECPRIM_P2VAR_PARA(actECCURVE)) wksp)->p_field.m_length;
  int n_length = ((VSECPRIM_P2VAR_PARA(actECCURVE)) wksp)->n_field.m_length;
  VSECPRIM_P2VAR_PARA(actU8) secret_x = wksp + sizeof(actECCURVE) + actECDHWksp(p_length, n_length) * actBN_BYTES_PER_DIGIT;

  /* calculate secret_x */
  returncode = actECDHGetSecret(privatekey, publickey_x, publickey_y, secret_x, (VSECPRIM_P2VAR_PARA(actU8)) NULL_PTR, wksp);
  if (returncode != actOK)
  {
    return returncode;
  }

  /* derive key from secret_x */
  returncode = actKDF2Init((VSECPRIM_P2VAR_PARA(actKDF2STRUCT)) wksp, iteration_count);
  if (returncode != actOK)
  {
    return returncode;
  }
  returncode = actKDF2Derive((VSECPRIM_P2VAR_PARA(actKDF2STRUCT)) wksp, secret_x, p_length, salt, salt_length, key, key_length);
  if (returncode != actOK)
  {
    return returncode;
  }

  return actOK;
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTIECDH_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actIECDH.c
 *********************************************************************************************************************/
