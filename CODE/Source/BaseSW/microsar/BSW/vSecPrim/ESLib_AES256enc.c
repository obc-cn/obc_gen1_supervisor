 /**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
 /**        \file  ESLib_AES256enc.c
 *        \brief  AES-256 (encyrption) implementation.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

 /**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#define ESLIB_AES256ENC_SOURCE

#include "ESLib.h"
#include "ESLib_types.h"
#include "ESLib_Helper.h"

/* actCLib includes */
#include "actIAES.h"
#include "actUtilities.h"

#if(VSECPRIM_AES256_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/

/****************************************************************************
 * esl_initEncryptAES256
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptAES256(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key,
                                                    const eslt_BlockMode blockMode, const eslt_PaddingMode paddingMode, VSECPRIM_P2CONST_PARA(eslt_Byte) initializationVector)
{
  actU8 mode;

  /* Any NULLs ? */
  if ((!workSpace) || (!key))
  {
    return ESL_ERC_PARAMETER_INVALID;
  }

  /* Check workSpace */
  if (workSpace->header.size < ESL_MINSIZEOF_WS_AES256)
  {
    return ESL_ERC_WS_TOO_SMALL;
  }

  /* Check paddingMode */
  switch (paddingMode)
  {
    case ESL_PM_PKCS5:
    {
      mode = actPADDING_PM_PKCS7;

      break;
    }
    case ESL_PM_OFF:
    {
      mode = actPADDING_PM_OFF;

      break;
    }
    case ESL_PM_ONEWITHZEROES:
    {
      mode = actPADDING_PM_ONEWITHZEROES;

      break;
    }
    default:
    {
      return ESL_ERC_MODE_INVALID;
    }
  }

  /* Check block mode */
  switch (blockMode)
  {
    case ESL_BM_ECB:
    {
      mode |= actAES_BM_ECB;

      break;
    }
    case ESL_BM_CBC:
    {
      mode |= actAES_BM_CBC;

      break;
    }
    default:
    {
      return ESL_ERC_MODE_INVALID;
    }
  }

  /* Init actCLib AES256 */
  actAESInitEncrypt((VSECPRIM_P2VAR_PARA(actAESSTRUCT))&workSpace->wsAES, key, ESL_SIZEOF_AES256_KEY, initializationVector, mode, workSpace->header.watchdog);

  /* Set workSpace state */
  workSpace->header.status = (ESL_WST_ALGO_AES256 | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);

  return ESL_ERC_NO_ERROR;
}

/****************************************************************************
 * esl_encryptAES256
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptAES256(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256) workSpace,
                                                const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input, VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output)
{
  /* Any NULLs ? */
  if ((!workSpace) || (!input) || (!output))
  {
    return ESL_ERC_PARAMETER_INVALID;
  }
  if (outputSize == NULL_PTR)
  {
    return ESL_ERC_PARAMETER_INVALID;
  }

  /* Check workSpace */
  if ((workSpace->header.status & ESL_WST_M_ALGO) != ESL_WST_ALGO_AES256)
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  if (!(workSpace->header.status & ESL_WST_M_RUNNING))
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  if (inputSize % ESL_SIZEOF_AES256_BLOCK == 0)
  {
    if (workSpace->header.size < ESL_MINSIZEOF_WS_AES256)
    {
      return ESL_ERC_WS_TOO_SMALL;
    }
  }
  else
  {
    if (workSpace->header.size < ESL_MAXSIZEOF_WS_AES256)
    {
      return ESL_ERC_WS_TOO_SMALL;
    }
  }

  /* Update actCLib AES256 */
  {
    actRETURNCODE result;
    actLengthType enc_len = *outputSize;

    result = actAESEncrypt((VSECPRIM_P2VAR_PARA(actAESSTRUCT))&workSpace->wsAES, input, (actLengthType)inputSize, output, &enc_len, 0, workSpace->header.watchdog);

    if (result == actEXCEPTION_LENGTH)
    {
      return ESL_ERC_OUTPUT_SIZE_TOO_SHORT;
    }
    else
    {
      *outputSize = (eslt_Length) (enc_len & 0xFFFFu);
    }
  }

  return ESL_ERC_NO_ERROR;
}

/****************************************************************************
 * esl_finalizeEncryptAES256
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeEncryptAES256(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256) workSpace, VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output)
{
  /* Any NULLs ? */
  if ((!workSpace) || (!output))
  {
    return ESL_ERC_PARAMETER_INVALID;
  }
  if (outputSize == NULL_PTR)
  {
    return ESL_ERC_PARAMETER_INVALID;
  }

  /* Check workSpace */
  if ((workSpace->header.status & ESL_WST_M_ALGO) != ESL_WST_ALGO_AES256)
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  if (!(workSpace->header.status & ESL_WST_M_RUNNING))
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  if (workSpace->header.size < ESL_MAXSIZEOF_WS_AES256)
  {
    return ESL_ERC_WS_TOO_SMALL;
  }

  /* Update actCLib AES256 */
  {
    actRETURNCODE result;
    actLengthType enc_len = *outputSize;

    result = actAESEncrypt((VSECPRIM_P2VAR_PARA(actAESSTRUCT))&workSpace->wsAES, (VSECPRIM_P2CONST_PARA(actU8)) NULL_PTR, 0, output, &enc_len, 1, workSpace->header.watchdog);

    if (result == actEXCEPTION_LENGTH)
    {
      return ESL_ERC_OUTPUT_SIZE_TOO_SHORT;
    }
    else
    {
      *outputSize = (eslt_Length) (enc_len & 0xFFFFu);
    }
  }

  /* Reset and clear workspace */
  esl_ResetAndClearWorkspace(&workSpace->header, &workSpace->wsAES); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */

  return ESL_ERC_NO_ERROR;
}

/****************************************************************************
 * esl_initEncryptAES256Block
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptAES256Block(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256Block) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key)
{
  /* Any NULLs ? */
  if ((!workSpace) || (!key))
  {
    return ESL_ERC_PARAMETER_INVALID;
  }

  /* Check workSpace */
  if (workSpace->header.size < ESL_MAXSIZEOF_WS_AES256BLOCK)
  {
    return ESL_ERC_WS_TOO_SMALL;
  }

  /* Init actCLib AES256 */
  actAESInitEncryptBlock((VSECPRIM_P2VAR_PARA(actAESSTRUCT))&workSpace->wsAES, key, ESL_SIZEOF_AES256_KEY, workSpace->header.watchdog);

  /* Set workSpace state */
  workSpace->header.status = (ESL_WST_ALGO_AES256 | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);

  return ESL_ERC_NO_ERROR;
}

/****************************************************************************
 * esl_encryptAES256Block
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptAES256Block(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256Block) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) inputBlock, VSECPRIM_P2VAR_PARA(eslt_Byte) outputBlock)
{
  /* Any NULLs ? */
  if ((!workSpace) || (!inputBlock) || (!outputBlock))
  {
    return ESL_ERC_PARAMETER_INVALID;
  }

  /* Check workSpace */
  if ((workSpace->header.status & ESL_WST_M_ALGO) != ESL_WST_ALGO_AES256)
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  if (!(workSpace->header.status & ESL_WST_M_RUNNING))
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  if (workSpace->header.size < ESL_MAXSIZEOF_WS_AES256BLOCK)
  {
    return ESL_ERC_WS_TOO_SMALL;
  }

  /* Update actCLib AES256 */
  actAESEncryptBlock((VSECPRIM_P2VAR_PARA(actAESSTRUCT))&workSpace->wsAES, inputBlock, outputBlock, workSpace->header.watchdog);

  return ESL_ERC_NO_ERROR;
}

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_AES256_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: ESLib_AES256enc.c
 *********************************************************************************************************************/
