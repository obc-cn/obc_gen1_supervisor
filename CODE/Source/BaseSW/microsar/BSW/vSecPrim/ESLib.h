/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file ESLib.h
 *        \brief Provide service API's for the Module
 *
 *      \details The interface for the standard set of functions.
 *               Currently the actClib version is used.
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the ESLib_version.h.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#ifndef ESLIB_H
# define ESLIB_H

# include "ESLib_t.h"
# include "ESLib_ERC.h"
# include "ESLib_RNG.h"

/* PRQA S 0777 EOF */ /* MD_MSR_Rule5.1 */

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
extern "C"
{
# endif

/*****************************************************************************
 * Library version
 *****************************************************************************/

# ifndef NULL /* COV_VSECPRIM_STD_VALUES */
#  define NULL                                                        ((void*)0)
# endif

# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_MemMap */

/*****************************************************************************
 * Library initialisation function
 * (must be called before any use of the library)
 *****************************************************************************/
/**********************************************************************************************************************
 *  esl_initESCryptoLib()
 *********************************************************************************************************************/
/*! \brief          This function sets the library to a default value.
 *                  sets LibStatus to some default value
 *                  also initializes Lib internal vars if needed
 *                  on some controlers this function initializes
 *                  the library for multithreading features
 *  \details        -
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initESCryptoLib(void);

/*****************************************************************************
 * Workspace initialisation function
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initWorkSpaceHeader()
 *********************************************************************************************************************/
/*! \brief       Init workspace header
 *  \details     This function initializes a workSpace header. This function must be called before calling (nearly)
 *               every function from the cv act library/es to initialize the workSpace header correctly.
 *  \param[in,out]  workSpaceHeader  Pointer to workspace header
 *  \param[in]   workSpaceSize  Size of the provided workspace
 *  \param[in]   watchdog  Pointer to watchdog trigger function
 *  \return      ESL_ERC_NO_ERROR - OK
 *               ESL_ERC_PARAMETER_INVALID - Nullpointer check failed
 *  \pre         workspaceHeader must be a valid pointer.
 *  \context     TASK
 *  \reentrant   TRUE, for different workspaces
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initWorkSpaceHeader(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader) workSpaceHeader,
  const eslt_Length workSpaceSize,
  esl_WatchdogFuncPtr watchdog);

# if (VSECPRIM_FIPS186_ENABLED == STD_ON)
/*****************************************************************************
 * FIPS186 functions for a random number generator
 *****************************************************************************/

/**********************************************************************************************************************
 * esl_initFIPS186
 *********************************************************************************************************************/
/*!
 * \brief          Initialize FIPS186-2
 * \details        This function initializes the random number generator according to FIPS-186-2.
 * \param[in,out]  workSpace     (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 * \param[in]      entropyLength The length of the given entropy used as seed for the RNG.
 * \param[in]      entropy       A pointer to the value that has to be used as seed.
 * \param[in,out]  savedState    A pointer to a state that has been saved after the previous
 *                 initialisation. Length is given by ESL_SIZEOF_RNGFIPS186_STATE.
 *                 If this pointer is set to NULL, no savedState is used.
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_RNG_ENTROPY_TOO_SMALL  provided entropy length is to small
 *                 ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initFIPS186(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceFIPS186) workSpace,
const eslt_Length entropyLength, VSECPRIM_P2CONST_PARA(eslt_Byte) entropy,
VSECPRIM_P2VAR_PARA(eslt_Byte) savedState);

/**********************************************************************************************************************
 * esl_getBytesFIPS186
 *********************************************************************************************************************/
/*!
 * \brief          Get random byte
 * \details        This function generates random numbers according to FIPS-186-2.
 * \param[in,out]  workSpace       (in) algorithm context buffer, initialized by esl_initFIPS186(..) function
 *                                 (out) updated algorithm context structure
 * \param[in]      targetLength    the number of bytes that shall be generated
 * \param[in,out]  target          (in) pointer to output buffer
 *                                 (out) pointer to generated random bytes
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initFIPS186(..)
 * \context        TASK
 * \reentrant      TRUE
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_getBytesFIPS186(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceFIPS186) workSpace,
const eslt_Length targetLength, VSECPRIM_P2VAR_PARA(eslt_Byte) target);

/**********************************************************************************************************************
 * esl_stirFIPS186
 *********************************************************************************************************************/
/*!
 * \brief          Stir up seed state.
 * \details        This function stirs up the internal state of the random number generator.
 * \param[in,out]  workSpace     (in) algorithm context buffer, initialized by esl_initFIPS186(..) function
 *                               (out) updated algorithm context structure
 * \param[in]      inputLength   the number of input bytes used to stir up therandom number generator
 * \param[in,out]  input         pointer to input bytes used to stir up the random number generator
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initFIPS186(..)
 * \context        TASK
 * \reentrant      TRUE
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_stirFIPS186(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceFIPS186) workSpace,
const eslt_Length inputLength, VSECPRIM_P2VAR_PARA(eslt_Byte) input);

# endif /* (VSECPRIM_FIPS186_ENABLED == STD_ON) */
/*****************************************************************************
 * AES functions supporting different modes
 *****************************************************************************/

# if (VSECPRIM_AES128_ENABLED == STD_ON)

/**********************************************************************************************************************
 * esl_initEncryptAES128
 *********************************************************************************************************************/
/*!
 * \brief          Init AES128 encryption.
 * \details        This function initializes the AES128 stream encryption algorithm.
 * \param[in,out]  workSpace    (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                              (out) initialized algorithm context structure
 * \param[in]      key          pointer to user key of length ESL_SIZEOF_AES128_KEY
 * \param[in]      blockMode    ECB, CBC, etc. block mode switch
 * \param[in]      paddingMode  padding mode switch
 * \param[in]      initializationVector    pointer to initialization vector of length ESL_SIZEOF_AES128_BLOCK
 *                 (redundant in ECB mode, if iV==NULL, a trivial zero block is used as iV)
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptAES128(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES128) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key,
  const eslt_BlockMode blockMode, const eslt_PaddingMode paddingMode,
  VSECPRIM_P2CONST_PARA(eslt_Byte) initializationVector);

/**********************************************************************************************************************
 * esl_encryptAES128
 *********************************************************************************************************************/
/*!
 * \brief          This function encrypts input data of any length and can be called
 *                 arbitrary often after the algorithm initialization.
 * \details        Exact (inputSize+buffered_bytes)/16 blocks will be encrypted and written to the output buffer,
 *                 (inputSize+buffered_bytes)%16 remaining bytes will be buffered, where 0 <= buffered_bytes < 16.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initEncryptAES128(..) function
 *                             (out) updated algorithm context structure
 * \param[in]      inputSize   length of input data in bytes
 * \param[in]      input       pointer to data to be encrypted
 * \param[in,out]  outputSize  (in) output buffer size in bytes, a safe length is  ((inputSize+15)/16)*16  (see output)
 *                             (out) total size of encrypted blocks  ((inputSize+buffered_bytes)/16)*16
 * \param[in,out]   output     (in) pointer to encrypted data buffer
 *                             (out) pointer to encrypted data blocks
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initEncryptAES128(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptAES128(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES128) workSpace,
  const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input,
  VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 * esl_finalizeEncryptAES128
 *********************************************************************************************************************/
/*!
 * \brief          Finalize AES 128 encrypt
 * \details        This function finalizes the AES encryption by proccessing the remaining input bytes in the
 *                 internal buffer and padding the input according to the chosen padding in the corresponding
 *                 init function.
 * \param[in,out]  workSpace    (in) algorithm context structure
 *                              (out) updated algorithm context structure
 * \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is  2*AES_BLOCK_LENGTH
 *                              (out) total size of encrypted input  16 or 32 bytes
 * \param[in,out]  output       (in) pointer to encrypted data buffer
 *                              (out) pointer to encrypted data blocks
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initEncryptAES128(..)
 * \pre            esl_encryptAES128 called before
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeEncryptAES128(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES128) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 * esl_initDecryptAES128
 *********************************************************************************************************************/
/*!
 * \brief          Init AES128 decryption
 * \details        This function initializes the AES stream decryption algorithm.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                             (out) initialized algorithm context structure
 * \param[in]      key         pointer to user key of length ESL_SIZEOF_AES128_KEY
 * \param[in]      blockMode   ECB, CBC, etc. block mode switch
 * \param[in]      paddingMode padding mode switch
 * \param[in]      initializationVector   pointer to initialization vector of length ESL_SIZEOF_AES128_BLOCK
 *                                         (redundant in ECB mode, if iV==NULL, a trivial zero block is used as iV)
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptAES128(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES128) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key,
  const eslt_BlockMode blockMode, const eslt_PaddingMode paddingMode,
  VSECPRIM_P2CONST_PARA(eslt_Byte) initializationVector);

/**********************************************************************************************************************
 * esl_decryptAES128
 *********************************************************************************************************************/
/*!
 * \brief          This function decrypts input data of any length and can be called arbitrary often
 *                 after the algorithm initialization.
 * \details        Exact (inputSize+buffered_bytes)/16 blocks will be decrypted and written to the output buffer,
 *                 (inputSize+buffered_bytes)%16 remaining bytes will be buffered, where 0 <= buffered_bytes < 16.
 * \param[in,out]  workSpace    (in) algorithm context buffer, initialized by esl_initDecryptAES128(..) function
 *                              (out) updated algorithm context structure
 * \param[in]      inputSize    length of input data in bytes
 * \param[in]      input        pointer to data to be encrypted
 * \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is  ((inputSize+15)/16)*16  (see output)
 *                              (out) total size of encrypted blocks  ((inputSize+buffered_bytes)/16)*16
 * \param[in,out]  output       (in) pointer to encrypted data buffer
 *                              (out) pointer to encrypted data blocks
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initDecryptAES128(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptAES128(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES128) workSpace,
  const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input,
  VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 * esl_finalizeDecryptAES128
 *********************************************************************************************************************/
/*!
 * \brief          Finalize AES 128 decrypt
 * \details        This function finalizes the AES decryption by proccessing the remaining input bytes in the
 *                 internal buffer.
 * \param[in,out]  workSpace   (in) algorithm context structure
 *                             (out) updated algorithm context structure
 * \param[in,out]  outputSize  (in) output buffer size in bytes, a safe length is  AES_BLOCK_LENGTH
 *                             (out) total size of decryted input  16 or 32 bytes
 * \param[in,out]  output      (in) pointer to decrypted data buffer
 *                             (out) pointer to encrypted data blocks
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                 ESL_ERC_AES_PADDING_INVALID  the padding is invalid
 *                 ESL_ERC_INPUT_INVALID        the total input length is not zero mod blocksize
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initDecryptAES128(..)
 * \pre            esl_decryptAES128 called before
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeDecryptAES128(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES128) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 * esl_initEncryptAES128Block
 *********************************************************************************************************************/
/*!
 * \brief          Init AES128 block decryption
 * \details        This function initializes the AES block encryption algorithm.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                             (out) initialized algorithm context structure
 * \param[in]      key         pointer to user key of length ESL_SIZEOF_AES128_KEY
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptAES128Block(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES128Block) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 * esl_encryptAES128Block
 *********************************************************************************************************************/
/*!
 * \brief          Encrypt AES Block
 * \details        This function encrypts an AES Block (= 16 bytes)
 * \param[in,out]  workSpace     (in) algorithm context buffer, initialized by esl_initEncryptAES128Block(..) function
 *                               (out) updated algorithm context structure
 * \param[in]      inputBlock    pointer to the given input block (16 bytes)
 * \param[in,out]  outputBlock   (in) pointer to output buffer (at least 16 bytes)
 *                               (out) pointer to encrypted data block
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initEncryptAES128Block(..)
 * \context        TASK
 * \reentrant      TRUE
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptAES128Block(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES128Block) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) inputBlock, VSECPRIM_P2VAR_PARA(eslt_Byte) outputBlock);

/**********************************************************************************************************************
 * esl_initDecryptAES128Block
 *********************************************************************************************************************/
/*!
 * \brief          Decrypt AES 128 block.
 * \details        This function initializes the AES block decryption algorithm.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                             (out) initialized algorithm context structure
 * \param[in]      key         pointer to user key of length ESL_SIZEOF_AES128_KEY
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptAES128Block(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES128Block) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 * esl_decryptAES128Block
 *********************************************************************************************************************/
/*!
 * \brief          Decrypt AES block
 * \details        This function decrypts an AES Block (= 16 bytes)
 * \param[in]      workSpace     (in) algorithm context buffer, initialized by esl_initDecryptAES128Block(..) function
 *                               (out) updated algorithm context structure
 * \param[in]      inputBlock    pointer to the given input block (16 bytes)
 * \param[in,out]  outputBlock   (in) pointer to output buffer (at least 16 bytes)
 *                               (out) pointer to encrypted data block
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initDecryptAES128Block(..)
 * \context        TASK
 * \reentrant      TRUE
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptAES128Block(
  VSECPRIM_P2CONST_PARA(eslt_WorkSpaceAES128Block) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) inputBlock, VSECPRIM_P2VAR_PARA(eslt_Byte) outputBlock);
# endif /* VSECPRIM_AES128_ENABLED */

# if (VSECPRIM_AES192_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/**********************************************************************************************************************
 * esl_initEncryptAES192
 *********************************************************************************************************************/
/*!
 * \brief          Init AES192 encryption.
 * \details        This function initializes the AES192 stream encryption algorithm.
 * \param[in,out]  workSpace    (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                              (out) initialized algorithm context structure
 * \param[in]      key          pointer to user key of length ESL_SIZEOF_AES192_KEY
 * \param[in]      blockMode    ECB, CBC, etc. block mode switch
 * \param[in]      paddingMode  padding mode switch
 * \param[in]      initializationVector    pointer to initialization vector of length ESL_SIZEOF_AES192_BLOCK
 *                 (redundant in ECB mode, if iV==NULL, a trivial zero block is used as iV)
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptAES192(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES192) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key,
  const eslt_BlockMode blockMode, const eslt_PaddingMode paddingMode,
  VSECPRIM_P2CONST_PARA(eslt_Byte) initializationVector);

/**********************************************************************************************************************
 *  esl_encryptAES192()
 *********************************************************************************************************************/
/*! \brief          This function encrypts input data of any length and can be called
 *                  arbitrary often after the algorithm initialization.
 *                  Exact (inputSize+buffered_bytes)/16 blocks will be encrypted
 *                  and written to the output buffer, (inputSize+buffered_bytes)%16
 *                  remaining bytes will be buffered, where 0 <= buffered_bytes < 16.
 *  \details        -
 *  \param[in]      input        pointer to data to be encrypted
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               ((inputSize+15)/16)*16       (see output)
 *                               (out) total size of encrypted blocks
 *                               ((inputSize+buffered_bytes)/16)*16
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initEncryptAES192(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \param[in]      inputSize    length of input data in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initEncryptAES192(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptAES192(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES192) workSpace,
  const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input,
  VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_finalizeEncryptAES192()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the AES encryption by proccessing the
 *                  remaining input bytes in the internal buffer and padding the
 *                  input according to the chosen padding in the corresponding
 *                  init function.
 *  \details        -
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               2*AES_BLOCK_LENGTH
 *                               (out) total size of encrypted input
 *                               16 or 32 bytes
 *  \param[out]     workSpace    actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initEncryptAES192(..)
 *                  esl_encryptAES192 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeEncryptAES192(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES192) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_initDecryptAES192()
 *********************************************************************************************************************/
/*! \brief          This function initializes the AES stream decryption algorithm.
 *  \details        -
 *  \param[in]      initializationVector  pointer to initialization vector of length
 *                               ESL_SIZEOF_AES192_BLOCK (redundant in ECB mode,
 *                               if iV==NULL, a trivial zero block is used as iV)
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_AES192_KEY
 *  \param[in]      paddingMode  padding mode switch
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      blockMode    ECB, CBC, etc. block mode switch
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptAES192(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES192) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key,
  const eslt_BlockMode blockMode, const eslt_PaddingMode paddingMode,
  VSECPRIM_P2CONST_PARA(eslt_Byte) initializationVector);

/**********************************************************************************************************************
 *  esl_decryptAES192()
 *********************************************************************************************************************/
/*! \brief          This function decrypts input data of any length and can be called
 *                  arbitrary often after the algorithm initialization.
 *                  Exact (inputSize+buffered_bytes)/16 blocks will be decrypted
 *                  and written to the output buffer, (inputSize+buffered_bytes)%16
 *                  remaining bytes will be buffered, where 0 <= buffered_bytes < 16.
 *  \details        -
 *  \param[in]      input        pointer to data to be encrypted
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               ((inputSize+15)/16)*16       (see output)
 *                               (out) total size of encrypted blocks
 *                               ((inputSize+buffered_bytes)/16)*16
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initDecryptAES192(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \param[in]      inputSize    length of input data in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptAES192(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptAES192(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES192) workSpace,
  const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input,
  VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_finalizeDecryptAES192()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the AES decryption by proccessing the
 *                  remaining input bytes in the internal buffer.
 *  \details        -
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               AES_BLOCK_LENGTH
 *                               (out) total size of decryted input
 *                               16 or 32 bytes
 *  \param[out]     workSpace    actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to decrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_AES_PADDING_INVALID  the padding is invalid
 *                  ESL_ERC_INPUT_INVALID        the total input length is not zero mod blocksize
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptAES192(..)
 *                  esl_decryptAES192 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeDecryptAES192(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES192) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_initEncryptAES192Block()
 *********************************************************************************************************************/
/*! \brief          This function initializes the AES block encryption algorithm.
 *  \details        -
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_AES192_KEY
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptAES192Block(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES192Block) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_encryptAES192Block()
 *********************************************************************************************************************/
/*! \brief          This function encrypts an AES Block (= 16 bytes)
 *  \details        -
 *  \param[in,out]  outputBlock  (in) pointer to output buffer (at least 16 bytes)
 *                               (out) pointer to encrypted data block
 *  \param[in]      inputBlock   pointer to the given input block (16 bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initEncryptAES192Block(..) function
 *                               (out) actualized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initEncryptAES192Block(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptAES192Block(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES192Block) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) inputBlock, VSECPRIM_P2VAR_PARA(eslt_Byte) outputBlock);

/**********************************************************************************************************************
 *  esl_initDecryptAES192Block()
 *********************************************************************************************************************/
/*! \brief          This function initializes the AES block decryption algorithm.
 *  \details        -
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_AES192_KEY
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptAES192Block(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES192Block) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_decryptAES192Block()
 *********************************************************************************************************************/
/*! \brief          This function decrypts an AES Block (= 16 bytes)
 *  \details        -
 *  \param[in,out]  outputBlock  (in) pointer to output buffer (at least 16 bytes)
 *                               (out) pointer to encrypted data block
 *  \param[in]      inputBlock   pointer to the given input block (16 bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initDecryptAES192Block(..) function
 *                               (out) actualized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptAES192Block(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptAES192Block(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES192Block) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) inputBlock, VSECPRIM_P2VAR_PARA(eslt_Byte) outputBlock);

# endif /* VSECPRIM_AES192_ENABLED */

# if (VSECPRIM_AES256_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/**********************************************************************************************************************
 * esl_initEncryptAES256
 *********************************************************************************************************************/
/*!
 * \brief          Init AES256 encryption.
 * \details        This function initializes the AES256 stream encryption algorithm.
 * \param[in,out]  workSpace    (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                              (out) initialized algorithm context structure
 * \param[in]      key          pointer to user key of length ESL_SIZEOF_AES256_KEY
 * \param[in]      blockMode    ECB, CBC, etc. block mode switch
 * \param[in]      paddingMode  padding mode switch
 * \param[in]      initializationVector    pointer to initialization vector of length ESL_SIZEOF_AES256_BLOCK
 *                 (redundant in ECB mode, if iV==NULL, a trivial zero block is used as iV)
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptAES256(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key,
  const eslt_BlockMode blockMode, const eslt_PaddingMode paddingMode,
  VSECPRIM_P2CONST_PARA(eslt_Byte) initializationVector);

/**********************************************************************************************************************
 *  esl_encryptAES256()
 *********************************************************************************************************************/
/*! \brief          This function encrypts input data of any length and can be called
 *                  arbitrary often after the algorithm initialization.
 *                  Exact (inputSize+buffered_bytes)/16 blocks will be encrypted
 *                  and written to the output buffer, (inputSize+buffered_bytes)%16
 *                  remaining bytes will be buffered, where 0 <= buffered_bytes < 16.
 *  \details        -
 *  \param[in]      input        pointer to data to be encrypted
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               ((inputSize+15)/16)*16       (see output)
 *                               (out) total size of encrypted blocks
 *                               ((inputSize+buffered_bytes)/16)*16
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initEncryptAES256(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \param[in]      inputSize    length of input data in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initEncryptAES256(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptAES256(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256) workSpace,
  const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input,
  VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_finalizeEncryptAES256()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the AES encryption by proccessing the
 *                  remaining input bytes in the internal buffer and padding the
 *                  input according to the chosen padding in the corresponding
 *                  init function.
 *  \details        -
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               2*AES_BLOCK_LENGTH
 *                               (out) total size of encrypted input
 *                               16 or 32 bytes
 *  \param[out]     workSpace    actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initEncryptAES256(..)
 *                  esl_encryptAES256 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeEncryptAES256(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_initDecryptAES256()
 *********************************************************************************************************************/
/*! \brief          This function initializes the AES stream decryption algorithm.
 *  \details        -
 *  \param[in]      initializationVector  pointer to initialization vector of length
 *                               ESL_SIZEOF_AES256_BLOCK (redundant in ECB mode,
 *                               if iV==NULL, a trivial zero block is used as iV)
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_AES256_KEY
 *  \param[in]      paddingMode  padding mode switch
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      blockMode    ECB, CBC, etc. block mode switch
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptAES256(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key,
  const eslt_BlockMode blockMode, const eslt_PaddingMode paddingMode,
  VSECPRIM_P2CONST_PARA(eslt_Byte) initializationVector);

/**********************************************************************************************************************
 *  esl_decryptAES256()
 *********************************************************************************************************************/
/*! \brief          This function decrypts input data of any length and can be called
 *                  arbitrary often after the algorithm initialization.
 *                  Exact (inputSize+buffered_bytes)/16 blocks will be decrypted
 *                  and written to the output buffer, (inputSize+buffered_bytes)%16
 *                  remaining bytes will be buffered, where 0 <= buffered_bytes < 16.
 *  \details        -
 *  \param[in]      input        pointer to data to be encrypted
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               ((inputSize+15)/16)*16       (see output)
 *                               (out) total size of encrypted blocks
 *                               ((inputSize+buffered_bytes)/16)*16
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initDecryptAES256(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \param[in]      inputSize    length of input data in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptAES256(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptAES256(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256) workSpace,
  const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input,
  VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_finalizeDecryptAES256()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the AES decryption by proccessing the
 *                  remaining input bytes in the internal buffer.
 *  \details        -
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               AES_BLOCK_LENGTH
 *                               (out) total size of decryted input
 *                               16 or 32 bytes
 *  \param[out]     workSpace    actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to decrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_AES_PADDING_INVALID  the padding is invalid
 *                  ESL_ERC_INPUT_INVALID        the total input length is not zero mod blocksize
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptAES256(..)
 *                  esl_decryptAES256 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeDecryptAES256(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_initEncryptAES256Block()
 *********************************************************************************************************************/
/*! \brief          This function initializes the AES block encryption algorithm.
 *  \details        -
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_AES256_KEY
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptAES256Block(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256Block) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_encryptAES256Block()
 *********************************************************************************************************************/
/*! \brief          This function encrypts an AES Block (= 16 bytes)
 *  \details        -
 *  \param[in,out]  outputBlock  (in) pointer to output buffer (at least 16 bytes)
 *                               (out) pointer to encrypted data block
 *  \param[in]      inputBlock   pointer to the given input block (16 bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initEncryptAES256Block(..) function
 *                               (out) actualized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initEncryptAES256Block(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptAES256Block(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256Block) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) inputBlock, VSECPRIM_P2VAR_PARA(eslt_Byte) outputBlock);

/**********************************************************************************************************************
 *  esl_initDecryptAES256Block()
 *********************************************************************************************************************/
/*! \brief          This function initializes the AES block decryption algorithm.
 *  \details        -
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_AES256_KEY
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptAES256Block(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256Block) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_decryptAES256Block()
 *********************************************************************************************************************/
/*! \brief          This function decrypts an AES Block (= 16 bytes)
 *  \details        -
 *  \param[in,out]  outputBlock  (in) pointer to output buffer (at least 16 bytes)
 *                               (out) pointer to encrypted data block
 *  \param[in]      inputBlock   pointer to the given input block (16 bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initDecryptAES256Block(..) function
 *                               (out) actualized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptAES256Block(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptAES256Block(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceAES256Block) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) inputBlock, VSECPRIM_P2VAR_PARA(eslt_Byte) outputBlock);

# endif /* VSECPRIM_AES256_ENABLED */

# if (VSECPRIM_DES_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * DES functions supporting different modes
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_generateKeyDES()
 *********************************************************************************************************************/
/*! \brief           This function generates a 64 bit DES key for usage in block or
 *                   stream DES functions. It uses the RNG which is implemented
 *                   by the call back function esl_getBytesRNG(..).
 *  \details        -
 *  \param[in,out]  key          (in) pointer to user key buffer of size ESL_SIZEOF_DES_KEY
 *                               (out) pointer to the generated key
 *  \return         esl_getBytesRNG(..) return values are passed through
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateKeyDES(VSECPRIM_P2VAR_PARA(eslt_Byte) key);

/****************************************************************************
 * DES multi block crypto functions
 ****************************************************************************/

/**********************************************************************************************************************
 *  esl_initEncryptDES()
 *********************************************************************************************************************/
/*! \brief          This function initializes the DES stream encryption algorithm.
 *  \details        -
 *  \param[in]      initializationVector  pointer to initialization vector of length
 *                               ESL_SIZEOF_DES_BLOCK (redundant in ECB mode,
 *                               if iV==NULL, a trivial zero block is used as iV)
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_DES_KEY
 *  \param[in]      paddingMode  padding mode switch
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      blockMode    ECB, CBC, etc. block mode switch
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptDES(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceDES) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key,
const eslt_BlockMode blockMode, const eslt_PaddingMode paddingMode,
VSECPRIM_P2CONST_PARA(eslt_Byte) initializationVector);

/**********************************************************************************************************************
 *  esl_encryptDES()
 *********************************************************************************************************************/
/*! \brief          This function encrypts input data of any length and can be called
 *                  arbitrary often after the algorithm initialization.
 *                  Exact (inputSize+buffered_bytes)/8 blocks will be encrypted
 *                  and written to the output buffer, (inputSize+buffered_bytes)%8
 *                  remaining bytes will be buffered, where 0 <= buffered_bytes < 8.
 *  \details        -
 *  \param[in]      input        pointer to data to be encrypted
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               ((inputSize+7)/8)*8       (see output)
 *                               (out) total size of encrypted blocks
 *                               ((inputSize+buffered_bytes)/8)*8
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initEncryptDES(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \param[in]      inputSize    length of input data in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initEncryptDES(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptDES(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceDES) workSpace,
const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input,
VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_finalizeEncryptDES()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the DES encryption by proccessing the
 *                  remaining input bytes in the internal buffer and padding the
 *                  input according to the chosen padding in the corresponding
 *                  init function.
 *  \details        -
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               2*DES_BLOCK_LENGTH
 *                               (out) total size of encrypted input
 *                               8 or 16 bytes
 *  \param[out]     workSpace    actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initEncryptDES(..)
 *                  esl_encryptDES called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeEncryptDES(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceDES) workSpace,
VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_initDecryptDES()
 *********************************************************************************************************************/
/*! \brief          This function initializes the DES stream decryption algorithm.
 *  \details        -
 *  \param[in]      initializationVector  pointer to initialization vector of length
 *                               ESL_SIZEOF_DES_BLOCK (redundant in ECB mode,
 *                               if iV==NULL, a trivial zero block is used as iV)
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_DES_KEY
 *  \param[in]      paddingMode  padding mode switch
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      blockMode    ECB, CBC, etc. block mode switch
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptDES(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceDES) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key,
const eslt_BlockMode blockMode, const eslt_PaddingMode paddingMode,
VSECPRIM_P2CONST_PARA(eslt_Byte) initializationVector);

/**********************************************************************************************************************
 *  esl_decryptDES()
 *********************************************************************************************************************/
/*! \brief          This function decrypts input data of any length and can be called
 *                  arbitrary often after the algorithm initialization.
 *                  Exact (inputSize+buffered_bytes)/8 blocks will be decrypted
 *                  and written to the output buffer, (inputSize+buffered_bytes)%8
 *                  remaining bytes will be buffered, where 0 <= buffered_bytes < 8.
 *  \details        -
 *  \param[in]      input        pointer to data to be encrypted
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               ((inputSize+7)/8)*8       (see output)
 *                               (out) total size of encrypted blocks
 *                               ((inputSize+buffered_bytes)/8)*8
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initDecryptDES(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \param[in]      inputSize    length of input data in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptDES(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptDES(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceDES) workSpace,
const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input,
VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_finalizeDecryptDES()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the DES decryption by proccessing the
 *                  remaining input bytes in the internal buffer.
 *  \details        -
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               DES_BLOCK_LENGTH
 *                               (out) total size of decryted input
 *                               8 or 16 bytes
 *  \param[out]     workSpace    actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to decrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_DES_PADDING_INVALID  the padding is invalid
 *                  ESL_ERC_INPUT_INVALID        the total input length is not zero mod blocksize
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptDES(..)
 *                  esl_decryptDES called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeDecryptDES(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceDES) workSpace,
VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/****************************************************************************
 * DES single block crypto functions (other key length on request)
 ****************************************************************************/

/**********************************************************************************************************************
 *  esl_initEncryptDESBlock()
 *********************************************************************************************************************/
/*! \brief          This function initializes the DES block encryption algorithm.
 *  \details        -
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_DES_KEY
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptDESBlock(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceDESBlock) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_encryptDESBlock()
 *********************************************************************************************************************/
/*! \brief          This function encrypts an DES Block (= 8 bytes)
 *  \details        -
 *  \param[in,out]  outputBlock  (in) pointer to output buffer (at least 8 bytes)
 *                               (out) pointer to encrypted data block
 *  \param[in]      inputBlock   pointer to the given input block (8 bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initEncryptDESBlock(..) function
 *                               (out) actualized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initEncryptDESBlock(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptDESBlock(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceDESBlock) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) inputBlock, VSECPRIM_P2VAR_PARA(eslt_Byte) outputBlock);

/**********************************************************************************************************************
 *  esl_initDecryptDESBlock()
 *********************************************************************************************************************/
/*! \brief          This function initializes the DES block decryption algorithm.
 *  \details        -
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_DES_KEY
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptDESBlock(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceDESBlock) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_decryptDESBlock()
 *********************************************************************************************************************/
/*! \brief          This function decrypts an DES Block (= 8 bytes)
 *  \details        -
 *  \param[in,out]  outputBlock  (in) pointer to output buffer (at least 8 bytes)
 *                               (out) pointer to encrypted data block
 *  \param[in]      inputBlock   pointer to the given input block (8 bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initDecryptDESBlock(..) function
 *                               (out) actualized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptDESBlock(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptDESBlock(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceDESBlock) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) inputBlock, VSECPRIM_P2VAR_PARA(eslt_Byte) outputBlock);

# endif /* (VSECPRIM_DES_ENABLED == STD_ON) */

# if (VSECPRIM_TDES_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * TDES functions supporting different modes
 *****************************************************************************/

/****************************************************************************
 * TDES functions for multi and single mode (key generation)
 ****************************************************************************/

/**********************************************************************************************************************
 *  esl_generateKey3TDES()
 *********************************************************************************************************************/
/*! \brief           This function generates a 192 bit TDES key for usage in block or
 *                   stream TDES functions. It uses the RNG which is implemented
 *                   by the call back function esl_getBytesRNG(..).
 *  \details        -
 *  \param[in,out]  key          (in) pointer to user key buffer of size ESL_SIZEOF_3TDES_KEY
 *                               (out) pointer to the generated key
 *  \return         esl_getBytesRNG(..) return values are passed through
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateKey3TDES(VSECPRIM_P2VAR_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_generateKey2TDES()
 *********************************************************************************************************************/
/*! \brief           This function generates a 128 bit TDES key for usage in block or
 *                   stream TDES functions. It uses the RNG which is implemented
 *                   by the call back function esl_getBytesRNG(..).
 *  \details        -
 *  \param[in,out]  key          (in) pointer to user key buffer of size ESL_SIZEOF_2TDES_KEY
 *                               (out) pointer to the generated key
 *  \return         esl_getBytesRNG(..) return values are passed through
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateKey2TDES(VSECPRIM_P2VAR_PARA(eslt_Byte) key);

/****************************************************************************
 * TDES multi block crypto functions
 ****************************************************************************/

/**********************************************************************************************************************
 *  esl_initEncryptTDES()
 *********************************************************************************************************************/
/*! \brief          This function initializes the TDES stream encryption algorithm.
 *  \details        -
 *  \param[in]      key          pointer to user key
 *  \param[in]      initializationVector  pointer to initialization vector of length
 *                               ESL_SIZEOF_DES_BLOCK (redundant in ECB mode,
 *                               if iV==NULL, a trivial zero block is used as iV)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      key_len      length of user key
 *  \param[in]      paddingMode  padding mode switch
 *  \param[in]      blockMode    ECB, CBC, etc. block mode switch
 *  \return         ESL_ERC_PARAMETER_INVALID       input parameter is NULL
 *                  ESL_ERC_TDES_KEY_LENGTH_INVALID key length != 16 or 24
 *                  ESL_ERC_WS_TOO_SMALL            work space too small
 *                  ESL_ERC_MODE_INVALID            block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR                else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptTDES(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceTDES) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) key, const eslt_Byte key_len,
const eslt_BlockMode blockMode, const eslt_PaddingMode paddingMode,
VSECPRIM_P2CONST_PARA(eslt_Byte) initializationVector);

/**********************************************************************************************************************
 *  esl_encryptTDES()
 *********************************************************************************************************************/
/*! \brief          This function encrypts input data of any length and can be called
 *                  arbitrary often after the algorithm initialization.
 *                  Exact (inputSize+buffered_bytes)/8 blocks will be encrypted
 *                  and written to the output buffer, (inputSize+buffered_bytes)%8
 *                  remaining bytes will be buffered, where 0 <= buffered_bytes < 8.
 *  \details        -
 *  \param[in]      input        pointer to data to be encrypted
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               ((inputSize+7)/8)*8       (see output)
 *                               (out) total size of encrypted blocks
 *                               ((inputSize+buffered_bytes)/8)*8
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initEncryptTDES(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \param[in]      inputSize    length of input data in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initEncryptTDES(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptTDES(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceTDES) workSpace,
const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input,
VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_finalizeEncryptTDES()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the TDES encryption by proccessing the
 *                  remaining input bytes in the internal buffer and padding the
 *                  input according to the chosen padding in the corresponding
 *                  init function.
 *  \details        -
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               2*DES_BLOCK_LENGTH
 *                               (out) total size of encrypted input
 *                               8 or 16 bytes
 *  \param[out]     workSpace    actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initEncryptTDES(..)
 *                  esl_encryptTDES called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeEncryptTDES(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceTDES) workSpace,
VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_initDecryptTDES()
 *********************************************************************************************************************/
/*! \brief          This function initializes the TDES stream decryption algorithm.
 *  \details        -
 *  \param[in]      initializationVector  pointer to initialization vector of length
 *                               ESL_SIZEOF_DES_BLOCK (redundant in ECB mode,
 *                               if iV==NULL, a trivial zero block is used as iV)
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_3TDES_KEY or ESL_SIZEOF_2TDES_KEY
 *  \param[in]      key_len      length of the key
 *  \param[in]      paddingMode  padding mode switch
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      blockMode    ECB, CBC, etc. block mode switch
 *  \return         ESL_ERC_PARAMETER_INVALID       input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL            work space too small
 *                  ESL_ERC_MODE_INVALID            block or padding mode is invalid
 *                  ESL_ERC_TDES_KEY_LENGTH_INVALID key length != 16 or 24
 *                  ESL_ERC_NO_ERROR                else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptTDES(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceTDES) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) key, const eslt_Byte key_len,
const eslt_BlockMode blockMode, const eslt_PaddingMode paddingMode,
VSECPRIM_P2CONST_PARA(eslt_Byte) initializationVector);

/**********************************************************************************************************************
 *  esl_decryptTDES()
 *********************************************************************************************************************/
/*! \brief          This function decrypts input data of any length and can be called
 *                  arbitrary often after the algorithm initialization.
 *                  Exact (inputSize+buffered_bytes)/8 blocks will be decrypted
 *                  and written to the output buffer, (inputSize+buffered_bytes)%8
 *                  remaining bytes will be buffered, where 0 <= buffered_bytes < 8.
 *  \details        -
 *  \param[in]      input        pointer to data to be encrypted
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               ((inputSize+7)/8)*8       (see output)
 *                               (out) total size of encrypted blocks
 *                               ((inputSize+buffered_bytes)/8)*8
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initDecryptTDES(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \param[in]      inputSize    length of input data in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptTDES(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptTDES(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceTDES) workSpace,
const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input,
VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_finalizeDecryptTDES()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the TDES decryption by proccessing the
 *                  remaining input bytes in the internal buffer.
 *  \details        -
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               DES_BLOCK_LENGTH
 *                               (out) total size of decryted input
 *                               8 or 16 bytes
 *  \param[out]     workSpace    actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to decrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_DES_PADDING_INVALID  the padding is invalid
 *                  ESL_ERC_INPUT_INVALID        the total input length is not zero mod blocksize
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptTDES(..)
 *                  esl_decryptTDES called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeDecryptTDES(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceTDES) workSpace,
VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/****************************************************************************
 * TDES single block crypto functions (other key length on request)
 ****************************************************************************/

/**********************************************************************************************************************
 *  esl_initEncryptTDESBlock()
 *********************************************************************************************************************/
/*! \brief          This function initializes the TDES block encryption algorithm.
 *  \details        -
 *  \param[in]      key          pointer to user key
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      key_len      length of user key
 *  \return         ESL_ERC_PARAMETER_INVALID       input parameter is NULL
 *                  ESL_ERC_TDES_KEY_LENGTH_INVALID key length != 16 or 24
 *                  ESL_ERC_WS_TOO_SMALL            work space too small
 *                  ESL_ERC_MODE_INVALID            block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR                else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptTDESBlock(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceTDESBlock) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) key, const eslt_Byte key_len);

/**********************************************************************************************************************
 *  esl_encryptTDESBlock()
 *********************************************************************************************************************/
/*! \brief          This function encrypts an TDES Block (= 8 bytes)
 *  \details        -
 *  \param[in,out]  outputBlock  (in) pointer to output buffer (at least 8 bytes)
 *                               (out) pointer to encrypted data block
 *  \param[in]      inputBlock   pointer to the given input block (8 bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initEncryptTDESBlock(..) function
 *                               (out) actualized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initEncryptTDESBlock(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptTDESBlock(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceTDESBlock) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) inputBlock, VSECPRIM_P2VAR_PARA(eslt_Byte) outputBlock);

/**********************************************************************************************************************
 *  esl_initDecryptTDESBlock()
 *********************************************************************************************************************/
/*! \brief          This function initializes the TDES block decryption algorithm.
 *  \details        -
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_3TDES_KEY or ESL_SIZEOF_2TDES_KEY
 *  \param[in]      key_len      key length
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID       input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL            work space too small
 *                  ESL_ERC_TDES_KEY_LENGTH_INVALID key length != 16 or 24
 *                  ESL_ERC_NO_ERROR                else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptTDESBlock(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceTDESBlock) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) key, const eslt_Byte key_len);

/**********************************************************************************************************************
 *  esl_decryptTDESBlock()
 *********************************************************************************************************************/
/*! \brief          This function decrypts an TDES Block (= 8 bytes)
 *  \details        -
 *  \param[in,out]  outputBlock  (in) pointer to output buffer (at least 8 bytes)
 *                               (out) pointer to encrypted data block
 *  \param[in]      inputBlock   pointer to the given input block (8 bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initDecryptTDESBlock(..) function
 *                               (out) actualized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptTDESBlock(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptTDESBlock(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceTDESBlock) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) inputBlock, VSECPRIM_P2VAR_PARA(eslt_Byte) outputBlock);

# endif /* (VSECPRIM_TDES_ENABLED == STD_ON) */

# if (VSECPRIM_RC2_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * RC2 functions supporting different modes
 *****************************************************************************/

/****************************************************************************
 * RC2 functions for multi and single mode (key generation)
 ****************************************************************************/

/**********************************************************************************************************************
 *  esl_generateKeyRC2()
 *********************************************************************************************************************/
/*! \brief           This function generates a ESL_SIZEOF_RC2_KEY RC2 key for usage in block or
 *                   stream RC2 functions. It uses the RNG which is implemented
 *                   by the call back function esl_getBytesRNG(..).
 *  \details        -
 *  \param[in,out]  key          (in) pointer to user key buffer of size ESL_SIZEOF_RC2_KEY
 *                               (out) pointer to the generated key
 *  \return         esl_getBytesRNG(..) return values are passed through
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateKeyRC2(VSECPRIM_P2VAR_PARA(eslt_Byte) key);

/****************************************************************************
 * RC2 multi block crypto functions
 ****************************************************************************/

/**********************************************************************************************************************
 *  esl_initEncryptRC2()
 *********************************************************************************************************************/
/*! \brief          This function initializes the RC2 stream encryption algorithm.
 *  \details        -
 *  \param[in]      initializationVector  pointer to initialization vector of length
 *                               ESL_SIZEOF_RC2_BLOCK (redundant in ECB mode,
 *                               if iV==NULL, a trivial zero block is used as iV)
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_RC2_KEY
 *  \param[in]      key_len      length of key in byte
 *  \param[in]      eff_key_len  eff_key_len length of effective key in Byte
 *  \param[in]      paddingMode  padding mode switch
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      blockMode    ECB, CBC, etc. block mode switch
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptRC2(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRC2) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) key, const eslt_Length key_len, const eslt_Length eff_key_len,
const eslt_BlockMode blockMode, const eslt_PaddingMode paddingMode,
VSECPRIM_P2CONST_PARA(eslt_Byte) initializationVector);

/**********************************************************************************************************************
 *  esl_encryptRC2()
 *********************************************************************************************************************/
/*! \brief          This function encrypts input data of any length and can be called
 *                  arbitrary often after the algorithm initialization.
 *                  Exact (inputSize+buffered_bytes)/8 blocks will be encrypted
 *                  and written to the output buffer, (inputSize+buffered_bytes)%8
 *                  remaining bytes will be buffered, where 0 <= buffered_bytes < 8.
 *  \details        -
 *  \param[in]      input        pointer to data to be encrypted
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               ((inputSize+7)/8)*8       (see output)
 *                               (out) total size of encrypted blocks
 *                               ((inputSize+buffered_bytes)/8)*8
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initRC2(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \param[in]      inputSize    length of input data in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initRC2(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptRC2(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRC2) workSpace,
const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input,
VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_finalizeEncryptRC2()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the RC2 encryption by proccessing the
 *                  remaining input bytes in the internal buffer and padding the
 *                  input according to the chosen padding in the corresponding
 *                  init function.
 *  \details        -
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               2*RC2_BLOCK_LENGTH
 *                               (out) total size of encrypted input
 *  \param[out]     workSpace    actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initRC2(..)
 *                  esl_encryptRC2 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeEncryptRC2(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRC2) workSpace,
VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_initDecryptRC2()
 *********************************************************************************************************************/
/*! \brief          This function initializes the RC2 stream decryption algorithm.
 *  \details        -
 *  \param[in]      initializationVector  pointer to initialization vector of length
 *                               ESL_SIZEOF_RC2_BLOCK (redundant in ECB mode,
 *                               if iV==NULL, a trivial zero block is used as iV)
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_RC2_KEY
 *  \param[in]      key_len      length of key in byte
 *  \param[in]      eff_key_len  eff_key_len length of effective key in Byte
 *  \param[in]      paddingMode  padding mode switch
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      blockMode    ECB, CBC, etc. block mode switch
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptRC2(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRC2) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) key, const eslt_Length key_len, const eslt_Length eff_key_len,
const eslt_BlockMode blockMode, const eslt_PaddingMode paddingMode,
VSECPRIM_P2CONST_PARA(eslt_Byte) initializationVector);

/**********************************************************************************************************************
 *  esl_decryptRC2()
 *********************************************************************************************************************/
/*! \brief          This function decrypts input data of any length and can be called
 *                  arbitrary often after the algorithm initialization.
 *                  Exact (inputSize+buffered_bytes)/8 blocks will be decrypted
 *                  and written to the output buffer, (inputSize+buffered_bytes)%8
 *                  remaining bytes will be buffered, where 0 <= buffered_bytes < 8.
 *  \details        -
 *  \param[in]      input        pointer to data to be encrypted
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               ((inputSize+7)/8)*8       (see output)
 *                               (out) total size of encrypted blocks
 *                               ((inputSize+buffered_bytes)/8)*8
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initDecryptRC2(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to encrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \param[in]      inputSize    length of input data in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptRC2(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRC2(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRC2) workSpace,
const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input,
VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/**********************************************************************************************************************
 *  esl_finalizeDecryptRC2()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the RC2 decryption by proccessing the
 *                  remaining input bytes in the internal buffer.
 *  \details        -
 *  \param[in,out]  outputSize   (in) output buffer size in bytes, a safe length is
 *                               RC2_BLOCK_LENGTH
 *                               (out) total size of decryted input
 *  \param[out]     workSpace    actualized algorithm context structure
 *  \param[in,out]  output       (in) pointer to decrypted data buffer
 *                               (out) pointer to encrypted data blocks
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   outputSize too small
 *                  ESL_ERC_RC2_PADDING_INVALID  the padding is invalid
 *                  ESL_ERC_INPUT_INVALID        the total input length is not zero mod blocksize
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptRC2(..)
 *                  esl_decryptRC2 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeDecryptRC2(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRC2) workSpace,
VSECPRIM_P2VAR_PARA(eslt_Length) outputSize, VSECPRIM_P2VAR_PARA(eslt_Byte) output);

/****************************************************************************
 * RC2 single block crypto functions (other key length on request)
 ****************************************************************************/

/**********************************************************************************************************************
 *  esl_initEncryptRC2Block()
 *********************************************************************************************************************/
/*! \brief          This function initializes the RC2 block encryption algorithm.
 *  \details        -
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_RC2_KEY
 *  \param[in]      key_len      length of key in byte
 *  \param[in]      eff_key_len  eff_key_len length of effective key in Byte
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptRC2Block(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRC2Block) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) key, const eslt_Length key_len, const eslt_Length eff_key_len);

/**********************************************************************************************************************
 *  esl_encryptRC2Block()
 *********************************************************************************************************************/
/*! \brief          This function encrypts an RC2 Block (= 8 bytes)
 *  \details        -
 *  \param[in,out]  outputBlock  (in) pointer to output buffer (at least 8 bytes)
 *                               (out) pointer to encrypted data block
 *  \param[in]      inputBlock   pointer to the given input block (8 bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initEncryptRC2Block(..) function
 *                               (out) actualized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initEncryptRC2Block(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptRC2Block(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRC2Block) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) inputBlock, VSECPRIM_P2VAR_PARA(eslt_Byte) outputBlock);

/**********************************************************************************************************************
 *  esl_initDecryptRC2Block()
 *********************************************************************************************************************/
/*! \brief          This function initializes the RC2 block decryption algorithm.
 *  \details        -
 *  \param[in]      key          pointer to user key of length ESL_SIZEOF_RC2_KEY
 *  \param[in]      key_len      length of key in byte
 *  \param[in]      eff_key_len  eff_key_len length of effective key in Byte
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_MODE_INVALID      block or padding mode is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptRC2Block(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRC2Block) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) key, const eslt_Length key_len, const eslt_Length eff_key_len);

/**********************************************************************************************************************
 *  esl_decryptRC2Block()
 *********************************************************************************************************************/
/*! \brief          This function decrypts an RC2 Block (= 8 bytes)
 *  \details        -
 *  \param[in,out]  outputBlock  (in) pointer to output buffer (at least 8 bytes)
 *                               (out) pointer to encrypted data block
 *  \param[in]      inputBlock   pointer to the given input block (8 bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initDecryptRC2Block(..) function
 *                               (out) actualized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initDecryptRC2Block(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRC2Block(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRC2Block) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) inputBlock, VSECPRIM_P2VAR_PARA(eslt_Byte) outputBlock);

# endif /* (VSECPRIM_RC2_ENABLED == STD_ON) */

# if (VSECPRIM_GMAC_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * GMAC functions
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initGMAC()
 *********************************************************************************************************************/
/*! \brief          This function initializes the GMAC calculation.
 *  \details        -
 *  \param[in]      key          the symmetric AES master key
 *  \param[in]      ivLength     the length of the initialization vector in bytes
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      iv           the initialization vector
 *  \param[in]      keyLength    the length of the AES key in bytes, must be 16, 24 or 32
 *  \return         ESL_ERC_PARAMETER_INVALID       input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL            work space too small
 *                  ESL_ERC_GCM_INVALID_KEY_LENGTH  no valid AES keyLength
 *                  ESL_ERC_NO_ERROR                else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initGMAC(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGMAC) workSpace,
const eslt_Length keyLength,
VSECPRIM_P2CONST_PARA(eslt_Byte) key,
const eslt_Length ivLength,
VSECPRIM_P2CONST_PARA(eslt_Byte) iv);

/**********************************************************************************************************************
 *  esl_updateGMAC()
 *********************************************************************************************************************/
/*! \brief          This function updates the GMAC operation.
 *  \details        -
 *  \param[in]      input        the input data
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      inputLength  the length of the input
 *  \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID           work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL               work space too small
 *                  ESL_ERC_GCM_TOTAL_LENGTH_OVERFLOW  the input exceeded the length limit
 *                  ESL_ERC_NO_ERROR                   else
 *  \pre            workSpace is initialized by esl_initGMAC(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateGMAC(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGMAC) workSpace,
const eslt_Length inputLength,
VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeGMAC()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the GMAC operation.
 *  \details        -
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *  \param[in,out]  tag          (in) a pointer to a buffer for the authentication tag
 *                               (out) this variable is incremented by the number of bytes that
 *                               were handled in this iteration of updateEncryptGCM
 *  \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID           work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL               work space too small
 *                  ESL_ERC_NO_ERROR                   else
 *  \pre            workSpace is initialized by esl_initGMAC(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeGMAC(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGMAC) workSpace,
VSECPRIM_P2VAR_PARA(eslt_Byte) tag);

/**********************************************************************************************************************
 *  esl_verifyGMAC()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the GMAC operation.
 *  \details        -
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *  \param[in]      tag          a pointer to a buffer containing the authentication tag
 *  \return         ESL_ERC_PARAMETER_INVALID            input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID             work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL                 work space too small
 *                  ESL_ERC_GCM_TAG_VERIFICATION_FAILED  the provided tag does not
 *                  match the computed one
 *                  ESL_ERC_NO_ERROR                     else
 *  \pre            workSpace is initialized by esl_initGMAC(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifyGMAC(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGMAC) workSpace,
VSECPRIM_P2CONST_PARA(eslt_Byte) tag);

# endif /* (VSECPRIM_GMAC_ENABLED == STD_ON) */

# if (VSECPRIM_GCM_ENABLED == STD_ON)
/*****************************************************************************
 * GCM functions
 *****************************************************************************/

/**********************************************************************************************************************
 * esl_initEncryptGCM
 *********************************************************************************************************************/
/*!
 * \brief          Initializes the GCM encryption.
 * \details        This function initializes the GCM encryption.
 * \param[in,out]  workSpace     (in)  algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 * \param[in]      key           the symmetric AES master key
 * \param[in]      keyLength     the length of the AES key in bytes, must be 16, 24 or 32
 * \param[in]      iv            the initialization vector
 * \param[in]      ivLength      the length of the initialization vector in bytes
 * \return         ESL_ERC_PARAMETER_INVALID       input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL            work space too small
 *                 ESL_ERC_GCM_INVALID_KEY_LENGTH  no valid AES keyLength
 *                 ESL_ERC_NO_ERROR                else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptGCM(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGCM) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) key,
  const eslt_Length keyLength,
  VSECPRIM_P2CONST_PARA(eslt_Byte) iv,
  const eslt_Length ivLength);

/**********************************************************************************************************************
 * esl_initDecryptGCM
 *********************************************************************************************************************/
/*!
 * \brief          Initializes the GCM decryption.
 * \details        This function initializes the GCM decryption.
 * \param[in,out]  workSpace     (in)  algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 * \param[in]      key           the symmetric AES master key
 * \param[in]      keyLength     the length of the AES key in bytes, must be 16, 24 or 32
 * \param[in]      iv            the initialization vector
 * \param[in]      ivLength      the length of the initialization vector in bytes
 * \return         ESL_ERC_PARAMETER_INVALID       input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL            work space too small
 *                 ESL_ERC_GCM_INVALID_KEY_LENGTH  no valid AES keyLength
 *                 ESL_ERC_NO_ERROR                else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptGCM(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGCM) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) key,
  const eslt_Length keyLength,
  VSECPRIM_P2CONST_PARA(eslt_Byte) iv,
  const eslt_Length ivLength);

/**********************************************************************************************************************
 * esl_updateAuthDataEncryptGCM
 *********************************************************************************************************************/
/*!
 * \brief          Hashes additional authenticated data using AES-GCM.
 * \details        This function hashes additional authenticated data using AES-GCM.
 * \param[in,out]  workSpace     (in)  algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) updated algorithm context structure
 * \param[in]      input         additional authenticated data
 * \param[in]      inputLength   the length of the input
 * \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID           work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL               work space too small
 *                 ESL_ERC_GCM_STATE_INVALID          GCM state invalid
 *                 ESL_ERC_GCM_TOTAL_LENGTH_OVERFLOW  the input exceeded the length limit
 *                 ESL_ERC_NO_ERROR                   else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateAuthDataEncryptGCM(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGCM) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) input,
  const eslt_Length inputLength);

/**********************************************************************************************************************
 * esl_updateAuthDataDecryptGCM
 *********************************************************************************************************************/
/*!
 * \brief          Decrypts the ciphertext using AES-GCM.
 * \details        This function decrypts the ciphertext using AES-GCM.
 * \param[in,out]  workSpace     (in)  algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) updated algorithm context structure
 * \param[in]      input         additional authenticated data
 * \param[in]      inputLength   the length of the input
 * \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID           work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL               work space too small
 *                 ESL_ERC_GCM_STATE_INVALID          GCM state invalid
 *                 ESL_ERC_GCM_TOTAL_LENGTH_OVERFLOW  the input exceeded the length limit
 *                 ESL_ERC_NO_ERROR                   else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateAuthDataDecryptGCM(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGCM) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) input,
  const eslt_Length inputLength);

/**********************************************************************************************************************
 * esl_updatePlaintextEncryptGCM
 *********************************************************************************************************************/
/*!
 * \brief          Encrypts the plaintext message using AES-GCM.
 * \details        This function encrypts the plaintext message using AES-GCM.
 * \param[in,out]  workSpace     (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) updated algorithm context structure
 * \param[in]      input         plaintext message
 * \param[in]      inputLength   the length of the input
 * \param[out]     output        a pointer to the output buffer for the encrypted message
 * \param[in,out]  bytes_out     a pointer to an output counter variable, this variable is incremented by the number
 *                               of bytes that were handled in this iteration of esl_updatePlaintextEncryptGCM
 * \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID           work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL               work space too small
 *                 ESL_ERC_GCM_TOTAL_LENGTH_OVERFLOW  the input exceeded the length limit
 *                 ESL_ERC_NO_ERROR                   else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..),
 *                 output references a valid memory location whose size is a multiple of the GCM block size (16 bytes) and greater or equal than inputLength,
 *                 bytes_out is a valid pointer
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updatePlaintextEncryptGCM(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGCM) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) input,
  const eslt_Length inputLength,
  VSECPRIM_P2VAR_PARA(eslt_Byte) output,
  VSECPRIM_P2VAR_PARA(eslt_Size32) bytes_out);

/**********************************************************************************************************************
 * esl_updateCiphertextDecryptGCM
 *********************************************************************************************************************/
/*!
 * \brief          Decrypts the ciphertext using AES-GCM.
 * \details        This function decrypts the ciphertext using AES-GCM.
 * \param[in,out]  workSpace     (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) updated algorithm context structure
 * \param[in]      input         ciphertext
 * \param[in]      inputLength   the length of the input
 * \param[out]     output        a pointer to the output buffer for the decrypted ciphertext
 * \param[in,out]  bytes_out     a pointer to an output counter variable, this variable is incremented by the number
 *                               of bytes that were handled in this iteration of esl_updateCiphertextDecryptGCM
 * \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID           work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL               work space too small
 *                 ESL_ERC_GCM_TOTAL_LENGTH_OVERFLOW  the input exceeded the length limit
 *                 ESL_ERC_NO_ERROR                   else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..),
 *                 output references a valid memory location whose size is a multiple of the GCM block size (16 bytes) and greater or equal than inputLength,
 *                 bytes_out is a valid pointer
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateCiphertextDecryptGCM(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGCM) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) input,
  const eslt_Length inputLength,
  VSECPRIM_P2VAR_PARA(eslt_Byte) output,
  VSECPRIM_P2VAR_PARA(eslt_Size32) bytes_out);

/**********************************************************************************************************************
 * esl_finalizeEncryptGCM
 *********************************************************************************************************************/
/*!
 * \brief          Finalizes the GCM encryption.
 * \details        This function finalizes the GCM encryption.
 * \param[in,out]  workSpace     (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) updated algorithm context structure
 * \param[out]     output         a pointer to the output buffer for a possible last block, if there is still data in
 *                                the internal buffer, the remaining bytes are written here
 * \param[out]     bytes_out      incremented by number of remaining bytes
 * \param[out]     tag            a pointer to a buffer for the authentication tag
 * \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID           work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL               work space too small
 *                 ESL_ERC_NO_ERROR                   else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..),
 *                 output references a valid memory location whose size is greater or equal than the GCM block size (16 bytes),
 *                 bytes_out is a valid pointer
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeEncryptGCM(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGCM) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Byte) output,
  VSECPRIM_P2VAR_PARA(eslt_Size32) bytes_out,
  VSECPRIM_P2VAR_PARA(eslt_Byte) tag);

/**********************************************************************************************************************
 * esl_finalizeDecryptGCM
 *********************************************************************************************************************/
/*!
 * \brief          Finalizes the GCM operation.
 * \details        This function finalizes the GCM operation.
 * \param[in,out]  workSpace     (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) updated algorithm context structure
 * \param[out]     output         a pointer to the output buffer for a possible last block, if there is still data in
 *                                the internal buffer, the remaining bytes are written here
 * \param[out]     bytes_out      incremented by number of remaining bytes
 * \param[in]      tag            a pointer to a buffer for the authentication tag
 * \return         ESL_ERC_PARAMETER_INVALID            input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID             work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL                 work space too small
 *                 ESL_ERC_GCM_TAG_VERIFICATION_FAILED  the provided tag does not
 *                                                      match the computed one
 *                 ESL_ERC_NO_ERROR                     else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..),
 *                 output references a valid memory location whose size is greater or equal than the GCM block size (16 bytes),
 *                 bytes_out is a valid pointer
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeDecryptGCM(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceGCM) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Byte) output,
  VSECPRIM_P2VAR_PARA(eslt_Size32) bytes_out,
  VSECPRIM_P2CONST_PARA(eslt_Byte) tag);
# endif /* (VSECPRIM_GCM_ENABLED == STD_ON) */

# if (VSECPRIM_SHA1_ENABLED == STD_ON)
/**********************************************************************************************************************
 * esl_initSHA1
 *********************************************************************************************************************/
/*!
 * \brief          Initializes the SHA-1 calculation.
 * \details        This function initializes the SHA-1 hash calculation.
 * \param[in,out]  workSpace     (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 * \return         ESL_ERC_PARAMETER_INVALID  input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL       work space too small
 *                 ESL_ERC_NO_ERROR           else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSHA1(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA1) workSpace);

/**********************************************************************************************************************
 * esl_updateSHA1
 *********************************************************************************************************************/
/*!
 * \brief          This function updates the SHA-1 calculation for the given input message (or parts of it).
 * \details        This function can be called several times until all input to be hashed is handled.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initSHA1(..) function
 *                             (out) updated algorithm context structure
 * \param[in]      inputSize   length of input data in bytes
 * \param[in]      input       pointer to data to be handled
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initSHA1(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSHA1(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA1) workSpace,
const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 * esl_finalizeSHA1
 *********************************************************************************************************************/
/*!
 * \brief          Finalizes the SHA-1 calculation.
 * \details        This function finalizes the SHA-1 calculation by doing the appropriate
 *                 padding of the input value and returning the hash value.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initSHA1(..) function
 *                             (out) updated algorithm context structure
 * \param[in,out]  messageDigest   (in) pointer to buffer for the hash value (20 bytes)
 *                                 (out) pointer to computed hash value
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initSHA1(..), esl_updateSHA1 called before
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSHA1(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA1) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

# endif /* (VSECPRIM_SHA1_ENABLED == STD_ON) */

# if (VSECPRIM_SHA2_256_ENABLED == STD_ON)
/******************************************************************************
 * SHA-224
 ******************************************************************************/

/**********************************************************************************************************************
 * esl_initSHA224
 *********************************************************************************************************************/
/*!
 * \brief          Initializes the SHA-224 calculation.
 * \details        This function initializes the SHA-224 hash calculation.
 * \param[in,out]  workSpace     (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 * \return         ESL_ERC_PARAMETER_INVALID  input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL       work space too small
 *                 ESL_ERC_NO_ERROR           else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSHA224(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA224) workSpace);

/**********************************************************************************************************************
 * esl_updateSHA224
 *********************************************************************************************************************/
/*!
 * \brief          This function updates the SHA-224 calculation for the given input message (or parts of it).
 * \details        This function can be called several times until all input to be hashed is handled.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initSHA224(..) function
 *                             (out) updated algorithm context structure
 * \param[in]      inputSize   length of input data in bytes
 * \param[in]      input       pointer to data to be handled
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initSHA224(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSHA224(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA224) workSpace,
  const eslt_Length inputSize,
  VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 * esl_finalizeSHA224
 *********************************************************************************************************************/
/*!
 * \brief          Finalizes the SHA-224 calculation.
 * \details        This function finalizes the SHA-224 calculation by doing the appropriate
 *                 padding of the input value and returning the hash value.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initSHA224(..) function
 *                             (out) updated algorithm context structure
 * \param[in,out]  messageDigest   (in) pointer to buffer for the hash value (28 bytes)
 *                                 (out) pointer to computed hash value
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initSHA224(..), esl_updateSHA224 called before
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSHA224(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA224) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

/******************************************************************************
 * SHA-256 hash functions                                                     *
 ******************************************************************************/

/**********************************************************************************************************************
 * esl_initSHA256
 *********************************************************************************************************************/
/*!
 * \brief          Initializes the SHA-256 calculation.
 * \details        This function initializes the SHA-256 hash calculation.
 * \param[in,out]  workSpace     (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 * \return         ESL_ERC_PARAMETER_INVALID  input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL       work space too small
 *                 ESL_ERC_NO_ERROR           else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSHA256(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA256) workSpace);

/**********************************************************************************************************************
 * esl_updateSHA256
 *********************************************************************************************************************/
/*!
 * \brief          This function updates the SHA-256 calculation for the given input message (or parts of it).
 * \details        This function can be called several times until all input to be hashed is handled.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initSHA256(..) function
 *                             (out) updated algorithm context structure
 * \param[in]      inputSize   length of input data in bytes
 * \param[in]      input       pointer to data to be handled
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initSHA256(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSHA256(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA256) workSpace,
const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 * esl_finalizeSHA256
 *********************************************************************************************************************/
/*!
 * \brief          Finalizes the SHA-256 calculation.
 * \details        This function finalizes the SHA-256 calculation by doing the appropriate
 *                 padding of the input value and returning the hash value.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initSHA256(..) function
 *                             (out) updated algorithm context structure
 * \param[in,out]  messageDigest   (in) pointer to buffer for the hash value (32 bytes)
 *                                 (out) pointer to computed hash value
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initSHA256(..), esl_updateSHA256 called before
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSHA256(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA256) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);
# endif /* (VSECPRIM_SHA2_256_ENABLED == STD_ON) */

# if (VSECPRIM_SHA2_512_ENABLED == STD_ON)
/******************************************************************************
 * SHA-384
 ******************************************************************************/

/**********************************************************************************************************************
 * esl_initSHA384
 *********************************************************************************************************************/
/*!
 * \brief          Initializes the SHA-384 calculation.
 * \details        This function initializes the SHA-384 hash calculation.
 * \param[in,out]  workSpace     (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 * \return         ESL_ERC_PARAMETER_INVALID  input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL       work space too small
 *                 ESL_ERC_NO_ERROR           else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSHA384(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA384) workSpace);

/**********************************************************************************************************************
 * esl_updateSHA384
 *********************************************************************************************************************/
/*!
 * \brief          This function updates the SHA-384 calculation for the given input message (or parts of it).
 * \details        This function can be called several times until all input to be hashed is handled.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initSHA384(..) function
 *                             (out) updated algorithm context structure
 * \param[in]      inputSize   length of input data in bytes
 * \param[in]      input       pointer to data to be handled
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initSHA384(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSHA384(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA384) workSpace,
  const eslt_Length inputSize,
  VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 * esl_finalizeSHA384
 *********************************************************************************************************************/
/*!
 * \brief          Finalizes the SHA-384 calculation.
 * \details        This function finalizes the SHA-384 calculation by doing the appropriate
 *                 padding of the input value and returning the hash value.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initSHA384(..) function
 *                             (out) updated algorithm context structure
 * \param[in,out]  messageDigest   (in) pointer to buffer for the hash value (48 bytes)
 *                                 (out) pointer to computed hash value
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initSHA384(..), esl_updateSHA384 called before
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSHA384(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA384) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

/******************************************************************************
 * SHA-512 hash functions
 ******************************************************************************/

/**********************************************************************************************************************
 * esl_initSHA512
 *********************************************************************************************************************/
/*!
 * \brief          Initializes the SHA-512 calculation.
 * \details        This function initializes the SHA-512 hash calculation.
 * \param[in,out]  workSpace     (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 * \return         ESL_ERC_PARAMETER_INVALID  input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL       work space too small
 *                 ESL_ERC_NO_ERROR           else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSHA512(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA512) workSpace);

/**********************************************************************************************************************
 * esl_updateSHA512
 *********************************************************************************************************************/
/*!
 * \brief          This function updates the SHA-512 calculation for the given input message (or parts of it).
 * \details        This function can be called several times until all input to be hashed is handled.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initSHA512(..) function
 *                             (out) updated algorithm context structure
 * \param[in]      inputSize   length of input data in bytes
 * \param[in]      input       pointer to data to be handled
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initSHA512(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSHA512(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA512) workSpace,
const eslt_Length inputSize,
VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 * esl_finalizeSHA512
 *********************************************************************************************************************/
/*!
 * \brief          Finalizes the SHA-512 calculation.
 * \details        This function finalizes the SHA-512 calculation by doing the appropriate
 *                 padding of the input value and returning the hash value.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initSHA512(..) function
 *                             (out) updated algorithm context structure
 * \param[in,out]  messageDigest   (in) pointer to buffer for the hash value (64 bytes)
 *                                 (out) pointer to computed hash value
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initSHA512(..), esl_updateSHA512 called before
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSHA512(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA512) workSpace,
VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

/**********************************************************************************************************************
 * esl_initSHA512_224
 *********************************************************************************************************************/
/*!
 * \brief          Initializes the SHA-512-224 calculation.
 * \details        This function initializes the SHA-512-224 hash calculation.
 * \param[in,out]  workSpace     (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 * \return         ESL_ERC_PARAMETER_INVALID  input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL       work space too small
 *                 ESL_ERC_NO_ERROR           else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSHA512_224(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA512_224) workSpace);

/**********************************************************************************************************************
 * esl_updateSHA512_224
 *********************************************************************************************************************/
/*!
 * \brief          This function updates the SHA-512-224 calculation for the given input message (or parts of it).
 * \details        This function can be called several times until all input to be hashed is handled.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initSHA512_224(..) function
 *                             (out) updated algorithm context structure
 * \param[in]      inputSize   length of input data in bytes
 * \param[in]      input       pointer to data to be handled
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initSHA512_224(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSHA512_224(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA512_224) workSpace,
const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 * esl_finalizeSHA512_224
 *********************************************************************************************************************/
/*!
 * \brief          Finalizes the SHA-512-224 calculation.
 * \details        This function finalizes the SHA-512-224 calculation by doing the appropriate
 *                 padding of the input value and returning the hash value.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initSHA512_224(..) function
 *                             (out) updated algorithm context structure
 * \param[in,out]  messageDigest   (in) pointer to buffer for the hash value (28 bytes)
 *                                 (out) pointer to computed hash value
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initSHA512_224(..), esl_updateSHA512_224 called before
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSHA512_224(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA512_224) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

/**********************************************************************************************************************
 * esl_initSHA512_256
 *********************************************************************************************************************/
/*!
 * \brief          Initializes the SHA-512-256 calculation.
 * \details        This function initializes the SHA-512-256 hash calculation.
 * \param[in,out]  workSpace     (in) algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 * \return         ESL_ERC_PARAMETER_INVALID  input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL       work space too small
 *                 ESL_ERC_NO_ERROR           else
 * \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSHA512_256(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA512_256) workSpace);

/**********************************************************************************************************************
 * esl_updateSHA512_256
 *********************************************************************************************************************/
/*!
 * \brief          This function updates the SHA-512-256 calculation for the given input message (or parts of it).
 * \details        This function can be called several times until all input to be hashed is handled.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initSHA512_256(..) function
 *                             (out) updated algorithm context structure
 * \param[in]      inputSize   length of input data in bytes
 * \param[in]      input       pointer to data to be handled
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initSHA512_256(..)
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSHA512_256(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA512_256) workSpace,
const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 * esl_finalizeSHA512_256
 *********************************************************************************************************************/
/*!
 * \brief          Finalizes the SHA-512-256 calculation.
 * \details        This function finalizes the SHA-512-256 calculation by doing the appropriate
 *                 padding of the input value and returning the hash value.
 * \param[in,out]  workSpace   (in) algorithm context buffer, initialized by esl_initSHA512_256(..) function
 *                             (out) updated algorithm context structure
 * \param[in,out]  messageDigest   (in) pointer to buffer for the hash value (32 bytes)
 *                                 (out) pointer to computed hash value
 * \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_NO_ERROR          else
 * \pre            workSpace is initialized by esl_initSHA512_256(..), esl_updateSHA512_256 called before
 * \context        TASK
 * \reentrant      TRUE, for different workspaces
 * \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSHA512_256(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA512_256) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

# endif /* (VSECPRIM_SHA2_512_ENABLED == STD_ON) */

# if (VSECPRIM_RIPEMD160_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * RIPEMD-160 hash functions
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initRIPEMD160()
 *********************************************************************************************************************/
/*! \brief          This function initializes the RIPEMD-160 hash calculation.
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initRIPEMD160(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160) workSpace);

/**********************************************************************************************************************
 *  esl_updateRIPEMD160()
 *********************************************************************************************************************/
/*! \brief          This function updates the RIPEMD-160 calculation for the given
 *                  input message (or parts of it). This function can be called
 *                  several times until all input to be hashed is handled.
 *  \details        -
 *  \param[in]      input        pointer to data to be handled
 *  \param[in]      inputSize    length of the input in bytes
 *  \param[in,out]  workSpace    (in) length of input data in bytes
 *                               (out) actualized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_RIPEMD160_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initRIPEMD160(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateRIPEMD160(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160) workSpace,
const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeRIPEMD160()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the RIPEMD-160 calculation by doing the appropriate
 *                  padding of the input value and returning the hash value.
 *  \details        -
 *  \param[in,out]  messageDigest  (in) pointer to buffer for the hash value (20 bytes)
 *                               (out) pointer to computed hash value
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initRIPEMD160(..) function
 *                               (out) actualized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initRIPEMD160(..)
 *                  esl_updateRIPEMD160 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeRIPEMD160(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

# endif /* (VSECPRIM_RIPEMD160_ENABLED == STD_ON) */

# if (VSECPRIM_MD5_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * MD5 hash functions
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initMD5()
 *********************************************************************************************************************/
/*! \brief          This function initializes the MD5 hash calculation.
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initMD5(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceMD5) workSpace);

/**********************************************************************************************************************
 *  esl_updateMD5()
 *********************************************************************************************************************/
/*! \brief          This function updates the MD5 calculation for the given
 *                  input message (or parts of it). This function can be called
 *                  several times until all input to be hashed is handled.
 *  \details        -
 *  \param[in]      input        pointer to data to be handled
 *  \param[in]      inputSize    length of the input in bytes
 *  \param[in,out]  workSpace    (in) length of input data in bytes
 *                               (out) actualized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initMD5(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateMD5(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceMD5) workSpace,
const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeMD5()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the MD5 calculation by doing the appropriate
 *                  padding of the input value and returning the hash value.
 *  \details        -
 *  \param[in,out]  messageDigest  (in) pointer to buffer for the hash value (20 bytes)
 *                               (out) pointer to computed hash value
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initMD5(..) function
 *                               (out) actualized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initMD5(..)
 *                  esl_updateMD5 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeMD5(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceMD5) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);
# endif /* (VSECPRIM_MD5_ENABLED == STD_ON) */

# if (VSECPRIM_HMAC_SHA1_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * HashMACSHA-1 crypto functions (according to RFC 2104 with SHA-1)
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_generateKeyHashMACSHA1()
 *********************************************************************************************************************/
/*! \brief           This function generates a HMAC key for an HashMac calculation.
 *  \details        -
 *  \param[in,out]  key          (in) pointer to user key buffer of size keyLength
 *                               (out) pointer to the generated key
 *  \param[in]      keyLength    the byte length of the key to be generated
 *  \return         esl_getBytesRNG(..) return values are passed through
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateKeyHashMACSHA1(
const eslt_Length keyLength, VSECPRIM_P2VAR_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_initHashMACSHA1()
 *********************************************************************************************************************/
/*! \brief          This function initializes the HashMAC calculation according
 *                  to RFC2104 based on the hash function SHA-1.
 *  \details        -
 *  \param[in]      key          pointer to user key
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      keyLength    the byte length of the key to be used
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_HMAC_KEY_LENGTH_OUT_OF_RANGE      the given key length is zero
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initHashMACSHA1(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA1) workSpace,
const eslt_Length keyLength, VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_updateHashMACSHA1()
 *********************************************************************************************************************/
/*! \brief          This function updates the HashMAC calculation according
 *                  to RFC2104 based on the hash function SHA-1.
 *  \details        -
 *  \param[in]      input        pointer to data to be handled
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initHashMACSHA1(..) function
 *                               (out) updated algorithm context structure
 *  \param[in]      inputLength  length of input data in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initHashMACSHA1(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateHashMACSHA1(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA1) workSpace,
const eslt_Length inputLength, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeHashMACSHA1()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the HashMAC calculation by returning
 *                  the calculated HashMAC according to RFC2104 based on SHA-1
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initHashMACSHA1(..) function
 *                               (out) updated algorithm context structure
 *  \param[in,out]  messageHashMAC  (in) pointer to buffer for HashMAC (at least 20 bytes)
 *                               (out) pointer to computed HashMAC
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initHashMACSHA1(..)
 *                  esl_updateHashMACSHA1 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashMACSHA1(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA1) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageHashMAC);

/**********************************************************************************************************************
 *  esl_verifyHashMACSHA1()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the verification of a given HashMAC
 *                  according to RFC2104 based on SHA-1.
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initHashMACSHA1(..) function
 *                               (out) updated algorithm context structure
 *  \param[in]      messageHashMAC  pointer to the given HashMAC (20 bytes)
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_HMAC_INCORRECT_MAC   the given HashMAC is incorrect
 *                  ESL_ERC_NO_ERROR          HashMAC is correct
 *  \pre            workSpace is initialized by esl_initHashMACSHA1(..)
 *                  esl_updateHashMACSHA1 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifyHashMACSHA1(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA1) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) messageHashMAC);
# endif /* (VSECPRIM_HMAC_SHA1_ENABLED == STD_ON) */

# if (VSECPRIM_HMAC_RMD160_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * HashMACRIPEMD160 crypto functions (according to RFC 2104 with RIPEMD-160)
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_generateKeyHashMACRIPEMD160()
 *********************************************************************************************************************/
/*! \brief           This function generates a HMAC key for an HashMac calculation.
 *  \details        -
 *  \param[in,out]  key          (in) pointer to user key buffer of size keyLength
 *                               (out) pointer to the generated key
 *  \param[in]      keyLength    the byte length of the key to be generated
 *  \return         esl_getBytesRNG(..) return values are passed through
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateKeyHashMACRIPEMD160(
const eslt_Length keyLength, VSECPRIM_P2VAR_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_initHashMACRIPEMD160()
 *********************************************************************************************************************/
/*! \brief          This function initializes the HashMAC calculation according
 *                  to RFC2104 based on the hash function RIPEMD-160.
 *  \details        -
 *  \param[in]      key          pointer to user key
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      keyLength    the byte length of the key to be used
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_HMAC_KEY_LENGTH_OUT_OF_RANGE      the given key length is zero
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initHashMACRIPEMD160(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACRIPEMD160) workSpace,
const eslt_Length keyLength, VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_updateHashMACRIPEMD160()
 *********************************************************************************************************************/
/*! \brief          This function updates the HashMAC calculation according
 *                  to RFC2104 based on the hash function RIPEMD-160.
 *  \details        -
 *  \param[in]      input        pointer to data to be handled
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initHashMACRIPEMD160(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in]      inputLength  length of input data in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_RIPEMD160_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initHashMACRIPEMD160(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateHashMACRIPEMD160(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACRIPEMD160) workSpace,
const eslt_Length inputLength, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeHashMACRIPEMD160()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the HashMAC calculation by returning
 *                  the calculated HashMAC according to RFC2104 based on RIPEMD-160
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initHashMACRIPEMD160(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in,out]  messageHashMAC  (in) pointer to buffer for HashMAC (at least 20 bytes)
 *                               (out) pointer to computed HashMAC
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initHashMACRIPEMD160(..)
 *                  esl_updateHashMACRIPEMD160 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashMACRIPEMD160(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACRIPEMD160) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageHashMAC);

/**********************************************************************************************************************
 *  esl_verifyHashMACRIPEMD160()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the verification of a given HashMAC
 *                  according to RFC2104 based on RIPEMD-160.
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initHashMACRIPEMD160(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in]      messageHashMAC  pointer to the given HashMAC (20 bytes)
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_HMAC_INCORRECT_MAC   the given HashMAC is incorrect
 *                  ESL_ERC_NO_ERROR          HashMAC is correct
 *  \pre            workSpace is initialized by esl_initHashMACRIPEMD160(..)
 *                  esl_updateHashMACRIPEMD160 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifyHashMACRIPEMD160(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACRIPEMD160) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) messageHashMAC);
# endif /* (VSECPRIM_HMAC_RMD160_ENABLED == STD_ON) */

# if (VSECPRIM_HMAC_SHA2_256_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * HashMACSHA256 crypto functions (according to RFC 2104 with SHA-256)
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_generateKeyHashMACSHA256()
 *********************************************************************************************************************/
/*! \brief           This function generates a HMAC key for an HashMac calculation.
 *  \details        -
 *  \param[in,out]  key          (in) pointer to user key buffer of size keyLength
 *                               (out) pointer to the generated key
 *  \param[in]      keyLength    the byte length of the key to be generated
 *  \return         esl_getBytesRNG(..) return values are passed through
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateKeyHashMACSHA256(
const eslt_Length keyLength, VSECPRIM_P2VAR_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_initHashMACSHA256()
 *********************************************************************************************************************/
/*! \brief          This function initializes the HashMAC calculation according
 *                  to RFC2104 based on the hash function SHA-256.
 *  \details        -
 *  \param[in]      key          pointer to user key
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      keyLength    the byte length of the key to be used
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_HMAC_KEY_LENGTH_OUT_OF_RANGE      the given key length is zero
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initHashMACSHA256(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA256) workSpace,
const eslt_Length keyLength, VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_updateHashMACSHA256()
 *********************************************************************************************************************/
/*! \brief          This function updates the HashMAC calculation according
 *                  to RFC2104 based on the hash function SHA-256.
 *  \details        -
 *  \param[in]      input        pointer to data to be handled
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initHashMACSHA256(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in]      inputLength  length of input data in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_SHA256_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initHashMACSHA256(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateHashMACSHA256(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA256) workSpace,
const eslt_Length inputLength, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeHashMACSHA256()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the HashMAC calculation by returning
 *                  the calculated HashMAC according to RFC2104 based on SHA-256
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initHashMACSHA256(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in,out]  messageHashMAC  (in) pointer to buffer for HashMAC (at least 32 bytes)
 *                               (out) pointer to computed HashMAC
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initHashMACSHA256(..)
 *                  esl_updateHashMACSHA256 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashMACSHA256(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA256) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageHashMAC);

/**********************************************************************************************************************
 *  esl_verifyHashMACSHA256()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the verification of a given HashMAC
 *                  according to RFC2104 based on SHA-256.
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initHashMACSHA256(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in]      messageHashMAC  pointer to the given HashMAC (32 bytes)
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_HMAC_INCORRECT_MAC   the given HashMAC is incorrect
 *                  ESL_ERC_NO_ERROR          HashMAC is correct
 *  \pre            workSpace is initialized by esl_initHashMACSHA256(..)
 *                  esl_updateHashMACSHA256 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifyHashMACSHA256(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA256) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) messageHashMAC);
# endif /* (VSECPRIM_HMAC_SHA2_256_ENABLED == STD_ON) */

# if (VSECPRIM_HMAC_SHA2_384_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * HashMACSHA384 crypto functions (according to RFC 2104 with SHA-384)
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_generateKeyHashMACSHA384()
 *********************************************************************************************************************/
/*! \brief           This function generates a HMAC key for an HashMac calculation.
 *  \details        -
 *  \param[in,out]  key          (in) pointer to user key buffer of size keyLength
 *                               (out) pointer to the generated key
 *  \param[in]      keyLength    the byte length of the key to be generated
 *  \return         esl_getBytesRNG(..) return values are passed through
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateKeyHashMACSHA384(
  const eslt_Length keyLength, VSECPRIM_P2VAR_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_initHashMACSHA384()
 *********************************************************************************************************************/
/*! \brief          This function initializes the HashMAC calculation according
 *                  to RFC2104 based on the hash function SHA-384.
 *  \details        -
 *  \param[in]      key          pointer to user key
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      keyLength    the byte length of the key to be used
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_HMAC_KEY_LENGTH_OUT_OF_RANGE      the given key length is zero
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initHashMACSHA384(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA384) workSpace,
  const eslt_Length keyLength, VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_updateHashMACSHA384()
 *********************************************************************************************************************/
/*! \brief          This function updates the HashMAC calculation according
 *                  to RFC2104 based on the hash function SHA-384.
 *  \details        -
 *  \param[in]      input        pointer to data to be handled
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initHashMACSHA384(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in]      inputLength  length of input data in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_SHA384_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initHashMACSHA384(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateHashMACSHA384(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA384) workSpace,
  const eslt_Length inputLength, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeHashMACSHA384()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the HashMAC calculation by returning
 *                  the calculated HashMAC according to RFC2104 based on SHA-384
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initHashMACSHA384(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in,out]  messageHashMAC  (in) pointer to buffer for HashMAC (at least 32 bytes)
 *                               (out) pointer to computed HashMAC
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initHashMACSHA384(..)
 *                  esl_updateHashMACSHA384 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashMACSHA384(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA384) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageHashMAC);

/**********************************************************************************************************************
 *  esl_verifyHashMACSHA384()
 *********************************************************************************************************************/
/*! \brief          This function finalizes the verification of a given HashMAC
 *                  according to RFC2104 based on SHA-384.
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initHashMACSHA384(..) function
 *                               (out) actualized algorithm context structure
 *  \param[in]      messageHashMAC  pointer to the given HashMAC (32 bytes)
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_HMAC_INCORRECT_MAC   the given HashMAC is incorrect
 *                  ESL_ERC_NO_ERROR          HashMAC is correct
 *  \pre            workSpace is initialized by esl_initHashMACSHA384(..)
 *                  esl_updateHashMACSHA384 called before
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifyHashMACSHA384(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHMACSHA384) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) messageHashMAC);
# endif /* (VSECPRIM_HMAC_SHA2_384_ENABLED == STD_ON) */

# if (VSECPRIM_CMAC_ENABLED == STD_ON)
/***********************************************************************************************************************
 *  esl_initCMACAES128
 **********************************************************************************************************************/
/*! \brief         Initialize CMAC calculation
 *  \details       This function initializes the CMAC calculation workspace.
 *  \param[in,out] workSpace  CMAC work space
 *  \param[in]     keyLength  Size of AES key to be used
 *  \param[in]     key  AES key to be used
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_HMAC_KEY_LENGTH_OUT_OF_RANGE  AES key length is invalid
 *  \pre           Pointers must be valid
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces.
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initCMACAES128(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceCMACAES) workSpace,
  const eslt_Length keyLength, VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/***********************************************************************************************************************
 *  esl_initCMACAES128
 **********************************************************************************************************************/
/*! \brief         Initialize CMAC calculation with a pre-calculated CMAC expanded key.
 *  \details       This function initializes the CMAC calculation workspace using a pre-calculated CMAC expanded key.
 *                 This will speed up the CMAC verification process.
 *  \param[in]     workSpace  CMAC work space
 *  \param[in]     expandedKey  CMAC expanded key which shall be used
 *  \param[in]     expandedKeyLength  Size of provided key
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_HMAC_KEY_LENGTH_OUT_OF_RANGE  AES key length is invalid
 *  \pre           Pointers must be valid
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces.
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initExpandedCMACAES128(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceCMACAES) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) expandedKey, const eslt_Length expandedKeyLength);

/***********************************************************************************************************************
 *  esl_getExpandedKeyCMACAES128
 **********************************************************************************************************************/
/*! \brief         Extracts the expanded CMAC key.
 *  \details       This function extracts the expanded CMAC key from the given workspace. Re-using the expanded key
 *                 on a system with constant key usage will speed up the CMAC calculation.
 *  \param[in]     workSpace  CMAC work space
 *  \param[out]    keyPtr  Pointer to buffer where the expanded key shall be stored.
 *  \param[in,out] keyLengthPtr  In  Size of provided buffer, Out  Size of written data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_WS_STATE_INVALID  Work space state invalid
 *                 ESL_ERC_INPUT_INVALID  Invalid input length
 *  \pre           Workspace needs to be initialized.
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces.
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_getExpandedKeyCMACAES128(
  VSECPRIM_P2CONST_PARA(eslt_WorkSpaceCMACAES) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Byte) keyPtr, VSECPRIM_P2VAR_PARA(eslt_Length) keyLengthPtr);

/***********************************************************************************************************************
 *  esl_updateCMACAES128
 **********************************************************************************************************************/
/*! \brief         Update CMAC calculation.
 *  \details       This function is used to feed the CMAC calculation with input data.
 *  \param[in,out] workSpace  CMAC work space
 *  \param[in]     inputLength  Length of input data
 *  \param[in]     input  Pointer to input data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Work space state invalid
 *                 ESL_ERC_INPUT_INVALID  Invalid input length
 *  \pre           Workspace needs to be initialized.
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateCMACAES128(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceCMACAES) workSpace,
  const eslt_Length inputLength, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/***********************************************************************************************************************
 *  esl_finalizeCMACAES128
 **********************************************************************************************************************/
/*! \brief         Finalize CMAC calculation.
 *  \details       This function finalizes the MAC calculation.
 *  \param[in,out] workSpace  CMAC work space
 *  \param[out]    messageCMAC  Pointer to computed CMAC. The size must be 16 bytes.
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_WS_STATE_INVALID  Work space state invalid
 *                 ESL_ERC_INPUT_INVALID  Invalid input length
 *  \pre           Workspace needs to be initialized.
 *                 messageCMAC must point to valid memory of at least 16 bytes
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces.
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeCMACAES128(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceCMACAES) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageCMAC);

/***********************************************************************************************************************
 *  esl_verifyCMACAES128
 **********************************************************************************************************************/
/*! \brief         Verify given CMAC.
 *  \details       This function verifies the provided CMAC against the calculated one.
 *  \param[in,out] workSpace  CMAC work space
 *  \param[in]     messageCMAC  Pointer to CMAC to be compared. The size must be 16 bytes.
 *  \return        ESL_ERC_NO_ERROR  CMAC is correct
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_WS_STATE_INVALID  Work space state invalid
 *  \pre           Workspace needs to be initialized.
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces.
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifyCMACAES128(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceCMACAES) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) messageCMAC);

# endif /* (VSECPRIM_CMAC_ENABLED == STD_ON) */

# if (VSECPRIM_SIP_HASH_ENABLED == STD_ON)
/***********************************************************************************************************************
 *  esl_initSipHash
 **********************************************************************************************************************/
/*! \brief         Initializes SipHash calculation.
 *  \details       This function initializes SipHash calculation.
 *  \param[in,out] workSpace  SipHash work space
 *  \param[in]     keyLength  Size of SipHash key to be used
 *  \param[in]     key  SipHash key to be used
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_HMAC_KEY_LENGTH_OUT_OF_RANGE  SipHash key length is invalid
 *  \pre           Pointers must be valid
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces.
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSipHash(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSipHash) workSpace,
  const eslt_Length keyLength,
  VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/***********************************************************************************************************************
 *  esl_updateSipHash
 **********************************************************************************************************************/
/*! \brief         Updates SipHash calculation.
 *  \details       This function updates SipHash calculation.
 *  \param[in,out] workSpace  SipHash work space
 *  \param[in]     inputLength  Length of input data
 *  \param[in]     input  Pointer to input data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_WS_STATE_INVALID  Work space state invalid
 *                 ESL_ERC_INPUT_INVALID  Invalid input length
 *  \pre           Pointers must be valid
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces.
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSipHash(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSipHash) workSpace,
  const eslt_Length inputLength,
  VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/***********************************************************************************************************************
 *  esl_finalizeSipHash
 **********************************************************************************************************************/
/*! \brief         Finalizes SipHash calculation.
 *  \details       This function finalizes SipHash calculation.
 *  \param[in,out] workSpace  SipHash work space
 *  \param[out]    messageMAC  Pointer to computed MAC. Size must be 8 bytes.
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_WS_STATE_INVALID  Work space state invalid
 *  \pre           Pointers must be valid
 *                 messageMAC must point to valid memory of at least 8 bytes
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces.
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSipHash(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSipHash) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageMAC);

/***********************************************************************************************************************
 *  esl_verifySipHash
 **********************************************************************************************************************/
/*! \brief         Verifies given SipHash against calculated one.
 *  \details       This function verifies given SipHash against calculated one.
 *  \param[in,out] workSpace  SipHash work space
 *  \param[in]     messageMAC  Pointer to SipHash to be compared. Size must be 8 bytes.
 *  \return        ESL_ERC_NO_ERROR  SipHash is correct
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_WS_STATE_INVALID  Work space state invalid
 *                 ESL_ERC_HMAC_INCORRECT_MAC   the given HashMAC is incorrect
 *  \pre           Pointers must be valid
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces.
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySipHash(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSipHash) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) messageMAC);
# endif /* (VSECPRIM_SIP_HASH_ENABLED == STD_ON) */

/*****************************************************************************
 * Key derivation function (KDF) according to ANSI X9.63
 *****************************************************************************/
# if (VSECPRIM_ANSI_X963_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/***********************************************************************************************************************
 *  esl_initKDFX963SHA1
 **********************************************************************************************************************/
/*! \brief         Initializes KDF X9.63 with SHA1 calculation.
 *  \details       This function initializes KDF X9.63 with SHA1 calculation.
 *  \param[in,out] workSpace  KDF X9.63 with SHA1 work space
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *  \pre           workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces.
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initKDFX963SHA1(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceKDFX963SHA1) workSpace);

/***********************************************************************************************************************
 *  esl_deriveKeyKDFX963SHA1
 **********************************************************************************************************************/
/*! \brief         Derive Key according KDF X9.63 with SHA1
 *  \details       This function derives a key according to KDF X9.63 with SHA1.
 *  \param[in,out] workSpace  KDF X9.63 with SHA1 work space
 *  \param[in]     secretLength   length of secret in bytes
 *  \param[in]     secret         Pointer to the secret
 *  \param[in]     infoLength     length of salt in bytes (if zero, no salt is used)
 *  \param[in]     info           pointer to the optional salt (if NULL, no salt is used)
 *  \param[in]     keyLength      length of output key buffer in bytes
 *  \param[out]    key            pointer to the output key buffer
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_WS_STATE_INVALID  Work space state invalid
 *                 ESL_ERC_INPUT_INVALID  Invalid input length
 *  \pre           workSpace is initialized by esl_initKDFX963SHA1(..)
 *                 key need to have at least the length of keyLength.
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces.
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_deriveKeyKDFX963SHA1(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceKDFX963SHA1) workSpace,
  const eslt_Length secretLength, VSECPRIM_P2CONST_PARA(eslt_Byte) secret,
  const eslt_Length infoLength, VSECPRIM_P2CONST_PARA(eslt_Byte) info,
  const eslt_Length keyLength, VSECPRIM_P2VAR_PARA(eslt_Byte) key);
# endif /* (VSECPRIM_ANSI_X963_ENABLED == STD_ON) */

# if (VSECPRIM_ANSI_X963_SHA256_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/***********************************************************************************************************************
 *  esl_initKDFX963SHA256
 **********************************************************************************************************************/
/*! \brief         Initializes KDF X9.63 with SHA256 calculation.
 *  \details       This function initializes KDF X9.63 with SHA256 calculation.
 *  \param[in,out] workSpace  KDF X9.63 with SHA256 work space
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *  \pre           workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces.
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initKDFX963SHA256(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceKDFX963SHA256) workSpace);

/***********************************************************************************************************************
 *  esl_deriveKeyKDFX963SHA256
 **********************************************************************************************************************/
/*! \brief         Derive Key according KDF X9.63 with SHA256
 *  \details       This function derives a key according to KDF X9.63 with SHA256.
 *  \param[in,out] workSpace  KDF X9.63 with SHA256 work space
 *  \param[in]     secretLength   length of secret in bytes
 *  \param[in]     secret         Pointer to the secret
 *  \param[in]     infoLength     length of salt in bytes (if zero, no salt is used)
 *  \param[in]     info           pointer to the optional salt (if NULL, no salt is used)
 *  \param[in]     keyLength      length of output key buffer in bytes
 *  \param[out]    key            pointer to the output key buffer
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_WS_STATE_INVALID  Work space state invalid
 *                 ESL_ERC_INPUT_INVALID  Invalid input length
 *  \pre           workSpace is initialized by esl_initKDFX963SHA256(..)
 *                 Length of the buffer provided by key need to have at least keyLength.
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces.
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_deriveKeyKDFX963SHA256(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceKDFX963SHA256) workSpace,
  const eslt_Length secretLength, VSECPRIM_P2CONST_PARA(eslt_Byte) secret,
  const eslt_Length infoLength, VSECPRIM_P2CONST_PARA(eslt_Byte) info,
  const eslt_Length keyLength, VSECPRIM_P2VAR_PARA(eslt_Byte) key);
# endif /* (VSECPRIM_ANSI_X963_SHA256_ENABLED == STD_ON) */

# if (VSECPRIM_PKCS5_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto functions KEY-derive according to PKCS#5 v2.0
 *****************************************************************************/

 /**********************************************************************************************************************
 *  esl_initKDF2HMACSHA1()
 *********************************************************************************************************************/
/*! \brief          Initialize the key derivation.
 *  \details        -
 *  \param[in]      iterationCount  number of (SHA-1) iterations
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_KDF_ITERATION_COUNT_OUT_OF_RANGE
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initKDF2HMACSHA1(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceKDF2HMACSHA1) workSpace, const eslt_Length iterationCount);


/***********************************************************************************************************************
 *  esl_deriveKeyKDF2HMACSHA1
 **********************************************************************************************************************/
/*! \brief         Derive Key according KDF2 with SHA1
 *  \details       -
 *  \param[in,out] workSpace algorithm context buffer, initialized by
 *                           esl_initWorkSpaceHeader(..) function
 *  \param[in]     secretLength   length of secret in bytes
 *  \param[in]     secret         Pointer to the secret
 *  \param[in]     infoLength     length of the shared info (bytes)
 *  \param[in]     info           pointer to the shared info
 *  \param[in]     keyLength      length of output key buffer in bytes
 *  \param[out]    key            pointer to the output key buffer
 *  \return        ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL      work space too small
 *                 ESL_ERC_WS_STATE_INVALID Invalid state
 *                 ESL_ERC_NO_ERROR          else
 *  \pre           workSpace is initialized by esl_initKDF2HMACSHA1(..)
 *                 key need to have at least the length of keyLength.
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces.
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_deriveKeyKDF2HMACSHA1(
VSECPRIM_P2VAR_PARA(eslt_WorkSpaceKDF2HMACSHA1) workSpace,
const eslt_Length secretLength, VSECPRIM_P2CONST_PARA(eslt_Byte) secret,
const eslt_Length infoLength, VSECPRIM_P2CONST_PARA(eslt_Byte) info,
const eslt_Length keyLength, VSECPRIM_P2VAR_PARA(eslt_Byte) key);

# endif /* (VSECPRIM_PKCS5_ENABLED == STD_ON) */
# if (VSECPRIM_ESLGETLENGTHECP_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF xf xf */
/*****************************************************************************
 * Crypto functions ECP (Ecc over Gf(p))
 *****************************************************************************/

/****************************************************************************
 * LengthOf-Functions
 ****************************************************************************
 * They can compute the bytelength of the i-n and outputparameters of EcP
 * functions */

/**********************************************************************************************************************
 *  esl_getMaxLengthOfEcPmessage()
 *********************************************************************************************************************/
/*! \brief          This functions calculate some usefull length values of ECC function
 *                  parameters. All sizes are returned in bytes.
 *  \details        -
 *  \param[in]      domain       pointer to domain parameter structure
 *  \return         0 if domain==NULL or domain decoding error,
 *                  length of desired parameter else
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_Length) esl_getMaxLengthOfEcPmessage(VSECPRIM_P2ROMCONST_PARA(eslt_EccDomain) domain);

/**********************************************************************************************************************
 *  esl_getLengthOfEcPpublicKey_comp()
 *********************************************************************************************************************/
/*! \brief          This functions calculate some usefull length values of ECC function
 *                  parameters. All sizes are returned in bytes.
 *  \details        -
 *  \param[in]      domain       pointer to domain parameter structure
 *  \return         0 if domain==NULL or domain decoding error,
 *                  length of desired parameter else
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_Length) esl_getLengthOfEcPpublicKey_comp(VSECPRIM_P2ROMCONST_PARA(eslt_EccDomain) domain);

/**********************************************************************************************************************
 *  esl_getLengthOfEcPprivateKey()
 *********************************************************************************************************************/
/*! \brief          This functions calculate some usefull length values of ECC function
 *                  parameters. All sizes are returned in bytes.
 *  \details        -
 *  \param[in]      domain       pointer to domain parameter structure
 *  \return         0 if domain==NULL or domain decoding error,
 *                  length of desired parameter else
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_Length) esl_getLengthOfEcPprivateKey(VSECPRIM_P2ROMCONST_PARA(eslt_EccDomain) domain);

/**********************************************************************************************************************
 *  esl_getLengthOfEcPsignature_comp()
 *********************************************************************************************************************/
/*! \brief          This functions calculate some usefull length values of ECC function
 *                  parameters. All sizes are returned in bytes.
 *  \details        -
 *  \param[in]      domain       pointer to domain parameter structure
 *  \return         0 if domain==NULL or domain decoding error,
 *                  length of desired parameter else
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_Length) esl_getLengthOfEcPsignature_comp(VSECPRIM_P2ROMCONST_PARA(eslt_EccDomain) domain);

/**********************************************************************************************************************
 *  esl_getLengthOfEcPsecret_comp()
 *********************************************************************************************************************/
/*! \brief          This functions calculate some usefull length values of ECC function
 *                  parameters. All sizes are returned in bytes.
 *  \details        -
 *  \param[in]      domain       pointer to domain parameter structure
 *  \return         0 if domain==NULL or domain decoding error,
 *                  length of desired parameter else
 *  \pre            -
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_Length) esl_getLengthOfEcPsecret_comp(VSECPRIM_P2ROMCONST_PARA(eslt_EccDomain) domain);
# endif /* (VSECPRIM_ESLGETLENGTHECP_ENABLED == STD_ON) */

# if (VSECPRIM_ECCENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto functions Key Generation based on EC-DH Generic
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initGenerateDSAEcP_prim()
 *********************************************************************************************************************/
/*! \brief          This function initializes the EcP key generation.
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      domain       pointer to domain parameter structure
 *  \param[in]      domainExt    pointer to domain parameter extension structure
 *  \param[in]      speedUpExt   pointer to precomputation structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_ECC_DOMAIN_INVALID      domain structure is invalid
 *                  ESL_ERC_ECC_DOMAINEXT_INVALID   domainExt structure is invalid
 *                  ESL_ERC_ECC_SPEEDUPEXT_INVALID  speedUpExt structure is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initGenerateKeyEcP_prim(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEcP) workSpace, VSECPRIM_P2ROMCONST_PARA(eslt_EccDomain) domain,
  VSECPRIM_P2ROMCONST_PARA(eslt_EccDomainExt) domainExt, VSECPRIM_P2ROMCONST_PARA(eslt_EccSpeedUpExt) speedUpExt);

/**********************************************************************************************************************
 *  esl_generateKeyEcP_prim()
 *********************************************************************************************************************/
/*! \brief          This function generates an ECC key pair
 *                      (d=privateKey, Q=(publicKey_x, publicKey_y))
 *                     if (secret_x == publicKey_x) secret_x will replace publicKey_x
 *                     if (secret_y == publicKey_y) secret_y will replace publicKey_y
 *  \details        -
 *  \param[in,out]  publicKey_x  (in) buffer to store x-coordinate of the public key
 *                               (out) x-coordinate of the public key Q
 *  \param[in,out]  publicKey_y  (in) buffer to store y-coordinate of the public key
 *                               (both of length getLengthOfEcPpublicKey_comp(domain))
 *                               (out) y-coordinate of the public key Q
 *  \param[in,out]  privateKey   (in) buffer to store the private key d
 *                               (of length getLengthOfEcPprivateKey(domain))
 *                               (out) the private key d
 *  \param[in,out] workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \return         ESL_ERC_PARAMETER_INVALID       input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID        wrong work space state
 *                  ESL_ERC_NO_ERROR                else
 *  \pre            workSpace is initialized by esl_initGenerateKeyEcP_prim(..)

 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateKeyEcP_prim(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEcP) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) privateKey,
  VSECPRIM_P2VAR_PARA(eslt_Byte) publicKey_x, VSECPRIM_P2VAR_PARA(eslt_Byte) publicKey_y);
# endif /* (VSECPRIM_ECCENABLED == STD_ON) */

# if (VSECPRIM_ECDSA_GENERIC_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto functions Digital Signature Generation / Verification based on EC-DH Generic
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initSignDSAEcP_prim()
 *********************************************************************************************************************/
/*! \brief          This function initializes the DSAEcP sign algorithm.
 *  \details        -
 *  \param[in]      domain       pointer to domain parameter structure
 *  \param[in]      speedUpExt   pointer to precomputation structure
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      domainExt    pointer to domain parameter extension structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_ECC_DOMAIN_INVALID      domain structure is invalid
 *                  ESL_ERC_ECC_DOMAINEXT_INVALID   domainExt structure is invalid
 *                  ESL_ERC_ECC_SPEEDUPEXT_INVALID  speedUpExt structure is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignDSAEcP_prim(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEcP) workSpace, VSECPRIM_P2ROMCONST_PARA(eslt_EccDomain) domain,
  VSECPRIM_P2ROMCONST_PARA(eslt_EccDomainExt) domainExt, VSECPRIM_P2ROMCONST_PARA(eslt_EccSpeedUpExt) speedUpExt);

/**********************************************************************************************************************
 *  esl_signDSAEcP_prim()
 *********************************************************************************************************************/
/*! \brief          This function signs the given message with the passed private key.
 *  \details        -
 *  \param[in,out]  signature_r  (in) pointer to signature_r buffer
 *                               (out) signature_r
 *  \param[in,out]  signature_s  (in) pointer to signature_s buffer
 *                               (out) signature_s
 *  \param[in]      privateKey   pointer to private key
 *                               (of length getLengthOfEcPprivateKey(domain))
 *  \param[in]      message      pointer to message
 *  \param[in,out] workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in,out]  signature_sLength  (in) signature_s buffer length in bytes
 *                               (>= esl_getLengthOfEcPsignature_comp(domain))
 *                               (out) signature_s length in bytes
 *  \param[in]      messageLength  message length in bytes
 *                               (<= esl_getMaxLengthOfEcPmessage(domain))
 *  \param[in,out]  signature_rLength  (in) signature_r buffer length in bytes
 *                               (>= esl_getLengthOfEcPsignature_comp(domain))
 *                               (out) signature_r length in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID       input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID        wrong work space state
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT   signature buffer too small
 *                  ESL_ERC_ECC_MESSAGE_TOO_LONG    wrong messageLength
 *                  ESL_ERC_ECC_PRIVKEY_INVALID     privateKey is invalid
 *                  ESL_ERC_NO_ERROR                else
 *  \pre            workSpace is initialized by esl_initSignDSAEcP_prim(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_signDSAEcP_prim(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEcP) workSpace,
  eslt_Length messageLength, VSECPRIM_P2CONST_PARA(eslt_Byte) message, VSECPRIM_P2CONST_PARA(eslt_Byte) privateKey,
  VSECPRIM_P2VAR_PARA(eslt_Length) signature_rLength, VSECPRIM_P2VAR_PARA(eslt_Byte) signature_r,
  VSECPRIM_P2VAR_PARA(eslt_Length) signature_sLength, VSECPRIM_P2VAR_PARA(eslt_Byte) signature_s);

/**********************************************************************************************************************
 *  esl_initVerifyDSAEcP_prim()
 *********************************************************************************************************************/
/*! \brief          This function initializes the DSAEcP verification algorithm.
 *  \details        -
 *  \param[in]      domain       pointer to domain parameter structure
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      domainExt    pointer to domain parameter extension structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_ECC_DOMAIN_INVALID      domain structure is invalid
 *                  ESL_ERC_ECC_DOMAINEXT_INVALID   domainExt structure is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initVerifyDSAEcP_prim(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEcP) workSpace,
  VSECPRIM_P2ROMCONST_PARA(eslt_EccDomain) domain, VSECPRIM_P2ROMCONST_PARA(eslt_EccDomainExt) domainExt);

/**********************************************************************************************************************
 *  esl_verifyDSAEcP_prim()
 *********************************************************************************************************************/
/*! \brief          This function verifies the given signature of the input message
 *                  with the passed public key.
 *  \details        -
 *  \param[in]      publicKey_x  pointer to x-coordinate of public key
 *  \param[in]      publicKey_y  pointer to y-coordinate of public key
 *                               (both of length getLengthOfEcPpublicKey_comp(domain))
 *  \param[in]      messageLength  message length in bytes
 *                               (<= esl_getMaxLengthOfEcPmessage(domain))
 *  \param[in]      message      pointer to message
 *  \param[in]      signature_r  pointer to signature_r
 *  \param[in,out] workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      signature_sLength  signature_slength in bytes
 *  \param[in]      signature_s  pointer to signature_s
 *  \param[in]      signature_rLength  signature_r length in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID       input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID        wrong work space state
 *                  ESL_ERC_ECC_MESSAGE_TOO_LONG    wrong messageLength
 *                  ESL_ERC_ECC_PUBKEY_INVALID      publicKey is invalid
 *                  ESL_ERC_ECC_SIGNATURE_INVALID   invalid signature
 *                  ESL_ERC_NO_ERROR                else
 *  \pre            workSpace is initialized by esl_initVerifyDSAEcP_prim(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifyDSAEcP_prim(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEcP) workSpace,
  eslt_Length messageLength, VSECPRIM_P2CONST_PARA(eslt_Byte) message,
  VSECPRIM_P2CONST_PARA(eslt_Byte) publicKey_x, VSECPRIM_P2CONST_PARA(eslt_Byte) publicKey_y,
  eslt_Length signature_rLength, VSECPRIM_P2CONST_PARA(eslt_Byte) signature_r,
  eslt_Length signature_sLength, VSECPRIM_P2CONST_PARA(eslt_Byte) signature_s);
# endif /* (VSECPRIM_ECDSA_GENERIC_ENABLED == STD_ON) */

# if (VSECPRIM_ECDH_GENERIC_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto functions Key Exchange EC-DH Generic
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initExchangeKeyDHEcP_key()
 *********************************************************************************************************************/
/*! \brief          This function initializes the DHEcP key exchange algorithm. For key
 *                  derivation KDF2HMACSHA1 of the library is used.
 *  \details        -
 *  \param[in]      domain       pointer to domain parameter structure
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      domainExt    pointer to domain parameter extension structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_ECC_DOMAIN_INVALID      domain structure is invalid
 *                  ESL_ERC_ECC_DOMAINEXT_INVALID   domainExt structure is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initExchangeKeyDHEcP_key(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEcP) workSpace,
  VSECPRIM_P2ROMCONST_PARA(eslt_EccDomain) domain, VSECPRIM_P2ROMCONST_PARA(eslt_EccDomainExt) domainExt);

/**********************************************************************************************************************
 *  esl_exchangeKeyDHEcP_key()
 *********************************************************************************************************************/
/*! \brief          This function calculates the common secret d1 * Q2 (= d1 * d2 * G)
 *                  and derives the output key from the x-coordinate of the secret with
 *                  the KDF2HMACSHA1 derivation function of the library.
 *                     if (secret_x == publicKey_x) secret_x will replace publicKey_x
 *                     if (secret_y == publicKey_y) secret_y will replace publicKey_y
 *  \details        -
 *  \param[in]      publicKey_x  x-coordinate of the public key Q2 of the key pair (d2, Q2)
 *  \param[in]      publicKey_y  y-coordinate of the public key Q2 of the key pair (d2, Q2)
 *                               (both of length getLengthOfEcPpublicKey_comp(domain))
 *  \param[in]      privateKey   the private key d1 of the key pair (d1, Q1)
 *                               (of length getLengthOfEcPprivateKey(domain))
 *  \param[in,out] key          output key buffer
 *  \param[in,out] workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      infoLength   KDF2 salt length in bytes
 *  \param[in]      info         pointer to KDF2 salt (optional, maybe NULL)
 *  \param[in]      iterationCount  KDF2 key derivation iteration count
 *  \param[in]      keyLength    output key buffer length in bytes
 *  \return         ESL_ERC_PARAMETER_INVALID       input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID        wrong work space state
 *                  ESL_ERC_ECC_PRIVKEY_INVALID     privateKey is invalid
 *                  ESL_ERC_ECC_PUBKEY_INVALID      publicKey is invalid
 *                  ESL_ERC_KDF_ITERATION_COUNT_OUT_OF_RANGE   iterationCount==0
 *                  ESL_ERC_NO_ERROR                else
 *  \pre            workSpace is initialized by esl_initExchangeKeyDHEcP_key(..)

 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_exchangeKeyDHEcP_key(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEcP) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) privateKey,
  VSECPRIM_P2CONST_PARA(eslt_Byte) publicKey_x, VSECPRIM_P2CONST_PARA(eslt_Byte) publicKey_y,
  eslt_Length infoLength, VSECPRIM_P2CONST_PARA(eslt_Byte) info, eslt_Length iterationCount,
  eslt_Length keyLength, VSECPRIM_P2VAR_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_initGenerateSharedSecretDHEcP_prim()
 *********************************************************************************************************************/
/*! \brief          This function initializes the DHEcP generate secret algorithm.
 *  \details        -
 *  \param[in]      domain       pointer to domain parameter structure
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      domainExt    pointer to domain parameter extension structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_ECC_DOMAIN_INVALID      domain structure is invalid
 *                  ESL_ERC_ECC_DOMAINEXT_INVALID   domainExt structure is invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initGenerateSharedSecretDHEcP_prim(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEcP) workSpace,
  VSECPRIM_P2ROMCONST_PARA(eslt_EccDomain) domain, VSECPRIM_P2ROMCONST_PARA(eslt_EccDomainExt) domainExt);

/**********************************************************************************************************************
 *  esl_generateSharedSecretDHEcP_prim()
 *********************************************************************************************************************/
/*! \brief          This function calculates the common secret d1 * Q2 (= d1 * d2 * G)
 *                     if (secret_x == publicKey_x) secret_x will replace publicKey_x
 *                     if (secret_y == publicKey_y) secret_y will replace publicKey_y
 *  \details        -
 *  \param[in,out]  secret_x     (in) buffer to store x-coordinate of the secret d1 * Q2
 *                               (out) x-coordinate of the secret d1 * Q2
 *  \param[in]      publicKey_x  x-coordinate of the public key Q2 of the key pair (d2, Q2)
 *  \param[in]      publicKey_y  y-coordinate of the public key Q2 of the key pair (d2, Q2)
 *                               (both of length getLengthOfEcPpublicKey_comp(domain))
 *  \param[in]      privateKey   the private key d1 of the key pair (d1, Q1)
 *                               (of length getLengthOfEcPprivateKey(domain))
 *  \param[in,out] workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in,out]  secret_y     (in) buffer to store y-coordinate of the secret d1 * Q2
 *                               (both of length getLengthOfEcPsecret_comp(domain))
 *                               (out) y-coordinate of the secret d1 * Q2
 *  \return         ESL_ERC_PARAMETER_INVALID       input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID        wrong work space state
 *                  ESL_ERC_ECC_PRIVKEY_INVALID     privateKey is invalid
 *                  ESL_ERC_ECC_PUBKEY_INVALID      publicKey is invalid
 *                  ESL_ERC_NO_ERROR                else
 *  \pre            workSpace is initialized by esl_initGenerateSharedSecretDHEcP_prim(..)

 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateSharedSecretDHEcP_prim(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEcP) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) privateKey,
  VSECPRIM_P2CONST_PARA(eslt_Byte) publicKey_x, VSECPRIM_P2CONST_PARA(eslt_Byte) publicKey_y,
  VSECPRIM_P2VAR_PARA(eslt_Byte) secret_x, VSECPRIM_P2VAR_PARA(eslt_Byte) secret_y);
# endif /* (VSECPRIM_ECDH_GENERIC_ENABLED == STD_ON) */

# if (VSECPRIM_RSA_CIPHER_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */ /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto functions RSA
 * according to PKCS#1 2.1
 * according to PKCS#1 primitives RSAEP, RSADP, RSASP1, RSAVP1
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initDecryptRSA_prim()
 *********************************************************************************************************************/
/*! \brief          This function initializes the RSA decryption primitive (plain version).
 *  \details        -
 *  \param[in]      privateKeyExponentSize  the byte length of the private exponent d
 *  \param[in]      keyPairModuleSize  the byte length of the modulus n
 *  \param[in]      privateKeyExponent  pointer to the private exponent d
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      keyPairModule  pointer to the modulus n
 *  \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL               work space too small
 *                  ESL_ERC_RSA_PRIVKEY_INVALID        the private exponent d
 *                  is invalid
 *                  ESL_ERC_NO_ERROR                   else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptRSA_prim(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec_prim) workSpace,
    eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
    eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent);

/**********************************************************************************************************************
 *  esl_decryptRSA_prim()
 *********************************************************************************************************************/
/*! \brief          This function performs the RSA decryption operation (RSA_DP1).
 *  \details        -
 *  \param[in,out]  messageSize  pointer to byte length of the output buffer
 *  \param[in,out]  message      pointer to the output buffer
 *  \param[in]      cipher       pointer to the input
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      cipherSize   the byte length of the input
 *  \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                  ESL_ERC_RSA_CODE_OUT_OF_RANGE   (int) cipher > n-1
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT      *messageSize is too small
 *                  ESL_ERC_WS_STATE_INVALID           invalid Algo
 *                  ESL_ERC_NO_ERROR                   else
 *  \pre            workSpace is initialized by esl_initDecryptRSA_prim(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSA_prim(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec_prim) workSpace,
   eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);

/**********************************************************************************************************************
 *  esl_initDecryptRSACRT_prim()
 *********************************************************************************************************************/
/*! \brief          This function initializes the RSA decryption primitive (CRT version).
 *  \details        -
 *  \param[in]      keyPairPrimeQSize  the byte length of the prime q
 *  \param[in]      keyPairPrimeQ  pointer to prime q
 *  \param[in]      keyPairPrimePSize  the byte length of the prime p
 *  \param[in]      keyPairPrimeP  pointer to prime p
 *  \param[in]      privateKeyInverseQISize  the byte length of CRT key component qInv
 *  \param[in]      privateKeyInverseQI  pointer to CRT key component qInv
 *  \param[in]      privateKeyExponentDPSize  the byte length of dp
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      privateKeyExponentDQSize  the byte length of dq
 *  \param[in]      privateKeyExponentDP  pointer to CRT key component dp
 *  \param[in]      privateKeyExponentDQ  pointer to CRT key component dq
 *  \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL               work space too small
 *                  ESL_ERC_RSA_PRIVKEY_INVALID        the private key
 *                  is invalid
 *                  ESL_ERC_NO_ERROR                   else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptRSACRT_prim(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTdec_prim) workSpace,
   eslt_Length   keyPairPrimePSize, eslt_pRomByte keyPairPrimeP,
   eslt_Length   keyPairPrimeQSize, eslt_pRomByte keyPairPrimeQ,
   eslt_Length   privateKeyExponentDPSize, eslt_pRomByte privateKeyExponentDP,
   eslt_Length   privateKeyExponentDQSize, eslt_pRomByte privateKeyExponentDQ,
   eslt_Length   privateKeyInverseQISize, eslt_pRomByte privateKeyInverseQI);

/**********************************************************************************************************************
 *  esl_decryptRSACRT_prim()
 *********************************************************************************************************************/
/*! \brief          This function performs the RSA decryption operation (RSA_DP1).
 *  \details        -
 *  \param[in,out]  messageSize  pointer to byte length of the output buffer
 *  \param[in,out]  message      pointer to the output buffer
 *  \param[in]      cipher       pointer to the input
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      cipherSize   the byte length of the input
 *  \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                  ESL_ERC_RSA_CODE_OUT_OF_RANGE   (int) cipher > n-1
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT      *messageSize is too small
 *                  ESL_ERC_WS_STATE_INVALID           invalid Algo
 *                  ESL_ERC_NO_ERROR                   else
 *  \pre            workSpace is initialized by esl_initDecryptRSACRT_prim(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSACRT_prim(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTdec_prim) workSpace,
   eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);
# endif /* (VSECPRIM_RSA_CIPHER_ENABLED == STD_ON) */

# if (VSECPRIM_RSA_SIGNATURE_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/**********************************************************************************************************************
 *  esl_initSignRSA_prim()
 *********************************************************************************************************************/
/*! \brief          This function initializes the RSA signature primitive (plain version).
 *  \details        -
 *  \param[in]      privateKeyExponentSize  the byte length of the private exponent d
 *  \param[in]      keyPairModuleSize  the byte length of the modulus n
 *  \param[in]      privateKeyExponent  pointer to the private exponent d
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      keyPairModule  pointer to the modulus n
 *  \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL               work space too small
 *                  ESL_ERC_RSA_PRIVKEY_INVALID        the private exponent d
 *                  is invalid
 *                  ESL_ERC_NO_ERROR                   else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignRSA_prim(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig_prim) workSpace,
     eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
     eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent);

/**********************************************************************************************************************
 *  esl_signRSA_prim()
 *********************************************************************************************************************/
/*! \brief          This function performs the RSA signature generation (RSA_SP1).
 *  \details        -
 *  \param[in]      message      pointer to the input
 *  \param[in,out]  signatureSize  pointer to byte length of the output buffer
 *  \param[in,out]  signature    pointer to the output buffer
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      messageSize  the byte length of the input
 *  \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                  ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE   (int) code > n-1
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT      *signatureSize is too small
 *                  ESL_ERC_WS_STATE_INVALID           invalid Algo
 *                  ESL_ERC_NO_ERROR                   else
 *  \pre            workSpace is initialized by esl_initSignRSA_prim(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_signRSA_prim(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig_prim) workSpace,
   eslt_Length messageSize, VSECPRIM_P2CONST_PARA(eslt_Byte) message,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/**********************************************************************************************************************
 *  esl_initSignRSACRT_prim()
 *********************************************************************************************************************/
/*! \brief          This function initializes the RSA signature primitive (CRT version).
 *  \details        -
 *  \param[in]      keyPairPrimeQSize  the byte length of the prime q
 *  \param[in]      keyPairPrimeP  pointer to prime p
 *  \param[in]      privateKeyInverseQISize  the byte length of CRT key component qInv
 *  \param[in]      privateKeyInverseQI  pointer to CRT key component qInv
 *  \param[in]      privateKeyExponentDPSize  the byte length of dp
 *  \param[in]      keyPairPrimeQ  pointer to prime q
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      privateKeyExponentDQSize  the byte length of dq
 *  \param[in]      privateKeyExponentDP  pointer to CRT key component dp
 *  \param[in]      privateKeyExponentDQ  pointer to CRT key component dq
 *  \param[in]      keyPairPrimePSize  the byte length of the prime p
 *  \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL               work space too small
 *                  ESL_ERC_RSA_PRIVKEY_INVALID        the private key
 *                  is invalid
 *                  ESL_ERC_NO_ERROR                   else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignRSACRT_prim(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTsig_prim) workSpace,
   eslt_Length keyPairPrimePSize, eslt_pRomByte keyPairPrimeP,
   eslt_Length keyPairPrimeQSize, eslt_pRomByte keyPairPrimeQ,
   eslt_Length privateKeyExponentDPSize, eslt_pRomByte privateKeyExponentDP,
   eslt_Length privateKeyExponentDQSize, eslt_pRomByte privateKeyExponentDQ,
   eslt_Length privateKeyInverseQISize, eslt_pRomByte privateKeyInverseQI);

/**********************************************************************************************************************
 *  esl_signRSACRT_prim()
 *********************************************************************************************************************/
/*! \brief          This function performs the RSA signature operation (RSA_SP1).
 *  \details        -
 *  \param[in]      message      pointer to the input
 *  \param[in,out]  signatureSize  pointer to byte length of the output buffer
 *  \param[in,out]  signature    pointer to the output buffer
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      messageSize  the byte length of the input
 *  \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                  ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE   (int) code > n-1
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT      *signatureSize is too small
 *                  ESL_ERC_WS_STATE_INVALID           invalid Algo
 *                  ESL_ERC_NO_ERROR                   else
 *  \pre            workSpace is initialized by esl_initSignRSACRT_prim(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_signRSACRT_prim(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTsig_prim) workSpace,
   eslt_Length messageSize, VSECPRIM_P2CONST_PARA(eslt_Byte) message,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte) signature);
# endif /* (VSECPRIM_RSA_SIGNATURE_ENABLED == STD_ON) */

# if (VSECPRIM_RSA_CIPHER_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/**********************************************************************************************************************
 *  esl_initEncryptRSA_prim()
 *********************************************************************************************************************/
/*! \brief          This function initializes the RSA encryption primitive.
 *  \details        -
 *  \param[in]      publicKeyExponentSize  the byte length of the public exponent e
 *  \param[in]      keyPairModuleSize  the byte length of the modulus n
 *  \param[in]      publicKeyExponent  pointer to the public exponent e
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      keyPairModule  pointer to the modulus n
 *  \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL               work space too small
 *                  ESL_ERC_RSA_PUBKEY_INVALID         the public exponent is
 *                  invalid
 *                  ESL_ERC_NO_ERROR                   else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptRSA_prim(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAenc_prim) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length publicKeyExponentSize, eslt_pRomByte publicKeyExponent);

/**********************************************************************************************************************
 *  esl_encryptRSA_prim()
 *********************************************************************************************************************/
/*! \brief          This function performs the RSA encryption operation (RSA_EP1).
 *  \details        -
 *  \param[in]      messageSize  the byte length of the input message
 *  \param[in,out]  cipher       pointer to the output buffer
 *  \param[in,out]  cipherSize   pointer to byte length of the output buffer
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      message      pointer to the input message
 *  \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                  ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE   (int) message > n-1
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT      *cipherSize is too small
 *                  ESL_ERC_WS_STATE_INVALID           invalid Algo
 *                  ESL_ERC_NO_ERROR                   else
 *  \pre            workSpace is initialized by esl_initEncryptRSA_prim(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptRSA_prim(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAenc_prim) workSpace,
   eslt_Length messageSize, VSECPRIM_P2CONST_PARA(eslt_Byte) message,
   VSECPRIM_P2VAR_PARA(eslt_Length) cipherSize, VSECPRIM_P2VAR_PARA(eslt_Byte) cipher);
# endif /* (VSECPRIM_RSA_CIPHER_ENABLED == STD_ON) */

# if (VSECPRIM_RSA_SIGNATURE_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/**********************************************************************************************************************
 *  esl_initVerifyRSA_prim()
 *********************************************************************************************************************/
/*! \brief          This function initializes the RSA verification primitive.
 *  \details        -
 *  \param[in]      publicKeyExponentSize  the byte length of the public exponent e
 *  \param[in]      keyPairModuleSize  the byte length of the modulus n
 *  \param[in]      publicKeyExponent  pointer to the public exponent e
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      keyPairModule  pointer to the modulus n
 *  \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL               work space too small
 *                  ESL_ERC_RSA_PUBKEY_INVALID         the public exponent is
 *                  invalid
 *                  ESL_ERC_NO_ERROR                   else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initVerifyRSA_prim(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver_prim) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length publicKeyExponentSize, eslt_pRomByte publicKeyExponent);

/**********************************************************************************************************************
 *  esl_verifyRSA_prim()
 *********************************************************************************************************************/
/*! \brief          This function performs the RSA verification primitive (RSA_VP1).
 *  \details        -
 *  \param[in,out]  messageSize  pointer to byte length of the output buffer
 *  \param[in,out]  message      pointer to the output buffer
 *  \param[in]      signature    pointer to the given signature
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *                               (out) initialized algorithm context structure
 *  \param[in]      signatureSize  the byte length of the given signature
 *  \return         ESL_ERC_PARAMETER_INVALID          input parameter is NULL
 *                  ESL_ERC_RSA_SIGNATURE_OUT_OF_RANGE (int) signature > n-1
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT      *codeSize is too small
 *                  ESL_ERC_WS_STATE_INVALID           invalid Algo
 *                  ESL_ERC_NO_ERROR                   else
 *  \pre            workSpace is initialized by esl_initVerifyRSA_prim(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifyRSA_prim(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver_prim) workSpace,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);
# endif /* (VSECPRIM_RSA_SIGNATURE_ENABLED == STD_ON) */

# if ((VSECPRIM_RSA_V15_SHA1_ENABLED == STD_ON) || (VSECPRIM_RSA_V15_ENABLED == STD_ON))  /* COV_VSECPRIM_NO_SAFE_CONFIG XF xf xf */
/*****************************************************************************
 * Crypto functions RSA
 * according to PKCS#1 2.1
 * according to PKCS#1 V1.5
 * these versions include simple padding, sign & verify use SHA-1
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initDecryptRSA_V15()
 *********************************************************************************************************************/
/*! \brief          Initialize decryption for RSA PKCS#1 V1.5
 *  \details        -
 *  \param[in]      keyPairModuleSize  length of the keyPairModulus (bytes)
 *  \param[in]      privateKeyExponent  pointer to the public exponent
 *  \param[in]      privateKeyExponentSize  length of the public keyExponent (bytes)
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      keyPairModule  pointer to the key modulus
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *                  keyExponent  < keyPairModule
 *                  Module is a prime
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptRSA_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent);

/**********************************************************************************************************************
 *  esl_decryptPubRSA_V15()
 *********************************************************************************************************************/
/*! \brief          Perform decryption for RSA PKCS#1 V1.5 with public key
 *  \details        -
 *  \param[out]     messageSize  pointer to length of the message buffer (bytes)
 *  \param[out]     message      pointer to the message buffer
 *  \param[in]      cipher       pointer to the cipher
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      cipherSize   length of the cipher (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_CODE_OUT_OF_RANGE (int) cipher > n-1
 *                  ESL_ERC_RSA_ENCODING_INVALID  Invalid padding
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT outputSize too small
 *  \pre            workSpace is initialized by esl_initDecryptRSA_V15(..)
 *                  public keyExponent  < keyPairModule
 *                  Module is a prime
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptPubRSA_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec) workSpace,
   eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);

/**********************************************************************************************************************
 *  esl_decryptPrivRSA_V15()
 *********************************************************************************************************************/
/*! \brief          Perform decryption for RSA PKCS#1 V1.5 with private key
 *  \details        -
 *  \param[out]     messageSize  pointer to length of the message buffer (bytes)
 *  \param[out]     message      pointer to the message buffer
 *  \param[in]      cipher       pointer to the cipher
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      cipherSize   length of the cipher (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_CODE_OUT_OF_RANGE (int) cipher > n-1
 *                  ESL_ERC_RSA_ENCODING_INVALID  Invalid padding
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT outputSize too small
 *  \pre            workSpace is initialized by esl_initDecryptRSA_V15(..)
 *                  private keyExponent  < keyPairModule
 *                  Module is a prime
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptPrivRSA_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec) workSpace,
   eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);

/**********************************************************************************************************************
 *  esl_decryptRSA_V15()
 *********************************************************************************************************************/
/*! \brief          Perform decryption for RSA PKCS#1 V1.5 (default with private key)
 *  \details        -
 *  \param[out]     messageSize  pointer to length of the message buffer (bytes)
 *  \param[out]     message      pointer to the message buffer
 *  \param[in]      cipher       pointer to the cipher
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      cipherSize   length of the cipher (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_CODE_OUT_OF_RANGE (int) cipher > n-1
 *                  ESL_ERC_RSA_ENCODING_INVALID  Invalid padding
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT outputSize too small
 *  \pre            workSpace is initialized by esl_initDecryptRSA_V15(..)
 *                  private keyExponent  < keyPairModule
 *                  Module is a prime
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSA_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAdec) workSpace,
   eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);

/**********************************************************************************************************************
 *  esl_initDecryptRSACRT_V15()
 *********************************************************************************************************************/
/*! \brief          Initialize decryption for RSA PKCS#1 V1.5 CRT
 *  \details        -
 *  \param[in]      keyPairPrimeQSize  length of the keyPairPrimeQ (bytes)
 *  \param[in]      keyPairPrimeP  pointer to the 1st key prime
 *  \param[in]      privateKeyInverseQISize  length of the privateKeyInverseQI (bytes)
 *  \param[in]      privateKeyInverseQI  pointer to the inverse
 *  \param[in]      privateKeyExponentDPSize  length of the privKeyExponenentDP (bytes)
 *  \param[in]      keyPairPrimeQ  pointer to the 2nd key prime
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      privateKeyExponentDQSize  length of the privKeyExponenentDQ (bytes)
 *  \param[in]      privateKeyExponentDP  pointer to the partial privKey exponent
 *  \param[in]      privateKeyExponentDQ  pointer to the partial privKey exponent
 *  \param[in]      keyPairPrimePSize  length of the keyPairPrimeP (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_PARAMETER_INVALID   format problems, nullpointer, ..
 *                  ESL_ERC_RSA_PRIVKEY_INVALID  The private exponent is invalid
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *                  PrimeP and Q are a prime
 *                  privateKeyExpDP<PrimeP
 *                  privateKeyExpDQ<PrimeQ
 *                  QI is inverse to Q, could be checked, then move this line to errorcodes
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptRSACRT_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTdec) workSpace,
   eslt_Length keyPairPrimePSize, eslt_pRomByte keyPairPrimeP,
   eslt_Length keyPairPrimeQSize, eslt_pRomByte keyPairPrimeQ,
   eslt_Length privateKeyExponentDPSize, eslt_pRomByte privateKeyExponentDP,
   eslt_Length privateKeyExponentDQSize, eslt_pRomByte privateKeyExponentDQ,
   eslt_Length privateKeyInverseQISize, eslt_pRomByte privateKeyInverseQI);

/**********************************************************************************************************************
 *  esl_decryptRSACRT_V15()
 *********************************************************************************************************************/
/*! \brief          Perform decryption for RSA PKCS#1 V1.5 CRT
 *  \details        -
 *  \param[out]     messageSize  pointer to length of the message buffer (bytes)
 *  \param[out]     message      pointer to the message buffer
 *  \param[in]      cipher       pointer to the cipher
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      cipherSize   length of the cipher (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_CODE_OUT_OF_RANGE (int) cipher > n-1
 *                  - 0 <= (int) cipher <= module-1, where module = PrimeP*PrimeQ
 *                  ESL_ERC_RSA_ENCODING_INVALID  Invalid padding
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT outputSize too small
 *  \pre            workSpace is initialized by esl_initDecryptRSACRT_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSACRT_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTdec) workSpace,
   eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);

/**********************************************************************************************************************
 *  esl_initSignRSASHA1_V15()
 *********************************************************************************************************************/
/*! \brief          Initialize Signature for RSA PKCS#1 V1.5 with SHA-1
 *  \details        -
 *  \param[in]      privateKeyExponentSize  length of the privateKeyExponent (bytes)
 *  \param[in]      keyPairModuleSize  length of the keyPairModulus (bytes)
 *  \param[in]      privateKeyExponent  pointer to the private key exponent
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      keyPairModule  pointer to the key modulus
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_PRIVKEY_INVALID  The private exponent is invalid
 *                  ESL_ERC_RSA_MODULE_OUT_OF_RANGE the given module is not supported the given module is not supported
 *                  - if keyPairModuleSize < 46d = headLen+hLen+11, where
 *                  + hLen is the length of the Hashvalue (SHA-1  20d= 0x14)
 *                  + headLen is the AlgoIDentifier (RSA+SHA1  15d=0x0f)
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *                  privateKeyExponent  < keyPairModule
 *                  Module is a prime
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignRSASHA1_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent);

/**********************************************************************************************************************
 *  esl_updateSignRSASHA1_V15()
 *********************************************************************************************************************/
/*! \brief          Update Signature for RSA PKCS#1 V1.5 with SHA-1
 *  \details        -
 *  \param[in]      input        pointer to the input data
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      inputSize    size of the input data (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID - format problems, nullpointer, ...
 *                  ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *  \pre            workSpace is initialized by esl_initSignRSASHA1_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSignRSASHA1_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig) workSpace,
   eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeSignRSASHA1_V15()
 *********************************************************************************************************************/
/*! \brief          Finalize Signature for RSA PKCS#1 V1.5 with SHA-1
 *  \details        -
 *  \param[out]     signature    pointer to the signature
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[out]     signatureSize  length of the signature (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT outputSize too small
 *  \pre            workSpace is initialized by esl_initSignRSASHA1_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSASHA1_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/**********************************************************************************************************************
 *  esl_initSignRSACRTSHA1_V15()
 *********************************************************************************************************************/
/*! \brief          Initialize Signature for RSA PKCS#1 V1.5 CRT with SHA-1
 *  \details        -
 *  \param[in]      keyPairPrimeQSize  length of the keyPairPrimeQ (bytes)
 *  \param[in]      keyPairPrimeP  pointer to the 1st key prime
 *  \param[in]      privateKeyInverseQISize  length of the privateKeyInverseQI (bytes)
 *  \param[in]      privateKeyInverseQI  pointer to the inverse
 *  \param[in]      privateKeyExponentDPSize  length of the privKeyExponenentDP (bytes)
 *  \param[in]      keyPairPrimeQ  pointer to the 2nd key prime
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      privateKeyExponentDQSize  length of the privKeyExponenentDQ (bytes)
 *  \param[in]      privateKeyExponentDP  pointer to the partial privKey exponent
 *  \param[in]      privateKeyExponentDQ  pointer to the partial privKey exponent
 *  \param[in]      keyPairPrimePSize  length of the keyPairPrimeP (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_PRIVKEY_INVALID  The private exponent is invalid
 *                  ESL_ERC_RSA_MODULE_OUT_OF_RANGE the given module is not supported
 *                  - keyPairModuleSize < 46d = headLen+hLen+11, where
 *                  + hLen is the length of the Hashvalue (SHA-1  20d= 0x14)
 *                  + headLen is the AlgoIDentifier (RSA+SHA1  15d=0x0f)
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *                  PrimeP and PrimeQ are a prime
 *                  privateKeyExpDP<PrimeP
 *                  privateKeyExpDQ<PrimeQ
 *                  QI is inverse to Q, could be checked, then move this line to errorcodes
 *                  the Hash-Function is SHA-1
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignRSACRTSHA1_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTsig) workSpace,
   eslt_Length keyPairPrimePSize, eslt_pRomByte keyPairPrimeP,
   eslt_Length keyPairPrimeQSize, eslt_pRomByte keyPairPrimeQ,
   eslt_Length privateKeyExponentDPSize, eslt_pRomByte privateKeyExponentDP,
   eslt_Length privateKeyExponentDQSize, eslt_pRomByte privateKeyExponentDQ,
   eslt_Length privateKeyInverseQISize, eslt_pRomByte privateKeyInverseQI);

/**********************************************************************************************************************
 *  esl_updateSignRSACRTSHA1_V15()
 *********************************************************************************************************************/
/*! \brief          Update Signature for RSA PKCS#1 V1.5 CRT with SHA-1
 *  \details        -
 *  \param[in]      input        pointer to the input data
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      inputSize    size of the input data (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID - format problems, nullpointer, ...
 *                  ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *  \pre            workSpace is initialized by esl_initSignRSACRTSHA1_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSignRSACRTSHA1_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTsig) workSpace,
   const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeSignRSACRTSHA1_V15()
 *********************************************************************************************************************/
/*! \brief          Finalize Signature for RSA PKCS#1 V1.5 CRT with SHA-1
 *  \details        -
 *  \param[out]     signature    pointer to the signature
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[out]     signatureSize  length of the signature (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT outputSize too small
 *  \pre            workSpace is initialized by esl_initSignRSACRTSHA1_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSACRTSHA1_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/**********************************************************************************************************************
 *  esl_initEncryptRSA_V15()
 *********************************************************************************************************************/
/*! \brief          Initialize encryption for RSA PKCS#1 V1.5
 *  \details        -
 *  \param[in]      publicKeyExponentSize  length of the public publicKeyExponent (bytes)
 *  \param[in]      keyPairModuleSize  length of the keyPairModulus (bytes)
 *  \param[in]      publicKeyExponent  pointer to the public exponent
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      keyPairModule  pointer to the key modulus
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_PUBKEY_INVALID  The public exponent is invalid
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *                  privateKeyExponent  < keyPairModule
 *                  Module is a prime
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptRSA_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAenc) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length publicKeyExponentSize, eslt_pRomByte publicKeyExponent);

/**********************************************************************************************************************
 *  esl_encryptRSA_V15()
 *********************************************************************************************************************/
/*! \brief          Perform encryption for RSA PKCS#1 V1.5
 *  \details        -
 *  \param[in]      messageSize  pointer to length of the message buffer (bytes)
 *  \param[out]     cipher       pointer to the cipher
 *  \param[out]     cipherSize   length of the cipher (bytes)
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      message      pointer to the message buffer
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE
 *                  - if messageSize > keyPairModuleSize - ASN1_SIZEOF_MINIMAL_PADDING
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT outputSize too small
 *                  and all possible error codes which can occur in esl_getBytesRNG
 *  \pre            workSpace is initialized by esl_initEncryptRSA_V15(..)
 *                  *cipherSize >= keyPairModuleSize
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptRSA_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAenc) workSpace,
   eslt_Length messageSize, VSECPRIM_P2CONST_PARA(eslt_Byte) message,
   VSECPRIM_P2VAR_PARA(eslt_Length) cipherSize, VSECPRIM_P2VAR_PARA(eslt_Byte) cipher);

/**********************************************************************************************************************
 *  esl_initVerifyRSASHA1_V15()
 *********************************************************************************************************************/
/*! \brief          Initialize signature verification for RSA PKCS#1 V1.5 with SHA-1
 *  \details        -
 *  \param[in]      publicKeyExponentSize  length of the public publicKeyExponent (bytes)
 *  \param[in]      keyPairModuleSize  length of the keyPairModulus (bytes)
 *  \param[in]      publicKeyExponent  pointer to the public exponent
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      keyPairModule  pointer to the key modulus
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *                  publicKeyExponent  < keyPairModule
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initVerifyRSASHA1_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length publicKeyExponentSize, eslt_pRomByte publicKeyExponent);

/**********************************************************************************************************************
 *  esl_updateVerifyRSASHA1_V15()
 *********************************************************************************************************************/
/*! \brief          Update signature verification for RSA PKCS#1 V1.5 with SHA-1
 *  \details        -
 *  \param[in]      input        pointer to the input data
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      inputSize    size of the input data (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID - format problems, nullpointer, ...
 *                  ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW   total input length overflow
 *  \pre            workSpace is initialized by esl_initVerifyRSASHA1_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateVerifyRSASHA1_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace,
   eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeHashVerifyRSASHA1_V15
 *********************************************************************************************************************/
/*! \brief          Finalize hash for signature verification for RSA PKCS#1 V1.5 with SHA-1
 *  \details        -
 *  \param[in,out]  messageDigest  pointer to computed hash value
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *  \pre            workSpace is initialized by esl_initVerifyRSASHA1_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashVerifyRSASHA1_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

/**********************************************************************************************************************
 *  esl_verifySigVerifyRSASHA1_V15()
 *********************************************************************************************************************/
/*! \brief          Finalize signature for signature verification for RSA PKCS#1 V1.5 with SHA-1
 *  \details        -
 *  \param[in]      signature    pointer to the signature
 *  \param[in]      messageDigest  pointer to computed hash value
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      signatureSize  length of the signature (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_SIGNATURE_OUT_OF_RANGE
 *                  - 0 <= (int) signature <= module-1, where module = PrimeP*PrimeQ
 *                  ESL_ERC_RSA_ENCODING_INVALID  Invalid padding
 *                  ESL_ERC_RSA_PUBKEY_INVALID  The public exponent is invalid
 *                  ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *  \pre            workSpace is initialized by esl_initVerifyRSASHA1_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSASHA1_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/**********************************************************************************************************************
 *  esl_finalizeVerifyRSASHA1_V15()
 *********************************************************************************************************************/
/*! \brief          Finalize hash and signature for signature verification for RSA PKCS#1 V1.5 with SHA-1
 *  \details        -
 *  \param[in]      signature    pointer to the signature
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      signatureSize  length of the signature (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_SIGNATURE_OUT_OF_RANGE
 *                  - 0 <= (int) signature <= module-1, where module = PrimeP*PrimeQ
 *                  ESL_ERC_RSA_ENCODING_INVALID  Invalid padding
 *                  ESL_ERC_RSA_PUBKEY_INVALID  The public exponent is invalid
 *                  ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *  \pre            workSpace is initialized by esl_initVerifyRSASHA1_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSASHA1_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);
# endif /* ((VSECPRIM_RSA_V15_SHA1_ENABLED == STD_ON) || (VSECPRIM_RSA_V15_ENABLED == STD_ON)) */

# if (VSECPRIM_RSA_V15_RIPEMD160_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto functions RSA (sign/verify) with RIPEMD-160
 * according to PKCS#1 1.5
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initSignRSARIPEMD160_V15()
 *********************************************************************************************************************/
/*! \brief          Initialize Signature for RSA PKCS#1 V1.5 with RIPEMD160
 *  \details        -
 *  \param[in]      privateKeyExponentSize  length of the privateKeyExponent (bytes)
 *  \param[in]      keyPairModuleSize  length of the keyPairModulus (bytes)
 *  \param[in]      privateKeyExponent  pointer to the private key exponent
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      keyPairModule  pointer to the key modulus
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_PRIVKEY_INVALID  The private exponent is invalid
 *                  ESL_ERC_RSA_MODULE_OUT_OF_RANGE the given module is not supported
 *                  - if keyPairModuleSize < 46d = headLen+hLen+11, where
 *                  + hLen is the length of the Hashvalue (RIPEMD160  20d= 0x14)
 *                  + headLen is the AlgoIDentifier (RSA+RIPEMD160  15d=0x0f)
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *                  privateKeyExponent  < keyPairModule
 *                  Module is a prime

 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignRSARIPEMD160_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent);

/**********************************************************************************************************************
 *  esl_updateSignRSARIPEMD160_V15()
 *********************************************************************************************************************/
/*! \brief          Update Signature for RSA PKCS#1 V1.5 with RIPEMD160
 *  \details        -
 *  \param[in]      input        pointer to the input data
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      inputSize    size of the input data (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID - format problems, nullpointer, ...
 *                  ESL_ERC_RIPEMD160_TOTAL_LENGTH_OVERFLOW  Maximum total input length exceeded
 *  \pre            workSpace is initialized by esl_initSignRSARIPEMD160_V15(..)

 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSignRSARIPEMD160_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig) workSpace,
   eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeSignRSARIPEMD160_V15()
 *********************************************************************************************************************/
/*! \brief          Finalize Signature for RSA PKCS#1 V1.5 with RIPEMD160
 *  \details        -
 *  \param[out]     signature    pointer to the signature
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[out]     signatureSize  length of the signature (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT outputSize too small
 *  \pre            workSpace is initialized by esl_initSignRSARIPEMD160_V15(..)

 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSARIPEMD160_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/**********************************************************************************************************************
 *  esl_initSignRSACRTRIPEMD160_V15()
 *********************************************************************************************************************/
/*! \brief          Initialize Signature for RSA PKCS#1 V1.5 CRT with RIPEMD160
 *  \details        -
 *  \param[in]      keyPairPrimeQSize  length of the keyPairPrimeQ (bytes)
 *  \param[in]      keyPairPrimeP  pointer to the 1st key prime
 *  \param[in]      privateKeyInverseQISize  length of the privateKeyInverseQI (bytes)
 *  \param[in]      privateKeyInverseQI  pointer to the inverse
 *  \param[in]      privateKeyExponentDPSize  length of the privKeyExponenentDP (bytes)
 *  \param[in]      keyPairPrimeQ  pointer to the 2nd key prime
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      privateKeyExponentDQSize  length of the privKeyExponenentDQ (bytes)
 *  \param[in]      privateKeyExponentDP  pointer to the partial privKey exponent
 *  \param[in]      privateKeyExponentDQ  pointer to the partial privKey exponent
 *  \param[in]      keyPairPrimePSize  length of the keyPairPrimeP (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_PRIVKEY_INVALID  The private exponent is invalid
 *                  ESL_ERC_RSA_MODULE_OUT_OF_RANGE the given module is not supported
 *                  - keyPairModuleSize < 46d = headLen+hLen+11, where
 *                  + hLen is the length of the Hashvalue (RIPEMD160  20d= 0x14)
 *                  + headLen is the AlgoIDentifier (RSA+RIPEMD160  15d=0x0f)
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *                  PrimeP and PrimeQ are a prime
 *                  privateKeyExpDP<PrimeP
 *                  privateKeyExpDQ<PrimeQ
 *                  QI is inverse to Q, could be checked, then move this line to errorcodes
 *                  the Hash-Function is SHA-1

 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignRSACRTRIPEMD160_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTsig) workSpace,
   eslt_Length keyPairPrimePSize, eslt_pRomByte keyPairPrimeP,
   eslt_Length keyPairPrimeQSize, eslt_pRomByte keyPairPrimeQ,
   eslt_Length privateKeyExponentDPSize, eslt_pRomByte privateKeyExponentDP,
   eslt_Length privateKeyExponentDQSize, eslt_pRomByte privateKeyExponentDQ,
   eslt_Length privateKeyInverseQISize, eslt_pRomByte privateKeyInverseQI);

/**********************************************************************************************************************
 *  esl_updateSignRSACRTRIPEMD160_V15()
 *********************************************************************************************************************/
/*! \brief          Update Signature for RSA PKCS#1 V1.5 CRT with RIPEMD160
 *  \details        -
 *  \param[in]      input        pointer to the input data
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      inputSize    size of the input data (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID - format problems, nullpointer, ...
 *                  ESL_ERC_RIPEMD160_TOTAL_LENGTH_OVERFLOW  Maximum total input length exceeded
 *  \pre            workSpace is initialized by esl_initSignRSACRTRIPEMD160_V15(..)

 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSignRSACRTRIPEMD160_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTsig) workSpace,
   const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeSignRSACRTRIPEMD160_V15()
 *********************************************************************************************************************/
/*! \brief          Finalize Signature for RSA PKCS#1 V1.5 CRT with RIPEMD160
 *  \details        -
 *  \param[out]     signature    pointer to the signature
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[out]     signatureSize  length of the signature (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT outputSize too small
 *  \pre            workSpace is initialized by esl_initSignRSACRTRIPEMD160_V15(..)

 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSACRTRIPEMD160_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte  ) signature);

/**********************************************************************************************************************
 *  esl_initVerifyRSARIPEMD160_V15()
 *********************************************************************************************************************/
/*! \brief          Initialize signature verification for RSA PKCS#1 V1.5 with RIPEMD160
 *  \details        -
 *  \param[in]      publicKeyExponentSize  length of the public publicKeyExponent (bytes)
 *  \param[in]      keyPairModuleSize  length of the keyPairModulus (bytes)
 *  \param[in]      publicKeyExponent  pointer to the public exponent
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      keyPairModule  pointer to the key modulus
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_PUBKEY_INVALID  The public exponent is invalid
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *                  publicKeyExponent  < keyPairModule

 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initVerifyRSARIPEMD160_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length publicKeyExponentSize, eslt_pRomByte publicKeyExponent);

/**********************************************************************************************************************
 *  esl_updateVerifyRSARIPEMD160_V15()
 *********************************************************************************************************************/
/*! \brief          Update signature verification for RSA PKCS#1 V1.5 with RIPEMD160
 *  \details        -
 *  \param[in]      input        pointer to the input data
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      inputSize    size of the input data (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID - format problems, nullpointer, ...
 *                  ESL_ERC_RIPEMD160_TOTAL_LENGTH_OVERFLOW  Maximum total input length exceeded
 *  \pre            workSpace is initialized by esl_initVerifyRSARIPEMD160_V15(..)

 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateVerifyRSARIPEMD160_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace,
   eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeHashVerifyRSARIPEMD160_V15
 *********************************************************************************************************************/
/*! \brief          Finalize hash for signature verification for RSA PKCS#1 V1.5 with RIPEMD160
 *  \details        -
 *  \param[in,out]  messageDigest  pointer to computed hash value
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *  \pre            workSpace is initialized by esl_initVerifyRSARIPEMD160_V15(..)

 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashVerifyRSARIPEMD160_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

/**********************************************************************************************************************
 *  esl_verifySigVerifyRSARIPEMD160_V15()
 *********************************************************************************************************************/
/*! \brief          Finalize signature for signature verification for RSA PKCS#1 V1.5 with RIPEMD160
 *  \details        -
 *  \param[in]      signature    pointer to the signature
 *  \param[in]      messageDigest  pointer to computed hash value
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      signatureSize  length of the signature (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_SIGNATURE_OUT_OF_RANGE
 *                  - 0 <= (int) signature <= module-1, where module = PrimeP*PrimeQ
 *                  ESL_ERC_RSA_ENCODING_INVALID  Invalid padding
 *                  ESL_ERC_RSA_PUBKEY_INVALID  The public exponent is invalid
 *                  ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *  \pre            workSpace is initialized by esl_initVerifyRSARIPEMD160_V15(..)

 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSARIPEMD160_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/**********************************************************************************************************************
 *  esl_finalizeVerifyRSARIPEMD160_V15()
 *********************************************************************************************************************/
/*! \brief          Finalize hash and signature for signature verification for RSA PKCS#1 V1.5 with RIPEMD160
 *  \details        -
 *  \param[in]      signature    pointer to the signature
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      signatureSize  length of the signature (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_SIGNATURE_OUT_OF_RANGE
 *                  - 0 <= (int) signature <= module-1, where module = PrimeP*PrimeQ
 *                  ESL_ERC_RSA_ENCODING_INVALID  Invalid padding
 *                  ESL_ERC_RSA_PUBKEY_INVALID  The public exponent is invalid
 *                  ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *  \pre            workSpace is initialized by esl_initVerifyRSARIPEMD160_V15(..)

 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSARIPEMD160_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);
# endif /* (VSECPRIM_RSA_V15_RIPEMD160_ENABLED == STD_ON) */

# if (VSECPRIM_RSA_V15_SHA2_256_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto functions RSA (sign/verify) with SHA-256
 * according to PKCS#1 1.5
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initSignRSASHA256_V15()
 *********************************************************************************************************************/
/*! \brief          Initialize Signature for RSA PKCS#1 V1.5 with SHA-256
 *  \details        -
 *  \param[in]      privateKeyExponentSize  length of the privateKeyExponent (bytes)
 *  \param[in]      keyPairModuleSize  length of the keyPairModulus (bytes)
 *  \param[in]      privateKeyExponent  pointer to the private key exponent
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      keyPairModule  pointer to the key modulus
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_PRIVKEY_INVALID  The private exponent is invalid
 *                  ESL_ERC_RSA_MODULE_OUT_OF_RANGE the given module is not supported
 *                  - if keyPairModuleSize < 46d = headLen+hLen+11, where
 *                  + hLen is the length of the Hashvalue (SHA-256  32d= 0x20)
 *                  + headLen is the AlgoIDentifier (RSA+SHA256  19d=0x13)
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *                  privateKeyExponent  < keyPairModule
 *                  Module is a prime
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignRSASHA256_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent   );

/**********************************************************************************************************************
 *  esl_updateSignRSASHA256_V15()
 *********************************************************************************************************************/
/*! \brief          Update Signature for RSA PKCS#1 V1.5 with SHA-256
 *  \details        -
 *  \param[in]      input        pointer to the input data
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      inputSize    size of the input data (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID - format problems, nullpointer, ...
 *                  ESL_ERC_SHA256_TOTAL_LENGTH_OVERFLOW  Maximum total input length exceeded
 *  \pre            workSpace is initialized by esl_initSignRSASHA256_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSignRSASHA256_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig) workSpace,
   eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeSignRSASHA256_V15()
 *********************************************************************************************************************/
/*! \brief          Finalize Signature for RSA PKCS#1 V1.5 with SHA-256
 *  \details        -
 *  \param[out]     signature    pointer to the signature
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[out]     signatureSize  length of the signature (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT outputSize too small
 *  \pre            workSpace is initialized by esl_initSignRSASHA256_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSASHA256_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/**********************************************************************************************************************
 *  esl_initSignRSACRTSHA256_V15()
 *********************************************************************************************************************/
/*! \brief          Initialize Signature for RSA PKCS#1 V1.5 CRT with SHA-256
 *  \details        -
 *  \param[in]      keyPairPrimeQSize  length of the keyPairPrimeQ (bytes)
 *  \param[in]      keyPairPrimeP  pointer to the 1st key prime
 *  \param[in]      privateKeyInverseQISize  length of the privateKeyInverseQI (bytes)
 *  \param[in]      privateKeyInverseQI  pointer to the inverse
 *  \param[in]      privateKeyExponentDPSize  length of the privKeyExponenentDP (bytes)
 *  \param[in]      keyPairPrimeQ  pointer to the 2nd key prime
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      privateKeyExponentDQSize  length of the privKeyExponenentDQ (bytes)
 *  \param[in]      privateKeyExponentDP  pointer to the partial privKey exponent
 *  \param[in]      privateKeyExponentDQ  pointer to the partial privKey exponent
 *  \param[in]      keyPairPrimePSize  length of the keyPairPrimeP (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_PRIVKEY_INVALID  The private exponent is invalid
 *                  ESL_ERC_RSA_MODULE_OUT_OF_RANGE the given module is not supported
 *                  - keyPairModuleSize < 46d = headLen+hLen+11, where
 *                  + hLen is the length of the Hashvalue (SHA-256  32d= 0x20)
 *                  + headLen is the AlgoIDentifier (RSA+SHA256  19d=0x13)
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *                  PrimeP and PrimeQ are a prime
 *                  privateKeyExpDP<PrimeP
 *                  privateKeyExpDQ<PrimeQ
 *                  QI is inverse to Q, could be checked, then move this line to errorcodes
 *                  the Hash-Function is SHA-1
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignRSACRTSHA256_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTsig) workSpace,
   eslt_Length keyPairPrimePSize, eslt_pRomByte keyPairPrimeP,
   eslt_Length keyPairPrimeQSize, eslt_pRomByte keyPairPrimeQ,
   eslt_Length privateKeyExponentDPSize, eslt_pRomByte privateKeyExponentDP,
   eslt_Length privateKeyExponentDQSize, eslt_pRomByte privateKeyExponentDQ,
   eslt_Length privateKeyInverseQISize, eslt_pRomByte privateKeyInverseQI);

/**********************************************************************************************************************
 *  esl_updateSignRSACRTSHA256_V15()
 *********************************************************************************************************************/
/*! \brief          Update Signature for RSA PKCS#1 V1.5 CRT with SHA-256
 *  \details        -
 *  \param[in]      input        pointer to the input data
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      inputSize    size of the input data (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID - format problems, nullpointer, ...
 *                  ESL_ERC_SHA256_TOTAL_LENGTH_OVERFLOW  Maximum total input length exceeded
 *  \pre            workSpace is initialized by esl_initSignRSACRTSHA256_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSignRSACRTSHA256_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTsig) workSpace,
   const eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeSignRSACRTSHA256_V15()
 *********************************************************************************************************************/
/*! \brief          Finalize Signature for RSA PKCS#1 V1.5 CRT with SHA-256
 *  \details        -
 *  \param[out]     signature    pointer to the signature
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[out]     signatureSize  length of the signature (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_OUTPUT_SIZE_TOO_SHORT outputSize too small
 *  \pre            workSpace is initialized by esl_initSignRSACRTSHA256_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSACRTSHA256_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/**********************************************************************************************************************
 *  esl_initVerifyRSASHA256_V15()
 *********************************************************************************************************************/
/*! \brief          Initialize signature verification for RSA PKCS#1 V1.5 with SHA-256
 *  \details        -
 *  \param[in]      publicKeyExponentSize  length of the public publicKeyExponent (bytes)
 *  \param[in]      keyPairModuleSize  length of the keyPairModulus (bytes)
 *  \param[in]      publicKeyExponent  pointer to the public exponent
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      keyPairModule  pointer to the key modulus
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_PUBKEY_INVALID  The public exponent is invalid
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *                  publicKeyExponent  < keyPairModule
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initVerifyRSASHA256_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length publicKeyExponentSize, eslt_pRomByte publicKeyExponent);

/**********************************************************************************************************************
 *  esl_updateVerifyRSASHA256_V15()
 *********************************************************************************************************************/
/*! \brief          Update signature verification for RSA PKCS#1 V1.5 with SHA-256
 *  \details        -
 *  \param[in]      input        pointer to the input data
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      inputSize    size of the input data (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID - format problems, nullpointer, ...
 *                  ESL_ERC_SHA256_TOTAL_LENGTH_OVERFLOW  Maximum total input length exceeded
 *  \pre            workSpace is initialized by esl_initVerifyRSASHA256_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateVerifyRSASHA256_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace,
   eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/**********************************************************************************************************************
 *  esl_finalizeHashVerifyRSASHA256_V15
 *********************************************************************************************************************/
/*! \brief          Finalize hash for signature verification for RSA PKCS#1 V1.5 with SHA-256
 *  \details        -
 *  \param[in,out]  messageDigest  pointer to computed hash value
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *  \pre            workSpace is initialized by esl_initVerifyRSASHA256_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashVerifyRSASHA256_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

/**********************************************************************************************************************
 *  esl_verifySigVerifyRSASHA256_V15()
 *********************************************************************************************************************/
/*! \brief          Finalize signature for signature verification for RSA PKCS#1 V1.5 with SHA-256
 *  \details        -
 *  \param[in]      signature    pointer to the signature
 *  \param[in]      messageDigest  pointer to computed hash value
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      signatureSize  length of the signature (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_SIGNATURE_OUT_OF_RANGE
 *                  - 0 <= (int) signature <= module-1, where module = PrimeP*PrimeQ
 *                  ESL_ERC_RSA_ENCODING_INVALID  Invalid padding
 *                  ESL_ERC_RSA_PUBKEY_INVALID  The public exponent is invalid
 *                  ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *  \pre            workSpace is initialized by esl_initVerifyRSASHA256_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSASHA256_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace, VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/**********************************************************************************************************************
 *  esl_finalizeVerifyRSASHA256_V15()
 *********************************************************************************************************************/
/*! \brief          Finalize hash and signature for signature verification for RSA PKCS#1 V1.5 with SHA-256
 *  \details        -
 *  \param[in]      signature    pointer to the signature
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      signatureSize  length of the signature (bytes)
 *  \return         ESL_ERC_NO_ERROR Operation successful
 *                  ESL_ERC_WS_TOO_SMALL Work space too small
 *                  ESL_ERC_WS_STATE_INVALID Invalid state
 *                  ESL_ERC_PARAMETER_INVALID  - format problems, nullpointer, ...
 *                  ESL_ERC_RSA_SIGNATURE_OUT_OF_RANGE
 *                  - 0 <= (int) signature <= module-1, where module = PrimeP*PrimeQ
 *                  ESL_ERC_RSA_ENCODING_INVALID  Invalid padding
 *                  ESL_ERC_RSA_PUBKEY_INVALID  The public exponent is invalid
 *                  ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *  \pre            workSpace is initialized by esl_initVerifyRSASHA256_V15(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSASHA256_V15(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAver) workSpace,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);
# endif /* (VSECPRIM_RSA_V15_SHA2_256_ENABLED == STD_ON) */

# if (VSECPRIM_RSA_OAEP_SHA1_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto functions RSA (encrypt/decrypt) using SHA-1
 * according to PKCS#1 OAEP
 *****************************************************************************/
/***********************************************************************************************************************
 *  esl_initEncryptRSASHA1_OAEP
 **********************************************************************************************************************/
/*! \brief         Initialize encryption
 *  \details       -
 *  \param[in,out] workSpace  Encryption work space
 *  \param[in]     keyPairModuleSize  Size of RSA key pair module
 *  \param[in]     keyPairModule  RSA key pair module
 *  \param[in]     publicKeyExponentSize  Size of public RSA key pair exponent
 *  \param[in]     publicKeyExponent  Public RSA key pair exponent
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_PUBKEY_INVALID  The public exponent is invalid
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptRSASHA1_OAEP(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPenc) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length publicKeyExponentSize, eslt_pRomByte publicKeyExponent);

/***********************************************************************************************************************
 *  esl_encryptRSASHA1_OAEP_Label_Seed
 **********************************************************************************************************************/
/*! \brief         Encrypt message
 *  \details       -
 *  \param[in,out] workSpace  Encryption work space
 *  \param[in]     messageSize  Size of data to be encrypted
 *  \param[in]     message  Data to be encrypted
 *  \param[in]     labelSize  Size of optional label
 *  \param[in]     label  Optional label to be associated with the message
 *  \param[in]     inSeedSize  Size of seed (must match size if hash digest)
 *  \param[in]     inSeed  Seed value (pass null pointer to trigger internal generation of random seed)
 *  \param[in,out] cipherSize  Input  Maximum size of encrypted data
 *                             Output  Actual size of encrypted data
 *  \param[out]    cipher  Encrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptRSASHA1_OAEP_Label_Seed(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPenc) workSpace,
   eslt_Length messageSize, VSECPRIM_P2CONST_PARA(eslt_Byte) message,
   eslt_Length labelSize, VSECPRIM_P2CONST_PARA(eslt_Byte) label,
   eslt_Length inSeedSize, VSECPRIM_P2CONST_PARA(eslt_Byte) inSeed,
   VSECPRIM_P2VAR_PARA(eslt_Length) cipherSize, VSECPRIM_P2VAR_PARA(eslt_Byte) cipher);

/***********************************************************************************************************************
 *  esl_encryptRSASHA1_OAEP
 **********************************************************************************************************************/
/*! \brief         Encrypt message
 *  \details       Random seed generated internally, empty string used as default label
 *  \param[in,out] workSpace  Encryption work space
 *  \param[in]     messageSize  Size of data to be encrypted
 *  \param[in]     message  Data to be encrypted
 *  \param[in,out] cipherSize  Input  Maximum size of encrypted data
 *                             Output  Actual size of encrypted data
 *  \param[out]    cipher  Encrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptRSASHA1_OAEP(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPenc) workSpace,
   eslt_Length messageSize, VSECPRIM_P2CONST_PARA(eslt_Byte) message,
   VSECPRIM_P2VAR_PARA(eslt_Length) cipherSize, VSECPRIM_P2VAR_PARA(eslt_Byte) cipher);

/***********************************************************************************************************************
 *  esl_encryptRSASHA1_OAEP_Seed
 **********************************************************************************************************************/
/*! \brief         Encrypt message
 *  \details       Empty string used as default label
 *  \param[in,out] workSpace  Encryption work space
 *  \param[in]     messageSize  Size of data to be encrypted
 *  \param[in]     message  Data to be encrypted
 *  \param[in]     inSeedSize  Size of seed (must match size if hash digest)
 *  \param[in]     inSeed  Seed value (pass null pointer to trigger internal generation of random seed)
 *  \param[in,out] cipherSize  Input  Maximum size of encrypted data
 *                             Output  Actual size of encrypted data
 *  \param[out]    cipher  Encrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptRSASHA1_OAEP_Seed(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPenc) workSpace,
   eslt_Length messageSize, VSECPRIM_P2CONST_PARA(eslt_Byte) message,
   eslt_Length inSeedSize, VSECPRIM_P2CONST_PARA(eslt_Byte) inSeed,
   VSECPRIM_P2VAR_PARA(eslt_Length) cipherSize, VSECPRIM_P2VAR_PARA(eslt_Byte) cipher);

/***********************************************************************************************************************
 *  esl_encryptRSASHA1_OAEP_Label
 **********************************************************************************************************************/
/*! \brief         Encrypt message
 *  \details       Random seed generated internally
 *  \param[in,out] workSpace  Encryption work space
 *  \param[in]     messageSize  Size of data to be encrypted
 *  \param[in]     message  Data to be encrypted
 *  \param[in]     labelSize  Size of optional label
 *  \param[in]     label  Optional label to be associated with the message
 *  \param[in,out] cipherSize  Input  Maximum size of encrypted data
 *                             Output  Actual size of encrypted data
 *  \param[out]    cipher  Encrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptRSASHA1_OAEP_Label(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPenc) workSpace,
   eslt_Length messageSize, VSECPRIM_P2CONST_PARA(eslt_Byte) message,
   eslt_Length labelSize, VSECPRIM_P2CONST_PARA(eslt_Byte) label,
   VSECPRIM_P2VAR_PARA(eslt_Length) cipherSize, VSECPRIM_P2VAR_PARA(eslt_Byte) cipher);

/***********************************************************************************************************************
 *  esl_initDecryptRSASHA1_OAEP
 **********************************************************************************************************************/
/*! \brief         Initialize decryption
 *  \details       -
 *  \param[in,out] workSpace  Decryption work space
 *  \param[in]     keyPairModuleSize  Size of RSA key pair module
 *  \param[in]     keyPairModule  RSA key pair module
 *  \param[in]     privateKeyExponentSize  Size of private RSA key pair exponent
 *  \param[in]     privateKeyExponent  Private RSA key pair exponent
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_PRIVKEY_INVALID  The private exponent is invalid
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptRSASHA1_OAEP(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPdec) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent);

/***********************************************************************************************************************
 *  esl_decodeRSASHA1_OAEP
 **********************************************************************************************************************/
/*! \brief         Decode an OAEP encoded message
 *  \details       -
 *  \param[in,out] workSpace  Decryption work space
 *  \param[in]     encodedMessageSize  Size of encoded message located in work space
 *  \param[in]     labelSize  Size of optional label
 *  \param[in]     label  Optional label associated with the message
 *  \param[in,out] messageSize  Input  Maximum size of decrypted data
 *                              Output  Actual size of decrypted data
 *  \param[out]    message  Decrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decodeRSASHA1_OAEP(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpace) workSpace, eslt_Length encodedMessageSize,
   eslt_Length labelSize, VSECPRIM_P2CONST_PARA(eslt_Byte) label,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);

/***********************************************************************************************************************
 *  esl_decryptRSASHA1_OAEP_Label
 **********************************************************************************************************************/
/*! \brief         Decrypt message
 *  \details       -
 *  \param[in,out] workSpace  Decryption work space
 *  \param[in]     cipherSize  Size of encrypted data
 *  \param[in]     cipher  Encrypted data
 *  \param[in]     labelSize  Size of optional label
 *  \param[in]     label  Optional label associated with the message
 *  \param[in,out] messageSize  Input  Maximum size of decrypted data
 *                              Output  Actual size of decrypted data
 *  \param[out]    message  Decrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSASHA1_OAEP_Label(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPdec) workSpace,
   eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
   eslt_Length labelSize, VSECPRIM_P2CONST_PARA(eslt_Byte) label,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);

/***********************************************************************************************************************
 *  esl_decryptRSASHA1_OAEP
 **********************************************************************************************************************/
/*! \brief         Decrypt message
 *  \details       Empty string used as default label
 *  \param[in,out] workSpace  Decryption work space
 *  \param[in]     cipherSize  Size of encrypted data
 *  \param[in]     cipher  Encrypted data
 *  \param[in,out] messageSize  Input  Maximum size of decrypted data
 *                              Output  Actual size of decrypted data
 *  \param[out]    message  Decrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSASHA1_OAEP(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPdec) workSpace,
   eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);

/***********************************************************************************************************************
 *  esl_initDecryptRSACRTSHA1_OAEP
 **********************************************************************************************************************/
/*! \brief         Initialize decryption (CRT variant)
 *  \details       -
 *  \param[in,out] workSpace  Decryption work space
 *  \param[in]     keyPairPrimePSize  Size of secret prime p
 *  \param[in]     keyPairPrimeP  Secret prime p
 *  \param[in]     keyPairPrimeQSize  Size of secret prime q
 *  \param[in]     keyPairPrimeQ  Secret prime q
 *  \param[in]     privateKeyExponentDPSize  Size of private key component dp
 *  \param[in]     privateKeyExponentDP  Private key component dp
 *  \param[in]     privateKeyExponentDQSize  Size of private key component dq
 *  \param[in]     privateKeyExponentDQ  Private key component dq
 *  \param[in]     privateKeyInverseQISize  Size of private key component q^-1 mod p
 *  \param[in]     privateKeyInverseQI  Private key component q^-1 mod p
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_PRIVKEY_INVALID  The private exponent is invalid
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptRSACRTSHA1_OAEP(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTOAEPdec) workSpace,
   eslt_Length keyPairPrimePSize, eslt_pRomByte keyPairPrimeP,
   eslt_Length keyPairPrimeQSize, eslt_pRomByte keyPairPrimeQ,
   eslt_Length privateKeyExponentDPSize, eslt_pRomByte privateKeyExponentDP,
   eslt_Length privateKeyExponentDQSize, eslt_pRomByte privateKeyExponentDQ,
   eslt_Length privateKeyInverseQISize, eslt_pRomByte privateKeyInverseQI);

/***********************************************************************************************************************
 *  esl_decryptRSACRTSHA1_OAEP_Label
 **********************************************************************************************************************/
/*! \brief         Decrypt message (CRT variant)
 *  \details       -
 *  \param[in,out] workSpace  Decryption work space
 *  \param[in]     cipherSize  Size of encrypted data
 *  \param[in]     cipher  Encrypted data
 *  \param[in]     labelSize  Size of optional label
 *  \param[in]     label  Optional label associated with the message
 *  \param[in,out] messageSize  Input  Maximum size of decrypted data
 *                              Output  Actual size of decrypted data
 *  \param[out]    message  Decrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSACRTSHA1_OAEP_Label(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTOAEPdec) workSpace,
   eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
   eslt_Length labelSize, VSECPRIM_P2CONST_PARA(eslt_Byte) label,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);

/***********************************************************************************************************************
 *  esl_decryptRSACRTSHA1_OAEP
 **********************************************************************************************************************/
/*! \brief         Decrypt message (CRT variant)
 *  \details       Empty string used as default label
 *  \param[in,out] workSpace  Decryption work space
 *  \param[in]     cipherSize  Size of encrypted data
 *  \param[in]     cipher  Encrypted data
 *  \param[in,out] messageSize  Input  Maximum size of decrypted data
 *                              Output  Actual size of decrypted data
 *  \param[out]    message  Decrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSACRTSHA1_OAEP(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTOAEPdec) workSpace,
   eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);
# endif /* (VSECPRIM_RSA_OAEP_SHA1_ENABLED == STD_ON) */

# if (VSECPRIM_RSA_OAEP_SHA2_256_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto functions RSA (encrypt/decrypt) using SHA-256
 * according to PKCS#1 OAEP
 *****************************************************************************/

/***********************************************************************************************************************
 *  esl_initEncryptRSASHA256_OAEP
 **********************************************************************************************************************/
/*! \brief         Initialize encryption
 *  \details       -
 *  \param[in,out] workSpace  Encryption work space
 *  \param[in]     keyPairModuleSize  Size of RSA key pair module
 *  \param[in]     keyPairModule  RSA key pair module
 *  \param[in]     publicKeyExponentSize  Size of public RSA key pair exponent
 *  \param[in]     publicKeyExponent  Public RSA key pair exponent
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_PUBKEY_INVALID  The public exponent is invalid
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptRSASHA256_OAEP(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPenc) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length publicKeyExponentSize, eslt_pRomByte publicKeyExponent);

/***********************************************************************************************************************
 *  esl_encryptRSASHA256_OAEP_Label_Seed
 **********************************************************************************************************************/
/*! \brief         Encrypt message
 *  \details       -
 *  \param[in,out] workSpace  Encryption work space
 *  \param[in]     messageSize  Size of data to be encrypted
 *  \param[in]     message  Data to be encrypted
 *  \param[in]     labelSize  Size of optional label
 *  \param[in]     label  Optional label to be associated with the message
 *  \param[in]     inSeedSize  Size of seed (must match size if hash digest)
 *  \param[in]     inSeed  Seed value (pass null pointer to trigger internal generation of random seed)
 *  \param[in,out] cipherSize  Input  Maximum size of encrypted data
 *                             Output  Actual size of encrypted data
 *  \param[out]    cipher  Encrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptRSASHA256_OAEP_Label_Seed(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPenc) workSpace,
   eslt_Length messageSize, VSECPRIM_P2CONST_PARA(eslt_Byte) message,
   eslt_Length labelSize, VSECPRIM_P2CONST_PARA(eslt_Byte) label,
   eslt_Length inSeedSize, VSECPRIM_P2CONST_PARA(eslt_Byte) inSeed,
   VSECPRIM_P2VAR_PARA(eslt_Length) cipherSize, VSECPRIM_P2VAR_PARA(eslt_Byte) cipher);

/***********************************************************************************************************************
 *  esl_encryptRSASHA256_OAEP
 **********************************************************************************************************************/
/*! \brief         Encrypt message
 *  \details       Random seed generated internally, empty string used as default label
 *  \param[in,out] workSpace  Encryption work space
 *  \param[in]     messageSize  Size of data to be encrypted
 *  \param[in]     message  Data to be encrypted
 *  \param[in,out] cipherSize  Input  Maximum size of encrypted data
 *                             Output  Actual size of encrypted data
 *  \param[out]    cipher  Encrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptRSASHA256_OAEP(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPenc) workSpace,
   eslt_Length messageSize, VSECPRIM_P2CONST_PARA(eslt_Byte) message,
   VSECPRIM_P2VAR_PARA(eslt_Length) cipherSize, VSECPRIM_P2VAR_PARA(eslt_Byte) cipher);

/***********************************************************************************************************************
 *  esl_encryptRSASHA256_OAEP_Seed
 **********************************************************************************************************************/
/*! \brief         Encrypt message
 *  \details       Empty string used as default label
 *  \param[in,out] workSpace  Encryption work space
 *  \param[in]     messageSize  Size of data to be encrypted
 *  \param[in]     message  Data to be encrypted
 *  \param[in]     inSeedSize  Size of seed (must match size if hash digest)
 *  \param[in]     inSeed  Seed value (pass null pointer to trigger internal generation of random seed)
 *  \param[in,out] cipherSize  Input  Maximum size of encrypted data
 *                             Output  Actual size of encrypted data
 *  \param[out]    cipher  Encrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptRSASHA256_OAEP_Seed(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPenc) workSpace,
   eslt_Length messageSize, VSECPRIM_P2CONST_PARA(eslt_Byte) message,
   eslt_Length inSeedSize, VSECPRIM_P2CONST_PARA(eslt_Byte) inSeed,
   VSECPRIM_P2VAR_PARA(eslt_Length) cipherSize, VSECPRIM_P2VAR_PARA(eslt_Byte) cipher);

/***********************************************************************************************************************
 *  esl_encryptRSASHA256_OAEP_Label
 **********************************************************************************************************************/
/*! \brief         Encrypt message
 *  \details       Random seed generated internally
 *  \param[in,out] workSpace  Encryption work space
 *  \param[in]     messageSize  Size of data to be encrypted
 *  \param[in]     message  Data to be encrypted
 *  \param[in]     labelSize  Size of optional label
 *  \param[in]     label  Optional label to be associated with the message
 *  \param[in,out] cipherSize  Input  Maximum size of encrypted data
 *                             Output  Actual size of encrypted data
 *  \param[out]    cipher  Encrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptRSASHA256_OAEP_Label(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPenc) workSpace,
   eslt_Length messageSize, VSECPRIM_P2CONST_PARA(eslt_Byte) message,
   eslt_Length labelSize, VSECPRIM_P2CONST_PARA(eslt_Byte) label,
   VSECPRIM_P2VAR_PARA(eslt_Length) cipherSize, VSECPRIM_P2VAR_PARA(eslt_Byte) cipher);

/***********************************************************************************************************************
 *  esl_initDecryptRSASHA256_OAEP
 **********************************************************************************************************************/
/*! \brief         Initialize decryption
 *  \details       -
 *  \param[in,out] workSpace  Decryption work space
 *  \param[in]     keyPairModuleSize  Size of RSA key pair module
 *  \param[in]     keyPairModule  RSA key pair module
 *  \param[in]     privateKeyExponentSize  Size of private RSA key pair exponent
 *  \param[in]     privateKeyExponent  Private RSA key pair exponent
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_PRIVKEY_INVALID  The private exponent is invalid
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptRSASHA256_OAEP(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPdec) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent);

/***********************************************************************************************************************
 *  esl_decodeRSASHA256_OAEP
 **********************************************************************************************************************/
/*! \brief         Decode an OAEP encoded message
 *  \details       -
 *  \param[in,out] workSpace  Decryption work space
 *  \param[in]     encodedMessageSize  Size of encoded message located in work space
 *  \param[in]     labelSize  Size of optional label
 *  \param[in]     label  Optional label associated with the message
 *  \param[in,out] messageSize  Input  Maximum size of decrypted data
 *                              Output  Actual size of decrypted data
 *  \param[out]    message  Decrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decodeRSASHA256_OAEP(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpace) workSpace, eslt_Length encodedMessageSize,
   eslt_Length labelSize, VSECPRIM_P2CONST_PARA(eslt_Byte) label,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);

/***********************************************************************************************************************
 *  esl_decryptRSASHA256_OAEP_Label
 **********************************************************************************************************************/
/*! \brief         Decrypt message
 *  \details       -
 *  \param[in,out] workSpace  Decryption work space
 *  \param[in]     cipherSize  Size of encrypted data
 *  \param[in]     cipher  Encrypted data
 *  \param[in]     labelSize  Size of optional label
 *  \param[in]     label  Optional label associated with the message
 *  \param[in,out] messageSize  Input  Maximum size of decrypted data
 *                              Output  Actual size of decrypted data
 *  \param[out]    message  Decrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSASHA256_OAEP_Label(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPdec) workSpace,
   eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
   eslt_Length labelSize, VSECPRIM_P2CONST_PARA(eslt_Byte) label,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);

/***********************************************************************************************************************
 *  esl_decryptRSASHA256_OAEP
 **********************************************************************************************************************/
/*! \brief         Decrypt message
 *  \details       Empty string used as default label
 *  \param[in,out] workSpace  Decryption work space
 *  \param[in]     cipherSize  Size of encrypted data
 *  \param[in]     cipher  Encrypted data
 *  \param[in,out] messageSize  Input  Maximum size of decrypted data
 *                              Output  Actual size of decrypted data
 *  \param[out]    message  Decrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSASHA256_OAEP(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAOAEPdec) workSpace,
   eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);

/***********************************************************************************************************************
 *  esl_initDecryptRSACRTSHA256_OAEP
 **********************************************************************************************************************/
/*! \brief         Initialize decryption (CRT variant)
 *  \details       -
 *  \param[in,out] workSpace  Decryption work space
 *  \param[in]     keyPairPrimePSize  Size of secret prime p
 *  \param[in]     keyPairPrimeP  Secret prime p
 *  \param[in]     keyPairPrimeQSize  Size of secret prime q
 *  \param[in]     keyPairPrimeQ  Secret prime q
 *  \param[in]     privateKeyExponentDPSize  Size of private key component dp
 *  \param[in]     privateKeyExponentDP  Private key component dp
 *  \param[in]     privateKeyExponentDQSize  Size of private key component dq
 *  \param[in]     privateKeyExponentDQ  Private key component dq
 *  \param[in]     privateKeyInverseQISize  Size of private key component q^-1 mod p
 *  \param[in]     privateKeyInverseQI  Private key component q^-1 mod p
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_PRIVKEY_INVALID  The private exponent is invalid
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptRSACRTSHA256_OAEP(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTOAEPdec) workSpace,
   eslt_Length keyPairPrimePSize, eslt_pRomByte keyPairPrimeP,
   eslt_Length keyPairPrimeQSize, eslt_pRomByte keyPairPrimeQ,
   eslt_Length privateKeyExponentDPSize, eslt_pRomByte privateKeyExponentDP,
   eslt_Length privateKeyExponentDQSize, eslt_pRomByte privateKeyExponentDQ,
   eslt_Length privateKeyInverseQISize, eslt_pRomByte privateKeyInverseQI);

/***********************************************************************************************************************
 *  esl_decryptRSACRTSHA256_OAEP_Label
 **********************************************************************************************************************/
/*! \brief         Decrypt message (CRT variant)
 *  \details       -
 *  \param[in,out] workSpace  Decryption work space
 *  \param[in]     cipherSize  Size of encrypted data
 *  \param[in]     cipher  Encrypted data
 *  \param[in]     labelSize  Size of optional label
 *  \param[in]     label  Optional label associated with the message
 *  \param[in,out] messageSize  Input  Maximum size of decrypted data
 *                              Output  Actual size of decrypted data
 *  \param[out]    message  Decrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSACRTSHA256_OAEP_Label(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTOAEPdec) workSpace,
   eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
   eslt_Length labelSize, VSECPRIM_P2CONST_PARA(eslt_Byte) label,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);

/***********************************************************************************************************************
 *  esl_decryptRSACRTSHA256_OAEP
 **********************************************************************************************************************/
/*! \brief         Decrypt message (CRT variant)
 *  \details       Empty string used as default label
 *  \param[in,out] workSpace  Decryption work space
 *  \param[in]     cipherSize  Size of encrypted data
 *  \param[in]     cipher  Encrypted data
 *  \param[in,out] messageSize  Input  Maximum size of decrypted data
 *                              Output  Actual size of decrypted data
 *  \param[out]    message  Decrypted data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_MESSAGE_OUT_OF_RANGE  Input message out of range
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptRSACRTSHA256_OAEP(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSACRTOAEPdec) workSpace,
   eslt_Length cipherSize, VSECPRIM_P2CONST_PARA(eslt_Byte) cipher,
   VSECPRIM_P2VAR_PARA(eslt_Length) messageSize, VSECPRIM_P2VAR_PARA(eslt_Byte) message);
# endif /* (VSECPRIM_RSA_OAEP_SHA2_256_ENABLED == STD_ON) */

# if (VSECPRIM_RSA_PSS_SHA1_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto functions RSA (sign/verify) with SHA-1
 * according to PKCS#1 PSS
 *****************************************************************************/

/***********************************************************************************************************************
 *  esl_initSignRSASHA1_PSS
 **********************************************************************************************************************/
/*! \brief         Initialize signature generation
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     keyPairModuleSize  Size of RSA key pair module
 *  \param[in]     keyPairModule  RSA key pair module
 *  \param[in]     privateKeyExponentSize  Size of private RSA key pair exponent
 *  \param[in]     privateKeyExponent  Private RSA key pair exponent
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_PRIVKEY_INVALID  The private exponent is invalid
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignRSASHA1_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent);

/***********************************************************************************************************************
 *  esl_updateSignRSASHA1_PSS
 **********************************************************************************************************************/
/*! \brief         Update message digest (hash) with data to be signed
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     inputSize  Size of data to be hashed
 *  \param[in]     input  Data to be hashed
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW  Maximum total input length exceeded
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSignRSASHA1_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/***********************************************************************************************************************
 *  esl_finalizeHashSignRSASHA1_PSS
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[out]    messageDigest  Message digest (hash) of previously hashed data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashSignRSASHA1_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

/***********************************************************************************************************************
 *  esl_finalizeSigSignRSASHA1_PSS
 **********************************************************************************************************************/
/*! \brief         Generate signature
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     saltSize  Size of salt to be used
 *  \param[in]     salt  Salt to be used
 *                       0 (NULL) if salt should be generated randomly
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSigSignRSASHA1_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length saltSize, VSECPRIM_P2CONST_PARA(eslt_Byte) salt,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSigSignRSASHA1_PSS_NoSalt
 **********************************************************************************************************************/
/*! \brief         Generate signature
 *  \details       Use no salt for signature generation
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSigSignRSASHA1_PSS_NoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSigSignRSASHA1_PSS_DigestLengthSalt
 **********************************************************************************************************************/
/*! \brief         Generate signature
 *  \details       Use random salt with size equal to message digest for signature generation
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSigSignRSASHA1_PSS_DigestLengthSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSignRSASHA1_PSS
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and generate signature
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     saltSize  Size of salt to be used
 *  \param[in]     salt  Salt to be used
 *                       0 (NULL) if salt should be generated randomly
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSASHA1_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   eslt_Length saltSize, VSECPRIM_P2CONST_PARA(eslt_Byte) salt,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSignRSASHA1_PSS_NoSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and generate signature
 *  \details       Use no salt for signature generation
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSASHA1_PSS_NoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSignRSASHA1_PSS_DigestLengthSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and generate signature
 *  \details       Use random salt with size equal to message digest for signature generation
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSASHA1_PSS_DigestLengthSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_initVerifyRSASHA1_PSS
 **********************************************************************************************************************/
/*! \brief         Initialize signature verification
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     keyPairModuleSize  Size of RSA key pair module
 *  \param[in]     keyPairModule  RSA key pair module
 *  \param[in]     publicKeyExponentSize  Size of public RSA key pair exponent
 *  \param[in]     publicKeyExponent  Public RSA key pair exponent
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_PUBKEY_INVALID  The public exponent is invalid
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initVerifyRSASHA1_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length publicKeyExponentSize, eslt_pRomByte publicKeyExponent);

/***********************************************************************************************************************
 *  esl_updateVerifyRSASHA1_PSS
 **********************************************************************************************************************/
/*! \brief         Update message digest (hash) with signed data
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     inputSize  Size of data to be hashed
 *  \param[in]     input  Data to be hashed
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_SHA1_TOTAL_LENGTH_OVERFLOW  Maximum total input length exceeded
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateVerifyRSASHA1_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/***********************************************************************************************************************
 *  esl_finalizeHashVerifyRSASHA1_PSS
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[out]    messageDigest  Message digest (hash) of previously hashed data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashVerifyRSASHA1_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

/***********************************************************************************************************************
 *  esl_verifySigVerifyRSASHA1_PSS
 **********************************************************************************************************************/
/*! \brief         Verify signature
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     saltSize  Expected size of salt,
 *                           -1 if size of salt used during signature generation should be evaluated automatically
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSASHA1_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length saltSize,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_verifySigVerifyRSASHA1_PSS_AutoSalt
 **********************************************************************************************************************/
/*! \brief         Verify signature
 *  \details       Size of salt used during signature generation is evaluated automatically
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSASHA1_PSS_AutoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_verifySigVerifyRSASHA1_PSS_NoSalt
 **********************************************************************************************************************/
/*! \brief         Verify signature
 *  \details       Requires that no salt was used during signature generation
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSASHA1_PSS_NoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_verifySigVerifyRSASHA1_PSS_DigestLengthSalt
 **********************************************************************************************************************/
/*! \brief         Verify signature
 *  \details       Requires that salt with size equal to message digest was used during signature generation
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSASHA1_PSS_DigestLengthSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeVerifyRSASHA1_PSS
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and verify signature
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     saltSize  Expected size of salt,
 *                           -1 if size of salt used during signature generation should be evaluated automatically
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSASHA1_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length saltSize, eslt_Length signatureSize,
   VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeVerifyRSASHA1_PSS_AutoSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and verify signature
 *  \details       Size of salt used during signature generation is evaluated automatically
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSASHA1_PSS_AutoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeVerifyRSASHA1_PSS_NoSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and verify signature
 *  \details       Requires that no salt was used during signature generation
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSASHA1_PSS_NoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeVerifyRSASHA1_PSS_DigestLengthSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and verify signature
 *  \details       Requires that salt with size equal to message digest was used during signature generation
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSASHA1_PSS_DigestLengthSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_calcSaltedHashRSASHA1_PSS
 **********************************************************************************************************************/
/*! \brief      Calculate salted hash value (SHA-1)
 *  \details    Based on a given hash and a random salt the hash of
 *                M' = (0x) 00 00 00 00 00 00 00 00 || hash || salt
 *              is calculated, as described in chapter 9.1.1, steps 5-6 and 9.1.2, steps 12-13 of PKCS#1 2.2
 *  \param[in,out] wsHash  Pointer to hash workspace
 *  \param[in]  saltSize  Length of salt
 *  \param[in]  salt  Input salt
 *  \param[in]  messageDigest  Input hash (SHA-1)
 *  \param[out] saltedHash  Output buffer for calculated hash (SHA-1), has to be large enough to hold requested length
 *  \return     ESL_ERC_NO_ERROR  Operation successful
 *              ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *              ESL_ERC_WS_STATE_INVALID  Invalid state
 *              ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_calcSaltedHashRSASHA1_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA1) wsHash,
   eslt_Length saltSize, VSECPRIM_P2CONST_PARA(eslt_Byte) salt,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   VSECPRIM_P2VAR_PARA(eslt_Byte) saltedHash);
# endif /* (VSECPRIM_RSA_PSS_SHA1_ENABLED == STD_ON) */

# if (VSECPRIM_RSA_PSS_RIPEMD160_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto functions RSA (sign/verify) with RIPEMD-160
 * according to PKCS#1 PSS
 *****************************************************************************/

/***********************************************************************************************************************
 *  esl_initSignRSARIPEMD160_PSS
 **********************************************************************************************************************/
/*! \brief         Initialize signature generation
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     keyPairModuleSize  Size of RSA key pair module
 *  \param[in]     keyPairModule  RSA key pair module
 *  \param[in]     privateKeyExponentSize  Size of private RSA key pair exponent
 *  \param[in]     privateKeyExponent  Private RSA key pair exponent
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_PRIVKEY_INVALID  The private exponent is invalid
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignRSARIPEMD160_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent);

/***********************************************************************************************************************
 *  esl_updateSignRSARIPEMD160_PSS
 **********************************************************************************************************************/
/*! \brief         Update message digest (hash) with data to be signed
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     inputSize  Size of data to be hashed
 *  \param[in]     input  Data to be hashed
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RIPEMD160_TOTAL_LENGTH_OVERFLOW  Maximum total input length exceeded
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSignRSARIPEMD160_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/***********************************************************************************************************************
 *  esl_finalizeHashSignRSARIPEMD160_PSS
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[out]    messageDigest  Message digest (hash) of previously hashed data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashSignRSARIPEMD160_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

/***********************************************************************************************************************
 *  esl_finalizeSigSignRSARIPEMD160_PSS
 **********************************************************************************************************************/
/*! \brief         Generate signature
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     saltSize  Size of salt to be used
 *  \param[in]     salt  Salt to be used
 *                       0 (NULL) if salt should be generated randomly
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSigSignRSARIPEMD160_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length saltSize, VSECPRIM_P2CONST_PARA(eslt_Byte) salt,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSigSignRSARIPEMD160_PSS_NoSalt
 **********************************************************************************************************************/
/*! \brief         Generate signature
 *  \details       Use no salt for signature generation
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSigSignRSARIPEMD160_PSS_NoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSigSignRSARIPEMD160_PSS_DigestLengthSalt
 **********************************************************************************************************************/
/*! \brief         Generate signature
 *  \details       Use random salt with size equal to message digest for signature generation
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSigSignRSARIPEMD160_PSS_DigestLengthSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSignRSARIPEMD160_PSS
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and generate signature
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     saltSize  Size of salt to be used
 *  \param[in]     salt  Salt to be used
 *                       0 (NULL) if salt should be generated randomly
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSARIPEMD160_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   eslt_Length saltSize, VSECPRIM_P2CONST_PARA(eslt_Byte) salt,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSignRSARIPEMD160_PSS_NoSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and generate signature
 *  \details       Use no salt for signature generation
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSARIPEMD160_PSS_NoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSignRSARIPEMD160_PSS_DigestLengthSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and generate signature
 *  \details       Use random salt with size equal to message digest for signature generation
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSARIPEMD160_PSS_DigestLengthSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_initVerifyRSARIPEMD160_PSS
 **********************************************************************************************************************/
/*! \brief         Initialize signature verification
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     keyPairModuleSize  Size of RSA key pair module
 *  \param[in]     keyPairModule  RSA key pair module
 *  \param[in]     publicKeyExponentSize  Size of public RSA key pair exponent
 *  \param[in]     publicKeyExponent  Public RSA key pair exponent
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_PUBKEY_INVALID  The public exponent is invalid
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initVerifyRSARIPEMD160_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length publicKeyExponentSize, eslt_pRomByte publicKeyExponent);

/***********************************************************************************************************************
 *  esl_updateVerifyRSARIPEMD160_PSS
 **********************************************************************************************************************/
/*! \brief         Update message digest (hash) with signed data
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     inputSize  Size of data to be hashed
 *  \param[in]     input  Data to be hashed
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RIPEMD160_TOTAL_LENGTH_OVERFLOW  Maximum total input length exceeded
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateVerifyRSARIPEMD160_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/***********************************************************************************************************************
 *  esl_finalizeHashVerifyRSARIPEMD160_PSS
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[out]    messageDigest  Message digest (hash) of previously hashed data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashVerifyRSARIPEMD160_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

/***********************************************************************************************************************
 *  esl_verifySigVerifyRSARIPEMD160_PSS
 **********************************************************************************************************************/
/*! \brief         Verify signature
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     saltSize  Expected size of salt,
 *                           -1 if size of salt used during signature generation should be evaluated automatically
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSARIPEMD160_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length saltSize,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_verifySigVerifyRSARIPEMD160_PSS_AutoSalt
 **********************************************************************************************************************/
/*! \brief         Verify signature
 *  \details       Size of salt used during signature generation is evaluated automatically
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSARIPEMD160_PSS_AutoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_verifySigVerifyRSARIPEMD160_PSS_NoSalt
 **********************************************************************************************************************/
/*! \brief         Verify signature
 *  \details       Requires that no salt was used during signature generation
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSARIPEMD160_PSS_NoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_verifySigVerifyRSARIPEMD160_PSS_DigestLengthSalt
 **********************************************************************************************************************/
/*! \brief         Verify signature
 *  \details       Requires that salt with size equal to message digest was used during signature generation
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSARIPEMD160_PSS_DigestLengthSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeVerifyRSARIPEMD160_PSS
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and verify signature
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     saltSize  Expected size of salt,
 *                           -1 if size of salt used during signature generation should be evaluated automatically
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSARIPEMD160_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length saltSize, eslt_Length signatureSize,
   VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeVerifyRSARIPEMD160_PSS_AutoSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and verify signature
 *  \details       Size of salt used during signature generation is evaluated automatically
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSARIPEMD160_PSS_AutoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeVerifyRSARIPEMD160_PSS_NoSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and verify signature
 *  \details       Requires that no salt was used during signature generation
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSARIPEMD160_PSS_NoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeVerifyRSARIPEMD160_PSS_DigestLengthSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and verify signature
 *  \details       Requires that salt with size equal to message digest was used during signature generation
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSARIPEMD160_PSS_DigestLengthSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_calcSaltedHashRSARIPEMD160_PSS
 **********************************************************************************************************************/
/*! \brief      Calculate salted hash value (RIPEMD-160)
 *  \details    Based on a given hash and a random salt the hash of
 *                M' = (0x) 00 00 00 00 00 00 00 00 || hash || salt
 *              is calculated, as described in chapter 9.1.1, steps 5-6 and 9.1.2, steps 12-13 of PKCS#1 2.2
 *  \param[in,out] wsHash  Pointer to hash workspace
 *  \param[in]  saltSize  Length of salt
 *  \param[in]  salt  Input salt
 *  \param[in]  messageDigest  Input hash (RIPEMD-160)
 *  \param[out] saltedHash  Output buffer for calculated hash (RIPEMD-160), has to be large enough to hold requested length
 *  \return     ESL_ERC_NO_ERROR  Operation successful
 *              ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *              ESL_ERC_WS_STATE_INVALID  Invalid state
 *              ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_calcSaltedHashRSARIPEMD160_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160) wsHash,
   eslt_Length saltSize, VSECPRIM_P2CONST_PARA(eslt_Byte) salt,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   VSECPRIM_P2VAR_PARA(eslt_Byte) saltedHash);
# endif /* (VSECPRIM_RSA_PSS_RIPEMD160_ENABLED == STD_ON) */

# if (VSECPRIM_RSA_PSS_SHA2_256_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto functions RSA (sign/verify) with SHA-256
 * according to PKCS#1 PSS
 *****************************************************************************/

/***********************************************************************************************************************
 *  esl_initSignRSASHA256_PSS
 **********************************************************************************************************************/
/*! \brief         Initialize signature generation
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     keyPairModuleSize  Size of RSA key pair module
 *  \param[in]     keyPairModule  RSA key pair module
 *  \param[in]     privateKeyExponentSize  Size of private RSA key pair exponent
 *  \param[in]     privateKeyExponent  Private RSA key pair exponent
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_PRIVKEY_INVALID  The private exponent is invalid
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignRSASHA256_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent);

/***********************************************************************************************************************
 *  esl_updateSignRSASHA256_PSS
 **********************************************************************************************************************/
/*! \brief         Update message digest (hash) with data to be signed
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     inputSize  Size of data to be hashed
 *  \param[in]     input  Data to be hashed
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_SHA256_TOTAL_LENGTH_OVERFLOW  Maximum total input length exceeded
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSignRSASHA256_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/***********************************************************************************************************************
 *  esl_finalizeHashSignRSASHA256_PSS
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[out]    messageDigest  Message digest (hash) of previously hashed data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashSignRSASHA256_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

/***********************************************************************************************************************
 *  esl_finalizeSigSignRSASHA256_PSS
 **********************************************************************************************************************/
/*! \brief         Generate signature
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     saltSize  Size of salt to be used
 *  \param[in]     salt  Salt to be used
 *                       0 (NULL) if salt should be generated randomly
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSigSignRSASHA256_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length saltSize, VSECPRIM_P2CONST_PARA(eslt_Byte) salt,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSigSignRSASHA256_PSS_NoSalt
 **********************************************************************************************************************/
/*! \brief         Generate signature
 *  \details       Use no salt for signature generation
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSigSignRSASHA256_PSS_NoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSigSignRSASHA256_PSS_DigestLengthSalt
 **********************************************************************************************************************/
/*! \brief         Generate signature
 *  \details       Use random salt with size equal to message digest for signature generation
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSigSignRSASHA256_PSS_DigestLengthSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSignRSASHA256_PSS
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and generate signature
 *  \details       -
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in]     saltSize  Size of salt to be used
 *  \param[in]     salt  Salt to be used
 *                       0 (NULL) if salt should be generated randomly
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSASHA256_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   eslt_Length saltSize, VSECPRIM_P2CONST_PARA(eslt_Byte) salt,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSignRSASHA256_PSS_NoSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and generate signature
 *  \details       Use no salt for signature generation
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSASHA256_PSS_NoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeSignRSASHA256_PSS_DigestLengthSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and generate signature
 *  \details       Use random salt with size equal to message digest for signature generation
 *  \param[in,out] workSpace  Signature generation work space
 *  \param[in,out] signatureSize  Input  Maximum size of signature
 *                                Output  Actual size of signature
 *  \param[out]    signature  Generated signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL or out of range
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSASHA256_PSS_DigestLengthSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize,
   VSECPRIM_P2VAR_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_initVerifyRSASHA256_PSS
 **********************************************************************************************************************/
/*! \brief         Initialize signature verification
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     keyPairModuleSize  Size of RSA key pair module
 *  \param[in]     keyPairModule  RSA key pair module
 *  \param[in]     publicKeyExponentSize  Size of public RSA key pair exponent
 *  \param[in]     publicKeyExponent  Public RSA key pair exponent
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_RSA_PUBKEY_INVALID  The public exponent is invalid
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initVerifyRSASHA256_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule,
   eslt_Length publicKeyExponentSize, eslt_pRomByte publicKeyExponent);

/***********************************************************************************************************************
 *  esl_updateVerifyRSASHA256_PSS
 **********************************************************************************************************************/
/*! \brief         Update message digest (hash) with signed data
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     inputSize  Size of data to be hashed
 *  \param[in]     input  Data to be hashed
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *                 ESL_ERC_SHA256_TOTAL_LENGTH_OVERFLOW  Maximum total input length exceeded
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateVerifyRSASHA256_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input);

/***********************************************************************************************************************
 *  esl_finalizeHashVerifyRSASHA256_PSS
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[out]    messageDigest  Message digest (hash) of previously hashed data
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashVerifyRSASHA256_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest);

/***********************************************************************************************************************
 *  esl_verifySigVerifyRSASHA256_PSS
 **********************************************************************************************************************/
/*! \brief         Verify signature
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     saltSize  Expected size of salt,
 *                           -1 if size of salt used during signature generation should be evaluated automatically
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSASHA256_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length saltSize,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_verifySigVerifyRSASHA256_PSS_AutoSalt
 **********************************************************************************************************************/
/*! \brief         Verify signature
 *  \details       Size of salt used during signature generation is evaluated automatically
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSASHA256_PSS_AutoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_verifySigVerifyRSASHA256_PSS_NoSalt
 **********************************************************************************************************************/
/*! \brief         Verify signature
 *  \details       Requires that no salt was used during signature generation
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSASHA256_PSS_NoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_verifySigVerifyRSASHA256_PSS_DigestLengthSalt
 **********************************************************************************************************************/
/*! \brief         Verify signature
 *  \details       Requires that salt with size equal to message digest was used during signature generation
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     messageDigest  Message digest (hash) of previously hashed data
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifySigVerifyRSASHA256_PSS_DigestLengthSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeVerifyRSASHA256_PSS
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and verify signature
 *  \details       -
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     saltSize  Expected size of salt,
 *                           -1 if size of salt used during signature generation should be evaluated automatically
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSASHA256_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length saltSize, eslt_Length signatureSize,
   VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeVerifyRSASHA256_PSS_AutoSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and verify signature
 *  \details       Size of salt used during signature generation is evaluated automatically
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSASHA256_PSS_AutoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeVerifyRSASHA256_PSS_NoSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and verify signature
 *  \details       Requires that no salt was used during signature generation
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSASHA256_PSS_NoSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_finalizeVerifyRSASHA256_PSS_DigestLengthSalt
 **********************************************************************************************************************/
/*! \brief         Finalize message digest (hash) calculation and verify signature
 *  \details       Requires that salt with size equal to message digest was used during signature generation
 *  \param[in,out] workSpace  Signature verification work space
 *  \param[in]     signatureSize  Size of signature
 *  \param[in]     signature  Signature value
 *  \return        ESL_ERC_NO_ERROR  Operation successful
 *                 ESL_ERC_RSA_SIGNATURE_INVALID  Signature verification failed
 *                 ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *                 ESL_ERC_WS_STATE_INVALID  Invalid state
 *                 ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeVerifyRSASHA256_PSS_DigestLengthSalt(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSver) workSpace,
   eslt_Length signatureSize, VSECPRIM_P2CONST_PARA(eslt_Byte) signature);

/***********************************************************************************************************************
 *  esl_calcSaltedHashRSASHA256_PSS
 **********************************************************************************************************************/
/*! \brief      Calculate salted hash value (SHA-256)
 *  \details    Based on a given hash and a random salt the hash of
 *                M' = (0x) 00 00 00 00 00 00 00 00 || hash || salt
 *              is calculated, as described in chapter 9.1.1, steps 5-6 and 9.1.2, steps 12-13 of PKCS#1 2.2
 *  \param[in,out] wsHash  Pointer to hash workspace
 *  \param[in]  saltSize  Length of salt
 *  \param[in]  salt  Input salt
 *  \param[in]  messageDigest  Input hash (SHA-256)
 *  \param[out] saltedHash  Output buffer for calculated hash (SHA-256), has to be large enough to hold requested length
 *  \return     ESL_ERC_NO_ERROR  Operation successful
 *              ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *              ESL_ERC_WS_STATE_INVALID  Invalid state
 *              ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_calcSaltedHashRSASHA256_PSS(
   VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA256) wsHash,
   eslt_Length saltSize, VSECPRIM_P2CONST_PARA(eslt_Byte) salt,
   VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
   VSECPRIM_P2VAR_PARA(eslt_Byte) saltedHash);
# endif /* (VSECPRIM_RSA_PSS_SHA2_256_ENABLED == STD_ON) */

/*****************************************************************************
 * Crypto function to generate MGF1 masks with different hash algorithms
 * according to PKCS#1 PSS
 *****************************************************************************/

# if (VSECPRIM_RSA_HASHALGORITHM_RIPEMD160_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/***********************************************************************************************************************
 *  esl_generateMaskMGF1RSARIPEMD160_PSS
 **********************************************************************************************************************/
/*! \brief      Mask generation function (MGF1)
 *  \details    MGF1 implementation as described in appendix B.2.1 of PKCS#1 2.2
 *              Based on hash function (RIPEMD-160) this function generates a pseudorandom output string of arbitrary length
 *              depending on an input string (seed) of variable length
 *  \param[in,out] wsHash  Pointer to hash workspace
 *  \param[in,out] tempHash  Temporary buffer for intermediate hash, has to be large enough to hold complete hash value
 *  \param[in]  seedLength  Length input seed
 *  \param[in]  seed  Input seed from which mask is generated
 *  \param[in]  maskLength  Length of mask to be generated
 *  \param[out] mask  Output buffer for generated mask, has to be large enough to hold requested length
 *  \return     ESL_ERC_NO_ERROR  Operation successful
 *              ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *              ESL_ERC_WS_STATE_INVALID  Invalid state
 *              ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateMaskMGF1RSARIPEMD160_PSS(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRIPEMD160) wsHash,
  VSECPRIM_P2VAR_PARA(eslt_Byte) tempHash,
  eslt_Length seedLength, VSECPRIM_P2CONST_PARA(eslt_Byte) seed,
  eslt_Length maskLength, VSECPRIM_P2VAR_PARA(eslt_Byte) mask);
# endif /* (VSECPRIM_RSA_HASHALGORITHM_RIPEMD160_ENABLED == STD_ON) */

# if (VSECPRIM_RSA_HASHALGORITHM_SHA1_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/***********************************************************************************************************************
 *  esl_generateMaskMGF1RSASHA1_PSS
 **********************************************************************************************************************/
/*! \brief      Mask generation function (MGF1)
 *  \details    MGF1 implementation as described in appendix B.2.1 of PKCS#1 2.2
 *              Based on hash function (SHA-1) this function generates a pseudorandom output string of arbitrary length
 *              depending on an input string (seed) of variable length
 *  \param[in,out] wsHash  Pointer to hash workspace
 *  \param[in,out] tempHash  Temporary buffer for intermediate hash, has to be large enough to hold complete hash value
 *  \param[in]  seedLength  Length input seed
 *  \param[in]  seed  Input seed from which mask is generated
 *  \param[in]  maskLength  Length of mask to be generated
 *  \param[out] mask  Output buffer for generated mask, has to be large enough to hold requested length
 *  \return     ESL_ERC_NO_ERROR  Operation successful
 *              ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *              ESL_ERC_WS_STATE_INVALID  Invalid state
 *              ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateMaskMGF1RSASHA1_PSS(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA1) wsHash,
  VSECPRIM_P2VAR_PARA(eslt_Byte) tempHash,
  eslt_Length seedLength, VSECPRIM_P2CONST_PARA(eslt_Byte) seed,
  eslt_Length maskLength, VSECPRIM_P2VAR_PARA(eslt_Byte) mask);
# endif /* (VSECPRIM_RSA_HASHALGORITHM_SHA1_ENABLED == STD_ON) */

# if (VSECPRIM_RSA_HASHALGORITHM_SHA2_256_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/***********************************************************************************************************************
 *  esl_generateMaskMGF1RSASHA256_PSS
 **********************************************************************************************************************/
/*! \brief     Mask generation function (MGF1)
 *  \details    MGF1 implementation as described in appendix B.2.1 of PKCS#1 2.2
 *              Based on hash function (SHA-256) this function generates a pseudorandom output string of arbitrary length
 *              depending on an input string (seed) of variable length
 *  \param[in,out] wsHash  Pointer to hash workspace
 *  \param[in,out] tempHash  Temporary buffer for intermediate hash, has to be large enough to hold complete hash value
 *  \param[in]  seedLength  Length input seed
 *  \param[in]  seed  Input seed from which mask is generated
 *  \param[in]  maskLength  Length of mask to be generated
 *  \param[out] mask  Output buffer for generated mask, has to be large enough to hold requested length
 *  \return     ESL_ERC_NO_ERROR  Operation successful
 *              ESL_ERC_PARAMETER_INVALID  Input parameter is NULL
 *              ESL_ERC_WS_STATE_INVALID  Invalid state
 *              ESL_ERC_WS_TOO_SMALL  Work space too small
 *  \pre           Work space initialized
 *  \context       TASK
 *  \reentrant     TRUE, for different workspaces
 *  \synchronous   TRUE
 **********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateMaskMGF1RSASHA256_PSS(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceSHA256) wsHash,
  VSECPRIM_P2VAR_PARA(eslt_Byte) tempHash,
  eslt_Length seedLength, VSECPRIM_P2CONST_PARA(eslt_Byte) seed,
  eslt_Length maskLength, VSECPRIM_P2VAR_PARA(eslt_Byte) mask);
# endif /* (VSECPRIM_RSA_HASHALGORITHM_SHA2_256_ENABLED == STD_ON) */

# if (VSECPRIM_RSA_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto function to verify Padding RSA EM
 * according to PKCS#1 1.5
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_verifyPaddingRSAEM_V15()
 *********************************************************************************************************************/
/*! \brief          Verify padding for RSA PKCS#1 V1.5
 *  \details        -
 *  \param[in,out]  paddingLength  (in) Length of message
 *                               (out) Length of padding
 *  \param[in]      message      Decrypted message
 *  \param[in]      blockType    Block type according to PKCS #1 1.5
 *                               ASN1_PADDING_BLOCK_TYPE_PRIVATE_ZERO
 *                               ASN1_PADDING_BLOCK_TYPE_PRIVATE
 *                               ASN1_PADDING_BLOCK_TYPE_PUBLIC
 *  \return         ESL_ERC_PARAMETER_INVALID     Message length smaller than minimum padding
 *                  ESL_ERC_RSA_ENCODING_INVALID  Invalid padding
 *                  ESL_ERC_NO_ERROR              else
 *  \pre            Work space initialized
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifyPaddingRSAEM_V15(
  VSECPRIM_P2CONST_PARA(eslt_Byte) message, VSECPRIM_P2VAR_PARA(eslt_Length) paddingLength, eslt_Byte blockType);
# endif /* (VSECPRIM_RSA_ENABLED == STD_ON) */
/*****************************************************************************
 * Crypto functions EdDSA & ECDH
 *****************************************************************************/

# if (VSECPRIM_ECDSA_25519_ENABLED == STD_ON)
/**********************************************************************************************************************
 *  esl_initSignEdDSA()
 *********************************************************************************************************************/
/*! \brief       Init EdDSA.
 *  \details     This function initializes the EdDSA signature.
 *  \param[in,out]  workSpace  algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *  \param[in]   curve  underlying curve; supports
 *                      - ESL_Curve25519
 *  \param[in]   instance  EdDSA instance to use; one of
 *                         - ESL_INSTANCE_Ed25519
 *                         - ESL_INSTANCE_Ed25519ctx
 *                         - ESL_INSTANCE_Ed25519ph
 *  \param[in]   context  Context data pointer
 *  \param[in]   contextLength The length of the context data
 *  \return      ESL_ERC_NO_ERROR - success
 *               ESL_ERC_PARAMETER_INVALID      - some parameter is NULL
 *               ESL_ERC_CURVE_NOT_SUPPORTED    - unknown curve
 *               ESL_ERC_INSTANCE_NOT_SUPPORTED - unknown EdDSA instance
 *               ESL_ERC_WS_TOO_SMALL           - work space too small
 *  \pre         workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context     TASK
 *  \reentrant   TRUE, for different workspaces
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignEdDSA(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEd25519) workSpace,
  eslt_Size16 curve,
  const eslt_Byte instance,
  VSECPRIM_P2CONST_PARA(eslt_Byte) context,
  const eslt_Length contextLength);

/**********************************************************************************************************************
 *  esl_signEdDSA()
 *********************************************************************************************************************/
/*! \brief       Sign data EdDSA.
 *  \details     This function signs the given input data.
 *  \param[in,out]  workSpace  algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *  \param[in]   input  Pointer to the input data
 *  \param[in]   inputLength  The length of the provided input data
 *  \param[in]   secretKey  Pointer to the secret key of length 32 byte to use for signature
 *  \param[in]   publicKey  Pointer to the public key of length 32 byte. Pass null pointer to calculate from private key.
 *  \param[out]   signature  Pointer to where the signature shall be stored.
 *  \param[in,out]   signatureLength  Pointer to where the size of the buffer is provided
 *  \return      ESL_ERC_NO_ERROR - success
 *               ESL_ERC_PARAMETER_INVALID     - some parameter is NULL
 *               ESL_ERC_WS_STATE_INVALID      - work space state invalid
 *               ESL_ERC_WS_TOO_SMALL          - work space too small
 *               ESL_ERC_BUFFER_TOO_SMALL      - buffer for signature too small
 *               ESL_ERC_TOTAL_LENGTH_OVERFLOW - internal overflow (== message too long - SHA-512)
 *  \pre         workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context     TASK
 *  \reentrant   TRUE, for different workspaces
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_signEdDSA(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEd25519) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) input,
  eslt_Length inputLength,
  VSECPRIM_P2CONST_PARA(eslt_Byte) secretKey,
  VSECPRIM_P2CONST_PARA(eslt_Byte) publicKey,
  VSECPRIM_P2VAR_PARA(eslt_Byte) signature,
  VSECPRIM_P2VAR_PARA(eslt_Length) signatureLength);

/**********************************************************************************************************************
 *  esl_initVerifyEdDSA()
 *********************************************************************************************************************/
/*! \brief       Init verification for EdDSA.
 *  \details     This function initializes the EdDSA verification.
 *  \param[in,out]  workSpace  algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *  \param[in]   curve  Underlying curve; supports
 *                      - ESL_Curve25519
 *  \param[in]   instance  EdDSA instance to use; one of
 *                         - ESL_INSTANCE_Ed25519
 *                         - ESL_INSTANCE_Ed25519ctx
 *                         - ESL_INSTANCE_Ed25519ph
 *  \param[in]   context  Pointer to the context data
 *  \param[in]   contextLength  Size of provided context data
 *  \return      ESL_ERC_NO_ERROR - success
 *               ESL_ERC_PARAMETER_INVALID      - some parameter is NULL
 *               ESL_ERC_CURVE_NOT_SUPPORTED    - unknown curve
 *               ESL_ERC_INSTANCE_NOT_SUPPORTED - unknown EdDSA instance
 *               ESL_ERC_WS_TOO_SMALL           - work space too small
 *  \pre         workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context     TASK
 *  \reentrant   TRUE, for different workspaces
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initVerifyEdDSA(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEd25519) workSpace,
  eslt_Size16 curve,
  const eslt_Byte instance,
  VSECPRIM_P2CONST_PARA(eslt_Byte) context,
  const eslt_Length contextLength);

/**********************************************************************************************************************
 *  esl_verifyEdDSA()
 *********************************************************************************************************************/
/*! \brief       EdDSA verification.
 *  \details     This function verifies given signature against the given data.
 *  \param[in,out]  workSpace  algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *  \param[in]   input  Pointer to the input data.
 *  \param[in]   inputLength  The length of the input data
 *  \param[in]   publicKey  Pointer to the public key
 *  \param[in]   signature  Pointer to the signature to be validated against input data
 *  \param[in]   signatureLength  Length of the signature
 *  \return      ESL_ERC_NO_ERROR - success
 *               ESL_ERC_PARAMETER_INVALID     - some parameter is NULL
 *               ESL_ERC_WS_STATE_INVALID      - work space state invalid
 *               ESL_ERC_WS_TOO_SMALL          - work space too small
 *               ESL_ERC_TOTAL_LENGTH_OVERFLOW - internal overflow (== message too long - SHA-512)
 *               ESL_ERC_SIGNATURE_INVALID     - the signature does NOT verify
 *  \pre         workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context     TASK
 *  \reentrant   TRUE, for different workspaces
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifyEdDSA(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEd25519) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) input,
  eslt_Length inputLength,
  VSECPRIM_P2CONST_PARA(eslt_Byte) publicKey,
  VSECPRIM_P2CONST_PARA(eslt_Byte) signature,
  eslt_Length signatureLength);

/**********************************************************************************************************************
 *  esl_updateEdDSA()
 *********************************************************************************************************************/
/*! \brief       Perform hashing
 *  \details     This function updates the internal hash value in a pre-hash EdDSA signature OR verification.
 *  \param[in,out]  workSpace  algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *  \param[in]   input  Pointer to the input data.
 *  \param[in]   inputLength  The length of the input data
 *  \return      ESL_ERC_NO_ERROR - success
 *               ESL_ERC_PARAMETER_INVALID     - some parameter is NULL
 *               ESL_ERC_WS_STATE_INVALID      - work space state invalid
 *               ESL_ERC_WS_TOO_SMALL          - work space too small
 *               ESL_ERC_TOTAL_LENGTH_OVERFLOW - internal overflow (== message too long - SHA-512)
 *  \pre         workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context     TASK
 *  \reentrant   TRUE, for different workspaces
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateEdDSA(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEd25519) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) input,
  eslt_Length inputLength);

/**********************************************************************************************************************
 *  esl_initGenerateKeyPairEdDSA()
 *********************************************************************************************************************/
/*! \brief       Initialize generated key pair
 *  \details     This function initializes the EdDSA key-pair generation.
 *  \param[in,out]  workSpace  algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *  \param[in]   curve  Underlying curve; supports
 *                      - ESL_Curve25519
 *  \return      ESL_ERC_NO_ERROR - success
 *               ESL_ERC_PARAMETER_INVALID   - some parameter is NULL
 *               ESL_ERC_CURVE_NOT_SUPPORTED - unknown curve
 *               ESL_ERC_WS_TOO_SMALL        - work space too small
 *  \pre         workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context     TASK
 *  \reentrant   TRUE, for different workspaces
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initGenerateKeyPairEdDSA(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEd25519) workSpace,
  eslt_Size16 curve);

/**********************************************************************************************************************
 *  esl_generateKeyPairEdDSA()
 *********************************************************************************************************************/
/*! \brief       Generate Key Pair
 *  \details     This function generates an EdDSA key-pair.
 *  \param[in,out]  workSpace  algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *  \param[out]  secretKey  Pointer to where the secret key shall be stored
 *  \param[in,out]  secretKeyLength  Length of the provided buffer/Length of the written data
 *  \param[out]  publicKey  Pointer to where the public key shall be stored
 *  \param[in,out]  publicKeyLength  Length of the provided buffer/Length of the written data
 *  \return      ESL_ERC_NO_ERROR - success
 *               ESL_ERC_PARAMETER_INVALID - some parameter is NULL
 *               ESL_ERC_WS_STATE_INVALID  - work space state invalid
 *               ESL_ERC_WS_TOO_SMALL      - work space too small
 *               ESL_ERC_BUFFER_TOO_SMALL  - buffer for private or public key too small
 *               ESL_ERC_RNG_FAILED        - the random number generator failed
 *  \pre         workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context     TASK
 *  \reentrant   TRUE, for different workspaces
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateKeyPairEdDSA(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEd25519) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Byte) secretKey,
  VSECPRIM_P2VAR_PARA(eslt_Length) secretKeyLength,
  VSECPRIM_P2VAR_PARA(eslt_Byte) publicKey,
  VSECPRIM_P2VAR_PARA(eslt_Length) publicKeyLength);

# endif /* (VSECPRIM_ECDSA_25519_ENABLED == STD_ON) */

# if (VSECPRIM_ECDH_25519_ENABLED == STD_ON)
/**********************************************************************************************************************
 *  esl_initECDH()
 *********************************************************************************************************************/
/*! \brief       Init ECDH
 *  \details     This function initializes the EC-D/H key-exchange.
 *  \param[in,out]  workSpace  algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *  \param[in]     curve  underlying curve; supports
 *                      - ESL_Curve25519
 *  \return      ESL_ERC_NO_ERROR - success
 *               ESL_ERC_PARAMETER_INVALID - some parameter is NULL
 *               ESL_ERC_CURVE_NOT_SUPPORTED - unknown curve
 *               ESL_ERC_WS_TOO_SMALL - work space too small
 *  \pre         workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context     TASK
 *  \reentrant   TRUE, for different workspaces
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initECDH(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceX25519) workSpace,
  eslt_Size16 curve);

/**********************************************************************************************************************
 *  esl_generateEphemeralKeyPairECDH()
 *********************************************************************************************************************/
/*! \brief       Generate Ephemeral Key Pair.
 *  \details     This function generates an ephemeral key-pair for EC-D/H.
 *  \param[in,out]  workSpace  Algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *  \param[in,out]  publicKey  Pointer to public key memory
 *  \param[in,out]  publicKeyLength  Pointer to public key memory size
 *  \return      ESL_ERC_NO_ERROR - success
 *               ESL_ERC_PARAMETER_INVALID - some parameter is NULL
 *               ESL_ERC_WS_STATE_INVALID  - work space state invalid
 *               ESL_ERC_WS_TOO_SMALL      - work space too small
 *               ESL_ERC_BUFFER_TOO_SMALL  - buffer for private or public key too small
 *               ESL_ERC_RNG_FAILED        - the random number generator failed
 *  \pre         workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context     TASK
 *  \reentrant   TRUE, for different workspaces
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateEphemeralKeyPairECDH(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceX25519) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Byte) publicKey,
  VSECPRIM_P2VAR_PARA(eslt_Length) publicKeyLength);

/**********************************************************************************************************************
 *  esl_importStaticPrivateKeyECDH()
 *********************************************************************************************************************/
/*! \brief       Import private key
 *  \details     This function imports a static private key for EC-D/H.
 *  \param[in,out]  workSpace  Algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *  \param[in]   privateKey  Pointer to private key
 *  \return      ESL_ERC_NO_ERROR - success
 *               ESL_ERC_PARAMETER_INVALID - some parameter is NULL
 *               ESL_ERC_WS_STATE_INVALID  - work space state invalid
 *               ESL_ERC_WS_TOO_SMALL      - work space too small
 *               ESL_ERC_BUFFER_TOO_SMALL  - buffer for private or public key too small
 *               ESL_ERC_RNG_FAILED        - the random number generator failed
 *  \pre         workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context     TASK
 *  \reentrant   TRUE, for different workspaces
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_importStaticPrivateKeyECDH(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceX25519) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) privateKey);

/**********************************************************************************************************************
 *  esl_generateSharedSecretECDH()
 *********************************************************************************************************************/
/*! \brief       Generate shared secret
 *  \details     This function generates the shared secret EC-D/H.
 *  \param[in,out]  workSpace  Algorithm context buffer, initialized by esl_initWorkSpaceHeader(..) function
 *  \param[in,out]  publicKey  Pointer to public key
 *  \param[out]  sharedSecret  Pointer to shared secret memory
 *  \param[in,out]  sharedSecretLength  Pointer to shared secret memory size
 *  \return      ESL_ERC_NO_ERROR - success
 *               ESL_ERC_PARAMETER_INVALID - some parameter is NULL
 *               ESL_ERC_WS_STATE_INVALID  - work space state invalid
 *               ESL_ERC_WS_TOO_SMALL      - work space too small
 *               ESL_ERC_BUFFER_TOO_SMALL  - buffer for shared secret too small
 *  \pre         workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context     TASK
 *  \reentrant   TRUE, for different workspaces
 *  \synchronous TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateSharedSecretECDH(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceX25519) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Byte) publicKey,
  VSECPRIM_P2VAR_PARA(eslt_Byte) sharedSecret,
  VSECPRIM_P2VAR_PARA(eslt_Length) sharedSecretLength);
# endif /* (VSECPRIM_ECDH_25519_ENABLED == STD_ON) */

# if (VSECPRIM_CHA_CHA20_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto functions ChaCha20
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initEncryptChaCha20()
 *********************************************************************************************************************/
/*! \brief          This function initializes the ChaCha20 stream encryption.
 *  \details        -
 *  \param[in]      key          pointer to key of length ESL_SIZEOF_ChaCha20_KEY
 *  \param[in]      nonce        pointer to nonce of length ESL_SIZEOF_ChaCha20_NONCE
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader () function
 *                               (out) initialized algorithm context structure
 *  \param[in]      blockCount   initial block count
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptChaCha20(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceChaCha20) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) key,
  VSECPRIM_P2CONST_PARA(eslt_Byte) nonce,
  const eslt_Size32 blockCount);

/**********************************************************************************************************************
 *  esl_initDecryptChaCha20()
 *********************************************************************************************************************/
/*! \brief          This function initializes the ChaCha20 stream DEcryption.
 *  \details        -
 *  \param[in]      key          pointer to key of length ESL_SIZEOF_ChaCha20_KEY
 *  \param[in]      nonce        pointer to nonce of length ESL_SIZEOF_ChaCha20_NONCE
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader () function
 *                               (out) initialized algorithm context structure
 *  \param[in]      blockCount   initial block count
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptChaCha20(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceChaCha20) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) key,
  VSECPRIM_P2CONST_PARA(eslt_Byte) nonce,
  const eslt_Size32 blockCount);

/**********************************************************************************************************************
 *  esl_encryptChaCha20()
 *********************************************************************************************************************/
/*! \brief          This function encrypts input data of any length and can be called
 *                  arbitrarily often after the algorithm initialization.
 *  \details        -
 *  \param[in]      in           pointer to input data
 *  \param[out]     outSize      length of encrypted data
 *  \param[in,out]  out          pointer to output buffer
 *  \param[in]      inSize       length of input data (bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initChaCha20 () function
 *                               (out) updated algorithm context buffer
 *  \param[in,out]  outSize      pointer to size of output buffer (bytes)
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_BUFFER_TOO_SMALL  output buffer too small
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by initChaCha20 ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_encryptChaCha20(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceChaCha20) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) in,
  const eslt_Length inSize,
  VSECPRIM_P2VAR_PARA(eslt_Byte) out,
  VSECPRIM_P2VAR_PARA(eslt_Length) outSize);

/**********************************************************************************************************************
*  esl_finalizeEncryptChaCha20()
*********************************************************************************************************************/
/*! \brief          This function encrypts the input data of any length and resets and clears the workspace
 *  \details        -
 *  \param[in]      in           pointer to input data
 *  \param[out]     outSize      length of encrypted data
 *  \param[in,out]  out          pointer to output buffer
 *  \param[in]      inSize       length of input data (bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initChaCha20 () function
 *                               (out) updated algorithm context buffer
 *  \param[in,out]  outSize      pointer to size of output buffer (bytes)
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_BUFFER_TOO_SMALL  output buffer too small
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by initChaCha20 ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeEncryptChaCha20(
    VSECPRIM_P2VAR_PARA(eslt_WorkSpaceChaCha20) workSpace,
    VSECPRIM_P2CONST_PARA(eslt_Byte) in,
    const eslt_Length inSize,
    VSECPRIM_P2VAR_PARA(eslt_Byte) out,
    VSECPRIM_P2VAR_PARA(eslt_Length) outSize);

/**********************************************************************************************************************
 *  esl_decryptChaCha20()
 *********************************************************************************************************************/
/*! \brief          This function DEcrypts input data of any length and can be called
 *                  arbitrarily often after the algorithm initialization.
 *  \details        -
 *  \param[in]      in           pointer to input data
 *  \param[out]     outSize      length of encrypted data
 *  \param[in,out]  out          pointer to output buffer
 *  \param[in]      inSize       length of input data (bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initChaCha20 () function
 *                               (out) updated algorithm context buffer
 *  \param[in,out]  outSize      size of output buffer (bytes)
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_BUFFER_TOO_SMALL  output buffer too small
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by initChaCha20 ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_decryptChaCha20(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceChaCha20) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) in,
  const eslt_Length inSize,
  VSECPRIM_P2VAR_PARA(eslt_Byte) out,
  VSECPRIM_P2VAR_PARA(eslt_Length) outSize);

/**********************************************************************************************************************
*  esl_finalizeDecryptChaCha20()
*********************************************************************************************************************/
/*! \brief          This function decrypts the input data of any length and resets and clears the workspace
 *  \details        -
 *  \param[in]      in           pointer to input data
 *  \param[out]     outSize      length of encrypted data
 *  \param[in,out]  out          pointer to output buffer
 *  \param[in]      inSize       length of input data (bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initChaCha20 () function
 *                               (out) updated algorithm context buffer
 *  \param[in,out]  outSize      size of output buffer (bytes)
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_BUFFER_TOO_SMALL  output buffer too small
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by initChaCha20 ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeDecryptChaCha20(
    VSECPRIM_P2VAR_PARA(eslt_WorkSpaceChaCha20) workSpace,
    VSECPRIM_P2CONST_PARA(eslt_Byte) in,
    const eslt_Length inSize,
    VSECPRIM_P2VAR_PARA(eslt_Byte) out,
    VSECPRIM_P2VAR_PARA(eslt_Length) outSize);
# endif /* (VSECPRIM_CHA_CHA20_ENABLED == STD_ON) */

# if (VSECPRIM_POLY1305_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
      /*****************************************************************************
 * Crypto functions Poly1305
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initPoly1305()
 *********************************************************************************************************************/
/*! \brief          This function initializes the Poly1305 autenticator.
 *  \details        -
 *  \param[in]      key          pointer to key of length ESL_SIZEOF_Poly1305_KEY
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader () function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL      work space too small
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initPoly1305(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpacePoly1305) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) key);

/**********************************************************************************************************************
 *  esl_updatePoly1305()
 *********************************************************************************************************************/
/*! \brief          This function updates the Poly1305 authenticator.
 *  \details        -
 *  \param[in]      msgSize      length of message data (bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initPoly1305 () function
 *                               (out) updated algorithm context structure
 *  \param[in]      message      pointer to message data
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by initPoly1305 ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updatePoly1305(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpacePoly1305) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) message,
  const eslt_Length msgSize);

/**********************************************************************************************************************
 *  esl_finalizePoly1305()
 *********************************************************************************************************************/
/*! \brief          This function calculates and returns the Poly1305 TAG.
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initPoly1305 () function
 *                               (out) updated algorithm context buffer
 *  \param[in,out]  tag          (in) pointer to TAG (output) buffer;
 *                               size must be at least ESL_SIZEOF_Poly1305_TAG
 *                               (out) authentication TAG calculated
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID  work space state invalid
 *                  ESL_ERC_NO_ERROR          else
 *  \pre            workSpace is initialized by initPoly1305 ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizePoly1305(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpacePoly1305) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Byte) tag);

/**********************************************************************************************************************
 *  esl_verifyPoly1305()
 *********************************************************************************************************************/
/*! \brief          This function verifies a Poly1305 TAG.
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initPoly1305 () function
 *                               (out) updated algorithm context buffer
 *  \param[in]      tag          (in) pointer to TAG (input)
 *                               (out) authentication TAG calculated
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID work space state invalid
 *                  ESL_ERC_INCORRECT_TAG the given TAG is incorrect
 *                  ESL_ERC_NO_ERROR Poly1305 TAG is correct
 *  \pre            workSpace is initialized by initPoly1305 ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifyPoly1305(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpacePoly1305) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) tag);
# endif /* (VSECPRIM_POLY1305_ENABLED == STD_ON) */

# if (VSECPRIM_AEAD_CHA_CHA20_POLY1305_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Crypto functions AEAD_ChaCha20_Poly1305
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initEncryptChaChaPoly()
 *********************************************************************************************************************/
/*! \brief          This function initializes the AEAD_ChaCha20_Poly1305 (RFC 7359)
 *                  stream encryption and authentication TAG creation.
 *  \details        -
 *  \param[in]      key          pointer to key of length ESL_SIZEOF_ChaCha20_KEY
 *  \param[in]      nonce        pointer to nonce of length ESL_SIZEOF_ChaCha20_NONCE
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader () function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL work space too small
 *                  ESL_ERC_NO_ERROR else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initEncryptChaChaPoly(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceChaChaPoly) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) key,
  VSECPRIM_P2CONST_PARA(eslt_Byte) nonce);

/**********************************************************************************************************************
 *  esl_initDecryptChaChaPoly()
 *********************************************************************************************************************/
/*! \brief          This function initializes the AEAD_ChaCha20_Poly1305
 *                  stream decryption and authentication TAG verification.
 *  \details        -
 *  \param[in]      key          pointer to key of length ESL_SIZEOF_ChaCha20_KEY
 *  \param[in]      nonce        pointer to nonce of length ESL_SIZEOF_ChaCha20_NONCE
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader () function
 *                               (out) initialized algorithm context structure
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL work space too small
 *                  ESL_ERC_NO_ERROR else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initDecryptChaChaPoly(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceChaChaPoly) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) key,
  VSECPRIM_P2CONST_PARA(eslt_Byte) nonce);

/**********************************************************************************************************************
 *  esl_updateAADChaChaPoly()
 *********************************************************************************************************************/
/*! \brief          This function passes additional authenticated data (AAD)
 *                  to the AEAD_ChaCha20_Poly1305.
 *  \details        -
 *  \param[in]      aad          pointer to AAD
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initXXcryptChaChaPoly () function
 *                               (out) internal state updated
 *  \param[in]      aadSize      length of AAD (bytes)
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID work space state invalid
 *                  ESL_ERC_TOTAL_LENGTH_OVERFLOW length of AAD > 2^64 -1
 *                  ESL_ERC_NO_ERROR else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateAADChaChaPoly(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceChaChaPoly) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) aad,
  const eslt_Length aadSize);

/**********************************************************************************************************************
 *  esl_updateDataChaChaPoly()
 *********************************************************************************************************************/
/*! \brief          This function passes data to be encrypted / decrypted
 *                  to the AEAD_ChaCha20_Poly1305.
 *                  When the workSpace has been initialized for encryption,
 *                  "in" is the message and "out" is the cipher; in case of
 *                  decryption, it's the other way 'round.
 *  \details        -
 *  \param[in]      in           pointer to input data
 *  \param[out]     outSize      length of encrypted data
 *  \param[in,out]  out          pointer to output buffer
 *  \param[in]      inSize       length of input data (bytes)
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initXXcryptChaChaPoly () function
 *                               (out) internal state updated
 *  \param[in,out]  outSize      pointer to size of output buffer (bytes)
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_BUFFER_TOO_SMALL output buffer too small
 *                  ESL_ERC_WS_STATE_INVALID work space state invalid
 *                  ESL_ERC_TOTAL_LENGTH_OVERFLOW length of data > (2^32 -1) *64
 *                  ESL_ERC_NO_ERROR else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateDataChaChaPoly(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceChaChaPoly) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) in,
  const eslt_Length inSize,
  VSECPRIM_P2VAR_PARA(eslt_Byte) out,
  VSECPRIM_P2VAR_PARA(eslt_Length) outSize);

/**********************************************************************************************************************
 *  esl_finalizeChaChaPoly()
 *********************************************************************************************************************/
/*! \brief          This function calculates the authentication TAG of the
 *                  AEAD_ChaCha20_Poly1305.
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initEncryptChaChaPoly () function
 *                               (out) internal state updated
 *  \param[in,out]  tag          pointer to TAG buffer of size ESL_SIZEOF_Poly1305_TAG
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID work space state invalid
 *                  ESL_ERC_NO_ERROR else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeChaChaPoly(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceChaChaPoly) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Byte) tag);

/**********************************************************************************************************************
 *  esl_verifyChaChaPoly()
 *********************************************************************************************************************/
/*! \brief          This function verifies the authentication TAG of the
 *                  AEAD_ChaCha20_Poly1305.
 *  \details        -
 *  \param[in,out]  workSpace    (in) algorithm context buffer, initialized by
 *                               esl_initDecryptChaChaPoly () function
 *                               (out) internal state updated
 *  \param[in]      tag          pointer to TAG of length ESL_SIZEOF_Poly1305_TAG
 *  \return         ESL_ERC_PARAMETER_INVALID input parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID work space state invalid
 *                  ESL_ERC_INCORRECT_TAG the given TAG is incorrect
 *                  ESL_ERC_NO_ERROR else
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader ()
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_verifyChaChaPoly(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceChaChaPoly) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) tag);
# endif /* (VSECPRIM_AEAD_CHA_CHA20_POLY1305_ENABLED == STD_ON) */

# if (VSECPRIM_ECBD_GENERIC_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/*****************************************************************************
 * Burmester-Desmedt key agreement protocol
 *****************************************************************************/

/**********************************************************************************************************************
 *  esl_initECBD()
 *********************************************************************************************************************/
/*! \brief           This function initializes the EC-B/D key agreement.
 *  \details        -
 *  \param[in]      deviceID     ID of this device (1..nDevices)
 *  \param[in]      domainExt    domain parameter extension structure
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initWorkSpaceHeader(..) function
 *  \param[in]      nDevices     number of devices partaking in the key agreement
 *  \param[in]      domain       domain parameter structure
 *  \param[in]      speedUpExt   pre-computation structure
 *  \return         ESL_ERC_PARAMETER_INVALID     some parameter is NULL
 *                  ESL_ERC_WS_TOO_SMALL          work space too small
 *                  ESL_ERC_INPUT_INVALID         deviceID is not matching nDevices
 *                  ESL_ERC_NO_ERROR              otherwise
 *  \pre            workSpace is initialized by esl_initWorkSpaceHeader(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_initECBD(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceECBD) workSpace,
  const eslt_Size32 nDevices,
  const eslt_Size32 deviceID,
  VSECPRIM_P2CONST_PARA(eslt_EccDomain) domain,
  VSECPRIM_P2CONST_PARA(eslt_EccDomainExt) domainExt,
  VSECPRIM_P2CONST_PARA(eslt_EccSpeedUpExt) speedUpExt);

/**********************************************************************************************************************
 *  esl_generateEphemeralKeyPairECBD()
 *********************************************************************************************************************/
/*! \brief          This function generates an ephemeral key-pair for EC-B/D.
 *  \details        -
 *  \param[in,out]  publicKey_x  (in) pointer to memory for public key (X coordinate)
 *                               (out) public key (X coordinate)
 *  \param[in,out]  publicKey_y  (in) pointer to memory for public key (Y coordinate)
 *                               (out) public key (Y coordinate)
 *  \param[in,out]  privateKey   (in) pointer to memory for private key (if NULL, key is not returned)
 *                               (out) private key (if pointer not NULL)
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initECBD(..) function
 *  \return         ESL_ERC_PARAMETER_INVALID   some parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID    work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL        work space too small
 *                  ESL_ERC_RNG_FAILED          the random number generator failed
 *                  ESL_ERC_ECC_INTERNAL_ERROR  calculation failed
 *                  ESL_ERC_NO_ERROR            otherwise
 *  \pre            workSpace is initialized by esl_initECBD(..)
 *                  both publicKey_x and .._y are sufficiently large memory areas
(>= prime length of the underlying curve)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_generateEphemeralKeyPairECBD(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceECBD) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Byte) privateKey,
  VSECPRIM_P2VAR_PARA(eslt_Byte) publicKey_x,
  VSECPRIM_P2VAR_PARA(eslt_Byte) publicKey_y);

/**********************************************************************************************************************
 *  esl_calculateIntermediateECBD()
 *********************************************************************************************************************/
/*! \brief          This function calculates the intermediate value (Xi)
 *                  from the left and right neighbors' public keys.
 *  \details        -
 *  \param[in,out]  intermediate_y  (in) pointer to memory for intermediate value (Xi - Y coordinate)
 *                               (out) intermediate value (Xi - Y coordinate)
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initECBD(..) function
 *  \param[in,out]  intermediate_x  (in) pointer to memory for intermediate value (Xi - X coordinate)
 *                               (out) intermediate value (Xi - X coordinate)
 *  \param[in]      pubKeyLeft_y  pointer to left  neighbor's public key (Zi-1 - Y coordinate)
 *  \param[in]      pubKeyLeft_x  pointer to left  neighbor's public key (Zi-1 - X coordinate)
 *  \param[in]      pubKeyRight_y  pointer to right neighbor's public key (Zi+1 - Y coordinate)
 *  \param[in]      pubKeyRight_x  pointer to right neighbor's public key (Zi+1 - X coordinate)
 *  \return         ESL_ERC_PARAMETER_INVALID   some parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID    work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL        work space too small
 *                  ESL_ERC_ECC_INTERNAL_ERROR  calculation failed
 *                  ESL_ERC_NO_ERROR            otherwise
 *  \pre            workSpace is initialized by esl_initECBD(..)
 *                  both intermediate_x and .._y are sufficiently large memory areas
(>= prime length of the underlying curve)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_calculateIntermediateECBD(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceECBD) workSpace,
  VSECPRIM_P2CONST_PARA(eslt_Byte) pubKeyLeft_x,
  VSECPRIM_P2CONST_PARA(eslt_Byte) pubKeyLeft_y,
  VSECPRIM_P2CONST_PARA(eslt_Byte) pubKeyRight_x,
  VSECPRIM_P2CONST_PARA(eslt_Byte) pubKeyRight_y,
  VSECPRIM_P2VAR_PARA(eslt_Byte) intermediate_x,
  VSECPRIM_P2VAR_PARA(eslt_Byte) intermediate_y);

/**********************************************************************************************************************
 *  esl_initSharedSecretECBD()
 *********************************************************************************************************************/
/*! \brief           This function initializes the shared secret generation for EC-B/D.
 *                   It calculates the starting value from already known values (stored in the work space)
 *                   Zi, ai, Xi-1 and nNodes
 *  \details        -
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initECBD(..) function
 *  \return         ESL_ERC_PARAMETER_INVALID   some parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID    work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL        work space too small
 *                  ESL_ERC_NO_ERROR            otherwise
 *  \pre            workSpace is initialized by esl_initECBD(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC( eslt_ErrorCode ) esl_initSharedSecretECBD(
  VSECPRIM_P2VAR_PARA( eslt_WorkSpaceECBD ) workSpace);

/**********************************************************************************************************************
 *  esl_updateSharedSecretECBD()
 *********************************************************************************************************************/
/*! \brief           This function updates the shared secret with an intermediate from another ECU.
 *                   It is to be called repeatedly for all intermediate values received.
 *  \details        -
 *  \param[in]      intermediate_y  pointer to intermediate value (Xi - Y coordinate)
 *  \param[in]      deviceID     ID of the device the intermediate was received from
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initECBD(..) function
 *  \param[in]      intermediate_x  pointer to intermediate value (Xi - X coordinate)
 *  \return         ESL_ERC_PARAMETER_INVALID   some parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID    work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL        work space too small
 *                  ESL_ERC_NO_ERROR            otherwise
 *  \pre            workSpace is initialized by esl_initECBD(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSharedSecretECBD(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceECBD) workSpace,
  const eslt_Size32          deviceID,
  VSECPRIM_P2CONST_PARA(eslt_Byte) intermediate_x,
  VSECPRIM_P2CONST_PARA(eslt_Byte) intermediate_y);

/**********************************************************************************************************************
 *  esl_retrieveSharedSecretECBD()
 *********************************************************************************************************************/
/*! \brief           This function retrieves the shared secret (K).
 *                   When either sharedSecret_x or .._y are NULL, the respective value is DISCARDED.
 *  \details        -
 *  \param[in,out]  sharedSecret_x  (in) pointer to memory for shared secret (K - X coordinate)
 *                               (out) shared secret (K - X coordinate)
 *  \param[in,out]  sharedSecret_y  (in) pointer to memory for shared secret (K - Y coordinate)
 *                               (if NULL, coordinate is not returned)
 *                               (out) shared secret (K - Y coordinate)
 *  \param[in,out]  workSpace    algorithm context buffer, initialized by
 *                               esl_initECBD(..) function
 *  \return         ESL_ERC_PARAMETER_INVALID   some parameter is NULL
 *                  ESL_ERC_WS_STATE_INVALID    work space state invalid
 *                  ESL_ERC_WS_TOO_SMALL        work space too small
 *                  ESL_ERC_ECC_INTERNAL_ERROR  calculation failed
 *                  ESL_ERC_NO_ERROR            otherwise
 *  \pre            workSpace is initialized by esl_initECBD(..)
 *  \context        TASK
 *  \reentrant      TRUE, for different workspaces
 *  \synchronous    TRUE
 *********************************************************************************************************************/
extern VSECPRIM_FUNC(eslt_ErrorCode) esl_retrieveSharedSecretECBD(
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceECBD) workSpace,
  VSECPRIM_P2VAR_PARA(eslt_Byte) sharedSecret_x,
  VSECPRIM_P2VAR_PARA(eslt_Byte) sharedSecret_y);
# endif /* (VSECPRIM_ECBD_GENERIC_ENABLED == STD_ON) */

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
} /* extern "C" */
# endif

#endif /* ESLIB_H */

/**********************************************************************************************************************
 *  END OF FILE  ESLib_types.h
 *********************************************************************************************************************/
