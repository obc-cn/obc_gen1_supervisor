 /**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
 /**        \file vSecPrim.h
 *        \brief  Header file containing current version and revision history
 *
 *      \details  Contains the version, history and static code checker justifications
 *********************************************************************************************************************/

/***********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  --------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  --------------------------------------------------------------------------------------------------------------------
 *  Mathias Waldenburger          vismaw        Vector Informatik GmbH
 *  Eckart Witzel                 visewl        Vector Informatik GmbH
 *  Matthias Weniger              vismwe        Vector Informatik GmbH
 *  Ivo Georgiev                  visivg        Vector Informatik GmbH
 *  --------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  --------------------------------------------------------------------------------------------------------------------
 *  Version    Date        Author  Change Id        Description
 *  --------------------------------------------------------------------------------------------------------------------
 *  01.00.00  2018-07-04   vismaw  STORYC-5521      Initial creation base on former modules SedMod/CryptoCv
 *  01.01.00  2018-08-16   visewl  ESCAN00100414    actMemcmp now performs comparisons in constant time
 *                                 ESCAN00100671    Set memory map sections in ESLib_AES_common.h
 *                                 STORYC-1927      SafeBSW Release of AES-GCM and SipHash
 *                                 STORYC-5720      MISRA-C:2012 Compliance
 *  02.00.00  2018-12-03   vismwe  STORYC-7254      Avoid usage of limit.h
 *                                 STORYC-7277      Support ChaCha20, Poly1305 and EC-Burmester-Desmedt
 *                                 STORYC-7162      Support  KDF X9.63 with SHA-2 256
 *                                 ESCAN00101592    Hash calculation is wrong for data >= 512MB for SHA2-64 (SHA384 and SHA512 variants)
 *                                 ESCAN00101643    x64 only: esl_initVerifyDSAEcP_prim returns always ESL_ERC_WS_TOO_SMALL (160bit)
 *  02.01.00  2019-01-21   vismwe  ESCAN00101811    Compiler error: actBNLENGTH is undefined
 *                         visivg  STORYC-7448      Supprt of HMAC-SHA-384
 *  02.02.00  2019-02-20   vismwe  STORYC-7656      Release of vSecPrim 2.x
 *                                 ESCAN00102231    RC2 encryption and decryption output is to short (4Byte instead of 8Byte).
 **********************************************************************************************************************/

#ifndef VSECPRIM_H
# define VSECPRIM_H

#include "ESLib.h"
/**********************************************************************************************************************
 *  VERSION
 *********************************************************************************************************************/
/* ----- Component version information (decimal version of ALM implementation package) ----- */
# define VSECPRIM_SW_MAJOR_VERSION                    (2u)
# define VSECPRIM_SW_MINOR_VERSION                    (2u)
# define VSECPRIM_SW_PATCH_VERSION                    (0u)

#endif /* VSECPRIM_H */

/**********************************************************************************************************************
 *  END OF FILE: vSecPrim.h
 *********************************************************************************************************************/
