 /**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
 /**        \file vSecPrim.c
 *        \brief  Provides static informations. No functionality provided.
 *
 *      \details  Main file of vSecPrim providing Misra and SBSW justifications.
 *                This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

 /**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/
#define VSECPRIM_SOURCE
#include "vSecPrim.h"
#include "vSecPrim_Cfg.h"
#include "actPlatformTypes.h"

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/
/* Check the version of vSecPrim header file */
#if ((VSECPRIM_SW_MAJOR_VERSION != (2u)) \
  || (VSECPRIM_SW_MINOR_VERSION != (2u)) \
  || (VSECPRIM_SW_PATCH_VERSION != (0u)))
# error "Vendor specific version numbers of vSecPrim.c and vSecPrim.h are inconsistent"
#endif


/**********************************************************************************************************************
 *  Module specific MISRA deviations
 *********************************************************************************************************************/
/* Module specific MISRA deviations:

  MD_VSECPRIM_8.7:
    Reason:     Following this MISRA rule would lead to unreadable code, as the constant fields are huge.
                Vector style guide prevents usage of static variables/constant objects in function scope.
    Risk:       None
    Prevention: Not necessary

  MD_VSECPRIM_5.6:
    Reason:     Naming of the buffer and struct members are similar to provided parameters to make the code more
                readable.
    Risk:       One could read wrong and adapt the code falsely.
    Prevention: Covered by code review and tests.

  MD_VSECPRIM_11.4:
    Reason:     Casting between different pointer is necessary due to the nature of this library.
    Risk:       None - the basic types remain the same.
    Prevention: Covered by code review and tests.

  MD_VSECPRIM_13.1:
    Reason:     An assignment operator is being used in a boolean expression this decreases the
                readability of the code.
    Risk:       Code could be mistaken.
    Prevention: Additional comment is stated.

  MD_VSECPRIM_21.1:
    Reason:     There could be no wraparound because of the previous check.
    Risk:       None
    Prevention: N/A

  MD_VSECPRIM_16.7:
    Reason:     Pointer can only be const when method is empty and only Dummy statements are used.
    Risk:       None
    Prevention: Covered by code review.

  MD_VSECPRIM_9.1:
    Reason:     This function argument is not being unset.
    Risk:       None
    Prevention: Covered by code review.

  MD_VSECPRIM_14.6:
    Reason:     Two break statements are implemented to provide simple yet efficient code.
    Risk:       None
    Prevention: N/A

  MD_VSECPRIM_WATCHDOG:
    Reason:     Calling the watchdog trigger function depends basically on two factors:
                  - If the watchdog trigger function is a valid pointer
                  - Watchdog trigger level
    Risk:       None
    Prevention: N/A

  MD_VSECPRIM_P2VOID_CAST: MISRA Dir 1.1
    Reason:     The implicit conversion from a pointer to object type to a pointer to void is used calling functions
                that allow pointers to different object types as parameter.
    Risk:       There is an invalid conversion within the function from a pointer to void to a pointer to object type
                and thus invalid data are read.
    Prevention: Code inspection.

  MD_VSECPRIM_11.4_P2UINT8_CAST: MISRA Rule 11.4
    Reason:     The conversion from a pointer to void to a pointer to object type is used to access the object's memory bytewise.
                The size of the object's memory is known.
    Risk:       None
    Prevention: N/A

  MD_VSECPRIM_REDUNDANT_OP:
    Reason:     The redundant operation is a side effect of a better readability of the code.
    Risk:       None
    Prevention: N/A

  MD_VSECPRIM_ASSIGNMENT_RESULT_IN_OP: Misra Rule 13.4
    Reason:     The result of an assignment is being used in an arithmetic operation or another assigning operation in
                order to keep the processing flow of the algorithm's complex arithmetic operations as they are 
                described in the algorithm's specification.
    Risk:       None
    Prevention: N/A

  MD_VSECPRIM_1.2_NO_FLEX_ARRAY_MEMBER: Misra Rule 1.2
    Reason:     The array of length is not used as flexible array member but as marker for the start address of the workspace buffer.
    Risk:       None
    Prevention: N/A

  MD_VSECPRIM_USE_OF_BASIC_TYPES: Misra Dir 4.6
    Reason:     Only non safe files are affected.
    Risk:       None
    Prevention: Review.

  MD_VSECPRIM_CONST_BOOL_OPERAND: Misra Rule 14.3
    Reason:     Boolean operand might be constant in definition depend on configuration.
    Risk:       None
    Prevention: N/A

  MD_VSECPRIM_RESDUNDANT_ASSIGNMENT: Misra Rule 2.2
    Reason:     Redundant assignment in last cycle of for loop.
    Risk:       None
    Prevention: N/A
 */

/* SBSW_JUSTIFICATION_BEGIN

  \ID SBSW_VSECPRIM_WRITE_ARRAY_NO_CHECK
    \DESCRIPTION This function has writing operation on an array without any boundary checks.
    \COUNTERMEASURE \N The specific function must not be called by the user but only by the vSecPrim module itself.
                       The calling operations where a possible boundary violation could occur have been validated during the code review.

  \ID SBSW_VSECPRIM_WRITE_ARRAY
    \DESCRIPTION This function has writing operation on an array with boundary check.
    \COUNTERMEASURE \N The specific function must not be called by the user but only by the vSecPrim module itself.
                       Possible boundary violations are evaluated during the code review.

  \ID SBSW_VSECPRIM_WRITE_POINTER
    \DESCRIPTION    The function does a write access on an pointer.
    \COUNTERMEASURE \R The validity of the pointer was checked before.

  \ID SBSW_VSECPRIM_CALL_FUNCTION
    \DESCRIPTION    The function passes a pointer to another function.
    \COUNTERMEASURE \R The validity of the pointer was checked before either by the calling function or a function in the upper layer.

  \ID SBSW_VSECPRIM_FUNCTION_CALL_WATCHDOG
    \DESCRIPTION    The function passes a function pointer referencing a watchdog trigger fucntion to another function.
                    That passed function pointer in turn has been passed by the caller. The API requires that the referenced fucntion is 
                    a watchdog trigger fucntion.
    \COUNTERMEASURE \N The caller ensures that the function pointer passed as parameter is valid and references a watchdog trigger function.

  \ID SBSW_VSECPRIM_FUNCTION_POINTER_CALL_WATCHDOG
    \DESCRIPTION    A function pointer to a watchdog trigger function to be called inside the called function is
                    provided by the caller. The API requires that watchdog is a pointer to a watchdog trigger 
                    function.
    \COUNTERMEASURE \N The specific function must not be called by the user but only by the vSecPrim module itself.
                       The validity of the function pointer is guaranted by the caller of this function.

  \ID SBSW_VSECPRIM_FUNCTION_POINTER_CALL_TRANSFORM
    \DESCRIPTION    A function pointer to a transformation function to be called inside the called function is
                    provided by the caller. The API requires that transfom is a pointer to a  transformation
                    function.
    \COUNTERMEASURE \N The specific function must not be called by the user but only by the vSecPrim module itself.
                       The validity of the function pointer is guaranted by the caller of this function.

  \ID SBSW_VSECPRIM_FUNCTION_CALL_POINTER_CHECKED_WITHIN
    \DESCRIPTION    Pointer(s) are passed to the function without previous validity checks.
    \COUNTERMEASURE \N The function pointer is checked for validity within this function.

  \ID SBSW_VSECPRIM_PTR_WRITE_ACCESS_PASSED_BUFFER
    \DESCRIPTION    The function uses the pointer to write to the referenced object. The pointer is passed as
                    parameter. The API requires that the referenced object is of valid size.
    \COUNTERMEASURE \N [CM_VSECPRIM_CALLER_ENSURES_BUFFER_SIZE]

  \ID SBSW_VSECPRIM_ARRAY_WRITE_ACCESS_LOCAL_BUFFER_FIXED_INDEX
    \DESCRIPTION    The function writes array elememts using fixed indices. The array is defined locally and of known
                    size.
    \COUNTERMEASURE \N [CM_VSECPRIM_EXCLUDE_BOUNDARY_VIOLATION_BY_REVIEW]

  \ID SBSW_VSECPRIM_ARRAY_WRITE_ACCESS_LOCAL_BUFFER_MASKED_INDEX
    \DESCRIPTION    The function writes array elememts using indices that are masked resp. size-restricted by a modulo
                    operation before being applied. The array is defined locally and of known size.
    \COUNTERMEASURE \R [CM_VSECPRIM_EXCLUDE_BOUNDARY_VIOLATION_BY_RUNTIME_MEASURE]

  \ID SBSW_VSECPRIM_ARRAY_WRITE_ACCESS_LOCAL_BUFFER_VARIABLE_INDEX_CHECK
    \DESCRIPTION    The function writes array elememts using indices that are checked against the array size before 
                    being applied. The array is defined locally and of known size.
    \COUNTERMEASURE \N [CM_VSECPRIM_EXCLUDE_BOUNDARY_VIOLATION_BY_REVIEW]

  \ID SBSW_VSECPRIM_ARRAY_WRITE_ACCESS_PASSED_BUFFER_FIXED_INDEX
    \DESCRIPTION    The function writes array elememts using fixed indices. A pointer to the array is passed as
                    parameter. The API requires that the referenced array is of valid size.
    \COUNTERMEASURE \N [CM_VSECPRIM_CALLER_ENSURES_BUFFER_SIZE] and [CM_VSECPRIM_EXCLUDE_BOUNDARY_VIOLATION_BY_REVIEW]

  \ID SBSW_VSECPRIM_ARRAY_WRITE_ACCESS_PASSED_BUFFER_VARIABLE_INDEX_CHECK
    \DESCRIPTION    The function writes array elememts using variable indices that are checked against the array size
                    before being applied. A pointer to the array is passed as parameter. The API requires that the
                    referenced array is of valid size.
    \COUNTERMEASURE \N [CM_VSECPRIM_CALLER_ENSURES_BUFFER_SIZE] and [CM_VSECPRIM_EXCLUDE_BOUNDARY_VIOLATION_BY_REVIEW]

  \ID SBSW_VSECPRIM_FCT_CALL_LOCAL_BUFFER
    \DESCRIPTION    The function passes a pointer referencing a locally defined buffer of known size to another function.
    \COUNTERMEASURE \N [CM_VSECPRIM_CALLEE_ENSURES_BUFFER_SIZE]

  \ID SBSW_VSECPRIM_FCT_CALL_LOCAL_BUFFER_FIXED_SIZE
    \DESCRIPTION    The function passes a pointer referencing a locally defined buffer of known size and its size to
                    another function. The passed buffer size is fixed.
    \COUNTERMEASURE \N [CM_VSECPRIM_CALLEE_ENSURES_PASSED_BUFFER_SIZE]

  \ID SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER
    \DESCRIPTION    The function passes a pointer referencing a buffer to another function. That passed pointer in
                    turn has been passed by the caller. The API requires that the referenced object is of valid size.
    \COUNTERMEASURE \N [CM_VSECPRIM_CALLER_ENSURES_BUFFER_SIZE]

  \ID SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER_AND_LOCAL_BUFFER
    \DESCRIPTION    The function passes a pointer referencing a buffer to another function. That passed pointer in
                    turn has been passed by the caller. The API requires that the referenced object is of valid size.
                    The function passes a pointer referencing a locally defined buffer of known size and its size to
                    another function.
    \COUNTERMEASURE \N [CM_VSECPRIM_CALLER_ENSURES_BUFFER_SIZE] and [CM_VSECPRIM_CALLEE_ENSURES_BUFFER_SIZE]

  \ID SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER_FIXED_SIZE
    \DESCRIPTION    The function passes a pointer referencing a buffer and its size to another function. The passed
                    buffer size is fixed. That passed pointer in turn has been passed by the caller. The API requires 
                    that the referenced object is of valid size.
    \COUNTERMEASURE \N [CM_VSECPRIM_CALLER_ENSURES_BUFFER_SIZE] and [CM_VSECPRIM_CALLEE_ENSURES_PASSED_BUFFER_SIZE]

  \ID SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER_VARIABLE_SIZE
    \DESCRIPTION    The function passes a pointer referencing a buffer and its size to another function. The passed
                    buffer size is variable. That passed pointer in turn has been passed by the caller. The API requires 
                    that the referenced object is of valid size.
    \COUNTERMEASURE \N [CM_VSECPRIM_CALLER_ENSURES_BUFFER_SIZE] and [CM_VSECPRIM_CALLEE_ENSURES_PASSED_BUFFER_SIZE]
                       and:
                       The buffer to be written is at least of the size of one block,
                       The size of the written bytes is always equal or smaller than the block size, see [CM_VSECPRIM_BUFFER_USED_SMALLER_BLOCK_SIZE]

  \ID SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER_VARIABLE_SIZE_CHECK
    \DESCRIPTION    The function passes a pointer referencing a buffer and its size to another function. The passed
                    buffer size is variable and it is checked that the size does not exceed the size of the passed buffer.
                    That passed pointer in  turn has been passed by the caller. The API requires 
                    that the referenced object is of valid size.
    \COUNTERMEASURE \N [CM_VSECPRIM_CALLER_ENSURES_BUFFER_SIZE] and [CM_VSECPRIM_CALLEE_ENSURES_PASSED_BUFFER_SIZE]

  \ID SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER_FIXED_SUBADDRESS
    \DESCRIPTION    The function passes a pointer referencing a buffer to another function. That passed pointer is a
                    fixed sub-address within the buffer passed by the caller. The API requires that the referenced object is
                    of valid size.
    \COUNTERMEASURE \N [CM_VSECPRIM_CALLER_ENSURES_BUFFER_SIZE] and [CM_VSECPRIM_CALLEE_ENSURES_VALID_SUBADDRESS]

  \ID SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER_FIXED_SUBADDRESS_FIXED_SIZE
    \DESCRIPTION    The function passes a pointer referencing a buffer to another function. That passed pointer is a
                    fixed sub-address within the buffer passed by the caller. The passed buffer size is fixed. The API
                    requires that the referenced object is of valid size.
    \COUNTERMEASURE \N [CM_VSECPRIM_CALLER_ENSURES_BUFFER_SIZE] and [CM_VSECPRIM_CALLEE_ENSURES_PASSED_BUFFER_SIZE] and [CM_VSECPRIM_CALLEE_ENSURES_VALID_SUBADDRESS]

  \ID SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER_VARIABLE_SUBADDRESS
    \DESCRIPTION    The function passes a pointer referencing a buffer to another function. That passed pointer is a
                    variable sub-address within the buffer passed by the caller. The API requires that the referenced object is
                    of valid size.
    \COUNTERMEASURE \N [CM_VSECPRIM_CALLER_ENSURES_BUFFER_SIZE] and [CM_VSECPRIM_CALLEE_ENSURES_VALID_SUBADDRESS]

  \ID SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER_VARIABLE_SUBADDRESS_VARIABLE_SIZE
    \DESCRIPTION    The function passes a pointer referencing a buffer to another function. That passed pointer is a
                    variable sub-address within the buffer passed by the caller. The passed buffer size is variable.
                    The API requires that the referenced object is of valid size.
    \COUNTERMEASURE \N [CM_VSECPRIM_CALLER_ENSURES_BUFFER_SIZE] and [CM_VSECPRIM_CALLEE_ENSURES_PASSED_BUFFER_SIZE] and [CM_VSECPRIM_CALLEE_ENSURES_VALID_SUBADDRESS]
                    and:
                    info->buffer is the size of one block  (e.g. actSIPHASH_BLOCK_SIZE, actGHASH_BLOCK_SIZE, actGCM_BLOCK_SIZE)
                    &info->buffer[info->buffer_used] is within info->buffer since info->buffer_used is always equal or smaller than the block size, see [CM_VSECPRIM_BUFFER_USED_SMALLER_BLOCK_SIZE]
                    the size of the buffer passed by &info->buffer[info->buffer_used] is (block size - info->buffer_used),
                    the size to be written (e.g. diff) is equal or smaller than (block size - info->buffer_used), see [CM_VSECPRIM_BUFFER_USED_SMALLER_BLOCK_SIZE]

  \ID SBSW_VSECPRIM_FCT_CALL_P2CONST_PARAM
    \DESCRIPTION    The callee passes a pointer referencing a buffer to another function. The parameter for passing the pointer is qualified as "pointer to const".
    \COUNTERMEASURE \N Pointers qualified as "pointer to const" can't be used for write access.

  \ID SBSW_VSECPRIM_ARRAY_WRITE_ACCESS_PASSED_BUFFER_FIXED_INDEX_SIPHASH_FINALIZE
    \DESCRIPTION    The function esl_finalizeSipHash writes to the obejct referenced by parameter 'messageMAC'.
                    It writes array elememts using fixed indices.
    \COUNTERMEASURE \S The caller of function esl_finalizeSipHash must ensure that the pointer passed to the parameter 'messageMAC'
                       references a valid memory location and that the size of the array referenced by the parameter 'messageMAC'
                       is greater or equal to 8 bytes. [SMI-160485]

  \ID SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER_GCM_ENCRYPT_DECRYPT_UPDATE
    \DESCRIPTION    The functions esl_updatePlaintextEncryptGCM and esl_updateCiphertextDecryptGCM write to the object referenced by parameter 'output'.
                    They pass the pointer given by 'output' to another function.
    \COUNTERMEASURE \S The caller of functions esl_updatePlaintextEncryptGCM and esl_updateCiphertextDecryptGCM must ensure that the pointer passed to 
                       the parameter 'output' references a valid memory location and that the size of the array referenced by the 
                       parameter 'output' is a multiple of the GCM block size (16 bytes) and greater or equal than the size given by parameter 'inputLength'. [SMI-160486]

  \ID SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER_GCM_ENCRYPT_DECRYPT_FINALIZE
    \DESCRIPTION    The functions esl_finalizeEncryptGCM and esl_finalizeDecryptGCM write to the object referenced by parameter 'output'.
                    They pass the pointer given by 'output' to another function.
    \COUNTERMEASURE \S The caller of functions esl_finalizeEncryptGCM and esl_finalizeDecryptGCM must ensure that the pointer passed to 
                       the parameter 'output' references a valid memory location and that the size of the array referenced by the 
                       parameter 'output' is greater or equal than the GCM block size (16 bytes). [SMI-160487]

  \ID SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER_CMACAES128_FINALIZE
    \DESCRIPTION    The function esl_finalizeCMACAES128 writes to the object referenced by parameter 'messageCMAC'.
                    It passes the pointer given by 'messageCMAC' to another function.
    \COUNTERMEASURE \S The caller of functions esl_finalizeCMACAES128 must ensure that the pointer passed to 
                       the parameter 'messageCMAC' references a valid memory location and that the size of the array referenced by the 
                       parameter 'messageCMAC' is greater or equal than the AES block size (16 bytes). [SMI-163627]

  \ID SBSW_VSECPRIM_FCT_CALL_PASSED_BUFFER_SHA_FINALIZE
    \DESCRIPTION    The functions 
                      esl_finalizeSHA1
                      esl_finalizeSHA224
                      esl_finalizeSHA256
                      esl_finalizeSHA384
                      esl_finalizeSHA512
                      esl_finalizeSHA512_224
                      esl_finalizeSHA512_256
                    write to the object referenced by parameter 'messageDigest'.
                    They pass the pointer given by 'messageDigest' to another function.
    \COUNTERMEASURE \S The caller of functions 
                        esl_finalizeSHA1
                        esl_finalizeSHA224
                        esl_finalizeSHA256
                        esl_finalizeSHA384
                        esl_finalizeSHA512
                        esl_finalizeSHA512_224
                        esl_finalizeSHA512_256
                       must ensure that the pointer passed to the parameter 'messageDigest' references a valid memory
                       location and that the size of the array referenced by the parameter 'messageDigest' is greater 
                       or equal than 20 bytes. [SMI-163628]

SBSW_JUSTIFICATION_END */

/*

  \CM CM_VSECPRIM_EXCLUDE_BOUNDARY_VIOLATION_BY_REVIEW The write operations where a possible boundary violation could occur have been validated during the code review.

  \CM CM_VSECPRIM_EXCLUDE_BOUNDARY_VIOLATION_BY_RUNTIME_MEASURE Write operations where a possible boundary violation could occur are prevented by run-time measure.

  \CM CM_VSECPRIM_CALLER_ENSURES_BUFFER_SIZE The caller ensures that the pointer passed as parameter is valid and references an object of valid size.

  \CM CM_VSECPRIM_CALLEE_ENSURES_BUFFER_SIZE The callee ensures that the passed buffer is of valid size, i.e. that the size is as required by the called function.

  \CM CM_VSECPRIM_CALLEE_ENSURES_PASSED_BUFFER_SIZE The callee ensures that the passed buffer size does not exceed the size of the passed buffer.

  \CM CM_VSECPRIM_CALLEE_ENSURES_VALID_SUBADDRESS The callee ensures that the passed buffer referenced by the sub-address is of valid size, i.e. that it is fully within the buffer passed by the caller.

  \CM CM_VSECPRIM_BUFFER_USED_SMALLER_BLOCK_SIZE  Evaulation of all write accesses to info->buffer_used, proofing that info->buffer_used < BLOCK_SIZE
                                                  1.) In init and finalize functions:
                                                      info->buffer_used = 0u;
                                                  2.) In update functions:
                                                      I) diff = minimum(length, BLOCK_SIZE - info->buffer_used)
                                                          info->buffer_used += diff
                                                          --> info->buffer_used <= BLOCK_SIZE
                                                          A) If info->buffer_used < BLOCK_SIZE:
                                                            Update is finished 
                                                            --> info->buffer_used < BLOCK_SIZE
                                                          B) If info->buffer_used == BLOCK_SIZE:
                                                            blocks = (length - diff) / BLOCK_SIZE
                                                            diff is as calculated previously
                                                            info->buffer_used = (length - (diff + (BLOCK_SIZE * blocks)))
                                                            info->buffer_used = length - (diff + (BLOCK_SIZE * blocks))
                                                                              = (length - diff) - ((length - diff) / BLOCK_SIZE ) * BLOCK_SIZE
                                                                              = (length - diff) % BLOCK_SIZE
                                                            --> info->buffer_used < BLOCK_SIZE

 */


/* COV_JUSTIFICATION_BEGIN

  \ID COV_VSECPRIM_GHASH_SPEED_UP
    \REASON This check always has the same result since the switch actGHASH_SPEED_UP is constantly defined to the value 1.
    \PREVENTION Covered by code review.

  \ID COV_VSECPRIM_CPLUSPLUS
    \REASON This check always results in FALSE since the switch __cplusplus is always undefined.
    \PREVENTION Covered by code review.

  \ID COV_VSECPRIM_NO_SAFE_CONFIG
    \REASON This algorithm or option must not be configured for building a safe libraray.
    \PREVENTION ELISA plug-in

  \ID COV_VSECPRIM_STD_VALUES
    \ACCEPT TX
    \ACCEPT XF
    \REASON This check can only be true if the parameter is not defined externally.
            This check can only be false if the parameter is defined externally.

  \ID COV_VSECPRIM_NOT_CONFIGURABLE
    \REASON This check always results in the same boolean value since the considered parameter is constantly defined internally.
    \PREVENTION Covered by code review.

COV_JUSTIFICATION_END */


/**********************************************************************************************************************
 *  END OF FILE: vSecPrim.c
 *********************************************************************************************************************/
