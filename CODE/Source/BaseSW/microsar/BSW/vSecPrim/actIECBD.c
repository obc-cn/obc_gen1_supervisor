/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2018 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  actIECBD.c
 *        \brief  ECBD implementation.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library actCLib
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

/****************************************************************************
 **
 ** EC-B/D key agreement (internal) programming interface
 **
 ** constants:
 **
 ** types:
 **   actECBDscratch        scratch pad addressing
 **
 ** macros:
 **   thisECU               this ECUs ID
 **   leftECU               the "left neighbor" ECUs ID
 **   ecoOff                offset of the given ECU to this ECU (circular left)
 **
 ** functions:
 **   actECBDinit           initialize EC-N/D protocol
 **   actECBDgenKeyPair     generate the ephemeral key pair
 **   actECBDcalcXi         calculate the intermediate value
 **   actECBDinitK          initialize calculation of common secret
 **   actECBDupdateK        update common secret with another ECUs intermediate
 **   actECBDretrieveK      retrieve the common secret
 **
 ***************************************************************************/
 
#define ACTIECBD_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

#include "actIECBD.h"
#include "actECTools.h"
#include "actECPoint.h"
#include "actECLengthInfo.h"

#if (VSECPRIM_ACTECBD_GENERIC_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

 /* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/**********************************************************************************************************************
 *  DATA PROTOTYPES
 *********************************************************************************************************************/

typedef struct 
{
    VSECPRIM_P2VAR_PARA(actBNDIGIT) ai;   /*  private key */
    actECPOINT   Zi;   /*  public  key  (own / left) */
    actECPOINT   Xi;   /*  intermediate (own) */
    actECPOINT   TP;   /*  temporary point */
} actECBDscratch;


/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

#define thisECU(ws)     (     (ws)->bd.ecuID)
#define leftECU(ws)     (1 == (ws)->bd.ecuID ? (ws)->bd.nECUs : (ws)->bd.ecuID -1)
#define ecuOff(ecu, ws)  (((ws)->bd.nECUs + (ecu) - (ws)->bd.ecuID) % (ws)->bd.nECUs)

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*  initialize scratch-pad memory */
VSECPRIM_LOCAL_FUNC( void )           initScratch  (VSECPRIM_P2VAR_PARA( actECBDscratch ) scratch, VSECPRIM_P2VAR_PARA( actECBDstruct ) ws);

VSECPRIM_LOCAL_FUNC( void )          actECPImport (VSECPRIM_P2VAR_PARA( actECPOINT ) P, 
                                                    VSECPRIM_P2CONST_PARA( actU8 ) x, 
                                                    VSECPRIM_P2CONST_PARA( actU8 ) y, 
                                                    VSECPRIM_P2VAR_PARA( actECCURVE ) curve);
VSECPRIM_LOCAL_FUNC( actRETURNCODE ) actECPExport (VSECPRIM_P2VAR_PARA( actU8 ) x, 
                                                    VSECPRIM_P2VAR_PARA( actU8 ) y, 
                                                    VSECPRIM_P2VAR_PARA( actECPOINT ) P,
                                                    VSECPRIM_P2VAR_PARA( actECPOINT )TP, 
                                                    VSECPRIM_P2VAR_PARA( actECCURVE ) curve);

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/


VSECPRIM_FUNC( actRETURNCODE ) actECBDinit (VSECPRIM_P2VAR_PARA( actECBDstruct ) ws,
                     const int              wsSize,
                     const actU32              nECUs,
                     const actU32              ecuID,
                     actPROMU8 domain,
                     actPROMU8 domainExt,
                     actPROMU8 speedupExt) {

    VSECPRIM_P2VAR_PARA( actU8 ) ecWS     = (VSECPRIM_P2VAR_PARA( actU8 )) &ws->curve;       /*  actECInit() expects:         actECCURVE | scratch */
    int     ecWSSize = wsSize - sizeof (ws->bd);   /*  actECInit() expects: sizeof (actECCURVE | scratch) */

   /*  parameters already validated */
    ws->bd.nECUs     = nECUs;
    ws->bd.ecuID     = ecuID;

    return actECInit (domain, domainExt, speedupExt, actEC_ALGO_FLAG_BDKA, ecWS, ecWSSize);
}


VSECPRIM_FUNC( actRETURNCODE ) actECBDgenKeyPair(VSECPRIM_P2VAR_PARA( actECBDstruct ) ws,
                                                 VSECPRIM_P2VAR_PARA( actU8 ) privKey,
                                                 VSECPRIM_P2VAR_PARA( actU8 ) pubKey_x,
                                                 VSECPRIM_P2VAR_PARA( actU8 ) pubKey_y) 
{

    actECBDscratch  scratch;
    actRETURNCODE   rc;


   /*  parameters already validated */
    if (NULL_PTR == ws)
        return actEXCEPTION_NULL;

   /*  initialize working variables */
    initScratch ((VSECPRIM_P2VAR_PARA( actECBDscratch ))&scratch, ws);


   /*  generate ai at random */
    if (actOK != (rc = actBNModRandomize (scratch.ai, &ws->curve.n_field, ws->curve.t))) {
       /*  clear ai */
        actBNSetZero (scratch.ai, ws->curve.n_field.m_length);
        return rc;
    }

   /*  calculate Zi = ai * G */
    actECPMultG (scratch.ai, &scratch.TP, &scratch.Zi, &ws->curve);

   /*  return public key */
    rc = actECPExport (pubKey_x, pubKey_y, &scratch.Zi, NULL_PTR, &ws->curve);

    if (actOK != rc) {
       /*  clear ai and Zi */
        actBNSetZero (scratch.ai,      ws->curve.n_field.m_length);
        actBNSetZero (scratch.Zi.x, 3* ws->curve.p_field.m_length);
    } else {
        if (NULL_PTR != privKey)
           /*  return private key */
            actBNOctetString (privKey,  ws->curve.n_field.m_byte_length, scratch.ai,   ws->curve.n_field.m_length);
    }

    return rc;
}


VSECPRIM_FUNC( actRETURNCODE ) actECBDcalcXi(VSECPRIM_P2VAR_PARA( actECBDstruct ) ws,
                                             VSECPRIM_P2CONST_PARA( actU8 ) ZiLeft_x,
                                             VSECPRIM_P2CONST_PARA( actU8 ) ZiLeft_y,
                                             VSECPRIM_P2CONST_PARA( actU8 ) ZiRight_x,
                                             VSECPRIM_P2CONST_PARA( actU8 ) ZiRight_y,
                                             VSECPRIM_P2VAR_PARA( actU8 ) Xi_x,
                                             VSECPRIM_P2VAR_PARA( actU8 ) Xi_y)
{

    actECBDscratch  scratch;
    actRETURNCODE   rc;


   /*  parameters already validated */
    if (NULL_PTR == ws)
        return actEXCEPTION_NULL;

   /*  initialize working variables */
    initScratch (&scratch, ws);

   /*  import Zi(l) -> Zi (remains stored) */
    actECPImport (&scratch.Zi, ZiLeft_x,  ZiLeft_y,  &ws->curve);

   /*  import Zi(r) -> TP (just temporarily) */
    actECPImport (&scratch.TP, ZiRight_x, ZiRight_y, &ws->curve);

   /*  Xi = ai * (Zi(r) - Zi(l)) */
    actECPSub    (&scratch.TP, &scratch.Zi,              &ws->curve);      /*  TP = (Zi(r) - Zi(l)) */
    actECPMult   (&scratch.TP,  scratch.ai, &scratch.Xi, &ws->curve);      /*  Xi = ai * (Zi(r) - Zi(l)) */

   /*  export Xi */

    rc = actECPExport (Xi_x, Xi_y, &scratch.Xi, &scratch.TP, &ws->curve);
    if (actOK != rc) {
       /*  clear ai, Zi and Xi */
        actBNSetZero (scratch.ai,      ws->curve.n_field.m_length);
        actBNSetZero (scratch.Zi.x, 6* ws->curve.p_field.m_length);
    }
   /*  clear TP */
    actBNSetZero     (scratch.TP.x, 3* ws->curve.p_field.m_length);

    return rc;
}


VSECPRIM_FUNC( actRETURNCODE ) actECBDinitK(VSECPRIM_P2VAR_PARA( actECBDstruct ) ws) {

    actECBDscratch  scratch;


   /*  NO parameters to validate */
    if (NULL_PTR == ws)
        return actEXCEPTION_NULL;

   /*  initialize working variables */
    initScratch (&scratch, ws);

   /*  K   = (nECUs * ai) * Zi(l) + (nECUs -1) * Xi */
    actECPMult      (&scratch.Zi, scratch.ai,      &scratch.TP, &ws->curve);   /*  TP      = ai    * Zi(l) */
    actECPMultShort (&scratch.TP, ws->bd.nECUs,    &scratch.Zi, &ws->curve);   /*  K (Zi)  = nECUs * TP */
    actECPMultShort (&scratch.Xi, ws->bd.nECUs -1, &scratch.TP, &ws->curve);   /*  TP      = (nECUs -1) * Xi */
    actECPAdd       (&scratch.Zi, &scratch.TP,                  &ws->curve);   /*  K (Zi) += TP */

   /*  clear ai, Xi and TP */
    actBNSetZero (scratch.ai,      ws->curve.n_field.m_length);
    actBNSetZero (scratch.Xi.x, 6* ws->curve.p_field.m_length);

   /*  Zi has been overwritten with K */

    return actOK;
}


VSECPRIM_FUNC( actRETURNCODE ) actECBDupdateK(VSECPRIM_P2VAR_PARA( actECBDstruct ) ws,
                        const actU32         ecuID,
                        VSECPRIM_P2CONST_PARA(actU8) Xi_x,
                        VSECPRIM_P2CONST_PARA(actU8) Xi_y) {

    actECBDscratch  scratch;
    actU32          tmp;

   /*  parameters already validated */
    if (NULL_PTR == ws)
        return actEXCEPTION_NULL;

   /*  initialize working variables */
    initScratch (&scratch, ws);

   /*  check ecuID */
    if ((ecuID == thisECU (ws))
    ||  (ecuID == leftECU (ws)))
       /*  done! */
       /*  these have been handled in actECBDinitK() */
        return actOK;

   /*  import Xi */
    actECPImport (&scratch.Xi, Xi_x,  Xi_y,  &ws->curve);

   /*  K  += ((nECUs -1) - ((ecuID - ownID) mod nECUs)) * Xi */
    tmp = (ws->bd.nECUs -1) - ecuOff (ecuID, ws);                  /*  tmp     = (nECUs -1) - ((ecuID - ownID) mod nECUs) */
    actECPMultShort (&scratch.Xi, tmp, &scratch.TP, &ws->curve);   /*  TP      = tmp * Xi */
    actECPAdd       (&scratch.Zi,      &scratch.TP, &ws->curve);   /*  K (Zi) += TP */

   /*  clear Xi, TP */
    actBNSetZero (scratch.Xi.x, 6* ws->curve.p_field.m_length);

    return actOK;
}


VSECPRIM_FUNC( actRETURNCODE ) actECBDretrieveK (VSECPRIM_P2VAR_PARA( actECBDstruct ) ws,
                                VSECPRIM_P2VAR_PARA(actU8) K_x,
                                VSECPRIM_P2VAR_PARA(actU8) K_y) {

    actECBDscratch  scratch;
    actRETURNCODE   rc;


   /*  parameters already validated */
    if (NULL_PTR == ws)
        return actEXCEPTION_NULL;

   /*  initialize working variables */
    initScratch (&scratch, ws);

   /*  export K (Zi) */
    rc = actECPExport (K_x, K_y, &scratch.Zi, NULL_PTR, &ws->curve);

   /*  clear K (Zi) */
    actBNSetZero (scratch.Zi.x, 3* ws->curve.p_field.m_length);

    return rc;
}


/* --------------------------------------------------------------------------
 | local (private) functions
 ------------------------------------------------------------------------- */

VSECPRIM_LOCAL_FUNC( void ) initScratch(VSECPRIM_P2VAR_PARA( actECBDscratch )scratch, VSECPRIM_P2VAR_PARA( actECBDstruct ) ws) {
    VSECPRIM_REGISTER VSECPRIM_P2VAR_PARA( actBNDIGIT ) memory;

    int primeLen = ws->curve.p_field.m_length;
    int orderLen = ws->curve.n_field.m_length;

    memory  = (VSECPRIM_P2VAR_PARA( actBNDIGIT )) (((VSECPRIM_P2VAR_PARA( actU8 )) ws ) + sizeof (actECBDstruct));  /*  scratch is "behind" the work space */
    memory += actECBasicWksp (primeLen, orderLen);                     /*  this part of scratch is in use by actECInit() */

    scratch->ai     = memory;   memory += orderLen;
    scratch->Zi.x   = memory;   memory += primeLen;
    scratch->Zi.y   = memory;   memory += primeLen;
    scratch->Zi.z   = memory;   memory += primeLen;
    scratch->Xi.x   = memory;   memory += primeLen;
    scratch->Xi.y   = memory;   memory += primeLen;
    scratch->Xi.z   = memory;   memory += primeLen;
    scratch->TP.x   = memory;   memory += primeLen;
    scratch->TP.y   = memory;   memory += primeLen;
    scratch->TP.z   = memory;

    /*  ASSUME Zi and Xi are storing points in projective (Montgomery) representation!
     *
     *  This is a valid assumption, as those points have been calculated before:
     *  * keyGeneration:    Zi   = ai * G
     *  * intermediate:     Zi   = Zi(l) (imported)
     *                      Xi   = Zi(r) - Zi(l)
     *  * init K calc.:  K (Zi)  = (n * ai) * Zi(l) + (n -1) * Xi
     *  * update K:      K (Zi) += (x) * Xi (imported)
     *
     *  TP is never used to store points between function calls.
     */
    scratch->Zi.is_affine = scratch->Zi.is_infinity = 0;
    scratch->Xi.is_affine = scratch->Xi.is_infinity = 0;
}


/*  import a point (x, y) and convert it to */
/*  projective Montgomery representation */
VSECPRIM_LOCAL_FUNC( void ) actECPImport (VSECPRIM_P2VAR_PARA( actECPOINT ) P, VSECPRIM_P2CONST_PARA( actU8 ) x, VSECPRIM_P2CONST_PARA( actU8 ) y, VSECPRIM_P2VAR_PARA( actECCURVE ) curve) {
    int pDigits = curve->p_field.m_length;
    int pBytes  = curve->p_field.m_byte_length;

    actBNSetOctetString (P->x, pDigits, x, pBytes);
    actBNSetOctetString (P->y, pDigits, y, pBytes);
    actBNSetOne         (P->z, pDigits);
    P->is_affine   = 0;
    P->is_infinity = 0;

    actECPToMont (P, curve);
}

/*  convert a point to affine coordinates (x, y) and export it */
/*  TP is an optional temporary work space, if the point P has to be left unchanged! */
VSECPRIM_LOCAL_FUNC( actRETURNCODE ) actECPExport (VSECPRIM_P2VAR_PARA( actU8 ) x, VSECPRIM_P2VAR_PARA( actU8 ) y, VSECPRIM_P2VAR_PARA( actECPOINT ) P, VSECPRIM_P2VAR_PARA( actECPOINT ) TP, VSECPRIM_P2VAR_PARA( actECCURVE ) curve) {
    int pDigits = curve->p_field.m_length;
    int pBytes  = curve->p_field.m_byte_length;

    if (NULL_PTR != TP)
        actECPAssign (TP, P, curve);
    else
        TP = P;

    if (! actECPToAffineFromMont (TP, curve, 0))
        return actEXCEPTION_POINT;

    if (NULL_PTR != x)  actBNOctetString (x, pBytes, TP->x, pDigits);
    if (NULL_PTR != y)  actBNOctetString (y, pBytes, TP->y, pDigits);

    return actOK;
}

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ACTECBD_GENERIC_ENABLED == STD_ON) */

/**********************************************************************************************************************
 *  END OF FILE: actIECBD.c
 *********************************************************************************************************************/
