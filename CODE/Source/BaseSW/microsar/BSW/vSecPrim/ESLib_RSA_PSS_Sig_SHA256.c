/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file ESLib_RSA_PSS_Sig_SHA256.c
 *        \brief Crypto library - PKCS #1 RSA PSS signature generation (SHA-256)
 *
 *      \details RSA signature generation using RSA PSS encoding scheme according to PKCS #1 v2.2
 *               Currently the actClib version is used.
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/
#define ESLIB_RSA_PSS_SIG_SHA256_SOURCE

/***********************************************************************************************************************
 *  INCLUDES
 **********************************************************************************************************************/

#include "ESLib.h"
#include "ESLib_types.h"

#include "actIRSA.h"
#include "actUtilities.h"
#include "actISHA2_32.h"

#if (VSECPRIM_RSA_PSS_SHA2_256_ENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */
/***********************************************************************************************************************
 *  DEFINES
 **********************************************************************************************************************/

/** Trailer field of encoded message */
# define ESL_RSA_PSS_TRAILER                                          (0xBCu)

/** Hash initialization function */
# define ESL_RSA_PSS_INIT_HASH_FCT(workspace)                         (void)actSHA256Init(&((workspace)->wsSHA256))
/** Hash update function */
# define ESL_RSA_PSS_UPDATE_HASH_FCT(workspace, inputSize, input)     actSHA256Update(&((workspace)->wsSHA256), (VSECPRIM_P2CONST_PARA(actU8))(input), (actLengthType)(inputSize), (workspace)->header.watchdog)
/** Hash finalization function */
# define ESL_RSA_PSS_FINALIZE_HASH_FCT(workspace, messageDigest)      (void)actSHA256Finalize(&((workspace)->wsSHA256), (VSECPRIM_P2VAR_PARA(actU8))(messageDigest),  (workspace)->header.watchdog)

/** Size of message digest (hash) */
# define ESL_RSA_PSS_SIZEOF_DIGEST                                    ESL_SIZEOF_SHA256_DIGEST

/** Offset of buffer length in work space */
# define ESL_WS_RSA_PSS_SV_BUFFERLENGTH                               ESL_WS_RSA_PSS_SV_SHA256_BUFFERLENGTH
/** Offset of message buffer in work space */
# define ESL_WS_RSA_PSS_SV_BUFFER                                     ESL_WS_RSA_PSS_SV_SHA256_BUFFER
/** Offset of DB in work space */
# define ESL_WS_RSA_PSS_SV_DB_BUFFER(ByteSizeOfBuffer)                ESL_WS_RSA_PSS_SV_SHA256_DB_BUFFER(ByteSizeOfBuffer)
/** Offset of hash work space embedded in work space */
# define ESL_WS_RSA_PSS_SV_WS_HASH(ByteSizeOfBuffer)                  ESL_WS_RSA_PSS_SV_SHA256_WS_SHA256(ByteSizeOfBuffer)
/** Offset of RSA work space embedded in work space */
# define ESL_WS_RSA_PSS_SV_WS_RSA_PRIM(ByteSizeOfBuffer)              ESL_WS_RSA_PSS_SV_SHA256_WS_RSA_PRIM(ByteSizeOfBuffer)
/** Total work space overhead */
# define ESL_SIZEOF_WS_RSA_PSS_SV_OVERHEAD(ByteSizeOfBuffer)          ESL_SIZEOF_WS_RSA_PSS_SV_SHA256_OVERHEAD(ByteSizeOfBuffer)

/***********************************************************************************************************************
 *  TYPEDEFS
 **********************************************************************************************************************/

/** Hash operation work space */
typedef eslt_WorkSpaceSHA256 eslt_WorkSpaceHash;

/***********************************************************************************************************************
 *  LOCAL DATA
 **********************************************************************************************************************/

VSECPRIM_ROM(extern, eslt_Byte) zeroMasks[8];

/***********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/
# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

VSECPRIM_LOCAL_FUNC(eslt_ErrorCode) esl_getKeyPairModuleSizeSignSHA256(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace, VSECPRIM_P2VAR_PARA(eslt_Length) keyPairModuleSize);

VSECPRIM_LOCAL_FUNC(eslt_ErrorCode) esl_getWorkspaceHashSignSHA256(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace, VSECPRIM_P2VAR_PARA(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash)) wsHash);

/***********************************************************************************************************************
 *  LOCAL FUNCTIONS
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  esl_getKeyPairModuleSizeSignSHA256
 **********************************************************************************************************************/
/*! \brief         Evaluate module size of key pair stored in workspace
 *  \pre           Work space initialized
 *  \param[in]     workSpace  Signature generation work space
 *  \param[out]    keyPairModuleSize  Size of key pair module
 *  \return        Result of operation
 *  \retval        ESL_ERC_NO_ERROR  Operation successful
 *  \retval        ESL_ERC_PARAMETER_INVALID  Work space is NULL
 *  \retval        ESL_ERC_WS_STATE_INVALID  Work space state invalid
 **********************************************************************************************************************/
VSECPRIM_LOCAL_FUNC(eslt_ErrorCode) esl_getKeyPairModuleSizeSignSHA256(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace, VSECPRIM_P2VAR_PARA(eslt_Length) keyPairModuleSize)
{
  eslt_ErrorCode returnValue;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader) wsHeader;

  returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* Get underlying workspace pointer */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte)) workSpace;
    wsHeader = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader)) wsRAW;
    *keyPairModuleSize = (eslt_Length) (((eslt_Length) wsRAW[ESL_WS_RSA_PSS_SV_BUFFERLENGTH] << 8) + (wsRAW[ESL_WS_RSA_PSS_SV_BUFFERLENGTH + 1]));

    if ((ESL_WST_ALGO_RSA | ESL_WST_M_RUNNING) != (wsHeader->status & (ESL_WST_M_ALGO | ESL_WST_M_RUNNING)))
    {
      returnValue = ESL_ERC_WS_STATE_INVALID;
    }
  }

  return returnValue;
}

/***********************************************************************************************************************
 *  esl_getWorkspaceHashSignSHA256
 **********************************************************************************************************************/
/*! \brief         Return pointer to work space of hash embedded in signature work space
 *  \pre           Work space initialized
 *  \param[in]     workSpace  Signature generation work space
 *  \param[out]    wsHash  Hash calculation work space
 *  \return        Result of operation
 *  \retval        ESL_ERC_NO_ERROR  Operation successful
 *  \retval        ESL_ERC_PARAMETER_INVALID  Work space is NULL
 *  \retval        ESL_ERC_WS_STATE_INVALID  Work space state invalid
 **********************************************************************************************************************/
VSECPRIM_LOCAL_FUNC(eslt_ErrorCode) esl_getWorkspaceHashSignSHA256(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace, VSECPRIM_P2VAR_PARA(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash)) wsHash)
{
  eslt_ErrorCode returnValue;
  eslt_Length keyPairModuleSize;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;

  /* Evaluate workspace status, value of key pair module size not used */
  returnValue = esl_getKeyPairModuleSizeSignSHA256(workSpace, &keyPairModuleSize);

  if (ESL_ERC_NO_ERROR == returnValue)
  {
    /* Get underlying workspace pointer */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte)) workSpace;
    *wsHash = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash)) (wsRAW + ESL_WS_RSA_PSS_SV_WS_HASH(keyPairModuleSize));
  }

  return returnValue;
}

/***********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  esl_initSignRSASHA256_PSS
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_initSignRSASHA256_PSS(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
                                                        eslt_Length keyPairModuleSize, eslt_pRomByte keyPairModule, eslt_Length privateKeyExponentSize, eslt_pRomByte privateKeyExponent)
{
  eslt_ErrorCode returnValue;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader) wsHeader;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash) wsHash;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig_prim) wsPRIM;

  returnValue = ESL_ERC_NO_ERROR;

  if (!workSpace)
  {
    returnValue = ESL_ERC_PARAMETER_INVALID;
  }
  else
  {
    /* Get underlying work space pointers */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte)) workSpace;
    wsHeader = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHeader)) wsRAW;
    wsHash = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash)) (wsRAW + ESL_WS_RSA_PSS_SV_WS_HASH(keyPairModuleSize));
    wsPRIM = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig_prim)) (wsRAW + ESL_WS_RSA_PSS_SV_WS_RSA_PRIM(keyPairModuleSize));

    if (wsHeader->size < (ESL_SIZEOF_WS_RSA_PSS_SV_OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)))
    {
      returnValue = ESL_ERC_WS_TOO_SMALL;
    }
    else
    {
      wsHash->header.size = (eslt_Length) (sizeof(eslt_WorkSpaceHash) - sizeof(eslt_WorkSpaceHeader));
      wsHash->header.watchdog = wsHeader->watchdog;

      ESL_RSA_PSS_INIT_HASH_FCT(wsHash);

      wsPRIM->header.size = (eslt_Length) (wsHeader->size - (ESL_SIZEOF_WS_RSA_PSS_SV_OVERHEAD(keyPairModuleSize) + sizeof(eslt_WorkSpaceHeader)));
      wsPRIM->header.watchdog = wsHeader->watchdog;
      returnValue = esl_initSignRSA_prim(wsPRIM, keyPairModuleSize, keyPairModule, privateKeyExponentSize, privateKeyExponent);
    }

    if (ESL_ERC_NO_ERROR == returnValue)
    {
      wsHeader->status = (ESL_WST_ALGO_RSA | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);
      /* Store key pair module size / message buffer length in work space */
      wsRAW[ESL_WS_RSA_PSS_SV_BUFFERLENGTH] = (eslt_Byte) (keyPairModuleSize >> 8u);
      wsRAW[ESL_WS_RSA_PSS_SV_BUFFERLENGTH + 1u] = (eslt_Byte) (keyPairModuleSize);
    }
  }

  return returnValue;
}

/***********************************************************************************************************************
 *  esl_updateSignRSASHA256_PSS
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_updateSignRSASHA256_PSS(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace, eslt_Length inputSize, VSECPRIM_P2CONST_PARA(eslt_Byte) input)
{
  eslt_ErrorCode returnValue;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash) wsHash;

  returnValue = esl_getWorkspaceHashSignSHA256(workSpace, &wsHash);

  if (ESL_ERC_NO_ERROR == returnValue)
  {
    /* 9.1.1, Step 2 */
    if (actOK != ESL_RSA_PSS_UPDATE_HASH_FCT(wsHash, inputSize, input))
    {
      returnValue = ESL_ERC_SHA256_TOTAL_LENGTH_OVERFLOW;
    }
  }

  return returnValue;
}

/***********************************************************************************************************************
 *  esl_finalizeHashSignRSASHA256_PSS
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeHashSignRSASHA256_PSS(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) messageDigest)
{
  eslt_ErrorCode returnValue;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash) wsHash;

  returnValue = esl_getWorkspaceHashSignSHA256(workSpace, &wsHash);

  if (ESL_ERC_NO_ERROR == returnValue)
  {
    /* 9.1.1, Step 2 */
    ESL_RSA_PSS_FINALIZE_HASH_FCT(wsHash, messageDigest);
  }

  return returnValue;
}

/***********************************************************************************************************************
 *  esl_finalizeSigSignRSASHA256_PSS
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSigSignRSASHA256_PSS(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
                                                               VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
                                                               eslt_Length saltSize, VSECPRIM_P2CONST_PARA(eslt_Byte) salt,
                                                               VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte) signature)
{
  eslt_ErrorCode returnValue;
  eslt_Length keyPairModuleSize;
  eslt_Size32 emBits;
  eslt_Length emLength;
  eslt_Length psLength;
  eslt_Length dbLength = 0;
  VSECPRIM_P2VAR_PARA(eslt_Byte) wsRAW;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig_prim) wsPRIM;
  VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash) wsHash;
  VSECPRIM_P2VAR_PARA(eslt_Byte) encodedMessage;
  VSECPRIM_P2VAR_PARA(eslt_Byte) encodedHash;
  VSECPRIM_P2VAR_PARA(eslt_Byte) dbSalt;
  VSECPRIM_P2VAR_PARA(eslt_Byte) db;

  returnValue = esl_getKeyPairModuleSizeSignSHA256(workSpace, &keyPairModuleSize);

  if (ESL_ERC_NO_ERROR == returnValue)
  {
    /* Get underlying workspace pointers */
    wsRAW = (VSECPRIM_P2VAR_PARA(eslt_Byte)) workSpace;
    wsPRIM = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAsig_prim)) (wsRAW + ESL_WS_RSA_PSS_SV_WS_RSA_PRIM(keyPairModuleSize));
    /* General memory layout in workspace:
       | EM                               | DB                 | Temp | -    |
       | maskedDB           | H    | 0xBC | PS   | 0x01 | salt |      | -    | */
    encodedMessage = wsRAW + ESL_WS_RSA_PSS_SV_BUFFER;
    db = wsRAW + ESL_WS_RSA_PSS_SV_DB_BUFFER(keyPairModuleSize);
    wsHash = (VSECPRIM_P2VAR_PARA(eslt_WorkSpaceHash)) (wsRAW + ESL_WS_RSA_PSS_SV_WS_HASH(keyPairModuleSize));

    emBits = actRSAPrivateKeyGetBitLength(wsPRIM->wsRSA) - 1u;
    emLength = (eslt_Length) ((emBits + 7u) / 8u);

    if (*signatureSize < emLength)
    {
      returnValue = ESL_ERC_PARAMETER_INVALID;
    }
    else
    {
      *signatureSize = emLength;
    }

    /* 8.1.1, Step 1: EMSA-PSS encoding */
    if (ESL_ERC_NO_ERROR == returnValue)
    {
      /* Length of maskedDB / DB */
      dbLength = emLength - ESL_RSA_PSS_SIZEOF_DIGEST - 1u;
      /* Points to H in EM */
      encodedHash = &encodedMessage[dbLength];

      /* 9.1, Note 4: Typical salt length is length of the output of the hash function */
      if ((eslt_Length)-1 == saltSize)
      {
        saltSize = ESL_RSA_PSS_SIZEOF_DIGEST;
      }

      /* 9.1.1, Step 3: Salt and 0x01 have to fit into DB */
      if ((saltSize + 1u) > dbLength)
      {
        returnValue = ESL_ERC_PARAMETER_INVALID;
      }
      else
      {
        /* Calculate length of PS in DB */
        psLength = dbLength - saltSize - 1u;
        /* Points to salt in DB
           May be behind actual DB, when saltSize == 0 (but still inside of workspace) */
        dbSalt = &db[psLength + 1u];

        /* 9.1.1, Step 8: Prepare constant parts of DB
           Memory contents:
           | EM                               | DB                 | Temp | -    |
           | -                  | -    | -    | PS   | 0x01 | -    | -    | -    | */
        actMemset(db, 0x00u, psLength); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
        db[psLength] = 0x01u;

        /* 9.1.1, Steps 4, 8: Add salt to DB
           Memory contents:
           | EM                               | DB                 | Temp | -    |
           | -                  | -    | -    | PS   | 0x01 | salt | -    | -    | */
        if (saltSize > 0u)
        {
          /* Any salt provided by caller? */
          if (0u == salt)
          {
            /* No salt provided, generate random value */
            returnValue = esl_getBytesRNG(saltSize, dbSalt);
          }
          else
          {
            /* Use provided salt instead */
            actMemcpy(dbSalt, salt, saltSize); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
          }
        }

        if (ESL_ERC_NO_ERROR == returnValue)
        {
          /* 9.1.1, Steps 5-6: Calculate salted hash H from given message digest
          Memory contents:
          | EM                               | DB                 | Temp | -    |
          | -                  | H    | -    | PS   | 0x01 | salt | -    | -    | */
          returnValue = esl_calcSaltedHashRSASHA256_PSS(wsHash, saltSize, dbSalt, messageDigest, encodedHash);
        }
      }

      if (ESL_ERC_NO_ERROR == returnValue)
      {
        /* 9.1.1, Step 9: Generate dbMask using mask generation function (MGF)
        Memory contents:
        | EM                               | DB                 | Temp | -    |
        | dbMask             | H    | -    | PS   | 0x01 | salt | hash | -    | */
        returnValue = esl_generateMaskMGF1RSASHA256_PSS(wsHash, &db[dbLength], ESL_RSA_PSS_SIZEOF_DIGEST, encodedHash, dbLength, encodedMessage);
      }
    }

    if (ESL_ERC_NO_ERROR == returnValue)
    {
      /* 9.1.1, Steps 10, 12: Apply mask to DB using XOR to create maskedDB
         Memory contents:
         | EM                               | DB                 | Temp | -    |
         | maskedDb           | H    | -    | PS   | 0x01 | salt | -    | -    | */
      actXOR(encodedMessage, db, dbLength); /* PRQA S 0315 */ /* MD_VSECPRIM_P2VOID_CAST */
      /* 9.1.1, Step 11: Set leftmost 8emLen -emBits bits of the leftmost octet in maskedDB to zero */
      encodedMessage[0u] &= zeroMasks[emBits & 0x07u];
      /* 9.1.1, Step 12: Set constant "trailer field" in EM (also see 9.1, Note 1)
         Memory contents:
         | EM                               | DB                 | Temp | -    |
         | maskedDb           | H    | 0xBC | PS   | 0x01 | salt | -    | -    | */
      encodedMessage[emLength - 1u] = ESL_RSA_PSS_TRAILER;

      /* 8.1.1, Step 2: RSA signature */
      returnValue = esl_signRSA_prim(wsPRIM, keyPairModuleSize, encodedMessage, signatureSize, signature);
    }
  }

  return returnValue;
}

/***********************************************************************************************************************
 *  esl_finalizeSigSignRSASHA256_PSS_NoSalt
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSigSignRSASHA256_PSS_NoSalt(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
                                                                      VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
                                                                      VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte) signature)
{
  return esl_finalizeSigSignRSASHA256_PSS(workSpace, messageDigest, 0u, 0u, signatureSize, signature);
}

/***********************************************************************************************************************
 *  esl_finalizeSigSignRSASHA256_PSS_DigestLengthSalt
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSigSignRSASHA256_PSS_DigestLengthSalt(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
                                                                                VSECPRIM_P2CONST_PARA(eslt_Byte) messageDigest,
                                                                                VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte) signature)
{
  return esl_finalizeSigSignRSASHA256_PSS(workSpace, messageDigest, ESL_RSA_PSS_SIZEOF_DIGEST, 0u, signatureSize, signature);
}

/***********************************************************************************************************************
 *  esl_finalizeSignRSASHA256_PSS
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSASHA256_PSS(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
                                                            eslt_Length saltSize, VSECPRIM_P2CONST_PARA(eslt_Byte) salt,
                                                            VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte) signature)
{
  eslt_ErrorCode returnValue;
  eslt_Byte hash[ESL_RSA_PSS_SIZEOF_DIGEST];

  returnValue = esl_finalizeHashSignRSASHA256_PSS(workSpace, hash);
  if (ESL_ERC_NO_ERROR == returnValue)
  {
    returnValue = esl_finalizeSigSignRSASHA256_PSS(workSpace, hash, saltSize, salt, signatureSize, signature);
  }

  return returnValue;
}

/***********************************************************************************************************************
 *  esl_finalizeSignRSASHA256_PSS_NoSalt
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSASHA256_PSS_NoSalt(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
                                                                   VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte) signature)
{
  return esl_finalizeSignRSASHA256_PSS(workSpace, 0u, 0u, signatureSize, signature);
}

/***********************************************************************************************************************
 *  esl_finalizeSignRSASHA256_PSS_DigestLengthSalt
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_finalizeSignRSASHA256_PSS_DigestLengthSalt(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceRSAPSSsig) workSpace,
                                                                             VSECPRIM_P2VAR_PARA(eslt_Length) signatureSize, VSECPRIM_P2VAR_PARA(eslt_Byte) signature)
{
  return esl_finalizeSignRSASHA256_PSS(workSpace, ESL_RSA_PSS_SIZEOF_DIGEST, 0u, signatureSize, signature);
}

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_RSA_PSS_SHA2_256_ENABLED == STD_ON) */
/********************************************************************************************************************** 
 *  END OF FILE: ESLib_RSA_PSS_Sig_SHA256.c 
 *********************************************************************************************************************/
