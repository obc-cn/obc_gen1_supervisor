 /**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 1999 - 2016 cv cryptovision GmbH.                                                All rights reserved.
 *
 *  For modifications by Vector Informatik GmbH:
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is protected under intellectual property laws and proprietary to cv cryptovision GmbH
 *                and/or Vector Informatik GmbH.
 *                No right or license is granted save as expressly set out in the applicable license conditions.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
 /**        \file  ESLib_KGEcP.c
 *        \brief  EC-DH generate secret / key exchange implementation.
 *
 *      \details Currently the actClib version is used. 
 *               This file is part of the embedded systems library cvActLib/ES
 *********************************************************************************************************************/

 /**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 * 
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to module's header file.
 *********************************************************************************************************************/

#define ESLIB_KGECP_SOURCE

#include "ESLib.h"
#include "ESLib_types.h"

/* actCLib includes */
#include "actIECKey.h"

#if (VSECPRIM_ECCENABLED == STD_ON) /* COV_VSECPRIM_NO_SAFE_CONFIG XF */

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */


# define VSECPRIM_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 ** Global Functions
 ***************************************************************************/

/****************************************************************************
 * esl_initGenerateDSAEcP_prim
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_initGenerateKeyEcP_prim(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEcP) workSpace, VSECPRIM_P2ROMCONST_PARA(eslt_EccDomain) domain,
                                                          VSECPRIM_P2ROMCONST_PARA(eslt_EccDomainExt) domainExt, VSECPRIM_P2ROMCONST_PARA(eslt_EccSpeedUpExt) speedUpExt)
{
  /* Need speed up extensions */
  if (!speedUpExt)
  {
    return ESL_ERC_PARAMETER_INVALID;
  }

  /* Init actCLib EC key generation */
  {
    actRETURNCODE result = actECInitGenerateKeyPair(domain, domainExt, speedUpExt, workSpace->wsEcP, (int)(workSpace->header.size));
    if (result != actOK)
    {
      if (result == actEXCEPTION_NULL)
      {
        return ESL_ERC_PARAMETER_INVALID;
      }
      else if (result == actEXCEPTION_MEMORY)
      {
        return ESL_ERC_WS_TOO_SMALL;
      }
      else if (result == actEXCEPTION_DOMAIN)
      {
        return ESL_ERC_ECC_DOMAIN_INVALID;
      }
      else if (result == actEXCEPTION_DOMAIN_EXT)
      {
        return ESL_ERC_ECC_DOMAINEXT_INVALID;
      }
      else if (result == actEXCEPTION_SPEEDUP_EXT)
      {
        return ESL_ERC_ECC_SPEEDUPEXT_INVALID;
      }
    }
  }

  /* Set workSpace state */
  workSpace->header.status = (ESL_WST_ALGO_ECP | ESL_WST_M_RUNNING | ESL_WST_M_CRITICAL);

  return ESL_ERC_NO_ERROR;
}

/****************************************************************************
 * esl_generateKeyEcP_prim
 ***************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
VSECPRIM_FUNC(eslt_ErrorCode) esl_generateKeyEcP_prim(VSECPRIM_P2VAR_PARA(eslt_WorkSpaceEcP) workSpace, VSECPRIM_P2VAR_PARA(eslt_Byte) privateKey,
                                                      VSECPRIM_P2VAR_PARA(eslt_Byte) publicKey_x, VSECPRIM_P2VAR_PARA(eslt_Byte) publicKey_y)
{
  /* Check workSpace */
  if ((workSpace->header.status & ESL_WST_M_ALGO) != ESL_WST_ALGO_ECP)
  {
    return ESL_ERC_WS_STATE_INVALID;
  }
  if (!(workSpace->header.status & ESL_WST_M_RUNNING))
  {
    return ESL_ERC_WS_STATE_INVALID;
  }

  /* Call actCLib EC key pair generation */
  {
    actRETURNCODE result = actECGenerateKeyPair(1, privateKey, publicKey_x, publicKey_y, workSpace->wsEcP);
    if (result == actEXCEPTION_NULL)
    {
      return ESL_ERC_PARAMETER_INVALID;
    }
    else if (result == actEXCEPTION_UNKNOWN)
    {
      return ESL_ERC_ECC_INTERNAL_ERROR;
    }
    else if (result >= ESL_ERT_ERROR)  /* workaround for random callback function esl_getBytesRNG */
    {
      return ((eslt_ErrorCode) result);
    }
  }

  return ESL_ERC_NO_ERROR;
}

# define VSECPRIM_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* (VSECPRIM_ECCENABLED == STD_ON) */
/**********************************************************************************************************************
 *  END OF FILE: ESLib_KGEcP.c
 *********************************************************************************************************************/
