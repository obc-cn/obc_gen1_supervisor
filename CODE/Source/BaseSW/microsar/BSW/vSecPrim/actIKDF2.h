/****************************************************************************
 ** Main author: Ubr                     Creation date: 03/18/05
 ** Author: mns                          JustDate: 03/18/05
 ** Workfile: actIKDF2.h                 Revision: 553
 ** NoKeywords:
 **
 **
 ** \copyright(cv cryptovision GmbH, 1999 - 2005                         )
 **
 ** \version(1.0 (beta)                                                 )
 ***************************************************************************/

/****************************************************************************
 **
 **     Part of the actCLibrary
 **
 **     Layer: User Module - Interface
 **
 ***************************************************************************/

/****************************************************************************
 **
 ** This file contains: The interface for HMAC-SHA-1 based key derivation
 **                     function KDF2. (Implementation reference is PKCS#5)
 **
 ** constants:
 **   actKDF2_DEFAULT_ITERATION_COUNT
 **
 ** types:
 **   actKDF2STRUCT
 **
 ** macros:
 **
 ** functions:
 **   actKDF2Init
 **   actKDF2Derive
 **
 ***************************************************************************/


#ifndef ACTIKDF2_H
# define ACTIKDF2_H


# include "actITypes.h"
# include "actIHashMAC.h"

/* PRQA S 5209 EOF */ /* MD_VSECPRIM_USE_OF_BASIC_TYPES */

/****************************************************************************
 ** Types and constants
 ***************************************************************************/

/* Workspace structure for KDF2 */
typedef struct
{
   int iteration_count;
   actHASHMACSTRUCT hmac;
   /* 2 internal buffers for HMAC results: */
   actU8 U_buf[actHASH_SIZE_SHA];
   actU8 F_buf[actHASH_SIZE_SHA];
} actKDF2STRUCT;



/****************************************************************************
 ** Function Prototypes
 ***************************************************************************/

# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
extern "C" {
# endif

#define VSECPRIM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/****************************************************************************
 **
 ** FUNCTION:
 ** actRETURNCODE actKDF2Init(actKDF2STRUCT* info, int iteration_count)
 **
 **  This function initializes the KDF2 algorithm.
 **
 ** input:
 ** - info:             pointer to context structure
 ** - iteration_count:  number of iterations used for the key derivation
 **
 ** output:
 ** - info:       initialized context structure
 ** - returns:    actEXCEPTION_LENGTH   iteration_count is negative
 **               actOK                 else
 **
 ** assumes:
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actKDF2Init(VSECPRIM_P2VAR_PARA(actKDF2STRUCT) info, int iteration_count);

/****************************************************************************
 **
 ** FUNCTION:
 **  actRETURNCODE actKDF2Derive(actKDF2STRUCT* info,
 **                              const actU8* passwd,
 **                              int passwd_length,
 **                              const actU8* salt,
 **                              int salt_length,
 **                              actU8* key,
 **                              int key_length)
 **
 **  This function derives a key of the desired length from the input
 **  password and the (optional) salt.
 **
 ** input:
 ** - info:          pointer to context structure
 ** - passwd:        pointer to the password
 ** - passwd_length: length of passwd in bytes
 ** - salt:          pointer to the optional salt (if NULL, no salt is used)
 ** - salt_length:   length of salt in bytes
 ** - key:           pointer to the output key buffer
 ** - key_length:    length of output key buffer in bytes
 **
 ** output:
 ** - info:       actualized context structure
 ** - key:        the derived key
 ** - returns:    actEXCEPTION_NULL     passwd or key is NULL
 **               actEXCEPTION_LENGTH   an input length value is negative
 **               actOK                 else
 **
 ** assumes:
 ** - actKDF2Init() is called once before calling this function
 **
 ** uses:
 ***************************************************************************/
VSECPRIM_FUNC(actRETURNCODE) actKDF2Derive(
   VSECPRIM_P2VAR_PARA(actKDF2STRUCT) info,
   VSECPRIM_P2CONST_PARA(actU8) passwd, int passwd_length,
   VSECPRIM_P2CONST_PARA(actU8) salt, int salt_length,
   VSECPRIM_P2VAR_PARA(actU8) key, int key_length);

#define VSECPRIM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifdef __cplusplus                                       /* COV_VSECPRIM_CPLUSPLUS XF */
} /* extern "C" */
# endif

#endif /* ACTIKDF2_H */

