/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 * 
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  Tls_Types.h
 *      Project:  OnBoardCharger
 *       Module:  Transport Layer Security
 *    Generator:  SysSercice_Tls.dll
 *
 *  Description:  types header
 *  
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Tls module. >> Tls.h
 *********************************************************************************************************************/

#if !defined (TLS_TYPES_H)  /* PRQA S 0883 */ /* MD_TLS_19.15_0883 */
# define TLS_TYPES_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "Tls_Cfg.h"
# include "TcpIp_Types.h"

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
typedef uint8     Tls_StateType;   /* Tls needs state also without DET */
typedef uint8     Tls_EncryptionStateType;
typedef uint8     Tls_TConnIdType;

typedef struct
{
  uint16 ContOffs;
  uint16 ContLen;
} Tls_Cert_EleInfo;

#define TLS_CSM_INVALID_JOB_ID 0xFFFFFFFFu
#define TLS_CSM_INVALID_KEY_ID 0xFFFFFFFFu

# define TLS_CERT_INT_HASH_LEN                                        8u

# define TLS_HASH_SHA1_LEN                                            20u
# define TLS_HASH_SHA256_LEN                                          32u
# define TLS_HASH_MD5_LEN                                             16u
# define TLS_HASH_NULL_LEN                                            0u

typedef struct
{
  uint32           CertFoundElements; /* chapters successfully found in the certificate */
  uint8            Hash8Sha1Issuer[TLS_CERT_INT_HASH_LEN];   /* sha1 hash of issuer (first 8 byte) */
  uint8            Hash8Sha1Subject[TLS_CERT_INT_HASH_LEN];  /* sha1 hash of subject (first 8 byte) */
  uint8            HashSha1Cert[TLS_HASH_SHA1_LEN];          /* sha1 hash of the whole certificate */
  Tls_Cert_EleInfo CertCh1;           /* chapter 1 */
  Tls_Cert_EleInfo SignedCert;        /* chapter 1.1 */
# if (TLS_SUPPORT_CRL == STD_ON)
  Tls_Cert_EleInfo SerialNumber;      /* chapter 1.1.2 */
# endif
  Tls_Cert_EleInfo MySignAlgId;       /* chapter 1.1.3 */
  Tls_Cert_EleInfo Issuer;            /* chapter 1.1.4 */
  Tls_Cert_EleInfo ValidNotBefore;    /* chapter 1.1.5.1 */
  Tls_Cert_EleInfo ValidNotAfter;     /* chapter 1.1.5.2 */
  Tls_Cert_EleInfo Subject;           /* chapter 1.1.6 */
  Tls_Cert_EleInfo KeyAlgId;          /* chapter 1.1.7.1.1 */
  Tls_Cert_EleInfo KeyMain;           /* chapter 1.1.7.2_1.1  -- RSA modulus, ECC key */
  Tls_Cert_EleInfo KeyExp;            /* chapter 1.1.7.2_1.2  -- RSA exponent */
  Tls_Cert_EleInfo EccCurveId;        /* chapter 1.1.7.1.2    -- RSA exponent */
  Tls_Cert_EleInfo CertSignAlgId;     /* chapter 1.2 */
  Tls_Cert_EleInfo CertSignature;     /* chapter 1.3 */
  boolean          CertWasParsed;     /* certificate has been read and parsed */
} Tls_Cert_Descr;

# if (TLS_SUPPORT_CRL == STD_ON)
typedef struct
{
  uint32           CrlFoundElements; /* chapters successfully found in the CRL */
  uint8            Hash8Sha1Issuer[TLS_CERT_INT_HASH_LEN];   /* sha1 hash of issuer (first 8 byte) */
  boolean          CrlWasParsed;      /* CRL has been read and parsed */
} Tls_Crl_Descr;

/* number of elements in this list is 'TLS_CRL_NUM_OF_CRL_CHAPTERS' */
#  define TLS_CRL_ELE_CRL_CHAPTER_1                                   0
#  define TLS_CRL_ELE_TBS_CERT_LIST                                   1
#  define TLS_CRL_ELE_MY_SIGN_ALG_ID                                  2
#  define TLS_CRL_ELE_ISSUER                                          3
#  define TLS_CRL_ELE_THIS_UPDATE                                     4
#  define TLS_CRL_ELE_NEXT_UPDATE                                     5
#  define TLS_CRL_ELE_REVOKED_CERTS                                   6
#  define TLS_CRL_ELE_EXTENSIONS                                      7
#  define TLS_CRL_ELE_CRL_SIGN_ALG_ID                                 8
#  define TLS_CRL_ELE_CRL_SIGN                                        9
# endif

# define TLS_KEY_ALG_TYPE_INVALID                                     0U
# define TLS_KEY_ALG_TYPE_RSA                                         1U
# define TLS_KEY_ALG_TYPE_ECC                                         2U

typedef struct
{
  uint16 ContOffs;
  uint16 ContLen;
} Tls_Ocsp_RespEleInfoType;

typedef struct
{
  uint32           RespFoundElements; /* chapters successfully found in the certificate */
  Tls_Ocsp_RespEleInfoType Response;              /* chapter 1 */
  Tls_Ocsp_RespEleInfoType ResponseStatus;        /* chapter 1.1 */
  Tls_Ocsp_RespEleInfoType RespTypeOid;           /* chapter 1.2.1.1 */
  Tls_Ocsp_RespEleInfoType BasicOcspResp;         /* chapter 1.2.1.2 */

  Tls_Ocsp_RespEleInfoType TbsRespDataFull;       /* chapter 1.2.1.2___1.1   tbsResponseDataFull, element including length field */
  Tls_Ocsp_RespEleInfoType TbsRespData;           /* chapter 1.2.1.2___1.1   tbsResponseData */
  Tls_Ocsp_RespEleInfoType ResponderId;           /* chapter 1.2.1.2___1.1.1   responderId */
  Tls_Ocsp_RespEleInfoType ProducedAt;            /* chapter 1.2.1.2___1.1.2   producedAt */
  Tls_Ocsp_RespEleInfoType Responses;             /* chapter 1.2.1.2___1.1.3   responses */
  Tls_Ocsp_RespEleInfoType Response1;             /* chapter 1.2.1.2___1.1.3.1   response1 */
  Tls_Ocsp_RespEleInfoType SignAlgId;             /* chapter 1.2.1.2___1.2   signatureAlgorithm */
  Tls_Ocsp_RespEleInfoType Signature;             /* chapter 1.2.1.2___1.3   signature */
  Tls_Ocsp_RespEleInfoType Certs;                 /* chapter 1.2.1.2___1.4   certs */  /* OPTIONAL */
  Tls_Ocsp_RespEleInfoType Cert1;                 /* chapter 1.2.1.2___1.4.1   cert1 */  /* OPTIONAL */

  Tls_Ocsp_RespEleInfoType R1CertId;              /* chapter 1.2.1.2___1.1.3.1...1   r1_certID */
  Tls_Ocsp_RespEleInfoType R1Cid_HashAlg;         /* chapter 1.2.1.2___1.1.3.1...1.1   r1_hashAlgorithm */
  Tls_Ocsp_RespEleInfoType R1Cid_IssuerNameHash;  /* chapter 1.2.1.2___1.1.3.1...1.2   r1_issuerNameHash */
  Tls_Ocsp_RespEleInfoType R1Cid_IssuerKeyHash;   /* chapter 1.2.1.2___1.1.3.1...1.3   r1_issuerKeyHash */
  Tls_Ocsp_RespEleInfoType R1Cid_serialNumber;    /* chapter 1.2.1.2___1.1.3.1...1.4   r1_serialNumber */
  Tls_Ocsp_RespEleInfoType R1CertStatus;          /* chapter 1.2.1.2___1.1.3.1...2   r1_certStatus */
  Tls_Ocsp_RespEleInfoType R1RevocationTime;      /* chapter 1.2.1.2___1.1.3.1...2.1   r1_revocationTime */  /* only for status 'revoked' */
  Tls_Ocsp_RespEleInfoType R1ThisUpdate;          /* chapter 1.2.1.2___1.1.3.1...3   r1_thisUpdate */
  Tls_Ocsp_RespEleInfoType R1NextUpdate;          /* chapter 1.2.1.2___1.1.3.1...4   r1_nextUpdate */  /* OPTIONAL */

  uint8            ResponderIdType;      /* 0xA1 = 'by name' */
  uint8            R1CertStatusValue;    /* good, revoked, unknown */
} Tls_Ocsp_RespDescrType;
/* status parsing elements (used in StatusDesc): */
# define TLS_STATUS_ELE_RESPONSE                                      0x00000001u
# define TLS_STATUS_ELE_RESPONSE_STATUS                               0x00000002u
# define TLS_STATUS_ELE_RESP_TYPE_OID                                 0x00000004u
# define TLS_STATUS_ELE_BASIC_OCSP_RESP                               0x00000008u

# define TLS_STATUS_ELE_TBS_RESP_DATA                                 0x00000010u
# define TLS_STATUS_ELE_RESPONDER_ID                                  0x00000020u
# define TLS_STATUS_ELE_PRODUCED_AT                                   0x00000040u
# define TLS_STATUS_ELE_RESPONSES                                     0x00000080u
# define TLS_STATUS_ELE_RESPONSE_1                                    0x00000100u
# define TLS_STATUS_ELE_SIGN_ALG_ID                                   0x00000200u
# define TLS_STATUS_ELE_SIGNATURE                                     0x00000400u
# define TLS_STATUS_ELE_CERTS                                         0x00000800u

# define TLS_STATUS_ELE_CERT1                                         0x00200000u

# define TLS_STATUS_ELE_R1_CERT_ID                                    0x00001000u
# define TLS_STATUS_ELE_R1_CID_HASH_ALG                               0x00002000u
# define TLS_STATUS_ELE_R1_CID_ISSU_NAME_HASH                         0x00004000u
# define TLS_STATUS_ELE_R1_CID_ISSU_KEY_HASH                          0x00008000u
# define TLS_STATUS_ELE_R1_CID_SERIAL_NUMBER                          0x00010000u
# define TLS_STATUS_ELE_R1_CERT_STATUS                                0x00020000u
# define TLS_STATUS_ELE_R1_REVOC_TIME                                 0x00040000u
# define TLS_STATUS_ELE_R1_THIS_UPDATE                                0x00080000u
# define TLS_STATUS_ELE_R1_NEXT_UPDATE                                0x00100000u

/* status values used for OCSP status messages */
# define TLS_STATUS_VALUE_GOOD                                        0x00u
# define TLS_STATUS_VALUE_REVOKED                                     0x01u
# define TLS_STATUS_VALUE_UNKNOWN                                     0x02u

# define Tls_SockAddrType                                             TcpIp_SockAddrType
# define Tls_SockAddrIn6Type                                          TcpIp_SockAddrInet6Type
# define Tls_PbufType                                                 TcpIp_PbufType
# define Tls_SocketIdType                                             TcpIp_SocketIdType

typedef struct
{
  uint32 Date;  /* YYYYMMDD */
  uint32 Time;  /* DayligthSavingTime & --HHMMSS (UTC time) */
} Tls_TimeStampType;

typedef uint8      Tls_ConfigType;

typedef uint8      Tls_HeartbeatModeType;
# define TLS_HB_MODE_NONE                                             0u  /* heartbeat disabled */
# define TLS_HB_PEER_ALLOWED_TO_SEND                                  1u  /* peer_allowed_to_send */
# define TLS_HB_PEER_NOT_ALLOWED_TO_SEND                              2u  /* peer_not_allowed_to_send */

typedef uint8      Tls_HeartbeatStatusType;
# define TLS_HEARTBEAT_STATUS_DISABLED                                0u  /* usage of heartbeat is disabled */
# define TLS_HEARTBEAT_STATUS_COM_ACTIV                               1u  /* normal communication is active, idle timeout has not been reached */
# define TLS_HEARTBEAT_STATUS_WAIT_FOR_RESPONSE                       2u  /* heartbeat request has been sent, wait for the heartbeat response */
# define TLS_HEARTBEAT_STATUS_RESPONSE_TIMED_OUT                      3u  /* no heartbeat response has been received in time */

typedef enum
{
  WAIT_FOR_EXCH_BUF_IDLE,  /* waiting until the TLS NvmExchBuf is available */
  WAIT_FOR_READY_TO_READ,  /* waiting until read request can be triggered (while blocked by state NVM_REQ_PENDING) */
  TRIGGER_READING,         /* trying to trigger reading (until trigger has been accepted or timed out) */
  WAIT_FOR_READ_RESULT,    /* waiting until the read job in NVM has been finished in any way  */
  READY_TO_BE_PARSED       /* cert has been read from NVM, now it can be parsed */
} Tls_SingleCertReadStatusType;  /* read status for one single cert in NVM */

typedef enum
{
  Tls_HSMHS_Init              = 0u,
  Tls_HSMHS_ClientHello       = 1u,
  Tls_HSMHS_ClientKeyExchange = 2u,
  Tls_HSMHS_ClientCert        = 3u,
  Tls_HSMHS_ClientCertVerify  = 4u,
  Tls_HSMHS_ClientFinished    = 5u,
  Tls_HSMHS_IsRxMessage       = 6u,
  Tls_HSMHS_HandshakeDone     = 7u
} Tls_HandshakeMessageHashStateType;

typedef struct
{
  uint32 KeyId; /* KeyId of the CsmKey */
  uint32 KeyElementId; /* KeyElementId of the crypto driver CryptoKeyElement */
  uint32 KeyLen;
} Tls_CsmKeyIdType;

typedef struct
{
  uint32 JobIdSha1; /* Used for root certificate hashes */
} Tls_CsmWorkspaceGlobalType;

typedef struct
{
  uint32 KeyElementIdOutput; /* Intern: PRF HMAC job output, used for redirection feature */
  uint32 KeyElementIdActive; /* Intern: PRF temp value during calculation */
  uint32 KeyElementIdPasive; /* Intern: PRF temp value during calculation */
  uint32 JobIdHMac_Generate_Private; /* Required seperately due to CSM redirect feature */
  uint32 JobIdHMac_Generate_Public; /* Required seperately due to CSM redirect feature */
  uint32 KeyIdSecret; /* Intern: Secret that is used to calculate the PRF. Linked to PRF HMAC job */
  uint32 KeyIdCalculationValuesPrivate;  /* Intern: PRF HMAC job input, used for redirection feature */
  uint32 KeyIdCalculationValuesPublic;  /* Intern: PRF HMAC job input, used for redirection feature */
  uint32 KeyIdResultPrivate; /* Extern: Result of the PRF, used for the key block calculation. */
  uint32 KeyIdResultPublic; /* Extern: Result of the PRF, used in case result shall be extracted for transmission. */
} Tls_CsmWorkspacePrfType;

typedef struct
{
  uint32 KeyElementIdCommon; /* Dummy value used to store/read/copy keys */
  uint32 JobIdHashSha256_ToClientFinished; /* Used for hash calculation during handshake */
  uint32 JobIdHashSha256_ToServerFinished; /* Used for hash calculation during handshake */
  uint32 JobIdCertVerifyEccNistP256R1Sha1;
  uint32 JobIdCertVerifyEccNistP256R1Sha256;
  uint32 KeyIdCertVerifyPubKey;
  uint32 JobIdRandomNumber; /* Used to generate random numbers */
  uint32 KeyIdRandomNumber; /* Stores the seed and algorithm that is used to generate random numbers */
  uint32 KeyIdPreMasterSecretInfo; /* Stores the private key, public key and pre-master secret */
  uint32 KeyIdMasterSecret; /* Stores the master secret */
  uint32 KeyIdTmpBuffer; /* Is used to set partial data to a key */
  Tls_CsmWorkspacePrfType Prf; /* Job and keys for PRF calculations */
} Tls_CsmWorkspaceHandshakeType;

typedef struct
{
  uint32 JobIdHMacSha1_Generate;
  uint32 JobIdHMacSha1_Verify;
  uint32 JobIdHMacSha256_Generate;
  uint32 JobIdHMacSha256_Verify;
  uint32 KeyIdHMacShaX_Generate;
  uint32 KeyIdHMacShaX_Verify;
  uint32 JobIdAes128Cbc_Decrypt;
  uint32 JobIdAes128Cbc_Encrypt;
  uint32 KeyIdAes128Cbc_Decrypt;
  uint32 KeyIdAes128Cbc_Encrypt;
} Tls_CsmWorkspaceDataExchangeType;

typedef struct
{
  uint16 SupportedCipherSuites[11u]; /* TLS implementation supports 10 chiper suites the 11th entry will always be 0x0000 */
  uint16  SupportedCipherSuitesCnt;
} TlsCsmWorkspaceAllowedCipherSuitesType;

typedef struct
{
  TlsCsmWorkspaceAllowedCipherSuitesType AllowedCipherSuites;
  Tls_CsmWorkspaceHandshakeType Handshake;
  Tls_CsmWorkspaceDataExchangeType DataExchange;
} Tls_CsmWorkspaceConnectionSpecificType;

/* CSM Workspace */
typedef struct 
{
  Tls_CsmWorkspaceGlobalType Global;
  Tls_CsmWorkspaceConnectionSpecificType Connection[TLS_CFG_NUM_CONNECTIONS];
} Tls_CsmWorkspaceType;

typedef struct
{
  Std_ReturnType CsmCallResult;
  Tls_HandshakeMessageHashStateType MessageHashState;
} Tls_CsmConnectionRelatedStatesType;

typedef struct
{
  Tls_CsmConnectionRelatedStatesType  Connection[TLS_CFG_NUM_CONNECTIONS];
} Tls_CsmRelatedStatesType;

typedef struct
{
  P2CONST(uint8, AUTOMATIC, TLS_APPL_DATA) DataPtr;
  uint32 DataLen;
} Tls_DataBufferType;

typedef struct
{
  P2CONST(uint8, AUTOMATIC, TLS_APPL_DATA) KeyPtr; /* Pointer to the public or private key */
  uint32 KeyLen; /* Length of the key */
  P2CONST(uint8, AUTOMATIC, TLS_APPL_DATA) SignaturePtr; /* Pointer to the BER encoded signature */
  uint32 SignatureLen; /* Length of the signature */
  Tls_DataBufferType Data1; /* First data chunk that shall be verified */
  Tls_DataBufferType Data2; /* Optional: First data chunk that shall be verified. Set Data2.DataPtr = NULL_PTR to skip. */
  Tls_DataBufferType Data3; /* Used if Data2 is present; Skipped if Data2 is skipped */
  uint8 HashAlgorithm; /* Set: TLS_HASH_ALGORITHM_SHA1 or TLS_HASH_ALGORITHM_SHA256 */
} Tls_ValidateSingnatureType;

typedef enum {
  TLS_E_NOT_SET             = 0x00u, /* No error indication is set */
  TLS_E_SERVER_CERT_EXPIRED = 0x01u, /* The server's certificate has expired */
  TLS_E_OCSP_RESP_NOT_GOOD  = 0x02u, /* The server's OCSP response contains a certificate which is not 'good' */
  TLS_E_UNKNOWN_CA          = 0x03u, /* The issuing CA of the received server certificate is unknown */
  TLS_E_SERVER_TERMINATED   = 0x04u  /* The server terminated */
}Tls_ErrorIndicationType;

/***********************************************************************************************************************
 *  Tls_ErrorIndicationCallbackType
 **********************************************************************************************************************/
/*! \brief        Prototype definition for error indication callouts
 *  \param[in]     SockHnd            Handle of the socket the error event occured on
 *  \param[in]     ErrorId            ID of the error event
 *  \param[in]     ErrorOccurred      TRUE if there was an error, FALSE if there was no error and everything went fine
 **********************************************************************************************************************/
typedef void (*Tls_ErrorIndicationCallbackType)(Tls_SocketIdType SockHnd, Tls_ErrorIndicationType ErrorId, boolean ErrorOccurred);

#endif  /* TLS_TYPES_H */
/**********************************************************************************************************************
 *  END OF FILE: Tls_Types.h
 *********************************************************************************************************************/
