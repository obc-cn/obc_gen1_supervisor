/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc_ExiRx_ISO.c
 *        \brief  Smart Charging Communication Source Code File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *********************************************************************************************************************/

#define SCC_EXIRX_ISO_SOURCE

/**********************************************************************************************************************
 LOCAL MISRA / PCLINT JUSTIFICATION
 **********************************************************************************************************************/
/* PRQA S 0777 EOF */ /* MD_MSR_Rule5.1 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Scc_Cfg.h"

#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )

#include "Scc_Exi.h"
#include "Scc.h"
#include "Scc_Priv.h"
#include "Scc_Lcfg.h"
#include "Scc_Interface_Cfg.h"

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
#  include "Csm.h"
#endif /* SCC_ENABLE_PNC_CHARGING */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
#include "Det.h"
#endif /* SCC_DEV_ERROR_DETECT */
#include "Exi.h"
#include "IpBase.h"
#include "NvM.h"
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
#include "XmlSecurity.h"
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined (SCC_LOCAL)
# define SCC_LOCAL static
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3453 MACROS_FUNCTION_LIKE */ /* MD_MSR_FctLikeMacro */

/* PRQA L:MACROS_FUNCTION_LIKE */
/**********************************************************************************************************************
 *  LOCAL / GLOBAL DATA
 *********************************************************************************************************************/
/* other variables */
#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
SCC_LOCAL P2VAR(Exi_ISO_V2G_MessageType, AUTOMATIC, SCC_VAR_NOINIT) Scc_ExiRx_ISO_MsgPtr;

#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_<ISO-Response>
 *********************************************************************************************************************/
 /*! \brief         Get all parameters of ISO response message
 *  \details        Validates parameter and reports them to the application.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_SessionSetupRes(void);
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_ServiceDiscoveryRes(void);
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_ServiceDetailRes(void);
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_PaymentServiceSelectionRes(void);
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_PaymentDetailsRes(void);
#endif /* SCC_ENABLE_PNC_CHARGING */
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_AuthorizationRes(void);
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_ChargeParameterDiscoveryRes(void);
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_PowerDeliveryRes(void);
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_MeteringReceiptRes(void);
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_CertificateUpdateRes(void);
#endif /* SCC_ENABLE_PNC_CHARGING */
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_SessionStopRes(void);

/* AC */

/* DC */
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_CableCheckRes(void);
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_PreChargeRes(void);
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_CurrentDemandRes(void);
/* see pattern Scc_ExiRx_ISO_<ISO-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_WeldingDetectionRes(void);
#endif /* SCC_CHARGING_DC */


#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/**********************************************************************************************************************
*  Scc_ExiRx_ISO_ReadEVSEStatusDC
*********************************************************************************************************************/
/*! \brief          reports the EVSEStatus to the application.
 *  \details        reports EVSEIsolationStatus, EVSEStatusCode, EVSENotification and NotificationMaxDelay to the application.
 *  \param[in]      EVSEStatusPtr             Pointer to the EVSEstatus
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
*********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiRx_ISO_ReadEVSEStatusDC(P2CONST(Exi_ISO_DC_EVSEStatusType, AUTOMATIC, SCC_VAR_NOINIT) EVSEStatusPtr);
#endif /* SCC_CHARGING_DC */

/* XmlSecurity */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
*  Scc_ExiRx_ISO_XmlSecDereference
*********************************************************************************************************************/
/*! \brief          check the validity of Contract Certificate
 *  \details        check if the currently installed certificate is expired or will expire soon.
 *  \param[out]     ExiStructPtr          returns the pointer to the signed element.
 *  \param[out]     ExiRootElementId      root element of the signed element.
 *  \param[out]     ExiNamespaceId        Name space if of the signed element.
 *  \param[in]      URIPtr                Pointer to the URI of the signed element
 *  \param[in]      URILength             Length of the URI
 *  \return         OK                    Signed element was found and.
 *  \return         NotOK                 Signed element was not found.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
*********************************************************************************************************************/
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_XmlSecDereference(P2VAR(uint8*, AUTOMATIC, SCC_VAR_NOINIT) ExiStructPtr,
  P2VAR(Exi_RootElementIdType, AUTOMATIC, SCC_VAR_NOINIT) ExiRootElementId, P2VAR(Exi_NamespaceIdType, AUTOMATIC, SCC_VAR_NOINIT) ExiNamespaceId,
  P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) URIPtr, uint16 URILength);
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Init
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_ExiRx_ISO_Init(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the message pointer */
  Scc_ExiRx_ISO_MsgPtr = (P2VAR(Exi_ISO_V2G_MessageType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_StructBuf[0];  /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */

}

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_InitXmlSecurityWorkspace
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */

FUNC(void, SCC_CODE) Scc_ExiRx_ISO_InitXmlSecurityWorkspace(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize the XmlSecurity workspace */
  if ( E_OK != XmlSecurity_InitSigValWorkspace(
   &Scc_Exi_XmlSecSigValWs,
    Scc_ExiRx_ISO_XmlSecDereference, Scc_XmlSecGetPublicKey) )
  {
    /* since this should not happen, issue a DET */
#if(SCC_DEV_ERROR_REPORT == STD_ON)
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_CERTIFICATE_INSTALL_RES, SCC_DET_XML_SEC);
#endif /* SCC_DEV_ERROR_REPORT */
  }
}
#endif /* SCC_ENABLE_PNC_CHARGING */


/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_DecodeMessage
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiRx_ISO_DecodeMessage(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize the Exi workspace */
  if ( (Std_ReturnType)E_OK == Scc_Exi_InitDecodingWorkspace() )
  {
    /* set the decode information */
    Scc_Exi_DecWs.OutputData.SchemaSetId = EXI_SCHEMA_SET_ISO_TYPE;

    /* #20 Exi-decode the V2G response */
    if ( (Std_ReturnType)E_OK == Exi_Decode(&Scc_Exi_DecWs) )
    {
#if ( defined SCC_DEM_EXI )
      /* report status to DEM */
      Scc_DemReportErrorStatusPassed(SCC_DEM_EXI);
#endif /* SCC_DEM_EXI */

      /* #30 check the SessionID starting from ServiceDiscovery */
      if ( Scc_MsgTrig_SessionSetup < Scc_MsgTrig )
      {
        /* compare the length of the stored and received SessionID */
        if ( Scc_ExiRx_ISO_MsgPtr->Header->SessionID->Length == Scc_SessionIDNvm[0] )
        {
          /* if the length is the same, check if the content is the same, too */
          if ( IPBASE_CMP_EQUAL != IpBase_StrCmpLen(&Scc_ExiRx_ISO_MsgPtr->Header->SessionID->Buffer[0],
            &Scc_SessionIDNvm[1], Scc_SessionIDNvm[0] ))
          {
            /* SessionID is not equal, report the error */
            Scc_ReportError(Scc_StackError_InvalidRxParameter);
          }
          else
          {
            retVal = E_OK;
          }
        }
        /* length is not the same */
        else
        {
          /* report the error */
          Scc_ReportError(Scc_StackError_InvalidRxParameter);
        }
        /* Notification */
        Scc_Set_ISO_Notification(Scc_ExiRx_ISO_MsgPtr->Header->Notification, Scc_ExiRx_ISO_MsgPtr->Header->NotificationFlag);
      }
      else
      {
        retVal = E_OK;
      }

      if ( retVal == E_OK )
      {
        retVal = E_NOT_OK;

        /* #40 handle an incoming response message */
        switch ( Scc_MsgTrig )
        {
          /* handle an incoming session setup response message */
        case Scc_MsgTrig_SessionSetup:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_SessionSetupRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_SessionSetup_OK);
            /* the session has been successfully connected */
            Scc_State = Scc_State_Connected;
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming service discovery response message */
        case Scc_MsgTrig_ServiceDiscovery:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_ServiceDiscoveryRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_ServiceDiscovery_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming service detail response message */
        case Scc_MsgTrig_ServiceDetail:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_ServiceDetailRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_ServiceDetail_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming service payment selection response message */
        case Scc_MsgTrig_PaymentServiceSelection:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_PaymentServiceSelectionRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_PaymentServiceSelection_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

      #if ( SCC_ENABLE_PNC_CHARGING == STD_ON )

      #if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
          /* handle an incoming certificate installation response message */
        case Scc_MsgTrig_CertificateInstallation:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_CertificateInstallationRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_CertificateInstallation_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;
      #endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

      #if ( SCC_ENABLE_CERTIFICATE_UPDATE == STD_ON )
          /* handle an incoming certificate update response message */
        case Scc_MsgTrig_CertificateUpdate:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_CertificateUpdateRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_CertificateUpdate_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;
      #endif /* SCC_ENABLE_CERTIFICATE_UPDATE */

          /* handle an incoming payment details response message */
        case Scc_MsgTrig_PaymentDetails:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_PaymentDetailsRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_PaymentDetails_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

      #endif /* SCC_ENABLE_PNC_CHARGING */

          /* handle an incoming contract authentication response message */
        case Scc_MsgTrig_Authorization:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_AuthorizationRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_Authorization_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming charge parameter discovery response message */
        case Scc_MsgTrig_ChargeParameterDiscovery:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_ChargeParameterDiscoveryRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_ChargeParameterDiscovery_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming power delivery response message */
        case Scc_MsgTrig_PowerDelivery:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_PowerDeliveryRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_PowerDelivery_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

      #if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
          /* handle an incoming metering receipt response message */
        case Scc_MsgTrig_MeteringReceipt:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_MeteringReceiptRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_MeteringReceipt_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;
      #endif /* SCC_ENABLE_PNC_CHARGING */

          /* handle an incoming session stop response message */
        case Scc_MsgTrig_SessionStop:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_SessionStopRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_SessionStop_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }

          /* the session is now disconnected */
          Scc_State = Scc_State_Disconnected;
          break;


      #if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )

          /* handle an incoming cable check response message */
        case Scc_MsgTrig_CableCheck:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_CableCheckRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_CableCheck_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming pre charge response message */
        case Scc_MsgTrig_PreCharge:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_PreChargeRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_PreCharge_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming current demand response message */
        case Scc_MsgTrig_CurrentDemand:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_CurrentDemandRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_CurrentDemand_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming welding detection response message */
        case Scc_MsgTrig_WeldingDetection:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_WeldingDetectionRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_WeldingDetection_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

      #endif /* SCC_CHARGING_DC */

        default:
          /* invalid state */
          break;
        }
      }
    }
    else
    {
      /* report the error */
      Scc_ReportError(Scc_StackError_Exi);
  #if ( defined SCC_DEM_EXI )
      /* report status to DEM */
      Scc_DemReportErrorStatusFailed(SCC_DEM_EXI);
  #endif /* SCC_DEM_EXI */
      retVal = E_NOT_OK;
    }
  }

  return retVal;
} /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_SessionSetupRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_SessionSetupRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Exi_ISO_responseCodeType responseCode;
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_SessionSetupResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_SessionSetupResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_SESSION_SETUP_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* ResponseCode */
      responseCode = BodyPtr->ResponseCode;

      /* Handle SessionID */
      /* check for a positive response code */
      if ( EXI_ISO_RESPONSE_CODE_TYPE_OK == responseCode )
      {
        /* check if the lengths do not match */
        if ( Scc_ExiRx_ISO_MsgPtr->Header->SessionID->Length != Scc_SessionIDNvm[0] )
        {
          responseCode = EXI_ISO_RESPONSE_CODE_TYPE_OK_NEW_SESSION_ESTABLISHED;
        }
        /* if the lengths match */
        else
        {
          /* check if the SessionIDs match */
          if ( IPBASE_CMP_EQUAL == IpBase_StrCmpLen(&Scc_SessionIDNvm[1],
            &Scc_ExiRx_ISO_MsgPtr->Header->SessionID->Buffer[0], Scc_SessionIDNvm[0]) )
          {
            responseCode = EXI_ISO_RESPONSE_CODE_TYPE_OK_OLD_SESSION_JOINED;
          }
          /* if they do not match */
          else
          {
            responseCode = EXI_ISO_RESPONSE_CODE_TYPE_OK_NEW_SESSION_ESTABLISHED;
          }
        }
      }
      /* check if the EVSE has opened a new session */
      if ( EXI_ISO_RESPONSE_CODE_TYPE_OK_NEW_SESSION_ESTABLISHED == responseCode )
      {
        /* get the new length of the SessionID */
        Scc_SessionIDNvm[0] = (uint8) Scc_ExiRx_ISO_MsgPtr->Header->SessionID->Length;
        /* copy the SessionID */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &Scc_SessionIDNvm[1],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &Scc_ExiRx_ISO_MsgPtr->Header->SessionID->Buffer[0], Scc_SessionIDNvm[0]);
        /* write the SessionID to the NvM */
        (void) NvM_SetRamBlockStatus((NvM_BlockIdType)SCC_SESSION_ID_NVM_BLOCK, TRUE);
      }

      /* SessionID */
      Scc_Set_ISO_SessionID(Scc_ExiRx_ISO_MsgPtr->Header->SessionID);

      /* EVSEID */
      Scc_Set_ISO_EVSEID(BodyPtr->EVSEID);

      /* EVSETimeStamp */
      Scc_Set_ISO_EVSETimeStamp(BodyPtr->EVSETimeStamp, BodyPtr->EVSETimeStampFlag);

      /* ResponseCode */
      Scc_Set_ISO_ResponseCode(responseCode);

      retVal = E_OK;
    }
  }

  return retVal;
} /* PRQA S 6080, 6050 */ /* MD_MSR_STMIF, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_ServiceDiscoveryRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_ServiceDiscoveryRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ServiceDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ServiceDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_SERVICE_DISCOVERY_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* PaymentOptionList */
      Scc_Set_ISO_PaymentOptionList(BodyPtr->PaymentOptionList);

      /* ChargeService */
      Scc_Set_ISO_ChargeService(BodyPtr->ChargeService);

      /* ServiceList */
      Scc_Set_ISO_ServiceList(BodyPtr->ServiceList, BodyPtr->ServiceListFlag);

      /* ResponseCode */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }
  return retVal;
} /* PRQA S 6050 */ /* MD_MSR_STCAL */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_ServiceDetailRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_ServiceDetailRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ServiceDetailResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ServiceDetailResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_SERVICE_DETAIL_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }

  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* ServiceID */
      Scc_Set_ISO_ServiceIDTx(BodyPtr->ServiceID);

      /* ServiceParameterList */
      Scc_Set_ISO_ServiceParameterList(BodyPtr->ServiceParameterList, BodyPtr->ServiceParameterListFlag);

      /* ResponseCode */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_PaymentServiceSelectionRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_PaymentServiceSelectionRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_PaymentServiceSelectionResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_PaymentServiceSelectionResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_PAYMENT_SERVICE_SELECTION_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* ResponseCode */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )

#if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_CertificateInstallationRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiRx_ISO_CertificateInstallationRes(void) /* PRQA S 2889 */ /* MD_Scc_15.5 */
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = SCC_DET_NO_ERROR;
  Std_ReturnType RetVal = E_NOT_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  /* PRQA S 0316 3 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_CertificateInstallationResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_CertificateInstallationResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;
    P2VAR(Exi_ISO_certificateType, AUTOMATIC, SCC_APPL_DATA) LastCertInChainPtr;

  uint32                 CurrentTime = 0;
  uint8                  Counter;
#if ( defined SCC_ENABLE_CPS_CHECK ) && ( SCC_ENABLE_CPS_CHECK == STD_ON )
                                       /* 'C' ,  'P' ,  'S' */
  uint8                  CPSValue[3] = { 0x43u, 0x50u, 0x53u };
#endif /* SCC_ENABLE_CPS_CHECK */
#if ( defined SCC_ENABLE_EMAID_VALIDATION ) && ( SCC_ENABLE_EMAID_VALIDATION == STD_ON )
  Exi_ISO_EMAIDType      lEMAID;
  Exi_ISO_EMAIDType      rEMAID;
#endif /* SCC_ENABLE_EMAID_VALIDATION */
  XmlSecurity_ReturnType XmlSecRetVal;


  /* ----- Development Error Checks ------------------------------------- */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of runtime parameters. */
  if ( ( BodyPtr->SAProvisioningCertificateChain->SubCertificatesFlag != 0u )
    && ( BodyPtr->SAProvisioningCertificateChain->SubCertificates->Certificate == NULL_PTR ) )
  {
    errorId = SCC_DET_INV_POINTER;
  }
  else
#endif /* SCC_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */

    /* #20 Check if the message is not the one that was expected. */
    if ( EXI_ISO_CERTIFICATE_INSTALLATION_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
    {
      Scc_ReportError(Scc_StackError_InvalidRxMessage);
  #if ( defined SCC_DEM_UNEXPECTED_MSG )
      /* report status to DEM */
      Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
  #endif /* SCC_DEM_UNEXPECTED_MSG */
      return (Std_ReturnType)E_NOT_OK;
    }
  #if ( defined SCC_DEM_UNEXPECTED_MSG )
    else
    {
      /* report status to DEM */
      Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
    }
  #endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #30 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* #40 Set the parameter from the response. */

  #if ( defined SCC_ENABLE_CPS_CHECK ) && ( SCC_ENABLE_CPS_CHECK == STD_ON )
    /* #50 check if the CPS content is set in the DomainComponent */
    if ( (Std_ReturnType)E_OK != Scc_SearchDomainComponentForValue(&BodyPtr->SAProvisioningCertificateChain->Certificate->Buffer[0],
      BodyPtr->SAProvisioningCertificateChain->Certificate->Length, &CPSValue[0], sizeof(CPSValue)) )
    {
      /* CPS was not set as content */
      Scc_ReportError(Scc_StackError_IpBase);
      return (Std_ReturnType)E_NOT_OK;
    }
  #endif /* SCC_ENABLE_CPS_CHECK */

    /* #60 Verify SAProvisioningCertificateChain against root certificate */
    /* add the root cert as the last element of the chain */
    /* check if there are no sub certificates */
    if ( 0u == BodyPtr->SAProvisioningCertificateChain->SubCertificatesFlag )
    {
      /* set the last cert pointer */
      LastCertInChainPtr = BodyPtr->SAProvisioningCertificateChain->Certificate;
    }
    /* if there are sub certificates */
    else
    {
      /* check if the SubCertificate is a null pointer, should not happen */
      /* connect the chain from the certificate to its sub certificates */
      BodyPtr->SAProvisioningCertificateChain->Certificate->NextCertificatePtr =
        BodyPtr->SAProvisioningCertificateChain->SubCertificates->Certificate;
      /* set the last cert pointer to the first sub certificate */
      LastCertInChainPtr = BodyPtr->SAProvisioningCertificateChain->SubCertificates->Certificate;
      /* step to the last sub certificate in the chain */
      while ( NULL_PTR != LastCertInChainPtr->NextCertificatePtr )
      {
        /* set the last cert pointer to this last sub certificate in the chain */
        LastCertInChainPtr = LastCertInChainPtr->NextCertificatePtr;
      }
    }

    /* get the current time */
    Scc_Get_ISO_PnC_CurrentTime(&CurrentTime);

    /* step through all root certificates to find a matching one */
    for ( Counter = 0; Counter < Scc_CertsNvm.RootCertNvmCnt; Counter++ )
    {
      /* check if this root certificate is set */
      if ( Scc_NvMBlockReadState_Processed == Scc_CertsWs.RootCertsReadStates[Counter] )
      {
        /* set the root certificate */
        LastCertInChainPtr->NextCertificatePtr = Scc_CertsNvm.RootCerts[Counter].RootCert;
        /* if this root cert was successfully validated against the chain, break the loop */
        if ( (Std_ReturnType)E_OK == Scc_ValidateCertChain(BodyPtr->SAProvisioningCertificateChain->Certificate, CurrentTime) )
        {
          break;
        }
      }

      /* if this root certificate was not the correct one, try the next */
      LastCertInChainPtr->NextCertificatePtr = (P2VAR(Exi_ISO_certificateType, AUTOMATIC, SCC_VAR_NOINIT))NULL_PTR;
    }

    /* check if a matching root certificate was found */
    if ( NULL_PTR != LastCertInChainPtr->NextCertificatePtr )
    {
      uint16 PubKeyIdx;
      uint16 PubKeyLen;
      /* get the index of the public key */
      if ( (Std_ReturnType)E_OK != Scc_GetIndexOfPublicKey(&BodyPtr->SAProvisioningCertificateChain->Certificate->Buffer[0],
        BodyPtr->SAProvisioningCertificateChain->Certificate->Length, &PubKeyIdx, &PubKeyLen) )
      {
        return (Std_ReturnType)E_NOT_OK;
      }
      /* check the length of the public key */
      if ( SCC_PUB_KEY_LEN < PubKeyLen )
      {
        /* the public key is too big for the buffer */
        Scc_ReportError(Scc_StackError_InvalidRxParameter);
        return (Std_ReturnType)E_NOT_OK;
      }
      else
      {
        /* the public key fits into the buffer */
        Scc_CertsWs.SaCertPubKeyLen = PubKeyLen;
      }
      /* copy the public key */ /* PRQA S 0310, 3305, 0315 2*/ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
      IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.SaCertPubKey[0],
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->SAProvisioningCertificateChain->Certificate->Buffer[PubKeyIdx], Scc_CertsWs.SaCertPubKeyLen);
      /* set the flag which defines if the public key buffer contains a valid value */
      Scc_SaPubKeyRcvd = TRUE;
  #if ( defined SCC_DEM_CRYPTO )
      /* report status to DEM */
      Scc_DemReportErrorStatusPassed(SCC_DEM_CRYPTO);
  #endif
    }
    /* no matching root certificate was found */
    else
    {
      /* report error to application */
      Scc_ReportError(Scc_StackError_Crypto);
  #if ( defined SCC_DEM_CRYPTO )
      /* report status to DEM */
      Scc_DemReportErrorStatusFailed(SCC_DEM_CRYPTO);
  #endif
      return (Std_ReturnType)E_NOT_OK;
    }

    /* #70 Verify XmlSecurity signature */
    /* check if the signature exists */
    if ( 1U == Scc_ExiRx_ISO_MsgPtr->Header->SignatureFlag )
    {
      /* Set value to all mandatory references in Signed Info */
      Scc_Exi_RefInSigInfo.Flags = 0u;
      Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureCertChain = TRUE;
      Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureEncryptedPrivateKey = TRUE;
      Scc_Exi_RefInSigInfo.Flag.RISI_DHpublickey = TRUE;
      Scc_Exi_RefInSigInfo.Flag.RISI_eMAID = TRUE;


      /* validate the signatures */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
      XmlSecRetVal = XmlSecurity_ValidateExiSignature(&Scc_Exi_XmlSecSigValWs,
        Scc_ExiRx_ISO_MsgPtr->Header->Signature, &Scc_Exi_TempBuf[0], SCC_EXI_TEMP_BUF_LEN,
        Scc_ECDSAVerifyJobId, Scc_ECDSAVerifyKeyId, Scc_SHA256JobId);

      /* Check if all mandatory references were checked in the function Scc_ExiRx_ISO_XmlSecDereference() */
      if ( Scc_Exi_RefInSigInfo.Flags != 0u)
      {
        /* report the error to the application */
        Scc_ReportError(Scc_StackError_XmlSecurityMissingReference);
  #if ( defined SCC_DEM_XML_SEC )
        /* EVSE provided an invalid signature */
        Scc_DemReportErrorStatusFailed(SCC_DEM_XML_SEC);
  #endif /* SCC_DEM_XML_SEC */
        return (Std_ReturnType)E_NOT_OK;
      }

      /* Check if the signature is valid */
      if ( XmlSec_RetVal_OK != XmlSecRetVal )
      {
        /* report the error to the application */
        Scc_ReportError(Scc_StackError_XmlSecurity);
  #if ( defined SCC_DEM_XML_SEC )
        /* EVSE provided an invalid signature */
        Scc_DemReportErrorStatusFailed(SCC_DEM_XML_SEC);
  #endif /* SCC_DEM_XML_SEC */
        return (Std_ReturnType)E_NOT_OK;
      }
  #if ( defined SCC_DEM_XML_SEC )
      else
      {
        /* signature was valid */
        Scc_DemReportErrorStatusPassed(SCC_DEM_XML_SEC);
      }
  #endif /* SCC_DEM_XML_SEC */
    }
    /* the CertificateInstallation has no signature */
    else
    {
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }
    /* #80 Check EMAID from new ContractCertificate against EMAID from message. */
  #if ( defined SCC_ENABLE_EMAID_VALIDATION ) && ( SCC_ENABLE_EMAID_VALIDATION == STD_ON )
    /* set the maximum length of the EMAIDs */
    lEMAID.Length = sizeof(lEMAID.Buffer);
    rEMAID.Length = sizeof(rEMAID.Buffer);
    /* extract the EMAID out of the certificate */
    if ( (Std_ReturnType)E_OK != Scc_GetCertDistinguishedNameObject(
      &BodyPtr->ContractSignatureCertChain->Certificate->Buffer[0],
      BodyPtr->ContractSignatureCertChain->Certificate->Length,
      &lEMAID.Buffer[0], &lEMAID.Length, Scc_BEROID_Subject_EMAID) )
    {
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }
    /* remove the dashes from the EMAID that is sent as a parameter */
    if ( E_OK != Scc_ExiRx_ISO_RemoveDashesFromEMAID(&BodyPtr->eMAID->Buffer[0], BodyPtr->eMAID->Length,
                                                     &rEMAID.Buffer[0], &rEMAID.Length) )
    {
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }
    /* check if the length of the EMAIDs match */
    if ( lEMAID.Length == rEMAID.Length )
    {
      /* check if the EMAID is the same */
      if ( (uint8)IPBASE_CMP_EQUAL != IpBase_StrCmpLen(&lEMAID.Buffer[0], &rEMAID.Buffer[0], lEMAID.Length) )
      {
        /* EMAIDs do not match */
        Scc_ReportError(Scc_StackError_InvalidRxParameter);
        return (Std_ReturnType)E_NOT_OK;
      }
    }
    else
    { /* PRQA S 2880 */ /* MD_Scc_2742_2880_2995 */
      /* EMAIDs do not match */
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }
  #endif /* SCC_ENABLE_EMAID_VALIDATION */

    /* set the maximum length of the EMAID */
    Scc_CertsWs.eMAID->Length = sizeof(Scc_CertsWs.eMAID->Buffer);
    /* extract the EMAID out of the certificate */
    if ( (Std_ReturnType)E_OK != Scc_GetCertDistinguishedNameObject(
      &BodyPtr->ContractSignatureCertChain->Certificate->Buffer[0],
      BodyPtr->ContractSignatureCertChain->Certificate->Length,
      &Scc_CertsWs.eMAID->Buffer[0], &Scc_CertsWs.eMAID->Length, Scc_BEROID_Subject_EMAID) )
    {
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* #90 get the certificate and its private key */

    /* ContractSignatureChain -> Certificate */
    /* get the length of the contract certificate */
    Scc_CertsWs.ContrCert->Length = BodyPtr->ContractSignatureCertChain->Certificate->Length;
    /* copy the contract certificate */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305,MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy  */
    IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrCert->Buffer[0],
      (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->ContractSignatureCertChain->Certificate->Buffer[0],
      Scc_CertsWs.ContrCert->Length);



    /* check if the encrypted private key has not the correct length -> length of parameter minus 16 bytes IV */
    if ( SCC_PRIV_KEY_LEN != (uint16)(BodyPtr->ContractSignatureEncryptedPrivateKey->Length - SCC_IV_LEN) )
    {
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }
    /* Copy provisioning private key to KeyExchange KeyType */
    else if ( E_OK != Csm_KeyElementCopy(Scc_ECDSASignProvCertKeyId,
                                         CRYPTO_KE_SIGNATURE_KEY,
                                         Scc_ECDHExchangeKeyId,
                                         CRYPTO_KE_KEYEXCHANGE_PRIVKEY) )
    {
      Scc_ReportError(Scc_StackError_Crypto);
      return (Std_ReturnType)E_NOT_OK;
    }
    /* Decrypt the PrivateKey of the new ContractCertificate */
    else if (E_OK != Scc_DecryptPrivateKey(Scc_ECDHExchangeKeyId, /* PRQA S 2004  */ /* MD_Scc_2004 */
                                           Scc_ECDSASignCertKeyIds[Scc_CertsWs.ChosenContrCertChainIdx],
                                           &BodyPtr->DHpublickey->Buffer[1],
                                           &BodyPtr->ContractSignatureEncryptedPrivateKey->Buffer[SCC_IV_LEN],
                                           &BodyPtr->ContractSignatureEncryptedPrivateKey->Buffer[0]))
    {
      Scc_ReportError(Scc_StackError_Crypto);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* validate if the new private key belongs to the new contract certificate and check if it failed */
    if ((Std_ReturnType)E_OK != Scc_ValidateKeyPair(&Scc_CertsWs.ContrCert->Buffer[0],
        Scc_CertsWs.ContrCert->Length,
        Scc_ECDSASignCertJobIds[Scc_CertsWs.ChosenContrCertChainIdx]))
    {
      /* report the error */
      Scc_ReportError(Scc_StackError_InvalidKeyPair);
      return (Std_ReturnType)E_NOT_OK;
    }


    /* #100 write the certificates to the NvM */

    /* set the read state */
    Scc_CertsWs.ContrCertReadState = Scc_NvMBlockReadState_Processed;
    /* write the ContractCertificate to the NvM */ /* PRQA S 0314 3 */ /* MD_Scc_Nvm_Generic */
    if ( (Std_ReturnType)E_OK != NvM_WriteBlock(
      (NvM_BlockIdType)Scc_GetNvMBlockIDContrCert(Scc_CertsWs.ChosenContrCertChainIdx),
      (P2VAR(void,AUTOMATIC,SCC_APPL_DATA))&Scc_CertsWs.ContrCert->Buffer[0]) )
    {
      Scc_ReportError(Scc_StackError_NvM);
  #if ( defined SCC_DEM_NVM_READ_CONTR_CERT_FAIL )
      Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_CONTR_CERT_FAIL);
  #endif /* SCC_DEM_NVM_READ_CONTR_CERT_FAIL */
      return (Std_ReturnType)E_NOT_OK;
    }
  #if ( defined SCC_DEM_NVM_READ_CONTR_CERT_FAIL )
    else
    {
      Scc_DemReportErrorStatusPassed(SCC_DEM_NVM_READ_CONTR_CERT_FAIL);
    }
  #endif /* SCC_DEM_NVM_READ_CONTR_CERT_FAIL */

    /* adjust the contract certificate chain size */
    Scc_CertsWs.ContrCertChainSize = 0;
    Scc_CertsWs.ContrSubCertsProcessedFlags = 0;

    /* #110 get the sub certificates and write them to the NvM */

    /* ContractSignatureChain -> SubCertificates */
    if ( 1u == BodyPtr->ContractSignatureCertChain->SubCertificatesFlag )
    {
      P2VAR(Exi_ISO_certificateType, AUTOMATIC, SCC_VAR_NOINIT) SubCertificatePtr =
        BodyPtr->ContractSignatureCertChain->SubCertificates->Certificate;

      do
      {
        /* get the length of the sub certificates */
        Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize].Length = SubCertificatePtr->Length;
        /* copy the sub certificates */ /* PRQA S 0310,3305,0315 3 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))
          &Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize].Buffer[0],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&SubCertificatePtr->Buffer[0], SubCertificatePtr->Length);
        /* write the ContractSubCertificates to the NvM */ /* PRQA S 0314 3 */ /* MD_Scc_Nvm_Generic */
        if ( (Std_ReturnType)E_OK != NvM_WriteBlock(
          (NvM_BlockIdType)Scc_GetNvMBlockIDContrSubCerts(Scc_CertsWs.ChosenContrCertChainIdx,Scc_CertsWs.ContrCertChainSize),
          (P2VAR(void,AUTOMATIC,SCC_APPL_DATA))&Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize].Buffer[0]) )
        {
          Scc_ReportError(Scc_StackError_NvM);
          return (Std_ReturnType)E_NOT_OK;
        }

        if ( 0 < Scc_CertsWs.ContrCertChainSize )
        {
          /* link the sub certificates */
          Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize-1].NextCertificatePtr =
            &Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize];
        }

        /* set the new nvm read state */
        Scc_CertsWs.ContrSubCertsReadStates[Scc_CertsWs.ContrCertChainSize] = Scc_NvMBlockReadState_Processed;
        /* update the status flag */
        Scc_CertsWs.ContrSubCertsProcessedFlags |= (uint32)( (uint32)0x01 << (uint8)Scc_CertsWs.ContrCertChainSize );

        /* adjust the contract certificate chain size */
        Scc_CertsWs.ContrCertChainSize++;

        /* adjust the certificate pointer */
        SubCertificatePtr = SubCertificatePtr->NextCertificatePtr;
      }
      while (   ( NULL_PTR != SubCertificatePtr )
             && ( (sint8)Scc_CertsWs.ContrSubCertCnt > Scc_CertsWs.ContrCertChainSize ));
    }

    /* #120 write the new chain size to the NvM */

    /* set the read state of the contract certificate chain size*/
    Scc_CertsWs.ContrCertChainSizeReadState = Scc_NvMBlockReadState_Processed;
    /* write the ContractCertificateChainSize and check if it was not successful */ /* PRQA S 0314 3 */ /* MD_Scc_Nvm_Generic */
    if ( (Std_ReturnType)E_OK != NvM_WriteBlock(
      (NvM_BlockIdType)Scc_GetNvMBlockIDContrCertChainSize(Scc_CertsWs.ChosenContrCertChainIdx),
      (P2VAR(void, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrCertChainSize) )
    {
      Scc_ReportError(Scc_StackError_NvM);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* --- handle the rest of the parameters --- */

    /* ResponseCode */
    Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    RetVal = E_OK;
  }

  /* ----- Development Error Report --------------------------------------- */
#if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #130 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_CERTIFICATE_INSTALL_RES, errorId);
  }
#else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* SCC_DEV_ERROR_REPORT */

  return RetVal;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */
#endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

#if ( SCC_ENABLE_CERTIFICATE_UPDATE == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_CertificateUpdateRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_CertificateUpdateRes(void) /* PRQA S 2889 */ /* MD_Scc_15.5 */
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = SCC_DET_NO_ERROR;
  Std_ReturnType RetVal = E_NOT_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_CertificateUpdateResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_CertificateUpdateResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;
  P2VAR(Exi_ISO_certificateType, AUTOMATIC, SCC_APPL_DATA) LastCertInChainPtr;

  uint32                 CurrentTime = 0;
  uint8                  Counter;
#if ( defined SCC_ENABLE_CPS_CHECK ) && ( SCC_ENABLE_CPS_CHECK == STD_ON )
                                       /* 'C' ,  'P' ,  'S' */
  uint8                  CPSValue[3] = { 0x43u, 0x50u, 0x53u };
#endif /* SCC_ENABLE_CPS_CHECK */
#if ( defined SCC_ENABLE_EMAID_VALIDATION ) && ( SCC_ENABLE_EMAID_VALIDATION == STD_ON )
  Exi_ISO_EMAIDType      lEMAID;
  Exi_ISO_EMAIDType      rEMAID;
#endif /* SCC_ENABLE_EMAID_VALIDATION */
  XmlSecurity_ReturnType XmlSecRetVal;

  /* ----- Development Error Checks ------------------------------------- */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of runtime parameters. */
  if ((BodyPtr->SAProvisioningCertificateChain->SubCertificatesFlag != 0u)
    && (BodyPtr->SAProvisioningCertificateChain->SubCertificates->Certificate == NULL_PTR))
  {
    errorId = SCC_DET_INV_POINTER;
  }
  else
#endif /* SCC_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Check if the message is not the one that was expected. */
    if ( EXI_ISO_CERTIFICATE_UPDATE_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
    {
      Scc_ReportError(Scc_StackError_InvalidRxMessage);
  #if ( defined SCC_DEM_UNEXPECTED_MSG )
      /* report status to DEM */
      Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
  #endif /* SCC_DEM_UNEXPECTED_MSG */
      return (Std_ReturnType)E_NOT_OK;
    }
  #if ( defined SCC_DEM_UNEXPECTED_MSG )
    else
    {
      /* report status to DEM */
      Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
    }
  #endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #30 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* RetryCounter shall not be ignored according to V2G2-928 */
      Scc_Set_ISO_PnC_RetryCounter(BodyPtr->RetryCounter, BodyPtr->RetryCounterFlag);
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* #40 Report the parameter from the response to the application. */

  #if ( defined SCC_ENABLE_CPS_CHECK ) && ( SCC_ENABLE_CPS_CHECK == STD_ON )
    /* check if the CPS content is set in the DomainComponent */
    if ( (Std_ReturnType)E_OK != Scc_SearchDomainComponentForValue(&BodyPtr->SAProvisioningCertificateChain->Certificate->Buffer[0],
      BodyPtr->SAProvisioningCertificateChain->Certificate->Length, &CPSValue[0], sizeof(CPSValue)) )
    {
      /* CPS was not set as content */
      Scc_ReportError(Scc_StackError_IpBase);
      return (Std_ReturnType)E_NOT_OK;
    }
  #endif /* SCC_ENABLE_CPS_CHECK */

    /* add the root cert as the last element of the chain */
    /* check if there are no sub certificates */
    if ( 0u == BodyPtr->SAProvisioningCertificateChain->SubCertificatesFlag )
    {
      /* set the last cert pointer */
      LastCertInChainPtr = BodyPtr->SAProvisioningCertificateChain->Certificate;
    }
    /* if there are sub certificates */
    else
    {
      /* connect the chain from the certificate to its sub certificates */
      BodyPtr->SAProvisioningCertificateChain->Certificate->NextCertificatePtr =
        BodyPtr->SAProvisioningCertificateChain->SubCertificates->Certificate;
      /* set the last cert pointer to the first sub certificate */
      LastCertInChainPtr = BodyPtr->SAProvisioningCertificateChain->SubCertificates->Certificate;
      /* step to the last sub certificate in the chain */
      while ( NULL_PTR != LastCertInChainPtr->NextCertificatePtr )
      {
        /* set the last cert pointer to this last sub certificate in the chain */
        LastCertInChainPtr = LastCertInChainPtr->NextCertificatePtr;
      }
    }

    /* get the current time */
    Scc_Get_ISO_PnC_CurrentTime(&CurrentTime);

    /* step through all root certificates to find a matching one */
    for ( Counter = 0; Counter < Scc_CertsNvm.RootCertNvmCnt; Counter++ )
    {
      /* check if this root certificate is set */
      if ( Scc_NvMBlockReadState_Processed == Scc_CertsWs.RootCertsReadStates[Counter] )
      {
        /* set the root certificate */
        LastCertInChainPtr->NextCertificatePtr = Scc_CertsNvm.RootCerts[Counter].RootCert;
        /* check if this root cert was successfully validated against the chain */
        if ( (Std_ReturnType)E_OK == Scc_ValidateCertChain(BodyPtr->SAProvisioningCertificateChain->Certificate, CurrentTime) )
        {
          /* break the loop */
          break;
        }
      }

      /* reset the next certificate pointer back to NULL_PTR */
      LastCertInChainPtr->NextCertificatePtr = (P2VAR(Exi_ISO_certificateType, AUTOMATIC, SCC_VAR_NOINIT))NULL_PTR;
    }
    /* check if a matching root certificate was found */
    if ( NULL_PTR != LastCertInChainPtr->NextCertificatePtr )
    {
      uint16 PubKeyIdx;
      uint16 PubKeyLen;
      /* try to get the index of the public key and check if it failed */
      if ( (Std_ReturnType)E_OK != Scc_GetIndexOfPublicKey(&BodyPtr->SAProvisioningCertificateChain->Certificate->Buffer[0],
        BodyPtr->SAProvisioningCertificateChain->Certificate->Length, &PubKeyIdx, &PubKeyLen) )
      {
        return (Std_ReturnType)E_NOT_OK;
      }
      /* check the length of the public key */
      if ( SCC_PUB_KEY_LEN < PubKeyLen )
      {
        /* the public key is too big for the buffer */
        Scc_ReportError(Scc_StackError_InvalidRxParameter);
        return (Std_ReturnType)E_NOT_OK;
      }
      else
      {
        /* the public key fits into the buffer */
        Scc_CertsWs.SaCertPubKeyLen = PubKeyLen;
      }
      /* copy the public key */ /* PRQA S 0310, 3305, 0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
      IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.SaCertPubKey[0],
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->SAProvisioningCertificateChain->Certificate->Buffer[PubKeyIdx],
        Scc_CertsWs.SaCertPubKeyLen);
      /* set the flag which defines if the public key buffer contains a valid value */
      Scc_SaPubKeyRcvd = TRUE;

  #if ( defined SCC_DEM_CRYPTO )
      /* report status to DEM */
      Scc_DemReportErrorStatusPassed(SCC_DEM_CRYPTO);
  #endif
    }
    /* no matching root certificate was found */
    else
    {
      /* report error to application */
      Scc_ReportError(Scc_StackError_Crypto);
  #if ( defined SCC_DEM_CRYPTO )
      /* report status to DEM */
      Scc_DemReportErrorStatusFailed(SCC_DEM_CRYPTO);
  #endif
      return (Std_ReturnType)E_NOT_OK;
    }

    /* check if the signature exists */
    if ( 1u == Scc_ExiRx_ISO_MsgPtr->Header->SignatureFlag )
    {
      /* Set value to all mandatory references in Signed Info */
      Scc_Exi_RefInSigInfo.Flags = 0u;
      Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureCertChain = TRUE;
      Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureEncryptedPrivateKey = TRUE;
      Scc_Exi_RefInSigInfo.Flag.RISI_DHpublickey = TRUE;
      Scc_Exi_RefInSigInfo.Flag.RISI_eMAID = TRUE;

      /* validate the signatures */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
      XmlSecRetVal = XmlSecurity_ValidateExiSignature(&Scc_Exi_XmlSecSigValWs,
        Scc_ExiRx_ISO_MsgPtr->Header->Signature, &Scc_Exi_TempBuf[0], SCC_EXI_TEMP_BUF_LEN,
        Scc_ECDSAVerifyJobId, Scc_ECDSAVerifyKeyId, Scc_SHA256JobId);

      /* Check if all mandatory references were checked in the function Scc_ExiRx_ISO_XmlSecDereference() */
      if ( Scc_Exi_RefInSigInfo.Flags != 0u)
      {
        /* report the error to the application */
        Scc_ReportError(Scc_StackError_XmlSecurityMissingReference);
  #if ( defined SCC_DEM_XML_SEC )
        /* EVSE provided an invalid signature */
        Scc_DemReportErrorStatusFailed(SCC_DEM_XML_SEC);
  #endif /* SCC_DEM_XML_SEC */
        return (Std_ReturnType)E_NOT_OK;
      }

      /* Check if the signature is valid */
      if ( XmlSec_RetVal_OK != XmlSecRetVal )
      {
        /* report the error to the application */
        Scc_ReportError(Scc_StackError_XmlSecurity);
  #if ( defined SCC_DEM_XML_SEC )
        /* EVSE provided an invalid signature */
        Scc_DemReportErrorStatusFailed(SCC_DEM_XML_SEC);
  #endif /* SCC_DEM_XML_SEC */
        return (Std_ReturnType)E_NOT_OK;
      }
  #if ( defined SCC_DEM_XML_SEC )
      else
      {
        /* signature was valid */
        Scc_DemReportErrorStatusPassed(SCC_DEM_XML_SEC);
      }
  #endif /* SCC_DEM_XML_SEC */
    }
    /* the CertificateUpdateRes has no signature */
    else
    {
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }

  #if ( defined SCC_ENABLE_EMAID_VALIDATION ) && ( SCC_ENABLE_EMAID_VALIDATION == STD_ON )
    /* set the maximum length of the EMAIDs */
    lEMAID.Length = sizeof(lEMAID.Buffer);
    rEMAID.Length = sizeof(rEMAID.Buffer);
    /* extract the EMAID out of the certificate */
    if ( (Std_ReturnType)E_OK != Scc_GetCertDistinguishedNameObject(
      &BodyPtr->ContractSignatureCertChain->Certificate->Buffer[0],
      BodyPtr->ContractSignatureCertChain->Certificate->Length,
      &lEMAID.Buffer[0], &lEMAID.Length, Scc_BEROID_Subject_EMAID) )
    {
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }
    /* remove the dashes from the EMAID that is sent as a parameter */
    if ( E_OK != Scc_ExiRx_ISO_RemoveDashesFromEMAID(&BodyPtr->eMAID->Buffer[0], BodyPtr->eMAID->Length,
                                                     &rEMAID.Buffer[0], &rEMAID.Length) )
    {
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }
    /* check if the length of the EMAIDs match */
    if ( lEMAID.Length == rEMAID.Length )
    {
      /* check if the EMAID is the same */
      if ( (uint8)IPBASE_CMP_EQUAL != IpBase_StrCmpLen(&lEMAID.Buffer[0], &rEMAID.Buffer[0], lEMAID.Length) )
      {
        /* EMAIDs do not match */
        Scc_ReportError(Scc_StackError_InvalidRxParameter);
        return (Std_ReturnType)E_NOT_OK;
      }
    }
    else
    { /* PRQA S 2880 */ /* MD_Scc_2742_2880_2995 */
      /* EMAIDs do not match */
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }
  #endif /* SCC_ENABLE_EMAID_VALIDATION */

    /* set the maximum length of the EMAID */
    Scc_CertsWs.eMAID->Length = sizeof(Scc_CertsWs.eMAID->Buffer);
    /* extract the EMAID out of the certificate */
    if ( (Std_ReturnType)E_OK != Scc_GetCertDistinguishedNameObject(
      &BodyPtr->ContractSignatureCertChain->Certificate->Buffer[0],
      BodyPtr->ContractSignatureCertChain->Certificate->Length,
      &Scc_CertsWs.eMAID->Buffer[0], &Scc_CertsWs.eMAID->Length, Scc_BEROID_Subject_EMAID) )
    {
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* --- get the certificate and its private key --- */

    /* ContractSignatureChain -> Certificate */
    /* get the length */
    Scc_CertsWs.ContrCert->Length = BodyPtr->ContractSignatureCertChain->Certificate->Length;
    /* copy the certificate */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305,MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
    IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrCert->Buffer[0],
      (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->ContractSignatureCertChain->Certificate->Buffer[0],
      Scc_CertsWs.ContrCert->Length);

    /* check if the encrypted private key has the correct length -> length of parameter minus 16 bytes IV */
    if ( SCC_PRIV_KEY_LEN != (uint16)(BodyPtr->ContractSignatureEncryptedPrivateKey->Length - SCC_IV_LEN) )
    {
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }
    /* Copy contract private key to KeyExchange KeyType */
    else if ( E_OK != Csm_KeyElementCopy(Scc_ECDSASignCertKeyIds[Scc_CertsWs.ChosenContrCertChainIdx],
                                         CRYPTO_KE_SIGNATURE_KEY,
                                         Scc_ECDHExchangeKeyId,
                                         CRYPTO_KE_KEYEXCHANGE_PRIVKEY) )
    {
      Scc_ReportError(Scc_StackError_Crypto);
      return (Std_ReturnType)E_NOT_OK;
    }
    /* Decrypt the PrivateKey of the new ContractCertificate */
    else if (E_OK != Scc_DecryptPrivateKey(Scc_ECDHExchangeKeyId, /* PRQA S 2004  */ /* MD_Scc_2004 */
                                           Scc_ECDSASignCertKeyIds[Scc_CertsWs.ChosenContrCertChainIdx],
                                           &BodyPtr->DHpublickey->Buffer[1],
                                           &BodyPtr->ContractSignatureEncryptedPrivateKey->Buffer[SCC_IV_LEN],
                                           &BodyPtr->ContractSignatureEncryptedPrivateKey->Buffer[0]))
    {
      Scc_ReportError(Scc_StackError_Crypto);
      return (Std_ReturnType)E_NOT_OK;
    }


    /* try to validate if the new private key belongs to the new contract certificate and check if it failed */
    if ( (Std_ReturnType)E_OK != Scc_ValidateKeyPair(&Scc_CertsWs.ContrCert->Buffer[0],
                                                     Scc_CertsWs.ContrCert->Length,
                                                     Scc_ECDSASignCertJobIds[Scc_CertsWs.ChosenContrCertChainIdx]) )
    {
      /* report the error */
      Scc_ReportError(Scc_StackError_InvalidKeyPair);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* --- write the certificates to the NvM --- */
    /* try to write the ContractCertificate to the NvM and check if an error occurred */ /* PRQA S 0314 3 */ /* MD_Scc_Nvm_Generic */
    if ( (Std_ReturnType)E_OK != NvM_WriteBlock(
      (NvM_BlockIdType)Scc_GetNvMBlockIDContrCert(Scc_CertsWs.ChosenContrCertChainIdx),
      (P2VAR(void,AUTOMATIC,SCC_APPL_DATA))&Scc_CertsWs.ContrCert->Buffer[0]) )
    {
      Scc_ReportError(Scc_StackError_NvM);
  #if ( defined SCC_DEM_NVM_READ_CONTR_CERT_FAIL )
      Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_CONTR_CERT_FAIL);
  #endif /* SCC_DEM_NVM_READ_CONTR_CERT_FAIL */
      return (Std_ReturnType)E_NOT_OK;
    }
  #if ( defined SCC_DEM_NVM_READ_CONTR_CERT_FAIL )
    else
    {
      Scc_DemReportErrorStatusPassed(SCC_DEM_NVM_READ_CONTR_CERT_FAIL);
    }
  #endif /* SCC_DEM_NVM_READ_CONTR_CERT_FAIL */
      /* adjust the contract certificate chain size */
    Scc_CertsWs.ContrCertChainSize = 0;
    Scc_CertsWs.ContrSubCertsProcessedFlags = 0;

    /* --- get the sub certificates and write them to the NvM --- */

    /* ContractSignatureChain -> SubCertificates */
    if ( 1u == BodyPtr->ContractSignatureCertChain->SubCertificatesFlag )
    {
      /* counter for the amount of sub certificates */
      uint8 AmountOfSubCertificates = 0;
      /* set the sub certificate pointer to the first in the list */
      P2VAR(Exi_ISO_certificateType, AUTOMATIC, SCC_VAR_NOINIT) SubCertificatePtr =
        BodyPtr->ContractSignatureCertChain->SubCertificates->Certificate;
      /* step through all sub certificates to check how many sub certificates there are */
      do
      {
        AmountOfSubCertificates++;
        SubCertificatePtr = SubCertificatePtr->NextCertificatePtr;
      }
      while (   ( (sint8)Scc_CertsWs.ContrSubCertCnt > Scc_CertsWs.ContrCertChainSize )
             && ( NULL_PTR != SubCertificatePtr ));

      /* reset the sub certificate pointer */
      SubCertificatePtr = BodyPtr->ContractSignatureCertChain->SubCertificates->Certificate;

      /* check if there are not enough NvM blocks for all sub certificates in the new chain */
      if ( AmountOfSubCertificates > Scc_CertsWs.ContrSubCertCnt )
      {
        Scc_ReportError(Scc_StackError_InvalidRxParameter);
        return (Std_ReturnType)E_NOT_OK;
      }
      /* step through all sub certificates to link them and copy them to their NvM blocks */
      do
      {
        /* get the length of the sub certificates */
        Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize].Length = SubCertificatePtr->Length;
        /* copy the sub certificates */ /* PRQA S 0310,33050,315 3 */ /* MD_Scc_0310_0314_0316_3305,MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy  */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))
          &Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize].Buffer[0],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&SubCertificatePtr->Buffer[0], SubCertificatePtr->Length);
        /* try to write the ContractSubCertificates to the NvM and check if it failed */ /* PRQA S 0314 3 */ /* MD_Scc_Nvm_Generic */
        if ( (Std_ReturnType)E_OK != NvM_WriteBlock(
          (NvM_BlockIdType)Scc_GetNvMBlockIDContrSubCerts(Scc_CertsWs.ChosenContrCertChainIdx,Scc_CertsWs.ContrCertChainSize),
          (P2VAR(void,AUTOMATIC,SCC_APPL_DATA))&Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize].Buffer[0]) )
        {
          Scc_ReportError(Scc_StackError_NvM);
  #if ( defined SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL )
          /* report status to DEM */
          Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL);
  #endif /* SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL */
          return (Std_ReturnType)E_NOT_OK;
        }
  #if ( defined SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL )
        else
        {
          /* report status to DEM */
          Scc_DemReportErrorStatusPassed(SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL);
        }
  #endif /* SCC_DEM_NVM_READ_CONTR_SUB_CERT_FAIL */

        if ( 0 < Scc_CertsWs.ContrCertChainSize )
        {
          /* link the sub certificates */
          Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize-1].NextCertificatePtr =
            &Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize];
        }

        /* set the new nvm read state */
        Scc_CertsWs.ContrSubCertsReadStates[Scc_CertsWs.ContrCertChainSize] = Scc_NvMBlockReadState_Processed;
        /* update the status flag */
        Scc_CertsWs.ContrSubCertsProcessedFlags |= (uint32)( (uint32)0x01 << (uint8)Scc_CertsWs.ContrCertChainSize );

        /* adjust the contract certificate chain size */
        Scc_CertsWs.ContrCertChainSize++;

        /* adjust the certificate pointer */
        SubCertificatePtr = SubCertificatePtr->NextCertificatePtr;
      }
      while ( NULL_PTR != SubCertificatePtr );
    }

    /* --- write the new chain size to the NvM */

    /* try to write the ContractCertificateChainSize and check if it failed */ /* PRQA S 0314 3 */ /* MD_Scc_Nvm_Generic */
    if ( (Std_ReturnType)E_OK != NvM_WriteBlock(
      (NvM_BlockIdType)Scc_GetNvMBlockIDContrCertChainSize(Scc_CertsWs.ChosenContrCertChainIdx),
      (P2VAR(void, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrCertChainSize) )
    {
      Scc_ReportError(Scc_StackError_NvM);
  #if ( defined SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL )
      Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL);
  #endif /* SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL */
      return (Std_ReturnType)E_NOT_OK;
    }
  #if ( defined SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL )
    else
    {
      Scc_DemReportErrorStatusPassed(SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL);
    }
  #endif /* SCC_DEM_NVM_READ_CONTR_CERT_CHAIN_SIZE_FAIL */

    /* --- handle the rest of the parameters --- */

    /* RetryCounter */
    Scc_Set_ISO_PnC_RetryCounter(BodyPtr->RetryCounter, BodyPtr->RetryCounterFlag);

    /* ResponseCode */
    Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    RetVal = E_OK;
  }

  /* ----- Development Error Report --------------------------------------- */
#if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #50 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_CERTIFICATE_UPDATE_RES, errorId);
  }
#else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* SCC_DEV_ERROR_REPORT */
  return RetVal;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */
#endif /* SCC_ENABLE_CERTIFICATE_UPDATE */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_PaymentDetailsRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_PaymentDetailsRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_PaymentDetailsResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_PaymentDetailsResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;
  P2VAR(Exi_ISO_genChallengeType, AUTOMATIC, SCC_VAR_NOINIT) GenChallengePtr;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_PAYMENT_DETAILS_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

    #if ( defined SCC_ENABLE_PNC_CHARGING) && ( SCC_ENABLE_PNC_CHARGING == STD_ON )

      /* GenChallenge */ /* PRQA S 0310,3305 4 */ /* MD_Scc_0310_0314_0316_3305 */
      GenChallengePtr = (P2VAR(Exi_ISO_genChallengeType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[0];
      Scc_Exi_TempBufPos = sizeof(Exi_ISO_genChallengeType);
      /* PRQA S 0315 2 */ /* MD_MSR_VStdLibCopy */
      IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&GenChallengePtr->Buffer[0],
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->GenChallenge->Buffer[0], BodyPtr->GenChallenge->Length);
      GenChallengePtr->Length = BodyPtr->GenChallenge->Length;

    #endif /* SCC_ENABLE_PNC_CHARGING */

      /* EVSETimeStamp */
      Scc_Set_ISO_EVSETimeStamp(BodyPtr->EVSETimeStamp, TRUE);

      /* ResponseCode */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_AuthorizationRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_AuthorizationRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_AuthorizationResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_AuthorizationResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_AUTHORIZATION_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* EVSEProcessing */
      Scc_Set_ISO_EVSEProcessing(BodyPtr->EVSEProcessing);

      /* ResponseCode */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_ChargeParameterDiscoveryRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_ChargeParameterDiscoveryRes(void) /* PRQA S 2889 */ /* MD_Scc_15.5 */
{
  /* ----- Local Variables ---------------------------------------------- */
  boolean skipSAScheduleListCheck = FALSE; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  Scc_PhysicalValueType scc_PhysicalValueTmp;
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  boolean removeSalesTariffs = FALSE;
  boolean skipSalesTariffSignatureCheck = FALSE;
#endif /* SCC_ENABLE_PNC_CHARGING */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ChargeParameterDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ChargeParameterDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
  P2CONST(Exi_ISO_DC_EVSEChargeParameterType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVSEChargeParameterPtr;
#endif /* SCC_CHARGING_DC */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_CHARGE_PARAMETER_DISCOVERY_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
    return (Std_ReturnType)E_NOT_OK;
  }
#if ( defined SCC_DEM_UNEXPECTED_MSG )
  else
  {
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
  }
#endif /* SCC_DEM_UNEXPECTED_MSG */

  /* #20 Check if the response is negative. */
  if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
  {
    /* report the negative response code as stack error */
    Scc_ReportError(Scc_StackError_NegativeResponseCode);
    /* provide the response code to the application */
    Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    return (Std_ReturnType)E_NOT_OK;
  }

  /* #30 Report the parameter from the response to the application. */

  /* EVSEProcessing */
  Scc_Set_ISO_EVSEProcessing(BodyPtr->EVSEProcessing);

  /* only update the values when EVSEProcessing is set to Finished */
  if ( EXI_ISO_EVSEPROCESSING_TYPE_FINISHED == BodyPtr->EVSEProcessing )
  {
    /* SAScheduleListCheck */
    Scc_Get_ISO_SkipSAScheduleListCheck(&skipSAScheduleListCheck);

    /* Check if the SAScheduleListCheck should be skipped */
    if ( TRUE != skipSAScheduleListCheck ) /* PRQA S 2991,2995 */ /* MD_Scc_VariantDependent */
    {
      /* check if at least one SASchedule exists */
      if ( 0u == BodyPtr->SASchedulesFlag )
      {
        /* report the invalid parameter as stack error */
        Scc_ReportError(Scc_StackError_InvalidRxParameter);
        return (Std_ReturnType)E_NOT_OK;
      }
    }

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )

    Scc_Get_ISO_PnC_SkipSalesTariffSignatureCheck(&skipSalesTariffSignatureCheck);
    if (   ( TRUE != skipSalesTariffSignatureCheck) /*lint !e506 */
        && ( 0u != BodyPtr->SASchedulesFlag ) )
    {
      /* only execute the following code if the PnC profile is active */
      if ( SCC_ISO_PAYMENT_OPTION_TYPE_CONTRACT == Scc_Exi_PaymentOption )
      {
        uint16 PubKeyIdx;
        uint16 PubKeyLen;
        /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
        P2VAR(Exi_ISO_SAScheduleTupleType, AUTOMATIC, SCC_APPL_DATA) SAScheduleTuplePtr =
          ((P2VAR(Exi_ISO_SAScheduleListType, AUTOMATIC, SCC_APPL_DATA))(BodyPtr->SASchedules))->SAScheduleTuple;
        boolean SalesTariffFound = FALSE;

        /* check if the ContrSubCert1 exists */
        if ( 1 > Scc_CertsWs.ContrCertChainSize )
        {
          /* report the missing certificate as stack error */
          Scc_ReportError(Scc_StackError_InvalidTxParameter);
          /* the ContrSubCert1 does not exist, it is not possible to validate the signature */
          return (Std_ReturnType)E_NOT_OK;
        }
        /* get the index of the public key */
        if ( (Std_ReturnType)E_OK != Scc_GetIndexOfPublicKey(&Scc_CertsWs.ContrSubCerts[0].Buffer[0],
          Scc_CertsWs.ContrSubCerts[0].Length, &PubKeyIdx, &PubKeyLen) )
        {
          return (Std_ReturnType)E_NOT_OK;
        }
        /* check the length of the public key */
        if ( SCC_PUB_KEY_LEN < PubKeyLen )
        {
          /* report the invalid public key as stack error */
          Scc_ReportError(Scc_StackError_InvalidTxParameter);
          /* the public key is too big for the buffer */
          return (Std_ReturnType)E_NOT_OK;
        }
        else
        {
          /* the public key fits into the buffer */
          Scc_CertsWs.SaCertPubKeyLen = PubKeyLen;
        }
        /* copy the public key */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.SaCertPubKey[0],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrSubCerts[0].Buffer[PubKeyIdx],
          Scc_CertsWs.SaCertPubKeyLen);
        /* set the flag which defines if the public key buffer contains a valid value */
        Scc_SaPubKeyRcvd = TRUE;

        /* Reset value */
        Scc_Exi_RefInSigInfo.Flags = 0u;

        /* step through the SAScheduleTuples to search for a SalesTariff */
        while ( NULL_PTR != SAScheduleTuplePtr )
        {
          /* check if a SalesTariff exists in this SAScheduleTuple */
          if ( 1u == SAScheduleTuplePtr->SalesTariffFlag )
          {
            SalesTariffFound = TRUE;

            /* Set the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
            if ( Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff2 == TRUE )
            {
              Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff3 = TRUE;
            }
            else if (Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff1 == TRUE )
            {
              Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff2 = TRUE;
            }
            else
            {
              Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff1 = TRUE;
            }
          }
          /* get to the next SAScheduleTuple */
          SAScheduleTuplePtr = SAScheduleTuplePtr->NextSAScheduleTuplePtr;
        }

        /* check if signature has to exist */
        if ( TRUE == SalesTariffFound )
        {
          /* check if the signature exists */
          if ( 1u == Scc_ExiRx_ISO_MsgPtr->Header->SignatureFlag )
          {
            /* validate the signatures */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
            if ( XmlSec_RetVal_OK != XmlSecurity_ValidateExiSignature(&Scc_Exi_XmlSecSigValWs,
              Scc_ExiRx_ISO_MsgPtr->Header->Signature, &Scc_Exi_TempBuf[0], SCC_EXI_TEMP_BUF_LEN,
              Scc_ECDSAVerifyJobId, Scc_ECDSAVerifyKeyId, Scc_SHA256JobId) )
            {
              /* remove the SalesTariff */
              removeSalesTariffs = TRUE;
#if ( defined SCC_DEM_XML_SEC )
              /* EVSE provided an invalid signature */
              Scc_DemReportErrorStatusFailed(SCC_DEM_XML_SEC);
#endif /* SCC_DEM_XML_SEC */
            }
#if ( defined SCC_DEM_XML_SEC )
            else
            {
              /* signature was valid */
              Scc_DemReportErrorStatusPassed(SCC_DEM_XML_SEC);
            }
#endif /* SCC_DEM_XML_SEC */

            /* Check if all mandatory references were checked in the function Scc_ExiRx_ISO_XmlSecDereference() */
            if ( Scc_Exi_RefInSigInfo.Flags != 0u )
            {
              /* remove the SalesTariff */
              removeSalesTariffs = TRUE;
        #if ( defined SCC_DEM_XML_SEC )
              /* EVSE provided an invalid signature */
              Scc_DemReportErrorStatusFailed(SCC_DEM_XML_SEC);
        #endif /* SCC_DEM_XML_SEC */
            }


          }
          /* the SalesTariff has no signature */
          else
          {
            /* remove the SalesTariff */
            removeSalesTariffs = TRUE;
          }
        }
      }

      /* check if the SalesTariff shall be removed (V2G2-908) */
      if ( TRUE == removeSalesTariffs )
      {
        /* reset the SAScheduleTuplePtr */ /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
        Exi_ISO_SAScheduleTupleType *SAScheduleTuplePtr =
          ((P2VAR(Exi_ISO_SAScheduleListType, AUTOMATIC, SCC_APPL_DATA))(BodyPtr->SASchedules))->SAScheduleTuple;
        /* step through the SAScheduleTuples to search for a SalesTariff */
        while ( NULL_PTR != SAScheduleTuplePtr )
        {
          /* remove the SalesTariff from the structure  */
          SAScheduleTuplePtr->SalesTariffFlag = 0;
          /* get to the next SAScheduleTuple */
          SAScheduleTuplePtr = SAScheduleTuplePtr->NextSAScheduleTuplePtr;
        }
      }
    }
#endif /* SCC_ENABLE_PNC_CHARGING */
    /* PRQA S 0316 1 */ /* MD_Scc_0310_0314_0316_3305 */
    Scc_Set_ISO_SAScheduleList((P2CONST(Exi_ISO_SAScheduleListType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->SASchedules);
  }

#if ( SCC_ENABLE_ONGOING_CALLBACKS ) && ( SCC_ENABLE_ONGOING_CALLBACKS == STD_ON )
#else
  /* only update the values when EVSEProcessing is set to Finished */
  if ( EXI_ISO_EVSEPROCESSING_TYPE_FINISHED == BodyPtr->EVSEProcessing )
#endif
  {

#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
    {
      /* check if the EVSEChargeParameter have the correct type */
      if ( EXI_ISO_DC_EVSECHARGE_PARAMETER_TYPE != BodyPtr->EVSEChargeParameterElementId )
      {
        Scc_ReportError(Scc_StackError_InvalidRxParameter);
        return (Std_ReturnType)E_NOT_OK;
      }
      /* set the pointer to the DC_EVSEChargeParameterPtr */ /* PRQA S 0316 1 */ /* MD_Scc_0310_0314_0316_3305 */
      DC_EVSEChargeParameterPtr = (P2VAR(Exi_ISO_DC_EVSEChargeParameterType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->EVSEChargeParameter;

      /* DC_EVSEChargeParameter -> DC_EVSEStatus */
      Scc_ExiRx_ISO_ReadEVSEStatusDC(DC_EVSEChargeParameterPtr->DC_EVSEStatus);

      /* DC_EVSEChargeParameter -> EVSEMaximumCurrentLimit */
      Scc_Conv_ISO2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMaximumCurrentLimit, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSEMaximumCurrentLimit(&scc_PhysicalValueTmp, 1);

      /* DC_EVSEChargeParameter -> EVSEMaximumPowerLimit */
      Scc_Conv_ISO2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMaximumPowerLimit, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSEMaximumPowerLimit(&scc_PhysicalValueTmp, 1);

      /* DC_EVSEChargeParameter -> EVSEMaximumVoltageLimit */
      Scc_Conv_ISO2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMaximumVoltageLimit, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSEMaximumVoltageLimit(&scc_PhysicalValueTmp, 1);

      /* DC_EVSEChargeParameter -> EVSEMinimumCurrentLimit */
      Scc_Conv_ISO2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMinimumCurrentLimit, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSEMinimumCurrentLimit(&scc_PhysicalValueTmp);

      /* DC_EVSEChargeParameter -> EVSEMinimumVoltageLimit */
      Scc_Conv_ISO2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMinimumVoltageLimit, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSEMinimumVoltageLimit(&scc_PhysicalValueTmp);

      /* DC_EVSEChargeParameter -> EVSECurrentRegulationTolerance */
      Scc_Conv_ISO2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSECurrentRegulationTolerance, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSECurrentRegulationTolerance(&scc_PhysicalValueTmp,
        DC_EVSEChargeParameterPtr->EVSECurrentRegulationToleranceFlag);

      /* DC_EVSEChargeParameter -> EVSEPeakCurrentRipple */
      Scc_Conv_ISO2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEPeakCurrentRipple, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSEPeakCurrentRipple(&scc_PhysicalValueTmp);

      /* DC_EVSEChargeParameter -> EVSEEnergyToBeDelivered */
      Scc_Conv_ISO2Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEEnergyToBeDelivered, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSEEnergyToBeDelivered(&scc_PhysicalValueTmp,
        DC_EVSEChargeParameterPtr->EVSEEnergyToBeDeliveredFlag);
    }
#endif /* SCC_CHARGING_DC */
  }

  /* ResponseCode */
  Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);

  return (Std_ReturnType)E_OK;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_PowerDeliveryRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_PowerDeliveryRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_PowerDeliveryResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_PowerDeliveryResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_POWER_DELIVERY_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */


    #if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
      {
        /* check if the EVSEStatus contains an DC_EVSEStatus */
        if ( EXI_ISO_DC_EVSESTATUS_TYPE != BodyPtr->EVSEStatusElementId )
        {
          Scc_ReportError(Scc_StackError_InvalidRxParameter);
        }
        else
        {
          retVal = E_OK;
        }
        /* DC_EVSEStatus */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
        Scc_ExiRx_ISO_ReadEVSEStatusDC((P2VAR(Exi_ISO_DC_EVSEStatusType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->EVSEStatus);
      }
    #endif /* SCC_CHARGING_DC */

      if ( retVal == E_OK )
      {
        /* ResponseCode */
        Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
      }
    }
  }

  return retVal;
}

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_MeteringReceiptRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_MeteringReceiptRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_MeteringReceiptResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_MeteringReceiptResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_METERING_RECEIPT_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }

  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */


    #if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
      {
        /* check if the EVSEStatus contains an DC_EVSEStatus */
        if ( EXI_ISO_DC_EVSESTATUS_TYPE != BodyPtr->EVSEStatusElementId )
        {
          Scc_ReportError(Scc_StackError_InvalidRxParameter);
        }
        else
        {
          /* DC_EVSEStatus */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
          Scc_ExiRx_ISO_ReadEVSEStatusDC((P2VAR(Exi_ISO_DC_EVSEStatusType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->EVSEStatus);

          retVal = E_OK;
        }
      }
    #endif /* SCC_CHARGING_DC */

      if ( retVal == E_OK )
      {
        /* ResponseCode */
        Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
      }
    }
  }

  return retVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_SessionStopRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_SessionStopRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_SessionStopResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_SessionStopResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_SESSION_STOP_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* reset the SessionID if the session was terminated */
    if ( SCC_ISO_CHARGING_SESSION_TYPE_TERMINATE == Scc_Exi_ChargingSession )
    {
      /* reset the SessionID */
      Scc_SessionIDNvm[0] = 0x01;
      Scc_SessionIDNvm[1] = 0x00;

      /* set the flag for the NvM to copy the new SessionID */
      (void) NvM_SetRamBlockStatus((NvM_BlockIdType)SCC_SESSION_ID_NVM_BLOCK, TRUE);
    }

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {

      /* #30 Report the parameter from the response to the application. */
      /* ResponseCode */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}


#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_CableCheckRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_CableCheckRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_CableCheckResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_CableCheckResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_CABLE_CHECK_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {

      /* #30 Report the parameter from the response to the application. */
      /* EVSEProcessing */
      Scc_Set_ISO_EVSEProcessing(BodyPtr->EVSEProcessing);

    #if ( SCC_ENABLE_ONGOING_CALLBACKS ) && ( SCC_ENABLE_ONGOING_CALLBACKS == STD_ON )
    #else
      /* only update the values when EVSEProcessing is set to Finished */

      /*workaround start: PABLO. */
      /*Report alwys the EVSE status to the application*/
      //if ( EXI_ISO_EVSEPROCESSING_TYPE_FINISHED == BodyPtr->EVSEProcessing )
      if (TRUE)
      /*Workaround end*/
    #endif
      {
        /* DC_EVSEStatus */
        Scc_ExiRx_ISO_ReadEVSEStatusDC((P2VAR(Exi_ISO_DC_EVSEStatusType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->DC_EVSEStatus);
      }

      /* ResponseCode */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_PreChargeRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_PreChargeRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  Scc_PhysicalValueType scc_PhysicalValueTmp;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_PreChargeResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_PreChargeResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_PRE_CHARGE_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* DC_EVSEStatus */
      Scc_ExiRx_ISO_ReadEVSEStatusDC((P2VAR(Exi_ISO_DC_EVSEStatusType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->DC_EVSEStatus);

      /* EVSEPresentVoltage */
      Scc_Conv_ISO2Scc_PhysicalValue(BodyPtr->EVSEPresentVoltage, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSEPresentVoltage(&scc_PhysicalValueTmp);

      /* ResponseCode */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_CurrentDemandRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_CurrentDemandRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  Scc_PhysicalValueType scc_PhysicalValueTmp;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_CurrentDemandResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_CurrentDemandResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_CURRENT_DEMAND_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* DC_EVSEStatus */
      Scc_ExiRx_ISO_ReadEVSEStatusDC((P2VAR(Exi_ISO_DC_EVSEStatusType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->DC_EVSEStatus);

      /* EVSEPresentVoltage */
      Scc_Conv_ISO2Scc_PhysicalValue(BodyPtr->EVSEPresentVoltage, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSEPresentVoltage(&scc_PhysicalValueTmp);

      /* EVSEPresentCurrent */
      Scc_Conv_ISO2Scc_PhysicalValue(BodyPtr->EVSEPresentCurrent, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSEPresentCurrent(&scc_PhysicalValueTmp);

      /* EVSECurrentLimitAchieved */
      Scc_Set_ISO_DC_EVSECurrentLimitAchieved(BodyPtr->EVSECurrentLimitAchieved);

      /* EVSEVoltageLimitAchieved */
      Scc_Set_ISO_DC_EVSEVoltageLimitAchieved(BodyPtr->EVSEVoltageLimitAchieved);

      /* EVSEPowerLimitAchieved */
      Scc_Set_ISO_DC_EVSEPowerLimitAchieved(BodyPtr->EVSEPowerLimitAchieved);

      /* EVSEMaximumVoltageLimit */
      Scc_Conv_ISO2Scc_PhysicalValue(BodyPtr->EVSEMaximumVoltageLimit, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSEMaximumVoltageLimit(&scc_PhysicalValueTmp, BodyPtr->EVSEMaximumVoltageLimitFlag);

      /* EVSEMaximumCurrentLimit */
      Scc_Conv_ISO2Scc_PhysicalValue(BodyPtr->EVSEMaximumCurrentLimit, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSEMaximumCurrentLimit(&scc_PhysicalValueTmp, BodyPtr->EVSEMaximumCurrentLimitFlag);

      /* EVSEMaximumPowerLimit */
      Scc_Conv_ISO2Scc_PhysicalValue(BodyPtr->EVSEMaximumPowerLimit, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSEMaximumPowerLimit(&scc_PhysicalValueTmp, BodyPtr->EVSEMaximumPowerLimitFlag);

      /* EVSEID */
      Scc_Set_ISO_EVSEID(BodyPtr->EVSEID);

      /* SAScheduleTupleID */
      if ( BodyPtr->SAScheduleTupleID != Scc_Exi_SAScheduleTupleID )
      {
        /* the received SAScheduleTupleID is different from the one which was sent to the EVSE in the PowerDeliveryReq */
        Scc_ReportError(Scc_StackError_InvalidRxParameter);
      }
      else
      {
        /* MeterInfo */
        Scc_Set_ISO_MeterInfo(BodyPtr->MeterInfo, BodyPtr->MeterInfoFlag);

#if ( defined SCC_ENABLE_PNC_CHARGING) && ( SCC_ENABLE_PNC_CHARGING == STD_ON )

        /* ReceiptRequired */
        Scc_Set_ISO_PnC_ReceiptRequired(BodyPtr->ReceiptRequired, BodyPtr->ReceiptRequiredFlag);

        /* check if ReceiptRequired was received with value TRUE */
        if (   ( 1u == BodyPtr->ReceiptRequiredFlag )
            && ( TRUE == BodyPtr->ReceiptRequired ))
        {
          /* check if a MeterInfo element is present */
          if ( 1u == BodyPtr->MeterInfoFlag )
          {
            /* copy the meter info element to the temp buf */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
            P2VAR(Exi_ISO_MeterInfoType, AUTOMATIC, SCC_VAR_NOINIT) MeterInfoPtr =
              (P2VAR(Exi_ISO_MeterInfoType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[0];
            Scc_Exi_TempBufPos = (uint16)sizeof(Exi_ISO_MeterInfoType);
            /* create the MeterID element */ /* PRQA S 0310, 3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
            MeterInfoPtr->MeterID = (P2VAR(Exi_ISO_meterIDType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[Scc_Exi_TempBufPos];
            Scc_Exi_TempBufPos += (uint16)sizeof(Exi_ISO_meterIDType);
            /* copy the content */ /* PRQA S 0310, 3305, 0315 3 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
            IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&MeterInfoPtr->MeterID->Buffer[0],
              (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->MeterInfo->MeterID->Buffer[0],
              BodyPtr->MeterInfo->MeterID->Length);
            MeterInfoPtr->MeterID->Length = BodyPtr->MeterInfo->MeterID->Length;
            /* copy the SigMeterReading */
            if ( 1u == BodyPtr->MeterInfo->SigMeterReadingFlag )
            {
              /* create the SigMeterReading element */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
              MeterInfoPtr->SigMeterReading =
                (P2VAR(Exi_ISO_sigMeterReadingType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[Scc_Exi_TempBufPos];
              Scc_Exi_TempBufPos += (uint16)sizeof(Exi_ISO_sigMeterReadingType);
              /* copy the content */ /* PRQA S 0310, 3305, 0315 3 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
              IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&MeterInfoPtr->SigMeterReading->Buffer[0],
                (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->MeterInfo->SigMeterReading->Buffer[0],
                BodyPtr->MeterInfo->SigMeterReading->Length);
              /* set the length and the flag */
              MeterInfoPtr->SigMeterReading->Length = BodyPtr->MeterInfo->SigMeterReading->Length;
              MeterInfoPtr->SigMeterReadingFlag = 1;
            }
            else
            {
              MeterInfoPtr->SigMeterReadingFlag = 0;
            }
            /* copy the TMeter */
            MeterInfoPtr->TMeterFlag = BodyPtr->MeterInfo->TMeterFlag;
            MeterInfoPtr->TMeter = BodyPtr->MeterInfo->TMeter;
            /* copy the MeterReading */
            MeterInfoPtr->MeterReadingFlag = BodyPtr->MeterInfo->MeterReadingFlag;
            MeterInfoPtr->MeterReading = BodyPtr->MeterInfo->MeterReading;
            /* copy the MeterStatus */
            MeterInfoPtr->MeterStatusFlag = BodyPtr->MeterInfo->MeterStatusFlag;
            MeterInfoPtr->MeterStatus = BodyPtr->MeterInfo->MeterStatus;

            retVal = E_OK;
          }
          else
          {
            /* a receipt is required, but no MeterInfo was sent */
            Scc_ReportError(Scc_StackError_InvalidRxParameter);
          }
        }
        else
        {
          retVal = E_OK;
        }
#else /* SCC_ENABLE_PNC_CHARGING */

        retVal = E_OK;

#endif /* SCC_ENABLE_PNC_CHARGING */
        if (retVal == E_OK) /* PRQA S 2991,2995 */ /* MD_Scc_VariantDependent */
        {
          /* ResponseCode */
          Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
        }
      }
    }
  }

  return retVal;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_WeldingDetectionRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_WeldingDetectionRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  Scc_PhysicalValueType scc_PhysicalValueTmp;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_WeldingDetectionResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_WeldingDetectionResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement;
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_WELDING_DETECTION_RES_TYPE != Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* DC_EVSEStatus */
      Scc_ExiRx_ISO_ReadEVSEStatusDC((P2VAR(Exi_ISO_DC_EVSEStatusType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->DC_EVSEStatus);

      /* EVSEPresentVoltage */
      Scc_Conv_ISO2Scc_PhysicalValue(BodyPtr->EVSEPresentVoltage, &scc_PhysicalValueTmp);
      Scc_Set_ISO_DC_EVSEPresentVoltage(&scc_PhysicalValueTmp);

      /* ResponseCode */
      Scc_Set_ISO_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

#endif /* SCC_CHARGING_DC */


#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_ReadEVSEStatusDC
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_ExiRx_ISO_ReadEVSEStatusDC(P2CONST(Exi_ISO_DC_EVSEStatusType, AUTOMATIC, SCC_VAR_NOINIT) EVSEStatusPtr)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Report the parameter from the response to the application. */
  /* EVSEIsolationStatus */
  Scc_Set_ISO_DC_EVSEIsolationStatus(EVSEStatusPtr->EVSEIsolationStatus, EVSEStatusPtr->EVSEIsolationStatusFlag);
  /* EVSEStatusCode */
  Scc_Set_ISO_DC_EVSEStatusCode(EVSEStatusPtr->EVSEStatusCode);
  /* EVSENotification */
  Scc_Set_ISO_EVSENotification(EVSEStatusPtr->EVSENotification);
  /* NotificationMaxDelay */
  Scc_Set_ISO_NotificationMaxDelay(EVSEStatusPtr->NotificationMaxDelay);

}
#endif /* SCC_CHARGING_DC */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )



/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_XmlSecDereference
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_XmlSecDereference(P2VAR(uint8*, AUTOMATIC, SCC_VAR_NOINIT) ExiStructPtr,
  P2VAR(Exi_RootElementIdType, AUTOMATIC, SCC_VAR_NOINIT) ExiRootElementId, P2VAR(Exi_NamespaceIdType, AUTOMATIC, SCC_VAR_NOINIT) ExiNamespaceId,
  P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) URIPtr, uint16 URILength)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;

  P2CONST(uint8, AUTOMATIC, SCC_VAR_INIT) currentURIPtr = &(URIPtr[0]);
  uint16 currentURILength = URILength;

  /* ----- Implementation ----------------------------------------------- */
  if ( (uint8)'#' == currentURIPtr[0] )
  {
    currentURIPtr = &currentURIPtr[1];
    currentURILength--;
  }

  /* #10 Choose response type and set Struct-Pointer, RootElementId and NameSpaceId */
  switch ( Scc_ExiRx_ISO_MsgPtr->Body->BodyElementElementId )
  {
  /* #20 current message in the rx buffer is a ChargeParameterDiscoveryRes */
  case EXI_ISO_CHARGE_PARAMETER_DISCOVERY_RES_TYPE:
    {
      P2CONST(Exi_ISO_ChargeParameterDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
        (P2CONST(Exi_ISO_ChargeParameterDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement; /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */

      /* get the pointer to the SAScheduleTuples */
      P2VAR(Exi_ISO_SAScheduleTupleType, AUTOMATIC, SCC_APPL_DATA) SAScheduleTuplePtr =
        ((P2VAR(Exi_ISO_SAScheduleListType, AUTOMATIC, SCC_APPL_DATA))(BodyPtr->SASchedules))->SAScheduleTuple; /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */

      /* check the ID of the SalesTariff of the SAScheduleTuples */
      while ( NULL_PTR != SAScheduleTuplePtr )
      {
        /* check if a SalesTariff exists */
        if ( 1u == SAScheduleTuplePtr->SalesTariffFlag )
        {
          /* check if the lengths of the URI and the ID match */
          if ( currentURILength == SAScheduleTuplePtr->SalesTariff->Id->Length )
          {
            /* check if the strings match */
            if ( IPBASE_CMP_EQUAL == IpBase_StrCmpLen(currentURIPtr, &SAScheduleTuplePtr->SalesTariff->Id->Buffer[0], currentURILength) )
            {
              retVal = (Std_ReturnType)E_OK;

              /* set the ExiStruct pointer and the RootElementId for the pointer */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
              *ExiStructPtr = (uint8*)SAScheduleTuplePtr->SalesTariff;
              *ExiRootElementId = EXI_ISO_SALES_TARIFF_TYPE;
              *ExiNamespaceId = (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_TYPE;

              /* Remove the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
              if (Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff1 == TRUE )
              {
                Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff1 = FALSE;
              }
              else if (Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff2 == TRUE)
              {
                Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff2 = FALSE;
              }
              else if (Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff3 == TRUE)
              {
                Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff3 = FALSE;
              }
              else
              {
                /* only 3 SalesTariffs are possible due to the Schema */
                retVal = E_NOT_OK;
              }

              break;
            }
          }
        }
        /* switch to the next SAScheduleTuple */
        SAScheduleTuplePtr = SAScheduleTuplePtr->NextSAScheduleTuplePtr;
      }
    }
    break;

  /* #30 current message in the rx buffer is a CertificateInstallationRes */
  case EXI_ISO_CERTIFICATE_INSTALLATION_RES_TYPE:
    {
      P2CONST(Exi_ISO_CertificateInstallationResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
        (P2CONST(Exi_ISO_CertificateInstallationResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement; /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */

      /* check if the ContractSignatureCertChain shall be dereferenced */
      /* check if the length of the URI matches */
      if (   ( 1u == BodyPtr->ContractSignatureCertChain->IdFlag )
          && ( currentURILength == BodyPtr->ContractSignatureCertChain->Id->Length ))
      {
        /* check if the URIs are the same */
        if ( IPBASE_CMP_EQUAL ==
          IpBase_StrCmpLen(currentURIPtr, &BodyPtr->ContractSignatureCertChain->Id->Buffer[0], currentURILength) )
        {
          /* set the ExiStruct pointer and the RootElementId for the pointer */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
          *ExiStructPtr = (uint8*)BodyPtr->ContractSignatureCertChain;
          *ExiRootElementId = EXI_ISO_CONTRACT_SIGNATURE_CERT_CHAIN_TYPE;
          *ExiNamespaceId = (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_TYPE;

          /* Remove the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
          if ( Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureCertChain == TRUE )
          {
            Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureCertChain = FALSE;
            retVal = (Std_ReturnType)E_OK;
          }
          else
          {
            retVal = E_NOT_OK;
          }

          break; /* PRQA S 3333 */ /* MD_Scc_3333 */
        }
      }

      /* check if the ContractSignatureEncryptedPrivateKey shall be dereferenced */
      /* check if the length of the URI matches */
      if ( currentURILength == BodyPtr->ContractSignatureEncryptedPrivateKey->Id->Length )
      {
        /* check if the URIs are the same */
        if ( IPBASE_CMP_EQUAL ==
          IpBase_StrCmpLen(currentURIPtr, &BodyPtr->ContractSignatureEncryptedPrivateKey->Id->Buffer[0], currentURILength) )
        {
          /* set the ExiStruct pointer and the RootElementId for the pointer */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
          *ExiStructPtr = (uint8*)BodyPtr->ContractSignatureEncryptedPrivateKey;
          *ExiRootElementId = EXI_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY_TYPE;
          *ExiNamespaceId = (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_TYPE;

          /* Remove the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
          if (Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureEncryptedPrivateKey == TRUE )
          {
            Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureEncryptedPrivateKey = FALSE;
            retVal = (Std_ReturnType)E_OK;
          }
          else
          {
            retVal = E_NOT_OK;
          }

          break; /* PRQA S 3333 */ /* MD_Scc_3333 */
        }
      }

      /* check if the DHpublickey shall be dereferenced */
      /* check if the length of the URI matches */
      if ( currentURILength == BodyPtr->DHpublickey->Id->Length )
      {
        /* check if the URIs are the same */
        if ( IPBASE_CMP_EQUAL == IpBase_StrCmpLen(currentURIPtr, &BodyPtr->DHpublickey->Id->Buffer[0], currentURILength) )
        {
          /* set the ExiStruct pointer and the RootElementId for the pointer */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
          *ExiStructPtr = (uint8*)BodyPtr->DHpublickey;
          *ExiRootElementId = EXI_ISO_DHPUBLICKEY_TYPE;
          *ExiNamespaceId = (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_TYPE;

          /* Remove the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
          if (Scc_Exi_RefInSigInfo.Flag.RISI_DHpublickey == TRUE )
          {
            Scc_Exi_RefInSigInfo.Flag.RISI_DHpublickey = FALSE;
            retVal = (Std_ReturnType)E_OK;
          }
          else
          {
            retVal = E_NOT_OK;
          }

          break; /* PRQA S 3333 */ /* MD_Scc_3333 */
        }
      }

      /* check if the eMAID shall be dereferenced */
      /* check if the length of the URI matches */
      if ( currentURILength == BodyPtr->eMAID->Id->Length ) /* PRQA S 2004 */ /* MD_Scc_2004 */
      {
        /* check if the URIs are the same */
        if ( IPBASE_CMP_EQUAL == IpBase_StrCmpLen(currentURIPtr, &BodyPtr->eMAID->Id->Buffer[0], currentURILength) )
        {
          /* set the ExiStruct pointer and the RootElementId for the pointer */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
          *ExiStructPtr = (uint8*)BodyPtr->eMAID;
          *ExiRootElementId = EXI_ISO_EMAID_TYPE;
          *ExiNamespaceId = (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_TYPE;

          /* Remove the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
          if (Scc_Exi_RefInSigInfo.Flag.RISI_eMAID == TRUE )
          {
            Scc_Exi_RefInSigInfo.Flag.RISI_eMAID = FALSE;
            retVal = (Std_ReturnType)E_OK;
          }
          else
          {
            retVal = E_NOT_OK;
          }

          break; /* PRQA S 3333 */ /* MD_Scc_3333 */
        }
      }
    }
    break;

  /* #40 current message in the rx buffer is a CertificateUpdateRes */
  case EXI_ISO_CERTIFICATE_UPDATE_RES_TYPE:
    {
      P2CONST(Exi_ISO_CertificateUpdateResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
        (P2CONST(Exi_ISO_CertificateUpdateResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_MsgPtr->Body->BodyElement; /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */

      /* check if the ContractSignatureCertChain shall be dereferenced */
      /* check if the length of the URI matches */
      if (   ( 1u == BodyPtr->ContractSignatureCertChain->IdFlag )
          && ( currentURILength == BodyPtr->ContractSignatureCertChain->Id->Length ))
      {
        /* check if the URIs are the same */
        if ( IPBASE_CMP_EQUAL ==
          IpBase_StrCmpLen(currentURIPtr, &BodyPtr->ContractSignatureCertChain->Id->Buffer[0], currentURILength) )
        {
          /* set the ExiStruct pointer and the RootElementId for the pointer */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
          *ExiStructPtr = (uint8*)BodyPtr->ContractSignatureCertChain;
          *ExiRootElementId = EXI_ISO_CONTRACT_SIGNATURE_CERT_CHAIN_TYPE;
          *ExiNamespaceId = (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_TYPE;

          /* Remove the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
          if (Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureCertChain == TRUE )
          {
            Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureCertChain = FALSE;
            retVal = (Std_ReturnType)E_OK;
          }
          else
          {
            retVal = E_NOT_OK;
          }

          break; /* PRQA S 3333 */ /* MD_Scc_3333 */
        }
      }

      /* check if the ContractSignatureEncryptedPrivateKey shall be dereferenced */
      /* check if the length of the URI matches */
      if ( currentURILength == BodyPtr->ContractSignatureEncryptedPrivateKey->Id->Length )
      {
        /* check if the URIs are the same */
        if ( IPBASE_CMP_EQUAL ==
          IpBase_StrCmpLen(currentURIPtr, &BodyPtr->ContractSignatureEncryptedPrivateKey->Id->Buffer[0], currentURILength) )
        {
          /* set the ExiStruct pointer and the RootElementId for the pointer */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
          *ExiStructPtr = (uint8*)BodyPtr->ContractSignatureEncryptedPrivateKey;
          *ExiRootElementId = EXI_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY_TYPE;
          *ExiNamespaceId = (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_TYPE;

          /* Remove the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
          if (Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureEncryptedPrivateKey == TRUE )
          {
            Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureEncryptedPrivateKey = FALSE;
            retVal = (Std_ReturnType)E_OK;
          }
          else
          {
            retVal = E_NOT_OK;
          }


          break; /* PRQA S 3333 */ /* MD_Scc_3333 */
        }
      }

      /* check if the DHpublickey shall be dereferenced */
      /* check if the length of the URI matches */
      if ( currentURILength == BodyPtr->DHpublickey->Id->Length )
      {
        /* check if the URIs are the same */
        if ( IPBASE_CMP_EQUAL == IpBase_StrCmpLen(currentURIPtr, &BodyPtr->DHpublickey->Id->Buffer[0], currentURILength) )
        {
          /* set the ExiStruct pointer and the RootElementId for the pointer */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
          *ExiStructPtr = (uint8*)BodyPtr->DHpublickey;
          *ExiRootElementId = EXI_ISO_DHPUBLICKEY_TYPE;
          *ExiNamespaceId = (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_TYPE;

          /* Remove the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
          if (Scc_Exi_RefInSigInfo.Flag.RISI_DHpublickey == TRUE )
          {
            Scc_Exi_RefInSigInfo.Flag.RISI_DHpublickey = FALSE;
            retVal = (Std_ReturnType)E_OK;
          }
          else
          {
            retVal = E_NOT_OK;
          }

          break; /* PRQA S 3333 */ /* MD_Scc_3333 */
        }
      }

      /* check if the eMAID shall be dereferenced */
      /* check if the length of the URI matches */
      if ( currentURILength == BodyPtr->eMAID->Id->Length ) /* PRQA S 2004 */ /* MD_Scc_2004 */
      {
        /* check if the URIs are the same */
        if ( IPBASE_CMP_EQUAL == IpBase_StrCmpLen(currentURIPtr, &BodyPtr->eMAID->Id->Buffer[0], currentURILength) )
        {
          /* set the ExiStruct pointer and the RootElementId for the pointer */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
          *ExiStructPtr = (uint8*)BodyPtr->eMAID;
          *ExiRootElementId = EXI_ISO_EMAID_TYPE;
          *ExiNamespaceId = (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_TYPE;

          /* Remove the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
          if (Scc_Exi_RefInSigInfo.Flag.RISI_eMAID == TRUE )
          {
            Scc_Exi_RefInSigInfo.Flag.RISI_eMAID = FALSE;
            retVal = (Std_ReturnType)E_OK;
          }
          else
          {
            retVal = E_NOT_OK;
          }

          break; /* PRQA S 3333 */ /* MD_Scc_3333 */
        }
      }
    }
    break;

    /* unknown BodyElementId */
  default:
    retVal = (Std_ReturnType)E_NOT_OK;
    break;
  }

  return retVal;
} /* PRQA S 6010,6030,6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STMIF */

#endif /* SCC_ENABLE_PNC_CHARGING */

#define SCC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
/* PRQA L:NEST_STRUCTS */
/* PRQA L:RETURN_PATHS */

#endif /* SCC_SCHEMA_ISO */

/**********************************************************************************************************************
 *  END OF FILE: Scc_ExiRx_ISO.c
 *********************************************************************************************************************/
