/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc.h
 *        \brief  Smart Charging Communication Header File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * AUTHOR IDENTITY
 * --------------------------------------------------------------------------------------------------------------------
 * Name                          Initials      Company
 * --------------------------------------------------------------------------------------------------------------------
 * Alex Lunkenheimer             visalr        Vector Informatik GmbH
 * Fabian Eisele                 visefa        Vector Informatik GmbH
 * Phanuel Hieber                visphh        Vector Informatik GmbH
 * Danny Bogner                  visdyb        Vector Informatik GmbH
 * --------------------------------------------------------------------------------------------------------------------
 * REVISION HISTORY
 * --------------------------------------------------------------------------------------------------------------------
 * Version  Date       Author  Change Id     Description
 * --------------------------------------------------------------------------------------------------------------------
 * 01.00.00 2010-01-01 visalr  -             created
 * 01.xx.xx 2013-07-04 visalr  -             removed revision history for version 1.xx.xx
 * 02.xx.xx 2013-08-21 visefa  -             removed revision history for version 2.xx.xx
 * 04.00.00 2012-09-24 visefa  ESCAN00061627 TCP Timeout Handling not functional
 *          2012-10-22 visefa  ESCAN00062434 Implemented EIM AC charging according to ISO15118 DIS
 *          2013-01-18 visefa  ESCAN00064321 Implemented PnC AC charging according to ISO15118 DIS
 *          2013-01-23 visefa  ESCAN00064463 Added compatibility for AUTOSAR4
 * 04.00.01 2013-04-09 visefa  ESCAN00066501 OEM specific: Issues with transmission of COM signals
 *          2013-04-19 visefa  ESCAN00066791 OEM specific: SAScheduleList is not processed correctly
 * 04.00.02 2013-04-24 visefa  ESCAN00066883 OEM specific: Issue with transmission of SAScheduleList over CanTp
 * 04.00.03 2013-05-24 visefa  ESCAN00067642 Compiler error: last line of file ends without a newline
 * 04.01.00 2013-06-11 visefa  ESCAN00067976 Smaller NvM blocks are now read with ReadAll()
 *          2013-06-17 visefa  ESCAN00068105 EVCCID is now read from EthIf
 *          2013-07-01 visefa  ESCAN00068451 OEM specific: Added Vector defined state machine
 *          2013-07-01 visefa  ESCAN00068457 OEM specific: Update of implementation to latest spec (2013-05-02)
 *          2013-07-01 visefa  ESCAN00068458 OEM specific: Update of implementation to latest spec (2013-06-04)
 *          2013-07-01 visefa  ESCAN00068459 Incoming EXI encoded messages are now handled in the MainFunction()
 * 04.01.01 2013-07-29 visefa  ESCAN00069382 Issues with integration of XmlSecurity
 *          2013-08-06 visefa  ESCAN00069621 OEM specific: Issue at transition from ChargingStatus to PowerDelivery
 *          2013-08-08 visefa  ESCAN00069658 Rx and Tx messages now share the same Exi Stream Buffer
 *          2013-08-19 visefa  ESCAN00069936 OEM specific: Incorrect error handling in case of negative SAP response
 *          2013-08-22 visefa  ESCAN00069975 Issues during CertificateInstallation and CertificateUpdate
 *          2013-09-03 visefa  ESCAN00069998 OEM specific: Invalid V2GTP payload type is accepted for SDP responses
 *          2013-09-27 visefa  ESCAN00070685 OEM specific: Extended RTE interface by SCCStatus and ErrorStatus
 *          2013-09-27 visefa  ESCAN00070757 OEM specific: Amount of ChargingProfile entries is now variable
 *          2013-10-10 visefa  ESCAN00071013 OEM specific: Previous message set is not saved to NvM
 * 05.00.00 2013-12-20 visefa  ESCAN00072746 Implemented AC charging according to ISO15118 FDIS
 *          2013-12-20 visefa  ESCAN00072748 OEM specific: Update of implementation to latest spec (2013-12-09)
 * 05.00.01 2014-01-17 visefa  ESCAN00073053 MISRA corrections (2014-01-17)
 * 05.00.02 2014-01-21 visefa  ESCAN00073108 Code Maintenance (2014-01-21)
 * 05.01.00 2014-01-30 visefa  ESCAN00073281 OEM specific: Implemented DC charging according to ISO15118 FDIS
 *          2014-01-30 visefa  ESCAN00073283 OEM specific: Implemented DC charging according to DIN70121 RC6
 * 05.01.01 2014-02-25 visefa  ESCAN00073915 OEM specific: Added "DIN70121 only" support
 * 05.01.02 2014-03-10 visefa  ESCAN00074185 OEM specific: Resolved issues in the OEM StateMachine
 * 05.02.00 2014-03-25 visefa  ESCAN00074546 OEM specific: Added support for additional OEM
 * 05.02.01 2014-04-24 visefa  ESCAN00075209 X509IssuerName is now encoded according to RFC4514
 *          2014-04-29 visefa  ESCAN00075251 OEM specific: Changed the order of the AppProtocols
 * 05.02.02 2014-05-19 visefa  ESCAN00075440 Signatures of CertInstall/Update parameters aren't validated correctly
 *          2014-06-10 visefa  ESCAN00076172 OEM specific: Update of implementation to latest spec (2014-05-23)
 * 05.03.00 2014-06-16 visefa  ESCAN00076292 OEM specific: Implemented IC charging according to customer spec
 *          2014-06-27 visefa  ESCAN00076549 OEM specific: Value of a CAN signal is not correctly set
 *          2014-06-27 visefa  ESCAN00076550 Message Timeout when looping in cyclic messages
 * 05.03.01 2014-07-02 visefa  ESCAN00076652 OEM specific: Compiler error: "undeclared identifier"
 *          2014-07-03 visefa  ESCAN00076719 OEM specific: Retransmission of CANTP packets not working correctly
 *          2014-07-03 visefa  ESCAN00076723 OEM specific: Changed unit of Next Request Delay to 100ms
 * 06.00.00 2014-06-26 visefa  ESCAN00076507 Added support for Tp_TcpIp API of MSR4R8 and higher
 *          2014-07-10 visefa  ESCAN00076909 Added support for encrypted storage of PrivateKeys
 *          2014-07-17 visefa  ESCAN00077168 EXI Stream and one of the two EXI Struct buffers are no longer required
 *          2014-07-18 visefa  ESCAN00077203 OEM specific: Update of implementation to latest spec (2014-07-09)
 *          2014-08-06 visefa  ESCAN00077655 OEM specific: NvM is not included if not used
 * 06.01.00 2014-12-05 visefa  ESCAN00080039 OEM specific: Update of implementation to latest spec (2014-12-02)
 *          2014-12-05 visefa  ESCAN00080040 OEM specific: Next Request Delay is now based on MainFunction cycles
 * 06.02.00 2014-12-17 visefa  ESCAN00079925 Invalidated NvM blocks are not handled correctly
 *          2015-01-09 visefa  ESCAN00080478 OEM specific: SCC does not accept an UDS ACK according to ISO14229
 *          2015-01-19 visefa  ESCAN00080041 OEM specific: Implemented IC charging according to customer spec
 *          2015-01-21 visefa  ESCAN00080714 Added support for TX streaming
 *          2015-02-02 visefa  ESCAN00081707 OEM specific: Added ContrCertPrivKey password callback for CANTP
 *          2015-02-17 visefa  ESCAN00081296 OEM specific: Rework of IEC61851 handling
 *          2015-02-18 visefa  ESCAN00081333 OEM specific: Enumeration was not implemented as specified
 *          2015-02-18 visefa  ESCAN00081336 OEM specific: Parameter was not reset only on edge detection
 *          2015-02-18 visefa  ESCAN00081337 OEM specific: Signal regarding CAN communication was not reset properly
 *          2015-02-18 visefa  ESCAN00081338 OEM specific: Error event not correctly reported to application
 *          2015-02-20 visefa  ESCAN00081389 DC_EVSEChargeParameter were reported before EVSEProcessing == Finished
 * 06.03.00 2015-02-27 visefa  ESCAN00081514 Added support for SLAC handling
 * 06.03.01 2015-03-09 visefa  ESCAN00081668 Reworked NvM handling
 * 06.03.02 2015-03-17 visefa  ESCAN00081865 SLAC doesn't start after Firmware Download since QCA is not responding
 * 07.00.00 2015-04-01 visefa  ESCAN00082090 Added support for Tp_TcpIp API of MSR4R12 and higher
 *          2015-05-11 visefa  ESCAN00082930 OEM specific: Update of implementation to latest spec (2015-04-22)
 *          2015-06-08 visefa  ESCAN00083287 OEM specific: Issue during restart after error in SAP
 *          2015-06-08 visefa  ESCAN00083288 OEM specific: Issue during conversion of ChargingProfile
 *          2015-06-30 visefa  ESCAN00083734 OEM specific: Error event not correctly reported to application
 *          2015-07-15 visefa  ESCAN00084009 Incorporated findings of review
 * 07.00.01 2015-07-20 visefa  ESCAN00084090 Obsolete DET error occurs periodically
 * 07.00.02 2015-07-23 visefa  ESCAN00084190 OEM specific: Update of customer requirement
 * 07.00.03 2015-08-13 visefa  ESCAN00084513 OEM specific: Provisioning Data is not always being sent automatically
 * 07.01.00 2015-08-31 visefa  ESCAN00084887 OEM specific: ChargingProfile can now be stored in internal SCC buffer
 *          2015-08-28 visefa  ESCAN00084838 OEM specific: Provisioning Data API keeps returning E_PENDING
 *          2015-09-11 visefa  ESCAN00085007 MSR4: Scc_Cbk_TL_TCPAccepted callback was removed
 *          2015-09-11 visefa  ESCAN00084956 CPS check is not working
 * 07.01.01 2015-10-13 visefa  ESCAN00085826 OEM specific: ChargingStatus stop condition not met
 *          2015-10-13 visefa  ESCAN00085827 OEM specific: Requirement change of SessionStop error handling
 * 08.00.00 2015-11-20 visefa  ESCAN00086647 OEM specific: Update of implementation to latest spec (2015-10-08)
 *          2015-11-20 visefa  ESCAN00086648 Introduction of Scc_ReturnType
 *          2015-11-20 visefa  ESCAN00086643 Compiler error: ASR4 APIs of TcpIp used for ASR3
 *          2015-11-24 visefa  ESCAN00086707 Extended StackError enumeration by Request(Not)Acknowledged
 *          2016-01-14 visefa  ESCAN00087057 PrivKeys of ContrCert and ProvCert are only read when required
 *          2016-01-14 visefa  ESCAN00087378 SCC keeps trying to decode received response after a decode error
 *          2016-01-14 visefa  ESCAN00087465 OEM specific: SalesTariff without duration element is not being accepted
 *          2016-02-03 visefa  ESCAN00088041 Source of configuration is now configurable
 *          2016-02-03 visefa  ESCAN00088042 OEM specific: Update of implementation to latest spec (2016-02-01)
 *          2016-02-16 visefa  ESCAN00088324 OEM specific: PnC is selected for resumed session even if TLS is not used
 *          2016-02-16 visefa  ESCAN00088325 OEM specific: Single phase not selected if only three phase offered
 * 08.00.01 2016-04-21 visefa  ESCAN00089647 OEM specific: Incorrect calculation of timeouts
 * 08.01.00 2016-06-21 visefa  ESCAN00090581 Configure parameter reporting during EVSEProcessing(Ongoing)
 *          2016-06-24 visefa  ESCAN00090652 OEM specific: Update of implementation to latest spec (2016-05-20)
 *          2016-06-24 visefa  ESCAN00090652 Introduced EMAID validation during CertInstallation & CertUpdate
 *          2016-07-04 visefa  ESCAN00090812 OEM specific: Added Ethernet Test Mode
 * 08.01.01 2016-07-11 visefa  ESCAN00090928 OEM specific: MessageSet selection via SupportedMessageSet not working
 * 08.01.02 2016-08-29 visefa  ESCAN00091627 OEM specific: Compiler error: Pre-processor defines not set correctly
 * 09.00.00 2016-07-18 visefa  ESCAN00090914 Added support for Tp_TcpIp API of MSR4R15 and higher
 *          2016-07-18 visefa  ESCAN00091045 Removed 'V' prefix from internal functions
 * 09.01.00 2016-10-07 visefa  ESCAN00092040 Local static variable used that cannot be mapped using MemMap.h
 *          2016-10-07 visefa  ESCAN00092047 Variable in ZERO_INIT section has no value assignment
 *          2016-12-05 visefa  ESCAN00092926 Empty RootCertificateID sent if not all root cert slots filled
 *          2016-12-05 visefa  ESCAN00092814 TCP FIN cannot be transmitted in case SCC message timeout expires
 *          2016-12-05 visefa  ESCAN00092681 Wrong ResponseCodes are stored in error bit environment data
 *          2016-12-05 visefa  ESCAN00092923 Command to open S2 is sent too early
 *          2016-12-06 visefa  ESCAN00092919 Add option to block V2G communication
 *          2016-12-07 visefa  ESCAN00092842 Add separate MemMap.h sections for NvM RAM/ROM variables/constants
 *          2016-12-07 visefa  ESCAN00093202 OEM specific: Increased signal size
 *          2016-12-07 visefa  ESCAN00092920 eMAID check fails if '-' is used in eMAID
 * 09.02.00 2017-01-16 visefa  ESCAN00093559 OEM specific: Updated RTE wrapper (2017-01-13)
 *          2017-01-16 visefa  ESCAN00093564 OEM specific: EVEnergyCapacity is not sent with the correct Unit value
 *          2017-01-25 visphh  ESCAN00093440 Remove monitoring of TxConfirmation
 *          2017-02-01 visphh  ESCAN00093788 OEM specific: Timeout after ChargeParameterDiscoveryRes
 *          2017-02-02 visphh  ESCAN00093202 EAmount can now handle a uint32
 * 09.03.00 2017-03-22 visphh  ESCAN00092165 Support of Dynamic Length in CopyTxData in TcpIp
 *          2017-03-22 visphh  ESCAN00094084 Return value of EthTrcv_30_Ar7000_Slac_Start() is not evaluated
 * 09.03.01 2017-03-22 visphh  ESCAN00093554 The check for the EVSEMaximumPowerLimit can now be disabled
 * 09.04.00 2017-05-10 visphh  ESCAN00095100 OEM specific: Updated RTE wrapper (2017-05-09)
 * 09.04.01 2017-05-11 visphh  ESCAN00095119 Changed handling of ForceReset in Scc_ResetSessionID
 * 09.05.00 2017-05-29 visphh  ESCAN00095336 Scc_ConfigValue_StateM_AcceptUnsecureConnection undeclared identifier
 *          2017-05-29 visphh  ESCAN00094843 SCC wants to restart without SLAC even if no link is available
 *          2017-06-06 visphh  ESCAN00095186 Compiler warning: comma at end of enumerator list
 *          2017-07-04 visphh  ESCAN00095008 OEM specific: Component TLS can not process other users in addition to component SCC
 *          2017-07-03 visphh  ESCAN00095709 XmlSec_ReturnType is now an enumeration instead of a uint8 using defines
 *          2017-07-12 visefa  ESCAN00095558 OEM specific: RemainingTimeToFullSoC is not sent in case of DIN SPEC 70121
 *          2017-07-12 visefa  ESCAN00095549 OEM specific: Update of implementation to latest spec (2017-04-07)
 *          2017-07-12 visefa  ESCAN00095158 OEM specific: Random values are being sent when CAN signal is SNA
 *          2017-07-12 visefa  ESCAN00095431 OEM specific: PnC is selected instead of EIM
 *          2017-07-12 visefa  ESCAN00095440 Added SessionID to the parameter interface
 *          2017-07-12 visefa  ESCAN00095539 Added V2GRequest and V2GResponse to the parameter interface
 *          2017-07-12 visefa  ESCAN00095161 Invalid SECCDiscoveryProtocolRes are not ignored
 *          2017-07-12 visefa  ESCAN00094502 OEM specific: PCID can't be read from Provisioning Certificate
 *          2017-07-03 visphh  ESCAN00095706 Add CSM (ASR4.3) Support
 *          2017-07-31 visphh  ESCAN00096116 No report of SLAC failure and ReadRootCerts failure via Scc_StackError
 * 09.05.01 2017-08-30 visphh  ESCAN00096485 OEM specific: DIN EVSEProcessing Callback returns RTE-Error
 * 09.06.00 2017-09-07 visphh  STORYC-2437   Extend Scc_GetCertDistinguishedNameObject() for Subject_DomainComponent
 *          2017-09-12 visphh  STORYC-2465   Report Scc_ReportError if Scc_MsgTrig is Scc_MsgTrig_None
 *          2017-09-13 visphh  ESCAN00096642 Function Scc_ChangeProvCertPrivKeyPassword() returns Pending instead of Busy
 *          2017-09-14 visphh  STORYC-2490   Add Callback to report an unknown leaf certificate during TLS handshake
 * 09.06.01 2017-09-21 visphh  ESCAN00096762 Scc_CheckForCpoInDc only available for PnC
 * 10.00.00 2017-09-26 visphh  ESCAN00096831 Write ContractCertificatePrivateKey via Scc_DiagDataWriteAccess() fails
 *          2017-10-06 visphh  ESCAN00096939 Reconnection after CommunicationSetupTimeout if transceiver link was down
 *          2017-10-11 visphh  ESCAN00096996 CertificateInstallation fails if CSM is enabled
 *          2017-11-08 visphh  STORYC-1215   Support TCP/IP API according to ASR (CopyTxData)
 *          2017-11-09 visphh  ESCAN00097342 Removed ASR3 support
 *          2017-11-09 visphh  ESCAN00097162 EMAIDs with a length of 14 bytes cannot be processed
 *          2017-11-13 visphh  ESCAN00097318 OEM specific: Report InvalidTxParameter instead of InvalidRxParameter
 *          2017-11-13 visphh  ESCAN00097320 OEM specific: Used unsigned values instead of signed for Min Max Values
 *          2017-11-13 visphh  ESCAN00097322 OEM specific: Update Rx/Tx-Interface
 *          2017-11-16 visphh  ESCAN00097436 SCC needs Crypto_Types.h if CSM is enabled
 * 10.01.00 2017-12-19 visphh  ESCAN00097796 OEM specific: Wrong maximum value saturation for ISO_DC_EVSEPresentVoltage
 *          2017-12-20 visphh  ESCAN00097816 Missing memory mapping
 *          2017-12-20 visphh  ESCAN00097818 Scc_StateM_TrcvLinkState is not initialized in Scc_StateM_InitMemory()
 *          2017-12-20 visphh  ESCAN00097819 Missing check if TcpRxData fits in Scc_ExiStreamRxPBuf
 *          2017-12-20 visphh  ESCAN00097820 Missing conditions for SLAC failure
 *          2018-01-09 visphh  STORYC-3814   Added Scc_Cbk_SLAC_FirmwareDownloadStart
 *          2018-01-09 visphh  ESCAN00096232 Certificates cannot be loaded properly after being installed via API
 *          2018-01-09 visphh  ESCAN00096184 SCC does send messages with session ID set to 0 in the mid of a session
 *          2018-01-10 visphh  STORYC-2788   Support Wireless Power Transfer (WPT) according to ISO/IEC 15118 ED2 CD2
 *          2018-01-19 visphh  ESCAN00098066 Scc sends invalid V2GTP header
 * 10.01.01 2018-01-22 visphh  ESCAN00098078 OEM specific: Undeclared identifier - Scc_ServiceID_Internet/Certificate
 * 10.01.02 2018-02-01 visphh  ESCAN00098253 Scc_DeleteRootCert() does not return E_NOT_OK if RootCertIdx is invalid
 *          2018-02-09 visphh  ESCAN00098348 Compiler error: undeclared identifier Scc_ExiRx_ISO_MsgPtr (CSM & ISO Ed2)
 *          2018-02-13 visphh  ESCAN00098378 OEM specific: ContrCertInstallTrigger failed - XmlSec WS is not initialized
 *          2018-02-19 visphh  ESCAN00098442 Compiler error: Macro P2CONST does not take 1 argument
 *          2018-02-19 visphh  ESCAN00098441 Compiler warning: unreferenced parameter BufIdxPtr
 *          2018-02-19 visphh  ESCAN00098440 Compiler warning: unreferenced parameter PrivKeyLen/PrivKeyPtr
 * 11.00.00 2018-04-13 visphh  STORYC-3886   Code Refactoring and CDD (Proc3.0)
 *          2018-05-02 visphh  STORYC-3520   Remove Crypto Legacy support. Support only CSM (ASR4.3)
 *          2018-06-13 visphh  ESCAN00099678 Compiler error: ETHTRCV_30_AR7000_SLAC_E_UNKNOWN_STATE is undefined
 *          2018-07-02 visphh  ESCAN00099844 Scc triggers the Tcp to send more data than expected.
 *          2018-07-02 visphh  ESCAN00099796 OEM specific: Wrong SessionID in ServiceDiscoveryReq
 *          2018-08-01 visphh  ESCAN00098360 Compiler error: undefined symbol "Scc_ValidateKeyPair"
 *          2018-08-06 visphh  ESCAN00099799 OEM specific: Incorrect multiplier for EVSEMaximumPowerLimit and EVSEEnergyToBeDelivered
 *          2018-08-07 visphh  ESCAN00097794 SupportedSAPSchemas - Out of bounds array access (corrupt stack)
 * 11.01.00 2018-08-15 visphh  STORYC-6027   OEM specific: Improve process for OEM Add-On
 *          2018-08-20 visphh  ESCAN00100370 Scc does not send SessionStop after ResponseCode "FAILED" was received
 *          2018-08-24 visphh  ESCAN00100511 Scc does not report DEM-ErrorStatusFailed for SCC_DEM_IP_BASE
 *          2018-08-28 visphh  ESCAN00100551 Incorrect message status if SLAC failed
 *          2018-08-29 visphh  ESCAN00100558 OEM specific: Incorrect handling of EVMaximumVoltageLimit for DIN charging
 *          2018-08-29 visphh  ESCAN00100575 Scc reports SLAC failed for ETHTRCV_30_AR7000_SLAC_E_REMOTE_AMP_MAP_RCVD
 * 12.00.00 2018-09-06 visphh  STORYC-1410   Update AC and DC charging according to DIS of ISO/IEC 15118 ED2
 *          2018-09-06 visphh  STORYC-1412   Support Bidirectional Power Transfer (BPT) according to ISO/IEC 15118 ED2
 *          2018-09-06 visphh  STORYC-6588   OEM specific: Improve process for OEM Add-On
 *          2018-09-17 visphh  ESCAN00100767 ServiceDetail will not stop if PnC is disabled
 *          2018-09-20 visphh  ESCAN00100768 Scc fails checking CPO in SECC Certificate if PnC is disabled
 *          2018-09-27 visphh  STORYC-6592   Update WPT according to DIS of ISO/IEC 15118 ED2
 *          2018-10-08 visphh  ESCAN00100959 OEM specific: TLS leaf certificate is not allways reporterd via callback
 *          2018-11-08 visphh  ESCAN00101169 OEM specific: Scc does not detect invalid value of multiplier (3..-3)
 *          2018-11-13 visphh  ESCAN00101315 OEM specific Compiler error: Tx_s_valNorm_HlcScc should be Rx_s_valNorm_HlcScc
 *          2018-11-13 visphh  STORYC-6588   OEM specific: Migration from ASR3 to ASR4
 *          2018-11-23 visphh  STORYC-7136   Changed ISO/DIN to Scc_PhysicalValueType
 *          2018-12-02 visphh  ESCAN00101482 Scc ignores missing reference in SignedInfo
 * 12.01.00 2018-12-20 visphh  ESCAN00101633 OEM specific: Scc does not forward maximum value for sint16-value if received parameter is invalid
 *          2019-01-29 visphh  STORYC-6031   OEM specific: Improve process for OEM Add-On
 *          2019-01-30 visphh  ESCAN00101768 OEM specific: Scc_ExiRx_ISO_CertificateInstallationRes() is not defined
 *          2019-02-01 visphh  ESCAN00101658 Scc reports error if Tcp buffer is not available
 *          2019-02-07 visphh  ESCAN00102041 Scc_SessionIDNvm is not mapped to SEC_VAR_ZERO_INIT_8BIT_NVM
 *          2019-02-08 visphh  ESCAN00102044 Scc_ValidateProvCertKeyPair() uses private key of contract certficate
 * 12.01.01 2019-02-19 visphh  ESCAN00102182 SLAC starts again after DLink is ready
 *          2019-02-20 visphh  ESCAN00102189 Scc_DiagDataWriteAccess returns NotOk for the PrivateKey of ContractCertificate or ProvisioningCertificate
 *          2019-02-25 visphh  STORYC-7773   Add callback for ServiceCategory (DIN)
 * 12.02.00 2019-03-04 visphh  ESCAN00094838 OEM specific: Add the SLAC assoc status call back
 *          2019-03-06 visphh  ESCAN00102370 ECU gets stuck or resets occurs unexpectedly due to a null pointer exception
 *          2019-03-07 visphh  ESCAN00102203 OEM specific: Incorrect mapping of Scc_StateMachineStatus to RTE-Value StMacSts1
 *          2019-03-11 visphh  STORYC-6829   OEM specific: Update Add-On according to SPECDOC46254
 * 13.00.00 2019-03-20 visphh  STORYC-7983   Add CBK SDPTransportProtocol and enabled CallOut for DemReport-API
 *          2019-04-05 visphh  ESCAN00102745 DIN: Scc does not send SessionStopReq after PreCharge completed and ChargingControl is StopCharging
 *          2019-04-09 visphh  ESCAN00102726 DIN: Scc starts ReadyToChargeTimer not after DutyCycle 5% is reached
 * 13.01.00 2019-04-17 visphh  ESCAN00102804 OEM specific: Scc reports error InvalidRxParameter if ContractCertificate is not available
 *          2019-04-30 visphh  STORYC-7469   Support new CSM API according to ASR4.4 to securely install Private Key of Contract Certificate
 *          2019-05-06 visphh  ESCAN00103013 CertificateInstallationUpdate fails if last SccRootCertConfig container is empty
 * 14.00.00 2019-09-10 visdyb  STORYC-8371   Changed to MISRA2012
 *          2019-09-10 visphh  ESCAN00104256 OEM specific: Memory access out of bounds for 'Value Added Services'
 *          2019-09-17 visphh  ESCAN00103379 Scc_StateM_GlobalParamsChecks case Scc_ChargingControl_StopCharging does not work for all messages
 *          2019-09-19 visphh  V2G-235       Continue operation in case of invalid root certificates
 *          2019-09-19 visphh  V2G-264       Search String in DomainComponent(Subject) of TLS-LeafCertificate
 *          2019-09-23 visphh  ESCAN00103794 OEM specific: s_blTupelErhalten_SccHlc is always reported TRUE, even when SAScheduleList is missing
 *          2019-09-23 visphh  ESCAN00103819 OEM specific: PKCS7 containers are not only validated against OEM Root Certificate
 *          2019-09-23 visphh  ESCAN00103633 OEM specific: Signature Check of SalesTariff is not disabled in case Provisioning Certificate is used as Contract Certificate
 *          2019-09-23 visphh  ESCAN00103846 Scc reports error InvalidRxParameter if ContrSubCert1's public key is invalid
 *          2019-09-23 visphh  ESCAN00103778 OEM specific: eMAID check fails when ProvisioningCertificate is used as ContractCertificate
 *          2019-09-23 visphh  ESCAN00103845 OEM specific: Incorrect private environment is reported to the application
 *          2019-09-23 visphh  ESCAN00103478 OEM specific: TLS-check for DomainComponent(Subject) fails for private environment
 *          2019-09-23 visphh  ESCAN00103842 OEM specific: RTE wrapper reports an error in case of invalid / out of bounds value instead of only limiting the output value
 *          2019-09-24 visphh  ESCAN00103661 SalesTariff is not ignored when signature is missing or invalid
 *          2019-09-24 visphh  ESCAN00103610 Multiple domainComponent entries are not supported
 *          2019-09-25 visphh  V2G-1         OEM specific: Update according to SPEC
 *          2019-09-25 visphh  V2G-3         Increase interoperability of SAP for DIN SPEC 70121 SECCs
 * 14.00.01 2019-10-31 visphh  ESCAN00103715 TCP Connection is never closed when Server never sends a FIN
 * 14.00.02 2020-03-16 visdyb  ESCAN00105294 PnC will not be used in case of a free charging service
 *          2020-03-16 visdyb  ESCAN00105525 DIN: Missing Units in PhysicalValues are not accepted by EVSE
 *          2020-03-26 visdyb  ESCAN00104929 Error reported for reset-event from TCP
 *********************************************************************************************************************/
#if !defined (SCC_H)
#define SCC_H


/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Scc_Types.h"
#include "Scc_Cfg.h"
#include "Scc_Lcfg.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* Software Version */
#define SCC_SW_MAJOR_VERSION                0x14u  /* BCD coded version number */
#define SCC_SW_MINOR_VERSION                0x00u  /* BCD coded version number */
#define SCC_SW_PATCH_VERSION                0x02u  /* BCD coded version number */

/* Scc ModuleId */
#define SCC_VENDOR_ID    30u /* Vector ID */
#define SCC_MODULE_ID   255u /* Vector Module ID for Complex Device Drivers */
#define SCC_INSTANCE_ID 111u /* Vector Instance ID for SysService_SswScc */

/* Scc API IDs */
#define SCC_API_ID_INIT                       0x01u /* Scc_Init() */
#define SCC_API_ID_MAIN_FUNCTION              0x02u /* Scc_MainFunction() */
#define SCC_API_ID_RX_INDICATION              0x03u /* Scc_Cbk_TL_RxIndication() */
#define SCC_API_ID_IP_ADDRESS_CHG_CBK         0x04u /* Scc_Cbk_LocalIpAssignmentChg() */
#define SCC_API_ID_GET_VERSION_INFO           0x05u /* Scc_GetVersionInfo() */
#define SCC_API_ID_DIAG_DATA_READ_ACCESS      0x06u /* Scc_DiagDataReadAccess() */
#define SCC_API_ID_DIAG_DATA_WRITE_ACCESS     0x07u /* Scc_DiagDataWriteAccess() */
#define SCC_API_ID_TCP_CONNECTED              0x08u /* Scc_TcpConnected() */
#define SCC_API_ID_TCP_ACCEPTED               0x09u /* Scc_TcpAccepted() */
#define SCC_API_ID_TCP_IP_EVENT               0x0Au /* Scc_TcpIpEvent() */
#define SCC_API_ID_TCP_IP_CERT                0x0Bu /* Scc_Cbk_TLS_Cert() */

#define SCC_API_ID_V_TRIG_MSG                 0x20u /* Scc_TriggerV2G() */
#define SCC_API_ID_V_TIMEOUT_HANDLING         0x21u /* Scc_TimeoutHandling() */
#define SCC_API_ID_V_NVM_READ_ROOT_CERTS      0x22u /* Scc_NvmReadRootCerts() */
#define SCC_API_ID_V_CHECK_V2G_HEADER         0x23u /* Scc_CheckV2GHeader() */

#define SCC_API_ID_V_VALIDATE_CERT_CHAIN      0x25u /* Scc_ValidateCertChain() */
#define SCC_API_ID_V_TRIG_NVM                 0x26u /* Scc_TriggerNVRAM() */
#define SCC_API_ID_V_ADJUST_AMOUNT_2_MULT     0x27u /* Scc_AdjustAmountToMultiplier() */
#define SCC_API_ID_V_REPORT_ERROR             0x28u /* Scc_ReportError() */
#define SCC_API_ID_V_INIT                     0x29u /* Scc_Init() */

#define SCC_API_ID_V_GET_CERT_DN              0x2Cu /* Scc_GetCertDistinguishedNameObject() */
#define SCC_API_ID_V_GET_CERT_ISSUER          0x2Du /* Scc_GetCertIssuer() */
#define SCC_API_ID_V_ADJUST_VALUE_2_MULT      0x2Eu /* Scc_AdjustAmountToMultiplier() */

#define SCC_API_ID_V_EXI_DECODE_SAP_RES       0x40u /* Scc_Exi_DecodeSupportedAppProtocolRes() */

#define SCC_API_ID_V_EXITX_INV_ENERG_TRANS_M  0x41u /* invalid energy transfer mode selected */
#define SCC_API_ID_V_EXIRX_ISO_DECODE_MESSAGE 0x42u /* Scc_ExiRx_ISO_DecodeMessage() */
#define SCC_API_ID_V_EXIRX_DIN_DECODE_MESSAGE 0x43u /* Scc_ExiRx_DIN_DecodeMessage() */
#define SCC_API_ID_V_CERTIFICATE_UPDATE_RES   0x44u /* Scc_ExiRx_ISO_CertificateUpdateRes() */
#define SCC_API_ID_V_CERTIFICATE_INSTALL_RES  0x45u /* Scc_ExiRx_ISO_CertificateInstallationRes() */
#define SCC_API_ID_V_EXITX_ISO_ENCODE_MESSAGE 0x46u /* Scc_ExiTx_ISO_EncodeMessage() */
#define SCC_API_ID_V_EXITX_DIN_ENCODE_MESSAGE 0x47u /* Scc_ExiTx_DIN_EncodeMessage() */
#define SCC_API_ID_V_EXI_INIT_WS              0x48u /* Scc_Exi_Init(En/De)codingWorkspace() */
#define SCC_API_ID_V_EXI_FIN_EXI_STREAM       0x49u /* Scc_Exi_EncodeExiStream() */
#define SCC_API_ID_V_EXI_WRITE_HEADER         0x4Au /* Scc_Exi_WriteHeader() */

#define SCC_API_ID_V_EXI_ISO_CURRENT_DEMAND   0x4Bu /* Scc_ExiRx_ISO_CurrentDemandRes() */
#define SCC_API_ID_V_EXI_ISO_CHARGING_STATUS  0x4Cu /* Scc_ExiRx_ISO_ChargingStatusRes() */
#define SCC_API_ID_V_CERTIFICATE_UPDATE_REQ   0x4Du /* Scc_ExiRx_ISO_CertificateUpdateRes() */
#define SCC_API_ID_V_CERTIFICATE_INSTALL_REQ  0x4Eu /* Scc_ExiRx_ISO_CertificateInstallationRes() */
#define SCC_API_ID_V_AUTHORIZATION_REQ        0x4Fu /* Scc_ExiTx_ISO_AuthorizationReq() */
#define SCC_API_ID_V_METERING_RECEIPT_REQ     0x50u /* Scc_ExiTx_ISO_MeteringReceiptReq() */
#define SCC_API_ID_V_CBK_COPY_TX_DATA         0x51u /* Scc_Cbk_CopyTxData() */
#define SCC_API_ID_V_POWER_DELIVERY_REQ       0x52u /* Scc_ExiTx_ISO_PowerDeliveryReq() */

#if ( SCC_ENABLE_STATE_MACHINE == STD_ON )
#define SCC_API_ID_V_STATEM_MAIN_FUNCTION     0xA0u /* Scc_StateM_MainFunction() */
#define SCC_API_ID_V_STATEM_GLOBAL_TIMER      0xA1u /* Scc_StateM_VGlobalTimerChecks () */
#define SCC_API_ID_V_STATEM_PREP_SERV_DETAIL  0xA2u /* Scc_StateM_PrepareServiceDetailReq() */
#define SCC_API_ID_V_STATEM_SET_MSG_STATUS    0xA3u /* Scc_StateM_Set_Core_MsgStatus() */
#define SCC_API_ID_V_STATEM_PROC_CHARGE_PARAM 0xA4u /* Scc_StateM_ProcessChargeParameterDiscoveryRes() */
#define SCC_API_ID_V_STATEM_PROC_SERV_DISC    0xA5u /* Scc_StateM_ProcessServiceDiscoveryRes() */
#define SCC_API_ID_V_STATEM_PROC_POW_DEL      0xA6u /* Scc_StateM_ProcessPowerDeliveryRes() */
#define SCC_API_ID_V_STATEM_CORE_GET_CBK      0xA9u /* Scc_StateM_Get_Xyz_Zyx() */
#endif /* SCC_ENABLE_STATE_MACHINE */

/* SCC DET errors */
#define SCC_DET_NO_ERROR         0x00u /* No error occurred */
#define SCC_DET_NOT_INITIALIZED  0x01u /* This module is not initialized yet */
#define SCC_DET_INV_POINTER      0x02u /* The provided pointer(s) is/are invalid */
#define SCC_DET_INV_PARAM        0x03u /* The provided parameter(s) is/are invalid */
#define SCC_DET_INV_STATE        0x04u /* The state machine ended up in an invalid state */
#define SCC_DET_EXI              0x05u /* An error occurred during an interaction with Exi */
#define SCC_DET_XML_SEC          0x06u /* An error occurred during an interaction with XmlSecurity */
#define SCC_DET_CRYPTO           0x07u /* An error occurred during an interaction with Crypto */
#define SCC_DET_IP_BASE          0x08u /* An error occurred during an interaction with IpBase */
#define SCC_DET_TL               0x09u /* An error occurred during an interaction with the transport layer (TLS/TCP) */
#define SCC_DET_EXT_CMP_NOT_INIT 0x0Au /* An external component was not initialized */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/
/* Read 3 bytes out of a buffer into a uint32 variable. Implementation is similar to IPBASE_GET_UINT32() and TLS_GET_UINT24. */
/* PRQA S 3453 3 */ /* MD_MSR_FctLikeMacro */
#define SCC_GET_UINT24(Buffer, Offset, Variable) ( (Variable) = (uint32)(((uint32)((Buffer)[(Offset)])    << 16u) | \
                                                                         ((uint32)((Buffer)[(Offset)+1u]) <<  8u) | \
                                                                         ((uint32)((Buffer)[(Offset)+2u]))) )

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/
/* 8bit variables - zero init for the NvM */
#define SCC_START_SEC_VAR_ZERO_INIT_8BIT_NVM
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(uint8, SCC_VAR_ZERO_INIT) Scc_SessionIDNvm[SCC_SESSION_ID_NVM_BLOCK_LEN];

#define SCC_STOP_SEC_VAR_ZERO_INIT_8BIT_NVM
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/* 8bit variables - const - nvm */
#define SCC_START_SEC_CONST_8BIT_NVM
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern CONST(uint8, SCC_CONST) Scc_SessionIDNvmRomDefault[SCC_SESSION_ID_NVM_BLOCK_LEN];

#define SCC_STOP_SEC_CONST_8BIT_NVM
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/* other variables */
#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Scc_SDPSecurityType, SCC_VAR_NOINIT) Scc_Security;

#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Scc_GetVersionInfo
 *********************************************************************************************************************/
/*! \brief          Returns the version information.
 *  \details        Scc_GetVersionInfo() returns version information, vendor ID and AUTOSAR module ID of the component.
 *                  The versions are BCD-coded.
 *  \param[in]      VersionInfoPtr        pointer for version info
 *  \pre            -
 *  \context        TASK|ISR2
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *  \config         This function is only available if SccVersionInfoApi is enabled.
 *  \trace          CREQ-133185
 *********************************************************************************************************************/
#if ( SCC_VERSION_INFO_API == STD_ON )
FUNC(void, SCC_CODE) Scc_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC, SCC_APPL_DATA) VersionInfoPtr);
#endif /* SCC_VERSION_INFO_API == STD_ON */

/**********************************************************************************************************************
 *  Scc_InitMemory
 *********************************************************************************************************************/
/*! \brief          initializes global variables
 *  \details        Initializes component variables in *_INIT_* sections at power up.
 *  \pre            has to be called before any other calls to the module
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \note           This function shall be called before Scc_Init.
 *  \trace          CREQ-133183
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_InitMemory(void);

/**********************************************************************************************************************
 *  Scc_Init
 *********************************************************************************************************************/
/*! \brief          Initialization function
 *  \details        This function initializes module global variables at power up. This function initializes the
 *                  variables in *_INIT_* sections. Used in case they are not initialized by the startup code
 *  \pre            has to be called before useage of the module
 *  \pre            Interrupts are disabled.
 *  \pre            Module is uninitialized.
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \note           Specification of module initialization.
 *  \trace          CREQ-133183
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_Init(void);

/**********************************************************************************************************************
 *  Scc_MainFunction
 *********************************************************************************************************************/
/*! \brief          main function of this module, has to be called cyclically
 *  \details        Periodically called MainFunction (typically 10-20 ms) that handles the Scc message triggers,
 *                  re-initialize closed connections and transmit pending data. Make sure to call TcpIp_MainFunction
 *                  twice as often as Scc_MainFunction.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
/* FUNC(void, SCC_CODE) Scc_MainFunction(void); */

/**********************************************************************************************************************
 *  Scc_ResetSessionID
 *********************************************************************************************************************/
/*! \brief          Resets the SessionID to '0x00'
 *  \details        Reset the SessionID to 0x00 and the length to 0x01 in the NvM.
 *  \pre            will only have effect outside of a V2G session
 *  \param[in]      ForceReset      force the reset, even when a V2G Communication Session is ongoing
 *  \return         E_OK            SessionID has been reset
 *  \return         E_NOT_OK        a V2G session is currently active, please try again later
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-174851
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ResetSessionID(boolean ForceReset);

#if ( SCC_NUM_OF_DYN_CONFIG_PARAMS != 0 )
/**********************************************************************************************************************
 *  Scc_DynConfigDataReadAccess
 *********************************************************************************************************************/
/*! \brief          configuration data read access
 *  \details        Gets the configuration values of the Timer (DIN/ISO_Ed1/ISO_Ed2) and the Vector StateMachine
 *  \param[in]      DataID         data identifier
 *  \param[out]     DataPtr        pointer for diagnostic data
 *  \return         E_OK           configuration data was successfully read
 *  \return         E_NOT_OK       invalid DataID
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-163493
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_DynConfigDataReadAccess(Scc_DynConfigParamsType DataID,
  P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) DataPtr);
#endif /* SCC_NUM_OF_DYN_CONFIG_PARAMS */

#if ( SCC_NUM_OF_DYN_CONFIG_PARAMS != 0 )
/**********************************************************************************************************************
 *  Scc_DynConfigDataWriteAccess
 *********************************************************************************************************************/
/*! \brief          configuration data write access
 *  \details        Sets the configuration values of the Timer (DIN/ISO_Ed1/ISO_Ed2) and the Vector StateMachine
 *  \param[in]      DataID         data identifier
 *  \param[in]      Data           configuration data that shall be written to NVRAM
 *  \return         E_OK           configuration data was successfully written
 *  \return         E_NOT_OK       invalid DataID
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-163493
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_DynConfigDataWriteAccess(Scc_DynConfigParamsType DataID, uint16 Data);
#endif /* SCC_NUM_OF_DYN_CONFIG_PARAMS */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_DiagDataReadAccess
 *********************************************************************************************************************/
/*! \brief          diagnostic data read access
 *  \details        Gets the value of the ContractCertificateChainSize, Contract Certificat, EMAID,
 *                  Contract Sub Certificates, Provisioning Certificate, PCID and Root Certificates.
 *  \param[in]      DataID         data identifier
 *  \param[out]     DataPtr        pointer for diagnostic data
 *  \param[in,out]  DataLenPtr     pointer for maximum / actual length of diagnostic data in bytes
 *  \return         OK             diagnostic data was successfully read
 *  \return         Pending        the requested data is currently being read from NVRAM
 *  \return         NotOK          an error occurred
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    FALSE
 *  \trace          CREQ-133187
 *********************************************************************************************************************/
FUNC(Scc_ReturnType, SCC_CODE) Scc_DiagDataReadAccess(Scc_DiagParamsType DataID,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) DataLenPtr);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_DiagDataWriteAccess
 *********************************************************************************************************************/
/*! \brief          diagnostic data write access
 *  \details        Sets the value of the ContractCertificateChainSize, Contract Certificat, EMAID,
 *                  Contract Sub Certificates, Provisioning Certificate, PCID and Root Certificates.
 *  \param[in]      DataID           data identifier
 *  \param[in]      DataPtr          pointer with address of the diagnostic data
 *  \param[in]      DataLen          length of the diagnostic data in bytes
 *  \return         OK               diagnostic data written
 *  \return         NotOK            invalid parameter (data identifier not found, NULL_PTR parameter, invalid length)
 *  \return         Pending          NvM is currently still processing, keep calling this API
 *  \return         Busy             diagnostic job currently not possible, try again later
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    FALSE
 *  \trace          CREQ-133187
 *********************************************************************************************************************/
FUNC(Scc_ReturnType, SCC_CODE) Scc_DiagDataWriteAccess(Scc_DiagParamsType DataID,
  P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, uint16 DataLen);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_DiagDataGetBlockStatus
 *********************************************************************************************************************/
/*! \brief          provides information about the current status of the NvM block of the diag data
 *  \details        Gets the status of the NvM Block of the ContractCertificateChainSize, Contract Certificat,
 *                  Contract Sub Certificates, Provisioning Certificate, Root Certificates.
 *  \param[in]      DataID          data identifier
 *  \param[out]     NvmResultPtr    current status of the requested block (NvM_RequestResultType)
 *  \return         E_OK            operation was successful
 *  \return         E_NOT_OK        invalid DataID or certificate index
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-163494
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_DiagDataGetBlockStatus(Scc_DiagParamsType DataID,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) NvmResultPtr);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_TLS == STD_ON )
/**********************************************************************************************************************
 *  Scc_GetCertSize()
 *********************************************************************************************************************/
/*! \brief          Get size of certificate.
 *  \details        -
 *  \param[in]      CertPtr               Pointer to the certificate.
 *  \param[in,out]  CertLen               Length of the certificate (output) and length of the input data (CertPtr).
 *  \return         E_OK                  Certificate BER-decoding was successful.
 *  \return         E_NOT_OK              Certificate BER-decoding failed.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-174855
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_GetCertSize(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr,
                                               P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) CertLenPtr);
#endif /* SCC_ENABLE_TLS */

#if ( SCC_ENABLE_TLS == STD_ON )
/**********************************************************************************************************************
 *  Scc_GetCertDistinguishedNameObject()
 *********************************************************************************************************************/
/*! \brief          Get distinguished name from certificate.
 *  \details        -
 *  \param[in]      CertPtr               Pointer to the certificate.
 *  \param[in]      CertLen               Length of the certificate.
 *  \param[out]     DataPtr               Pointer to the output buffer.
 *  \param[in,out]  DataLenPtr            Length of the output buffer.
 *  \param[in]      ObjectID              Object of the distinguished name element in the certificate.
 *  \return         E_OK                  Certificate BER-decoding and data copying was successful.
 *  \return         E_NOT_OK              ObjectID was not found in Certificate or BER-encode error.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-174854
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_GetCertDistinguishedNameObject(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr, uint16 CertLen,
                                                                  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) DataLenPtr, Scc_BERObjectIDsType ObjectID);
#endif /* SCC_ENABLE_TLS */

#if ( SCC_ENABLE_TLS == STD_ON )
/**********************************************************************************************************************
 *  Scc_GetCertSubjectRaw()
 *********************************************************************************************************************/
/*! \brief          Get subject from certificate.
 *  \details        -
 *  \param[in]      CertPtr               Pointer to the certificate.
 *  \param[in]      CertLen               Length of the certificate.
 *  \param[out]     DataPtr               Pointer to the output buffer.
 *  \param[in,out]  DataLenPtr            Length of the output buffer.
 *  \return         E_OK                  Certificate BER-decoding and data copying was successful.
 *  \return         E_NOT_OK              Certificate BER-decoding or data copying failed.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-174856
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_GetCertSubjectRaw(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr, uint16 CertLen,
                                                     P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) DataLenPtr);
#endif /* SCC_ENABLE_TLS */

#if ( SCC_ENABLE_TLS == STD_ON )
/**********************************************************************************************************************
 *  Scc_SearchDomainComponentForValue()
 *********************************************************************************************************************/
/*! \brief          Checks if domain component of the certificate matches the provided value.
 *  \details        -
 *  \param[in]      CertPtr               Pointer to the certificate.
 *  \param[in]      CertLen               Length of the certificate.
 *  \param[in]      ValuePtr              Pointer to the value.
 *  \param[in]      ValueLen              Length of the value.
 *  \return         E_OK                  Provided value was found in one of the domain component fields.
 *  \return         E_NOT_OK              Provided value was not found, certificate could have an invalid encoding.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-216995
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_SearchDomainComponentForValue(P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) CertPtr, uint16 CertLen, P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) ValuePtr, uint8 ValueLen);
#endif /* SCC_ENABLE_TLS */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_CheckContrCertValidity
 *********************************************************************************************************************/
/*! \brief          check the validity of Contract Certificate
 *  \details        check if the currently installed certificate is expired or will expire soon.
 *  \param[in]      DateTimeNow            current time in Unix-Format
 *  \param[in]      DateTimeThreshold      a time (unix) in the future to check whether the certificate expires until then
 *  \param[out]     ContrCertStatus        current status of the contract certificate
 *  \return         OK                     Checked certifcate validity successfully
 *  \return         Pending                NvM block is busy, keep calling this API
 *  \pre            the certificates have to be read from the NvM
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-163905
 *********************************************************************************************************************/
FUNC(Scc_ReturnType, SCC_CODE) Scc_CheckContrCertValidity(uint32 DateTimeNow, uint32 DateTimeThreshold,
  P2VAR(Scc_CertificateStatusType, AUTOMATIC, SCC_VAR_NOINIT) ContrCertStatus);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_CheckProvCertValidity
 *********************************************************************************************************************/
/*! \brief          check the validity of Provisioning Certificate
 *  \details        check if the currently installed certificate is expired or will expire soon.
 *  \param[in]      DateTimeNow            current time in Unix-Format
 *  \param[in]      DateTimeThreshold      a time (unix) in the future to check whether the certificate expires until then
 *  \param[out]     ProvCertStatus         current status of the provisioning certificate
 *  \return         OK                     Checked certifcate validity successfully
 *  \return         Pending                NvM block is busy, keep calling this API
 *  \pre            the certificates have to be read from the NvM
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-163905
 *********************************************************************************************************************/
FUNC(Scc_ReturnType, SCC_CODE) Scc_CheckProvCertValidity(uint32 DateTimeNow, uint32 DateTimeThreshold,
  P2VAR(Scc_CertificateStatusType, AUTOMATIC, SCC_VAR_NOINIT) ProvCertStatus);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_SwapContrCertChain
 *********************************************************************************************************************/
/*! \brief          swap the used contract certificate chain slot
 *  \details        Swap ContractCertificate and reset read states.
 *  \param[in]      NewContractCertificateChainIndex   new slot index
 *  \param[in]      ForceSwap                          force to swap the chain (i.e. reset the current chain status)
 *  \return         OK                                 chain swapped successfully
 *  \return         Busy                               a V2G session is active,  swap is currently not possible
 *  \return         NotOK                              invalid chain index
 *  \pre            not available during an active V2G session, unless ForceSwap is used
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-174857
 *********************************************************************************************************************/
FUNC(Scc_ReturnType, SCC_CODE) Scc_SwapContrCertChain(uint8 NewContractCertificateChainIndex, boolean ForceSwap);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ValidateContractCertChain()
 *********************************************************************************************************************/
/*! \brief         Checks if the Contract Certificate Chain is valid.
 *  \details        -
 *  \param[in]      CurrentTime           Current time.
 *  \return         E_OK                  Certificate chain is valid.
 *  \return         E_NOT_OK              Certificate is expired, BER decoding failed or signature invalid.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-174853
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ValidateContractCertChain(uint32 CurrentTime);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ValidateCertChain()
 *********************************************************************************************************************/
/*! \brief          Checks if the provided certificate chain is valid.
 *  \details        -
 *  \param[in]      CertChainPtr          Pointer to the certificate chain.
 *  \param[in]      CurrentTime           Current time.
 *  \return         E_OK                  Certificate chain is valid.
 *  \return         E_NOT_OK              Certificate is expired, BER decoding failed or signature invalid.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-174853
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_ValidateCertChain(P2CONST(Exi_ISO_certificateType, AUTOMATIC, SCC_VAR_NOINIT) CertChainPtr,
  uint32 CurrentTime);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ResetNvMBlockStatus
 *********************************************************************************************************************/
/*! \brief          Reset the status of the NvM read status.
 *  \details        Reset the status of a RAM block of an NvM block, so it can be read again
 *  \param[in]      NvMBlock        NvM block of which the status shall be reset
 *  \return         OK              status was reset, NvM block can be read again from NVRAM
 *  \return         Busy            a V2G session is currently active, try again later or stop charging
 *  \return         Pending         NvM block is busy, keep calling this API
 *  \return         NotOK           invalid NvMBlock or index (Sub or Root Certificates) selected
 *  \pre            will only have effect outside of a V2G session
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    FALSE
 *  \trace          CREQ-163495
 *********************************************************************************************************************/
FUNC(Scc_ReturnType, SCC_CODE) Scc_ResetNvMBlockStatus(Scc_DiagParamsType NvMBlock);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_LoadAllCertificates
 *********************************************************************************************************************/
/*! \brief          Loads all certificates
 *  \details        Triggers to loads all installed certificates (Contract, Intermediate Provisioning and Root
 *                  Certificates). This function can be called before calling the function Scc_DiagDataReadAccess() to
 *                  reduce the waiting time for the function Scc_DiagDataReadAccess().
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    FALSE
 *  \trace          CREQ-174852
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_LoadAllCertificates(void);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_GetContractCertChainIndexInUse
 *********************************************************************************************************************/
/*! \brief          returns index of currently used contract certificate chain
 *  \details        -
 *  \param[out]     IndexOfUsedContractCertChain       index of currently used contract certificate chain
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \trace          CREQ-174857
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_GetContractCertChainIndexInUse(
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) IndexOfUsedContractCertChain);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_DeleteContract
 *********************************************************************************************************************/
/*! \brief          Deltes the Contract Certificate
 *  \details        deletes the Contract Certificate including all sub certificates and the private key
 *  \param[in]      ContractIdx    the slot of the contract certificate that shall be deleted, starts with '0'
 *  \param[in]      ForceDelete    If this value is true, an active V2G-Session will be ignored
 *  \return         OK             contract deleted successfully
 *  \return         Busy           a V2G session is currently active, wait until it is finished or stop charging
 *  \return         NotOK          an error occurred
 *  \pre            not available during an active V2G session
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    FALSE
 *  \trace          CREQ-133189
 *********************************************************************************************************************/
FUNC(Scc_ReturnType, SCC_CODE) Scc_DeleteContract(uint8 ContractIdx, boolean ForceDelete);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_DeleteRootCert
 *********************************************************************************************************************/
/*! \brief          Deletes the selected Root Certificate
 *  \details        Invalidate the NvM block of the RootCertificate
 *  \param[in]      RootCertIdx    the slot of the root certificate that shall be deleted, starts with '0'
 *  \return         OK             root certificate deleted successfully
 *  \return         Busy           a V2G session is currently active, wait until it is finished or stop charging
 *  \return         NotOK          an error occurred
 *  \pre            not available during an active V2G session
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    FALSE
 *  \trace          CREQ-133189
 *********************************************************************************************************************/
FUNC(Scc_ReturnType, SCC_CODE) Scc_DeleteRootCert(uint8 RootCertIdx);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_DeleteProvCert
 *********************************************************************************************************************/
/*! \brief          Deletes the Provisioning Certificate related to the used contract certificate
 *  \details        Invalidate the NvM block of the ProvCertificate
 *  \return         OK             Prov certificate deleted successfully
 *  \return         Busy           a V2G session is currently active, wait until it is finished or stop charging
 *  \return         NotOK          an error occurred
 *  \pre            not available during an active V2G session
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    FALSE
 *  \trace          CREQ-133189
 *********************************************************************************************************************/
FUNC(Scc_ReturnType, SCC_CODE) Scc_DeleteProvCert(void);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ValidateContrCertKeyPair
 *********************************************************************************************************************/
/*! \brief          validates the contract certificate by checking the private key against the public key
 *  \details        Sign data with the private Key and verify the siganture with the public key of the certificate
 *  \return         OK             private and public key pair validated successfully
 *  \return         Pending        contract certificate or its private key are still loaded from NVRAM
 *  \return         NotOK          NvM read error, or public and private key are no pair
 *  \pre            contract certificate has to be read from NVRAM
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    FALSE
 *  \trace          CREQ-163496
 *********************************************************************************************************************/
FUNC(Scc_ReturnType, SCC_CODE) Scc_ValidateContrCertKeyPair(void);
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
/**********************************************************************************************************************
 *  Scc_ValidateProvCertKeyPair
 *********************************************************************************************************************/
/*! \brief          validates the provisioning certificate by checking the private key against the public key
 *  \details        Sign data with the private Key and verify the siganture with the public key of the certificate
 *  \return         OK             private and public key pair validated successfully
 *  \return         Pending        provisioning certificate or its private key are still loaded from NVRAM
 *  \return         NotOK          NvM read error, or public and private key are no pair
 *  \pre            provisioning certificate has to be read from NVRAM
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    FALSE
 *  \trace          CREQ-163496
 *********************************************************************************************************************/
FUNC(Scc_ReturnType, SCC_CODE) Scc_ValidateProvCertKeyPair(void);
#endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON ) && (defined TLS_SUPPORT_GET_NVM_BLOCK_ID_FOR_USED_ROOT_CERT && (TLS_SUPPORT_GET_NVM_BLOCK_ID_FOR_USED_ROOT_CERT == STD_ON))
/**********************************************************************************************************************
 *  Scc_Get_RootCertUsed
 *********************************************************************************************************************/
/*! \brief          Returns the root certificate index used for Tls handshake
 *  \details        -
 *  \param[out]     RootCertId     Root certificate index used for Tls handshake
 *  \return         OK             Fetching root cert Id was successful
 *  \return         NotOK          Error while fetching root cert Id
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    FALSE
 *  \trace          CREQ-217361
 *********************************************************************************************************************/
FUNC(Std_ReturnType, SCC_CODE) Scc_Get_RootCertUsed(P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) RootCertId);
#endif /* SCC_ENABLE_PNC_CHARGING, TLS_SUPPORT_GET_NVM_BLOCK_ID_FOR_USED_ROOT_CERT */

#define SCC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* SCC_H */
/**********************************************************************************************************************
 *  END OF FILE: Scc.h
 *********************************************************************************************************************/
