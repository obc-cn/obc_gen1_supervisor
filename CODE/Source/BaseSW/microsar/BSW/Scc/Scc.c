/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc.c
 *        \brief  Smart Charging Communication Source Code File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/
#define SCC_SOURCE

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
/* PRQA S 0777 EOF */ /* MD_MSR_Rule5.1 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Scc.h"
#include "Scc_Priv.h"
#include "Scc_Cbk.h"
#include "Scc_Cfg.h"
#include "Scc_Lcfg.h"
#include "Scc_Exi.h"
#include "Scc_Interface_Cfg.h"
#include "Scc_ConfigParams_Cfg.h"


#include "SchM_Scc.h"

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
#  include "Csm.h"
#endif /* SCC_ENABLE_PNC_CHARGING */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
#include "Det.h"
#endif /* SCC_DEV_ERROR_DETECT */
#include "EthIf_Types.h"
#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
#include "EthTrcv_30_Ar7000.h"
#endif /* SCC_ENABLE_SLAC_HANDLING */
#include "IpBase.h"
#include "TcpIp_IpV6.h"
#include "NvM.h"
#include "TcpIp.h"
#if ( SCC_ENABLE_TLS == STD_ON )
#include "Tls.h"
#endif /* SCC_ENABLE_TLS */

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/
/* Check consistency of source and header file. */
#if ( (SCC_SW_MAJOR_VERSION != 0x14u) || (SCC_SW_MINOR_VERSION != 0x00u) || (SCC_SW_PATCH_VERSION != 0x02u) )
  #error "Scc.c: Source and Header file are inconsistent!"
#endif

/**********************************************************************************************************************
 *  MISRA & PClint
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined (SCC_LOCAL_INLINE)
# define SCC_LOCAL_INLINE LOCAL_INLINE
#endif

#if !defined (SCC_LOCAL)
# define SCC_LOCAL static
#endif

#define SCC_V2GTP_HDR_PAYLOAD_LEN_OFFSET     4U
#define SCC_SDP_TP_TCP                    0x00U
#define SCC_SDP_REQ_LEN                     10U
#define SCC_SDP_RES_LEN                     28U
#define SCC_SDP_MULTCAST_IP_ADDR         { 0xFF, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, \
                                           0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01 }

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3458 MACROS_BRACES */ /* MD_Scc_20.4 */
/* PRQA S 3453 MACROS_FUNCTION_LIKE */ /* MD_MSR_FctLikeMacro */

#define Scc_MsgStateFct(MsgStateNew) \
  { Scc_MsgState = (MsgStateNew); Scc_Set_Core_MsgState(MsgStateNew); }

/* PRQA L:MACROS_BRACES */
/* PRQA L:MACROS_FUNCTION_LIKE */
/**********************************************************************************************************************
 *  LOCAL / GLOBAL DATA
 *********************************************************************************************************************/

/* 8bit variables - no init */
#define SCC_START_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

SCC_LOCAL VAR(boolean, SCC_VAR_NOINIT) Scc_CyclicMsgTrig;
SCC_LOCAL VAR(uint8, SCC_VAR_NOINIT)   Scc_ExiStreamRxPBufIdx;
SCC_LOCAL VAR(boolean, SCC_VAR_NOINIT) Scc_V2GSocketInUse;

VAR(boolean, SCC_VAR_NOINIT) Scc_TxStreamingActive;

#define SCC_STOP_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* 16bit variables no init */
#define SCC_START_SEC_VAR_NOINIT_16BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

VAR(uint16, SCC_VAR_NOINIT) Scc_TimeoutCnt;
SCC_LOCAL VAR(uint16, SCC_VAR_NOINIT)  Scc_SdpRetransRetriesCnt;

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
SCC_LOCAL VAR(uint16, SCC_VAR_NOINIT) Scc_QCAIdleTimer;
#endif /* SCC_ENABLE_SLAC_HANDLING */

#define SCC_STOP_SEC_VAR_NOINIT_16BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* 32bit variables */
#define SCC_START_SEC_VAR_NOINIT_32BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

VAR(uint32, SCC_VAR_NOINIT) Scc_TxDataSent;

#define SCC_STOP_SEC_VAR_NOINIT_32BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* other variables */
#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

SCC_LOCAL VAR(Scc_MsgTrigType, SCC_VAR_NOINIT)  Scc_MsgTrigNew;
SCC_LOCAL VAR(Scc_MsgStateType, SCC_VAR_NOINIT) Scc_MsgState;

VAR(Scc_MsgTrigType, SCC_VAR_NOINIT)     Scc_MsgTrig;
VAR(Scc_MsgStatusType, SCC_VAR_NOINIT)   Scc_MsgStatus;
VAR(Scc_StackErrorType, SCC_VAR_NOINIT)  Scc_StackError;
SCC_LOCAL VAR(Scc_SDPSecurityType, SCC_VAR_NOINIT) Scc_SelectedConnectionType; /* PRQA S 3218 */ /* MD_Scc_3218 */

VAR(Scc_PbufType, SCC_VAR_NOINIT)   Scc_ExiStreamRxPBuf[SCC_EXI_RX_STREAM_PBUF_ELEMENTS];

VAR(Scc_SockAddrIn6Type, SCC_VAR_NOINIT) Scc_ServerSockAddr;

VAR(TcpIp_SocketIdType, SCC_VAR_NOINIT) Scc_SDPSocket;
VAR(TcpIp_SocketIdType, SCC_VAR_NOINIT) Scc_V2GSocket;

VAR(Scc_SDPSecurityType, SCC_VAR_NOINIT) Scc_Security;

#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* 8bit variables - zero init */
#define SCC_START_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

SCC_LOCAL VAR(uint8, SCC_VAR_ZERO_INIT) Scc_IPAddressCnt = 0;

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
SCC_LOCAL VAR(EthTrcv_LinkStateType, SCC_VAR_ZERO_INIT)          Scc_DLinkReady = (EthTrcv_LinkStateType)0u;
#endif /* SCC_ENABLE_SLAC_HANDLING */

#define SCC_STOP_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* 8bit variables - zero init for the NvM */
#define SCC_START_SEC_VAR_ZERO_INIT_8BIT_NVM
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

VAR(uint8, SCC_VAR_ZERO_INIT) Scc_SessionIDNvm[SCC_SESSION_ID_NVM_BLOCK_LEN] = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

#define SCC_STOP_SEC_VAR_ZERO_INIT_8BIT_NVM
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* unspecified variables - zero init */
#define SCC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
SCC_LOCAL VAR(Scc_FirmwareDownloadStatusType, SCC_VAR_ZERO_INIT) Scc_QCAFirmwareDownloadComplete = Scc_FirmwareDownloadStatus_Unknown;
#endif /* SCC_ENABLE_SLAC_HANDLING */

VAR(Scc_StateType, SCC_VAR_ZERO_INIT) Scc_State = Scc_State_Uninitialized;

#define SCC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* 8bit variables - const - nvm */
#define SCC_START_SEC_CONST_8BIT_NVM
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

CONST(uint8, SCC_CONST) Scc_SessionIDNvmRomDefault[SCC_SESSION_ID_NVM_BLOCK_LEN] = {
  0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

#define SCC_STOP_SEC_CONST_8BIT_NVM
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

 /**********************************************************************************************************************
 *  Scc_Reset
 *********************************************************************************************************************/
 /*! \brief         Reset the Scc Module and all global parameters.
 *  \details        Close UDP and TCP socket and reset all state and message relevant parameters
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    FALSE
 *********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_Reset(void);

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
*  Scc_TriggerSLAC
*********************************************************************************************************************/
/*! \brief         Starts SLAC handling
*  \details        Checks if link is established and firmware download is finished and starts then SLAC.
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    FALSE
*********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_TriggerSLAC(void);
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
*  Scc_TriggerNVRAM
*********************************************************************************************************************/
/*! \brief         Read the certificates from the NvM
*  \details        Read the root, contract and provisioning certificates from the NvM, depending on the Message Trigger.
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    FALSE
*********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_TriggerNVRAM(void);
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
*  Scc_TriggerV2G
*********************************************************************************************************************/
/*! \brief         Triggers the transmit of a V2G Message
*  \details        Transmit the V2G message depending on the Message Trigger
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    FALSE
*********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_TriggerV2G(void);

/**********************************************************************************************************************
*  Scc_TimeoutHandling
*********************************************************************************************************************/
/*! \brief         Check if a timeout occur
*  \details        Check if a timeout occur and report the timeout.
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    FALSE
*********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_TimeoutHandling(void);

/**********************************************************************************************************************
*  Scc_TxSECCDiscoveryProtocolReq
*********************************************************************************************************************/
/*! \brief         Transmit SECCDiscoveryProtocol message.
*  \details        Create SECCDiscoveryProtocol and transmit message.
*  \return         E_OK            Udp send whole message
*  \return         E_NOT_OK        Udp did not accepted the request
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    FALSE
*********************************************************************************************************************/
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_TxSECCDiscoveryProtocolReq(void);

/**********************************************************************************************************************
*  Scc_RxSECCDiscoveryProtocolRes
*********************************************************************************************************************/
/*! \brief         Receive SECCDiscoveryProtocol message.
*  \details        Receive and process SECCDiscoveryProtocol message.
*  \return         E_OK            Received valid message
*  \return         E_NOT_OK        Option for the transport protocol and security is not correct
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    FALSE
*********************************************************************************************************************/
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_RxSECCDiscoveryProtocolRes(void);

/**********************************************************************************************************************
*  Scc_CheckV2GHeader
*********************************************************************************************************************/
/*! \brief         Checks the V2G Header
*  \details        Checks if the V2G Protocol Version and payload type is correct.
*  \param[in]      Socket          Socket Id for SDP and V2G Socket
*  \return         E_OK            V2G header is valid
*  \return         E_NOT_OK        V2G Protocol Version or payload type is not correct.
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    FALSE
*********************************************************************************************************************/
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_CheckV2GHeader(Scc_SocketType Socket);

/**********************************************************************************************************************
*  Scc_EstablishTLConnection
*********************************************************************************************************************/
/*! \brief         Configure the Tcp socket
*  \details        Configure the Rx/Tx buffer for TCP or TLS. Binds the socket and starts connection.
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    FALSE
*********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_EstablishTLConnection(void);

/**********************************************************************************************************************
*  Scc_TransmitExi
*********************************************************************************************************************/
/*! \brief         Trigger Exi transmit
*  \details        Transmit the data and reset the length of the sned data.
*  \param[in]      void
*  \return         E_OK            Tcp send whole message
*  \return         PENDING         message did not fit into first Tcp package
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    FALSE
*********************************************************************************************************************/
SCC_LOCAL FUNC(Scc_ReturnType, SCC_CODE) Scc_TransmitExi(void);

/**********************************************************************************************************************
*  Scc_TcpTransmit
*********************************************************************************************************************/
/*! \brief         Trigger TcpIp transmit
*  \details        Transmit the whole message or return pending if message needs to be streamed.
*  \return         E_OK            Tcp send whole message
*  \return         PENDING         message did not fit into first Tcp package
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    FALSE
*********************************************************************************************************************/
SCC_LOCAL FUNC(Scc_ReturnType, SCC_CODE) Scc_TcpTransmit(void);

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_GetVersionInfo
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
#if ( defined SCC_VERSION_INFO_API ) && ( SCC_VERSION_INFO_API == STD_ON )
FUNC(void, SCC_CODE) Scc_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC, SCC_APPL_DATA) VersionInfoPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = SCC_DET_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
# if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of input parameters. */
  if ( VersionInfoPtr == NULL_PTR )
  {
    errorId = SCC_DET_INV_POINTER;
  }
  else if ( Scc_State == Scc_State_Uninitialized )
  {
    errorId = SCC_DET_NOT_INITIALIZED;
  }
  else
# endif /* SCC_DEV_ERROR_DETECT == STD_ON */
  {
  /* ----- Implementation ----------------------------------------------- */

    /* #20 Set VersionInfo structure with corresponding macros. */
    VersionInfoPtr->vendorID   = SCC_VENDOR_ID;
    VersionInfoPtr->moduleID   = SCC_MODULE_ID;
    VersionInfoPtr->sw_major_version = SCC_SW_MAJOR_VERSION;
    VersionInfoPtr->sw_minor_version = SCC_SW_MINOR_VERSION;
    VersionInfoPtr->sw_patch_version = SCC_SW_PATCH_VERSION;
  }
  /* ----- Development Error Report --------------------------------------- */
# if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #30 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_GET_VERSION_INFO, errorId);
  }
# else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
# endif /* SCC_DEV_ERROR_REPORT */
}
#endif /* SCC_VERSION_INFO_API */

/**********************************************************************************************************************
 *  Scc_InitMemory
 *********************************************************************************************************************/
  /*!
   *
   * Internal comment removed.
 *
 *
 *
 *
   */
FUNC(void, SCC_CODE) Scc_InitMemory(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set module state to uninitialized. */
  Scc_State = Scc_State_Uninitialized;

  /* #20 Initialize local data. */
  Scc_IPAddressCnt = 0;

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
  Scc_QCAFirmwareDownloadComplete = Scc_FirmwareDownloadStatus_Unknown;
  Scc_DLinkReady                  = FALSE;
#endif /* SCC_ENABLE_SLAC_HANDLING */

  /* #30 call InitMemory for StateMachine. */
#if ( SCC_ENABLE_STATE_MACHINE == STD_ON )
  Scc_StateM_InitMemory();
#endif /* SCC_ENABLE_STATE_MACHINE */
}

/**********************************************************************************************************************
 *  Scc_Init
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

FUNC(void, SCC_CODE) Scc_Init(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize general data. */

  Scc_V2GSocketInUse = FALSE;
  Scc_SDPSocket      = (Scc_SocketType)0xFFu;
  Scc_V2GSocket      = (Scc_SocketType)0xFFu;
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
    Scc_InitLinkTimeRAMVariables();
#endif /* SCC_ENABLE_PNC_CHARGING */

  /* #20 initialize the core parameters */
  Scc_Reset();

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  /* #30 initialize the root certificate read states */
  {
    uint8_least Counter;
    for ( Counter = 0; Counter < SCC_ROOT_CERT_CNT; Counter++ )
    {
      Scc_CertsWs.RootCertsReadStates[Counter] = Scc_NvMBlockReadState_NotSet;
    }
  }
  Scc_CertsWs.RootCertsProcessedFlags = 0;
  /* #40 initialize the contract certificate chain */
  (void) Scc_SwapContrCertChain(0, TRUE);
#endif

  /* #50 initialize BER workspace. */
  Scc_Priv_Init();
  /* #60 initialize Scc_Exi_TempBufPos. */
  Scc_Exi_Init();
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) /* PRQA S 3332 */ /* MD_Scc_3332 */
  /* #70 initialize the message pointer for ISO ED1. */
  Scc_ExiTx_ISO_Init();
  Scc_ExiRx_ISO_Init();
#endif /* SCC_SCHEMA_ISO */
#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) /* PRQA S 3332 */ /* MD_Scc_3332 */
  /* #80 initialize the message pointer for ISO ED2. */
  Scc_ExiTx_ISO_Ed2_Init();
  Scc_ExiRx_ISO_Ed2_Init();
#endif /* SCC_SCHEMA_ISO_ED2 */
#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) /* PRQA S 3332 */ /* MD_Scc_3332 */
  /* #90 initialize the message pointer for DIN. */
  Scc_ExiTx_DIN_Init();
  Scc_ExiRx_DIN_Init();
#endif /* SCC_SCHEMA_DIN */

#if ( defined SCC_ENABLE_STATE_MACHINE ) && ( SCC_ENABLE_STATE_MACHINE == STD_ON )
  Scc_StateM_Init();
#endif /* SCC_ENABLE_STATE_MACHINE */
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_MainFunction
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */

FUNC(void, SCC_CODE) Scc_MainFunction(void)
{
  /* ----- Implementation ----------------------------------------------- */
  if ( Scc_State != Scc_State_Uninitialized )
  {
#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    /* check if the QCA idle timer is active */
    if ( 0u != Scc_QCAIdleTimer )
    {
      /* decrement the idle timer */
      Scc_QCAIdleTimer--;
    }
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_STATE_MACHINE == STD_ON )
    Scc_StateM_MainFunction();
#endif

    /* #10 get the current message trigger. */
    Scc_Get_Core_MsgTrig(&Scc_MsgTrigNew);
    Scc_Get_Core_CyclicMsgTrigRx(&Scc_CyclicMsgTrig);

    switch ( Scc_MsgTrigNew )
    {
      /* #20 check if the SCC should be reset */
    case Scc_MsgTrig_None:
      /* check if this trigger is new */
      if (   ( Scc_MsgTrig != Scc_MsgTrigNew )
          || ( TRUE == Scc_CyclicMsgTrig ))
      {
        /* accept the new trigger */
        Scc_MsgTrig = Scc_MsgTrigNew;
        /* reset the core module */
        Scc_Reset();
        /* reset the cyclic message trigger at the application */
        Scc_Set_Core_CyclicMsgTrigTx(FALSE);
      }
      break;

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_MsgTrig_SLAC:
      Scc_TriggerSLAC();
      break;
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
    /* #30 read certificates from NvM. */
    case Scc_MsgTrig_ReadContrCertChain:
    case Scc_MsgTrig_ReadRootCerts:
    case Scc_MsgTrig_ReadProvCert:
      Scc_TriggerNVRAM();
      break;
#endif /* SCC_ENABLE_PNC_CHARGING */

    /* #40 handle V2G messages. */
    default:
      /* only check V2G message triggers if an IP address is assigned */
      if (   ( Scc_State_Initialized <= Scc_State )
          && ( 0u < Scc_IPAddressCnt ))
      {
        switch ( Scc_MsgState )
        {
          /* #50 check if a response was received, which needs to be decoded */
        case Scc_MsgState_ResponseReceived:
          /* check if this should be a SECCDiscoveryProtocolRes */
          if ( Scc_State_Initialized == Scc_State )
          {
            /* decode and process the SECCDiscoveryProtocolRes */
            if ( E_OK == Scc_RxSECCDiscoveryProtocolRes() )
            {
              /* SDPRes was successfully received, UDP socket no longer necessary */
              (void) TcpIp_Close(Scc_SDPSocket, FALSE);
              Scc_SDPSocket = (Scc_SocketType)0xFFu;

              Scc_MsgStateFct(Scc_MsgState_WaitForNextRequest)
            }
            /* received SECCDiscoveryProtocolRes was not valid, wait for the next one */
            else
            {
              /* go back to the transmission confirmed state */
              Scc_MsgStateFct(Scc_MsgState_RequestSent)
            }
          }
          /* check if this should be a SupportedAppProtocolRes */
          else if ( Scc_State_TLConnected == Scc_State )
          {
            /* decode and process the SupportedAppProtocolRes */
            Scc_Exi_DecodeSupportedAppProtocolRes();

            Scc_MsgStateFct(Scc_MsgState_WaitForNextRequest)

            /* make the received message available to the application */
            {
              /* create and set the rx buffer pointer */
              Scc_TxRxBufferPointerType V2GResponse;
              V2GResponse.PbufPtr = &Scc_ExiStreamRxPBuf[0];
              V2GResponse.FirstPart = TRUE;
              V2GResponse.StreamComplete = TRUE;
              /* provide the buffer to the application */
              Scc_Set_Core_V2GResponse(&V2GResponse);
            } /*lint !e550 */

            /* free the buffer */
            (void) TcpIp_TcpReceived(Scc_V2GSocket, Scc_ExiStreamRxPBuf[0].totLen);
          }
          /* if this should be a V2G message */
          else if (   ( Scc_State_SAPComplete == Scc_State )
                   || ( Scc_State_Connected == Scc_State ))
          {
            /* decode and process the received message */
            (void) (*Scc_ExiRx_Xyz_DecodeMessageFctPtr)();

            Scc_MsgStateFct(Scc_MsgState_WaitForNextRequest)

            /* make the received message available to the application */
            {
              /* create and set the rx buffer pointer */
              Scc_TxRxBufferPointerType V2GResponse;
              V2GResponse.PbufPtr = &Scc_ExiStreamRxPBuf[0];
              V2GResponse.FirstPart = TRUE;
              V2GResponse.StreamComplete = TRUE;
              /* provide the buffer to the application */
              Scc_Set_Core_V2GResponse(&V2GResponse);
            } /*lint !e550 */

            /* free the buffer */
            (void) TcpIp_TcpReceived(Scc_V2GSocket, Scc_ExiStreamRxPBuf[0].totLen);
          }
          /* received message can be ignored */
          else
          {
            /* check if data was received on the TCP connection */
            if ( 0u != Scc_ExiStreamRxPBuf[0].totLen )
            {
              /* free the buffer */
              (void) TcpIp_TcpReceived(Scc_V2GSocket, Scc_ExiStreamRxPBuf[0].totLen);
            }
          }
          /* the whole message was processed, thus reset the pbuf */
          Scc_ExiStreamRxPBuf[0].totLen = 0;
          Scc_ExiStreamRxPBuf[0].len = 0;
          Scc_ExiStreamRxPBufIdx = 0;
          break;

          /* #60 trigger the TcpIp_TcpTransmit and check if the whole message did fit into the first packet */
        case Scc_MsgState_StreamingRequest:
          if ( Scc_ReturnType_OK == Scc_TcpTransmit() )
          {
              Scc_MsgStateFct(Scc_MsgState_RequestSent)
          }
          break;

          /* #70 send next request */
        case Scc_MsgState_WaitForNextRequest:
          Scc_TriggerV2G();
          break;

          /* check if a request was sent and the response is still pending */
        default:
          Scc_TimeoutHandling();
          break;
        }
      }
      break;
    }
  }
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_TransmitExi
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_CODE) Scc_TransmitExi(void)
{
  /* #10 reset the length of the transmitted data */
  Scc_TxDataSent = 0;
  return Scc_TcpTransmit();
}

/**********************************************************************************************************************
 *  Scc_TcpTransmit
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_CODE) Scc_TcpTransmit(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 send the message and check if it failed */
  /* PRQA S 4342 1 */ /* MD_Scc_ReturnType */
  retVal = (Scc_ReturnType)TcpIp_TcpTransmit(Scc_V2GSocket, NULL_PTR,
    SCC_V2GTP_MAX_TX_LEN, TRUE);
  if ( Scc_ReturnType_OK != retVal )
  {
    /* #20 The request has not been accepted, e.g. due to a lack of buffer space or the socket is not connected.  */
    retVal = Scc_ReturnType_Pending;
  }
  else
  {
    /* #30 check if the whole message did not fit into the first packet */
    if ( TRUE == Scc_TxStreamingActive )
    {
      retVal = Scc_ReturnType_Pending;
    }
    else
    {
      retVal = Scc_ReturnType_OK;
    }
  }
  return retVal;
}

/**********************************************************************************************************************
 *  GLOBAL CALLBACKS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_Cbk_TL_RxIndication
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Cbk_TL_RxIndication(
  Scc_SocketType SockHnd,
  P2VAR(Scc_SockAddrType, AUTOMATIC, SCC_APPL_DATA) SourcePtr, /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr,
  uint16 DataLen)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = SCC_DET_NO_ERROR;
  uint8_least Counter;

  /* create a pseudo pbuf and a pointer to it */
  Scc_PbufType Pbuf;

  /* ----- Development Error Checks ------------------------------------- */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of runtime parameters. */
  if ( Scc_State == Scc_State_Uninitialized )
  {
    errorId = SCC_DET_NOT_INITIALIZED;
  }
  else if ( DataPtr == NULL_PTR )
  {
    errorId = SCC_DET_INV_POINTER;
  }
  else if ( SourcePtr == NULL_PTR )
  {
    errorId = SCC_DET_INV_POINTER;
  }
  else
#endif /* SCC_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */
    SCC_DUMMY_STATEMENT(SourcePtr); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */

    /* set the pbuf */
    Pbuf.payload = DataPtr;
    Pbuf.len     = DataLen;
    Pbuf.totLen  = DataLen;
    /* set the pbuf pointer */

    /* #20 check if a SDP or V2G message is expected. */
    if (   ( Scc_MsgState_RequestSent == Scc_MsgState )
        && (   (   ( Scc_SDPSocket == SockHnd )
                && ( Scc_MsgTrig_SECCDiscoveryProtocol == Scc_MsgTrig ))
            || (   ( Scc_V2GSocket == SockHnd )
                && ( Scc_MsgTrig_SupportedAppProtocol <= Scc_MsgTrig )
                && ( Scc_MsgTrig_StopCommunicationSession > Scc_MsgTrig ))))
    {
      /* #30 Convert linear buffer (TcpIp) to Pbuf (Exi).*/
      /* check if this element is adjacent to the previous one. */
      if (   ( 0u < Scc_ExiStreamRxPBufIdx )
          && ( &Scc_ExiStreamRxPBuf[Scc_ExiStreamRxPBufIdx-1u].payload[Scc_ExiStreamRxPBuf[Scc_ExiStreamRxPBufIdx-1u].len]
                 == Pbuf.payload ))
      {
        /* only increase the length */
        Scc_ExiStreamRxPBuf[Scc_ExiStreamRxPBufIdx-1u].len += Pbuf.len;
        Scc_ExiStreamRxPBuf[0].totLen                     += Pbuf.len;
      }
      /* this element is somewhere else in the memory. */
      else
      {
        /* Check if TcpRxdata fits in ExiStream buffer. */
        if ( Scc_ExiStreamRxPBufIdx < SCC_EXI_RX_STREAM_PBUF_ELEMENTS )
        {
          /* add the next segment */
          Scc_ExiStreamRxPBuf[Scc_ExiStreamRxPBufIdx].payload = Pbuf.payload;
          Scc_ExiStreamRxPBuf[0].totLen                      += Pbuf.len;
          Scc_ExiStreamRxPBuf[Scc_ExiStreamRxPBufIdx].len     = Pbuf.len;
          /* increase the index */
          Scc_ExiStreamRxPBufIdx++;
        }
        else
        {
          Scc_ReportError(Scc_StackError_PBufToSmall);
        }
      }

      /* #40 check if the V2G header is completely received. */
      if ( SCC_V2GTP_HDR_LEN < Scc_ExiStreamRxPBuf[0].totLen )
      {
        /* #50 check if the v2g header is invalid. */
        if ( (Std_ReturnType)E_OK != Scc_CheckV2GHeader(SockHnd) )
        {
          /* reset the PBuf */
          Scc_ExiStreamRxPBuf[0].totLen = 0;
          Scc_ExiStreamRxPBuf[0].len    = 0;
          Scc_ExiStreamRxPBufIdx        = 0;
          /* ignore this message */
#if(SCC_DEV_ERROR_REPORT == STD_ON)
          ( void )Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_RX_INDICATION, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
        }
        else
        {
          uint8  PayloadLengthArr[4];
          uint32 PayloadLengthBuf;
          /* copy the payload length from the pbuf */
          if ( (Std_ReturnType)E_OK != IpBase_CopyPbuf2String(&PayloadLengthArr[0], &Scc_ExiStreamRxPBuf[0], 4, 4) )
          {
#if ( defined SCC_DEM_IP_BASE )
            /* report status to DEM */
            Scc_DemReportErrorStatusFailed(SCC_DEM_IP_BASE);
#endif /* SCC_DEM_IP_BASE */
          }
          /* convert the uint8 array to uint32 */
          PayloadLengthBuf = PayloadLengthArr[3];
          PayloadLengthBuf += (uint32)( ((uint32)PayloadLengthArr[2]) << (8U));
          PayloadLengthBuf += (uint32)( ((uint32)PayloadLengthArr[1]) << (16U));
          PayloadLengthBuf += (uint32)( ((uint32)PayloadLengthArr[0]) << (24U));

          /* #60 check if the packet is received completely. */
          if ( ( SCC_V2GTP_HDR_LEN + PayloadLengthBuf ) <= Scc_ExiStreamRxPBuf[0].totLen )
          {
            /* set the totLen element of all PBufs */
            for ( Counter = 1; Counter < Scc_ExiStreamRxPBufIdx; Counter++ )
            {
              Scc_ExiStreamRxPBuf[Counter].totLen = Scc_ExiStreamRxPBuf[0].totLen;
            }

            /* #70 check if this message was received on an UDP socket (SDP). */
            if ( Scc_SDPSocket == SockHnd )
            {
              /* Check if the received data length fit in the Scc_Exi_StructBuf */
              if ( ( Scc_ExiStreamRxPBuf[0].totLen - SCC_V2GTP_HDR_LEN ) < SCC_EXI_STRUCT_BUF_LEN )
              {
                /* copy pbuf to exi struct buf, since the data will be gone after return */
                if ( (Std_ReturnType)E_OK != IpBase_CopyPbuf2String(&Scc_Exi_StructBuf[0], &Scc_ExiStreamRxPBuf[0],
                  (uint16)( Scc_ExiStreamRxPBuf[0].totLen - SCC_V2GTP_HDR_LEN ), SCC_V2GTP_HDR_LEN) )
                {
        #if(SCC_DEV_ERROR_REPORT == STD_ON)
                  (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_RX_INDICATION, SCC_DET_INV_PARAM);
        #endif /* SCC_DEV_ERROR_REPORT */
                }
              }
              else
              {
        #if(SCC_DEV_ERROR_REPORT == STD_ON)
                (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_RX_INDICATION, SCC_DET_TL);
        #endif /* SCC_DEV_ERROR_REPORT */
              }
            }

            /* #80 set the flag for the received response, the message will be processed in the main function. */
            Scc_MsgStateFct(Scc_MsgState_ResponseReceived) /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
          }
        }
      }
    }
    /* #90 if no message was excepected, ignore it. */
    else
    {
      /* #100 check if this RxIndication was issued for the TCP socket. */
      if ( Scc_V2GSocket == SockHnd )
      {
        /* release buffer in transport layer */
        (void) TcpIp_TcpReceived(SockHnd, Pbuf.totLen);
      }
    }
  }
  /* ----- Development Error Report --------------------------------------- */
#if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #110 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_RX_INDICATION, errorId);
  }
#else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* SCC_DEV_ERROR_REPORT */

  return;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_Cbk_TL_TCPConnected
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Cbk_TL_TCPConnected(Scc_SocketType SockHnd)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = SCC_DET_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of runtime parameters. */
  if ( Scc_State == Scc_State_Uninitialized )
  {
    errorId = SCC_DET_NOT_INITIALIZED;
  }
  else
#endif /* SCC_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */
    if ( SockHnd != Scc_V2GSocket )
    {
      errorId = SCC_DET_INV_PARAM;
    }
    /* #20 Update states and report to status. */
    else if ( Scc_State_TLConnecting == Scc_State )
    {
      /* update the states */
      Scc_State = Scc_State_TLConnected;
      Scc_MsgStateFct(Scc_MsgState_WaitForNextRequest)
      /* report it to the application */
      Scc_Set_Core_TCPSocketState(Scc_TCPSocketState_Connected);
      Scc_ReportSuccessAndStatus(Scc_MsgStatus_TransportLayer_OK);
    }
    /* illegal state for this callback */
    else
    {
      /* ignore */
#if(SCC_DEV_ERROR_REPORT == STD_ON)
      errorId = SCC_DET_INV_STATE;
#endif /* SCC_DEV_ERROR_REPORT */
    }
  }
  /* ----- Development Error Report --------------------------------------- */
#if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #30 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_TCP_CONNECTED, errorId);
  }
#else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* SCC_DEV_ERROR_REPORT */
}

/**********************************************************************************************************************
 *  Scc_Cbk_TL_TCPEvent
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Cbk_TL_TCPEvent(Scc_SocketType SockHnd, IpBase_TcpIpEventType Event)
{
  /* ----- Implementation ----------------------------------------------- */
  if ( Scc_State == Scc_State_Uninitialized )
  {
#if(SCC_DEV_ERROR_REPORT == STD_ON)
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_TCP_IP_EVENT, SCC_DET_NOT_INITIALIZED);
#endif /* SCC_DEV_ERROR_REPORT */
  }

  /* check if this callback does concern the TL (TCP/TLS) socket */
  else if ( Scc_V2GSocket == SockHnd )
  {
    /* #10 Set variables if TCP connection is closed/reset. */
    switch ( Event )
    {
      /* check if the connection was closed */
    case IPBASE_TCP_EVENT_CLOSED:
    case IPBASE_TCP_EVENT_RESET:
      /* update the flag */
      Scc_V2GSocketInUse = FALSE;
      Scc_V2GSocket = (Scc_SocketType)0xFFu;
      /* report it to the application */
      Scc_Set_Core_TCPSocketState(Scc_TCPSocketState_Closed);
      break;

    default:
      /* ignore other events */
      break;
    }

    /* #20 Check if charging session exist an report status.  */
    if (( Scc_State_TLConnecting <= Scc_State ) && ( Scc_State_ShuttingDown >= Scc_State ))
    {
      switch ( Event )
      {
      case IPBASE_TCP_EVENT_FIN_RECEIVED:
        /* the SECC is not allowed to close the connection */

        /* write the error and report it to the application */
        Scc_ReportError(Scc_StackError_TransportLayer);
        break;

      case IPBASE_TCP_EVENT_CLOSED:
        /* check if this shutdown was correctly triggered */
        if ( Scc_State_ShuttingDown == Scc_State )
        {
          Scc_MsgStateFct(Scc_MsgState_WaitForNextRequest)
          Scc_ReportSuccessAndStatus(Scc_MsgStatus_StopCommunicationSession_OK);
        }
        /* the connection was closed although there was still an active V2G session */
        else
        {
          /* write the error and report it to the application */
          Scc_ReportError(Scc_StackError_TransportLayer);
        }
        /* TL connection was disconnected, module will be shut down */
        Scc_State = Scc_State_ShutDown;
        break;

      case IPBASE_TCP_EVENT_RESET:
        /* the connection must not be reset by the EVSE */
        /* [V2G2-UG-050-2] After sending the SessionStopRes message, the SECC shall wait for the termination of the TLS/TCP connection by the EVCC for =5s.
           [V2G2-UG-050-3] If the EVCC did not terminate the TLS/TCP connection after =5s, the SECC shall terminate the connection. */

        /* write the error and report it to the application */
        if (Scc_MsgStatus < Scc_MsgStatus_SessionStop_OK)
        {
          Scc_ReportError(Scc_StackError_TransportLayer);
        }
        break;

      default:
  #if(SCC_DEV_ERROR_REPORT == STD_ON)
        (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_TCP_IP_EVENT, SCC_DET_INV_PARAM);
  #endif /* SCC_DEV_ERROR_REPORT */
        break;
      }
    }
  }
  else if (Scc_SDPSocket == SockHnd)
  {
    /* Do nothing */
  }
  else
  {
    /* Do nothing */
    /* Removed Det_ReportError due to ESCAN00103972 in TCP */
  }
} /* PRQA S 6010, 6030, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_Cbk_TLS_CertChain
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Cbk_TLS_CertChain(
  Scc_SocketType              SockHnd,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA)    validationResultPtr, /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
  P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA)  certChainPtr,
  uint32                      certChainLen )
{
  /* ----- Local Variables ---------------------------------------------- */
  uint32 certLen; /* PRQA S 0781 */ /* MD_Scc_0781 */
  uint8 errorId = SCC_DET_NO_ERROR;
#if ( SCC_ENABLE_TLS == STD_ON )
                                       /* 'C' ,  'P' ,  'S' */
  uint8                  cpoValue[3] = { 0x43u, 0x50u, 0x4Fu };
#endif /* SCC_ENABLE_TLS */

  /* ----- Development Error Checks ------------------------------------- */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of runtime parameters. */
  if ( Scc_State == Scc_State_Uninitialized )
  {
    errorId = SCC_DET_NOT_INITIALIZED;
  }
  else if (Scc_V2GSocket != SockHnd)
  {
    errorId = SCC_DET_INV_PARAM;
  }
  else if (validationResultPtr == NULL_PTR)
  {
    errorId = SCC_DET_INV_POINTER;
  }
  else if (certChainPtr == NULL_PTR)
  {
    errorId = SCC_DET_INV_POINTER;
  }
  else
#else /* SCC_DEV_ERROR_DETECT */
  SCC_DUMMY_STATEMENT(SockHnd);
#endif /* SCC_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */

    /* read certLen from first 3 bytes of certChainPtr */
    SCC_GET_UINT24(certChainPtr, 0u, certLen);

    if (certChainLen < (certLen + 3u))
    {
#if(SCC_DEV_ERROR_REPORT == STD_ON)
      (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_TCP_IP_CERT, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
    }
    else
    {
      switch (*validationResultPtr)
      {
        case TCPIP_TLS_VALIDATION_OK:
#if ( SCC_ENABLE_TLS == STD_ON )

          /* #20 Check if DomainComponent(Issuer) == "CPO" is in leaf certificate of certificate chain from TLS-Server. */
          if ( (Std_ReturnType)E_OK != Scc_SearchDomainComponentForValue(&certChainPtr[3], (uint16)certLen, &cpoValue[0], sizeof(cpoValue)) )
          {
            /* CPO was not set in Domain Component(Issuer) */
            *validationResultPtr = TCPIP_TLS_VALIDATION_REFUSED_BY_OWNER;
          }

          /* Report leaf certificate and validation result to application */
          Scc_Set_Core_TLS_CertChain(certChainPtr, validationResultPtr);

#else /* SCC_ENABLE_TLS */
          SCC_DUMMY_STATEMENT(certLen); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */
#endif /* SCC_ENABLE_TLS */
          break;

          /* #30 Report untrustworthy certificate chain to application. */
        case TCPIP_TLS_VALIDATION_UNKNOWN_CA:
          Scc_Set_Core_TLS_UnknownCALeafCert(certChainPtr);
          break;

        case TCPIP_TLS_VALIDATION_LAST_SIGN_INVALID:
        case TCPIP_TLS_VALIDATION_INT_SIGN_INVALID:
        case TCPIP_TLS_VALIDATION_REFUSED_BY_OWNER:
          break;

        default:
          Scc_ReportError(Scc_StackError_TransportLayer);
          break;
      }
    }
  }

  /* ----- Development Error Report --------------------------------------- */
#if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #40 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_TCP_IP_CERT, errorId);
  }
#else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* SCC_DEV_ERROR_REPORT */

} /* PRQA S 6010, 6080, 6030 */ /* MD_MSR_STPTH, MD_MSR_STMIF, MD_MSR_STCYC */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_Cbk_SLAC_FirmwareDownloadComplete
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Cbk_SLAC_FirmwareDownloadComplete(boolean DownloadSuccessful)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if firmware download was successful and set global variable. */
  if ( TRUE == DownloadSuccessful )
  {
    /* firmware download is complete */
    Scc_QCAFirmwareDownloadComplete = Scc_FirmwareDownloadStatus_Complete;
    /* set the qca idle timer */
    Scc_Get_SLAC_QCAIdleTimer(&Scc_QCAIdleTimer);
  }
  else
  {
    /* firmware download did not complete successfully */
    Scc_QCAFirmwareDownloadComplete = Scc_FirmwareDownloadStatus_ErrorOccurred;
  }
}
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_Cbk_SLAC_FirmwareDownloadStart
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */

FUNC(void, SCC_CODE) Scc_Cbk_SLAC_FirmwareDownloadStart(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Firmware download started, reset Scc_QCAFirmwareDownloadComplete. */
  Scc_QCAFirmwareDownloadComplete = Scc_FirmwareDownloadStatus_Unknown;
}
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_Cbk_SLAC_AssociationStatus
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Cbk_SLAC_AssociationStatus(uint8 AssociationStatus)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 AssocStep =  (uint8)(AssociationStatus >> 4u);
  uint8 AssocStatus = (uint8)(AssociationStatus & 0x0Fu);

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if SLAC failed completely and report status to application. */
  if ( ( ( ETHTRCV_30_AR7000_SLAC_STEP_5_SELECT_EVSE == AssocStep )
    && ( ETHTRCV_30_AR7000_SLAC_E_NO_DIRECT_OR_INDIRECT != AssocStatus ) )
    || ( ETHTRCV_30_AR7000_SLAC_STEP_6_VALIDATE_REQ == AssocStep )
    || ( ( ETHTRCV_30_AR7000_SLAC_STEP_8_VALID_FAILED == AssocStep )
      && ( ETHTRCV_30_AR7000_SLAC_E_VALIDATION_NO_SUCCESS != AssocStatus ) )
    || ( ETHTRCV_30_AR7000_SLAC_E_REMOTE_AMP_MAP_RCVD == AssocStatus )
    )
  {
    /* no error */
  }
  else
  {
    /* report the failure to the application */
    Scc_ReportError(Scc_StackError_SLAC);
  }

  /* provide detailed information to the application */
  Scc_Set_SLAC_AssociationStatus(AssociationStatus);

  return;
}
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_Cbk_SLAC_DLinkReady
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Cbk_SLAC_DLinkReady(EthTrcv_LinkStateType DLinkReady,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) NMKPtr, P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) NIDPtr)
{
  /* ----- Implementation ----------------------------------------------- */
  Scc_DLinkReady = DLinkReady;

  /* #10 check if SLAC was successful and report NMK and NID to the application.*/
  if ( ETHTRCV_LINK_STATE_ACTIVE == Scc_DLinkReady )
  {
    Scc_BufferPointerType BufPtr;
    /* provide the NMK to the application */
    BufPtr.BufferLen = ETHTRCV_30_AR7000_NMK_SIZE_BYTE;
    BufPtr.BufferPtr = NMKPtr;
    Scc_Set_SLAC_NMK(&BufPtr);
    /* provide the NID to the application */
    BufPtr.BufferLen = ETHTRCV_30_AR7000_NID_SIZE_BYTE;
    BufPtr.BufferPtr = NIDPtr;
    Scc_Set_SLAC_NID(&BufPtr);
    /* set the next state */
    Scc_MsgStateFct(Scc_MsgState_WaitForNextRequest)
    /* report the success to the application */
    Scc_ReportSuccessAndStatus(Scc_MsgStatus_SLAC_OK);
  }
  /* check if no link could be established during SLAC */
  else if ( Scc_MsgTrig_SLAC == Scc_MsgTrigNew ) /* PRQA S 2004 */ /* MD_Scc_2004 */
  {
    /* report the failure to the application */
    Scc_ReportError(Scc_StackError_SLAC);
  }
  /* if the link was lost, the module will be informed via the TrcvLinkStateChg callback */

  return;
}
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_Cbk_SLAC_GetRandomizedDataBuffer
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Cbk_SLAC_GetRandomizedDataBuffer(P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) RandomDataPtr, uint16 RandomDataLen)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 provide random data for the NMK and RunId. */
  Scc_Get_RandomData(RandomDataPtr, RandomDataLen);

  return;
}
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_Cbk_SLAC_GetValidateToggles
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(uint8, SCC_CODE) Scc_Cbk_SLAC_GetValidateToggles(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 ToggleNum;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 provide random data for toggle number. */
  Scc_Get_RandomData(&ToggleNum, sizeof(ToggleNum));

  /* value might only be between 1 and 3 */
  return (( ToggleNum % 3u ) + 1u );
}
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_Cbk_SLAC_ToggleRequest
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */

FUNC(void, SCC_CODE) Scc_Cbk_SLAC_ToggleRequest(uint8 ToggleNum)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 inform the application that toggling shall be started. */
  Scc_Set_SLAC_ToggleRequest(ToggleNum);

  return;
}
#endif /* SCC_ENABLE_SLAC_HANDLING */

/**********************************************************************************************************************
 *  Scc_Cbk_Eth_TransceiverLinkStateChange
 **********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Cbk_Eth_TransceiverLinkStateChange(uint8 CtrlIdx, EthTrcv_LinkStateType TrcvLinkState)
{
  /* ----- Implementation ----------------------------------------------- */
  /* check if this callback is for a this controller */
  if ( (uint8)SCC_CTRL_IDX == CtrlIdx )
  {
    /* #10 report the transceiver link state to the application. */
    if ( ETHTRCV_LINK_STATE_ACTIVE == TrcvLinkState )
    {
      /* link state is up */
      /* inform the application */
      Scc_Set_Core_TrcvLinkState(TRUE);
    }
    else
    {
      /* link state is down */
      /* remove all assigned IP addresses */
      Scc_IPAddressCnt = 0;
      /* inform the application */
      Scc_Set_Core_TrcvLinkState(FALSE);
    }
  }

  return;
}

/**********************************************************************************************************************
 *  Scc_Cbk_IP_AddressAssignmentChange
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Cbk_IP_AddressAssignmentChange(TcpIp_LocalAddrIdType LocalAddrId, TcpIp_IpAddrStateType State)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = SCC_DET_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of runtime parameters. */
  if ( Scc_State == Scc_State_Uninitialized )
  {
    errorId = SCC_DET_NOT_INITIALIZED;
  }
  else
#endif /* SCC_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* check if this is the assigned address */
    if ( SCC_IPV6_ADDRESS == LocalAddrId )
    {
      /* check if the SCC was already initialized */
      if ( Scc_State_Initialized <= Scc_State )
      {
        /* check if the IP address was assigned */
        if ( State == TCPIP_IPADDR_STATE_ASSIGNED )
        {
          if ( 0u == Scc_IPAddressCnt )
          {
            /* #20 inform the application about the IP assignment. */
            Scc_Set_Core_IPAssigned(TRUE);
          }

          /* more than 255 IPv6 addresses are not supported */
          if ( 0xFFu > Scc_IPAddressCnt )
          {
            Scc_IPAddressCnt++;
          }
        }
        else
        {
          /* the IP address was lost */

          if ( 1u >= Scc_IPAddressCnt )
          {
            /* check if a V2G communication session was running */
            if (   ( Scc_State_SDPComplete <= Scc_State )
                && ( Scc_State_Disconnected >= Scc_State ))
            {
              /* reset the message state */
              Scc_MsgStateFct(Scc_MsgState_WaitForNextRequest)
              /* stop the timeout counter */
              Scc_TimeoutCnt = 0;
              /* report the error to the application */
              Scc_ReportError(Scc_StackError_TransportLayer);
            }

            /* stop connection with charge point, reset state to init */
            Scc_State = Scc_State_Initialized;
            Scc_IPAddressCnt = 0;
            /* #30 inform the application about the loss of the IP addresses. */
            Scc_Set_Core_IPAssigned(FALSE);
          }
          else
          {
            Scc_IPAddressCnt--;
          }
        }
      }
    }


  }
  /* ----- Development Error Report --------------------------------------- */
#if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #40 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_IP_ADDRESS_CHG_CBK, errorId);
  }
#else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* SCC_DEV_ERROR_REPORT */
} /* PRQA S 6010, 6030, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STMIF */


#if ( SCC_NUM_OF_DYN_CONFIG_PARAMS != 0 )
/**********************************************************************************************************************
 *  Scc_DynConfigDataReadAccess
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_DynConfigDataReadAccess(Scc_DynConfigParamsType DataID,
  P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) DataPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = SCC_DET_NO_ERROR;
  Std_ReturnType RetVal = E_OK;

  /* ----- Development Error Checks ------------------------------------- */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of runtime parameters. */
  if ( Scc_State == Scc_State_Uninitialized )
  {
    errorId = SCC_DET_NOT_INITIALIZED;
    RetVal = E_NOT_OK;
  }
  else if ( DataPtr == NULL_PTR )
  {
    errorId = SCC_DET_INV_POINTER;
    RetVal = E_NOT_OK;
  }
  else
#endif /* SCC_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */

    switch ( DataID )
    {

    /* #20 get value for Timer General */

  #if ( defined Scc_ConfigType_Timer_General_IPAddressWaitTimeout ) && ( Scc_ConfigType_Timer_General_IPAddressWaitTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_General_IPAddressWaitTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_General_IPAddressWaitTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_General_IPAddressWaitTimeout */

  #if ( defined Scc_ConfigType_Timer_General_SECCDiscoveryProtocolRetries ) && ( Scc_ConfigType_Timer_General_SECCDiscoveryProtocolRetries == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_General_SECCDiscoveryProtocolRetries:
      *DataPtr = (uint16)Scc_ConfigValue_Timer_General_SECCDiscoveryProtocolRetries;
      break;
  #endif /* Scc_ConfigType_Timer_General_SECCDiscoveryProtocolRetries */

  #if ( defined Scc_ConfigType_Timer_General_SECCDiscoveryProtocolTimeout ) && ( Scc_ConfigType_Timer_General_SECCDiscoveryProtocolTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_General_SECCDiscoveryProtocolTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_General_SECCDiscoveryProtocolTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_General_SECCDiscoveryProtocolTimeout */

  #if ( defined Scc_ConfigType_Timer_General_TransportLayerTimeout ) && ( Scc_ConfigType_Timer_General_TransportLayerTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_General_TransportLayerTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_General_TransportLayerTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_General_TransportLayerTimeout */

  #if ( defined Scc_ConfigType_Timer_General_SupportedAppProtocolMessageTimeout ) && ( Scc_ConfigType_Timer_General_SupportedAppProtocolMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_General_SupportedAppProtocolMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_General_SupportedAppProtocolMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_General_SupportedAppProtocolMessageTimeout */

    /* #30 get value for Timer ISO */

  #if ( defined Scc_ConfigType_Timer_ISO_SequencePerformanceTimeout ) && ( Scc_ConfigType_Timer_ISO_SequencePerformanceTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_SequencePerformanceTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_SequencePerformanceTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_SequencePerformanceTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_CommunicationSetupTimeout ) && ( Scc_ConfigType_Timer_ISO_CommunicationSetupTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_CommunicationSetupTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_CommunicationSetupTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_CommunicationSetupTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_AuthorizationMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_AuthorizationMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_AuthorizationMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_AuthorizationMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_AuthorizationMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_CableCheckMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_CableCheckMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_CableCheckMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_CableCheckMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_CableCheckMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_CertificateInstallationMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_CertificateInstallationMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_CertificateInstallationMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_CertificateInstallationMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_CertificateInstallationMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_CertificateUpdateMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_CertificateUpdateMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_CertificateUpdateMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_CertificateUpdateMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_CertificateUpdateMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_ChargeParameterDiscoveryMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_ChargeParameterDiscoveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_ChargeParameterDiscoveryMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_ChargeParameterDiscoveryMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_ChargeParameterDiscoveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_ChargingStatusMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_ChargingStatusMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_ChargingStatusMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_ChargingStatusMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_ChargingStatusMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_CurrentDemandMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_CurrentDemandMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_CurrentDemandMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_CurrentDemandMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_CurrentDemandMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_MeteringReceiptMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_MeteringReceiptMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_MeteringReceiptMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_MeteringReceiptMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_MeteringReceiptMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_PaymentDetailsMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_PaymentDetailsMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_PaymentDetailsMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_PaymentDetailsMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_PaymentDetailsMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_PaymentServiceSelectionMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_PaymentServiceSelectionMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_PaymentServiceSelectionMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_PaymentServiceSelectionMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_PaymentServiceSelectionMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_PowerDeliveryMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_PowerDeliveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_PowerDeliveryMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_PowerDeliveryMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_PowerDeliveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_PreChargeMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_PreChargeMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_PreChargeMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_PreChargeMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_PreChargeMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_ServiceDetailMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_ServiceDetailMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_ServiceDetailMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_ServiceDetailMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_ServiceDetailMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_ServiceDiscoveryMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_ServiceDiscoveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_ServiceDiscoveryMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_ServiceDiscoveryMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_ServiceDiscoveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_SessionSetupMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_SessionSetupMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_SessionSetupMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_SessionSetupMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_SessionSetupMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_SessionStopMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_SessionStopMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_SessionStopMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_SessionStopMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_SessionStopMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_WeldingDetectionMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_WeldingDetectionMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_WeldingDetectionMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_WeldingDetectionMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_WeldingDetectionMessageTimeout */

    /* #40 get value for Timer ISO_Ed2_DIS */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_SequencePerformanceTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_SequencePerformanceTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_SequencePerformanceTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_SequencePerformanceTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_SequencePerformanceTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_CommunicationSetupTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_CommunicationSetupTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_CommunicationSetupTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_CommunicationSetupTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_CommunicationSetupTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_AuthorizationMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_AuthorizationMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_AuthorizationMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_AuthorizationMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_AuthorizationMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_CableCheckMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_CableCheckMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_CableCheckMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_CableCheckMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_CableCheckMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_CertificateInstallationMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_CertificateInstallationMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_CertificateInstallationMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_CertificateInstallationMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_CertificateInstallationMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_CertificateUpdateMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_CertificateUpdateMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_CertificateUpdateMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_CertificateUpdateMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_CertificateUpdateMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_ChargeParameterDiscoveryMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_ChargeParameterDiscoveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_ChargeParameterDiscoveryMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_ChargeParameterDiscoveryMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_ChargeParameterDiscoveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_ChargingStatusMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_ChargingStatusMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_ChargingStatusMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_ChargingStatusMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_ChargingStatusMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_CurrentDemandMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_CurrentDemandMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_CurrentDemandMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_CurrentDemandMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_CurrentDemandMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_MeteringReceiptMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_MeteringReceiptMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_MeteringReceiptMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_MeteringReceiptMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_MeteringReceiptMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_PaymentDetailsMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_PaymentDetailsMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_PaymentDetailsMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_PaymentDetailsMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_PaymentDetailsMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_PaymentServiceSelectionMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_PaymentServiceSelectionMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_PaymentServiceSelectionMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_PaymentServiceSelectionMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_PaymentServiceSelectionMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_PowerDeliveryMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_PowerDeliveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_PowerDeliveryMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_PowerDeliveryMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_PowerDeliveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_PreChargeMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_PreChargeMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_PreChargeMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_PreChargeMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_PreChargeMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_ServiceDetailMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_ServiceDetailMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_ServiceDetailMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_ServiceDetailMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_ServiceDetailMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_ServiceDiscoveryMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_ServiceDiscoveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_ServiceDiscoveryMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_ServiceDiscoveryMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_ServiceDiscoveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_SessionSetupMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_SessionSetupMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_SessionSetupMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_SessionSetupMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_SessionSetupMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_SessionStopMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_SessionStopMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_SessionStopMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_SessionStopMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_SessionStopMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_WeldingDetectionMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_WeldingDetectionMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_WeldingDetectionMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_WeldingDetectionMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_WeldingDetectionMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_FinePositioningSetupMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_FinePositioningSetupMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_FinePositioningSetupMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_FinePositioningSetupMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_FinePositioningSetupMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_FinePositioningMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_FinePositioningMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_FinePositioningMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_FinePositioningMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_FinePositioningMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_PairingMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_PairingMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_PairingMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_PairingMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_PairingMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_InitialAlignmentCheckMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_InitialAlignmentCheckMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_InitialAlignmentCheckMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_InitialAlignmentCheckMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_InitialAlignmentCheckMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_PowerDemandMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_PowerDemandMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_PowerDemandMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_ISO_Ed2_DIS_PowerDemandMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_PowerDemandMessageTimeout */

    /* #50 get value for Timer DIN */

  #if ( defined Scc_ConfigType_Timer_DIN_SequencePerformanceTimeout ) && ( Scc_ConfigType_Timer_DIN_SequencePerformanceTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_SequencePerformanceTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_DIN_SequencePerformanceTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_SequencePerformanceTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_CommunicationSetupTimeout ) && ( Scc_ConfigType_Timer_DIN_CommunicationSetupTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_CommunicationSetupTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_DIN_CommunicationSetupTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_CommunicationSetupTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_ReadyToChargeTimeout ) && ( Scc_ConfigType_Timer_DIN_ReadyToChargeTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_ReadyToChargeTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_DIN_ReadyToChargeTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_ReadyToChargeTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_ContractAuthenticationMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_ContractAuthenticationMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_ContractAuthenticationMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_DIN_ContractAuthenticationMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_ContractAuthenticationMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_CableCheckMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_CableCheckMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_CableCheckMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_DIN_CableCheckMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_CableCheckMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_ChargeParameterDiscoveryMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_ChargeParameterDiscoveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_ChargeParameterDiscoveryMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_DIN_ChargeParameterDiscoveryMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_ChargeParameterDiscoveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_CurrentDemandMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_CurrentDemandMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_CurrentDemandMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_DIN_CurrentDemandMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_CurrentDemandMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_ServicePaymentSelectionMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_ServicePaymentSelectionMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_ServicePaymentSelectionMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_DIN_ServicePaymentSelectionMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_ServicePaymentSelectionMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_PowerDeliveryMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_PowerDeliveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_PowerDeliveryMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_DIN_PowerDeliveryMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_PowerDeliveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_PreChargeMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_PreChargeMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_PreChargeMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_DIN_PreChargeMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_PreChargeMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_ServiceDiscoveryMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_ServiceDiscoveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_ServiceDiscoveryMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_DIN_ServiceDiscoveryMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_ServiceDiscoveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_SessionSetupMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_SessionSetupMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_SessionSetupMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_DIN_SessionSetupMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_SessionSetupMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_SessionStopMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_SessionStopMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_SessionStopMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_DIN_SessionStopMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_SessionStopMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_WeldingDetectionMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_WeldingDetectionMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_WeldingDetectionMessageTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_Timer_DIN_WeldingDetectionMessageTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_WeldingDetectionMessageTimeout */

  #if ( defined SCC_ENABLE_STATE_MACHINE )

    /* #60 get value for OEM Vector - State Machine */

  #if ( defined Scc_ConfigType_StateM_AcceptUnsecureConnection ) && ( Scc_ConfigType_StateM_AcceptUnsecureConnection == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Accept Unsecure Connection */
    case Scc_DynConfigParam_StateM_AcceptUnsecureConnection:
      *DataPtr = (uint16)Scc_ConfigValue_StateM_AcceptUnsecureConnection;
      break;
  #endif /* Scc_ConfigType_StateM_AcceptUnsecureConnection */

  #if ( defined Scc_ConfigType_StateM_ReconnectRetries ) && ( Scc_ConfigType_StateM_ReconnectRetries == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Reconnect Retries */
    case Scc_DynConfigParam_StateM_ReconnectRetries:
      *DataPtr = (uint16)Scc_ConfigValue_StateM_ReconnectRetries;
      break;
  #endif /* Scc_ConfigType_StateM_ReconnectRetries */

  #if ( defined Scc_ConfigType_StateM_ReconnectDelay ) && ( Scc_ConfigType_StateM_ReconnectDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Reconnect Delay */
    case Scc_DynConfigParam_StateM_ReconnectDelay:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_ReconnectDelay * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_ReconnectDelay */

  #if ( defined Scc_ConfigType_StateM_RequestInternetDetails ) && ( Scc_ConfigType_StateM_RequestInternetDetails == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Request Internet Details */
    case Scc_DynConfigParam_StateM_RequestInternetDetails:
      *DataPtr = (uint16)Scc_ConfigValue_StateM_RequestInternetDetails;
      break;
  #endif /* Scc_ConfigType_StateM_RequestInternetDetails */

  #if ( defined Scc_ConfigType_StateM_AuthorizationOngoingTimeout ) && ( Scc_ConfigType_StateM_AuthorizationOngoingTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Authorization Cycle Timeout */
    case Scc_DynConfigParam_StateM_AuthorizationOngoingTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_AuthorizationOngoingTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_AuthorizationOngoingTimeout */

  #if ( defined Scc_ConfigType_StateM_ChargeParameterDiscoveryOngoingTimeout ) && ( Scc_ConfigType_StateM_ChargeParameterDiscoveryOngoingTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Charge Parameter Discovery Cycle Timeout */
    case Scc_DynConfigParam_StateM_ChargeParameterDiscoveryOngoingTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_ChargeParameterDiscoveryOngoingTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_ChargeParameterDiscoveryOngoingTimeout */

  #if ( defined Scc_ConfigType_StateM_CableCheckOngoingTimeout ) && ( Scc_ConfigType_StateM_CableCheckOngoingTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* CableCheck Cycle Timeout */
    case Scc_DynConfigParam_StateM_CableCheckOngoingTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_CableCheckOngoingTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_CableCheckOngoingTimeout */

  #if ( defined Scc_ConfigType_StateM_PreChargeOngoingTimeout ) && ( Scc_ConfigType_StateM_PreChargeOngoingTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* PreCharge Cycle Timeout */
    case Scc_DynConfigParam_StateM_PreChargeOngoingTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_PreChargeOngoingTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_PreChargeOngoingTimeout */

  #if ( defined Scc_ConfigType_StateM_FinePositioningOngoingTimeout ) && ( Scc_ConfigType_StateM_FinePositioningOngoingTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* FinePositioning Cycle Timeout */
    case Scc_DynConfigParam_StateM_FinePositioningOngoingTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_FinePositioningOngoingTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_FinePositioningOngoingTimeout */

  #if ( defined Scc_ConfigType_StateM_PairingOngoingTimeout ) && ( Scc_ConfigType_StateM_PairingOngoingTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Pairing Cycle Timeout */
    case Scc_DynConfigParam_StateM_PairingOngoingTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_PairingOngoingTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_PairingOngoingTimeout */

  #if ( defined Scc_ConfigType_StateM_InitialAlignmentCheckOngoingTimeout ) && ( Scc_ConfigType_StateM_InitialAlignmentCheckOngoingTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Initial Alignment Check Cycle Timeout */
    case Scc_DynConfigParam_StateM_InitialAlignmentCheckOngoingTimeout:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_InitialAlignmentCheckOngoingTimeout * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_InitialAlignmentCheckOngoingTimeout */
  #if ( defined Scc_ConfigType_StateM_AuthorizationNextReqDelay ) && ( Scc_ConfigType_StateM_AuthorizationNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Authorization Next Request Delay */
    case Scc_DynConfigParam_StateM_AuthorizationNextReqDelay:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_AuthorizationNextReqDelay * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_AuthorizationNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_ChargeParameterDiscoveryNextReqDelay ) && ( Scc_ConfigType_StateM_ChargeParameterDiscoveryNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Charge Parameter Discovery Next Request Delay */
    case Scc_DynConfigParam_StateM_ChargeParameterDiscoveryNextReqDelay:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_ChargeParameterDiscoveryNextReqDelay * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_ChargeParameterDiscoveryNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_ChargingStatusNextReqDelay ) && ( Scc_ConfigType_StateM_ChargingStatusNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Charging Status Next Request Delay */
    case Scc_DynConfigParam_StateM_ChargingStatusNextReqDelay:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_ChargingStatusNextReqDelay * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_ChargingStatusNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_CableCheckNextReqDelay ) && ( Scc_ConfigType_StateM_CableCheckNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Cable Check Next Request Delay */
    case Scc_DynConfigParam_StateM_CableCheckNextReqDelay:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_CableCheckNextReqDelay * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_CableCheckNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_PreChargeNextReqDelay ) && ( Scc_ConfigType_StateM_PreChargeNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Pre Charge Next Request Delay */
    case Scc_DynConfigParam_StateM_PreChargeNextReqDelay:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_PreChargeNextReqDelay * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_PreChargeNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_CurrentDemandNextReqDelay ) && ( Scc_ConfigType_StateM_CurrentDemandNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Current Demand Next Request Delay */
    case Scc_DynConfigParam_StateM_CurrentDemandNextReqDelay:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_CurrentDemandNextReqDelay * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_CurrentDemandNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_WeldingDetectionNextReqDelay ) && ( Scc_ConfigType_StateM_WeldingDetectionNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Welding Detection Next Request Delay */
    case Scc_DynConfigParam_StateM_WeldingDetectionNextReqDelay:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_WeldingDetectionNextReqDelay * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_WeldingDetectionNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_FinePositioningNextReqDelay ) && ( Scc_ConfigType_StateM_FinePositioningNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Cable Check Next Request Delay */
    case Scc_DynConfigParam_StateM_FinePositioningNextReqDelay:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_FinePositioningNextReqDelay * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_FinePositioningNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_PairingNextReqDelay ) && ( Scc_ConfigType_StateM_PairingNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Pre Charge Next Request Delay */
    case Scc_DynConfigParam_StateM_PairingNextReqDelay:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_PairingNextReqDelay * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_PairingNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_InitialAlignmentCheckNextReqDelay ) && ( Scc_ConfigType_StateM_InitialAlignmentCheckNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Current Demand Next Request Delay */
    case Scc_DynConfigParam_StateM_InitialAlignmentCheckNextReqDelay:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_InitialAlignmentCheckNextReqDelay * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_InitialAlignmentCheckNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_PowerDemandNextReqDelay ) && ( Scc_ConfigType_StateM_PowerDemandNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Welding Detection Next Request Delay */
    case Scc_DynConfigParam_StateM_PowerDemandNextReqDelay:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_PowerDemandNextReqDelay * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_PowerDemandNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_PaymentPrioritization ) && ( Scc_ConfigType_StateM_PaymentPrioritization == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Payment Priorisation */
    case Scc_DynConfigParam_StateM_PaymentPrioritization:
      *DataPtr = (uint16)Scc_ConfigValue_StateM_PaymentPrioritization;
      break;
  #endif /* Scc_ConfigType_StateM_PaymentPrioritization */

  #if ( defined Scc_ConfigType_StateM_ContractCertificateChainIndexInUse ) && ( Scc_ConfigType_StateM_ContractCertificateChainIndexInUse == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Selected Contract Certificate Slot */
    case Scc_DynConfigParam_StateM_ContractCertificateChainIndexInUse:
      *DataPtr = (uint16)Scc_ConfigValue_StateM_ContractCertificateChainIndexInUse;
      break;
  #endif /* Scc_ConfigType_StateM_ContractCertificateChainIndexInUse */

  #if ( defined Scc_ConfigType_StateM_CertificateExpireThreshold ) && ( Scc_ConfigType_StateM_CertificateExpireThreshold == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Certificate Expire Threshold */
    case Scc_DynConfigParam_StateM_CertificateExpireThreshold:
      *DataPtr = (uint16)Scc_ConfigValue_StateM_CertificateExpireThreshold;
      break;
  #endif /* Scc_ConfigType_StateM_CertificateExpireThreshold */

  #if ( defined Scc_ConfigType_StateM_RequestCertificateDetails ) && ( Scc_ConfigType_StateM_RequestCertificateDetails == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Request Certificate Details */
    case Scc_DynConfigParam_StateM_RequestCertificateDetails:
      *DataPtr = (uint16)Scc_ConfigValue_StateM_RequestCertificateDetails;
      break;
  #endif /* Scc_ConfigType_StateM_RequestCertificateDetails */

  #if ( defined Scc_ConfigType_StateM_RequestCertificateInstallation ) && ( Scc_ConfigType_StateM_RequestCertificateInstallation == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Request Certificate Install */
    case Scc_DynConfigParam_StateM_RequestCertificateInstallation:
      *DataPtr = (uint16)Scc_ConfigValue_StateM_RequestCertificateInstallation;
      break;
  #endif /* Scc_ConfigType_StateM_RequestCertificateInstallation */

  #if ( defined Scc_ConfigType_StateM_RequestCertificateUpdate ) && ( Scc_ConfigType_StateM_RequestCertificateUpdate == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* Request Certificate Update */
    case Scc_DynConfigParam_StateM_RequestCertificateUpdate:
      *DataPtr = (uint16)Scc_ConfigValue_StateM_RequestCertificateUpdate;
      break;
  #endif /* Scc_ConfigType_StateM_RequestCertificateUpdate */

  #if ( defined Scc_ConfigType_StateM_SLACStartMode ) && ( Scc_ConfigType_StateM_SLACStartMode == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* SLAC Start Mode */
    case Scc_DynConfigParam_StateM_SLACStartMode:
      *DataPtr = (uint16)Scc_ConfigValue_StateM_SLACStartMode;
      break;
  #endif /* Scc_ConfigType_StateM_SLACStartMode */

  #if ( defined Scc_ConfigType_StateM_QCAIdleTimer ) && ( Scc_ConfigType_StateM_QCAIdleTimer == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* QCA Idle Timer */
    case Scc_DynConfigParam_StateM_QCAIdleTimer:
      *DataPtr = (uint16)( Scc_ConfigValue_StateM_QCAIdleTimer * SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_QCAIdleTimer */

  #endif /* SCC_ENABLE_STATE_MACHINE */

      /* invalid DataID */
    default:
      RetVal = E_NOT_OK;
      break;
    }
  }

  /* ----- Development Error Report --------------------------------------- */
#if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #70 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_DIAG_DATA_READ_ACCESS, errorId);
  }
#else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* SCC_DEV_ERROR_REPORT */

  return RetVal;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */
#endif /* SCC_NUM_OF_DYN_CONFIG_PARAMS */


#if ( SCC_NUM_OF_DYN_CONFIG_PARAMS != 0 )
/**********************************************************************************************************************
 *  Scc_DynConfigDataWriteAccess
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_DynConfigDataWriteAccess(Scc_DynConfigParamsType DataID, uint16 Data)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = SCC_DET_NO_ERROR;
  Std_ReturnType RetVal = E_OK;

  /* ----- Development Error Checks ------------------------------------- */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of runtime parameters. */
  if ( Scc_State == Scc_State_Uninitialized )
  {
    errorId = SCC_DET_NOT_INITIALIZED;
    RetVal = E_NOT_OK;
  }
  else
#endif /* SCC_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */
    switch ( DataID )
    {

    /* #20 set value for Timer General */

  #if ( defined Scc_ConfigType_Timer_General_IPAddressWaitTimeout ) && ( Scc_ConfigType_Timer_General_IPAddressWaitTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_General_IPAddressWaitTimeout:
      Scc_ConfigValue_Timer_General_IPAddressWaitTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_General_IPAddressWaitTimeout */

  #if ( defined Scc_ConfigType_Timer_General_SECCDiscoveryProtocolRetries ) && ( Scc_ConfigType_Timer_General_SECCDiscoveryProtocolRetries == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_General_SECCDiscoveryProtocolRetries:
      Scc_ConfigValue_Timer_General_SECCDiscoveryProtocolRetries = Data;
      break;
  #endif /* Scc_ConfigType_Timer_General_SECCDiscoveryProtocolRetries */

  #if ( defined Scc_ConfigType_Timer_General_SECCDiscoveryProtocolTimeout ) && ( Scc_ConfigType_Timer_General_SECCDiscoveryProtocolTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_General_SECCDiscoveryProtocolTimeout:
      Scc_ConfigValue_Timer_General_SECCDiscoveryProtocolTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_General_SECCDiscoveryProtocolTimeout */

  #if ( defined Scc_ConfigType_Timer_General_TransportLayerTimeout ) && ( Scc_ConfigType_Timer_General_TransportLayerTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_General_TransportLayerTimeout:
      Scc_ConfigValue_Timer_General_TransportLayerTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_General_TransportLayerTimeout */

  #if ( defined Scc_ConfigType_Timer_General_SupportedAppProtocolMessageTimeout ) && ( Scc_ConfigType_Timer_General_SupportedAppProtocolMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_General_SupportedAppProtocolMessageTimeout:
      Scc_ConfigValue_Timer_General_SupportedAppProtocolMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_General_SupportedAppProtocolMessageTimeout */

    /* #30 set value for Timer ISO */

  #if ( defined Scc_ConfigType_Timer_ISO_SequencePerformanceTimeout ) && ( Scc_ConfigType_Timer_ISO_SequencePerformanceTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_SequencePerformanceTimeout:
      Scc_ConfigValue_Timer_ISO_SequencePerformanceTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_SequencePerformanceTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_CommunicationSetupTimeout ) && ( Scc_ConfigType_Timer_ISO_CommunicationSetupTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_CommunicationSetupTimeout:
      Scc_ConfigValue_Timer_ISO_CommunicationSetupTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_CommunicationSetupTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_AuthorizationMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_AuthorizationMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_AuthorizationMessageTimeout:
      Scc_ConfigValue_Timer_ISO_AuthorizationMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_AuthorizationMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_CableCheckMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_CableCheckMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_CableCheckMessageTimeout:
      Scc_ConfigValue_Timer_ISO_CableCheckMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_CableCheckMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_CertificateInstallationMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_CertificateInstallationMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_CertificateInstallationMessageTimeout:
      Scc_ConfigValue_Timer_ISO_CertificateInstallationMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_CertificateInstallationMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_CertificateUpdateMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_CertificateUpdateMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_CertificateUpdateMessageTimeout:
      Scc_ConfigValue_Timer_ISO_CertificateUpdateMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_CertificateUpdateMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_ChargeParameterDiscoveryMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_ChargeParameterDiscoveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_ChargeParameterDiscoveryMessageTimeout:
      Scc_ConfigValue_Timer_ISO_ChargeParameterDiscoveryMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_ChargeParameterDiscoveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_ChargingStatusMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_ChargingStatusMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_ChargingStatusMessageTimeout:
      Scc_ConfigValue_Timer_ISO_ChargingStatusMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_ChargingStatusMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_CurrentDemandMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_CurrentDemandMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_CurrentDemandMessageTimeout:
      Scc_ConfigValue_Timer_ISO_CurrentDemandMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_CurrentDemandMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_MeteringReceiptMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_MeteringReceiptMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_MeteringReceiptMessageTimeout:
      Scc_ConfigValue_Timer_ISO_MeteringReceiptMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_MeteringReceiptMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_PaymentDetailsMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_PaymentDetailsMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_PaymentDetailsMessageTimeout:
      Scc_ConfigValue_Timer_ISO_PaymentDetailsMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_PaymentDetailsMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_PaymentServiceSelectionMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_PaymentServiceSelectionMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_PaymentServiceSelectionMessageTimeout:
      Scc_ConfigValue_Timer_ISO_PaymentServiceSelectionMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_PaymentServiceSelectionMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_PowerDeliveryMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_PowerDeliveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_PowerDeliveryMessageTimeout:
      Scc_ConfigValue_Timer_ISO_PowerDeliveryMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_PowerDeliveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_PreChargeMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_PreChargeMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_PreChargeMessageTimeout:
      Scc_ConfigValue_Timer_ISO_PreChargeMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_PreChargeMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_ServiceDetailMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_ServiceDetailMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_ServiceDetailMessageTimeout:
      Scc_ConfigValue_Timer_ISO_ServiceDetailMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_ServiceDetailMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_ServiceDiscoveryMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_ServiceDiscoveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_ServiceDiscoveryMessageTimeout:
      Scc_ConfigValue_Timer_ISO_ServiceDiscoveryMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_ServiceDiscoveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_SessionSetupMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_SessionSetupMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_SessionSetupMessageTimeout:
      Scc_ConfigValue_Timer_ISO_SessionSetupMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_SessionSetupMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_SessionStopMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_SessionStopMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_SessionStopMessageTimeout:
      Scc_ConfigValue_Timer_ISO_SessionStopMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_SessionStopMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_WeldingDetectionMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_WeldingDetectionMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_WeldingDetectionMessageTimeout:
      Scc_ConfigValue_Timer_ISO_WeldingDetectionMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_WeldingDetectionMessageTimeout */

    /* #40 set value for Timer ISO_Ed2_DIS */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_SequencePerformanceTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_SequencePerformanceTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_SequencePerformanceTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_SequencePerformanceTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_SequencePerformanceTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_CommunicationSetupTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_CommunicationSetupTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_CommunicationSetupTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_CommunicationSetupTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_CommunicationSetupTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_AuthorizationMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_AuthorizationMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_AuthorizationMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_AuthorizationMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_AuthorizationMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_CableCheckMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_CableCheckMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_CableCheckMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_CableCheckMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_CableCheckMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_CertificateInstallationMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_CertificateInstallationMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_CertificateInstallationMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_CertificateInstallationMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_CertificateInstallationMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_CertificateUpdateMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_CertificateUpdateMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_CertificateUpdateMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_CertificateUpdateMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_CertificateUpdateMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_ChargeParameterDiscoveryMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_ChargeParameterDiscoveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_ChargeParameterDiscoveryMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_ChargeParameterDiscoveryMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_ChargeParameterDiscoveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_ChargingStatusMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_ChargingStatusMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_ChargingStatusMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_ChargingStatusMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_ChargingStatusMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_CurrentDemandMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_CurrentDemandMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_CurrentDemandMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_CurrentDemandMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_CurrentDemandMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_MeteringReceiptMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_MeteringReceiptMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_MeteringReceiptMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_MeteringReceiptMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_MeteringReceiptMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_PaymentDetailsMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_PaymentDetailsMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_PaymentDetailsMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_PaymentDetailsMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_PaymentDetailsMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_PaymentServiceSelectionMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_PaymentServiceSelectionMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_PaymentServiceSelectionMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_PaymentServiceSelectionMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_PaymentServiceSelectionMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_PowerDeliveryMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_PowerDeliveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_PowerDeliveryMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_PowerDeliveryMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_PowerDeliveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_PreChargeMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_PreChargeMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_PreChargeMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_PreChargeMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_PreChargeMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_ServiceDetailMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_ServiceDetailMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_ServiceDetailMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_ServiceDetailMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_ServiceDetailMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_ServiceDiscoveryMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_ServiceDiscoveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_ServiceDiscoveryMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_ServiceDiscoveryMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_ServiceDiscoveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_SessionSetupMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_SessionSetupMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_SessionSetupMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_SessionSetupMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_SessionSetupMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_SessionStopMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_SessionStopMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_SessionStopMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_SessionStopMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_SessionStopMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_WeldingDetectionMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_WeldingDetectionMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_WeldingDetectionMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_WeldingDetectionMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_WeldingDetectionMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_FinePositioningSetupMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_FinePositioningSetupMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_FinePositioningSetupMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_FinePositioningSetupMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_FinePositioningSetupMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_FinePositioningMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_FinePositioningMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_FinePositioningMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_FinePositioningMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_FinePositioningMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_PairingMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_PairingMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_PairingMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_PairingMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_PairingMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_InitialAlignmentCheckMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_InitialAlignmentCheckMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_InitialAlignmentCheckMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_InitialAlignmentCheckMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_InitialAlignmentCheckMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_ISO_Ed2_DIS_PowerDemandMessageTimeout ) && ( Scc_ConfigType_Timer_ISO_Ed2_DIS_PowerDemandMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_ISO_Ed2_DIS_PowerDemandMessageTimeout:
      Scc_ConfigValue_Timer_ISO_Ed2_DIS_PowerDemandMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_ISO_Ed2_DIS_PowerDemandMessageTimeout */

    /* #50 set value for Timer DIN */

  #if ( defined Scc_ConfigType_Timer_DIN_SequencePerformanceTimeout ) && ( Scc_ConfigType_Timer_DIN_SequencePerformanceTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_SequencePerformanceTimeout:
      Scc_ConfigValue_Timer_DIN_SequencePerformanceTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_SequencePerformanceTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_CommunicationSetupTimeout ) && ( Scc_ConfigType_Timer_DIN_CommunicationSetupTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_CommunicationSetupTimeout:
      Scc_ConfigValue_Timer_DIN_CommunicationSetupTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_CommunicationSetupTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_ReadyToChargeTimeout ) && ( Scc_ConfigType_Timer_DIN_ReadyToChargeTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_ReadyToChargeTimeout:
      Scc_ConfigValue_Timer_DIN_ReadyToChargeTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_ReadyToChargeTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_ContractAuthenticationMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_ContractAuthenticationMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_ContractAuthenticationMessageTimeout:
      Scc_ConfigValue_Timer_DIN_ContractAuthenticationMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_ContractAuthenticationMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_CableCheckMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_CableCheckMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_CableCheckMessageTimeout:
      Scc_ConfigValue_Timer_DIN_CableCheckMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_CableCheckMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_ChargeParameterDiscoveryMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_ChargeParameterDiscoveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_ChargeParameterDiscoveryMessageTimeout:
      Scc_ConfigValue_Timer_DIN_ChargeParameterDiscoveryMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_ChargeParameterDiscoveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_CurrentDemandMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_CurrentDemandMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_CurrentDemandMessageTimeout:
      Scc_ConfigValue_Timer_DIN_CurrentDemandMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_CurrentDemandMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_ServicePaymentSelectionMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_ServicePaymentSelectionMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_ServicePaymentSelectionMessageTimeout:
      Scc_ConfigValue_Timer_DIN_ServicePaymentSelectionMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_ServicePaymentSelectionMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_PowerDeliveryMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_PowerDeliveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_PowerDeliveryMessageTimeout:
      Scc_ConfigValue_Timer_DIN_PowerDeliveryMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_PowerDeliveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_PreChargeMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_PreChargeMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_PreChargeMessageTimeout:
      Scc_ConfigValue_Timer_DIN_PreChargeMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_PreChargeMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_ServiceDiscoveryMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_ServiceDiscoveryMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_ServiceDiscoveryMessageTimeout:
      Scc_ConfigValue_Timer_DIN_ServiceDiscoveryMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_ServiceDiscoveryMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_SessionSetupMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_SessionSetupMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_SessionSetupMessageTimeout:
      Scc_ConfigValue_Timer_DIN_SessionSetupMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_SessionSetupMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_SessionStopMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_SessionStopMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_SessionStopMessageTimeout:
      Scc_ConfigValue_Timer_DIN_SessionStopMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_SessionStopMessageTimeout */

  #if ( defined Scc_ConfigType_Timer_DIN_WeldingDetectionMessageTimeout ) && ( Scc_ConfigType_Timer_DIN_WeldingDetectionMessageTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_Timer_DIN_WeldingDetectionMessageTimeout:
      Scc_ConfigValue_Timer_DIN_WeldingDetectionMessageTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_Timer_DIN_WeldingDetectionMessageTimeout */


#if ( SCC_ENABLE_STATE_MACHINE == STD_ON )

    /* #60 set value for OEM Vector - State Machine */

  #if ( defined Scc_ConfigType_StateM_AcceptUnsecureConnection ) && ( Scc_ConfigType_StateM_AcceptUnsecureConnection == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_AcceptUnsecureConnection:
      Scc_ConfigValue_StateM_AcceptUnsecureConnection = Data;
      break;
  #endif /* Scc_ConfigType_StateM_AcceptUnsecureConnection */

  #if ( defined Scc_ConfigType_StateM_ReconnectRetries ) && ( Scc_ConfigType_StateM_ReconnectRetries == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_ReconnectRetries:
      Scc_ConfigValue_StateM_ReconnectRetries = Data;
      break;
  #endif /* Scc_ConfigType_StateM_ReconnectRetries */

  #if ( defined Scc_ConfigType_StateM_ReconnectDelay ) && ( Scc_ConfigType_StateM_ReconnectDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_ReconnectDelay:
      Scc_ConfigValue_StateM_ReconnectDelay = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_ReconnectDelay */

  #if ( defined Scc_ConfigType_StateM_RequestInternetDetails ) && ( Scc_ConfigType_StateM_RequestInternetDetails == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_RequestInternetDetails:
      Scc_ConfigValue_StateM_RequestInternetDetails = Data;
      break;
  #endif /* Scc_ConfigType_StateM_RequestInternetDetails */

  #if ( defined Scc_ConfigType_StateM_AuthorizationOngoingTimeout ) && ( Scc_ConfigType_StateM_AuthorizationOngoingTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_AuthorizationOngoingTimeout:
      Scc_ConfigValue_StateM_AuthorizationOngoingTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_AuthorizationOngoingTimeout */

  #if ( defined Scc_ConfigType_StateM_ChargeParameterDiscoveryOngoingTimeout ) && ( Scc_ConfigType_StateM_ChargeParameterDiscoveryOngoingTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_ChargeParameterDiscoveryOngoingTimeout:
      Scc_ConfigValue_StateM_ChargeParameterDiscoveryOngoingTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_ChargeParameterDiscoveryOngoingTimeout */

  #if ( defined Scc_ConfigType_StateM_CableCheckOngoingTimeout ) && ( Scc_ConfigType_StateM_CableCheckOngoingTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_CableCheckOngoingTimeout:
      Scc_ConfigValue_StateM_CableCheckOngoingTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_CableCheckOngoingTimeout */

  #if ( defined Scc_ConfigType_StateM_PreChargeOngoingTimeout ) && ( Scc_ConfigType_StateM_PreChargeOngoingTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_PreChargeOngoingTimeout:
      Scc_ConfigValue_StateM_PreChargeOngoingTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_PreChargeOngoingTimeout */

  #if ( defined Scc_ConfigType_StateM_FinePositioningOngoingTimeout ) && ( Scc_ConfigType_StateM_FinePositioningOngoingTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_FinePositioningOngoingTimeout:
      Scc_ConfigValue_StateM_FinePositioningOngoingTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_FinePositioningOngoingTimeout */

  #if ( defined Scc_ConfigType_StateM_PairingOngoingTimeout ) && ( Scc_ConfigType_StateM_PairingOngoingTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_PairingOngoingTimeout:
      Scc_ConfigValue_StateM_PairingOngoingTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_PairingOngoingTimeout */

  #if ( defined Scc_ConfigType_StateM_InitialAlignmentCheckOngoingTimeout ) && ( Scc_ConfigType_StateM_InitialAlignmentCheckOngoingTimeout == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_InitialAlignmentCheckOngoingTimeout:
      Scc_ConfigValue_StateM_InitialAlignmentCheckOngoingTimeout = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_InitialAlignmentCheckOngoingTimeout */
  #if ( defined Scc_ConfigType_StateM_AuthorizationNextReqDelay ) && ( Scc_ConfigType_StateM_AuthorizationNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_AuthorizationNextReqDelay:
      Scc_ConfigValue_StateM_AuthorizationNextReqDelay = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_AuthorizationNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_ChargeParameterDiscoveryNextReqDelay ) && ( Scc_ConfigType_StateM_ChargeParameterDiscoveryNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_ChargeParameterDiscoveryNextReqDelay:
      Scc_ConfigValue_StateM_ChargeParameterDiscoveryNextReqDelay = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_ChargeParameterDiscoveryNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_ChargingStatusNextReqDelay ) && ( Scc_ConfigType_StateM_ChargingStatusNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_ChargingStatusNextReqDelay:
      Scc_ConfigValue_StateM_ChargingStatusNextReqDelay = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_ChargingStatusNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_CableCheckNextReqDelay ) && ( Scc_ConfigType_StateM_CableCheckNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_CableCheckNextReqDelay:
      Scc_ConfigValue_StateM_CableCheckNextReqDelay = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_CableCheckNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_PreChargeNextReqDelay ) && ( Scc_ConfigType_StateM_PreChargeNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_PreChargeNextReqDelay:
      Scc_ConfigValue_StateM_PreChargeNextReqDelay = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_PreChargeNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_CurrentDemandNextReqDelay ) && ( Scc_ConfigType_StateM_CurrentDemandNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_CurrentDemandNextReqDelay:
      Scc_ConfigValue_StateM_CurrentDemandNextReqDelay = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_CurrentDemandNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_WeldingDetectionNextReqDelay ) && ( Scc_ConfigType_StateM_WeldingDetectionNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_WeldingDetectionNextReqDelay:
      Scc_ConfigValue_StateM_WeldingDetectionNextReqDelay = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_WeldingDetectionNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_FinePositioningNextReqDelay ) && ( Scc_ConfigType_StateM_FinePositioningNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_FinePositioningNextReqDelay:
      Scc_ConfigValue_StateM_FinePositioningNextReqDelay = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_FinePositioningNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_PairingNextReqDelay ) && ( Scc_ConfigType_StateM_PairingNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_PairingNextReqDelay:
      Scc_ConfigValue_StateM_PairingNextReqDelay = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_PairingNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_InitialAlignmentCheckNextReqDelay ) && ( Scc_ConfigType_StateM_InitialAlignmentCheckNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_InitialAlignmentCheckNextReqDelay:
      Scc_ConfigValue_StateM_InitialAlignmentCheckNextReqDelay = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_InitialAlignmentCheckNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_PowerDemandNextReqDelay ) && ( Scc_ConfigType_StateM_PowerDemandNextReqDelay == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_PowerDemandNextReqDelay:
      Scc_ConfigValue_StateM_PowerDemandNextReqDelay = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_PowerDemandNextReqDelay */

  #if ( defined Scc_ConfigType_StateM_PaymentPrioritization ) && ( Scc_ConfigType_StateM_PaymentPrioritization == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_PaymentPrioritization:
      Scc_ConfigValue_StateM_PaymentPrioritization = Data;
      break;
  #endif /* Scc_ConfigType_StateM_PaymentPrioritization */

  #if ( defined Scc_ConfigType_StateM_ContractCertificateChainIndexInUse ) && ( Scc_ConfigType_StateM_ContractCertificateChainIndexInUse == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_ContractCertificateChainIndexInUse:
      Scc_ConfigValue_StateM_ContractCertificateChainIndexInUse = Data;
      break;
  #endif /* Scc_ConfigType_StateM_ContractCertificateChainIndexInUse */

  #if ( defined Scc_ConfigType_StateM_CertificateExpireThreshold ) && ( Scc_ConfigType_StateM_CertificateExpireThreshold == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_CertificateExpireThreshold:
      Scc_ConfigValue_StateM_CertificateExpireThreshold = Data;
      break;
  #endif /* Scc_ConfigType_StateM_CertificateExpireThreshold */

  #if ( defined Scc_ConfigType_StateM_RequestCertificateDetails ) && ( Scc_ConfigType_StateM_RequestCertificateDetails == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_RequestCertificateDetails:
      Scc_ConfigValue_StateM_RequestCertificateDetails = Data;
      break;
  #endif /* Scc_ConfigType_StateM_RequestCertificateDetails */

  #if ( defined Scc_ConfigType_StateM_RequestCertificateInstallation ) && ( Scc_ConfigType_StateM_RequestCertificateInstallation == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_RequestCertificateInstallation:
      Scc_ConfigValue_StateM_RequestCertificateInstallation = Data;
      break;
  #endif /* Scc_ConfigType_StateM_RequestCertificateInstallation */

  #if ( defined Scc_ConfigType_StateM_RequestCertificateUpdate ) && ( Scc_ConfigType_StateM_RequestCertificateUpdate == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
    case Scc_DynConfigParam_StateM_RequestCertificateUpdate:
      Scc_ConfigValue_StateM_RequestCertificateUpdate = Data;
      break;
  #endif /* Scc_ConfigType_StateM_RequestCertificateUpdate */

  #if ( defined Scc_ConfigType_StateM_SLACStartMode ) && ( Scc_ConfigType_StateM_SLACStartMode == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* SLAC Start Mode */
    case Scc_DynConfigParam_StateM_SLACStartMode:
      Scc_ConfigValue_StateM_SLACStartMode = Data;
      break;
  #endif /* Scc_ConfigType_StateM_SLACStartMode */

  #if ( defined Scc_ConfigType_StateM_QCAIdleTimer ) && ( Scc_ConfigType_StateM_QCAIdleTimer == SCC_CFG_TYPE_DYNAMIC ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      /* QCA Idle Timer */
    case Scc_DynConfigParam_StateM_QCAIdleTimer:
      Scc_ConfigValue_StateM_QCAIdleTimer = ( Data / SCC_MAIN_FUNCTION_PERIOD_MS );
      break;
  #endif /* Scc_ConfigType_StateM_QCAIdleTimer */

  #endif /* SCC_ENABLE_STATE_MACHINE */

      /* invalid DataID */
    default:
      RetVal = E_NOT_OK;
      break;
    }

    if ( RetVal == E_OK )
    {
      /* inform the NvM to update the NVRAM */
      (void) NvM_SetRamBlockStatus((NvM_BlockIdType)SCC_DIAG_PARAMETERS_NVM_BLOCK, TRUE);
    }
  }

  /* ----- Development Error Report --------------------------------------- */
#if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #70 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_DIAG_DATA_WRITE_ACCESS, errorId);
  }
#else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* SCC_DEV_ERROR_REPORT */

  return RetVal;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */
#endif /* SCC_NUM_OF_DYN_CONFIG_PARAMS */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_DiagDataReadAccess
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Scc_ReturnType, SCC_CODE) Scc_DiagDataReadAccess(Scc_DiagParamsType DataID,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) DataLenPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = SCC_DET_NO_ERROR;
  Scc_ReturnType RetVal = Scc_ReturnType_OK;

  /* ----- Development Error Checks ------------------------------------- */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of runtime parameters. */
  if ( Scc_State == Scc_State_Uninitialized )
  {
    errorId = SCC_DET_NOT_INITIALIZED;
    RetVal = Scc_ReturnType_NotOK;
  }
  else if ( DataPtr == NULL_PTR )
  {
    errorId = SCC_DET_INV_POINTER;
    RetVal = Scc_ReturnType_NotOK;
  }
  else if ( DataLenPtr == NULL_PTR )
  {
    errorId = SCC_DET_INV_POINTER;
    RetVal = Scc_ReturnType_NotOK;
  }
  else
#endif /* SCC_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 get the Contract Certificate Chain Size */
    if ( Scc_DP_ContractCertificateChainSize == DataID )
    {
      if ( Scc_NvMBlockReadState_Processed == Scc_CertsWs.ContrCertChainSizeReadState )
      {
        *DataPtr = (uint8)Scc_CertsWs.ContrCertChainSize;
        *DataLenPtr = 1;
      }
      else if ( Scc_NvMBlockReadState_Invalidated == Scc_CertsWs.ContrCertChainSizeReadState )
      {
        /* the NvM block is "empty" */
        *DataLenPtr = 0;
      }
      else if ( Scc_NvMBlockReadState_Error != Scc_CertsWs.ContrCertChainSizeReadState )
      {
        /* read the contract certificate chain */
        Scc_NvmReadContrCertChain(FALSE);
        /* set the return value */
        RetVal = Scc_ReturnType_Pending;
      }
      else
      {
        RetVal = Scc_ReturnType_NotOK;
      }
    }

    /* #30 get the Contract Certificate */
    else if ( Scc_DP_ContractCertificate == DataID )
    {
      if ( Scc_NvMBlockReadState_Processed == Scc_CertsWs.ContrCertReadState )
      {
        if ( *DataLenPtr >= Scc_CertsWs.ContrCert->Length )
        {
          /* copy the provisioning certificate to the out buffer */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy  */
          IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))DataPtr,
            (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrCert->Buffer[0],
            Scc_CertsWs.ContrCert->Length);
          /* set the real length of the certificate */
          *DataLenPtr = Scc_CertsWs.ContrCert->Length;
        }
        else
        {
          RetVal = Scc_ReturnType_NotOK;
        }
      }
      else if ( Scc_NvMBlockReadState_Invalidated == Scc_CertsWs.ContrCertReadState )
      {
        /* the NvM block is "empty" */
        *DataLenPtr = 0;
      }
      else if ( Scc_NvMBlockReadState_Error != Scc_CertsWs.ContrCertReadState )
      {
        /* read the contract certificate chain */
        Scc_NvmReadContrCertChain(FALSE);
        /* set the return value */
        RetVal = Scc_ReturnType_Pending;
      }
      else
      {
        RetVal = Scc_ReturnType_NotOK;
      }
    }

    /* #40 get the EMAID */
    else if ( Scc_DP_EMAID == DataID )
    {
      if ( Scc_NvMBlockReadState_Processed == Scc_CertsWs.ContrCertReadState )
      {
        if ( *DataLenPtr >= Scc_CertsWs.eMAID->Length )
        {
          /* copy the eMAID to the out buffer */ /* PRQA S 0310, 3305, 0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
          IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))DataPtr,
            (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.eMAID->Buffer[0],
            Scc_CertsWs.eMAID->Length);
          /* set the real length of the eMAID */
          *DataLenPtr = Scc_CertsWs.eMAID->Length;
        }
        else
        {
          RetVal = Scc_ReturnType_NotOK;
        }
      }
      else if ( Scc_NvMBlockReadState_Invalidated == Scc_CertsWs.ContrCertReadState )
      {
        /* the NvM block is "empty" */
        *DataLenPtr = 0;
      }
      else if ( Scc_NvMBlockReadState_Error != Scc_CertsWs.ContrCertReadState )
      {
        /* read the contract certificate chain */
        Scc_NvmReadContrCertChain(FALSE);
        /* set the return value */
        RetVal = Scc_ReturnType_Pending;
      }
      else
      {
        RetVal = Scc_ReturnType_NotOK;
      }
    }

    /* #50 get the Contract Sub Certificates */
    else if ( (uint8)Scc_DP_ContractSubCertificate1 == ( (uint8)DataID & (uint8)0xF0u ) )
    {
      /* get the contr sub cert nr */
      uint8 ContrSubCertNr = (uint8)DataID & (uint8)0x0Fu;

      /* check if the requested NvM block does not exist */
      if ( ContrSubCertNr >= Scc_CertsWs.ContrSubCertCnt )
      {
        RetVal = Scc_ReturnType_NotOK;
      }
      /* NvM block exists */
      else
      {
        /* check if the certificate was already read */
        if ( Scc_NvMBlockReadState_Processed == Scc_CertsWs.ContrSubCertsReadStates[ContrSubCertNr] )
        {
          /* check if the out buffer is big enough */
          if ( *DataLenPtr >= Scc_CertsWs.ContrSubCerts[ContrSubCertNr].Length )
          {
            /* copy the contract sub certificate to the out buffer */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
            IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))DataPtr,
              (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrSubCerts[ContrSubCertNr].Buffer[0],
              Scc_CertsWs.ContrSubCerts[ContrSubCertNr].Length);
            /* set the real length of the certificate */
            *DataLenPtr = Scc_CertsWs.ContrSubCerts[ContrSubCertNr].Length;
          }
          else
          {
            RetVal = Scc_ReturnType_NotOK;
          }
        }
        /* check if the NvM block is empty */
        else if ( Scc_NvMBlockReadState_Invalidated == Scc_CertsWs.ContrSubCertsReadStates[ContrSubCertNr] )
        {
          /* the NvM block is "empty" */
          *DataLenPtr = 0;
        }
        /* check if the NvM is still reading this certificate */
        else if ( Scc_NvMBlockReadState_Error != Scc_CertsWs.ContrSubCertsReadStates[ContrSubCertNr] )
        {
          /* read the contract certificate chain */
          Scc_NvmReadContrCertChain(TRUE);
          /* set the return value */
          RetVal = Scc_ReturnType_Pending;
        }
        /* an error occurred */
        else
        {
          RetVal = Scc_ReturnType_NotOK;
        }
      }
    }

  #if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
    /* #60 get the Provisioning Certificate */
    else if ( Scc_DP_ProvisioningCertificate == DataID )
    {
      if ( Scc_NvMBlockReadState_Processed == Scc_CertsWs.ProvCertReadState )
      {
        if ( *DataLenPtr >= Scc_CertsWs.ProvCert->Length )
        {
          /* copy the provisioning certificate to the out buffer */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
          IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))DataPtr,
            (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ProvCert->Buffer[0],
            Scc_CertsWs.ProvCert->Length);
          /* set the real length of the certificate */
          *DataLenPtr = Scc_CertsWs.ProvCert->Length;
        }
        else
        {
          RetVal = Scc_ReturnType_NotOK;
        }
      }
      else if ( Scc_NvMBlockReadState_Invalidated == Scc_CertsWs.ProvCertReadState )
      {
        /* the NvM block is "empty" */
        *DataLenPtr = 0;
      }
      /* the NvM is still reading this certificate */
      else if ( Scc_NvMBlockReadState_Error != Scc_CertsWs.ProvCertReadState )
      {
        /* read the provisioning certificates */
        Scc_NvmReadProvCert();
        /* set the return value */
        RetVal = Scc_ReturnType_Pending;
      }
      /* an error occurred */
      else
      {
        RetVal = Scc_ReturnType_NotOK;
      }
    }
    /* #70 get the PCID */
    else if ( Scc_DP_PCID == DataID )
    {
      if ( Scc_NvMBlockReadState_Processed == Scc_CertsWs.ProvCertReadState )
      {
        if ( *DataLenPtr >= Scc_CertsWs.ProvCertIDLen )
        {
          /* copy the PCID to the out buffer */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
          IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))DataPtr,
            (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ProvCertID[0],
            Scc_CertsWs.ProvCertIDLen);
          /* set the real length of the PCID */
          *DataLenPtr = Scc_CertsWs.ProvCertIDLen;
        }
        else
        {
          RetVal = Scc_ReturnType_NotOK;
        }
      }
      else if ( Scc_NvMBlockReadState_Invalidated == Scc_CertsWs.ProvCertReadState )
      {
        /* the NvM block is "empty" */
        *DataLenPtr = 0;
      }
      else if ( Scc_NvMBlockReadState_Error != Scc_CertsWs.ProvCertReadState )
      {
        /* read the provisioning certificate */
        Scc_NvmReadProvCert();
        /* set the return value */
        RetVal = Scc_ReturnType_Pending;
      }
      else
      {
        RetVal = Scc_ReturnType_NotOK;
      }
    }
  #endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

    /* #80 get the Root Certificates */
    else if ( (uint8)Scc_DP_RootCertificate1 == ( (uint8)DataID & (uint8)0xF0u ) )
    {
      /* get the root cert nr */
      uint8 RootCertNr = (uint8)DataID & (uint8)0x0Fu;

      /* check if the requested root certificate exists */
      if ( Scc_CertsNvm.RootCertNvmCnt > RootCertNr )
      {
        /* check if the certificate was already read */
        if ( Scc_NvMBlockReadState_Processed == Scc_CertsWs.RootCertsReadStates[RootCertNr] )
        {
          /* check if the out buffer is big enough */
          if ( *DataLenPtr >= Scc_CertsNvm.RootCerts[RootCertNr].RootCert->Length )
          {
            /* copy the root certificate to the out buffer */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
            IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))DataPtr,
              (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsNvm.RootCerts[RootCertNr].RootCert->Buffer[0],
              Scc_CertsNvm.RootCerts[RootCertNr].RootCert->Length);
            /* set the real length of the certificate */
            *DataLenPtr = Scc_CertsNvm.RootCerts[RootCertNr].RootCert->Length;
          }
          else
          {
            /* output buffer to small */
            RetVal = Scc_ReturnType_NotOK;
          }
        }
        else if ( Scc_NvMBlockReadState_Invalidated == Scc_CertsWs.RootCertsReadStates[RootCertNr] )
        {
          /* the NvM block is "empty" */
          *DataLenPtr = 0;
        }
        /* the NvM is still reading this certificate */
        else if ( Scc_NvMBlockReadState_Error != Scc_CertsWs.RootCertsReadStates[RootCertNr] )
        {
          /* read the root certificates */
          Scc_NvmReadRootCerts(FALSE, RootCertNr);
          /* set the return value */
          RetVal = Scc_ReturnType_Pending;
        }
        /* an error occurred */
        else
        {
          *DataLenPtr = 0;
          RetVal = Scc_ReturnType_NotOK;
        }
      }
      /* invalid root cert index */
      else
      {
        RetVal = Scc_ReturnType_NotOK;
      }
    }

    /* unknown DataID */
    else
    {
      RetVal = Scc_ReturnType_NotOK;
#if(SCC_DEV_ERROR_REPORT == STD_ON)
      (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_DIAG_DATA_READ_ACCESS, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
    }
  }

  /* ----- Development Error Report --------------------------------------- */
#if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #90 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_DIAG_DATA_READ_ACCESS, errorId);
  }
#else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* SCC_DEV_ERROR_REPORT */
  return RetVal;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_DiagDataWriteAccess
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Scc_ReturnType, SCC_CODE) Scc_DiagDataWriteAccess(Scc_DiagParamsType DataID,
  P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) DataPtr, uint16 DataLen)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = SCC_DET_NO_ERROR;
  Scc_ReturnType RetVal = Scc_ReturnType_NotOK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  uint8          NvmReadState = NVM_REQ_PENDING;

  /* ----- Development Error Checks ------------------------------------- */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of runtime parameters. */
  if ( Scc_State == Scc_State_Uninitialized )
  {
    errorId = SCC_DET_NOT_INITIALIZED;
  }
  else if ( DataPtr == NULL_PTR )
  {
    errorId = SCC_DET_INV_POINTER;
  }
  else if ( DataLen == 0u )
  {
    errorId = SCC_DET_INV_PARAM;
  }
  else
#endif /* SCC_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* EMAID can be written even when charging session is in progress */
    /* #20 Write EMAID to Scc_CertsWs */
    if ( DataID == Scc_DP_EMAID )
    {
      if ( DataLen <= sizeof(Scc_CertsWs.eMAID->Buffer) )
      {
        /* write the new EMAID */ /* PRQA S 0310,3305,0315 1 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.eMAID->Buffer[0],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))DataPtr, DataLen);
        Scc_CertsWs.eMAID->Length = DataLen;

        RetVal = Scc_ReturnType_OK;
      }
    }
    /* certificate jobs are only allowed before or after charging */
    else if (   ( Scc_State_TLConnecting <= Scc_State )
        && ( Scc_State_Connected >= Scc_State ))
    {
      RetVal = Scc_ReturnType_Busy;
    }
    else
    {
      /* #30 Write ContractCertificateChainSize to NvM and Scc_CertsWs. */
      if ( Scc_DP_ContractCertificateChainSize == DataID )
      {
        /* check if the NvM is not busy */
        (void) NvM_GetErrorStatus((NvM_BlockIdType)Scc_GetNvMBlockIDContrCertChainSize(Scc_CertsWs.ChosenContrCertChainIdx), &NvmReadState);
        if ( NVM_REQ_PENDING == NvmReadState )
        {
          RetVal = Scc_ReturnType_Pending;
        }
        /* check if the in-data is not too big or invalid */
        else if (   ( DataLen <= Scc_GetNvMBlockLenContrCertChainSize(Scc_CertsWs.ChosenContrCertChainIdx) )
                 && ( Scc_CertsWs.ContrSubCertCnt >= (uint8)DataPtr[0] ))
        {
          sint8_least Counter;

          /* write the new contract certificate chain size to the RAM */
          Scc_CertsWs.ContrCertChainSize = (sint8)DataPtr[0];

          /* check if sub certificates are available */
          if ( 0 < Scc_CertsWs.ContrCertChainSize )
          {
            /* connect the sub certificates */
            for ( Counter = 1; Counter < Scc_CertsWs.ContrCertChainSize; Counter++ )
            {
              Scc_CertsWs.ContrSubCerts[Counter-1].NextCertificatePtr = &Scc_CertsWs.ContrSubCerts[Counter];
            }
            Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize-1].NextCertificatePtr =
              (P2VAR(Exi_ISO_certificateType, AUTOMATIC, SCC_VAR_NOINIT))NULL_PTR;
          }

          /* update the read state of the chain size */
          Scc_CertsWs.ContrCertChainSizeReadState = Scc_NvMBlockReadState_Processed;
          /* write the new contract certificate chain size to the NvM */ /* PRQA S 0314,4342 2 */ /* MD_Scc_Nvm_Generic, */
          RetVal = (Scc_ReturnType)NvM_WriteBlock((NvM_BlockIdType)Scc_GetNvMBlockIDContrCertChainSize(Scc_CertsWs.ChosenContrCertChainIdx),
            (P2CONST(void, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrCertChainSize);
        }
        /* the in-data is too big */
        else
        {
          RetVal = Scc_ReturnType_NotOK;
        }
      }
      /* #40 Write ContractCertificate to NvM and Scc_CertsWs. */
      else if ( Scc_DP_ContractCertificate == DataID )
      {
        /* check if the NvM is not busy */
        (void) NvM_GetErrorStatus((NvM_BlockIdType)Scc_GetNvMBlockIDContrCert(Scc_CertsWs.ChosenContrCertChainIdx), &NvmReadState);
        if ( NVM_REQ_PENDING == NvmReadState )
        {
          RetVal = Scc_ReturnType_Pending;
        }
        /* check if the in-data is not too big */
        else if ( DataLen <= Scc_GetNvMBlockLenContrCert(Scc_CertsWs.ChosenContrCertChainIdx) )
        {
          /* temp params */ /* PRQA S 0781 2 */ /* MD_Scc_0781 */
          uint16 CertLen = Scc_GetNvMBlockLenContrCert(Scc_CertsWs.ChosenContrCertChainIdx);
          Exi_ISO_eMAIDType EMAID = { 0x00, { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                              0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }};
          /* get the size of the certificate */
          RetVal = (Scc_ReturnType)Scc_GetCertSize(&DataPtr[0], &CertLen); /* PRQA S 4342 */ /* MD_Scc_ReturnType */
          if ( Scc_ReturnType_OK == RetVal )
          {
            /* check if the certificate is longer than the provided data */
            if ( CertLen > DataLen )
            {
              RetVal = Scc_ReturnType_NotOK;
            }
            /* certificate is fully contained in the provided data */
            else
            {
              /* get the eMAID */
              EMAID.Length = sizeof(EMAID.Buffer);
              /* PRQA S 4342 2 */ /* MD_Scc_ReturnType */
              RetVal = (Scc_ReturnType)Scc_GetCertDistinguishedNameObject(&DataPtr[0], CertLen,
                &EMAID.Buffer[0], &EMAID.Length, Scc_BEROID_Subject_EMAID);
            }
          }
          if ( Scc_ReturnType_OK == RetVal )
          {
            /* set the next pointer of the certificate */
            Scc_CertsWs.ContrCert->NextCertificatePtr = (P2VAR(Exi_ISO_certificateType, AUTOMATIC, SCC_VAR_NOINIT))NULL_PTR;
            /* set the length of the certificate */
            Scc_CertsWs.ContrCert->Length = CertLen;
            /* write the new contract certificate to the RAM */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
            IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrCert->Buffer[0],
              (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))DataPtr, DataLen);
            /* copy the EMAID */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
            IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.eMAID->Buffer[0],
              (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&EMAID.Buffer[0], EMAID.Length);
            Scc_CertsWs.eMAID->Length = EMAID.Length;
            /* set the read state */
            Scc_CertsWs.ContrCertReadState = Scc_NvMBlockReadState_Processed;
            /* write the new contract certificate to the NvM */ /* PRQA S 0314, 4342 3 */ /* MD_Scc_Nvm_Generic,MD_Scc_ReturnType */
            RetVal = (Scc_ReturnType)NvM_WriteBlock(
              (NvM_BlockIdType)Scc_GetNvMBlockIDContrCert(Scc_CertsWs.ChosenContrCertChainIdx),
              (P2VAR(void,AUTOMATIC,SCC_APPL_DATA))&Scc_CertsWs.ContrCert->Buffer[0]);
          }
        }
        /* if the in-data was too big */
        else
        {
          RetVal = Scc_ReturnType_NotOK;
        }
      }
      /* #50 Write ContractCertificatePrivateKey to CSM. */
      else if ( Scc_DP_ContractCertificatePrivateKey  == DataID )
      {
        if( (Std_ReturnType)E_OK != Csm_KeyElementSet(Scc_ECDSASignCertKeyIds[Scc_CertsWs.ChosenContrCertChainIdx], CRYPTO_KE_SIGNATURE_KEY, DataPtr, DataLen))
        {
          RetVal = Scc_ReturnType_NotOK;
        }
        else if( (Std_ReturnType)E_OK != Csm_KeySetValid(Scc_ECDSASignCertKeyIds[Scc_CertsWs.ChosenContrCertChainIdx])) /* PRQA S 2004 1 */ /* MD_Scc_2004 */
        {
          RetVal = Scc_ReturnType_NotOK;
        }
        else
        {
          RetVal = Scc_ReturnType_OK;
        }
      }

      /* #60 Write ContractSubCertificate to NvM and Scc_CertsWs. */
      else if ( (uint8)Scc_DP_ContractSubCertificate1 == ( (uint8)DataID & (uint8)0xF0u ))
      {
        /* get the contract sub cert nr */
        uint8 ContrSubCertNr = (uint8)DataID & (uint8)0x0Fu;
        /* set the return value to failed, it will be set to passed if operation was successful */
        RetVal = Scc_ReturnType_NotOK;

        /* check if the requested contract sub certificate exists */
        if ( Scc_CertsWs.ContrSubCertCnt > ContrSubCertNr )
        {
          /* check if the NvM is not busy */
          (void) NvM_GetErrorStatus(
            (NvM_BlockIdType)Scc_GetNvMBlockIDContrSubCerts(Scc_CertsWs.ChosenContrCertChainIdx, ContrSubCertNr),
            &NvmReadState);
          if ( NVM_REQ_PENDING == NvmReadState )
          {
            RetVal = Scc_ReturnType_Pending;
          }
          /* check if the in-data is not too big */
          else if ( DataLen <= Scc_GetNvMBlockLenContrSubCerts(Scc_CertsWs.ChosenContrCertChainIdx, ContrSubCertNr) )
          {
            /* temp params */ /* PRQA S 0781 1 */ /* MD_Scc_0781 */
            uint16 CertLen = Scc_GetNvMBlockLenContrSubCerts(Scc_CertsWs.ChosenContrCertChainIdx, ContrSubCertNr);
            /* get the size of the certificate */ /* PRQA S 4342 1 */ /* MD_Scc_ReturnType */
            RetVal = (Scc_ReturnType)Scc_GetCertSize(&DataPtr[0], &CertLen);
            if ( Scc_ReturnType_OK == RetVal )
            {
              /* check if the certificate is longer than the provided data */
              if ( CertLen > DataLen )
              {
                RetVal = Scc_ReturnType_NotOK;
              }
            }
            if ( Scc_ReturnType_OK == RetVal )
            {
              /* set the length */
              Scc_CertsWs.ContrSubCerts[ContrSubCertNr].Length = CertLen;
              /* write the new contract sub certificate to the RAM */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
              IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrSubCerts[ContrSubCertNr].Buffer[0],
                (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))DataPtr, DataLen);
              /* set the read state */
              Scc_CertsWs.ContrSubCertsReadStates[ContrSubCertNr] = Scc_NvMBlockReadState_Processed;
              /* update the status flag */
              Scc_CertsWs.ContrSubCertsProcessedFlags |= ((uint32)((uint32)0x01 << (uint32)ContrSubCertNr));
              /* copy the contract sub certificate to the NvM */  /* PRQA S 0314, 4342 3 */ /* MD_Scc_Nvm_Generic,MD_Scc_ReturnType */
              RetVal = (Scc_ReturnType)NvM_WriteBlock(
                (NvM_BlockIdType)Scc_GetNvMBlockIDContrSubCerts(Scc_CertsWs.ChosenContrCertChainIdx, ContrSubCertNr),
                (P2CONST(void,AUTOMATIC,SCC_APPL_DATA))DataPtr);
            }
          }
          /* if the in-data was too big */
          else
          {
            RetVal = Scc_ReturnType_NotOK;
          }
        }
      }

    #if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
      /* #70 Write ProvisioningCertificate to NvM and Scc_CertsWs. */
      else if ( Scc_DP_ProvisioningCertificate == DataID )
      {
        /* check if the NvM is not busy */
        (void) NvM_GetErrorStatus((NvM_BlockIdType)Scc_GetNvMBlockIDProvCert(Scc_CertsWs.ChosenContrCertChainIdx), &NvmReadState);
        if ( NVM_REQ_PENDING == NvmReadState )
        {
          RetVal = Scc_ReturnType_Pending;
        }
        /* check if the in-data is not too big */
        else if ( DataLen <= Scc_GetNvMBlockLenProvCert(Scc_CertsWs.ChosenContrCertChainIdx) )
        {
          /* temp params */ /* PRQA S 0781 3 */ /* MD_Scc_0781 */
          uint16 CertLen = Scc_GetNvMBlockLenProvCert(Scc_CertsWs.ChosenContrCertChainIdx);
          uint8  lProvCertID[SCC_PROV_CERT_ID_MAX_LEN];
          uint16 lProvCertIDLen = SCC_PROV_CERT_ID_MAX_LEN;

          /* get the size of the certificate */ /* PRQA S 4342 1 */ /* MD_Scc_ReturnType */
          RetVal = (Scc_ReturnType)Scc_GetCertSize(&DataPtr[0], &CertLen);
          if ( Scc_ReturnType_OK == RetVal )
          {
            /* check if the certificate is longer than the provided data */
            if ( CertLen > DataLen )
            {
              RetVal = Scc_ReturnType_NotOK;
            }
            /* certificate is fully contained in the provided data */
            else
            {
              /* get the PCID */
              RetVal = (Scc_ReturnType)Scc_GetCertDistinguishedNameObject(&DataPtr[0], CertLen, /* PRQA S 4342 */ /* MD_Scc_ReturnType */
                &lProvCertID[0], &lProvCertIDLen, Scc_BEROID_Subject_CommonName);
            }
          }
          if ( Scc_ReturnType_OK == RetVal )
          {
            /* set the next pointer of the certificate */
            Scc_CertsWs.ProvCert->NextCertificatePtr = (P2VAR(Exi_ISO_certificateType, AUTOMATIC, SCC_VAR_NOINIT))NULL_PTR;
            /* set the length of the certificate */
            Scc_CertsWs.ProvCert->Length = CertLen;
            /* write the new contract certificate to the RAM */ /* PRQA S 0315 2 */ /* MD_MSR_VStdLibCopy */
            IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ProvCert->Buffer[0],
              (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))DataPtr, DataLen);
            /* copy the ProvCertID */ /* PRQA S 0310,3305 3 */ /* MD_Scc_0310_0314_0316_3305 */
              /*lint -e(645) If ProvCertID would not be initialized, RetVal would not be equal to E_OK */
            IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ProvCertID[0], /* PRQA S 0315 */ /* MD_MSR_VStdLibCopy */
              (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&lProvCertID[0], lProvCertIDLen);
              /*lint +e645 */
            Scc_CertsWs.ProvCertIDLen = lProvCertIDLen;
            /* set the read state */
            Scc_CertsWs.ProvCertReadState = Scc_NvMBlockReadState_Processed;
            /* write the new provisioning certificate to the NvM */ /* PRQA S 0314,4342 2 */ /* MD_Scc_Nvm_Generic,MD_Scc_ReturnType */
            RetVal = (Scc_ReturnType)NvM_WriteBlock((NvM_BlockIdType)Scc_GetNvMBlockIDProvCert(Scc_CertsWs.ChosenContrCertChainIdx),
              (P2VAR(void,AUTOMATIC,SCC_APPL_DATA))&Scc_CertsWs.ProvCert->Buffer[0]);
          }
        }
        /* if the in-data was too big */
        else
        {
          RetVal = Scc_ReturnType_NotOK;
        }
      }

      /* #80 Write ProvisioningCertificatePrivateKey to CSM. */
      else if ( Scc_DP_ProvisioningCertificatePrivateKey == DataID )
      {
        if( (Std_ReturnType)E_OK != Csm_KeyElementSet(Scc_ECDSASignProvCertKeyId, CRYPTO_KE_SIGNATURE_KEY, DataPtr, DataLen))
        {
          RetVal = Scc_ReturnType_NotOK;
        }
        else if( (Std_ReturnType)E_OK != Csm_KeySetValid(Scc_ECDSASignProvCertKeyId)) /* PRQA S 2004 1 */ /* MD_Scc_2004 */
        {
          RetVal = Scc_ReturnType_NotOK;
        }
        else
        {
          RetVal = Scc_ReturnType_OK;
        }
      }
    #endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

      /* #90 Write RootCertificate to NvM. */
      else if ( (uint8)Scc_DP_RootCertificate1 == ( (uint8)DataID & (uint8)0xF0u ))
      {
        /* get the root cert nr */
        uint8 RootCertNr = (uint8)DataID & (uint8)0x0Fu;
        /* set the return value to failed, it will be set to passed if operation was successful */
        RetVal = Scc_ReturnType_NotOK;

        /* check if the requested root certificate exists */
        if ( Scc_CertsNvm.RootCertNvmCnt > RootCertNr )
        {
          /* check if the NvM is not busy */
          (void) NvM_GetErrorStatus((NvM_BlockIdType)Scc_CertsNvm.RootCerts[RootCertNr].NvmBlockId, &NvmReadState);
          if ( NVM_REQ_PENDING == NvmReadState )
          {
            RetVal = Scc_ReturnType_Pending;
          }
          /* check if the in-data is not too big */
          else if ( DataLen <= Scc_CertsNvm.RootCerts[RootCertNr].NvmBlockLen )
          {
            /* temp params */
            uint16 CertLen = Scc_CertsNvm.RootCerts[RootCertNr].NvmBlockLen; /* PRQA S 0781 */ /* MD_Scc_0781 */
            /* get the size of the certificate */
            RetVal = (Scc_ReturnType)Scc_GetCertSize(&DataPtr[0], &CertLen); /* PRQA S 4342 */ /* MD_Scc_ReturnType */
            if ( Scc_ReturnType_OK == RetVal )
            {
              /* check if the certificate is longer than the provided data */
              if ( CertLen > DataLen )
              {
                RetVal = Scc_ReturnType_NotOK;
              }
            }
            if ( Scc_ReturnType_OK == RetVal )
            {
              /* set the next pointer of the certificate */
              Scc_CertsNvm.RootCerts[RootCertNr].RootCert->NextCertificatePtr =
                (P2VAR(Exi_ISO_certificateType, AUTOMATIC, SCC_VAR_NOINIT))NULL_PTR;
              /* set the length of the certificate */
              Scc_CertsNvm.RootCerts[RootCertNr].RootCert->Length = CertLen;
              /* write the new root certificate to the RAM */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
              IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsNvm.RootCerts[RootCertNr].RootCert->Buffer[0],
                (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))DataPtr, DataLen);
              /* set the read state */
              Scc_CertsWs.RootCertsReadStates[RootCertNr] = Scc_NvMBlockReadState_Processed;
              /* processing of this root cert is finished */
              Scc_CertsWs.RootCertsProcessedFlags |= ( (uint32)0x01 << (uint32)RootCertNr );
              /* copy the root certificate to the nvm */ /* PRQA S 0314, 4342 2 */ /* MD_Scc_Nvm_Generic,MD_Scc_ReturnType */
              RetVal = (Scc_ReturnType)NvM_WriteBlock((NvM_BlockIdType)Scc_CertsNvm.RootCerts[RootCertNr].NvmBlockId,
                (P2CONST(void,AUTOMATIC,SCC_APPL_DATA))DataPtr);
            }

    #if ( defined TLS_SUPPORT_ROOT_CERT_UPDATE ) && ( TLS_SUPPORT_ROOT_CERT_UPDATE == STD_ON )
            /* check if the operation was successful */
            if ( Scc_ReturnType_OK == RetVal )
            {
              /* inform TLS about the updated root cert */
              (void) TcpIp_Tls_RootCertWasModified((NvM_BlockIdType)Scc_CertsNvm.RootCerts[RootCertNr].NvmBlockId);
            }
    #endif /* TLS_SUPPORT_ROOT_CERT_UPDATE */
          }
          /* if the in-data was too big */
          else
          {
            /* set the return value */
            RetVal = Scc_ReturnType_NotOK;
          }
        }
      }
      else
      {
        RetVal = Scc_ReturnType_NotOK;
  #if(SCC_DEV_ERROR_REPORT == STD_ON)
        errorId = SCC_DET_INV_PARAM;
  #endif /* SCC_DEV_ERROR_REPORT */
      }
    }
  }

  /* ----- Development Error Report --------------------------------------- */
#if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #100 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_DIAG_DATA_WRITE_ACCESS, errorId);
  }
#else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* SCC_DEV_ERROR_REPORT */

  return RetVal;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_DiagDataGetBlockStatus
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_DiagDataGetBlockStatus(Scc_DiagParamsType DataID,
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) NvmResultPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType RetVal = (Std_ReturnType)E_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get NvM block status of ContractCertificateChainSize. */
  if ( Scc_DP_ContractCertificateChainSize == DataID )
  {
    /* get the current status of this NvM block */
    (void) NvM_GetErrorStatus((NvM_BlockIdType)Scc_GetNvMBlockIDContrCertChainSize(Scc_CertsWs.ChosenContrCertChainIdx), NvmResultPtr);
  }
  /* #20 Get NvM block status of ContractCertificate. */
  else if ( Scc_DP_ContractCertificate == DataID )
  {
    /* get the current status of this NvM block */
    (void) NvM_GetErrorStatus((NvM_BlockIdType)Scc_GetNvMBlockIDContrCert(Scc_CertsWs.ChosenContrCertChainIdx), NvmResultPtr);
  }
  /* #30 Get NvM block status of ContractCertificatePrivateKey. */
  else if ( Scc_DP_ContractCertificatePrivateKey  == DataID )
  {
    /* The private Key is stored via the CSM, check status of private key via CSM */
    Scc_ReportError(Scc_StackError_InvalidRxParameter);
    RetVal = (Std_ReturnType)E_NOT_OK;
  }
  /* #40 Get NvM block status of ContractSubCertificate. */
  else if ( (uint8)Scc_DP_ContractSubCertificate1 == ( (uint8)DataID & (uint8)0xF0u ))
  {
    /* get the contract sub cert nr */
    uint8 ContrSubCertNr = (uint8)DataID & (uint8)0x0Fu;

    /* check if the requested contract sub certificate exists */
    if ( Scc_CertsWs.ContrSubCertCnt > ContrSubCertNr )
    {
      /* get the current status of this NvM block */
      (void) NvM_GetErrorStatus(
        (NvM_BlockIdType)Scc_GetNvMBlockIDContrSubCerts(Scc_CertsWs.ChosenContrCertChainIdx, ContrSubCertNr),
        NvmResultPtr);
    }
    /* requested sub certificate does not exist */
    else
    {
      RetVal = (Std_ReturnType)E_NOT_OK;
    }
  }

#if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
  /* #50 Get NvM block status of ProvisioningCertificate. */
  else if ( Scc_DP_ProvisioningCertificate == DataID )
  {
    /* get the current status of this NvM block */
    (void) NvM_GetErrorStatus((NvM_BlockIdType)Scc_GetNvMBlockIDProvCert(Scc_CertsWs.ChosenContrCertChainIdx), NvmResultPtr);
  }

  /* #60 Get NvM block status of ProvisioningCertificatePrivateKey. */
  else if ( Scc_DP_ProvisioningCertificatePrivateKey == DataID )
  {
    /* The private Key is stored via the CSM, check status of private key via CSM */
    Scc_ReportError(Scc_StackError_InvalidRxParameter);
    RetVal = (Std_ReturnType)E_NOT_OK;
  }
#endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

  /* #70 Get NvM block status of RootCertificate. */
  else if ( (uint8)Scc_DP_RootCertificate1 == ( (uint8)DataID & (uint8)0xF0u) )
  {
    /* get the root cert nr */
    uint8 RootCertNr = (uint8)DataID & (uint8)0x0Fu;

    /* check if the requested root certificate exists */
    if ( Scc_CertsNvm.RootCertNvmCnt > RootCertNr )
    {
      /* get the current status of this NvM block */
      (void) NvM_GetErrorStatus((NvM_BlockIdType)Scc_CertsNvm.RootCerts[RootCertNr].NvmBlockId, NvmResultPtr);
    }
    /* requested root certificate does not exist */
    else
    {
      RetVal = (Std_ReturnType)E_NOT_OK;
    }
  }

  /* invalid DataID */
  else
  {
    RetVal = (Std_ReturnType)E_NOT_OK;
  }

  return RetVal;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_CheckContrCertValidity
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(Scc_ReturnType, SCC_CODE) Scc_CheckContrCertValidity(uint32 DateTimeNow, uint32 DateTimeThreshold,
  P2VAR(Scc_CertificateStatusType, AUTOMATIC, SCC_VAR_NOINIT) ContrCertStatus)
{
  Scc_ReturnType RetVal = Scc_ReturnType_OK;

  /* check if a contract certificate is available */
  if ( Scc_NvMBlockReadState_Processed == Scc_CertsWs.ContrCertReadState )
  {
    /* check if the contract certificate is not expired */
    if ( (Std_ReturnType)E_OK == Scc_CheckCertTimeExpiration(&Scc_CertsWs.ContrCert->Buffer[0],
      Scc_CertsWs.ContrCert->Length, DateTimeNow) )
    {
      /* check if the contract certificate expires soon */
      if ( (Std_ReturnType)E_OK == Scc_CheckCertTimeExpiration(&Scc_CertsWs.ContrCert->Buffer[0],
        Scc_CertsWs.ContrCert->Length, DateTimeThreshold) )
      {
        /* #10 contract certificate is not expired and available */
        *ContrCertStatus = Scc_CS_CertAvailable;
      }
      else
      {
        /* #20 contract certificate is expires soon */
        *ContrCertStatus = Scc_CS_CertExpiresSoon;
      }
    }

    else
    {
      /* #30 contract certificate is expired */
      *ContrCertStatus = Scc_CS_CertExpired;
    }
  }
  /* if the contract certificate is not available */
  else if (   ( Scc_NvMBlockReadState_Invalidated == Scc_CertsWs.ContrCertReadState )
           || ( Scc_NvMBlockReadState_Error       == Scc_CertsWs.ContrCertReadState ))
  {
    /* #40 no contract certificate in the NvM */
    *ContrCertStatus = Scc_CS_NoCertAvailable;
  }
  else
  {
    /* #50 contract certificate hasn't been read from NvM yet */
    /* read the contract certificate */
    Scc_NvmReadContrCertChain(FALSE);
    /* operation needs more time */
    RetVal = Scc_ReturnType_Pending;
  }

  return RetVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
*  Scc_CheckProvCertValidity
*********************************************************************************************************************/
/*!
*
* Internal comment removed.
 *
 *
 *
 *
 *
 *
*/
FUNC(Scc_ReturnType, SCC_CODE) Scc_CheckProvCertValidity(uint32 DateTimeNow, uint32 DateTimeThreshold,
  P2VAR(Scc_CertificateStatusType, AUTOMATIC, SCC_VAR_NOINIT) ProvCertStatus)
{
  Scc_ReturnType RetVal = Scc_ReturnType_OK;

  /* check if a contract certificate is available */
  if ( Scc_NvMBlockReadState_Processed == Scc_CertsWs.ProvCertReadState )
  {
    /* check if the provisioning certificate is not expired */
    if ( (Std_ReturnType)E_OK == Scc_CheckCertTimeExpiration(&Scc_CertsWs.ProvCert->Buffer[0],
      Scc_CertsWs.ProvCert->Length, DateTimeNow) )
    {
      /* check if the provisioning certificate expires soon */
      if ( (Std_ReturnType)E_OK == Scc_CheckCertTimeExpiration(&Scc_CertsWs.ProvCert->Buffer[0],
        Scc_CertsWs.ProvCert->Length, DateTimeThreshold) )
      {
        /* #10 provisioning certificate is not expired and available */
        *ProvCertStatus = Scc_CS_CertAvailable;
      }
      else
      {
        /* #20 provisioning certificate is expires soon */
        *ProvCertStatus = Scc_CS_CertExpiresSoon;
      }
    }

    else
    {
      /* #30 provisioning certificate is expired */
      *ProvCertStatus = Scc_CS_CertExpired;
    }
  }
  /* if the provisioning certificate is not available */
  else if (   ( Scc_NvMBlockReadState_Invalidated == Scc_CertsWs.ProvCertReadState )
           || ( Scc_NvMBlockReadState_Error       == Scc_CertsWs.ProvCertReadState ))
  {
    /* #40 no provisioning certificate in the NvM */
    *ProvCertStatus = Scc_CS_NoCertAvailable;
  }
  else
  {
    /* #50 provisioning certificate hasn't been read from NvM yet */
    /* read the provisioning certificate */
    Scc_NvmReadProvCert();
    /* operation needs more time */
    RetVal = Scc_ReturnType_Pending;
  }

  return RetVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ResetNvMBlockStatus
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(Scc_ReturnType, SCC_CODE) Scc_ResetNvMBlockStatus(Scc_DiagParamsType NvMBlock)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType  RetVal = Scc_ReturnType_NotOK;

  /* ----- Implementation ----------------------------------------------- */
  /* check if currently no V2G session is active */
  if (   ( Scc_State_TLConnecting > Scc_State )
      || ( Scc_State_Connected < Scc_State ))
  {
    /* get the NvM BlockID */
    switch ( NvMBlock )
    {
    case Scc_DP_ContractCertificateChainSize:
      /* check if the block is not busy */
      if ( Scc_NvMBlockReadState_ReadRequested != Scc_CertsWs.ContrCertChainSizeReadState )
      {
        /* #10 Reset ContractCertificate ChainSize ReadStatus. */
        Scc_CertsWs.ContrCertChainSizeReadState = Scc_NvMBlockReadState_NotSet;
        RetVal = Scc_ReturnType_OK;
      }
      else
      {
        RetVal = Scc_ReturnType_Pending;
      }
      break;

    case Scc_DP_ContractCertificate:
    case Scc_DP_EMAID:
      /* check if the block is not busy */
      if ( Scc_NvMBlockReadState_ReadRequested != Scc_CertsWs.ContrCertReadState )
      {
        /* #20 Reset ContractCertificate ReadStatus. */
        Scc_CertsWs.ContrCertReadState = Scc_NvMBlockReadState_NotSet;
        RetVal = Scc_ReturnType_OK;
      }
      else
      {
        RetVal = Scc_ReturnType_Pending;
      }
      break;

    case Scc_DP_ProvisioningCertificate:
    case Scc_DP_PCID:
      /* check if the block is not busy */
      if ( Scc_NvMBlockReadState_ReadRequested != Scc_CertsWs.ProvCertReadState )
      {
        /* #30 Reset ProvisioningCertificate ReadStatus. */
        Scc_CertsWs.ProvCertReadState = Scc_NvMBlockReadState_NotSet;
        RetVal = Scc_ReturnType_OK;
      }
      else
      {
        RetVal = Scc_ReturnType_Pending;
      }
      break;

    default:
      /* check for ContractSubCertificate */
      if ( (uint8)Scc_DP_ContractSubCertificate1 == ( (uint8)NvMBlock & (uint8)Scc_DP_ContractSubCertificate1 )) /*lint !e655 */
      {
        uint32 SubCertIdx = (uint32)NvMBlock & 0x0000000Fu;
        /* check if the index is valid */
        if ( Scc_CertsWs.ContrSubCertCnt > SubCertIdx )
        {
          /* check if the block is not busy */
          if ( Scc_NvMBlockReadState_ReadRequested != Scc_CertsWs.ContrSubCertsReadStates[SubCertIdx] )
          {
            /* #40 Reset ContractSubCertificate ReadStatus. */
            Scc_CertsWs.ContrSubCertsReadStates[SubCertIdx] = Scc_NvMBlockReadState_NotSet;
            /* reset the processed counter */
            Scc_CertsWs.ContrSubCertsProcessedFlags &= ~( 1u << SubCertIdx );
            RetVal = Scc_ReturnType_OK;
          }
          /* block is busy */
          else
          {
            RetVal = Scc_ReturnType_Pending;
          }
        }
      }
      /* check for RootCertificate */
      if ( (uint8)Scc_DP_RootCertificate1 == ((uint8)NvMBlock & (uint8)Scc_DP_RootCertificate1 )) /*lint !e655 */
      {
        uint32 RootCertIdx = (uint32)NvMBlock & 0x0000000Fu;
        /* check if the index is valid */
        if ( Scc_CertsNvm.RootCertNvmCnt > RootCertIdx )
        {
          /* check if the block is not busy */
          if ( Scc_NvMBlockReadState_ReadRequested != Scc_CertsWs.RootCertsReadStates[RootCertIdx] )
          {
            /* #50 Reset RootCertificate ReadStatus. */
            Scc_CertsWs.RootCertsReadStates[RootCertIdx] = Scc_NvMBlockReadState_NotSet;
            /* reset the flag of this root certificate */
            Scc_CertsWs.RootCertsProcessedFlags &= ~( 1u << RootCertIdx );
            RetVal = Scc_ReturnType_OK;
          }
          /* block is busy */
          else
          {
            RetVal = Scc_ReturnType_Pending;
          }
        }
      }
      break;
    }
  }
  else
  {
    RetVal = Scc_ReturnType_Busy;
  }

  return RetVal;
} /* PRQA S 6030,6080 */ /* MD_MSR_STCYC,MD_MSR_STMIF */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_LoadAllCertificates
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_LoadAllCertificates(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 load all installed certificates */
  Scc_NvmReadContrCertChain(TRUE);
  Scc_NvmReadProvCert();
  Scc_NvmReadRootCerts(TRUE, 0u);

  return;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_GetContractCertChainIndexInUse
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_GetContractCertChainIndexInUse(
  P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) IndexOfUsedContractCertChain)
{
  /* #10 Copy index of currently used contract certificate chain */
  *IndexOfUsedContractCertChain = Scc_CertsWs.ChosenContrCertChainIdx;

  return;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_DeleteContract
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(Scc_ReturnType, SCC_CODE) Scc_DeleteContract(uint8 ContractIdx, boolean ForceDelete)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType RetVal = Scc_ReturnType_OK;
  uint8_least Counter;

  /* ----- Implementation ----------------------------------------------- */
  /* check if currently no V2G session is active */
  if (   ( TRUE != ForceDelete )
      && ( ContractIdx == Scc_CertsWs.ChosenContrCertChainIdx )
      && ( Scc_State_TLConnecting <= Scc_State )
      && ( Scc_State_Connected >= Scc_State ))
  {
    RetVal = Scc_ReturnType_Busy;
  }

  if ( Scc_ReturnType_OK == RetVal )
  {
    /* #10 invalidate the ContractCertificateChainSize. */
    /* PRQA S 4342 1 */ /* MD_Scc_ReturnType */
    RetVal = (Scc_ReturnType)NvM_InvalidateNvBlock((NvM_BlockIdType)Scc_GetNvMBlockIDContrCertChainSize(ContractIdx));
    /* check if this chain is currently active */
    if ( ContractIdx == Scc_CertsWs.ChosenContrCertChainIdx )
    {
      Scc_CertsWs.ContrCertChainSize = -1;
      Scc_CertsWs.ContrCertChainSizeReadState = Scc_NvMBlockReadState_Invalidated;
    }
  }

  if ( Scc_ReturnType_OK == RetVal )
  {
    /* #20 invalidate the ContractCertificate. */
    /* PRQA S 4342 1 */ /* MD_Scc_ReturnType */
    RetVal = (Scc_ReturnType)NvM_InvalidateNvBlock((NvM_BlockIdType)Scc_GetNvMBlockIDContrCert(ContractIdx));
    /* check if this chain is currently active */
    if ( ContractIdx == Scc_CertsWs.ChosenContrCertChainIdx )
    {
      Scc_CertsWs.ContrCert->Length = 0;
      Scc_CertsWs.ContrCertReadState = Scc_NvMBlockReadState_Invalidated;
      Scc_CertsWs.eMAID->Length = 0;
    }
  }

  if ( Scc_ReturnType_OK == RetVal )
  {
    /* #30 invalidate the ContractSubCertificates. */
    for ( Counter = 0; Counter < Scc_CertsWs.ContrSubCertCnt; Counter++ )
    {
      RetVal = (Scc_ReturnType)NvM_InvalidateNvBlock((NvM_BlockIdType)Scc_GetNvMBlockIDContrSubCerts(ContractIdx, Counter)); /* PRQA S 4342 */ /* MD_Scc_ReturnType */
      /* check if this chain is currently active */
      if ( ContractIdx == Scc_CertsWs.ChosenContrCertChainIdx )
      {
        Scc_CertsWs.ContrSubCerts[Counter].Length = 0;
        Scc_CertsWs.ContrSubCertsReadStates[Counter] = Scc_NvMBlockReadState_Invalidated;
      }
      /* check if an error occurred */
      if ( Scc_ReturnType_OK != RetVal )
      {
        break;
      }
    }
  }

  return RetVal;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_DeleteRootCert
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(Scc_ReturnType, SCC_CODE) Scc_DeleteRootCert(uint8 RootCertIdx)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal = Scc_ReturnType_NotOK;

  /* ----- Implementation ----------------------------------------------- */
  /* check if currently no V2G session is active */
  if (   ( Scc_State_TLConnecting > Scc_State )
      || ( Scc_State_Connected < Scc_State ))
  {
    /* check if the RootCertIdx is a valid one */
    if ( Scc_CertsNvm.RootCertNvmCnt > RootCertIdx )
    {
      /* forget that the root certificate is in the RAM */
      Scc_CertsWs.RootCertsReadStates[RootCertIdx] = Scc_NvMBlockReadState_Invalidated;
      /* processing of this root cert is finished */
      Scc_CertsWs.RootCertsProcessedFlags |= ( (uint32)0x01 << (uint32)RootCertIdx );

      /* #10 invalidate the NvM block of the RootCertificate. */
      /* PRQA S 4342 1 */ /* MD_Scc_ReturnType */
      retVal = (Scc_ReturnType)NvM_InvalidateNvBlock((NvM_BlockIdType)Scc_CertsNvm.RootCerts[RootCertIdx].NvmBlockId);
    }
  }
  else
  {
    retVal = Scc_ReturnType_Busy;
  }

  return retVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_DeleteProvCert
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Scc_ReturnType, SCC_CODE) Scc_DeleteProvCert(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal = Scc_ReturnType_NotOK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 check if currently no V2G session is active */
  if ( (Scc_State_TLConnecting > Scc_State)
    || (Scc_State_Connected < Scc_State) )
  {
    const uint8 provCertPrivateKeyBuff[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    uint16 dataLen = 32;
    uint8  NvmReadState = NVM_REQ_PENDING;

    /* #20 forget that the provisioning certificate is in the RAM */
    Scc_CertsWs.ProvCertReadState = Scc_NvMBlockReadState_Invalidated;
    /* #30 Overwrite the private key */
    retVal = Scc_DiagDataWriteAccess(Scc_DP_ProvisioningCertificatePrivateKey, provCertPrivateKeyBuff, dataLen);

    if ( retVal == Scc_ReturnType_OK )
    {
      /* #40 check if the NvM is not busy */
      (void)NvM_GetErrorStatus((NvM_BlockIdType)Scc_GetNvMBlockIDProvCert(Scc_CertsWs.ChosenContrCertChainIdx), &NvmReadState);
      if ( NVM_REQ_PENDING == NvmReadState )
      {
        retVal = Scc_ReturnType_Pending;
      }
      else
      {
        uint16 index;

        /* set the next pointer of the certificate */
        Scc_CertsWs.ProvCert->NextCertificatePtr = (P2VAR(Exi_ISO_certificateType, AUTOMATIC, SCC_VAR_NOINIT))NULL_PTR;
        /* set the length of the certificate */
        Scc_CertsWs.ProvCert->Length = 0;

        /* #50 Overwrite the provisioning certificate */
        for ( index = 0u; index < 800u; index++ )
        {
          Scc_CertsWs.ProvCert->Buffer[index] = 0u;
        }

        /* copy the ProvCertID */ /* PRQA S 0310,3305 3 */ /* MD_Scc_0310_3305 */
        /*lint -e(645) If ProvCertID would not be initialized, RetVal would not be equal to E_OK */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ProvCertID[0], /* PRQA S 0315 */ /* MD_MSR_VStdLibCopy */
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&provCertPrivateKeyBuff[0], SCC_PROV_CERT_ID_MAX_LEN);
        /*lint +e645 */
        Scc_CertsWs.ProvCertIDLen = 0;
        /* set the read state */
        Scc_CertsWs.ProvCertReadState = Scc_NvMBlockReadState_Invalidated;

        /* #60 invalidate the NvM block of the RootCertificate. */
        /* PRQA S 4342 1 */ /* MD_Scc_ReturnType */
        retVal = (Scc_ReturnType)NvM_InvalidateNvBlock((NvM_BlockIdType)Scc_GetNvMBlockIDProvCert(Scc_CertsWs.ChosenContrCertChainIdx));
      }
    }
  }
  /* #70 Otherwise set the return value to busy */
  else
  {
    retVal = Scc_ReturnType_Busy;
  }

  return retVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ValidateContrCertKeyPair
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Scc_ReturnType, SCC_CODE) Scc_ValidateContrCertKeyPair(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal = Scc_ReturnType_NotOK;

  /* ----- Implementation ----------------------------------------------- */

  /* check if the certificate and/or the private key have not been read from NVRAM yet */
  if ( Scc_NvMBlockReadState_Processed != Scc_CertsWs.ContrCertReadState )

  {
    /* check if an error occurred or there is no contract cert and/or private key installed */
    if (   ( Scc_NvMBlockReadState_Error != Scc_CertsWs.ContrCertReadState )
        && ( Scc_NvMBlockReadState_Invalidated != Scc_CertsWs.ContrCertReadState ))
    {
      /* #10 read the ContractCertificate and PrivateKey from the NvM. */
      Scc_NvmReadContrCertChain(FALSE);
      /* return pending */
      retVal = Scc_ReturnType_Pending;
    }
  }

  else
  {
    /* certificate and private key were successfully read from NVRAM */
    /* #20 validate PrivateKey and PublicKey of ContractCertificate */
    /* PRQA S 4342 1 */ /* MD_Scc_ReturnType */
    retVal = (Scc_ReturnType)Scc_ValidateKeyPair(&Scc_CertsWs.ContrCert->Buffer[0], Scc_CertsWs.ContrCert->Length,
                                                 Scc_ECDSASignCertJobIds[Scc_CertsWs.ChosenContrCertChainIdx]);
  }

  return retVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ValidateProvCertKeyPair
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Scc_ReturnType, SCC_CODE) Scc_ValidateProvCertKeyPair(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal = Scc_ReturnType_NotOK;

  /* ----- Implementation ----------------------------------------------- */

  /* check if the certificate and/or the private key have not been read from NVRAM yet */
  if ( Scc_NvMBlockReadState_Processed != Scc_CertsWs.ProvCertReadState )
  {
    /* check if an error occurred or there is no provisioning cert and/or private key installed */
    if (   ( Scc_NvMBlockReadState_Error != Scc_CertsWs.ProvCertReadState )
        && ( Scc_NvMBlockReadState_Invalidated != Scc_CertsWs.ProvCertReadState ))
    {
      /* #10 read the ProvisioningCertificate and PrivateKey from the NvM. */
      Scc_NvmReadProvCert();
      /* return pending */
      retVal = Scc_ReturnType_Pending;
    }
  }
  else
  {
    /* certificate and private key were successfully read from NVRAM */
    /* #20 validate PrivateKey and PublicKey of ProvisioningCertificate */
    /* PRQA S 4342 1 */ /* MD_Scc_ReturnType */
    retVal = (Scc_ReturnType)Scc_ValidateKeyPair(&Scc_CertsWs.ProvCert->Buffer[0], Scc_CertsWs.ProvCert->Length,
                                                 Scc_ECDSASignProvCertJobId);
  }

  return retVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_Reset
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_Reset(void)
{
  /* check if an UDP socket is opened */
  if ( (Scc_SocketType)0xFFu != Scc_SDPSocket )
  {
    /* #10 close the UDP socket */
    (void) TcpIp_Close(Scc_SDPSocket, FALSE);
  }

  /* check if a TL connection is established */
  if ( TRUE == Scc_V2GSocketInUse )
  {
    /* Prepare shutdown */
    Scc_State = Scc_State_ShuttingDown;
    /* #20 close the TL connection */
    (void) TcpIp_Close(Scc_V2GSocket, TRUE);
    /* report it to the application */
    Scc_Set_Core_TCPSocketState(Scc_TCPSocketState_Disconnecting);
  }
  else
  {
    /* report to the application that the socket is closed*/
    Scc_Set_Core_TCPSocketState(Scc_TCPSocketState_Closed);
  }

  /* #30 reset all state and message relevant parameters */
  Scc_State                     = Scc_State_Initialized;
  Scc_MsgState                  = Scc_MsgState_WaitForNextRequest;
  Scc_MsgTrig                   = Scc_MsgTrig_None;
  Scc_CyclicMsgTrig             = FALSE;
  Scc_MsgTrigNew                = Scc_MsgTrig_None;
  Scc_MsgStatus                 = Scc_MsgStatus_None;
  Scc_StackError                = Scc_StackError_NoError;
  Scc_TimeoutCnt                = 0;
  Scc_SdpRetransRetriesCnt      = (uint16)Scc_ConfigValue_Timer_General_SECCDiscoveryProtocolRetries;
  Scc_ExiStreamRxPBuf[0].totLen = 0;
  Scc_ExiStreamRxPBuf[0].len    = 0;
  Scc_ExiStreamRxPBufIdx        = 0;
  Scc_TxDataSent                = 0;
  Scc_TxStreamingActive         = FALSE;
  Scc_SDPSocket                 = (Scc_SocketType)0xFFu;
  Scc_Security                  = Scc_SDPSecurity_None;

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
  Scc_QCAIdleTimer = 0;
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  Scc_SaPubKeyRcvd = FALSE;
#endif /* SCC_ENABLE_PNC_CHARGING */
}

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
/**********************************************************************************************************************
 *  Scc_TriggerSLAC
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */

SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_TriggerSLAC(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  EthTrcv_30_Ar7000_Slac_StartModeType SLACStartMode;
  boolean stop = FALSE;

  /* ----- Implementation ----------------------------------------------- */
  /* check if this is the initial trigger */
  if ( TRUE == Scc_CyclicMsgTrig )
  {
    /* accept the new trigger */
    Scc_MsgTrig = Scc_MsgTrigNew;
    /* reset the StackError */
    Scc_StackError = Scc_StackError_NoError;
    /* reset the cyclic message trigger at the application */
    Scc_Set_Core_CyclicMsgTrigTx(FALSE);
  }

  if ( Scc_MsgState_WaitForNextRequest == Scc_MsgState )
  {
    /* #10 check if the link is already established. */
    if ( TRUE == Scc_DLinkReady )
    {
      /* check if it was not already reported to the application that SLAC is done */
      if ( Scc_MsgStatus_SLAC_OK != Scc_MsgStatus )
      {
        /* report the success to the application */
        Scc_ReportSuccessAndStatus(Scc_MsgStatus_SLAC_OK);
      }
      stop = TRUE;
    }

    if (stop == FALSE)
    {
      /* #20 check if the firmware download is not finished or the idle timer is not elapsed yet. */
      if ((Scc_FirmwareDownloadStatus_Unknown == Scc_QCAFirmwareDownloadComplete)
          || ((0u != Scc_QCAIdleTimer)
          && (Scc_FirmwareDownloadStatus_ErrorOccurred != Scc_QCAFirmwareDownloadComplete)))
      {

      }
      /* #30 check if an error occurred during the download of the firmware to the QCA. */
      else if (Scc_FirmwareDownloadStatus_ErrorOccurred == Scc_QCAFirmwareDownloadComplete)
      {
        /* check if it was not already reported to the application that SLAC has failed */
        if (Scc_MsgStatus_SLAC_Failed != Scc_MsgStatus)
        {
          /* report the failure to the application */
          Scc_ReportError(Scc_StackError_SLAC);
        }

      }
      /* #40 firmware download was successful. */
      else /* if ( Scc_FirmwareDownloadStatus_Complete == Scc_QCAFirmwareDownloadComplete ) */
      {
        /* get the start type */
        Scc_Get_SLAC_StartMode(&SLACStartMode);
        /* start SLAC handling */
        if ((uint8)E_OK == EthTrcv_30_Ar7000_Slac_Start((uint8)SCC_TRANSCEIVER_INDEX, SLACStartMode))
        {
          /* go to the next state */
          Scc_MsgStateFct(Scc_MsgState_RequestSent)
        }
      }
    }
  }
} /* PRQA S 6080 */ /* MD_MSR_STMIF */
#endif /* SCC_ENABLE_SLAC_HANDLING */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_TriggerNVRAM
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_TriggerNVRAM(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType RetVal = Scc_ReturnType_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* check if this is the initial trigger */
  if ( TRUE == Scc_CyclicMsgTrig )
  {
    /* accept the new trigger */
    Scc_MsgTrig = Scc_MsgTrigNew;
    /* reset the StackError */
    Scc_StackError = Scc_StackError_NoError;
    /* reset the cyclic message trigger at the application */
    Scc_Set_Core_CyclicMsgTrigTx(FALSE);
  }

  switch ( Scc_MsgTrig )
  {
    /* #10 wait till ContractCertificateChainSize is read from NvM. */
  case Scc_MsgTrig_ReadContrCertChain:
    /* read all certificates */
    Scc_NvmReadContrCertChain(TRUE);


    /* #20 check the status of the ContractCertificateChainSize. */
    switch ( Scc_CertsWs.ContrCertChainSizeReadState )
    {
      /* check if the NvM needs more time */
    case Scc_NvMBlockReadState_NotSet:
    case Scc_NvMBlockReadState_ReadRequested:
    case Scc_NvMBlockReadState_ReadFinished:
      RetVal = Scc_ReturnType_Pending;
      break;

      /* check if an error occurred */
    case Scc_NvMBlockReadState_Error:
      RetVal = Scc_ReturnType_NotOK;
      break;

    default:
      /* ignore other states, RetVal is already set to E_OK */
      break;
    }

    if (( Scc_ReturnType_OK == RetVal ) && ( 0 <= Scc_CertsWs.ContrCertChainSize ))
    {
      /* #30 check the status of the ContractCertificate. */
      switch ( Scc_CertsWs.ContrCertReadState )
      {
        /* check if the NvM needs more time */
      case Scc_NvMBlockReadState_NotSet:
      case Scc_NvMBlockReadState_ReadRequested:
      case Scc_NvMBlockReadState_ReadFinished:
        RetVal = Scc_ReturnType_Pending;
        break;

        /* check if an error occurred */
      case Scc_NvMBlockReadState_Error:
        RetVal = Scc_ReturnType_NotOK;
        break;

      default:
        /* ignore other states, RetVal is already set to E_OK */
        break;
      }
    }

    if (( Scc_ReturnType_OK == RetVal ) && ( 1 <= Scc_CertsWs.ContrCertChainSize ))
    {
      /* #40 check if not all sub certs were processed yet. */
      if (((uint32)( (uint32)0x01 << (uint8)Scc_CertsWs.ContrCertChainSize ) - 1u ) != Scc_CertsWs.ContrSubCertsProcessedFlags )
      {
        boolean     ErrorOccurred = FALSE;
        sint8_least Counter;
        /* step through all sub certs */
        for ( Counter = 0; Counter < Scc_CertsWs.ContrCertChainSize; Counter++ )
        {
          /* check if one of them could not be read successfully */
          if ( Scc_NvMBlockReadState_Error == Scc_CertsWs.ContrSubCertsReadStates[Counter] )
          {
            /* set the error flag */
            ErrorOccurred = TRUE;
            break;
          }
        }
        /* check if an error occurred */
        if ( TRUE == ErrorOccurred )
        {
          RetVal = Scc_ReturnType_NotOK;
        }
        /* NvM needs more time to read the sub certs */
        else
        {
          RetVal = Scc_ReturnType_Pending;
        }
      }
    }

    /* check if everything is ready */
    if ( Scc_ReturnType_OK == RetVal )
    {
      Scc_ReportSuccessAndStatus(Scc_MsgStatus_ReadContrCertChain_OK);
    }
    /* check if an error occurred */
    else if ( Scc_ReturnType_NotOK == RetVal ) /* PRQA S 2004 */ /* MD_Scc_2004 */
    {
      Scc_ReportError(Scc_StackError_NvM);
    }
    break;

    /* #50 wait till RootCertificates is read from NvM. */
  case Scc_MsgTrig_ReadRootCerts:
    /* read all root certificates */
    Scc_NvmReadRootCerts(TRUE, 0);

    /* check if all root certs were processed. */
    if ( (( (uint32)0x01 << (uint32)Scc_CertsNvm.RootCertNvmCnt ) - 1u ) == Scc_CertsWs.RootCertsProcessedFlags )
    {
      uint8_least Counter;
      RetVal = Scc_ReturnType_NotOK;
      /* step through all root certs */
      for ( Counter = 0; Counter < Scc_CertsNvm.RootCertNvmCnt; Counter++ )
      {
        /* #60 check if at least one block contains a valid RootCertificate */
        if ( Scc_NvMBlockReadState_Processed == Scc_CertsWs.RootCertsReadStates[Counter] )
        {
          RetVal = Scc_ReturnType_OK;
          break;
        }
      }
      /* check if at least one root certificate was read from NVRAM */
      if ( Scc_ReturnType_OK == RetVal )
      {
        Scc_ReportSuccessAndStatus(Scc_MsgStatus_ReadRootCerts_OK);
      }
      /* all blocks are empty or an error occurred while reading them */
      else
      {
#if ( defined SCC_DEM_NVM_READ_ROOT_CERT_FAIL )
        /* set DEM event, if enabled */
        Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_ROOT_CERT_FAIL);
#endif /* SCC_DEM_NVM_READ_ROOT_CERT_FAIL */

        /* report the error to the application */
        Scc_ReportError(Scc_StackError_NvM);
      }
    }
    break;

    /* #70 wait till ProvisioningCertificate is read from NvM. */
  case Scc_MsgTrig_ReadProvCert:
    /* read the prov certificate */
    Scc_NvmReadProvCert();



    /*  #80 check the status of the ProvisioningCertificate */
    switch ( Scc_CertsWs.ProvCertReadState )
    {
      /* check if the NvM needs more time */
    case Scc_NvMBlockReadState_NotSet:
    case Scc_NvMBlockReadState_ReadRequested:
    case Scc_NvMBlockReadState_ReadFinished:
      RetVal = Scc_ReturnType_Pending;
      break;

      /* check if an error occurred */
    case Scc_NvMBlockReadState_Error:
      RetVal = Scc_ReturnType_NotOK;
      break;

      /* prov cert is empty */
    case Scc_NvMBlockReadState_Invalidated:
#if ( defined SCC_DEM_NVM_READ_PROV_CERT_FAIL )
      /* set DEM event, if enabled */
      Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_PROV_CERT_FAIL);
#endif /* SCC_DEM_NVM_READ_PROV_CERT_FAIL */
      RetVal = Scc_ReturnType_NotOK;
      break;

    default:
      /* ignore other states, RetVal is already set to E_OK */
      break;
    }

    /* check if it was finished successfully */
    if ( Scc_ReturnType_OK == RetVal )
    {
      Scc_ReportSuccessAndStatus(Scc_MsgStatus_ReadProvCert_OK);
    }
    /* check if an error occurred */
    else if ( Scc_ReturnType_NotOK == RetVal ) /* PRQA S 2004 */ /* MD_Scc_2004 */
    {
      Scc_ReportError(Scc_StackError_NvM);
    }
    break;

  default:
#if(SCC_DEV_ERROR_REPORT == STD_ON)
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_TRIG_NVM, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
    break;
  }
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  Scc_TriggerV2G
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_TriggerV2G(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* check if it is allowed to send a request */
  if (   ( Scc_MsgTrig != Scc_MsgTrigNew )
      || ( TRUE == Scc_CyclicMsgTrig ))
  {
    /* #10 check if there is still an open connection */
    if (   ( Scc_MsgTrig_TransportLayer == Scc_MsgTrigNew )
        && ( TRUE == Scc_V2GSocketInUse ))
    {
      /* Prepare shutdown */
      Scc_State = Scc_State_ShuttingDown;
      /* close the connection before opening a new one */
      (void) TcpIp_Close(Scc_V2GSocket, TRUE);
      /* report it to the application */
      Scc_Set_Core_TCPSocketState(Scc_TCPSocketState_Disconnecting);
    }
    else
    {
      /* accept the new trigger */
      Scc_MsgTrig = Scc_MsgTrigNew;
      /* reset the StackError */
      Scc_StackError = Scc_StackError_NoError;
      /* reset the cyclic message trigger at the application */
      Scc_Set_Core_CyclicMsgTrigTx(FALSE);

      /* #20 check if a secc discovery protocol request shall be sent */
      if ( Scc_MsgTrig_SECCDiscoveryProtocol == Scc_MsgTrig )
      {
        uint8 ChangeParameterParam = 1;
        uint16 Port = TCPIP_PORT_ANY;

        /* check if the UDP socket is not already bound to SCC */
        if ( (Scc_SocketType)0xFFu == Scc_SDPSocket )
        {
          /* get the socket */
          if ( (Std_ReturnType)E_OK !=
            TcpIp_SccGetSocket(TCPIP_AF_INET6, TCPIP_IPPROTO_UDP, &Scc_SDPSocket) )
          {
            Scc_ReportError(Scc_StackError_TransportLayer);
          }
          /* set the tx confirmation list */
          else if ( (Std_ReturnType)E_OK !=
            TcpIp_ChangeParameter(Scc_SDPSocket, TCPIP_PARAMID_V_UDP_TXREQLISTSIZE, &ChangeParameterParam) )
          {
            Scc_ReportError(Scc_StackError_TransportLayer);
          }
          /* bind the socket */
          else if ( (Std_ReturnType)E_OK != TcpIp_Bind(Scc_SDPSocket, (TcpIp_LocalAddrIdType)SCC_IPV6_ADDRESS, &Port) )
          {
            Scc_ReportError(Scc_StackError_TransportLayer);
          }
          /* try to send a SDP request and check if it was successful */
          else if ( (Std_ReturnType)E_OK == Scc_TxSECCDiscoveryProtocolReq() ) /* PRQA S 2004 */ /* MD_Scc_2004 */
          {
              Scc_MsgStateFct(Scc_MsgState_RequestSent)
            /* start the timer */
            Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_General_SECCDiscoveryProtocolTimeout;
          }
        }
      }

      /* #30 check if the transport layer connection shall be established */
      else if (   ( Scc_State_SDPComplete == Scc_State )
               && ( Scc_MsgTrig_TransportLayer == Scc_MsgTrig ))
      {
        Scc_EstablishTLConnection();
      }

      /* #40 check if a SupportedAppProtocolReq shall be sent */
      else if (   ( Scc_State_TLConnected == Scc_State )
               && ( Scc_MsgTrig_SupportedAppProtocol == Scc_MsgTrig ))
      {
        /* send the SAP request */
        Scc_ReturnType RetVal = Scc_TransmitExi();

        /* check if it was successful */
        if ( Scc_ReturnType_OK == RetVal )
        {
            Scc_MsgStateFct(Scc_MsgState_RequestSent)
        }
        /* check if this packet is streamed */
        else
        { /* Scc_ReturnType_Pending == RetVal */
          Scc_MsgStateFct(Scc_MsgState_StreamingRequest)
        }
      }

      /* #50 check if a charging session is started */
      else if (   (   ( Scc_State_SAPComplete == Scc_State )
                   && ( Scc_MsgTrig_SessionSetup == Scc_MsgTrig ))
               || (   ( Scc_State_Connected == Scc_State )
                   && (   ( Scc_MsgTrig_ServiceDiscovery <= Scc_MsgTrig )
                       && ( Scc_MsgTrig_SessionStop >= Scc_MsgTrig )))
               || ( Scc_MsgTrig_SessionStop == Scc_MsgTrig ) )
      {
        /* #60 send a V2G request */
        Scc_ReturnType RetVal = Scc_TransmitExi();

        /* check if it was successful */
        if ( Scc_ReturnType_OK == RetVal )
        {
            Scc_MsgStateFct(Scc_MsgState_RequestSent)
        }
        /* check if this packet is streamed */
        else
        { /* Scc_ReturnType_Pending == RetVal */
          Scc_MsgStateFct(Scc_MsgState_StreamingRequest)
        }
      }

      /* #70 check if the tcp connection should be closed */
      else if (   ( Scc_State_Disconnected == Scc_State )
               && ( Scc_MsgTrig_StopCommunicationSession == Scc_MsgTrig ))
      {
        if ( (Std_ReturnType)E_OK == TcpIp_Close(Scc_V2GSocket, FALSE) )
        {
          Scc_State = Scc_State_ShuttingDown;
          Scc_MsgStateFct(Scc_MsgState_RequestSent)
          /* inform the application */
          Scc_Set_Core_TCPSocketState(Scc_TCPSocketState_Disconnecting);
          /* start the timeout (if value is 0, timeout will not be started) */
          Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_General_TransportLayerTimeout;
        }
        else
        {
          /* write the error */
          Scc_ReportError(Scc_StackError_TransportLayer);
        }
      }

      /* if the trigger was not valid */
      else
      {
        /* report a V2G error of the current trigger */
        Scc_ReportError(Scc_StackError_InvalidTxParameter);
      }
    }
  }
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_TimeoutHandling
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_TimeoutHandling(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 check if the timer is active */
  if ( 0u < Scc_TimeoutCnt )
  {
    /* decrement the timer */
    Scc_TimeoutCnt--;
    /* #20 check if a timeout occurred */
    if ( 0u == Scc_TimeoutCnt )
    {
      /* #30 check if the timeout happened during the SDP */
      if ( Scc_MsgTrig_SECCDiscoveryProtocol == Scc_MsgTrig )
      {
        /* check if at least one more retry is left */
        if ( 0u < Scc_SdpRetransRetriesCnt )
        {
          /* decrement the SDP retries */
          Scc_SdpRetransRetriesCnt--;
          /* try to send another SDP request and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_TxSECCDiscoveryProtocolReq() )
          {
            Scc_MsgStateFct(Scc_MsgState_RequestSent)
            /* set the timer */
            Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_General_SECCDiscoveryProtocolTimeout;
          }
          else
          {
            /* if the packet was not transmitted (completely) */
            /* report the timeout to the application */
            Scc_ReportError(Scc_StackError_Timeout);
          }
        }
        else
        {
          /* no retries left */
          /* report the timeout to the application */
          Scc_ReportError(Scc_StackError_Timeout);
        }
      }
      /* #40 check if timeout happened while trying to close the TCP connection */
      else if ( Scc_MsgTrig_StopCommunicationSession == Scc_MsgTrig )
      {
        /* Prepare shutdown */
        Scc_State = Scc_State_ShuttingDown;
        /* close the connection */
        (void) TcpIp_Close(Scc_V2GSocket, TRUE);
      }
      else
      {
        /* report the timeout to the application */
        Scc_ReportError(Scc_StackError_Timeout);
      }
    }
  }
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_TxSECCDiscoveryProtocolReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_TxSECCDiscoveryProtocolReq(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* create the socket address struct */
  Scc_SockAddrIn6Type IpAddrPortDst;

  /* ----- Implementation ----------------------------------------------- */
  /* use the exi struct buf to create the v2gtp header */
  /* #10 set the version info */
  Scc_Exi_StructBuf[Scc_V2GTPOffsets_Version]        = (uint8)   SCC_V2G_PROT_VER;
  Scc_Exi_StructBuf[Scc_V2GTPOffsets_InverseVersion] = (uint8)((~SCC_V2G_PROT_VER) & 0xFFu);
  /* #20 set the payload type */
  Scc_Exi_StructBuf[Scc_V2GTPOffsets_PayloadType] =
    (uint8)(( (uint16)SCC_V2GTP_HDR_PAYLOAD_TYPE_SDP_REQ & 0xFF00u ) >> 8u );
  Scc_Exi_StructBuf[Scc_V2GTPOffsets_PayloadType+1u] =
    (uint8) ( (uint16)SCC_V2GTP_HDR_PAYLOAD_TYPE_SDP_REQ & 0x00FFu );
  /* #30 set the payload length */
  Scc_Exi_StructBuf[Scc_V2GTPOffsets_PayloadLength] =
    (uint8)(( (SCC_SDP_REQ_LEN-SCC_V2GTP_HDR_LEN) & 0xFF000000u ) >> 24u ); /*lint !e572 Excessive shift value */
  Scc_Exi_StructBuf[Scc_V2GTPOffsets_PayloadLength+1u] =
    (uint8)(( (SCC_SDP_REQ_LEN-SCC_V2GTP_HDR_LEN) & 0x00FF0000u ) >> 16u ); /*lint !e572 Excessive shift value */
  Scc_Exi_StructBuf[Scc_V2GTPOffsets_PayloadLength+2u] =
    (uint8)(( (SCC_SDP_REQ_LEN-SCC_V2GTP_HDR_LEN) & 0x0000FF00u ) >> 8u ); /*lint !e572 Excessive shift value */
  Scc_Exi_StructBuf[Scc_V2GTPOffsets_PayloadLength+3u] =
    (uint8) ( (SCC_SDP_REQ_LEN-SCC_V2GTP_HDR_LEN) & 0x000000FFu );

  /* #40 set the security (TLS or TCP) */
  Scc_Get_Core_SDPSecurityRx(&Scc_SelectedConnectionType);

  /* Scc_SdpRetransRetriesCnt is a decrementing counter. Hence < is used */
  /* #50 Check if number of SDP retries with Tls exceeds the threshold */
  if ( (Scc_SdpRetransRetriesCnt <= (Scc_ConfigValue_Timer_General_SECCDiscoveryProtocolRetries - Scc_ConfigValue_Timer_General_SECCDiscoveryProtocolTlsRetries))
    && (Scc_SelectedConnectionType == Scc_SDPSecurity_Tls) )
  {
    Scc_SelectedConnectionType = Scc_SDPSecurity_None;
    /* #60 Inform upper layer about the change in security type */
    Scc_Set_Core_SDPSecurityTx(Scc_SelectedConnectionType); /* PRQA S 3109, 4342 */ /* MD_Scc_Empty_Statement, MD_Scc_Exi_Generic */
  }

  Scc_Exi_StructBuf[SCC_V2GTP_HDR_LEN+Scc_SDPReqOffsets_Security] = (uint8)Scc_SelectedConnectionType;
  Scc_Exi_StructBuf[SCC_V2GTP_HDR_LEN+Scc_SDPReqOffsets_Protocol] = SCC_SDP_TP_TCP;

  /* #70 create and initialize the socket address struct */
  IpAddrPortDst.domain = SCC_IPvX_IPV6;
  IpAddrPortDst.port   = IPBASE_HTON16(SCC_SDP_SERVER_PORT);
  /* copy the IP address */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
  IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&IpAddrPortDst.addr[0],
    (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&IpV6_AddrAllNodesLL, sizeof(IpV6_AddrAllNodesLL));

  /* #80 transmit SDP request and check if it failed */
  if ( (Std_ReturnType)E_OK != TcpIp_UdpTransmit(Scc_SDPSocket, (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[0],  /* PRQA S 0310 2 */ /* MD_Scc_0310 */
    (P2VAR(Scc_SockAddrType, AUTOMATIC, SCC_APPL_DATA))&IpAddrPortDst, SCC_SDP_REQ_LEN) )
  {
    Scc_TxDataSent = 0;
    Scc_ReportError(Scc_StackError_TransportLayer);
  }
  else
  {
    Scc_TxDataSent = SCC_SDP_REQ_LEN;
    retVal = E_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_RxSECCDiscoveryProtocolRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_RxSECCDiscoveryProtocolRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 provide the TransportProtocol information to the application */
  Scc_Set_Core_SDPTransportProtocol((Scc_SDPTransportProtocolType)Scc_Exi_StructBuf[Scc_SDPResOffsets_Protocol]); /* PRQA S 4342 */ /* MD_Scc_Exi_Generic */

  /* #20 provide the security information to the application */
  Scc_Set_Core_SDPSecurityTx((Scc_SDPSecurityType)Scc_Exi_StructBuf[Scc_SDPResOffsets_Security]); /* PRQA S 4342 */ /* MD_Scc_Exi_Generic */
  Scc_Security = (Scc_SDPSecurityType)Scc_Exi_StructBuf[Scc_SDPResOffsets_Security]; /* PRQA S 4342 */ /* MD_Scc_Exi_Generic */

  /* #30 check if the option for the transport protocol and security are correct, else ignore this message */
  if (   ( (uint8)SCC_SDP_TP_TCP == Scc_Exi_StructBuf[Scc_SDPResOffsets_Protocol] )
      && (   ( (uint8)Scc_SDPSecurity_Tls == Scc_Exi_StructBuf[Scc_SDPResOffsets_Security] )
          || ( (uint8)Scc_SDPSecurity_None == Scc_Exi_StructBuf[Scc_SDPResOffsets_Security] )))
  {
    uint16 Port;

    Port  = (uint16)( (uint16)Scc_Exi_StructBuf[Scc_SDPResOffsets_Port   ] << 8u );
    Port |= (uint16)          Scc_Exi_StructBuf[Scc_SDPResOffsets_Port + 1u];

    /* #40 set the server socket address struct */
    Scc_ServerSockAddr.domain = SCC_IPvX_IPV6;
    Scc_ServerSockAddr.port   = (uint16)(IPBASE_HTON16(Port));
    /* #50 copy the IP address of the server socket */
    /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy  */
    IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_ServerSockAddr.addr[0u],
      (P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[Scc_SDPResOffsets_IPAddress], 16u);

    /* provide the socket to the application */
    Scc_Set_Core_SECCIPAddress(&Scc_Exi_StructBuf[Scc_SDPResOffsets_IPAddress]);
    Scc_Set_Core_SECCPort(Port);

    /* reset the retry counter */
    Scc_SdpRetransRetriesCnt = (uint16)Scc_ConfigValue_Timer_General_SECCDiscoveryProtocolRetries;

    /* report the status to the application */
    Scc_ReportSuccessAndStatus(Scc_MsgStatus_SECCDiscoveryProtocol_OK);

    /* set the new state */
    Scc_State = Scc_State_SDPComplete;

    retVal = E_OK;
  }


  return retVal;
} /* PRQA S 6050 */ /* MD_MSR_STCAL */

/**********************************************************************************************************************
 *  Scc_CheckV2GHeader
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_CheckV2GHeader(Scc_SocketType Socket)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  uint8          V2GTPHeaderBuf[4];

  /* ----- Implementation ----------------------------------------------- */
  /* copy the header from the pbuf */
  if ( (Std_ReturnType)E_OK != IpBase_CopyPbuf2String(&V2GTPHeaderBuf[0], &Scc_ExiStreamRxPBuf[0], 4, 0) )
  {
#if(SCC_DEV_ERROR_REPORT == STD_ON)
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_CHECK_V2G_HEADER, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
  }

  /* #10 check the V2G Protocol Version */
  if (   ( (uint8)SCC_V2G_PROT_VER == V2GTPHeaderBuf[0] )
      && ( (uint8)SCC_V2G_PROT_VER == (uint8)(~V2GTPHeaderBuf[1]) ))
  {
    /* #20 check the payload type of the SDP packet */
    if ( Scc_SDPSocket == Socket )
    {
      /* check the payload type */
      if ( (   ( ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_SDP_RES & 0xFF00u) ) >> 8 )               == V2GTPHeaderBuf[2] )
            &&   ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_SDP_RES & 0x00FFu) )                      == V2GTPHeaderBuf[3] ))
        || (   ( ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_SDP_PAIR_POSITION_RES & 0xFF00u) ) >> 8 ) == V2GTPHeaderBuf[2] )
            &&   ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_SDP_PAIR_POSITION_RES & 0x00FFu) )        == V2GTPHeaderBuf[3] )) )
      {
        retVal = E_OK;
      }
    }
    /* #30 check the payload type of the V2G packet */
    else if ( Scc_V2GSocket == Socket )
    {
      /* check the payload type */
      if (   (   ( ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_SAP & 0xFF00u) ) >> 8 )               == V2GTPHeaderBuf[2] )
              &&   ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_SAP & 0x00FFu) )                      == V2GTPHeaderBuf[3] ))
          || (   ( ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_EXI & 0xFF00u) ) >> 8 )               == V2GTPHeaderBuf[2] )
              &&   ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_EXI & 0x00FFu) )                      == V2GTPHeaderBuf[3] ))
          || (   ( ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_RENEGOTIATION & 0xFF00u)) >> 8)       == V2GTPHeaderBuf[2])
              &&   ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_RENEGOTIATION & 0x00FFu))             == V2GTPHeaderBuf[3]))
          || (   ( ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_ACD_SYSTEM_STATUS & 0xFF00u)) >> 8)   == V2GTPHeaderBuf[2])
              &&   ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_ACD_SYSTEM_STATUS & 0x00FFu))         == V2GTPHeaderBuf[3]))
          || (   ( ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_METERING_RECEIPT & 0xFF00u)) >> 8)    == V2GTPHeaderBuf[2] )
              &&   ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_METERING_RECEIPT & 0x00FFu))          == V2GTPHeaderBuf[3] ))
          || (   ( ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_CERTIFICATE_INSTALL & 0xFF00u)) >> 8) == V2GTPHeaderBuf[2] )
              &&   ( ( (uint16)(SCC_V2GTP_HDR_PAYLOAD_TYPE_CERTIFICATE_INSTALL & 0x00FFu))       == V2GTPHeaderBuf[3] )))
      {
        retVal = E_OK;
      }
    }
    else
    {
      /* invalid socket */
      retVal = E_NOT_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_EstablishTLConnection
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_EstablishTLConnection(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 get the socket from the TcpIp*/
  if ( (Std_ReturnType)E_OK != TcpIp_SccGetSocket(TCPIP_AF_INET6, TCPIP_IPPROTO_TCP, &Scc_V2GSocket) )
  {
    Scc_ReportError(Scc_StackError_TransportLayer);
  }
  else
  {
    uint32 u32TxBufferLen = SCC_TCP_TX_BUF_LEN;
    uint32 u32RxBufferLen = SCC_TCP_RX_BUF_LEN;

    /* #20 set the TCP Tx buffer */
    if ( (Std_ReturnType)E_OK != TcpIp_ChangeParameter(Scc_V2GSocket, TCPIP_PARAMID_V_TCP_TXBUFSIZE,  /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
      (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA))&u32TxBufferLen) )
    {
      /* Prepare shutdown */
      Scc_State = Scc_State_ShuttingDown;
      /* close the connection */
      (void) TcpIp_Close(Scc_V2GSocket, TRUE);
      Scc_ReportError(Scc_StackError_TransportLayer);
    }
    /* #30 set the TCP Rx buffer */
    else if ( (Std_ReturnType)E_OK != TcpIp_ChangeParameter(Scc_V2GSocket, TCPIP_PARAMID_TCP_RXWND_MAX,  /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
      (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA))&u32RxBufferLen) )
    {
      /* Prepare shutdown */
      Scc_State = Scc_State_ShuttingDown;
      /* close the connection */
      (void) TcpIp_Close(Scc_V2GSocket, TRUE);
      Scc_ReportError(Scc_StackError_TransportLayer);
    }
    else
    {
      Std_ReturnType retVal = E_OK;
#if ( SCC_ENABLE_TLS == STD_ON )
      /* get the SDPSecurity from the application */
      Scc_Get_Core_SDPSecurityRx(&Scc_SelectedConnectionType);

      /* #40 check if TLS shall be used */
      if ( Scc_SDPSecurity_Tls == Scc_SelectedConnectionType )
      {

        uint8 u8ChangeParameterParam = (uint8)TRUE;
        u32TxBufferLen = SCC_TLS_TX_BUF_LEN;
        u32RxBufferLen = SCC_TLS_RX_BUF_LEN;

        /* #50 set the TLS option */
        if ( (Std_ReturnType)E_OK != TcpIp_ChangeParameter(Scc_V2GSocket, TCPIP_PARAMID_V_USE_TLS,  /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
          (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA))&u8ChangeParameterParam) )
        {
          /* Prepare shutdown */
          Scc_State = Scc_State_ShuttingDown;
          /* close the connection */
          (void) TcpIp_Close(Scc_V2GSocket, TRUE);
          Scc_ReportError(Scc_StackError_TransportLayer);
          retVal = E_NOT_OK;
        }
        /* #60 set the TLS Tx buffer */
        /* PRQA S 0310,3305 3 */ /* MD_Scc_0310_0314_0316_3305 */
        else if ( (Std_ReturnType)E_OK != TcpIp_ChangeParameter(Scc_V2GSocket, TCPIP_PARAMID_V_TLS_TXBUFSIZE,
          (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA))&u32TxBufferLen) )
        {
          /* Prepare shutdown */
          Scc_State = Scc_State_ShuttingDown;
          /* close the connection */
          (void) TcpIp_Close(Scc_V2GSocket, TRUE);
          Scc_ReportError(Scc_StackError_TransportLayer);
          retVal = E_NOT_OK;
        }
        /* #70 set the TLS Rx buffer */
        else if ( (Std_ReturnType)E_OK != TcpIp_ChangeParameter(Scc_V2GSocket, TCPIP_PARAMID_V_TLS_RXBUFSIZE, /* PRQA S 2004 */ /* MD_Scc_2004 */
          (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA))&u32RxBufferLen) ) /* PRQA S 0310, 3305 */ /* MD_Scc_0310_0314_0316_3305 */
        {
          /* Prepare shutdown */
          Scc_State = Scc_State_ShuttingDown;
          /* close the connection */
          (void) TcpIp_Close(Scc_V2GSocket, TRUE);
          Scc_ReportError(Scc_StackError_TransportLayer);
          retVal = E_NOT_OK;
        }
      }
#endif /* SCC_ENABLE_TLS */

      if ( retVal == E_OK ) /* PRQA S 2991,2995 */ /* MD_Scc_VariantDependent */
      {
        uint16 Port = TCPIP_PORT_ANY;
        /* #80 bind the socket */
        if ( (Std_ReturnType)E_OK != TcpIp_Bind(Scc_V2GSocket, (TcpIp_LocalAddrIdType)SCC_IPV6_ADDRESS, &Port) )
        {
          /* Prepare shutdown */
          Scc_State = Scc_State_ShuttingDown;
          /* close the connection */
          (void) TcpIp_Close(Scc_V2GSocket, TRUE);
          Scc_ReportError(Scc_StackError_TransportLayer);
        }
        else
        {
          /* #90 start the TLS/TCP connection */
          if ( (Std_ReturnType)E_OK == TcpIp_TcpConnect(Scc_V2GSocket, (P2VAR(Scc_SockAddrType, AUTOMATIC, SCC_APPL_DATA))&Scc_ServerSockAddr) ) /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
          {
            /* socket is now in use */
            Scc_V2GSocketInUse = TRUE;
            /* update the states */
            Scc_State = Scc_State_TLConnecting;
            Scc_MsgStateFct(Scc_MsgState_RequestSent)
            /* report it to the application */
            Scc_Set_Core_TCPSocketState(Scc_TCPSocketState_Connecting);
            /* start the timeout (if value is 0, timeout will not be started) */
            Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_General_TransportLayerTimeout;
          }
          else
          {
            /* report the error */
            Scc_ReportError(Scc_StackError_TransportLayer);
            /* Prepare shutdown */
            Scc_State = Scc_State_ShuttingDown;
            /* close the connection */
            /* close the transport layer connection */
            (void) TcpIp_Close(Scc_V2GSocket, TRUE);
          }
        }
      }
    }
  }
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_Cbk_CopyTxData
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(BufReq_ReturnType, SCC_CODE) Scc_Cbk_CopyTxData(
    TcpIp_SocketIdType SocketId,
    P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr
)
{
  /* ----- Local Variables ---------------------------------------------- */
  BufReq_ReturnType retVal = BUFREQ_E_NOT_OK;
  Std_ReturnType retVal_Scc = E_NOT_OK;
  Scc_PbufType TxPbuf;
  uint8 errorId = SCC_DET_NO_ERROR;

  /* ----- Development Error Checks ------------------------------------- */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of runtime parameters. */
  if ( SocketId != Scc_V2GSocket )
  {
    errorId = SCC_DET_INV_PARAM;
    retVal = BUFREQ_E_NOT_OK;
  }
  else if ( BufPtr == NULL_PTR )
  {
    errorId = SCC_DET_INV_POINTER;
    retVal = BUFREQ_E_NOT_OK;
  }
  else if ( BufLengthPtr == NULL_PTR )
  {
    errorId = SCC_DET_INV_POINTER;
    retVal = BUFREQ_E_NOT_OK;
  }
  else
#endif /* SCC_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */
    SCC_DUMMY_STATEMENT(SocketId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */

    /* EXI needs Pbuf */
    TxPbuf.payload = BufPtr;
    TxPbuf.len = *BufLengthPtr;
    TxPbuf.totLen = *BufLengthPtr;

    if(Scc_MsgState_StreamingRequest == Scc_MsgState)
    {
      /* #20 Exi encode stream all schemata message */
      retVal_Scc = Scc_Exi_StreamRequest( BufPtr, BufLengthPtr, &TxPbuf);
    }
    else if(Scc_MsgState_WaitForNextRequest == Scc_MsgState) /* PRQA S 2004 */ /* MD_Scc_2004 */
    {
      if( Scc_MsgTrig_SupportedAppProtocol == Scc_MsgTrig )
      {
        /* #30 Exi encode SupportedAppProtocol Request */
        retVal_Scc = Scc_Exi_EncodeSupportedAppProtocolReq( BufPtr, BufLengthPtr, &TxPbuf );
      }
      else
      {
        /* #40 Exi encode DIN/ISO_Ed1/ISO_Ed2 V2G message */
        retVal_Scc = (*Scc_ExiTx_Xyz_EncodeMessageFctPtr)( BufPtr, BufLengthPtr, &TxPbuf );
      }
    }

    /* Convert the return type from Scc_ReturnType to BufReq_ReturnType */
    if(retVal_Scc == (Std_ReturnType)Scc_ReturnType_OK)
    {
      retVal = BUFREQ_OK;
    }
    else
    {
      *BufLengthPtr = 0;
    }
  }

  /* ----- Development Error Report --------------------------------------- */
#if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #50 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_CBK_COPY_TX_DATA, errorId);
  }
#else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* SCC_DEV_ERROR_REPORT */

  return retVal;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON ) && (defined TLS_SUPPORT_GET_NVM_BLOCK_ID_FOR_USED_ROOT_CERT && (TLS_SUPPORT_GET_NVM_BLOCK_ID_FOR_USED_ROOT_CERT == STD_ON))
/**********************************************************************************************************************
 *  Scc_Get_RootCertUsed
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_Get_RootCertUsed(P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) RootCertId)
{
  /* ----- Local Variables ---------------------------------------------- */
  NvM_BlockIdType nvM_BlockId = 0xFFFF;
  uint8_least index;
  Std_ReturnType retVal = E_NOT_OK;

  /* ----- Implementation ----------------------------------------------- */
  *RootCertId = 0xFF;
  /* #10 Check if Tls connection was established */
  if ( Scc_Security == Scc_SDPSecurity_Tls )
  {
    /* #20 Get block Id used for Tls handshake */
    retVal = TcpIp_Tls_GetNvmBlockIdForUsedRootCert(Scc_V2GSocket, &nvM_BlockId);

    /* #30 Check if retVal is ok */
    if (retVal == E_OK)
    {
      /* #40 Set retVal to not ok and Iterate for number of root certificates */
      retVal = E_NOT_OK;
      for (index = 0; index < SCC_ROOT_CERT_CNT; index++)
      {
        /* #50 Check if Nvm block id matches */
        if (nvM_BlockId == Scc_CertsNvm.RootCerts[index].NvmBlockId)
        {
          /* #60 Set retVal to ok and set the index */
          retVal = E_OK;
          *RootCertId = (uint8)index;
          break;
        }
      }
    }
  }

  return retVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING, TLS_SUPPORT_GET_NVM_BLOCK_ID_FOR_USED_ROOT_CERT */

#define SCC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/

/* Justification for module specific MISRA deviations:

  MD_Scc_0310_0314_0316_3305:
    Reason:     Cast is necessary to better use the resources.
    Risk:       Alignment issues may occur.
    Prevention: Covered by code review.

  MD_Scc_0770:
    Reason:     While searching, the code after the continue must not be executed.
    Risk:       Statements that follow the continue are not executed, code could behave unexpected.
    Prevention: Covered by code review.

  MD_Scc_0771:
    Reason:     The code after the break must not be executed.
    Risk:       Statements that follow the break are not executed, code could behave unexpected.
    Prevention: Covered by code review.

  MD_Scc_0781:
    Reason:     Name of atomic parameter is also used as a name of a struct parameter.
    Risk:       None.
    Prevention: Covered by code review.

  MD_Scc_2002:
    Reason:     No default handling required.
    Risk:       Missing default handling.
    Prevention: Covered by test cases and code review.

  MD_Scc_2004:
    Reason:     Missing else path in if-condition.
    Risk:       Missing default handling.
    Prevention: Covered by test cases and code review.

  MD_Scc_2742_2880_2995:
    Reason:     The statement will not be reached in this configuration. But there are other configuration variants
                  in which this statement will be reached. E.g. other post-build configuration or different use-case.
    Risk:       None.
    Prevention: Already correctly handled, but not detected by PRQA.

  MD_Scc_2982:
    Reason:     Assignment is only obsolete, if application correctly sets the value in the callback. Otherwise it
                  prevents an issue from occurring.
    Risk:       None.
    Prevention: Covered by code review.

  MD_Scc_3215:
    Reason:     This 'switch' statement contains only a single path because there is only one schema enabled.
    Risk:       There is no risk.
    Prevention: Covered by code review.

  MD_Scc_3218:
    Reason:     Local static variables are forbidden due to memory mapping, therefore variable has to be file static.
    Risk:       Small, parameters are already file-local and variable may be accessed from other functions.
    Prevention: Covered by code review.

  MD_Scc_3332:
    Reason:     It is checked if the macro is defined.
    Risk:       None.
    Prevention: Already correctly handled, but not detected by PRQA.

  MD_Scc_3333:
    Reason:     The loop/case is used to search for something. If it was found,
                  the loop/case can be quit (by using break).
    Risk:       Statements that follow the break are not executed, code could behave unexpected.
    Prevention: Covered by code review.

  MD_Scc_2996:
    Reason:     This condition is always false only in case Det is enabled. To protect code against stack corruption
                in case Det is disabled this if condition is added.
    Risk:       No risk.
    Prevention: Covered by code review.

  MD_Scc_QAC_Mistaken:
    Reason:     MISRA detected an issue where there is none. The code does not violate any rules.
    Risk:       None.
    Prevention: Covered by code review.

  MD_Scc_Exi_Generic:
    Reason:     The Exi component provides and consumes information in a generic way. Scc uses type safe storage.
                  Therefore, the values must be converted.
    Risk:       Alignment issues may occur.
    Prevention: Covered by code review.

  MD_Scc_Nvm_Generic:
    Reason:     The Nvm component takes information in a generic way. Scc uses type safe storage.
                  Therefore, the values must be converted.
    Risk:       None.
    Prevention: Covered by code review.

  MD_Scc_Empty_Statement:
    Reason:     If macros are used in different code variants it may happen that after resolving
                  the macro only a null statement remains. This happens typically if a callback is not set.
    Risk:       Wrong macro implementations could remain undetected.
    Prevention: Covered by code review and component test suite.

  MD_Scc_20.4:
    Reason:     Since efficiency is a primary implementation target it is necessary to use macros.
    Risk:       Resulting code is difficult to understand or may not work as expected.
    Prevention: Covered by code review and component test suite.

  MD_Scc_15.5:
    Reason:     A single exit point may lead to an inefficient or more difficult to understand
                  code because the structure of the function can become more complex.
                  Therefore it may be necessary to violate this rule for runtime and maintenance
                  efficiency reasons.
    Risk:       Function may e.g. terminate with locked interrupts. Control and data flow have
                  multiple paths -> increased risk of adding errors during maintenance activities.
    Prevention: Product design defines two "blocks" in each function, one block is
                  "checking/validity action like DET checks", and another block is "functional
                  behavior". "Checking/validity actions" are allowed to terminate immediately
                  resulting in more than one exit in the function. "Functional behavior" checks
                  are expected to terminate at the end of the function in one single exit.
                  Based on this design rule, a justification is necessary for each deviating
                  function.

  MD_Scc_ReturnType:
    Reason:     The Scc_StdReturnType includes the Std_TReturnType values.
    Risk:       Wrong mapping of return values.
    Prevention: Covered by code review and component test suite.

  MD_Scc_VariantDependent:
    Reason:     The variable usage depends on the actual variant.
    Risk:       None.
    Prevention: Covered by code review.

  MD_SCC_DummyCallback:
    Reason:     The callback is used to enable compiling but does not contain any functionality.
    Risk:       None.
    Prevention: Covered by code review.
*/

/**********************************************************************************************************************
 *  END OF FILE: Scc.c
 *********************************************************************************************************************/
