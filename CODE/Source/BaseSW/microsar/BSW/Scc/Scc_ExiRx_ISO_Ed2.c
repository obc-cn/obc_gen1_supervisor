/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc_ExiRx_ISO_Ed2.c
 *        \brief  Smart Charging Communication Source Code File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *********************************************************************************************************************/

#define SCC_EXIRX_ISO_ED2_SOURCE

/**********************************************************************************************************************
   LOCAL MISRA / PCLINT JUSTIFICATION
**********************************************************************************************************************/
/* PRQA S 0777 EOF */ /* MD_MSR_Rule5.1 */
/* PRQA S 0779 EOF */ /* MD_MSR_Rule5.2 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Scc_Cfg.h"

#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) /* PRQA S 3332 */ /* MD_Scc_3332 */

#include "Scc_Exi.h"
#include "Scc.h"
#include "Scc_Priv.h"
#include "Scc_Lcfg.h"
#include "Scc_Interface_Cfg.h"

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
#  include "Csm.h"
#endif /* SCC_ENABLE_PNC_CHARGING */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
#include "Det.h"
#endif /* SCC_DEV_ERROR_DETECT */
#include "Exi.h"
#include "IpBase.h"
#include "NvM.h"
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
#include "XmlSecurity.h"
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined (SCC_LOCAL)
# define SCC_LOCAL static
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3453 MACROS_FUNCTION_LIKE */ /* MD_MSR_FctLikeMacro */

/* PRQA L:MACROS_FUNCTION_LIKE */
/**********************************************************************************************************************
 *  LOCAL / GLOBAL DATA
 *********************************************************************************************************************/
/* other variables */
#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
SCC_LOCAL P2VAR(Exi_ISO_ED2_DIS_V2G_MessageType, AUTOMATIC, SCC_VAR_NOINIT) Scc_ExiRx_ISO_Ed2_DIS_MsgPtr;

#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response>
 *********************************************************************************************************************/
/*! \brief         Get all parameters of ISO_Ed2 response message
 *  \details        Validates parameter and reports them to the application.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_SessionSetupRes(void);
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_ServiceDiscoveryRes(void);
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_ServiceDetailRes(void);
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_ServiceSelectionRes(void);
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_IdentificationDetailsRes(void);
#endif /* SCC_ENABLE_PNC_CHARGING */
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_AuthorizationRes(void);
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_ChargeParameterDiscoveryRes(void);
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_PowerDeliveryRes(void);
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_MeteringReceiptRes(void);
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_CertificateInstallationRes(void);
#endif /* SCC_ENABLE_PNC_CHARGING */
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_SessionStopRes(void);

/* AC */

/* DC */
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_CableCheckRes(void);
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_PreChargeRes(void);
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_CurrentDemandRes(void);
/* see pattern Scc_ExiRx_ISO_Ed2_<ISO_Ed2-Response> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_WeldingDetectionRes(void);
#endif /* SCC_CHARGING_DC */

/* WPT */

/* AC_BPT */
#if ( SCC_CHARGING_AC_BPT == STD_ON )
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_AC_BidirectionalControlRes(void);
#endif /* SCC_CHARGING_AC_BPT */

/* DC_BPT */
#if (SCC_CHARGING_DC_BPT == STD_ON )
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_DC_BidirectionalControlRes(void);
#endif /* SCC_CHARGING_DC_BPT */

/* XmlSecurity */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
*  Scc_ExiRx_ISO_Ed2_XmlSecDereference
*********************************************************************************************************************/
/*! \brief          check the validity of Contract Certificate
*  \details        check if the currently installed certificate is expired or will expire soon.
*  \param[out]     ExiStructPtr          returns the pointer to the signed element.
*  \param[out]     ExiRootElementId      root element of the signed element.
*  \param[out]     ExiNamespaceId        Name space if of the signed element.
*  \param[in]      URIPtr                Pointer to the URI of the signed element
*  \param[in]      URILength             Length of the URI
*  \return         OK                    Signed element was found and.
*  \return         NotOK                 Signed element was not found.
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    TRUE
*********************************************************************************************************************/
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_XmlSecDereference(P2VAR(uint8*, AUTOMATIC, SCC_VAR_NOINIT) ExiStructPtr,
P2VAR(Exi_RootElementIdType, AUTOMATIC, SCC_VAR_NOINIT) ExiRootElementId, P2VAR(Exi_NamespaceIdType, AUTOMATIC, SCC_VAR_NOINIT) ExiNamespaceId,
P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) URIPtr, uint16 URILength);
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_Init
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_ExiRx_ISO_Ed2_Init(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the message pointer */
  Scc_ExiRx_ISO_Ed2_DIS_MsgPtr = (P2VAR(Exi_ISO_ED2_DIS_V2G_MessageType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_StructBuf[0]; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */

}

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_InitXmlSecurityWorkspace
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */

FUNC(void, SCC_CODE) Scc_ExiRx_ISO_Ed2_InitXmlSecurityWorkspace(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize the XmlSecurity workspace */
  if ( E_OK != XmlSecurity_InitSigValWorkspace(
   &Scc_Exi_XmlSecSigValWs,
   Scc_ExiRx_ISO_Ed2_XmlSecDereference, Scc_XmlSecGetPublicKey) )
  {
    /* since this should not happen, issue a DET */
#if(SCC_DEV_ERROR_REPORT == STD_ON)
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_CERTIFICATE_INSTALL_RES, SCC_DET_XML_SEC);
#endif /* SCC_DEV_ERROR_REPORT */
  }
}
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DecodeMessage
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_ExiRx_ISO_Ed2_DecodeMessage(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize the Exi workspace */
  if ( (Std_ReturnType)E_OK == Scc_Exi_InitDecodingWorkspace() )
  {
    /* set the decode information */
    Scc_Exi_DecWs.OutputData.SchemaSetId = EXI_SCHEMA_SET_ISO_ED2_DIS_TYPE;

    /* #20 Exi-decode the V2G response */
    if ( (Std_ReturnType)E_OK == Exi_Decode(&Scc_Exi_DecWs) )
    {
#if ( defined SCC_DEM_EXI )
      /* report status to DEM */
      Scc_DemReportErrorStatusPassed(SCC_DEM_EXI);
#endif /* SCC_DEM_EXI */

      /* #30 check the SessionID starting from ServiceDiscovery */
      if ( Scc_MsgTrig_SessionSetup < Scc_MsgTrig )
      {
        /* compare the length of the stored and received SessionID */
        if ( Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Header->SessionID->Length == Scc_SessionIDNvm[0] )
        {
          /* if the length is the same, check if the content is the same, too */
          if ( IPBASE_CMP_EQUAL != IpBase_StrCmpLen(&Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Header->SessionID->Buffer[0],
            &Scc_SessionIDNvm[1], Scc_SessionIDNvm[0] ))
          {
            /* SessionID is not equal, report the error */
            Scc_ReportError(Scc_StackError_InvalidRxParameter);
            retVal = E_NOT_OK;
          }
          else
          {
            retVal = E_OK;
          }
        }
        /* length is not the same */
        else
        {
          /* report the error */
          Scc_ReportError(Scc_StackError_InvalidRxParameter);
          retVal = E_NOT_OK;
        }
      }
      else
      {
        retVal = E_OK;
      }

      if ( retVal == E_OK )
      {
        retVal = E_NOT_OK;

        /* #40 handle an incoming response message */
        switch ( Scc_MsgTrig )
        {
          /* handle an incoming session setup response message */
        case Scc_MsgTrig_SessionSetup:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_SessionSetupRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_SessionSetup_OK);
            /* the session has been successfully connected */
            Scc_State = Scc_State_Connected;
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming service discovery response message */
        case Scc_MsgTrig_ServiceDiscovery:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_ServiceDiscoveryRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_ServiceDiscovery_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming service detail response message */
        case Scc_MsgTrig_ServiceDetail:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_ServiceDetailRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_ServiceDetail_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming service selection response message */
        case Scc_MsgTrig_PaymentServiceSelection:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_ServiceSelectionRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_PaymentServiceSelection_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

      #if ( SCC_ENABLE_PNC_CHARGING == STD_ON )

      #if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
          /* handle an incoming certificate installation response message */
        case Scc_MsgTrig_CertificateInstallation:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_CertificateInstallationRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_CertificateInstallation_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;
      #endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

          /* handle an incoming identification details response message */
        case Scc_MsgTrig_PaymentDetails:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_IdentificationDetailsRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_PaymentDetails_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

      #endif /* SCC_ENABLE_PNC_CHARGING */

          /* handle an incoming contract authentication response message */
        case Scc_MsgTrig_Authorization:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_AuthorizationRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_Authorization_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming charge parameter discovery response message */
        case Scc_MsgTrig_ChargeParameterDiscovery:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_ChargeParameterDiscoveryRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_ChargeParameterDiscovery_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming power delivery response message */
        case Scc_MsgTrig_PowerDelivery:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_PowerDeliveryRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_PowerDelivery_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

      #if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
          /* handle an incoming metering receipt response message */
        case Scc_MsgTrig_MeteringReceipt:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_MeteringReceiptRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_MeteringReceipt_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;
      #endif /* SCC_ENABLE_PNC_CHARGING */

          /* handle an incoming session stop response message */
        case Scc_MsgTrig_SessionStop:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_SessionStopRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_SessionStop_OK);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          /* the session is now disconnected */
          Scc_State = Scc_State_Disconnected;
          break;


      #if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )

          /* handle an incoming cable check response message */
        case Scc_MsgTrig_CableCheck:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_CableCheckRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_CableCheck_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming pre charge response message */
        case Scc_MsgTrig_PreCharge:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_PreChargeRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_PreCharge_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming current demand response message */
        case Scc_MsgTrig_CurrentDemand:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_CurrentDemandRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_CurrentDemand_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

          /* handle an incoming welding detection response message */
        case Scc_MsgTrig_WeldingDetection:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_WeldingDetectionRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_WeldingDetection_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;

      #endif /* SCC_CHARGING_DC */


#if ( defined SCC_CHARGING_DC_BPT ) && ( SCC_CHARGING_DC_BPT == STD_ON )
          /* check if AC_BidirectionalControl request shall be sent */
        case Scc_MsgTrig_DC_BidirectionalControl:
          /* try to process the message and check if it was successful */
          if ( (Std_ReturnType)E_OK == Scc_ExiRx_ISO_Ed2_DIS_DC_BidirectionalControlRes() )
          {
            Scc_ReportSuccessAndStatus(Scc_MsgStatus_DC_BidirectionalControl_OK);
            Scc_Set_Core_CyclicMsgRcvd(TRUE);
            retVal = E_OK;
          }
          else
          {
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
          }
          break;
#endif /* SCC_CHARGING_DC_BPT */

        default:
          /* invalid state */
          break;
        }
      }
    }
    else
    {
      /* report the error */
      Scc_ReportError(Scc_StackError_Exi);
  #if ( defined SCC_DEM_EXI )
      /* report status to DEM */
      Scc_DemReportErrorStatusFailed(SCC_DEM_EXI);
  #endif /* SCC_DEM_EXI */
      retVal = E_NOT_OK;
    }
  }

  return retVal;
} /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_SessionSetupRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_SessionSetupRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  Exi_ISO_ED2_DIS_responseCodeType responseCode;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_SessionSetupResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_SessionSetupResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_SESSION_SETUP_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* ResponseCode */
      responseCode = BodyPtr->ResponseCode;

      /* EVSEStatus */
      Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

      /* SessionID */
      /* check for a positive response code */
      if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_OK == responseCode )
      {
        /* check if the lengths do not match */
        if ( Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Header->SessionID->Length != Scc_SessionIDNvm[0] )
        {
          responseCode = EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_OK_NEW_SESSION_ESTABLISHED;
        }
        /* if the lengths match */
        else
        {
          /* check if the SessionIDs match */
          if ( IPBASE_CMP_EQUAL == IpBase_StrCmpLen(&Scc_SessionIDNvm[1],
            &Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Header->SessionID->Buffer[0], Scc_SessionIDNvm[0]) )
          {
            responseCode = EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_OK_OLD_SESSION_JOINED;
          }
          /* if they do not match */
          else
          {
            responseCode = EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_OK_NEW_SESSION_ESTABLISHED;
          }
        }
      }
      /* check if the EVSE has opened a new session */
      if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_OK_NEW_SESSION_ESTABLISHED == responseCode )
      {
        /* get the new length of the SessionID */
        Scc_SessionIDNvm[0] = (uint8) Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Header->SessionID->Length;
        /* copy the SessionID */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &Scc_SessionIDNvm[1],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA)) &Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Header->SessionID->Buffer[0], Scc_SessionIDNvm[0]);
        /* write the SessionID to the NvM */
        (void) NvM_SetRamBlockStatus((NvM_BlockIdType)SCC_SESSION_ID_NVM_BLOCK, TRUE);
      }

      /* SessionID */
      Scc_Set_ISO_Ed2_DIS_SessionID(Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Header->SessionID);
      /* EVSEID */
      Scc_Set_ISO_Ed2_DIS_EVSEID(BodyPtr->EVSEID);

      /* ResponseCode */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(responseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}/* PRQA S 6080, 6050 */ /* MD_MSR_STMIF, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_ServiceDiscoveryRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_ServiceDiscoveryRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_ServiceDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_ServiceDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_SERVICE_DISCOVERY_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* EVSEStatus */
      Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

      /* IdentificationOptionList */
      Scc_Set_ISO_Ed2_DIS_IdentificationOptionList(BodyPtr->IdentificationOptionList);

      /* EnergyTransferServiceList */
      Scc_Set_ISO_Ed2_DIS_EnergyTransferServiceList(BodyPtr->EnergyTransferServiceList);

      /* VASList */
      Scc_Set_ISO_Ed2_DIS_VASList(BodyPtr->VASList, BodyPtr->VASListFlag);

      /* ResponseCode */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
} /* PRQA S 6050 */ /* MD_MSR_STCAL */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_ServiceDetailRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_ServiceDetailRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_ServiceDetailResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_ServiceDetailResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_SERVICE_DETAIL_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* EVSEStatus */
      Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

      /* ServiceID */
      Scc_Set_ISO_Ed2_DIS_ServiceID(BodyPtr->ServiceID);

      /* ServiceParameterList */
      Scc_Set_ISO_Ed2_DIS_ServiceParameterList(BodyPtr->ServiceParameterList, 1);

      /* ResponseCode */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
} /* PRQA S 6050 */ /* MD_MSR_STCAL */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_ServiceSelectionRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_ServiceSelectionRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_ServiceSelectionResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_ServiceSelectionResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if (EXI_ISO_ED2_DIS_SERVICE_SELECTION_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* EVSEStatus */
      Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

      /* ResponseCode */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )

#if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_CertificateInstallationRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_CertificateInstallationRes(void) /* PRQA S 2889 */ /* MD_Scc_15.5 */
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 errorId = SCC_DET_NO_ERROR;
  Std_ReturnType RetVal = E_NOT_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_CertificateInstallationResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_CertificateInstallationResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;
  P2VAR(Exi_ISO_ED2_DIS_certificateType, AUTOMATIC, SCC_APPL_DATA) LastCertInChainPtr;

  uint32                 CurrentTime = 0;
  uint8                  Counter;
#if ( defined SCC_ENABLE_CPS_CHECK ) && ( SCC_ENABLE_CPS_CHECK == STD_ON )
                                       /* 'C' ,  'P' ,  'S' */
  uint8                  CPSValue[3] = { 0x43u, 0x50u, 0x53u };
#endif /* SCC_ENABLE_CPS_CHECK */
  XmlSecurity_ReturnType XmlSecRetVal;

  /* ----- Development Error Checks ------------------------------------- */
#if ( SCC_DEV_ERROR_DETECT == STD_ON )
  /* #10 Check plausibility of runtime parameters. */
  if ( ( BodyPtr->SAProvisioningCertificateChain->SubCertificatesFlag != 0u )
    && ( BodyPtr->SAProvisioningCertificateChain->SubCertificates->Certificate == NULL_PTR ) )
  {
    errorId = SCC_DET_INV_POINTER;
  }
  else
#endif /* SCC_DEV_ERROR_DETECT */
  {
    /* ----- Implementation ----------------------------------------------- */
    /* #20 Check if the message is not the one that was expected. */
    if ( EXI_ISO_ED2_DIS_CERTIFICATE_INSTALLATION_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
    {
      Scc_ReportError(Scc_StackError_InvalidRxMessage);
  #if ( defined SCC_DEM_UNEXPECTED_MSG )
      /* report status to DEM */
      Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
  #endif /* SCC_DEM_UNEXPECTED_MSG */
      return (Std_ReturnType)E_NOT_OK;
    }
  #if ( defined SCC_DEM_UNEXPECTED_MSG )
    else
    {
      /* report status to DEM */
      Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
    }
  #endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #30 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* #40 Report the parameter from the response to the application. */

  #if ( defined SCC_ENABLE_CPS_CHECK ) && ( SCC_ENABLE_CPS_CHECK == STD_ON )
    /* check if the CPS content is set in the DomainComponent */
    if ( (Std_ReturnType)E_OK != Scc_SearchDomainComponentForValue(&BodyPtr->SAProvisioningCertificateChain->Certificate->Buffer[0],
      BodyPtr->SAProvisioningCertificateChain->Certificate->Length, &CPSValue[0], sizeof(CPSValue)) )
    {
      /* CPS was not set as content */
      Scc_ReportError(Scc_StackError_IpBase);
      return (Std_ReturnType)E_NOT_OK;
    }
  #endif /* SCC_ENABLE_CPS_CHECK */

    /* add the root cert as the last element of the chain */
    /* check if there are no sub certificates */
    if ( 0u == BodyPtr->SAProvisioningCertificateChain->SubCertificatesFlag )
    {
      /* set the last cert pointer */
      LastCertInChainPtr = BodyPtr->SAProvisioningCertificateChain->Certificate;
    }
    /* if there are sub certificates */
    else
    {
      /* connect the chain from the certificate to its sub certificates */
      BodyPtr->SAProvisioningCertificateChain->Certificate->NextCertificatePtr =
        BodyPtr->SAProvisioningCertificateChain->SubCertificates->Certificate;
      /* set the last cert pointer to the first sub certificate */
      LastCertInChainPtr = BodyPtr->SAProvisioningCertificateChain->SubCertificates->Certificate;
      /* step to the last sub certificate in the chain */
      while ( NULL_PTR != LastCertInChainPtr->NextCertificatePtr )
      {
        /* set the last cert pointer to this last sub certificate in the chain */
        LastCertInChainPtr = LastCertInChainPtr->NextCertificatePtr;
      }
    }

    /* get the current time */
    Scc_Get_ISO_PnC_CurrentTime(&CurrentTime);

    /* step through all root certificates to find a matching one */
    for ( Counter = 0; Counter < Scc_CertsNvm.RootCertNvmCnt; Counter++ )
    {
      /* check if this root certificate is set */
      if ( Scc_NvMBlockReadState_Processed == Scc_CertsWs.RootCertsReadStates[Counter] )
      {
        /* set the root certificate */
        LastCertInChainPtr->NextCertificatePtr =
          (Exi_ISO_ED2_DIS_certificateType*)Scc_CertsNvm.RootCerts[Counter].RootCert; /* [ISOVersionCast] */ /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
        /* if this root cert was successfully validated against the chain, break the loop */
        if ( (Std_ReturnType)E_OK == Scc_ValidateCertChain(
          (Exi_ISO_certificateType*)BodyPtr->SAProvisioningCertificateChain->Certificate, CurrentTime) ) /* [ISOVersionCast] */ /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
        {
          break;
        }
      }

      /* if this root certificate was not the correct one, try the next */
      LastCertInChainPtr->NextCertificatePtr = (P2VAR(Exi_ISO_ED2_DIS_certificateType, AUTOMATIC, SCC_VAR_NOINIT))NULL_PTR;
    }

    /* check if a matching root certificate was found */
    if ( NULL_PTR != LastCertInChainPtr->NextCertificatePtr )
    {
      uint16 PubKeyIdx;
      uint16 PubKeyLen;
      /* get the index of the public key */
      if ( (Std_ReturnType)E_OK != Scc_GetIndexOfPublicKey(&BodyPtr->SAProvisioningCertificateChain->Certificate->Buffer[0],
        BodyPtr->SAProvisioningCertificateChain->Certificate->Length, &PubKeyIdx, &PubKeyLen) )
      {
        return (Std_ReturnType)E_NOT_OK;
      }
      /* check the length of the public key */
      if ( SCC_PUB_KEY_LEN < PubKeyLen )
      {
        /* the public key is too big for the buffer */
        Scc_ReportError(Scc_StackError_InvalidRxParameter);
        return (Std_ReturnType)E_NOT_OK;
      }
      else
      {
        /* the public key fits into the buffer */
        Scc_CertsWs.SaCertPubKeyLen = PubKeyLen;
      }
      /* copy the public key */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
      IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.SaCertPubKey[0],
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->SAProvisioningCertificateChain->Certificate->Buffer[PubKeyIdx], Scc_CertsWs.SaCertPubKeyLen);
      /* set the flag which defines if the public key buffer contains a valid value */
      Scc_SaPubKeyRcvd = TRUE;
  #if ( defined SCC_DEM_CRYPTO )
      /* report status to DEM */
      Scc_DemReportErrorStatusPassed(SCC_DEM_CRYPTO);
  #endif
    }
    /* no matching root certificate was found */
    else
    {
      /* report error to application */
      Scc_ReportError(Scc_StackError_Crypto);
  #if ( defined SCC_DEM_CRYPTO )
      /* report status to DEM */
      Scc_DemReportErrorStatusFailed(SCC_DEM_CRYPTO);
  #endif
      return (Std_ReturnType)E_NOT_OK;
    }

    /* check if the signature exists */
    if ( 1u == Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Header->SignatureFlag )
    {
      /* Set value to all mandatory references in Signed Info */
      Scc_Exi_RefInSigInfo.Flags = 0u;
      Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureCertChain = TRUE;
      Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureEncryptedPrivateKey = TRUE;
      Scc_Exi_RefInSigInfo.Flag.RISI_DHpublickey = TRUE;

      /* validate the signatures */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
      XmlSecRetVal = XmlSecurity_ValidateExiSignature(&Scc_Exi_XmlSecSigValWs,
        Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Header->Signature, &Scc_Exi_TempBuf[0], SCC_EXI_TEMP_BUF_LEN,
        Scc_ECDSAVerifyJobId, Scc_ECDSAVerifyKeyId, Scc_SHA256JobId);

      /* Check if all mandatory references were checked in the function Scc_ExiRx_ISO_XmlSecDereference() */
      if ( Scc_Exi_RefInSigInfo.Flags != 0u )
      {
        /* report the error to the application */
        Scc_ReportError(Scc_StackError_XmlSecurityMissingReference);
  #if ( defined SCC_DEM_XML_SEC )
        /* EVSE provided an invalid signature */
        Scc_DemReportErrorStatusFailed(SCC_DEM_XML_SEC);
  #endif /* SCC_DEM_XML_SEC */
        return (Std_ReturnType)E_NOT_OK;
      }

      /* Check if the signature is valid */
      if ( XmlSec_RetVal_OK != XmlSecRetVal )
      {
        /* report the error to the application */
        Scc_ReportError(Scc_StackError_XmlSecurity);
  #if ( defined SCC_DEM_XML_SEC )
        /* EVSE provided an invalid signature */
        Scc_DemReportErrorStatusFailed(SCC_DEM_XML_SEC);
  #endif /* SCC_DEM_XML_SEC */
        return (Std_ReturnType)E_NOT_OK;
      }
  #if ( defined SCC_DEM_XML_SEC )
      else
      {
        /* signature was valid */
        Scc_DemReportErrorStatusPassed(SCC_DEM_XML_SEC);
      }
  #endif /* SCC_DEM_XML_SEC */
    }
    /* the CertificateInstallation has no signature */
    else
    {
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* set the maximum length of the EMAID */
    Scc_CertsWs.eMAID->Length = (uint16)sizeof(Scc_CertsWs.eMAID->Buffer);
    /* extract the EMAID out of the certificate */
    if ( (Std_ReturnType)E_OK != Scc_GetCertDistinguishedNameObject(
      &BodyPtr->ContractCertificateChain->Certificate->Buffer[0],
      BodyPtr->ContractCertificateChain->Certificate->Length,
      &Scc_CertsWs.eMAID->Buffer[0], &Scc_CertsWs.eMAID->Length, Scc_BEROID_Subject_EMAID) )
    {
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* --- get the certificate and its private key --- */

    /* ContractSignatureChain -> Certificate */
    /* get the length of the contract certificate */
    Scc_CertsWs.ContrCert->Length = BodyPtr->ContractCertificateChain->Certificate->Length;
    /* copy the contract certificate */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
    IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrCert->Buffer[0],
      (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->ContractCertificateChain->Certificate->Buffer[0],
      Scc_CertsWs.ContrCert->Length);

    /* check if the encrypted private key has not the correct length -> length of parameter minus 32 bytes (2 x IV) */
    if ( SCC_PRIV_KEY_LEN != (uint16)(BodyPtr->ContractCertificateEncryptedPrivateKey->Length - SCC_IV_LEN - SCC_IV_LEN) ) /* Delete the extra subtraction once the ED2 has been changed to correct value*/
    {
      Scc_ReportError(Scc_StackError_InvalidRxParameter);
      return (Std_ReturnType)E_NOT_OK;
    }
    /* Copy provisioning private key to KeyExchange KeyType */
    else if ( E_OK != Csm_KeyElementCopy(Scc_ECDSASignProvCertKeyId,
                                         CRYPTO_KE_SIGNATURE_KEY,
                                         Scc_ECDHExchangeKeyId,
                                         CRYPTO_KE_KEYEXCHANGE_PRIVKEY) )
    {
      Scc_ReportError(Scc_StackError_Crypto);
      return (Std_ReturnType)E_NOT_OK;
    }
    /* Decrypt the PrivateKey of the new ContractCertificate */
    else if (E_OK != Scc_DecryptPrivateKey(Scc_ECDHExchangeKeyId, /* PRQA S 2004  */ /* MD_Scc_2004 */
                                           Scc_ECDSASignCertKeyIds[Scc_CertsWs.ChosenContrCertChainIdx],
                                           &BodyPtr->DHPublicKey->Buffer[1],
                                           &BodyPtr->ContractCertificateEncryptedPrivateKey->Buffer[SCC_IV_LEN],
                                           &BodyPtr->ContractCertificateEncryptedPrivateKey->Buffer[0]))
    {
      Scc_ReportError(Scc_StackError_Crypto);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* validate if the new private key belongs to the new contract certificate and check if it failed */
    if ( (Std_ReturnType)E_OK != Scc_ValidateKeyPair(&Scc_CertsWs.ContrCert->Buffer[0],
                                                     Scc_CertsWs.ContrCert->Length,
                                                     Scc_ECDSASignCertJobIds[Scc_CertsWs.ChosenContrCertChainIdx]) )
    {
      /* report the error */
      Scc_ReportError(Scc_StackError_InvalidKeyPair);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* --- write the certificates to the NvM --- */

    /* set the read state */
    Scc_CertsWs.ContrCertReadState = Scc_NvMBlockReadState_Processed;
    /* write the ContractCertificate to the NvM */ /* PRQA S 0314 2 */ /* MD_Scc_Nvm_Generic */
    if ( (Std_ReturnType)E_OK != NvM_WriteBlock((NvM_BlockIdType)Scc_GetNvMBlockIDContrCert(Scc_CertsWs.ChosenContrCertChainIdx),
      (P2VAR(void,AUTOMATIC,SCC_APPL_DATA))&Scc_CertsWs.ContrCert->Buffer[0]) )
    {
      Scc_ReportError(Scc_StackError_NvM);
  #if ( defined SCC_DEM_NVM_READ_CONTR_CERT_FAIL )
      Scc_DemReportErrorStatusFailed(SCC_DEM_NVM_READ_CONTR_CERT_FAIL);
  #endif /* SCC_DEM_NVM_READ_CONTR_CERT_FAIL */
      return (Std_ReturnType)E_NOT_OK;
    }
  #if ( defined SCC_DEM_NVM_READ_CONTR_CERT_FAIL )
    else
    {
      Scc_DemReportErrorStatusPassed(SCC_DEM_NVM_READ_CONTR_CERT_FAIL);
    }
  #endif /* SCC_DEM_NVM_READ_CONTR_CERT_FAIL */

    /* adjust the contract certificate chain size */
    Scc_CertsWs.ContrCertChainSize = 0;
    Scc_CertsWs.ContrSubCertsProcessedFlags = 0;

    /* --- get the sub certificates and write them to the NvM --- */

    /* ContractSignatureChain -> SubCertificates */
    if ( 1u == BodyPtr->ContractCertificateChain->SubCertificatesFlag )
    {
      P2VAR(Exi_ISO_ED2_DIS_certificateType, AUTOMATIC, SCC_VAR_NOINIT) SubCertificatePtr =
        BodyPtr->ContractCertificateChain->SubCertificates->Certificate;

      do
      {
        /* get the length of the sub certificates */
        Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize].Length = SubCertificatePtr->Length;
        /* copy the sub certificates */ /* PRQA S 0310,3305,0315 3 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))
          &Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize].Buffer[0],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&SubCertificatePtr->Buffer[0], SubCertificatePtr->Length);
        /* write the ContractSubCertificates to the NvM */
        if ( (Std_ReturnType)E_OK != NvM_WriteBlock(
          (NvM_BlockIdType)Scc_GetNvMBlockIDContrSubCerts(Scc_CertsWs.ChosenContrCertChainIdx,Scc_CertsWs.ContrCertChainSize),
          (P2VAR(void,AUTOMATIC,SCC_APPL_DATA))&Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize].Buffer[0]) ) /* PRQA S 0314 */ /* MD_Scc_Nvm_Generic */
        {
          Scc_ReportError(Scc_StackError_NvM);
          return (Std_ReturnType)E_NOT_OK;
        }

        if ( 0 < Scc_CertsWs.ContrCertChainSize )
        {
          /* link the sub certificates */
          Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize-1].NextCertificatePtr =
            &Scc_CertsWs.ContrSubCerts[Scc_CertsWs.ContrCertChainSize];
        }

        /* set the new nvm read state */
        Scc_CertsWs.ContrSubCertsReadStates[Scc_CertsWs.ContrCertChainSize] = Scc_NvMBlockReadState_Processed;
        /* update the status flag */
        Scc_CertsWs.ContrSubCertsProcessedFlags |= (uint32)( (uint32)0x01 << (uint8)Scc_CertsWs.ContrCertChainSize );

        /* adjust the contract certificate chain size */
        Scc_CertsWs.ContrCertChainSize++;

        /* adjust the certificate pointer */
        SubCertificatePtr = SubCertificatePtr->NextCertificatePtr;
      }
      while (   ( NULL_PTR != SubCertificatePtr )
             && ( (sint8)Scc_CertsWs.ContrSubCertCnt > Scc_CertsWs.ContrCertChainSize ));
    }

    /* --- write the new chain size to the NvM */

    /* set the read state of the contract certificate chain size*/
    Scc_CertsWs.ContrCertChainSizeReadState = Scc_NvMBlockReadState_Processed;
    /* write the ContractCertificateChainSize and check if it was not successful */
    if ( (Std_ReturnType)E_OK != NvM_WriteBlock(
      (NvM_BlockIdType)Scc_GetNvMBlockIDContrCertChainSize(Scc_CertsWs.ChosenContrCertChainIdx),
      (P2VAR(void, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrCertChainSize) ) /* PRQA S 0314 */ /* MD_Scc_Nvm_Generic */
    {
      Scc_ReportError(Scc_StackError_NvM);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* --- handle the rest of the parameters --- */

    /* BodyPtr -> RemainingContractCertificateChains */
    Scc_Set_ISO_Ed2_DIS_RemainingContractCertificateChains(BodyPtr->RemainingContractCertificateChains);

    /* EVSEStatus */
    Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

    /* ResponseCode */
    Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
      RetVal = E_OK;
    }

  /* ----- Development Error Report --------------------------------------- */
#if ( SCC_DEV_ERROR_REPORT == STD_ON )
  /* #50 Report default errors if any occurred. */
  if ( errorId != SCC_DET_NO_ERROR )
  {
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_CERTIFICATE_INSTALL_RES, errorId);
  }
#else /* SCC_DEV_ERROR_REPORT */
  SCC_DUMMY_STATEMENT(errorId); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */ /*lint !e438 */
#endif /* SCC_DEV_ERROR_REPORT */

  return RetVal;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */
#endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_IdentificationDetailsRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_IdentificationDetailsRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_IdentificationDetailsResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_IdentificationDetailsResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;
  P2VAR(Exi_ISO_ED2_DIS_genChallengeType, AUTOMATIC, SCC_VAR_NOINIT) GenChallengePtr;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_IDENTIFICATION_DETAILS_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* EVSEStatus */
      Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

    #if ( defined SCC_ENABLE_PNC_CHARGING) && ( SCC_ENABLE_PNC_CHARGING == STD_ON )

      /* GenChallenge */ /* PRQA S 0310,3305,0315 4 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
      GenChallengePtr = (P2VAR(Exi_ISO_ED2_DIS_genChallengeType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[0];
      Scc_Exi_TempBufPos = sizeof(Exi_ISO_ED2_DIS_genChallengeType);
      IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&GenChallengePtr->Buffer[0],
        (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&BodyPtr->GenChallenge->Buffer[0], BodyPtr->GenChallenge->Length);
      GenChallengePtr->Length = BodyPtr->GenChallenge->Length;

    #endif /* SCC_ENABLE_PNC_CHARGING */

      /* ResponseCode */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
      /* EVSEProcessing */
      Scc_Set_ISO_Ed2_DIS_EVSEProcessing(BodyPtr->EVSEProcessing);

      retVal = E_OK;
    }
  }
  return retVal;

}

#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_AuthorizationRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_AuthorizationRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_AuthorizationResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_AuthorizationResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_AUTHORIZATION_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* EVSEStatus */
      Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

      /* EVSEProcessing */
      Scc_Set_ISO_Ed2_DIS_EVSEProcessing(BodyPtr->EVSEProcessing);

      /* ResponseCode */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_ChargeParameterDiscoveryRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_ChargeParameterDiscoveryRes(void) /* PRQA S 2889 */ /* MD_Scc_15.5 */
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  boolean removeSalesTariffs = FALSE;
#endif /* SCC_ENABLE_PNC_CHARGING */
  Scc_PhysicalValueType scc_PhysicalValueTmp;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_ChargeParameterDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_ChargeParameterDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
  P2CONST(Exi_ISO_ED2_DIS_DC_CPDResEnergyTransferModeType, AUTOMATIC, SCC_VAR_NOINIT) DC_EVSEChargeParameterPtr;
#endif /* SCC_CHARGING_DC */
#if ( defined SCC_CHARGING_DC_BPT ) && ( SCC_CHARGING_DC_BPT == STD_ON )
  P2CONST(Exi_ISO_ED2_DIS_BPT_DC_CPDResEnergyTransferModeType, AUTOMATIC, SCC_VAR_NOINIT) BPT_DC_EVSEChargeParameterPtr;
#endif /* SCC_CHARGING_DC_BPT */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_CHARGE_PARAMETER_DISCOVERY_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* #30 Report the parameter from the response to the application. */
    /* EVSEStatus */
    Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

    /* EVSEProcessing */
    Scc_Set_ISO_Ed2_DIS_EVSEProcessing(BodyPtr->EVSEProcessing);

  #if ( defined SCC_IGNORE_SA_SCHEDULE_LIST ) && ( SCC_IGNORE_SA_SCHEDULE_LIST == STD_ON ) /* PRQA S 3332 */ /* MD_Scc_3332 */
  #else
    /* only update the values when EVSEProcessing is set to Finished */
    if (( EXI_ISO_ED2_DIS_PROCESSING_TYPE_FINISHED == BodyPtr->EVSEProcessing ) && (Scc_Exi_ControlMode == SCC_ISO_ED2_CONTROL_MODE_TYPE_SCHEDULED))
    {
      /* check if at least one Schedule exists */
      if ((0u == BodyPtr->CPDResControlModeFlag) || (BodyPtr->CPDResControlModeElementId != EXI_ISO_ED2_DIS_SCHEDULED_CPDRES_CONTROL_MODE_TYPE))
      {
        /* report the invalid parameter as stack error */
        Scc_ReportError(Scc_StackError_InvalidRxParameter);
        return (Std_ReturnType)E_NOT_OK;
      }

  #if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
      /* only execute the following code if the PnC profile is active */
      if ( SCC_ISO_PAYMENT_OPTION_TYPE_CONTRACT == Scc_Exi_PaymentOption )
      {
        uint16 PubKeyIdx;
        uint16 PubKeyLen;
        Exi_ISO_ED2_DIS_ScheduleTupleType *ScheduleTuplePtr = ((Exi_ISO_ED2_DIS_Scheduled_CPDResControlModeType*)BodyPtr->CPDResControlMode)->ScheduleList->ScheduleTuple; /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */
        boolean SalesTariffFound = FALSE;

        /* check if the ContrSubCert1 exists */
        if ( 1 > Scc_CertsWs.ContrCertChainSize )
        {
          /* report the missing certificate as stack error */
          Scc_ReportError(Scc_StackError_InvalidTxParameter);
          /* the ContrSubCert1 does not exist, it is not possible to validate the signature */
          return (Std_ReturnType)E_NOT_OK;
        }
        /* get the index of the public key */
        if ( (Std_ReturnType)E_OK != Scc_GetIndexOfPublicKey(&Scc_CertsWs.ContrSubCerts[0].Buffer[0],
          Scc_CertsWs.ContrSubCerts[0].Length, &PubKeyIdx, &PubKeyLen) )
        {
          return (Std_ReturnType)E_NOT_OK;
        }
        /* check the length of the public key */
        if ( SCC_PUB_KEY_LEN < PubKeyLen )
        {
          /* report the invalid public key as stack error */
          Scc_ReportError(Scc_StackError_InvalidTxParameter);
          /* the public key is too big for the buffer */
          return (Std_ReturnType)E_NOT_OK;
        }
        else
        {
          /* the public key fits into the buffer */
          Scc_CertsWs.SaCertPubKeyLen = PubKeyLen;
        }
        /* copy the public key */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.SaCertPubKey[0],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&Scc_CertsWs.ContrSubCerts[0].Buffer[PubKeyIdx],
          Scc_CertsWs.SaCertPubKeyLen);
        /* set the flag which defines if the public key buffer contains a valid value */
        Scc_SaPubKeyRcvd = TRUE;

        /* Reset value */
        Scc_Exi_RefInSigInfo.Flags = 0u;
        /* step through the ScheduleTuples to search for a SalesTariff */
        while ( NULL_PTR != ScheduleTuplePtr )
        {
          /* check if a SalesTariff exists in this ScheduleTuple */
          if ( 1u == ScheduleTuplePtr->SalesTariffFlag )
          {
            SalesTariffFound = TRUE;

            /* Set the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
            if ( Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff2 == TRUE )
            {
              Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff3 = TRUE;
            }
            else if ( Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff1 == TRUE )
            {
              Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff2 = TRUE;
            }
            else
            {
              Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff1 = TRUE;
            }
          }
          /* get to the next ScheduleTuple */
          ScheduleTuplePtr = ScheduleTuplePtr->NextScheduleTuplePtr;
        }

        /* check if signature has to exist */
        if ( TRUE == SalesTariffFound )
        {
          /* check if the signature exists */
          if ( 1u == Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Header->SignatureFlag )
          {
              /* validate the signatures */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
              if ( XmlSec_RetVal_OK != XmlSecurity_ValidateExiSignature(&Scc_Exi_XmlSecSigValWs,
                  Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Header->Signature, &Scc_Exi_TempBuf[0], SCC_EXI_TEMP_BUF_LEN,
                  Scc_ECDSAVerifyJobId, Scc_ECDSAVerifyKeyId, Scc_SHA256JobId) )
              {
                /* remove the SalesTariff */
                removeSalesTariffs = TRUE;
  #if ( defined SCC_DEM_XML_SEC )
                /* EVSE provided an invalid signature */
                Scc_DemReportErrorStatusFailed(SCC_DEM_XML_SEC);
  #endif /* SCC_DEM_XML_SEC */
              }
  #if ( defined SCC_DEM_XML_SEC )
              else
              {
                /* signature was valid */
                Scc_DemReportErrorStatusPassed(SCC_DEM_XML_SEC);
              }
  #endif /* SCC_DEM_XML_SEC */
            /* Check if all mandatory references were checked in the function Scc_ExiRx_ISO_XmlSecDereference() */
            if ( Scc_Exi_RefInSigInfo.Flags != 0u)
            {
              /* remove the SalesTariff */
              removeSalesTariffs = TRUE;
        #if ( defined SCC_DEM_XML_SEC )
              /* EVSE provided an invalid signature */
              Scc_DemReportErrorStatusFailed(SCC_DEM_XML_SEC);
        #endif /* SCC_DEM_XML_SEC */
            }
          }
          /* the SalesTariff has no signature */
          else
          {
            /* remove the SalesTariff */
            removeSalesTariffs = TRUE;
          }
        }

        /* check if the SalesTariff shall be removed (V2G2-908) */
        if ( TRUE == removeSalesTariffs )
        {
          /* reset the ScheduleTuplePtr */ /* PRQA S 0316 1 */ /* MD_Scc_0310_0314_0316_3305 */
          ScheduleTuplePtr = ((Exi_ISO_ED2_DIS_Scheduled_CPDResControlModeType*)BodyPtr->CPDResControlMode)->ScheduleList->ScheduleTuple;
          /* step through the SAScheduleTuples to search for a SalesTariff */
          while ( NULL_PTR != ScheduleTuplePtr )
          {
            /* remove the SalesTariff from the structure  */
            ScheduleTuplePtr->SalesTariffFlag = 0;
            /* get to the next SAScheduleTuple */
            ScheduleTuplePtr = ScheduleTuplePtr->NextScheduleTuplePtr;
          }
        }
      }
  #endif /* SCC_ENABLE_PNC_CHARGING */
      Scc_Set_ISO_Ed2_DIS_ScheduleList(BodyPtr->CPDResControlMode);
    }
  #endif /* SCC_IGNORE_SA_SCHEDULE_LIST */

  #if ( SCC_ENABLE_ONGOING_CALLBACKS ) && ( SCC_ENABLE_ONGOING_CALLBACKS == STD_ON )
  #else
    /* only update the values when EVSEProcessing is set to Finished */
    if ( EXI_ISO_ED2_DIS_PROCESSING_TYPE_FINISHED == BodyPtr->EVSEProcessing )
  #endif
    {

#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
      /* check if DC charging was selected */
      if (Scc_ExiTx_ISO_Ed2_DIS_SelectedEnergyTransferServiceID == Scc_SID_DC_Charging)
      {
        /* check if the EVSEChargeParameter have the correct type */
        if (EXI_ISO_ED2_DIS_DC_CPDRES_ENERGY_TRANSFER_MODE_TYPE != BodyPtr->CPDResEnergyTransferModeElementId)
        {
          Scc_ReportError(Scc_StackError_InvalidRxParameter);
          return (Std_ReturnType)E_NOT_OK;
        }
        /* set the pointer to the DC_EVSEChargeParameterPtr */ /* PRQA S 0316 1 */ /* MD_Scc_0310_0314_0316_3305 */
        DC_EVSEChargeParameterPtr = (P2VAR(Exi_ISO_ED2_DIS_DC_CPDResEnergyTransferModeType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->CPDResEnergyTransferMode;

        /*  DC_EVSEChargeParameterPtr -> EVSEMaximumChargePower */
        Scc_Conv_ISOEd2_2_Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMaximumChargePower, &scc_PhysicalValueTmp);
        Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumChargePower(&scc_PhysicalValueTmp, 1);

        /* DC_EVSEChargeParameterPtr -> EVSEMaximumChargeCurrent */
        Scc_Conv_ISOEd2_2_Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMaximumChargeCurrent, &scc_PhysicalValueTmp);
        Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumChargeCurrent(&scc_PhysicalValueTmp, 1);

        /* DC_EVSEChargeParameterPtr -> EVSEMinimumChargeCurrent */
        Scc_Conv_ISOEd2_2_Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMinimumChargeCurrent, &scc_PhysicalValueTmp);
        Scc_Set_ISO_Ed2_DIS_DC_EVSEMinimumChargeCurrent(&scc_PhysicalValueTmp);

        /* DC_EVSEChargeParameterPtr -> EVSEMaximumVoltage */
        Scc_Conv_ISOEd2_2_Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMaximumVoltage, &scc_PhysicalValueTmp);
        Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumVoltage(&scc_PhysicalValueTmp, 1);

        /* DC_EVSEChargeParameterPtr -> EVSEMinimumVoltage */
        Scc_Conv_ISOEd2_2_Scc_PhysicalValue(DC_EVSEChargeParameterPtr->EVSEMinimumVoltage, &scc_PhysicalValueTmp);
        Scc_Set_ISO_Ed2_DIS_DC_EVSEMinimumVoltage(&scc_PhysicalValueTmp);
      }
#endif /* SCC_CHARGING_DC */



#if ( defined SCC_CHARGING_DC_BPT ) && ( SCC_CHARGING_DC_BPT == STD_ON )
      /* check if DC charging was selected */
      if ( Scc_ExiTx_ISO_Ed2_DIS_SelectedEnergyTransferServiceID == Scc_SID_DC_BPT )
      {
        /* check if the EVSEChargeParameter have the correct type */
        if ( EXI_ISO_ED2_DIS_BPT_DC_CPDRES_ENERGY_TRANSFER_MODE_TYPE != BodyPtr->CPDResEnergyTransferModeElementId )
        {
          Scc_ReportError(Scc_StackError_InvalidRxParameter);
          return (Std_ReturnType)E_NOT_OK;
        }
        /* set the pointer to the BPT_DC_EVSEChargeParameterPtr */ /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
        BPT_DC_EVSEChargeParameterPtr = (P2VAR(Exi_ISO_ED2_DIS_BPT_DC_CPDResEnergyTransferModeType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->CPDResEnergyTransferMode;

        /*  BPT_DC_EVSEChargeParameterPtr -> EVSEMaximumChargePower */
        Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BPT_DC_EVSEChargeParameterPtr->EVSEMaximumChargePower, &scc_PhysicalValueTmp);
        Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumChargePower(&scc_PhysicalValueTmp, 1);

        /* BPT_DC_EVSEChargeParameterPtr -> EVSEMaximumChargeCurrent */
        Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BPT_DC_EVSEChargeParameterPtr->EVSEMaximumChargeCurrent, &scc_PhysicalValueTmp);
        Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumChargeCurrent(&scc_PhysicalValueTmp, 1);

        /* BPT_DC_EVSEChargeParameterPtr -> EVSEMinimumChargeCurrent */
        Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BPT_DC_EVSEChargeParameterPtr->EVSEMinimumChargeCurrent, &scc_PhysicalValueTmp);
        Scc_Set_ISO_Ed2_DIS_DC_EVSEMinimumChargeCurrent(&scc_PhysicalValueTmp);

        /* BPT_DC_EVSEChargeParameterPtr -> EVSEMaximumVoltage */
        Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BPT_DC_EVSEChargeParameterPtr->EVSEMaximumVoltage, &scc_PhysicalValueTmp);
        Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumVoltage(&scc_PhysicalValueTmp, 1);

        /* BPT_DC_EVSEChargeParameterPtr -> EVSEMinimumVoltage */
        Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BPT_DC_EVSEChargeParameterPtr->EVSEMinimumVoltage, &scc_PhysicalValueTmp);
        Scc_Set_ISO_Ed2_DIS_DC_EVSEMinimumVoltage(&scc_PhysicalValueTmp);

        /* BPT_DC_EVSEChargeParameterPtr -> EVSEMaximumDischargePower */
        Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BPT_DC_EVSEChargeParameterPtr->EVSEMaximumDischargePower, &scc_PhysicalValueTmp);
        Scc_Set_ISO_Ed2_DIS_BPT_DC_EVSEMaximumDischargePower(&scc_PhysicalValueTmp);

        /* BPT_DC_EVSEChargeParameterPtr -> EVSEMaximumDischargeCurrent */
        Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BPT_DC_EVSEChargeParameterPtr->EVSEMaximumDischargeCurrent, &scc_PhysicalValueTmp);
        Scc_Set_ISO_Ed2_DIS_BPT_DC_EVSEMaximumDischargeCurrent(&scc_PhysicalValueTmp);
#
        /* BPT_DC_EVSEChargeParameterPtr -> EVSEMinimumDischargeCurrent */
        Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BPT_DC_EVSEChargeParameterPtr->EVSEMinimumDischargeCurrent, &scc_PhysicalValueTmp);
        Scc_Set_ISO_Ed2_DIS_BPT_DC_EVSEMinimumDischargeCurrent(&scc_PhysicalValueTmp);
      }
#endif /* SCC_CHARGING_DC_BPT */


  #if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
      {
        /* check if the EVSEChargeParameter have the correct type */
        /* if ( EXI_ISO_ED2_DIS_DC_EVSECHARGE_PARAMETER_TYPE != BodyPtr->EVSEChargeParameterElementId )
        {
          Scc_ReportError(Scc_StackError_InvalidRxParameter);
          return (Std_ReturnType)E_NOT_OK;
        } */
        /* set the pointer to the DC_EVSEChargeParameterPtr */
        /* DC_EVSEChargeParameterPtr =
          (P2VAR(Exi_ISO_ED2_DIS_DC_EVSEChargeParameterType, AUTOMATIC, SCC_APPL_DATA))BodyPtr->EVSEChargeParameter; */
        /* DC_EVSEChargeParameter -> EVSEMaximumCurrent */
        /* Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumCurrent(DC_EVSEChargeParameterPtr->EVSEMaximumCurrent, 1); */
        /* DC_EVSEChargeParameter -> EVSEMaximumPower */
        /* Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumPower(DC_EVSEChargeParameterPtr->EVSEMaximumPower, 1); */
        /* DC_EVSEChargeParameter -> EVSEMaximumVoltage */
        /* Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumVoltage(DC_EVSEChargeParameterPtr->EVSEMaximumVoltage, 1); */
        /* DC_EVSEChargeParameter -> EVSEMinimumCurrent */
        /* Scc_Set_ISO_Ed2_DIS_DC_EVSEMinimumCurrent(DC_EVSEChargeParameterPtr->EVSEMinimumCurrent); */
        /* DC_EVSEChargeParameter -> EVSEMinimumVoltage */
        /* Scc_Set_ISO_Ed2_DIS_DC_EVSEMinimumVoltage(DC_EVSEChargeParameterPtr->EVSEMinimumVoltage); */
        /* DC_EVSEChargeParameter -> EVSECurrentRegulationTolerance */
        /* Scc_Set_ISO_Ed2_DIS_DC_EVSECurrentRegulationTolerance(DC_EVSEChargeParameterPtr->EVSECurrentRegulationTolerance,
          DC_EVSEChargeParameterPtr->EVSECurrentRegulationToleranceFlag); */
        /* DC_EVSEChargeParameter -> EVSEPeakCurrentRipple */
        /* Scc_Set_ISO_Ed2_DIS_DC_EVSEPeakCurrentRipple(DC_EVSEChargeParameterPtr->EVSEPeakCurrentRipple); */
      }
  #endif /* SCC_CHARGING_DC */
    }

    /* ResponseCode */
    Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);

    retVal = E_OK;
  }

  return retVal; /*lint !e438 */
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_PowerDeliveryRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_PowerDeliveryRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_PowerDeliveryResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_PowerDeliveryResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_POWER_DELIVERY_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* EVSEStatus */
      Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

      /* EVSEProcessing */
      Scc_Set_ISO_Ed2_DIS_EVSEProcessing(BodyPtr->EVSEProcessing);

      /* ResponseCode */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_MeteringReceiptRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_MeteringReceiptRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_MeteringReceiptResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_MeteringReceiptResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_METERING_RECEIPT_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* EVSEStatus */
      Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

      /* ResponseCode */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_SessionStopRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_SessionStopRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_SessionStopResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_SessionStopResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_SESSION_STOP_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 reset the SessionID if the session was terminated */
    if ( SCC_ISO_CHARGING_SESSION_TYPE_TERMINATE == Scc_Exi_ChargingSession )
    {
      /* reset the SessionID */
      Scc_SessionIDNvm[0] = 0x01;
      Scc_SessionIDNvm[1] = 0x00;

      /* set the flag for the NvM to copy the new SessionID */
      (void) NvM_SetRamBlockStatus((NvM_BlockIdType)SCC_SESSION_ID_NVM_BLOCK, TRUE);
    }

    /* #30 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #40 Report the parameter from the response to the application. */

      /* EVSEStatus */
      Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

      /* ResponseCode */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}


#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_CableCheckRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_CableCheckRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_CableCheckResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_CableCheckResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_CABLE_CHECK_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* EVSEStatus */
      Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

      /* EVSEProcessing */
      Scc_Set_ISO_Ed2_DIS_EVSEProcessing(BodyPtr->EVSEProcessing);

      /* ResponseCode */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_PreChargeRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_PreChargeRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  Scc_PhysicalValueType scc_PhysicalValueTmp;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_PreChargeResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_PreChargeResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_PRE_CHARGE_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* EVSEStatus */
      Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

      /* EVSEPresentVoltage */
      Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BodyPtr->EVSEPresentVoltage, &scc_PhysicalValueTmp);
      Scc_Set_ISO_Ed2_DIS_DC_EVSEPresentVoltage(&scc_PhysicalValueTmp);

      /* ResponseCode */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);

      retVal = E_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_CurrentDemandRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_CurrentDemandRes(void) /* PRQA S 2889 */ /* MD_Scc_15.5 */
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  Scc_PhysicalValueType scc_PhysicalValueTmp;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_CurrentDemandResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_CurrentDemandResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

#if ( defined SCC_ENABLE_PNC_CHARGING) && ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  Exi_ISO_ED2_DIS_PnC_CLResIdentificationModeType *resPnC_CLResIdentificationModePtr;
#endif /* SCC_ENABLE_PNC_CHARGING */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_CURRENT_DEMAND_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* #30 Report the parameter from the response to the application. */

    /* EVSEStatus */
    Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

    /* EVSEID */
    Scc_Set_ISO_Ed2_DIS_EVSEID(BodyPtr->EVSEID);


    /* EVSEPresentCurrent */
    Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BodyPtr->EVSEPresentCurrent, &scc_PhysicalValueTmp);
    Scc_Set_ISO_Ed2_DIS_DC_EVSEPresentCurrent(&scc_PhysicalValueTmp);

    /* EVSEPresentVoltage */
    Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BodyPtr->EVSEPresentVoltage, &scc_PhysicalValueTmp);
    Scc_Set_ISO_Ed2_DIS_DC_EVSEPresentVoltage(&scc_PhysicalValueTmp);

    /* EVSEPowerLimitAchieved */
    Scc_Set_ISO_Ed2_DIS_DC_EVSEPowerLimitAchieved(BodyPtr->EVSEPowerLimitAchieved);

    /* EVSECurrentLimitAchieved */
    Scc_Set_ISO_Ed2_DIS_DC_EVSECurrentLimitAchieved(BodyPtr->EVSECurrentLimitAchieved);

    /* EVSEVoltageLimitAchieved */
    Scc_Set_ISO_Ed2_DIS_DC_EVSEVoltageLimitAchieved(BodyPtr->EVSEVoltageLimitAchieved);

    /* EVSEMaximumPower */
    Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BodyPtr->EVSEMaximumChargePower, &scc_PhysicalValueTmp);
    Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumChargePower(&scc_PhysicalValueTmp, BodyPtr->EVSEMaximumChargePowerFlag);

    /* EVSEMaximumCurrent */
    Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BodyPtr->EVSEMaximumChargeCurrent, &scc_PhysicalValueTmp);
    Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumChargeCurrent(&scc_PhysicalValueTmp, BodyPtr->EVSEMaximumChargeCurrentFlag);

    /* EVSEMaximumVoltage */
    Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BodyPtr->EVSEMaximumVoltage, &scc_PhysicalValueTmp);
    Scc_Set_ISO_Ed2_DIS_DC_EVSEMaximumVoltage(&scc_PhysicalValueTmp, BodyPtr->EVSEMaximumVoltageFlag);

    /* ScheduleTupleID */
    if ( 0u != BodyPtr->CLResControlModeFlag )
    { /* PRQA S 0316 3 */ /* MD_Scc_0310_0314_0316_3305 */
      if (   ( BodyPtr->CLResControlModeElementId != EXI_ISO_ED2_DIS_SCHEDULED_CLRES_CONTROL_MODE_TYPE )
        || ((( Exi_ISO_ED2_DIS_Scheduled_CLResControlModeType *)BodyPtr->CLResControlMode)->ScheduleTupleIDFlag != 1u )
        || ((( Exi_ISO_ED2_DIS_Scheduled_CLResControlModeType *)BodyPtr->CLResControlMode)->ScheduleTupleID != Scc_Exi_SAScheduleTupleID))
      {
        /* the received ScheduleTupleID is different from the one which was sent to the EVSE in the PowerDeliveryReq */
        Scc_ReportError(Scc_StackError_InvalidRxParameter);
        return (Std_ReturnType)E_NOT_OK;
      }
    }

    /* MeterInfo */
  /*  Scc_Set_ISO_Ed2_DIS_MeterInfo(BodyPtr->MeterInfo, BodyPtr->MeterInfoFlag); */

  #if ( defined SCC_ENABLE_PNC_CHARGING) && ( SCC_ENABLE_PNC_CHARGING == STD_ON )
    /* PRQA S 0316 1 */ /* MD_Scc_0310_0314_0316_3305 */
    resPnC_CLResIdentificationModePtr = (Exi_ISO_ED2_DIS_PnC_CLResIdentificationModeType *)BodyPtr->CLResIdentificationMode;

    if (SCC_ISO_PAYMENT_OPTION_TYPE_CONTRACT == Scc_Exi_PaymentOption)
    {
      /* check if ReceiptRequired was received with value TRUE */
      if ((1u == BodyPtr->CLResIdentificationModeFlag)
        && (BodyPtr->CLResIdentificationModeElementId == EXI_ISO_ED2_DIS_PN_C_CLRES_IDENTIFICATION_MODE_TYPE)
        && (resPnC_CLResIdentificationModePtr->MeterInfoFlag == TRUE))
      {
        /* copy the meter info element to the temp buf */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
        P2VAR(Exi_ISO_ED2_DIS_MeterInfoType, AUTOMATIC, SCC_VAR_NOINIT) MeterInfoPtr =
          ( P2VAR(Exi_ISO_ED2_DIS_MeterInfoType, AUTOMATIC, SCC_VAR_NOINIT) )&Scc_Exi_TempBuf[0];

        Exi_ISO_ED2_DIS_MeterInfoType *resMeterInfoPtr = resPnC_CLResIdentificationModePtr->MeterInfo;
        /* ReceiptRequired */
        Scc_Set_ISO_PnC_ReceiptRequired(resMeterInfoPtr->ReceiptRequired, 1);

        Scc_Exi_TempBufPos = (uint16)sizeof(Exi_ISO_ED2_DIS_MeterInfoType);
        /* create the MeterID element */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
        MeterInfoPtr->MeterID = (P2VAR(Exi_ISO_ED2_DIS_meterIDType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[Scc_Exi_TempBufPos];
        Scc_Exi_TempBufPos += (uint16)sizeof(Exi_ISO_ED2_DIS_meterIDType);
        /* copy the content */ /* PRQA S 0310,3305,0315 3 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&MeterInfoPtr->MeterID->Buffer[0],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&resMeterInfoPtr->MeterID->Buffer[0],
          resMeterInfoPtr->MeterID->Length);
        MeterInfoPtr->MeterID->Length = resMeterInfoPtr->MeterID->Length;
        /* copy the SigMeterReading */
        if (1u == resMeterInfoPtr->MeterSignatureFlag)
        {
          /* create the SigMeterReading element */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
          MeterInfoPtr->MeterSignature =
            (P2VAR(Exi_ISO_ED2_DIS_meterSignatureType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[Scc_Exi_TempBufPos];
          Scc_Exi_TempBufPos += (uint16)sizeof(Exi_ISO_ED2_DIS_meterSignatureType);
          /* copy the content */ /* PRQA S 0310, 3305, 0315 3 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
          IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&MeterInfoPtr->MeterSignature->Buffer[0],
            (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&resMeterInfoPtr->MeterSignature->Buffer[0],
            resMeterInfoPtr->MeterSignature->Length);
          /* set the length and the flag */
          MeterInfoPtr->MeterSignature->Length = resMeterInfoPtr->MeterSignature->Length;
          MeterInfoPtr->MeterSignatureFlag = 1;
        }
        else
        {
          MeterInfoPtr->MeterSignatureFlag = 0;
        }
        /* copy the TMeter */
        MeterInfoPtr->TMeterFlag = resMeterInfoPtr->TMeterFlag;
        MeterInfoPtr->TMeter = resMeterInfoPtr->TMeter;
        /* copy the MeterReading */
  /*      MeterInfoPtr->MeterReadingFlag = resMeterInfoPtr->MeterReadingFlag;  */
  /*      MeterInfoPtr->MeterReading = resMeterInfoPtr->MeterReading;  */
        /* copy the MeterStatus */
        MeterInfoPtr->MeterStatusFlag = resMeterInfoPtr->MeterStatusFlag;
        MeterInfoPtr->MeterStatus = resMeterInfoPtr->MeterStatus;
      }
    }

  #endif /* SCC_ENABLE_PNC_CHARGING */

    /* ResponseCode */
    Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
    retVal = E_OK;
  }

  return retVal;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_WeldingDetectionRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_WeldingDetectionRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  Scc_PhysicalValueType scc_PhysicalValueTmp;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_WeldingDetectionResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_WeldingDetectionResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_WELDING_DETECTION_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
    }
    else
    {
      /* #30 Report the parameter from the response to the application. */

      /* EVSEStatus */
      Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

      /* EVSEPresentVoltage */
      Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BodyPtr->EVSEPresentVoltage, &scc_PhysicalValueTmp);
      Scc_Set_ISO_Ed2_DIS_DC_EVSEPresentVoltage(&scc_PhysicalValueTmp);

      retVal = E_OK;
    }

    /* ResponseCode */
    Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
  }

  return retVal;
}

#endif /* SCC_CHARGING_DC */


#if ( SCC_CHARGING_AC_BPT == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_AC_BidirectionalControlRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */

SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_AC_BidirectionalControlRes(void) /* PRQA S 2889 */ /* MD_Scc_15.5 */
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  Scc_PhysicalValueType scc_PhysicalValueTmp;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_AC_BidirectionalControlResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_AC_BidirectionalControlResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_AC_BIDIRECTIONAL_CONTROL_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* #30 Report the parameter from the response to the application. */

    /* EVSEStatus */
    Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

    /* EVSEID */
    Scc_Set_ISO_Ed2_DIS_EVSEID(BodyPtr->EVSEID);

    /* ScheduleTupleID */
    if ( Scc_Exi_ControlMode == SCC_ISO_ED2_CONTROL_MODE_TYPE_SCHEDULED )
    {
      if (BodyPtr->CLResControlModeFlag != 0u)
      {
        /* Scheduled_CLResControlModeType contains only ScheduleTupleID which is optional. Hence throw error in case the flag is false */
        /* PRQA S 0316 3 */ /* MD_Scc_0310_0314_0316_3305 */
        if ( ( BodyPtr->CLResControlModeElementId != EXI_ISO_ED2_DIS_SCHEDULED_CLRES_CONTROL_MODE_TYPE )
          || (((Exi_ISO_ED2_DIS_Scheduled_CLResControlModeType*)BodyPtr->CLResControlMode)->ScheduleTupleIDFlag != TRUE)
          || (((Exi_ISO_ED2_DIS_Scheduled_CLResControlModeType*)BodyPtr->CLResControlMode)->ScheduleTupleID != Scc_Exi_SAScheduleTupleID) )
        {
          /* the received ScheduleTupleID is different from the one which was sent to the EVSE in the PowerDeliveryReq */
          Scc_ReportError(Scc_StackError_InvalidRxParameter);
          return (Std_ReturnType)E_NOT_OK;
        }
      }
    }

#if ( defined SCC_ENABLE_PNC_CHARGING) && ( SCC_ENABLE_PNC_CHARGING == STD_ON )
    if ( SCC_ISO_PAYMENT_OPTION_TYPE_CONTRACT == Scc_Exi_PaymentOption )
    {
      /* PRQA S 0316 2 */ /* MD_Scc_Exi_Generic */
      Exi_ISO_ED2_DIS_PnC_CLResIdentificationModeType *resPnC_CLResIdentificationModePtr =
        (P2VAR(Exi_ISO_ED2_DIS_PnC_CLResIdentificationModeType,AUTOMATIC,SCC_VAR_NOINIT))BodyPtr->CLResIdentificationMode;

      /* check if a MeterInfo element is present */ /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
      if ( (BodyPtr->CLResIdentificationModeFlag == TRUE)
        && (BodyPtr->CLResIdentificationModeElementId == EXI_ISO_ED2_DIS_PN_C_CLRES_IDENTIFICATION_MODE_TYPE)
        && (resPnC_CLResIdentificationModePtr->MeterInfoFlag == TRUE) )
      {
        /* copy the meter info element to the temp buf */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
        P2VAR(Exi_ISO_ED2_DIS_MeterInfoType, AUTOMATIC, SCC_VAR_NOINIT) MeterInfoPtr =
          ( P2VAR(Exi_ISO_ED2_DIS_MeterInfoType, AUTOMATIC, SCC_VAR_NOINIT) )&Scc_Exi_TempBuf[0];

        Exi_ISO_ED2_DIS_MeterInfoType *resMeterInfoPtr = resPnC_CLResIdentificationModePtr->MeterInfo;
        /* MeterInfo */
        Scc_Set_ISO_Ed2_DIS_MeterInfo(resPnC_CLResIdentificationModePtr->MeterInfo, resPnC_CLResIdentificationModePtr->MeterInfoFlag);
        /* ReceiptRequired */
        Scc_Set_ISO_PnC_ReceiptRequired(resMeterInfoPtr->ReceiptRequired, 1);

        Scc_Exi_TempBufPos = (uint16)sizeof(Exi_ISO_ED2_DIS_MeterInfoType);
        /* create the MeterID element */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
        MeterInfoPtr->MeterID = (P2VAR(Exi_ISO_ED2_DIS_meterIDType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[Scc_Exi_TempBufPos];
        Scc_Exi_TempBufPos += (uint16)sizeof(Exi_ISO_ED2_DIS_meterIDType);
        /* copy the content */ /* PRQA S 0310, 3305, 0315 3 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&MeterInfoPtr->MeterID->Buffer[0],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&resMeterInfoPtr->MeterID->Buffer[0],
          resMeterInfoPtr->MeterID->Length);
        MeterInfoPtr->MeterID->Length = resMeterInfoPtr->MeterID->Length;
        /* copy the SigMeterReading */
        if ( 1u == resMeterInfoPtr->MeterSignatureFlag )
        {
          /* create the SigMeterReading element */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
          MeterInfoPtr->MeterSignature =
            (P2VAR(Exi_ISO_ED2_DIS_meterSignatureType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[Scc_Exi_TempBufPos];
          Scc_Exi_TempBufPos += (uint16)sizeof(Exi_ISO_ED2_DIS_meterSignatureType);
          /* copy the content */ /* PRQA S 0310, 3305, 0315 3 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
          IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&MeterInfoPtr->MeterSignature->Buffer[0],
            (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&resMeterInfoPtr->MeterSignature->Buffer[0],
            resMeterInfoPtr->MeterSignature->Length);
          /* set the length and the flag */
          MeterInfoPtr->MeterSignature->Length = resMeterInfoPtr->MeterSignature->Length;
          MeterInfoPtr->MeterSignatureFlag = 1;
        }
        else
        {
          MeterInfoPtr->MeterSignatureFlag = 0;
        }
        /* copy the TMeter */
        MeterInfoPtr->TMeterFlag = resMeterInfoPtr->TMeterFlag;
        MeterInfoPtr->TMeter = resMeterInfoPtr->TMeter;
        /* copy the MeterReading */
        MeterInfoPtr->ChargedEnergyReadingWhFlag = resMeterInfoPtr->ChargedEnergyReadingWhFlag;
        MeterInfoPtr->ChargedEnergyReadingWh = resMeterInfoPtr->ChargedEnergyReadingWh;
        /* copy the MeterStatus */
        MeterInfoPtr->MeterStatusFlag = resMeterInfoPtr->MeterStatusFlag;
        MeterInfoPtr->MeterStatus = resMeterInfoPtr->MeterStatus;
        MeterInfoPtr->LaggingEnergyReadingVARhFlag = resMeterInfoPtr->LaggingEnergyReadingVARhFlag;
        MeterInfoPtr->LaggingEnergyReadingVARh = resMeterInfoPtr->LaggingEnergyReadingVARh;
        MeterInfoPtr->LeadingEnergyReadingVARhFlag = resMeterInfoPtr->LeadingEnergyReadingVARhFlag;
        MeterInfoPtr->LeadingEnergyReadingVARh = resMeterInfoPtr->LeadingEnergyReadingVARh;
        MeterInfoPtr->DischargedEnergyReadingWhFlag = resMeterInfoPtr->DischargedEnergyReadingWhFlag;
        MeterInfoPtr->DischargedEnergyReadingWh = resMeterInfoPtr->DischargedEnergyReadingWh;
      }
    }
#endif /* SCC_ENABLE_PNC_CHARGING */

    /* BodyPtr -> EVSETargetFrequency */
    Scc_Conv_ISOEd2_2_Scc_PhysicalValue(BodyPtr->EVSETargetFrequency, &scc_PhysicalValueTmp);
    Scc_Set_ISO_Ed2_DIS_EVSETargetFrequency(&scc_PhysicalValueTmp, BodyPtr->EVSETargetFrequencyFlag);

    if ( BodyPtr->ACBCResControlModeElementId == EXI_ISO_ED2_DIS_DYNAMIC_ACBCRES_CONTROL_MODE_TYPE )
    {
      Exi_ISO_ED2_DIS_Dynamic_ACBCResControlModeType *resDynamic_ACBCResControlModeType = (Exi_ISO_ED2_DIS_Dynamic_ACBCResControlModeType *)BodyPtr->ACBCResControlMode; /* PRQA S 0310, 3305 */ /* MD_Scc_0310_0314_0316_3305 */

      Scc_Conv_ISOEd2_2_Scc_PhysicalValue(resDynamic_ACBCResControlModeType->EVSETargetActivePower, &scc_PhysicalValueTmp);
      Scc_Set_ISO_Ed2_DIS_EVSETargetActivePower(&scc_PhysicalValueTmp, 1);

      Scc_Conv_ISOEd2_2_Scc_PhysicalValue(resDynamic_ACBCResControlModeType->EVSETargetReactivePower, &scc_PhysicalValueTmp);
      Scc_Set_ISO_Ed2_DIS_EVSETargetReactivePower(&scc_PhysicalValueTmp, 1);
    }
    else
    {
      Exi_ISO_ED2_DIS_Scheduled_ACBCResControlModeType *resScheduled_ACBCResControlModeType = (Exi_ISO_ED2_DIS_Scheduled_ACBCResControlModeType *)BodyPtr->ACBCResControlMode; /* PRQA S 0310, 3305 */ /* MD_Scc_0310_0314_0316_3305 */

      Scc_Conv_ISOEd2_2_Scc_PhysicalValue(resScheduled_ACBCResControlModeType->EVSETargetActivePower, &scc_PhysicalValueTmp);
      Scc_Set_ISO_Ed2_DIS_EVSETargetActivePower(&scc_PhysicalValueTmp, resScheduled_ACBCResControlModeType->EVSETargetActivePowerFlag);

      Scc_Conv_ISOEd2_2_Scc_PhysicalValue(resScheduled_ACBCResControlModeType->EVSETargetReactivePower, &scc_PhysicalValueTmp);
      Scc_Set_ISO_Ed2_DIS_EVSETargetReactivePower(&scc_PhysicalValueTmp, resScheduled_ACBCResControlModeType->EVSETargetReactivePowerFlag);
    }

    /* ResponseCode */
    Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);

    retVal = E_OK;
  }

  return retVal;
} /* PRQA S 6050 */ /* MD_MSR_STCAL */
#endif /* SCC_CHARGING_AC_BPT */

#if ( SCC_CHARGING_DC_BPT == STD_ON )
/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_DIS_DC_BidirectionalControlRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_DIS_DC_BidirectionalControlRes(void) /* PRQA S 2889 */ /* MD_Scc_15.5 */
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  /* PRQA S 0316 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2CONST(Exi_ISO_ED2_DIS_DC_BidirectionalControlResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
    (P2CONST(Exi_ISO_ED2_DIS_DC_BidirectionalControlResType, AUTOMATIC, SCC_VAR_NOINIT))Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the message is not the one that was expected. */
  if ( EXI_ISO_ED2_DIS_DC_BIDIRECTIONAL_CONTROL_RES_TYPE != Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
    Scc_ReportError(Scc_StackError_InvalidRxMessage);
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusFailed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */
  }
  else
  {
#if ( defined SCC_DEM_UNEXPECTED_MSG )
    /* report status to DEM */
    Scc_DemReportErrorStatusPassed(SCC_DEM_UNEXPECTED_MSG);
#endif /* SCC_DEM_UNEXPECTED_MSG */

    /* #20 Check if the response is negative. */
    if ( EXI_ISO_ED2_DIS_RESPONSE_CODE_TYPE_FAILED <= BodyPtr->ResponseCode )
    {
      /* report the negative response code as stack error */
      Scc_ReportError(Scc_StackError_NegativeResponseCode);
      /* provide the response code to the application */
      Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);
      return (Std_ReturnType)E_NOT_OK;
    }

    /* #30 Report the parameter from the response to the application. */

    /* EVSEStatus */
    Scc_Set_ISO_Ed2_DIS_EVSEStatus(BodyPtr->EVSEStatus, BodyPtr->EVSEStatusFlag);

    /* EVSEID */
    Scc_Set_ISO_Ed2_DIS_EVSEID(BodyPtr->EVSEID);

    /* ScheduleTupleID */
    if ( Scc_Exi_ControlMode == SCC_ISO_ED2_CONTROL_MODE_TYPE_SCHEDULED )
    {
      if (BodyPtr->CLResControlModeFlag != 0u)
      {
        /* Scheduled_CLResControlModeType contains only ScheduleTupleID which is optional. Hence throw error in case the flag is false */
        /* PRQA S 0316 3 */ /* MD_Scc_0310_0314_0316_3305 */
        if ( ( BodyPtr->CLResControlModeElementId != EXI_ISO_ED2_DIS_SCHEDULED_CLRES_CONTROL_MODE_TYPE )
          || (((Exi_ISO_ED2_DIS_Scheduled_CLResControlModeType*)BodyPtr->CLResControlMode)->ScheduleTupleIDFlag != TRUE)
          || (((Exi_ISO_ED2_DIS_Scheduled_CLResControlModeType*)BodyPtr->CLResControlMode)->ScheduleTupleID != Scc_Exi_SAScheduleTupleID) )
        {
          /* the received ScheduleTupleID is different from the one which was sent to the EVSE in the PowerDeliveryReq */
          Scc_ReportError(Scc_StackError_InvalidRxParameter);
          return (Std_ReturnType)E_NOT_OK;
        }
      }
    }

#if ( defined SCC_ENABLE_PNC_CHARGING) && ( SCC_ENABLE_PNC_CHARGING == STD_ON )
    if ( SCC_ISO_PAYMENT_OPTION_TYPE_CONTRACT == Scc_Exi_PaymentOption )
    { /* PRQA S 0316 1 */ /* MD_Scc_0310_0314_0316_3305 */
      Exi_ISO_ED2_DIS_PnC_CLResIdentificationModeType *resPnC_CLResIdentificationModePtr = (Exi_ISO_ED2_DIS_PnC_CLResIdentificationModeType * )BodyPtr->CLResIdentificationMode;

      /* check if a MeterInfo element is present */
      if ( (BodyPtr->CLResIdentificationModeFlag == TRUE)
        && (BodyPtr->CLResIdentificationModeElementId == EXI_ISO_ED2_DIS_PN_C_CLRES_IDENTIFICATION_MODE_TYPE)
        && (resPnC_CLResIdentificationModePtr->MeterInfoFlag == TRUE) )
      {
        /* copy the meter info element to the temp buf */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
        P2VAR(Exi_ISO_ED2_DIS_MeterInfoType, AUTOMATIC, SCC_VAR_NOINIT) MeterInfoPtr =
          ( P2VAR(Exi_ISO_ED2_DIS_MeterInfoType, AUTOMATIC, SCC_VAR_NOINIT) )&Scc_Exi_TempBuf[0];

        Exi_ISO_ED2_DIS_MeterInfoType *resMeterInfoPtr = resPnC_CLResIdentificationModePtr->MeterInfo;
        /* MeterInfo */
        Scc_Set_ISO_Ed2_DIS_MeterInfo(resPnC_CLResIdentificationModePtr->MeterInfo, resPnC_CLResIdentificationModePtr->MeterInfoFlag);
        /* ReceiptRequired */
        Scc_Set_ISO_PnC_ReceiptRequired(resMeterInfoPtr->ReceiptRequired, 1);

        Scc_Exi_TempBufPos = (uint16)sizeof(Exi_ISO_ED2_DIS_MeterInfoType);
        /* create the MeterID element */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
        MeterInfoPtr->MeterID = (P2VAR(Exi_ISO_ED2_DIS_meterIDType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[Scc_Exi_TempBufPos];
        Scc_Exi_TempBufPos += (uint16)sizeof(Exi_ISO_ED2_DIS_meterIDType);
        /* copy the content */ /* PRQA S 0310, 3305, 0315 3 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
        IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&MeterInfoPtr->MeterID->Buffer[0],
          (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&resMeterInfoPtr->MeterID->Buffer[0],
          resMeterInfoPtr->MeterID->Length);
        MeterInfoPtr->MeterID->Length = resMeterInfoPtr->MeterID->Length;
        /* copy the SigMeterReading */
        if ( 1u == resMeterInfoPtr->MeterSignatureFlag )
        {
          /* create the SigMeterReading element */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
          MeterInfoPtr->MeterSignature =
            (P2VAR(Exi_ISO_ED2_DIS_meterSignatureType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_TempBuf[Scc_Exi_TempBufPos];
          Scc_Exi_TempBufPos += (uint16)sizeof(Exi_ISO_ED2_DIS_meterSignatureType);
          /* copy the content */ /* PRQA S 0310, 3305, 0315 3 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
          IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&MeterInfoPtr->MeterSignature->Buffer[0],
            (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&resMeterInfoPtr->MeterSignature->Buffer[0],
            resMeterInfoPtr->MeterSignature->Length);
          /* set the length and the flag */
          MeterInfoPtr->MeterSignature->Length = resMeterInfoPtr->MeterSignature->Length;
          MeterInfoPtr->MeterSignatureFlag = 1;
        }
        else
        {
          MeterInfoPtr->MeterSignatureFlag = 0;
        }
        /* copy the TMeter */
        MeterInfoPtr->TMeterFlag = resMeterInfoPtr->TMeterFlag;
        MeterInfoPtr->TMeter = resMeterInfoPtr->TMeter;
        /* copy the MeterReading */
        MeterInfoPtr->ChargedEnergyReadingWhFlag = resMeterInfoPtr->ChargedEnergyReadingWhFlag;
        MeterInfoPtr->ChargedEnergyReadingWh = resMeterInfoPtr->ChargedEnergyReadingWh;
        /* copy the MeterStatus */
        MeterInfoPtr->MeterStatusFlag = resMeterInfoPtr->MeterStatusFlag;
        MeterInfoPtr->MeterStatus = resMeterInfoPtr->MeterStatus;
        MeterInfoPtr->LaggingEnergyReadingVARhFlag = resMeterInfoPtr->LaggingEnergyReadingVARhFlag;
        MeterInfoPtr->LaggingEnergyReadingVARh = resMeterInfoPtr->LaggingEnergyReadingVARh;
        MeterInfoPtr->LeadingEnergyReadingVARhFlag = resMeterInfoPtr->LeadingEnergyReadingVARhFlag;
        MeterInfoPtr->LeadingEnergyReadingVARh = resMeterInfoPtr->LeadingEnergyReadingVARh;
        MeterInfoPtr->DischargedEnergyReadingWhFlag = resMeterInfoPtr->DischargedEnergyReadingWhFlag;
        MeterInfoPtr->DischargedEnergyReadingWh = resMeterInfoPtr->DischargedEnergyReadingWh;
      }
    }
#endif /* SCC_ENABLE_PNC_CHARGING */

    /* EVSEPresentCurrent to EVSEMaximumDischargeCurrent parameters are set here */
    Scc_Set_ISO_Ed2_DIS_DC_BidirectionalControlRes(BodyPtr);

    /* ResponseCode */
    Scc_Set_ISO_Ed2_DIS_ResponseCode(BodyPtr->ResponseCode);

    retVal = E_OK;
  }

  return retVal;
} /* PRQA S 6050 */ /* MD_MSR_STCAL */
#endif /* SCC_CHARGING_DC_BPT */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )

/**********************************************************************************************************************
 *  Scc_ExiRx_ISO_Ed2_XmlSecDereference
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_ExiRx_ISO_Ed2_XmlSecDereference(P2VAR(uint8*, AUTOMATIC, SCC_VAR_NOINIT) ExiStructPtr,
  P2VAR(Exi_RootElementIdType, AUTOMATIC, SCC_VAR_NOINIT) ExiRootElementId, P2VAR(Exi_NamespaceIdType, AUTOMATIC, SCC_VAR_NOINIT) ExiNamespaceId,
  P2CONST(uint8, AUTOMATIC, SCC_VAR_NOINIT) URIPtr, uint16 URILength)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType RetVal = (Std_ReturnType)E_NOT_OK;

  P2CONST(uint8, AUTOMATIC, SCC_VAR_INIT) currentURIPtr = &URIPtr[0];
  uint16 currentURILength = URILength;

  /* ----- Implementation ----------------------------------------------- */
  if ( (uint8)'#' == currentURIPtr[0] )
  {
    currentURIPtr = &currentURIPtr[1];
    currentURILength--;
  }

  /* #10 Choose response type and set Struct-Pointer, RootElementId and NameSpaceId */
  switch ( Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElementElementId )
  {
  /* #20 current message in the rx buffer is a ChargeParameterDiscoveryRes */
  case EXI_ISO_ED2_DIS_CHARGE_PARAMETER_DISCOVERY_RES_TYPE:
    {
      P2CONST(Exi_ISO_ED2_DIS_ChargeParameterDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
        (P2CONST(Exi_ISO_ED2_DIS_ChargeParameterDiscoveryResType, AUTOMATIC, SCC_VAR_NOINIT)) /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */
        Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;
      P2VAR(Exi_ISO_ED2_DIS_ScheduleTupleType, AUTOMATIC, SCC_APPL_DATA) ScheduleTuplePtr;

      if ((BodyPtr->CPDResControlModeFlag == TRUE) && (BodyPtr->CPDResControlModeElementId == EXI_ISO_ED2_DIS_SCHEDULED_CPDRES_CONTROL_MODE_TYPE))
      {
        /* get the pointer to the ScheduleTuples */
        ScheduleTuplePtr = ((Exi_ISO_ED2_DIS_Scheduled_CPDResControlModeType *)BodyPtr->CPDResControlMode)->ScheduleList->ScheduleTuple; /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */

        /* check the ID of the SalesTariff of the ScheduleTuples */
        while (NULL_PTR != ScheduleTuplePtr)
        {
          /* check if a SalesTariff exists */
          if (1u == ScheduleTuplePtr->SalesTariffFlag)
          {
            /* check if the lengths of the URI and the ID match */
            if (currentURILength == ScheduleTuplePtr->SalesTariff->Id->Length)
            {
              /* check if the strings match */
              if (IPBASE_CMP_EQUAL == IpBase_StrCmpLen(
                currentURIPtr, &ScheduleTuplePtr->SalesTariff->Id->Buffer[0], currentURILength))
              {
                RetVal = (Std_ReturnType)E_OK;

                /* set the ExiStruct pointer and the RootElementId for the pointer */
                *ExiStructPtr = (uint8*)ScheduleTuplePtr->SalesTariff; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
                *ExiRootElementId = (Exi_RootElementIdType)EXI_ISO_ED2_DIS_SALES_TARIFF_TYPE;
                *ExiNamespaceId = (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_ED2_DIS_TYPE;

                /* Remove the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
                if (Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff1 == TRUE )
                {
                  Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff1 = FALSE;
                }
                else if (Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff2 == TRUE )
                {
                  Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff2 = FALSE;
                }
                else if (Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff3 == TRUE )
                {
                  Scc_Exi_RefInSigInfo.Flag.RISI_SalesTariff3 = FALSE;
                }
                else
                {
                  /* only 3 SalesTariffs are possible due to the Schema */
                  RetVal = E_NOT_OK;
                }

                break;
              }
            }
          }
          /* switch to the next ScheduleTuple */
          ScheduleTuplePtr = ScheduleTuplePtr->NextScheduleTuplePtr;
        }
      }
    }
    break;

  /* #30 current message in the rx buffer is a CertificateInstallationRes */
  case EXI_ISO_ED2_DIS_CERTIFICATE_INSTALLATION_RES_TYPE:
    {
      P2CONST(Exi_ISO_ED2_DIS_CertificateInstallationResType, AUTOMATIC, SCC_VAR_NOINIT) BodyPtr =
        (P2CONST(Exi_ISO_ED2_DIS_CertificateInstallationResType, AUTOMATIC, SCC_VAR_NOINIT)) /* PRQA S 0316 */ /* MD_Scc_0310_0314_0316_3305 */
        Scc_ExiRx_ISO_Ed2_DIS_MsgPtr->Body->BodyElement;

      /* check if the ContractCertificateChain shall be dereferenced */
      /* check if the length of the URI matches */
      if (   ( 1u == BodyPtr->ContractCertificateChain->IdFlag )
          && ( currentURILength == BodyPtr->ContractCertificateChain->Id->Length ))
      {
        /* check if the URIs are the same */
        if ( IPBASE_CMP_EQUAL == IpBase_StrCmpLen(
          currentURIPtr, &BodyPtr->ContractCertificateChain->Id->Buffer[0], currentURILength) )
        {
          /* set the ExiStruct pointer and the RootElementId for the pointer */
          *ExiStructPtr = (uint8*)BodyPtr->ContractCertificateChain; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
          *ExiRootElementId = (Exi_RootElementIdType)EXI_ISO_ED2_DIS_CONTRACT_CERTIFICATE_CHAIN_TYPE;
          *ExiNamespaceId = (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_ED2_DIS_TYPE;

          /* Remove the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
          if (Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureCertChain == TRUE )
          {
            Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureCertChain = FALSE;
            RetVal = (Std_ReturnType)E_OK;
          }
          else
          {
            RetVal = E_NOT_OK;
          }

          break; /* PRQA S 3333 */ /* MD_Scc_3333 */
        }
      }

      /* check if the ContractCertificateEncryptedPrivateKey shall be dereferenced */
      /* check if the length of the URI matches */
      if ( currentURILength == BodyPtr->ContractCertificateEncryptedPrivateKey->Id->Length )
      {
        /* check if the URIs are the same */
        if ( IPBASE_CMP_EQUAL == IpBase_StrCmpLen(
          currentURIPtr, &BodyPtr->ContractCertificateEncryptedPrivateKey->Id->Buffer[0], currentURILength) )
        {
          /* set the ExiStruct pointer and the RootElementId for the pointer */
          *ExiStructPtr = (uint8*)BodyPtr->ContractCertificateEncryptedPrivateKey; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
          *ExiRootElementId = (Exi_RootElementIdType)EXI_ISO_ED2_DIS_CONTRACT_CERTIFICATE_ENCRYPTED_PRIVATE_KEY_TYPE;
          *ExiNamespaceId = (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_ED2_DIS_TYPE;

          /* Remove the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
          if (Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureEncryptedPrivateKey == TRUE)
          {
            Scc_Exi_RefInSigInfo.Flag.RISI_ContractSignatureEncryptedPrivateKey = FALSE;
            RetVal = (Std_ReturnType)E_OK;
          }
          else
          {
            RetVal = E_NOT_OK;
          }

          break; /* PRQA S 3333 */ /* MD_Scc_3333 */
        }
      }

      /* check if the DHPublicKey shall be dereferenced */
      /* check if the length of the URI matches */
      if ( currentURILength == BodyPtr->DHPublicKey->Id->Length )
      {
        /* check if the URIs are the same */
        if ( IPBASE_CMP_EQUAL == IpBase_StrCmpLen(currentURIPtr, &BodyPtr->DHPublicKey->Id->Buffer[0], currentURILength) )
        {
          /* set the ExiStruct pointer and the RootElementId for the pointer */
          *ExiStructPtr = (uint8*)BodyPtr->DHPublicKey; /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
          *ExiRootElementId = (Exi_RootElementIdType)EXI_ISO_ED2_DIS_DHPUBLIC_KEY_TYPE;
          *ExiNamespaceId = (Exi_NamespaceIdType)EXI_SCHEMA_SET_ISO_ED2_DIS_TYPE;

          /* Remove the flag in Scc_Exi_RefInSigInfo to check later if every reference from the SignedInfo was found */
          if (Scc_Exi_RefInSigInfo.Flag.RISI_DHpublickey == TRUE)
          {
            Scc_Exi_RefInSigInfo.Flag.RISI_DHpublickey = FALSE;
            RetVal = (Std_ReturnType)E_OK;
          }
          else
          {
            RetVal = E_NOT_OK;
          }

          break; /* PRQA S 3333 */ /* MD_Scc_3333 */
        }
      }
    }
    break;

    /* unknown BodyElementId */
  default:
    RetVal = (Std_ReturnType)E_NOT_OK;
    break;
  }

  return RetVal;
} /* PRQA S 6010,6030,6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STMIF */

#endif /* SCC_ENABLE_PNC_CHARGING */

#define SCC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
/* PRQA L:NEST_STRUCTS */
/* PRQA L:RETURN_PATHS */

#endif /* SCC_SCHEMA_ISO_ED2 */

/**********************************************************************************************************************
 *  END OF FILE: Scc_ExiRx_ISO_Ed2_DIS.c
 *********************************************************************************************************************/
