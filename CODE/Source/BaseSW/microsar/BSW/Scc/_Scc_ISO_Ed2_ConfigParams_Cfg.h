#if !defined(SCC_WPT_CONFIGPARAMS_CFG_H)
#define SCC_WPT_CONFIGPARAMS_CFG_H

/***********************************************************************************************************************
 *  Scc_ConfigParams_Cfg.h
 **********************************************************************************************************************/

 /**********************************************************************************************************************
*  FILE REQUIRES USER MODIFICATIONS
*  Template Scope: whole file
*  -------------------------------------------------------------------------------------------------------------------
*  This file includes template code that must be completed and/or adapted during BSW integration.
*  The template code is incomplete and only intended for providing a signature and an empty implementation. It is
*  neither intended nor qualified for use in series production without applying suitable quality measures. The template
*  code must be completed as described in the instructions given within this file and/or in the Technical Reference.
*  The completed implementation must be tested with diligent care and must comply with all quality requirements which
*  are necessary according to the state of the art before its use.
*********************************************************************************************************************/

/* -------------------------- ISO ED2 CD2 Timer Config --------------------------------*/

#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_SequencePerformanceTimeout                   8000U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_CommunicationSetupTimeout                    4000U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_AuthorizationMessageTimeout                  400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_CableCheckMessageTimeout                     1000U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_CertificateInstallationMessageTimeout        1000U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_CertificateUpdateMessageTimeout              400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_ChargeParameterDiscoveryMessageTimeout       400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_ChargingStatusMessageTimeout                 400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_CurrentDemandMessageTimeout                  400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_MeteringReceiptMessageTimeout                400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_AC_BidirectionalControlTimeout               400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_DC_BidirectionalControlTimeout               400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_PaymentDetailsMessageTimeout                 1000U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_PaymentServiceSelectionMessageTimeout        400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_PowerDeliveryMessageTimeout                  1000U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_PreChargeMessageTimeout                      400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_ServiceDetailMessageTimeout                  1000U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_ServiceDiscoveryMessageTimeout               400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_SessionSetupMessageTimeout                   400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_SessionStopMessageTimeout                    400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_WeldingDetectionMessageTimeout               400U

#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_FinePositioningSetupMessageTimeout           400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_FinePositioningMessageTimeout                400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_PairingMessageTimeout                        400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_InitialAlignmentCheckMessageTimeout          400U
#define Scc_ConfigValue_Timer_ISO_Ed2_DIS_PowerDemandMessageTimeout                    400U

#define Scc_ConfigValue_StateM_FinePositioningOngoingTimeout                           5000U
#define Scc_ConfigValue_StateM_PairingOngoingTimeout                                   5000U
#define Scc_ConfigValue_StateM_InitialAlignmentCheckOngoingTimeout                     5000U

#define Scc_ConfigValue_StateM_PowerDemandNextReqDelay                                 0U
#define Scc_ConfigValue_StateM_FinePositioningNextReqDelay                             0U
#define Scc_ConfigValue_StateM_PairingNextReqDelay                                     0U
#define Scc_ConfigValue_StateM_InitialAlignmentCheckNextReqDelay                       0U
#define Scc_ConfigValue_StateM_AC_BidirectionalControlNextReqDelay                     0U
#define Scc_ConfigValue_StateM_DC_BidirectionalControlNextReqDelay                     0U

#endif /* SCC_WPT_CONFIGPARAMS_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Scc_Cfg.h
 *********************************************************************************************************************/
