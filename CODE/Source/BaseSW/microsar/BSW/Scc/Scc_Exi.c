/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc_Exi.c
 *        \brief  Smart Charging Communication Source Code File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *********************************************************************************************************************/

#define SCC_EXI_SOURCE

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
 /* PRQA S 0777 EOF */ /* MD_MSR_Rule5.1 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Scc_Exi.h"
#include "Scc.h"
#include "Scc_Cfg.h"
#include "Scc_Lcfg.h"
#include "Scc_Priv.h"
#include "Scc_Interface_Cfg.h"
#include "Scc_ConfigParams_Cfg.h"

#if ( SCC_DEV_ERROR_DETECT == STD_ON )
#include "Det.h"
#endif /* SCC_DEV_ERROR_DETECT */
#include "EthIf.h"
#include "Exi.h"
#include "IpBase.h"
#include "TcpIp.h"
#if ( SCC_ENABLE_TLS == STD_ON )
#include "Tls.h"
#endif /* SCC_ENABLE_TLS */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined (SCC_LOCAL)
# define SCC_LOCAL static
#endif

/**********************************************************************************************************************
 *  LOCAL / GLOBAL DATA
 *********************************************************************************************************************/

/* 8bit variables */
#define SCC_START_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
VAR(Scc_Exi_TempBufType, SCC_VAR_NOINIT)   Scc_Exi_TempBufUnion; /* PRQA S 0759 */ /* MD_MSR_Union */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ((( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))) /* PRQA S 3332 */ /* MD_Scc_3332 */
VAR(uint8, SCC_VAR_NOINIT) Scc_Exi_SAScheduleTupleID;
#endif /* SCC_SCHEMA_ISO, SCC_SCHEMA_ISO_ED2 */

#define SCC_STOP_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* 16bit variables */
#define SCC_START_SEC_VAR_NOINIT_16BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
VAR(uint16, SCC_VAR_NOINIT) Scc_Exi_TempBufPos;
#endif /* SCC_ENABLE_PNC_CHARGING */

#define SCC_STOP_SEC_VAR_NOINIT_16BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* other variables */
#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* Scc_Exi_StructBufUnion can be an uint8 or uint32 value */
VAR(Scc_Exi_StructBufType, SCC_VAR_NOINIT) Scc_Exi_StructBufUnion; /* PRQA S 0759 */ /* MD_MSR_Union */

VAR(Exi_EncodeWorkspaceType, SCC_VAR_NOINIT) Scc_Exi_EncWs;
VAR(Exi_DecodeWorkspaceType, SCC_VAR_NOINIT) Scc_Exi_DecWs;
#if ( defined SCC_ENABLE_PNC_CHARGING) && ( SCC_ENABLE_PNC_CHARGING == STD_ON )
VAR(XmlSecurity_SigGenWorkspaceType, SCC_VAR_NOINIT) Scc_Exi_XmlSecSigGenWs;
VAR(XmlSecurity_SigValWorkspaceType, SCC_VAR_NOINIT) Scc_Exi_XmlSecSigValWs;
VAR(Scc_RefInSigInfoType, SCC_VAR_ZERO_INIT) Scc_Exi_RefInSigInfo; /* PRQA S 0759 */ /* MD_MSR_Union */
#endif /* SCC_ENABLE_PNC_CHARGING */

#if ((( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))) /* PRQA S 3332 */ /* MD_Scc_3332 */
VAR(Scc_ISO_PaymentOptionType, SCC_VAR_NOINIT)   Scc_Exi_PaymentOption;
VAR(Scc_ISO_ChargingSessionType, SCC_VAR_NOINIT) Scc_Exi_ChargingSession;
#endif /* SCC_SCHEMA_ISO, SCC_SCHEMA_ISO_ED2 */

#if ( SCC_SCHEMA_ISO_ED2 != 0 )
/* Global switch between scheduled and dynamic mode */
VAR(Scc_ISO_Ed2_ControlModeType, SCC_VAR_NOINIT) Scc_Exi_ControlMode;
#endif /* SCC_SCHEMA_ISO_ED2 */


Std_ReturnType (*Scc_ExiTx_Xyz_EncodeMessageFctPtr)(
    P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr
);
Std_ReturnType (*Scc_ExiRx_Xyz_DecodeMessageFctPtr)(void);

#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/
/* PRQA S 3453 MACROS_FUNCTION_LIKE */ /* MD_MSR_FctLikeMacro */
/* PRQA L:MACROS_FUNCTION_LIKE */

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_Exi_Init
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */

FUNC(void, SCC_CODE) Scc_Exi_Init(void)
{
  /* ----- Implementation ----------------------------------------------- */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  /* #10 reset the position of the Scc_Exi_TempBuf */
  Scc_Exi_TempBufPos = 0;
#endif /* SCC_ENABLE_PNC_CHARGING */

  return;
}

/**********************************************************************************************************************
 *  Scc_Exi_StreamRequest
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */

FUNC(Std_ReturnType, SCC_CODE) Scc_Exi_StreamRequest(
    P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr
)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType            retVal = E_NOT_OK;
  Exi_ReturnType            exiRetVal;
  uint16                    remLen;
  uint16                    packetLen;
  Scc_TxRxBufferPointerType v2GRequest;
  Scc_PbufType              v2GRequestPbuf;

  /* ----- Implementation ----------------------------------------------- */

  v2GRequestPbuf.payload = PBufPtr->payload;
  v2GRequestPbuf.len = PBufPtr->len;
  v2GRequestPbuf.totLen = PBufPtr->totLen;
  v2GRequest.PbufPtr = &v2GRequestPbuf;

  PBufPtr->payload = BufPtr;
  PBufPtr->len = *BufLengthPtr;
  PBufPtr->totLen = *BufLengthPtr;

  /* #10 initialize the exi encode workspace */
  if ( (Std_ReturnType)E_OK != Exi_InitEncodeWorkspace((P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_EncWs,  /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[0],
    (P2VAR(IpBase_PbufType, AUTOMATIC, SCC_APPL_DATA))PBufPtr, 0,
    Scc_Exi_EncWs.EncWs.StartWriteAtBytePos + Scc_Exi_EncWs.EncWs.CurrentStreamSegmentLen, FALSE) )
  {
    /* report the error to the application */
    Scc_ReportError(Scc_StackError_Exi);
  }
  else
  {
    /* #20 encode the message and check if the encoding failed */
    exiRetVal = Exi_Encode((P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_EncWs);
    if ( ( EXI_E_OK != exiRetVal ) && ( EXI_E_EOS != exiRetVal ))
    {
      /* report the error */
      Scc_ReportError(Scc_StackError_Exi);
    }

    /* #30 finalize the exi stream and check if an error occurred */
    else if ( EXI_E_OK != Exi_FinalizeExiStream((P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_EncWs) )
    {
      /* report the error to the application */
      Scc_ReportError(Scc_StackError_Exi);
    }
    else
    {
      /* #40 copy the streaming status from the EXI workspace */
      if ( FALSE != Scc_Exi_EncWs.EncWs.StreamComplete )
      {
        Scc_TxStreamingActive = FALSE;
      }

      /* #50 set the length of the tx data */
      Scc_TxDataSent += Scc_Exi_EncWs.EncWs.CurrentStreamSegmentLen;
      packetLen = (uint16)Scc_Exi_EncWs.EncWs.CurrentStreamSegmentLen;
      remLen = packetLen;

      *BufLengthPtr = remLen;
      v2GRequestPbuf.len += packetLen; /* PRQA S 2983 */ /* MD_Scc_QAC_Mistaken  */

      /* provide sent data to application */
      {
        /* create and set the tx buffer pointer */
        v2GRequest.FirstPart = FALSE;
        v2GRequest.StreamComplete = Scc_Exi_EncWs.EncWs.StreamComplete;
        /* provide the buffer to the application */
        Scc_Set_Core_V2GRequest(&v2GRequest);
      }

      retVal = E_OK;
    }
  }

  return retVal; /*lint !e438 */
} /*lint !e550 */

/**********************************************************************************************************************
 *  Scc_Exi_EncodeSupportedAppProtocolReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_Exi_EncodeSupportedAppProtocolReq(
    P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr
)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType                                                        retVal = E_NOT_OK;
  boolean                                                               InitialAppProtocol = TRUE;
  uint8_least                                                           Counter;
  uint16                                                                lBufIdx = 0;
  Scc_SupportedSAPSchemasType                                           SupportedSAPSchemas;
  P2VAR(Exi_SAP_supportedAppProtocolReqType, AUTOMATIC, SCC_VAR_NOINIT) ExiSapMsgPtr;
  P2VAR(Exi_SAP_AppProtocolType, AUTOMATIC, SCC_VAR_NOINIT)             AppProtocolPtr;
  Scc_ForceSAPSchemasType                                               ForceSAPSchema = Scc_ForceSAPSchemas_None;
  uint8_least                                                           SchemaPosition = 0;
  Scc_SDPSecurityType                                                   securityFlag = Scc_Security;

  /* ----- Implementation ----------------------------------------------- */
  /* Since no data has been sent, this means that the initializations has to be done */
  if(0u == Scc_TxDataSent)
  {
    /* #10 initialize the exi workspace and check if it failed */
    if ( (Std_ReturnType)E_OK == Scc_Exi_InitEncodingWorkspace(PBufPtr) )
    {
      /* #20 Get the Forced Schema from the application and set the schema and the priority. */
      Scc_Get_Core_ForceSAPSchema( &ForceSAPSchema );

      if ( securityFlag == Scc_SDPSecurity_Tls )
      {
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) && ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
        if ( (ForceSAPSchema == Scc_ForceSAPSchemas_DIN_ISO)
          || (ForceSAPSchema == Scc_ForceSAPSchemas_ISO_DIN) )
        {
          ForceSAPSchema = Scc_ForceSAPSchemas_ISO;
        }
        else
#endif /* ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) && ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) */
#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
        {
          if ( ForceSAPSchema == Scc_ForceSAPSchemas_DIN )
          {
            ForceSAPSchema = Scc_ForceSAPSchemas_InvalidValue;
          }
        }
#endif /* SCC_SCHEMA_DIN */
      }

      switch ( ForceSAPSchema )
      {
      case Scc_ForceSAPSchemas_None:
  #if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
        if (securityFlag == Scc_SDPSecurity_None)
        {
          SupportedSAPSchemas.Schema[SchemaPosition] = Scc_SAPSchemaIDs_DIN;
          SupportedSAPSchemas.Priority[SchemaPosition] = SCC_SCHEMA_DIN;
          SchemaPosition++;
        }
  #endif /* SCC_SCHEMA_DIN */
  #if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
        SupportedSAPSchemas.Schema[SchemaPosition]   = Scc_SAPSchemaIDs_ISO;
        SupportedSAPSchemas.Priority[SchemaPosition] = SCC_SCHEMA_ISO;
        SchemaPosition++;
  #endif /* SCC_SCHEMA_ISO */
  #if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) /* PRQA S 3332 */ /* MD_Scc_3332 */
        SupportedSAPSchemas.Schema[SchemaPosition]   = Scc_SAPSchemaIDs_ISO_Ed2_DIS;
        SupportedSAPSchemas.Priority[SchemaPosition] = SCC_SCHEMA_ISO_ED2;
        SchemaPosition++;
  #endif /* SCC_SCHEMA_ISO_ED2 */
        SupportedSAPSchemas.Schema[SchemaPosition]   = Scc_SAPSchemaIDs_None;
        retVal = E_OK;
        break;

  #if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
      case Scc_ForceSAPSchemas_DIN: /* PRQA S 2880 */ /* MD_Scc_2742_2880_2995 */
        /* only offer DIN schema in SAP */
        SupportedSAPSchemas.Schema[0]   = Scc_SAPSchemaIDs_DIN;
        SupportedSAPSchemas.Priority[0] = 1U;
        SupportedSAPSchemas.Schema[1]   = Scc_SAPSchemaIDs_None;
        retVal = E_OK;
        break;
  #endif /* SCC_SCHEMA_DIN */

  #if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
      case Scc_ForceSAPSchemas_ISO: /* PRQA S 2880 */ /* MD_Scc_2742_2880_2995 */
        /* only offer ISO Ed1 schema in SAP */
        SupportedSAPSchemas.Schema[0]   = Scc_SAPSchemaIDs_ISO;
        SupportedSAPSchemas.Priority[0] = 1U;
        SupportedSAPSchemas.Schema[1]   = Scc_SAPSchemaIDs_None;
        retVal = E_OK;
        break;
  #endif /* SCC_SCHEMA_ISO */

  #if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) /* PRQA S 3332 */ /* MD_Scc_3332 */
      case Scc_ForceSAPSchemas_ISO_Ed2: /* PRQA S 2880 */ /* MD_Scc_2742_2880_2995 */
        /* only offer ISO Ed2 schema in SAP */
        SupportedSAPSchemas.Schema[0]   = Scc_SAPSchemaIDs_ISO_Ed2_DIS;
        SupportedSAPSchemas.Priority[0] = 1U;
        SupportedSAPSchemas.Schema[1]   = Scc_SAPSchemaIDs_None;
        retVal = E_OK;
        break;
  #endif /* SCC_SCHEMA_ISO_ED2 */

#if (( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) && ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ))
      case Scc_ForceSAPSchemas_DIN_ISO:
        /* only offer DIN schema in SAP */
        SupportedSAPSchemas.Schema[0] = Scc_SAPSchemaIDs_DIN;
        SupportedSAPSchemas.Priority[0] = 1U;
        SupportedSAPSchemas.Schema[1] = Scc_SAPSchemaIDs_ISO;
        SupportedSAPSchemas.Priority[1] = 2U;
        SupportedSAPSchemas.Schema[2] = Scc_SAPSchemaIDs_None;
        retVal = E_OK;
        break;
#endif /* SCC_SCHEMA_DIN, SCC_SCHEMA_ISO */

#if (( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) && ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ))
      case Scc_ForceSAPSchemas_ISO_DIN:
        /* only offer DIN schema in SAP */
        SupportedSAPSchemas.Schema[0] = Scc_SAPSchemaIDs_DIN;
        SupportedSAPSchemas.Priority[0] = 2U;
        SupportedSAPSchemas.Schema[1] = Scc_SAPSchemaIDs_ISO;
        SupportedSAPSchemas.Priority[1] = 1U;
        SupportedSAPSchemas.Schema[2] = Scc_SAPSchemaIDs_None;
        retVal = E_OK;
        break;
#endif /* SCC_SCHEMA_DIN, SCC_SCHEMA_ISO */
      default:
        Scc_ReportError(Scc_StackError_InvalidTxParameter);
        SupportedSAPSchemas.Schema[0]   = Scc_SAPSchemaIDs_None;
        SupportedSAPSchemas.Priority[0] = 0U;
        retVal = E_NOT_OK;
        break;
      }

      if ( retVal == E_OK ) /* PRQA S 2880 */ /* MD_Scc_2742_2880_2995 */
      {
        /* set the supportedAppProtocolReq as root element */
        Scc_Exi_EncWs.InputData.RootElementId = EXI_SAP_SUPPORTED_APP_PROTOCOL_REQ_TYPE;

        /* #30 Add the schema DIN/ISO_Ed1/ISO_Ed2 to the SAP request */

        /* set the buffer for the SAP message */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
        ExiSapMsgPtr = (P2VAR(Exi_SAP_supportedAppProtocolReqType, AUTOMATIC, SCC_VAR_NOINIT))&Scc_Exi_StructBuf[lBufIdx];
        lBufIdx += (uint16)sizeof(Exi_SAP_supportedAppProtocolReqType);

        /* set the first app protocol buffer pointer */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
        ExiSapMsgPtr->AppProtocol = (P2VAR(Exi_SAP_AppProtocolType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[lBufIdx];

        /* get the starting pointer */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
        AppProtocolPtr = (P2VAR(Exi_SAP_AppProtocolType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[lBufIdx];
        lBufIdx += (uint16)sizeof(Exi_SAP_AppProtocolType);

        for ( Counter = 0; Counter < (uint8)Scc_SAPSchemaIDs_NumOfSupportedSchemas; Counter++ )
        {
          /* check if no more schemas shall be sent */
          if ( Scc_SAPSchemaIDs_None == SupportedSAPSchemas.Schema[Counter] )
          {
            break;
          }
          /* check if this is not the first app protocol */
          if ( FALSE == InitialAppProtocol )
          {
            /* reference the buffer for the next AppProtocol element */ /* PRQA S 0310,3305 1 */ /* MD_Scc_0310_0314_0316_3305 */
            AppProtocolPtr->NextAppProtocolPtr = (P2VAR(Exi_SAP_AppProtocolType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[lBufIdx];
            lBufIdx += (uint16)sizeof(Exi_SAP_AppProtocolType);
            /* set the AppProtocolPtr to the second element */
            AppProtocolPtr = AppProtocolPtr->NextAppProtocolPtr;
          }
          /* use the existing buffer */
          else
          {
             InitialAppProtocol = FALSE; /* PRQA S 2983 */ /* MD_Scc_VariantDependent */
          }
          /* set the buffer for the protocol namespace */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
          AppProtocolPtr->ProtocolNamespace = (P2VAR(Exi_SAP_protocolNamespaceType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[lBufIdx];
          lBufIdx += (uint16)sizeof(Exi_SAP_protocolNamespaceType); /* PRQA S 2983 */ /* MD_Scc_VariantDependent */

          switch ( SupportedSAPSchemas.Schema[Counter] )
          {
    #if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
          case Scc_SAPSchemaIDs_ISO:
            /* copy the namespace */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
            IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&AppProtocolPtr->ProtocolNamespace->Buffer[0],
              (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))SCC_EXI_SAP_ISO_FDIS_PROTOCOL_NAMESPACE,
              SCC_EXI_SAP_ISO_FDIS_PROTOCOL_NAMESPACE_LEN);
            /* copy the length of the namespace */
            AppProtocolPtr->ProtocolNamespace->Length = SCC_EXI_SAP_ISO_FDIS_PROTOCOL_NAMESPACE_LEN;

            /* VersionNumberMajor */
            AppProtocolPtr->VersionNumberMajor = (uint32)SCC_EXI_SAP_ISO_FDIS_VERSION_NUMBER_MAJOR;
            /* VersionNumberMinor */
            AppProtocolPtr->VersionNumberMinor = (uint32)SCC_EXI_SAP_ISO_FDIS_VERSION_NUMBER_MINOR;
            /* SchemaID */
            AppProtocolPtr->SchemaID = (uint8)Scc_SAPSchemaIDs_ISO;
            /* Priority */
            AppProtocolPtr->Priority = SupportedSAPSchemas.Priority[Counter];
            break;
    #endif /* SCC_SCHEMA_ISO */

    #if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) /* PRQA S 3332 */ /* MD_Scc_3332 */
          case Scc_SAPSchemaIDs_ISO_Ed2_DIS:
            /* copy the namespace */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
            IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&AppProtocolPtr->ProtocolNamespace->Buffer[0],
              (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))SCC_EXI_SAP_ISO_ED2_DIS_PROTOCOL_NAMESPACE,
              SCC_EXI_SAP_ISO_ED2_DIS_PROTOCOL_NAMESPACE_LEN);
            /* copy the length of the namespace */
            AppProtocolPtr->ProtocolNamespace->Length = SCC_EXI_SAP_ISO_ED2_DIS_PROTOCOL_NAMESPACE_LEN;

            /* VersionNumberMajor */
            AppProtocolPtr->VersionNumberMajor = (uint32)SCC_EXI_SAP_ISO_ED2_DIS_VERSION_NUMBER_MAJOR;
            /* VersionNumberMinor */
            AppProtocolPtr->VersionNumberMinor = (uint32)SCC_EXI_SAP_ISO_ED2_DIS_VERSION_NUMBER_MINOR;
            /* SchemaID */
            AppProtocolPtr->SchemaID = (uint8)Scc_SAPSchemaIDs_ISO_Ed2_DIS;
            /* Priority */
            AppProtocolPtr->Priority = SupportedSAPSchemas.Priority[Counter];
            break;
    #endif /* SCC_SCHEMA_ISO_ED2 */

    #if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
          case Scc_SAPSchemaIDs_DIN:
            if (securityFlag == Scc_SDPSecurity_None)
            {
              /* copy the namespace */ /* PRQA S 0310,3305,0315 2 */ /* MD_Scc_0310_0314_0316_3305, MD_Scc_0310_0314_0316_3305, MD_MSR_VStdLibCopy */
              IpBase_Copy((P2VAR(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))&AppProtocolPtr->ProtocolNamespace->Buffer[0],
                (P2CONST(IpBase_CopyDataType, AUTOMATIC, SCC_APPL_DATA))SCC_EXI_SAP_DIN_PROTOCOL_NAMESPACE,
                SCC_EXI_SAP_DIN_PROTOCOL_NAMESPACE_LEN);
              /* copy the length of the namespace */
              AppProtocolPtr->ProtocolNamespace->Length = SCC_EXI_SAP_DIN_PROTOCOL_NAMESPACE_LEN;

              /* VersionNumberMajor */
              AppProtocolPtr->VersionNumberMajor = (uint32)SCC_EXI_SAP_DIN_VERSION_NUMBER_MAJOR;
              /* VersionNumberMinor */
              AppProtocolPtr->VersionNumberMinor = (uint32)SCC_EXI_SAP_DIN_VERSION_NUMBER_MINOR;
              /* SchemaID */
              AppProtocolPtr->SchemaID = (uint8)Scc_SAPSchemaIDs_DIN;
              /* Priority */
              AppProtocolPtr->Priority = SupportedSAPSchemas.Priority[Counter];
            }
            else
            {
              /* report the error to the application */
              Scc_ReportError(Scc_StackError_InvalidTxParameter);
              retVal = E_NOT_OK;
            }
            break;
    #endif /* SCC_SCHEMA_DIN */

          default:
            /* report the error to the application */
            Scc_ReportError(Scc_StackError_InvalidTxParameter);
            retVal = E_NOT_OK;
            break;
          }
        }

        if ( retVal == E_OK )
        {
          /* set the last NextAppProtocolPtr to NULL */
          AppProtocolPtr->NextAppProtocolPtr = (P2VAR(Exi_SAP_AppProtocolType, AUTOMATIC, SCC_APPL_DATA))NULL_PTR;

          /* start the timeout counter */
          Scc_TimeoutCnt = (uint16)Scc_ConfigValue_Timer_General_SupportedAppProtocolMessageTimeout;

          /* #40 Finalize and send the SAP message */
          if(E_OK != Scc_Exi_EncodeExiStream((uint16)SCC_V2GTP_HDR_PAYLOAD_TYPE_SAP, PBufPtr))
          {
            retVal = E_NOT_OK;
          }
          else
          {
            *BufLengthPtr = (uint16)Scc_TxDataSent;
            PBufPtr->totLen = *BufLengthPtr;
            PBufPtr->len    = *BufLengthPtr;

            /* provide sent data to application */
            {
              /* create and set the tx buffer pointer */
              Scc_TxRxBufferPointerType V2GRequest;
              V2GRequest.PbufPtr = PBufPtr;
              V2GRequest.FirstPart = TRUE;
              V2GRequest.StreamComplete = Scc_Exi_EncWs.EncWs.StreamComplete;
              /* provide the buffer to the application */
              Scc_Set_Core_V2GRequest(&V2GRequest);
            } /*lint !e550 */
          }
        }
      }
    }
  }
  else
  {
    /* #50 Exi encode a part of the SAP request and send this part in Tcp segment */
    retVal = Scc_Exi_SendSubsequentMessage(BufPtr, BufLengthPtr, (uint16)SCC_V2GTP_HDR_PAYLOAD_TYPE_SAP, PBufPtr);
  }

  return retVal; /*lint !e438 */
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_Exi_DecodeSupportedAppProtocolRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Exi_DecodeSupportedAppProtocolRes(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* set the exi message pointer */ /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
  P2VAR(Exi_SAP_supportedAppProtocolResType, AUTOMATIC, SCC_VAR_NOINIT) ExiSapMsgPtr =
    (P2VAR(Exi_SAP_supportedAppProtocolResType, AUTOMATIC, SCC_VAR_NOINIT)) &Scc_Exi_StructBuf[0];

  /* initialize the exi workspace and check if the workspace initialization failed */
  if ( (Std_ReturnType)E_OK == Scc_Exi_InitDecodingWorkspace() )
  {
    /* set the decode information */
    Scc_Exi_DecWs.OutputData.SchemaSetId = EXI_SCHEMA_SET_SAP_TYPE;
    /* #10 Exi decode message */
    if ( (Std_ReturnType)E_OK != Exi_Decode(&Scc_Exi_DecWs) )
    {
      /* report the error */
      Scc_ReportError(Scc_StackError_Exi);
  #if ( defined SCC_DEM_EXI )
      /* report status to DEM */
      Scc_DemReportErrorStatusFailed(SCC_DEM_EXI);
  #endif /* SCC_DEM_EXI */
    }
    else
    {
#if ( defined SCC_DEM_EXI )
      /* report status to DEM */
      Scc_DemReportErrorStatusPassed(SCC_DEM_EXI);
#endif /* SCC_DEM_EXI */

      /* #20 check if the response is negative */
      if ( EXI_SAP_RESPONSE_CODE_TYPE_FAILED_NO_NEGOTIATION <= ExiSapMsgPtr->ResponseCode )
      {
        /* report the negative response code */
        Scc_ReportError(Scc_StackError_NegativeResponseCode);
        /* provide the response code to the application */
        Scc_Set_Core_SAPResponseCode(ExiSapMsgPtr->ResponseCode);
    #if ( defined SCC_DEM_SAP_NO_NEGOTIATION )
        /* set DEM event */
        Scc_DemReportErrorStatusFailed(SCC_DEM_SAP_NO_NEGOTIATION);
    #endif /* SCC_DEM_SAP_NO_NEGOTIATION */
      }
      else
      {
#if ( defined SCC_DEM_SAP_NO_NEGOTIATION )
        /* set DEM event */
        Scc_DemReportErrorStatusPassed(SCC_DEM_SAP_NO_NEGOTIATION);
#endif /* SCC_DEM_SAP_NO_NEGOTIATION */

        /* #30 evaluate schema and set FunctionPointer */

        /* SchemaID */
        if ( 0u == ExiSapMsgPtr->SchemaIDFlag )
        {
          Scc_ReportError(Scc_StackError_InvalidRxParameter);
        }
        else
        {
          Scc_SAPSchemaIDType sapSchemaID = (Scc_SAPSchemaIDType)ExiSapMsgPtr->SchemaID; /* PRQA S 4342 */ /* MD_Scc_Exi_Generic */
          Scc_Set_Core_SAPSchemaID(sapSchemaID);
          /* set the function pointers depending on the selected Schema */
          switch ( sapSchemaID )
          {
        #if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
          case Scc_SAPSchemaIDs_ISO:
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
            Scc_ExiRx_ISO_InitXmlSecurityWorkspace();
#endif /* SCC_ENABLE_PNC_CHARGING */
            Scc_ExiTx_Xyz_EncodeMessageFctPtr = Scc_ExiTx_ISO_EncodeMessage;
            Scc_ExiRx_Xyz_DecodeMessageFctPtr = Scc_ExiRx_ISO_DecodeMessage;
            break;
        #endif /* SCC_SCHEMA_ISO */

        #if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) /* PRQA S 3332 */ /* MD_Scc_3332 */
          case Scc_SAPSchemaIDs_ISO_Ed2_DIS:
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
            Scc_ExiRx_ISO_Ed2_InitXmlSecurityWorkspace();
#endif /* SCC_ENABLE_PNC_CHARGING */
            Scc_ExiTx_Xyz_EncodeMessageFctPtr = Scc_ExiTx_ISO_Ed2_EncodeMessage;
            Scc_ExiRx_Xyz_DecodeMessageFctPtr = Scc_ExiRx_ISO_Ed2_DecodeMessage;
            break;
        #endif /* SCC_SCHEMA_ISO_ED2 */

        #if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
          case Scc_SAPSchemaIDs_DIN:
            Scc_ExiTx_Xyz_EncodeMessageFctPtr = Scc_ExiTx_DIN_EncodeMessage;
            Scc_ExiRx_Xyz_DecodeMessageFctPtr = Scc_ExiRx_DIN_DecodeMessage;
            /* reset the SessionID */
            Scc_SessionIDNvm[0] = 0x01;
            Scc_SessionIDNvm[1] = 0x00;
            break;
        #endif /* SCC_SCHEMA_DIN */

          default:
            Scc_ReportError(Scc_StackError_InvalidRxParameter);
            break;
          }

          /* ResponseCode */
          Scc_Set_Core_SAPResponseCode(ExiSapMsgPtr->ResponseCode);

          /* set the new state and report the success */
          Scc_State = Scc_State_SAPComplete;
          Scc_ReportSuccessAndStatus(Scc_MsgStatus_SupportedAppProtocol_OK);
        }
      }
    }
  }

  return;
} /* PRQA S 6050, 6080   */ /* MD_MSR_STCAL, MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_Exi_InitEncodingWorkspace
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_Exi_InitEncodingWorkspace(
  P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
  uint16 OutBufOfs;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the buffer is big enough to store the (first part of the) EXI stream. */
  if(PBufPtr->totLen > SCC_V2GTP_HDR_LEN)
  {
    /* Start writing the EXI stream after V2GTP header */
    OutBufOfs = SCC_V2GTP_HDR_LEN;
  }
  else
  {
    /* EXI stream cannot be encoded, initialize the EXI workspace to only calculate the stream length to put it into the V2GTP header */
    /* Buffer is not big enough to trigger an Exi write. */
    /* Exi init required at least 8 bytes of header buffer and 1 byte of Exi buffer. */
    OutBufOfs = 0;
  }

  /* #20 initialize the exi encode workspace */
  if ( (Std_ReturnType)E_OK != Exi_InitEncodeWorkspace((P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_EncWs,  /* PRQA S 0310,3305 2 */ /* MD_Scc_0310_0314_0316_3305 */
    (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[0],
    (P2VAR(IpBase_PbufType, AUTOMATIC, SCC_APPL_DATA))PBufPtr, OutBufOfs, 0, TRUE) )
  {
    /* report the error to the application */
    Scc_ReportError(Scc_StackError_Exi);
  }
  else
  {
    retVal = E_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_Exi_InitDecodingWorkspace
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_Exi_InitDecodingWorkspace(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 initialize the decoding workspace */
  if ( (Std_ReturnType)E_OK != Exi_InitDecodeWorkspace((P2VAR(Exi_DecodeWorkspaceType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_DecWs,
    (P2VAR(IpBase_PbufType, AUTOMATIC, SCC_APPL_DATA))&Scc_ExiStreamRxPBuf[0], (P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_StructBuf[0],
    (uint16)SCC_EXI_STRUCT_BUF_LEN, SCC_V2GTP_HDR_LEN) )
  {
    /* report the error to the application */
    Scc_ReportError(Scc_StackError_Exi);
  }
  else
  {
    retVal = E_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_Exi_SendSubsequentMessage
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_Exi_SendSubsequentMessage(
    P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) BufPtr,
    P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) BufLengthPtr,
    uint16 PayloadType,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal;
  uint16 LengthRemaining;
  uint16 LengthSent = (uint16)Scc_TxDataSent;
  P2VAR(uint8, AUTOMATIC, SCC_VAR_NOINIT) currentBufPtr = BufPtr;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 If only the header has been sent, then reset the Exi variables so that Exi encoding can start from the first byte. */
  if(Scc_TxDataSent <= SCC_V2GTP_HDR_LEN)
  {
    Scc_Exi_EncWs.EncWs.StartWriteAtBytePos     = 0;
    Scc_Exi_EncWs.EncWs.CurrentStreamSegmentLen = 0;
  }

  /* #20 Check if the header was sent. If not, copy header and then call function to work with the EXI message body. */
  if(SCC_V2GTP_HDR_LEN > Scc_TxDataSent)
  {
    /* Copy rest of the header */
    Scc_Exi_CopyPendingHeader(PayloadType, PBufPtr);
    /* Calculate number of bytes sent in this loop */
    LengthSent = (uint16)Scc_TxDataSent - LengthSent;
    /* Calculate number of bytes remaining in the TcpIp buffer */
    LengthRemaining = *BufLengthPtr - LengthSent;

    /* If there is length remaining to be filled then call function to get the Exi stream
    after recalculating the length and pointer values */
    if(LengthRemaining > 0u)
    {
      *BufLengthPtr = LengthRemaining;
      currentBufPtr = &currentBufPtr[LengthSent];
      retVal = Scc_Exi_StreamRequest( currentBufPtr, BufLengthPtr, PBufPtr);
      *BufLengthPtr = *BufLengthPtr + LengthSent;
    }
    else
    {
      *BufLengthPtr = LengthSent;
      PBufPtr->totLen = *BufLengthPtr;
      PBufPtr->len    = *BufLengthPtr;

      /* provide sent data to application */
      {
        /* create and set the tx buffer pointer */
        Scc_TxRxBufferPointerType V2GRequest;
        V2GRequest.PbufPtr = PBufPtr;
        V2GRequest.FirstPart = FALSE;
        V2GRequest.StreamComplete = Scc_Exi_EncWs.EncWs.StreamComplete;
        /* provide the buffer to the application */
        Scc_Set_Core_V2GRequest(&V2GRequest);
      } /*lint !e550 */

      retVal = E_OK;
    }
  }
  else
  {
    retVal = Scc_Exi_StreamRequest( currentBufPtr, BufLengthPtr, PBufPtr);
  }

  return retVal; /*lint !e438 */
}

/**********************************************************************************************************************
 *  Scc_Exi_CopyPendingHeader
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Exi_CopyPendingHeader(uint16 PayloadType,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr)
{
  /* ----- Implementation ----------------------------------------------- */
  Scc_Exi_WriteHeader(PayloadType, PBufPtr);

  /* #10 If the output buffer is capable of holding all 8 bytes of the header then set length to 8. */
  if((Scc_TxDataSent + PBufPtr->totLen) >= SCC_V2GTP_HDR_LEN)
  {
    /* set the length of the tx data */
    Scc_TxDataSent = SCC_V2GTP_HDR_LEN;
  }
  else
  {
    /* set the length of the tx data */
    Scc_TxDataSent += PBufPtr->totLen;
  }
} /* PRQA S 6010 */ /* MD_MSR_STPTH */

/**********************************************************************************************************************
 *  Scc_Exi_WriteHeader
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_Exi_WriteHeader(uint16 PayloadType,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8       Buf[SCC_V2GTP_HDR_LEN];

  /* ----- Implementation ----------------------------------------------- */
  /* #10 set the V2GTP header (8 byte) with version info (2 byte), payload type (2 byte) and payload length (2 byte). */

  /* set the version info (2 byte) */
  Buf[Scc_V2GTPOffsets_Version]         = (uint8)SCC_V2G_PROT_VER;
  Buf[Scc_V2GTPOffsets_InverseVersion]  = (uint8)((~SCC_V2G_PROT_VER) & 0xFFu);
  /* set the payload type (2 byte) */
  Buf[Scc_V2GTPOffsets_PayloadType]     = (uint8)(( PayloadType & 0xFF00u ) >> 8u );
  Buf[Scc_V2GTPOffsets_PayloadType+1u]   = (uint8) ( PayloadType & 0x00FFu );
  /* set the payload length (4 byte) */
  Buf[Scc_V2GTPOffsets_PayloadLength]   =
    (uint8)(( Scc_Exi_EncWs.EncWs.TotalStreamLength & 0xFF000000u ) >> 24u );
  Buf[Scc_V2GTPOffsets_PayloadLength+1u] =
    (uint8)(( Scc_Exi_EncWs.EncWs.TotalStreamLength & 0x00FF0000u ) >> 16u );
  Buf[Scc_V2GTPOffsets_PayloadLength+2u] =
    (uint8)(( Scc_Exi_EncWs.EncWs.TotalStreamLength & 0x0000FF00u ) >> 8u );
  Buf[Scc_V2GTPOffsets_PayloadLength+3u] =
    (uint8) ( Scc_Exi_EncWs.EncWs.TotalStreamLength & 0x000000FFu );

  /* #20 copy the V2GTP header to the PBuf */
  if ( Scc_TxDataSent < SCC_V2GTP_HDR_LEN )
  {

    if ( (Std_ReturnType)E_OK != IpBase_CopyString2PbufAt(( P2CONST(uint8, AUTOMATIC, SCC_APPL_DATA) )&Buf[Scc_TxDataSent], (uint16)( SCC_V2GTP_HDR_LEN - Scc_TxDataSent ), /* PRQA S 0310,3305 */ /* MD_Scc_0310_0314_0316_3305 */
      ( P2VAR(IpBase_PbufType, AUTOMATIC, SCC_APPL_DATA) )PBufPtr, 0) )
    {
#if(SCC_DEV_ERROR_REPORT == STD_ON)
      ( void )Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_EXI_WRITE_HEADER, SCC_DET_IP_BASE);
#endif /* SCC_DEV_ERROR_REPORT */
    }
  }
  else
  {
#if(SCC_DEV_ERROR_REPORT == STD_ON)
    ( void )Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_EXI_WRITE_HEADER, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
  }
}

/**********************************************************************************************************************
 *  Scc_Exi_EncodeExiStream
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(Std_ReturnType, SCC_CODE) Scc_Exi_EncodeExiStream(uint16 PayloadType,
    P2VAR(Scc_PbufType, AUTOMATIC, SCC_APPL_DATA) PBufPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Exi encode the message */
  if ( EXI_E_OK != Exi_Encode((P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_EncWs) )
  {
    /* report the error */
    Scc_ReportError(Scc_StackError_Exi);
  }

  /* #20 finalize the exi stream */
  else if ( EXI_E_OK != Exi_FinalizeExiStream((P2VAR(Exi_EncodeWorkspaceType, AUTOMATIC, SCC_APPL_DATA))&Scc_Exi_EncWs) )
  {
    /* report the error to the application */
    Scc_ReportError(Scc_StackError_Exi);
  }
  else
  {
    /* #30 copy the streaming status from the EXI workspace */
    if ( FALSE == Scc_Exi_EncWs.EncWs.StreamComplete )
    {
      Scc_TxStreamingActive = TRUE;
    }

    /* #40 write V2GTP header to PBuf */
    Scc_Exi_WriteHeader(PayloadType, PBufPtr);

    if(SCC_V2GTP_HDR_LEN < PBufPtr->totLen)
    {
      /* set the length of the tx data */
      Scc_TxDataSent = Scc_Exi_EncWs.EncWs.CurrentStreamSegmentLen + SCC_V2GTP_HDR_LEN;
    }
    else
    {
      /* set the length of the tx data */
      Scc_TxDataSent = PBufPtr->totLen;
    }

    retVal = E_OK;
  }

  return retVal;
} /* PRQA S 6010 */ /* MD_MSR_STPTH */

#define SCC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
/* PRQA L:NEST_STRUCTS */
/* PRQA L:RETURN_PATHS */

/**********************************************************************************************************************
 *  END OF FILE: Scc_ExiTx_ISO.c
 *********************************************************************************************************************/
