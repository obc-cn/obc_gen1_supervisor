/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc_StateM_Vector.c
 *        \brief  Smart Charging Communication Source Code File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *********************************************************************************************************************/

#define SCC_STATEM_VECTOR_SOURCE

/**********************************************************************************************************************
   LOCAL MISRA / PCLINT JUSTIFICATION
 **********************************************************************************************************************/
   /* PRQA S 0777 EOF */ /* MD_MSR_Rule5.1 */
   /* PRQA S 0779 EOF */ /* MD_MSR_Rule5.2 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Scc_Cfg.h"

#if ( SCC_ENABLE_STATE_MACHINE == STD_ON )

#include "Scc_StateM_Vector.h"

#include "Scc.h"
#include "Scc_Lcfg.h"
#include "Scc_Priv.h"
#include "Scc_Interface_Cfg.h"
#include "Scc_ConfigParams_Cfg.h"

#if ( SCC_DEV_ERROR_DETECT == STD_ON )
#include "Det.h"
#endif /* SCC_DEV_ERROR_DETECT */
#include "IpBase.h"
#include "NvM.h"

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/


 /**********************************************************************************************************************
  *  LOCAL CONSTANT MACROS
  *********************************************************************************************************************/
#if !defined (SCC_LOCAL)
# define SCC_LOCAL static
#endif

/* PRQA S 3458 MACROS_BRACES */ /* MD_Scc_20.4 */
/* PRQA S 3453 MACROS_FUNCTION_LIKE */ /* MD_MSR_FctLikeMacro */

#define Scc_StateM_MsgStateFct(MsgStateNew) \
  { Scc_StateM_MsgState = (MsgStateNew); Scc_Set_StateM_StateMachineMessageState(MsgStateNew); }

/* PRQA L:MACROS_BRACES */
/* PRQA L:MACROS_FUNCTION_LIKE */
/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

typedef struct
{
  uint16                                              ChargeServiceID;
  boolean                                             ChargeServiceFree;
  Scc_StateM_PaymentPrioritizationType                EVSEOfferedPaymentOptions;
  Scc_StateM_EVSEProcessingType                       EVSEProcessing;
  uint16                                              ProvServiceID;
  boolean                                             ReceiptRequired;
  Scc_SAPSchemaIDType                                 SAPSchemaID;
#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
  P2CONST(Exi_ISO_ServiceParameterListType, AUTOMATIC, SCC_VAR_NOINIT) ServiceParameterListPtr;
#endif /* SCC_SCHEMA_ISO */
#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
  P2CONST(Exi_ISO_ED2_DIS_ServiceParameterListType, AUTOMATIC, SCC_VAR_NOINIT) ServiceParameterListPtr_ISO_Ed2_DIS;
  uint16                                              VasIdFlags;
  uint16                                              RxEnergyTransferServiceIDFlags;
#endif /* SCC_SCHEMA_ISO_ED2 */
} Scc_StateM_RxEVSEType;

typedef struct
{
  Scc_StateM_ChargeStatusType               ChargeStatus;
  Scc_StateM_EnergyTransferModeType         EnergyTransferMode;
  Scc_StateM_PaymentOptionType              SelectedPaymentOption;
  uint16                                    ReqServiceID;
#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
  Exi_ISO_SelectedServiceListType           SelectedServiceList;
  Exi_ISO_SelectedServiceType               SelectedServices[3];
#endif /* SCC_SCHEMA_ISO */
#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
  Exi_ISO_ED2_DIS_SelectedServiceType       SelectedEnergyTransferService;
  Exi_ISO_ED2_DIS_SelectedServiceListType   SelectedVasList;
  Exi_ISO_ED2_DIS_SelectedServiceType       ED2_SelectedServices;
  Exi_ISO_ED2_DIS_processingType            EVProcessing;
  uint16                                    TxEnergyTransferServiceIDFlags;
#endif /* SCC_SCHEMA_ISO_ED2 */
} Scc_StateM_TxEVSEType;

typedef struct
{
  uint16 ReconnectRetryCnt;
  uint16 GeneralDelayCnt;
  uint16 CommunicationSetupTimerCnt;
  uint16 ReadyToChargeTimerCnt;
  boolean ReadyToChargeTimerStart;
  uint16 OngoingTimerCnt;
  uint16 IPAddressWaitTimerCnt;
  uint16 NvmReadTimerCnt;
  uint16 SequencePerformanceTimerCnt;
} Scc_StateM_CounterType;

typedef struct
{
  Scc_MsgTrigType     MsgTrig;
  boolean             CyclicMsgTrig;
  Scc_SDPSecurityType ReqSDPSecurity;
} Scc_StateM_TxCoreType;

typedef struct
{
  Scc_MsgStatusType   MsgStatus;
  boolean             CyclicMsgRcvd;
  Scc_StackErrorType  StackError;
  Scc_SDPSecurityType SupSDPSecurity;
} Scc_StateM_RxCoreType;

typedef struct
{
  Scc_StateM_ChargingControlType   ChargingControl;
  uint32                           EVTimeStamp;
  Scc_StateM_FunctionControlType   FunctionControl;
#if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
  boolean                          FlowControl;
#endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
  Scc_StateM_ControlPilotStateType ControlPilotState;
  Scc_StateM_PWMStateType          PWMState;
  boolean                          SendChargingProfile;
#if ( (( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0u )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )) )
  boolean                          InternetAvailable;
  boolean                          ReqInetDetails;
  boolean                          InetDetailsRequested;
#endif /* SCC_SCHEMA_ISO || SCC_SCHEMA_ISO_ED2 */
#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )
  boolean                          WPTChargingAvailable;
  boolean                          ReqWPTChargingDetails;
  boolean                          WPTChargingDetailsRequested;
  boolean                          ACChargingAvailable;
  boolean                          ReqACChargingDetails;
  boolean                          ACChargingDetailsRequested;
  boolean                          ReqDCChargingDetails;
  boolean                          DCChargingDetailsRequested;
  boolean                          ReqBPTACChargingDetails;
  boolean                          BPTACChargingDetailsRequested;
  boolean                          ReqBPTDCChargingDetails;
  boolean                          BPTDCChargingDetailsRequested;
  boolean                          ReqWPTFinePositioningDetails;
  boolean                          WPTFinePositioningDetailsRequested;
  boolean                          ReqWPTPairingDetails;
  boolean                          WPTPairingDetailsRequested;
  boolean                          ReqWPTInitialAlignmentCheckDetails;
  boolean                          WPTInitialAlignmentCheckDetailsRequested;
  uint8                            RemainingContractCertificateChains;
#endif /* SCC_SCHEMA_ISO_ED2 */
#if ( ( defined SCC_ENABLE_PNC_CHARGING ) && ( SCC_ENABLE_PNC_CHARGING == STD_ON ) )
  Scc_CertificateStatusType          ContrCertStatus;
  boolean                          ReqCertDetails;
  boolean                          CertDetailsRequested;
  boolean                          ReqCertInstall;
  boolean                          ReqCertUpdate;
  uint8                            SelectedCertChainIndex;
#endif
} Scc_StateM_LocalType;

/**********************************************************************************************************************
 *  LOCAL DATA
 *********************************************************************************************************************/
 /* 8bit variables */
#define SCC_START_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

SCC_LOCAL VAR(boolean, SCC_VAR_ZERO_INIT) Scc_StateM_IPAssigned = FALSE;
#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
SCC_LOCAL VAR(boolean, SCC_VAR_ZERO_INIT) Scc_StateM_TrcvLinkState = FALSE;
#endif /* SCC_ENABLE_SLAC_HANDLING */


#define SCC_STOP_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* other variables */
#define SCC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

SCC_LOCAL VAR(Scc_StateM_StateType, SCC_VAR_ZERO_INIT)   Scc_StateM_State = Scc_SMS_Uninitialized;
SCC_LOCAL VAR(Scc_StateM_CounterType, SCC_VAR_ZERO_INIT) Scc_StateM_Counter =
  { 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u };

#define SCC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* other variables */
#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

SCC_LOCAL VAR(Scc_StateM_MsgStateType, SCC_VAR_NOINIT) Scc_StateM_MsgState;

SCC_LOCAL VAR(Scc_StateM_TxEVSEType, SCC_VAR_NOINIT) Scc_StateM_TxEVSE;
SCC_LOCAL VAR(Scc_StateM_RxEVSEType, SCC_VAR_NOINIT) Scc_StateM_RxEVSE;
SCC_LOCAL VAR(Scc_StateM_TxCoreType, SCC_VAR_NOINIT) Scc_StateM_TxCore;
SCC_LOCAL VAR(Scc_StateM_RxCoreType, SCC_VAR_NOINIT) Scc_StateM_RxCore;
SCC_LOCAL VAR(Scc_StateM_LocalType, SCC_VAR_NOINIT)  Scc_StateM_Local;

SCC_LOCAL VAR(Scc_StateM_ControlPilotStateType, SCC_VAR_NOINIT) Scc_StateM_LastControlPilotState;

#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

 /**********************************************************************************************************************
 *  Scc_StateM_Prepare<StateMachineState>
 *********************************************************************************************************************/
 /*! \brief         Set all parameters for this state
 *  \details        Set all parameters, the message trigger and the cyclic message trigger.
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_PrepareSLAC(void);
#endif /* SCC_ENABLE_SLAC_HANDLING */
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareSECCDiscoveryProtocolReq(void);
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareTlConnection(void);
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareSupportedAppProtocolReq(void);
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareSessionSetupReq(void);
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareServiceDiscoveryReq(void);
#if ( (( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0u )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )) )
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareServiceDetailReq(void);
#endif /* SCC_SCHEMA_ISO || SCC_SCHEMA_ISO_ED2 */
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_StateM_PreparePaymentServiceSelectionReq(void);
#if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareCertificateInstallationReq(void);
#endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */
#if ( SCC_ENABLE_CERTIFICATE_UPDATE == STD_ON )
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareCertificateUpdateReq(void);
#endif /* SCC_ENABLE_CERTIFICATE_UPDATE */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PreparePaymentDetailsReq(void);
#endif /* SCC_ENABLE_PNC_CHARGING */
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareAuthorizationReq(void);
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_PrepareChargeParameterDiscoveryReq(void);
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_PrepareCableCheckReq(void);
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PreparePreChargeReq(void);
#endif /* SCC_CHARGING_DC */
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_PreparePowerDeliveryReq(void);
#if ( defined SCC_CHARGING_DC_BPT ) && ( SCC_CHARGING_DC_BPT == STD_ON )
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareDC_BidirectionalControlReq(void);
#endif /* SCC_CHARGING_DC_BPT */
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareCurrentDemandReq(void);
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareWeldingDetectionReq(void);
#endif /* SCC_CHARGING_DC */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareMeteringReceiptReq(void);
#endif /* SCC_ENABLE_PNC_CHARGING */
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareSessionStopReq(void);
/* see pattern Scc_StateM_Prepare<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareStopCommunicationSession(void);

/**********************************************************************************************************************
*  Scc_StateM_Proces<StateMachineState>
*********************************************************************************************************************/
/*! \brief         Set all parameters for this state
*  \details        Set new StateMachine State, set timeouts,
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    TRUE
*********************************************************************************************************************/
#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessSLAC(void);
#endif /* SCC_ENABLE_SLAC_HANDLING */
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessSECCDiscoveryProtocolRes(void);
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessTLConnected(void);
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessSupportedAppProtocolRes(void);
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessSessionSetupRes(void);
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessServiceDiscoveryRes(void);
#if ( (( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0u )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )) )
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessServiceDetailRes(void);
#endif /* SCC_SCHEMA_ISO || SCC_SCHEMA_ISO_ED2 */
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessPaymentServiceSelectionRes(void);
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
#if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessCertificateInstallationRes(void);
#endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */
#if ( SCC_ENABLE_CERTIFICATE_UPDATE == STD_ON )
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessCertificateUpdateRes(void);
#endif /* SCC_ENABLE_CERTIFICATE_UPDATE */
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessPaymentDetailsRes(void);
#endif /* SCC_ENABLE_PNC_CHARGING */
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessAuthorizationRes(void);
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessChargeParameterDiscoveryRes(void);
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessCableCheckRes(void);
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessPreChargeRes(void);
#endif /* SCC_CHARGING_DC */
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessPowerDeliveryRes(void);
#if ( defined SCC_CHARGING_DC_BPT ) && ( SCC_CHARGING_DC_BPT == STD_ON )
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessDC_BidirectionalControlRes(void);
#endif /* SCC_CHARGING_DC_BPT */
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessCurrentDemandRes(void);
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessWeldingDetectionRes(void);
#endif /* SCC_CHARGING_DC */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessMeteringReceiptRes(void);
#endif /* SCC_ENABLE_PNC_CHARGING */
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessSessionStopRes(void);
/* see pattern Scc_StateM_Proces<StateMachineState> */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessStopCommunicationSession(void);

/**********************************************************************************************************************
*  Scc_StateM_GlobalTimerChecks
*********************************************************************************************************************/
/*! \brief         Handle the SequencePerformanceTimer and CommunicationSetupTimer.
*  \details        Decrement the timer and report a timeout error if timer elapsed.
*  \return         OK              No timeout occurred
*  \return         NotOK           timeout occurred
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    TRUE
*********************************************************************************************************************/
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_StateM_GlobalTimerChecks(void);

/**********************************************************************************************************************
*  Scc_StateM_GlobalParamsChecks
*********************************************************************************************************************/
/*! \brief         Checks all global parameters
*  \details        Checks all global parameters and if module shall be reset.
*  \return         OK              parameter checks passed
*  \return         Pending         Triggert reset
*  \return         NotOK           Transport-error during a active session
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    TRUE
*********************************************************************************************************************/
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_GlobalParamsChecks(void);

/**********************************************************************************************************************
*  Scc_StateM_InitParams
*********************************************************************************************************************/
/*! \brief         Initialized all global parameters
*  \details        Initialized all global parameters after a reset.
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    TRUE
*********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_InitParams(void);

/**********************************************************************************************************************
*  Scc_StateM_ErrorHandling
*********************************************************************************************************************/
/*! \brief         Handles error
*  \details        Reports the error to the application, resets the core, state machine and message state.
*  \pre            -
*  \context        TASK
*  \reentrant      FALSE
*  \synchronous    TRUE
*********************************************************************************************************************/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ErrorHandling(Scc_StateM_StateMachineErrorType ErrorReason);

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Scc_StateM_InitMemory
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_InitMemory(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set module state to uninitialized. */
  Scc_StateM_State = Scc_SMS_Uninitialized;

  /* #20 Initialize local data. */
  Scc_StateM_IPAssigned = FALSE;
#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
  Scc_StateM_TrcvLinkState = FALSE;
#endif /* SCC_ENABLE_SLAC_HANDLING */

  Scc_StateM_Counter.CommunicationSetupTimerCnt = 0;
  Scc_StateM_Counter.GeneralDelayCnt = 0;
  Scc_StateM_Counter.IPAddressWaitTimerCnt = 0;
  Scc_StateM_Counter.NvmReadTimerCnt = 0;
  Scc_StateM_Counter.OngoingTimerCnt = 0;
  Scc_StateM_Counter.ReadyToChargeTimerCnt = 0;
  Scc_StateM_Counter.ReadyToChargeTimerStart = FALSE;
  Scc_StateM_Counter.ReconnectRetryCnt = 0;
  Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;

}

/**********************************************************************************************************************
 *  Scc_StateM_Init
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Init(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Initialize general data. */
  Scc_StateM_State = Scc_SMS_Initialized;

  Scc_StateM_Counter.ReconnectRetryCnt = (uint16)Scc_ConfigValue_StateM_ReconnectRetries;

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  Scc_StateM_Local.ReqCertDetails = (boolean)Scc_ConfigValue_StateM_RequestCertificateDetails;
#endif /* SCC_ENABLE_PNC_CHARGING */
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
  Scc_StateM_Local.ReqInetDetails = (boolean)Scc_ConfigValue_StateM_RequestInternetDetails;
#endif /* SCC_SCHEMA_ISO */

  Scc_StateM_LastControlPilotState = Scc_ControlPilotState_None;

  /* #20 Initialize the parameters */
  Scc_StateM_InitParams();

}

/**********************************************************************************************************************
 *  Scc_StateM_MainFunction
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_MainFunction(void) /* PRQA S 2889 */ /* MD_Scc_15.5 */
{
  /* #10 check if module was already initialized */
  if ( Scc_SMS_Uninitialized < Scc_StateM_State )
  {
    /* check if global timers elapsed */
    if (E_OK == Scc_StateM_GlobalTimerChecks())
    {
      if (Scc_ReturnType_OK == Scc_StateM_GlobalParamsChecks())
      {
#if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
        /* check if the state machine is waiting for the application */
        if (Scc_SMMS_WaitingForApplication == Scc_StateM_MsgState)
        {
          /* get the flow control from the application */
          Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
          /* check if the state machine has to wait */
          if (TRUE == Scc_StateM_Local.FlowControl)
          {
            /* set the msg state */
            Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)
          }
        }
#endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */

        switch (Scc_StateM_State)
        {
          /* #20 SCC is initialized */
          case Scc_SMS_Initialized:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_Initialized);
            /* update the FunctionControl value */
            Scc_Get_StateM_FunctionControl(&Scc_StateM_Local.FunctionControl);
            /* check if the module was signalized to get ready */
            if ( Scc_FunctionControl_ChargingMode == Scc_StateM_Local.FunctionControl )
            {
        #if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
              /* change the contract certificate chain (if necessary) */
              if ( (Std_ReturnType)E_OK != (Std_ReturnType)Scc_SwapContrCertChain(
                (uint8)Scc_ConfigValue_StateM_ContractCertificateChainIndexInUse, FALSE) )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_Initialization_ContractSelectionFailed);
              }
        #endif

        #if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
              /* set the new internal state */
              Scc_StateM_State = Scc_SMS_SLAC;
        #else
              /* set the new internal state */
              Scc_StateM_State = Scc_SMS_WaitForIPAddress;
        #endif /* SCC_ENABLE_SLAC_HANDLING */
            }
          }
          break;

        #if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
          case Scc_SMS_SLAC:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_SLAC);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              if ( Scc_ReturnType_OK == Scc_StateM_PrepareSLAC() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
              }
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              /* if the response was received successfully */
              if ( Scc_MsgStatus_SLAC_OK == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_SLAC_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                /* stop the communication session */
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              Scc_StateM_ProcessSLAC();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

      #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
              /* get the flow control from the application */
              Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
              /* check if the state machine has to wait */
              if ( TRUE != Scc_StateM_Local.FlowControl )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
              }
      #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
            }
          }
          break;
        #endif /* SCC_ENABLE_SLAC_HANDLING */

          /* #30 Wait until the IP address is assigned */
          case Scc_SMS_WaitForIPAddress:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_WaitForIP);
            /* check if an IPv6 address has been assigned */
            if ( TRUE == Scc_StateM_IPAssigned )
            {
              /* stop the IP wait timer */
              Scc_StateM_Counter.IPAddressWaitTimerCnt = 0;
              /* continue with the SDP */
              Scc_StateM_State = Scc_SMS_SECCDiscoveryProtocol;
            }
            /* IPv6 address has not been assigned yet */
            else
            {
              /* check if the IP address timer is active */
              if ( 0u < Scc_StateM_Counter.IPAddressWaitTimerCnt )
              {
                /* decrement the timer */
                Scc_StateM_Counter.IPAddressWaitTimerCnt--;
                /* check if the timer elapsed */
                if ( 0u == Scc_StateM_Counter.IPAddressWaitTimerCnt )
                {
                  /* start error handling */
                  Scc_StateM_ErrorHandling(Scc_SMER_Timer_IPAddressWaitTimer);
                }
              }
              /* if the timer was not started yet */ /* PRQA S 2880,2742 4 */ /* MD_Scc_2742_2880_2995 */
              else if (0u < (uint16)Scc_ConfigValue_Timer_General_IPAddressWaitTimeout) /* PRQA S 2004 */ /* MD_Scc_2004 */ /*lint !e506 */
              {
                /* set the timer to the configured timeout and start it */
                Scc_StateM_Counter.IPAddressWaitTimerCnt = (uint16)Scc_ConfigValue_Timer_General_IPAddressWaitTimeout;
              }
            }
          }
          break;

          /* #40 SECC Discovery Protocol */
          case Scc_SMS_SECCDiscoveryProtocol:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_SECCDiscoveryProtocol);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareSECCDiscoveryProtocolReq();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              /* if the response was received successfully */
              if ( Scc_MsgStatus_SECCDiscoveryProtocol_OK == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_SECCDiscoveryProtocol_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                /* stop the communication session */
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              if ( (Std_ReturnType)E_OK == Scc_StateM_ProcessSECCDiscoveryProtocolRes() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

        #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
                /* get the flow control from the application */
                Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
                /* check if the state machine has to wait */
                if ( TRUE != Scc_StateM_Local.FlowControl )
                {
                  /* set the msg state */
                  Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
                }
        #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
              }
            }
          }
          break;

          /* #50 Transport Layer */
          case Scc_SMS_TLConnection:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_TLConnection);
            /* prepare the establishment of the transport layer connection */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareTlConnection();

              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)

            }
            /* wait for the transport layer to be connected */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              /* if the connection establishment of either TLS or TCP was successful */
              if ( Scc_MsgStatus_TransportLayer_OK == Scc_StateM_RxCore.MsgStatus )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_TransportLayer_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the result of the connection establishment */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              Scc_StateM_ProcessTLConnected();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

      #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
              /* get the flow control from the application */
              Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
              /* check if the state machine has to wait */
              if ( TRUE != Scc_StateM_Local.FlowControl )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
              }
      #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
            }
          }
          break;

          /* #60 Supported App Protocol */
          case Scc_SMS_SupportedAppProtocol:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_SupportedAppProtocol);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareSupportedAppProtocolReq();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
              /* stop the sequence performance timeout */
              Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if ( Scc_MsgStatus_SupportedAppProtocol_OK == Scc_StateM_RxCore.MsgStatus )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_SupportedAppProtocol_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              Scc_StateM_ProcessSupportedAppProtocolRes();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

      #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
              /* get the flow control from the application */
              Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
              /* check if the state machine has to wait */
              if ( TRUE != Scc_StateM_Local.FlowControl )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
              }
      #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
            }
          }
          break;

          /* #70 Session Setup */
          case Scc_SMS_SessionSetup:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_SessionSetup);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareSessionSetupReq();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
              /* stop the sequence performance timeout */
              Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if ( Scc_MsgStatus_SessionSetup_OK == Scc_StateM_RxCore.MsgStatus )
              {
                /* stop the CommunicationSetup timer */
                Scc_StateM_Counter.CommunicationSetupTimerCnt = 0;

                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_SessionSetup_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                /* stop the CommunicationSetup timer */
                Scc_StateM_Counter.CommunicationSetupTimerCnt = 0;
                /* start error handling */
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              Scc_StateM_ProcessSessionSetupRes();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

      #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
              /* get the flow control from the application */
              Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
              /* check if the state machine has to wait */
              if ( TRUE != Scc_StateM_Local.FlowControl )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
              }
      #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
            }
          }
          break;

          /* #80 Service Discovery */
          case Scc_SMS_ServiceDiscovery:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_ServiceDiscovery);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareServiceDiscoveryReq();

              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
              /* stop the sequence performance timeout */
              Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if ( Scc_MsgStatus_ServiceDiscovery_OK == Scc_StateM_RxCore.MsgStatus )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_ServiceDiscovery_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              /* process the response */
              if ( Scc_ReturnType_OK == Scc_StateM_ProcessServiceDiscoveryRes() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

        #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
                /* get the flow control from the application */
                Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
                /* check if the state machine has to wait */
                if ( TRUE != Scc_StateM_Local.FlowControl )
                {
                  /* set the msg state */
                  Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
                }
        #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
              }
            }
          }
          break;

        #if ( (( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0u )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )) )
          /* #90 Service Detail */
          case Scc_SMS_ServiceDetail:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_ServiceDetail);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareServiceDetailReq();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
              /* stop the sequence performance timeout */
              Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if (   ( Scc_MsgStatus_ServiceDetail_OK == Scc_StateM_RxCore.MsgStatus )
                  && ( TRUE == Scc_StateM_RxCore.CyclicMsgRcvd ))
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
                /* reset the cyclic msg status */
                Scc_StateM_RxCore.CyclicMsgRcvd = FALSE;
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_ServiceDetail_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              Scc_StateM_ProcessServiceDetailRes();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

  #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
              /* get the flow control from the application */
              Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
              /* check if the state machine has to wait */
              if ( TRUE != Scc_StateM_Local.FlowControl )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
              }
  #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
            }
          }
          break;
  #endif /* SCC_SCHEMA_ISO || SCC_SCHEMA_ISO_ED2 */

          /* #100 Payment Service Selection */
          case Scc_SMS_PaymentServiceSelection:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_PaymentServiceSelection);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              if ( (Std_ReturnType)E_OK == Scc_StateM_PreparePaymentServiceSelectionReq() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
                /* stop the sequence performance timeout */
                Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
              }
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if ( Scc_MsgStatus_PaymentServiceSelection_OK == Scc_StateM_RxCore.MsgStatus )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_PaymentServiceSelection_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              if (Scc_ReturnType_OK == Scc_StateM_ProcessPaymentServiceSelectionRes()) /* PRQA S 2991,2995 */ /* MD_Scc_VariantDependent */
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

        #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
                /* get the flow control from the application */
                Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
                /* check if the state machine has to wait */
                if ( TRUE != Scc_StateM_Local.FlowControl )
                {
                  /* set the msg state */
                  Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
                }
        #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
              }
            }
          }
          break;
        #if ( SCC_ENABLE_PNC_CHARGING == STD_ON )

        #if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
          /* #110 Certificate Installation */
          case Scc_SMS_CertificateInstallation:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_CertificateInstallation);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareCertificateInstallationReq();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
              /* stop the sequence performance timeout */
              Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if ( Scc_MsgStatus_CertificateInstallation_OK == Scc_StateM_RxCore.MsgStatus )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_CertificateInstallation_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              Scc_StateM_ProcessCertificateInstallationRes();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

      #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
              /* get the flow control from the application */
              Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
              /* check if the state machine has to wait */
              if ( TRUE != Scc_StateM_Local.FlowControl )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
              }
      #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
            }
          }
          break;
        #endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

        #if ( SCC_ENABLE_CERTIFICATE_UPDATE == STD_ON )
          /* #120 Certificate Update */
          case Scc_SMS_CertificateUpdate:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_CertificateUpdate);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareCertificateUpdateReq();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
              /* stop the sequence performance timeout */
              Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if ( Scc_MsgStatus_CertificateUpdate_OK == Scc_StateM_RxCore.MsgStatus )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_CertificateUpdate_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              Scc_StateM_ProcessCertificateUpdateRes();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

      #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
              /* get the flow control from the application */
              Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
              /* check if the state machine has to wait */
              if ( TRUE != Scc_StateM_Local.FlowControl )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
              }
      #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
            }
          }
          break;
        #endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

          /* #130 Payment Details */
          case Scc_SMS_PaymentDetails:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_PaymentDetails);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PreparePaymentDetailsReq();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
              /* stop the sequence performance timeout */
              Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if ( Scc_MsgStatus_PaymentDetails_OK == Scc_StateM_RxCore.MsgStatus )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_PaymentDetails_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              Scc_StateM_ProcessPaymentDetailsRes();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

      #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
              /* get the flow control from the application */
              Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
              /* check if the state machine has to wait */
              if ( TRUE != Scc_StateM_Local.FlowControl )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
              }
      #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
            }
          }
          break;
        #endif /* SCC_ENABLE_PNC_CHARGING */



          /* #170 Authorization */
          case Scc_SMS_Authorization:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_Authorization);
        #if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
            /* check if the cycle timeout is active */
            if ( Scc_SAPSchemaIDs_ISO == Scc_StateM_RxEVSE.SAPSchemaID )
            {
              /* check the cycle timeout */
              if ( 0u < Scc_StateM_Counter.OngoingTimerCnt )
              {
                Scc_StateM_Counter.OngoingTimerCnt--;

                if ( 0u == Scc_StateM_Counter.OngoingTimerCnt )
                {
                  Scc_StateM_ErrorHandling(Scc_SMER_Timer_AuthorizationOngoingTimer);
                  return;
                }
              }
              /* if the cycle timer is inactive, check if it shall be enabled */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_EVSEProcessing_Finished != Scc_StateM_RxEVSE.EVSEProcessing )
              {
                Scc_StateM_Counter.OngoingTimerCnt = (uint16)Scc_ConfigValue_StateM_AuthorizationOngoingTimeout;
              }
            }
        #endif /* SCC_SCHEMA_ISO */
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareAuthorizationReq();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
              /* stop the sequence performance timeout */
              Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if (   ( Scc_MsgStatus_Authorization_OK == Scc_StateM_RxCore.MsgStatus )
                  && ( TRUE == Scc_StateM_RxCore.CyclicMsgRcvd ))
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
                /* reset the cyclic msg status */
                Scc_StateM_RxCore.CyclicMsgRcvd = FALSE;
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_Authorization_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              if ( Scc_ReturnType_OK == Scc_StateM_ProcessAuthorizationRes() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

        #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
                /* get the flow control from the application */
                Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
                /* check if the state machine has to wait */
                if ( TRUE != Scc_StateM_Local.FlowControl )
                {
                  /* set the msg state */
                  Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
                }
        #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
              }
            }
          }
          break;

          /* #180 Charge Parameter Discovery */
          case Scc_SMS_ChargeParameterDiscovery:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_ChargeParameterDiscovery);
        #if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
            /* check if the cycle timeout is active */
            if ( Scc_SAPSchemaIDs_ISO == Scc_StateM_RxEVSE.SAPSchemaID )
            {
              /* check the cycle timeout */
              if ( 0u < Scc_StateM_Counter.OngoingTimerCnt )
              {
                Scc_StateM_Counter.OngoingTimerCnt--;

                if ( 0u == Scc_StateM_Counter.OngoingTimerCnt )
                {
                  Scc_StateM_ErrorHandling(Scc_SMER_Timer_ChargeParameterDiscoveryOngoingTimer);
                  return;
                }
              }
              /* if the cycle timer is inactive, check if it shall be enabled */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_EVSEProcessing_Finished != Scc_StateM_RxEVSE.EVSEProcessing )
              {
                Scc_StateM_Counter.OngoingTimerCnt =
                  (uint16)Scc_ConfigValue_StateM_ChargeParameterDiscoveryOngoingTimeout;
              }
            }
        #endif /* SCC_SCHEMA_ISO */
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              if ( Scc_ReturnType_OK == Scc_StateM_PrepareChargeParameterDiscoveryReq() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
                /* stop the sequence performance timeout */
                Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
              }
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if (   ( Scc_MsgStatus_ChargeParameterDiscovery_OK == Scc_StateM_RxCore.MsgStatus )
                  && ( TRUE == Scc_StateM_RxCore.CyclicMsgRcvd ))
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
                /* reset the cyclic msg status */
                Scc_StateM_RxCore.CyclicMsgRcvd = FALSE;
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_ChargeParameterDiscovery_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              if ( Scc_ReturnType_OK == Scc_StateM_ProcessChargeParameterDiscoveryRes() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

        #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
                /* get the flow control from the application */
                Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
                /* check if the state machine has to wait */
                if ( TRUE != Scc_StateM_Local.FlowControl )
                {
                  /* set the msg state */
                  Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
                }
        #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
              }
            }
          }
          break;


        #if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
          /* #200 Cable Check */
          case Scc_SMS_CableCheck:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_CableCheck);
            /* check the cycle timeout */
            if ( 0u < Scc_StateM_Counter.OngoingTimerCnt )
            {
              Scc_StateM_Counter.OngoingTimerCnt--;

              if ( 0u == Scc_StateM_Counter.OngoingTimerCnt )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_Timer_CableCheckOngoingTimer);
                return;
              }
            }
            /* if the cycle timer is inactive, check if it shall be enabled */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
            else if ( Scc_EVSEProcessing_Finished != Scc_StateM_RxEVSE.EVSEProcessing )
            {
              Scc_StateM_Counter.OngoingTimerCnt = (uint16)Scc_ConfigValue_StateM_CableCheckOngoingTimeout;
            }
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              if ( Scc_ReturnType_OK == Scc_StateM_PrepareCableCheckReq() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
                /* stop the sequence performance timeout */
                Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
              }
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if (   ( Scc_MsgStatus_CableCheck_OK == Scc_StateM_RxCore.MsgStatus )
                  && ( TRUE == Scc_StateM_RxCore.CyclicMsgRcvd ))
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
                /* reset the cyclic msg status */
                Scc_StateM_RxCore.CyclicMsgRcvd = FALSE;
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_CableCheck_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              if ( Scc_ReturnType_OK == Scc_StateM_ProcessCableCheckRes() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

        #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
                /* get the flow control from the application */
                Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
                /* check if the state machine has to wait */
                if ( TRUE != Scc_StateM_Local.FlowControl )
                {
                  /* set the msg state */
                  Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
                }
        #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
              }
            }
          }
          break;

          /* #210 PreCharge */
          case Scc_SMS_PreCharge:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_PreCharge);
            /* check the cycle timeout */
            if ( 0u < Scc_StateM_Counter.OngoingTimerCnt )
            {
              Scc_StateM_Counter.OngoingTimerCnt--;

              if ( 0u == Scc_StateM_Counter.OngoingTimerCnt )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_Timer_PreChargeOngoingTimer);
                return;
              }
            }
            /* if the cycle timer is inactive, check if it shall be enabled */
            else
            {
              Scc_StateM_Counter.OngoingTimerCnt = (uint16)Scc_ConfigValue_StateM_PreChargeOngoingTimeout;
            }
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PreparePreChargeReq();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
              /* stop the sequence performance timeout */
              Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if (   ( Scc_MsgStatus_PreCharge_OK == Scc_StateM_RxCore.MsgStatus )
                  && ( TRUE == Scc_StateM_RxCore.CyclicMsgRcvd ))
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
                /* reset the cyclic msg status */
                Scc_StateM_RxCore.CyclicMsgRcvd = FALSE;
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_PreCharge_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              if ( Scc_ReturnType_OK == Scc_StateM_ProcessPreChargeRes() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

        #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
                /* get the flow control from the application */
                Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
                /* check if the state machine has to wait */
                if ( TRUE != Scc_StateM_Local.FlowControl )
                {
                  /* set the msg state */
                  Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
                }
        #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
              }
            }
          }
          break;
        #endif /* SCC_CHARGING_DC */

          /* #220 Power Delivery */
          case Scc_SMS_PowerDelivery:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_PowerDelivery);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              if ( Scc_ReturnType_OK == Scc_StateM_PreparePowerDeliveryReq() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
                /* stop the sequence performance timeout */
                Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
              }
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if ( Scc_MsgStatus_PowerDelivery_OK == Scc_StateM_RxCore.MsgStatus )
              {
                /* stop the ReadyToCharge timer */
                Scc_StateM_Counter.ReadyToChargeTimerStart = FALSE;
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_PowerDelivery_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                /* stop the ReadyToCharge timer */
                Scc_StateM_Counter.ReadyToChargeTimerStart = FALSE;
                /* start error handling */
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              if ( Scc_ReturnType_OK == Scc_StateM_ProcessPowerDeliveryRes() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

        #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
                /* get the flow control from the application */
                Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
                /* check if the state machine has to wait */
                if ( TRUE != Scc_StateM_Local.FlowControl )
                {
                  /* set the msg state */
                  Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
                }
        #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
              }
            }
          }
          break;



  #if ( defined SCC_CHARGING_DC_BPT ) && ( SCC_CHARGING_DC_BPT == STD_ON )
          /* #250 DC_BidirectionalControl */
          case Scc_SMS_DC_BidirectionalControl:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_DC_BidirectionalControl);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareDC_BidirectionalControlReq();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
                /* stop the sequence performance timeout */
                Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if ( (Scc_MsgStatus_DC_BidirectionalControl_OK == Scc_StateM_RxCore.MsgStatus)
                && (TRUE == Scc_StateM_RxCore.CyclicMsgRcvd) )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
                  /* reset the cyclic msg status */
                  Scc_StateM_RxCore.CyclicMsgRcvd = FALSE;
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_DC_BidirectionalControl_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              if ( Scc_ReturnType_OK == Scc_StateM_ProcessDC_BidirectionalControlRes() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)
              }
            }
          }
          break;
  #endif /* SCC_CHARGING_DC_BPT */


        #if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )

          /* #270 Current Demand */
          case Scc_SMS_CurrentDemand:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_CurrentDemand);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareCurrentDemandReq();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
              /* stop the sequence performance timeout */
              Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if (   ( Scc_MsgStatus_CurrentDemand_OK == Scc_StateM_RxCore.MsgStatus )
                  && ( TRUE == Scc_StateM_RxCore.CyclicMsgRcvd ))
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
                /* reset the cyclic msg status */
                Scc_StateM_RxCore.CyclicMsgRcvd = FALSE;
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_CurrentDemand_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              if ( Scc_ReturnType_OK == Scc_StateM_ProcessCurrentDemandRes() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

        #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
                /* get the flow control from the application */
                Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
                /* check if the state machine has to wait */
                if ( TRUE != Scc_StateM_Local.FlowControl )
                {
                  /* set the msg state */
                  Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
                }
        #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
              }
            }
          }
          break;

          /* #280 Welding Detection */
          case Scc_SMS_WeldingDetection:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_WeldingDetection);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareWeldingDetectionReq();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
              /* stop the sequence performance timeout */
              Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if (   ( Scc_MsgStatus_WeldingDetection_OK == Scc_StateM_RxCore.MsgStatus )
                  && ( TRUE == Scc_StateM_RxCore.CyclicMsgRcvd ))
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
                /* reset the cyclic msg status */
                Scc_StateM_RxCore.CyclicMsgRcvd = FALSE;
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_WeldingDetection_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              if ( Scc_ReturnType_OK == Scc_StateM_ProcessWeldingDetectionRes() )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

        #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
                /* get the flow control from the application */
                Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
                /* check if the state machine has to wait */
                if ( TRUE != Scc_StateM_Local.FlowControl )
                {
                  /* set the msg state */
                  Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
                }
        #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
              }
            }
          }
          break;
        #endif /* SCC_CHARGING_DC */

        #if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
          /* #290 Metering Receipt */
          case Scc_SMS_MeteringReceipt:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_MeteringReceipt);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareMeteringReceiptReq();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
              /* stop the sequence performance timeout */
              Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if ( Scc_MsgStatus_MeteringReceipt_OK == Scc_StateM_RxCore.MsgStatus )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_MeteringReceipt_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              if ( (Std_ReturnType)E_OK == Scc_StateM_ProcessMeteringReceiptRes())
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

      #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
                /* get the flow control from the application */
                Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
                /* check if the state machine has to wait */
                if ( TRUE != Scc_StateM_Local.FlowControl )
                {
                  /* set the msg state */
                  Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
                }
      #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
              }
            }
          }
          break;
        #endif /* SCC_ENABLE_PNC_CHARGING */

          /* #300 Session Stop */
          case Scc_SMS_SessionStop:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_SessionStop);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareSessionStopReq();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
              /* stop the sequence performance timeout */
              Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if ( Scc_MsgStatus_SessionStop_OK == Scc_StateM_RxCore.MsgStatus )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_SessionStop_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              if (Scc_ReturnType_OK == Scc_StateM_ProcessSessionStopRes()) /* PRQA S 2991,2995 */ /* MD_Scc_VariantDependent */
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

        #if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
                /* get the flow control from the application */
                Scc_Get_StateM_FlowControl(&Scc_StateM_Local.FlowControl);
                /* check if the state machine has to wait */
                if ( TRUE != Scc_StateM_Local.FlowControl )
                {
                  /* set the msg state */
                  Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
                }
        #endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
              }
            }
          }
          break;

          /* #310 Stop Communication Session */
          case Scc_SMS_StopCommunicationSession:
          {
            /* report the current status to the application */
            Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_StopCommunicationSession);
            /* prepare the request */
            if ( Scc_SMMS_PreparingRequest == Scc_StateM_MsgState )
            {
              Scc_StateM_PrepareStopCommunicationSession();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForResponse)
            }
            /* wait for the response */
            if ( Scc_SMMS_WaitingForResponse == Scc_StateM_MsgState )
            {
              if ( Scc_MsgStatus_StopCommunicationSession_OK == Scc_StateM_RxCore.MsgStatus )
              {
                /* set the msg state */
                Scc_StateM_MsgStateFct(Scc_SMMS_ProcessingResponse)
              }
              /* if an error occurred */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
              else if ( Scc_MsgStatus_StopCommunicationSession_Failed == Scc_StateM_RxCore.MsgStatus )
              {
                Scc_StateM_ErrorHandling(Scc_SMER_StackError);
              }
            }
            /* process the response */
            if ( Scc_SMMS_ProcessingResponse == Scc_StateM_MsgState )
            {
              Scc_StateM_ProcessStopCommunicationSession();
              /* set the msg state */
              Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)
            }
          }
          break;

          /* #320 Scc Stopped */
          case Scc_SMS_Stopped:
          {
            /* read the FunctionControl value */
            Scc_Get_StateM_FunctionControl(&Scc_StateM_Local.FunctionControl);

            /* check if the application requests an immediate restart */
            if ( Scc_FunctionControl_Reset == Scc_StateM_Local.FunctionControl )
            {
              /* reinitialize the volatile parameters */
              Scc_StateM_InitParams();
              /* go back to the init state */
              Scc_StateM_State = Scc_SMS_Initialized;
            }
            else
            {
              /* check if a reconnect was started with a delay */
              if ( 0u < Scc_StateM_Counter.GeneralDelayCnt )
              {
                Scc_StateM_Counter.GeneralDelayCnt--;
                /* if the timer has elapsed, start the reconnect */
                if ( 0u == Scc_StateM_Counter.GeneralDelayCnt )
                {
                  Scc_StateM_InitParams();
        #if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
                  if ( FALSE == Scc_StateM_TrcvLinkState )
                  {
                    Scc_StateM_State = Scc_SMS_SLAC;
                  }
                  else
        #endif /* SCC_ENABLE_SLAC_HANDLING */
                  {
                    Scc_StateM_State = Scc_SMS_WaitForIPAddress;
                  }
                }
              }
            }
          }
          break;

          default: /* PRQA S 2016 */ /*MD_MSR_EmptyClause*/
            break;
        }
      }
    }
  }
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_StateM_Set_Core_TrcvLinkState()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_TrcvLinkState(boolean LinkStateActive)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the timer and tranceiverLinkState acording to the */
  if ( TRUE == LinkStateActive )
  {
#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
    Scc_StateM_TrcvLinkState = TRUE;
#endif /* SCC_ENABLE_SLAC_HANDLING */
    /* start the communication setup timer */
    Scc_StateM_Counter.CommunicationSetupTimerCnt = 1;
  }
  else
  {
#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
    Scc_StateM_TrcvLinkState = FALSE;
#endif /* SCC_ENABLE_SLAC_HANDLING */
    Scc_StateM_IPAssigned = FALSE;
    /* stop the communication setup timer */
    Scc_StateM_Counter.CommunicationSetupTimerCnt = 0;
    /* stop the ready to charge timer */
    Scc_StateM_Counter.ReadyToChargeTimerStart = FALSE;
  }

}

/**********************************************************************************************************************
 *  Scc_StateM_Set_Core_TrcvLinkState()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_IPAssigned(boolean IPAssigned)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the IPAssigned paramter from the TcpIp. */
  if ( TRUE == IPAssigned )
  {
    Scc_StateM_IPAssigned = TRUE;
  }
  else
  {
    Scc_StateM_IPAssigned = FALSE;
  }

}

/**********************************************************************************************************************
 *  Scc_StateM_Set_Core_CyclicMsgTrigTx()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_CyclicMsgTrigTx(boolean CyclicMsgTrig)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the cyclic message trigger from the Scc Core. */
  Scc_StateM_TxCore.CyclicMsgTrig = CyclicMsgTrig;

}

/**********************************************************************************************************************
 *  Scc_StateM_Set_Core_CyclicMsgTrigTx()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_CyclicMsgRcvd(boolean CyclicMsgRcvd)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the cyclic message received if a cyclic message was received. */
  Scc_StateM_RxCore.CyclicMsgRcvd = CyclicMsgRcvd;

}

/**********************************************************************************************************************
 *  Scc_StateM_Set_Core_MsgStatus()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_MsgStatus(Scc_MsgStatusType MsgStatus)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if the msg status is positive (OK) and set SequencePerformanceTimeout. */
  if (   (1u == ((uint16)MsgStatus % (uint16)2) )
      && ( Scc_MsgStatus_SupportedAppProtocol_OK <= MsgStatus )
      && ( Scc_MsgStatus_SessionStop_OK > MsgStatus ))
  {
    /* check which schema is used */
    switch ( Scc_StateM_RxEVSE.SAPSchemaID )
    {
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
    case Scc_SAPSchemaIDs_ISO:
      /* start the sequence performance timeout */
      Scc_StateM_Counter.SequencePerformanceTimerCnt =
        (uint16)Scc_ConfigValue_Timer_ISO_SequencePerformanceTimeout;
      break;
#endif /* SCC_SCHEMA_ISO */

#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )
    case Scc_SAPSchemaIDs_ISO_Ed2_DIS:
      /* start the sequence performance timeout */
      Scc_StateM_Counter.SequencePerformanceTimerCnt =
        (uint16)Scc_ConfigValue_Timer_ISO_Ed2_DIS_SequencePerformanceTimeout;
      break;
#endif /* SCC_SCHEMA_ISO_ED2 */

#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
    case Scc_SAPSchemaIDs_DIN:
      /* start the sequence performance timeout */
      Scc_StateM_Counter.SequencePerformanceTimerCnt =
        (uint16)Scc_ConfigValue_Timer_DIN_SequencePerformanceTimeout;
      break;
#endif /* SCC_SCHEMA_DIN */

    default:
      /* this is an invalid state */
#if(SCC_DEV_ERROR_REPORT == STD_ON)
      (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_STATEM_SET_MSG_STATUS, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
      break;
    }
  }
  else
  {
    /* stop the sequence performance timeout */
    Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
  }

  Scc_StateM_RxCore.MsgStatus = MsgStatus;

}

/**********************************************************************************************************************
 *  Scc_StateM_Set_Core_StackError()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_StackError(Scc_StackErrorType StackError)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the StackError */
  Scc_StateM_RxCore.StackError = StackError;

}


#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
/**********************************************************************************************************************
 *  Scc_StateM_Set_ISO_EVSEProcessing()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_EVSEProcessing(Exi_ISO_EVSEProcessingType EVSEProcessing)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set EVSEProcessing for ISO Ed1 */
  switch ( EVSEProcessing )
  {
  case EXI_ISO_EVSEPROCESSING_TYPE_ONGOING:
  case EXI_ISO_EVSEPROCESSING_TYPE_ONGOING_WAITING_FOR_CUSTOMER_INTERACTION:
    Scc_StateM_RxEVSE.EVSEProcessing = Scc_EVSEProcessing_Ongoing;
    break;

  default: /* case EXI_ISO_EVSEPROCESSING_TYPE_FINISHED: */
    Scc_StateM_RxEVSE.EVSEProcessing = Scc_EVSEProcessing_Finished;
    break;
  }

}
#endif /* SCC_SCHEMA_ISO */

#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
/**********************************************************************************************************************
 *  Scc_StateM_Set_ISO_Ed2_DIS_EVSEProcessing()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_Ed2_DIS_EVSEProcessing(Exi_ISO_ED2_DIS_processingType EVSEProcessing)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set EVSEProcessing for ISO Ed2 */
  switch ( EVSEProcessing )
  {
    case EXI_ISO_ED2_DIS_PROCESSING_TYPE_ONGOING:
    case EXI_ISO_ED2_DIS_PROCESSING_TYPE_ONGOING_WAITING_FOR_CUSTOMER_INTERACTION:
      Scc_StateM_RxEVSE.EVSEProcessing = Scc_EVSEProcessing_Ongoing;
      break;

    default: /* case EXI_ISO_EVSEPROCESSING_TYPE_FINISHED: */
      Scc_StateM_RxEVSE.EVSEProcessing = Scc_EVSEProcessing_Finished;
      break;
  }

}
#endif /* SCC_SCHEMA_ISO_ED2 */

#if ( ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) )
/**********************************************************************************************************************
 *  Scc_StateM_Set_DIN_EVSEProcessing()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_DIN_EVSEProcessing(Exi_DIN_EVSEProcessingType EVSEProcessing)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set EVSEProcessing for DIN */
  switch ( EVSEProcessing )
  {
  case EXI_DIN_EVSEPROCESSING_TYPE_ONGOING:
    Scc_StateM_RxEVSE.EVSEProcessing = Scc_EVSEProcessing_Ongoing;
    break;

  default: /* case EXI_DIN_EVSEPROCESSING_TYPE_FINISHED: */
    Scc_StateM_RxEVSE.EVSEProcessing = Scc_EVSEProcessing_Finished;
    break;
  }

}
#endif /* SCC_SCHEMA_DIN */

/**********************************************************************************************************************
 *  Scc_StateM_Set_Core_SDPSecurityTx()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_SDPSecurityTx(Scc_SDPSecurityType SDPSecurity)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the security flag (Tls or Tcp) from SECC Discovery Protocol Response */
  Scc_StateM_RxCore.SupSDPSecurity = SDPSecurity;

}

/**********************************************************************************************************************
 *  Scc_StateM_Set_Core_SAPSchemaID()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_SAPSchemaID(Scc_SAPSchemaIDType SAPSchemaID)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set Schema (DIN, ISO Ed1 or ISO Ed2) from the Supported App Protocol Response */
  Scc_StateM_RxEVSE.SAPSchemaID = SAPSchemaID;

#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
  /* check if ISO was selected */
  if ( Scc_SAPSchemaIDs_ISO == Scc_StateM_RxEVSE.SAPSchemaID )
  {
    /* disable the ReadyToChargeTimer (only used for DIN) */
    Scc_StateM_Counter.ReadyToChargeTimerStart = FALSE;
  }
#endif /* SCC_SCHEMA_ISO */

#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )
  /* check if ISO was selected */
  if (Scc_SAPSchemaIDs_ISO_Ed2_DIS == Scc_StateM_RxEVSE.SAPSchemaID)
  {
    /* disable the ReadyToChargeTimer (only used for DIN) */
    Scc_StateM_Counter.ReadyToChargeTimerStart = FALSE;
  }
#endif /* SCC_SCHEMA_ISO_ED2 */

}

/**********************************************************************************************************************
 *  Scc_StateM_Set_ISO_PaymentOptionList()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_PaymentOptionList(P2CONST(Exi_ISO_PaymentOptionListType, AUTOMATIC, SCC_VAR_NOINIT) PaymentOptionList)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8_least counter;
  boolean ContractFound = FALSE;
  boolean ExternalPaymentFound = FALSE;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over the PaymentOptionList from the Service Discovery Response and search for EIM and PnC */
  for (counter = 0; counter < PaymentOptionList->PaymentOptionCount; counter++)
  {
    /* check if contract based payment was offered */
    if ( EXI_ISO_PAYMENT_OPTION_TYPE_CONTRACT == PaymentOptionList->PaymentOption[counter] )
    {
      /* PnC charging is available */
      ContractFound = TRUE;
    }
    /* check if external payment was offered */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
    else if ( EXI_ISO_PAYMENT_OPTION_TYPE_EXTERNAL_PAYMENT == PaymentOptionList->PaymentOption[counter] )
    {
      /* EIM charging is available */
      ExternalPaymentFound = TRUE;
    }
  }

  /* #20 Check the combination of EIM and PnC charging */
  /* check if only contract based payment was found */
  if (   (TRUE == ContractFound)
      && (FALSE == ExternalPaymentFound) )
  {
    Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions = Scc_PaymentPrioritization_OnlyPnC;
  }
  /* check if only external payment was found */
  else if (   (FALSE == ContractFound)
           && (TRUE == ExternalPaymentFound) )
  {
    Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions = Scc_PaymentPrioritization_OnlyEIM;
  }
  /* check if both contract based and external payment was found */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
  else if (   (TRUE == ContractFound)
           && (TRUE == ExternalPaymentFound) )
  {
    Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions = Scc_PaymentPrioritization_PrioritizePnC;
  }

}

/**********************************************************************************************************************
 *  Scc_StateM_Set_ISO_ChargeService()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_ChargeService(P2CONST(Exi_ISO_ChargeServiceType, AUTOMATIC, SCC_VAR_NOINIT) ChargeService)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8_least Counter;
  uint8       EnergyTransferModeFlags = 0;

  /* ----- Implementation ----------------------------------------------- */
  Scc_StateM_RxEVSE.ChargeServiceID = ChargeService->ServiceID;
  Scc_StateM_RxEVSE.ChargeServiceFree = ChargeService->FreeService;

  /* #10 Step through all supported energy transfer modes */
  for ( Counter = 0; Counter < ChargeService->SupportedEnergyTransferMode->EnergyTransferModeCount; Counter++ )
  {
    switch ( ChargeService->SupportedEnergyTransferMode->EnergyTransferMode[Counter] )
    {
    case EXI_ISO_ENERGY_TRANSFER_MODE_TYPE_AC_SINGLE_PHASE_CORE:
      EnergyTransferModeFlags |= (uint8)Scc_EnergyTransferMode_AC_1P;
      break;

    case EXI_ISO_ENERGY_TRANSFER_MODE_TYPE_AC_THREE_PHASE_CORE:
      EnergyTransferModeFlags |= (uint8)Scc_EnergyTransferMode_AC_3P;
      break;

    case EXI_ISO_ENERGY_TRANSFER_MODE_TYPE_DC_CORE:
      EnergyTransferModeFlags |= (uint8)Scc_EnergyTransferMode_DC_Core;
      break;

    case EXI_ISO_ENERGY_TRANSFER_MODE_TYPE_DC_EXTENDED:
      EnergyTransferModeFlags |= (uint8)Scc_EnergyTransferMode_DC_Extended;
      break;

    case EXI_ISO_ENERGY_TRANSFER_MODE_TYPE_DC_COMBO_CORE:
      EnergyTransferModeFlags |= (uint8)Scc_EnergyTransferMode_DC_Combo_Core;
      break;

    case EXI_ISO_ENERGY_TRANSFER_MODE_TYPE_DC_UNIQUE:
      EnergyTransferModeFlags |= (uint8)Scc_EnergyTransferMode_DC_Unique;
      break;

      /* invalid EnergyTransferMode */
    default:
      Scc_StateM_ErrorHandling(Scc_SMER_ServiceDiscovery_InvalidEnergyTransferMode);
      break;
    }
  }

  /* #20 provide the supported energy transfer modes to the application */
  Scc_Set_StateM_EnergyTransferModeFlags(EnergyTransferModeFlags);

}

/**********************************************************************************************************************
 *  Scc_StateM_Set_ISO_ServiceList()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_ServiceList(P2CONST(Exi_ISO_ServiceListType, AUTOMATIC, SCC_VAR_NOINIT) ServiceList, Exi_BitType Flag)
{
  /* ----- Implementation ----------------------------------------------- */
  if ( 1u == Flag )
  {
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
    boolean OffersCertificateService = FALSE;
#endif /* SCC_ENABLE_PNC_CHARGING */
    boolean OffersInternetService = FALSE;
    Exi_ISO_ServiceType *ServicePtr = ServiceList->Service;

    /* #10 Iterate through all available services */
    while ( ServicePtr != NULL_PTR )
    {
      /* PRQA S 4342 1 */ /* MD_Scc_Exi_Generic */
      Scc_ServiceIDType AvailableServices = (Scc_ServiceIDType)ServicePtr->ServiceID;

      /* only selected free services */
      if ( TRUE == ServicePtr->FreeService )
      {
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
        /* check if a certificate service is available */
        if ( Scc_SID_Certificate == AvailableServices )
        {
          /* #20 CertificateService is available */
          OffersCertificateService = TRUE;
        }
        else
#endif /* SCC_ENABLE_PNC_CHARGING */
        /* check if a internet service is available */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
        if ( Scc_SID_Internet == AvailableServices )
        {
          /* #30 InternetService is available */
          OffersInternetService = TRUE;
        }
      }
      /* select the next service */
      ServicePtr = ServicePtr->NextServicePtr;
    }

    /* #40 Check which services were found */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
    /* check if no certificate service was found */
    if ( OffersCertificateService == FALSE )
    {
      Scc_StateM_Local.ReqCertDetails = FALSE;
    }
#endif /* SCC_ENABLE_PNC_CHARGING */
    /* check if internet service was not found */
    if ( OffersInternetService == FALSE )
    {
      Scc_StateM_Local.ReqInetDetails = FALSE;
    }
  }
  else
  {
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
    Scc_StateM_Local.ReqCertDetails = FALSE;
#endif /* SCC_ENABLE_PNC_CHARGING */
    Scc_StateM_Local.ReqInetDetails = FALSE;
  }

} /* PRQA S 6080 */ /* MD_MSR_STMIF */
#endif /* SCC_SCHEMA_ISO */

#if ( ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) )
/**********************************************************************************************************************
 *  Scc_StateM_Set_DIN_ChargeService()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_DIN_ChargeService(P2CONST(Exi_DIN_ServiceChargeType, AUTOMATIC, SCC_VAR_NOINIT) ChargeService)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 EnergyTransferModeFlags = 0;

  /* ----- Implementation ----------------------------------------------- */
  Scc_StateM_RxEVSE.ChargeServiceID = ChargeService->ServiceTag->ServiceID;
  Scc_StateM_RxEVSE.ChargeServiceFree = ChargeService->FreeService;

  /* #10 Set EnergyTransferMode for DIN DC charging */
  switch ( ChargeService->EnergyTransferType )
  {
  case EXI_DIN_EVSESUPPORTED_ENERGY_TRANSFER_TYPE_DC_CORE:
    EnergyTransferModeFlags |= (uint8)Scc_EnergyTransferMode_DC_Core;
    break;

  case EXI_DIN_EVSESUPPORTED_ENERGY_TRANSFER_TYPE_DC_EXTENDED:
    EnergyTransferModeFlags |= (uint8)Scc_EnergyTransferMode_DC_Extended;
    break;

    /* invalid EnergyTransferType */
  default:
    Scc_StateM_ErrorHandling(Scc_SMER_ServiceDiscovery_InvalidEnergyTransferMode);
    break;
  }

  /* #20 provide the supported energy transfer modes to the application */
  Scc_Set_StateM_EnergyTransferModeFlags(EnergyTransferModeFlags);

#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
  /* disable VAS */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  Scc_StateM_Local.ReqCertDetails = FALSE;
#endif /* SCC_ENABLE_PNC_CHARGING */
  Scc_StateM_Local.ReqInetDetails = FALSE;
#endif /* SCC_SCHEMA_ISO */

}
#endif /* SCC_SCHEMA_DIN */

#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
/**********************************************************************************************************************
 *  Scc_StateM_Set_ISO_Ed2_DIS_IdentificationOptionList()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_Ed2_DIS_IdentificationOptionList(P2CONST(Exi_ISO_ED2_DIS_IdentificationOptionListType, AUTOMATIC, SCC_VAR_NOINIT) IdentificationOptionList)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8_least counter;
  boolean ContractFound = FALSE;
  boolean ExternalIdentificationFound = FALSE;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Iterate over the IdentificationOptionList from the Service Discovery Response and search for EIM and PnC */
  for (counter = 0; counter < IdentificationOptionList->IdentificationOptionCount; counter++)
  {
    /* check if contract based Identification was offered */
    if (EXI_ISO_ED2_DIS_IDENTIFICATION_OPTION_TYPE_CONTRACT ==
      IdentificationOptionList->IdentificationOption[counter])
    {
      /* PnC charging is available */
      ContractFound = TRUE;
    }
    /* check if external Identification was offered */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
    else if (EXI_ISO_ED2_DIS_IDENTIFICATION_OPTION_TYPE_EXTERNAL_IDENTIFICATION ==
      IdentificationOptionList->IdentificationOption[counter])
    {
      /* EIM charging is available */
      ExternalIdentificationFound = TRUE;
    }
  }

  /* #20 Check the combination of EIM and PnC charging */
  /* check if only contract based Identification was found */
  if (   (TRUE == ContractFound)
      && (FALSE == ExternalIdentificationFound) )
  {
    Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions = Scc_PaymentPrioritization_OnlyPnC;
  }
  /* check if only external Identification was found */
  else if (   (FALSE == ContractFound)
           && (TRUE == ExternalIdentificationFound) )
  {
    Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions = Scc_PaymentPrioritization_OnlyEIM;
  }
  /* check if both contract based and external Identification was found */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
  else if (   (TRUE == ContractFound)
           && (TRUE == ExternalIdentificationFound) )
  {
    Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions = Scc_PaymentPrioritization_PrioritizePnC;
  }

}

/**********************************************************************************************************************
 *  Scc_StateM_Set_ISO_Ed2_DIS_EnergyTransferServiceList()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_Ed2_DIS_EnergyTransferServiceList(P2CONST(Exi_ISO_ED2_DIS_ServiceListType, AUTOMATIC, SCC_VAR_NOINIT) EnergyTransferServiceList)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2CONST(Exi_ISO_ED2_DIS_ServiceType, AUTOMATIC, SCC_VAR_NOINIT) ServicePtr;

  /* ----- Implementation ----------------------------------------------- */
  ServicePtr = EnergyTransferServiceList->Service;
  Scc_StateM_RxEVSE.RxEnergyTransferServiceIDFlags = 0;

  /* #10 Iterate over EnergyTransferServiceList and set the service */
  while ( ServicePtr != NULL_PTR )
  {
    switch ( (Scc_StateM_SupportedServiceIDType)ServicePtr->ServiceID ) /* PRQA S 4342 */ /* MD_Scc_Exi_Generic */
    {
      case Scc_SID_AC_Charging:
      {
        Scc_StateM_RxEVSE.RxEnergyTransferServiceIDFlags |= (uint16)Scc_ET_SID_AC_Charging;
        break;
      }
      case Scc_SID_DC_Charging:
      {
        Scc_StateM_RxEVSE.RxEnergyTransferServiceIDFlags |= (uint16)Scc_ET_SID_DC_Charging;
        break;
      }
      case Scc_SID_WPT_Charging:
      {
        Scc_StateM_RxEVSE.RxEnergyTransferServiceIDFlags |= (uint16)Scc_ET_SID_WPT_Charging;
        break;
      }
      case Scc_SID_ACD_Charging:
      {
        Scc_StateM_RxEVSE.RxEnergyTransferServiceIDFlags |= (uint16)Scc_ET_SID_ACD_Charging;
        break;
      }
      case Scc_SID_AC_BPT:
      {
        Scc_StateM_RxEVSE.RxEnergyTransferServiceIDFlags |= (uint16)Scc_ET_SID_AC_BPT;
        break;
      }
      case Scc_SID_DC_BPT:
      {
        Scc_StateM_RxEVSE.RxEnergyTransferServiceIDFlags |= (uint16)Scc_ET_SID_DC_BPT;
        break;
      }
      default: /* PRQA S 2016 */ /* MD_MSR_EmptyClause */
        break;
    }
    ServicePtr = ServicePtr->NextServicePtr;
  }

  /* #20 Report the EnergyTransferServiceIDFlags to the application */
  Scc_Set_StateM_EnergyTransferServiceIDFlags(Scc_StateM_RxEVSE.RxEnergyTransferServiceIDFlags);

} /* PRQA S 6030 */ /* MD_MSR_STCYC */

/**********************************************************************************************************************
 *  Scc_StateM_Set_ISO_Ed2_DIS_VASList()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_Ed2_DIS_VASList(P2CONST(Exi_ISO_ED2_DIS_ServiceListType, AUTOMATIC, SCC_VAR_NOINIT) VASList, Exi_BitType Flag)
{
  /* ----- Local Variables ---------------------------------------------- */
  uint8 VasIdFlags = 0; /* PRQA S 0781 2 */ /* MD_Scc_0781 */
  P2CONST(Exi_ISO_ED2_DIS_ServiceType, AUTOMATIC, SCC_VAR_NOINIT) ServicePtr;

  /* ----- Implementation ----------------------------------------------- */
  if ( Flag == TRUE )
  {
    ServicePtr = VASList->Service;

    /* #10 Iterate over all services */
    while ( ServicePtr != NULL_PTR )
    {
      switch ( (Scc_StateM_SupportedServiceIDType)ServicePtr->ServiceID ) /* PRQA S 4342 */ /* MD_Scc_Exi_Generic */
      {
        case Scc_SID_WPT_FinePositioning:
        {
          VasIdFlags |= (uint8)Scc_VasId_WPT_FinePositioning;
          break;
        }
        case Scc_SID_WPT_Pairing:
        {
          VasIdFlags |= (uint8)Scc_VasId_WPT_Pairing;
          break;
        }
        case Scc_SID_WPT_InitialAlignmentCheck:
        {
          VasIdFlags |= (uint8)Scc_VasId_WPT_InitialAlignmentCheck;
          break;
        }
        case Scc_SID_Certificate_ISO_Ed2_DIS:
        {
          VasIdFlags |= (uint8)Scc_VasId_Certificate;
          break;
        }
        case Scc_SID_Internet_ISO_Ed2_DIS:
        {
          VasIdFlags |= (uint8)Scc_VasId_Internet;
          break;
        }
        case Scc_SID_SystemStatus_ISO_Ed2_DIS:
        {
          VasIdFlags |= (uint8)Scc_VasId_SystemStatus;
          break;
        }
        case Scc_SID_ParkingAssistance_ISO_Ed2_DIS:
        {
          VasIdFlags |= (uint8)Scc_VasId_ParkingAssistance;
          break;
        }
        default: /* PRQA S 2016 */ /* MD_MSR_EmptyClause */
          break;
      }

      ServicePtr = ServicePtr->NextServicePtr;
    }

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
    if ( (VasIdFlags & (uint8)Scc_VasId_Certificate) == 0u )
    {
      /* #20 Certificate Service was not offered. */
      Scc_StateM_Local.ReqCertDetails = FALSE;
    }
#endif /* SCC_ENABLE_PNC_CHARGING */
    if ( (VasIdFlags & (uint8)Scc_VasId_WPT_FinePositioning) == 0u )
    {
      /* #30 Fine Positioning was not offered. */
      Scc_StateM_Local.ReqWPTFinePositioningDetails = FALSE;
    }
    if ( (VasIdFlags & (uint8)Scc_VasId_WPT_Pairing) == 0u )
    {
      /* #40 WPT Pairing was not offered. */
      Scc_StateM_Local.ReqWPTPairingDetails = FALSE;
    }
    if ( (VasIdFlags & (uint8)Scc_VasId_WPT_InitialAlignmentCheck) == 0u )
    {
      /* #50 Initial Alignment Check was not offered. */
      Scc_StateM_Local.ReqWPTInitialAlignmentCheckDetails = FALSE;
    }

    /* #60 Set Value Added Serive flags and report to application */
    Scc_StateM_RxEVSE.VasIdFlags = VasIdFlags;
    Scc_Set_StateM_VasIdFlags(VasIdFlags);
  }
} /* PRQA S 6030, 6010 */ /* MD_MSR_STCYC, MD_MSR_STPTH */
#endif /* SCC_SCHEMA_ISO_ED2 */

#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
/**********************************************************************************************************************
 *  Scc_StateM_Set_Core_TrcvLinkState()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_ServiceIDTx(uint16 ServiceID)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set provided ServiceID from Serivce Detail Response. */
  Scc_StateM_RxEVSE.ProvServiceID = ServiceID;

}
/**********************************************************************************************************************
 *  Scc_StateM_Set_ISO_ServiceParameterList()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_ServiceParameterList(
  P2CONST(Exi_ISO_ServiceParameterListType, AUTOMATIC, SCC_VAR_NOINIT) ServiceParameterList, Exi_BitType ServiceParameterListFlag)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set ServiceParameterList from Serivce Detail Response. */
  if ( 1u == ServiceParameterListFlag )
  {
    Scc_StateM_RxEVSE.ServiceParameterListPtr = ServiceParameterList;
  }
  else
  {
    Scc_StateM_RxEVSE.ServiceParameterListPtr = (Exi_ISO_ServiceParameterListType*)NULL_PTR;
  }

}
#endif /* SCC_SCHEMA_ISO */

#if (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
/**********************************************************************************************************************
 *  Scc_StateM_Set_ISO_Ed2_DIS_ServiceID()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_Ed2_DIS_ServiceID(uint16 ServiceID)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set Service IDs from Serivce Detail Response. */
  Scc_StateM_RxEVSE.ProvServiceID = ServiceID;

}

/**********************************************************************************************************************
 *  Scc_StateM_Set_ISO_Ed2_DIS_ServiceParameterList()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_Ed2_DIS_ServiceParameterList(P2CONST(Exi_ISO_ED2_DIS_ServiceParameterListType, AUTOMATIC, SCC_VAR_NOINIT) ServiceParameterList, Exi_BitType ServiceParameterListFlag)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set ServiceParameterList from Serivce Detail Response. */
  if ( 1u == ServiceParameterListFlag )
  {
    Scc_StateM_RxEVSE.ServiceParameterListPtr_ISO_Ed2_DIS = ServiceParameterList;
  }
  else
  {
    Scc_StateM_RxEVSE.ServiceParameterListPtr_ISO_Ed2_DIS = (Exi_ISO_ED2_DIS_ServiceParameterListType*)NULL_PTR;
  }
}
#endif /* SCC_SCHEMA_ISO_ED2 */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/* ChargingStatus */
/**********************************************************************************************************************
 *  Scc_StateM_Set_ISO_PnC_ReceiptRequired()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_PnC_ReceiptRequired(boolean ReceiptRequired, Exi_BitType Flag)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set ServiceParameterList from ChargingStatus or CurrentDemand Reponse. */
  if ( 1u == Flag )
  {
    Scc_StateM_RxEVSE.ReceiptRequired = ReceiptRequired;
  }
  else
  {
    Scc_StateM_RxEVSE.ReceiptRequired = FALSE;
  }

}
#endif /* SCC_ENABLE_PNC_CHARGING */

#if (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
# if ( defined SCC_ENABLE_PNC_CHARGING ) && ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_Set_ISO_Ed2_DIS_PnC_ReceiptRequired()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_Ed2_DIS_PnC_ReceiptRequired(boolean ReceiptRequired, Exi_BitType Flag)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set ServiceParameterList from ChargingStatus or CurrentDemand or PowerDemand Reponse. */
  if ( 1u == Flag )
  {
    Scc_StateM_RxEVSE.ReceiptRequired = ReceiptRequired;
  }
  else
  {
    Scc_StateM_RxEVSE.ReceiptRequired = FALSE;
  }

}
# endif /* SCC_ENABLE_PNC_CHARGING */
#endif /* SCC_SCHEMA_ISO_ED2 */
/**********************************************************************************************************************
 *  Scc_StateM_Get_Core_MsgTrig()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_Core_MsgTrig(P2VAR(Scc_MsgTrigType, AUTOMATIC, SCC_VAR_NOINIT) MsgTrig)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the current message trigger. */
  *MsgTrig = Scc_StateM_TxCore.MsgTrig;

}
/**********************************************************************************************************************
 *  Scc_StateM_Get_Core_CyclicMsgTrigRx()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_Core_CyclicMsgTrigRx(P2VAR(boolean, AUTOMATIC, SCC_VAR_NOINIT) CyclicMsgTrig)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the cyclic message trigger. */
  *CyclicMsgTrig = Scc_StateM_TxCore.CyclicMsgTrig;

}

/**********************************************************************************************************************
 *  Scc_StateM_Get_ISO_Ed2_DIS_EVProcessing()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
#if (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_Ed2_DIS_EVProcessing(P2VAR(Exi_ISO_ED2_DIS_processingType, AUTOMATIC, SCC_VAR_NOINIT) EvProcessing)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the EV Processing status. */
  *EvProcessing = Scc_StateM_TxEVSE.EVProcessing;

}
#endif /* SCC_SCHEMA_ISO_ED2 */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_Get_SLAC_QCAIdleTimer()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_SLAC_QCAIdleTimer(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) QCAIdleTimer)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get QCAIdleTimer. */
  *QCAIdleTimer = (uint16)Scc_ConfigValue_StateM_QCAIdleTimer;
}

/**********************************************************************************************************************
 *  Scc_StateM_Get_SLAC_StartMode()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_SLAC_StartMode(P2VAR(EthTrcv_30_Ar7000_Slac_StartModeType, AUTOMATIC, SCC_VAR_NOINIT) SLACStartMode)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get SLACStartMode. */
  *SLACStartMode = (EthTrcv_30_Ar7000_Slac_StartModeType)Scc_ConfigValue_StateM_SLACStartMode;
}
#endif /* SCC_ENABLE_SLAC_HANDLING */

/**********************************************************************************************************************
 *  Scc_StateM_Set_Core_TLS_CertChain()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 */ /* PRQA S 3673 1 */ /* MD_Scc_VariantDependent */
FUNC(void, SCC_RTE_CODE) Scc_StateM_Set_Core_TLS_CertChain(P2CONST(uint8, AUTOMATIC, SCC_RTE_DATA) certChainPtr, P2VAR(uint8, AUTOMATIC, SCC_RTE_DATA) validationResultPtr)
{
  /* ----- Local Variables ---------------------------------------------- */
#if (0 == SCC_DOMAIN_COMPONENT_TLS_LEAF_LENGTH)
  SCC_DUMMY_STATEMENT(certChainPtr); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */
  SCC_DUMMY_STATEMENT(validationResultPtr); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */
#else /* SCC_DOMAIN_COMPONENT_TLS_LEAF_LENGTH */

  uint32                     CertLen; /* PRQA S 0781 */ /* MD_Scc_0781 */

  /* ----- Implementation ----------------------------------------------- */

  /* read certLen from first 3 bytes of certChainPtr */
  SCC_GET_UINT24(certChainPtr, 0u, CertLen);

  /* #10 search for the private environment value in the server certificate */
  if( (Std_ReturnType)E_OK == Scc_SearchDomainComponentForValue(&certChainPtr[3], (uint16)CertLen, &Scc_DomainComponentTlsLeafCert[0], SCC_DOMAIN_COMPONENT_TLS_LEAF_LENGTH) )
  {
    /* Set validation result to ok for TLS */
    *validationResultPtr = TCPIP_TLS_VALIDATION_OK;
  }

#endif /* SCC_DOMAIN_COMPONENT_TLS_LEAF_LENGTH */
}

/**********************************************************************************************************************
 *  Scc_StateM_Get_Core_SDPSecurityRx()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_Core_SDPSecurityRx(P2VAR(Scc_SDPSecurityType, AUTOMATIC, SCC_VAR_NOINIT) SDPSecurity)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the Security flag (TLs or Tcp) for SECC Discovery Protocol Request. */
  *SDPSecurity = Scc_StateM_TxCore.ReqSDPSecurity;

}

/**********************************************************************************************************************
 *  Scc_StateM_Get_Core_ForceSAPSchema()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_Core_ForceSAPSchema(P2VAR(Scc_ForceSAPSchemasType, AUTOMATIC, SCC_VAR_NOINIT) ForceSAPSchema)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Do not force a Schema for the SupportedAppProtocolReq. */
  *ForceSAPSchema = Scc_ForceSAPSchemas_None;

}

/**********************************************************************************************************************
 *  Scc_StateM_Get_ISO_ServiceScope()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_ServiceScope(P2VAR(Exi_ISO_serviceScopeType, AUTOMATIC, SCC_VAR_NOINIT) ServiceScope,  /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
  P2VAR(boolean, AUTOMATIC, SCC_VAR_NOINIT) Flag)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the ServiceScope for ServiceDiscoveryReq. */
  *Flag = FALSE;

  SCC_DUMMY_STATEMENT(ServiceScope); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */

}

/**********************************************************************************************************************
 *  Scc_StateM_Get_ISO_ServiceCategory()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_ServiceCategory(P2VAR(Exi_ISO_serviceCategoryType, AUTOMATIC, SCC_VAR_NOINIT) ServiceCategory,  /* PRQA S 3673 */ /* MD_MSR_Rule8.13 */
  P2VAR(boolean, AUTOMATIC, SCC_VAR_NOINIT) Flag)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the ServiceCategory for ServiceDiscoveryReq. */
  *Flag = FALSE;

  SCC_DUMMY_STATEMENT(ServiceCategory); /* PRQA S 3112 */ /* MD_MSR_DummyStmt */

}
#endif /* SCC_SCHEMA_ISO */

#if ( ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) )
/**********************************************************************************************************************
 *  Scc_StateM_Get_DIN_ServiceCategory()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_DIN_ServiceCategory(P2VAR(Exi_DIN_serviceCategoryType, AUTOMATIC, SCC_VAR_NOINIT) ServiceCategory,
  P2VAR(boolean, AUTOMATIC, SCC_VAR_NOINIT) Flag)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the ServiceCategory for ServiceDiscoveryReq (EVcharging). */
  *Flag = TRUE;

  /* [V2G-DC-557] In the scope of DIN SPEC SPEC 70121, if the optional element ServiceCategory is used, it shall always contain the value "EVcharging". */
  *ServiceCategory = EXI_DIN_SERVICE_CATEGORY_TYPE_EVCHARGING;
}
#endif /* SCC_SCHEMA_DIN */

#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
/**********************************************************************************************************************
 *  Scc_StateM_Get_ISO_ServiceIDRx()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_ServiceIDRx(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) ServiceID)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the ServiceID for ServiceDetailReq. */
  *ServiceID = Scc_StateM_TxEVSE.ReqServiceID;

}
#endif /* SCC_SCHEMA_ISO */

#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
/**********************************************************************************************************************
 *  Scc_StateM_Get_ISO_Ed2_DIS_ServiceID()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_Ed2_DIS_ServiceID(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) ServiceID)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the ServiceID for ServiceDetailReq. */
  *ServiceID = Scc_StateM_TxEVSE.ReqServiceID;

}
#endif /* SCC_SCHEMA_ISO_ED2 */

#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
/**********************************************************************************************************************
 *  Scc_StateM_Get_ISO_SelectedPaymentOption()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_SelectedPaymentOption(P2VAR(Exi_ISO_paymentOptionType, AUTOMATIC, SCC_VAR_NOINIT) SelectedPaymentOption)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the SelectedPaymentOption for PaymentServiceSelectionReq. */
  switch ( Scc_StateM_TxEVSE.SelectedPaymentOption )
  {
  case Scc_PaymentOption_PnC:
    *SelectedPaymentOption = EXI_ISO_PAYMENT_OPTION_TYPE_CONTRACT;
    break;

  default: /* case Scc_PaymentOption_EIM: */
    *SelectedPaymentOption = EXI_ISO_PAYMENT_OPTION_TYPE_EXTERNAL_PAYMENT;
    break;
  }

}

/**********************************************************************************************************************
 *  Scc_StateM_Get_ISO_SelectedServiceListPtr()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_SelectedServiceListPtr(P2VAR(Exi_ISO_SelectedServiceListType*, AUTOMATIC, SCC_VAR_NOINIT) SelectedServiceListPtr)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the SelectedServiceListPtr for PaymentServiceSelectionReq. */
  Scc_StateM_TxEVSE.SelectedServiceList.SelectedService = &Scc_StateM_TxEVSE.SelectedServices[0];
  (*SelectedServiceListPtr) = &Scc_StateM_TxEVSE.SelectedServiceList;

}
#endif /* SCC_SCHEMA_ISO */

#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
/**********************************************************************************************************************
 *  Scc_StateM_Get_ISO_Ed2_DIS_SelectedIdentificationOption()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_Ed2_DIS_SelectedIdentificationOption(P2VAR(Exi_ISO_ED2_DIS_identificationOptionType, AUTOMATIC, SCC_VAR_NOINIT) SelectedPaymentOption)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the SelectedIdentificationOption for ServiceSelectionReq. */
  switch ( Scc_StateM_TxEVSE.SelectedPaymentOption )
  {
  case Scc_PaymentOption_PnC:
    *SelectedPaymentOption = EXI_ISO_ED2_DIS_IDENTIFICATION_OPTION_TYPE_CONTRACT;
    break;

  default: /* case Scc_PaymentOption_EIM: */
    *SelectedPaymentOption = EXI_ISO_ED2_DIS_IDENTIFICATION_OPTION_TYPE_EXTERNAL_IDENTIFICATION;
    break;
  }

}

/**********************************************************************************************************************
 *  Scc_StateM_Get_ISO_Ed2_DIS_SelectedEnergyTransferService()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_Ed2_DIS_SelectedEnergyTransferService(P2VAR(Exi_ISO_ED2_DIS_SelectedServiceType, AUTOMATIC, SCC_VAR_NOINIT) SelectedEnergyTransferService)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the SelectedEnergyTransferService for PaymentServiceSelectionReq. */
  *SelectedEnergyTransferService = Scc_StateM_TxEVSE.SelectedEnergyTransferService;

}

/**********************************************************************************************************************
 *  Scc_StateM_Get_ISO_Ed2_DIS_SelectedVASListPtr()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_Ed2_DIS_SelectedVASListPtr(P2VAR(Exi_ISO_ED2_DIS_SelectedServiceListType, AUTOMATIC, SCC_VAR_NOINIT) SelectedVASListPtr, P2VAR(boolean, AUTOMATIC, SCC_VAR_NOINIT) SelectedVASListFlag)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the SelectedVASListPtr for PaymentServiceSelectionReq. */
  *SelectedVASListFlag = 0;

  if(Scc_StateM_TxEVSE.SelectedVasList.SelectedService != NULL_PTR)
  {
    if (Scc_StateM_TxEVSE.SelectedVasList.SelectedService->ServiceID != 0u)
    {
      *SelectedVASListPtr = Scc_StateM_TxEVSE.SelectedVasList;
      *SelectedVASListFlag = 1;
    }
  }

}

/**********************************************************************************************************************
 *  Scc_StateM_Get_ISO_Ed2_DIS_SelectedVasIdFlags()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_Ed2_DIS_SelectedVasIdFlags(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) vasIdFlags)
{
  /* ----- Local Variables ---------------------------------------------- */
  P2VAR(Exi_ISO_ED2_DIS_SelectedServiceType, AUTOMATIC, SCC_VAR_NOINIT) selectedServicePtr;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the SelectedVasIdFlags for PaymentServiceSelectionReq. */
  if(Scc_StateM_TxEVSE.SelectedVasList.SelectedService != NULL_PTR)
  {
    selectedServicePtr = Scc_StateM_TxEVSE.SelectedVasList.SelectedService;

    while(selectedServicePtr != NULL_PTR)
    {
      switch ((Scc_StateM_SupportedServiceIDType)selectedServicePtr->ServiceID) /* PRQA S 4342 */ /* MD_Scc_Exi_Generic */
      {
        case Scc_SID_WPT_FinePositioning:
        {
          *vasIdFlags |= (uint16)Scc_VasId_WPT_FinePositioning;
          break;
        }
        case Scc_SID_WPT_Pairing:
        {
          *vasIdFlags |= (uint16)Scc_VasId_WPT_Pairing;
          break;
        }
        case Scc_SID_WPT_InitialAlignmentCheck:
        {
          *vasIdFlags |= (uint16)Scc_VasId_WPT_InitialAlignmentCheck;
          break;
        }
        case Scc_SID_Certificate_ISO_Ed2_DIS:
        {
          *vasIdFlags |= (uint16)Scc_VasId_Certificate;
          break;
        }
        case Scc_SID_Internet_ISO_Ed2_DIS:
        {
          *vasIdFlags |= (uint16)Scc_VasId_Internet;
          break;
        }
        case Scc_SID_SystemStatus_ISO_Ed2_DIS:
        {
          *vasIdFlags |= (uint16)Scc_VasId_SystemStatus;
          break;
        }
        case Scc_SID_ParkingAssistance_ISO_Ed2_DIS:
        {
          *vasIdFlags |= (uint16)Scc_VasId_ParkingAssistance;
          break;
        }
        default: /* PRQA S 2016 */ /* MD_MSR_EmptyClause */
          break;
      }
      selectedServicePtr = selectedServicePtr->NextSelectedServicePtr;
    }
  }
}
#endif /* SCC_SCHEMA_ISO_ED2 */

/**********************************************************************************************************************
 *  Scc_StateM_Get_ISO_RequestedEnergyTransferMode()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_RequestedEnergyTransferMode(P2VAR(Exi_ISO_EnergyTransferModeType, AUTOMATIC, SCC_VAR_NOINIT) EnergyTransferMode)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the RequestedEnergyTransferMode for ChargeParameterDiscoveryReq. */
  switch ( Scc_StateM_TxEVSE.EnergyTransferMode )
  {

#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )

    case Scc_EnergyTransferMode_DC_Core:
      *EnergyTransferMode = EXI_ISO_ENERGY_TRANSFER_MODE_TYPE_DC_CORE;
      break;

    case Scc_EnergyTransferMode_DC_Extended:
      *EnergyTransferMode = EXI_ISO_ENERGY_TRANSFER_MODE_TYPE_DC_EXTENDED;
      break;

    case Scc_EnergyTransferMode_DC_Combo_Core:
      *EnergyTransferMode = EXI_ISO_ENERGY_TRANSFER_MODE_TYPE_DC_COMBO_CORE;
      break;

    case Scc_EnergyTransferMode_DC_Unique:
      *EnergyTransferMode = EXI_ISO_ENERGY_TRANSFER_MODE_TYPE_DC_UNIQUE;
      break;

#endif /* SCC_CHARGING_DC */

    default:
#if(SCC_DEV_ERROR_REPORT == STD_ON)
      (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_STATEM_CORE_GET_CBK, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
      break;
  }

}
#endif /* SCC_SCHEMA_ISO */
#if ( ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) )
/**********************************************************************************************************************
 *  Scc_StateM_Get_DIN_RequestedEnergyTransferMode()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_DIN_RequestedEnergyTransferMode(P2VAR(Exi_DIN_EVRequestedEnergyTransferType, AUTOMATIC, SCC_VAR_NOINIT) EnergyTransferMode)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the RequestedEnergyTransferMode for ChargeParameterDiscoveryReq. */
  switch ( Scc_StateM_TxEVSE.EnergyTransferMode )
  {
    case Scc_EnergyTransferMode_DC_Core:
      *EnergyTransferMode = EXI_DIN_EVREQUESTED_ENERGY_TRANSFER_TYPE_DC_CORE;
      break;

    case Scc_EnergyTransferMode_DC_Extended:
      *EnergyTransferMode = EXI_DIN_EVREQUESTED_ENERGY_TRANSFER_TYPE_DC_EXTENDED;
      break;

    default:
#if(SCC_DEV_ERROR_REPORT == STD_ON)
      (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_STATEM_CORE_GET_CBK, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
      break;
  }

}

#endif /* SCC_SCHEMA_DIN */

/**********************************************************************************************************************
 *  Scc_StateM_Get_ISO_Ed2_DIS_ChargeProgress()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_Ed2_DIS_ChargeProgress(P2VAR(Exi_ISO_ED2_DIS_chargeProgressType, AUTOMATIC, SCC_VAR_NOINIT) ChargeProgress)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the ChargeProgress for PowerDeliveryReq. */
  switch ( Scc_StateM_TxEVSE.ChargeStatus )
  {
  case Scc_ChargeStatus_Starting:
    *ChargeProgress = EXI_ISO_ED2_DIS_CHARGE_PROGRESS_TYPE_START;
    break;

  case Scc_ChargeStatus_Stopping:
    *ChargeProgress = EXI_ISO_ED2_DIS_CHARGE_PROGRESS_TYPE_STOP;
    break;

  case Scc_ChargeStatus_Renegotiating:
    *ChargeProgress = EXI_ISO_ED2_DIS_CHARGE_PROGRESS_TYPE_RENEGOTIATE;
    break;

  default:
#if(SCC_DEV_ERROR_REPORT == STD_ON)
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_STATEM_CORE_GET_CBK, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
    break;
  }

}
#endif /* SCC_SCHEMA_ISO_ED2 */

#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
/**********************************************************************************************************************
 *  Scc_StateM_Get_ISO_ChargeProgress()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_ChargeProgress(P2VAR(Exi_ISO_chargeProgressType, AUTOMATIC, SCC_VAR_NOINIT) ChargeProgress)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the ChargeProgress for PowerDeliveryReq. */
  switch ( Scc_StateM_TxEVSE.ChargeStatus )
  {
  case Scc_ChargeStatus_Starting:
    *ChargeProgress = EXI_ISO_CHARGE_PROGRESS_TYPE_START;
    break;

  case Scc_ChargeStatus_Stopping:
    *ChargeProgress = EXI_ISO_CHARGE_PROGRESS_TYPE_STOP;
    break;

  case Scc_ChargeStatus_Renegotiating:
    *ChargeProgress = EXI_ISO_CHARGE_PROGRESS_TYPE_RENEGOTIATE;
    break;

  default:
#if(SCC_DEV_ERROR_REPORT == STD_ON)
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_STATEM_CORE_GET_CBK, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
    break;
  }

}
#endif /* SCC_SCHEMA_ISO */
#if ( ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) )
/**********************************************************************************************************************
 *  Scc_StateM_Get_DIN_ReadyToChargeState()
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
FUNC(void, SCC_CODE) Scc_StateM_Get_DIN_ReadyToChargeState(P2VAR(boolean, AUTOMATIC, SCC_VAR_NOINIT) ReadyToChargeState)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the ReadyToChargeState for PowerDeliveryReq. */
  switch ( Scc_StateM_TxEVSE.ChargeStatus )
  {
  case Scc_ChargeStatus_Starting:
    *ReadyToChargeState = TRUE;
    break;

  case Scc_ChargeStatus_Stopping:
    *ReadyToChargeState = FALSE;
    break;

  default:
#if(SCC_DEV_ERROR_REPORT == STD_ON)
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_STATEM_CORE_GET_CBK, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
    break;
  }

}
#endif /* SCC_SCHEMA_DIN */

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_PrepareSLAC
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_PrepareSLAC(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal = Scc_ReturnType_NotOK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Get the current status of the cable */
  Scc_Get_StateM_ControlPilotState(&Scc_StateM_Local.ControlPilotState);
  /* wait for State B, C or D */
  if ( Scc_ControlPilotState_State_B > Scc_StateM_Local.ControlPilotState )
  {
    retVal = Scc_ReturnType_Pending;
  }
  else
  {
    /* #20 start SLAC */
    Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_SLAC;
    /* set the cyclic trigger */
    Scc_StateM_TxCore.CyclicMsgTrig = TRUE;

    retVal = Scc_ReturnType_OK;
  }

  return retVal;
}
#endif /* SCC_ENABLE_SLAC_HANDLING */

/**********************************************************************************************************************
 *  Scc_StateM_PrepareSECCDiscoveryProtocolReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareSECCDiscoveryProtocolReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Set the current status of Security (TLS) */
#if (( defined SCC_ENABLE_TLS ) && ( SCC_ENABLE_TLS == STD_ON ))
  Scc_StateM_TxCore.ReqSDPSecurity = Scc_SDPSecurity_Tls;
#else
  Scc_StateM_TxCore.ReqSDPSecurity = Scc_SDPSecurity_None;
#endif /* SCC_ENABLE_TLS */

  /* #20 trigger the SDP request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_SECCDiscoveryProtocol;

}

/**********************************************************************************************************************
 *  Scc_StateM_PrepareTlConnection
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareTlConnection(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the TL connection establishment */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_TransportLayer;

}

/**********************************************************************************************************************
 *  Scc_StateM_PrepareSupportedAppProtocolReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareSupportedAppProtocolReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the SAP request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_SupportedAppProtocol;

}

/**********************************************************************************************************************
 *  Scc_StateM_PrepareSessionSetupReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareSessionSetupReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the SessionSetup request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_SessionSetup;

}

/**********************************************************************************************************************
 *  Scc_StateM_PrepareServiceDiscoveryReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareServiceDiscoveryReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the ServiceDiscovery request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_ServiceDiscovery;

}

#if ( (( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0u )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )) )
/**********************************************************************************************************************
 *  Scc_StateM_PrepareServiceDetailReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareServiceDetailReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )
  /* #10 Check if EnergyTransferService has to be handled for ISO Ed2  */
  if (   ( 0u != Scc_StateM_TxEVSE.TxEnergyTransferServiceIDFlags )
      && ( Scc_SAPSchemaIDs_ISO_Ed2_DIS == Scc_StateM_RxEVSE.SAPSchemaID ) )
  {
    /* #20 Check if AC_Charging was requested from the application (TxEnergyTransferServiceIDFlags) and the EVSE offers the service (RxEnergyTransferServiceIDFlags) */
    if ( ((uint16)Scc_ET_SID_AC_Charging == ((uint16)Scc_ET_SID_AC_Charging & Scc_StateM_TxEVSE.TxEnergyTransferServiceIDFlags))
      && ((uint16)Scc_ET_SID_AC_Charging == ((uint16)Scc_ET_SID_AC_Charging & Scc_StateM_RxEVSE.RxEnergyTransferServiceIDFlags)))
    {
      /* remove AC_Charging from TxEnergyTransferServiceIDFlags */
      Scc_StateM_TxEVSE.TxEnergyTransferServiceIDFlags -= (uint16)Scc_ET_SID_AC_Charging;
      Scc_StateM_TxEVSE.ReqServiceID = (uint16)Scc_SID_AC_Charging;
      /* send a ServiceDetailReq requesting more details about the AC Charging PLC */
      Scc_StateM_Local.ACChargingDetailsRequested = TRUE;
    }
    /* #30 Check if DC_Charging was requested from the application (TxEnergyTransferServiceIDFlags) and the EVSE offers the service (RxEnergyTransferServiceIDFlags) */
    else if ( ((uint16)Scc_ET_SID_DC_Charging == ((uint16)Scc_ET_SID_DC_Charging & Scc_StateM_TxEVSE.TxEnergyTransferServiceIDFlags))
           && ((uint16)Scc_ET_SID_DC_Charging == ((uint16)Scc_ET_SID_DC_Charging & Scc_StateM_RxEVSE.RxEnergyTransferServiceIDFlags)))
    {
      /* remove DC_Charging from TxEnergyTransferServiceIDFlags */
      Scc_StateM_TxEVSE.TxEnergyTransferServiceIDFlags -= (uint16)Scc_ET_SID_DC_Charging;
      Scc_StateM_TxEVSE.ReqServiceID = (uint16)Scc_SID_DC_Charging;
      /* send a ServiceDetailReq requesting more details about the AC Charging PLC */
      Scc_StateM_Local.DCChargingDetailsRequested = TRUE;
    }
    /* #40 Check if WPT_Charging was requested from the application (TxEnergyTransferServiceIDFlags) and the EVSE offers the service (RxEnergyTransferServiceIDFlags) */
    else if ( ((uint16)Scc_ET_SID_WPT_Charging == ((uint16)Scc_ET_SID_WPT_Charging & Scc_StateM_TxEVSE.TxEnergyTransferServiceIDFlags))
           && ((uint16)Scc_ET_SID_WPT_Charging == ((uint16)Scc_ET_SID_WPT_Charging & Scc_StateM_RxEVSE.RxEnergyTransferServiceIDFlags)))
    {
      /* remove WPT_Charging from TxEnergyTransferServiceIDFlags */
      Scc_StateM_TxEVSE.TxEnergyTransferServiceIDFlags -= (uint16)Scc_ET_SID_WPT_Charging;
      Scc_StateM_TxEVSE.ReqServiceID = (uint16)Scc_SID_WPT_Charging;

      /* Set Service for FinePositioningSetup, FinePositioning and Pairing */

      /* send a ServiceDetailReq requesting more details about the WPT Charging */
      Scc_StateM_Local.WPTChargingDetailsRequested = TRUE;

      /* Service detail has to be requested for below service Ids if WPT is requested */
      Scc_StateM_Local.ReqWPTFinePositioningDetails = TRUE;
      Scc_StateM_Local.WPTFinePositioningDetailsRequested = FALSE;
      Scc_StateM_Local.ReqWPTPairingDetails = TRUE;
      Scc_StateM_Local.WPTPairingDetailsRequested = FALSE;
      Scc_StateM_Local.ReqWPTInitialAlignmentCheckDetails = TRUE;
      Scc_StateM_Local.WPTInitialAlignmentCheckDetailsRequested = FALSE;
    }
    /* #50 Check if AC_BPT Charging was requested from the application (TxEnergyTransferServiceIDFlags) and the EVSE offers the service (RxEnergyTransferServiceIDFlags) */
    else if ( ((uint16)Scc_ET_SID_AC_BPT == ((uint16)Scc_ET_SID_AC_BPT & Scc_StateM_TxEVSE.TxEnergyTransferServiceIDFlags))
           && ((uint16)Scc_ET_SID_AC_BPT == ((uint16)Scc_ET_SID_AC_BPT & Scc_StateM_RxEVSE.RxEnergyTransferServiceIDFlags)))
    {
      /* remove DC_Charging from TxEnergyTransferServiceIDFlags */
      Scc_StateM_TxEVSE.TxEnergyTransferServiceIDFlags -= (uint16)Scc_ET_SID_AC_BPT;
      Scc_StateM_TxEVSE.ReqServiceID = (uint16)Scc_SID_AC_BPT;
      /* send a ServiceDetailReq requesting more details about the AC Charging PLC */
      Scc_StateM_Local.BPTACChargingDetailsRequested = TRUE;
    }
    /* #60 Check if DC_BPT Charging was requested from the application (TxEnergyTransferServiceIDFlags) and the EVSE offers the service (RxEnergyTransferServiceIDFlags) */
    else if ( ((uint16)Scc_ET_SID_DC_BPT == ((uint16)Scc_ET_SID_DC_BPT & Scc_StateM_TxEVSE.TxEnergyTransferServiceIDFlags))
           && ((uint16)Scc_ET_SID_DC_BPT == ((uint16)Scc_ET_SID_DC_BPT & Scc_StateM_RxEVSE.RxEnergyTransferServiceIDFlags)))
    {
      /* remove DC_Charging from TxEnergyTransferServiceIDFlags */
      Scc_StateM_TxEVSE.TxEnergyTransferServiceIDFlags -= (uint16)Scc_ET_SID_DC_BPT;
      Scc_StateM_TxEVSE.ReqServiceID = (uint16)Scc_SID_DC_BPT;
      /* send a ServiceDetailReq requesting more details about the AC Charging PLC */
      Scc_StateM_Local.BPTDCChargingDetailsRequested = TRUE;
    }
    else
    {
      /* Not supported EnergyTransfers */
      /* Scc_ET_SID_ACD_Charging */
#if(SCC_DEV_ERROR_REPORT == STD_ON)
      (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_STATEM_PREP_SERV_DETAIL, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
    }
  }
  else
#endif /* SCC_SCHEMA_ISO_ED2 */



#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  /* #70 check if certificate details are required */
  if (   ( TRUE == Scc_StateM_Local.ReqCertDetails )
      && ( FALSE == Scc_StateM_Local.CertDetailsRequested ) )
  {
    /* #80 send a ServiceDetailReq requesting more details about the certificate services */
#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )
    if ( Scc_SAPSchemaIDs_ISO_Ed2_DIS == Scc_StateM_RxEVSE.SAPSchemaID )
    {
      Scc_StateM_TxEVSE.ReqServiceID = (uint16)Scc_SID_Certificate_ISO_Ed2_DIS;
    }
#endif /* SCC_SCHEMA_ISO_ED2 */
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
    if ( Scc_SAPSchemaIDs_ISO == Scc_StateM_RxEVSE.SAPSchemaID ) /* PRQA S 2004 */ /* MD_Scc_2004 */
    {
      Scc_StateM_TxEVSE.ReqServiceID = (uint16)Scc_SID_Certificate;
    }
#endif /* SCC_SCHEMA_ISO */
    Scc_StateM_Local.CertDetailsRequested = TRUE;
  }
  else
#endif /* SCC_ENABLE_PNC_CHARGING */
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
  /* #90 check if internet details are required for ISO Ed1 */
  if ( ( ( TRUE == Scc_StateM_Local.ReqInetDetails )
      && ( FALSE == Scc_StateM_Local.InetDetailsRequested ) )
      && ( Scc_SAPSchemaIDs_ISO == Scc_StateM_RxEVSE.SAPSchemaID ) )
  {
    /* #100 send a ServiceDetailReq requesting more details about the internet services */
    Scc_StateM_TxEVSE.ReqServiceID = (uint16)Scc_SID_Internet;
    Scc_StateM_Local.InetDetailsRequested = TRUE;
  }
  else
#endif /* SCC_SCHEMA_ISO */
#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )
  /* #110 check if internet details are required for ISO Ed2 */
  if ( ( ( TRUE == Scc_StateM_Local.ReqInetDetails )
      && ( FALSE == Scc_StateM_Local.InetDetailsRequested ) )
      && ( Scc_SAPSchemaIDs_ISO_Ed2_DIS == Scc_StateM_RxEVSE.SAPSchemaID ) )
  {
    /* #120 send a ServiceDetailReq requesting more details about the internet services */
    Scc_StateM_TxEVSE.ReqServiceID = (uint16)Scc_SID_Internet_ISO_Ed2_DIS;
    Scc_StateM_Local.InetDetailsRequested = TRUE;
  }
  /* #130 check if fine positioning is required for ISO Ed2 */
  else if ( ( (Scc_StateM_Local.ReqWPTFinePositioningDetails == TRUE)
         && (Scc_StateM_Local.WPTFinePositioningDetailsRequested == FALSE) )
         && (Scc_SAPSchemaIDs_ISO_Ed2_DIS == Scc_StateM_RxEVSE.SAPSchemaID) )
  {
    /* #140 send a ServiceDetailReq requesting more details about the fine positioning */
          Scc_StateM_TxEVSE.ReqServiceID = (uint16)Scc_SID_WPT_FinePositioning;
    Scc_StateM_Local.WPTFinePositioningDetailsRequested = TRUE;
  }
  /* #150 check if WPT pairing is required for ISO Ed2 */
  else if ( ( (Scc_StateM_Local.ReqWPTPairingDetails == TRUE)
         && (Scc_StateM_Local.WPTPairingDetailsRequested == FALSE) )
         && (Scc_SAPSchemaIDs_ISO_Ed2_DIS == Scc_StateM_RxEVSE.SAPSchemaID) )
  {
    /* #160 send a ServiceDetailReq requesting more details about the WPT pairing */
          Scc_StateM_TxEVSE.ReqServiceID = (uint16)Scc_SID_WPT_Pairing;
    Scc_StateM_Local.WPTPairingDetailsRequested = TRUE;
  }
  /* #170 check if Initial alignment check is required for ISO Ed2 */
  else if ( ( (Scc_StateM_Local.ReqWPTInitialAlignmentCheckDetails == TRUE)
         && (Scc_StateM_Local.WPTInitialAlignmentCheckDetailsRequested == FALSE) )
         && (Scc_SAPSchemaIDs_ISO_Ed2_DIS == Scc_StateM_RxEVSE.SAPSchemaID) )
  {
    /* #180 send a ServiceDetailReq requesting more details about the Initial alignment check */
          Scc_StateM_TxEVSE.ReqServiceID = (uint16)Scc_SID_WPT_InitialAlignmentCheck;
    Scc_StateM_Local.WPTInitialAlignmentCheckDetailsRequested = TRUE;
  }
  else
#endif /* SCC_SCHEMA_ISO_ED2 */
  {
    /* illegal state */
#if(SCC_DEV_ERROR_REPORT == STD_ON)
    (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_STATEM_PREP_SERV_DETAIL, SCC_DET_INV_STATE);
#endif /* SCC_DEV_ERROR_REPORT */
  }
  /* trigger the ServiceDetail request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_ServiceDetail;
  /* set the cyclic trigger */
  Scc_StateM_TxCore.CyclicMsgTrig = TRUE;

} /* PRQA S 6080, 6030 */ /* MD_MSR_STMIF, MD_MSR_STCYC*/
#endif /* SCC_SCHEMA_ISO || SCC_SCHEMA_ISO_ED2 */

/**********************************************************************************************************************
 *  Scc_StateM_PreparePaymentServiceSelectionReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_StateM_PreparePaymentServiceSelectionReq(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
  Exi_ISO_SelectedServiceType *SelectedServiceTypePtr = &Scc_StateM_TxEVSE.SelectedServices[0];
#endif /* SCC_SCHEMA_ISO */

#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )
  uint16 SelectedVasIdFlags = 0;
#endif /* SCC_SCHEMA_ISO_ED2 */

  /* ----- Implementation ----------------------------------------------- */
  switch ( Scc_StateM_RxEVSE.SAPSchemaID )
  {
#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )
  case Scc_SAPSchemaIDs_ISO_Ed2_DIS:
    /* #10 trigger the PaymentServiceSelection request for ISO ED2 */
    Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_PaymentServiceSelection;

    Scc_Get_StateM_SelectedEnergyTransferService(&Scc_StateM_TxEVSE.SelectedEnergyTransferService);

    Scc_Get_ISO_Ed2_DIS_SelectedVasList(&Scc_StateM_TxEVSE.SelectedVasList);

    Scc_Get_ISO_Ed2_DIS_SelectedVasIdFlags(&SelectedVasIdFlags);

    if((SelectedVasIdFlags & Scc_StateM_RxEVSE.VasIdFlags) != SelectedVasIdFlags)
    {
      Scc_StateM_ErrorHandling(Scc_SMER_PaymentServiceSelection_ServiceMismatch);
    }

    retVal = E_OK;
    break;
#endif /* SCC_SCHEMA_ISO_ED2 */

#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
  case Scc_SAPSchemaIDs_DIN:
    /* #20 trigger the PaymentServiceSelection request for DIN */
    Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_PaymentServiceSelection;

    retVal = E_OK;
    break;
#endif /* SCC_SCHEMA_DIN */

#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
  case Scc_SAPSchemaIDs_ISO:
    /* #30 trigger the PaymentServiceSelection request for ISO */
    Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_PaymentServiceSelection;

#if ( ( defined SCC_ENABLE_PNC_CHARGING ) && ( SCC_ENABLE_PNC_CHARGING == STD_ON ) )
    /* get the current status of the cable */
    Scc_Get_StateM_PWMState(&Scc_StateM_Local.PWMState);
    /* check if the duty cycle is nominal */
    if ( Scc_PWMState_HLCOnly != Scc_StateM_Local.PWMState )
    {
      /* #40 Set the payment option to EIM of PWM Signal is not HLC only (5%) */
      Scc_StateM_TxEVSE.SelectedPaymentOption = Scc_PaymentOption_EIM;
      /* disable the certificate services */
      Scc_StateM_Local.ReqCertInstall = FALSE;
      Scc_StateM_Local.ReqCertUpdate = FALSE;
    }
#endif /* SCC_ENABLE_PNC_CHARGING */

    /* #50 Set the selected service list */

    /* set the charge service */
    SelectedServiceTypePtr->ParameterSetIDFlag = 0;
    SelectedServiceTypePtr->ServiceID = Scc_StateM_RxEVSE.ChargeServiceID;

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
    /* if true set the certificate installation service */
    if ( TRUE == Scc_StateM_Local.ReqCertInstall )
    {
      SelectedServiceTypePtr->NextSelectedServicePtr = &Scc_StateM_TxEVSE.SelectedServices[1];
      SelectedServiceTypePtr = SelectedServiceTypePtr->NextSelectedServicePtr;
      SelectedServiceTypePtr->ServiceID = (uint16)Scc_SID_Certificate;
      SelectedServiceTypePtr->ParameterSetIDFlag = 1;
      SelectedServiceTypePtr->ParameterSetID = (sint16)Scc_ParameterSetID_Certificate_Installation;
    }
    /* if true set the certificate update service */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
    else if ( TRUE == Scc_StateM_Local.ReqCertUpdate )
    {
      SelectedServiceTypePtr->NextSelectedServicePtr = &Scc_StateM_TxEVSE.SelectedServices[1];
      SelectedServiceTypePtr = SelectedServiceTypePtr->NextSelectedServicePtr;
      SelectedServiceTypePtr->ServiceID = (uint16)Scc_SID_Certificate;
      SelectedServiceTypePtr->ParameterSetIDFlag = 1;
      SelectedServiceTypePtr->ParameterSetID = (sint16)Scc_ParameterSetID_Certificate_Update;
    }
#endif /* SCC_ENABLE_PNC_CHARGING */

    /* if true set the internet service */
    if ( TRUE == Scc_StateM_Local.InternetAvailable )
    {
      SelectedServiceTypePtr->NextSelectedServicePtr = &Scc_StateM_TxEVSE.SelectedServices[2];
      SelectedServiceTypePtr = SelectedServiceTypePtr->NextSelectedServicePtr;
      SelectedServiceTypePtr->ServiceID = (uint16)Scc_SID_Internet;
      SelectedServiceTypePtr->ParameterSetIDFlag = 1;
      SelectedServiceTypePtr->ParameterSetID = (sint16)Scc_ParameterSetID_Internet_Https;
    }

    /* mark the end of the SelectedServiceList */
    SelectedServiceTypePtr->NextSelectedServicePtr = (Exi_ISO_SelectedServiceType*)NULL_PTR;

    retVal = E_OK;
    break;
#endif /* SCC_SCHEMA_ISO */

  default: /* PRQA S 2016 */ /* MD_MSR_EmptyClause */
    break;
  }

  return retVal;
}

#if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_PrepareCertificateInstallationReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareCertificateInstallationReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the CertificateInstallation request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_CertificateInstallation;

}
#endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

#if ( SCC_ENABLE_CERTIFICATE_UPDATE == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_PrepareCertificateUpdateReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareCertificateUpdateReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the CertificateUpdate request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_CertificateUpdate;

}
#endif /* SCC_ENABLE_CERTIFICATE_UPDATE */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_PreparePaymentDetailsReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PreparePaymentDetailsReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the PaymentDetails request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_PaymentDetails;

}
#endif/* SCC_ENABLE_PNC_CHARGING */


/**********************************************************************************************************************
 *  Scc_StateM_PrepareAuthorizationReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareAuthorizationReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the Authorization request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_Authorization;
  /* #20 set the cyclic trigger */
  Scc_StateM_TxCore.CyclicMsgTrig = TRUE;

}

/**********************************************************************************************************************
 *  Scc_StateM_PrepareChargeParameterDiscoveryReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_PrepareChargeParameterDiscoveryReq(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal = Scc_ReturnType_Pending;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 get the current charging control command */
  Scc_Get_StateM_ChargingControl(&Scc_StateM_Local.ChargingControl);
  /* check if the application is not ready to continue */
  if ( Scc_ChargingControl_NegotiateChargingParameters != Scc_StateM_Local.ChargingControl )
  {
    /* pending */
  }
  else
  {
    /* #20 get the selected EnergyTransferMode */
    Scc_Get_StateM_EnergyTransferMode(&Scc_StateM_TxEVSE.EnergyTransferMode);

    {
      /* trigger the ChargeParameterDiscovery request */
      Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_ChargeParameterDiscovery;
      /* set the cyclic trigger */
      Scc_StateM_TxCore.CyclicMsgTrig = TRUE;

      retVal = Scc_ReturnType_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_StateM_PrepareInitialAlignmentCheckReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */

#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_PrepareCableCheckReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_PrepareCableCheckReq(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 get the current status of the cable */
  Scc_Get_StateM_ControlPilotState(&Scc_StateM_Local.ControlPilotState);

  /* #20 check if the power switch was not closed */
  if ( Scc_ControlPilotState_State_C_D > Scc_StateM_Local.ControlPilotState )
  {
    retVal = Scc_ReturnType_Pending;
  }
  else
  {
    /* trigger the Cable Check request */
    Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_CableCheck;
    /* set the cyclic trigger */
    Scc_StateM_TxCore.CyclicMsgTrig = TRUE;

    retVal = Scc_ReturnType_OK;
  }

  return retVal;
}
#endif /* SCC_CHARGING_DC */

#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_PreparePreChargeReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PreparePreChargeReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the Pre Charge request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_PreCharge;
  /* #20 set the cyclic trigger */
  Scc_StateM_TxCore.CyclicMsgTrig = TRUE;

}
#endif /* SCC_CHARGING_DC */

/**********************************************************************************************************************
 *  Scc_StateM_PreparePowerDeliveryReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_PreparePowerDeliveryReq(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  /* get the current charging control command */
  Scc_Get_StateM_ChargingControl(&Scc_StateM_Local.ChargingControl);

  /* #10 check if the charging shall be started */
  if ( Scc_ChargingControl_StartPowerDelivery == Scc_StateM_Local.ChargingControl )
  {
    /* check for AC Charging (State C is already active in case of DC) */
    if (   ( Scc_EnergyTransferMode_AC_1P == Scc_StateM_TxEVSE.EnergyTransferMode )
        || ( Scc_EnergyTransferMode_AC_3P == Scc_StateM_TxEVSE.EnergyTransferMode ) )
    {
      /* check if there was no previous renegotiation (since then State C is OK) */
      if ( Scc_ChargeStatus_Renegotiating != Scc_StateM_TxEVSE.ChargeStatus )
      {
        /* get the current status of the cable */
        Scc_Get_StateM_ControlPilotState(&Scc_StateM_Local.ControlPilotState);
        /* get the current PWM state */
        Scc_Get_StateM_PWMState(&Scc_StateM_Local.PWMState);
        /* #20 check if CP is not set to State B (V2G2-846) */
        if (   ( Scc_ControlPilotState_State_B != Scc_StateM_Local.ControlPilotState )
            && ( Scc_PWMState_HLCOptional_BCActive != Scc_StateM_Local.PWMState ) )
        {
          retVal = Scc_ReturnType_Pending;
        }
        else
        {
          /* trigger the PowerDelivery request */
          Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_PowerDelivery;

          Scc_StateM_TxEVSE.ChargeStatus = Scc_ChargeStatus_Starting;
          retVal = Scc_ReturnType_OK;
        }
      }
      else
      {
        /* trigger the PowerDelivery request */
        Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_PowerDelivery;

        Scc_StateM_TxEVSE.ChargeStatus = Scc_ChargeStatus_Starting;
        retVal = Scc_ReturnType_OK;
      }
    }
    else
    {
      /* trigger the PowerDelivery request */
      Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_PowerDelivery;

      Scc_StateM_TxEVSE.ChargeStatus = Scc_ChargeStatus_Starting;
      retVal = Scc_ReturnType_OK;
    }
  }
#if (( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0u )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0u ))
  /* #30 check if a renegotation shall be started */
  else if (   ( Scc_ChargingControl_Renegotiation == Scc_StateM_Local.ChargingControl )
           && ( Scc_ChargeStatus_Starting == Scc_StateM_TxEVSE.ChargeStatus ))
  {
    /* trigger the PowerDelivery request */
    Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_PowerDelivery;

    Scc_StateM_TxEVSE.ChargeStatus = Scc_ChargeStatus_Renegotiating;
    retVal = Scc_ReturnType_OK;
  }
#endif /* SCC_SCHEMA_ISO || SCC_SCHEMA_ISO_ED2 */
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
  /* #40 check if a welding detection shall be started */
  else if (   ( Scc_ChargingControl_StartWeldingDetection == Scc_StateM_Local.ChargingControl )
           && ( Scc_EnergyTransferMode_AC_1P != Scc_StateM_TxEVSE.EnergyTransferMode )
           && ( Scc_EnergyTransferMode_AC_3P != Scc_StateM_TxEVSE.EnergyTransferMode ))
  {
    /* trigger the PowerDelivery request */
    Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_PowerDelivery;

    Scc_StateM_TxEVSE.ChargeStatus = Scc_ChargeStatus_Stopping;
    retVal = Scc_ReturnType_OK;
  }
#endif /* SCC_CHARGING_DC */
  /* #50 check if the charging shall be stopped or paused */
  else if ( Scc_ChargingControl_StopCharging == Scc_StateM_Local.ChargingControl )
  {
    /* trigger the PowerDelivery request */
    Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_PowerDelivery;

    Scc_StateM_TxEVSE.ChargeStatus = Scc_ChargeStatus_Stopping;
    retVal = Scc_ReturnType_OK;
  }
  else
  {
    /* if the application is not ready to continue yet */
    retVal = Scc_ReturnType_Pending;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_StateM_PreparePowerDemandReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */



#if ( defined SCC_CHARGING_DC_BPT ) && ( SCC_CHARGING_DC_BPT == STD_ON )
/**********************************************************************************************************************
*  Scc_StateM_PrepareDC_BidirectionalControlReq
*********************************************************************************************************************/
/*!
*
* Internal comment removed.
 *
 *
 *
*/
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareDC_BidirectionalControlReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the DC_BidirectionalControl request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_DC_BidirectionalControl;
  /* #20 set the cyclic trigger */
  Scc_StateM_TxCore.CyclicMsgTrig = TRUE;
}
#endif /* SCC_CHARGING_DC_BPT */

#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )

/**********************************************************************************************************************
 *  Scc_StateM_PrepareCurrentDemandReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareCurrentDemandReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the CurrentDemand request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_CurrentDemand;
  /* #20 set the cyclic trigger */
  Scc_StateM_TxCore.CyclicMsgTrig = TRUE;

}

/**********************************************************************************************************************
 *  Scc_StateM_PrepareWeldingDetectionReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareWeldingDetectionReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the WeldingDetection request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_WeldingDetection;
  /* #20 set the cyclic trigger */
  Scc_StateM_TxCore.CyclicMsgTrig = TRUE;

}

#endif /* SCC_CHARGING_DC */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_PrepareMeteringReceiptReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareMeteringReceiptReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the MeteringReceipt request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_MeteringReceipt;

}
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  Scc_StateM_PrepareSessionStopReq
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareSessionStopReq(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the SessionStop request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_SessionStop;

}

/**********************************************************************************************************************
 *  Scc_StateM_PrepareStopCommunicationSession
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_PrepareStopCommunicationSession(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 trigger the StopCommunicationSession request */
  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_StopCommunicationSession;

}

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_ProcessSLAC
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessSLAC(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 set the new WaitForIPAddress state */
  Scc_StateM_State = Scc_SMS_WaitForIPAddress;

}
#endif /* SCC_ENABLE_SLAC_HANDLING */

/**********************************************************************************************************************
 *  Scc_StateM_ProcessSECCDiscoveryProtocolRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessSECCDiscoveryProtocolRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType RetVal = (Std_ReturnType)E_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 check if the requested security option was not chosen by the EVSE */
  if ( Scc_StateM_TxCore.ReqSDPSecurity != Scc_StateM_RxCore.SupSDPSecurity )
  {
    /* --- determine if the EVSE security option can be accepted --- */

    /* #20 check if the EVSE only supports a connection with TLS */
    if ( Scc_SDPSecurity_Tls == Scc_StateM_RxCore.SupSDPSecurity )
    {
#if ( SCC_ENABLE_TLS == STD_ON )
      /* use the supported SDPSecurity */
      Scc_StateM_TxCore.ReqSDPSecurity = Scc_StateM_RxCore.SupSDPSecurity;
#else
      /* transport layer connection cannot be established */
      RetVal = (Std_ReturnType)E_NOT_OK;
#endif /* SCC_ENABLE_TLS */
    }
    /* check if the EVSE only supports a connection without TLS */
    else /* if ( Scc_SDPSecurity_None == Scc_StateM_RxCore.SupSDPSecurity ) */
    {
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
      /* #30 check if unsecure connections are not accepted */
      if (FALSE == (boolean)Scc_ConfigValue_StateM_AcceptUnsecureConnection) /* PRQA S 2741 */ /* MD_Scc_VariantDependent */ /*lint !e506 */
      {
        /* transport layer connection cannot be established */
        RetVal = (Std_ReturnType)E_NOT_OK;
      }
      else
#endif
      {
        /* use the supported SDPSecurity */ /* PRQA S 2880 1 */ /* MD_Scc_2742_2880_2995 */
        Scc_StateM_TxCore.ReqSDPSecurity = Scc_StateM_RxCore.SupSDPSecurity;
      }
    }
  }

  if ( (Std_ReturnType)E_OK == RetVal )
  {
    /* set the new state */
    Scc_StateM_State = Scc_SMS_TLConnection;
  }
  else
  {
    /* start error handling */
    Scc_StateM_ErrorHandling(Scc_SMER_SECCDiscoveryProtocol_NoSharedSecurityOption);
  }

  return RetVal;
}

/**********************************************************************************************************************
 *  Scc_StateM_ProcessTLConnected
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessTLConnected(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 set the new state */
  Scc_StateM_State = Scc_SMS_SupportedAppProtocol;

}

/**********************************************************************************************************************
 *  Scc_StateM_ProcessSupportedAppProtocolRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessSupportedAppProtocolRes(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 set the new state */
  Scc_StateM_State = Scc_SMS_SessionSetup;

  /* For schema ISO_Ed2 a TLS connection is mandatory */
  #if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )
    if ( ( Scc_SDPSecurity_None == Scc_StateM_RxCore.SupSDPSecurity )
      && ( Scc_SAPSchemaIDs_ISO_Ed2_DIS == Scc_StateM_RxEVSE.SAPSchemaID ))
    {
      /* #20 stop the communication session if TLS is disabled for ISO Ed2 */
      Scc_StateM_ErrorHandling(Scc_SMER_SECCDiscoveryProtocol_NoSharedSecurityOption);
    }
  #endif /* SCC_SCHEMA_ISO_ED2 */

}

/**********************************************************************************************************************
 *  Scc_StateM_ProcessSessionSetupRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessSessionSetupRes(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 set the new state */
  Scc_StateM_State = Scc_SMS_ServiceDiscovery;

}

/**********************************************************************************************************************
 *  Scc_StateM_ProcessServiceDiscoveryRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessServiceDiscoveryRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal = Scc_ReturnType_NotOK;

  /* ----- Implementation ----------------------------------------------- */
  switch ( Scc_StateM_RxEVSE.SAPSchemaID )
  {
#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
  case Scc_SAPSchemaIDs_DIN:
    /* #10 Set PaymentOption and State for DIN schema */
    /* set the PaymentOption */
    Scc_StateM_TxEVSE.SelectedPaymentOption = Scc_PaymentOption_EIM;
    /* set the new state */
    Scc_StateM_State = Scc_SMS_PaymentServiceSelection;

    retVal = Scc_ReturnType_OK;
    break;
#endif /* SCC_SCHEMA_DIN */

# if (( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ))
  case Scc_SAPSchemaIDs_ISO:
# endif /* SCC_SCHEMA_ISO */
# if(( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
  case Scc_SAPSchemaIDs_ISO_Ed2_DIS:
# endif /* SCC_SCHEMA_ISO_ED2 */

    #if (( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
    /* #20 Set PaymentOption and State for ISO schema */
    /* if no message set was selected yet */
    if ( Scc_PaymentOption_None == Scc_StateM_TxEVSE.SelectedPaymentOption )
    {
#if (( defined SCC_ENABLE_TLS ) && ( SCC_ENABLE_TLS == STD_ON ))
      /* #30 if a TLS encrypted connection was established */
      if ( Scc_SDPSecurity_Tls == Scc_StateM_RxCore.SupSDPSecurity )
      {
# if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
        /* #40 if the EVSE offers only contract, but the EV only supports external payment */
        if (   (   ( Scc_PaymentPrioritization_OnlyPnC == Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions )
                && ( (uint16)Scc_PaymentPrioritization_OnlyEIM == Scc_ConfigValue_StateM_PaymentPrioritization ) ) /*lint !e506 */ /* PRQA S 2996,2992 */ /* MD_Scc_2996 */
            || (   ( Scc_PaymentPrioritization_OnlyEIM == Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions ) /* PRQA S 2996,2992 */ /* MD_Scc_2996 */
                && ( (uint16)Scc_PaymentPrioritization_OnlyPnC == Scc_ConfigValue_StateM_PaymentPrioritization ) ) ) /*lint !e506 */ /* PRQA S 2996,2992 */ /* MD_Scc_2996 */
        { /* PRQA S 2880 1 */ /* MD_Scc_2742_2880_2995 */
          Scc_StateM_TxEVSE.SelectedPaymentOption = Scc_PaymentOption_None;
        }
        /* #50 if the EVSE offers contract and external or only external, but the EV only supports external payment */
        else if (   (   ( Scc_PaymentPrioritization_PrioritizePnC == Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions )
                     && ( (uint16)Scc_PaymentPrioritization_OnlyEIM == Scc_ConfigValue_StateM_PaymentPrioritization ) ) /*lint !e506 */ /* PRQA S 2996,2992 */ /* MD_Scc_2996 */
                 || (   ( Scc_PaymentPrioritization_OnlyEIM == Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions )
                     && ( (uint16)Scc_PaymentPrioritization_OnlyPnC != Scc_ConfigValue_StateM_PaymentPrioritization ) ) ) /*lint !e506 */ /* PRQA S 2996,2992 */ /* MD_Scc_2996 */
        {
          Scc_StateM_TxEVSE.SelectedPaymentOption = Scc_PaymentOption_EIM;
        }
        /* #60 if the EVSE offers contract based payment and the EV supports it too */
        else
        {
          Scc_StateM_TxEVSE.SelectedPaymentOption = Scc_PaymentOption_PnC;
          /* start loading the certificates */
          Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_ReadContrCertChain;
          Scc_StateM_TxCore.CyclicMsgTrig = TRUE;
          /* wait until the contract certificates are loaded */
          retVal = Scc_ReturnType_Pending;
          break;                                                                                                        /* PRQA S 3333 */ /* MD_Scc_3333 */
        }
# else /* SCC_ENABLE_PNC_CHARGING */
        /* check if the EVSE offers EIM */
        if (   ( Scc_PaymentPrioritization_OnlyEIM == Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions )
            || ( Scc_PaymentPrioritization_PrioritizePnC == Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions ) )
        {
          Scc_StateM_TxEVSE.SelectedPaymentOption = Scc_PaymentOption_EIM;
        }
        /* no matching message set could be determined */
        else
        {
          Scc_StateM_TxEVSE.SelectedPaymentOption = Scc_PaymentOption_None;
        }
# endif /* SCC_ENABLE_PNC_CHARGING */
      }
      /* #70 if an unsecured connection was established */
      else /* if ( Scc_SDPSecurity_None == Scc_StateM_RxCore.SupSDPSecurity ) */
#endif /* SCC_ENABLE_TLS */
      {
        /* #80 if the EVSE only offers contract, or the EV only supports contract */
        if (   ( Scc_PaymentPrioritization_OnlyPnC == Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions )
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
            || ( (uint16)Scc_PaymentPrioritization_OnlyPnC == Scc_ConfigValue_StateM_PaymentPrioritization))
#else
           )
#endif /* SCC_ENABLE_PNC_CHARGING */
        {
          Scc_StateM_TxEVSE.SelectedPaymentOption = Scc_PaymentOption_None;
        }
        /*#90 if the EVSE and the EV support external payment */
        else
        {
          Scc_StateM_TxEVSE.SelectedPaymentOption = Scc_PaymentOption_EIM;
        }
      }

      /* #100 if no message set could be determined */
      if ( Scc_PaymentOption_None == Scc_StateM_TxEVSE.SelectedPaymentOption )
      {
        /* stop the communication session */
        Scc_StateM_ErrorHandling(Scc_SMER_ServiceDiscovery_NoSharedPaymentOption);
        retVal = Scc_ReturnType_NotOK;
        break;   /* PRQA S 3333 */ /* MD_Scc_3333 */
      }
    }

    /* #110 check if EIM charging was chosen */
    if ( Scc_PaymentOption_EIM == Scc_StateM_TxEVSE.SelectedPaymentOption )
    {
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
      /* disable the certificate services */
      Scc_StateM_Local.ReqCertDetails = FALSE;
#endif /* SCC_ENABLE_PNC_CHARGING */

#if (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
      if ( Scc_SAPSchemaIDs_ISO_Ed2_DIS == Scc_StateM_RxEVSE.SAPSchemaID )
      {
        /* ISO_Ed2_DIS needs ServiceDetail state for EnergyTransferService details. */
        Scc_StateM_State = Scc_SMS_ServiceDetail;

        /* get all required EVCharging services */
        Scc_Get_StateM_EnergyTransferServiceIDFlags(&Scc_StateM_TxEVSE.TxEnergyTransferServiceIDFlags);
      }
      else
#endif /* SCC_SCHEMA_ISO_ED2 */

#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
      /* if a connection with TLS was established and internet service is required */
      if (   ( Scc_SDPSecurity_Tls == Scc_StateM_TxCore.ReqSDPSecurity )
          && ( TRUE == Scc_StateM_Local.ReqInetDetails ) )
      {
        Scc_StateM_Local.InetDetailsRequested = FALSE;

        /* set the new state */
        Scc_StateM_State = Scc_SMS_ServiceDetail;
      }
      else
#endif /* SCC_SCHEMA_ISO */
      {
        /* set the new state */
        Scc_StateM_State = Scc_SMS_PaymentServiceSelection;
      }
    }
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
    /* #120 check if PnC charging was chosen */
    else /* if ( Scc_PaymentOption_PnC == Scc_StateM_TxEVSE.SelectedPaymentOption ) */
    {
      /* if an error occurred while reading the certificates */
      if ( Scc_MsgStatus_ReadContrCertChain_Failed == Scc_StateM_RxCore.MsgStatus )
      {
        Scc_StateM_ErrorHandling(Scc_SMER_StackError);
        retVal = Scc_ReturnType_NotOK;
        break;  /* PRQA S 3333 */ /* MD_Scc_3333 */
      }
      /* if the NvM is not finished yet */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
      else if ( Scc_MsgStatus_ReadContrCertChain_OK != Scc_StateM_RxCore.MsgStatus )
      {
        retVal = Scc_ReturnType_Pending;
        break;  /* PRQA S 3333 */ /* MD_Scc_3333 */
      }

      Scc_Get_StateM_EVTimeStamp(&Scc_StateM_Local.EVTimeStamp);

      /* check the validity of the contract certificate */
      if (Scc_ReturnType_OK != Scc_CheckContrCertValidity(Scc_StateM_Local.EVTimeStamp,
        (Scc_StateM_Local.EVTimeStamp + (uint32)(Scc_ConfigValue_StateM_CertificateExpireThreshold * (uint32)86400)),
        &Scc_StateM_Local.ContrCertStatus))
      {
        /* The NvM is not finished yet */
        retVal = Scc_ReturnType_Pending;
        break;  /* PRQA S 3333 */ /* MD_Scc_3333 */
      }

      /* check if neither a contract certificate nor the installation service is available */
      if (   (   ( Scc_CS_CertExpired == Scc_StateM_Local.ContrCertStatus )
              || ( Scc_CS_NoCertAvailable == Scc_StateM_Local.ContrCertStatus ))
          && ( FALSE == Scc_StateM_Local.ReqCertDetails ))
      {
        /* check if it is possible to switch to EIM */
        if (   ( Scc_PaymentPrioritization_PrioritizePnC == Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions )
            && ( (uint16)Scc_PaymentPrioritization_PrioritizePnC == Scc_ConfigValue_StateM_PaymentPrioritization)) /*lint !e506 */
        {
          /* switch to EIM */
          Scc_StateM_TxEVSE.SelectedPaymentOption = Scc_PaymentOption_EIM;
        }
        else
        {
          /* report the error */
          Scc_StateM_ErrorHandling(Scc_SMER_ServiceDiscovery_NoContractAndNoCertificateServiceAvailable);
          retVal = Scc_ReturnType_NotOK;
          break;  /* PRQA S 3333 */ /* MD_Scc_3333 */
        }
      }

      /* check if no certificate installation or update is necessary */
      if ( Scc_CS_CertAvailable == Scc_StateM_Local.ContrCertStatus )
      {
        /* disable the certificate services */
        Scc_StateM_Local.ReqCertDetails = FALSE;
      }


      /* if service details are required */
      if (   ( TRUE == Scc_StateM_Local.ReqCertDetails )
          || ( TRUE == Scc_StateM_Local.ReqInetDetails ) )
      {
        Scc_StateM_Local.CertDetailsRequested = FALSE;
        Scc_StateM_Local.InetDetailsRequested = FALSE;

        /* set the new state */
        Scc_StateM_State = Scc_SMS_ServiceDetail;
      }
      /* if no service details are required */
      else
      {
        /* set the new state */
        Scc_StateM_State = Scc_SMS_PaymentServiceSelection;
      }

#if (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
      if ( Scc_SAPSchemaIDs_ISO_Ed2_DIS == Scc_StateM_RxEVSE.SAPSchemaID )
      {
        /* ISO_Ed2_DIS needs ServiceDetail state for EnergyTransferService details. */
        Scc_StateM_State = Scc_SMS_ServiceDetail;

        /* get all required EVCharging services */
        Scc_Get_StateM_EnergyTransferServiceIDFlags(&Scc_StateM_TxEVSE.TxEnergyTransferServiceIDFlags);
      }
#endif /* SCC_SCHEMA_ISO_ED2 || SCC_SCHEMA_ISO_ED2 */

    }
#endif /* SCC_ENABLE_PNC_CHARGING */

    retVal = Scc_ReturnType_OK;
    break;
#endif /* SCC_SCHEMA_ISO OR  */

  default: /* PRQA S 2016 */ /* MD_MSR_EmptyClause */
    break;
  }



  return retVal;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

#if (( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
/**********************************************************************************************************************
 *  Scc_StateM_ProcessServiceDetailRes
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessServiceDetailRes(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 check if the provided ServiceID is the same as the requested */
  if ( Scc_StateM_RxEVSE.ProvServiceID == Scc_StateM_TxEVSE.ReqServiceID )
  {
#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) && ( SCC_ENABLE_PNC_CHARGING == STD_ON )
    if ( Scc_StateM_RxEVSE.SAPSchemaID == Scc_SAPSchemaIDs_ISO_Ed2_DIS )
    {
      if (NULL_PTR != Scc_StateM_RxEVSE.ServiceParameterListPtr_ISO_Ed2_DIS)
      {
        Exi_ISO_ED2_DIS_ParameterSetType *ParameterSetPtr_ISO_Ed2_DIS = Scc_StateM_RxEVSE.ServiceParameterListPtr_ISO_Ed2_DIS->ParameterSet;

        Scc_StateM_TxEVSE.ReqServiceID = 0u;

        /* #20 if the EVSE offers certificate services  for ISO ED2 */
        if (((uint16)Scc_SID_Certificate_ISO_Ed2_DIS == Scc_StateM_RxEVSE.ProvServiceID) && (Scc_SAPSchemaIDs_ISO_Ed2_DIS == Scc_StateM_RxEVSE.SAPSchemaID)) /* PRQA S 2995 */ /* MD_Scc_2742_2880_2995 */
        {
          while (NULL_PTR != ParameterSetPtr_ISO_Ed2_DIS)
          {
            /* if the EVSE offers the service CertificateInstallation */
            if (((uint16)Scc_ParameterSetID_Certificate_Installation == ParameterSetPtr_ISO_Ed2_DIS->ParameterSetID)
              && ((Scc_CS_CertExpired == Scc_StateM_Local.ContrCertStatus)
              || (Scc_CS_NoCertAvailable == Scc_StateM_Local.ContrCertStatus)))
            {
              /* enable the certificate installation service */
              Scc_StateM_Local.ReqCertInstall = TRUE;
            }
            /* if the EVSE offers the service CertificateUpdate */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
            else if (((uint16)Scc_ParameterSetID_Certificate_Update == ParameterSetPtr_ISO_Ed2_DIS->ParameterSetID)
              && (Scc_CS_CertExpiresSoon == Scc_StateM_Local.ContrCertStatus))
            {
              /* enable the certificate update service */
              Scc_StateM_Local.ReqCertUpdate = TRUE;
            }
            /* step to the next parameter */
            ParameterSetPtr_ISO_Ed2_DIS = ParameterSetPtr_ISO_Ed2_DIS->NextParameterSetPtr;
          }
        }
      }
    }
#endif /* SCC_SCHEMA_ISO_ED2 && SCC_ENABLE_PNC_CHARGING */
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
    if ( Scc_StateM_RxEVSE.SAPSchemaID == Scc_SAPSchemaIDs_ISO ) /* PRQA S 2004 */ /* MD_Scc_2004 */
    {
      /* process the received ServiceParameterList */
      if (NULL_PTR != Scc_StateM_RxEVSE.ServiceParameterListPtr)
      {
        Exi_ISO_ParameterSetType *ParameterSetPtr = Scc_StateM_RxEVSE.ServiceParameterListPtr->ParameterSet;

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
        /* #30 if the EVSE offers certificate services for ISO ED1 */
        if ((uint16)Scc_SID_Certificate == Scc_StateM_RxEVSE.ProvServiceID)
        {
          /* loop as long as the ParameterSet contains another Parameter */
          while (NULL_PTR != ParameterSetPtr)
          {
            /* if the EVSE offers the service CertificateInstallation */
            if (((sint16)Scc_ParameterSetID_Certificate_Installation == ParameterSetPtr->ParameterSetID)
              && ((Scc_CS_CertExpired == Scc_StateM_Local.ContrCertStatus)
              || (Scc_CS_NoCertAvailable == Scc_StateM_Local.ContrCertStatus)))
            {
              /* enable the certificate installation service */
              Scc_StateM_Local.ReqCertInstall = TRUE;
            }
            /* if the EVSE offers the service CertificateUpdate */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
            else if (((sint16)Scc_ParameterSetID_Certificate_Update == ParameterSetPtr->ParameterSetID)
              && (Scc_CS_CertExpiresSoon == Scc_StateM_Local.ContrCertStatus))
            {
              /* enable the certificate update service */
              Scc_StateM_Local.ReqCertUpdate = TRUE;
            }
            /* step to the next parameter */
            ParameterSetPtr = ParameterSetPtr->NextParameterSetPtr;
          }
        }
#endif /* SCC_ENABLE_PNC_CHARGING */

        /* #40 if the EVSE offers internet services for ISO ED1 */
        if (((uint16)Scc_SID_Internet == Scc_StateM_RxEVSE.ProvServiceID) && (Scc_SAPSchemaIDs_ISO == Scc_StateM_RxEVSE.SAPSchemaID))/* PRQA S 2995 */ /* MD_Scc_2742_2880_2995 */
        {
          /* loop as long as the ParameterSet contains another Parameter */
          while (NULL_PTR != ParameterSetPtr)
          {
            /* check if the ParameterSetID equals the relevant HTTPS service */
            if ((sint16)Scc_ParameterSetID_Internet_Https == ParameterSetPtr->ParameterSetID)
            {
              /* enable the internet service */
              Scc_StateM_Local.InternetAvailable = TRUE;
              break;
            }
            else
            {
              /* disable the internet service */
              Scc_StateM_Local.InternetAvailable = FALSE;
            }
            /* step to the next Parameter */
            ParameterSetPtr = ParameterSetPtr->NextParameterSetPtr;
          }
          /* report the status of the internet service */
          Scc_Set_StateM_InternetAvailable(Scc_StateM_Local.InternetAvailable);
        }
      }
    }
#endif /* SCC_SCHEMA_ISO */
  }
  else
  {
    /* Received ServiceID is not the one transmitted. */
  }

#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
  if ( ( Scc_SAPSchemaIDs_ISO_Ed2_DIS == Scc_StateM_RxEVSE.SAPSchemaID )
    && ( Scc_StateM_TxEVSE.TxEnergyTransferServiceIDFlags != 0u))
  {
    /* nothing has to be done */
  }
  else
#endif
  {
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  /* check if another ServiceDetail request has to be sent */

    if (   (   ( TRUE == Scc_StateM_Local.ReqCertDetails ) && ( FALSE == Scc_StateM_Local.CertDetailsRequested ) )
      || (   ( TRUE == Scc_StateM_Local.ReqInetDetails ) && ( FALSE == Scc_StateM_Local.InetDetailsRequested ) )
#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )
      || (   ( TRUE == Scc_StateM_Local.ReqWPTFinePositioningDetails ) && ( FALSE == Scc_StateM_Local.WPTFinePositioningDetailsRequested ) )
      || (   ( TRUE == Scc_StateM_Local.ReqWPTPairingDetails ) && ( FALSE == Scc_StateM_Local.WPTPairingDetailsRequested ) )
      || (   ( TRUE == Scc_StateM_Local.ReqWPTInitialAlignmentCheckDetails ) && ( FALSE == Scc_StateM_Local.WPTInitialAlignmentCheckDetailsRequested ) )
#endif /* SCC_SCHEMA_ISO_ED2 */
       )
    {
      /* nothing has to be done */
    }
    else
#endif /* SCC_ENABLE_PNC_CHARGING */
    {
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
      /* check if PnC shall be used */
      if ( Scc_PaymentOption_PnC == Scc_StateM_TxEVSE.SelectedPaymentOption )
      {
        /* check if neither contract certificate nor installation service is available */
        if (   (   ( Scc_CS_CertExpired == Scc_StateM_Local.ContrCertStatus )
                || ( Scc_CS_NoCertAvailable == Scc_StateM_Local.ContrCertStatus ))
            && ( FALSE == Scc_StateM_Local.ReqCertInstall ))
        {
          /* check if it is possible to switch to EIM */
          if (   ( Scc_PaymentPrioritization_PrioritizePnC == Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions )
              && ( (uint16)Scc_PaymentPrioritization_PrioritizePnC == Scc_ConfigValue_StateM_PaymentPrioritization)) /*lint !e506 */
          {
            /* switch to EIM */
            Scc_StateM_TxEVSE.SelectedPaymentOption = Scc_PaymentOption_EIM;

            /* set the new state */
            Scc_StateM_State = Scc_SMS_PaymentServiceSelection;
          }
          else
          {
            /* report the error */
            Scc_StateM_ErrorHandling(Scc_SMER_ServiceDetail_NoContractAndNoInstallationServiceAvailable);
          }
        }
        else
        {
          /* set the new state */
          Scc_StateM_State = Scc_SMS_PaymentServiceSelection;
        }
      }
      else
#endif /* SCC_ENABLE_PNC_CHARGING */
      {
        /* set the new state */
        Scc_StateM_State = Scc_SMS_PaymentServiceSelection;
      }
    }
  }

} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */
#endif /* SCC_SCHEMA_ISO */

/**********************************************************************************************************************
 *  Scc_StateM_ProcessPaymentServiceSelectionRes
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessPaymentServiceSelectionRes(void) /* PRQA S 2889 */ /* MD_Scc_15.5 */
{
  /* ----- Implementation ----------------------------------------------- */
#if ( ( defined SCC_ENABLE_PNC_CHARGING ) && ( SCC_ENABLE_PNC_CHARGING == STD_ON ) )
  /* if the PnC profile is active */
  if ( Scc_PaymentOption_PnC == Scc_StateM_TxEVSE.SelectedPaymentOption )
  {
    /* #10 check if a CertificateInstallation or a CertificateUpdate is required */
    if (   ( TRUE == Scc_StateM_Local.ReqCertInstall )
        || ( TRUE == Scc_StateM_Local.ReqCertUpdate ) )
    {
      /* #20 read the root certificates */
      if ( Scc_MsgStatus_PaymentServiceSelection_OK == Scc_StateM_RxCore.MsgStatus )
      {
        /* set the next state to wait for the root certificates */
        Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_ReadRootCerts;
        Scc_StateM_TxCore.CyclicMsgTrig = TRUE;

        return Scc_ReturnType_Pending;
      }

      if ( Scc_MsgStatus_ReadRootCerts_Failed == Scc_StateM_RxCore.MsgStatus )
      {
        Scc_StateM_ErrorHandling(Scc_SMER_StackError);
        return Scc_ReturnType_NotOK;
      }

      /* #30 check if the prov certs are required */
      if ( TRUE == Scc_StateM_Local.ReqCertInstall )
      {
        /* secondly read the prov cert */
        if ( Scc_MsgStatus_ReadRootCerts_OK == Scc_StateM_RxCore.MsgStatus )
        {
          /* set the next state to wait for the root certificates */
          Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_ReadProvCert;
          Scc_StateM_TxCore.CyclicMsgTrig = TRUE;

          return Scc_ReturnType_Pending;
        }
        if ( Scc_MsgStatus_ReadProvCert_Failed == Scc_StateM_RxCore.MsgStatus )
        {
          Scc_StateM_ErrorHandling(Scc_SMER_StackError);
          return Scc_ReturnType_NotOK;
        }
      }

      /* #40 check if CertificateInstallation or CertificateUpdate is required */
      if ( TRUE == Scc_StateM_Local.ReqCertInstall )
      {
        /* set the new state */
        Scc_StateM_State = Scc_SMS_CertificateInstallation;
      }
      /* CertificateUpdate is required */
      else /* if ( TRUE == Scc_StateM_Local.ReqCertUpdate ) */
      {
        /* set the new state */
        Scc_StateM_State = Scc_SMS_CertificateUpdate;
      }
    }
    else
    {
      /* set the new state */
      Scc_StateM_State = Scc_SMS_PaymentDetails;
    }
  }
  /* if the EIM profile is active */
  else
#endif /* SCC_ENABLE_PNC_CHARGING */
  {
    {
      /* set the new state */
      Scc_StateM_State = Scc_SMS_Authorization;
    }
  }

  return Scc_ReturnType_OK;
}

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )

#if ( SCC_ENABLE_CERTIFICATE_INSTALLATION == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_ProcessCertificateInstallationRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessCertificateInstallationRes(void)
{
  /* ----- Implementation ----------------------------------------------- */
#if (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
  /* #10 Check if the schema used is ED2 and the RemainingContractCertificateChains is 0 */
  if ( Scc_StateM_RxEVSE.SAPSchemaID == Scc_SAPSchemaIDs_ISO_Ed2_DIS )
  {
    Scc_Get_StateM_RemainingContractCertificateChains(&Scc_StateM_Local.RemainingContractCertificateChains);
    if ( Scc_StateM_Local.RemainingContractCertificateChains == 0u )
    {
      /* #20  set the new state */
      Scc_StateM_State = Scc_SMS_PaymentDetails;
    }
  }
  else
#endif
  {
    /* #30 Otherwise set the new state */
    Scc_StateM_State = Scc_SMS_PaymentDetails;
  }
}
#endif /* SCC_ENABLE_CERTIFICATE_INSTALLATION */

#if ( SCC_ENABLE_CERTIFICATE_UPDATE == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_ProcessCertificateUpdateRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessCertificateUpdateRes(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 set the new state */
  Scc_StateM_State = Scc_SMS_PaymentDetails;

}
#endif /* SCC_ENABLE_CERTIFICATE_UPDATE */

/**********************************************************************************************************************
 *  Scc_StateM_ProcessPaymentDetailsRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessPaymentDetailsRes(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 set the new state */
  {
    Scc_StateM_State = Scc_SMS_Authorization;
  }

}

#endif /* SCC_ENABLE_PNC_CHARGING */


/**********************************************************************************************************************
 *  Scc_StateM_ProcessAuthorizationRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessAuthorizationRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  if ( Scc_EVSEProcessing_Finished == Scc_StateM_RxEVSE.EVSEProcessing )
  {
    /* #10 Reset cycle timer and delay timer if processing is finished */
    /* reset the cycle timer */
    Scc_StateM_Counter.OngoingTimerCnt = 0;
    /* reset the delay timer */
    Scc_StateM_Counter.GeneralDelayCnt = 0;

    /* set the new state */
    Scc_StateM_State = Scc_SMS_ChargeParameterDiscovery;

    retVal = Scc_ReturnType_OK;
  }
  else /* Scc_EVSEProcessing_Ongoing */
  {
    /* #20 Check the delay counter */
    /* check if the delay counter is active */
    if ( 0u < Scc_StateM_Counter.GeneralDelayCnt )
    {
      Scc_StateM_Counter.GeneralDelayCnt--;
      /* if the next request has to be further delayed */
      if ( 0u != Scc_StateM_Counter.GeneralDelayCnt )
      {
        /* return pending */
        retVal = Scc_ReturnType_Pending;
      }
      else
      {
        retVal = Scc_ReturnType_OK;
      }
    }
    /* if the delay counter is still disabled */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
    else if ( 0u < (uint16)Scc_ConfigValue_StateM_AuthorizationNextReqDelay ) /* PRQA S 2741 */ /* MD_Scc_VariantDependent */ /*lint !e506 */
    {
      /* enable the delay counter */
      Scc_StateM_Counter.GeneralDelayCnt = (uint16)Scc_ConfigValue_StateM_AuthorizationNextReqDelay;
      /* return pending */
      retVal = Scc_ReturnType_Pending;
    }
    else
    { /* PRQA S 2880 1 */ /* MD_Scc_2742_2880_2995 */
      retVal = Scc_ReturnType_OK;
    }
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_StateM_ProcessChargeParameterDiscoveryRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessChargeParameterDiscoveryRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal = Scc_ReturnType_NotOK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 check if the EVSE signals that ChargeParameterDiscovery has finished */
  if ( Scc_EVSEProcessing_Finished == Scc_StateM_RxEVSE.EVSEProcessing )
  {
    /* reset the cycle timer */
    Scc_StateM_Counter.OngoingTimerCnt = 0;
    /* reset the delay timer */
    Scc_StateM_Counter.GeneralDelayCnt = 0;

#if (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
    if ( Scc_StateM_RxEVSE.SAPSchemaID == Scc_SAPSchemaIDs_ISO_Ed2_DIS )
    {
      /* #20 Set next state depending on the EnergyTransfer for ISO ED2 */
      switch ( (Scc_StateM_SupportedServiceIDType)Scc_StateM_TxEVSE.SelectedEnergyTransferService.ServiceID ) /* PRQA S 4342 */ /* MD_Scc_Exi_Generic */
      {
#if ( SCC_CHARGING_AC == STD_ON )
        case Scc_SID_AC_Charging:
        {
          Scc_StateM_State = Scc_SMS_PowerDelivery;
          retVal = Scc_ReturnType_OK;
          break;
        }
#endif /* SCC_CHARGING_AC */
#if(SCC_CHARGING_DC == STD_ON)
        case Scc_SID_DC_Charging:
        {
          Scc_StateM_State = Scc_SMS_CableCheck;
          retVal = Scc_ReturnType_OK;
          break;
        }
#endif /* SCC_CHARGING_DC */
#if(SCC_CHARGING_WPT == STD_ON)
        case Scc_SID_WPT_Charging:
        {
          Scc_StateM_State = Scc_SMS_InitialAlignmentCheck;
          retVal = Scc_ReturnType_OK;
          break;
        }
#endif /* SCC_CHARGING_WPT */
#if(SCC_CHARGING_AC_BPT == STD_ON)
        case Scc_SID_AC_BPT:
        {
          Scc_StateM_State = Scc_SMS_PowerDelivery;
          retVal = Scc_ReturnType_OK;
          break;
        }
#endif /* SCC_CHARGING_AC_BPT */
#if(SCC_CHARGING_DC_BPT == STD_ON)
        case Scc_SID_DC_BPT:
        {
          Scc_StateM_State = Scc_SMS_CableCheck;
          retVal = Scc_ReturnType_OK;
          break;
        }
#endif /* SCC_CHARGING_DC_BPT */
        default:
#if(SCC_DEV_ERROR_REPORT == STD_ON)
          (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_STATEM_PROC_CHARGE_PARAM, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
          break;
      }
    }
    else
#endif /* SCC_SCHEMA_ISO_ED2 */
    /* check which energy transfer mode is active */
    {
      /* #30 Set next state depending on the EnergyTransfer for ISO ED1 */
      switch ( Scc_StateM_TxEVSE.EnergyTransferMode )
      {
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
          /* check for DC Charging */
        case Scc_EnergyTransferMode_DC_Combo_Core:
        case Scc_EnergyTransferMode_DC_Core:
        case Scc_EnergyTransferMode_DC_Extended:
        case Scc_EnergyTransferMode_DC_Unique:
          /* set the new state */
          Scc_StateM_State = Scc_SMS_CableCheck;
          retVal = Scc_ReturnType_OK;
          break;
#endif /* SCC_CHARGING_DC */

        default:
          /* this is an invalid state */
#if(SCC_DEV_ERROR_REPORT == STD_ON)
          (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID, SCC_API_ID_V_STATEM_PROC_CHARGE_PARAM, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
          break;
      }
    }
  }
  else
  {
    /* #40 continue with ChargeParameterDiscovery */
    /* check if the delay counter is active */
    if ( 0u < Scc_StateM_Counter.GeneralDelayCnt )
    {
      Scc_StateM_Counter.GeneralDelayCnt--;
      /* check if the delay counter has not elapsed yet */
      if ( 0u != Scc_StateM_Counter.GeneralDelayCnt )
      {
        /* return pending */
        retVal = Scc_ReturnType_Pending;
      }
      else
      {
        retVal = Scc_ReturnType_OK;
      }
    }
    /* if the delay counter is still disabled */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
    else if ( 0u < (uint16)Scc_ConfigValue_StateM_ChargeParameterDiscoveryNextReqDelay ) /* PRQA S 2741 */ /* MD_Scc_VariantDependent */ /*lint !e506 */
    {
      /* enable the delay counter */
      Scc_StateM_Counter.GeneralDelayCnt = (uint16)Scc_ConfigValue_StateM_ChargeParameterDiscoveryNextReqDelay;
      /* return pending */
      retVal = Scc_ReturnType_Pending;
    }
    else
    { /* PRQA S 2880 1 */ /* MD_Scc_2742_2880_2995 */
      retVal = Scc_ReturnType_OK;
    }
  }

  return retVal;
} /* PRQA S 6030 */ /* MD_MSR_STCYC */


#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_ProcessCableCheckRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessCableCheckRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  /* check if the EVSE signals that CableCheck has finished */
  if ( Scc_EVSEProcessing_Finished == Scc_StateM_RxEVSE.EVSEProcessing )
  {
    /* #10 Reset cycle timer and delay timer if processing is finished */
    /* reset the cycle timer */
    Scc_StateM_Counter.OngoingTimerCnt = 0;
    /* reset the delay timer */
    Scc_StateM_Counter.GeneralDelayCnt = 0;

    /* set the new state */
    Scc_StateM_State = Scc_SMS_PreCharge;

    retVal = Scc_ReturnType_OK;
  }
  /* continue with CableCheck */
  else
  {
    /* #20 Check the delay counter */
    /* check if the delay counter is active */
    if ( 0u < Scc_StateM_Counter.GeneralDelayCnt )
    {
      Scc_StateM_Counter.GeneralDelayCnt--;
      /* check if the delay counter has not elapsed yet */
      if ( 0u != Scc_StateM_Counter.GeneralDelayCnt )
      {
        /* return pending */
        retVal = Scc_ReturnType_Pending;
      }
      else
      {
        retVal = Scc_ReturnType_OK;
      }
    }
    /* if the delay counter is still disabled */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
    else if (0u < (uint16)Scc_ConfigValue_StateM_CableCheckNextReqDelay) /* PRQA S 2741 */ /* MD_Scc_VariantDependent */ /*lint !e506 */
    {
      /* enable the delay counter */
      Scc_StateM_Counter.GeneralDelayCnt = (uint16)Scc_ConfigValue_StateM_CableCheckNextReqDelay;
      /* return pending */
      retVal = Scc_ReturnType_Pending;
    }
    else
    { /* PRQA S 2880 1 */ /* MD_MSR_Unreachable */
      retVal = Scc_ReturnType_OK;
    }
  }

  return retVal;
}
#endif /* SCC_CHARGING_DC */

#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_ProcessPreChargeRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessPreChargeRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  /* get the current ChargingControl */
  Scc_Get_StateM_ChargingControl(&Scc_StateM_Local.ChargingControl);

  /* check if the application wants to stop the PreCharge sequence */
  if ( Scc_ChargingControl_PreChargeCompleted == Scc_StateM_Local.ChargingControl )
  {
    /* #10 Reset cycle timer and delay timer if processing is finished */
    /* reset the cycle timer */
    Scc_StateM_Counter.OngoingTimerCnt = 0;
    /* reset the delay timer */
    Scc_StateM_Counter.GeneralDelayCnt = 0;
    /* set the new state */
    Scc_StateM_State = Scc_SMS_PowerDelivery;

    retVal = Scc_ReturnType_OK;
  }
  /* continue with PreCharge */
  else
  {
    /* #20 Check the delay counter */
    /* check if the delay counter is active */
    if ( 0u < Scc_StateM_Counter.GeneralDelayCnt )
    {
      Scc_StateM_Counter.GeneralDelayCnt--;
      /* check if the delay counter has not elapsed yet */
      if ( 0u != Scc_StateM_Counter.GeneralDelayCnt )
      {
        /* wait */
        retVal = Scc_ReturnType_Pending;
      }
      else
      {
        retVal = Scc_ReturnType_OK;
      }
    }
    /* if the delay counter is still disabled */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
    else if ( 0u < (uint16)Scc_ConfigValue_StateM_PreChargeNextReqDelay ) /* PRQA S 2741 */ /* MD_Scc_VariantDependent */ /*lint !e506 */
    {
      /* enable the delay counter */
      Scc_StateM_Counter.GeneralDelayCnt = (uint16)Scc_ConfigValue_StateM_PreChargeNextReqDelay;
      /* return pending */
      retVal = Scc_ReturnType_Pending;
    }
    else
    {  /* PRQA S 2880 1 */ /* MD_MSR_Unreachable */
      retVal = Scc_ReturnType_OK;
    }
  }

  return retVal;
}
#endif /* SCC_CHARGING_DC */

/**********************************************************************************************************************
 *  Scc_StateM_ProcessPowerDeliveryRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessPowerDeliveryRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal = Scc_ReturnType_NotOK;

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Check if charging shall be started */
  if ( Scc_ChargingControl_StartPowerDelivery == Scc_StateM_Local.ChargingControl )
  {
#if (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
    if ( Scc_SAPSchemaIDs_ISO_Ed2_DIS == Scc_StateM_RxEVSE.SAPSchemaID )
    {
      /* #20 Set next state for ISO ED2 depending on the EnergyTransferService */
      switch ( (Scc_StateM_SupportedServiceIDType)Scc_StateM_TxEVSE.SelectedEnergyTransferService.ServiceID ) /* PRQA S 4342 */ /* MD_Scc_Exi_Generic */
      {
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
        case Scc_SID_DC_Charging:
        {
          /* set the next state */
          Scc_StateM_State = Scc_SMS_CurrentDemand;
          retVal = Scc_ReturnType_OK;
          break;
        }
#endif /* SCC_CHARGING_DC */
#if(SCC_CHARGING_WPT == STD_ON)
        case Scc_SID_WPT_Charging:
        {
          Scc_StateM_State = Scc_SMS_PowerDemand;
          retVal = Scc_ReturnType_OK;
          break;
        }
#endif
#if(SCC_CHARGING_AC_BPT == STD_ON)
        case Scc_SID_AC_BPT:
        {
          Scc_StateM_State = Scc_SMS_AC_BidirectionalControl;
          retVal = Scc_ReturnType_OK;
          break;
        }
#endif
#if(SCC_CHARGING_DC_BPT == STD_ON)
        case Scc_SID_DC_BPT:
        {
          Scc_StateM_State = Scc_SMS_DC_BidirectionalControl;
          retVal = Scc_ReturnType_OK;
          break;
        }
#endif
        default:
          /* this is an invalid state */
#if(SCC_DEV_ERROR_REPORT == STD_ON)
          (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID,
            SCC_API_ID_V_STATEM_PROC_POW_DEL, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
          break;
      }
    }
    else
#endif /* SCC_SCHEMA_ISO_ED2 */
    {
      /* #30 Set next state for ISO ED1 depending on the EnergyTransferMode */
      switch ( Scc_StateM_TxEVSE.EnergyTransferMode )
      {

#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
      /* check for DC Charging */
      case Scc_EnergyTransferMode_DC_Combo_Core:
      case Scc_EnergyTransferMode_DC_Core:
      case Scc_EnergyTransferMode_DC_Extended:
      case Scc_EnergyTransferMode_DC_Unique:
        /* set the next state */
        Scc_StateM_State = Scc_SMS_CurrentDemand;
        retVal = Scc_ReturnType_OK;
        break;
#endif /* SCC_CHARGING_DC */

      default:
        /* this is an invalid state */
#if(SCC_DEV_ERROR_REPORT == STD_ON)
        (void)Det_ReportError(SCC_MODULE_ID, SCC_INSTANCE_ID,
          SCC_API_ID_V_STATEM_PROC_POW_DEL, SCC_DET_INV_PARAM);
#endif /* SCC_DEV_ERROR_REPORT */
        break;
      }
    }
  }
#if (( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
  /* #40 Check if a renegotiation shall be triggered */
  else if ( Scc_ChargingControl_Renegotiation == Scc_StateM_Local.ChargingControl )
  {
    /* set the next state */
    Scc_StateM_State = Scc_SMS_ChargeParameterDiscovery;
    retVal = Scc_ReturnType_OK;
  }
#endif /* SCC_SCHEMA_ISO  || SCC_SCHEMA_ISO_ED2 */
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
  /* #50 Check if a welding detection shall be started */
  else if ( Scc_ChargingControl_StartWeldingDetection == Scc_StateM_Local.ChargingControl )
  {
    Scc_Get_StateM_ControlPilotState(&Scc_StateM_Local.ControlPilotState);

    /* check if the power switch is still closed */
    if ( Scc_ControlPilotState_State_C_D == Scc_StateM_Local.ControlPilotState )
    {
      retVal = Scc_ReturnType_Pending;
    }
    else
    {
      /* set the next state */
      Scc_StateM_State = Scc_SMS_WeldingDetection;

      retVal = Scc_ReturnType_OK;
    }
  }
#endif /* SCC_CHARGING_DC */
  /* #60 Check if the session shall be paused or stopped */
  else if ( Scc_ChargingControl_StopCharging == Scc_StateM_Local.ChargingControl ) /* PRQA S 2004 */ /* MD_Scc_2004 */
  {
    Scc_Get_StateM_ControlPilotState(&Scc_StateM_Local.ControlPilotState);

    /* check if the power switch is still closed */
    if ( Scc_ControlPilotState_State_C_D == Scc_StateM_Local.ControlPilotState )
    {
      retVal = Scc_ReturnType_Pending;
    }
    else
    {
      /* set the next state */
      Scc_StateM_State = Scc_SMS_SessionStop;

      retVal = Scc_ReturnType_OK;
    }
  }
  else
  {
    retVal = Scc_ReturnType_OK;
  }

  return retVal;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */




#if ( defined SCC_CHARGING_DC_BPT ) && ( SCC_CHARGING_DC_BPT == STD_ON )
/**********************************************************************************************************************
*  Scc_StateM_ProcessDC_BidirectionalControlRes
*********************************************************************************************************************/
/*!
*
* Internal comment removed.
 *
 *
 *
*/
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessDC_BidirectionalControlRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  Scc_Get_StateM_ChargingControl(&Scc_StateM_Local.ChargingControl);

#if ( defined SCC_ENABLE_PNC_CHARGING ) && ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  /* #10 check if no metering receipt was requested or if the eim profile is active */
  if ( (FALSE == Scc_StateM_RxEVSE.ReceiptRequired)
    || (Scc_PaymentOption_EIM == Scc_StateM_TxEVSE.SelectedPaymentOption) )
#endif /* SCC_ENABLE_PNC_CHARGING */
  {
    /* #20 check if the charging shall be stopped */
    if ( (Scc_ChargingControl_StartWeldingDetection == Scc_StateM_Local.ChargingControl)
      || (Scc_ChargingControl_Renegotiation == Scc_StateM_Local.ChargingControl)
      || (Scc_ChargingControl_StopCharging == Scc_StateM_Local.ChargingControl) )
    {
      /* reset the delay */
      Scc_StateM_Counter.GeneralDelayCnt = 0;
      /* set the next state */
      Scc_StateM_State = Scc_SMS_PowerDelivery;

      retVal = Scc_ReturnType_OK;
    }
    /* otherwise send another DC_BidirectionalControlReq */
    else
    {
      /* check if the delay counter is active */
      if ( 0u < Scc_StateM_Counter.GeneralDelayCnt )
      {
        Scc_StateM_Counter.GeneralDelayCnt--;
        /* if the next request has to be further delayed */
        if ( 0u != Scc_StateM_Counter.GeneralDelayCnt )
        {
          /* wait */
          retVal = Scc_ReturnType_Pending;
        }
        else
        {
          retVal = Scc_ReturnType_OK;
        }
      }
      /* if the delay counter is still disabled */
      else if ( 0u < Scc_ConfigValue_StateM_DC_BidirectionalControlNextReqDelay ) /* PRQA S 2742, 2004 */ /* MD_Scc_2742_2880_2995, MD_Scc_2004 */ /*lint !e506 */
      {
        /* enable the delay counter */ /* PRQA S 2880 1 */ /* MD_Scc_2742_2880_2995 */
        Scc_StateM_Counter.GeneralDelayCnt = (uint16)Scc_ConfigValue_StateM_DC_BidirectionalControlNextReqDelay;
        /* wait */
        retVal = Scc_ReturnType_Pending;
      }
      else
      {
        retVal = Scc_ReturnType_OK;
      }
    }
  }
#if ( defined SCC_ENABLE_PNC_CHARGING ) && ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  /* if metering receipt was requested and pnc profile is active */
  else
  {
    /* reset the delay */
    Scc_StateM_Counter.GeneralDelayCnt = 0;
    /* set the new state */
    Scc_StateM_State = Scc_SMS_MeteringReceipt;

    retVal = Scc_ReturnType_OK;
  }
#endif /* SCC_ENABLE_PNC_CHARGING */

  return retVal;
}
#endif /* SCC_CHARGING_DC_BPT */

#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )

/**********************************************************************************************************************
 *  Scc_StateM_ProcessCurrentDemandRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessCurrentDemandRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  Scc_Get_StateM_ChargingControl(&Scc_StateM_Local.ChargingControl);

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  /* #10 check if no metering receipt was requested or if the eim profile is active */
  if (   ( FALSE == Scc_StateM_RxEVSE.ReceiptRequired )
      || ( Scc_PaymentOption_EIM == Scc_StateM_TxEVSE.SelectedPaymentOption ) )
#endif /* SCC_ENABLE_PNC_CHARGING */
  {
    /* #20 check if the charging shall be stopped */
    if (   ( Scc_ChargingControl_StartWeldingDetection == Scc_StateM_Local.ChargingControl )
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
        || ( Scc_ChargingControl_Renegotiation == Scc_StateM_Local.ChargingControl )
#endif /* SCC_SCHEMA_ISO */
        || ( Scc_ChargingControl_StopCharging == Scc_StateM_Local.ChargingControl ) )
    {
      /* reset the delay */
      Scc_StateM_Counter.GeneralDelayCnt = 0;
      /* set the next state */
      Scc_StateM_State = Scc_SMS_PowerDelivery;

      retVal = Scc_ReturnType_OK;
    }
    /* otherwise send another CurrentDemandReq */
    else
    {
      /* check if the delay counter is active */
      if ( 0u < Scc_StateM_Counter.GeneralDelayCnt )
      {
        /* decrement the delay counter */
        Scc_StateM_Counter.GeneralDelayCnt--;
        /* check if the delay counter has not elapsed yet */
        if ( 0u != Scc_StateM_Counter.GeneralDelayCnt )
        {
          /* wait */
          retVal = Scc_ReturnType_Pending;
        }
        else
        {
          retVal = Scc_ReturnType_OK;
        }
      }
      /* if the delay counter is still disabled */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
      else if ( 0u < (uint16)Scc_ConfigValue_StateM_CurrentDemandNextReqDelay ) /* PRQA S 2741 */ /* MD_Scc_VariantDependent */ /*lint !e506 */
      {
        /* enable the delay counter */
        Scc_StateM_Counter.GeneralDelayCnt = (uint16)Scc_ConfigValue_StateM_CurrentDemandNextReqDelay;
        /* wait */
        retVal = Scc_ReturnType_Pending;
      }
      else
      {  /* PRQA S 2880 1 */ /* MD_MSR_Unreachable */
        retVal = Scc_ReturnType_OK;
      }
    }
  }
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  /* if metering receipt was requested and pnc profile is active */
  else
  {
    /* reset the delay */
    Scc_StateM_Counter.GeneralDelayCnt = 0;
    /* set the new state */
    Scc_StateM_State = Scc_SMS_MeteringReceipt;

    retVal = Scc_ReturnType_OK;
  }
#endif /* SCC_ENABLE_PNC_CHARGING */

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_StateM_ProcessWeldingDetectionRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessWeldingDetectionRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  /* get the current ChargingControl */
  Scc_Get_StateM_ChargingControl(&Scc_StateM_Local.ChargingControl);

  /* check if the charging shall be stopped */
  if ( Scc_ChargingControl_StopCharging == Scc_StateM_Local.ChargingControl )
  {
    /* #10 Reset cycle timer and delay timer */
    /* reset the cycle timer */
    Scc_StateM_Counter.OngoingTimerCnt = 0;
    /* reset the delay timer */
    Scc_StateM_Counter.GeneralDelayCnt = 0;
    /* set the new state */
    Scc_StateM_State = Scc_SMS_SessionStop;

    retVal = Scc_ReturnType_OK;
  }
  else
  {
    /* #20 continue with WeldingDetectionRes */
    /* check if the delay counter is active */
    if ( 0u < Scc_StateM_Counter.GeneralDelayCnt )
    {
      Scc_StateM_Counter.GeneralDelayCnt--;
      /* if the next request has to be further delayed */
      if ( 0u != Scc_StateM_Counter.GeneralDelayCnt )
      {
        /* return pending */
        retVal = Scc_ReturnType_Pending;
      }
      else
      {
        retVal = Scc_ReturnType_OK;
      }
    }
    /* if the delay counter is still disabled */ /* PRQA S 2004 1 */ /* MD_Scc_2004 */
    else if ( 0u < (uint16)Scc_ConfigValue_StateM_WeldingDetectionNextReqDelay ) /* PRQA S 2741 */ /* MD_Scc_VariantDependent */ /*lint !e506 */
    {
      /* enable the delay counter */
      Scc_StateM_Counter.GeneralDelayCnt = (uint16)Scc_ConfigValue_StateM_WeldingDetectionNextReqDelay;
      /* return pending */
      retVal = Scc_ReturnType_Pending;
    }
    else
    {  /* PRQA S 2880 1 */ /* MD_MSR_Unreachable */
      retVal = Scc_ReturnType_OK;
    }
  }

  return retVal;
}

#endif /* SCC_CHARGING_DC */

#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
/**********************************************************************************************************************
 *  Scc_StateM_ProcessMeteringReceiptRes
 *********************************************************************************************************************/
 /*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessMeteringReceiptRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal = E_NOT_OK;

  /* ----- Implementation ----------------------------------------------- */
  Scc_Get_StateM_ChargingControl(&Scc_StateM_Local.ChargingControl);

  /* #10 check if the charging shall be stopped */
  if (   ( Scc_ChargingControl_StopCharging == Scc_StateM_Local.ChargingControl )
#if (( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
      || ( Scc_ChargingControl_Renegotiation == Scc_StateM_Local.ChargingControl )
#endif /* SCC_SCHEMA_ISO */
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
      || ( Scc_ChargingControl_StartWeldingDetection == Scc_StateM_Local.ChargingControl )
#endif /* SCC_CHARGING_DC */
    )
  {
    /* stop the charging session */
    Scc_StateM_State = Scc_SMS_PowerDelivery;

    retVal = E_OK;
  }
  /* the module will continue with charging */
  else
  {
#if (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ))
    /* #20 Set next message state for ISO ED2 depending on the EnergyTransferService */
    if (Scc_SAPSchemaIDs_ISO_Ed2_DIS == Scc_StateM_RxEVSE.SAPSchemaID)
    {
      switch ( (Scc_StateM_SupportedServiceIDType)Scc_StateM_TxEVSE.SelectedEnergyTransferService.ServiceID ) /* PRQA S 4342 */ /* MD_Scc_Exi_Generic */
      {
#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
        case Scc_SID_DC_Charging:
        {
          Scc_StateM_State = Scc_SMS_CurrentDemand;
          retVal = E_OK;
          break;
        }
#endif /* SCC_CHARGING_DC */
#if(SCC_CHARGING_WPT == STD_ON)
        case Scc_SID_WPT_Charging:
        {
          Scc_StateM_State = Scc_SMS_PowerDemand;
          retVal = E_OK;
          break;
        }
#endif
#if(SCC_CHARGING_AC_BPT == STD_ON)
        case Scc_SID_AC_BPT:
        {
          Scc_StateM_State = Scc_SMS_AC_BidirectionalControl;
          retVal = E_OK;
          break;
        }
#endif
#if(SCC_CHARGING_DC_BPT == STD_ON)
        case Scc_SID_DC_BPT:
        {
          Scc_StateM_State = Scc_SMS_DC_BidirectionalControl;
          retVal = E_OK;
          break;
        }
#endif
        default: /* PRQA S 2016 */ /* MD_MSR_EmptyClause */
          break;
      }
    }
    else
#endif /* SCC_SCHEMA_ISO_ED2 */
    {
      /* #30 Set next message state for ISO ED1 depending on the EnergyTransferService */
      switch (Scc_StateM_TxEVSE.EnergyTransferMode)
      {

#if ( defined SCC_CHARGING_DC ) && ( SCC_CHARGING_DC == STD_ON )
        /* check for DC Charging */
      case Scc_EnergyTransferMode_DC_Combo_Core:
      case Scc_EnergyTransferMode_DC_Core:
      case Scc_EnergyTransferMode_DC_Extended:
      case Scc_EnergyTransferMode_DC_Unique:
        /* set the next state */
        Scc_StateM_State = Scc_SMS_CurrentDemand;
        retVal = E_OK;
        break;
      default:
        Scc_ReportError(Scc_StackError_InvalidRxParameter);
        break;
#endif /* SCC_CHARGING_DC */
      } /* PRQA S 2002 */ /* MD_Scc_2002 */
    }
  }

  return retVal;

}
#endif /* SCC_ENABLE_PNC_CHARGING */

/**********************************************************************************************************************
 *  Scc_StateM_ProcessSessionStopRes
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_ProcessSessionStopRes(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
  /* #10 check if DIN schema is active */
  if ( Scc_SAPSchemaIDs_DIN == Scc_StateM_RxEVSE.SAPSchemaID )
  {
    Scc_Get_StateM_ControlPilotState(&Scc_StateM_Local.ControlPilotState);
    /* #20 check if the power switch is closed (V2G-DC-750) */
    if ( Scc_ControlPilotState_State_C_D == Scc_StateM_Local.ControlPilotState )
    {
      retVal = Scc_ReturnType_Pending;
    }
    else
    {
      /* switch to the next state */
      Scc_StateM_State = Scc_SMS_StopCommunicationSession;

      retVal = Scc_ReturnType_OK;
    }
  }
  else
#endif /* SCC_SCHEMA_DIN */
  {
    /* switch to the next state */
    Scc_StateM_State = Scc_SMS_StopCommunicationSession;

    retVal = Scc_ReturnType_OK;
  }

  return retVal;
}

/**********************************************************************************************************************
 *  Scc_StateM_ProcessStopCommunicationSession
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ProcessStopCommunicationSession(void)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 switch to the next state */
  Scc_StateM_State = Scc_SMS_Stopped;

  /* #20 update the status */
  Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_Finished);

}

/**********************************************************************************************************************
 *  Scc_StateM_GlobalTimerChecks
 *********************************************************************************************************************/
 /*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Std_ReturnType, SCC_STATIC_CODE) Scc_StateM_GlobalTimerChecks(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Std_ReturnType retVal;

  /* ----- Implementation ----------------------------------------------- */
  Scc_Get_StateM_ControlPilotState(&Scc_StateM_Local.ControlPilotState);

  /* #10 check if a cable is plugged in */
  if ( Scc_ControlPilotState_State_A_E_F < Scc_StateM_Local.ControlPilotState )
  {
    /* #20 check the sequence performance timer */
    if ( 0u < Scc_StateM_Counter.SequencePerformanceTimerCnt )
    {
      /* decrement the timer */
      Scc_StateM_Counter.SequencePerformanceTimerCnt--;
      /* check if the timer elapsed */
      if ( 0u == Scc_StateM_Counter.SequencePerformanceTimerCnt )
      {
        /* start error handling */
        Scc_StateM_ErrorHandling(Scc_SMER_Timer_SequencePerformanceTimer);
        retVal = E_NOT_OK;
      }
      else
      {
        retVal = E_OK;
      }
    }
    else
    {
      retVal = E_OK;
    }

    if ( retVal == E_OK )
    {
      /* #30 check the communication setup timer */
      if ( 0u < Scc_StateM_Counter.CommunicationSetupTimerCnt )
      {
        uint16 Timeout;

        switch ( Scc_StateM_RxEVSE.SAPSchemaID )
        {
  #if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
        case Scc_SAPSchemaIDs_ISO:
          Timeout = (uint16)Scc_ConfigValue_Timer_ISO_CommunicationSetupTimeout;
          break;
  #endif /* SCC_SCHEMA_ISO */

  #if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )
        case Scc_SAPSchemaIDs_ISO_Ed2_DIS:
          Timeout = (uint16)Scc_ConfigValue_Timer_ISO_CommunicationSetupTimeout;
          break;
  #endif /* SCC_SCHEMA_ISO_ED2 */

  #if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
        case Scc_SAPSchemaIDs_DIN:
          Timeout = (uint16)Scc_ConfigValue_Timer_DIN_CommunicationSetupTimeout;
          break;
  #endif /* SCC_SCHEMA_DIN */

        default:
  #if ( (( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO == 1 )) || (( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 == 1 )))
          Timeout = (uint16)Scc_ConfigValue_Timer_ISO_CommunicationSetupTimeout;
  #else
  #if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN == 1 )
          Timeout = (uint16)Scc_ConfigValue_Timer_DIN_CommunicationSetupTimeout;
  #endif /* SCC_SCHEMA_DIN */
  #endif /* SCC_SCHEMA_ISO || SCC_SCHEMA_ISO_ED2 */
          break;
        }

        /* #40 check if the timer elapsed */
        if ( Timeout == Scc_StateM_Counter.CommunicationSetupTimerCnt )
        {
          /* stop the communication setup timer */
          Scc_StateM_Counter.CommunicationSetupTimerCnt = 0;
          /* start error handling */
          Scc_StateM_ErrorHandling(Scc_SMER_Timer_CommunicationSetupTimer);
          retVal = E_NOT_OK;
        }
        /* the timer has not yet elapsed */
        else
        {
          /* continue to count the timer */
          Scc_StateM_Counter.CommunicationSetupTimerCnt++;
          retVal = E_OK;
        }
      }
      else
      {
        retVal = E_OK;
      }
    }
    /* Handle ready to charge timer (only for DIN charging) */
#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
    if ( retVal == E_OK )
    {
      /* Check if ready to charge timer is not started yet */
      if  (( Scc_StateM_Counter.ReadyToChargeTimerStart == FALSE )
        && ( Scc_StateM_Counter.ReadyToChargeTimerCnt == 0u ))
      {
        /* get the current PWM state */
        Scc_Get_StateM_PWMState(&Scc_StateM_Local.PWMState);

        /* [V2G-DC-373] Start ready to charge timer when CP State B with 5% duty cycle is indicated */
        if (Scc_StateM_Local.PWMState == Scc_PWMState_HLCOnly)
        {
          Scc_StateM_Counter.ReadyToChargeTimerStart = TRUE;
          Scc_StateM_Counter.ReadyToChargeTimerCnt = 1;
        }
      }
      /* #50 check the ready to charge timer */
      else if (( Scc_StateM_Counter.ReadyToChargeTimerStart == TRUE )
            && ( Scc_StateM_Counter.ReadyToChargeTimerCnt != 0u ))
      {
        uint16 Timeout = 0xFFFF;

        if ( Scc_SAPSchemaIDs_DIN == Scc_StateM_RxEVSE.SAPSchemaID )
        {
          /* Set ready to charge timer */
          Timeout = (uint16)Scc_ConfigValue_Timer_DIN_ReadyToChargeTimeout;
        }

        /* check if the timer elapsed */
        if ( Timeout == Scc_StateM_Counter.ReadyToChargeTimerCnt )
        {
          /* stop the ready to charge timer */
          Scc_StateM_Counter.ReadyToChargeTimerStart = FALSE;
          /* start error handling */
          Scc_StateM_ErrorHandling(Scc_SMER_Timer_ReadyToChargeTimer);
          retVal = E_NOT_OK;
        }
        /* the timer has not yet elapsed */
        else
        {
          /* continue to count the timer */
          Scc_StateM_Counter.ReadyToChargeTimerCnt++;
        }
      }
      else
      {
        retVal = E_OK;
      }
    }
#endif /* SCC_SCHEMA_DIN */
  }
  else
  {
    retVal = E_OK;
  }

  return retVal;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_StateM_GlobalParamsChecks
 *********************************************************************************************************************/
 /*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(Scc_ReturnType, SCC_STATIC_CODE) Scc_StateM_GlobalParamsChecks(void)
{
  /* ----- Local Variables ---------------------------------------------- */
  Scc_ReturnType                          retVal = Scc_ReturnType_OK;

  /* ----- Implementation ----------------------------------------------- */
  /* get the current state of the cable */
  Scc_Get_StateM_ControlPilotState(&Scc_StateM_Local.ControlPilotState);
  /* get the current function command of the application */
  Scc_Get_StateM_FunctionControl(&Scc_StateM_Local.FunctionControl);

  /* #10 check if the module shall be reset */
  if (Scc_FunctionControl_Reset == Scc_StateM_Local.FunctionControl)
  {
    /* only reset if state is higher than INIT */
    if ((Scc_SMS_Initialized < Scc_StateM_State)
      && (Scc_SMS_Stopped > Scc_StateM_State))
    {
      /* init the params */
      Scc_StateM_InitParams();
      /* set the state to INIT */
      Scc_StateM_State = Scc_SMS_Initialized;
      retVal = Scc_ReturnType_Pending;
    }
  }

  /* #20 check if the control pilot changed */
  if ( Scc_StateM_LastControlPilotState != Scc_StateM_Local.ControlPilotState )
  {
    /* update the function-local control pilot status */
    Scc_StateM_LastControlPilotState = Scc_StateM_Local.ControlPilotState;

    /* check for error state (A = not connected, E = no Grid connecte, F = EVSE not available) or CP is turned off */
    if ( Scc_ControlPilotState_State_A_E_F == Scc_StateM_Local.ControlPilotState )
    {
      /* reset the retries */
      Scc_StateM_Counter.ReconnectRetryCnt = (uint16)Scc_ConfigValue_StateM_ReconnectRetries;

      /* reset the VAS settings */
#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
      Scc_StateM_Local.ReqCertDetails = (boolean)Scc_ConfigValue_StateM_RequestCertificateDetails;
#endif /* SCC_ENABLE_PNC_CHARGING */
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
      Scc_StateM_Local.ReqInetDetails = (boolean)Scc_ConfigValue_StateM_RequestInternetDetails;
#endif /* SCC_SCHEMA_ISO */

      /* Check if plug out (state A,E,F) is allowed */
      if ( ( Scc_SMS_Initialized < Scc_StateM_State )
        && ( Scc_SMS_StopCommunicationSession > Scc_StateM_State ))
      {
        /* plug out while in active charging session */
        Scc_StateM_ErrorHandling(Scc_SMER_InvalidControlPilotState);
      }

      /* Reset SessionID */
      (void) Scc_ResetSessionID(TRUE);
    }
  }

  /* #30 check if the cable is not plugged out */
  if (Scc_ControlPilotState_State_A_E_F != Scc_StateM_Local.ControlPilotState)
  {
#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
    /* check if DIN charging is active */
    if (Scc_SAPSchemaIDs_DIN == Scc_StateM_RxEVSE.SAPSchemaID)
    {
      /* check if PowerDelivery was not sent */
      if (   (   (Scc_SMS_PowerDelivery > Scc_StateM_State)
              && (Scc_SMS_SessionSetup <= Scc_StateM_State))
          /* detect case that PreCharge is completed, but trigger to send PowerDeliveryReq isn't set */
          || (   (Scc_SMS_PowerDelivery == Scc_StateM_State)
              && (Scc_MsgStatus_PreCharge_OK == Scc_StateM_RxCore.MsgStatus)
              && (Scc_MsgTrig_PowerDelivery != Scc_StateM_TxCore.MsgTrig)))
      {
        /* check if the application wants to stop */
        Scc_Get_StateM_ChargingControl(&Scc_StateM_Local.ChargingControl);
        if (Scc_ChargingControl_StopCharging == Scc_StateM_Local.ChargingControl)
        {
          /* check if we are not waiting for a response */
          if ((Scc_SMMS_WaitingForResponse != Scc_StateM_MsgState)
            || ((((uint8)Scc_StateM_TxCore.MsgTrig * 2u) == (uint8)Scc_StateM_RxCore.MsgStatus)
              || ((((uint8)Scc_StateM_TxCore.MsgTrig * 2u) - 1u) == (uint8)Scc_StateM_RxCore.MsgStatus)))
          {
            /* stop the delay timer */
            Scc_StateM_Counter.GeneralDelayCnt = 0;
            /* reset the cycle timer */
            Scc_StateM_Counter.OngoingTimerCnt = 0;
            /* go directly to SessionStop */
            Scc_StateM_State = Scc_SMS_SessionStop;
            /* go directly to SessionStop */
#if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
            Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
#else /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
            Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)
#endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */

          }
        }
      }
    }
#endif /* SCC_SCHEMA_DIN */

    /* #40 check if there was an error with the transport layer */
    if (Scc_StackError_TransportLayer == Scc_StateM_RxCore.StackError)
    {
      /* check if there is an active V2G communication session */
      if ((Scc_SMS_SupportedAppProtocol <= Scc_StateM_State)
        && (Scc_SMS_SessionStop >= Scc_StateM_State))
      {
        /* this is not allowed to happen */
        Scc_StateM_ErrorHandling(Scc_SMER_StackError);
        retVal = Scc_ReturnType_NotOK;
      }
    }
  }

  return retVal;
} /* PRQA S 6010,6030,6050,6080 */ /* MD_MSR_STPTH,MD_MSR_STCYC,MD_MSR_STCAL,MD_MSR_STMIF */

/**********************************************************************************************************************
 *  Scc_StateM_InitParams
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_InitParams(void)
{
  /* ----- Implementation ----------------------------------------------- */

  /* #10 Init the global parameters Scc_StateM_T/RxCore, Scc_StateM_Local, Scc_StateM_Counter & Scc_StateM_T/RxEVSE */
  Scc_StateM_MsgState = Scc_SMMS_PreparingRequest;

  Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_None;
  Scc_StateM_TxCore.CyclicMsgTrig = FALSE;
  Scc_StateM_TxCore.ReqSDPSecurity = Scc_SDPSecurity_Tls;

  Scc_StateM_RxCore.CyclicMsgRcvd = FALSE;
  Scc_StateM_RxCore.MsgStatus = Scc_MsgStatus_None;
  Scc_StateM_RxCore.StackError = Scc_StackError_NoError;
  Scc_StateM_RxCore.SupSDPSecurity = Scc_SDPSecurity_None;

#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
  Scc_StateM_Local.InternetAvailable = FALSE;
#endif /* SCC_SCHEMA_ISO */

  Scc_StateM_Local.ChargingControl = Scc_ChargingControl_None;
  Scc_StateM_Local.EVTimeStamp = 0xFFFFFFFFU;
  Scc_StateM_Local.FunctionControl = Scc_FunctionControl_None;
  Scc_StateM_Local.ControlPilotState = Scc_ControlPilotState_None;
  Scc_StateM_Local.PWMState = Scc_PWMState_ChargingNotAllowed;

  Scc_StateM_TxEVSE.ChargeStatus = Scc_ChargeStatus_Preparing;
  Scc_StateM_TxEVSE.EnergyTransferMode = Scc_EnergyTransferMode_None;
  Scc_StateM_TxEVSE.SelectedPaymentOption = Scc_PaymentOption_None;

#if ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 )
  Scc_StateM_TxEVSE.EVProcessing = EXI_ISO_ED2_DIS_PROCESSING_TYPE_FINISHED;
  Scc_StateM_TxEVSE.SelectedEnergyTransferService.ServiceID = 0;
  Scc_StateM_TxEVSE.SelectedEnergyTransferService.ParameterSetID = 0;
#endif /* SCC_SCHEMA_ISO_ED2 */

  Scc_StateM_RxEVSE.EVSEOfferedPaymentOptions = Scc_PaymentPrioritization_OnlyEIM;
  Scc_StateM_RxEVSE.EVSEProcessing = Scc_EVSEProcessing_Finished;
  Scc_StateM_RxEVSE.ProvServiceID = 0;
  Scc_StateM_RxEVSE.ReceiptRequired = FALSE;
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
  Scc_StateM_RxEVSE.SAPSchemaID = Scc_SAPSchemaIDs_ISO;
#else
# if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
  Scc_StateM_RxEVSE.SAPSchemaID = Scc_SAPSchemaIDs_DIN;
# endif /* SCC_SCHEMA_DIN */
#endif /* SCC_SCHEMA_ISO */


#if ( SCC_ENABLE_PNC_CHARGING == STD_ON )
  Scc_StateM_Local.CertDetailsRequested = FALSE;
  Scc_StateM_Local.ReqCertInstall = FALSE;
  Scc_StateM_Local.ReqCertUpdate = FALSE;
#endif /* SCC_ENABLE_PNC_CHARGING */
#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
  Scc_StateM_Local.InetDetailsRequested = FALSE;
#endif /* SCC_SCHEMA_ISO */
  Scc_StateM_Local.SendChargingProfile = TRUE;

  Scc_StateM_Counter.OngoingTimerCnt = 0;
  Scc_StateM_Counter.GeneralDelayCnt = 0;
  Scc_StateM_Counter.IPAddressWaitTimerCnt = 0;
  Scc_StateM_Counter.NvmReadTimerCnt = 0;
  Scc_StateM_Counter.SequencePerformanceTimerCnt = 0;
  Scc_StateM_Counter.ReadyToChargeTimerCnt = 0;
  Scc_StateM_Counter.ReadyToChargeTimerStart = FALSE;

}

/**********************************************************************************************************************
 *  Scc_StateM_ErrorHandling
 *********************************************************************************************************************/
/*!
 *
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
SCC_LOCAL FUNC(void, SCC_STATIC_CODE) Scc_StateM_ErrorHandling(Scc_StateM_StateMachineErrorType ErrorReason)
{
  /* ----- Implementation ----------------------------------------------- */

  /* #10 check if active session is a DIN charging session and if message is before ChargeParameterDiscovery and after SessionSetup (V2G-DC-649) */
#if ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 )
  if (( Scc_StateM_RxEVSE.SAPSchemaID == Scc_SAPSchemaIDs_DIN )
    && ( Scc_SMS_SessionSetup < Scc_StateM_State )
    && ( Scc_SMS_ChargeParameterDiscovery > Scc_StateM_State )
    && ( Scc_StateM_RxCore.StackError == Scc_StackError_NegativeResponseCode ) )
  {
    /* #20 report the error to the application */
    Scc_Set_StateM_StateMachineError(ErrorReason);

    /* #30 request SessionStop */
    Scc_StateM_State = Scc_SMS_SessionStop;

    Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)

    /* #40 if there are still retries left and the error was not caused by the NvM */
    if ( ( 0u < Scc_StateM_Counter.ReconnectRetryCnt )
      && ( Scc_StackError_NvM != Scc_StateM_RxCore.StackError ) )
    {
      /* decrement the retries */
      Scc_StateM_Counter.ReconnectRetryCnt--;
      /* start the delay timer for the reconnect */
      Scc_StateM_Counter.GeneralDelayCnt = (uint16)Scc_ConfigValue_StateM_ReconnectDelay;

      /* update StateMachineStatus */
      Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_Error_WaitingForRetry);
    }
    /* no retries are left */
    else
    {
      /* reset the delay counter */
      Scc_StateM_Counter.GeneralDelayCnt = 0;

      /* update StateMachineStatus */
      Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_Error_Stopped);
    }

  }
  else
#endif /* SCC_SCHEMA_DIN */
  {
    /* #50 check if the module was not already stopped (new error will be ignored otherwise) */
    if ( Scc_SMS_Stopped > Scc_StateM_State )
    {
      /* #60 report the error to the application */
      Scc_Set_StateM_StateMachineError(ErrorReason);

      /* Pablo Bol�s Workaround start:
       * Don't reset the SCC state machine to be able to close the TCP from the app
       */

      /* #70 reset the core state machine */
      //Scc_StateM_TxCore.MsgTrig = Scc_MsgTrig_None;

      /*Workaround end*/

      /* wait before starting with the next retry */
      Scc_StateM_State = Scc_SMS_Stopped;
      /* #80 reset the msg state */
#if ( SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW == STD_ON )
      Scc_StateM_MsgStateFct(Scc_SMMS_WaitingForApplication)
#else /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */
      Scc_StateM_MsgStateFct(Scc_SMMS_PreparingRequest)
#endif /* SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW */

        /* #90 if there are still retries left and the error was not caused by the NvM */
        if ( ( 0u < Scc_StateM_Counter.ReconnectRetryCnt )
          && ( Scc_StackError_NvM != Scc_StateM_RxCore.StackError ) )
        {
          /* decrement the retries */
          Scc_StateM_Counter.ReconnectRetryCnt--;
          /* start the delay timer for the reconnect */
          Scc_StateM_Counter.GeneralDelayCnt = (uint16)Scc_ConfigValue_StateM_ReconnectDelay;

          /* update StateMachineStatus */
          Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_Error_WaitingForRetry);
        }
      /* no retries are left */
        else
        {
          /* reset the delay counter */
          Scc_StateM_Counter.GeneralDelayCnt = 0;

          /* update StateMachineStatus */
          Scc_Set_StateM_StateMachineStatus(Scc_StateMachineStatus_Error_Stopped);
        }
    }
  }
}


#define SCC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
/* PRQA L:NEST_STRUCTS */
/* PRQA L:RETURN_PATHS */

#endif /* SCC_ENABLE_STATE_MACHINE */

/**********************************************************************************************************************
 *  END OF FILE: Scc_StateM_Vector.c
 *********************************************************************************************************************/
