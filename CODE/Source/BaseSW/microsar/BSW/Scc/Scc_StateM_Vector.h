/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2020 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Scc_StateM_Vector.h
 *        \brief  Smart Charging Communication Header File
 *
 *      \details  Implements Vehicle 2 Grid communication according to the specifications ISO/IEC 15118-2,
 *                DIN SPEC 70121 and customer specific schemas.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the header file of the Scc module. >> Scc.h
 *********************************************************************************************************************/
#if !defined (SCC_STATEM_H)
# define SCC_STATEM_H

/**********************************************************************************************************************
   LOCAL MISRA / PCLINT JUSTIFICATION
**********************************************************************************************************************/
/* PRQA S 0777 EOF */ /* MD_MSR_Rule5.1 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Scc_Cfg.h"

#if ( SCC_ENABLE_STATE_MACHINE == STD_ON )

#include "Scc_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Scc_StateM_InitMemory
 *********************************************************************************************************************/
/*! \brief          Initializes global variables
 *  \details        Initializes component variables in *_INIT_* sections at power up.
 *  \pre            Module is uninitialized.
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \note           Is called in the Scc_InitMemory()
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_StateM_InitMemory(void);

/**********************************************************************************************************************
 *  Scc_Init
 *********************************************************************************************************************/
/*! \brief          Initialization function
 *  \details        This function initializes module global variables at power up. This function initializes the
 *                  variables in *_INIT_* sections. Used in case they are not initialized by the startup code
 *  \pre            has to be called before useage of the module
 *  \pre            Interrupts are disabled.
 *  \pre            Module is uninitialized.
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \note           Is called in the Scc_Init()
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_StateM_Init(void);

/**********************************************************************************************************************
 *  Scc_MainFunction
 *********************************************************************************************************************/
/*! \brief          main function of the state machine, has to be called cyclically
 *  \details        handles FunctionControl from the application, trigger states and timeout handling
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \note           Is called in the Scc_MainFunction()
 *********************************************************************************************************************/
FUNC(void, SCC_CODE) Scc_StateM_MainFunction(void);


/**********************************************************************************************************************
 *  Scc_StateM_Get_<StateMachineData>
 *********************************************************************************************************************/
/*! \brief          Scc <- Scc_StateMachine
 *  \details        Scc Core gets the variable from the StateMachine
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
/* message independent */
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_Core_MsgTrig(P2VAR(Scc_MsgTrigType, AUTOMATIC, SCC_VAR_NOINIT) MsgTrig);
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_Core_CyclicMsgTrigRx(P2VAR(boolean, AUTOMATIC, SCC_VAR_NOINIT) CyclicMsgTrig);


/* multiple messages */
#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_Ed2_DIS_EVProcessing(P2VAR(Exi_ISO_ED2_DIS_processingType, AUTOMATIC, SCC_VAR_NOINIT) EvProcessing);
#endif /* SCC_SCHEMA_ISO_ED2 */

#if ( SCC_ENABLE_SLAC_HANDLING == STD_ON )
/* SLAC */
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_SLAC_QCAIdleTimer(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) QCAIdleTimer);
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_SLAC_StartMode(P2VAR(EthTrcv_30_Ar7000_Slac_StartModeType, AUTOMATIC, SCC_VAR_NOINIT) SLACStartMode);
#endif /* SCC_ENABLE_SLAC_HANDLING */

/* TLS-Handshake */
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_RTE_CODE) Scc_StateM_Set_Core_TLS_CertChain(P2CONST(uint8, AUTOMATIC, SCC_RTE_DATA) certChainPtr, P2VAR(uint8, AUTOMATIC, SCC_RTE_DATA) validationResultPtr);

/* SECC Discovery Protocol */
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_Core_SDPSecurityRx(P2VAR(Scc_SDPSecurityType, AUTOMATIC, SCC_VAR_NOINIT) SDPSecurity);

/* SupportedAppProtocolReq */
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_Core_ForceSAPSchema(P2VAR(Scc_ForceSAPSchemasType, AUTOMATIC, SCC_VAR_NOINIT) ForceSAPSchema);

/* ServiceDiscovery */
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_ServiceScope(P2VAR(Exi_ISO_serviceScopeType, AUTOMATIC, SCC_VAR_NOINIT) ServiceScope,
  P2VAR(boolean, AUTOMATIC, SCC_VAR_NOINIT) Flag);
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_ServiceCategory(P2VAR(Exi_ISO_serviceCategoryType, AUTOMATIC, SCC_VAR_NOINIT) ServiceCategory,
  P2VAR(boolean, AUTOMATIC, SCC_VAR_NOINIT) Flag);
#endif /* SCC_SCHEMA_ISO */
#if ( ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) )
FUNC(void, SCC_CODE) Scc_StateM_Get_DIN_ServiceCategory(P2VAR(Exi_DIN_serviceCategoryType, AUTOMATIC, SCC_VAR_NOINIT) ServiceCategory,
   P2VAR(boolean, AUTOMATIC, SCC_VAR_NOINIT) Flag);
#endif /* SCC_SCHEMA_DIN */

/* ServiceDetail */
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_ServiceIDRx(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) ServiceID);
#endif /* SCC_SCHEMA_ISO */

#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_Ed2_DIS_ServiceID(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) ServiceID);
#endif /* SCC_SCHEMA_ISO_ED2 */

/* Payment Service Selection */
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_SelectedPaymentOption(P2VAR(Exi_ISO_paymentOptionType, AUTOMATIC, SCC_VAR_NOINIT) SelectedPaymentOption);
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_SelectedServiceListPtr(P2VAR(Exi_ISO_SelectedServiceListType*, AUTOMATIC, SCC_VAR_NOINIT) SelectedServiceListPtr);
#endif /* SCC_SCHEMA_ISO */

#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_Ed2_DIS_SelectedIdentificationOption(P2VAR(Exi_ISO_ED2_DIS_identificationOptionType, AUTOMATIC, SCC_VAR_NOINIT) SelectedPaymentOption);
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_Ed2_DIS_SelectedEnergyTransferService(P2VAR(Exi_ISO_ED2_DIS_SelectedServiceType, AUTOMATIC, SCC_VAR_NOINIT) SelectedEnergyTransferService);
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_Ed2_DIS_SelectedVASListPtr(P2VAR(Exi_ISO_ED2_DIS_SelectedServiceListType, AUTOMATIC, SCC_VAR_NOINIT) SelectedVASListPtr, P2VAR(boolean, AUTOMATIC, SCC_VAR_NOINIT) SelectedVASListFlag);
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_Ed2_DIS_SelectedVasIdFlags(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) vasIdFlags);
#endif /* SCC_SCHEMA_ISO_ED2 */

/* Charge Parameter Discovery */
#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_RequestedEnergyTransferMode(P2VAR(Exi_ISO_EnergyTransferModeType, AUTOMATIC, SCC_VAR_NOINIT) EnergyTransferMode);
#endif /* SCC_SCHEMA_ISO */
#if ( ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) )
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_DIN_RequestedEnergyTransferMode(P2VAR(Exi_DIN_EVRequestedEnergyTransferType, AUTOMATIC, SCC_VAR_NOINIT) EnergyTransferMode);
#endif /* SCC_SCHEMA_DIN */

/* Power Delivery */
#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_ChargeProgress(P2VAR(Exi_ISO_chargeProgressType, AUTOMATIC, SCC_VAR_NOINIT) ChargeProgress);
#endif /* SCC_SCHEMA_ISO */
#if ( ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) )
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_DIN_ReadyToChargeState(P2VAR(boolean, AUTOMATIC, SCC_VAR_NOINIT) ReadyToChargeState);
#endif /* SCC_SCHEMA_DIN */
#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
/* see pattern Scc_StateM_Get_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Get_ISO_Ed2_DIS_ChargeProgress(P2VAR(Exi_ISO_ED2_DIS_chargeProgressType, AUTOMATIC, SCC_VAR_NOINIT) ChargeProgress);
#endif /* SCC_SCHEMA_ISO_ED2 */


/**********************************************************************************************************************
 *  Scc_StateM_Set_<StateMachineData>
 *********************************************************************************************************************/
/*! \brief          Scc -> Scc_StateMachine
 *  \details        Scc Core sets the variable in the StateMachine
 *  \pre            -
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *********************************************************************************************************************/
/* message independent */
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_TrcvLinkState(boolean LinkStateActive);
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_IPAssigned(boolean IPAssigned);
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_CyclicMsgTrigTx(boolean CyclicMsgTrig);
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_CyclicMsgRcvd(boolean CyclicMsgRcvd);
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_MsgStatus(Scc_MsgStatusType MsgStatus);
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_StackError(Scc_StackErrorType StackError);

/* multiple messages */
#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_EVSEProcessing(Exi_ISO_EVSEProcessingType EVSEProcessing);
#endif /* SCC_SCHEMA_ISO */
#if ( ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) )
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_DIN_EVSEProcessing(Exi_DIN_EVSEProcessingType EVSEProcessing);
#endif /* SCC_SCHEMA_DIN */
#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_Ed2_DIS_EVSEProcessing(Exi_ISO_ED2_DIS_processingType EVSEProcessing);
#endif /* SCC_SCHEMA_ISO_ED2 */


/* SECC Discovery Protocol */
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_SDPSecurityTx(Scc_SDPSecurityType SDPSecurity);

/* Supported App Protocol */
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_Core_SAPSchemaID(Scc_SAPSchemaIDType SAPSchemaID);

/* Service Discovery */
#if ( ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 ) )
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_ChargeService(P2CONST(Exi_ISO_ChargeServiceType, AUTOMATIC, SCC_VAR_NOINIT) ChargeService);
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_PaymentOptionList(P2CONST(Exi_ISO_PaymentOptionListType, AUTOMATIC, SCC_VAR_NOINIT) PaymentOptionList);
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_ServiceList(P2CONST(Exi_ISO_ServiceListType, AUTOMATIC, SCC_VAR_NOINIT) ServiceList, Exi_BitType Flag);
#endif /* SCC_SCHEMA_ISO */
#if ( ( defined SCC_SCHEMA_DIN ) && ( SCC_SCHEMA_DIN != 0 ) )
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_DIN_ChargeService(P2CONST(Exi_DIN_ServiceChargeType, AUTOMATIC, SCC_VAR_NOINIT) ChargeService);
#endif /* SCC_SCHEMA_DIN */
#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_Ed2_DIS_EnergyTransferServiceList(P2CONST(Exi_ISO_ED2_DIS_ServiceListType, AUTOMATIC, SCC_VAR_NOINIT) EnergyTransferServiceList);
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_Ed2_DIS_VASList(P2CONST(Exi_ISO_ED2_DIS_ServiceListType, AUTOMATIC, SCC_VAR_NOINIT) VASList, Exi_BitType Flag);
#endif /* SCC_SCHEMA_ISO_ED2 */

/* ServiceDetail */
#if ( defined SCC_SCHEMA_ISO ) && ( SCC_SCHEMA_ISO != 0 )
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_ServiceIDTx(uint16 ServiceID);
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_ServiceParameterList(
  P2CONST(Exi_ISO_ServiceParameterListType, AUTOMATIC, SCC_VAR_NOINIT) ServiceParameterList, Exi_BitType ServiceParameterListFlag);
#endif /* SCC_SCHEMA_ISO */
#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_Ed2_DIS_ServiceID(uint16 ServiceID);
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_Ed2_DIS_ServiceParameterList(P2CONST(Exi_ISO_ED2_DIS_ServiceParameterListType, AUTOMATIC, SCC_VAR_NOINIT) ServiceParameterList, Exi_BitType ServiceParameterListFlag);
#endif /* SCC_SCHEMA_ISO_ED2 */

/* IdentificationServiceSelection */
#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_Ed2_DIS_IdentificationOptionList(P2CONST(Exi_ISO_ED2_DIS_IdentificationOptionListType, AUTOMATIC, SCC_VAR_NOINIT) IdentificationOptionList);
#endif /* SCC_SCHEMA_ISO_ED2 */


/* ChargingStatus */
#if ( SCC_ENABLE_PNC_CHARGING != 0 )
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_PnC_ReceiptRequired(boolean ReceiptRequired, Exi_BitType Flag);
#endif /* SCC_ENABLE_PNC_CHARGING */

/* PowerDemand / ChargingStatus / CurrentDemand */
#if ( ( defined SCC_SCHEMA_ISO_ED2 ) && ( SCC_SCHEMA_ISO_ED2 != 0 ) )
/* see pattern Scc_StateM_Set_<StateMachineData> */
FUNC(void, SCC_CODE) Scc_StateM_Set_ISO_Ed2_DIS_PnC_ReceiptRequired(boolean ReceiptRequired, Exi_BitType Flag);
#endif /* SCC_SCHEMA_ISO_ED2 */

#define SCC_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* SCC_ENABLE_STATE_MACHINE */

#endif /* SCC_STATEM_H */
/**********************************************************************************************************************
 *  END OF FILE: Scc_StateM_Vector.h
 *********************************************************************************************************************/
