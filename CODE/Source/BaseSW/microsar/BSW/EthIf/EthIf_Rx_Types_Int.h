/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/*!        \file  EthIf_Rx_Types_Int.h
 *        \brief  EthIf Rx internal types header
 *
 *      \details  Provides access to the internal types of the sub-module Rx of EthIf.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/

#if !defined (ETHIF_RX_TYPES_INT_H)
# define ETHIF_RX_TYPES_INT_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/*********************************************************************************************************
 * Predefined file includes based on \trace DSGN-EthIfDiag1975
 *********************************************************************************************************/
# include "EthIf_Rx_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
/*! Exchange data to call respective upper layer callouts during reception of a frame */
typedef struct
{
  P2VAR(uint8, TYPEDEF, ETHIF_APPL_VAR)  SrcMacPtr;
  P2VAR(uint8, TYPEDEF, ETHIF_APPL_VAR)  PayloadPtr;
        EthIf_EthIfCtrlIterType          EthIfCtrlIdx;
        Eth_FrameType                    EtherType;
        uint16                           PayloadLen;
        boolean                          IsBroadcast;
} EthIf_UlRxIndiactionDataType;

#endif /* ETHIF_RX_TYPES_INT_H */

/**********************************************************************************************************************
 *  END OF FILE: EthIf_Rx_Types_Int.h
 *********************************************************************************************************************/

