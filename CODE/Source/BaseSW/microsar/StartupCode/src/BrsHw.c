
/**********************************************************************************************************************
  COPYRIGHT
-----------------------------------------------------------------------------------------------------------------------
  \par      copyright
  \verbatim
  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.

                This software is copyright protected and proprietary to Vector Informatik GmbH.
                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
                All other rights remain with Vector Informatik GmbH.
  \endverbatim
-----------------------------------------------------------------------------------------------------------------------
  FILE DESCRIPTION
-----------------------------------------------------------------------------------------------------------------------
  \file  File:  BrsHw.c
      Project:  Vector Basic Runtime System
       Module:  BrsHw for platform Infineon Aurix
     Template:  This file is reviewed according to zBrs_Template@root[2.02.02]

  \brief Description:  This is the hardware specific code file for Vector Basic Runtime System (BRS).
                       This file supports: see BrsHw_DerivativeList.h

  \attention Please note:
    The demo and example programs only show special aspects of the software. With regard to the fact
    that these programs are meant for demonstration purposes only, Vector Informatik liability shall be
    expressly excluded in cases of ordinary negligence, to the extent admissible by law or statute.
**********************************************************************************************************************/

/**********************************************************************************************************************
  AUTHOR IDENTITY
 ----------------------------------------------------------------------------------------------------------------------
  Name                          Initials      Company
  ----------------------------  ------------  -------------------------------------------------------------------------
  Benjamin Walter               visbwa        Vector Informatik GmbH
  Timo M�ller                   vistir        Vector Informatik GmbH
  Abir Bazzi                    vadaba        Vector Informatik GmbH
  Daniel Kuhnle                 viskdl        Vector Informatik GmbH
  Jens Haerer                   visjhr        Vector Informatik GmbH
  Volker Kaiser                 viskvr        Vector Informatik GmbH
  Sascha Mauser                 vismaa        Vector Informatik GmbH
  Bastian Molkenthin            visbmo        Vector Informatik GmbH
  Steffen Frank                 visfsn        Vector Informatik GmbH
  Hakan Capci                   vishci        Vector Informatik GmbH
-----------------------------------------------------------------------------------------------------------------------
  REVISION HISTORY
 ----------------------------------------------------------------------------------------------------------------------
  Version   Date        Author  Description
  --------  ----------  ------  ---------------------------------------------------------------------------------------
  01.00.00  2014-09-04  visbwa  First Version (derived from zBrs_Tricore 2.01.24)
  01.00.00  2014-10-30  vistir  Added new BrsHwPllInitPowerOn()
                                Added PortInitPowerOn() for CAN2 and CAN3
                                Introduced BRS_DERIVATIVE_TC21X and BRS_DERIVATIVE_TC22X
                                Removed PLL_ERAY support for TC21x and TC22x (no PLL_ERAY available for them)
  01.00.00  2014-11-25  visbwa  Final review for first version; adopted Eth support
  01.00.01  2014-11-25  visbwa  fixed encapsulation of BrsHwFrLine0Interrupt()
  01.00.02  2014-12-02  visbwa  fixed wrong BrsHwRestoreInterrupt() implementation
  01.00.03  2015-02-13  vadaba  Introduced BRS_OSC_CLK = 16 MHZ and separated BrsHwErayPllInitPowerOn() from
                                BrsHwPllInitPowerOn()
  01.00.04  2015-02-23  vistir  Bugfix within BrsHwPllInitPowerOn(), rework of BrsHwErayPllInitPowerOn()
  01.00.05  2015-02-24  visbwa  Review
  01.00.06  2015-04-09  visbwa  Added support for DrvPort and DrvDio within LED-support
  01.00.07  2015-04-13  viskdl  Bugfix within BrsHwErayPllInitPowerOn() (PLLPWD)
  01.00.08  2015-04-30  visjhr  Added support for TriBoards: TC2x2, TC2x3, TC2x4, TC2x5, TC2x7
  01.00.09  2015-05-05  visbwa  Enhanced SafeCtx-support of function ApplCanWaitSetMCR()
  01.00.10  2015-05-06  visbwa  Review of TriBoard support
  01.00.11  2015-09-02  visvnn/vismin  Added support for TriBoard TC2x8
  01.00.12  2015-10-05  visvnn  Fixed PortPin settings for CAN0 and LIN0 of TriBoard TC2x8,
                                added support for Eray interrupt 1,
                                added SCR-Register settings for DrvLin (necessary for DrvLin_TricoreAsr@root>=7.00.00)
  01.00.13  2015-11-04  visbwa  Fixed SCR-Register settings for DrvLin
  01.00.14  2016-03-10  visbwa  Fixed TRIBOARD_TC2x7 Pin settings for Ethernet RMII interface
  01.00.15  2016-05-24  viskvr  Added support for TRIBOARD_TC2x9, fixed FR and ETH pins for TRIBOARD_TC2x8
  01.00.16  2016-05-24  visbwa  Introduced support for Os_CoreGen7
  01.00.17  2016-06-07  visbwa  Reworked usage of BRSHW_ENABLE_TIMER_INTERRUPT and BRS_OS_USECASE_BRS to allow
                                automatic filtering by SWCP/Organi
  01.00.18  2016-06-15  visbwa  Removed unused Timer-registers, re-organisiert LED alive-blinking functions,
                                removed static inline from FR-PllInit functions
  01.00.19  2016-06-20  visbwa  Readded _endinit() for Os_UseCase OS
  01.00.20  2016-06-23  visbwa  Changed BRS_IOS into BRSHW_IOS according to Brs_Template 2.00.01
  01.00.21  2016-09-06  visbwa  Added prototypes of several Appl functions, to prevent compiler warnings with Tasking 6
  01.00.22  2016-09-16  vishan  Added generic port access macros
                                Added port init helper functions
                                Added custom port toggle test functions
  01.00.23  2016-12-02  vismaa  Added HSR_365, outsourced BrsHwPortInitPowerOn (Pin Configuration) to BrsHw_Ports.h,
                                fixed GNU interrupt handling, added DrvEth ISR for GNU
  01.00.24  2016-12-15  visbwa  Encapsulated timer interrupt initialization for GNU compiler (not necessary with OS),
                                Review
  01.00.25  2016-12-16  vismaa  Added EthIsr for GNU
  01.00.26  2017-01-12  visbwa  Simplified ApplCanWaitSetMCR() implementation (replaced mydummy++ by nop())
  01.00.27  2017-01-12  visbwa  Added Appl_UnlockEndinit() and Appl_LockEndinit() (used by newer driver implementation)
                                and made it available also with DrvLin and DrvEth
  01.00.28  2017-02-28  visbwa  Encapsuled parts for ISR initialization with != OsGen7 (has to be done with OS-APIs),
                                replaced "error:" by "error" (proper C-syntax)
  01.00.29  2017-02-28  visbwa  Added ISR-handler for DrvLin and UseCase w/o OS
  01.01.00  2017-05-02  visbwa  Added TCM support, review according Brs_Template 2.01.00
  01.01.01  2017-05-10  visbwa  Fixed comments of some PORT registers
  01.01.02  2017-06-19  visbwa  Added hint for Silicon Revision dependant include folder for HighTec Ifx_reg.h,
                                encapsuled extern declaration of ISRs for HighTec compiler with != OsGen7
  01.01.03  2017-08-23  visbmo  Fixed Canbedded LIN support
  01.01.04  2017-11-21  visbwa  Introduced BRS_SCU_BASE to simplify SCU register usage
  01.01.05  2017-11-22  vismaa  Fixed OSCCON for OSC_CLK=16MHz, added support for OSC_CLK=40
  01.01.06  2018-03-01  visbwa  Fixed encapsulation of not always mandatory pins within BrsHwPortInitPowerOn()
  01.02.00  2018-02-01  visfsn  Added FBL Support
                                Changed Unlock in BrsHwSoftwareResetECU() to SafetyLockInit/UnlockInit
                        vismaa  Fix of the fix (PLLERAY)
  01.02.01  2018-06-18  vishci  Fixed BRSHW_INT_DECL for DiabData compiler
  01.03.00  2018-08-17  vishci  Reworked Port initialization functionality, removed PortTest fragments
  01.03.01  2018-09-11  visbwa  Fixed encapsulation of BrsHwLin ISRs and channel define wrapper (needed for Ports!)
  01.03.02  2018-09-11  visbwa  Review according to Brs_Template 2.02.02 (several fixes for FBL support)
  01.04.00  2018-10-30  vismaa  Replaced Ifx/sfr register usage by Brs-internal register definitions (no include of
                                compiler headers any more), transferred watchdog register defines into BrsHw.h
  02.00.00  2018-11-15  vismaa  Renamed BRS_COMP_GNU to BRS_COMP_HIGHTEC
  02.00.01  2019-01-08  vismaa  Added hint for bmiField changes
  02.00.02  2019-02-20  visrgm  Edited comment about spbdivider define to "fspb = 50MHZ"
**********************************************************************************************************************/

/**********************************************************************************************************************
*  EXAMPLE CODE ONLY
*  -------------------------------------------------------------------------------------------------------------------
*  This Example Code is only intended for illustrating an example of a possible BSW integration and BSW configuration.
*  The Example Code has not passed any quality control measures and may be incomplete. The Example Code is neither
*  intended nor qualified for use in series production. The Example Code as well as any of its modifications and/or
*  implementations must be tested with diligent care and must comply with all quality requirements which are necessary
*  according to the state of the art before their use.
*********************************************************************************************************************/

/**********************************************************************************************************************
  INCLUDES
**********************************************************************************************************************/
/*
 * Description: The BrsHw header provides all the necessary interfaces to
 *              the microcontroller hardware features like ports, timers, LEDs, ...
 */
#include "BrsHw.h"
#include "WdgM_Types.h"

#if defined (VGEN_ENABLE_CAN_DRV)
# if defined (VGEN_ENABLE_IF_ASRIFCAN)
  /*Autosar component*/
  #include "Can.h"
# else
  /*CANbedded component*/
  #include "can_inc.h"
# endif
#endif

#if defined (VGEN_ENABLE_LIN_DRV)
# if defined (VGEN_ENABLE_IF_ASRIFLIN)
  /*Autosar component*/
  #include "Lin.h"
# else
  /*CANbedded component*/
  #include "lin_api.h"
  #include "sio_par.h"
# endif

/**********************************************************************************************************************
  WRAPPER FOR DIFFERENT DRVLIN VARIANTS
**********************************************************************************************************************/
/* Autosar - CANbedded compatibility */
# if defined (LIN_ENABLE_HARDWARE_INTERFACE_0)
  #define SIO_ENABLE_SIO_UART0
# endif
# if defined (LIN_ENABLE_HARDWARE_INTERFACE_1)
  #define SIO_ENABLE_SIO_UART1
# endif
# if defined (LIN_ENABLE_HARDWARE_INTERFACE_2)
  #define SIO_ENABLE_SIO_UART2
# endif
# if defined (LIN_ENABLE_HARDWARE_INTERFACE_3)
  #define SIO_ENABLE_SIO_UART3
# endif
#endif /*VGEN_ENABLE_LIN_DRV*/

#if defined (VGEN_ENABLE_DRVFR__BASEASR)
  #include "Fr.h"
# if !defined (FR_CHANNEL_A_USED) && !defined (FR_CHANNEL_B_USED)
  #error "Could not detect DrvFr channel configuration automatically. Please define here manually, which channel is used within your configuration."*/
  /*#define FR_CHANNEL_A_USED STD_ON*/
  /*#define FR_CHANNEL_B_USED STD_ON*/
# endif
#endif

#if defined (VGEN_ENABLE_DRVETH__BASEASR)
  #include "Eth.h"
#endif

#include "watchdog.h"


#if !defined (VGEN_ENABLE_DRVPORT)
  #include "BrsHw_Ports.h"
#endif

#if defined (VGEN_ENABLE_DRVDIO)
# if defined (BRS_ENABLE_SUPPORT_LEDS)          || \
     defined (BRS_ENABLE_SUPPORT_TOGGLE_WD_PIN) || \
     defined (BRS_ENABLE_SUPPORT_TOGGLE_CUSTOM_PIN)
  #include "Dio.h"
#  if defined (VGEN_ENABLE_DRVPORT)
  #define BRSHW_PORT_LOGIC_HIGH STD_HIGH
  #define BRSHW_PORT_LOGIC_LOW STD_LOW
#  else
  #error "LED alive mechanism and MCAL does only work proper if DrvPort and DrvDio are used together!"
#  endif
# endif /*BRS_ENABLE_SUPPORT_LEDS||BRS_ENABLE_SUPPORT_TOGGLE_WD_PIN||BRS_ENABLE_SUPPORT_TOGGLE_CUSTOM_PIN*/
#else
# if defined (VGEN_ENABLE_DRVPORT)
#  if defined (BRS_ENABLE_SUPPORT_LEDS)          || \
      defined (BRS_ENABLE_SUPPORT_TOGGLE_WD_PIN) || \
      defined (BRS_ENABLE_SUPPORT_TOGGLE_CUSTOM_PIN)
  #error "LED alive mechanism and MCAL does only work proper if DrvPort and DrvDio are used together!"
#  endif
# endif /*VGEN_ENABLE_DRVPORT*/
#endif /*VGEN_ENABLE_DRVDIO*/

/**********************************************************************************************************************
  VERSION CHECK
**********************************************************************************************************************/
#if (BRSHW_VERSION != 0x0200u)
  #error "Header and source file are inconsistent!"
#endif
#if (BRSHW_BUGFIX_VERSION != 0x02u)
  #error "Different versions of bugfix in Header and Source used!"
#endif

/**********************************************************************************************************************
  CONFIGURATION CHECK
**********************************************************************************************************************/
#if defined (BRS_COMP_HIGHTEC) 

#else
  #error "Unknown compiler specified!"
#endif

/**********************************************************************************************************************
  DEFINITION + MACROS
**********************************************************************************************************************/


#if !defined (BRS_OS_USECASE_OSGEN7)
# if defined (Lin_PISEL_0)
  #define kSioPISEL_0  Lin_PISEL_0
# endif
# if defined (Lin_PISEL_1)
  #define kSioPISEL_1  Lin_PISEL_1
# endif
# if defined (Lin_PISEL_2)
  #define kSioPISEL_2  Lin_PISEL_2
# endif
# if defined (Lin_PISEL_3)
  #define kSioPISEL_3  Lin_PISEL_3
# endif

# if !defined (Lin_InterruptPriority_HWI0)
  #define Lin_InterruptPriority_HWI0 BRSHW_LIN0PRIO
# endif
# if !defined (Lin_InterruptPriority_0)
  #define Lin_InterruptPriority_0 Lin_InterruptPriority_HWI0
# endif
# if !defined (Lin_InterruptPriority_HWI1)
  #define Lin_InterruptPriority_HWI1 BRSHW_LIN1PRIO
# endif
# if !defined (Lin_InterruptPriority_1)
  #define Lin_InterruptPriority_1 Lin_InterruptPriority_HWI1
# endif
# if !defined (Lin_InterruptPriority_HWI2)
  #define Lin_InterruptPriority_HWI2 BRSHW_LIN2PRIO
# endif
# if !defined (Lin_InterruptPriority_2)
  #define Lin_InterruptPriority_2 Lin_InterruptPriority_HWI2
# endif
# if !defined (Lin_InterruptPriority_HWI3)
  #define Lin_InterruptPriority_HWI3 BRSHW_LIN3PRIO
# endif
# if !defined (Lin_InterruptPriority_3)
  #define Lin_InterruptPriority_3 Lin_InterruptPriority_HWI3
# endif
#endif /*BRS_OS_USECASE_OSGEN7*/

/**********************************************************************************************************************
  GLOBAL VARIABLES
**********************************************************************************************************************/

/**********************************************************************************************************************
  GLOBAL CONST VARIABLES
**********************************************************************************************************************/
/*
 * Description: These constants are used to propagate the Versions over
 *              module boundaries.The version numbers are BCD coded. E.g. a sub
 *              version of 23 is coded with 0x23, a bug fix version of 9 is
 *              coded 0x09.
 */
uint8 const kBrsHwMainVersion   = (uint8)(BRSHW_VERSION >> 8);
uint8 const kBrsHwSubVersion    = (uint8)(BRSHW_VERSION & 0xFF);
uint8 const kBrsHwBugfixVersion = (uint8)(BRSHW_BUGFIX_VERSION);

/* Boot Mode Headers - use boot from internal flash      */
/* At least 1 of these headers has to be present         */
/* TODO: Check if this configuration suits your setup.   */

/*+++++++++++++++++++++++++++++++++++++Attention!++++++++++++++++++++++++++++++++++++++++++++++++++++++
++ If you have to adjust the bmiField you have to change the bmi header value in the startupcode too. +
++ Otherwise you could be locked out of your device!                                                  +
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
volatile const uint32 bmiField0[0x08] =
{
   0x00000000ul, 0xB3590170ul, 0x00000000ul, 0x00000000ul,
   0x00000000ul, 0x00000000ul, 0x964C0E85ul, 0x69B3F17Aul
};
volatile const uint32 bmiField1[0x08] =
{
   0x00000000ul, 0xB3590170ul, 0x00000000ul, 0x00000000ul,
   0x00000000ul, 0x00000000ul, 0x964C0E85ul, 0x69B3F17Aul
};
volatile const uint32 bmiField2[0x08] =
{
   0x00000000ul, 0xB3590170ul, 0x00000000ul, 0x00000000ul,
   0x00000000ul, 0x00000000ul, 0x964C0E85ul, 0x69B3F17Aul
};
volatile const uint32 bmiField3[0x08] =
{
   0x00000000ul, 0xB3590170ul, 0x00000000ul, 0x00000000ul,
   0x00000000ul, 0x00000000ul, 0x964C0E85ul, 0x69B3F17Aul
};

/**********************************************************************************************************************
  LOCAL VARIABLES AND LOCAL HW REGISTERS
**********************************************************************************************************************/
/*
 * Description: This counter is used to count the number of nested calls to
 *              disable and restore the state of the global INT enable bit.
 *              Please note: This variable is used only in this file.
 * Values     : 0 if no BrsHwDisableInterrupt is called and INT is allowed by the
 *              BRS. Value > 0 if INT is locked by the BRS interrupt control API.
 */
static uint8 bBrsHwIntDiCounter;

/*
 * Description: Store value of ICR register on DI command
 * See BrsHwDisableInterrupt() and BrsHwDisableInterrupt()
 */
uint16 bBrsHwICR;

/**********************************************************************************************************************
  CONTROLLER CONFIGURATION REGISTERS
**********************************************************************************************************************/

/* Necessary for _endinit(), called by e.g. StartUpCode */
#define BRS_GPT_CLC        BRSHW_IOS(uint32, 0xF0002E00) /* GPT_CLC */

/*******************************************************************************
  PLL  GROUP  CONFIG
*******************************************************************************/
#define BRS_PLLCLC         BRSHW_IOS(uint32, 0xF0000040) /* original name: PLL_CLC */
#define BRS_VCO_BYPASS     0x00000020
#define BRS_OSC_DISC       0x01000000
#define BRS_PLL_LOCK       0x00000001
#define BRS_PLL_LOCK_RES   0x00000002
#define BRS_CLOCK_MASK     0x007FFFC4

/*******************************************************************************
  PORT  GROUP  CONFIG
*******************************************************************************/
#define PORT_BASE_ADDRESS 0xF003A000

#define BRS_OFFSET_IOCR0 0x10 /* Port n Input/Output Control Register 0 */
#define BRS_OFFSET_POUT  0x00 /* Port n Output Register */
#define BRS_OFFSET_PIN   0x24 /* Port n Input Register */
#define BRS_OFFSET_PDR0  0x40 /* Port n Pad Driver Mode 0 Register */
#define BRS_OFFSET_PCSR  0x64 /* Port n Pin Controller Select Register */

#define BRS_PORT_IOCR(g,n) BRSHW_IOS(uint32, PORT_BASE_ADDRESS + 0x1000 * (g / 10) + 0x100 * (g % 10) + BRS_OFFSET_IOCR0 + 4 * ((uint32)n / 4) ) /* Port Input/Output Control Register (group, number) */
#define BRS_PORT_POUT(g)   BRSHW_IOS(uint32, PORT_BASE_ADDRESS + 0x1000 * (g / 10) + 0x100 * (g % 10) + BRS_OFFSET_POUT)                         /* Port Output Register (group) */
#define BRS_PORT_PIN(g)    BRSHW_IOS(uint32, PORT_BASE_ADDRESS + 0x1000 * (g / 10) + 0x100 * (g % 10) + BRS_OFFSET_PIN)                          /* Port Input Register (group) */
#define BRS_PORT_PDR(g,n)  BRSHW_IOS(uint32, PORT_BASE_ADDRESS + 0x1000 * (g / 10) + 0x100 * (g % 10) + BRS_OFFSET_PDR0 + 4 * ((uint32)n / 8) )  /* Port Pad Driver Mode Register (group, number) */
#define BRS_PORT_PCSR(g)   BRSHW_IOS(uint32, PORT_BASE_ADDRESS + 0x1000 * (g / 10) + 0x100 * (g % 10) + BRS_OFFSET_PCSR)                         /* Port g Pin Controller Select Register */

#if !defined (BRS_OS_USECASE_OSGEN7)
/*******************************************************************************
  INTERRUPT  GROUP  CONFIG
*******************************************************************************/
/*Base Address*/
#define BRS_SRC_BASE 0xF0038000
/*Flexray / ERAY*/
#define BRS_SRC_ERAYINT0   BRSHW_IOS(uint32, BRS_SRC_BASE + 0xBE0) /* ERAYINT0 */
#define BRS_SRC_ERAYINT1   BRSHW_IOS(uint32, BRS_SRC_BASE + 0xBE4) /* ERAYINT1 */
#define BRS_SRC_ERAYTINT0  BRSHW_IOS(uint32, BRS_SRC_BASE + 0xBE8) /* ERAYINTT0 */
#define BRS_SRC_ERAYTINT1  BRSHW_IOS(uint32, BRS_SRC_BASE + 0xBEC) /* ERAYINTT1 */

/*Ethernet*/
#define BRS_SRC_ETH        BRSHW_IOS(uint32, BRS_SRC_BASE + 0x8F0) /* ETH INT */

/*LIN*/
/*RX*/
#define BRS_SRC_LIN0RX     BRSHW_IOS(uint32, BRS_SRC_BASE + 0x84) /* LIN0RX */
#define BRS_SRC_LIN1RX     BRSHW_IOS(uint32, BRS_SRC_BASE + 0x90) /* LIN1RX */
#define BRS_SRC_LIN2RX     BRSHW_IOS(uint32, BRS_SRC_BASE + 0x9C) /* LIN2RX */
#define BRS_SRC_LIN3RX     BRSHW_IOS(uint32, BRS_SRC_BASE + 0xA8) /* LIN3RX */
/*TX*/
#define BRS_SRC_LIN0TX     BRSHW_IOS(uint32, BRS_SRC_BASE + 0x80) /* LIN0TX */
#define BRS_SRC_LIN1TX     BRSHW_IOS(uint32, BRS_SRC_BASE + 0x8C) /* LIN1TX */
#define BRS_SRC_LIN2TX     BRSHW_IOS(uint32, BRS_SRC_BASE + 0x98) /* LIN2TX */
#define BRS_SRC_LIN3TX     BRSHW_IOS(uint32, BRS_SRC_BASE + 0xA4) /* LIN3TX */
/*EX*/
#define BRS_SRC_LIN0EX     BRSHW_IOS(uint32, BRS_SRC_BASE + 0x88) /* LIN0EX */
#define BRS_SRC_LIN1EX     BRSHW_IOS(uint32, BRS_SRC_BASE + 0x94) /* LIN1EX */
#define BRS_SRC_LIN2EX     BRSHW_IOS(uint32, BRS_SRC_BASE + 0xA0) /* LIN2EX */
#define BRS_SRC_LIN3EX     BRSHW_IOS(uint32, BRS_SRC_BASE + 0xAC) /* LIN3EX */
#endif /*!BRS_OS_USECASE_OSGEN7*/

/*******************************************************************************
  SYSTEM  CONTROL  UNIT  SCU
*******************************************************************************/
#define BRS_SCU_BASE 0xF0036000

#define BRS_SCU_OSCCON       BRSHW_IOS(uint32, BRS_SCU_BASE + 0x10)  /* OSCCON  OSC Control Register */
#define BRS_SCU_PLLCON0      BRSHW_IOS(uint32, BRS_SCU_BASE + 0x18)  /* PLLCON0 PLL Configuration 0 Register */
#define BRS_SCU_PLLCON1      BRSHW_IOS(uint32, BRS_SCU_BASE + 0x1C)  /* PLLCON1 PLL Configuration 1 Register */
#define BRS_SCU_PLLCON2      BRSHW_IOS(uint32, BRS_SCU_BASE + 0x20)  /* PLLCON2 PLL Configuration 2 Register */
#define BRS_SCU_PLLSTAT      BRSHW_IOS(uint32, BRS_SCU_BASE + 0x14)  /* PLLSTAT PLL Status Register */

#define BRS_SCU_CCUCON0      BRSHW_IOS(uint32, BRS_SCU_BASE + 0x30)  /* CCUCON0 CCU Control Register 0 */
#define BRS_SCU_CCUCON1      BRSHW_IOS(uint32, BRS_SCU_BASE + 0x34)  /* CCUCON1 CCU Control Register 1 */
#define BRS_SCU_CCUCON5      BRSHW_IOS(uint32, BRS_SCU_BASE + 0x4C)  /* CCUCON5 CCU Control Register 5 */
#define BRS_SCU_CCUCON6      BRSHW_IOS(uint32, BRS_SCU_BASE + 0x80)  /* CCUCON6 CCU Control Register 6 */
#define BRS_SCU_CCUCON7      BRSHW_IOS(uint32, BRS_SCU_BASE + 0x84)  /* CCUCON7 CCU Control Register 7 */
#define BRS_SCU_CCUCON8      BRSHW_IOS(uint32, BRS_SCU_BASE + 0x88)  /* CCUCON8 CCU Control Register 8 */

#define BRS_SCU_PLLERAYCON0  BRSHW_IOS(uint32, BRS_SCU_BASE + 0x28)  /* PLL_ERAY Configuration 0 Register */
#define BRS_SCU_PLLERAYCON1  BRSHW_IOS(uint32, BRS_SCU_BASE + 0x2C)  /* PLL_ERAY Configuration 1 Register */
#define BRS_SCU_PLLERAYSTAT  BRSHW_IOS(uint32, BRS_SCU_BASE + 0x24)  /* PLL_ERAY Status Register */

#define BRS_SWRSTCON         BRSHW_IOS(uint32, BRS_SCU_BASE + 0x60)  /*Software Reset Configuration Register */
#define BRS_SCU_TRAPCLR      BRSHW_IOS(uint32, BRS_SCU_BASE + 0x12C) /* Trap Clear Register */
#define BRS_SCU_TRAPDIS      BRSHW_IOS(uint32, BRS_SCU_BASE + 0x130) /* Trap Disable Register */

/*******************************************************************************
| Local defines
|******************************************************************************/


#if defined (BRS_COMP_HIGHTEC)
  #define __nop()             nop()
  #define nop()               __asm__ ("nop")     /*NOP operation*/
  #define enable_interrupt()  __asm__ ("enable")  /*Enable global interrupt operation*/
  #define disable_interrupt() __asm__ ("disable") /*Disable global interrupt operation*/
  #define isync()             __asm__ ("isync")   /*Synchronize instruction stream (pipeline?)*/
  #define __interrupt(x)      void
  /*Read from (normally protected) CPU register*/
  #define mfcr(regaddr)       ({ int res; __asm__ ("mfcr %0,"#regaddr : "=d" (res)); res; })
  /*Write to (normally protected) CPU register*/
  #define mtcr(regaddr,val)   __asm__ ("mtcr "#regaddr",%0" : : "d" (val))

  #include "cint.h"
  #include "intrinsics.h"

# if !defined (BRS_OS_USECASE_OSGEN7)
#  if defined (VGEN_ENABLE_CAN_DRV)
#   if defined (C_ENABLE_TX_POLLING)  && \
       defined (C_ENABLE_RX_POLLING)  && \
       defined (C_ENABLE_ERROR_POLLING)
  /* All polling modes enabled => don't use interrupts (Wakeup not supported by CAN-HW => no WU polling) */
#   else
#    if defined (kCanPhysToLogChannelIndex_0)
  extern void CanIsr_0(void);
#    endif
#    if defined (kCanPhysToLogChannelIndex_1)
  extern void CanIsr_1(void);
#    endif
#    if defined (kCanPhysToLogChannelIndex_2)
  extern void CanIsr_2(void);
#    endif
#    if defined (kCanPhysToLogChannelIndex_3)
  extern void CanIsr_3(void);
#    endif
#    if defined (kCanPhysToLogChannelIndex_4)
  extern void CanIsr_4(void);
#    endif
#    if defined (kCanPhysToLogChannelIndex_5)
  extern void CanIsr_5(void);
#    endif
#    if defined (kCanPhysToLogChannelIndex_6)
  extern void CanIsr_6(void);
#    endif
#    if defined (kCanPhysToLogChannelIndex_7)
  extern void CanIsr_7(void);
#    endif
#   endif /*else Polling*/
#  endif /*VGEN_ENABLE_CAN_DRV*/

#  if defined (VGEN_ENABLE_LIN_DRV)
#   if defined (SIO_ENABLE_SIO_UART0)
  extern void BrsHwLin0Interrupt(void);
#   endif 
#   if defined (SIO_ENABLE_SIO_UART1)
  extern void BrsHwLin1Interrupt(void);
#   endif
#   if defined (SIO_ENABLE_SIO_UART2)
  extern void BrsHwLin2Interrupt(void);
#   endif
#   if defined (SIO_ENABLE_SIO_UART3)
  extern void BrsHwLin3Interrupt(void);
#   endif
#  endif /*VGEN_ENABLE_LIN_DRV*/

#  if defined (VGEN_ENABLE_DRVFR__BASEASR)
  extern void BrsHwFrLine0Interrupt(void);
  extern void BrsHwFrTimer0Interrupt(void);
#  endif

#  if defined (VGEN_ENABLE_DRVETH__BASEASR)
  extern void EthIsr_0 (void);
#  endif
# endif /*!BRS_OS_USECASE_OSGEN7*/
#endif /*BRS_COMP_HIGHTEC*/

/*******************************************************************************
* Global variables
*******************************************************************************/
#if defined (BRS_COMP_HIGHTEC)
/* provide the abs function for PLL calculation for the HIGHTEC compiler */
uint32 __abs(sint32 val)
{
  if(val < 0)
  {
    return val * -1;
  }
  else
  {
    return val;
  }
}
#endif /*BRS_COMP_x*/

/**********************************************************************************************************************
  LOCAL VARIABLES
**********************************************************************************************************************/

/**********************************************************************************************************************
  LOCAL CONST VARIABLES
**********************************************************************************************************************/

/**********************************************************************************************************************
  PROTOTYPES OF LOCAL FUNCTIONS
**********************************************************************************************************************/
void BrsHwSafetyUnlockInit(void);
void BrsHwSafetyLockInit(void);
void BrsHwUnlockInit(void);
void BrsHwLockInit(void);

#if defined (VGEN_ENABLE_CAN_DRV)        || \
    defined (VGEN_ENABLE_LIN_DRV)        || \
    defined (VGEN_ENABLE_DRVFR__BASEASR) || \
    defined (VGEN_ENABLE_DRVETH__BASEASR)
void Appl_UnlockInit(void);
void Appl_UnlockEndinit(void);
void Appl_LockInit(void);
void Appl_LockEndinit(void);
#endif
#if defined (VGEN_ENABLE_CAN_DRV)
void ApplCanWaitSetMCR(void);
#endif

#if defined (BRSHW_PLL_ERAY_AVAILABLE)
Std_ReturnType BrsHwErayPllInitPowerOn(void);
Std_ReturnType BrsHwErayPllWaitVcoLock(void);
#endif

/**********************************************************************************************************************
  FUNCTION DEFINITIONS
**********************************************************************************************************************/

/*****************************************************************************/
/**
 * @brief      This function has to be used to initialize the Watchdog.
 * @pre        -
 * @param[in]  -
 * @param[out] -
 * @return     -
 * @context    Function is called from main@BrsMain or EcuM at power on initialization
 */
/*****************************************************************************/
void BrsHwWatchdogInitPowerOn(void)
{
#if !defined (VGEN_ENABLE_DRVWD)
  BrsHwUnlockInit();
  BRS_SFR_WDTCPU0CON1 |= 0x8; /* WDTCPU0CON1.DR=1 (Request to disable the WDT) */
  BrsHwLockInit();
#endif /*!VGEN_ENABLE_DRVWD*/
}

/*****************************************************************************/
/**
 * @brief      This function has to be used to initialize the PLL.
 * @pre        -
 * @param[in]  -
 * @param[out] -
 * @return     -
 * @context    Function is called from main@BrsMain or EcuM at power on initialization
 */
/*****************************************************************************/
void BrsHwPllInitPowerOn(void)
{
#if !defined (VGEN_ENABLE_DRVMCU)
  uint32 pllfreq;
  /*CPU DIVIDER VALUES*/
  uint32 cpu0div, cpu1div, cpu2div;
  /*PERIPHERAL DIVIDER VALUES*/
  uint32 stmdivider, spbdivider, candivider, linfdivider, linsdivider, baud1divider, baud2divider, sridivider, gtmdivider, ethdivider, eraydivider;

# if (BRS_TIMEBASE_CLOCK > BRSHW_CPU_MAX_FREQUENCY)
  #error "The selected derivative does not support frequencies above BRSHW_CPU_MAX_FREQUENCY (please find the defined value inside BrsHw_DerivativeList.h)."
# endif

  /* fvco has to be in the range 400...800MHZ */
  /* fpll = fosc*(N_DIVIDER/(P_DIVIDER*K2_DIVIDER)) */

  /*Divider values for fpll and fpllERAY*/
  #define P_DIVIDER 0x1  /*P_DIVIDER = 2*/
  #define K2_DIVIDER 0x3 /*K2_DIVIDER = 4*/

  /*fpll = 200MHZ fix*/
# if (BRS_OSC_CLK == 40)
  #define N_DIVIDER 0x27 /*N_DIVIDER = 40*/

# elif (BRS_OSC_CLK == 20)
  #define N_DIVIDER 0x4F /*N_DIVIDER = 80*/

# elif (BRS_OSC_CLK == 16)
  #define N_DIVIDER 0x63/*N_DIVIDER = 100*/

# else
  #error "Please check makefile.config ! Actual BRS implementation only supports 40, 20 or 16 MHz"
# endif /*BRS_OSC_CLK*/

  /*check vco range*/
# if( (BRS_OSC_CLK*(N_DIVIDER+0x1))/(P_DIVIDER+0x1) < 400  ||  (BRS_OSC_CLK*(N_DIVIDER+0x1))/(P_DIVIDER+0x1) > 800)
  #error "fvco must be greater than 400MHZ and less than 800MHZ."
# endif

  pllfreq= BRS_OSC_CLK *(N_DIVIDER+0x1)/((K2_DIVIDER+0x1)*(P_DIVIDER+0x1)); /*pllfreq = 200MHZ*/

# if (BRS_TIMEBASE_CLOCK == 50 || BRS_TIMEBASE_CLOCK == 75 || BRS_TIMEBASE_CLOCK == 100|| BRS_TIMEBASE_CLOCK == 125|| BRS_TIMEBASE_CLOCK == 150|| BRS_TIMEBASE_CLOCK == 175 || BRS_TIMEBASE_CLOCK == 200)
    cpu0div = 64 - ((BRS_TIMEBASE_CLOCK*64)/pllfreq);
    cpu1div = 64 - ((BRS_TIMEBASE_CLOCK*64)/pllfreq);
    cpu2div = 64 - ((BRS_TIMEBASE_CLOCK*64)/pllfreq);
# else
  #error "The selected CPU frequency is not yet supported (please choose 25, 50, 75, 100, 125, 150, 175 or 200MHZ)."
# endif

  /*Set peripheral, system timer and SPB bus clock divider values*/
  stmdivider = pllfreq/25;   /*stmdivider for fstm = 25MHZ;*/
  spbdivider = pllfreq/50;   /*spbdivider for fspb = 50MHZ*/
  candivider = pllfreq/25;   /*candivider for fcan = 25MHZ*/
  linfdivider = pllfreq/25;  /*linfdivider for flinf = 25MHZ*/
  linsdivider = pllfreq/25;  /*linsdivider for flins = 25MHZ*/
  baud1divider = pllfreq/25; /*baud1divider for fbaud1 = 25MHZ*/
  baud2divider = pllfreq/25; /*baud2divider for fbaud2 = 25MHZ*/
  sridivider = 0x1;          /*sridivider for fsri = BRS_TIMEBASE_CLOCK*/
  gtmdivider = pllfreq/25;   /*gtmdivider for fgtm = 25MHZ*/
# if defined (BRSHW_PLL_ERAY_AVAILABLE)
  eraydivider = 0x1;         /*eraydivider for feray = 80MHZ*/
# else
  eraydivider = 0x0;         /* no ERAYPLL */
# endif

# if defined (VGEN_ENABLE_DRVETH__BASEASR)
  ethdivider = 0x1;
# else
  ethdivider = 0x0;
# endif

  /*Configuration of PLL and PLLERAY starts here*/
  SCU_vResetENDINIT (0);
  BRS_SCU_TRAPDIS |= 0x3E0; /* Traps can not be generated for PLLs and Clocksystem */
  SCU_vSetENDINIT (0);

  SCU_vResetENDINIT (-1);
  /*Frist: Select prescaler mode*/
  /*Prescaler mode for pll*/
  BRS_SCU_PLLCON0 = BRS_SCU_PLLCON0
                  | (0x1 << 0)  /*VCOBYP=1    ---> Prescaler mode*/
                  | (0x1 << 4); /*SETFINDIS=1 ---> oscillator clock disconnect to PLL*/

  while((BRS_SCU_CCUCON1 & 0x80000000) == 0x80000000); /* wait until the lock of CCUCON1 is not set */
  BRS_SCU_CCUCON1 = (BRS_SCU_CCUCON1 & 0x0FFFFFFF)
                  | (0x1 << 28); /* pll and pll_ERAY clock source = fosc0 (fxtal1) */

  /* Write CCUCON1 with all values */ 
  while((BRS_SCU_CCUCON1 & 0x80000000) == 0x80000000); /* wait until the lock of CCUCON1 is not set */
  BRS_SCU_CCUCON1 =  (BRS_SCU_CCUCON1 & 0x30000000)
                  |  (candivider  << 0)   /*fcan = fpll/candivider*/
                  |  (eraydivider << 4)   /*fERAY = fplleray/eraydivider*/
                  |  (stmdivider  << 8)   /*fstm = fpll/stmdivider*/ /*fstm = system timer clock*/
                  |  (gtmdivider  << 12)  /*fgtm = fpll/gtmdivider*/
                  |  (ethdivider  << 16)  /*feth*/
                  |  (linfdivider << 20)  /*fasclinf = fpll/linfdivider*/
                  |  (linsdivider << 24); /*fasclins = fpll/linsdivider*/

  /*Write CCUCON0 with all values*/
  while((BRS_SCU_CCUCON0 & 0x80000000) == 0x80000000); /* wait until the lock of CCUCON0 is not set */
  BRS_SCU_CCUCON0 = (BRS_SCU_CCUCON0 & 0x0FFFFFFF)
                  | (0x1 << 28); /* Choose fpll as clock source, fpll=fsource */

  while((BRS_SCU_CCUCON0 & 0x80000000) == 0x80000000); /* wait until the lock of CCUCON0 is not set */
  BRS_SCU_CCUCON0 = (BRS_SCU_CCUCON0 & 0x30000000)
                  | (baud1divider << 0)   /*fbaud1 = fpll/baud1divider*/ /*Not available for TC22X and TC23X*/
                  | (baud2divider << 4)   /*fbaud2 = fpll/baud2divider*/
                  | (sridivider   << 8)   /*fsri = fpll/sridivider*/
                  | (spbdivider   << 16)  /*fspb = fpll/spbdivider*/ /*fspb = general purpose timer clock*/
                  | (0x2          << 20)  /*ffsi2= fsri/2*/
                  | (0x2          << 24)  /*ffsi =fsri/2*/
                  | (0x1          << 30); /*update CCUCON0, CCUCON1*/

  while((BRS_SCU_CCUCON5 & 0x80000000) == 0x80000000); /* wait until the lock of CCUCON5 is not set */
  BRS_SCU_CCUCON5 = (0x40000040); /* fmax = fsource and update CCUCON5 */

  BRS_SCU_CCUCON6 = cpu0div; /*fcpu0 = fsri*/
  BRS_SCU_CCUCON7 = cpu1div; /*fcpu1 = fsri*/
  BRS_SCU_CCUCON8 = cpu2div; /*fcpu2 = fsri*/

# if (BRS_OSC_CLK == 40)
  BRS_SCU_OSCCON = 0x00000000   /*clear reg.*/
                 | (0x2 << 3)   /*gain control from 4MHZ to 20MHZ*/
                 | (0xF << 16); /*crystalfreq(40MHZ) / 15+1 = 2,5MHZ ---> OSCVAL = 40MHZ/2.5MHZ*/

# elif (BRS_OSC_CLK == 20)
  BRS_SCU_OSCCON = 0x00000000   /*clear reg.*/
                 | (0x2 << 3)   /*gain control from 4MHZ to 20MHZ*/
                 | (0x7 << 16); /*crystalfreq(20MHZ) / 7+1 = 2,5MHZ ---> OSCVAL = 20MHZ/2.5MHZ*/

# elif (BRS_OSC_CLK == 16)
  BRS_SCU_OSCCON = 0x00000000   /*clear reg.*/
                 | (0x2 << 3)   /*gain control from 4MHZ to 20MHZ*/
                 | (0x5 << 16); /*crystalfreq(16MHZ) / 5+1 = 2,67MHZ ---> OSCVAL = 16MHZ/2.5MHZ*/
# endif /*BRS_OSC_CLK*/

  /* Set n and p dividers for pll */
  BRS_SCU_PLLCON0 = (BRS_SCU_PLLCON0 & 0xF0FF01FF)
                  | (N_DIVIDER << 9)    /* Set N-Divider */
                  | (P_DIVIDER  << 24); /* Set P-Divider */

  /* Set k2 divider for pll */
  BRS_SCU_PLLCON1  = (BRS_SCU_PLLCON1 & 0xFFFFFFC0)
                   | K2_DIVIDER; /* K2-Divider */

  while((BRS_SCU_PLLSTAT & 0x00000020) != 0x00000020); /* K2RDY == 1 ? */

  BRS_SCU_PLLCON0 |= 0x00000040; /* In case of a PLL loss-of-lock bit PLLSTAT.FINDIS is cleared */

  while((BRS_SCU_OSCCON & 0x00000102) != 0x00000102); /* check if the OSC frequency is usable */

  /* Second: Select normal mode for pll and for pllERAY */
  BRS_SCU_PLLCON0 |= 0x00000020; /* CLRFINDIS=1 ---> oscillator clock is connected to pll */
  BRS_SCU_PLLCON0 |= 0x00040000; /* RESLD = 1   ---> restart lock detection */
  SCU_vSetENDINIT (-1);

  /* wait for lock */
  while((BRS_SCU_PLLSTAT & 0x00000004) != 0x00000004); /* VCOLOCK == 1 ?*/

  SCU_vResetENDINIT (-1);
  BRS_SCU_PLLCON0 &= 0xFFFFFFFE; /* VCOBYP=0 ---> select normal mode */

# if defined (BRSHW_PLL_ERAY_AVAILABLE)
  /*Configuration of PLLERAY starts here*/
  BrsHwErayPllInitPowerOn();
# endif

  SCU_vSetENDINIT (-1);

  /* clear only pending traps related to PLLs and Clocksystem */
  BRS_SCU_TRAPCLR = 0x3E0;
  SCU_vResetENDINIT(0);

  /*enable all Traps again*/
  BRS_SCU_TRAPDIS = BRS_SCU_TRAPDIS & ~0x3E0;
  SCU_vSetENDINIT(0);

#endif /*!VGEN_ENABLE_DRVMCU*/
}

#if defined (BRSHW_PLL_ERAY_AVAILABLE)
/* function to initialize ERAY PLL */
Std_ReturnType BrsHwErayPllInitPowerOn(void)
{
  Std_ReturnType ErrorFlag;
  volatile uint32 PllStableDelay;
  uint8 PllNdiv;
  uint8 PllK2div;
  uint8 PllK3div;
  uint8 PllPdiv;

  /*Store PLL Divider values in local variables*/
# if (BRS_OSC_CLK == 40)
  PllNdiv = 0x7; /* 8 - 1*/
  PllK2div = 0x3; /* 4 - 1*/
  PllK3div = 0x3; /* 4 - 1*/
  PllPdiv =  0x0; /* 1 - 1*/

# elif (BRS_OSC_CLK == 20)
  PllNdiv = 0xF; /* 16 - 1*/
  PllK2div = 0x3; /* 4 - 1*/
  PllK3div = 0x3; /* 4 - 1*/
  PllPdiv =  0x0; /* 1 - 1*/

# elif (BRS_OSC_CLK == 16)
  PllNdiv = 0x13; /* 20 - 1*/
  PllK2div = 0x3; /* 4 - 1*/
  PllK3div = 0x3; /* 4 - 1*/
  PllPdiv =  0x0; /* 1 - 1*/
# endif /*BRS_OSC_CLK*/

  SCU_vResetENDINIT (-1);

  /* Enter Prescalar mode */
  /* Update K and N dividers */
  BRS_SCU_PLLERAYCON1 = (BRS_SCU_PLLERAYCON1 & 0xFF80FFFF); /* Clear K1DIV. K1DIV = 1 */

  BRS_SCU_PLLERAYCON0 = (BRS_SCU_PLLERAYCON0 & 0xFFFFFFEE)
                      | (0x1 << 0)    /* VCOBYP = 1 */
                      | (0x1 << 4);   /* SETFINDIS = 1 */

  BRS_SCU_PLLERAYCON1 = (BRS_SCU_PLLERAYCON1 & 0xFFFFF080)
                      | (PllK2div << 0)   /* K2DIV */
                      | (PllK3div << 8);  /* K3DIV */

  BRS_SCU_PLLERAYCON0 = (BRS_SCU_PLLERAYCON0 & 0xF0FFC1FF)
                      | (PllNdiv << 9)     /* NDIV */
                      | (PllPdiv << 24);   /* PDIV */

  /* Enter normal mode */
  BRS_SCU_PLLERAYCON0 = (BRS_SCU_PLLERAYCON0 & 0xFFFEFF9F)
                      | (0x1 << 5)     /* CLRFINDIS=1 */
                      | (0x1 << 6)     /* OSCDISCDIS=1 */
                      | (0x0 << 16);   /* PLLPWD=0 */

  /*
    RESLD = 1     ==> Restart VCO lock detection
    CLRFINDIS = 1 ==> Connect OSC to PLL
    PLLPWD = 1    ==> PLL Power Saving Mode : Normal behaviour
    NDIV = FR_17_ERAY_PLL_NDIV (Pre-compile parameter)
  */

  BRS_SCU_PLLERAYCON0 = (BRS_SCU_PLLERAYCON0 & 0xFFFEFFFF)
                      | (0x1 << 16);   /*PLLPWD = 1*/

  for(PllStableDelay=0U;PllStableDelay < 100 ;PllStableDelay++)
  {}/*Errata*/

  BRS_SCU_PLLERAYCON0 = (BRS_SCU_PLLERAYCON0 & 0xFFFBFFFF)
              | (0x1 << 18);   /* RESLD = 1 */

  /* Set the ENDINIT bit in the WDT_CON0 register again
  to enable the write-protection and to prevent a time-out */
  SCU_vSetENDINIT (-1);

  /* Wait for ERAY PLL VCO locking */
  ErrorFlag = BrsHwErayPllWaitVcoLock();

  /*By Pass VCO only if PLL is locked*/
  if(ErrorFlag == E_OK)
  {
    /* Clear the ENDINIT bit in the WDT_CON0 register in order
    to disable the write-protection for registers protected
    via the EndInit feature */
    SCU_vResetENDINIT (-1);

    /*Bypass VCO*/
    /* Distribute the clock */
    BRS_SCU_PLLERAYCON0 = (BRS_SCU_PLLERAYCON0 & 0xFFFFFFBE)
                        | (0x0 << 0)     /*VCOBYP = 0*/
                        | (0x0 << 6);    /*OSCDISCDIS = 0*/

    /* Set the ENDINIT bit in the WDT_CON0 register again
    to enable the write-protection and to prevent a time-out */
    SCU_vSetENDINIT (-1);
  }
  return(ErrorFlag);
}

Std_ReturnType BrsHwErayPllWaitVcoLock(void)
{
  Std_ReturnType ErrorFlag;
  uint32 TimeOutCount;

  ErrorFlag = E_OK;
  TimeOutCount = ((uint32)0x000001FFU);
  do
  {
    TimeOutCount-- ;
  } while (((BRS_SCU_PLLERAYSTAT & 0x00000004) != 0x00000004) && (TimeOutCount > 0U)) ;

  if (TimeOutCount == 0U)
  {
    ErrorFlag = E_NOT_OK;
  }
return(ErrorFlag);
}
#endif /*BRSHW_PLL_ERAY_AVAILABLE*/

#if !defined (VGEN_ENABLE_DRVPORT)
/*******************************************************************************
  Port Pin initialization helper functions for usage of BrsHw_Ports.h
********************************************************************************/
/*****************************************************************************/
/**
 * @brief      This function configures a port pin as input pin.
 * @pre        Port pin configuartions available within BrsHw_Ports.h and
 *             no DrvPort used for port pin initialization.
 * @param[in]  p - brsHw_Port_PortType, to be initialized.
 * @param[out] -
 * @return     -
 * @context    Function is called from BrsHwPortInitPowerOn() and
 *             BrsHwEvaBoardInitPowerOn() locally.
 */
/*****************************************************************************/
void BrsHwInitPortInput(brsHw_Port_PortType p)
{
  BRS_PORT_IOCR(p.portGroup,p.portNumber) &= ~(uint32)( 0x1F << (3 + 8 * ((uint32)p.portNumber % 4)));
  BRS_PORT_IOCR(p.portGroup,p.portNumber) |= (uint32)( 0xF ) << (3 + 8 * ((uint32)p.portNumber % 4));
}

/*****************************************************************************/
/**
 * @brief      This function configures a port pin as output pin.
 * @pre        Port pin configuartions available within BrsHw_Ports.h and
 *             no DrvPort used for port pin initialization.
 * @param[in]  p - brsHw_Port_PortType, to be initialized.
 * @param[out] -
 * @return     -
 * @context    Function is called from BrsHwPortInitPowerOn() and
 *             BrsHwEvaBoardInitPowerOn() locally.
 */
/*****************************************************************************/
void BrsHwInitPortOutput(brsHw_Port_PortType p)
{
  BRS_PORT_IOCR(p.portGroup,p.portNumber) &= ~(uint32)( 0x1F << (3 + 8 * ((uint32)p.portNumber % 4)) );
  BRS_PORT_IOCR(p.portGroup,p.portNumber) |= (uint32)( 0x10 + p.portAlternative ) << (3 + 8 * ((uint32)p.portNumber % 4));
}

/*****************************************************************************/
/**
 * @brief      This function configures the port pin configuration registers
 *             (set DriverStrength and pad level at automotive level)
 * @pre        Port pin configuartions available within BrsHw_Ports.h and
 *             no DrvPort used for port pin initialization.
 * @param[in]  p - brsHw_Port_PortType, to be initialized,
               n - brsHw_Port_ConfType, for port pin configuration
 * @param[out] -
 * @return     -
 * @context    Function is called from BrsHwPortInitPowerOn() and
 *             BrsHwEvaBoardInitPowerOn() locally.
 */
/*****************************************************************************/
void BrsHwInitPortConfig(brsHw_Port_PortType p, brsHw_Port_ConfType n)
{
  BrsHwUnlockInit();
  BRS_PORT_PDR(p.portGroup,p.portNumber)  &= ~(uint32)( 0x0F << (4 * ((uint32)p.portNumber % 8)) );
  switch (n.PortDriverSetting)
  {
    case PORT_STRONG_DRIVER_SHARP_EDGE:
      BRS_PORT_PDR(p.portGroup,p.portNumber)  |= (0x1 | PORT_STRONG_DRIVER_SHARP_EDGE) << (4 * ((uint32)p.portNumber % 8));
      break;
    case PORT_STRONG_DRIVER_MEDIUM_EDGE:
      BRS_PORT_PDR(p.portGroup,p.portNumber)  |= (0x1 | PORT_STRONG_DRIVER_MEDIUM_EDGE) << (4 * ((uint32)p.portNumber % 8));
      break;
    case PORT_MEDIUM_DRIVER:
      BRS_PORT_PDR(p.portGroup,p.portNumber)  |= (0x1 | PORT_MEDIUM_DRIVER) << (4 * ((uint32)p.portNumber % 8));
      break;
    case PORT_RGMII_DRIVER:
      BRS_PORT_PDR(p.portGroup,p.portNumber)  |= (0x1 | PORT_RGMII_DRIVER) << (4 * ((uint32)p.portNumber % 8));
      break;
  }
  BrsHwLockInit();
}

/*****************************************************************************/
/**
 * @brief      This function sets the output level of a port pin.
 * @pre        Port pin configuartions available within BrsHw_Ports.h,
 *             no DrvPort used for port pin initialization and
 *             transferred port pin has to be initialized as output pin with
 *             GPIO functionality.
 * @param[in]  p     - brsHw_Port_PortType, to be set,
 *             Level - level, port pin has to be set to
 *                     (BRSHW_PORT_LOGIC_LOW or BRSHW_PORT_LOGIC_HIGH).
 * @param[out] -
 * @return     -
 * @context    Function is called from BrsHwPortInitPowerOn(),
 *             BrsHwEvaBoardInitPowerOn(), BrsHw_WriteDio_TCM_SDA_OUT() and
 *             BrsHw_WriteDio_TCM_CLK_OUT() locally.
 */
/*****************************************************************************/
void BrsHwPort_SetLevel(brsHw_Port_PortType p, uint8 Level)
{
  if (Level == BRSHW_PORT_LOGIC_LOW)
  {
    BRS_PORT_POUT(p.portGroup) &= ~(0x1 << p.portNumber);
  }
  else
  {
    BRS_PORT_POUT(p.portGroup) |=  (0x1 << p.portNumber);
  }
}

/*****************************************************************************/
/**
 * @brief      This function reads the input level of a port pin.
 * @pre        Port pin configuartions available within BrsHw_Ports.h,
 *             no DrvPort used for port pin initialization and
 *             transferred port pin has to be initialized as input pin with
 *             GPIO functionality.
 * @param[in]  p - brsHw_Port_PortType, to be read.
 * @param[out] -
 * @return     Level, read from port pin
 *             (BRSHW_PORT_LOGIC_LOW or BRSHW_PORT_LOGIC_HIGH).
 * @context    Function is called from BrsHw_WriteDio_TCM_SDA_OUT() and
 *             BrsHw_WriteDio_TCM_CLK_OUT() locally.
 */
/*****************************************************************************/
uint8 BrsHwPort_GetLevel(brsHw_Port_PortType p)
{
  uint32 temp;

  temp = BRS_PORT_PIN(p.portGroup);
  temp &= (1 << p.portNumber);

  if(temp !=0x0000)
  {
    return BRSHW_PORT_LOGIC_HIGH;
  }
  else
  {
    return BRSHW_PORT_LOGIC_LOW;
  }
}

# if defined (BRS_ENABLE_TCM_SUPPORT)
/*******************************************************************************
  TCM Write Functions
********************************************************************************/
void BrsHw_WriteDio_TCM_SDA_OUT(uint8 Level)
{
  BrsHwPort_SetLevel(BRSHW_PORT_TCM_SDA_OUT, Level);
}

void BrsHw_WriteDio_TCM_CLK_OUT(uint8 Level)
{
  BrsHwPort_SetLevel(BRSHW_PORT_TCM_CLK_OUT, Level);
}

/*******************************************************************************
  TCM Read Functions
********************************************************************************/
uint8 BrsHw_ReadDio_TCM_SDA_IN(void)
{
  return BrsHwPort_GetLevel(BRSHW_PORT_TCM_SDA_IN);
}

uint8 BrsHw_ReadDio_TCM_CLK_IN(void)
{
  return BrsHwPort_GetLevel(BRSHW_PORT_TCM_CLK_IN);
}
# endif /*BRS_ENABLE_TCM_SUPPORT*/
#endif /*!VGEN_ENABLE_DRVPORT*/

/*****************************************************************************/
/**
 * @brief      This function has to be used to initialize the used ports.
 * @pre        -
 * @param[in]  -
 * @param[out] -
 * @return     -
 * @context    Function is called from main@BrsMain or EcuM at power on initialization
 */
/*****************************************************************************/
void BrsHwPortInitPowerOn(void)
{
#if defined (VGEN_ENABLE_DRVPORT)
# if defined (BRS_ENABLE_SUPPORT_LEDS)          || \
     defined (BRS_ENABLE_SUPPORT_TOGGLE_WD_PIN) || \
     defined (BRS_ENABLE_SUPPORT_TOGGLE_CUSTOM_PIN)
  #error "Configure the valid toggle pins within your DrvPort config. Or disable the depending BRS mechanism."*/
# endif

#else
# if defined (BRS_ENABLE_SUPPORT_LEDS)
  BrsHwInitPortOutput(BRSHW_PORT_LED);

  /* Set LED on EVB demo board to show the system is alive */
  BrsHwTogglePin(BRSHW_TOGGLEPIN_LED);
# endif /*BRS_ENABLE_SUPPORT_LEDS*/

# if defined (BRS_ENABLE_SUPPORT_TOGGLE_WD_PIN)
  BrsHwInitPortOutput(BRSHW_PORT_TOGGLE_WD);
# endif

# if defined (BRS_ENABLE_SUPPORT_TOGGLE_CUSTOM_PIN)
  BrsHwInitPortOutput(BRSHW_PORT_TOGGLE_CUSTOM);
# endif

# if defined (BRS_ENABLE_TCM_SUPPORT)
  BrsHwInitPortOutput(BRSHW_PORT_TCM_CLK_OUT);
  BrsHwInitPortOutput(BRSHW_PORT_TCM_SDA_OUT);
  BrsHwInitPortInput(BRSHW_PORT_TCM_SDA_IN);
  BrsHwInitPortInput(BRSHW_PORT_TCM_CLK_IN);
#  if defined (BRSHW_USE_TCM_EXT_IRQ)
  BrsHwInitPortInput(BRSHW_PORT_TCM_INTA_IN);
  BrsHwInitPortInput(BRSHW_PORT_TCM_INTB_IN);
#  endif
# endif /*BRS_ENABLE_TCM_SUPPORT*/

/*******************************************************************************
  CAN driver
********************************************************************************/
# if defined (VGEN_ENABLE_CAN_DRV)
#  if defined (kCanPhysToLogChannelIndex_0)
  BrsHwInitPortOutput(BRSHW_PORT_CAN0_TX);
  BrsHwInitPortInput(BRSHW_PORT_CAN0_RX);
  BrsHwInitPortConfig(BRSHW_PORT_CAN0_TX, BRSHW_PORT_CONF_CAN);
  BrsHwInitPortConfig(BRSHW_PORT_CAN0_RX, BRSHW_PORT_CONF_CAN);
#   if defined (_BRSHW_PORT_CAN0_TRCV_STB)
  BrsHwInitPortOutput(BRSHW_PORT_CAN0_TRCV_STB);
  BrsHwPort_SetLevel(BRSHW_PORT_CAN0_TRCV_STB, BRSHW_PORT_LOGIC_HIGH);
#   endif
#   if defined (_BRSHW_PORT_CAN0_TRCV_EN)
  BrsHwInitPortOutput(BRSHW_PORT_CAN0_TRCV_EN);
  BrsHwPort_SetLevel(BRSHW_PORT_CAN0_TRCV_EN, BRSHW_PORT_LOGIC_HIGH);
#   endif
#  endif /*kCanPhysToLogChannelIndex_0*/

#  if defined (kCanPhysToLogChannelIndex_1)
  BrsHwInitPortOutput(BRSHW_PORT_CAN1_TX);
  BrsHwInitPortInput(BRSHW_PORT_CAN1_RX);
  BrsHwInitPortConfig(BRSHW_PORT_CAN1_TX, BRSHW_PORT_CONF_CAN);
  BrsHwInitPortConfig(BRSHW_PORT_CAN1_RX, BRSHW_PORT_CONF_CAN);
#   if defined (_BRSHW_PORT_CAN1_TRCV_STB)
  BrsHwInitPortOutput(BRSHW_PORT_CAN1_TRCV_STB);
  BrsHwPort_SetLevel(BRSHW_PORT_CAN1_TRCV_STB, BRSHW_PORT_LOGIC_HIGH);
#   endif
#   if defined (_BRSHW_PORT_CAN1_TRCV_EN)
  BrsHwInitPortOutput(BRSHW_PORT_CAN1_TRCV_EN);
  BrsHwPort_SetLevel(BRSHW_PORT_CAN1_TRCV_EN, BRSHW_PORT_LOGIC_HIGH);
#   endif
#  endif /*kCanPhysToLogChannelIndex_1*/

#  if defined (kCanPhysToLogChannelIndex_2)
  BrsHwInitPortOutput(BRSHW_PORT_CAN2_TX);
  BrsHwInitPortInput(BRSHW_PORT_CAN2_RX);
  BrsHwInitPortConfig(BRSHW_PORT_CAN2_TX, BRSHW_PORT_CONF_CAN);
  BrsHwInitPortConfig(BRSHW_PORT_CAN2_RX, BRSHW_PORT_CONF_CAN);
#   if defined (_BRSHW_PORT_CAN2_TRCV_STB)
  BrsHwInitPortOutput(BRSHW_PORT_CAN2_TRCV_STB);
  BrsHwPort_SetLevel(BRSHW_PORT_CAN2_TRCV_STB, BRSHW_PORT_LOGIC_HIGH);
#   endif
#   if defined (_BRSHW_PORT_CAN2_TRCV_EN)
  BrsHwInitPortOutput(BRSHW_PORT_CAN2_TRCV_EN);
  BrsHwPort_SetLevel(BRSHW_PORT_CAN2_TRCV_EN, BRSHW_PORT_LOGIC_HIGH);
#   endif
#  endif /*kCanPhysToLogChannelIndex_2*/

#  if defined (kCanPhysToLogChannelIndex_3)
  BrsHwInitPortOutput(BRSHW_PORT_CAN3_TX);
  BrsHwInitPortInput(BRSHW_PORT_CAN3_RX);
  BrsHwInitPortConfig(BRSHW_PORT_CAN3_TX, BRSHW_PORT_CONF_CAN);
  BrsHwInitPortConfig(BRSHW_PORT_CAN3_RX, BRSHW_PORT_CONF_CAN);
#   if defined (_BRSHW_PORT_CAN3_TRCV_STB)
  BrsHwInitPortOutput(BRSHW_PORT_CAN3_TRCV_STB);
  BrsHwPort_SetLevel(BRSHW_PORT_CAN3_TRCV_STB, BRSHW_PORT_LOGIC_HIGH);
#   endif
#   if defined (_BRSHW_PORT_CAN3_TRCV_EN)
  BrsHwInitPortOutput(BRSHW_PORT_CAN3_TRCV_EN);
  BrsHwPort_SetLevel(BRSHW_PORT_CAN3_TRCV_EN, BRSHW_PORT_LOGIC_HIGH);
#   endif
#  endif /*kCanPhysToLogChannelIndex_3*/
# endif /*VGEN_ENABLE_CAN_DRV*/

/*******************************************************************************
  LIN driver
********************************************************************************/
# if defined (VGEN_ENABLE_LIN_DRV)
#  if defined (SIO_ENABLE_SIO_UART0)
  BrsHwInitPortOutput(BRSHW_PORT_LIN0_TX);
  BrsHwInitPortInput(BRSHW_PORT_LIN0_RX);
#   if defined(_BRSHW_PORT_LIN0_TRCV_STB)
  BrsHwInitPortOutput(BRSHW_PORT_LIN0_TRCV_STB);
  BrsHwPort_SetLevel(BRSHW_PORT_LIN0_TRCV_STB, BRSHW_PORT_LOGIC_HIGH);
#   endif
#  endif /*SIO_ENABLE_SIO_UART0*/

#  if defined (SIO_ENABLE_SIO_UART1)
  BrsHwInitPortOutput(BRSHW_PORT_LIN1_TX);
  BrsHwInitPortInput(BRSHW_PORT_LIN1_RX);
#   if defined(_BRSHW_PORT_LIN1_TRCV_STB)
  BrsHwInitPortOutput(BRSHW_PORT_LIN1_TRCV_STB);
  BrsHwPort_SetLevel(BRSHW_PORT_LIN1_TRCV_STB, BRSHW_PORT_LOGIC_HIGH);
#   endif
#  endif /*SIO_ENABLE_SIO_UART1*/

#  if defined (SIO_ENABLE_SIO_UART2)
  BrsHwInitPortOutput(BRSHW_PORT_LIN2_TX);
  BrsHwInitPortInput(BRSHW_PORT_LIN2_RX);
#   if defined(_BRSHW_PORT_LIN2_TRCV_STB)
  BrsHwInitPortOutput(BRSHW_PORT_LIN2_TRCV_STB);
  BrsHwPort_SetLevel(BRSHW_PORT_LIN2_TRCV_STB, BRSHW_PORT_LOGIC_HIGH);
#   endif
#  endif /*SIO_ENABLE_SIO_UART2*/

#  if defined (SIO_ENABLE_SIO_UART3)
  BrsHwInitPortOutput(BRSHW_PORT_LIN3_TX);
  BrsHwInitPortInput(BRSHW_PORT_LIN3_RX);
#   if defined(_BRSHW_PORT_LIN3_TRCV_STB)
  BrsHwInitPortOutput(BRSHW_PORT_LIN3_TRCV_STB);
  BrsHwPort_SetLevel(BRSHW_PORT_LIN3_TRCV_STB, BRSHW_PORT_LOGIC_HIGH);
#   endif
#  endif /*SIO_ENABLE_SIO_UART3*/
# endif /*VGEN_ENABLE_LIN_DRV*/

/*******************************************************************************
  FLEXRAY driver
********************************************************************************/
# if defined (VGEN_ENABLE_DRVFR__BASEASR)
#  if (FR_CHANNEL_A_USED == STD_ON)
  BrsHwInitPortOutput(BRSHW_PORT_FR0A_TX);
  BrsHwInitPortInput(BRSHW_PORT_FR0A_RX);
  BrsHwInitPortOutput(BRSHW_PORT_FR0A_TXEN);
#   if defined(_BRSHW_PORT_FR0A_TRCV_STB)
  BrsHwInitPortOutput(BRSHW_PORT_FR0A_TRCV_STB);
  BrsHwPort_SetLevel(BRSHW_PORT_FR0A_TRCV_STB, BRSHW_PORT_LOGIC_HIGH);
#   endif
#   if defined (_BRSHW_PORT_FR0A_TRCV_EN)
  BrsHwInitPortOutput(BRSHW_PORT_FR0A_TRCV_EN);
  BrsHwPort_SetLevel(BRSHW_PORT_FR0A_TRCV_EN, BRSHW_PORT_LOGIC_HIGH);
#   endif
#  endif /*FR_CHANNEL_A_USED*/

#  if (FR_CHANNEL_B_USED == STD_ON)
  BrsHwInitPortOutput(BRSHW_PORT_FR0B_TX);
  BrsHwInitPortInput(BRSHW_PORT_FR0B_RX);
  BrsHwInitPortOutput(BRSHW_PORT_FR0B_TXEN);
#   if defined(_BRSHW_PORT_FR0B_TRCV_STB)
  BrsHwInitPortOutput(BRSHW_PORT_FR0B_TRCV_STB);
  BrsHwPort_SetLevel(BRSHW_PORT_FR0B_TRCV_STB, BRSHW_PORT_LOGIC_HIGH);
#   endif
#   if defined (_BRSHW_PORT_FR0B_TRCV_EN)
  BrsHwInitPortOutput(BRSHW_PORT_FR0B_TRCV_EN);
  BrsHwPort_SetLevel(BRSHW_PORT_FR0B_TRCV_EN, BRSHW_PORT_LOGIC_HIGH);
#   endif
#  endif /*FR_CHANNEL_B_USED*/
# endif /*VGEN_ENABLE_DRVFR__BASEASR*/

/*******************************************************************************
  ETHERNET driver
********************************************************************************/
# if defined (VGEN_ENABLE_DRVETH__BASEASR)
  /* -- Transmitter signals -- */
  BrsHwInitPortInput(BRSHW_PORT_ETH_REFCLK);
  BrsHwInitPortOutput(BRSHW_PORT_ETH_TXD0);
  BrsHwInitPortOutput(BRSHW_PORT_ETH_TXD1);
  BrsHwInitPortConfig(BRSHW_PORT_ETH_TXD0, BRSHW_PORT_CONF_ETHERNET);
  BrsHwInitPortConfig(BRSHW_PORT_ETH_TXD1, BRSHW_PORT_CONF_ETHERNET);
#  if defined (_BRSHW_PORT_ETH_TXD2)
  /* Transmit signal 2 is obsolete within RMII */
  BrsHwInitPortOutput(BRSHW_PORT_ETH_TXD2);
  BrsHwInitPortConfig(BRSHW_PORT_ETH_TXD2, BRSHW_PORT_CONF_ETHERNET);
#  endif
#  if defined (_BRSHW_PORT_ETH_TXD3)
  /* Transmit signal 3 is obsolete within RMII */
  BrsHwInitPortOutput(BRSHW_PORT_ETH_TXD3);
  BrsHwInitPortConfig(BRSHW_PORT_ETH_TXD3, BRSHW_PORT_CONF_ETHERNET);
#  endif
  BrsHwInitPortOutput(BRSHW_PORT_ETH_TXEN);
  BrsHwInitPortConfig(BRSHW_PORT_ETH_TXEN, BRSHW_PORT_CONF_ETHERNET);
#  if defined (_BRSHW_PORT_ETH_TXER)
  /* Transmit Error signal is only optional within MII and obsolete within RMII */
  BrsHwInitPortOutput(BRSHW_PORT_ETH_TXER);
  BrsHwInitPortConfig(BRSHW_PORT_ETH_TXER, BRSHW_PORT_CONF_ETHERNET);
#  endif

  /* -- Receiver signals -- */
#  if defined (_BRSHW_PORT_ETH_RXCLK)
  /* Receive clock signal is obsolete within RMII */
  BrsHwInitPortInput(BRSHW_PORT_ETH_RXCLK);
#  endif
  BrsHwInitPortInput( BRSHW_PORT_ETH_RXD0);
  BrsHwInitPortInput( BRSHW_PORT_ETH_RXD1);
  BrsHwInitPortConfig(BRSHW_PORT_ETH_RXD0, BRSHW_PORT_CONF_ETHERNET);
  BrsHwInitPortConfig(BRSHW_PORT_ETH_RXD1, BRSHW_PORT_CONF_ETHERNET);
#  if defined (_BRSHW_PORT_ETH_RXD2)
  /* Receive signal 2 is obsolete within RMII */
  BrsHwInitPortInput(BRSHW_PORT_ETH_RXD2);
  BrsHwInitPortConfig(BRSHW_PORT_ETH_RXD2, BRSHW_PORT_CONF_ETHERNET);
#  endif
#  if defined (_BRSHW_PORT_ETH_RXD3)
  /* Receive signal 3 is obsolete within RMII */
  BrsHwInitPortInput(BRSHW_PORT_ETH_RXD3);
  BrsHwInitPortConfig(BRSHW_PORT_ETH_RXD3, BRSHW_PORT_CONF_ETHERNET);
#  endif
  BrsHwInitPortInput( BRSHW_PORT_ETH_CRSDV);

  /* -- Management signals -- */
  BrsHwInitPortInput(BRSHW_PORT_ETH_MDIO);
  BrsHwInitPortOutput(BRSHW_PORT_ETH_MDIO);
  BrsHwInitPortConfig(BRSHW_PORT_ETH_MDIO, BRSHW_PORT_CONF_ETHERNET);
#  if defined (_BRSHW_PORT_ETH_MDC)
  /* Management data clock not always needed */
  BrsHwInitPortOutput(BRSHW_PORT_ETH_MDC);
  BrsHwInitPortConfig(BRSHW_PORT_ETH_MDC, BRSHW_PORT_CONF_ETHERNET);
#  endif

  BrsHwInitPortConfig(BRSHW_PORT_ETH_REFCLK, BRSHW_PORT_CONF_ETHERNET);
# endif /*VGEN_ENABLE_DRVETH__BASEASR*/

#endif /*!VGEN_ENABLE_DRVPORT*/
}

/* BrsHwTimeBaseInitPowerOn() and BrsHwTimeBaseInterrupt() removed by Organi, because of ALM attributes of project */

/*****************************************************************************/
/**
 * @brief      Disable the global system interrupt and initialize the INT
 *             lock handler variables.
 * @pre        Must be the first function call in main@BrsMain
 * @param[in]  -
 * @param[out] -
 * @return     -
 * @context    Function is called from main@BrsMain at power on initialization
 */
/*****************************************************************************/
void BrsHwDisableInterruptAtPowerOn(void)
{
  bBrsHwIntDiCounter = 0;

  disable_interrupt();
}

/*****************************************************************************/
/**
 * @brief      Enable the global system interrupt the first time
 * @pre        Must be called after all initializations are done
 * @param[in]  -
 * @param[out] -
 * @return     -
 * @context    Function is called from main@BrsMain at power on initialization
 */
/*****************************************************************************/
void BrsHwEnableInterruptAtPowerOn(void)
{
  /* Configure used interrupts */
#if !defined (BRS_OS_USECASE_OSGEN7) 
  BrsHwConfigureInterruptsAtPowerOn();
#else
  /* With OsGen7, OS-APIs have to be used for this */
  /*BrsHwConfigureInterruptsAtPowerOn();*/
#endif

  /* unmask interrupts */
  bBrsHwIntDiCounter = 0;
  enable_interrupt();
}

#if !defined (BRS_OS_USECASE_OSGEN7) 
/*****************************************************************************/
/**
 * @brief      This function has to be used to initialize the used interrupts.
 * @pre        -
 * @param[in]  -
 * @param[out] -
 * @return     -
 * @context    Function is called from BrsHwEnableInterruptAtPowerOn or EcuM-DriverInitThree
 */
/*****************************************************************************/
void BrsHwConfigureInterruptsAtPowerOn(void)
{
#if defined (BRS_COMP_HIGHTEC)
  _init_vectab();
  unsigned int CoreID = _mfcr(0xfe1C);


# if defined (VGEN_ENABLE_CAN_DRV)
#  if defined (kCanPhysToLogChannelIndex_0)
  _install_int_handler(kCanISRPrio_0, (void (*) (int)) CanIsr_0, 0);
#  endif
#  if defined (kCanPhysToLogChannelIndex_1)
  _install_int_handler(kCanISRPrio_1, (void (*) (int)) CanIsr_1, 0);
#  endif
#  if defined (kCanPhysToLogChannelIndex_2)
  _install_int_handler(kCanISRPrio_2, (void (*) (int)) CanIsr_2, 0);
#  endif
#  if defined (kCanPhysToLogChannelIndex_3)
  _install_int_handler(kCanISRPrio_3, (void (*) (int)) CanIsr_3, 0);
#  endif
#  if defined (kCanPhysToLogChannelIndex_4)
  _install_int_handler(kCanISRPrio_4, (void (*) (int)) CanIsr_4, 0);
#  endif
#  if defined (kCanPhysToLogChannelIndex_5)
  _install_int_handler(kCanISRPrio_5, (void (*) (int)) CanIsr_5, 0);
#  endif
#  if defined (kCanPhysToLogChannelIndex_6)
  _install_int_handler(kCanISRPrio_6, (void (*) (int)) CanIsr_6, 0);
#  endif
#  if defined (kCanPhysToLogChannelIndex_7)
  _install_int_handler(kCanISRPrio_7, (void (*) (int)) CanIsr_7, 0);
#  endif
# endif

# if defined (VGEN_ENABLE_LIN_DRV)
#  if defined (SIO_ENABLE_SIO_UART0)
  _install_int_handler(Lin_InterruptPriority_0, (void (*) (int)) BrsHwLin0Interrupt, 0);
#  endif
#  if defined (SIO_ENABLE_SIO_UART1)
  _install_int_handler(Lin_InterruptPriority_1, (void (*) (int)) BrsHwLin1Interrupt, 0);
#  endif
#  if defined (SIO_ENABLE_SIO_UART2)
  _install_int_handler(Lin_InterruptPriority_2, (void (*) (int)) BrsHwLin2Interrupt, 0);
#  endif
#  if defined (SIO_ENABLE_SIO_UART3)
  _install_int_handler(Lin_InterruptPriority_3, (void (*) (int)) BrsHwLin3Interrupt, 0);
#  endif
# endif /*VGEN_ENABLE_LIN_DRV*/

# if defined (VGEN_ENABLE_DRVFR__BASEASR)
  _install_int_handler(BRSHW_FRLINE0PRIO, (void (*) (int)) BrsHwFrLine0Interrupt , 0);
  _install_int_handler(BRSHW_FRTIME0PRIO, (void (*) (int)) BrsHwFrTimer0Interrupt, 0);
  mtcr (0xfe2c, 0x00000000);  /* 2 clocks per Arb.cycle , 4 Arb.cycles , IE off */
  isync();
# endif /*VGEN_ENABLE_DRVFR__BASEASR*/

# if defined (VGEN_ENABLE_DRVETH__BASEASR)
  _install_int_handler(BRSHW_ETHPRIO, (void (*) (int)) EthIsr_0 , 0);
# endif

#else /*BRS_COMP_HIGHTEC*/
# if defined (VGEN_ENABLE_DRVFR__BASEASR)
  mtcr (0xfe2c, 0x00000000);     /* 2 clocks per Arb.cycle , 4 Arb.cycles , IE off */
  isync();
# endif /*VGEN_ENABLE_DRVFR__BASEASR*/
#endif /*else BRS_COMP_HIGHTEC*/

#if defined (VGEN_ENABLE_LIN_DRV)
# if defined (SIO_ENABLE_SIO_UART0)
  BRS_SRC_LIN0RX = 0x00000400 | Lin_InterruptPriority_0;
  BRS_SRC_LIN0TX = 0x00000400 | Lin_InterruptPriority_0;
  BRS_SRC_LIN0EX = 0x00000400 | Lin_InterruptPriority_0;
# endif
# if defined (SIO_ENABLE_SIO_UART1)
  BRS_SRC_LIN1RX = 0x00000400 | Lin_InterruptPriority_1;
  BRS_SRC_LIN1TX = 0x00000400 | Lin_InterruptPriority_1;
  BRS_SRC_LIN1EX = 0x00000400 | Lin_InterruptPriority_1;
# endif
# if defined (SIO_ENABLE_SIO_UART2)
  BRS_SRC_LIN2RX = 0x00000400 | Lin_InterruptPriority_2;
  BRS_SRC_LIN2TX = 0x00000400 | Lin_InterruptPriority_2;
  BRS_SRC_LIN2EX = 0x00000400 | Lin_InterruptPriority_2;
# endif
# if defined (SIO_ENABLE_SIO_UART3)
  BRS_SRC_LIN3RX = 0x00000400 | Lin_InterruptPriority_3;
  BRS_SRC_LIN3TX = 0x00000400 | Lin_InterruptPriority_3;
  BRS_SRC_LIN3EX = 0x00000400 | Lin_InterruptPriority_3;
# endif
#endif /*VGEN_ENABLE_LIN_DRV*/

#if defined (VGEN_ENABLE_DRVFR__BASEASR)
  /* enable line0 and time0 interrupt,
            line1 and time1 interrupt */
  BRS_SRC_ERAYINT0  = 0x00000400 | BRSHW_FRLINE0PRIO;
  BRS_SRC_ERAYINT1  = 0x00000400 | BRSHW_FRLINE1PRIO;
  BRS_SRC_ERAYTINT0 = 0x00000400 | BRSHW_FRTIME0PRIO;
  BRS_SRC_ERAYTINT1 = 0x00000400 | BRSHW_FRTIME1PRIO;
#endif

#if defined (VGEN_ENABLE_DRVETH__BASEASR)
  BRS_SRC_ETH  = 0x00000400 | BRSHW_ETHPRIO;
#endif
}
#endif /*!BRS_OS_USECASE_OSGEN7&&!BRS_ENABLE_FBL_SUPPORT*/

/*****************************************************************************/
/**
 * @brief      Disables the global interrupt of the micro. This is done in a
 *             "save way" to allow also nested calls of BrsHwDisableInterrupt
 *             and BrsHwRestoreInterrupt. The first call of BrsHwDisableInterrupt
 *             stores the current state of the global INT flag and the last
 *             call to BrsHwRestoreInterrupt restores the state.
 * @pre        -
 * @param[in]  -
 * @param[out] -
 * @return     -
 * @context    Function is called from all modules to disable the global interrupt
 */
/*****************************************************************************/
void BrsHwDisableInterrupt(void)
{
  /* No check for "overrun" of nested INT lock counter is performed, due to more
  *  than 255 nested calls to BrsHwDisableInterrupt are very unlikely. */
  if (bBrsHwIntDiCounter == 0)
  {
    bBrsHwICR = mfcr(0xFE2C) & 0x08000; /* Read currend status of IE bit */
    disable_interrupt();
  }

  bBrsHwIntDiCounter++;
}

/*****************************************************************************/
/**
 * @brief      Restores the state of the global interrupt of the micro. This
 *             is done in a "save way" to allow also nested calls of
 *             BrsHwDisableInterrupt and BrsHwRestoreInterrupt. The first call
 *             of BrsHwDisableInterrupt stores the current state of the global
 *             INT flag and the last call to BrsHwRestoreInterrupt restores the
 *             state.
 * @pre        -
 * @param[in]  -
 * @param[out] -
 * @return     -
 * @context    Function is called from all modules to enable the global interrupt
 */
/*****************************************************************************/
void BrsHwRestoreInterrupt(void)
{
  /* Check for illegal call of BrsHwRestoreInterrupt. If this function is called
   *  too often, the INT lock works incorrect. */
  if (bBrsHwIntDiCounter == 0)
  {
    /* Check is only performed, if no OS and no FBL are used */
  }

  bBrsHwIntDiCounter--;
  if (bBrsHwIntDiCounter == 0)
  {
    if (bBrsHwICR)
    {
      enable_interrupt();
    }
  }
}

/*****************************************************************************/
/**
 * @brief      restart ECU (issue a software reset or jump to startup code)
 * @pre        -
 * @param[in]  -
 * @param[out] -
 * @return     -
 * @context    Function is called from e.g. ECU state handling
 */
/*****************************************************************************/
void BrsHwSoftwareResetECU(void)
{
  BrsHwDisableInterrupt();

  BrsHwSafetyUnlockInit();
  BRS_SWRSTCON |= 0x00000002;
  BrsHwSafetyLockInit();

  while (1)
  {
    /* Wait until reset/watchdog reset occurs */
  }
}

#if defined (BRS_ENABLE_SUPPORT_LEDS)          || \
    defined (BRS_ENABLE_SUPPORT_TOGGLE_WD_PIN) || \
    defined (BRS_ENABLE_SUPPORT_TOGGLE_CUSTOM_PIN)
/*****************************************************************************/
/**
 * @brief      This API is used to toggle a PortPin.
 *             Per default, the following parameters are available:
 *               BRSHW_TOGGLEPIN_LED
 *               BRSHW_TOGGLEPIN_WD
 *               BRSHW_TOGGLEPIN_CUSTOM
 *             Depending pins have to be configured within BrsHw_Ports.h.
 * @pre        -
 * @param[in]  Pin has to configure the depending pin to be toggled
 * @param[out] -
 * @return     -
 * @context    Function is called from all modules to set or clear a PortPin
 */
/*****************************************************************************/
void BrsHwTogglePin(brsHw_TogglePin Pin)
{
#if defined (VGEN_ENABLE_DRVDIO)
  #error "Configure the valid DioChannel for the toggle pins within your DrvDio config and set it as value for BrsHw_Dio_ToggleX_Channel. Or disable the depending BRS mechanism."*/
# if defined (BRS_ENABLE_SUPPORT_LEDS)
  Dio_ChannelType BrsHw_Dio_ToggleLED_Channel = DioConf_DioChannel_DioChannel_LED;
# endif
# if defined (BRS_ENABLE_SUPPORT_TOGGLE_WD_PIN)
  Dio_ChannelType BrsHw_Dio_ToggleWD_Channel = DioConf_DioChannel_DioChannel_WD;
# endif
# if defined (BRS_ENABLE_SUPPORT_TOGGLE_CUSTOM_PIN)
  Dio_ChannelType BrsHw_Dio_ToggleCUSTOM_Channel = DioConf_DioChannel_DioChannel_CUSTOM;
# endif
#endif /*VGEN_ENABLE_DRVDIO*/

#if defined (BRS_ENABLE_SUPPORT_LEDS)
  static uint8 BrsHw_ToggleSwitch_LED = BRSHW_PORT_LOGIC_HIGH;
#endif
#if defined (BRS_ENABLE_SUPPORT_TOGGLE_WD_PIN)
  static uint8 BrsHw_ToggleSwitch_WD = BRSHW_PORT_LOGIC_HIGH;
#endif
#if defined (BRS_ENABLE_SUPPORT_TOGGLE_CUSTOM_PIN)
  static uint8 BrsHw_ToggleSwitch_CUSTOM = BRSHW_PORT_LOGIC_HIGH;
#endif

  switch (Pin)
  {
#if defined (BRS_ENABLE_SUPPORT_LEDS)
    case BRSHW_TOGGLEPIN_LED:
# if defined (VGEN_ENABLE_DRVDIO)
      Dio_WriteChannel(BrsHw_Dio_ToggleLED_Channel, BrsHw_ToggleSwitch_LED & 0x01);
# else
      BrsHwPort_SetLevel(BRSHW_PORT_LED, BrsHw_ToggleSwitch_LED & 0x01);
# endif
      BrsHw_ToggleSwitch_LED++;
      break;
#endif /*BRS_ENABLE_SUPPORT_LEDS*/
#if defined (BRS_ENABLE_SUPPORT_TOGGLE_WD_PIN)
    case BRSHW_TOGGLEPIN_WD:
# if defined (VGEN_ENABLE_DRVDIO)
      Dio_WriteChannel(BrsHw_Dio_ToggleWD_Channel, BrsHw_ToggleSwitch_WD & 0x01);
# else
      BrsHwPort_SetLevel(BRSHW_PORT_TOGGLE_WD, BrsHw_ToggleSwitch_WD & 0x01);
# endif
      BrsHw_ToggleSwitch_WD++;
      break;
#endif /*BRS_ENABLE_SUPPORT_TOGGLE_WD_PIN*/
#if defined (BRS_ENABLE_SUPPORT_TOGGLE_CUSTOM_PIN)
    case BRSHW_TOGGLEPIN_CUSTOM:
# if defined (VGEN_ENABLE_DRVDIO)
      Dio_WriteChannel(BrsHw_Dio_ToggleCUSTOM_Channel, BrsHw_ToggleSwitch_CUSTOM & 0x01);
# else
      BrsHwPort_SetLevel(BRSHW_PORT_TOGGLE_CUSTOM, BrsHw_ToggleSwitch_CUSTOM & 0x01);
# endif
      BrsHw_ToggleSwitch_CUSTOM++;
      break;
#endif /*BRS_ENABLE_SUPPORT_TOGGLE_CUSTOM_PIN*/
    default:
      break;
  }
}
#endif /*BRS_ENABLE_SUPPORT_LEDS||BRS_ENABLE_SUPPORT_TOGGLE_WD_PIN||BRS_ENABLE_SUPPORT_TOGGLE_CUSTOM_PIN*/

/*****************************************************************************/
/**
 * @brief      This API is used for the BRS time measurement support to get a
 *             default time value for all measurements with this platform to
 *             be able to compare time measurements on different dates based
 *             on this time result.
 * @pre        -
 * @param[in]  -
 * @param[out] -
 * @return     -
 * @context    Function is called from TimeMeasurement
 */
/*****************************************************************************/
void BrsHwTime100NOP(void)
{
  BrsHwDisableInterrupt();

  nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
  nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
  nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
  nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
  nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();

  nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
  nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
  nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
  nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
  nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();

  BrsHwRestoreInterrupt();
}

/*****************************************************************************/
/**
 * @brief Routine to unlock registers that are normally protected
 * @pre   Interrupts must be disabled
 */
/*****************************************************************************/
void BrsHwUnlockInit(void)
{
  volatile uint32 wdtcon0;
  /*
   * 1st step: Password access (create password and send to WDT_CON0)
   */
  wdtcon0 = BRS_SFR_WDTCPU0CON0;
  wdtcon0 &= 0xFFFFFF01;     /* clear WDTLCK, WDTHPW0, WDTHPW1 */
  wdtcon0 |= 0xF0;           /* set WDTHPW1 to 0xf */
  wdtcon0 |= 0x01;           /* 1 must be written to ENDINIT for password access
                                (but this will not actually modify the bit)      */
  BRS_SFR_WDTCPU0CON0 = wdtcon0; /* modify access to WDTCON0 */
  /*
   * 2nd step: Modify access, set the bit ENDINIT to 1 or 0 to allow access to
   *           registers: WDT_CON1, BTV, BIV, ISP and mod_CLC
   */
  wdtcon0 &= 0xFFFFFFF0;     /* clear WDTHPW0, WDTLCK, ENDINIT  */
  wdtcon0 |= 0x02;           /* WDTHPW0=0, WDTLCK=1, ENDINIT=0 */
  isync();
  BRS_SFR_WDTCPU0CON0 = wdtcon0;
  wdtcon0 = BRS_SFR_WDTCPU0CON0; /* read is required */
}

/*****************************************************************************/
/**
 * @brief Routine to lock registers that are normally protected
 * @pre   Interrupts must be disabled
 */
/*****************************************************************************/
void BrsHwLockInit(void)
{
 /* AURIX */
  volatile uint32 wdtcon0;
  /*
   * 1st step: Password access (create password and send to WDT_CON0)
   */
  wdtcon0 = BRS_SFR_WDTCPU0CON0;
  wdtcon0 &= 0xFFFFFF01;     /* clear WDTLCK, WDTHPW0, WDTHPW1 */
  wdtcon0 |= 0xF0;           /* set WDTHPW1 to 0xf */
  wdtcon0 |= 0x01;           /* 1 must be written to ENDINIT for password access
                                 (but this will not actually modify the bit) */
  BRS_SFR_WDTCPU0CON0 = wdtcon0; /* modify access to WDTCON0 */
  /*
   * 2nd step: Modify access, set the bit ENDINIT to 1 or 0 to allow access to
   *           registers: WDT_CON1, BTV, BIV, ISP and mod_CLC
   */
  wdtcon0 &= 0xFFFFFFF0;     /* clear WDTHPW0, WDTLCK, ENDINIT  */
  wdtcon0 |= 0x03;           /* WDTHPW0=0, WDTLCK=1, ENDINIT=0 */
  isync();
  BRS_SFR_WDTCPU0CON0 = wdtcon0;
  wdtcon0 = BRS_SFR_WDTCPU0CON0; /* read is required */
}

/*****************************************************************************/
/**
 * @brief Routine to unlock registers that are safety protected
 * @pre   Interrupts must be disabled
 */
/*****************************************************************************/
void BrsHwSafetyUnlockInit(void)
{
  volatile uint32 wdtcon0;
  /*
   * 1st step: Password access (create password and send to WDT_CON0)
   */
  wdtcon0 = BRS_SFR_WDTSCON0;
  wdtcon0 &= 0xFFFFFF01;     /* clear WDTLCK, WDTHPW0, WDTHPW1 */
  wdtcon0 |= 0xF0;           /* set WDTHPW1 to 0xf */
  wdtcon0 |= 0x01;           /* 1 must be written to ENDINIT for password access
                                (but this will not actually modify the bit)      */
  BRS_SFR_WDTSCON0 = wdtcon0; /* modify access to WDTCON0 */
  /*
   * 2nd step: Modify access, set the bit ENDINIT to 1 or 0 to allow access to
   *           registers: WDT_CON1, BTV, BIV, ISP and mod_CLC
   */
  wdtcon0 &= 0xFFFFFFF0;     /* clear WDTHPW0, WDTLCK, ENDINIT  */
  wdtcon0 |= 0x02;           /* WDTHPW0=0, WDTLCK=1, ENDINIT=0 */
  isync();
  BRS_SFR_WDTSCON0 = wdtcon0;
  wdtcon0 = BRS_SFR_WDTSCON0; /* read is required */
}

/*****************************************************************************/
/**
 * @brief Routine to lock registers that are normally protected
 * @pre   Interrupts must be disabled
 */
/*****************************************************************************/
void BrsHwSafetyLockInit(void)
{
  volatile uint32 wdtcon0;
  /*
   * 1st step: Password access (create password and send to WDT_CON0)
   */
  wdtcon0 = BRS_SFR_WDTSCON0;
  wdtcon0 &= 0xFFFFFF01;     /* clear WDTLCK, WDTHPW0, WDTHPW1 */
  wdtcon0 |= 0xF0;           /* set WDTHPW1 to 0xf */
  wdtcon0 |= 0x01;           /* 1 must be written to ENDINIT for password access
                                 (but this will not actually modify the bit)      */
  BRS_SFR_WDTSCON0 = wdtcon0; /* modify access to WDTCON0 */
  /*
   * 2nd step: Modify access, set the bit ENDINIT to 1 or 0 to allow access to
   *           registers: WDT_CON1, BTV, BIV, ISP and mod_CLC
   */
  wdtcon0 &= 0xFFFFFFF0;     /* clear WDTHPW0, WDTLCK, ENDINIT  */
  wdtcon0 |= 0x03;           /* WDTHPW0=0, WDTLCK=1, ENDINIT=0 */
  isync();
  BRS_SFR_WDTSCON0 = wdtcon0;
  wdtcon0 = BRS_SFR_WDTSCON0; /* read is required */
}

void _endinit(void)
{
  /* enable GPT12 clock */
  BRS_GPT_CLC = 0x00000000UL;

  /* wait until the module disable flag is cleared*/
  while ((BRS_GPT_CLC & 0x00000002UL)==2){}
}


/* DrvCan/DrvLin/DrvFr/DrvEth application callback functions for access protection of critical registers */
/* As different driver versions use different callback names, they are duplicated. */
#if defined (VGEN_ENABLE_CAN_DRV)        || \
    defined (VGEN_ENABLE_LIN_DRV)        || \
    defined (VGEN_ENABLE_DRVFR__BASEASR) || \
    defined (VGEN_ENABLE_DRVETH__BASEASR)
void Appl_UnlockInit(void)
{
  BrsHwUnlockInit();
}

void Appl_UnlockEndinit(void)
{
  BrsHwUnlockInit();
}

void Appl_LockInit(void)
{
  BrsHwLockInit();
}

void Appl_LockEndinit(void)
{
  BrsHwLockInit();
}
#endif /*VGEN_ENABLE_CAN_DRV || VGEN_ENABLE_LIN_DRV || VGEN_ENABLE_DRVFR__BASEASR || VGEN_ENABLE_DRVETH__BASEASR*/

/* DrvCan Application function for wait mechanism */
#if defined (VGEN_ENABLE_CAN_DRV)
void ApplCanWaitSetMCR(void)
{
  uint32 i;
  for (i=0; i<10000; i++)
  {
    nop();
  }
}
#endif /*VGEN_ENABLE_CAN_DRV*/

# if defined (BRSASR_ENABLE_SAFECTXSUPPORT)
/*****************************************************************************/
/**
 * @brief      This API is used to enable hardware access in untrusted mode
 * @pre        -
 * @param[in]  -
 * @param[out] -
 * @return     -
 * @context    Function is called from BrsAsrInitBsw
 */
/*****************************************************************************/
void BrsHw_EnableHwAccess(void)
{
  /* Nothing to do for Aurix yet */
}
# else
  /* BrsHw_EnableHwAccess() removed by Organi, because of ALM attributes of project */
# endif /*BRSASR_ENABLE_SAFECTXSUPPORT*/

void GlobalSuspendInterrupts(){
	
}
void GlobalRestoreInterrupts(){
	
}