
#include "AppExternals.h"

#define CANTRCV_30_TJA1145_WAKE_PIN_EVENT_RISING    STD_ON
#define CANTRCV_30_TJA1145_WAKE_PIN_EVENT_FALLING   STD_ON

/* [Enrique.Bueno] Workaround START: HW PN SUPPORT enabled manually in order to use 'ClearTrcvWufFlag' api */
#define CANTRCV_30_TJA1145_HW_PN_SUPPORT                    STD_ON
#define CANTRCV_30_TJA1145_VERIFY_DATA                      STD_OFF
#define CanTrcv_30_Tja1145_IsPnEnabled(ch)                  TRUE
#define CanIf_30_Tja1145_ConfirmPnAvailability(ind)         
#define CanIf_30_Tja1145_ClearTrcvWufFlagIndication(ind)    
#define CanIf_30_Tja1145_CheckTrcvWakeFlagIndication(ind)     
/* [Enrique.Bueno] Workaround STOP */

