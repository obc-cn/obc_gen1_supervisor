 #include "Mcal.h"
 #include "Std_Types.h"
 #include "IfxSmu_reg.h"
 #include "IfxPort_reg.h"
 #include "IfxScu_reg.h"
 #include "IfxCpu_reg.h"
 #include "IfxMtu_reg.h"
 #include "IfxMc_bf.h"
 #include "IfxMc_reg.h"
 #include "AppCbk.h"
 #include "TstHandler.h"
 #include "Mcal_Options.h"
 #include "Sl_FlsErrPtrn.h"
 #include "SramEccTst_MemDef.h"
 #include "Mcu.h"
 #include "ifxStm_reg.h"
 #include "IfxStm_bf.h"
 #include "Appexternals.h"

 #if((CHIP_ID != 21U) && (CHIP_ID != 22U))
#include "IfxEmem_reg.h"
#endif

#define IFX_APPL_START_SEC_CODE_ASIL_B //the MACRO need to update according to the defination of MemMap setting.
#include "Ifx_MemMap.h"
static void STL_ErrorPinSetup(void);
#define IFX_APPL_STOP_SEC_CODE_ASIL_B //the MACRO need to update according to the defination of MemMap setting.
#include "Ifx_MemMap.h"
/*******************************************************************************
 **                      Private Macro Definitions                            **
 ******************************************************************************/
/* Configuration Required */
#define TSTM_ALM_GROUP              (5)
#define TSTM_ALM_POS                (5)
#define TSTM_PRERUN_SEED            (10)
#define TSTM_RUN_SEED               (10)
#define MCAL_NO_OF_CORES            (1)
#define NO_OF_GROUPS                (3)

#define TSTM_SEED_NOTVALID          (255U)    /* seed not valid/not available */

/* Ignore early prerun and prerun signature validation */
#define SL_IGNORE_SIGNATUREVALIDATE (FALSE)
/* Configuration switch to enable or disable runtime tests */
#define SL_RUNTIME_TESTS            (TRUE)
/* Activate time measurement after PORST for first code execution inside StartupHook() */
#define SAFETLIB_DEBUG_TIME_MEASUREMENT (STD_OFF)

#define STL_UNUSED_PARAMETER(VariableName)     { if( (uint32)(VariableName) == 0x00000000) \
                                                 { /* Do nothing! */}  }

/* Total Number of ResetReasons snaps to be stored */
#define RR_MAX_SNAPS_TO_STORE 5u


#define IFX_APPL_START_SEC_VAR_NONZERO_INIT_8BIT_ASIL_B
#include "Ifx_MemMap.h"

uint8 RA_CoreId2CfgSetIdx[MCAL_NO_OF_CORES] = {0};
static uint8 TstM_TstSeed = TSTM_SEED_NOTVALID;
static const unsigned int FAILURE_THRESHOLD  = 5;

#define IFX_APPL_STOP_SEC_VAR_NONZERO_INIT_8BIT_ASIL_B
#include "Ifx_MemMap.h"

#define RR_RESET_DEBUG_DATA_START_SEC_VAR
#include "MemMap.h"

struct {
  uint16 TotalNumberOfResets;
  uint8 WatchdogResetCounter;
  uint8 SafetyResetCounter;
  uint32 ResetReasonsSnap[RR_MAX_SNAPS_TO_STORE];
  uint8 SafetyResetErrorIdSnap[RR_MAX_SNAPS_TO_STORE];
  uint32 SmuAlarmGroup0Snap[RR_MAX_SNAPS_TO_STORE];
  uint32 SmuAlarmGroup1Snap[RR_MAX_SNAPS_TO_STORE];
  uint32 SmuAlarmGroup2Snap[RR_MAX_SNAPS_TO_STORE];
  uint32 SmuAlarmGroup3Snap[RR_MAX_SNAPS_TO_STORE];
  uint32 SmuAlarmGroup4Snap[RR_MAX_SNAPS_TO_STORE];
  uint32 SmuAlarmGroup5Snap[RR_MAX_SNAPS_TO_STORE];
  uint32 SmuAlarmGroup6Snap[RR_MAX_SNAPS_TO_STORE];
} RR_ResetReasonsDiagData;

#define RR_RESET_DEBUG_DATA_STOP_SEC_VAR
#include "MemMap.h"

#define IFX_APPL_START_SEC_VAR_32BIT_ASIL_B
#include "Ifx_MemMap.h"
 
/* Buffer for consolidated signatures */
static uint32 TstM_ConsolidatedSignCoreX[MCAL_NO_OF_CORES];

/* Aux variable for consolidated signature calculation */
volatile uint32 Application_Check_Results = 0;

/* Test results buffer used for all SafeTlib phases */
static Sl_TstRsltType TstM_TestResults[MCAL_NO_OF_CORES][TH_TOTAL_TESTS];

/* Test signature buffer used for all SafeTlib phases */
static uint32 TstM_TestSignatures[MCAL_NO_OF_CORES][TH_TOTAL_TESTS];

/* Next variables will be reset to zero after each PORST */
#if ( SAFETLIB_DEBUG_TIME_MEASUREMENT == STD_ON )
    static volatile uint32 STL_systemtimerH;
    static volatile uint32 STL_systemtimerL;
#endif /* END of #if ( SAFETLIB_DEBUG_TIME_MEASUREMENT == STD_ON ) */
//static volatile uint32 TstM_failureCtr;

#define IFX_APPL_STOP_SEC_VAR_32BIT_ASIL_B
#include "Ifx_MemMap.h"

#define IFX_APPL_START_SEC_VAR_NONZERO_INIT_32BIT_ASIL_B
#include "Ifx_MemMap.h"

/* Pre-run test result */
static Sl_TstRsltType TstM_PreRunTstResult[3] = {TEST_FAIL, TEST_FAIL, TEST_FAIL};

/* Expected overall signatures for  pre-run phase for each group per core
 * This is just example one of the examples configuration (each group per core)
 * This should be adapted and changed according to customer's configuration
 * Customer can write his own method of signature validation
 */
uint32 TstM_ExptdSigPreRun[MCAL_NO_OF_CORES][NO_OF_GROUPS] = {{0xC88DBC96,0xC88DBC96,0xC88DBC96}};

#define IFX_APPL_STOP_SEC_VAR_NONZERO_INIT_32BIT_ASIL_B
#include "Ifx_MemMap.h"

#if (SAFETLIB_APPLN_DEBUG == TRUE)   
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ CAUTION ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
/* This array needs to be updated whenever ApplCbk_ErrorIdType is updated in 
   file ApplCbk.h */   
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ CAUTION ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
/* Global array which contains all possible call back error-ID type information */
const char AplCbk_ErrorNames[][50] =
{
"APLCBK_PRERUN_INIT_FAIL",
"APLCBK_SWDGM_INIT_FAIL",
"APLCBK_SWDGIF_SERV_FAIL",
"APLCBK_TSTM_INIT_FAIL",
"APLCBK_POSTRUN_TST_FAIL",
"APLCBK_WDG_SERV_FAIL",
"APLCBK_WDG_DISABLE_STATE",
"APLCBK_TST_MNG_FAIL",
"APLCBK_RUNTIM_DTI_OVRSHOOT",
"APLCBK_TST_SEED_CORPT",
"APLCBK_NEW_SEED_FAIL",
"APLCBK_INV_SEEDSIG_FAIL",
"APLCBK_WDGIF_SERV_FAIL",
"APLCBK_SWDGIF_INIT_FAIL",
"APLCBK_SWDGIF_ACT_FAIL",
"APLCBK_SWDGIF_GTSED_FAIL",
"APLCBK_SMU_SET_ALARM_FAIL",
"APLCBK_SL_INIT_FAIL",
"APLCBK_WDGM_GET_SEED_FAIL",
"APLCBK_SL_PRE_RUN_TST_FAIL",
"APLCBK_SL_SWITCH_PHASE_FAIL",
"APLCBK_TSTM_INV_SEED_SIG_FAIL",
"APLCBK_SL_RUN_TST_FAIL",
"APLCBK_SL_POST_RUN_TST_FAIL",
"APLCBK_TSTM_INVALID_PHASE",
"APLCBK_SWDGIF_GETSTATE_FAIL",
"APLCBK_SL_EARLYPRE_RUN_TST_FAIL",
"APLCBK_SWDGIF_CHMOD_FAIL",
"APLCBK_CIC_USRCMD_FAIL",
"APLCBK_SWDGM_REINIT_FAIL",
"APLCBK_ERR_INJ_FAIL",     
"APLCBK_ERR_SPINLOCK_FAIL",
"APLCBK_ERR_TASK_BUDGET_OVRRUN",   
"APLCBK_CIC_TRIP_FAIL",           
"APLCBK_CIC_CYCLE_FAIL",           
"APLCBK_CIC_MAXJOBS_FAIL",         
"APLCBK_CIC_GETINFO_FAIL",        
"APLCBK_CIC_VERSION_FAIL",        
"APLCBK_WDG_TASK1_FAIL",       
"APLCBK_WDG_TASK2_FAIL",      
"APLCBK_WDG_TASK3_FAIL",          
"APLCBK_WDG_SPIERR_FAIL",
"APLCBK_TLF_MAXJOBS_FAIL",
"APLCBK_TLF_ABIST_FAIL"
};
#endif


#define IFX_APPL_START_SEC_CODE_ASIL_B //the MACRO need to update according to the defination of MemMap setting.
#include "Ifx_MemMap.h"

/*******************************************************************************
** Traceability     : [cover parentID=]        [/cover]                       **
**                                                                            **
** Syntax           : void RA_Stop(void)                                      **
**                                                                            **
** Service ID       : none                                                    **
**                                                                            **
** Sync/Async       : Synchronous                                             **
**                                                                            **
** Reentrancy       : Non Reentrant                                           **
**                                                                            **
** Parameters(in)   : None                                                    **
**                                                                            **
** Parameters (out) : None                                                    **
**                                                                            **
** Return value     : None                                                    **
**                                                                            **
** Description      : This function is used to stop in an endless loop
*******************************************************************************/ 
void RA_Stop(void)
{
  volatile uint32 LoopCount = 1U ;
  while (LoopCount == 1)
  {
    (void) NOP();
  }
}

/*******************************************************************************
**                                                                            **
** Syntax           : Sl_TstRsltType TstM_GetTstResult(Sl_TstIndexType TstIdx)**
**                                                                            **
** Service ID       : none                                                    **
**                                                                            **
** Sync/Async       : Synchronous                                             **
**                                                                            **
** Reentrancy       : Non-reentrant                                           **
**                                                                            **
** Parameters (in)  : TstID : The Test Id for which Result is requested       **
**                                                                            **
** Parameters (out) : None                                                    **
**                                                                            **
** Return value     : Sl_TstRsltType                                          **
**                    TH_TST_NOT_EXECUTED : Test is not executed              **
**                    Non-zero value : Test result                            **
**                                                                            **
** Description      :  This function is used to retrieve the test result      **
**                        for the test index passed                           **
**                                                                            **
*******************************************************************************/
Sl_TstRsltType TstM_GetTstResult(Sl_TstIndexType TstIdx)
{
  uint8 CoreId;
  Sl_TstRsltType Result;
  
  CoreId = Mcal_GetCoreId(); 
  Result = TH_TST_NOT_EXECUTED;
  
  if (TstIdx < TH_TOTAL_TESTS)
  {
    Result = TstM_TestResults[CoreId][TstIdx] ;
  }
  else
  {

  }
  return Result;
}

/*******************************************************************************
** Traceability: [cover parentID= <id_1>, <id_2>]                             **
**                                                                            **
** Syntax : void AppCbk_ErrorHandler(ApplCbk_ErrorIdType ErrorId)             **
**                                                                            **
** Description :  This function gives notification to user incase of          **
        test failure at any phase of SafeTlib                                 **
**                                                                            **
** [/cover]                                                                   **
**                                                                            **
** Service ID: None                                                           **
**                                                                            **
** Sync/Async: Synchronous                                                    **
**                                                                            **
** Reentrancy: Non reentrant                                                  **
**                                                                            **
** Parameters (in) : None                                                     **
**                                                                            **
** Parameters (out): None                                                     **
**                                                                            **
** Return value : void                                                        **
*******************************************************************************/
void AppCbk_ErrorHandler(ApplCbk_ErrorIdType ErrorId)
{
  int UsrInfo;
  uint8 CoreId;
  CoreId = ((uint8)MFCR(CPU_CORE_ID) & (uint8)0x3U);
  boolean SafetyReaction;
  SafetyReaction = FALSE;
  
#if (SAFETLIB_APPLN_DEBUG == TRUE)    
  Mcal_SuspendAllInterrupts();
  // print_f("\n Error_ApplicationCallback occurred. Error = 0x%x", ErrorId);
  // print_f(" [%s]", AplCbk_ErrorNames[ErrorId]);   
  Mcal_ResumeAllInterrupts();
#endif

  switch(ErrorId)
  {
    /* Print information about tests failed and test result */
    case APLCBK_SL_EARLYPRE_RUN_TST_FAIL:
    case APLCBK_SL_PRE_RUN_TST_FAIL:
    case APLCBK_SL_RUN_TST_FAIL:
    case APLCBK_SL_POST_RUN_TST_FAIL:
    {
#if (SAFETLIB_APPLN_DEBUG == TRUE)       
      Sl_TstIndexType TstIdx ;
      
      for(TstIdx = 0; TstIdx < TH_TOTAL_TESTS ; TstIdx++)
      {
        if (TstM_GetTstResult(TstIdx) != TH_TST_NOT_EXECUTED)
        {
          if(!SL_SUCCESS(TstM_GetTstResult(TstIdx)))
          {
            /* LockStep Tst filtering (TestID=0x19, TestIndex=7) */
            if(TstIdx == 7u)
            {
                /* LockStep tst shall be executed */
                SafetyReaction = TRUE;
            }
            else
            {
              SafetyReaction = TRUE;
            }
          } /*End of (SL_FAILURE !=  SL_SUCCESS(TstM_GetTstResult(TstIdx)))*/
          else
          {

          }
        } /* End of (TstM_GetTstResult(TstIdx) != 0)*/
        else
        {

        }
      }
#endif
    }
    break;

    case APLCBK_SL_PRE_RUN_TST_SIGNATURE_FAIL:
      SafetyReaction = TRUE;
    break;

    case APLCBK_MCAL_SAFECHECK_FAIL:
    case APLCBK_MCAL_GENERAL_FAIL:
      SafetyReaction = TRUE;
    break;
  
    case APLCBK_MPU_VIOLATION_DETECT:
      SafetyReaction = TRUE;
    break;

    default:
      if(ErrorId >= APLCBK_PROTECTION_HOOK_STARTID_DETECT && \
         ErrorId <= APLCBK_PROTECTION_HOOK_ENDID_DETECT){
        SafetyReaction = TRUE;
      }
    break;
  }

  /* Safety Reaction */
  if(SafetyReaction != FALSE)
  {
    Mcal_SuspendAllInterrupts();

    /* Store the Safety reset error id (=ErrorId+1) */
    UsrInfo = MCU_SFR_RUNTIME_USER_MODE_READ32(SCU_RSTCON2.B.USRINFO);
    MCU_SFR_RUNTIME_RESETSAFETYENDINIT_TIMED(MCU_SAFETY_ENDINT_TIMEOUT);
    SCU_RSTCON2.B.USRINFO = ((UsrInfo & 0x00FF) | ((ErrorId + 1u) << 8u));
    MCU_SFR_RUNTIME_SETSAFETYENDINIT_TIMED();

    /* SYSTEM RESET */
    MCU_SFR_RUNTIME_RESETSAFETYENDINIT_TIMED(MCU_SAFETY_ENDINT_TIMEOUT);
    SCU_RSTCON.B.SW = 1u;
    MCU_SFR_RUNTIME_SETSAFETYENDINIT_TIMED();
    Mcu_PerformReset();

    Mcal_ResumeAllInterrupts();
   }

  Mcal_SuspendAllInterrupts();
 // RA_Stop();
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void TstM_Init(void)                                   **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in)  : None                                                   **
 **                                                                           **
 ** Parameters (out) : None                                                   **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : Initialization of the Test Manager global variables    **
 **                    and invoke the initialization of SafeTlib.             **
 **                    Error shall be reported through application callback   **
 **                                                                           **
 ******************************************************************************/
void TstM_Init(void)
{
  Std_ReturnType Result = E_NOT_OK;

  uint8 CoreId;

  CoreId = Mcal_GetCoreId();

  Result = Sl_Init(&Sl_ConfigRoot[RA_CoreId2CfgSetIdx[CoreId]]);
  
  if (Result != E_OK)
  {
    AppCbk_ErrorHandler(APLCBK_TSTM_INIT_FAIL);
  }
  else
  {

  }
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void TstM_InvalidateData(void)                         **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in)  : None                                                   **
 **                                                                           **
 ** Parameters (out) : None                                                   **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : Invalidates seed, test results and test signatures     **
 **                                                                           **
 ******************************************************************************/
void TstM_InvalidateData(void)
{
  uint8 CoreId;
  Sl_TstIndexType TstIdx;

  CoreId = Mcal_GetCoreId();

  if (SL_MASTER_CORE_ID == CoreId)
  {
    TstM_TstSeed = TSTM_SEED_NOTVALID;
  }
  else
  {

  }
  
  for (TstIdx = 0U; TstIdx < TH_TOTAL_TESTS; TstIdx++)
  {
    TstM_TestResults[CoreId][TstIdx] = TH_TST_NOT_EXECUTED;
    TstM_TestSignatures[CoreId][TstIdx] = (uint32)0U ;
  }
  
  TstM_ConsolidatedSignCoreX[CoreId] = 0x00;
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void TstM_lConsolidateSigCoreX(void)                   **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in)  : None                                                   **
 **                                                                           **
 ** Parameters (out) : None                                                   **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : Consolidation of signatures for one core               **
 **                                                                           **
 ******************************************************************************/
static void TstM_lConsolidateSigCoreX(void)
{
  volatile uint32 ConsolidatedSignCoreX = 0 ;
  volatile uint32 ConsolidatedSignCoreX0 = 0 ;
  Sl_TstIndexType TstIdx;
  uint8 CoreId;

  CoreId = Mcal_GetCoreId();
  
  for (TstIdx = 0; TstIdx < TH_TOTAL_TESTS ; TstIdx++)
  {
    if(TstM_TestSignatures[CoreId][TstIdx] != 0U)
    {
      ConsolidatedSignCoreX = CRC32(TstM_TestSignatures[CoreId][TstIdx], ConsolidatedSignCoreX);
    }
    else
    {

    }
  }

  if(Application_Check_Results != 0U)
  {
    ConsolidatedSignCoreX = CRC32(Application_Check_Results, ConsolidatedSignCoreX);
  }
  else
  {

  }
  
  ConsolidatedSignCoreX0 = CRC32(ConsolidatedSignCoreX,0U);
  TstM_ConsolidatedSignCoreX[CoreId] = ConsolidatedSignCoreX0;
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void TstM_CheckPreRunTestResult(uint32 Result)         **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in)  : None                                                   **
 **                                                                           **
 ** Parameters (out) : None                                                   **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : Verifies any test failed in prerun phase               **
 **                    Error shall be reported through application callback   **
 **                                                                           **
 ******************************************************************************/
void TstM_CheckPreRunTestResult(void)
{
  uint8 CoreId;
  
  CoreId = Mcal_GetCoreId();
  
  if(TstM_PreRunTstResult[CoreId] != TEST_PASS)
  {
    AppCbk_ErrorHandler(APLCBK_SL_PRE_RUN_TST_FAIL);
  }
}   

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void TstM_PreRunTst(uint8 GroupStartIndex,             **
 **                                                      uint8 GroupEndIndex) **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in)  : GroupStartIndex: First group index for test execution  **
 **                    GroupEndIndex: Last group index for test execution     **
 **                                                                           **
 ** Parameters (out) : None                                                   **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : Performs pre-run phase execution                       **
 **                    Error shall be reported through application callback   **
 **                                                                           **
 ******************************************************************************/
void TstM_PreRunTst(uint8 GroupStartIndex, uint8 GroupEndIndex)
{
  Std_ReturnType Result = E_NOT_OK;
  uint8 TestGroupIndex = 0;
  uint8 TstId = 0 ;
  volatile uint8 CoreId;
  uint32 FlagAnyOneTestFailed = 0 ;
  uint8  LastGroupIndex;
  
  TstM_TstSeed = TSTM_PRERUN_SEED;
  CoreId = Mcal_GetCoreId();  

  LastGroupIndex = (GroupEndIndex < Sl_ConfigRoot[RA_CoreId2CfgSetIdx[CoreId]].GroupCountPreRun)? GroupEndIndex : (Sl_ConfigRoot[RA_CoreId2CfgSetIdx[CoreId]].GroupCountPreRun - 1);
  for (TestGroupIndex = GroupStartIndex; TestGroupIndex <= LastGroupIndex;
       TestGroupIndex++)
  {
    Result = Sl_ExecPreRunTst(TstM_TstSeed, TestGroupIndex, TstM_TestSignatures[CoreId], TstM_TestResults[CoreId]);

    if (E_OK != Result)
    {
      FlagAnyOneTestFailed++ ;
    }
    else
    {

    }
  }
  
  if(FlagAnyOneTestFailed > 0)
  {
   AppCbk_ErrorHandler(APLCBK_SL_PRE_RUN_TST_FAIL);
  }
  else
  {

  }

  /* Pre_Run TstM raise alarm on test failure */
  for (TstId = 0; TstId < TH_TOTAL_TESTS; TstId++)
  {
    /* if false: Invalid test Id */
    if(TstM_TestResults[CoreId][TstId] != 0)
    {
      if (!(SL_SUCCESS(TstM_TestResults[CoreId][TstId]))) /* Test(s) in test group fails */
      {
        TstM_PreRunTstResult[CoreId] = TEST_FAIL;
        //Result = Smu_SetAlarmStatus(TSTM_ALM_GROUP, TSTM_ALM_POS);
		Result = E_NOT_OK;
        if (Result != E_OK)
        {
          AppCbk_ErrorHandler(APLCBK_SMU_SET_ALARM_FAIL);
        }
        else
        {

        }
        break;
      }
      else
      {

      }
    }
    else
    {

    }
  }
  
#if (SL_IGNORE_SIGNATUREVALIDATE == FALSE)  
  /* 
   * This is just an example, this is one of the ways for pre-run signature validation
   * Consolidate signatures of pre-run test signatures per core
   */
  TstM_lConsolidateSigCoreX();
  
  /* LockStep tst shall be executed and passed OK (FBL with LockStep enable is used) then,
     consolidated signature check shall be performed */
  if(TstM_ConsolidatedSignCoreX[CoreId] != TstM_ExptdSigPreRun[CoreId][GroupEndIndex])
  {
    AppCbk_ErrorHandler(APLCBK_SL_PRE_RUN_TST_SIGNATURE_FAIL);
  }
  else
  {
    /* Nothing */
  }

#endif
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void TstM_Run(void)                                    **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in)  : None                                                   **
 **                                                                           **
 ** Parameters (out) : None                                                   **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to execute all tests in     **
 **    run-time phase groups for current cycle of diagnostic test interval.   **
 **    Before executing tests, it also invalidates seed, test results and     **
 **    test signatures corresponding last cycle.                              **
 **       Incase of the master core, the following functionality is executed  **
 **    additionally:                                                          **
 **    - During first call to this API, it makes transition to run-time phase.**
 **    - During every call to this API, it gets the seed from SafeWDG Manager.**
 **    - Error shall be reported through application callback                 **
 **                                                                           **
 ******************************************************************************/
void TstM_Run(void)
{
  Std_ReturnType Result = E_NOT_OK;
  uint8 TstGroupIndex = 0;
  volatile uint8 CoreId; 
  /* CoreId is made volatile because tasking compiler gives improper warning
  on line 436 for array Sl_ConfigRoot as access out of bounds */
  /* Invalidate seed, test results and test signatures for current cycle */
  TstM_InvalidateData();
  
  CoreId = Mcal_GetCoreId();

  //Use fixed seed, accoording to the feedback from hitex, set the seed to be 10.
  TstM_TstSeed = TSTM_RUN_SEED;
  /* Run Tests */

  for (TstGroupIndex = 0; TstGroupIndex < Sl_ConfigRoot[RA_CoreId2CfgSetIdx[CoreId]].GroupCountRuntime ; TstGroupIndex++)
  {
    if(SL_MASTER_CORE_ID == CoreId)
    {
      Result = Sl_ExecRunTst(TstM_TstSeed, TstGroupIndex, TstM_TestSignatures[CoreId], TstM_TestResults[CoreId]);
    }
    else
    {
      Result = E_NOT_OK;
    }
    
  /* Only for debug purposes: */
  //if ( Result == E_OK)
  //{
  //  Result = E_NOT_OK;
  //}

    if (Result != E_OK)
    {
      //TstM_failureCtr++;
      /* Try five time to execute STL runtime tests before handle it. */
      //if ( TstM_failureCtr >= 5 )
      //{
        //Smu_ActivateFSP();//Activate fault run
        AppCbk_ErrorHandler(APLCBK_SL_RUN_TST_FAIL);
      //}
      break;
    }
    else
    {
      //TstM_failureCtr = 0U;
    }
  } 
  
  TstM_lConsolidateSigCoreX();    /* Signature consolidation */
}

static void STL_ErrorPinSetup(void)
{
  /* After a PoRst, the FSP pin can be managed as a GPIO */

  Mcal_ResetENDINIT();
  P33_PDR1.B.PD8  = 0x00U;  //(Pad Driver Mode 1 Register)
  Mcal_SetENDINIT();
  
  P33_OUT.B.P8 = 0x01U; // (Output Register)
  P33_IOCR8.B.PC8 = (unsigned int) 0x10U; //(Port 33 Input/Output Control Register 8)
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void Integration_PreRun_StartupHook(void)                                    **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in)  : None                                                   **
 **                                                                           **
 ** Parameters (out) : None                                                   **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to exectue the pre-run test **
 **                    First disable all the interrupt and init Test Manager  **
 **                    global variables. and implment the pre-condtion        **
 **                    according to the AoU. it also invalidates seed, test   **
 **                    results,test signatures. then run the pre-run test and **
 **                    check its results                                      **
 ******************************************************************************/
extern const PmuEccEdcTst_TestPatternType PmuEccTst_TestPatternB0;

void Integration_PreRun_StartupHook(void) 
{
	volatile uint8 nic;
	nic = *((uint8 *) &PmuEccTst_TestPatternB0);
	(void) nic;
	
	Std_ReturnType preCondResult = (Std_ReturnType) E_OK;
	uint32 errorLine     = 0;
    Std_ReturnType Result = E_NOT_OK ;
    uint8 CoreId;
    Ifx_Strict_32Bit Temp;
	Std_ReturnType procErrorPatternResult = E_NOT_OK;
    CoreId = Mcal_GetCoreId();
	
  #if ( SAFETLIB_DEBUG_TIME_MEASUREMENT == STD_ON )
    //Time measurement:
   
    STL_systemtimerL = (uint32) STM0_TIM0SV.U;
    STL_systemtimerH = (uint32) STM0_CAPSV.U;

    STL_UNUSED_PARAMETER(STL_systemtimerH);
    STL_UNUSED_PARAMETER(STL_systemtimerL);
  #endif /* END of #if ( SAFETLIB_DEBUG_TIME_MEASUREMENT == STD_ON ) */

	Mcal_SuspendAllInterrupts();

  //Config error pin
  STL_ErrorPinSetup();

	//Sl_ChkProgErrorPattern();
	if(SL_PROG_DONE == Sl_ChkProgErrorPattern())
	{
		procErrorPatternResult = (Std_ReturnType)E_OK;
	}
	else{
		procErrorPatternResult = (Std_ReturnType)E_NOT_OK;
	}
    Result = Sl_PreInit(&Sl_ConfigRoot[RA_CoreId2CfgSetIdx[CoreId]]);
    if(Result == E_NOT_OK)
    {
      //print_f("\n PreRun Initialization Failed");
      AppCbk_ErrorHandler(APLCBK_PRERUN_INIT_FAIL);
    }
  
  TstM_Init();

  //implment the pre-conditon for the pre-run test.
  /* Clockmon test pre-conditions */
        if ( ( SCU_CCUCON0.B.CLKSEL != (unsigned int) 0x01U ) || \
             ( SCU_CCUCON1.B.INSEL  != (unsigned int) 0x01U ) || \
             ( SCU_CCUCON1.B.GTMDIV == (unsigned int) 0x00U ) || \
             ( SCU_CCUCON1.B.STMDIV == 0x00U ) 
           )
        { /* register doesn't have the correct value, wrong precondition */
            preCondResult = (Std_ReturnType) E_NOT_OK;
            errorLine     = (uint32)__LINE__;
        } else { /* MISRA else */ }
  
  //SM_AURIX_STL_176, 122,126,129
  //unsigned long AG3CF0_Status = (unsigned long)(SMU_AGCF3_0.U);
  Ifx_Strict_32Bit CPU_PMA1_TMP = MFCR(CPU_PMA1);
  Ifx_Strict_32Bit CPU_PMA0_TMP = MFCR(CPU_PMA0);
  Ifx_Strict_32Bit CPU_PCONO_TMP = MFCR(CPU_PCON0);
  Ifx_Strict_32Bit CPU0_DATR_TMP = MFCR(CPU_DATR);
  Ifx_Strict_32Bit CPU0_PIETR_TMP =MFCR(CPU_PIETR);
  Ifx_Strict_32Bit CPU0_DSTR_TMP = MFCR(CPU_DSTR);
  Ifx_Strict_32Bit CPU0_DIETR_TMP =MFCR(CPU_DIETR);
  Ifx_Strict_32Bit CPU0_DIEAR_TMP =MFCR(CPU_DIEAR);
  Ifx_Strict_32Bit CPU0_DEADD_TMP =MFCR(CPU_DEADD);
  
  //disable code Access Cacheability for DSPR by setting bit field PMA1.CAC bits 5,6,7 and 13 to 0
  Mcal_ResetENDINIT();
  DSYNC();
  Temp = MFCR(CPU_PMA1);
  Temp &= ~(1<<5 | 1<<6 | 1<<7 | 1<<13);
  MTCR(CPU_PMA1, Temp);
  ISYNC();
  
  //disable data cacheability for segment C is off (PMA0[12] must be left to its default value 0
  DSYNC();
  Temp = MFCR(CPU_PMA0);
  Temp &= ~(1<<12);
  MTCR(CPU_PMA0, Temp);
  ISYNC();
  //enable program cache

  Temp = MFCR(CPU_PCON0);
  Temp &= ~(1<<1);
  MTCR(CPU_PCON0,Temp);
 
  MTCR(CPU_DATR,0x00U);
 
  MTCR(CPU_DSTR,0x00U);
  MTCR(CPU_PIETR,0x00U);
  MTCR(CPU_DIETR,0x00U);
  MTCR(CPU_DIEAR,0x00U);
  MTCR(CPU_DEADD,0x00U);

  Mcal_SetENDINIT();

  /* Invalidate seed, test results and test signatures for pre-run phase */
  TstM_InvalidateData();
  
  /*Clear Alarm status before the execution of Pre_run tests*/
  Pre_run_ResetSMUAlarms();

  TstM_PreRunTst(0,2);

  //restore the configuration for the register.
  Mcal_ResetENDINIT();
  DSYNC();
  MTCR(CPU_PMA1, CPU_PMA1_TMP);
  ISYNC();

  DSYNC();
  MTCR(CPU_PMA0, CPU_PMA0_TMP);
  ISYNC();

  MTCR(CPU_PCON0, CPU_PCONO_TMP);
 
  MTCR(CPU_DATR,0x00U);
  MTCR(CPU_DSTR,0x00U);
  MTCR(CPU_PIETR,0x00U);
  MTCR(CPU_DIETR,0x00U);
  MTCR(CPU_DIEAR,0x00U);
  MTCR(CPU_DEADD,0x00U);
  
  Mcal_SetENDINIT();
  Mcal_ResumeAllInterrupts();

  /*Enable SMU to control the error pin*/
  /*Check all the Alarm status and set App_Errorhandler(); if not then call Smu_SetupErrorPin*/
  
  /* Check if the SMU alarms are set or not. No SMU alarm should be set */
    Pre_run_ResetSMUAlarms();
    Smu_SetupErrorPin();
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void Integration_MoveToRunState(void)                                    **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in)  : None                                                   **
 **                                                                           **
 ** Parameters (out) : None                                                   **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to switch the test phase    **
 **                    into run state.                                        **
 ******************************************************************************/
void Integration_MoveToRunState(void)
{
  Std_ReturnType Result = E_NOT_OK;

  STL_ErrorPinSetup();

  //switch the state of the runtime state.
  Result = Sl_SwitchTstPhase(SL_RUN);
  if(E_OK != Result)
  {
    AppCbk_ErrorHandler(APLCBK_SL_SWITCH_PHASE_FAIL);
  }
  
} 

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void Intergration_RunTest(void)                                    **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in)  : None                                                   **
 **                                                                           **
 ** Parameters (out) : None                                                   **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to run the run-time test    **
 **                                                                           **
 ******************************************************************************/
void Intergration_RunTest(void)
{
  TstM_Run(); 

}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void Intergration_UnableToStartMng(void)               **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in)  : None                                                   **
 **                                                                           **
 ** Parameters (out) : None                                                   **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to manage the Unable To     **
 **                    Start state which is part of the Safety Reaction       **
 **                    implemented.                                           **
 **                    Additionally, ResetReasons are stored for debugging    **
 **                    purposes.                                              ** 
 **                                                                           **
 ******************************************************************************/
void Intergration_UnableToStartMng(void)
{
    Ifx_SCU_RSTSTAT RstReasons;
    Ifx_SCU_RSTCON RstConfig;
    int UsrInfo;
    uint8 WatchdogResetCounter;
    uint8 SafetyResetCounter;
    uint8 SafeTlibResetErrorId;
    uint8 SafetyResetErrorId;
    boolean SafetyReactionActions;

    SafetyReactionActions = FALSE;


    Mcal_SuspendAllInterrupts();

    /* Check for the Reset Reasons */
    RstReasons = MCU_SFR_RUNTIME_USER_MODE_READ32(SCU_RSTSTAT);
    RstConfig = MCU_SFR_RUNTIME_USER_MODE_READ32(SCU_RSTCON);
    UsrInfo = MCU_SFR_RUNTIME_USER_MODE_READ32(SCU_RSTCON2.B.USRINFO);
    WatchdogResetCounter = RR_ResetReasonsDiagData.WatchdogResetCounter;
    SafetyResetCounter = (uint8)(UsrInfo & 0x00FFu);
    SafeTlibResetErrorId = (uint8)((UsrInfo & 0xFF00u) >> 8u);
    SafetyResetErrorId = 0u;

    /* Cold Reset Detected (STBYR, EVR13, PORST triggered) */
    if((RstReasons.B.STBYR != FALSE) && (RstReasons.B.EVR13 != FALSE) && \
       (RstReasons.B.PORST != FALSE) && (RstReasons.B.SWD != FALSE))
    {
        /* Update SafetyReset error Id */
        SafetyResetErrorId = 0x00u;

        /* Clear cold reset flag */
        MCU_SFR_RUNTIME_RESETSAFETYENDINIT_TIMED(MCU_SAFETY_ENDINT_TIMEOUT);
        SCU_RSTCON2.B.CLRC = 1u;
        MCU_SFR_RUNTIME_SETSAFETYENDINIT_TIMED();

        /* Do NOT trigger SafetyReaction actions */
    }
    /* Watchdog Reset Detected (ESR0 pin triggered) */
    else if(RstReasons.B.ESR0 != FALSE)
    {
        /* Increment WatchdogReset counter */
        WatchdogResetCounter += 1u;

        /* Update SafetyReset error Id */
        SafetyResetErrorId = 0x00u;

        /* Do NOT trigger SafetyReaction actions */
    }
    /* Safety Reset Detected (SMU triggered) */
    else if(RstReasons.B.SMU != FALSE)
    {
        /* Increment SafetyReset counter */
        SafetyResetCounter += 1u;

        /* Update SafetyReset error Id */
        SafetyResetErrorId = 0xFFu;

        /* Clear SMU internal reset flag */
        MCU_SFR_RUNTIME_RESETSAFETYENDINIT_TIMED(MCU_SAFETY_ENDINT_TIMEOUT);
        SCU_WDTS_CON1.B.CLRIRF = 1u;
        MCU_SFR_RUNTIME_SETSAFETYENDINIT_TIMED();

        /* Trigger SafetyReaction actions */
        SafetyReactionActions = TRUE;
    }
    /* Safety Reset Detected (SW triggered) */
    else if((RstReasons.B.SW != FALSE) && (RstConfig.B.SW == 1u) && (SafeTlibResetErrorId != 0x00u))
    {
        /* Increment SafetyReset counter */
        SafetyResetCounter += 1u;

        /* Update SafetyReset error Id */
        SafetyResetErrorId = SafeTlibResetErrorId;

        /* Clear SafeTlib internal reset flag */
        MCU_SFR_RUNTIME_RESETSAFETYENDINIT_TIMED(MCU_SAFETY_ENDINT_TIMEOUT);
        SCU_RSTCON2.B.USRINFO = (UsrInfo & 0x00FFu);
        MCU_SFR_RUNTIME_SETSAFETYENDINIT_TIMED();

        /* Trigger SafetyReaction actions */
        SafetyReactionActions = TRUE;
    }
    /* Remaining Resets Detected... */
    else
    {
        /* Nothing */
    }

    /* Store Reset Reasons & related debug info. */
    RR_StoreResetReasons((uint32)RstReasons.U, WatchdogResetCounter, SafetyResetCounter, SafetyResetErrorId);

    /* SafetyReaction actions: 
       Manage FSP line & UnableToStart state */
    if(SafetyReactionActions != FALSE)
    {
        /* Update registers for SafetyReset counter and SafeTlib internal reset flag */
        MCU_SFR_RUNTIME_RESETSAFETYENDINIT_TIMED(MCU_SAFETY_ENDINT_TIMEOUT);
        SCU_RSTCON2.B.USRINFO = (0x0000u + SafetyResetCounter);
        MCU_SFR_RUNTIME_SETSAFETYENDINIT_TIMED();

        if(SafetyResetCounter >= FAILURE_THRESHOLD)
        {
            /* ACTIVATE FSP line */
            /* - After a PoRst, the FSP pin can be managed as a GPIO */
            P33_OUT.B.P8 = 0x00u;
            P33_IOCR8.B.PC8 = 0x10u;
            /* - After a SysRst/SwRst, the FSP pin shall be managed through SMU only */ 
            Smu_ActivateFSP();

            /* UNABLE TO START STATE (Keep in the endles loop) */
            RA_Stop();
        }
    }

    Mcal_ResumeAllInterrupts();
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void RR_GetResetReasons_RstCnts(uint16*,uint8*,uint8*) **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in) : None                                                    **
 **                                                                           **
 ** Parameters (out)  : total_num_rsts: variable pointer to store             **
 **                                     TotalNumberOfResets                   **
 **                    num_wdg_rsts: variable pointer to store                **
 **                                  WatchdogResetCounter                     **
 **                    num_safe_rsts: variable pointer to store               **
 **                                   SafetyResetCounter                      **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to report the three         **
 **                    reset counters stored by the RR_StoreResetReasons      **
 **                    function.                                              **
 **                                                                           **
 ******************************************************************************/
void RR_GetResetReasons_RstCnts(uint16 *total_num_rsts, uint8 *num_wdg_rsts , uint8 *num_safe_rsts)
{
  *total_num_rsts = RR_ResetReasonsDiagData.TotalNumberOfResets;
  *num_wdg_rsts = RR_ResetReasonsDiagData.WatchdogResetCounter;
  *num_safe_rsts = RR_ResetReasonsDiagData.SafetyResetCounter;
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void RR_GetResetReasons_RstRsnSnap(uint8,uint32*)      **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in) : snap_indx: snapshot index to be returned [0,..,4]       **
 **                                                                           **
 ** Parameters (out)  : rst_rsn_snap: variable pointer to store               **
 **                                   ResetReasonsSnap[snap_indx]             **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to report the reset         **
 **                    reasons snapshots.                                     **
 **                                                                           **
 ******************************************************************************/
void RR_GetResetReasons_RstRsnSnap(uint8 snap_indx, uint32 *rst_rsn_snap)
{
  *rst_rsn_snap = RR_ResetReasonsDiagData.ResetReasonsSnap[snap_indx];
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void RR_GetResetReasons_SafeRstErrIdSnap(uint8,uint8*)**
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in) : snap_indx: snapshot index to be returned [0,..,4]       **
 **                                                                           **
 ** Parameters (out)  : safe_rst_errid_snap: variable pointer to store        **
 **                                        SafetyResetErrorIdSnap[snap_indx]  **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to report the safety        **
 **                    reset error Id snapshots.                              **
 **                                                                           **
 ******************************************************************************/
void RR_GetResetReasons_SafeRstErrIdSnap(uint8 snap_indx, uint8 *safe_rst_errid_snap)
{
  *safe_rst_errid_snap = RR_ResetReasonsDiagData.SafetyResetErrorIdSnap[snap_indx];
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void RR_GetResetReasons_SmuAlrmGrp0Snap(uint8,uint32*) **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in) : snap_indx: snapshot index to be returned [0,..,4]       **
 **                                                                           **
 ** Parameters (out)  : safe_rst_errid_snap: variable pointer to store        **
 **                                          SmuAlarmGroup0Snap[snap_indx]    **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to report the smu alarms    **
 **                    snapshots.                                             **
 **                                                                           **
 ******************************************************************************/
void RR_GetResetReasons_SmuAlrmGrp0Snap(uint8 snap_indx, uint32 *smu_alrmgrp0_snap)
{
  *smu_alrmgrp0_snap = RR_ResetReasonsDiagData.SmuAlarmGroup0Snap[snap_indx];
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void RR_GetResetReasons_SmuAlrmGrp1Snap(uint8,uint32*) **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in) : snap_indx: snapshot index to be returned [0,..,4]       **
 **                                                                           **
 ** Parameters (out)  : safe_rst_errid_snap: variable pointer to store        **
 **                                          SmuAlarmGroup1Snap[snap_indx]    **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to report the smu alarms    **
 **                    snapshots.                                             **
 **                                                                           **
 ******************************************************************************/
void RR_GetResetReasons_SmuAlrmGrp1Snap(uint8 snap_indx, uint32 *smu_alrmgrp1_snap)
{
  *smu_alrmgrp1_snap = RR_ResetReasonsDiagData.SmuAlarmGroup1Snap[snap_indx];
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void RR_GetResetReasons_SmuAlrmGrp2Snap(uint8,uint32*) **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in) : snap_indx: snapshot index to be returned [0,..,4]       **
 **                                                                           **
 ** Parameters (out)  : safe_rst_errid_snap: variable pointer to store        **
 **                                          SmuAlarmGroup2Snap[snap_indx]    **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to report the smu alarms    **
 **                    snapshots.                                             **
 **                                                                           **
 ******************************************************************************/
void RR_GetResetReasons_SmuAlrmGrp2Snap(uint8 snap_indx, uint32 *smu_alrmgrp2_snap)
{
  *smu_alrmgrp2_snap = RR_ResetReasonsDiagData.SmuAlarmGroup2Snap[snap_indx];
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void RR_GetResetReasons_SmuAlrmGrp3Snap(uint8,uint32*) **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in) : snap_indx: snapshot index to be returned [0,..,4]       **
 **                                                                           **
 ** Parameters (out)  : safe_rst_errid_snap: variable pointer to store        **
 **                                          SmuAlarmGroup3Snap[snap_indx]    **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to report the smu alarms    **
 **                    snapshots.                                             **
 **                                                                           **
 ******************************************************************************/
void RR_GetResetReasons_SmuAlrmGrp3Snap(uint8 snap_indx, uint32 *smu_alrmgrp3_snap)
{
  *smu_alrmgrp3_snap = RR_ResetReasonsDiagData.SmuAlarmGroup3Snap[snap_indx];
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void RR_GetResetReasons_SmuAlrmGrp4Snap(uint8,uint32*) **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in) : snap_indx: snapshot index to be returned [0,..,4]       **
 **                                                                           **
 ** Parameters (out)  : safe_rst_errid_snap: variable pointer to store        **
 **                                          SmuAlarmGroup4Snap[snap_indx]    **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to report the smu alarms    **
 **                    snapshots.                                             **
 **                                                                           **
 ******************************************************************************/
void RR_GetResetReasons_SmuAlrmGrp4Snap(uint8 snap_indx, uint32 *smu_alrmgrp4_snap)
{
  *smu_alrmgrp4_snap = RR_ResetReasonsDiagData.SmuAlarmGroup4Snap[snap_indx];
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void RR_GetResetReasons_SmuAlrmGrp5Snap(uint8,uint32*) **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in) : snap_indx: snapshot index to be returned [0,..,4]       **
 **                                                                           **
 ** Parameters (out)  : safe_rst_errid_snap: variable pointer to store        **
 **                                          SmuAlarmGroup5Snap[snap_indx]    **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to report the smu alarms    **
 **                    snapshots.                                             **
 **                                                                           **
 ******************************************************************************/
void RR_GetResetReasons_SmuAlrmGrp5Snap(uint8 snap_indx, uint32 *smu_alrmgrp5_snap)
{
  *smu_alrmgrp5_snap = RR_ResetReasonsDiagData.SmuAlarmGroup5Snap[snap_indx];
}

/*******************************************************************************
 **                                                                           **
 ** Syntax           : void RR_GetResetReasons_SmuAlrmGrp6Snap(uint8,uint32*) **
 **                                                                           **
 ** Service ID       : none                                                   **
 **                                                                           **
 ** Sync/Async       : Synchronous                                            **
 **                                                                           **
 ** Reentrancy       : Non-reentrant                                          **
 **                                                                           **
 ** Parameters (in) : snap_indx: snapshot index to be returned [0,..,4]       **
 **                                                                           **
 ** Parameters (out)  : safe_rst_errid_snap: variable pointer to store        **
 **                                          SmuAlarmGroup6Snap[snap_indx]    **
 **                                                                           **
 ** Return value     : None                                                   **
 **                                                                           **
 ** Description      : The purpose of this API is to report the smu alarms    **
 **                    snapshots.                                             **
 **                                                                           **
 ******************************************************************************/
void RR_GetResetReasons_SmuAlrmGrp6Snap(uint8 snap_indx, uint32 *smu_alrmgrp6_snap)
{
  *smu_alrmgrp6_snap = RR_ResetReasonsDiagData.SmuAlarmGroup6Snap[snap_indx];
}




static void RR_StoreResetReasons(uint32 reset_reasons, uint8 watchdog_reset_counter, uint8 safety_reset_counter, uint8 safety_reset_errid)
{
  uint8 index;

  /* - DID FD07: total number of resets since last battery reset */
  RR_ResetReasonsDiagData.TotalNumberOfResets += 1u;

  /* - DID FD07: watchdog reset counter */
  RR_ResetReasonsDiagData.WatchdogResetCounter = watchdog_reset_counter;

  /* - DID FD07: safety reset counter */
  RR_ResetReasonsDiagData.SafetyResetCounter = safety_reset_counter;

  /* - DID FD08: reset reasons from the last 5 resets performed */
  for(index = RR_MAX_SNAPS_TO_STORE - 1u; index >= 1u; index--)
  {
      RR_ResetReasonsDiagData.ResetReasonsSnap[index] = RR_ResetReasonsDiagData.ResetReasonsSnap[index - 1u];
  }
  RR_ResetReasonsDiagData.ResetReasonsSnap[0u] = reset_reasons;
  
  /* - DID FD09: Safety reset error id snapshot from the last 5 resets performed */
  for(index = RR_MAX_SNAPS_TO_STORE - 1u; index >= 1u; index--)
  {
      RR_ResetReasonsDiagData.SafetyResetErrorIdSnap[index] = RR_ResetReasonsDiagData.SafetyResetErrorIdSnap[index - 1u];
  }
  RR_ResetReasonsDiagData.SafetyResetErrorIdSnap[0u] = safety_reset_errid;

  /* - DID FD0A-FD10: Smu alarm groupX snapshot from the last 5 resets performed */
  for(index = RR_MAX_SNAPS_TO_STORE - 1u; index >= 1u; index--)
  {
      RR_ResetReasonsDiagData.SmuAlarmGroup0Snap[index] = RR_ResetReasonsDiagData.SmuAlarmGroup0Snap[index - 1u];
      RR_ResetReasonsDiagData.SmuAlarmGroup1Snap[index] = RR_ResetReasonsDiagData.SmuAlarmGroup1Snap[index - 1u];
      RR_ResetReasonsDiagData.SmuAlarmGroup2Snap[index] = RR_ResetReasonsDiagData.SmuAlarmGroup2Snap[index - 1u];
      RR_ResetReasonsDiagData.SmuAlarmGroup3Snap[index] = RR_ResetReasonsDiagData.SmuAlarmGroup3Snap[index - 1u];
      RR_ResetReasonsDiagData.SmuAlarmGroup4Snap[index] = RR_ResetReasonsDiagData.SmuAlarmGroup4Snap[index - 1u];
      RR_ResetReasonsDiagData.SmuAlarmGroup5Snap[index] = RR_ResetReasonsDiagData.SmuAlarmGroup5Snap[index - 1u];
      RR_ResetReasonsDiagData.SmuAlarmGroup6Snap[index] = RR_ResetReasonsDiagData.SmuAlarmGroup6Snap[index - 1u];
  }
  MCU_SFR_RUNTIME_RESETSAFETYENDINIT_TIMED(MCU_SAFETY_ENDINT_TIMEOUT);
  RR_ResetReasonsDiagData.SmuAlarmGroup0Snap[0u] = MCU_SFR_RUNTIME_USER_MODE_READ32(SMU_AD0.U);
  RR_ResetReasonsDiagData.SmuAlarmGroup1Snap[0u] = MCU_SFR_RUNTIME_USER_MODE_READ32(SMU_AD1.U);
  RR_ResetReasonsDiagData.SmuAlarmGroup2Snap[0u] = MCU_SFR_RUNTIME_USER_MODE_READ32(SMU_AD2.U);
  RR_ResetReasonsDiagData.SmuAlarmGroup3Snap[0u] = MCU_SFR_RUNTIME_USER_MODE_READ32(SMU_AD3.U);
  RR_ResetReasonsDiagData.SmuAlarmGroup4Snap[0u] = MCU_SFR_RUNTIME_USER_MODE_READ32(SMU_AD4.U);
  RR_ResetReasonsDiagData.SmuAlarmGroup5Snap[0u] = MCU_SFR_RUNTIME_USER_MODE_READ32(SMU_AD5.U);
  RR_ResetReasonsDiagData.SmuAlarmGroup6Snap[0u] = MCU_SFR_RUNTIME_USER_MODE_READ32(SMU_AD6.U);
  MCU_SFR_RUNTIME_SETSAFETYENDINIT_TIMED();
}

static void Pre_run_ResetSMUAlarms ( void )
{
	uint8	smuAlarmGroup;
	uint8	smuAlarmPos;
	uint32	smuAlarmStatus;
	Std_ReturnType smuAlarmResult = (Std_ReturnType) E_NOT_OK;

	for ( smuAlarmGroup = SMU_ALARM_GROUP0 ; smuAlarmGroup <= SMU_ALARM_GROUP6 ; smuAlarmGroup++ )
	{
		smuAlarmStatus = 0x00U;
		
		/* Get alarm group status */
		smuAlarmResult = Smu_GetAlarmStatus( smuAlarmGroup, &smuAlarmStatus );

		if ( ( E_OK == smuAlarmResult ) && ( smuAlarmStatus != 0x00U ) )
		{ /* One or more alarms are already set */
				

			/* Clear alarms */
			for ( smuAlarmPos = SMU_ALARM_0; ( smuAlarmPos <= SMU_ALARM_31 ) && ( E_OK == smuAlarmResult ); smuAlarmPos++ )
			{
				if ( (smuAlarmStatus & ( ( (uint32)0x01U << smuAlarmPos) ) ) != 0x00U )
				{   /* Alarm is set, clear it */
					smuAlarmResult = Smu_ClearAlarmStatus( smuAlarmGroup, smuAlarmPos );
				}
			} /* END of for (smuAlarmPos = SMU_ALARM_0; (smuAlarmPos <= SMU_ALARM_31) && (E_OK == smuAlarmResult); smuAlarmPos++) */
		} /* END of if ((E_OK == smuAlarmResult) && (smuAlarmStatus != 0x00U)) */
		
		if ( E_OK != smuAlarmResult )
		{	/* Error occurred, break the loop */
	
			//Notify Errorhandler function if required
			break;
		} else { /* MISRA else */ }
	} /* END of for (smuAlarmGroup = SMU_ALARM_GROUP0; smuAlarmGroup <= SMU_ALARM_GROUP6; smuAlarmGroup++) */
}

#define IFX_APPL_STOP_SEC_CODE_ASIL_B
#include "Ifx_MemMap.h"
