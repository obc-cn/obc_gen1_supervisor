/* Vector Informatik 2016 */
/* This is a template file for application callouts, necessary for Infineon TC2xx MCAL */
#include "Os.h"
#include "Std_Types.h"
#include "AppCbk.h"

extern void BrsHwUnlockInit(void);
extern void BrsHwLockInit(void);

/*******************************************************************************
** Syntax           : void Mcal_SafeErrorHandler(uint32 ErrorType)            **
**                                                                            **
** Service ID       : None                                                    **
**                                                                            **
** Sync/Async       : Synchronous                                             **
**                                                                            **
** Reentrancy       : Reentrant                                               **
**                                                                            **
** Parameters(in)   : ErrorType                                               **
**                                                                            **
** Parameters (out) : None                                                    **
**                                                                            **
** Return value     : None                                                    **
**                                                                            **
** Description      : This is an Error handler function which can be called   **
                      in two conditions                                       **
** 1. Any detection of inconsistency in copies of relevant global variables   **
**    is reported to the user via the function Mcal_SafeErrorHandler.         **
**    Mcal_SafeErrorHandler shall never return, else it may lead to           **
**    unpredictable behavior.The user is expected to perform the error        **
**    handling in this function.                                              **
**                                                                            **
**    The error reported is MCAL_DIV_INCONSISTENCY:Typical error reactions    **
**    could be to reset the microcontroller (software reset)                  **
**                                                                            **
** 2. Support functions are called to access the protected registers,         **
**    these functions can be called from multiple cores concurrently, a       **
**    spinlock mechanism with timeout is provided. In case, the timeout       **
**    expires, it is reported to the user via Mcal_SafeErrorHandler.          **
**    The user is expected to perform the error handling in this function.    **
**                                                                            **
**    The error reported is MCAL_SPINLOCK_TIMEOUT:Typical error reactions     **
**    could be to the terminate the operating system task or restart the      **
**    partition or reset the microcontroller.                                 **
**                                                                            **
**    This funtion is written here only to avoid build error.                 **
**    The user needs to implement this functionality (Mcal_SafeErrorHandler)  **
**    in a separate file and needs to add this file as part of the build.     **
**                                                                            **
*******************************************************************************/
void Mcal_SafeErrorHandler(uint32 ErrorType)
{
  /* Manage SafeMcal General errors as a SafeTlib test failed */
  AppCbk_ErrorHandler(APLCBK_MCAL_GENERAL_FAIL);
}

void OS_WritePeripheral32(uint32 Area, volatile uint32* Address, uint32 value)
{
  //Os_WritePeripheral32(OsPeripheralRegion_MCAL, Address, value);
}

uint32 OS_ReadPeripheral32(uint32 Area, volatile uint32* Address)
{
  //return Os_ReadPeripheral32(OsPeripheralRegion_MCAL, Address);
}

void OS_ModifyPeripheral32(uint32 area,volatile uint32* address, uint32 clearmask, uint32  setmask)
{
 // Os_ModifyPeripheral32(OsPeripheralRegion_MCAL, address, clearmask, setmask);
}

void TRUSTED_ApplSetEndInit(void)
{
  BrsHwLockInit();
}

void TRUSTED_ApplResetEndInit(void)
{
  BrsHwUnlockInit();
}