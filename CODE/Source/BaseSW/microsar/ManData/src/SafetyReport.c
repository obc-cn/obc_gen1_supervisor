/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : SafetyReport.c $                                           **
**                                                                           **
**  $CC VERSION : \main\2 $                                                  **
**                                                                           **
**  $DATE       : 2013-06-10 $                                               **
**                                                                           **
**  AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                           **
**  VENDOR      : Infineon Technologies                                      **
**                                                                           **
**  DESCRIPTION : This file contains implementation of the reporting         **
**                mechanism for safety checks.                               **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: Yes                                     **
**                                                                           **
******************************************************************************/
#include "SafetyReport.h"
#include "AppCbk.h"


void SafeMcal_ReportError (uint16 ModuleId,uint8 InstanceId,
                                                  uint8 ApiId,uint8 ErrorId)
{
  /* Manage SafeMcal Safe check errors as a SafeTlib test failed */
  AppCbk_ErrorHandler(APLCBK_MCAL_SAFECHECK_FAIL);
}



