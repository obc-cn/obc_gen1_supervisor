/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_MemoryProtection_Lcfg.c
 *   Generation Time: 2020-08-19 13:07:50
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

/* PRQA S 0777, 0779, 0828 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2, MD_MSR_Dir1.1 */

#define OS_MEMORYROTECTION_LCFG_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
#include "Std_Types.h"

/* Os module declarations */
#include "Os_MemoryProtection_Lcfg.h"
#include "Os_MemoryProtection.h"

/* Os kernel module dependencies */

/* Os hal dependencies */
#include "Os_Hal_MemoryProtection_Lcfg.h"


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA
 *********************************************************************************************************************/

#define OS_START_SEC_CORE0_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Memory protection configuration data: OsCore0 */
CONST(Os_MpCoreConfigType, OS_CONST) OsCfg_Mp_OsCore0 =
{
  /* .HwConfig = */ &OsCfg_Hal_Mp_OsCore0
};

/*! Memory protection configuration data: OsApplication_ASILB */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_OsApplication_ASILB =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_OsApplication_ASILB,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_EmptyThread
};

/*! Memory protection configuration data: OsApplication_BSW_QM */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_OsApplication_BSW_QM =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_OsApplication_BSW_QM,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_EmptyThread
};

/*! Memory protection configuration data: OsApplication_QM */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_OsApplication_QM =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_OsApplication_QM,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_EmptyThread
};

/*! Memory protection configuration data: SystemApplication_OsCore0 */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_SystemApplication_OsCore0 =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_EmptyThread
};

/*! Memory protection configuration data: CanIsr_1 */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_CanIsr_1 =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_CanIsr_1
};

/*! Memory protection configuration data: CanIsr_2 */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_CanIsr_2 =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_CanIsr_2
};

/*! Memory protection configuration data: CounterIsr_SystemTimer */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_CounterIsr_SystemTimer =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_CounterIsr_SystemTimer
};

/*! Memory protection configuration data: DMACH10SR_ISR */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_DMACH10SR_ISR =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_DMACH10SR_ISR
};

/*! Memory protection configuration data: DMACH11SR_ISR */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_DMACH11SR_ISR =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_DMACH11SR_ISR
};

/*! Memory protection configuration data: GTMTOM1SR0_ISR */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_GTMTOM1SR0_ISR =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_GTMTOM1SR0_ISR
};

/*! Memory protection configuration data: PLC_Interrupt */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_PLC_Interrupt =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_PLC_Interrupt
};

/*! Memory protection configuration data: QSPI1ERR_ISR */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_QSPI1ERR_ISR =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_QSPI1ERR_ISR
};

/*! Memory protection configuration data: QSPI1PT_ISR */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_QSPI1PT_ISR =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_QSPI1PT_ISR
};

/*! Memory protection configuration data: QSPI1UD_ISR */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_QSPI1UD_ISR =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_QSPI1UD_ISR
};

/*! Memory protection configuration data: ASILB_MAIN_TASK */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_ASILB_MAIN_TASK =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_OsApplication_ASILB,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_ASILB_MAIN_TASK
};

/*! Memory protection configuration data: ASILB_init_task */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_ASILB_init_task =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_OsApplication_ASILB,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_ASILB_init_task
};

/*! Memory protection configuration data: Default_BSW_Async_QM_Task */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_Default_BSW_Async_QM_Task =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_OsApplication_BSW_QM,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_Default_BSW_Async_QM_Task
};

/*! Memory protection configuration data: Default_BSW_Async_Task */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_Default_BSW_Async_Task =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_Default_BSW_Async_Task
};

/*! Memory protection configuration data: Default_BSW_Sync_Task */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_Default_BSW_Sync_Task =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_Default_BSW_Sync_Task
};

/*! Memory protection configuration data: Default_Init_Task */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_Default_Init_Task =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_Default_Init_Task
};

/*! Memory protection configuration data: Default_RTE_Mode_switch_Task */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_Default_RTE_Mode_switch_Task =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_Default_RTE_Mode_switch_Task
};

/*! Memory protection configuration data: IdleTask_OsCore0 */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_IdleTask_OsCore0 =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_SystemApplication_OsCore0,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_IdleTask_OsCore0
};

/*! Memory protection configuration data: QM_MAIN_TASK */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_QM_MAIN_TASK =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_OsApplication_QM,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_QM_MAIN_TASK
};

/*! Memory protection configuration data: QM_init_task */
CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_QM_init_task =
{
  /* .AppAccessRights    = */ &OsCfg_Hal_Mp_OsApplication_QM,
  /* .ThreadAccessRights = */ &OsCfg_Hal_Mp_QM_init_task
};

#define OS_STOP_SEC_CORE0_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#define OS_START_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Memory protection configuration data: SystemMpu */
CONST(Os_MpSystemConfigType, OS_CONST) OsCfg_Mp_SystemMpu =
{
  /* .HwConfig = */ &OsCfg_Hal_Mp_SystemMpu
};

#define OS_STOP_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  END OF FILE: Os_MemoryProtection_Lcfg.c
 *********************************************************************************************************************/
