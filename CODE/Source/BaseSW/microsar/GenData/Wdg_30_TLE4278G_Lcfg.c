/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Wdg_30_TLE4278G
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Wdg_30_TLE4278G_Lcfg.c
 *   Generation Time: 2020-08-19 13:07:48
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#define WDG_30_TLE4278G_CFG_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Wdg_30_TLE4278G_Cfg.h"


/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/
#define WDG_30_TLE4278G_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**
  \var    Wdg_30_TLE4278G_Runtime
  \brief  Stores configuration(s) for hardware watchdog.
  \details
  Element                  Description
  EModeFailed              DEM event which shall be issued when setting a watchdog mode fails.
  EDisableRejected         DEM event which shall be issued when mode switch fails because it would disable the watchdog.
  DefaultMode              DefaultMode for watchdog driver initialization.
  DioPinWdiChannel         Dio Channel that is connected to pin WDI of device.
  DioPinWdiInitLevel       Level which is set at DioPinWdiChannel during initialization.
  TriggerTimer             Gpt Channel used for trigger timing.
  TriggerTimerCounter      Trigger cycle (in Gpt ticks)
  TriggerCycleDuration     Trigger cycle (in ms)
*/
CONST(Wdg_30_TLE4278G_ConfigType, WDG_30_TLE4278G_CONST) Wdg_30_TLE4278G_Runtime[1] =
{
  {
    DemConf_DemEventParameter_DTC_dummy2 /*  EModeFailed  */ , 
    DemConf_DemEventParameter_DTC_dummy1 /*  EDisableRejected  */ , 
    WDGIF_FAST_MODE /*  DefaultMode  */ , 
    DioConf_DioChannel_SUP_DO_WDI /*  DioPin SUP_DO_WDI  */ , 
    STD_HIGH, 
    { /*  WDGIF_FAST_MODE  */ 
      {
        DioConf_DioChannel_SUP_DO_WDI, 
        STD_HIGH
      }
    }, 
    { /*  WDGIF_SLOW_MODE  */ 
      {
        DioConf_DioChannel_SUP_DO_WDI, 
        STD_HIGH
      }
    }, 
    { /*  WDGIF_OFF_MODE  */ 
      {
        DioConf_DioChannel_SUP_DO_WDI, 
        STD_HIGH
      }
    }, 
    GptConf_GptChannelConfiguration_GptChannelConfiguration_0 /*  TriggerTimer  */ , 
    { /*  TriggerTimerCounter  */ 
      242uL /*  (for WDGIF_SLOW_MODE)  */ , 
      242uL /*  (for WDGIF_FAST_MODE)  */ 
    }, 
    { /*  RecoverTimerCounter  */ 
      1uL /*  (for WDGIF_SLOW_MODE)  */ , 
      1uL /*  (for WDGIF_FAST_MODE)  */ 
    }, 
    { /*  TriggerCycleDuration (Average)  */ 
      5uL /*  (WDGIF_SLOW_MODE)  */ , 
      5uL /*  (WDGIF_FAST_MODE)  */ 
    }
  } /*  WdgSettingsConfig  */ 
};


#define WDG_30_TLE4278G_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
