/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: EthIf
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: EthIf_Cfg.h
 *   Generation Time: 2020-08-19 13:07:44
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/


#if !defined (ETHIF_CFG_H)
# define ETHIF_CFG_H

/**********************************************************************************************************************
 *  INCLUDE
 *********************************************************************************************************************/
# include "Std_Types.h"

/**********************************************************************************************************************
 *  GENERAL DEFINES
 *********************************************************************************************************************/
#ifndef ETHIF_USE_DUMMY_STATEMENT
#define ETHIF_USE_DUMMY_STATEMENT STD_ON /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef ETHIF_DUMMY_STATEMENT
#define ETHIF_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef ETHIF_DUMMY_STATEMENT_CONST
#define ETHIF_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef ETHIF_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define ETHIF_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef ETHIF_ATOMIC_VARIABLE_ACCESS
#define ETHIF_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef ETHIF_PROCESSOR_TC234
#define ETHIF_PROCESSOR_TC234
#endif
#ifndef ETHIF_COMP_GNU
#define ETHIF_COMP_GNU
#endif
#ifndef ETHIF_GEN_GENERATOR_MSR
#define ETHIF_GEN_GENERATOR_MSR
#endif
#ifndef ETHIF_CPUTYPE_BITORDER_LSB2MSB
#define ETHIF_CPUTYPE_BITORDER_LSB2MSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef ETHIF_CONFIGURATION_VARIANT_PRECOMPILE
#define ETHIF_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef ETHIF_CONFIGURATION_VARIANT_LINKTIME
#define ETHIF_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef ETHIF_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define ETHIF_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef ETHIF_CONFIGURATION_VARIANT
#define ETHIF_CONFIGURATION_VARIANT ETHIF_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef ETHIF_POSTBUILD_VARIANT_SUPPORT
#define ETHIF_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


/**********************************************************************************************************************
 *  SNVS (SYMBOLIC NAME DEFINES)
 *********************************************************************************************************************/
#define EthIfConf_EthIfController_EthIfController (0uL)
#define EthIfConf_EthIfFrameOwnerConfig_TcpIp_IPv6 (0uL)
#define EthIfConf_EthIfFrameOwnerConfig_EthTrcv_Ar7000_MMEType (1uL)

/**********************************************************************************************************************
 *  FEATURE SWITCHES
 *********************************************************************************************************************/
# define ETHIF_DEV_ERROR_DETECT                 (STD_OFF)
# define ETHIF_DEV_ERROR_REPORT                 (STD_OFF)
# define ETHIF_ENABLE_RX_INTERRUPT              (STD_ON)
# define ETHIF_ENABLE_TX_INTERRUPT              (STD_ON)
# define ETHIF_VERSION_INFO_API                 (STD_OFF)
# define ETHIF_ENABLE_UPDATE_PHYS_ADDR_FILTER   (STD_OFF)
# define ETHIF_ENABLE_MAIN_FUNCTION_STATE       (STD_ON)
# define ETHIF_ENABLE_ZERO_COPY_EXTENSIONS      (STD_OFF)
# define ETHIF_ENABLE_HEADER_ACCESS_API         (STD_OFF)
# define ETHIF_GLOBAL_TIME_SUPPORT              (STD_OFF)
# define ETHIF_WAKEUP_SUPPORT                   (STD_OFF)
# define ETHIF_DYNAMIC_TRAFFIC_SHAPING_SUPPORT  (STD_OFF)
# define ETHIF_SWT_PORT_GROUP_MODE_REQ_SUPPORT  (STD_OFF)
# define ETHIF_FIREWALL_SUPPORT                 (STD_OFF)
# define ETHIF_EXTENDED_TRAFFIC_HANDLING_TYPE   ETHIF_NO_EXTENDED_TRAFFIC_HANDLING


/**********************************************************************************************************************
 *  ETHERNET TRANSCEIVER DEFINES
 *********************************************************************************************************************/
# define ETHIF_ETHTRCV_USED                        (STD_ON)

/**********************************************************************************************************************
 *  ETHERNET SWITCH DEFINES
 *********************************************************************************************************************/
# define ETHIF_ETHSWT_USED                         (STD_OFF)
# define ETHIF_STORE_CONFIG_API                    (STD_OFF)
# define ETHIF_RESET_CONFIG_API                    (STD_OFF)
# define ETHIF_GET_DROP_COUNT_API                  (STD_OFF)
# define ETHIF_GET_BUFFER_LEVEL_API                (STD_OFF)
# define ETHIF_GET_ARL_TABLE_API                   (STD_OFF)
# define ETHIF_GET_PORT_MAC_ADDR_API               (STD_OFF)
# define ETHIF_SWITCH_UPDATE_MCAST_PORT_ASSIGN_API (STD_OFF)
# define ETHIF_ETHSWT_FRAME_MANAGEMENT_SUPPORT     (STD_OFF)
# define ETHIF_ETHSWT_TIME_STAMP_SUPPORT           (STD_OFF)
# define ETHIF_SWITCH_ENABLE_VLAN_API              (STD_OFF)


#endif /* ETHIF_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: EthIf_Cfg.h
 *********************************************************************************************************************/


