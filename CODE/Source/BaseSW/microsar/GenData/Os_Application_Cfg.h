/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Application_Cfg.h
 *   Generation Time: 2020-08-19 13:07:48
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#ifndef OS_APPLICATION_CFG_H 
# define OS_APPLICATION_CFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
# include "Std_Types.h"


/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* Number of application objects: OsApplication_ASILB */
# define OS_CFG_NUM_APP_OSAPPLICATION_ASILB_ALARMS             (7uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_ASILB_COUNTERS           (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_ASILB_HOOKS              (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_ASILB_ISRS               (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_ASILB_CAT1ISRS           (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_ASILB_SCHTS              (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_ASILB_TASKS              (2uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_ASILB_SERVICES           (0uL)

/* Number of application objects: OsApplication_BSW_QM */
# define OS_CFG_NUM_APP_OSAPPLICATION_BSW_QM_ALARMS             (3uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_BSW_QM_COUNTERS           (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_BSW_QM_HOOKS              (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_BSW_QM_ISRS               (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_BSW_QM_CAT1ISRS           (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_BSW_QM_SCHTS              (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_BSW_QM_TASKS              (1uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_BSW_QM_SERVICES           (0uL)

/* Number of application objects: OsApplication_QM */
# define OS_CFG_NUM_APP_OSAPPLICATION_QM_ALARMS             (5uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_QM_COUNTERS           (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_QM_HOOKS              (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_QM_ISRS               (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_QM_CAT1ISRS           (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_QM_SCHTS              (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_QM_TASKS              (2uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_QM_SERVICES           (0uL)

/* Number of application objects: SystemApplication_OsCore0 */
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_ALARMS             (1uL)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_COUNTERS           (1uL)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_HOOKS              (0uL)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_ISRS               (10uL)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_CAT1ISRS           (0uL)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_SCHTS              (0uL)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_TASKS              (5uL)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_SERVICES           (0uL)


/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/


#endif /* OS_APPLICATION_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Application_Cfg.h
 *********************************************************************************************************************/
