/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Compiler_Cfg.h
 *   Generation Time: 2020-08-19 13:07:48
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#ifndef OS_COMPILER_CFG_H
# define OS_COMPILER_CFG_H

/**********************************************************************************************************************
 *  OS USER CALLOUT CODE SECTIONS
 *********************************************************************************************************************/

# define OS_ASILB_MAIN_TASK_CODE
# define OS_ASILB_INIT_TASK_CODE
# define OS_CANISR_1_CODE
# define OS_CANISR_2_CODE
# define OS_DMACH10SR_ISR_CODE
# define OS_DMACH11SR_ISR_CODE
# define OS_DEFAULT_BSW_ASYNC_QM_TASK_CODE
# define OS_DEFAULT_BSW_ASYNC_TASK_CODE
# define OS_DEFAULT_BSW_SYNC_TASK_CODE
# define OS_DEFAULT_INIT_TASK_CODE
# define OS_DEFAULT_RTE_MODE_SWITCH_TASK_CODE
# define OS_GTMTOM1SR0_ISR_CODE
# define OS_QM_MAIN_TASK_CODE
# define OS_QM_INIT_TASK_CODE
# define OS_QSPI1ERR_ISR_CODE
# define OS_QSPI1PT_ISR_CODE
# define OS_QSPI1UD_ISR_CODE
# define OS_SCUERUSR0_ISR_CODE


#endif /* OS_COMPILER_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Compiler_Cfg.h
 *********************************************************************************************************************/
 
