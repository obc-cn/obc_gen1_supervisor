/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApPLT.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApPLT.h"
#include "TSC_CtApPLT.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApPLT_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
{
  return Rte_Read_PpBatteryVolt_DeBatteryVolt(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Read_PpDutyControlPilot_DeDutyControlPilot(IdtDutyControlPilot *data)
{
  return Rte_Read_PpDutyControlPilot_DeDutyControlPilot(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
{
  return Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data)
{
  return Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
{
  return Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(PFC_VPH23_Peak_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(PFC_VPH31_Peak_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
{
  return Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
{
  return Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(VCU_PosShuntJDD *data)
{
  return Rte_Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(IdtProximityDetectPhysicalValue *data)
{
  return Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
{
  return Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(data);
}




Std_ReturnType TSC_CtApPLT_Rte_Write_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(IdtELockSetPoint data)
{
  return Rte_Write_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeChargeError(boolean data)
{
  return Rte_Write_PpLedModes_PlantMode_DeChargeError(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeChargeInProgress(boolean data)
{
  return Rte_Write_PpLedModes_PlantMode_DeChargeInProgress(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeEndOfCharge(boolean data)
{
  return Rte_Write_PpLedModes_PlantMode_DeEndOfCharge(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeGuideManagement(boolean data)
{
  return Rte_Write_PpLedModes_PlantMode_DeGuideManagement(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeProgrammingCharge(boolean data)
{
  return Rte_Write_PpLedModes_PlantMode_DeProgrammingCharge(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Write_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(boolean data)
{
  return Rte_Write_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Write_PpPlantModeState_DePlantModeState(boolean data)
{
  return Rte_Write_PpPlantModeState_DePlantModeState(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result(IdtPlantModeTestInfoDID_Result data)
{
  return Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State(IdtPlantModeTestInfoDID_State data)
{
  return Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase(IdtPlantModeTestInfoDID_Test data)
{
  return Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines(IdtPlantModeTestInfoDID_Test data)
{
  return Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty(IdtPlantModeTestInfoDID_Test data)
{
  return Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage(IdtPlantModeTestInfoDID_Test data)
{
  return Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage(data);
}

Std_ReturnType TSC_CtApPLT_Rte_Write_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(IdtRECHARGE_HMI_STATE data)
{
  return Rte_Write_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApPLT_Rte_Call_PpPlantModeDTC_OpPlantModeDTCReset(IdtPlantModeDTCNumber DTC_Id)
{
  return Rte_Call_PpPlantModeDTC_OpPlantModeDTCReset(DTC_Id);
}
Std_ReturnType TSC_CtApPLT_Rte_Call_PpPlantModeDTC_OpPlantModeDTCSet(IdtPlantModeDTCNumber DTC_Id)
{
  return Rte_Call_PpPlantModeDTC_OpPlantModeDTCSet(DTC_Id);
}
Std_ReturnType TSC_CtApPLT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}


     /* Service calls */
Std_ReturnType TSC_CtApPLT_Rte_Call_PS_CpApPLT_PLTNvMNeed_SetRamBlockStatus(boolean RamBlockStatus)
{
  return Rte_Call_PS_CpApPLT_PLTNvMNeed_SetRamBlockStatus(RamBlockStatus);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





IdtBatteryVoltMaxTest  TSC_CtApPLT_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(void)
{
  return (IdtBatteryVoltMaxTest ) Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest();
}
IdtBatteryVoltMinTest  TSC_CtApPLT_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(void)
{
  return (IdtBatteryVoltMinTest ) Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest();
}
IdtVehStopMaxTest  TSC_CtApPLT_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(void)
{
  return (IdtVehStopMaxTest ) Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest();
}

IdtGlobalTimeoutPlantMode  TSC_CtApPLT_Rte_CData_CalGlobalTimeoutPlantMode(void)
{
  return (IdtGlobalTimeoutPlantMode ) Rte_CData_CalGlobalTimeoutPlantMode();
}
IdtProximityDetectPlantMode  TSC_CtApPLT_Rte_CData_CalMaxProximityDetectPlantMode(void)
{
  return (IdtProximityDetectPlantMode ) Rte_CData_CalMaxProximityDetectPlantMode();
}
IdtProximityDetectPlantMode  TSC_CtApPLT_Rte_CData_CalMinProximityDetectPlantMode(void)
{
  return (IdtProximityDetectPlantMode ) Rte_CData_CalMinProximityDetectPlantMode();
}
IdtProximityVoltageStartingPlantMode  TSC_CtApPLT_Rte_CData_CalProximityVoltageStartingPlantMode(void)
{
  return (IdtProximityVoltageStartingPlantMode ) Rte_CData_CalProximityVoltageStartingPlantMode();
}
IdtDebounceControlPilotPlantMode  TSC_CtApPLT_Rte_CData_CalDebounceControlPilotPlantMode(void)
{
  return (IdtDebounceControlPilotPlantMode ) Rte_CData_CalDebounceControlPilotPlantMode();
}
IdtDebounceDCRelayVoltagePlantMode  TSC_CtApPLT_Rte_CData_CalDebounceDCRelayVoltagePlantMode(void)
{
  return (IdtDebounceDCRelayVoltagePlantMode ) Rte_CData_CalDebounceDCRelayVoltagePlantMode();
}
IdtDebouncePhasePlantMode  TSC_CtApPLT_Rte_CData_CalDebouncePhasePlantMode(void)
{
  return (IdtDebouncePhasePlantMode ) Rte_CData_CalDebouncePhasePlantMode();
}
IdtDebounceProximityPlantMode  TSC_CtApPLT_Rte_CData_CalDebounceProximityPlantMode(void)
{
  return (IdtDebounceProximityPlantMode ) Rte_CData_CalDebounceProximityPlantMode();
}
IdtControlPilotDutyPlantMode  TSC_CtApPLT_Rte_CData_CalMaxControlPilotDutyPlantMode(void)
{
  return (IdtControlPilotDutyPlantMode ) Rte_CData_CalMaxControlPilotDutyPlantMode();
}
IdtDCRelayVoltagePlantMode  TSC_CtApPLT_Rte_CData_CalMaxDCRelayVoltagePlantMode(void)
{
  return (IdtDCRelayVoltagePlantMode ) Rte_CData_CalMaxDCRelayVoltagePlantMode();
}
IdtPhasePlantmode  TSC_CtApPLT_Rte_CData_CalMaxPhasePlantMode(void)
{
  return (IdtPhasePlantmode ) Rte_CData_CalMaxPhasePlantMode();
}
IdtMaxTimePlantMode  TSC_CtApPLT_Rte_CData_CalMaxTimePlantMode(void)
{
  return (IdtMaxTimePlantMode ) Rte_CData_CalMaxTimePlantMode();
}
IdtControlPilotDutyPlantMode  TSC_CtApPLT_Rte_CData_CalMinControlPilotDutyPlantMode(void)
{
  return (IdtControlPilotDutyPlantMode ) Rte_CData_CalMinControlPilotDutyPlantMode();
}
IdtDCRelayVoltagePlantMode  TSC_CtApPLT_Rte_CData_CalMinDCRelayVoltagePlantMode(void)
{
  return (IdtDCRelayVoltagePlantMode ) Rte_CData_CalMinDCRelayVoltagePlantMode();
}
IdtPhasePlantmode  TSC_CtApPLT_Rte_CData_CalMinPhasePlantMode(void)
{
  return (IdtPhasePlantmode ) Rte_CData_CalMinPhasePlantMode();
}
boolean  TSC_CtApPLT_Rte_CData_CalInhibitControlPilotDetection(void)
{
  return (boolean ) Rte_CData_CalInhibitControlPilotDetection();
}
boolean  TSC_CtApPLT_Rte_CData_CalInhibitDCRelayVoltage(void)
{
  return (boolean ) Rte_CData_CalInhibitDCRelayVoltage();
}
boolean  TSC_CtApPLT_Rte_CData_CalInhibitPhaseVoltageDetection(void)
{
  return (boolean ) Rte_CData_CalInhibitPhaseVoltageDetection();
}
boolean  TSC_CtApPLT_Rte_CData_CalInhibitProximityDetection(void)
{
  return (boolean ) Rte_CData_CalInhibitProximityDetection();
}
IdtPlantModeTestUTPlugin * TSC_CtApPLT_Rte_CData_PLTNvMNeed_DefaultValue(void)
{
  return (IdtPlantModeTestUTPlugin *) Rte_CData_PLTNvMNeed_DefaultValue();
}

     /* CtApPLT */
      /* CtApPLT */

/** Per Instance Memories */
IdtTimeoutPlantMode *TSC_CtApPLT_Rte_Pim_PimGlobalTimeoutPlantMode(void)
{
  return Rte_Pim_PimGlobalTimeoutPlantMode();
}
IdtTimeoutPlantMode *TSC_CtApPLT_Rte_Pim_PimTimeoutPlantMode(void)
{
  return Rte_Pim_PimTimeoutPlantMode();
}
boolean *TSC_CtApPLT_Rte_Pim_PimPlantModeControlPilot(void)
{
  return Rte_Pim_PimPlantModeControlPilot();
}
boolean *TSC_CtApPLT_Rte_Pim_PimPlantModeDCRelayVoltage(void)
{
  return Rte_Pim_PimPlantModeDCRelayVoltage();
}
boolean *TSC_CtApPLT_Rte_Pim_PimPlantModePhaseVoltage(void)
{
  return Rte_Pim_PimPlantModePhaseVoltage();
}
boolean *TSC_CtApPLT_Rte_Pim_PimPlantModeProximity(void)
{
  return Rte_Pim_PimPlantModeProximity();
}
Rte_DT_IdtPlantModeTestUTPlugin_0 *TSC_CtApPLT_Rte_Pim_PimPlantModeTestUTPlugin(void)
{
  return Rte_Pim_PimPlantModeTestUTPlugin();
}



