/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApDCH.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApDCH.h"
#include "TSC_CtApDCH.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDCH_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
{
  return Rte_Read_PpBatteryVolt_DeBatteryVolt(data);
}

Std_ReturnType TSC_CtApDCH_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC(data);
}

Std_ReturnType TSC_CtApDCH_Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(DCDC_Status *data)
{
  return Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(data);
}

Std_ReturnType TSC_CtApDCH_Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
{
  return Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(data);
}

Std_ReturnType TSC_CtApDCH_Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
{
  return Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(data);
}

Std_ReturnType TSC_CtApDCH_Rte_Read_PpInt_OBC_ACRange_Delayed_OBC_ACRange(OBC_ACRange *data)
{
  return Rte_Read_PpInt_OBC_ACRange_Delayed_OBC_ACRange(data);
}

Std_ReturnType TSC_CtApDCH_Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status *data)
{
  return Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(data);
}

Std_ReturnType TSC_CtApDCH_Rte_Read_PpInt_PFC_PFCStatus_PFC_PFCStatus(PFC_PFCStatus *data)
{
  return Rte_Read_PpInt_PFC_PFCStatus_PFC_PFCStatus(data);
}

Std_ReturnType TSC_CtApDCH_Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(PFC_Vdclink_V *data)
{
  return Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(data);
}

Std_ReturnType TSC_CtApDCH_Rte_Read_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand(VCU_ActivedischargeCommand *data)
{
  return Rte_Read_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand(data);
}

Std_ReturnType TSC_CtApDCH_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
{
  return Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(data);
}




Std_ReturnType TSC_CtApDCH_Rte_Write_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean data)
{
  return Rte_Write_PpActiveDischargeRequest_DeActiveDischargeRequest(data);
}

Std_ReturnType TSC_CtApDCH_Rte_Write_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt(DCDC_ActivedischargeSt data)
{
  return Rte_Write_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt(data);
}

Std_ReturnType TSC_CtApDCH_Rte_Write_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(boolean data)
{
  return Rte_Write_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(data);
}

Std_ReturnType TSC_CtApDCH_Rte_Write_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue(boolean data)
{
  return Rte_Write_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtCalThresholdUVTripDischarge  TSC_CtApDCH_Rte_CData_CalThresholdUVTripDischarge(void)
{
  return (IdtCalThresholdUVTripDischarge ) Rte_CData_CalThresholdUVTripDischarge();
}
IdtCalTimeUVTripDischarge  TSC_CtApDCH_Rte_CData_CalTimeUVTripDischarge(void)
{
  return (IdtCalTimeUVTripDischarge ) Rte_CData_CalTimeUVTripDischarge();
}

     /* CtApDCH */
      /* CtApDCH */



