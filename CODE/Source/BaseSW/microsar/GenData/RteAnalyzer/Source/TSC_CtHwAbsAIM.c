/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtHwAbsAIM.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtHwAbsAIM.h"
#include "TSC_CtHwAbsAIM.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(IdtAmbientTemperaturePhysicalValue data)
{
  return Rte_Write_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue data)
{
  return Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue data)
{
  return Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue data)
{
  return Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempAC1Raw(IdtMsrTempRaw data)
{
  return Rte_Write_PpMsrTempRaw_DeMsrTempAC1Raw(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempAC2Raw(IdtMsrTempRaw data)
{
  return Rte_Write_PpMsrTempRaw_DeMsrTempAC2Raw(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempAC3Raw(IdtMsrTempRaw data)
{
  return Rte_Write_PpMsrTempRaw_DeMsrTempAC3Raw(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempACNRaw(IdtMsrTempRaw data)
{
  return Rte_Write_PpMsrTempRaw_DeMsrTempACNRaw(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempDC1Raw(IdtMsrTempRaw data)
{
  return Rte_Write_PpMsrTempRaw_DeMsrTempDC1Raw(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempDC2Raw(IdtMsrTempRaw data)
{
  return Rte_Write_PpMsrTempRaw_DeMsrTempDC2Raw(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(IdtTempSyncRectPhysicalValue data)
{
  return Rte_Write_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtHwAbsAIM_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}
Std_ReturnType TSC_CtHwAbsAIM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
{
  return Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(frameNum, byteNum, data);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt data)
{
  return Rte_Write_PpBatteryVolt_DeBatteryVolt(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpElockFeedbackCurrent_DeElockFeedbackCurrent(IdtElockFeedbackCurrent data)
{
  return Rte_Write_PpElockFeedbackCurrent_DeElockFeedbackCurrent(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpElockFeedbackLock_DeElockFeedbackLock(IdtElockFeedbackLock data)
{
  return Rte_Write_PpElockFeedbackLock_DeElockFeedbackLock(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpElockFeedbackUnlock_DeElockFeedbackUnlock(IdtElockFeedbackUnlock data)
{
  return Rte_Write_PpElockFeedbackUnlock_DeElockFeedbackUnlock(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(IdtExtOpPlugLockRaw data)
{
  return Rte_Write_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected data)
{
  return Rte_Write_PpHWEditionDetected_DeHWEditionDetected(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(IdtPlugLedFeedbackPhysicalValue data)
{
  return Rte_Write_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(IdtProximityDetectPhysicalValue data)
{
  return Rte_Write_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(data);
}





     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_CtHwAbsAIM_Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_ReadBlock(dtRef_VOID DstPtr)
{
  return Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_ReadBlock(DstPtr);
}
Std_ReturnType TSC_CtHwAbsAIM_Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr)
{
  return Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_WriteBlock(SrcPtr);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtVoltageExternalADCReference  TSC_CtHwAbsAIM_Rte_CData_CalVoltageExternalADCReferenceWave1(void)
{
  return (IdtVoltageExternalADCReference ) Rte_CData_CalVoltageExternalADCReferenceWave1();
}
IdtVoltageExternalADCReference  TSC_CtHwAbsAIM_Rte_CData_CalVoltageExternalADCReferenceWave2(void)
{
  return (IdtVoltageExternalADCReference ) Rte_CData_CalVoltageExternalADCReferenceWave2();
}
IdtDebounceADCReference  TSC_CtHwAbsAIM_Rte_CData_CalDebounceADCReference(void)
{
  return (IdtDebounceADCReference ) Rte_CData_CalDebounceADCReference();
}
IdtArrayInitAIMVoltRef * TSC_CtHwAbsAIM_Rte_CData_AIMVoltRefNvBlockNeed_DefaultValue(void)
{
  return (IdtArrayInitAIMVoltRef *) Rte_CData_AIMVoltRefNvBlockNeed_DefaultValue();
}

     /* CtHwAbsAIM */
      /* CtHwAbsAIM */

/** Per Instance Memories */
Rte_DT_IdtArrayInitAIMVoltRef_0 *TSC_CtHwAbsAIM_Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock(void)
{
  return Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock();
}



