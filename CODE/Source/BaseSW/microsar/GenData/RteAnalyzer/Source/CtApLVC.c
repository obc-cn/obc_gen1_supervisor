/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApLVC.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApLVC
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApLVC>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApLVC.h"
#include "TSC_CtApLVC.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApLVC_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DCDC_HighVoltConnectionAllowed: Boolean
 * DCDC_InputCurrent: Integer in interval [0...255]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_Applied_Derating_Factor: Integer in interval [0...2047]
 * DCLV_Input_Current: Integer in interval [0...255]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_Input_Voltage: Integer in interval [0...1000]
 *   Unit: [V], Factor: 1, Offset: 0
 * DCLV_Measured_Current: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_Measured_Voltage: Integer in interval [0...175]
 *   Unit: [V], Factor: 0.1, Offset: 4
 * DCLV_VoltageReference: Integer in interval [0...127]
 *   Unit: [V], Factor: 0.05, Offset: 10.6
 * IdtDCLVTimeConfirmDerating: Integer in interval [0...60]
 *   Unit: [s], Factor: 1, Offset: 0
 * VCU_DCDCVoltageReq: Integer in interval [0...127]
 *   Unit: [V], Factor: 0.05, Offset: 10.6
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DCDC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * DCLV_DCLVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_STANDBY (0U)
 *   Cx1_RUN (1U)
 *   Cx2_ERROR (2U)
 *   Cx3_ALARM (3U)
 *   Cx4_SAFE (4U)
 *   Cx5_DERATED (5U)
 *   Cx6_SHUTDOWN (6U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 *   POST_ONGOING (0U)
 *   POST_OK (1U)
 *   POST_NOK (2U)
 * SUP_RequestDCLVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCLV_STATE_STANDBY (0U)
 *   Cx1_DCLV_STATE_RUN (1U)
 *   Cx2_DCLV_STATE_RESET_ERROR (2U)
 *   Cx3_DCLV_STATE_DISCHARGE (3U)
 *   Cx4_DCLV_STATE_SHUTDOWN (4U)
 * VCU_DCDCActivation: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Not_used (0U)
 *   Cx1_DCDC_Off (1U)
 *   Cx2_DCDC_On (2U)
 *   Cx3_Unavailable_Value (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   boolean *Rte_Pim_PimDCLVShutdownPathFSP(void)
 *   boolean *Rte_Pim_PimDCLVShutdownPathGPIO(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   uint16 Rte_CData_CalDCLVDeratingThreshold(void)
 *   IdtDCLVTimeConfirmDerating Rte_CData_CalDCLVTimeConfirmDerating(void)
 *
 *********************************************************************************************************************/


#define CtApLVC_START_SEC_CODE
#include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLVC_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLVC_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLVC_CODE) RCtApLVC_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLVC_init
 *********************************************************************************************************************/

  boolean PimPimDCLVShutdownPathFSP;
  boolean PimPimDCLVShutdownPathGPIO;

  uint16 CalDCLVDeratingThreshold_data;
  IdtDCLVTimeConfirmDerating CalDCLVTimeConfirmDerating_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimDCLVShutdownPathFSP = *TSC_CtApLVC_Rte_Pim_PimDCLVShutdownPathFSP();
  *TSC_CtApLVC_Rte_Pim_PimDCLVShutdownPathFSP() = PimPimDCLVShutdownPathFSP;
  PimPimDCLVShutdownPathGPIO = *TSC_CtApLVC_Rte_Pim_PimDCLVShutdownPathGPIO();
  *TSC_CtApLVC_Rte_Pim_PimDCLVShutdownPathGPIO() = PimPimDCLVShutdownPathGPIO;

  CalDCLVDeratingThreshold_data = TSC_CtApLVC_Rte_CData_CalDCLVDeratingThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVTimeConfirmDerating_data = TSC_CtApLVC_Rte_CData_CalDCLVTimeConfirmDerating(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApLVC_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLVC_task10msA
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_Status_Delayed_DCDC_Status(DCDC_Status data)
 *   Std_ReturnType Rte_Write_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed(boolean data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLVC_task10msA_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLVC_CODE) RCtApLVC_task10msA(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLVC_task10msA
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  boolean PimPimDCLVShutdownPathFSP;
  boolean PimPimDCLVShutdownPathGPIO;

  uint16 CalDCLVDeratingThreshold_data;
  IdtDCLVTimeConfirmDerating CalDCLVTimeConfirmDerating_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimDCLVShutdownPathFSP = *TSC_CtApLVC_Rte_Pim_PimDCLVShutdownPathFSP();
  *TSC_CtApLVC_Rte_Pim_PimDCLVShutdownPathFSP() = PimPimDCLVShutdownPathFSP;
  PimPimDCLVShutdownPathGPIO = *TSC_CtApLVC_Rte_Pim_PimDCLVShutdownPathGPIO();
  *TSC_CtApLVC_Rte_Pim_PimDCLVShutdownPathGPIO() = PimPimDCLVShutdownPathGPIO;

  CalDCLVDeratingThreshold_data = TSC_CtApLVC_Rte_CData_CalDCLVDeratingThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVTimeConfirmDerating_data = TSC_CtApLVC_Rte_CData_CalDCLVTimeConfirmDerating(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApLVC_Rte_Write_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(Rte_InitValue_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Write_PpInt_DCDC_Status_Delayed_DCDC_Status(Rte_InitValue_PpInt_DCDC_Status_Delayed_DCDC_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Write_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed(Rte_InitValue_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLVC_task10msB
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data)
 *   Std_ReturnType Rte_Read_PpDCLVError_DeDCLVError(boolean *data)
 *   Std_ReturnType Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpImplausibilityTempBuck_DeImplausibilityTempBuck(boolean *data)
 *   Std_ReturnType Rte_Read_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(DCLV_Applied_Derating_Factor *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Input_Current_DCLV_Input_Current(DCLV_Input_Current *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(DCLV_Measured_Current *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(DCLV_Measured_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(VCU_DCDCActivation *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq(VCU_DCDCVoltageReq *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result data)
 *   Std_ReturnType Rte_Write_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault(boolean data)
 *   Std_ReturnType Rte_Write_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_InputCurrent_DCDC_InputCurrent(DCDC_InputCurrent data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_Status_DCDC_Status(DCDC_Status data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_VoltageReference_DCLV_VoltageReference(DCLV_VoltageReference data)
 *   Std_ReturnType Rte_Write_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(SUP_RequestDCLVStatus data)
 *   Std_ReturnType Rte_Write_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLVC_task10msB_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLVC_CODE) RCtApLVC_task10msB(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLVC_task10msB
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  boolean Read_PpActiveDischargeRequest_DeActiveDischargeRequest;
  boolean Read_PpDCLVError_DeDCLVError;
  boolean Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue;
  boolean Read_PpImplausibilityTempBuck_DeImplausibilityTempBuck;
  boolean Read_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull;
  DCLV_Applied_Derating_Factor Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor;
  DCLV_DCLVStatus Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus;
  DCLV_Input_Current Read_PpInt_DCLV_Input_Current_DCLV_Input_Current;
  DCLV_Input_Voltage Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage;
  DCLV_Measured_Current Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current;
  DCLV_Measured_Voltage Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage;
  VCU_DCDCActivation Read_PpInt_VCU_DCDCActivation_VCU_DCDCActivation;
  VCU_DCDCVoltageReq Read_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq;

  boolean PimPimDCLVShutdownPathFSP;
  boolean PimPimDCLVShutdownPathGPIO;

  uint16 CalDCLVDeratingThreshold_data;
  IdtDCLVTimeConfirmDerating CalDCLVTimeConfirmDerating_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimDCLVShutdownPathFSP = *TSC_CtApLVC_Rte_Pim_PimDCLVShutdownPathFSP();
  *TSC_CtApLVC_Rte_Pim_PimDCLVShutdownPathFSP() = PimPimDCLVShutdownPathFSP;
  PimPimDCLVShutdownPathGPIO = *TSC_CtApLVC_Rte_Pim_PimDCLVShutdownPathGPIO();
  *TSC_CtApLVC_Rte_Pim_PimDCLVShutdownPathGPIO() = PimPimDCLVShutdownPathGPIO;

  CalDCLVDeratingThreshold_data = TSC_CtApLVC_Rte_CData_CalDCLVDeratingThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVTimeConfirmDerating_data = TSC_CtApLVC_Rte_CData_CalDCLVTimeConfirmDerating(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApLVC_Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(&Read_PpActiveDischargeRequest_DeActiveDischargeRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Read_PpDCLVError_DeDCLVError(&Read_PpDCLVError_DeDCLVError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(&Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Read_PpImplausibilityTempBuck_DeImplausibilityTempBuck(&Read_PpImplausibilityTempBuck_DeImplausibilityTempBuck); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Read_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(&Read_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(&Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(&Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Read_PpInt_DCLV_Input_Current_DCLV_Input_Current(&Read_PpInt_DCLV_Input_Current_DCLV_Input_Current); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(&Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(&Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(&Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Read_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(&Read_PpInt_VCU_DCDCActivation_VCU_DCDCActivation); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Read_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq(&Read_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Write_PpDCDC_POST_Result_DeDCDC_POST_Result(Rte_InitValue_PpDCDC_POST_Result_DeDCDC_POST_Result); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Write_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault(Rte_InitValue_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Write_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV(Rte_InitValue_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Write_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(Rte_InitValue_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Write_PpInt_DCDC_InputCurrent_DCDC_InputCurrent(Rte_InitValue_PpInt_DCDC_InputCurrent_DCDC_InputCurrent); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Write_PpInt_DCDC_Status_DCDC_Status(Rte_InitValue_PpInt_DCDC_Status_DCDC_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Write_PpInt_DCLV_VoltageReference_DCLV_VoltageReference(Rte_InitValue_PpInt_DCLV_VoltageReference_DCLV_VoltageReference); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Write_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(Rte_InitValue_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Write_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue(Rte_InitValue_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Write_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue(Rte_InitValue_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLVC_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U, 0U, 0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApLVC_STOP_SEC_CODE
#include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApLVC_TestDefines(void)
{
  /* Enumeration Data Types */

  DCDC_Status Test_DCDC_Status_V_1 = Cx0_off_mode;
  DCDC_Status Test_DCDC_Status_V_2 = Cx1_Init_mode;
  DCDC_Status Test_DCDC_Status_V_3 = Cx2_standby_mode;
  DCDC_Status Test_DCDC_Status_V_4 = Cx3_conversion_working_;
  DCDC_Status Test_DCDC_Status_V_5 = Cx4_error_mode;
  DCDC_Status Test_DCDC_Status_V_6 = Cx5_degradation_mode;
  DCDC_Status Test_DCDC_Status_V_7 = Cx6_reserved;
  DCDC_Status Test_DCDC_Status_V_8 = Cx7_invalid;

  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_1 = Cx0_STANDBY;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_2 = Cx1_RUN;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_3 = Cx2_ERROR;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_4 = Cx3_ALARM;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_5 = Cx4_SAFE;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_6 = Cx5_DERATED;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_7 = Cx6_SHUTDOWN;

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;

  IdtPOST_Result Test_IdtPOST_Result_V_1 = POST_ONGOING;
  IdtPOST_Result Test_IdtPOST_Result_V_2 = POST_OK;
  IdtPOST_Result Test_IdtPOST_Result_V_3 = POST_NOK;

  SUP_RequestDCLVStatus Test_SUP_RequestDCLVStatus_V_1 = Cx0_DCLV_STATE_STANDBY;
  SUP_RequestDCLVStatus Test_SUP_RequestDCLVStatus_V_2 = Cx1_DCLV_STATE_RUN;
  SUP_RequestDCLVStatus Test_SUP_RequestDCLVStatus_V_3 = Cx2_DCLV_STATE_RESET_ERROR;
  SUP_RequestDCLVStatus Test_SUP_RequestDCLVStatus_V_4 = Cx3_DCLV_STATE_DISCHARGE;
  SUP_RequestDCLVStatus Test_SUP_RequestDCLVStatus_V_5 = Cx4_DCLV_STATE_SHUTDOWN;

  VCU_DCDCActivation Test_VCU_DCDCActivation_V_1 = Cx0_Not_used;
  VCU_DCDCActivation Test_VCU_DCDCActivation_V_2 = Cx1_DCDC_Off;
  VCU_DCDCActivation Test_VCU_DCDCActivation_V_3 = Cx2_DCDC_On;
  VCU_DCDCActivation Test_VCU_DCDCActivation_V_4 = Cx3_Unavailable_Value;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
