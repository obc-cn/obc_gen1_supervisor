/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApFCT.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApFCT
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApFCT>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApFCT.h"
#include "TSC_CtApFCT.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApFCT_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtLinTableGlobalTempElement: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtLinTableGlobalVoltElement: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtMsrTempRaw: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtOutputTempAftsales: Integer in interval [0...65535]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtOutputTempMeas: Integer in interval [0...65535]
 *   Unit: [deg C], Factor: 0.1, Offset: -40
 * IdtRampGradient: Integer in interval [0...100]
 *   Unit: [deg C / s], Factor: 0.1, Offset: 0
 * IdtSubstituteValueTemp: Integer in interval [0...240]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtThresholdTempFault: Integer in interval [0...5000]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtTimeDiagTemp: Integer in interval [0...1000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BSI_MainWakeup: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Invalid_RCD (0U)
 *   Cx1_No_main_wake_up_request (1U)
 *   Cx2_Main_wake_up_request (2U)
 *   Cx3_Not_valid (3U)
 * DCDC_FaultLampRequest: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_fault (0U)
 *   Cx1_Warning_limited_performance (1U)
 *   Cx2_Short_time_fault_Need_reset (2U)
 *   Cx3_Permernant_fault_need_replaced (3U)
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * IdtBatteryVoltageState: Enumeration of integer in interval [0...2] with enumerators
 *   BAT_VALID_RANGE (0U)
 *   BAT_OVERVOLTAGE (1U)
 *   BAT_UNDERVOLTAGE (2U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 *
 * Array Types:
 * ============
 * IdtLinTableGlobalTemp: Array with 39 element(s) of type IdtLinTableGlobalTempElement
 * IdtLinTableGlobalVolt: Array with 39 element(s) of type IdtLinTableGlobalVoltElement
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint16 *Rte_Pim_PimTempAC1CounterSCG(void)
 *   uint16 *Rte_Pim_PimTempAC1CounterSCP(void)
 *   uint16 *Rte_Pim_PimTempAC2CounterSCG(void)
 *   uint16 *Rte_Pim_PimTempAC2CounterSCP(void)
 *   uint16 *Rte_Pim_PimTempAC3CounterSCG(void)
 *   uint16 *Rte_Pim_PimTempAC3CounterSCP(void)
 *   uint16 *Rte_Pim_PimTempACNCounterSCG(void)
 *   uint16 *Rte_Pim_PimTempACNCounterSCP(void)
 *   uint16 *Rte_Pim_PimTempDC1CounterSCG(void)
 *   uint16 *Rte_Pim_PimTempDC1CounterSCP(void)
 *   uint16 *Rte_Pim_PimTempDC2CounterSCG(void)
 *   uint16 *Rte_Pim_PimTempDC2CounterSCP(void)
 *   boolean *Rte_Pim_PimTempAC1PrefaultSCG(void)
 *   boolean *Rte_Pim_PimTempAC1PrefaultSCP(void)
 *   boolean *Rte_Pim_PimTempAC2PrefaultSCG(void)
 *   boolean *Rte_Pim_PimTempAC2PrefaultSCP(void)
 *   boolean *Rte_Pim_PimTempAC3PrefaultSCG(void)
 *   boolean *Rte_Pim_PimTempAC3PrefaultSCP(void)
 *   boolean *Rte_Pim_PimTempACNPrefaultSCG(void)
 *   boolean *Rte_Pim_PimTempACNPrefaultSCP(void)
 *   boolean *Rte_Pim_PimTempDC1PrefaultSCG(void)
 *   boolean *Rte_Pim_PimTempDC1PrefaultSCP(void)
 *   boolean *Rte_Pim_PimTempDC2PrefaultSCG(void)
 *   boolean *Rte_Pim_PimTempDC2PrefaultSCP(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCG_AC1(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCG_AC2(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCG_AC3(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCG_ACN(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCG_DC1(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCG_DC2(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCP_AC1(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCP_AC2(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCP_AC3(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCP_ACN(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCP_DC1(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCP_DC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCG_AC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCG_AC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCG_AC3(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCG_ACN(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCG_DC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCG_DC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCP_AC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCP_AC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCP_AC3(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCP_ACN(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCP_DC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCP_DC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCG_AC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCG_AC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCG_AC3(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCG_ACN(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCG_DC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCG_DC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCP_AC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCP_AC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCP_AC3(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCP_ACN(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCP_DC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCP_DC2(void)
 *   IdtRampGradient Rte_CData_CalRampGradient_AC1(void)
 *   IdtRampGradient Rte_CData_CalRampGradient_AC2(void)
 *   IdtRampGradient Rte_CData_CalRampGradient_AC3(void)
 *   IdtRampGradient Rte_CData_CalRampGradient_ACN(void)
 *   IdtRampGradient Rte_CData_CalRampGradient_DC1(void)
 *   IdtRampGradient Rte_CData_CalRampGradient_DC2(void)
 *   IdtSubstituteValueTemp Rte_CData_CalSubstituteValueAC1_C(void)
 *   IdtSubstituteValueTemp Rte_CData_CalSubstituteValueAC2_C(void)
 *   IdtSubstituteValueTemp Rte_CData_CalSubstituteValueAC3_C(void)
 *   IdtSubstituteValueTemp Rte_CData_CalSubstituteValueACN_C(void)
 *   IdtSubstituteValueTemp Rte_CData_CalSubstituteValueDC1_C(void)
 *   IdtSubstituteValueTemp Rte_CData_CalSubstituteValueDC2_C(void)
 *   boolean Rte_CData_CalCtrlFlowAC1Plug(void)
 *   boolean Rte_CData_CalCtrlFlowAC2Plug(void)
 *   boolean Rte_CData_CalCtrlFlowAC3Plug(void)
 *   boolean Rte_CData_CalCtrlFlowACNPlug(void)
 *   boolean Rte_CData_CalCtrlFlowDC1Plug(void)
 *   boolean Rte_CData_CalCtrlFlowDC2Plug(void)
 *   IdtLinTableGlobalTempElement *Rte_CData_CalLinTableGlobalTemp_AC1(void)
 *     Returnvalue: IdtLinTableGlobalTempElement* is of type IdtLinTableGlobalTemp
 *   IdtLinTableGlobalTempElement *Rte_CData_CalLinTableGlobalTemp_AC2(void)
 *     Returnvalue: IdtLinTableGlobalTempElement* is of type IdtLinTableGlobalTemp
 *   IdtLinTableGlobalTempElement *Rte_CData_CalLinTableGlobalTemp_AC3(void)
 *     Returnvalue: IdtLinTableGlobalTempElement* is of type IdtLinTableGlobalTemp
 *   IdtLinTableGlobalTempElement *Rte_CData_CalLinTableGlobalTemp_ACN(void)
 *     Returnvalue: IdtLinTableGlobalTempElement* is of type IdtLinTableGlobalTemp
 *   IdtLinTableGlobalTempElement *Rte_CData_CalLinTableGlobalTemp_DC1(void)
 *     Returnvalue: IdtLinTableGlobalTempElement* is of type IdtLinTableGlobalTemp
 *   IdtLinTableGlobalTempElement *Rte_CData_CalLinTableGlobalTemp_DC2(void)
 *     Returnvalue: IdtLinTableGlobalTempElement* is of type IdtLinTableGlobalTemp
 *   IdtLinTableGlobalVoltElement *Rte_CData_CalLinTableGlobalVoltage_AC1(void)
 *     Returnvalue: IdtLinTableGlobalVoltElement* is of type IdtLinTableGlobalVolt
 *   IdtLinTableGlobalVoltElement *Rte_CData_CalLinTableGlobalVoltage_AC2(void)
 *     Returnvalue: IdtLinTableGlobalVoltElement* is of type IdtLinTableGlobalVolt
 *   IdtLinTableGlobalVoltElement *Rte_CData_CalLinTableGlobalVoltage_AC3(void)
 *     Returnvalue: IdtLinTableGlobalVoltElement* is of type IdtLinTableGlobalVolt
 *   IdtLinTableGlobalVoltElement *Rte_CData_CalLinTableGlobalVoltage_ACN(void)
 *     Returnvalue: IdtLinTableGlobalVoltElement* is of type IdtLinTableGlobalVolt
 *   IdtLinTableGlobalVoltElement *Rte_CData_CalLinTableGlobalVoltage_DC1(void)
 *     Returnvalue: IdtLinTableGlobalVoltElement* is of type IdtLinTableGlobalVolt
 *   IdtLinTableGlobalVoltElement *Rte_CData_CalLinTableGlobalVoltage_DC2(void)
 *     Returnvalue: IdtLinTableGlobalVoltElement* is of type IdtLinTableGlobalVolt
 *
 *********************************************************************************************************************/


#define CtApFCT_START_SEC_CODE
#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApFCT_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCT_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApFCT_CODE) RCtApFCT_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCT_init
 *********************************************************************************************************************/

  uint16 PimPimTempAC1CounterSCG;
  uint16 PimPimTempAC1CounterSCP;
  uint16 PimPimTempAC2CounterSCG;
  uint16 PimPimTempAC2CounterSCP;
  uint16 PimPimTempAC3CounterSCG;
  uint16 PimPimTempAC3CounterSCP;
  uint16 PimPimTempACNCounterSCG;
  uint16 PimPimTempACNCounterSCP;
  uint16 PimPimTempDC1CounterSCG;
  uint16 PimPimTempDC1CounterSCP;
  uint16 PimPimTempDC2CounterSCG;
  uint16 PimPimTempDC2CounterSCP;
  boolean PimPimTempAC1PrefaultSCG;
  boolean PimPimTempAC1PrefaultSCP;
  boolean PimPimTempAC2PrefaultSCG;
  boolean PimPimTempAC2PrefaultSCP;
  boolean PimPimTempAC3PrefaultSCG;
  boolean PimPimTempAC3PrefaultSCP;
  boolean PimPimTempACNPrefaultSCG;
  boolean PimPimTempACNPrefaultSCP;
  boolean PimPimTempDC1PrefaultSCG;
  boolean PimPimTempDC1PrefaultSCP;
  boolean PimPimTempDC2PrefaultSCG;
  boolean PimPimTempDC2PrefaultSCP;

  IdtThresholdTempFault CalThresholdSCG_AC1_data;
  IdtThresholdTempFault CalThresholdSCG_AC2_data;
  IdtThresholdTempFault CalThresholdSCG_AC3_data;
  IdtThresholdTempFault CalThresholdSCG_ACN_data;
  IdtThresholdTempFault CalThresholdSCG_DC1_data;
  IdtThresholdTempFault CalThresholdSCG_DC2_data;
  IdtThresholdTempFault CalThresholdSCP_AC1_data;
  IdtThresholdTempFault CalThresholdSCP_AC2_data;
  IdtThresholdTempFault CalThresholdSCP_AC3_data;
  IdtThresholdTempFault CalThresholdSCP_ACN_data;
  IdtThresholdTempFault CalThresholdSCP_DC1_data;
  IdtThresholdTempFault CalThresholdSCP_DC2_data;
  IdtTimeDiagTemp CalTimeConfSCG_AC1_data;
  IdtTimeDiagTemp CalTimeConfSCG_AC2_data;
  IdtTimeDiagTemp CalTimeConfSCG_AC3_data;
  IdtTimeDiagTemp CalTimeConfSCG_ACN_data;
  IdtTimeDiagTemp CalTimeConfSCG_DC1_data;
  IdtTimeDiagTemp CalTimeConfSCG_DC2_data;
  IdtTimeDiagTemp CalTimeConfSCP_AC1_data;
  IdtTimeDiagTemp CalTimeConfSCP_AC2_data;
  IdtTimeDiagTemp CalTimeConfSCP_AC3_data;
  IdtTimeDiagTemp CalTimeConfSCP_ACN_data;
  IdtTimeDiagTemp CalTimeConfSCP_DC1_data;
  IdtTimeDiagTemp CalTimeConfSCP_DC2_data;
  IdtTimeDiagTemp CalTimeHealSCG_AC1_data;
  IdtTimeDiagTemp CalTimeHealSCG_AC2_data;
  IdtTimeDiagTemp CalTimeHealSCG_AC3_data;
  IdtTimeDiagTemp CalTimeHealSCG_ACN_data;
  IdtTimeDiagTemp CalTimeHealSCG_DC1_data;
  IdtTimeDiagTemp CalTimeHealSCG_DC2_data;
  IdtTimeDiagTemp CalTimeHealSCP_AC1_data;
  IdtTimeDiagTemp CalTimeHealSCP_AC2_data;
  IdtTimeDiagTemp CalTimeHealSCP_AC3_data;
  IdtTimeDiagTemp CalTimeHealSCP_ACN_data;
  IdtTimeDiagTemp CalTimeHealSCP_DC1_data;
  IdtTimeDiagTemp CalTimeHealSCP_DC2_data;
  IdtRampGradient CalRampGradient_AC1_data;
  IdtRampGradient CalRampGradient_AC2_data;
  IdtRampGradient CalRampGradient_AC3_data;
  IdtRampGradient CalRampGradient_ACN_data;
  IdtRampGradient CalRampGradient_DC1_data;
  IdtRampGradient CalRampGradient_DC2_data;
  IdtSubstituteValueTemp CalSubstituteValueAC1_C_data;
  IdtSubstituteValueTemp CalSubstituteValueAC2_C_data;
  IdtSubstituteValueTemp CalSubstituteValueAC3_C_data;
  IdtSubstituteValueTemp CalSubstituteValueACN_C_data;
  IdtSubstituteValueTemp CalSubstituteValueDC1_C_data;
  IdtSubstituteValueTemp CalSubstituteValueDC2_C_data;
  boolean CalCtrlFlowAC1Plug_data;
  boolean CalCtrlFlowAC2Plug_data;
  boolean CalCtrlFlowAC3Plug_data;
  boolean CalCtrlFlowACNPlug_data;
  boolean CalCtrlFlowDC1Plug_data;
  boolean CalCtrlFlowDC2Plug_data;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_AC1_data;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_AC2_data;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_AC3_data;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_ACN_data;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_DC1_data;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_DC2_data;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_AC1_data;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_AC2_data;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_AC3_data;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_ACN_data;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_DC1_data;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_DC2_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimTempAC1CounterSCG = *TSC_CtApFCT_Rte_Pim_PimTempAC1CounterSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempAC1CounterSCG() = PimPimTempAC1CounterSCG;
  PimPimTempAC1CounterSCP = *TSC_CtApFCT_Rte_Pim_PimTempAC1CounterSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempAC1CounterSCP() = PimPimTempAC1CounterSCP;
  PimPimTempAC2CounterSCG = *TSC_CtApFCT_Rte_Pim_PimTempAC2CounterSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempAC2CounterSCG() = PimPimTempAC2CounterSCG;
  PimPimTempAC2CounterSCP = *TSC_CtApFCT_Rte_Pim_PimTempAC2CounterSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempAC2CounterSCP() = PimPimTempAC2CounterSCP;
  PimPimTempAC3CounterSCG = *TSC_CtApFCT_Rte_Pim_PimTempAC3CounterSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempAC3CounterSCG() = PimPimTempAC3CounterSCG;
  PimPimTempAC3CounterSCP = *TSC_CtApFCT_Rte_Pim_PimTempAC3CounterSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempAC3CounterSCP() = PimPimTempAC3CounterSCP;
  PimPimTempACNCounterSCG = *TSC_CtApFCT_Rte_Pim_PimTempACNCounterSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempACNCounterSCG() = PimPimTempACNCounterSCG;
  PimPimTempACNCounterSCP = *TSC_CtApFCT_Rte_Pim_PimTempACNCounterSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempACNCounterSCP() = PimPimTempACNCounterSCP;
  PimPimTempDC1CounterSCG = *TSC_CtApFCT_Rte_Pim_PimTempDC1CounterSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempDC1CounterSCG() = PimPimTempDC1CounterSCG;
  PimPimTempDC1CounterSCP = *TSC_CtApFCT_Rte_Pim_PimTempDC1CounterSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempDC1CounterSCP() = PimPimTempDC1CounterSCP;
  PimPimTempDC2CounterSCG = *TSC_CtApFCT_Rte_Pim_PimTempDC2CounterSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempDC2CounterSCG() = PimPimTempDC2CounterSCG;
  PimPimTempDC2CounterSCP = *TSC_CtApFCT_Rte_Pim_PimTempDC2CounterSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempDC2CounterSCP() = PimPimTempDC2CounterSCP;
  PimPimTempAC1PrefaultSCG = *TSC_CtApFCT_Rte_Pim_PimTempAC1PrefaultSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempAC1PrefaultSCG() = PimPimTempAC1PrefaultSCG;
  PimPimTempAC1PrefaultSCP = *TSC_CtApFCT_Rte_Pim_PimTempAC1PrefaultSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempAC1PrefaultSCP() = PimPimTempAC1PrefaultSCP;
  PimPimTempAC2PrefaultSCG = *TSC_CtApFCT_Rte_Pim_PimTempAC2PrefaultSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempAC2PrefaultSCG() = PimPimTempAC2PrefaultSCG;
  PimPimTempAC2PrefaultSCP = *TSC_CtApFCT_Rte_Pim_PimTempAC2PrefaultSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempAC2PrefaultSCP() = PimPimTempAC2PrefaultSCP;
  PimPimTempAC3PrefaultSCG = *TSC_CtApFCT_Rte_Pim_PimTempAC3PrefaultSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempAC3PrefaultSCG() = PimPimTempAC3PrefaultSCG;
  PimPimTempAC3PrefaultSCP = *TSC_CtApFCT_Rte_Pim_PimTempAC3PrefaultSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempAC3PrefaultSCP() = PimPimTempAC3PrefaultSCP;
  PimPimTempACNPrefaultSCG = *TSC_CtApFCT_Rte_Pim_PimTempACNPrefaultSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempACNPrefaultSCG() = PimPimTempACNPrefaultSCG;
  PimPimTempACNPrefaultSCP = *TSC_CtApFCT_Rte_Pim_PimTempACNPrefaultSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempACNPrefaultSCP() = PimPimTempACNPrefaultSCP;
  PimPimTempDC1PrefaultSCG = *TSC_CtApFCT_Rte_Pim_PimTempDC1PrefaultSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempDC1PrefaultSCG() = PimPimTempDC1PrefaultSCG;
  PimPimTempDC1PrefaultSCP = *TSC_CtApFCT_Rte_Pim_PimTempDC1PrefaultSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempDC1PrefaultSCP() = PimPimTempDC1PrefaultSCP;
  PimPimTempDC2PrefaultSCG = *TSC_CtApFCT_Rte_Pim_PimTempDC2PrefaultSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempDC2PrefaultSCG() = PimPimTempDC2PrefaultSCG;
  PimPimTempDC2PrefaultSCP = *TSC_CtApFCT_Rte_Pim_PimTempDC2PrefaultSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempDC2PrefaultSCP() = PimPimTempDC2PrefaultSCP;

  CalThresholdSCG_AC1_data = TSC_CtApFCT_Rte_CData_CalThresholdSCG_AC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCG_AC2_data = TSC_CtApFCT_Rte_CData_CalThresholdSCG_AC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCG_AC3_data = TSC_CtApFCT_Rte_CData_CalThresholdSCG_AC3(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCG_ACN_data = TSC_CtApFCT_Rte_CData_CalThresholdSCG_ACN(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCG_DC1_data = TSC_CtApFCT_Rte_CData_CalThresholdSCG_DC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCG_DC2_data = TSC_CtApFCT_Rte_CData_CalThresholdSCG_DC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCP_AC1_data = TSC_CtApFCT_Rte_CData_CalThresholdSCP_AC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCP_AC2_data = TSC_CtApFCT_Rte_CData_CalThresholdSCP_AC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCP_AC3_data = TSC_CtApFCT_Rte_CData_CalThresholdSCP_AC3(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCP_ACN_data = TSC_CtApFCT_Rte_CData_CalThresholdSCP_ACN(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCP_DC1_data = TSC_CtApFCT_Rte_CData_CalThresholdSCP_DC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCP_DC2_data = TSC_CtApFCT_Rte_CData_CalThresholdSCP_DC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCG_AC1_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCG_AC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCG_AC2_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCG_AC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCG_AC3_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCG_AC3(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCG_ACN_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCG_ACN(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCG_DC1_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCG_DC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCG_DC2_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCG_DC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCP_AC1_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCP_AC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCP_AC2_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCP_AC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCP_AC3_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCP_AC3(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCP_ACN_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCP_ACN(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCP_DC1_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCP_DC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCP_DC2_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCP_DC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCG_AC1_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCG_AC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCG_AC2_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCG_AC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCG_AC3_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCG_AC3(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCG_ACN_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCG_ACN(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCG_DC1_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCG_DC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCG_DC2_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCG_DC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCP_AC1_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCP_AC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCP_AC2_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCP_AC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCP_AC3_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCP_AC3(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCP_ACN_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCP_ACN(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCP_DC1_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCP_DC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCP_DC2_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCP_DC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalRampGradient_AC1_data = TSC_CtApFCT_Rte_CData_CalRampGradient_AC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalRampGradient_AC2_data = TSC_CtApFCT_Rte_CData_CalRampGradient_AC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalRampGradient_AC3_data = TSC_CtApFCT_Rte_CData_CalRampGradient_AC3(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalRampGradient_ACN_data = TSC_CtApFCT_Rte_CData_CalRampGradient_ACN(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalRampGradient_DC1_data = TSC_CtApFCT_Rte_CData_CalRampGradient_DC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalRampGradient_DC2_data = TSC_CtApFCT_Rte_CData_CalRampGradient_DC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalSubstituteValueAC1_C_data = TSC_CtApFCT_Rte_CData_CalSubstituteValueAC1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalSubstituteValueAC2_C_data = TSC_CtApFCT_Rte_CData_CalSubstituteValueAC2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalSubstituteValueAC3_C_data = TSC_CtApFCT_Rte_CData_CalSubstituteValueAC3_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalSubstituteValueACN_C_data = TSC_CtApFCT_Rte_CData_CalSubstituteValueACN_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalSubstituteValueDC1_C_data = TSC_CtApFCT_Rte_CData_CalSubstituteValueDC1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalSubstituteValueDC2_C_data = TSC_CtApFCT_Rte_CData_CalSubstituteValueDC2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowAC1Plug_data = TSC_CtApFCT_Rte_CData_CalCtrlFlowAC1Plug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowAC2Plug_data = TSC_CtApFCT_Rte_CData_CalCtrlFlowAC2Plug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowAC3Plug_data = TSC_CtApFCT_Rte_CData_CalCtrlFlowAC3Plug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowACNPlug_data = TSC_CtApFCT_Rte_CData_CalCtrlFlowACNPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowDC1Plug_data = TSC_CtApFCT_Rte_CData_CalCtrlFlowDC1Plug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowDC2Plug_data = TSC_CtApFCT_Rte_CData_CalCtrlFlowDC2Plug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  (void)memcpy(CalLinTableGlobalTemp_AC1_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_AC1(), sizeof(IdtLinTableGlobalTemp)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalTemp_AC2_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_AC2(), sizeof(IdtLinTableGlobalTemp)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalTemp_AC3_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_AC3(), sizeof(IdtLinTableGlobalTemp)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalTemp_ACN_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_ACN(), sizeof(IdtLinTableGlobalTemp)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalTemp_DC1_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_DC1(), sizeof(IdtLinTableGlobalTemp)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalTemp_DC2_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_DC2(), sizeof(IdtLinTableGlobalTemp)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalVoltage_AC1_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_AC1(), sizeof(IdtLinTableGlobalVolt)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalVoltage_AC2_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_AC2(), sizeof(IdtLinTableGlobalVolt)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalVoltage_AC3_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_AC3(), sizeof(IdtLinTableGlobalVolt)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalVoltage_ACN_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_ACN(), sizeof(IdtLinTableGlobalVolt)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalVoltage_DC1_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_DC1(), sizeof(IdtLinTableGlobalVolt)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalVoltage_DC2_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_DC2(), sizeof(IdtLinTableGlobalVolt)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */

  CtApFCT_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApFCT_task100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup *data)
 *   Std_ReturnType Rte_Read_PpMsrTempRaw_DeMsrTempAC1Raw(IdtMsrTempRaw *data)
 *   Std_ReturnType Rte_Read_PpMsrTempRaw_DeMsrTempAC2Raw(IdtMsrTempRaw *data)
 *   Std_ReturnType Rte_Read_PpMsrTempRaw_DeMsrTempAC3Raw(IdtMsrTempRaw *data)
 *   Std_ReturnType Rte_Read_PpMsrTempRaw_DeMsrTempACNRaw(IdtMsrTempRaw *data)
 *   Std_ReturnType Rte_Read_PpMsrTempRaw_DeMsrTempDC1Raw(IdtMsrTempRaw *data)
 *   Std_ReturnType Rte_Read_PpMsrTempRaw_DeMsrTempDC2Raw(IdtMsrTempRaw *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest(DCDC_FaultLampRequest data)
 *   Std_ReturnType Rte_Write_PpOutputTempAftsales_DeOutputTempAC1Aftsales(IdtOutputTempAftsales data)
 *   Std_ReturnType Rte_Write_PpOutputTempAftsales_DeOutputTempAC2Aftsales(IdtOutputTempAftsales data)
 *   Std_ReturnType Rte_Write_PpOutputTempAftsales_DeOutputTempAC3Aftsales(IdtOutputTempAftsales data)
 *   Std_ReturnType Rte_Write_PpOutputTempAftsales_DeOutputTempACNAftsales(IdtOutputTempAftsales data)
 *   Std_ReturnType Rte_Write_PpOutputTempAftsales_DeOutputTempDC1Aftsales(IdtOutputTempAftsales data)
 *   Std_ReturnType Rte_Write_PpOutputTempAftsales_DeOutputTempDC2Aftsales(IdtOutputTempAftsales data)
 *   Std_ReturnType Rte_Write_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas data)
 *   Std_ReturnType Rte_Write_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas data)
 *   Std_ReturnType Rte_Write_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas data)
 *   Std_ReturnType Rte_Write_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas data)
 *   Std_ReturnType Rte_Write_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas data)
 *   Std_ReturnType Rte_Write_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempAC1FaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempAC1FaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempAC2FaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempAC2FaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempAC3FaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempAC3FaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempACNFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempACNFaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempDC1FaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempDC1FaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempDC2FaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempDC2FaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpTempMonitoringConditions_DeAC1TempMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpTempMonitoringConditions_DeAC2TempMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpTempMonitoringConditions_DeAC3TempMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpTempMonitoringConditions_DeACNTempMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpTempMonitoringConditions_DeDC1TempMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpTempMonitoringConditions_DeDC2TempMonitoringConditions(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCT_task100ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApFCT_CODE) RCtApFCT_task100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCT_task100ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtAppRCDECUState Read_PpAppRCDECUState_DeAppRCDECUState;
  IdtBatteryVoltageState Read_PpBatteryVoltageState_DeBatteryVoltageState;
  BSI_MainWakeup Read_PpInt_BSI_MainWakeup_BSI_MainWakeup;
  IdtMsrTempRaw Read_PpMsrTempRaw_DeMsrTempAC1Raw;
  IdtMsrTempRaw Read_PpMsrTempRaw_DeMsrTempAC2Raw;
  IdtMsrTempRaw Read_PpMsrTempRaw_DeMsrTempAC3Raw;
  IdtMsrTempRaw Read_PpMsrTempRaw_DeMsrTempACNRaw;
  IdtMsrTempRaw Read_PpMsrTempRaw_DeMsrTempDC1Raw;
  IdtMsrTempRaw Read_PpMsrTempRaw_DeMsrTempDC2Raw;

  uint16 PimPimTempAC1CounterSCG;
  uint16 PimPimTempAC1CounterSCP;
  uint16 PimPimTempAC2CounterSCG;
  uint16 PimPimTempAC2CounterSCP;
  uint16 PimPimTempAC3CounterSCG;
  uint16 PimPimTempAC3CounterSCP;
  uint16 PimPimTempACNCounterSCG;
  uint16 PimPimTempACNCounterSCP;
  uint16 PimPimTempDC1CounterSCG;
  uint16 PimPimTempDC1CounterSCP;
  uint16 PimPimTempDC2CounterSCG;
  uint16 PimPimTempDC2CounterSCP;
  boolean PimPimTempAC1PrefaultSCG;
  boolean PimPimTempAC1PrefaultSCP;
  boolean PimPimTempAC2PrefaultSCG;
  boolean PimPimTempAC2PrefaultSCP;
  boolean PimPimTempAC3PrefaultSCG;
  boolean PimPimTempAC3PrefaultSCP;
  boolean PimPimTempACNPrefaultSCG;
  boolean PimPimTempACNPrefaultSCP;
  boolean PimPimTempDC1PrefaultSCG;
  boolean PimPimTempDC1PrefaultSCP;
  boolean PimPimTempDC2PrefaultSCG;
  boolean PimPimTempDC2PrefaultSCP;

  IdtThresholdTempFault CalThresholdSCG_AC1_data;
  IdtThresholdTempFault CalThresholdSCG_AC2_data;
  IdtThresholdTempFault CalThresholdSCG_AC3_data;
  IdtThresholdTempFault CalThresholdSCG_ACN_data;
  IdtThresholdTempFault CalThresholdSCG_DC1_data;
  IdtThresholdTempFault CalThresholdSCG_DC2_data;
  IdtThresholdTempFault CalThresholdSCP_AC1_data;
  IdtThresholdTempFault CalThresholdSCP_AC2_data;
  IdtThresholdTempFault CalThresholdSCP_AC3_data;
  IdtThresholdTempFault CalThresholdSCP_ACN_data;
  IdtThresholdTempFault CalThresholdSCP_DC1_data;
  IdtThresholdTempFault CalThresholdSCP_DC2_data;
  IdtTimeDiagTemp CalTimeConfSCG_AC1_data;
  IdtTimeDiagTemp CalTimeConfSCG_AC2_data;
  IdtTimeDiagTemp CalTimeConfSCG_AC3_data;
  IdtTimeDiagTemp CalTimeConfSCG_ACN_data;
  IdtTimeDiagTemp CalTimeConfSCG_DC1_data;
  IdtTimeDiagTemp CalTimeConfSCG_DC2_data;
  IdtTimeDiagTemp CalTimeConfSCP_AC1_data;
  IdtTimeDiagTemp CalTimeConfSCP_AC2_data;
  IdtTimeDiagTemp CalTimeConfSCP_AC3_data;
  IdtTimeDiagTemp CalTimeConfSCP_ACN_data;
  IdtTimeDiagTemp CalTimeConfSCP_DC1_data;
  IdtTimeDiagTemp CalTimeConfSCP_DC2_data;
  IdtTimeDiagTemp CalTimeHealSCG_AC1_data;
  IdtTimeDiagTemp CalTimeHealSCG_AC2_data;
  IdtTimeDiagTemp CalTimeHealSCG_AC3_data;
  IdtTimeDiagTemp CalTimeHealSCG_ACN_data;
  IdtTimeDiagTemp CalTimeHealSCG_DC1_data;
  IdtTimeDiagTemp CalTimeHealSCG_DC2_data;
  IdtTimeDiagTemp CalTimeHealSCP_AC1_data;
  IdtTimeDiagTemp CalTimeHealSCP_AC2_data;
  IdtTimeDiagTemp CalTimeHealSCP_AC3_data;
  IdtTimeDiagTemp CalTimeHealSCP_ACN_data;
  IdtTimeDiagTemp CalTimeHealSCP_DC1_data;
  IdtTimeDiagTemp CalTimeHealSCP_DC2_data;
  IdtRampGradient CalRampGradient_AC1_data;
  IdtRampGradient CalRampGradient_AC2_data;
  IdtRampGradient CalRampGradient_AC3_data;
  IdtRampGradient CalRampGradient_ACN_data;
  IdtRampGradient CalRampGradient_DC1_data;
  IdtRampGradient CalRampGradient_DC2_data;
  IdtSubstituteValueTemp CalSubstituteValueAC1_C_data;
  IdtSubstituteValueTemp CalSubstituteValueAC2_C_data;
  IdtSubstituteValueTemp CalSubstituteValueAC3_C_data;
  IdtSubstituteValueTemp CalSubstituteValueACN_C_data;
  IdtSubstituteValueTemp CalSubstituteValueDC1_C_data;
  IdtSubstituteValueTemp CalSubstituteValueDC2_C_data;
  boolean CalCtrlFlowAC1Plug_data;
  boolean CalCtrlFlowAC2Plug_data;
  boolean CalCtrlFlowAC3Plug_data;
  boolean CalCtrlFlowACNPlug_data;
  boolean CalCtrlFlowDC1Plug_data;
  boolean CalCtrlFlowDC2Plug_data;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_AC1_data;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_AC2_data;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_AC3_data;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_ACN_data;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_DC1_data;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_DC2_data;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_AC1_data;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_AC2_data;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_AC3_data;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_ACN_data;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_DC1_data;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_DC2_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimTempAC1CounterSCG = *TSC_CtApFCT_Rte_Pim_PimTempAC1CounterSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempAC1CounterSCG() = PimPimTempAC1CounterSCG;
  PimPimTempAC1CounterSCP = *TSC_CtApFCT_Rte_Pim_PimTempAC1CounterSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempAC1CounterSCP() = PimPimTempAC1CounterSCP;
  PimPimTempAC2CounterSCG = *TSC_CtApFCT_Rte_Pim_PimTempAC2CounterSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempAC2CounterSCG() = PimPimTempAC2CounterSCG;
  PimPimTempAC2CounterSCP = *TSC_CtApFCT_Rte_Pim_PimTempAC2CounterSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempAC2CounterSCP() = PimPimTempAC2CounterSCP;
  PimPimTempAC3CounterSCG = *TSC_CtApFCT_Rte_Pim_PimTempAC3CounterSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempAC3CounterSCG() = PimPimTempAC3CounterSCG;
  PimPimTempAC3CounterSCP = *TSC_CtApFCT_Rte_Pim_PimTempAC3CounterSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempAC3CounterSCP() = PimPimTempAC3CounterSCP;
  PimPimTempACNCounterSCG = *TSC_CtApFCT_Rte_Pim_PimTempACNCounterSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempACNCounterSCG() = PimPimTempACNCounterSCG;
  PimPimTempACNCounterSCP = *TSC_CtApFCT_Rte_Pim_PimTempACNCounterSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempACNCounterSCP() = PimPimTempACNCounterSCP;
  PimPimTempDC1CounterSCG = *TSC_CtApFCT_Rte_Pim_PimTempDC1CounterSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempDC1CounterSCG() = PimPimTempDC1CounterSCG;
  PimPimTempDC1CounterSCP = *TSC_CtApFCT_Rte_Pim_PimTempDC1CounterSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempDC1CounterSCP() = PimPimTempDC1CounterSCP;
  PimPimTempDC2CounterSCG = *TSC_CtApFCT_Rte_Pim_PimTempDC2CounterSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempDC2CounterSCG() = PimPimTempDC2CounterSCG;
  PimPimTempDC2CounterSCP = *TSC_CtApFCT_Rte_Pim_PimTempDC2CounterSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempDC2CounterSCP() = PimPimTempDC2CounterSCP;
  PimPimTempAC1PrefaultSCG = *TSC_CtApFCT_Rte_Pim_PimTempAC1PrefaultSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempAC1PrefaultSCG() = PimPimTempAC1PrefaultSCG;
  PimPimTempAC1PrefaultSCP = *TSC_CtApFCT_Rte_Pim_PimTempAC1PrefaultSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempAC1PrefaultSCP() = PimPimTempAC1PrefaultSCP;
  PimPimTempAC2PrefaultSCG = *TSC_CtApFCT_Rte_Pim_PimTempAC2PrefaultSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempAC2PrefaultSCG() = PimPimTempAC2PrefaultSCG;
  PimPimTempAC2PrefaultSCP = *TSC_CtApFCT_Rte_Pim_PimTempAC2PrefaultSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempAC2PrefaultSCP() = PimPimTempAC2PrefaultSCP;
  PimPimTempAC3PrefaultSCG = *TSC_CtApFCT_Rte_Pim_PimTempAC3PrefaultSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempAC3PrefaultSCG() = PimPimTempAC3PrefaultSCG;
  PimPimTempAC3PrefaultSCP = *TSC_CtApFCT_Rte_Pim_PimTempAC3PrefaultSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempAC3PrefaultSCP() = PimPimTempAC3PrefaultSCP;
  PimPimTempACNPrefaultSCG = *TSC_CtApFCT_Rte_Pim_PimTempACNPrefaultSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempACNPrefaultSCG() = PimPimTempACNPrefaultSCG;
  PimPimTempACNPrefaultSCP = *TSC_CtApFCT_Rte_Pim_PimTempACNPrefaultSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempACNPrefaultSCP() = PimPimTempACNPrefaultSCP;
  PimPimTempDC1PrefaultSCG = *TSC_CtApFCT_Rte_Pim_PimTempDC1PrefaultSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempDC1PrefaultSCG() = PimPimTempDC1PrefaultSCG;
  PimPimTempDC1PrefaultSCP = *TSC_CtApFCT_Rte_Pim_PimTempDC1PrefaultSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempDC1PrefaultSCP() = PimPimTempDC1PrefaultSCP;
  PimPimTempDC2PrefaultSCG = *TSC_CtApFCT_Rte_Pim_PimTempDC2PrefaultSCG();
  *TSC_CtApFCT_Rte_Pim_PimTempDC2PrefaultSCG() = PimPimTempDC2PrefaultSCG;
  PimPimTempDC2PrefaultSCP = *TSC_CtApFCT_Rte_Pim_PimTempDC2PrefaultSCP();
  *TSC_CtApFCT_Rte_Pim_PimTempDC2PrefaultSCP() = PimPimTempDC2PrefaultSCP;

  CalThresholdSCG_AC1_data = TSC_CtApFCT_Rte_CData_CalThresholdSCG_AC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCG_AC2_data = TSC_CtApFCT_Rte_CData_CalThresholdSCG_AC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCG_AC3_data = TSC_CtApFCT_Rte_CData_CalThresholdSCG_AC3(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCG_ACN_data = TSC_CtApFCT_Rte_CData_CalThresholdSCG_ACN(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCG_DC1_data = TSC_CtApFCT_Rte_CData_CalThresholdSCG_DC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCG_DC2_data = TSC_CtApFCT_Rte_CData_CalThresholdSCG_DC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCP_AC1_data = TSC_CtApFCT_Rte_CData_CalThresholdSCP_AC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCP_AC2_data = TSC_CtApFCT_Rte_CData_CalThresholdSCP_AC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCP_AC3_data = TSC_CtApFCT_Rte_CData_CalThresholdSCP_AC3(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCP_ACN_data = TSC_CtApFCT_Rte_CData_CalThresholdSCP_ACN(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCP_DC1_data = TSC_CtApFCT_Rte_CData_CalThresholdSCP_DC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdSCP_DC2_data = TSC_CtApFCT_Rte_CData_CalThresholdSCP_DC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCG_AC1_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCG_AC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCG_AC2_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCG_AC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCG_AC3_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCG_AC3(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCG_ACN_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCG_ACN(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCG_DC1_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCG_DC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCG_DC2_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCG_DC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCP_AC1_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCP_AC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCP_AC2_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCP_AC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCP_AC3_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCP_AC3(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCP_ACN_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCP_ACN(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCP_DC1_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCP_DC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfSCP_DC2_data = TSC_CtApFCT_Rte_CData_CalTimeConfSCP_DC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCG_AC1_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCG_AC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCG_AC2_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCG_AC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCG_AC3_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCG_AC3(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCG_ACN_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCG_ACN(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCG_DC1_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCG_DC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCG_DC2_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCG_DC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCP_AC1_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCP_AC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCP_AC2_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCP_AC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCP_AC3_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCP_AC3(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCP_ACN_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCP_ACN(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCP_DC1_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCP_DC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeHealSCP_DC2_data = TSC_CtApFCT_Rte_CData_CalTimeHealSCP_DC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalRampGradient_AC1_data = TSC_CtApFCT_Rte_CData_CalRampGradient_AC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalRampGradient_AC2_data = TSC_CtApFCT_Rte_CData_CalRampGradient_AC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalRampGradient_AC3_data = TSC_CtApFCT_Rte_CData_CalRampGradient_AC3(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalRampGradient_ACN_data = TSC_CtApFCT_Rte_CData_CalRampGradient_ACN(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalRampGradient_DC1_data = TSC_CtApFCT_Rte_CData_CalRampGradient_DC1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalRampGradient_DC2_data = TSC_CtApFCT_Rte_CData_CalRampGradient_DC2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalSubstituteValueAC1_C_data = TSC_CtApFCT_Rte_CData_CalSubstituteValueAC1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalSubstituteValueAC2_C_data = TSC_CtApFCT_Rte_CData_CalSubstituteValueAC2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalSubstituteValueAC3_C_data = TSC_CtApFCT_Rte_CData_CalSubstituteValueAC3_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalSubstituteValueACN_C_data = TSC_CtApFCT_Rte_CData_CalSubstituteValueACN_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalSubstituteValueDC1_C_data = TSC_CtApFCT_Rte_CData_CalSubstituteValueDC1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalSubstituteValueDC2_C_data = TSC_CtApFCT_Rte_CData_CalSubstituteValueDC2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowAC1Plug_data = TSC_CtApFCT_Rte_CData_CalCtrlFlowAC1Plug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowAC2Plug_data = TSC_CtApFCT_Rte_CData_CalCtrlFlowAC2Plug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowAC3Plug_data = TSC_CtApFCT_Rte_CData_CalCtrlFlowAC3Plug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowACNPlug_data = TSC_CtApFCT_Rte_CData_CalCtrlFlowACNPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowDC1Plug_data = TSC_CtApFCT_Rte_CData_CalCtrlFlowDC1Plug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowDC2Plug_data = TSC_CtApFCT_Rte_CData_CalCtrlFlowDC2Plug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  (void)memcpy(CalLinTableGlobalTemp_AC1_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_AC1(), sizeof(IdtLinTableGlobalTemp)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalTemp_AC2_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_AC2(), sizeof(IdtLinTableGlobalTemp)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalTemp_AC3_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_AC3(), sizeof(IdtLinTableGlobalTemp)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalTemp_ACN_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_ACN(), sizeof(IdtLinTableGlobalTemp)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalTemp_DC1_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_DC1(), sizeof(IdtLinTableGlobalTemp)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalTemp_DC2_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_DC2(), sizeof(IdtLinTableGlobalTemp)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalVoltage_AC1_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_AC1(), sizeof(IdtLinTableGlobalVolt)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalVoltage_AC2_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_AC2(), sizeof(IdtLinTableGlobalVolt)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalVoltage_AC3_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_AC3(), sizeof(IdtLinTableGlobalVolt)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalVoltage_ACN_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_ACN(), sizeof(IdtLinTableGlobalVolt)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalVoltage_DC1_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_DC1(), sizeof(IdtLinTableGlobalVolt)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(CalLinTableGlobalVoltage_DC2_data, TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_DC2(), sizeof(IdtLinTableGlobalVolt)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */

  fct_status = TSC_CtApFCT_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&Read_PpAppRCDECUState_DeAppRCDECUState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(&Read_PpBatteryVoltageState_DeBatteryVoltageState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(&Read_PpInt_BSI_MainWakeup_BSI_MainWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempAC1Raw(&Read_PpMsrTempRaw_DeMsrTempAC1Raw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempAC2Raw(&Read_PpMsrTempRaw_DeMsrTempAC2Raw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempAC3Raw(&Read_PpMsrTempRaw_DeMsrTempAC3Raw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempACNRaw(&Read_PpMsrTempRaw_DeMsrTempACNRaw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempDC1Raw(&Read_PpMsrTempRaw_DeMsrTempDC1Raw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempDC2Raw(&Read_PpMsrTempRaw_DeMsrTempDC2Raw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest(Rte_InitValue_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempAC1Aftsales(Rte_InitValue_PpOutputTempAftsales_DeOutputTempAC1Aftsales); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempAC2Aftsales(Rte_InitValue_PpOutputTempAftsales_DeOutputTempAC2Aftsales); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempAC3Aftsales(Rte_InitValue_PpOutputTempAftsales_DeOutputTempAC3Aftsales); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempACNAftsales(Rte_InitValue_PpOutputTempAftsales_DeOutputTempACNAftsales); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempDC1Aftsales(Rte_InitValue_PpOutputTempAftsales_DeOutputTempDC1Aftsales); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempDC2Aftsales(Rte_InitValue_PpOutputTempAftsales_DeOutputTempDC2Aftsales); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempAC1Meas(Rte_InitValue_PpOutputTempMeas_DeOutputTempAC1Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempAC2Meas(Rte_InitValue_PpOutputTempMeas_DeOutputTempAC2Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempAC3Meas(Rte_InitValue_PpOutputTempMeas_DeOutputTempAC3Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempACNMeas(Rte_InitValue_PpOutputTempMeas_DeOutputTempACNMeas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempDC1Meas(Rte_InitValue_PpOutputTempMeas_DeOutputTempDC1Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempDC2Meas(Rte_InitValue_PpOutputTempMeas_DeOutputTempDC2Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC1FaultSCG(Rte_InitValue_PpTempFaults_DeTempAC1FaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC1FaultSCP(Rte_InitValue_PpTempFaults_DeTempAC1FaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC2FaultSCG(Rte_InitValue_PpTempFaults_DeTempAC2FaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC2FaultSCP(Rte_InitValue_PpTempFaults_DeTempAC2FaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC3FaultSCG(Rte_InitValue_PpTempFaults_DeTempAC3FaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC3FaultSCP(Rte_InitValue_PpTempFaults_DeTempAC3FaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempACNFaultSCG(Rte_InitValue_PpTempFaults_DeTempACNFaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempACNFaultSCP(Rte_InitValue_PpTempFaults_DeTempACNFaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempDC1FaultSCG(Rte_InitValue_PpTempFaults_DeTempDC1FaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempDC1FaultSCP(Rte_InitValue_PpTempFaults_DeTempDC1FaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempDC2FaultSCG(Rte_InitValue_PpTempFaults_DeTempDC2FaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempDC2FaultSCP(Rte_InitValue_PpTempFaults_DeTempDC2FaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeAC1TempMonitoringConditions(Rte_InitValue_PpTempMonitoringConditions_DeAC1TempMonitoringConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeAC2TempMonitoringConditions(Rte_InitValue_PpTempMonitoringConditions_DeAC2TempMonitoringConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeAC3TempMonitoringConditions(Rte_InitValue_PpTempMonitoringConditions_DeAC3TempMonitoringConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeACNTempMonitoringConditions(Rte_InitValue_PpTempMonitoringConditions_DeACNTempMonitoringConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeDC1TempMonitoringConditions(Rte_InitValue_PpTempMonitoringConditions_DeDC1TempMonitoringConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeDC2TempMonitoringConditions(Rte_InitValue_PpTempMonitoringConditions_DeDC2TempMonitoringConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApFCT_STOP_SEC_CODE
#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApFCT_TestDefines(void)
{
  /* Enumeration Data Types */

  BSI_MainWakeup Test_BSI_MainWakeup_V_1 = Cx0_Invalid_RCD;
  BSI_MainWakeup Test_BSI_MainWakeup_V_2 = Cx1_No_main_wake_up_request;
  BSI_MainWakeup Test_BSI_MainWakeup_V_3 = Cx2_Main_wake_up_request;
  BSI_MainWakeup Test_BSI_MainWakeup_V_4 = Cx3_Not_valid;

  DCDC_FaultLampRequest Test_DCDC_FaultLampRequest_V_1 = Cx0_No_fault;
  DCDC_FaultLampRequest Test_DCDC_FaultLampRequest_V_2 = Cx1_Warning_limited_performance;
  DCDC_FaultLampRequest Test_DCDC_FaultLampRequest_V_3 = Cx2_Short_time_fault_Need_reset;
  DCDC_FaultLampRequest Test_DCDC_FaultLampRequest_V_4 = Cx3_Permernant_fault_need_replaced;

  IdtAppRCDECUState Test_IdtAppRCDECUState_V_1 = APP_STATE_0;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_2 = APP_STATE_1;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_3 = APP_STATE_2;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_4 = APP_STATE_3;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_5 = APP_STATE_4;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_6 = APP_STATE_5;

  IdtBatteryVoltageState Test_IdtBatteryVoltageState_V_1 = BAT_VALID_RANGE;
  IdtBatteryVoltageState Test_IdtBatteryVoltageState_V_2 = BAT_OVERVOLTAGE;
  IdtBatteryVoltageState Test_IdtBatteryVoltageState_V_3 = BAT_UNDERVOLTAGE;

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_Qac:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
