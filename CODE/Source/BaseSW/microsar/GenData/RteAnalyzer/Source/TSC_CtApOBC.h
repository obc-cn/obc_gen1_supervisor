/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApOBC.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApOBC_Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(boolean *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(boolean *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(BMS_HighestChargeCurrentAllow *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(BMS_HighestChargeVoltageAllow *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(DCHV_Command_Current_Reference *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_OBC_PowerMax_OBC_PowerMax(OBC_PowerMax *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_OBC_Status_OBC_Status(OBC_Status *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(PFC_IPH1_RMS_0A1 *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(PFC_IPH2_RMS_0A1 *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(PFC_IPH3_RMS_0A1 *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_PFC_PFCStatus_PFC_PFCStatus(PFC_PFCStatus *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(PFC_Vdclink_V *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(IdtMaxInputACCurrentEVSE *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(boolean *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays(boolean *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpRequestHWStopOBC_DeRequestHWStopOBC(boolean *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpStopConditions_DeStopConditions(boolean *data);
Std_ReturnType TSC_CtApOBC_Rte_Read_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset(IdtVoltageCorrectionOffset *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApOBC_Rte_Write_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(DCDC_CurrentReference data);
Std_ReturnType TSC_CtApOBC_Rte_Write_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed data);
Std_ReturnType TSC_CtApOBC_Rte_Write_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue(boolean data);
Std_ReturnType TSC_CtApOBC_Rte_Write_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC(boolean data);
Std_ReturnType TSC_CtApOBC_Rte_Write_PpInt_DCDC_CurrentReference_DCDC_CurrentReference(DCDC_CurrentReference data);
Std_ReturnType TSC_CtApOBC_Rte_Write_PpInt_DCDC_VoltageReference_DCDC_VoltageReference(DCDC_VoltageReference data);
Std_ReturnType TSC_CtApOBC_Rte_Write_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed data);
Std_ReturnType TSC_CtApOBC_Rte_Write_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V(SUP_CommandVDCLink_V data);
Std_ReturnType TSC_CtApOBC_Rte_Write_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus(SUP_RequestPFCStatus data);
Std_ReturnType TSC_CtApOBC_Rte_Write_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV(SUP_RequestStatusDCHV data);
Std_ReturnType TSC_CtApOBC_Rte_Write_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result data);
Std_ReturnType TSC_CtApOBC_Rte_Write_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault(boolean data);
Std_ReturnType TSC_CtApOBC_Rte_Write_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue(boolean data);
Std_ReturnType TSC_CtApOBC_Rte_Write_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue(boolean data);

/** Client server interfaces */
Std_ReturnType TSC_CtApOBC_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);
Std_ReturnType TSC_CtApOBC_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data);

/** Service interfaces */
Std_ReturnType TSC_CtApOBC_Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID);

/** SW-C local Calibration Parameters */
IdtMaxOutputDCHVCurrent  TSC_CtApOBC_Rte_CData_CalMaxOutputDCHVCurrent(void);
IdtMaxOutputDCHVVoltage  TSC_CtApOBC_Rte_CData_CalMaxOutputDCHVVoltage(void);
IdtMinDCHVOutputVoltage  TSC_CtApOBC_Rte_CData_CalMinDCHVOutputVoltage(void);
IdtTimeRelaysPrechargeClosed  TSC_CtApOBC_Rte_CData_CalTimeRelaysPrechargeClosed(void);
CalVDCLinkRequiredMonophasic  TSC_CtApOBC_Rte_CData_CalVDCLinkRequiredMonophasic(void);
IdtVDCLinkRequiredTriphasic  TSC_CtApOBC_Rte_CData_CalVDCLinkRequiredTriphasic(void);

/** Per Instance Memories */
boolean *TSC_CtApOBC_Rte_Pim_PimOBCShutdownPathFSP(void);
boolean *TSC_CtApOBC_Rte_Pim_PimOBCShutdownPathGPIO(void);



