/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApPLT.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApPLT_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpDutyControlPilot_DeDutyControlPilot(IdtDutyControlPilot *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(PFC_VPH23_Peak_V *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(PFC_VPH31_Peak_V *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(VCU_PosShuntJDD *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(IdtProximityDetectPhysicalValue *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data);
Std_ReturnType TSC_CtApPLT_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApPLT_Rte_Write_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(IdtELockSetPoint data);
Std_ReturnType TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeChargeError(boolean data);
Std_ReturnType TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeChargeInProgress(boolean data);
Std_ReturnType TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeEndOfCharge(boolean data);
Std_ReturnType TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeGuideManagement(boolean data);
Std_ReturnType TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeProgrammingCharge(boolean data);
Std_ReturnType TSC_CtApPLT_Rte_Write_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(boolean data);
Std_ReturnType TSC_CtApPLT_Rte_Write_PpPlantModeState_DePlantModeState(boolean data);
Std_ReturnType TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result(IdtPlantModeTestInfoDID_Result data);
Std_ReturnType TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State(IdtPlantModeTestInfoDID_State data);
Std_ReturnType TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase(IdtPlantModeTestInfoDID_Test data);
Std_ReturnType TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines(IdtPlantModeTestInfoDID_Test data);
Std_ReturnType TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty(IdtPlantModeTestInfoDID_Test data);
Std_ReturnType TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage(IdtPlantModeTestInfoDID_Test data);
Std_ReturnType TSC_CtApPLT_Rte_Write_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(IdtRECHARGE_HMI_STATE data);

/** Client server interfaces */
Std_ReturnType TSC_CtApPLT_Rte_Call_PpPlantModeDTC_OpPlantModeDTCReset(IdtPlantModeDTCNumber DTC_Id);
Std_ReturnType TSC_CtApPLT_Rte_Call_PpPlantModeDTC_OpPlantModeDTCSet(IdtPlantModeDTCNumber DTC_Id);
Std_ReturnType TSC_CtApPLT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);

/** Service interfaces */
Std_ReturnType TSC_CtApPLT_Rte_Call_PS_CpApPLT_PLTNvMNeed_SetRamBlockStatus(boolean RamBlockStatus);

/** Calibration Component Calibration Parameters */
IdtBatteryVoltMaxTest  TSC_CtApPLT_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(void);
IdtBatteryVoltMinTest  TSC_CtApPLT_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(void);
IdtVehStopMaxTest  TSC_CtApPLT_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(void);

/** SW-C local Calibration Parameters */
IdtGlobalTimeoutPlantMode  TSC_CtApPLT_Rte_CData_CalGlobalTimeoutPlantMode(void);
IdtProximityDetectPlantMode  TSC_CtApPLT_Rte_CData_CalMaxProximityDetectPlantMode(void);
IdtProximityDetectPlantMode  TSC_CtApPLT_Rte_CData_CalMinProximityDetectPlantMode(void);
IdtProximityVoltageStartingPlantMode  TSC_CtApPLT_Rte_CData_CalProximityVoltageStartingPlantMode(void);
IdtDebounceControlPilotPlantMode  TSC_CtApPLT_Rte_CData_CalDebounceControlPilotPlantMode(void);
IdtDebounceDCRelayVoltagePlantMode  TSC_CtApPLT_Rte_CData_CalDebounceDCRelayVoltagePlantMode(void);
IdtDebouncePhasePlantMode  TSC_CtApPLT_Rte_CData_CalDebouncePhasePlantMode(void);
IdtDebounceProximityPlantMode  TSC_CtApPLT_Rte_CData_CalDebounceProximityPlantMode(void);
IdtControlPilotDutyPlantMode  TSC_CtApPLT_Rte_CData_CalMaxControlPilotDutyPlantMode(void);
IdtDCRelayVoltagePlantMode  TSC_CtApPLT_Rte_CData_CalMaxDCRelayVoltagePlantMode(void);
IdtPhasePlantmode  TSC_CtApPLT_Rte_CData_CalMaxPhasePlantMode(void);
IdtMaxTimePlantMode  TSC_CtApPLT_Rte_CData_CalMaxTimePlantMode(void);
IdtControlPilotDutyPlantMode  TSC_CtApPLT_Rte_CData_CalMinControlPilotDutyPlantMode(void);
IdtDCRelayVoltagePlantMode  TSC_CtApPLT_Rte_CData_CalMinDCRelayVoltagePlantMode(void);
IdtPhasePlantmode  TSC_CtApPLT_Rte_CData_CalMinPhasePlantMode(void);
boolean  TSC_CtApPLT_Rte_CData_CalInhibitControlPilotDetection(void);
boolean  TSC_CtApPLT_Rte_CData_CalInhibitDCRelayVoltage(void);
boolean  TSC_CtApPLT_Rte_CData_CalInhibitPhaseVoltageDetection(void);
boolean  TSC_CtApPLT_Rte_CData_CalInhibitProximityDetection(void);
IdtPlantModeTestUTPlugin * TSC_CtApPLT_Rte_CData_PLTNvMNeed_DefaultValue(void);

/** Per Instance Memories */
IdtTimeoutPlantMode *TSC_CtApPLT_Rte_Pim_PimGlobalTimeoutPlantMode(void);
IdtTimeoutPlantMode *TSC_CtApPLT_Rte_Pim_PimTimeoutPlantMode(void);
boolean *TSC_CtApPLT_Rte_Pim_PimPlantModeControlPilot(void);
boolean *TSC_CtApPLT_Rte_Pim_PimPlantModeDCRelayVoltage(void);
boolean *TSC_CtApPLT_Rte_Pim_PimPlantModePhaseVoltage(void);
boolean *TSC_CtApPLT_Rte_Pim_PimPlantModeProximity(void);
Rte_DT_IdtPlantModeTestUTPlugin_0 *TSC_CtApPLT_Rte_Pim_PimPlantModeTestUTPlugin(void);



