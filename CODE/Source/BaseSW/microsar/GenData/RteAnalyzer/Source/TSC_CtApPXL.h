/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApPXL.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApPXL_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data);
Std_ReturnType TSC_CtApPXL_Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(IdtProximityDetectPhysicalValue *data);
Std_ReturnType TSC_CtApPXL_Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApPXL_Rte_Write_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati data);

/** Client server interfaces */
Std_ReturnType TSC_CtApPXL_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);

/** Service interfaces */
Std_ReturnType TSC_CtApPXL_Rte_Call_NvMService_AC3_SRBS_PXLNvBlockNeed_ReadBlock(dtRef_VOID DstPtr);
Std_ReturnType TSC_CtApPXL_Rte_Call_NvMService_AC3_SRBS_PXLNvBlockNeed_SetRamBlockStatus(boolean RamBlockStatus);
Std_ReturnType TSC_CtApPXL_Rte_Call_NvMService_AC3_SRBS_PXLNvBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr);

/** SW-C local Calibration Parameters */
IdtProximityDetectVoltThreshold  TSC_CtApPXL_Rte_CData_CalProximityDetectMaxVoltage(void);
IdtProximityDetectVoltThreshold  TSC_CtApPXL_Rte_CData_CalProximityDetectMinVoltage(void);
IdtProximityDetectVoltThreshold  TSC_CtApPXL_Rte_CData_CalProximityNotDetectMaxVoltage(void);
IdtProximityDetectVoltThreshold  TSC_CtApPXL_Rte_CData_CalProximityNotDetectMinVoltage(void);
IdtDebounceProxDetect  TSC_CtApPXL_Rte_CData_CalDebounceProxDetectConnected(void);
IdtDebounceProxDetect  TSC_CtApPXL_Rte_CData_CalDebounceProxDetectNotConnected(void);
IdtDebounceProxDetect  TSC_CtApPXL_Rte_CData_CalDebounceProxInvalid(void);

/** Per Instance Memories */
uint8 *TSC_CtApPXL_Rte_Pim_PimPXL_NvMRamMirror(void);
boolean *TSC_CtApPXL_Rte_Pim_PimProximityFailure(void);



