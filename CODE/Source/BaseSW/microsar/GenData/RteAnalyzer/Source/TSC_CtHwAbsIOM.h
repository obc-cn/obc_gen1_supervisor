/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtHwAbsIOM.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PiResetPLCPhysicalValue_DeResetPLCPhysicalValue(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue(boolean *data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(boolean *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtHwAbsIOM_Rte_Write_PpInputChargePushRaw_DeInputChargePushRaw(boolean data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Write_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue(boolean data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Write_PpExtRCDLineRaw_DeExtRCDLineRaw(boolean data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Write_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(boolean data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Write_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(boolean data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Write_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue(boolean data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Write_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue(boolean data);

/** Client server interfaces */
Std_ReturnType TSC_CtHwAbsIOM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data);

/** Service interfaces */
Std_ReturnType TSC_CtHwAbsIOM_Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Call_general_Core0_ActivateSupervisionEntity(WdgM_SupervisedEntityIdType SEID);
Std_ReturnType TSC_CtHwAbsIOM_Rte_Call_general_Core0_DeactivateSupervisionEntity(WdgM_SupervisedEntityIdType SEID);




