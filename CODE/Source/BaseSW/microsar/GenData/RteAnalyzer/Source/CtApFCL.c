/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApFCL.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApFCL
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApFCL>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   
 *
 * Dcm_OpStatusType
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CtApFCL.h"
#include "TSC_CtApFCL.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApFCL_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * ABS_VehSpd: Integer in interval [0...65535]
 *   Unit: [km/h], Factor: 0.01, Offset: 0
 * IdtAfts_DelayLed: Integer in interval [0...500]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtBatteryVoltMaxTest: Integer in interval [0...320]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtBatteryVoltMinTest: Integer in interval [0...320]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtChLedCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtExtChLedRGBCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtExtPlgLedrCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtLedFaultTime: Integer in interval [0...20]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtLedFeedbackPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtLedThresholdSCG: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtLedThresholdSCP: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 7000
 * IdtMaxNormalPlugLed: Integer in interval [0...1000]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtMinNormalPlugLed: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtPlgLedrCtrl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtPlugLedFeedbackPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtPlugLedMaxOCDetection: Integer in interval [0...1000]
 *   Unit: [mV], Factor: 10, Offset: 5000
 * IdtPlugLedMinOCDetection: Integer in interval [0...1000]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtPlugLedThresholdSCG: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtPlugLedThresholdSCP: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 7000
 * IdtVehStopMaxTest: Integer in interval [0...20]
 *   Unit: [km/h], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * IdtBatteryVoltageState: Enumeration of integer in interval [0...2] with enumerators
 *   BAT_VALID_RANGE (0U)
 *   BAT_OVERVOLTAGE (1U)
 *   BAT_UNDERVOLTAGE (2U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtHWEditionDetected: Enumeration of integer in interval [0...255] with enumerators
 *   HW_EDITION_UNKNOWN (0U)
 *   HW_EDITION_D21 (1U)
 *   HW_EDITION_D31 (2U)
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 *   POST_ONGOING (0U)
 *   POST_OK (1U)
 *   POST_NOK (2U)
 * VCU_EtatGmpHyb: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PWT_inactive (0U)
 *   Cx1_reserved (1U)
 *   Cx2_reserved (2U)
 *   Cx3_reserved (3U)
 *   Cx4_PWT_activation (4U)
 *   Cx5_reserved (5U)
 *   Cx6_reserved (6U)
 *   Cx7_reserved (7U)
 *   Cx8_active_PWT (8U)
 *   Cx9_reserved (9U)
 *   CxA_PWT_desactivation (10U)
 *   CxB_reserved (11U)
 *   CxC_PWT_in_hold_after_desactivation (12U)
 *   CxD_reserved (13U)
 *   CxE_PWT_at_rest (14U)
 *   CxF_reserved (15U)
 *
 * Array Types:
 * ============
 * Dcm_Data3ByteType: Array with 3 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint16 *Rte_Pim_PimLedBlueCounterOC(void)
 *   uint16 *Rte_Pim_PimLedBlueCounterSCG(void)
 *   uint16 *Rte_Pim_PimLedBlueCounterSCP(void)
 *   uint16 *Rte_Pim_PimLedGreenCounterOC(void)
 *   uint16 *Rte_Pim_PimLedGreenCounterSCG(void)
 *   uint16 *Rte_Pim_PimLedGreenCounterSCP(void)
 *   uint16 *Rte_Pim_PimLedPlugCounterOC(void)
 *   uint16 *Rte_Pim_PimLedPlugCounterSCG(void)
 *   uint16 *Rte_Pim_PimLedPlugCounterSCP(void)
 *   uint16 *Rte_Pim_PimLedRedCounterOC(void)
 *   uint16 *Rte_Pim_PimLedRedCounterSCG(void)
 *   uint16 *Rte_Pim_PimLedRedCounterSCP(void)
 *   boolean *Rte_Pim_PimLedBluePrefaultOC(void)
 *   boolean *Rte_Pim_PimLedBluePrefaultSCG(void)
 *   boolean *Rte_Pim_PimLedBluePrefaultSCP(void)
 *   boolean *Rte_Pim_PimLedGreenPrefaultOC(void)
 *   boolean *Rte_Pim_PimLedGreenPrefaultSCG(void)
 *   boolean *Rte_Pim_PimLedGreenPrefaultSCP(void)
 *   boolean *Rte_Pim_PimLedPlugPrefaultOC(void)
 *   boolean *Rte_Pim_PimLedPlugPrefaultSCG(void)
 *   boolean *Rte_Pim_PimLedPlugPrefaultSCP(void)
 *   boolean *Rte_Pim_PimLedRedPrefaultOC(void)
 *   boolean *Rte_Pim_PimLedRedPrefaultSCG(void)
 *   boolean *Rte_Pim_PimLedRedPrefaultSCP(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedBlueOff(void)
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedBlueOn(void)
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedGreenOff(void)
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedGreenOn(void)
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedPlugOff(void)
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedPlugOn(void)
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedRedOff(void)
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedRedOn(void)
 *   IdtLedThresholdSCG Rte_CData_CalLedBlueThresholdSCG(void)
 *   IdtLedThresholdSCP Rte_CData_CalLedBlueThresholdSCP(void)
 *   IdtLedThresholdSCG Rte_CData_CalLedGreenThresholdSCG(void)
 *   IdtLedThresholdSCP Rte_CData_CalLedGreenThresholdSCP(void)
 *   IdtLedThresholdSCG Rte_CData_CalLedRedThresholdSCG(void)
 *   IdtLedThresholdSCP Rte_CData_CalLedRedThresholdSCP(void)
 *   IdtMaxNormalPlugLed Rte_CData_CalMaxNormalPlugLed(void)
 *   IdtMinNormalPlugLed Rte_CData_CalMinNormalPlugLed(void)
 *   IdtPlugLedMaxOCDetection Rte_CData_CalPlugLedMaxOCDetection(void)
 *   IdtPlugLedMinOCDetection Rte_CData_CalPlugLedMinOCDetection(void)
 *   IdtPlugLedThresholdSCG Rte_CData_CalPlugLedThresholdSCG(void)
 *   IdtPlugLedThresholdSCP Rte_CData_CalPlugLedThresholdSCP(void)
 *   IdtLedFaultTime Rte_CData_CalLedBlueOCConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedBlueOCHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedBlueSCGConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedBlueSCGHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedBlueSCPConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedBlueSCPHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedGreenOCConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedGreenOCHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedGreenSCGConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedGreenSCGHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedGreenSCPConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedGreenSCPHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedPlugOCConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedPlugOCHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedPlugSCGConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedPlugSCGHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedPlugSCPConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedPlugSCPHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedRedOCConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedRedOCHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedRedSCGConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedRedSCGHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedRedSCPConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedRedSCPHealTime(void)
 *   boolean Rte_CData_CalEnableLedBlue(void)
 *   boolean Rte_CData_CalEnableLedGreen(void)
 *   boolean Rte_CData_CalEnableLedPlug(void)
 *   boolean Rte_CData_CalEnableLedRed(void)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtBatteryVoltMaxTest Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(void)
 *   IdtBatteryVoltMinTest Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(void)
 *   IdtVehStopMaxTest Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(void)
 *
 *********************************************************************************************************************/


#define CtApFCL_START_SEC_CODE
#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApFCL_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCL_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApFCL_CODE) RCtApFCL_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCL_init
 *********************************************************************************************************************/

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApFCL_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApFCL_task100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
 *   Std_ReturnType Rte_Read_PpChLedRGBCtl_DeChLedBrCtl(IdtChLedCtl *data)
 *   Std_ReturnType Rte_Read_PpChLedRGBCtl_DeChLedGrCtl(IdtChLedCtl *data)
 *   Std_ReturnType Rte_Read_PpChLedRGBCtl_DeChLedRrCtl(IdtChLedCtl *data)
 *   Std_ReturnType Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
 *   Std_ReturnType Rte_Read_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue *data)
 *   Std_ReturnType Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue *data)
 *   Std_ReturnType Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue *data)
 *   Std_ReturnType Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpExtChLedRGBCtl_DeExtChLedBrCtl(IdtExtChLedRGBCtl data)
 *   Std_ReturnType Rte_Write_PpExtChLedRGBCtl_DeExtChLedGrCtl(IdtExtChLedRGBCtl data)
 *   Std_ReturnType Rte_Write_PpExtChLedRGBCtl_DeExtChLedRrCtl(IdtExtChLedRGBCtl data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedBlueFaultOC(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedBlueFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedBlueFaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedGreenFaultOC(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedGreenFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedGreenFaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedRedFaultOC(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedRedFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedRedFaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpLedMonitoringConditions_DeBlueLedMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpLedMonitoringConditions_DeGreenLedMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpLedMonitoringConditions_DeRedLedMonitoringConditions(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCL_task100ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApFCL_CODE) RCtApFCL_task100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCL_task100ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtAppRCDECUState Read_PpAppRCDECUState_DeAppRCDECUState;
  IdtBatteryVolt Read_PpBatteryVolt_DeBatteryVolt;
  IdtBatteryVoltageState Read_PpBatteryVoltageState_DeBatteryVoltageState;
  IdtChLedCtl Read_PpChLedRGBCtl_DeChLedBrCtl;
  IdtChLedCtl Read_PpChLedRGBCtl_DeChLedGrCtl;
  IdtChLedCtl Read_PpChLedRGBCtl_DeChLedRrCtl;
  IdtPOST_Result Read_PpDCDC_POST_Result_DeDCDC_POST_Result;
  IdtHWEditionDetected Read_PpHWEditionDetected_DeHWEditionDetected;
  ABS_VehSpd Read_PpInt_ABS_VehSpd_ABS_VehSpd;
  VCU_EtatGmpHyb Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb;
  boolean Read_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue;
  IdtLedFeedbackPhysicalValue Read_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue;
  IdtLedFeedbackPhysicalValue Read_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue;
  IdtLedFeedbackPhysicalValue Read_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue;
  IdtPOST_Result Read_PpOBC_POST_Result_DeOBC_POST_Result;

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApFCL_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&Read_PpAppRCDECUState_DeAppRCDECUState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpBatteryVolt_DeBatteryVolt(&Read_PpBatteryVolt_DeBatteryVolt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(&Read_PpBatteryVoltageState_DeBatteryVoltageState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpChLedRGBCtl_DeChLedBrCtl(&Read_PpChLedRGBCtl_DeChLedBrCtl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpChLedRGBCtl_DeChLedGrCtl(&Read_PpChLedRGBCtl_DeChLedGrCtl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpChLedRGBCtl_DeChLedRrCtl(&Read_PpChLedRGBCtl_DeChLedRrCtl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(&Read_PpDCDC_POST_Result_DeDCDC_POST_Result); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpHWEditionDetected_DeHWEditionDetected(&Read_PpHWEditionDetected_DeHWEditionDetected); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(&Read_PpInt_ABS_VehSpd_ABS_VehSpd); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(&Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue(&Read_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(&Read_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(&Read_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(&Read_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(&Read_PpOBC_POST_Result_DeOBC_POST_Result); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue(Rte_InitValue_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpExtChLedRGBCtl_DeExtChLedBrCtl(Rte_InitValue_PpExtChLedRGBCtl_DeExtChLedBrCtl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpExtChLedRGBCtl_DeExtChLedGrCtl(Rte_InitValue_PpExtChLedRGBCtl_DeExtChLedGrCtl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpExtChLedRGBCtl_DeExtChLedRrCtl(Rte_InitValue_PpExtChLedRGBCtl_DeExtChLedRrCtl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedBlueFaultOC(Rte_InitValue_PpLedFaults_DeLedBlueFaultOC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedBlueFaultSCG(Rte_InitValue_PpLedFaults_DeLedBlueFaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedBlueFaultSCP(Rte_InitValue_PpLedFaults_DeLedBlueFaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedGreenFaultOC(Rte_InitValue_PpLedFaults_DeLedGreenFaultOC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedGreenFaultSCG(Rte_InitValue_PpLedFaults_DeLedGreenFaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedGreenFaultSCP(Rte_InitValue_PpLedFaults_DeLedGreenFaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedRedFaultOC(Rte_InitValue_PpLedFaults_DeLedRedFaultOC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedRedFaultSCG(Rte_InitValue_PpLedFaults_DeLedRedFaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedRedFaultSCP(Rte_InitValue_PpLedFaults_DeLedRedFaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpLedMonitoringConditions_DeBlueLedMonitoringConditions(Rte_InitValue_PpLedMonitoringConditions_DeBlueLedMonitoringConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpLedMonitoringConditions_DeGreenLedMonitoringConditions(Rte_InitValue_PpLedMonitoringConditions_DeGreenLedMonitoringConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpLedMonitoringConditions_DeRedLedMonitoringConditions(Rte_InitValue_PpLedMonitoringConditions_DeRedLedMonitoringConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApFCL_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
 *   Std_ReturnType Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
 *   Std_ReturnType Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpPlgLedrCtrl_DePlgLedrCtrl(IdtPlgLedrCtrl *data)
 *   Std_ReturnType Rte_Read_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(IdtPlugLedFeedbackPhysicalValue *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpExtPlgLedrCtl_DeExtPlgLedrCtl(IdtExtPlgLedrCtl data)
 *   Std_ReturnType Rte_Write_PpLedMonitoringConditions_DePlugLedMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpPlugLedFaults_DeLedPlugFaultOC(boolean data)
 *   Std_ReturnType Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCP(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCL_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApFCL_CODE) RCtApFCL_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCL_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtAppRCDECUState Read_PpAppRCDECUState_DeAppRCDECUState;
  IdtBatteryVolt Read_PpBatteryVolt_DeBatteryVolt;
  IdtBatteryVoltageState Read_PpBatteryVoltageState_DeBatteryVoltageState;
  IdtPOST_Result Read_PpDCDC_POST_Result_DeDCDC_POST_Result;
  IdtHWEditionDetected Read_PpHWEditionDetected_DeHWEditionDetected;
  ABS_VehSpd Read_PpInt_ABS_VehSpd_ABS_VehSpd;
  VCU_EtatGmpHyb Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb;
  IdtPOST_Result Read_PpOBC_POST_Result_DeOBC_POST_Result;
  IdtPlgLedrCtrl Read_PpPlgLedrCtrl_DePlgLedrCtrl;
  IdtPlugLedFeedbackPhysicalValue Read_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue;

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApFCL_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&Read_PpAppRCDECUState_DeAppRCDECUState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpBatteryVolt_DeBatteryVolt(&Read_PpBatteryVolt_DeBatteryVolt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(&Read_PpBatteryVoltageState_DeBatteryVoltageState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(&Read_PpDCDC_POST_Result_DeDCDC_POST_Result); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpHWEditionDetected_DeHWEditionDetected(&Read_PpHWEditionDetected_DeHWEditionDetected); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(&Read_PpInt_ABS_VehSpd_ABS_VehSpd); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(&Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(&Read_PpOBC_POST_Result_DeOBC_POST_Result); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpPlgLedrCtrl_DePlgLedrCtrl(&Read_PpPlgLedrCtrl_DePlgLedrCtrl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Read_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(&Read_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpExtPlgLedrCtl_DeExtPlgLedrCtl(Rte_InitValue_PpExtPlgLedrCtl_DeExtPlgLedrCtl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpLedMonitoringConditions_DePlugLedMonitoringConditions(Rte_InitValue_PpLedMonitoringConditions_DePlugLedMonitoringConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpPlugLedFaults_DeLedPlugFaultOC(Rte_InitValue_PpPlugLedFaults_DeLedPlugFaultOC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCG(Rte_InitValue_PpPlugLedFaults_DeLedPlugFaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCP(Rte_InitValue_PpPlugLedFaults_DeLedPlugFaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApFCL_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimLedBlueCounterOC;
  uint16 PimPimLedBlueCounterSCG;
  uint16 PimPimLedBlueCounterSCP;
  uint16 PimPimLedGreenCounterOC;
  uint16 PimPimLedGreenCounterSCG;
  uint16 PimPimLedGreenCounterSCP;
  uint16 PimPimLedPlugCounterOC;
  uint16 PimPimLedPlugCounterSCG;
  uint16 PimPimLedPlugCounterSCP;
  uint16 PimPimLedRedCounterOC;
  uint16 PimPimLedRedCounterSCG;
  uint16 PimPimLedRedCounterSCP;
  boolean PimPimLedBluePrefaultOC;
  boolean PimPimLedBluePrefaultSCG;
  boolean PimPimLedBluePrefaultSCP;
  boolean PimPimLedGreenPrefaultOC;
  boolean PimPimLedGreenPrefaultSCG;
  boolean PimPimLedGreenPrefaultSCP;
  boolean PimPimLedPlugPrefaultOC;
  boolean PimPimLedPlugPrefaultSCG;
  boolean PimPimLedPlugPrefaultSCP;
  boolean PimPimLedRedPrefaultOC;
  boolean PimPimLedRedPrefaultSCG;
  boolean PimPimLedRedPrefaultSCP;

  IdtAfts_DelayLed CalAfts_DelayLedBlueOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff_data;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn_data;
  IdtLedThresholdSCG CalLedBlueThresholdSCG_data;
  IdtLedThresholdSCP CalLedBlueThresholdSCP_data;
  IdtLedThresholdSCG CalLedGreenThresholdSCG_data;
  IdtLedThresholdSCP CalLedGreenThresholdSCP_data;
  IdtLedThresholdSCG CalLedRedThresholdSCG_data;
  IdtLedThresholdSCP CalLedRedThresholdSCP_data;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed_data;
  IdtMinNormalPlugLed CalMinNormalPlugLed_data;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection_data;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection_data;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG_data;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP_data;
  IdtLedFaultTime CalLedBlueOCConfirmTime_data;
  IdtLedFaultTime CalLedBlueOCHealTime_data;
  IdtLedFaultTime CalLedBlueSCGConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCGHealTime_data;
  IdtLedFaultTime CalLedBlueSCPConfirmTime_data;
  IdtLedFaultTime CalLedBlueSCPHealTime_data;
  IdtLedFaultTime CalLedGreenOCConfirmTime_data;
  IdtLedFaultTime CalLedGreenOCHealTime_data;
  IdtLedFaultTime CalLedGreenSCGConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCGHealTime_data;
  IdtLedFaultTime CalLedGreenSCPConfirmTime_data;
  IdtLedFaultTime CalLedGreenSCPHealTime_data;
  IdtLedFaultTime CalLedPlugOCConfirmTime_data;
  IdtLedFaultTime CalLedPlugOCHealTime_data;
  IdtLedFaultTime CalLedPlugSCGConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCGHealTime_data;
  IdtLedFaultTime CalLedPlugSCPConfirmTime_data;
  IdtLedFaultTime CalLedPlugSCPHealTime_data;
  IdtLedFaultTime CalLedRedOCConfirmTime_data;
  IdtLedFaultTime CalLedRedOCHealTime_data;
  IdtLedFaultTime CalLedRedSCGConfirmTime_data;
  IdtLedFaultTime CalLedRedSCGHealTime_data;
  IdtLedFaultTime CalLedRedSCPConfirmTime_data;
  IdtLedFaultTime CalLedRedSCPHealTime_data;
  boolean CalEnableLedBlue_data;
  boolean CalEnableLedGreen_data;
  boolean CalEnableLedPlug_data;
  boolean CalEnableLedRed_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimLedBlueCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC() = PimPimLedBlueCounterOC;
  PimPimLedBlueCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG() = PimPimLedBlueCounterSCG;
  PimPimLedBlueCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP() = PimPimLedBlueCounterSCP;
  PimPimLedGreenCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC() = PimPimLedGreenCounterOC;
  PimPimLedGreenCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG() = PimPimLedGreenCounterSCG;
  PimPimLedGreenCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP() = PimPimLedGreenCounterSCP;
  PimPimLedPlugCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC() = PimPimLedPlugCounterOC;
  PimPimLedPlugCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG() = PimPimLedPlugCounterSCG;
  PimPimLedPlugCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP() = PimPimLedPlugCounterSCP;
  PimPimLedRedCounterOC = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC() = PimPimLedRedCounterOC;
  PimPimLedRedCounterSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG() = PimPimLedRedCounterSCG;
  PimPimLedRedCounterSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP() = PimPimLedRedCounterSCP;
  PimPimLedBluePrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC() = PimPimLedBluePrefaultOC;
  PimPimLedBluePrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG() = PimPimLedBluePrefaultSCG;
  PimPimLedBluePrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP() = PimPimLedBluePrefaultSCP;
  PimPimLedGreenPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC() = PimPimLedGreenPrefaultOC;
  PimPimLedGreenPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG() = PimPimLedGreenPrefaultSCG;
  PimPimLedGreenPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP() = PimPimLedGreenPrefaultSCP;
  PimPimLedPlugPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC() = PimPimLedPlugPrefaultOC;
  PimPimLedPlugPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG() = PimPimLedPlugPrefaultSCG;
  PimPimLedPlugPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP() = PimPimLedPlugPrefaultSCP;
  PimPimLedRedPrefaultOC = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC() = PimPimLedRedPrefaultOC;
  PimPimLedRedPrefaultSCG = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG() = PimPimLedRedPrefaultSCG;
  PimPimLedRedPrefaultSCP = *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP();
  *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP() = PimPimLedRedPrefaultSCP;

  CalAfts_DelayLedBlueOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedBlueOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedGreenOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedPlugOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOff_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_DelayLedRedOn_data = TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinNormalPlugLed_data = TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMaxOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedMinOCDetection_data = TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCG_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalPlugLedThresholdSCP_data = TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedBlueSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedGreenSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedPlugSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedOCHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCGHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPConfirmTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLedRedSCPHealTime_data = TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedBlue_data = TSC_CtApFCL_Rte_CData_CalEnableLedBlue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedGreen_data = TSC_CtApFCL_Rte_CData_CalEnableLedGreen(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedPlug_data = TSC_CtApFCL_Rte_CData_CalEnableLedPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableLedRed_data = TSC_CtApFCL_Rte_CData_CalEnableLedRed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApFCL_STOP_SEC_CODE
#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApFCL_TestDefines(void)
{
  /* Enumeration Data Types */

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_42 = DCM_E_VMSCNC_0;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_43 = DCM_E_VMSCNC_1;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_44 = DCM_E_VMSCNC_2;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_45 = DCM_E_VMSCNC_3;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_46 = DCM_E_VMSCNC_4;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_47 = DCM_E_VMSCNC_5;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_48 = DCM_E_VMSCNC_6;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_49 = DCM_E_VMSCNC_7;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_50 = DCM_E_VMSCNC_8;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_51 = DCM_E_VMSCNC_9;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_52 = DCM_E_VMSCNC_A;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_53 = DCM_E_VMSCNC_B;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_54 = DCM_E_VMSCNC_C;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_55 = DCM_E_VMSCNC_D;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_56 = DCM_E_VMSCNC_E;

  Dcm_OpStatusType Test_Dcm_OpStatusType_V_1 = DCM_INITIAL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_2 = DCM_PENDING;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_3 = DCM_CANCEL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_4 = DCM_FORCE_RCRRP_OK;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_5 = DCM_FORCE_RCRRP_NOT_OK;

  IdtAppRCDECUState Test_IdtAppRCDECUState_V_1 = APP_STATE_0;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_2 = APP_STATE_1;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_3 = APP_STATE_2;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_4 = APP_STATE_3;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_5 = APP_STATE_4;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_6 = APP_STATE_5;

  IdtBatteryVoltageState Test_IdtBatteryVoltageState_V_1 = BAT_VALID_RANGE;
  IdtBatteryVoltageState Test_IdtBatteryVoltageState_V_2 = BAT_OVERVOLTAGE;
  IdtBatteryVoltageState Test_IdtBatteryVoltageState_V_3 = BAT_UNDERVOLTAGE;

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;

  IdtHWEditionDetected Test_IdtHWEditionDetected_V_1 = HW_EDITION_UNKNOWN;
  IdtHWEditionDetected Test_IdtHWEditionDetected_V_2 = HW_EDITION_D21;
  IdtHWEditionDetected Test_IdtHWEditionDetected_V_3 = HW_EDITION_D31;

  IdtPOST_Result Test_IdtPOST_Result_V_1 = POST_ONGOING;
  IdtPOST_Result Test_IdtPOST_Result_V_2 = POST_OK;
  IdtPOST_Result Test_IdtPOST_Result_V_3 = POST_NOK;

  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_1 = Cx0_PWT_inactive;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_2 = Cx1_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_3 = Cx2_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_4 = Cx3_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_5 = Cx4_PWT_activation;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_6 = Cx5_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_7 = Cx6_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_8 = Cx7_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_9 = Cx8_active_PWT;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_10 = Cx9_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_11 = CxA_PWT_desactivation;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_12 = CxB_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_13 = CxC_PWT_in_hold_after_desactivation;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_14 = CxD_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_15 = CxE_PWT_at_rest;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_16 = CxF_reserved;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
