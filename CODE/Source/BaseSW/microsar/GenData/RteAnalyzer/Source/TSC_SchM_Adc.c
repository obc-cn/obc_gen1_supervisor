/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_Adc.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "SchM_Adc.h"
#include "TSC_SchM_Adc.h"
void TSC_Adc_SchM_Enter_Adc_DisableHwTrig(void)
{
  SchM_Enter_Adc_DisableHwTrig();
}
void TSC_Adc_SchM_Exit_Adc_DisableHwTrig(void)
{
  SchM_Exit_Adc_DisableHwTrig();
}
void TSC_Adc_SchM_Enter_Adc_EnableHwTrig(void)
{
  SchM_Enter_Adc_EnableHwTrig();
}
void TSC_Adc_SchM_Exit_Adc_EnableHwTrig(void)
{
  SchM_Exit_Adc_EnableHwTrig();
}
void TSC_Adc_SchM_Enter_Adc_GetGrpStatus(void)
{
  SchM_Enter_Adc_GetGrpStatus();
}
void TSC_Adc_SchM_Exit_Adc_GetGrpStatus(void)
{
  SchM_Exit_Adc_GetGrpStatus();
}
void TSC_Adc_SchM_Enter_Adc_GetStreamLastPtr(void)
{
  SchM_Enter_Adc_GetStreamLastPtr();
}
void TSC_Adc_SchM_Exit_Adc_GetStreamLastPtr(void)
{
  SchM_Exit_Adc_GetStreamLastPtr();
}
void TSC_Adc_SchM_Enter_Adc_PopQueue(void)
{
  SchM_Enter_Adc_PopQueue();
}
void TSC_Adc_SchM_Exit_Adc_PopQueue(void)
{
  SchM_Exit_Adc_PopQueue();
}
void TSC_Adc_SchM_Enter_Adc_PushQueue(void)
{
  SchM_Enter_Adc_PushQueue();
}
void TSC_Adc_SchM_Exit_Adc_PushQueue(void)
{
  SchM_Exit_Adc_PushQueue();
}
void TSC_Adc_SchM_Enter_Adc_ReadGroup(void)
{
  SchM_Enter_Adc_ReadGroup();
}
void TSC_Adc_SchM_Exit_Adc_ReadGroup(void)
{
  SchM_Exit_Adc_ReadGroup();
}
void TSC_Adc_SchM_Enter_Adc_ScheduleNext(void)
{
  SchM_Enter_Adc_ScheduleNext();
}
void TSC_Adc_SchM_Exit_Adc_ScheduleNext(void)
{
  SchM_Exit_Adc_ScheduleNext();
}
void TSC_Adc_SchM_Enter_Adc_ScheduleStart(void)
{
  SchM_Enter_Adc_ScheduleStart();
}
void TSC_Adc_SchM_Exit_Adc_ScheduleStart(void)
{
  SchM_Exit_Adc_ScheduleStart();
}
void TSC_Adc_SchM_Enter_Adc_ScheduleStop(void)
{
  SchM_Enter_Adc_ScheduleStop();
}
void TSC_Adc_SchM_Exit_Adc_ScheduleStop(void)
{
  SchM_Exit_Adc_ScheduleStop();
}
void TSC_Adc_SchM_Enter_Adc_StartGroup(void)
{
  SchM_Enter_Adc_StartGroup();
}
void TSC_Adc_SchM_Exit_Adc_StartGroup(void)
{
  SchM_Exit_Adc_StartGroup();
}
void TSC_Adc_SchM_Enter_Adc_StopGroup(void)
{
  SchM_Enter_Adc_StopGroup();
}
void TSC_Adc_SchM_Exit_Adc_StopGroup(void)
{
  SchM_Exit_Adc_StopGroup();
}
