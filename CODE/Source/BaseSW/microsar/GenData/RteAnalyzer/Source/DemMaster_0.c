/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  DemMaster_0.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  DemMaster_0
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <DemMaster_0>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_DTCFormatType
 *   
 *
 * Dem_DTCGroupType
 *   
 *
 * Dem_DTCKindType
 *   
 *
 * Dem_DTCOriginType
 *   
 *
 * Dem_DTCSeverityType
 *   
 *
 * Dem_DTCStatusMaskType
 *   
 *
 * Dem_DTRControlType
 *   
 *
 * Dem_DebounceResetStatusType
 *   
 *
 * Dem_DebouncingStateType
 *   
 *
 * Dem_EventIdType
 *   
 *
 * Dem_EventStatusType
 *   
 *
 * Dem_IndicatorStatusType
 *   
 *
 * Dem_InitMonitorReasonType
 *   
 *
 * Dem_IumprDenomCondIdType
 *   
 *
 * Dem_IumprDenomCondStatusType
 *   
 *
 * Dem_IumprReadinessGroupType
 *   
 *
 * Dem_MonitorStatusType
 *   
 *
 * Dem_OperationCycleStateType
 *   
 *
 * Dem_RatioIdType
 *   
 *
 * Dem_UdsStatusByteType
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_DemMaster_0.h"
#include "TSC_DemMaster_0.h"
#include "SchM_Dem.h"
#include "TSC_SchM_Dem.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void DemMaster_0_TestDefines(void);

typedef P2FUNC(Std_ReturnType, RTE_CODE, FncPtrType)(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Dem_DTCGroupType: Integer in interval [0...16777215]
 * Dem_DTCStatusMaskType: Integer in interval [0...255]
 * Dem_EventIdType: Integer in interval [1...65535]
 * Dem_RatioIdType: Integer in interval [1...65535]
 * boolean: Boolean (standard type)
 * sint8: Integer in interval [-128...127] (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dem_DTCFormatType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_DTC_FORMAT_OBD (0U)
 *   DEM_DTC_FORMAT_UDS (1U)
 *   DEM_DTC_FORMAT_J1939 (2U)
 * Dem_DTCKindType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_DTC_KIND_ALL_DTCS (1U)
 *   DEM_DTC_KIND_EMISSION_REL_DTCS (2U)
 * Dem_DTCOriginType: Enumeration of integer in interval [0...65535] with enumerators
 *   DEM_DTC_ORIGIN_PRIMARY_MEMORY (1U)
 *   DEM_DTC_ORIGIN_MIRROR_MEMORY (2U)
 *   DEM_DTC_ORIGIN_PERMANENT_MEMORY (3U)
 *   DEM_DTC_ORIGIN_OBD_RELEVANT_MEMORY (4U)
 *   DEM_DTC_ORIGIN_SECONDARY_MEMORY (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_10 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_11 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_12 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_13 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_14 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_15 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_16 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_17 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_18 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_19 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_1A (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_1B (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_1C (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_1D (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_1E (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_1F (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_20 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_21 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_22 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_23 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_24 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_25 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_26 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_27 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_28 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_29 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_2A (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_2B (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_2C (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_2D (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_2E (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_2F (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_30 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_31 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_32 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_33 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_34 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_35 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_36 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_37 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_38 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_39 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_3A (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_3B (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_3C (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_3D (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_3E (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_3F (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_40 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_41 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_42 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_43 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_44 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_45 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_46 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_47 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_48 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_49 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_4A (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_4B (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_4C (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_4D (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_4E (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_4F (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_50 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_51 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_52 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_53 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_54 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_55 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_56 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_57 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_58 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_59 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_5A (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_5B (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_5C (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_5D (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_5E (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_5F (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_60 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_61 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_62 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_63 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_64 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_65 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_66 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_67 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_68 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_69 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_6A (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_6B (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_6C (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_6D (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_6E (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_6F (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_70 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_71 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_72 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_73 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_74 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_75 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_76 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_77 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_78 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_79 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_7A (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_7B (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_7C (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_7D (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_7E (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_7F (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_80 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_81 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_82 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_83 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_84 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_85 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_86 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_87 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_88 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_89 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_8A (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_8B (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_8C (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_8D (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_8E (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_8F (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_90 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_91 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_92 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_93 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_94 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_95 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_96 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_97 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_98 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_99 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_9A (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_9B (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_9C (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_9D (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_9E (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_9F (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A0 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A1 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A2 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A3 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A4 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A5 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A6 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A7 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A8 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A9 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_AA (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_AB (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_AC (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_AD (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_AE (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_AF (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B0 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B1 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B2 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B3 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B4 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B5 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B6 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B7 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B8 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B9 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_BA (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_BB (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_BC (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_BD (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_BE (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_BF (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C0 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C1 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C2 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C3 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C4 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C5 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C6 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C7 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C8 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C9 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_CA (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_CB (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_CC (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_CD (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_CE (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_CF (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D0 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D1 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D2 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D3 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D4 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D5 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D6 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D7 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D8 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D9 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_DA (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_DB (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_DC (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_DD (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_DE (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_DF (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E0 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E1 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E2 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E3 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E4 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E5 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E6 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E7 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E8 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E9 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_EA (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_EB (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_EC (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_ED (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_EE (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_EF (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F0 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F1 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F2 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F3 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F4 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F5 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F6 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F7 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F8 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F9 (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_FA (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_FB (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_FC (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_FD (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_FE (5U)
 *   DEM_DTC_ORIGIN_USERDEFINED_MEMORY_FF (5U)
 * Dem_DTCSeverityType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_SEVERITY_NO_SEVERITY (0U)
 *   DTC_NO_SEVERITY_BflMask 255U (0b11111111)
 *   DTC_NO_SEVERITY_BflPn 0
 *   DTC_NO_SEVERITY_BflLn 8
 *   DEM_SEVERITY_WWHOBD_CLASS_NO_CLASS (1U)
 *   DTC_CLASS_BflMask 31U (0b00011111)
 *   DTC_CLASS_BflPn 0
 *   DTC_CLASS_BflLn 5
 *   DEM_SEVERITY_WWHOBD_CLASS_A (2U)
 *   DTC_CLASS_BflMask 31U (0b00011111)
 *   DTC_CLASS_BflPn 0
 *   DTC_CLASS_BflLn 5
 *   DEM_SEVERITY_WWHOBD_CLASS_B1 (4U)
 *   DTC_CLASS_BflMask 31U (0b00011111)
 *   DTC_CLASS_BflPn 0
 *   DTC_CLASS_BflLn 5
 *   DEM_SEVERITY_WWHOBD_CLASS_B2 (8U)
 *   DTC_CLASS_BflMask 31U (0b00011111)
 *   DTC_CLASS_BflPn 0
 *   DTC_CLASS_BflLn 5
 *   DEM_SEVERITY_WWHOBD_CLASS_C (16U)
 *   DTC_CLASS_BflMask 31U (0b00011111)
 *   DTC_CLASS_BflPn 0
 *   DTC_CLASS_BflLn 5
 *   DEM_SEVERITY_MAINTENANCE_ONLY (32U)
 *   DTC_SEVERITY_BflMask 224U (0b11100000)
 *   DTC_SEVERITY_BflPn 5
 *   DTC_SEVERITY_BflLn 3
 *   DEM_SEVERITY_CHECK_AT_NEXT_HALT (64U)
 *   DTC_SEVERITY_BflMask 224U (0b11100000)
 *   DTC_SEVERITY_BflPn 5
 *   DTC_SEVERITY_BflLn 3
 *   DEM_SEVERITY_CHECK_IMMEDIATELY (128U)
 *   DTC_SEVERITY_BflMask 224U (0b11100000)
 *   DTC_SEVERITY_BflPn 5
 *   DTC_SEVERITY_BflLn 3
 * Dem_DTRControlType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_DTR_CTL_NORMAL (0U)
 *   DEM_DTR_CTL_NO_MAX (1U)
 *   DEM_DTR_CTL_NO_MIN (2U)
 *   DEM_DTR_CTL_RESET (3U)
 *   DEM_DTR_CTL_INVISIBLE (4U)
 * Dem_DebounceResetStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_DEBOUNCE_STATUS_FREEZE (0U)
 *   DEM_DEBOUNCE_STATUS_RESET (1U)
 * Dem_DebouncingStateType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_TEMPORARILY_DEFECTIVE (1U)
 *   DEM_TEMPORARILY_DEFECTIVE_BflMask 1U (0b00000001)
 *   DEM_TEMPORARILY_DEFECTIVE_BflPn 0
 *   DEM_TEMPORARILY_DEFECTIVE_BflLn 1
 *   DEM_FINALLY_DEFECTIVE (2U)
 *   DEM_FINALLY_DEFECTIVE_BflMask 2U (0b00000010)
 *   DEM_FINALLY_DEFECTIVE_BflPn 1
 *   DEM_FINALLY_DEFECTIVE_BflLn 1
 *   DEM_TEMPORARILY_HEALED (4U)
 *   DEM_TEMPORARILY_HEALED_BflMask 4U (0b00000100)
 *   DEM_TEMPORARILY_HEALED_BflPn 2
 *   DEM_TEMPORARILY_HEALED_BflLn 1
 *   DEM_TEST_COMPLETE (8U)
 *   DEM_TEST_COMPLETE_BflMask 8U (0b00001000)
 *   DEM_TEST_COMPLETE_BflPn 3
 *   DEM_TEST_COMPLETE_BflLn 1
 *   DEM_DTR_UPDATE (16U)
 *   DEM_DTR_UPDATE_BflMask 16U (0b00010000)
 *   DEM_DTR_UPDATE_BflPn 4
 *   DEM_DTR_UPDATE_BflLn 1
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * Dem_IndicatorStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_INDICATOR_OFF (0U)
 *   DEM_INDICATOR_CONTINUOUS (1U)
 *   DEM_INDICATOR_BLINKING (2U)
 *   DEM_INDICATOR_BLINK_CONT (3U)
 *   DEM_INDICATOR_SLOW_FLASH (4U)
 *   DEM_INDICATOR_FAST_FLASH (5U)
 *   DEM_INDICATOR_ON_DEMAND (6U)
 *   DEM_INDICATOR_SHORT (7U)
 * Dem_InitMonitorReasonType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_INIT_MONITOR_CLEAR (1U)
 *   DEM_INIT_MONITOR_RESTART (2U)
 *   DEM_INIT_MONITOR_REENABLED (3U)
 *   DEM_INIT_MONITOR_STORAGE_REENABLED (4U)
 * Dem_IumprDenomCondIdType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_IUMPR_GENERAL_DENOMINATOR (1U)
 *   DEM_IUMPR_DEN_COND_COLDSTART (2U)
 *   DEM_IUMPR_DEN_COND_EVAP (3U)
 *   DEM_IUMPR_DEN_COND_500MI (4U)
 * Dem_IumprDenomCondStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_IUMPR_DEN_STATUS_NOT_REACHED (0U)
 *   DEM_IUMPR_DEN_STATUS_REACHED (1U)
 *   DEM_IUMPR_DEN_STATUS_INHIBITED (2U)
 * Dem_IumprReadinessGroupType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_IUMPR_BOOSTPRS (0U)
 *   DEM_IUMPR_CAT1 (1U)
 *   DEM_IUMPR_CAT2 (2U)
 *   DEM_IUMPR_EGR (3U)
 *   DEM_IUMPR_EGSENSOR (4U)
 *   DEM_IUMPR_EVAP (5U)
 *   DEM_IUMPR_NMHCCAT (6U)
 *   DEM_IUMPR_NOXADSORB (7U)
 *   DEM_IUMPR_NOXCAT (8U)
 *   DEM_IUMPR_OXS1 (9U)
 *   DEM_IUMPR_OXS2 (10U)
 *   DEM_IUMPR_PMFILTER (11U)
 *   DEM_IUMPR_PRIVATE (12U)
 *   DEM_IUMPR_SAIR (13U)
 *   DEM_IUMPR_SECOXS1 (14U)
 *   DEM_IUMPR_SECOXS2 (15U)
 *   DEM_IUMPR_FLSYS (16U)
 *   DEM_IUMPR_AFRI1 (17U)
 *   DEM_IUMPR_AFRI2 (18U)
 *   DEM_IUMPR_PF1 (19U)
 *   DEM_IUMPR_PF2 (20U)
 *   DEM_IUMPR_ALLGROUPS (255U)
 * Dem_MonitorStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_MONITOR_STATUS_TF (1U)
 *   DEM_MONITOR_STATUS_TF_BflMask 1U (0b00000001)
 *   DEM_MONITOR_STATUS_TF_BflPn 0
 *   DEM_MONITOR_STATUS_TF_BflLn 1
 *   DEM_MONITOR_STATUS_TNCTOC (2U)
 *   DEM_MONITOR_STATUS_TNCTOC_BflMask 2U (0b00000010)
 *   DEM_MONITOR_STATUS_TNCTOC_BflPn 1
 *   DEM_MONITOR_STATUS_TNCTOC_BflLn 1
 * Dem_OperationCycleStateType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_CYCLE_STATE_START (0U)
 *   DEM_CYCLE_STATE_END (1U)
 * Dem_UdsStatusByteType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_UDS_STATUS_TF (1U)
 *   DEM_UDS_STATUS_TF_BflMask 1U (0b00000001)
 *   DEM_UDS_STATUS_TF_BflPn 0
 *   DEM_UDS_STATUS_TF_BflLn 1
 *   DEM_UDS_STATUS_TFTOC (2U)
 *   DEM_UDS_STATUS_TFTOC_BflMask 2U (0b00000010)
 *   DEM_UDS_STATUS_TFTOC_BflPn 1
 *   DEM_UDS_STATUS_TFTOC_BflLn 1
 *   DEM_UDS_STATUS_PDTC (4U)
 *   DEM_UDS_STATUS_PDTC_BflMask 4U (0b00000100)
 *   DEM_UDS_STATUS_PDTC_BflPn 2
 *   DEM_UDS_STATUS_PDTC_BflLn 1
 *   DEM_UDS_STATUS_CDTC (8U)
 *   DEM_UDS_STATUS_CDTC_BflMask 8U (0b00001000)
 *   DEM_UDS_STATUS_CDTC_BflPn 3
 *   DEM_UDS_STATUS_CDTC_BflLn 1
 *   DEM_UDS_STATUS_TNCSLC (16U)
 *   DEM_UDS_STATUS_TNCSLC_BflMask 16U (0b00010000)
 *   DEM_UDS_STATUS_TNCSLC_BflPn 4
 *   DEM_UDS_STATUS_TNCSLC_BflLn 1
 *   DEM_UDS_STATUS_TFSLC (32U)
 *   DEM_UDS_STATUS_TFSLC_BflMask 32U (0b00100000)
 *   DEM_UDS_STATUS_TFSLC_BflPn 5
 *   DEM_UDS_STATUS_TFSLC_BflLn 1
 *   DEM_UDS_STATUS_TNCTOC (64U)
 *   DEM_UDS_STATUS_TNCTOC_BflMask 64U (0b01000000)
 *   DEM_UDS_STATUS_TNCTOC_BflPn 6
 *   DEM_UDS_STATUS_TNCTOC_BflLn 1
 *   DEM_UDS_STATUS_WIR (128U)
 *   DEM_UDS_STATUS_WIR_BflMask 128U (0b10000000)
 *   DEM_UDS_STATUS_WIR_BflPn 7
 *   DEM_UDS_STATUS_WIR_BflLn 1
 *
 * Array Types:
 * ============
 * DataArrayType_uint8_1: Array with 1 element(s) of type uint8
 * DataArrayType_uint8_2: Array with 2 element(s) of type uint8
 * DataArrayType_uint8_4: Array with 4 element(s) of type uint8
 * Dem_MaxDataValueType: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


#define DemMaster_0_START_SEC_CODE
#include "DemMaster_0_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ClearDTC
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ClearDTC> of PortPrototype <ClearDTC_DemClient_CDD>
 *   - triggered by server invocation for OperationPrototype <ClearDTC> of PortPrototype <ClearDTC_DemClient_DCM>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_ClearDTC(void)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_ClearDTC_DEM_CLEAR_BUSY
 *   RTE_E_ClearDTC_DEM_CLEAR_FAILED
 *   RTE_E_ClearDTC_DEM_CLEAR_MEMORY_ERROR
 *   RTE_E_ClearDTC_DEM_PENDING
 *   RTE_E_ClearDTC_DEM_WRONG_DTC
 *   RTE_E_ClearDTC_DEM_WRONG_DTCORIGIN
 *   RTE_E_ClearDTC_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ClearDTC_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_ClearDTC(uint8 parg0) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_ClearDTC (returns application error)
 *********************************************************************************************************************/

  DemMaster_0_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Dem_MasterMainFunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x056216_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x056317_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x0a0804_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x0a084b_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x0a9464_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x0af864_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x0cf464_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x108093_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x10c413_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x10c512_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x10c613_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x10c713_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x120a11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x120a12_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x120b11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x120b12_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x120c64_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x120c98_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x120d64_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x120d98_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12d711_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12d712_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12d713_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12d811_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12d812_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12d813_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12d911_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12d912_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12d913_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12da11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12da12_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12da13_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12db12_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12dc11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12dd12_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12de11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12df13_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12e012_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12e111_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12e213_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12e319_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12e712_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12e811_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12e912_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12ea11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12f316_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x12f917_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x13e919_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x166c64_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x179e11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x179e12_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x179f11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x179f12_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x1a0064_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x1a7104_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x1a714b_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0x1a7172_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0xc07988_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0xc08913_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0xd18787_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0xd1a087_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0xd20781_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0xd2a081_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0xd38782_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0xd38783_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0xe00081_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0xe00087_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0xe00214_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBFaultDetectCtr_DTC_0xe00362_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D407_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D407_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D40C_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D40C_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D49C_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_4
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D49C_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D4CA_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_4
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D4CA_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D5CF_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_2
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D5CF_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D5D1_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_2
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D5D1_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D805_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_2
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D805_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D806_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D806_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D807_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_2
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D807_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D808_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D808_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D809_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_2
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D809_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D80C_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_2
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D80C_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D822_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_2
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D822_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D824_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_2
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D824_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D825_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D825_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D827_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D827_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D828_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D828_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D829_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D829_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D82B_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_2
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D82B_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D82C_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D82C_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D82D_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D82D_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D82E_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D82E_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D82F_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D82F_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D831_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D831_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D83B_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D83B_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D83C_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D83C_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D83D_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D83D_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D83E_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D83E_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D83F_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D83F_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D840_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D840_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D843_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D843_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D844_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D844_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D845_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_2
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D845_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D846_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D846_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D84A_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D84A_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D84B_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D84B_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D84C_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D84C_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D84D_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D84D_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D84E_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D84E_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D84F_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D84F_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D850_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D850_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D851_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D851_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D852_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D852_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D853_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D853_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D854_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D854_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D855_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D855_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D8E9_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D8E9_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_D8EB_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_2
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_D8EB_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE60_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE60_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE61_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE61_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE62_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE62_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE63_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_4
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE63_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE64_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_4
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE64_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE65_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE65_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE66_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE66_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE67_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_4
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE67_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE68_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE68_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE69_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE69_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE6A_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE6A_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE6B_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE6B_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE6C_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE6C_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE6D_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE6D_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE6E_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE6E_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE6F_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE6F_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE70_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE70_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE71_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_2
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE71_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE72_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE72_E_NOT_OK
 *   Std_ReturnType Rte_Call_CBReadData_DemDataClass_FE73_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_2
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CSDataServices_DemDataClass_FE73_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_MasterMainFunction_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DemMaster_0_CODE) Dem_MasterMainFunction(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_MasterMainFunction
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  sint8 Call_CBFaultDetectCtr_DTC_0x056216_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x056317_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x0a0804_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x0a084b_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x0a9464_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x0af864_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x0cf464_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x108093_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x10c413_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x10c512_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x10c613_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x10c713_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x120a11_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x120a12_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x120b11_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x120b12_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x120c64_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x120c98_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x120d64_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x120d98_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12d711_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12d712_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12d713_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12d811_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12d812_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12d813_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12d911_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12d912_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12d913_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12da11_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12da12_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12da13_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12db12_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12dc11_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12dd12_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12de11_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12df13_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12e012_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12e111_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12e213_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12e319_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12e712_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12e811_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12e912_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12ea11_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12f316_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x12f917_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x13e919_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x166c64_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x179e11_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x179e12_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x179f11_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x179f12_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x1a0064_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x1a7104_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x1a714b_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0x1a7172_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0xc07988_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0xc08913_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0xd18787_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0xd1a087_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0xd20781_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0xd2a081_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0xd38782_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0xd38783_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0xe00081_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0xe00087_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0xe00214_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  sint8 Call_CBFaultDetectCtr_DTC_0xe00362_GetFaultDetectionCounter_FaultDetectionCounter = 0;
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D407_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D40C_ReadData_Data = {
  0U
};
  DataArrayType_uint8_4 Call_CBReadData_DemDataClass_D49C_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  DataArrayType_uint8_4 Call_CBReadData_DemDataClass_D4CA_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  DataArrayType_uint8_2 Call_CBReadData_DemDataClass_D5CF_ReadData_Data = {
  0U, 0U
};
  DataArrayType_uint8_2 Call_CBReadData_DemDataClass_D5D1_ReadData_Data = {
  0U, 0U
};
  DataArrayType_uint8_2 Call_CBReadData_DemDataClass_D805_ReadData_Data = {
  0U, 0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D806_ReadData_Data = {
  0U
};
  DataArrayType_uint8_2 Call_CBReadData_DemDataClass_D807_ReadData_Data = {
  0U, 0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D808_ReadData_Data = {
  0U
};
  DataArrayType_uint8_2 Call_CBReadData_DemDataClass_D809_ReadData_Data = {
  0U, 0U
};
  DataArrayType_uint8_2 Call_CBReadData_DemDataClass_D80C_ReadData_Data = {
  0U, 0U
};
  DataArrayType_uint8_2 Call_CBReadData_DemDataClass_D822_ReadData_Data = {
  0U, 0U
};
  DataArrayType_uint8_2 Call_CBReadData_DemDataClass_D824_ReadData_Data = {
  0U, 0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D825_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D827_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D828_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D829_ReadData_Data = {
  0U
};
  DataArrayType_uint8_2 Call_CBReadData_DemDataClass_D82B_ReadData_Data = {
  0U, 0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D82C_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D82D_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D82E_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D82F_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D831_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D83B_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D83C_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D83D_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D83E_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D83F_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D840_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D843_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D844_ReadData_Data = {
  0U
};
  DataArrayType_uint8_2 Call_CBReadData_DemDataClass_D845_ReadData_Data = {
  0U, 0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D846_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D84A_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D84B_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D84C_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D84D_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D84E_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D84F_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D850_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D851_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D852_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D853_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D854_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D855_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_D8E9_ReadData_Data = {
  0U
};
  DataArrayType_uint8_2 Call_CBReadData_DemDataClass_D8EB_ReadData_Data = {
  0U, 0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE60_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE61_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE62_ReadData_Data = {
  0U
};
  DataArrayType_uint8_4 Call_CBReadData_DemDataClass_FE63_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  DataArrayType_uint8_4 Call_CBReadData_DemDataClass_FE64_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE65_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE66_ReadData_Data = {
  0U
};
  DataArrayType_uint8_4 Call_CBReadData_DemDataClass_FE67_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE68_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE69_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE6A_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE6B_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE6C_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE6D_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE6E_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE6F_ReadData_Data = {
  0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE70_ReadData_Data = {
  0U
};
  DataArrayType_uint8_2 Call_CBReadData_DemDataClass_FE71_ReadData_Data = {
  0U, 0U
};
  DataArrayType_uint8_1 Call_CBReadData_DemDataClass_FE72_ReadData_Data = {
  0U
};
  DataArrayType_uint8_2 Call_CBReadData_DemDataClass_FE73_ReadData_Data = {
  0U, 0U
};

  /**********************************************************
  * Direct Function Accesses and Take Addresses of Functions
  **********************************************************/

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x056216_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x056216_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x056216_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x056317_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x056317_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x056317_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0a0804_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0a0804_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x0a0804_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0a084b_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0a084b_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x0a084b_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0a9464_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0a9464_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x0a9464_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0af864_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0af864_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x0af864_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0cf464_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0cf464_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x0cf464_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x108093_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x108093_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x108093_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x10c413_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x10c413_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x10c413_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x10c512_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x10c512_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x10c512_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x10c613_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x10c613_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x10c613_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x10c713_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x10c713_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x10c713_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120a11_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120a11_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x120a11_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120a12_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120a12_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x120a12_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120b11_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120b11_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x120b11_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120b12_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120b12_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x120b12_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120c64_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120c64_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x120c64_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120c98_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120c98_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x120c98_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120d64_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120d64_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x120d64_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120d98_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120d98_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x120d98_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d711_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d711_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12d711_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d712_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d712_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12d712_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d713_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d713_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12d713_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d811_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d811_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12d811_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d812_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d812_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12d812_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d813_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d813_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12d813_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d911_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d911_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12d911_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d912_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d912_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12d912_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d913_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d913_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12d913_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12da11_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12da11_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12da11_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12da12_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12da12_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12da12_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12da13_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12da13_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12da13_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12db12_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12db12_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12db12_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12dc11_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12dc11_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12dc11_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12dd12_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12dd12_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12dd12_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12de11_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12de11_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12de11_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12df13_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12df13_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12df13_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e012_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e012_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12e012_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e111_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e111_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12e111_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e213_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e213_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12e213_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e319_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e319_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12e319_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e712_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e712_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12e712_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e811_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e811_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12e811_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e912_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e912_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12e912_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12ea11_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12ea11_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12ea11_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12f316_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12f316_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12f316_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12f917_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12f917_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x12f917_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x13e919_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x13e919_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x13e919_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x166c64_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x166c64_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x166c64_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x179e11_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x179e11_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x179e11_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x179e12_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x179e12_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x179e12_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x179f11_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x179f11_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x179f11_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x179f12_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x179f12_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x179f12_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x1a0064_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x1a0064_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x1a0064_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x1a7104_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x1a7104_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x1a7104_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x1a714b_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x1a714b_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x1a714b_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x1a7172_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x1a7172_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0x1a7172_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xc07988_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xc07988_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0xc07988_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xc08913_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xc08913_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0xc08913_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd18787_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd18787_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0xd18787_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd1a087_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd1a087_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0xd1a087_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd20781_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd20781_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0xd20781_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd2a081_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd2a081_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0xd2a081_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd38782_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd38782_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0xd38782_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd38783_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd38783_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0xd38783_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xe00081_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xe00081_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0xe00081_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xe00087_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xe00087_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0xe00087_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xe00214_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xe00214_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0xe00214_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xe00362_GetFaultDetectionCounter; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xe00362_GetFaultDetectionCounter(&Call_CBFaultDetectCtr_DTC_0xe00362_GetFaultDetectionCounter_FaultDetectionCounter); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D407_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D407_ReadData(Call_CBReadData_DemDataClass_D407_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D407_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D40C_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D40C_ReadData(Call_CBReadData_DemDataClass_D40C_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D40C_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D49C_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D49C_ReadData(Call_CBReadData_DemDataClass_D49C_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D49C_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D4CA_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D4CA_ReadData(Call_CBReadData_DemDataClass_D4CA_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D4CA_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D5CF_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D5CF_ReadData(Call_CBReadData_DemDataClass_D5CF_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D5CF_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D5D1_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D5D1_ReadData(Call_CBReadData_DemDataClass_D5D1_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D5D1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D805_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D805_ReadData(Call_CBReadData_DemDataClass_D805_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D805_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D806_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D806_ReadData(Call_CBReadData_DemDataClass_D806_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D806_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D807_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D807_ReadData(Call_CBReadData_DemDataClass_D807_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D807_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D808_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D808_ReadData(Call_CBReadData_DemDataClass_D808_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D808_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D809_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D809_ReadData(Call_CBReadData_DemDataClass_D809_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D809_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D80C_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D80C_ReadData(Call_CBReadData_DemDataClass_D80C_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D80C_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D822_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D822_ReadData(Call_CBReadData_DemDataClass_D822_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D822_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D824_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D824_ReadData(Call_CBReadData_DemDataClass_D824_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D824_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D825_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D825_ReadData(Call_CBReadData_DemDataClass_D825_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D825_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D827_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D827_ReadData(Call_CBReadData_DemDataClass_D827_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D827_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D828_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D828_ReadData(Call_CBReadData_DemDataClass_D828_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D828_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D829_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D829_ReadData(Call_CBReadData_DemDataClass_D829_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D829_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82B_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82B_ReadData(Call_CBReadData_DemDataClass_D82B_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D82B_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82C_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82C_ReadData(Call_CBReadData_DemDataClass_D82C_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D82C_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82D_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82D_ReadData(Call_CBReadData_DemDataClass_D82D_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D82D_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82E_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82E_ReadData(Call_CBReadData_DemDataClass_D82E_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D82E_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82F_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82F_ReadData(Call_CBReadData_DemDataClass_D82F_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D82F_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D831_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D831_ReadData(Call_CBReadData_DemDataClass_D831_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D831_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83B_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83B_ReadData(Call_CBReadData_DemDataClass_D83B_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D83B_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83C_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83C_ReadData(Call_CBReadData_DemDataClass_D83C_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D83C_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83D_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83D_ReadData(Call_CBReadData_DemDataClass_D83D_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D83D_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83E_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83E_ReadData(Call_CBReadData_DemDataClass_D83E_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D83E_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83F_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83F_ReadData(Call_CBReadData_DemDataClass_D83F_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D83F_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D840_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D840_ReadData(Call_CBReadData_DemDataClass_D840_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D840_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D843_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D843_ReadData(Call_CBReadData_DemDataClass_D843_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D843_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D844_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D844_ReadData(Call_CBReadData_DemDataClass_D844_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D844_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D845_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D845_ReadData(Call_CBReadData_DemDataClass_D845_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D845_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D846_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D846_ReadData(Call_CBReadData_DemDataClass_D846_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D846_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84A_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84A_ReadData(Call_CBReadData_DemDataClass_D84A_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D84A_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84B_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84B_ReadData(Call_CBReadData_DemDataClass_D84B_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D84B_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84C_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84C_ReadData(Call_CBReadData_DemDataClass_D84C_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D84C_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84D_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84D_ReadData(Call_CBReadData_DemDataClass_D84D_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D84D_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84E_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84E_ReadData(Call_CBReadData_DemDataClass_D84E_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D84E_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84F_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84F_ReadData(Call_CBReadData_DemDataClass_D84F_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D84F_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D850_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D850_ReadData(Call_CBReadData_DemDataClass_D850_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D850_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D851_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D851_ReadData(Call_CBReadData_DemDataClass_D851_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D851_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D852_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D852_ReadData(Call_CBReadData_DemDataClass_D852_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D852_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D853_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D853_ReadData(Call_CBReadData_DemDataClass_D853_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D853_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D854_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D854_ReadData(Call_CBReadData_DemDataClass_D854_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D854_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D855_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D855_ReadData(Call_CBReadData_DemDataClass_D855_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D855_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D8E9_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D8E9_ReadData(Call_CBReadData_DemDataClass_D8E9_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D8E9_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D8EB_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D8EB_ReadData(Call_CBReadData_DemDataClass_D8EB_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_D8EB_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE60_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE60_ReadData(Call_CBReadData_DemDataClass_FE60_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE60_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE61_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE61_ReadData(Call_CBReadData_DemDataClass_FE61_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE61_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE62_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE62_ReadData(Call_CBReadData_DemDataClass_FE62_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE62_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE63_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE63_ReadData(Call_CBReadData_DemDataClass_FE63_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE63_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE64_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE64_ReadData(Call_CBReadData_DemDataClass_FE64_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE64_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE65_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE65_ReadData(Call_CBReadData_DemDataClass_FE65_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE65_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE66_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE66_ReadData(Call_CBReadData_DemDataClass_FE66_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE66_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE67_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE67_ReadData(Call_CBReadData_DemDataClass_FE67_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE67_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE68_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE68_ReadData(Call_CBReadData_DemDataClass_FE68_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE68_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE69_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE69_ReadData(Call_CBReadData_DemDataClass_FE69_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE69_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6A_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6A_ReadData(Call_CBReadData_DemDataClass_FE6A_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE6A_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6B_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6B_ReadData(Call_CBReadData_DemDataClass_FE6B_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE6B_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6C_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6C_ReadData(Call_CBReadData_DemDataClass_FE6C_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE6C_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6D_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6D_ReadData(Call_CBReadData_DemDataClass_FE6D_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE6D_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6E_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6E_ReadData(Call_CBReadData_DemDataClass_FE6E_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE6E_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6F_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6F_ReadData(Call_CBReadData_DemDataClass_FE6F_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE6F_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE70_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE70_ReadData(Call_CBReadData_DemDataClass_FE70_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE70_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE71_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE71_ReadData(Call_CBReadData_DemDataClass_FE71_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE71_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE72_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE72_ReadData(Call_CBReadData_DemDataClass_FE72_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE72_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType DemMaster_0_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    DemMaster_0_FctPtr = (FncPtrType)TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE73_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE73_ReadData(Call_CBReadData_DemDataClass_FE73_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_CSDataServices_DemDataClass_FE73_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

 /* PRQA S 3226, 1863 L1 */ /* MD_Rte_Os, MD_Rte_Os */
  TSC_Dem_SchM_Enter_Dem_DEM_EXCLUSIVE_AREA_0();
  TSC_Dem_SchM_Exit_Dem_DEM_EXCLUSIVE_AREA_0();
  TSC_Dem_SchM_Enter_Dem_DEM_EXCLUSIVE_AREA_1();
  TSC_Dem_SchM_Exit_Dem_DEM_EXCLUSIVE_AREA_1();
  TSC_Dem_SchM_Enter_Dem_DEM_EXCLUSIVE_AREA_2();
  TSC_Dem_SchM_Exit_Dem_DEM_EXCLUSIVE_AREA_2();
  TSC_Dem_SchM_Enter_Dem_DEM_EXCLUSIVE_AREA_3();
  TSC_Dem_SchM_Exit_Dem_DEM_EXCLUSIVE_AREA_3();
  TSC_Dem_SchM_Enter_Dem_DEM_EXCLUSIVE_AREA_4();
  TSC_Dem_SchM_Exit_Dem_DEM_EXCLUSIVE_AREA_4();
/* PRQA L:L1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetDTCOfEvent
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDTCOfEvent> of PortPrototype <GeneralEvtInfo>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_GetDTCOfEvent(Dem_EventIdType EventId, Dem_DTCFormatType DTCFormat, uint32 *DTCOfEvent)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_GeneralDiagnosticInfo_DEM_E_NO_DTC_AVAILABLE
 *   RTE_E_GeneralDiagnosticInfo_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetDTCOfEvent_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) TSC_Inline_DemSatellite_0_Dem_GetDTCOfEvent(Dem_EventIdType EventId, Dem_DTCFormatType DTCFormat, P2VAR(uint32, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DTCOfEvent);
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetDTCOfEvent(Dem_EventIdType EventId, Dem_DTCFormatType DTCFormat, P2VAR(uint32, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DTCOfEvent) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_GetDTCOfEvent (returns application error)
 *********************************************************************************************************************/

  TSC_Inline_DemSatellite_0_Dem_GetDTCOfEvent(
    EventId,
    DTCFormat,
    DTCOfEvent
  );
  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetDTCStatusAvailabilityMask
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDTCStatusAvailabilityMask> of PortPrototype <DemServices>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_GetDTCStatusAvailabilityMask(uint8 ClientId, uint8 *DTCStatusMask)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DemServices_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetDTCStatusAvailabilityMask_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetDTCStatusAvailabilityMask(uint8 ClientId, P2VAR(uint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DTCStatusMask) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_GetDTCStatusAvailabilityMask (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetDebouncingOfEvent
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDebouncingOfEvent> of PortPrototype <GeneralEvtInfo>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_GetDebouncingOfEvent(Dem_EventIdType EventId, Dem_DebouncingStateType *DebouncingState)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_GeneralDiagnosticInfo_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetDebouncingOfEvent_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) TSC_Inline_DemSatellite_0_Dem_GetDebouncingOfEvent(Dem_EventIdType EventId, P2VAR(Dem_DebouncingStateType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DebouncingState);
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetDebouncingOfEvent(Dem_EventIdType EventId, P2VAR(Dem_DebouncingStateType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DebouncingState) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_GetDebouncingOfEvent (returns application error)
 *********************************************************************************************************************/

  TSC_Inline_DemSatellite_0_Dem_GetDebouncingOfEvent(
    EventId,
    DebouncingState
  );
  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetEventEnableCondition
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetEventEnableCondition> of PortPrototype <GeneralEvtInfo>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_GetEventEnableCondition(Dem_EventIdType EventId, boolean *ConditionFullfilled)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_GeneralDiagnosticInfo_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetEventEnableCondition_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) TSC_Inline_DemSatellite_0_Dem_GetEventEnableCondition(Dem_EventIdType EventId, P2VAR(boolean, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) ConditionFullfilled);
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventEnableCondition(Dem_EventIdType EventId, P2VAR(boolean, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) ConditionFullfilled) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_GetEventEnableCondition (returns application error)
 *********************************************************************************************************************/

  TSC_Inline_DemSatellite_0_Dem_GetEventEnableCondition(
    EventId,
    ConditionFullfilled
  );
  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetEventExtendedDataRecordEx
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetEventExtendedDataRecordEx> of PortPrototype <GeneralEvtInfo>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_GetEventExtendedDataRecordEx(Dem_EventIdType EventId, uint8 RecordNumber, uint8 *DestBuffer, uint16 *BufSize)
 *     Argument DestBuffer: uint8* is of type Dem_MaxDataValueType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_GeneralDiagnosticInfo_DEM_BUFFER_TOO_SMALL
 *   RTE_E_GeneralDiagnosticInfo_DEM_NO_SUCH_ELEMENT
 *   RTE_E_GeneralDiagnosticInfo_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetEventExtendedDataRecordEx_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) TSC_Inline_DemSatellite_0_Dem_GetEventExtendedDataRecordEx(Dem_EventIdType EventId, uint8 RecordNumber, P2VAR(uint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DestBuffer, P2VAR(uint16, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) BufSize);
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventExtendedDataRecordEx(Dem_EventIdType EventId, uint8 RecordNumber, P2VAR(uint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DestBuffer, P2VAR(uint16, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) BufSize) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_GetEventExtendedDataRecordEx (returns application error)
 *********************************************************************************************************************/

  TSC_Inline_DemSatellite_0_Dem_GetEventExtendedDataRecordEx(
    EventId,
    RecordNumber,
    DestBuffer,
    BufSize
  );
  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetEventFailed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetEventFailed> of PortPrototype <GeneralEvtInfo>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_GetEventFailed(Dem_EventIdType EventId, boolean *EventFailed)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_GeneralDiagnosticInfo_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetEventFailed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) TSC_Inline_DemSatellite_0_Dem_GetEventFailed(Dem_EventIdType EventId, P2VAR(boolean, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) EventFailed);
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventFailed(Dem_EventIdType EventId, P2VAR(boolean, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) EventFailed) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_GetEventFailed (returns application error)
 *********************************************************************************************************************/

  TSC_Inline_DemSatellite_0_Dem_GetEventFailed(
    EventId,
    EventFailed
  );
  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetEventFreezeFrameDataEx
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetEventFreezeFrameDataEx> of PortPrototype <GeneralEvtInfo>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_GetEventFreezeFrameDataEx(Dem_EventIdType EventId, uint8 RecordNumber, uint16 DataId, uint8 *DestBuffer, uint16 *BufSize)
 *     Argument DestBuffer: uint8* is of type Dem_MaxDataValueType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_GeneralDiagnosticInfo_DEM_BUFFER_TOO_SMALL
 *   RTE_E_GeneralDiagnosticInfo_DEM_NO_SUCH_ELEMENT
 *   RTE_E_GeneralDiagnosticInfo_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetEventFreezeFrameDataEx_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) TSC_Inline_DemSatellite_0_Dem_GetEventFreezeFrameDataEx(Dem_EventIdType EventId, uint8 RecordNumber, uint16 DataId, P2VAR(uint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DestBuffer, P2VAR(uint16, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) BufSize);
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventFreezeFrameDataEx(Dem_EventIdType EventId, uint8 RecordNumber, uint16 DataId, P2VAR(uint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DestBuffer, P2VAR(uint16, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) BufSize) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_GetEventFreezeFrameDataEx (returns application error)
 *********************************************************************************************************************/

  TSC_Inline_DemSatellite_0_Dem_GetEventFreezeFrameDataEx(
    EventId,
    RecordNumber,
    DataId,
    DestBuffer,
    BufSize
  );
  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetEventMemoryOverflow
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetEventMemoryOverflow> of PortPrototype <OverflowIndPrimaryMemory_DemClient_CDD>
 *   - triggered by server invocation for OperationPrototype <GetEventMemoryOverflow> of PortPrototype <OverflowIndPrimaryMemory_DemClient_DCM>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_GetEventMemoryOverflow(boolean *OverflowIndication)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_EvMemOverflowIndication_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetEventMemoryOverflow_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventMemoryOverflow(uint8 parg0, Dem_DTCOriginType parg1, P2VAR(boolean, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) OverflowIndication) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_GetEventMemoryOverflow (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetEventStatus
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetEventStatus> of PortPrototype <GeneralEvtInfo>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_GetEventUdsStatus(Dem_EventIdType EventId, Dem_UdsStatusByteType *UDSStatusByte)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_GeneralDiagnosticInfo_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetEventStatus_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) TSC_Inline_DemSatellite_0_Dem_GetEventUdsStatus(Dem_EventIdType EventId, P2VAR(Dem_UdsStatusByteType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) UDSStatusByte);
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventUdsStatus(Dem_EventIdType EventId, P2VAR(Dem_UdsStatusByteType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) UDSStatusByte) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_GetEventUdsStatus (returns application error)
 *********************************************************************************************************************/

  TSC_Inline_DemSatellite_0_Dem_GetEventUdsStatus(
    EventId,
    UDSStatusByte
  );
  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetEventTested
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetEventTested> of PortPrototype <GeneralEvtInfo>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_GetEventTested(Dem_EventIdType EventId, boolean *EventTested)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_GeneralDiagnosticInfo_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetEventTested_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) TSC_Inline_DemSatellite_0_Dem_GetEventTested(Dem_EventIdType EventId, P2VAR(boolean, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) EventTested);
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventTested(Dem_EventIdType EventId, P2VAR(boolean, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) EventTested) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_GetEventTested (returns application error)
 *********************************************************************************************************************/

  TSC_Inline_DemSatellite_0_Dem_GetEventTested(
    EventId,
    EventTested
  );
  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetFaultDetectionCounter
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetFaultDetectionCounter> of PortPrototype <GeneralEvtInfo>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_GetFaultDetectionCounter(Dem_EventIdType EventId, sint8 *FaultDetectionCounter)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_GeneralDiagnosticInfo_DEM_E_NO_FDC_AVAILABLE
 *   RTE_E_GeneralDiagnosticInfo_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetFaultDetectionCounter_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) TSC_Inline_DemSatellite_0_Dem_GetFaultDetectionCounter(Dem_EventIdType EventId, P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter);
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetFaultDetectionCounter(Dem_EventIdType EventId, P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_GetFaultDetectionCounter (returns application error)
 *********************************************************************************************************************/

  TSC_Inline_DemSatellite_0_Dem_GetFaultDetectionCounter(
    EventId,
    FaultDetectionCounter
  );
  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetMonitorStatus
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetMonitorStatus> of PortPrototype <GeneralEvtInfo>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_GetMonitorStatus(Dem_EventIdType EventId, Dem_MonitorStatusType *MonitorStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_GeneralDiagnosticInfo_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetMonitorStatus_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) TSC_Inline_DemSatellite_0_Dem_GetMonitorStatus(Dem_EventIdType EventId, P2VAR(Dem_MonitorStatusType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) MonitorStatus);
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetMonitorStatus(Dem_EventIdType EventId, P2VAR(Dem_MonitorStatusType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) MonitorStatus) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_GetMonitorStatus (returns application error)
 *********************************************************************************************************************/

  TSC_Inline_DemSatellite_0_Dem_GetMonitorStatus(
    EventId,
    MonitorStatus
  );
  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetNumberOfEventMemoryEntries
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetNumberOfEventMemoryEntries> of PortPrototype <OverflowIndPrimaryMemory_DemClient_CDD>
 *   - triggered by server invocation for OperationPrototype <GetNumberOfEventMemoryEntries> of PortPrototype <OverflowIndPrimaryMemory_DemClient_DCM>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_GetNumberOfEventMemoryEntries(uint8 *NumberOfEventMemoryEntries)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_EvMemOverflowIndication_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetNumberOfEventMemoryEntries_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetNumberOfEventMemoryEntries(uint8 parg0, Dem_DTCOriginType parg1, P2VAR(uint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) NumberOfEventMemoryEntries) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_GetNumberOfEventMemoryEntries (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetOperationCycleState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetOperationCycleState> of PortPrototype <OpCycle_DemAgingCycle>
 *   - triggered by server invocation for OperationPrototype <GetOperationCycleState> of PortPrototype <OpCycle_DemDrivingCycle>
 *   - triggered by server invocation for OperationPrototype <GetOperationCycleState> of PortPrototype <OpCycle_PowerCycle>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_GetOperationCycleState(Dem_OperationCycleStateType *CycleState)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_OperationCycle_E_NOT_OK
 *   RTE_E_OperationCycle_E_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetOperationCycleState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetOperationCycleState(uint8 parg0, P2VAR(Dem_OperationCycleStateType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) CycleState) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_GetOperationCycleState (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PostRunRequested
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetPostRunRequested> of PortPrototype <DemServices>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_PostRunRequested(boolean *IsRequested)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DemServices_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PostRunRequested_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_PostRunRequested(P2VAR(boolean, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) IsRequested) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_PostRunRequested (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SelectDTC
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SelectDTC> of PortPrototype <ClearDTC_DemClient_CDD>
 *   - triggered by server invocation for OperationPrototype <SelectDTC> of PortPrototype <ClearDTC_DemClient_DCM>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_SelectDTC(uint32 DTC, Dem_DTCFormatType DTCFormat, Dem_DTCOriginType DTCOrigin)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_ClearDTC_E_NOT_OK
 *   RTE_E_ClearDTC_E_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SelectDTC_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_SelectDTC(uint8 parg0, uint32 DTC, Dem_DTCFormatType DTCFormat, Dem_DTCOriginType DTCOrigin) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_SelectDTC (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SetEnableCondition
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableConditionDTC_0x10C413>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableConditionDTC_0x10C512>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableConditionDTC_0x10C613>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableConditionDTC_0x10C713>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableConditionDTC_0x12e912>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableConditionDTC_0x12f316>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x056216>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x056317>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x0a0804>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x0a084b>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x0a9464>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x0af864>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x0cf464>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x108093>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x120a11>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x120a12>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x120b11>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x120b12>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x120c64>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x120c98>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x120d64>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x120d98>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12d711>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12d712>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12d713>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12d811>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12d812>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12d813>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12d911>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12d912>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12d913>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12da11>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12da12>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12da13>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12db12>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12dc11>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12dd12>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12de11>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12df13>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12e012>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12e111>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12e213>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12e319>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12e712>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12e811>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12ea11>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x12f917>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x13e919>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x166C64>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x179e11>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x179e12>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x179f11>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x179f12>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x1a0064>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x1a7104>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x1a714b>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0x1a7172>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0xc07988>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0xc08913>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0xd18787>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0xd1a087>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0xd20781>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0xd2a081>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0xd38782>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0xd38783>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0xe00081>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0xe00087>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0xe00214>
 *   - triggered by server invocation for OperationPrototype <SetEnableCondition> of PortPrototype <EnableCond_DemEnableCondition_DTC_0xe00362>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_SetEnableCondition(boolean ConditionFulfilled)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_EnableCondition_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SetEnableCondition_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_SetEnableCondition(uint8 parg0, boolean ConditionFulfilled) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_SetEnableCondition (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SetOperationCycleState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetOperationCycleState> of PortPrototype <OpCycle_DemAgingCycle>
 *   - triggered by server invocation for OperationPrototype <SetOperationCycleState> of PortPrototype <OpCycle_DemDrivingCycle>
 *   - triggered by server invocation for OperationPrototype <SetOperationCycleState> of PortPrototype <OpCycle_PowerCycle>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dem_SetOperationCycleState(Dem_OperationCycleStateType CycleState)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_OperationCycle_E_NOT_OK
 *   RTE_E_OperationCycle_E_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SetOperationCycleState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_SetOperationCycleState(uint8 parg0, Dem_OperationCycleStateType CycleState) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dem_SetOperationCycleState (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define DemMaster_0_STOP_SEC_CODE
#include "DemMaster_0_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void DemMaster_0_TestDefines(void)
{
  /* Enumeration Data Types */

  Dem_DTCFormatType Test_Dem_DTCFormatType_V_1 = DEM_DTC_FORMAT_OBD;
  Dem_DTCFormatType Test_Dem_DTCFormatType_V_2 = DEM_DTC_FORMAT_UDS;
  Dem_DTCFormatType Test_Dem_DTCFormatType_V_3 = DEM_DTC_FORMAT_J1939;

  Dem_DTCKindType Test_Dem_DTCKindType_V_1 = DEM_DTC_KIND_ALL_DTCS;
  Dem_DTCKindType Test_Dem_DTCKindType_V_2 = DEM_DTC_KIND_EMISSION_REL_DTCS;

  Dem_DTCOriginType Test_Dem_DTCOriginType_V_1 = DEM_DTC_ORIGIN_PRIMARY_MEMORY;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_2 = DEM_DTC_ORIGIN_MIRROR_MEMORY;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_3 = DEM_DTC_ORIGIN_PERMANENT_MEMORY;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_4 = DEM_DTC_ORIGIN_OBD_RELEVANT_MEMORY;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_5 = DEM_DTC_ORIGIN_SECONDARY_MEMORY;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_6 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_10;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_7 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_11;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_8 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_12;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_9 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_13;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_10 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_14;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_11 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_15;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_12 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_16;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_13 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_17;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_14 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_18;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_15 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_19;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_16 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_1A;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_17 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_1B;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_18 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_1C;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_19 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_1D;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_20 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_1E;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_21 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_1F;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_22 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_20;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_23 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_21;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_24 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_22;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_25 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_23;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_26 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_24;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_27 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_25;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_28 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_26;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_29 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_27;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_30 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_28;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_31 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_29;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_32 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_2A;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_33 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_2B;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_34 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_2C;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_35 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_2D;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_36 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_2E;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_37 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_2F;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_38 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_30;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_39 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_31;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_40 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_32;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_41 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_33;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_42 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_34;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_43 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_35;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_44 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_36;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_45 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_37;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_46 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_38;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_47 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_39;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_48 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_3A;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_49 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_3B;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_50 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_3C;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_51 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_3D;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_52 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_3E;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_53 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_3F;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_54 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_40;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_55 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_41;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_56 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_42;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_57 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_43;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_58 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_44;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_59 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_45;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_60 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_46;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_61 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_47;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_62 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_48;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_63 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_49;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_64 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_4A;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_65 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_4B;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_66 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_4C;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_67 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_4D;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_68 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_4E;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_69 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_4F;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_70 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_50;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_71 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_51;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_72 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_52;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_73 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_53;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_74 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_54;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_75 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_55;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_76 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_56;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_77 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_57;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_78 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_58;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_79 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_59;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_80 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_5A;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_81 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_5B;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_82 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_5C;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_83 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_5D;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_84 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_5E;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_85 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_5F;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_86 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_60;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_87 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_61;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_88 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_62;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_89 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_63;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_90 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_64;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_91 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_65;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_92 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_66;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_93 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_67;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_94 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_68;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_95 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_69;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_96 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_6A;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_97 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_6B;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_98 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_6C;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_99 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_6D;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_100 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_6E;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_101 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_6F;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_102 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_70;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_103 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_71;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_104 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_72;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_105 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_73;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_106 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_74;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_107 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_75;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_108 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_76;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_109 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_77;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_110 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_78;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_111 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_79;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_112 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_7A;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_113 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_7B;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_114 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_7C;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_115 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_7D;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_116 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_7E;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_117 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_7F;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_118 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_80;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_119 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_81;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_120 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_82;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_121 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_83;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_122 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_84;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_123 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_85;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_124 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_86;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_125 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_87;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_126 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_88;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_127 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_89;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_128 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_8A;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_129 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_8B;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_130 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_8C;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_131 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_8D;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_132 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_8E;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_133 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_8F;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_134 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_90;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_135 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_91;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_136 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_92;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_137 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_93;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_138 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_94;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_139 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_95;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_140 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_96;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_141 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_97;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_142 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_98;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_143 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_99;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_144 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_9A;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_145 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_9B;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_146 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_9C;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_147 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_9D;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_148 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_9E;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_149 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_9F;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_150 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A0;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_151 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A1;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_152 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A2;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_153 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A3;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_154 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A4;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_155 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A5;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_156 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A6;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_157 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A7;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_158 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A8;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_159 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_A9;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_160 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_AA;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_161 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_AB;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_162 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_AC;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_163 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_AD;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_164 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_AE;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_165 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_AF;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_166 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B0;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_167 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B1;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_168 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B2;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_169 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B3;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_170 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B4;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_171 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B5;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_172 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B6;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_173 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B7;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_174 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B8;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_175 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_B9;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_176 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_BA;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_177 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_BB;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_178 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_BC;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_179 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_BD;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_180 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_BE;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_181 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_BF;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_182 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C0;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_183 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C1;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_184 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C2;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_185 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C3;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_186 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C4;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_187 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C5;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_188 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C6;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_189 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C7;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_190 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C8;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_191 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_C9;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_192 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_CA;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_193 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_CB;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_194 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_CC;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_195 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_CD;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_196 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_CE;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_197 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_CF;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_198 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D0;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_199 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D1;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_200 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D2;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_201 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D3;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_202 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D4;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_203 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D5;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_204 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D6;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_205 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D7;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_206 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D8;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_207 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_D9;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_208 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_DA;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_209 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_DB;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_210 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_DC;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_211 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_DD;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_212 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_DE;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_213 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_DF;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_214 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E0;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_215 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E1;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_216 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E2;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_217 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E3;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_218 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E4;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_219 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E5;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_220 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E6;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_221 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E7;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_222 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E8;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_223 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_E9;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_224 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_EA;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_225 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_EB;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_226 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_EC;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_227 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_ED;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_228 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_EE;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_229 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_EF;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_230 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F0;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_231 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F1;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_232 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F2;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_233 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F3;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_234 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F4;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_235 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F5;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_236 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F6;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_237 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F7;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_238 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F8;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_239 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_F9;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_240 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_FA;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_241 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_FB;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_242 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_FC;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_243 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_FD;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_244 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_FE;
  Dem_DTCOriginType Test_Dem_DTCOriginType_V_245 = DEM_DTC_ORIGIN_USERDEFINED_MEMORY_FF;

  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_1 = DEM_SEVERITY_NO_SEVERITY;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_1_BflMask = DTC_NO_SEVERITY_BflMask;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_1_BflPn = DTC_NO_SEVERITY_BflPn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_1_BflLn = DTC_NO_SEVERITY_BflLn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_2 = DEM_SEVERITY_WWHOBD_CLASS_NO_CLASS;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_2_BflMask = DTC_CLASS_BflMask;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_2_BflPn = DTC_CLASS_BflPn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_2_BflLn = DTC_CLASS_BflLn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_3 = DEM_SEVERITY_WWHOBD_CLASS_A;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_3_BflMask = DTC_CLASS_BflMask;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_3_BflPn = DTC_CLASS_BflPn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_3_BflLn = DTC_CLASS_BflLn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_4 = DEM_SEVERITY_WWHOBD_CLASS_B1;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_4_BflMask = DTC_CLASS_BflMask;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_4_BflPn = DTC_CLASS_BflPn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_4_BflLn = DTC_CLASS_BflLn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_5 = DEM_SEVERITY_WWHOBD_CLASS_B2;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_5_BflMask = DTC_CLASS_BflMask;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_5_BflPn = DTC_CLASS_BflPn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_5_BflLn = DTC_CLASS_BflLn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_6 = DEM_SEVERITY_WWHOBD_CLASS_C;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_6_BflMask = DTC_CLASS_BflMask;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_6_BflPn = DTC_CLASS_BflPn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_6_BflLn = DTC_CLASS_BflLn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_7 = DEM_SEVERITY_MAINTENANCE_ONLY;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_7_BflMask = DTC_SEVERITY_BflMask;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_7_BflPn = DTC_SEVERITY_BflPn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_7_BflLn = DTC_SEVERITY_BflLn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_8 = DEM_SEVERITY_CHECK_AT_NEXT_HALT;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_8_BflMask = DTC_SEVERITY_BflMask;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_8_BflPn = DTC_SEVERITY_BflPn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_8_BflLn = DTC_SEVERITY_BflLn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_9 = DEM_SEVERITY_CHECK_IMMEDIATELY;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_9_BflMask = DTC_SEVERITY_BflMask;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_9_BflPn = DTC_SEVERITY_BflPn;
  Dem_DTCSeverityType Test_Dem_DTCSeverityType_V_9_BflLn = DTC_SEVERITY_BflLn;

  Dem_DTRControlType Test_Dem_DTRControlType_V_1 = DEM_DTR_CTL_NORMAL;
  Dem_DTRControlType Test_Dem_DTRControlType_V_2 = DEM_DTR_CTL_NO_MAX;
  Dem_DTRControlType Test_Dem_DTRControlType_V_3 = DEM_DTR_CTL_NO_MIN;
  Dem_DTRControlType Test_Dem_DTRControlType_V_4 = DEM_DTR_CTL_RESET;
  Dem_DTRControlType Test_Dem_DTRControlType_V_5 = DEM_DTR_CTL_INVISIBLE;

  Dem_DebounceResetStatusType Test_Dem_DebounceResetStatusType_V_1 = DEM_DEBOUNCE_STATUS_FREEZE;
  Dem_DebounceResetStatusType Test_Dem_DebounceResetStatusType_V_2 = DEM_DEBOUNCE_STATUS_RESET;

  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_1 = DEM_TEMPORARILY_DEFECTIVE;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_1_BflMask = DEM_TEMPORARILY_DEFECTIVE_BflMask;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_1_BflPn = DEM_TEMPORARILY_DEFECTIVE_BflPn;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_1_BflLn = DEM_TEMPORARILY_DEFECTIVE_BflLn;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_2 = DEM_FINALLY_DEFECTIVE;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_2_BflMask = DEM_FINALLY_DEFECTIVE_BflMask;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_2_BflPn = DEM_FINALLY_DEFECTIVE_BflPn;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_2_BflLn = DEM_FINALLY_DEFECTIVE_BflLn;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_3 = DEM_TEMPORARILY_HEALED;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_3_BflMask = DEM_TEMPORARILY_HEALED_BflMask;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_3_BflPn = DEM_TEMPORARILY_HEALED_BflPn;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_3_BflLn = DEM_TEMPORARILY_HEALED_BflLn;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_4 = DEM_TEST_COMPLETE;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_4_BflMask = DEM_TEST_COMPLETE_BflMask;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_4_BflPn = DEM_TEST_COMPLETE_BflPn;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_4_BflLn = DEM_TEST_COMPLETE_BflLn;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_5 = DEM_DTR_UPDATE;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_5_BflMask = DEM_DTR_UPDATE_BflMask;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_5_BflPn = DEM_DTR_UPDATE_BflPn;
  Dem_DebouncingStateType Test_Dem_DebouncingStateType_V_5_BflLn = DEM_DTR_UPDATE_BflLn;

  Dem_EventStatusType Test_Dem_EventStatusType_V_1 = DEM_EVENT_STATUS_PASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_2 = DEM_EVENT_STATUS_FAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_3 = DEM_EVENT_STATUS_PREPASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_4 = DEM_EVENT_STATUS_PREFAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_5 = DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_6 = DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_7 = DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_8 = DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_9 = DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED;

  Dem_IndicatorStatusType Test_Dem_IndicatorStatusType_V_1 = DEM_INDICATOR_OFF;
  Dem_IndicatorStatusType Test_Dem_IndicatorStatusType_V_2 = DEM_INDICATOR_CONTINUOUS;
  Dem_IndicatorStatusType Test_Dem_IndicatorStatusType_V_3 = DEM_INDICATOR_BLINKING;
  Dem_IndicatorStatusType Test_Dem_IndicatorStatusType_V_4 = DEM_INDICATOR_BLINK_CONT;
  Dem_IndicatorStatusType Test_Dem_IndicatorStatusType_V_5 = DEM_INDICATOR_SLOW_FLASH;
  Dem_IndicatorStatusType Test_Dem_IndicatorStatusType_V_6 = DEM_INDICATOR_FAST_FLASH;
  Dem_IndicatorStatusType Test_Dem_IndicatorStatusType_V_7 = DEM_INDICATOR_ON_DEMAND;
  Dem_IndicatorStatusType Test_Dem_IndicatorStatusType_V_8 = DEM_INDICATOR_SHORT;

  Dem_InitMonitorReasonType Test_Dem_InitMonitorReasonType_V_1 = DEM_INIT_MONITOR_CLEAR;
  Dem_InitMonitorReasonType Test_Dem_InitMonitorReasonType_V_2 = DEM_INIT_MONITOR_RESTART;
  Dem_InitMonitorReasonType Test_Dem_InitMonitorReasonType_V_3 = DEM_INIT_MONITOR_REENABLED;
  Dem_InitMonitorReasonType Test_Dem_InitMonitorReasonType_V_4 = DEM_INIT_MONITOR_STORAGE_REENABLED;

  Dem_IumprDenomCondIdType Test_Dem_IumprDenomCondIdType_V_1 = DEM_IUMPR_GENERAL_DENOMINATOR;
  Dem_IumprDenomCondIdType Test_Dem_IumprDenomCondIdType_V_2 = DEM_IUMPR_DEN_COND_COLDSTART;
  Dem_IumprDenomCondIdType Test_Dem_IumprDenomCondIdType_V_3 = DEM_IUMPR_DEN_COND_EVAP;
  Dem_IumprDenomCondIdType Test_Dem_IumprDenomCondIdType_V_4 = DEM_IUMPR_DEN_COND_500MI;

  Dem_IumprDenomCondStatusType Test_Dem_IumprDenomCondStatusType_V_1 = DEM_IUMPR_DEN_STATUS_NOT_REACHED;
  Dem_IumprDenomCondStatusType Test_Dem_IumprDenomCondStatusType_V_2 = DEM_IUMPR_DEN_STATUS_REACHED;
  Dem_IumprDenomCondStatusType Test_Dem_IumprDenomCondStatusType_V_3 = DEM_IUMPR_DEN_STATUS_INHIBITED;

  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_1 = DEM_IUMPR_BOOSTPRS;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_2 = DEM_IUMPR_CAT1;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_3 = DEM_IUMPR_CAT2;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_4 = DEM_IUMPR_EGR;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_5 = DEM_IUMPR_EGSENSOR;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_6 = DEM_IUMPR_EVAP;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_7 = DEM_IUMPR_NMHCCAT;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_8 = DEM_IUMPR_NOXADSORB;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_9 = DEM_IUMPR_NOXCAT;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_10 = DEM_IUMPR_OXS1;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_11 = DEM_IUMPR_OXS2;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_12 = DEM_IUMPR_PMFILTER;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_13 = DEM_IUMPR_PRIVATE;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_14 = DEM_IUMPR_SAIR;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_15 = DEM_IUMPR_SECOXS1;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_16 = DEM_IUMPR_SECOXS2;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_17 = DEM_IUMPR_FLSYS;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_18 = DEM_IUMPR_AFRI1;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_19 = DEM_IUMPR_AFRI2;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_20 = DEM_IUMPR_PF1;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_21 = DEM_IUMPR_PF2;
  Dem_IumprReadinessGroupType Test_Dem_IumprReadinessGroupType_V_22 = DEM_IUMPR_ALLGROUPS;

  Dem_MonitorStatusType Test_Dem_MonitorStatusType_V_1 = DEM_MONITOR_STATUS_TF;
  Dem_MonitorStatusType Test_Dem_MonitorStatusType_V_1_BflMask = DEM_MONITOR_STATUS_TF_BflMask;
  Dem_MonitorStatusType Test_Dem_MonitorStatusType_V_1_BflPn = DEM_MONITOR_STATUS_TF_BflPn;
  Dem_MonitorStatusType Test_Dem_MonitorStatusType_V_1_BflLn = DEM_MONITOR_STATUS_TF_BflLn;
  Dem_MonitorStatusType Test_Dem_MonitorStatusType_V_2 = DEM_MONITOR_STATUS_TNCTOC;
  Dem_MonitorStatusType Test_Dem_MonitorStatusType_V_2_BflMask = DEM_MONITOR_STATUS_TNCTOC_BflMask;
  Dem_MonitorStatusType Test_Dem_MonitorStatusType_V_2_BflPn = DEM_MONITOR_STATUS_TNCTOC_BflPn;
  Dem_MonitorStatusType Test_Dem_MonitorStatusType_V_2_BflLn = DEM_MONITOR_STATUS_TNCTOC_BflLn;

  Dem_OperationCycleStateType Test_Dem_OperationCycleStateType_V_1 = DEM_CYCLE_STATE_START;
  Dem_OperationCycleStateType Test_Dem_OperationCycleStateType_V_2 = DEM_CYCLE_STATE_END;

  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_1 = DEM_UDS_STATUS_TF;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_1_BflMask = DEM_UDS_STATUS_TF_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_1_BflPn = DEM_UDS_STATUS_TF_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_1_BflLn = DEM_UDS_STATUS_TF_BflLn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_2 = DEM_UDS_STATUS_TFTOC;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_2_BflMask = DEM_UDS_STATUS_TFTOC_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_2_BflPn = DEM_UDS_STATUS_TFTOC_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_2_BflLn = DEM_UDS_STATUS_TFTOC_BflLn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_3 = DEM_UDS_STATUS_PDTC;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_3_BflMask = DEM_UDS_STATUS_PDTC_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_3_BflPn = DEM_UDS_STATUS_PDTC_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_3_BflLn = DEM_UDS_STATUS_PDTC_BflLn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_4 = DEM_UDS_STATUS_CDTC;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_4_BflMask = DEM_UDS_STATUS_CDTC_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_4_BflPn = DEM_UDS_STATUS_CDTC_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_4_BflLn = DEM_UDS_STATUS_CDTC_BflLn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_5 = DEM_UDS_STATUS_TNCSLC;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_5_BflMask = DEM_UDS_STATUS_TNCSLC_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_5_BflPn = DEM_UDS_STATUS_TNCSLC_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_5_BflLn = DEM_UDS_STATUS_TNCSLC_BflLn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_6 = DEM_UDS_STATUS_TFSLC;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_6_BflMask = DEM_UDS_STATUS_TFSLC_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_6_BflPn = DEM_UDS_STATUS_TFSLC_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_6_BflLn = DEM_UDS_STATUS_TFSLC_BflLn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_7 = DEM_UDS_STATUS_TNCTOC;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_7_BflMask = DEM_UDS_STATUS_TNCTOC_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_7_BflPn = DEM_UDS_STATUS_TNCTOC_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_7_BflLn = DEM_UDS_STATUS_TNCTOC_BflLn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_8 = DEM_UDS_STATUS_WIR;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_8_BflMask = DEM_UDS_STATUS_WIR_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_8_BflPn = DEM_UDS_STATUS_WIR_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_8_BflLn = DEM_UDS_STATUS_WIR_BflLn;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_Os:
     Reason:     This justification is used as summary justification for all deviations caused by the MICROSAR OS
                 which is for testing of the RTE. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
