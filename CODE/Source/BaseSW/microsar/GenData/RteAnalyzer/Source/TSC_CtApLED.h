/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApLED.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApLED_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data);
Std_ReturnType TSC_CtApLED_Rte_Read_PpLedModes_DeChargeError(boolean *data);
Std_ReturnType TSC_CtApLED_Rte_Read_PpLedModes_DeChargeInProgress(boolean *data);
Std_ReturnType TSC_CtApLED_Rte_Read_PpLedModes_DeEndOfCharge(boolean *data);
Std_ReturnType TSC_CtApLED_Rte_Read_PpLedModes_DeGuideManagement(boolean *data);
Std_ReturnType TSC_CtApLED_Rte_Read_PpLedModes_DeProgrammingCharge(boolean *data);
Std_ReturnType TSC_CtApLED_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data);
Std_ReturnType TSC_CtApLED_Rte_Read_PpLockLED2BEPR_DeLockLED2BEPR(boolean *data);
Std_ReturnType TSC_CtApLED_Rte_Read_PpLockStateError_DeLockStateError(boolean *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApLED_Rte_Write_PpChLedRGBCtl_DeChLedBrCtl(IdtChLedCtl data);
Std_ReturnType TSC_CtApLED_Rte_Write_PpChLedRGBCtl_DeChLedGrCtl(IdtChLedCtl data);
Std_ReturnType TSC_CtApLED_Rte_Write_PpChLedRGBCtl_DeChLedRrCtl(IdtChLedCtl data);
Std_ReturnType TSC_CtApLED_Rte_Write_PpPlgLedrCtrl_DePlgLedrCtrl(IdtPlgLedrCtrl data);

/** Client server interfaces */
Std_ReturnType TSC_CtApLED_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);
Std_ReturnType TSC_CtApLED_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);

/** SW-C local Calibration Parameters */
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyBlueChargeError(void);
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyBlueChargeInProgress(void);
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyBlueEndOfCharge(void);
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyBlueGuideManagement(void);
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyBlueProgrammingCharge(void);
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyGreenChargeError(void);
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyGreenChargeInProgress(void);
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyGreenEndOfCharge(void);
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyGreenGuideManagement(void);
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyGreenProgrammingCharge(void);
IdtDutyPlug  TSC_CtApLED_Rte_CData_CalDutyPlug(void);
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyRedChargeError(void);
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyRedChargeInProgress(void);
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyRedEndOfCharge(void);
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyRedGuideManagement(void);
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyRedProgrammingCharge(void);
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOffChargeError(void);
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOffChargeInProgress(void);
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOffEndOfCharge(void);
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOffGuideManagement(void);
IdtTimePlug  TSC_CtApLED_Rte_CData_CalTimeOffPlugA(void);
IdtTimePlug  TSC_CtApLED_Rte_CData_CalTimeOffPlugB(void);
IdtTimePlug  TSC_CtApLED_Rte_CData_CalTimeOffPlugC(void);
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOffProgrammingCharge(void);
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOnChargeError(void);
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOnChargeInProgress(void);
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOnEndOfCharge(void);
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOnGuideManagement(void);
IdtTimePlug  TSC_CtApLED_Rte_CData_CalTimeOnPlugA(void);
IdtTimePlug  TSC_CtApLED_Rte_CData_CalTimeOnPlugB(void);
IdtTimePlug  TSC_CtApLED_Rte_CData_CalTimeOnPlugC(void);
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOnProgrammingCharge(void);




