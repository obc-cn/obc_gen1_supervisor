/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApOBC.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApOBC
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApOBC>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * WdgM_CheckpointIdType
 *   
 *
 * WdgM_SupervisedEntityIdType
 *   
 *
 *
 * Operation Prototypes:
 * =====================
 * CheckpointReached of Port Interface WdgM_AliveSupervision
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CtApOBC.h"
#include "TSC_CtApOBC.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApOBC_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * BMS_HighestChargeCurrentAllow: Integer in interval [0...2047]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * BMS_HighestChargeVoltageAllow: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * CalVDCLinkRequiredMonophasic: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 500
 * DCDC_CurrentReference: Integer in interval [0...2047]
 * DCDC_VoltageReference: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCHV_ADC_IOUT: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCHV_ADC_VOUT: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCHV_Command_Current_Reference: Integer in interval [0...2047]
 * IdtMaxInputACCurrentEVSE: Integer in interval [0...65535]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * IdtMaxOutputDCHVCurrent: Integer in interval [0...200]
 *   Unit: [A], Factor: 1, Offset: 0
 * IdtMaxOutputDCHVVoltage: Integer in interval [0...250]
 *   Unit: [V], Factor: 1, Offset: 450
 * IdtMinDCHVOutputVoltage: Integer in interval [0...50]
 *   Unit: [V], Factor: 10, Offset: 0
 * IdtTimeRelaysPrechargeClosed: Integer in interval [0...200]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtVDCLinkRequiredTriphasic: Integer in interval [0...250]
 *   Unit: [V], Factor: 1, Offset: 500
 * IdtVoltageCorrectionOffset: Integer in interval [-32768...32767]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * OBC_HighVoltConnectionAllowed: Boolean
 * OBC_PowerMax: Integer in interval [0...255]
 *   Unit: [W], Factor: 100, Offset: 0
 * PFC_IPH1_RMS_0A1: Integer in interval [0...1023]
 * PFC_IPH2_RMS_0A1: Integer in interval [0...1023]
 * PFC_IPH3_RMS_0A1: Integer in interval [0...1023]
 * PFC_Vdclink_V: Integer in interval [0...1023]
 * SUP_CommandVDCLink_V: Integer in interval [0...1023]
 *   Unit: [V], Factor: 1, Offset: 0
 * WdgM_CheckpointIdType: Integer in interval [0...65535]
 * WdgM_SupervisedEntityIdType: Integer in interval [0...65535]
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DCHV_DCHVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCHV_STATUS_STANDBY (0U)
 *   Cx1_DCHV_STATUS_RUN (1U)
 *   Cx2_DCHV_STATUS_ERROR (2U)
 *   Cx3_DCHV_STATUS_ALARM (3U)
 *   Cx4_DCHV_STATUS_SAFE (4U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtInputVoltageMode: Enumeration of integer in interval [0...2] with enumerators
 *   IVM_NO_INPUT_DETECTED (0U)
 *   IVM_MONOPHASIC_INPUT (1U)
 *   IVM_TRIPHASIC_INPUT (2U)
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 *   POST_ONGOING (0U)
 *   POST_OK (1U)
 *   POST_NOK (2U)
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_no_charging (0U)
 *   Cx1_slow_charging (1U)
 *   Cx2_China_fast_charging (2U)
 *   Cx3_Euro_fast_charging (3U)
 * OBC_InputVoltageSt: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_220V_AC (0U)
 *   Cx1_220V_AC_connected (1U)
 *   Cx2_220V_AC_disconnected (2U)
 *   Cx3_Invalid_value (3U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * PFC_PFCStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PFC_STATE_STANDBY (0U)
 *   Cx1_PFC_STATE_TRI (1U)
 *   Cx2_PFC_STATE_MONO (2U)
 *   Cx3_PFC_STATE_RESET_ERROR (3U)
 *   Cx4_PFC_STATE_DISCHARGE (4U)
 *   Cx5_PFC_STATE_SHUTDOWN (5U)
 *   Cx6_PFC_STATE_START_TRI (6U)
 *   Cx7_PFC_STATE_START_MONO (7U)
 *   Cx8_PFC_STATE_ERROR (8U)
 *   Cx9_PFC_STATE_DIRECTPWM (9U)
 * SUP_RequestPFCStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PFC_STATE_STANDBY (0U)
 *   Cx1_PFC_STATE_TRI (1U)
 *   Cx2_PFC_STATE_MONO (2U)
 *   Cx3_PFC_STATE_RESET_ERROR (3U)
 *   Cx4_PFC_STATE_DISCHARGE (4U)
 *   Cx5_PFC_STATE_SHUTDOWN (5U)
 * SUP_RequestStatusDCHV: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCHV_STATE_STANDBY (0U)
 *   Cx1_DCHV_STATE_RUN (1U)
 *   Cx2_DCHV_STATE_RESET_ERROR (2U)
 *   Cx3_DCHV_STATE_DISCHARGE (3U)
 *   Cx4_DCHV_STATE_SHUTDOWN (4U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   boolean *Rte_Pim_PimOBCShutdownPathFSP(void)
 *   boolean *Rte_Pim_PimOBCShutdownPathGPIO(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtMaxOutputDCHVCurrent Rte_CData_CalMaxOutputDCHVCurrent(void)
 *   IdtMaxOutputDCHVVoltage Rte_CData_CalMaxOutputDCHVVoltage(void)
 *   IdtMinDCHVOutputVoltage Rte_CData_CalMinDCHVOutputVoltage(void)
 *   IdtTimeRelaysPrechargeClosed Rte_CData_CalTimeRelaysPrechargeClosed(void)
 *   CalVDCLinkRequiredMonophasic Rte_CData_CalVDCLinkRequiredMonophasic(void)
 *   IdtVDCLinkRequiredTriphasic Rte_CData_CalVDCLinkRequiredTriphasic(void)
 *
 *********************************************************************************************************************/


#define CtApOBC_START_SEC_CODE
#include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApOBC_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOBC_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApOBC_CODE) RCtApOBC_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOBC_init
 *********************************************************************************************************************/

  boolean PimPimOBCShutdownPathFSP;
  boolean PimPimOBCShutdownPathGPIO;

  IdtMaxOutputDCHVCurrent CalMaxOutputDCHVCurrent_data;
  IdtMaxOutputDCHVVoltage CalMaxOutputDCHVVoltage_data;
  IdtMinDCHVOutputVoltage CalMinDCHVOutputVoltage_data;
  IdtTimeRelaysPrechargeClosed CalTimeRelaysPrechargeClosed_data;
  CalVDCLinkRequiredMonophasic CalVDCLinkRequiredMonophasic_data;
  IdtVDCLinkRequiredTriphasic CalVDCLinkRequiredTriphasic_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBCShutdownPathFSP = *TSC_CtApOBC_Rte_Pim_PimOBCShutdownPathFSP();
  *TSC_CtApOBC_Rte_Pim_PimOBCShutdownPathFSP() = PimPimOBCShutdownPathFSP;
  PimPimOBCShutdownPathGPIO = *TSC_CtApOBC_Rte_Pim_PimOBCShutdownPathGPIO();
  *TSC_CtApOBC_Rte_Pim_PimOBCShutdownPathGPIO() = PimPimOBCShutdownPathGPIO;

  CalMaxOutputDCHVCurrent_data = TSC_CtApOBC_Rte_CData_CalMaxOutputDCHVCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxOutputDCHVVoltage_data = TSC_CtApOBC_Rte_CData_CalMaxOutputDCHVVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinDCHVOutputVoltage_data = TSC_CtApOBC_Rte_CData_CalMinDCHVOutputVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeRelaysPrechargeClosed_data = TSC_CtApOBC_Rte_CData_CalTimeRelaysPrechargeClosed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalVDCLinkRequiredMonophasic_data = TSC_CtApOBC_Rte_CData_CalVDCLinkRequiredMonophasic(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalVDCLinkRequiredTriphasic_data = TSC_CtApOBC_Rte_CData_CalVDCLinkRequiredTriphasic(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApOBC_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApOBC_task10msA
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(DCDC_CurrentReference data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed data)
 *   Std_ReturnType Rte_Write_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue(boolean data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOBC_task10msA_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApOBC_CODE) RCtApOBC_task10msA(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOBC_task10msA
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  boolean PimPimOBCShutdownPathFSP;
  boolean PimPimOBCShutdownPathGPIO;

  IdtMaxOutputDCHVCurrent CalMaxOutputDCHVCurrent_data;
  IdtMaxOutputDCHVVoltage CalMaxOutputDCHVVoltage_data;
  IdtMinDCHVOutputVoltage CalMinDCHVOutputVoltage_data;
  IdtTimeRelaysPrechargeClosed CalTimeRelaysPrechargeClosed_data;
  CalVDCLinkRequiredMonophasic CalVDCLinkRequiredMonophasic_data;
  IdtVDCLinkRequiredTriphasic CalVDCLinkRequiredTriphasic_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBCShutdownPathFSP = *TSC_CtApOBC_Rte_Pim_PimOBCShutdownPathFSP();
  *TSC_CtApOBC_Rte_Pim_PimOBCShutdownPathFSP() = PimPimOBCShutdownPathFSP;
  PimPimOBCShutdownPathGPIO = *TSC_CtApOBC_Rte_Pim_PimOBCShutdownPathGPIO();
  *TSC_CtApOBC_Rte_Pim_PimOBCShutdownPathGPIO() = PimPimOBCShutdownPathGPIO;

  CalMaxOutputDCHVCurrent_data = TSC_CtApOBC_Rte_CData_CalMaxOutputDCHVCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxOutputDCHVVoltage_data = TSC_CtApOBC_Rte_CData_CalMaxOutputDCHVVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinDCHVOutputVoltage_data = TSC_CtApOBC_Rte_CData_CalMinDCHVOutputVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeRelaysPrechargeClosed_data = TSC_CtApOBC_Rte_CData_CalTimeRelaysPrechargeClosed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalVDCLinkRequiredMonophasic_data = TSC_CtApOBC_Rte_CData_CalVDCLinkRequiredMonophasic(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalVDCLinkRequiredTriphasic_data = TSC_CtApOBC_Rte_CData_CalVDCLinkRequiredTriphasic(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApOBC_Rte_Write_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(Rte_InitValue_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Write_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(Rte_InitValue_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Write_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue(Rte_InitValue_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApOBC_task10msB
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(boolean *data)
 *   Std_ReturnType Rte_Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(BMS_HighestChargeCurrentAllow *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(BMS_HighestChargeVoltageAllow *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(DCHV_Command_Current_Reference *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_PowerMax_OBC_PowerMax(OBC_PowerMax *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Status_OBC_Status(OBC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(PFC_IPH1_RMS_0A1 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(PFC_IPH2_RMS_0A1 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(PFC_IPH3_RMS_0A1 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_PFCStatus_PFC_PFCStatus(PFC_PFCStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(PFC_Vdclink_V *data)
 *   Std_ReturnType Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(IdtMaxInputACCurrentEVSE *data)
 *   Std_ReturnType Rte_Read_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays(boolean *data)
 *   Std_ReturnType Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean *data)
 *   Std_ReturnType Rte_Read_PpRequestHWStopOBC_DeRequestHWStopOBC(boolean *data)
 *   Std_ReturnType Rte_Read_PpStopConditions_DeStopConditions(boolean *data)
 *   Std_ReturnType Rte_Read_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset(IdtVoltageCorrectionOffset *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_CurrentReference_DCDC_CurrentReference(DCDC_CurrentReference data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_VoltageReference_DCDC_VoltageReference(DCDC_VoltageReference data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed data)
 *   Std_ReturnType Rte_Write_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V(SUP_CommandVDCLink_V data)
 *   Std_ReturnType Rte_Write_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus(SUP_RequestPFCStatus data)
 *   Std_ReturnType Rte_Write_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV(SUP_RequestStatusDCHV data)
 *   Std_ReturnType Rte_Write_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result data)
 *   Std_ReturnType Rte_Write_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault(boolean data)
 *   Std_ReturnType Rte_Write_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_WdgM_AliveSupervision_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOBC_task10msB_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApOBC_CODE) RCtApOBC_task10msB(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOBC_task10msB
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  boolean Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop;
  boolean Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue;
  IdtInputVoltageMode Read_PpInputVoltageMode_DeInputVoltageMode;
  BMS_HighestChargeCurrentAllow Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow;
  BMS_HighestChargeVoltageAllow Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow;
  DCHV_ADC_IOUT Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT;
  DCHV_ADC_VOUT Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT;
  DCHV_Command_Current_Reference Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference;
  DCHV_DCHVStatus Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus;
  OBC_ChargingMode Read_PpInt_OBC_ChargingMode_OBC_ChargingMode;
  OBC_InputVoltageSt Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt;
  OBC_PowerMax Read_PpInt_OBC_PowerMax_OBC_PowerMax;
  OBC_Status Read_PpInt_OBC_Status_OBC_Status;
  PFC_IPH1_RMS_0A1 Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1;
  PFC_IPH2_RMS_0A1 Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1;
  PFC_IPH3_RMS_0A1 Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1;
  PFC_PFCStatus Read_PpInt_PFC_PFCStatus_PFC_PFCStatus;
  PFC_Vdclink_V Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V;
  IdtMaxInputACCurrentEVSE Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE;
  boolean Read_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue;
  boolean Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays;
  boolean Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled;
  boolean Read_PpRequestHWStopOBC_DeRequestHWStopOBC;
  boolean Read_PpStopConditions_DeStopConditions;
  IdtVoltageCorrectionOffset Read_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset;

  boolean PimPimOBCShutdownPathFSP;
  boolean PimPimOBCShutdownPathGPIO;

  IdtMaxOutputDCHVCurrent CalMaxOutputDCHVCurrent_data;
  IdtMaxOutputDCHVVoltage CalMaxOutputDCHVVoltage_data;
  IdtMinDCHVOutputVoltage CalMinDCHVOutputVoltage_data;
  IdtTimeRelaysPrechargeClosed CalTimeRelaysPrechargeClosed_data;
  CalVDCLinkRequiredMonophasic CalVDCLinkRequiredMonophasic_data;
  IdtVDCLinkRequiredTriphasic CalVDCLinkRequiredTriphasic_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBCShutdownPathFSP = *TSC_CtApOBC_Rte_Pim_PimOBCShutdownPathFSP();
  *TSC_CtApOBC_Rte_Pim_PimOBCShutdownPathFSP() = PimPimOBCShutdownPathFSP;
  PimPimOBCShutdownPathGPIO = *TSC_CtApOBC_Rte_Pim_PimOBCShutdownPathGPIO();
  *TSC_CtApOBC_Rte_Pim_PimOBCShutdownPathGPIO() = PimPimOBCShutdownPathGPIO;

  CalMaxOutputDCHVCurrent_data = TSC_CtApOBC_Rte_CData_CalMaxOutputDCHVCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxOutputDCHVVoltage_data = TSC_CtApOBC_Rte_CData_CalMaxOutputDCHVVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinDCHVOutputVoltage_data = TSC_CtApOBC_Rte_CData_CalMinDCHVOutputVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeRelaysPrechargeClosed_data = TSC_CtApOBC_Rte_CData_CalTimeRelaysPrechargeClosed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalVDCLinkRequiredMonophasic_data = TSC_CtApOBC_Rte_CData_CalVDCLinkRequiredMonophasic(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalVDCLinkRequiredTriphasic_data = TSC_CtApOBC_Rte_CData_CalVDCLinkRequiredTriphasic(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApOBC_Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(&Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(&Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInputVoltageMode_DeInputVoltageMode(&Read_PpInputVoltageMode_DeInputVoltageMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(&Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(&Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(&Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(&Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(&Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&Read_PpInt_OBC_ChargingMode_OBC_ChargingMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(&Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_OBC_PowerMax_OBC_PowerMax(&Read_PpInt_OBC_PowerMax_OBC_PowerMax); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_OBC_Status_OBC_Status(&Read_PpInt_OBC_Status_OBC_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(&Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(&Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(&Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_PFC_PFCStatus_PFC_PFCStatus(&Read_PpInt_PFC_PFCStatus_PFC_PFCStatus); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(&Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(&Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(&Read_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays(&Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(&Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpRequestHWStopOBC_DeRequestHWStopOBC(&Read_PpRequestHWStopOBC_DeRequestHWStopOBC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpStopConditions_DeStopConditions(&Read_PpStopConditions_DeStopConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Read_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset(&Read_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Write_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC(Rte_InitValue_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Write_PpInt_DCDC_CurrentReference_DCDC_CurrentReference(Rte_InitValue_PpInt_DCDC_CurrentReference_DCDC_CurrentReference); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Write_PpInt_DCDC_VoltageReference_DCDC_VoltageReference(Rte_InitValue_PpInt_DCDC_VoltageReference_DCDC_VoltageReference); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Write_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(Rte_InitValue_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Write_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V(Rte_InitValue_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Write_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus(Rte_InitValue_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Write_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV(Rte_InitValue_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Write_PpOBC_POST_Result_DeOBC_POST_Result(Rte_InitValue_PpOBC_POST_Result_DeOBC_POST_Result); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Write_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault(Rte_InitValue_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Write_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue(Rte_InitValue_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Write_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue(Rte_InitValue_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U, 0U, 0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOBC_Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_WdgM_AliveSupervision_E_NOT_OK:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApOBC_STOP_SEC_CODE
#include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApOBC_TestDefines(void)
{
  /* Enumeration Data Types */

  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_1 = Cx0_DCHV_STATUS_STANDBY;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_2 = Cx1_DCHV_STATUS_RUN;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_3 = Cx2_DCHV_STATUS_ERROR;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_4 = Cx3_DCHV_STATUS_ALARM;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_5 = Cx4_DCHV_STATUS_SAFE;

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;

  IdtInputVoltageMode Test_IdtInputVoltageMode_V_1 = IVM_NO_INPUT_DETECTED;
  IdtInputVoltageMode Test_IdtInputVoltageMode_V_2 = IVM_MONOPHASIC_INPUT;
  IdtInputVoltageMode Test_IdtInputVoltageMode_V_3 = IVM_TRIPHASIC_INPUT;

  IdtPOST_Result Test_IdtPOST_Result_V_1 = POST_ONGOING;
  IdtPOST_Result Test_IdtPOST_Result_V_2 = POST_OK;
  IdtPOST_Result Test_IdtPOST_Result_V_3 = POST_NOK;

  OBC_ChargingMode Test_OBC_ChargingMode_V_1 = Cx0_no_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_2 = Cx1_slow_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_3 = Cx2_China_fast_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_4 = Cx3_Euro_fast_charging;

  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_1 = Cx0_No_220V_AC;
  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_2 = Cx1_220V_AC_connected;
  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_3 = Cx2_220V_AC_disconnected;
  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_4 = Cx3_Invalid_value;

  OBC_Status Test_OBC_Status_V_1 = Cx0_off_mode;
  OBC_Status Test_OBC_Status_V_2 = Cx1_Init_mode;
  OBC_Status Test_OBC_Status_V_3 = Cx2_standby_mode;
  OBC_Status Test_OBC_Status_V_4 = Cx3_conversion_working_;
  OBC_Status Test_OBC_Status_V_5 = Cx4_error_mode;
  OBC_Status Test_OBC_Status_V_6 = Cx5_degradation_mode;
  OBC_Status Test_OBC_Status_V_7 = Cx6_reserved;
  OBC_Status Test_OBC_Status_V_8 = Cx7_invalid;

  PFC_PFCStatus Test_PFC_PFCStatus_V_1 = Cx0_PFC_STATE_STANDBY;
  PFC_PFCStatus Test_PFC_PFCStatus_V_2 = Cx1_PFC_STATE_TRI;
  PFC_PFCStatus Test_PFC_PFCStatus_V_3 = Cx2_PFC_STATE_MONO;
  PFC_PFCStatus Test_PFC_PFCStatus_V_4 = Cx3_PFC_STATE_RESET_ERROR;
  PFC_PFCStatus Test_PFC_PFCStatus_V_5 = Cx4_PFC_STATE_DISCHARGE;
  PFC_PFCStatus Test_PFC_PFCStatus_V_6 = Cx5_PFC_STATE_SHUTDOWN;
  PFC_PFCStatus Test_PFC_PFCStatus_V_7 = Cx6_PFC_STATE_START_TRI;
  PFC_PFCStatus Test_PFC_PFCStatus_V_8 = Cx7_PFC_STATE_START_MONO;
  PFC_PFCStatus Test_PFC_PFCStatus_V_9 = Cx8_PFC_STATE_ERROR;
  PFC_PFCStatus Test_PFC_PFCStatus_V_10 = Cx9_PFC_STATE_DIRECTPWM;

  SUP_RequestPFCStatus Test_SUP_RequestPFCStatus_V_1 = Cx0_PFC_STATE_STANDBY;
  SUP_RequestPFCStatus Test_SUP_RequestPFCStatus_V_2 = Cx1_PFC_STATE_TRI;
  SUP_RequestPFCStatus Test_SUP_RequestPFCStatus_V_3 = Cx2_PFC_STATE_MONO;
  SUP_RequestPFCStatus Test_SUP_RequestPFCStatus_V_4 = Cx3_PFC_STATE_RESET_ERROR;
  SUP_RequestPFCStatus Test_SUP_RequestPFCStatus_V_5 = Cx4_PFC_STATE_DISCHARGE;
  SUP_RequestPFCStatus Test_SUP_RequestPFCStatus_V_6 = Cx5_PFC_STATE_SHUTDOWN;

  SUP_RequestStatusDCHV Test_SUP_RequestStatusDCHV_V_1 = Cx0_DCHV_STATE_STANDBY;
  SUP_RequestStatusDCHV Test_SUP_RequestStatusDCHV_V_2 = Cx1_DCHV_STATE_RUN;
  SUP_RequestStatusDCHV Test_SUP_RequestStatusDCHV_V_3 = Cx2_DCHV_STATE_RESET_ERROR;
  SUP_RequestStatusDCHV Test_SUP_RequestStatusDCHV_V_4 = Cx3_DCHV_STATE_DISCHARGE;
  SUP_RequestStatusDCHV Test_SUP_RequestStatusDCHV_V_5 = Cx4_DCHV_STATE_SHUTDOWN;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
