/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_Adc.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
void TSC_Adc_SchM_Enter_Adc_DisableHwTrig();
void TSC_Adc_SchM_Exit_Adc_DisableHwTrig();
void TSC_Adc_SchM_Enter_Adc_EnableHwTrig();
void TSC_Adc_SchM_Exit_Adc_EnableHwTrig();
void TSC_Adc_SchM_Enter_Adc_GetGrpStatus();
void TSC_Adc_SchM_Exit_Adc_GetGrpStatus();
void TSC_Adc_SchM_Enter_Adc_GetStreamLastPtr();
void TSC_Adc_SchM_Exit_Adc_GetStreamLastPtr();
void TSC_Adc_SchM_Enter_Adc_PopQueue();
void TSC_Adc_SchM_Exit_Adc_PopQueue();
void TSC_Adc_SchM_Enter_Adc_PushQueue();
void TSC_Adc_SchM_Exit_Adc_PushQueue();
void TSC_Adc_SchM_Enter_Adc_ReadGroup();
void TSC_Adc_SchM_Exit_Adc_ReadGroup();
void TSC_Adc_SchM_Enter_Adc_ScheduleNext();
void TSC_Adc_SchM_Exit_Adc_ScheduleNext();
void TSC_Adc_SchM_Enter_Adc_ScheduleStart();
void TSC_Adc_SchM_Exit_Adc_ScheduleStart();
void TSC_Adc_SchM_Enter_Adc_ScheduleStop();
void TSC_Adc_SchM_Exit_Adc_ScheduleStop();
void TSC_Adc_SchM_Enter_Adc_StartGroup();
void TSC_Adc_SchM_Exit_Adc_StartGroup();
void TSC_Adc_SchM_Enter_Adc_StopGroup();
void TSC_Adc_SchM_Exit_Adc_StopGroup();
