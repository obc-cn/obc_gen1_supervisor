/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtHwAbsPOM.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtHwAbsPOM.h"
#include "TSC_CtHwAbsPOM.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtHwAbsPOM_Rte_Read_PpExtChLedRGBCtl_DeExtChLedBrCtl(IdtExtChLedRGBCtl *data)
{
  return Rte_Read_PpExtChLedRGBCtl_DeExtChLedBrCtl(data);
}

Std_ReturnType TSC_CtHwAbsPOM_Rte_Read_PpExtChLedRGBCtl_DeExtChLedGrCtl(IdtExtChLedRGBCtl *data)
{
  return Rte_Read_PpExtChLedRGBCtl_DeExtChLedGrCtl(data);
}

Std_ReturnType TSC_CtHwAbsPOM_Rte_Read_PpExtChLedRGBCtl_DeExtChLedRrCtl(IdtExtChLedRGBCtl *data)
{
  return Rte_Read_PpExtChLedRGBCtl_DeExtChLedRrCtl(data);
}

Std_ReturnType TSC_CtHwAbsPOM_Rte_Read_PpExtPlgLedrCtl_DeExtPlgLedrCtl(IdtExtPlgLedrCtl *data)
{
  return Rte_Read_PpExtPlgLedrCtl_DeExtPlgLedrCtl(data);
}

Std_ReturnType TSC_CtHwAbsPOM_Rte_Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays(boolean *data)
{
  return Rte_Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays(data);
}

Std_ReturnType TSC_CtHwAbsPOM_Rte_Read_PpOutputRelayMono_DeOutputRelayMono(boolean *data)
{
  return Rte_Read_PpOutputRelayMono_DeOutputRelayMono(data);
}








     /* Client Server Interfaces: */
Std_ReturnType TSC_CtHwAbsPOM_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtDutyCloseRelay  TSC_CtHwAbsPOM_Rte_CData_CalDutyKeepOutputPrechargeRelaysClosed(void)
{
  return (IdtDutyCloseRelay ) Rte_CData_CalDutyKeepOutputPrechargeRelaysClosed();
}
IdtDutyCloseRelay  TSC_CtHwAbsPOM_Rte_CData_CalDutyKeepRelayMonoClosed(void)
{
  return (IdtDutyCloseRelay ) Rte_CData_CalDutyKeepRelayMonoClosed();
}
IdtFrequencyActivateRelays  TSC_CtHwAbsPOM_Rte_CData_CalFrequencyActivateRelays(void)
{
  return (IdtFrequencyActivateRelays ) Rte_CData_CalFrequencyActivateRelays();
}
IdtFrequencyPWM  TSC_CtHwAbsPOM_Rte_CData_CalFrequencyPWM(void)
{
  return (IdtFrequencyPWM ) Rte_CData_CalFrequencyPWM();
}
IdtTimeCloseRelay  TSC_CtHwAbsPOM_Rte_CData_CalTimeCloseOutputPrechargeRelays(void)
{
  return (IdtTimeCloseRelay ) Rte_CData_CalTimeCloseOutputPrechargeRelays();
}
IdtTimeCloseRelay  TSC_CtHwAbsPOM_Rte_CData_CalTimeCloseRelayMono(void)
{
  return (IdtTimeCloseRelay ) Rte_CData_CalTimeCloseRelayMono();
}

     /* CtHwAbsPOM */
      /* CtHwAbsPOM */



