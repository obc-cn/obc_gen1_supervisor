/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApPSH.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApPSH
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApPSH>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * NvM_BlockIdType
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CtApPSH.h"
#include "TSC_CtApPSH.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApPSH_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtTimeDebChargePush: Integer in interval [0...15]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimePushInhDelay: Integer in interval [0...1000]
 *   Unit: [s], Factor: 1, Offset: 0
 * NvM_BlockIdType: Integer in interval [1...32767]
 * Rte_DT_IdtPSHNvMArray_0: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * BSI_LockedVehicle: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_Locked (0U)
 *   Cx1_Locked (1U)
 *   Cx2_Overlocked (2U)
 *   Cx3_Reserved (3U)
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * OBC_RechargeHMIState: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Disconnected (0U)
 *   Cx1_In_progress (1U)
 *   Cx2_Failure (2U)
 *   Cx3_Stopped (3U)
 *   Cx4_Finished (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 *
 * Array Types:
 * ============
 * IdtPSHNvMArray: Array with 4 element(s) of type Rte_DT_IdtPSHNvMArray_0
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   Rte_DT_IdtPSHNvMArray_0 *Rte_Pim_NvPSHBlockNeed_MirrorBlock(void)
 *     Returnvalue: Rte_DT_IdtPSHNvMArray_0* is of type IdtPSHNvMArray
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtTimePushInhDelay Rte_CData_CalTimePushInhDelay(void)
 *   IdtTimeDebChargePush Rte_CData_CalTimeDebChargePushHigh(void)
 *   IdtTimeDebChargePush Rte_CData_CalTimeDebChargePushLow(void)
 *   boolean Rte_CData_CalCtrlFlowChargePush(void)
 *
 *********************************************************************************************************************/


#define CtApPSH_START_SEC_CODE
#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPSH_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPSH_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPSH_CODE) RCtApPSH_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPSH_init
 *********************************************************************************************************************/

  IdtPSHNvMArray PimNvPSHBlockNeed_MirrorBlock;

  IdtTimePushInhDelay CalTimePushInhDelay_data;
  IdtTimeDebChargePush CalTimeDebChargePushHigh_data;
  IdtTimeDebChargePush CalTimeDebChargePushLow_data;
  boolean CalCtrlFlowChargePush_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/


  (void)memcpy(PimNvPSHBlockNeed_MirrorBlock, TSC_CtApPSH_Rte_Pim_NvPSHBlockNeed_MirrorBlock(), sizeof(IdtPSHNvMArray)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(TSC_CtApPSH_Rte_Pim_NvPSHBlockNeed_MirrorBlock(), PimNvPSHBlockNeed_MirrorBlock, sizeof(IdtPSHNvMArray)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */


  CalTimePushInhDelay_data = TSC_CtApPSH_Rte_CData_CalTimePushInhDelay(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeDebChargePushHigh_data = TSC_CtApPSH_Rte_CData_CalTimeDebChargePushHigh(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeDebChargePushLow_data = TSC_CtApPSH_Rte_CData_CalTimeDebChargePushLow(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowChargePush_data = TSC_CtApPSH_Rte_CData_CalCtrlFlowChargePush(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApPSH_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPSH_task100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpInputChargePushRaw_DeInputChargePushRaw(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(BSI_LockedVehicle *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState *data)
 *   Std_ReturnType Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpOutputChargePushFil_DeOutputChargePushFil(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvPSHBlockNeed_SetRamBlockStatus(boolean RamBlockStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPSH_task100ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPSH_CODE) RCtApPSH_task100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPSH_task100ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtAppRCDECUState Read_PpAppRCDECUState_DeAppRCDECUState;
  boolean Read_PpInputChargePushRaw_DeInputChargePushRaw;
  BSI_LockedVehicle Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle;
  OBC_RechargeHMIState Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState;
  boolean Read_PpShutdownAuthorization_DeShutdownAuthorization;

  IdtPSHNvMArray PimNvPSHBlockNeed_MirrorBlock;

  IdtTimePushInhDelay CalTimePushInhDelay_data;
  IdtTimeDebChargePush CalTimeDebChargePushHigh_data;
  IdtTimeDebChargePush CalTimeDebChargePushLow_data;
  boolean CalCtrlFlowChargePush_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/


  (void)memcpy(PimNvPSHBlockNeed_MirrorBlock, TSC_CtApPSH_Rte_Pim_NvPSHBlockNeed_MirrorBlock(), sizeof(IdtPSHNvMArray)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(TSC_CtApPSH_Rte_Pim_NvPSHBlockNeed_MirrorBlock(), PimNvPSHBlockNeed_MirrorBlock, sizeof(IdtPSHNvMArray)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */


  CalTimePushInhDelay_data = TSC_CtApPSH_Rte_CData_CalTimePushInhDelay(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeDebChargePushHigh_data = TSC_CtApPSH_Rte_CData_CalTimeDebChargePushHigh(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeDebChargePushLow_data = TSC_CtApPSH_Rte_CData_CalTimeDebChargePushLow(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowChargePush_data = TSC_CtApPSH_Rte_CData_CalCtrlFlowChargePush(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApPSH_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&Read_PpAppRCDECUState_DeAppRCDECUState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPSH_Rte_Read_PpInputChargePushRaw_DeInputChargePushRaw(&Read_PpInputChargePushRaw_DeInputChargePushRaw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPSH_Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(&Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPSH_Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(&Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPSH_Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(&Read_PpShutdownAuthorization_DeShutdownAuthorization); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPSH_Rte_Write_PpOutputChargePushFil_DeOutputChargePushFil(Rte_InitValue_PpOutputChargePushFil_DeOutputChargePushFil); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPSH_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPSH_Rte_Call_NvMService_AC3_SRBS_NvPSHBlockNeed_SetRamBlockStatus(FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_NvMService_AC3_SRBS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApPSH_STOP_SEC_CODE
#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApPSH_TestDefines(void)
{
  /* Enumeration Data Types */

  BSI_LockedVehicle Test_BSI_LockedVehicle_V_1 = Cx0_No_Locked;
  BSI_LockedVehicle Test_BSI_LockedVehicle_V_2 = Cx1_Locked;
  BSI_LockedVehicle Test_BSI_LockedVehicle_V_3 = Cx2_Overlocked;
  BSI_LockedVehicle Test_BSI_LockedVehicle_V_4 = Cx3_Reserved;

  IdtAppRCDECUState Test_IdtAppRCDECUState_V_1 = APP_STATE_0;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_2 = APP_STATE_1;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_3 = APP_STATE_2;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_4 = APP_STATE_3;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_5 = APP_STATE_4;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_6 = APP_STATE_5;

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;

  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_1 = Cx0_Disconnected;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_2 = Cx1_In_progress;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_3 = Cx2_Failure;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_4 = Cx3_Stopped;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_5 = Cx4_Finished;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_6 = Cx5_Reserved;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_7 = Cx6_Reserved;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_8 = Cx7_Reserved;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_Qac:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
