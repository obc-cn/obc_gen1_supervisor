/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApJDD.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApJDD
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApJDD>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventIdType
 *   
 *
 * Dem_UdsStatusByteType
 *   
 *
 * NvM_BlockIdType
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CtApJDD.h"
#include "TSC_CtApJDD.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApJDD_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DATA_ACQ_JDD_BSI_2: Integer in interval [0...255]
 * Dem_EventIdType: Integer in interval [1...65535]
 * NEW_JDD_OBC_DCDC_BYTE_0: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_1: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_2: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_3: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_4: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_5: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_6: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_7: Integer in interval [0...255]
 * NvM_BlockIdType: Integer in interval [1...32767]
 * Rte_DT_IdtArrayJDDNvMRamMirror_0: Integer in interval [0...255]
 * VCU_CDEAccJDD: Boolean
 * VCU_CompteurRazGct: Integer in interval [0...253]
 * VCU_CptTemporel: Integer in interval [0...4294967295]
 * VCU_Kilometrage: Integer in interval [0...16777214]
 * boolean: Boolean (standard type)
 * dtRef_VOID: DataReference
 * dtRef_const_VOID: DataReference
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dem_UdsStatusByteType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_UDS_STATUS_TF (1U)
 *   DEM_UDS_STATUS_TF_BflMask 1U (0b00000001)
 *   DEM_UDS_STATUS_TF_BflPn 0
 *   DEM_UDS_STATUS_TF_BflLn 1
 *   DEM_UDS_STATUS_TFTOC (2U)
 *   DEM_UDS_STATUS_TFTOC_BflMask 2U (0b00000010)
 *   DEM_UDS_STATUS_TFTOC_BflPn 1
 *   DEM_UDS_STATUS_TFTOC_BflLn 1
 *   DEM_UDS_STATUS_PDTC (4U)
 *   DEM_UDS_STATUS_PDTC_BflMask 4U (0b00000100)
 *   DEM_UDS_STATUS_PDTC_BflPn 2
 *   DEM_UDS_STATUS_PDTC_BflLn 1
 *   DEM_UDS_STATUS_CDTC (8U)
 *   DEM_UDS_STATUS_CDTC_BflMask 8U (0b00001000)
 *   DEM_UDS_STATUS_CDTC_BflPn 3
 *   DEM_UDS_STATUS_CDTC_BflLn 1
 *   DEM_UDS_STATUS_TNCSLC (16U)
 *   DEM_UDS_STATUS_TNCSLC_BflMask 16U (0b00010000)
 *   DEM_UDS_STATUS_TNCSLC_BflPn 4
 *   DEM_UDS_STATUS_TNCSLC_BflLn 1
 *   DEM_UDS_STATUS_TFSLC (32U)
 *   DEM_UDS_STATUS_TFSLC_BflMask 32U (0b00100000)
 *   DEM_UDS_STATUS_TFSLC_BflPn 5
 *   DEM_UDS_STATUS_TFSLC_BflLn 1
 *   DEM_UDS_STATUS_TNCTOC (64U)
 *   DEM_UDS_STATUS_TNCTOC_BflMask 64U (0b01000000)
 *   DEM_UDS_STATUS_TNCTOC_BflPn 6
 *   DEM_UDS_STATUS_TNCTOC_BflLn 1
 *   DEM_UDS_STATUS_WIR (128U)
 *   DEM_UDS_STATUS_WIR_BflMask 128U (0b10000000)
 *   DEM_UDS_STATUS_WIR_BflPn 7
 *   DEM_UDS_STATUS_WIR_BflLn 1
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * VCU_CDEApcJDD: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_APC_unchanged (0U)
 *   Cx1_APC_unchanged (1U)
 *   Cx2_APC_OFF (2U)
 *   Cx3_APC_ON_ (3U)
 * VCU_EtatPrincipSev: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Stop (0U)
 *   Cx1_Contact (1U)
 *   Cx2_Start (2U)
 *   Cx3_Reserved (3U)
 * VCU_EtatReseauElec: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_TI_NOMINAL (0U)
 *   Cx1_TI_DEGRADE (1U)
 *   Cx2_TI_DISPO_DEM (2U)
 *   Cx3_DEMARRAGE (3U)
 *   Cx4_REDEMARRAGE (4U)
 *   Cx5_TA_NOMINAL (5U)
 *   Cx6_TA_DEGRADE (6U)
 *   Cx7_TA_SECU (7U)
 *   Cx8_Reserved (8U)
 *   Cx9_Reserved (9U)
 *   CxA_Reserved (10U)
 *   CxB_Reserved (11U)
 *   CxC_Reserved (12U)
 *   CxD_Reserved (13U)
 *   CxE_Reserved (14U)
 *   CxF_Reserved (15U)
 * VCU_PosShuntJDD: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Park (0U)
 *   Cx1_Client (1U)
 *   Cx2_Absent (2U)
 *   Cx3_Undetermined (3U)
 *
 * Array Types:
 * ============
 * IdtArrayJDDNvMRamMirror: Array with 2000 element(s) of type Rte_DT_IdtArrayJDDNvMRamMirror_0
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   Rte_DT_IdtArrayJDDNvMRamMirror_0 *Rte_Pim_PimJDD_NvMRamMirror(void)
 *     Returnvalue: Rte_DT_IdtArrayJDDNvMRamMirror_0* is of type IdtArrayJDDNvMRamMirror
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   boolean Rte_CData_CalEnableJDD(void)
 *
 *********************************************************************************************************************/


#define CtApJDD_START_SEC_CODE
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpJDD_FrameNewJDD55FRcvd> of PortPrototype <PpJDD_FrameNewJDD55FRcvd>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApJDD_CODE) PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd
 *********************************************************************************************************************/

  IdtArrayJDDNvMRamMirror PimPimJDD_NvMRamMirror;

  boolean CalEnableJDD_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/


  (void)memcpy(PimPimJDD_NvMRamMirror, TSC_CtApJDD_Rte_Pim_PimJDD_NvMRamMirror(), sizeof(IdtArrayJDDNvMRamMirror)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(TSC_CtApJDD_Rte_Pim_PimJDD_NvMRamMirror(), PimPimJDD_NvMRamMirror, sizeof(IdtArrayJDDNvMRamMirror)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */


  CalEnableJDD_data = TSC_CtApJDD_Rte_CData_CalEnableJDD(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApJDD_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpJDD_FrameVCU552Rcvd> of PortPrototype <PpJDD_FrameVCU552Rcvd>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApJDD_CODE) PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd
 *********************************************************************************************************************/

  IdtArrayJDDNvMRamMirror PimPimJDD_NvMRamMirror;

  boolean CalEnableJDD_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/


  (void)memcpy(PimPimJDD_NvMRamMirror, TSC_CtApJDD_Rte_Pim_PimJDD_NvMRamMirror(), sizeof(IdtArrayJDDNvMRamMirror)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(TSC_CtApJDD_Rte_Pim_PimJDD_NvMRamMirror(), PimPimJDD_NvMRamMirror, sizeof(IdtArrayJDDNvMRamMirror)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */


  CalEnableJDD_data = TSC_CtApJDD_Rte_CData_CalEnableJDD(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApJDD_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApJDD_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApJDD_CODE) RCtApJDD_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApJDD_init
 *********************************************************************************************************************/

  IdtArrayJDDNvMRamMirror PimPimJDD_NvMRamMirror;

  boolean CalEnableJDD_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/


  (void)memcpy(PimPimJDD_NvMRamMirror, TSC_CtApJDD_Rte_Pim_PimJDD_NvMRamMirror(), sizeof(IdtArrayJDDNvMRamMirror)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(TSC_CtApJDD_Rte_Pim_PimJDD_NvMRamMirror(), PimPimJDD_NvMRamMirror, sizeof(IdtArrayJDDNvMRamMirror)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */


  CalEnableJDD_data = TSC_CtApJDD_Rte_CData_CalEnableJDD(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApJDD_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(boolean *data)
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpBusOffCANFault_DeBusOffCANFault(boolean *data)
 *   Std_ReturnType Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup(boolean *data)
 *   Std_ReturnType Rte_Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(DATA_ACQ_JDD_BSI_2 *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD(VCU_CDEAccJDD *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD(VCU_CDEApcJDD *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct(VCU_CompteurRazGct *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_CptTemporel_VCU_CptTemporel(VCU_CptTemporel *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev(VCU_EtatPrincipSev *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec(VCU_EtatReseauElec *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_Kilometrage_VCU_Kilometrage(VCU_Kilometrage *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(VCU_PosShuntJDD *data)
 *   Std_ReturnType Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
 *   Std_ReturnType Rte_Read_Pp_VCU_CompteurRazGct_VCU_CompteurRazGct(VCU_CompteurRazGct *data)
 *   Std_ReturnType Rte_Read_Pp_VCU_CptTemporel_VCU_CptTemporel(VCU_CptTemporel *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(NEW_JDD_OBC_DCDC_BYTE_0 data)
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(NEW_JDD_OBC_DCDC_BYTE_1 data)
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(NEW_JDD_OBC_DCDC_BYTE_2 data)
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(NEW_JDD_OBC_DCDC_BYTE_3 data)
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(NEW_JDD_OBC_DCDC_BYTE_4 data)
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(NEW_JDD_OBC_DCDC_BYTE_5 data)
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(NEW_JDD_OBC_DCDC_BYTE_6 data)
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(NEW_JDD_OBC_DCDC_BYTE_7 data)
 *   Std_ReturnType Rte_Write_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx(void)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x056216_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x056216_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x056317_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x056317_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0a0804_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0a0804_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0a084b_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0a084b_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0a9464_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0a9464_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0af864_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0af864_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0cf464_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0cf464_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x108093_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x108093_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c413_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c413_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c512_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c512_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c613_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c613_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c713_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c713_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120a11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120a11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120a12_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120a12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120b11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120b11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120b12_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120b12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120c64_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120c64_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120c98_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120c98_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120d64_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120d64_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120d98_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120d98_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d711_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d711_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d712_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d712_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d713_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d713_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d811_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d811_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d812_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d812_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d813_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d813_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d911_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d911_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d912_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d912_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d913_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d913_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12da11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12da11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12da12_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12da12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12da13_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12da13_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12db12_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12db12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12dc11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12dc11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12dd12_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12dd12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12de11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12de11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12df13_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12df13_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e012_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e012_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e111_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e111_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e213_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e213_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e319_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e319_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e712_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e712_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e811_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e811_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e912_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e912_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12ea11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12ea11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12f316_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12f316_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12f917_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12f917_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x13e919_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x13e919_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x166c64_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x166c64_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179e11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179e11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179e12_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179e12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179f11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179f11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179f12_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179f12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a0064_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a0064_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a7104_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a7104_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a714b_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a714b_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a7172_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a7172_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xc07988_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xc07988_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xc08913_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xc08913_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd18787_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd18787_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd1a087_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd1a087_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd20781_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd20781_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd2a081_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd2a081_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd38782_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd38782_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd38783_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd38783_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00081_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00081_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00087_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00087_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00214_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00214_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00362_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00362_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_ReadBlock(dtRef_VOID DstPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_SetRamBlockStatus(boolean RamBlockStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApJDD_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApJDD_CODE) RCtApJDD_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApJDD_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  boolean Read_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault;
  IdtAppRCDECUState Read_PpAppRCDECUState_DeAppRCDECUState;
  boolean Read_PpBusOffCANFault_DeBusOffCANFault;
  boolean Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup;
  boolean Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest;
  DATA_ACQ_JDD_BSI_2 Read_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2;
  VCU_CDEAccJDD Read_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD;
  VCU_CDEApcJDD Read_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD;
  VCU_CompteurRazGct Read_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct;
  VCU_CptTemporel Read_PpInt_VCU_CptTemporel_VCU_CptTemporel;
  VCU_EtatPrincipSev Read_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev;
  VCU_EtatReseauElec Read_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec;
  VCU_Kilometrage Read_PpInt_VCU_Kilometrage_VCU_Kilometrage;
  VCU_PosShuntJDD Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD;
  boolean Read_PpShutdownAuthorization_DeShutdownAuthorization;
  VCU_CompteurRazGct Read_Pp_VCU_CompteurRazGct_VCU_CompteurRazGct;
  VCU_CptTemporel Read_Pp_VCU_CptTemporel_VCU_CptTemporel;

  IdtArrayJDDNvMRamMirror PimPimJDD_NvMRamMirror;

  boolean CalEnableJDD_data;

  boolean Call_EvtInfo_DTC_0x056216_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x056216_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x056317_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x056317_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x0a0804_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x0a0804_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x0a084b_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x0a084b_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x0a9464_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x0a9464_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x0af864_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x0af864_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x0cf464_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x0cf464_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x108093_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x108093_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x10c413_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x10c413_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x10c512_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x10c512_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x10c613_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x10c613_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x10c713_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x10c713_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x120a11_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x120a11_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x120a12_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x120a12_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x120b11_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x120b11_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x120b12_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x120b12_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x120c64_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x120c64_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x120c98_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x120c98_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x120d64_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x120d64_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x120d98_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x120d98_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12d711_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12d711_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12d712_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12d712_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12d713_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12d713_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12d811_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12d811_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12d812_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12d812_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12d813_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12d813_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12d911_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12d911_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12d912_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12d912_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12d913_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12d913_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12da11_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12da11_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12da12_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12da12_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12da13_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12da13_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12db12_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12db12_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12dc11_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12dc11_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12dd12_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12dd12_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12de11_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12de11_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12df13_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12df13_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12e012_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12e012_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12e111_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12e111_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12e213_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12e213_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12e319_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12e319_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12e712_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12e712_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12e811_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12e811_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12e912_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12e912_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12ea11_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12ea11_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12f316_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12f316_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x12f917_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x12f917_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x13e919_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x13e919_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x166c64_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x166c64_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x179e11_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x179e11_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x179e12_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x179e12_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x179f11_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x179f11_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x179f12_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x179f12_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x1a0064_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x1a0064_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x1a7104_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x1a7104_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x1a714b_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x1a714b_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0x1a7172_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0x1a7172_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0xc07988_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0xc07988_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0xc08913_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0xc08913_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0xd18787_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0xd18787_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0xd1a087_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0xd1a087_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0xd20781_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0xd20781_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0xd2a081_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0xd2a081_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0xd38782_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0xd38782_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0xd38783_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0xd38783_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0xe00081_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0xe00081_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0xe00087_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0xe00087_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0xe00214_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0xe00214_GetEventStatus_UDSStatusByte = 0U;
  boolean Call_EvtInfo_DTC_0xe00362_GetEventFailed_EventFailed = FALSE;
  Dem_UdsStatusByteType Call_EvtInfo_DTC_0xe00362_GetEventStatus_UDSStatusByte = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/


  (void)memcpy(PimPimJDD_NvMRamMirror, TSC_CtApJDD_Rte_Pim_PimJDD_NvMRamMirror(), sizeof(IdtArrayJDDNvMRamMirror)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(TSC_CtApJDD_Rte_Pim_PimJDD_NvMRamMirror(), PimPimJDD_NvMRamMirror, sizeof(IdtArrayJDDNvMRamMirror)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */


  CalEnableJDD_data = TSC_CtApJDD_Rte_CData_CalEnableJDD(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApJDD_Rte_Read_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(&Read_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&Read_PpAppRCDECUState_DeAppRCDECUState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_PpBusOffCANFault_DeBusOffCANFault(&Read_PpBusOffCANFault_DeBusOffCANFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup(&Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(&Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(&Read_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD(&Read_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD(&Read_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct(&Read_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_PpInt_VCU_CptTemporel_VCU_CptTemporel(&Read_PpInt_VCU_CptTemporel_VCU_CptTemporel); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev(&Read_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec(&Read_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_PpInt_VCU_Kilometrage_VCU_Kilometrage(&Read_PpInt_VCU_Kilometrage_VCU_Kilometrage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(&Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(&Read_PpShutdownAuthorization_DeShutdownAuthorization); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_Pp_VCU_CompteurRazGct_VCU_CompteurRazGct(&Read_Pp_VCU_CompteurRazGct_VCU_CompteurRazGct); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Read_Pp_VCU_CptTemporel_VCU_CptTemporel(&Read_Pp_VCU_CptTemporel_VCU_CptTemporel); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Write_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError(Rte_InitValue_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx(); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x056216_GetEventFailed(&Call_EvtInfo_DTC_0x056216_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x056216_GetEventStatus(&Call_EvtInfo_DTC_0x056216_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x056317_GetEventFailed(&Call_EvtInfo_DTC_0x056317_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x056317_GetEventStatus(&Call_EvtInfo_DTC_0x056317_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0a0804_GetEventFailed(&Call_EvtInfo_DTC_0x0a0804_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0a0804_GetEventStatus(&Call_EvtInfo_DTC_0x0a0804_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0a084b_GetEventFailed(&Call_EvtInfo_DTC_0x0a084b_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0a084b_GetEventStatus(&Call_EvtInfo_DTC_0x0a084b_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0a9464_GetEventFailed(&Call_EvtInfo_DTC_0x0a9464_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0a9464_GetEventStatus(&Call_EvtInfo_DTC_0x0a9464_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0af864_GetEventFailed(&Call_EvtInfo_DTC_0x0af864_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0af864_GetEventStatus(&Call_EvtInfo_DTC_0x0af864_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0cf464_GetEventFailed(&Call_EvtInfo_DTC_0x0cf464_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0cf464_GetEventStatus(&Call_EvtInfo_DTC_0x0cf464_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x108093_GetEventFailed(&Call_EvtInfo_DTC_0x108093_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x108093_GetEventStatus(&Call_EvtInfo_DTC_0x108093_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c413_GetEventFailed(&Call_EvtInfo_DTC_0x10c413_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c413_GetEventStatus(&Call_EvtInfo_DTC_0x10c413_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c512_GetEventFailed(&Call_EvtInfo_DTC_0x10c512_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c512_GetEventStatus(&Call_EvtInfo_DTC_0x10c512_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c613_GetEventFailed(&Call_EvtInfo_DTC_0x10c613_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c613_GetEventStatus(&Call_EvtInfo_DTC_0x10c613_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c713_GetEventFailed(&Call_EvtInfo_DTC_0x10c713_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c713_GetEventStatus(&Call_EvtInfo_DTC_0x10c713_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120a11_GetEventFailed(&Call_EvtInfo_DTC_0x120a11_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120a11_GetEventStatus(&Call_EvtInfo_DTC_0x120a11_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120a12_GetEventFailed(&Call_EvtInfo_DTC_0x120a12_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120a12_GetEventStatus(&Call_EvtInfo_DTC_0x120a12_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120b11_GetEventFailed(&Call_EvtInfo_DTC_0x120b11_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120b11_GetEventStatus(&Call_EvtInfo_DTC_0x120b11_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120b12_GetEventFailed(&Call_EvtInfo_DTC_0x120b12_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120b12_GetEventStatus(&Call_EvtInfo_DTC_0x120b12_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120c64_GetEventFailed(&Call_EvtInfo_DTC_0x120c64_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120c64_GetEventStatus(&Call_EvtInfo_DTC_0x120c64_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120c98_GetEventFailed(&Call_EvtInfo_DTC_0x120c98_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120c98_GetEventStatus(&Call_EvtInfo_DTC_0x120c98_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120d64_GetEventFailed(&Call_EvtInfo_DTC_0x120d64_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120d64_GetEventStatus(&Call_EvtInfo_DTC_0x120d64_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120d98_GetEventFailed(&Call_EvtInfo_DTC_0x120d98_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120d98_GetEventStatus(&Call_EvtInfo_DTC_0x120d98_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d711_GetEventFailed(&Call_EvtInfo_DTC_0x12d711_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d711_GetEventStatus(&Call_EvtInfo_DTC_0x12d711_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d712_GetEventFailed(&Call_EvtInfo_DTC_0x12d712_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d712_GetEventStatus(&Call_EvtInfo_DTC_0x12d712_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d713_GetEventFailed(&Call_EvtInfo_DTC_0x12d713_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d713_GetEventStatus(&Call_EvtInfo_DTC_0x12d713_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d811_GetEventFailed(&Call_EvtInfo_DTC_0x12d811_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d811_GetEventStatus(&Call_EvtInfo_DTC_0x12d811_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d812_GetEventFailed(&Call_EvtInfo_DTC_0x12d812_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d812_GetEventStatus(&Call_EvtInfo_DTC_0x12d812_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d813_GetEventFailed(&Call_EvtInfo_DTC_0x12d813_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d813_GetEventStatus(&Call_EvtInfo_DTC_0x12d813_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d911_GetEventFailed(&Call_EvtInfo_DTC_0x12d911_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d911_GetEventStatus(&Call_EvtInfo_DTC_0x12d911_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d912_GetEventFailed(&Call_EvtInfo_DTC_0x12d912_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d912_GetEventStatus(&Call_EvtInfo_DTC_0x12d912_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d913_GetEventFailed(&Call_EvtInfo_DTC_0x12d913_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d913_GetEventStatus(&Call_EvtInfo_DTC_0x12d913_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12da11_GetEventFailed(&Call_EvtInfo_DTC_0x12da11_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12da11_GetEventStatus(&Call_EvtInfo_DTC_0x12da11_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12da12_GetEventFailed(&Call_EvtInfo_DTC_0x12da12_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12da12_GetEventStatus(&Call_EvtInfo_DTC_0x12da12_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12da13_GetEventFailed(&Call_EvtInfo_DTC_0x12da13_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12da13_GetEventStatus(&Call_EvtInfo_DTC_0x12da13_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12db12_GetEventFailed(&Call_EvtInfo_DTC_0x12db12_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12db12_GetEventStatus(&Call_EvtInfo_DTC_0x12db12_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12dc11_GetEventFailed(&Call_EvtInfo_DTC_0x12dc11_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12dc11_GetEventStatus(&Call_EvtInfo_DTC_0x12dc11_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12dd12_GetEventFailed(&Call_EvtInfo_DTC_0x12dd12_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12dd12_GetEventStatus(&Call_EvtInfo_DTC_0x12dd12_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12de11_GetEventFailed(&Call_EvtInfo_DTC_0x12de11_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12de11_GetEventStatus(&Call_EvtInfo_DTC_0x12de11_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12df13_GetEventFailed(&Call_EvtInfo_DTC_0x12df13_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12df13_GetEventStatus(&Call_EvtInfo_DTC_0x12df13_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e012_GetEventFailed(&Call_EvtInfo_DTC_0x12e012_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e012_GetEventStatus(&Call_EvtInfo_DTC_0x12e012_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e111_GetEventFailed(&Call_EvtInfo_DTC_0x12e111_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e111_GetEventStatus(&Call_EvtInfo_DTC_0x12e111_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e213_GetEventFailed(&Call_EvtInfo_DTC_0x12e213_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e213_GetEventStatus(&Call_EvtInfo_DTC_0x12e213_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e319_GetEventFailed(&Call_EvtInfo_DTC_0x12e319_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e319_GetEventStatus(&Call_EvtInfo_DTC_0x12e319_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e712_GetEventFailed(&Call_EvtInfo_DTC_0x12e712_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e712_GetEventStatus(&Call_EvtInfo_DTC_0x12e712_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e811_GetEventFailed(&Call_EvtInfo_DTC_0x12e811_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e811_GetEventStatus(&Call_EvtInfo_DTC_0x12e811_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e912_GetEventFailed(&Call_EvtInfo_DTC_0x12e912_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e912_GetEventStatus(&Call_EvtInfo_DTC_0x12e912_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12ea11_GetEventFailed(&Call_EvtInfo_DTC_0x12ea11_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12ea11_GetEventStatus(&Call_EvtInfo_DTC_0x12ea11_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12f316_GetEventFailed(&Call_EvtInfo_DTC_0x12f316_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12f316_GetEventStatus(&Call_EvtInfo_DTC_0x12f316_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12f917_GetEventFailed(&Call_EvtInfo_DTC_0x12f917_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12f917_GetEventStatus(&Call_EvtInfo_DTC_0x12f917_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x13e919_GetEventFailed(&Call_EvtInfo_DTC_0x13e919_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x13e919_GetEventStatus(&Call_EvtInfo_DTC_0x13e919_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x166c64_GetEventFailed(&Call_EvtInfo_DTC_0x166c64_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x166c64_GetEventStatus(&Call_EvtInfo_DTC_0x166c64_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179e11_GetEventFailed(&Call_EvtInfo_DTC_0x179e11_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179e11_GetEventStatus(&Call_EvtInfo_DTC_0x179e11_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179e12_GetEventFailed(&Call_EvtInfo_DTC_0x179e12_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179e12_GetEventStatus(&Call_EvtInfo_DTC_0x179e12_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179f11_GetEventFailed(&Call_EvtInfo_DTC_0x179f11_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179f11_GetEventStatus(&Call_EvtInfo_DTC_0x179f11_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179f12_GetEventFailed(&Call_EvtInfo_DTC_0x179f12_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179f12_GetEventStatus(&Call_EvtInfo_DTC_0x179f12_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a0064_GetEventFailed(&Call_EvtInfo_DTC_0x1a0064_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a0064_GetEventStatus(&Call_EvtInfo_DTC_0x1a0064_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a7104_GetEventFailed(&Call_EvtInfo_DTC_0x1a7104_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a7104_GetEventStatus(&Call_EvtInfo_DTC_0x1a7104_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a714b_GetEventFailed(&Call_EvtInfo_DTC_0x1a714b_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a714b_GetEventStatus(&Call_EvtInfo_DTC_0x1a714b_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a7172_GetEventFailed(&Call_EvtInfo_DTC_0x1a7172_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a7172_GetEventStatus(&Call_EvtInfo_DTC_0x1a7172_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xc07988_GetEventFailed(&Call_EvtInfo_DTC_0xc07988_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xc07988_GetEventStatus(&Call_EvtInfo_DTC_0xc07988_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xc08913_GetEventFailed(&Call_EvtInfo_DTC_0xc08913_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xc08913_GetEventStatus(&Call_EvtInfo_DTC_0xc08913_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd18787_GetEventFailed(&Call_EvtInfo_DTC_0xd18787_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd18787_GetEventStatus(&Call_EvtInfo_DTC_0xd18787_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd1a087_GetEventFailed(&Call_EvtInfo_DTC_0xd1a087_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd1a087_GetEventStatus(&Call_EvtInfo_DTC_0xd1a087_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd20781_GetEventFailed(&Call_EvtInfo_DTC_0xd20781_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd20781_GetEventStatus(&Call_EvtInfo_DTC_0xd20781_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd2a081_GetEventFailed(&Call_EvtInfo_DTC_0xd2a081_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd2a081_GetEventStatus(&Call_EvtInfo_DTC_0xd2a081_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd38782_GetEventFailed(&Call_EvtInfo_DTC_0xd38782_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd38782_GetEventStatus(&Call_EvtInfo_DTC_0xd38782_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd38783_GetEventFailed(&Call_EvtInfo_DTC_0xd38783_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd38783_GetEventStatus(&Call_EvtInfo_DTC_0xd38783_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00081_GetEventFailed(&Call_EvtInfo_DTC_0xe00081_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00081_GetEventStatus(&Call_EvtInfo_DTC_0xe00081_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00087_GetEventFailed(&Call_EvtInfo_DTC_0xe00087_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00087_GetEventStatus(&Call_EvtInfo_DTC_0xe00087_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00214_GetEventFailed(&Call_EvtInfo_DTC_0xe00214_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00214_GetEventStatus(&Call_EvtInfo_DTC_0xe00214_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00362_GetEventFailed(&Call_EvtInfo_DTC_0xe00362_GetEventFailed_EventFailed); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00362_GetEventStatus(&Call_EvtInfo_DTC_0xe00362_GetEventStatus_UDSStatusByte); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DiagnosticInfo_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_ReadBlock(NULL_PTR); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_NvMService_AC3_SRBS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_SetRamBlockStatus(FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_NvMService_AC3_SRBS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApJDD_Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_WriteBlock(NULL_PTR); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_NvMService_AC3_SRBS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApJDD_STOP_SEC_CODE
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApJDD_TestDefines(void)
{
  /* Enumeration Data Types */

  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_1 = DEM_UDS_STATUS_TF;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_1_BflMask = DEM_UDS_STATUS_TF_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_1_BflPn = DEM_UDS_STATUS_TF_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_1_BflLn = DEM_UDS_STATUS_TF_BflLn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_2 = DEM_UDS_STATUS_TFTOC;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_2_BflMask = DEM_UDS_STATUS_TFTOC_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_2_BflPn = DEM_UDS_STATUS_TFTOC_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_2_BflLn = DEM_UDS_STATUS_TFTOC_BflLn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_3 = DEM_UDS_STATUS_PDTC;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_3_BflMask = DEM_UDS_STATUS_PDTC_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_3_BflPn = DEM_UDS_STATUS_PDTC_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_3_BflLn = DEM_UDS_STATUS_PDTC_BflLn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_4 = DEM_UDS_STATUS_CDTC;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_4_BflMask = DEM_UDS_STATUS_CDTC_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_4_BflPn = DEM_UDS_STATUS_CDTC_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_4_BflLn = DEM_UDS_STATUS_CDTC_BflLn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_5 = DEM_UDS_STATUS_TNCSLC;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_5_BflMask = DEM_UDS_STATUS_TNCSLC_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_5_BflPn = DEM_UDS_STATUS_TNCSLC_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_5_BflLn = DEM_UDS_STATUS_TNCSLC_BflLn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_6 = DEM_UDS_STATUS_TFSLC;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_6_BflMask = DEM_UDS_STATUS_TFSLC_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_6_BflPn = DEM_UDS_STATUS_TFSLC_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_6_BflLn = DEM_UDS_STATUS_TFSLC_BflLn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_7 = DEM_UDS_STATUS_TNCTOC;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_7_BflMask = DEM_UDS_STATUS_TNCTOC_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_7_BflPn = DEM_UDS_STATUS_TNCTOC_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_7_BflLn = DEM_UDS_STATUS_TNCTOC_BflLn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_8 = DEM_UDS_STATUS_WIR;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_8_BflMask = DEM_UDS_STATUS_WIR_BflMask;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_8_BflPn = DEM_UDS_STATUS_WIR_BflPn;
  Dem_UdsStatusByteType Test_Dem_UdsStatusByteType_V_8_BflLn = DEM_UDS_STATUS_WIR_BflLn;

  IdtAppRCDECUState Test_IdtAppRCDECUState_V_1 = APP_STATE_0;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_2 = APP_STATE_1;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_3 = APP_STATE_2;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_4 = APP_STATE_3;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_5 = APP_STATE_4;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_6 = APP_STATE_5;

  VCU_CDEApcJDD Test_VCU_CDEApcJDD_V_1 = Cx0_APC_unchanged;
  VCU_CDEApcJDD Test_VCU_CDEApcJDD_V_2 = Cx1_APC_unchanged;
  VCU_CDEApcJDD Test_VCU_CDEApcJDD_V_3 = Cx2_APC_OFF;
  VCU_CDEApcJDD Test_VCU_CDEApcJDD_V_4 = Cx3_APC_ON_;

  VCU_EtatPrincipSev Test_VCU_EtatPrincipSev_V_1 = Cx0_Stop;
  VCU_EtatPrincipSev Test_VCU_EtatPrincipSev_V_2 = Cx1_Contact;
  VCU_EtatPrincipSev Test_VCU_EtatPrincipSev_V_3 = Cx2_Start;
  VCU_EtatPrincipSev Test_VCU_EtatPrincipSev_V_4 = Cx3_Reserved;

  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_1 = Cx0_TI_NOMINAL;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_2 = Cx1_TI_DEGRADE;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_3 = Cx2_TI_DISPO_DEM;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_4 = Cx3_DEMARRAGE;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_5 = Cx4_REDEMARRAGE;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_6 = Cx5_TA_NOMINAL;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_7 = Cx6_TA_DEGRADE;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_8 = Cx7_TA_SECU;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_9 = Cx8_Reserved;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_10 = Cx9_Reserved;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_11 = CxA_Reserved;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_12 = CxB_Reserved;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_13 = CxC_Reserved;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_14 = CxD_Reserved;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_15 = CxE_Reserved;
  VCU_EtatReseauElec Test_VCU_EtatReseauElec_V_16 = CxF_Reserved;

  VCU_PosShuntJDD Test_VCU_PosShuntJDD_V_1 = Cx0_Park;
  VCU_PosShuntJDD Test_VCU_PosShuntJDD_V_2 = Cx1_Client;
  VCU_PosShuntJDD Test_VCU_PosShuntJDD_V_3 = Cx2_Absent;
  VCU_PosShuntJDD Test_VCU_PosShuntJDD_V_4 = Cx3_Undetermined;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_Qac:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
