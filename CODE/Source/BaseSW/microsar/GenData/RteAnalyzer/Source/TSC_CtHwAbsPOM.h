/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtHwAbsPOM.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtHwAbsPOM_Rte_Read_PpExtChLedRGBCtl_DeExtChLedBrCtl(IdtExtChLedRGBCtl *data);
Std_ReturnType TSC_CtHwAbsPOM_Rte_Read_PpExtChLedRGBCtl_DeExtChLedGrCtl(IdtExtChLedRGBCtl *data);
Std_ReturnType TSC_CtHwAbsPOM_Rte_Read_PpExtChLedRGBCtl_DeExtChLedRrCtl(IdtExtChLedRGBCtl *data);
Std_ReturnType TSC_CtHwAbsPOM_Rte_Read_PpExtPlgLedrCtl_DeExtPlgLedrCtl(IdtExtPlgLedrCtl *data);
Std_ReturnType TSC_CtHwAbsPOM_Rte_Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays(boolean *data);
Std_ReturnType TSC_CtHwAbsPOM_Rte_Read_PpOutputRelayMono_DeOutputRelayMono(boolean *data);

/** Client server interfaces */
Std_ReturnType TSC_CtHwAbsPOM_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);

/** SW-C local Calibration Parameters */
IdtDutyCloseRelay  TSC_CtHwAbsPOM_Rte_CData_CalDutyKeepOutputPrechargeRelaysClosed(void);
IdtDutyCloseRelay  TSC_CtHwAbsPOM_Rte_CData_CalDutyKeepRelayMonoClosed(void);
IdtFrequencyActivateRelays  TSC_CtHwAbsPOM_Rte_CData_CalFrequencyActivateRelays(void);
IdtFrequencyPWM  TSC_CtHwAbsPOM_Rte_CData_CalFrequencyPWM(void);
IdtTimeCloseRelay  TSC_CtHwAbsPOM_Rte_CData_CalTimeCloseOutputPrechargeRelays(void);
IdtTimeCloseRelay  TSC_CtHwAbsPOM_Rte_CData_CalTimeCloseRelayMono(void);




