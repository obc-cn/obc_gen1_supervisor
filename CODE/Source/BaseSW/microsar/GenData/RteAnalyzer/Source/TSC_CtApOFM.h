/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApOFM.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApOFM_Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpDiagnosticNoACInput_DeDiagnosticNoACInput(boolean *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(boolean *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(DCDC_CurrentReference *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(DCHV_Command_Current_Reference *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(DCHV_IOM_ERR_CAP_FAIL_H *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(DCHV_IOM_ERR_CAP_FAIL_L *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(DCHV_IOM_ERR_OC_IOUT *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(DCHV_IOM_ERR_OC_NEG *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(DCHV_IOM_ERR_OC_POS *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(DCHV_IOM_ERR_OT *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(DCHV_IOM_ERR_OT_IN *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(DCHV_IOM_ERR_OT_NTC_MOD5 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(DCHV_IOM_ERR_OT_NTC_MOD6 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(DCHV_IOM_ERR_OV_VOUT *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(DCHV_IOM_ERR_UV_12V *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(PFC_IOM_ERR_OC_PH1 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(PFC_IOM_ERR_OC_PH2 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(PFC_IOM_ERR_OC_PH3 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(PFC_IOM_ERR_OC_SHUNT1 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(PFC_IOM_ERR_OC_SHUNT2 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(PFC_IOM_ERR_OC_SHUNT3 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(PFC_IOM_ERR_OT_NTC1_M1 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(PFC_IOM_ERR_OT_NTC1_M3 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(PFC_IOM_ERR_OT_NTC1_M4 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(PFC_IOM_ERR_OV_DCLINK *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(PFC_IOM_ERR_OV_VPH12 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(PFC_IOM_ERR_OV_VPH23 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(PFC_IOM_ERR_OV_VPH31 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(PFC_IOM_ERR_UVLO_ISO4 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(PFC_IOM_ERR_UVLO_ISO7 *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(PFC_IOM_ERR_UV_12V *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOBCFramesReception_Error_DeOBCFramesReception_Error(boolean *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error(IdtFaultLevel *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error(IdtFaultLevel *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error(IdtFaultLevel *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error(IdtFaultLevel *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error(IdtFaultLevel *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error(IdtFaultLevel *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error(IdtFaultLevel *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error(IdtFaultLevel *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue(boolean *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas *data);
Std_ReturnType TSC_CtApOFM_Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApOFM_Rte_Write_PpFaultChargeSoftStop_DeFaultChargeSoftStop(boolean data);
Std_ReturnType TSC_CtApOFM_Rte_Write_PpInt_OBC_Fault_OBC_Fault(OBC_Fault data);
Std_ReturnType TSC_CtApOFM_Rte_Write_PpOBCFaultsList_DeInlet_OvertempACSensorFault(boolean data);
Std_ReturnType TSC_CtApOFM_Rte_Write_PpOBCFaultsList_DeInlet_OvertempDCSensorFault(boolean data);
Std_ReturnType TSC_CtApOFM_Rte_Write_PpOBCFaultsList_DeOBC_HWErrors_Fault(boolean data);
Std_ReturnType TSC_CtApOFM_Rte_Write_PpOBCFaultsList_DeOBC_InternalComFault(boolean data);
Std_ReturnType TSC_CtApOFM_Rte_Write_PpOBCFaultsList_DeOBC_OvercurrentOutputFault(boolean data);
Std_ReturnType TSC_CtApOFM_Rte_Write_PpOBCFaultsList_DeOBC_OvertemperatureFault(boolean data);
Std_ReturnType TSC_CtApOFM_Rte_Write_PpOBCFaultsList_DeOBC_OvervoltageOutputFault(boolean data);

/** Client server interfaces */
Std_ReturnType TSC_CtApOFM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data);

/** Calibration Component Calibration Parameters */
IdtOBCFaultTimeToRetryDefault  TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(void);
IdtOBCFaultTimeToRetryOvertemp  TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(void);
IdtOBCFaultTimeToRetryVoltageError  TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(void);
IdtDebounceOvercurrentDCHV  TSC_CtApOFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(void);

/** SW-C local Calibration Parameters */
IdtOBC_MaxNumberRetries  TSC_CtApOFM_Rte_CData_CalOBC_MaxNumberRetries(void);

/** Per Instance Memories */
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error(void);
IdtFaultLevel *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error(void);



