/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApWUM.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApWUM.h"
#include "TSC_CtApWUM.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_CtApWUM_Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_ReadBlock(dtRef_VOID DstPtr)
{
  return Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_ReadBlock(DstPtr);
}
Std_ReturnType TSC_CtApWUM_Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(ComM_ModeType ComMode)
{
  return Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(ComMode);
}
Std_ReturnType TSC_CtApWUM_Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(ComM_ModeType ComMode)
{
  return Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(ComMode);
}
Std_ReturnType TSC_CtApWUM_Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN(void)
{
  return Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN();
}
Std_ReturnType TSC_CtApWUM_Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN(void)
{
  return Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN();
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApWUM_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
{
  return Rte_Read_PpAppRCDECUState_DeAppRCDECUState(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpDutyControlPilot_DeDutyControlPilot(IdtDutyControlPilot *data)
{
  return Rte_Read_PpDutyControlPilot_DeDutyControlPilot(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(BSI_PostDriveWakeup *data)
{
  return Rte_Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(BSI_PreDriveWakeup *data)
{
  return Rte_Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(BSI_VCUHeatPumpWorkingMode *data)
{
  return Rte_Read_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP(COUPURE_CONSO_CTP *data)
{
  return Rte_Read_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(COUPURE_CONSO_CTPE2 *data)
{
  return Rte_Read_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
{
  return Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection *data)
{
  return Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType *data)
{
  return Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState *data)
{
  return Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState *data)
{
  return Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM(VCU_DDEGMVSEEM *data)
{
  return Rte_Read_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller(VCU_DMDActivChiller *data)
{
  return Rte_Read_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(VCU_DMDMeap2SEEM *data)
{
  return Rte_Read_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(VCU_DiagMuxOnPwt *data)
{
  return Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
{
  return Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(VCU_PrecondElecWakeup *data)
{
  return Rte_Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(VCU_StopDelayedHMIWakeup *data)
{
  return Rte_Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(data);
}




Std_ReturnType TSC_CtApWUM_Rte_Write_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC(boolean data)
{
  return Rte_Write_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Write_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP(boolean data)
{
  return Rte_Write_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Write_PpDiagToolsRequest_DeDiagToolsRequest(boolean data)
{
  return Rte_Write_PpDiagToolsRequest_DeDiagToolsRequest(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Write_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(boolean data)
{
  return Rte_Write_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Write_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP(boolean data)
{
  return Rte_Write_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Write_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP(boolean data)
{
  return Rte_Write_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Write_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState(SUPV_CoolingWupState data)
{
  return Rte_Write_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Write_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(SUPV_HVBattChargeWupState data)
{
  return Rte_Write_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Write_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(SUPV_HoldDiscontactorWupState data)
{
  return Rte_Write_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Write_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(SUPV_PIStateInfoWupState data)
{
  return Rte_Write_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Write_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP(boolean data)
{
  return Rte_Write_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApWUM_Rte_Call_PpDGN_AppCleanDTCs_OpAppCleanDTCs(void)
{
  return Rte_Call_PpDGN_AppCleanDTCs_OpAppCleanDTCs();
}
Std_ReturnType TSC_CtApWUM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
{
  return Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(frameNum, byteNum, data);
}


     /* Service calls */
Std_ReturnType TSC_CtApWUM_Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr)
{
  return Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_WriteBlock(SrcPtr);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApWUM_Rte_Read_PpCANComRequest_DeCANComRequest(sint32 *data)
{
  return Rte_Read_PpCANComRequest_DeCANComRequest(data);
}

Std_ReturnType TSC_CtApWUM_Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
{
  return Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtMaxTimeDiagTools  TSC_CtApWUM_Rte_CData_CalMaxTimeDiagTools(void)
{
  return (IdtMaxTimeDiagTools ) Rte_CData_CalMaxTimeDiagTools();
}
IdtTimeWakeupHoldDiscontactorMax  TSC_CtApWUM_Rte_CData_CalTimeWakeupHoldDiscontactorMax(void)
{
  return (IdtTimeWakeupHoldDiscontactorMax ) Rte_CData_CalTimeWakeupHoldDiscontactorMax();
}
IdtDebounceHoldNetworkCom  TSC_CtApWUM_Rte_CData_CalDebounceHoldNetworkCom(void)
{
  return (IdtDebounceHoldNetworkCom ) Rte_CData_CalDebounceHoldNetworkCom();
}
IdtDebounceTempoEndCharge  TSC_CtApWUM_Rte_CData_CalDebounceTempoEndCharge(void)
{
  return (IdtDebounceTempoEndCharge ) Rte_CData_CalDebounceTempoEndCharge();
}
IdtMaxTimeShutdown  TSC_CtApWUM_Rte_CData_CalMaxTimeShutdown(void)
{
  return (IdtMaxTimeShutdown ) Rte_CData_CalMaxTimeShutdown();
}
IdtTimeWakeupCoolingMax  TSC_CtApWUM_Rte_CData_CalTimeWakeupCoolingMax(void)
{
  return (IdtTimeWakeupCoolingMax ) Rte_CData_CalTimeWakeupCoolingMax();
}
IdtTimeWakeupCoolingMin  TSC_CtApWUM_Rte_CData_CalTimeWakeupCoolingMin(void)
{
  return (IdtTimeWakeupCoolingMin ) Rte_CData_CalTimeWakeupCoolingMin();
}
IdtTimeWakeupHoldDiscontactorMin  TSC_CtApWUM_Rte_CData_CalTimeWakeupHoldDiscontactorMin(void)
{
  return (IdtTimeWakeupHoldDiscontactorMin ) Rte_CData_CalTimeWakeupHoldDiscontactorMin();
}
IdtTimeWakeupPiStateInfo  TSC_CtApWUM_Rte_CData_CalTimeWakeupPiStateInfo(void)
{
  return (IdtTimeWakeupPiStateInfo ) Rte_CData_CalTimeWakeupPiStateInfo();
}
uint8  TSC_CtApWUM_Rte_CData_NvWUMBlockNeed_DefaultValue(void)
{
  return (uint8 ) Rte_CData_NvWUMBlockNeed_DefaultValue();
}

     /* CtApWUM */
      /* CtApWUM */

/** Per Instance Memories */
uint8 *TSC_CtApWUM_Rte_Pim_NvWUMBlockNeed_MirrorBlock(void)
{
  return Rte_Pim_NvWUMBlockNeed_MirrorBlock();
}



