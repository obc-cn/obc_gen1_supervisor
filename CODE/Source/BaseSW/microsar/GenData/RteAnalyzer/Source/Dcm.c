/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Dcm.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  Dcm
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <Dcm>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_CommunicationModeType
 *   
 *
 * Dcm_ConfirmationStatusType
 *   
 *
 * Dcm_DiagnosticSessionControlType
 *   
 *
 * Dcm_EcuResetType
 *   
 *
 * Dcm_NegativeResponseCodeType
 *   
 *
 * Dcm_OpStatusType
 *   
 *
 * Dcm_ProtocolType
 *   
 *
 * Dcm_RequestKindType
 *   
 *
 * Dcm_SecLevelType
 *   
 *
 * Dcm_SesCtrlType
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_Dcm.h"
#include "TSC_Dcm.h"
#include "SchM_Dcm.h"
#include "TSC_SchM_Dcm.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void Dcm_TestDefines(void);

typedef P2FUNC(Std_ReturnType, RTE_CODE, FncPtrType)(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_CommunicationModeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_ENABLE_RX_TX_NORM (0U)
 *   DCM_ENABLE_RX_DISABLE_TX_NORM (1U)
 *   DCM_DISABLE_RX_ENABLE_TX_NORM (2U)
 *   DCM_DISABLE_RX_TX_NORMAL (3U)
 *   DCM_ENABLE_RX_TX_NM (4U)
 *   DCM_ENABLE_RX_DISABLE_TX_NM (5U)
 *   DCM_DISABLE_RX_ENABLE_TX_NM (6U)
 *   DCM_DISABLE_RX_TX_NM (7U)
 *   DCM_ENABLE_RX_TX_NORM_NM (8U)
 *   DCM_ENABLE_RX_DISABLE_TX_NORM_NM (9U)
 *   DCM_DISABLE_RX_ENABLE_TX_NORM_NM (10U)
 *   DCM_DISABLE_RX_TX_NORM_NM (11U)
 * Dcm_ConfirmationStatusType: Enumeration of integer in interval [0...3] with enumerators
 *   DCM_RES_POS_OK (0U)
 *   DCM_RES_POS_NOT_OK (1U)
 *   DCM_RES_NEG_OK (2U)
 *   DCM_RES_NEG_NOT_OK (3U)
 * Dcm_DiagnosticSessionControlType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_ENUM_DEFAULT_SESSION (1U)
 *   DCM_ENUM_PROGRAMMING_SESSION (2U)
 *   DCM_ENUM_EXTENDED_SESSION (3U)
 *   DCM_ENUM_distantVehicleAccessSession_DVAS (80U)
 * Dcm_EcuResetType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_ENUM_NONE (0U)
 *   DCM_ENUM_HARD (1U)
 *   DCM_ENUM_KEYONOFF (2U)
 *   DCM_ENUM_SOFT (3U)
 *   DCM_ENUM_JUMPTOBOOTLOADER (4U)
 *   DCM_ENUM_JUMPTOSYSSUPPLIERBOOTLOADER (5U)
 *   DCM_ENUM_EXECUTE (6U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * Dcm_ProtocolType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_OBD_ON_CAN (0U)
 *   DCM_OBD_ON_FLEXRAY (1U)
 *   DCM_OBD_ON_IP (2U)
 *   DCM_UDS_ON_CAN (3U)
 *   DCM_UDS_ON_FLEXRAY (4U)
 *   DCM_UDS_ON_IP (5U)
 *   DCM_NO_ACTIVE_PROTOCOL (12U)
 *   DCM_SUPPLIER_1 (240U)
 *   DCM_SUPPLIER_2 (241U)
 *   DCM_SUPPLIER_3 (242U)
 *   DCM_SUPPLIER_4 (243U)
 *   DCM_SUPPLIER_5 (244U)
 *   DCM_SUPPLIER_6 (245U)
 *   DCM_SUPPLIER_7 (246U)
 *   DCM_SUPPLIER_8 (247U)
 *   DCM_SUPPLIER_9 (248U)
 *   DCM_SUPPLIER_10 (249U)
 *   DCM_SUPPLIER_11 (250U)
 *   DCM_SUPPLIER_12 (251U)
 *   DCM_SUPPLIER_13 (252U)
 *   DCM_SUPPLIER_14 (253U)
 *   DCM_SUPPLIER_15 (254U)
 * Dcm_RequestKindType: Enumeration of integer in interval [0...2] with enumerators
 *   DCM_REQ_KIND_NONE (0U)
 *   DCM_REQ_KIND_EXTERNAL (1U)
 *   DCM_REQ_KIND_ROE (2U)
 * Dcm_SecLevelType: Enumeration of integer in interval [0...1] with enumerators
 *   DCM_SEC_LEV_LOCKED (0U)
 *   DCM_SEC_LEV_L1 (1U)
 * Dcm_SesCtrlType: Enumeration of integer in interval [0...80] with enumerators
 *   DCM_DEFAULT_SESSION (1U)
 *   DCM_PROGRAMMING_SESSION (2U)
 *   DCM_EXTENDED_DIAGNOSTIC_SESSION (3U)
 *   DCM_distantVehicleAccessSession_DVAS (80U)
 *
 * Array Types:
 * ============
 * Dcm_Data10ByteType: Array with 10 element(s) of type uint8
 * Dcm_Data16ByteType: Array with 16 element(s) of type uint8
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data20ByteType: Array with 20 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * Dcm_Data3ByteType: Array with 3 element(s) of type uint8
 * Dcm_Data4095ByteType: Array with 4095 element(s) of type uint8
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 * Dcm_Data5ByteType: Array with 5 element(s) of type uint8
 * Dcm_Data6ByteType: Array with 6 element(s) of type uint8
 * Dcm_Data76ByteType: Array with 76 element(s) of type uint8
 *
 *********************************************************************************************************************/


#define Dcm_START_SEC_CODE
#include "Dcm_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Dcm_MainFunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Mode Interfaces:
 * ================
 *   Std_ReturnType Rte_Switch_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(Dcm_DiagnosticSessionControlType mode)
 *   Modes of Rte_ModeType_DcmDiagnosticSessionControl:
 *   - RTE_MODE_DcmDiagnosticSessionControl_DEFAULT_SESSION
 *   - RTE_MODE_DcmDiagnosticSessionControl_EXTENDED_SESSION
 *   - RTE_MODE_DcmDiagnosticSessionControl_PROGRAMMING_SESSION
 *   - RTE_MODE_DcmDiagnosticSessionControl_distantVehicleAccessSession_DVAS
 *   - RTE_TRANSITION_DcmDiagnosticSessionControl
 *   Std_ReturnType Rte_Switch_DcmEcuReset_DcmEcuReset(Dcm_EcuResetType mode)
 *   Modes of Rte_ModeType_DcmEcuReset:
 *   - RTE_MODE_DcmEcuReset_EXECUTE
 *   - RTE_MODE_DcmEcuReset_HARD
 *   - RTE_MODE_DcmEcuReset_JUMPTOBOOTLOADER
 *   - RTE_MODE_DcmEcuReset_JUMPTOSYSSUPPLIERBOOTLOADER
 *   - RTE_MODE_DcmEcuReset_KEYONOFF
 *   - RTE_MODE_DcmEcuReset_NONE
 *   - RTE_MODE_DcmEcuReset_SOFT
 *   - RTE_TRANSITION_DcmEcuReset
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_DCM_E_PENDING, RTE_E_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_DCM_E_PENDING, RTE_E_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data5ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data5ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Reserved_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Reserved_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Reserved_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Reserved_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_DCM_E_PENDING, RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_DCM_E_PENDING, RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_DCM_E_PENDING, RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_DCM_E_PENDING, RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data10ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_DCM_E_PENDING, RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_DCM_E_PENDING, RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_DCM_E_PENDING, RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_DCM_E_PENDING, RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_DCM_E_PENDING, RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_DCM_E_PENDING, RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_DCM_E_PENDING, RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_DCM_E_PENDING, RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_DCM_E_PENDING, RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_DCM_E_PENDING, RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_DCM_E_PENDING, RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_DCM_E_PENDING, RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_DCM_E_PENDING, RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_DCM_E_PENDING, RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_DCM_E_PENDING, RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_DCM_E_PENDING, RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data20ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_DCM_E_PENDING, RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadDataLength(Dcm_OpStatusType OpStatus, uint16 *DataLength)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_DCM_E_PENDING, RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_DCM_E_PENDING, RTE_E_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data5ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_DCM_E_PENDING, RTE_E_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_DCM_E_PENDING, RTE_E_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_DCM_E_PENDING, RTE_E_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_DCM_E_PENDING, RTE_E_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data16ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_DCM_E_PENDING, RTE_E_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_DCM_E_PENDING, RTE_E_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_DCM_E_PENDING, RTE_E_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_DCM_E_PENDING, RTE_E_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_DCM_E_PENDING, RTE_E_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_DCM_E_PENDING, RTE_E_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_DCM_E_PENDING, RTE_E_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_DCM_E_PENDING, RTE_E_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_DCM_E_PENDING, RTE_E_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data3ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data3ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_DCM_E_PENDING, RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_DCM_E_PENDING, RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_DCM_E_PENDING, RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData(const uint8 *Data, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_DCM_E_PENDING, RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_LFM_Faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_LFM_Faults_DataRecord_DCM_E_PENDING, RTE_E_DataServices_LFM_Faults_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_LFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_LFM_Faults_DataRecord_DCM_E_PENDING, RTE_E_DataServices_LFM_Faults_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_LFM_HW_faults_DataRecord_DCM_E_PENDING, RTE_E_DataServices_LFM_HW_faults_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_LFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_LFM_HW_faults_DataRecord_DCM_E_PENDING, RTE_E_DataServices_LFM_HW_faults_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Mahle_EOL_information_EOLData_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Mahle_EOL_information_EOLData_DCM_E_PENDING, RTE_E_DataServices_Mahle_EOL_information_EOLData_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Mahle_EOL_information_EOLData_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data76ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Mahle_EOL_information_EOLData_DCM_E_PENDING, RTE_E_DataServices_Mahle_EOL_information_EOLData_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Mahle_EOL_information_EOLData_WriteData(const uint8 *Data, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data76ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Mahle_EOL_information_EOLData_DCM_E_PENDING, RTE_E_DataServices_Mahle_EOL_information_EOLData_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_New_Identification_Parameters_1_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_New_Identification_Parameters_1_DataRecord_DCM_E_PENDING, RTE_E_DataServices_New_Identification_Parameters_1_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_New_Identification_Parameters_1_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_New_Identification_Parameters_1_DataRecord_DCM_E_PENDING, RTE_E_DataServices_New_Identification_Parameters_1_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_New_Identification_Parameters_2_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_New_Identification_Parameters_2_DataRecord_DCM_E_PENDING, RTE_E_DataServices_New_Identification_Parameters_2_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_New_Identification_Parameters_2_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_New_Identification_Parameters_2_DataRecord_DCM_E_PENDING, RTE_E_DataServices_New_Identification_Parameters_2_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_New_Identification_Parameters_3_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_New_Identification_Parameters_3_DataRecord_DCM_E_PENDING, RTE_E_DataServices_New_Identification_Parameters_3_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_New_Identification_Parameters_3_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_New_Identification_Parameters_3_DataRecord_DCM_E_PENDING, RTE_E_DataServices_New_Identification_Parameters_3_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_New_Identification_Parameters_4_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_New_Identification_Parameters_4_DataRecord_DCM_E_PENDING, RTE_E_DataServices_New_Identification_Parameters_4_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_New_Identification_Parameters_4_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_New_Identification_Parameters_4_DataRecord_DCM_E_PENDING, RTE_E_DataServices_New_Identification_Parameters_4_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_New_Identification_Parameters_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_New_Identification_Parameters_DataRecord_DCM_E_PENDING, RTE_E_DataServices_New_Identification_Parameters_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_New_Identification_Parameters_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_New_Identification_Parameters_DataRecord_DCM_E_PENDING, RTE_E_DataServices_New_Identification_Parameters_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_OFM_Faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_OFM_Faults_DataRecord_DCM_E_PENDING, RTE_E_DataServices_OFM_Faults_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_OFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_OFM_Faults_DataRecord_DCM_E_PENDING, RTE_E_DataServices_OFM_Faults_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_OFM_HW_faults_DataRecord_DCM_E_PENDING, RTE_E_DataServices_OFM_HW_faults_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_OFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_OFM_HW_faults_DataRecord_DCM_E_PENDING, RTE_E_DataServices_OFM_HW_faults_DataRecord_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_DCM_E_PENDING, RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_PCOM_debug_signals_PCOMDebug_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_DCM_E_PENDING, RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_PLC_MAC_address_MAC_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_PLC_MAC_address_MAC_DCM_E_PENDING, RTE_E_DataServices_PLC_MAC_address_MAC_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_PLC_MAC_address_MAC_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data6ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_PLC_MAC_address_MAC_DCM_E_PENDING, RTE_E_DataServices_PLC_MAC_address_MAC_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_PLC_MAC_address_MAC_WriteData(const uint8 *Data, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data6ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_PLC_MAC_address_MAC_DCM_E_PENDING, RTE_E_DataServices_PLC_MAC_address_MAC_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_DCM_E_PENDING, RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_DCM_E_PENDING, RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_DCM_E_PENDING, RTE_E_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_DCM_E_PENDING, RTE_E_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_DCM_E_PENDING, RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_DCM_E_PENDING, RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_DCM_E_PENDING, RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_DCM_E_PENDING, RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_SW_version_Major_version_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_SW_version_Major_version_DCM_E_PENDING, RTE_E_DataServices_SW_version_Major_version_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_SW_version_Major_version_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_SW_version_Major_version_DCM_E_PENDING, RTE_E_DataServices_SW_version_Major_version_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_SW_version_Minor_version_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_SW_version_Minor_version_DCM_E_PENDING, RTE_E_DataServices_SW_version_Minor_version_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_SW_version_Minor_version_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_SW_version_Minor_version_DCM_E_PENDING, RTE_E_DataServices_SW_version_Minor_version_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_DCM_E_PENDING, RTE_E_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_DCM_E_PENDING, RTE_E_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_DCM_E_PENDING, RTE_E_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_DCM_E_PENDING, RTE_E_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_DCM_E_PENDING, RTE_E_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_DCM_E_PENDING, RTE_E_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_DCM_E_PENDING, RTE_E_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_DCM_E_PENDING, RTE_E_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_DCM_E_PENDING, RTE_E_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_DCM_E_PENDING, RTE_E_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_DCM_E_PENDING, RTE_E_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_DCM_E_PENDING, RTE_E_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_DCM_E_PENDING, RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_DCM_E_PENDING, RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_DCM_E_PENDING, RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_DCM_E_PENDING, RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_DCM_E_PENDING, RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_DCM_E_PENDING, RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_DCM_E_PENDING, RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_DCM_E_PENDING, RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_V2G_States_Debug_CHGDebugData_DCM_E_PENDING, RTE_E_DataServices_V2G_States_Debug_CHGDebugData_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_V2G_States_Debug_CHGDebugData_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_V2G_States_Debug_CHGDebugData_DCM_E_PENDING, RTE_E_DataServices_V2G_States_Debug_CHGDebugData_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_V2G_States_Debug_CHGInternalState_DCM_E_PENDING, RTE_E_DataServices_V2G_States_Debug_CHGInternalState_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_V2G_States_Debug_CHGInternalState_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_V2G_States_Debug_CHGInternalState_DCM_E_PENDING, RTE_E_DataServices_V2G_States_Debug_CHGInternalState_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_DCM_E_PENDING, RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_V2G_States_Debug_SCCStateMachine_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_DCM_E_PENDING, RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_DCM_E_PENDING, RTE_E_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_DCM_E_PENDING, RTE_E_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_PENDING, RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_02, uint8 *Out_MODE_TEST, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_PENDING, RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start(uint8 In_CHOIX_MODE_TEST, Dcm_OpStatusType OpStatus, uint8 *Out_RSR_02, uint8 *Out_MODE_TEST, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_PENDING, RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_RoutineStatusRecord_01_02, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_PENDING, RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_Powerlatch_Information_Positioning_PIP_Start(Dcm_OpStatusType OpStatus, uint8 *Out_RoutineStatusRecord_01_02, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_PENDING, RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_check_Memory_CM_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_check_Memory_CM_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_check_Memory_CM_DCM_E_PENDING, RTE_E_RoutineServices_check_Memory_CM_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_check_Memory_CM_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data2ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_check_Memory_CM_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_check_Memory_CM_DCM_E_PENDING, RTE_E_RoutineServices_check_Memory_CM_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_erase_Memory_EM_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_routineInfo_FF00, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_erase_Memory_EM_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_erase_Memory_EM_DCM_E_PENDING, RTE_E_RoutineServices_erase_Memory_EM_E_NOT_OK
 *   Std_ReturnType Rte_Call_RoutineServices_erase_Memory_EM_Start(uint8 In_routineControlOptionRecord_FF00, uint16 In_RCEOR_SGN_OTL, Dcm_OpStatusType OpStatus, uint8 *Out_routineInfo_FF00, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RoutineServices_erase_Memory_EM_DCM_E_FORCE_RCRRP, RTE_E_RoutineServices_erase_Memory_EM_DCM_E_PENDING, RTE_E_RoutineServices_erase_Memory_EM_E_NOT_OK
 *   Std_ReturnType Rte_Call_SecurityAccess_UnlockedL1_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_SecurityAccess_UnlockedL1_DCM_E_COMPARE_KEY_FAILED, RTE_E_SecurityAccess_UnlockedL1_DCM_E_PENDING, RTE_E_SecurityAccess_UnlockedL1_E_NOT_OK
 *   Std_ReturnType Rte_Call_SecurityAccess_UnlockedL1_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: uint8* is of type Dcm_Data4ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_SecurityAccess_UnlockedL1_DCM_E_PENDING, RTE_E_SecurityAccess_UnlockedL1_E_NOT_OK
 *   Std_ReturnType Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Confirmation(uint8 SID, uint8 ReqType, uint16 SourceAddress, Dcm_ConfirmationStatusType ConfirmationStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ServiceRequestNotification_E_NOT_OK
 *   Std_ReturnType Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication(uint8 SID, const uint8 *RequestData, uint16 DataSize, uint8 ReqType, uint16 SourceAddress, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument RequestData: uint8* is of type Dcm_Data4095ByteType
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ServiceRequestNotification_E_NOT_OK, RTE_E_ServiceRequestNotification_E_REQUEST_NOT_ACCEPTED
 *
 * Status Interfaces:
 * ==================
 *   Mode Switch Acknowledge:
 *   -------------------------
 *   Std_ReturnType Rte_SwitchAck_DcmEcuReset_DcmEcuReset(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Dcm_MainFunction_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Dcm_CODE) Dcm_MainFunction(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dcm_MainFunction
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  Dcm_NegativeResponseCodeType Call_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data5ByteType Call_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ReadData_Data = {
  0U, 0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data5ByteType Call_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_ReadData_Data = {
  0U, 0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Authentification_Zone_ZA_Reserved_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_Authentification_Zone_ZA_Reserved_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data10ByteType Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ReadData_Data = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data20ByteType Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadData_Data = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  uint16 Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadDataLength_DataLength = 0U;
  Dcm_NegativeResponseCodeType Call_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data5ByteType Call_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ReadData_Data = {
  0U, 0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data16ByteType Call_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ReadData_Data = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data3ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ReadData_Data = {
  0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data3ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ReadData_Data = {
  0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData_Data = {
  0U
};
  Dcm_Data1ByteType Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData_ErrorCode = 0U;
  Dcm_NegativeResponseCodeType Call_DataServices_LFM_Faults_DataRecord_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_LFM_Faults_DataRecord_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_LFM_HW_faults_DataRecord_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Mahle_EOL_information_EOLData_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data76ByteType Call_DataServices_Mahle_EOL_information_EOLData_ReadData_Data = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  Dcm_Data76ByteType Call_DataServices_Mahle_EOL_information_EOLData_WriteData_Data = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Mahle_EOL_information_EOLData_WriteData_ErrorCode = 0U;
  Dcm_NegativeResponseCodeType Call_DataServices_New_Identification_Parameters_1_DataRecord_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_New_Identification_Parameters_1_DataRecord_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_New_Identification_Parameters_2_DataRecord_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_New_Identification_Parameters_2_DataRecord_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_New_Identification_Parameters_3_DataRecord_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_New_Identification_Parameters_3_DataRecord_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_New_Identification_Parameters_4_DataRecord_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_New_Identification_Parameters_4_DataRecord_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_New_Identification_Parameters_DataRecord_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_New_Identification_Parameters_DataRecord_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_OFM_Faults_DataRecord_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_OFM_Faults_DataRecord_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_OFM_HW_faults_DataRecord_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_PCOM_debug_signals_PCOMDebug_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_PLC_MAC_address_MAC_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data6ByteType Call_DataServices_PLC_MAC_address_MAC_ReadData_Data = {
  0U, 0U, 0U, 0U, 0U, 0U
};
  Dcm_Data6ByteType Call_DataServices_PLC_MAC_address_MAC_WriteData_Data = {
  0U, 0U, 0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_PLC_MAC_address_MAC_WriteData_ErrorCode = 0U;
  Dcm_NegativeResponseCodeType Call_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data4ByteType Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ReadData_Data = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_SW_version_Major_version_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_SW_version_Major_version_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_SW_version_Minor_version_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_SW_version_Minor_version_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_V2G_States_Debug_CHGDebugData_ReadData_Data = {
  0U, 0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_V2G_States_Debug_CHGInternalState_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data1ByteType Call_DataServices_V2G_States_Debug_SCCStateMachine_ReadData_Data = {
  0U
};
  Dcm_NegativeResponseCodeType Call_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ConditionCheckRead_ErrorCode = 0U;
  Dcm_Data2ByteType Call_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ReadData_Data = {
  0U, 0U
};
  Dcm_Data3ByteType Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults_Out_ComplexStructure_0 = {
  0U, 0U, 0U
};
  uint16 Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults_DataLength = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults_ErrorCode = 0U;
  Dcm_Data3ByteType Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start_Out_ComplexStructure_0 = {
  0U, 0U, 0U
};
  uint16 Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start_DataLength = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start_ErrorCode = 0U;
  uint8 Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop_Out_RSR_04 = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop_ErrorCode = 0U;
  Dcm_Data3ByteType Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults_Out_ComplexStructure_0 = {
  0U, 0U, 0U
};
  uint16 Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults_DataLength = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults_ErrorCode = 0U;
  Dcm_Data3ByteType Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start_Out_ComplexStructure_0 = {
  0U, 0U, 0U
};
  uint16 Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start_DataLength = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start_ErrorCode = 0U;
  uint8 Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop_Out_RSR_04 = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop_ErrorCode = 0U;
  Dcm_Data3ByteType Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults_Out_ComplexStructure_0 = {
  0U, 0U, 0U
};
  uint16 Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults_DataLength = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults_ErrorCode = 0U;
  Dcm_Data3ByteType Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start_Out_ComplexStructure_0 = {
  0U, 0U, 0U
};
  uint16 Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start_DataLength = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start_ErrorCode = 0U;
  uint8 Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop_Out_RSR_04 = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop_ErrorCode = 0U;
  Dcm_Data3ByteType Call_RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults_Out_ComplexStructure_0 = {
  0U, 0U, 0U
};
  uint16 Call_RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults_DataLength = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults_ErrorCode = 0U;
  Dcm_Data3ByteType Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Start_Out_ComplexStructure_0 = {
  0U, 0U, 0U
};
  uint16 Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Start_DataLength = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Start_ErrorCode = 0U;
  uint8 Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Stop_Out_RSR_04 = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Stop_ErrorCode = 0U;
  Dcm_Data3ByteType Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults_Out_ComplexStructure_0 = {
  0U, 0U, 0U
};
  uint16 Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults_DataLength = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults_ErrorCode = 0U;
  Dcm_Data3ByteType Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start_Out_ComplexStructure_0 = {
  0U, 0U, 0U
};
  uint16 Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start_DataLength = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start_ErrorCode = 0U;
  uint8 Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop_Out_RSR_04 = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop_ErrorCode = 0U;
  uint8 Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults_Out_RSR_02 = 0U;
  uint8 Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults_Out_MODE_TEST = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults_ErrorCode = 0U;
  uint8 Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start_Out_RSR_02 = 0U;
  uint8 Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start_Out_MODE_TEST = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start_ErrorCode = 0U;
  uint8 Call_RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults_Out_RoutineStatusRecord_01_02 = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults_ErrorCode = 0U;
  uint8 Call_RoutineServices_Powerlatch_Information_Positioning_PIP_Start_Out_RoutineStatusRecord_01_02 = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_Powerlatch_Information_Positioning_PIP_Start_ErrorCode = 0U;
  Dcm_Data2ByteType Call_RoutineServices_check_Memory_CM_RequestResults_Out_ComplexStructure_0 = {
  0U, 0U
};
  uint16 Call_RoutineServices_check_Memory_CM_RequestResults_DataLength = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_check_Memory_CM_RequestResults_ErrorCode = 0U;
  Dcm_Data2ByteType Call_RoutineServices_check_Memory_CM_Start_Out_ComplexStructure_0 = {
  0U, 0U
};
  uint16 Call_RoutineServices_check_Memory_CM_Start_DataLength = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_check_Memory_CM_Start_ErrorCode = 0U;
  uint8 Call_RoutineServices_erase_Memory_EM_RequestResults_Out_routineInfo_FF00 = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_erase_Memory_EM_RequestResults_ErrorCode = 0U;
  uint8 Call_RoutineServices_erase_Memory_EM_Start_Out_routineInfo_FF00 = 0U;
  Dcm_NegativeResponseCodeType Call_RoutineServices_erase_Memory_EM_Start_ErrorCode = 0U;
  Dcm_Data4ByteType Call_SecurityAccess_UnlockedL1_CompareKey_Key = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_SecurityAccess_UnlockedL1_CompareKey_ErrorCode = 0U;
  Dcm_Data4ByteType Call_SecurityAccess_UnlockedL1_GetSeed_Seed = {
  0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_SecurityAccess_UnlockedL1_GetSeed_ErrorCode = 0U;
  Dcm_Data4095ByteType Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication_RequestData = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U
};
  Dcm_NegativeResponseCodeType Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication_ErrorCode = 0U;

  /**********************************************************
  * Direct Function Accesses and Take Addresses of Functions
  **********************************************************/

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)Rte_Switch_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Switch_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(RTE_MODE_DcmDiagnosticSessionControl_DEFAULT_SESSION); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_LIMIT:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)Rte_Switch_DcmEcuReset_DcmEcuReset; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Switch_DcmEcuReset_DcmEcuReset(RTE_MODE_DcmEcuReset_NONE); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_LIMIT:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ConditionCheckRead(0U, &Call_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ReadData(0U, Call_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ConditionCheckRead(0U, &Call_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ReadData(0U, Call_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ConditionCheckRead(0U, &Call_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ReadData(0U, Call_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ConditionCheckRead(0U, &Call_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ReadData(0U, Call_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ConditionCheckRead(0U, &Call_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ReadData(0U, Call_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_ConditionCheckRead(0U, &Call_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_ReadData(0U, Call_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_ConditionCheckRead(0U, &Call_DataServices_Authentification_Zone_ZA_Reserved_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Reserved_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Reserved_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_ReadData(0U, Call_DataServices_Authentification_Zone_ZA_Reserved_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Reserved_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Reserved_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ConditionCheckRead(0U, &Call_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ReadData(0U, Call_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ConditionCheckRead(0U, &Call_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ReadData(0U, Call_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ConditionCheckRead(0U, &Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ReadData(0U, Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ConditionCheckRead(0U, &Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ReadData(0U, Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ConditionCheckRead(0U, &Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ReadData(0U, Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ConditionCheckRead(0U, &Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ReadData(0U, Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ConditionCheckRead(0U, &Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ReadData(0U, Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ConditionCheckRead(0U, &Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ReadData(0U, Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ConditionCheckRead(0U, &Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ReadData(0U, Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ConditionCheckRead(0U, &Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ReadData(0U, Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead(0U, &Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ReadData(0U, Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead(0U, &Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData(0U, Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead(0U, &Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData(0U, Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ConditionCheckRead(0U, &Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadData(0U, Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadDataLength; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadDataLength(0U, &Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadDataLength_DataLength); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ConditionCheckRead(0U, &Call_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ReadData(0U, Call_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ConditionCheckRead(0U, &Call_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ReadData(0U, Call_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ConditionCheckRead(0U, &Call_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ReadData(0U, Call_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ConditionCheckRead(0U, &Call_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ReadData(0U, Call_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_ConditionCheckRead(0U, &Call_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_ReadData(0U, Call_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_ConditionCheckRead(0U, &Call_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_ReadData(0U, Call_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_ConditionCheckRead(0U, &Call_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_ReadData(0U, Call_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ConditionCheckRead(0U, &Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ReadData(0U, Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead(0U, &Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData(0U, Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData(Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData_Data, 0U, &Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_LFM_Faults_DataRecord_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_LFM_Faults_DataRecord_ConditionCheckRead(0U, &Call_DataServices_LFM_Faults_DataRecord_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_LFM_Faults_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_LFM_Faults_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_LFM_Faults_DataRecord_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_LFM_Faults_DataRecord_ReadData(0U, Call_DataServices_LFM_Faults_DataRecord_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_LFM_Faults_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_LFM_Faults_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead(0U, &Call_DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_LFM_HW_faults_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_LFM_HW_faults_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_LFM_HW_faults_DataRecord_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_LFM_HW_faults_DataRecord_ReadData(0U, Call_DataServices_LFM_HW_faults_DataRecord_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_LFM_HW_faults_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_LFM_HW_faults_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Mahle_EOL_information_EOLData_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Mahle_EOL_information_EOLData_ConditionCheckRead(0U, &Call_DataServices_Mahle_EOL_information_EOLData_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Mahle_EOL_information_EOLData_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Mahle_EOL_information_EOLData_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Mahle_EOL_information_EOLData_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Mahle_EOL_information_EOLData_ReadData(0U, Call_DataServices_Mahle_EOL_information_EOLData_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Mahle_EOL_information_EOLData_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Mahle_EOL_information_EOLData_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Mahle_EOL_information_EOLData_WriteData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Mahle_EOL_information_EOLData_WriteData(Call_DataServices_Mahle_EOL_information_EOLData_WriteData_Data, 0U, &Call_DataServices_Mahle_EOL_information_EOLData_WriteData_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Mahle_EOL_information_EOLData_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Mahle_EOL_information_EOLData_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_1_DataRecord_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_1_DataRecord_ConditionCheckRead(0U, &Call_DataServices_New_Identification_Parameters_1_DataRecord_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_1_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_1_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_1_DataRecord_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_1_DataRecord_ReadData(0U, Call_DataServices_New_Identification_Parameters_1_DataRecord_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_1_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_1_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_2_DataRecord_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_2_DataRecord_ConditionCheckRead(0U, &Call_DataServices_New_Identification_Parameters_2_DataRecord_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_2_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_2_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_2_DataRecord_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_2_DataRecord_ReadData(0U, Call_DataServices_New_Identification_Parameters_2_DataRecord_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_2_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_2_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_3_DataRecord_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_3_DataRecord_ConditionCheckRead(0U, &Call_DataServices_New_Identification_Parameters_3_DataRecord_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_3_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_3_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_3_DataRecord_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_3_DataRecord_ReadData(0U, Call_DataServices_New_Identification_Parameters_3_DataRecord_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_3_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_3_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_4_DataRecord_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_4_DataRecord_ConditionCheckRead(0U, &Call_DataServices_New_Identification_Parameters_4_DataRecord_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_4_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_4_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_4_DataRecord_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_4_DataRecord_ReadData(0U, Call_DataServices_New_Identification_Parameters_4_DataRecord_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_4_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_4_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_DataRecord_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_DataRecord_ConditionCheckRead(0U, &Call_DataServices_New_Identification_Parameters_DataRecord_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_DataRecord_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_New_Identification_Parameters_DataRecord_ReadData(0U, Call_DataServices_New_Identification_Parameters_DataRecord_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_New_Identification_Parameters_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_OFM_Faults_DataRecord_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_OFM_Faults_DataRecord_ConditionCheckRead(0U, &Call_DataServices_OFM_Faults_DataRecord_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_OFM_Faults_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_OFM_Faults_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_OFM_Faults_DataRecord_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_OFM_Faults_DataRecord_ReadData(0U, Call_DataServices_OFM_Faults_DataRecord_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_OFM_Faults_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_OFM_Faults_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead(0U, &Call_DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_OFM_HW_faults_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_OFM_HW_faults_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_OFM_HW_faults_DataRecord_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_OFM_HW_faults_DataRecord_ReadData(0U, Call_DataServices_OFM_HW_faults_DataRecord_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_OFM_HW_faults_DataRecord_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_OFM_HW_faults_DataRecord_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead(0U, &Call_DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_PCOM_debug_signals_PCOMDebug_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_PCOM_debug_signals_PCOMDebug_ReadData(0U, Call_DataServices_PCOM_debug_signals_PCOMDebug_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_PLC_MAC_address_MAC_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_PLC_MAC_address_MAC_ConditionCheckRead(0U, &Call_DataServices_PLC_MAC_address_MAC_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_PLC_MAC_address_MAC_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_PLC_MAC_address_MAC_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_PLC_MAC_address_MAC_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_PLC_MAC_address_MAC_ReadData(0U, Call_DataServices_PLC_MAC_address_MAC_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_PLC_MAC_address_MAC_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_PLC_MAC_address_MAC_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_PLC_MAC_address_MAC_WriteData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_PLC_MAC_address_MAC_WriteData(Call_DataServices_PLC_MAC_address_MAC_WriteData_Data, 0U, &Call_DataServices_PLC_MAC_address_MAC_WriteData_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_PLC_MAC_address_MAC_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_PLC_MAC_address_MAC_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead(0U, &Call_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData(0U, Call_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ConditionCheckRead(0U, &Call_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ReadData(0U, Call_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ReadData(0U, Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ReadData(0U, Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ReadData(0U, Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ReadData(0U, Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ReadData(0U, Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ReadData(0U, Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ReadData(0U, Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ReadData(0U, Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ReadData(0U, Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ReadData(0U, Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ReadData(0U, Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ReadData(0U, Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ReadData(0U, Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ReadData(0U, Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ReadData(0U, Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ReadData(0U, Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ReadData(0U, Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ReadData(0U, Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ReadData(0U, Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ReadData(0U, Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ReadData(0U, Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ReadData(0U, Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ReadData(0U, Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ReadData(0U, Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ReadData(0U, Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ReadData(0U, Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ReadData(0U, Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ReadData(0U, Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ReadData(0U, Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ReadData(0U, Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ReadData(0U, Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ReadData(0U, Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ReadData(0U, Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ReadData(0U, Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ReadData(0U, Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ReadData(0U, Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ReadData(0U, Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ReadData(0U, Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ReadData(0U, Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ReadData(0U, Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ReadData(0U, Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ReadData(0U, Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ReadData(0U, Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ReadData(0U, Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ReadData(0U, Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ReadData(0U, Call_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ReadData(0U, Call_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ConditionCheckRead(0U, &Call_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ReadData(0U, Call_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ConditionCheckRead(0U, &Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ReadData(0U, Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ConditionCheckRead(0U, &Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ReadData(0U, Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_SW_version_Major_version_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_SW_version_Major_version_ConditionCheckRead(0U, &Call_DataServices_SW_version_Major_version_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_SW_version_Major_version_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_SW_version_Major_version_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_SW_version_Major_version_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_SW_version_Major_version_ReadData(0U, Call_DataServices_SW_version_Major_version_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_SW_version_Major_version_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_SW_version_Major_version_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_SW_version_Minor_version_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_SW_version_Minor_version_ConditionCheckRead(0U, &Call_DataServices_SW_version_Minor_version_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_SW_version_Minor_version_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_SW_version_Minor_version_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_SW_version_Minor_version_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_SW_version_Minor_version_ReadData(0U, Call_DataServices_SW_version_Minor_version_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_SW_version_Minor_version_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_SW_version_Minor_version_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ConditionCheckRead(0U, &Call_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ReadData(0U, Call_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ConditionCheckRead(0U, &Call_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ReadData(0U, Call_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ConditionCheckRead(0U, &Call_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ReadData(0U, Call_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_ConditionCheckRead(0U, &Call_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_ReadData(0U, Call_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ConditionCheckRead(0U, &Call_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ReadData(0U, Call_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ConditionCheckRead(0U, &Call_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ReadData(0U, Call_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ConditionCheckRead(0U, &Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ReadData(0U, Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ConditionCheckRead(0U, &Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ReadData(0U, Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_ConditionCheckRead(0U, &Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_ReadData(0U, Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_ConditionCheckRead(0U, &Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_ReadData(0U, Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead(0U, &Call_DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_V2G_States_Debug_CHGDebugData_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_V2G_States_Debug_CHGDebugData_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_V2G_States_Debug_CHGDebugData_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_V2G_States_Debug_CHGDebugData_ReadData(0U, Call_DataServices_V2G_States_Debug_CHGDebugData_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_V2G_States_Debug_CHGDebugData_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_V2G_States_Debug_CHGDebugData_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead(0U, &Call_DataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_V2G_States_Debug_CHGInternalState_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_V2G_States_Debug_CHGInternalState_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_V2G_States_Debug_CHGInternalState_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_V2G_States_Debug_CHGInternalState_ReadData(0U, Call_DataServices_V2G_States_Debug_CHGInternalState_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_V2G_States_Debug_CHGInternalState_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_V2G_States_Debug_CHGInternalState_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead(0U, &Call_DataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_V2G_States_Debug_SCCStateMachine_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_V2G_States_Debug_SCCStateMachine_ReadData(0U, Call_DataServices_V2G_States_Debug_SCCStateMachine_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ConditionCheckRead; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ConditionCheckRead(0U, &Call_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ConditionCheckRead_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ReadData; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ReadData(0U, Call_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ReadData_Data); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults(0U, Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults_Out_ComplexStructure_0, &Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults_DataLength, &Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start(0U, Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start_Out_ComplexStructure_0, &Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start_DataLength, &Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop(0U, &Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop_Out_RSR_04, &Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults(0U, Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults_Out_ComplexStructure_0, &Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults_DataLength, &Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start(0U, Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start_Out_ComplexStructure_0, &Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start_DataLength, &Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop(0U, &Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop_Out_RSR_04, &Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults(0U, Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults_Out_ComplexStructure_0, &Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults_DataLength, &Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start(0U, Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start_Out_ComplexStructure_0, &Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start_DataLength, &Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop(0U, &Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop_Out_RSR_04, &Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults(0U, Call_RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults_Out_ComplexStructure_0, &Call_RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults_DataLength, &Call_RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Start; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Start(0U, Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Start_Out_ComplexStructure_0, &Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Start_DataLength, &Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Start_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Stop; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Stop(0U, &Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Stop_Out_RSR_04, &Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Stop_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults(0U, Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults_Out_ComplexStructure_0, &Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults_DataLength, &Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start(0U, Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start_Out_ComplexStructure_0, &Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start_DataLength, &Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop(0U, &Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop_Out_RSR_04, &Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults(0U, &Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults_Out_RSR_02, &Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults_Out_MODE_TEST, &Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start(0U, 0U, &Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start_Out_RSR_02, &Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start_Out_MODE_TEST, &Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults(0U, &Call_RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults_Out_RoutineStatusRecord_01_02, &Call_RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_Powerlatch_Information_Positioning_PIP_Start; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_Powerlatch_Information_Positioning_PIP_Start(0U, &Call_RoutineServices_Powerlatch_Information_Positioning_PIP_Start_Out_RoutineStatusRecord_01_02, &Call_RoutineServices_Powerlatch_Information_Positioning_PIP_Start_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_check_Memory_CM_RequestResults; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_check_Memory_CM_RequestResults(0U, Call_RoutineServices_check_Memory_CM_RequestResults_Out_ComplexStructure_0, &Call_RoutineServices_check_Memory_CM_RequestResults_DataLength, &Call_RoutineServices_check_Memory_CM_RequestResults_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_check_Memory_CM_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_check_Memory_CM_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_check_Memory_CM_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_check_Memory_CM_Start; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_check_Memory_CM_Start(0U, Call_RoutineServices_check_Memory_CM_Start_Out_ComplexStructure_0, &Call_RoutineServices_check_Memory_CM_Start_DataLength, &Call_RoutineServices_check_Memory_CM_Start_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_check_Memory_CM_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_check_Memory_CM_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_check_Memory_CM_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_erase_Memory_EM_RequestResults; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_erase_Memory_EM_RequestResults(0U, &Call_RoutineServices_erase_Memory_EM_RequestResults_Out_routineInfo_FF00, &Call_RoutineServices_erase_Memory_EM_RequestResults_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_erase_Memory_EM_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_erase_Memory_EM_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_erase_Memory_EM_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_RoutineServices_erase_Memory_EM_Start; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_RoutineServices_erase_Memory_EM_Start(0U, 0U, 0U, &Call_RoutineServices_erase_Memory_EM_Start_Out_routineInfo_FF00, &Call_RoutineServices_erase_Memory_EM_Start_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_erase_Memory_EM_DCM_E_FORCE_RCRRP:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_erase_Memory_EM_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_RoutineServices_erase_Memory_EM_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_SecurityAccess_UnlockedL1_CompareKey; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_SecurityAccess_UnlockedL1_CompareKey(Call_SecurityAccess_UnlockedL1_CompareKey_Key, 0U, &Call_SecurityAccess_UnlockedL1_CompareKey_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_SecurityAccess_UnlockedL1_DCM_E_COMPARE_KEY_FAILED:
      fct_error = TRUE;
      break;
    case RTE_E_SecurityAccess_UnlockedL1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_SecurityAccess_UnlockedL1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_SecurityAccess_UnlockedL1_GetSeed; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_SecurityAccess_UnlockedL1_GetSeed(0U, Call_SecurityAccess_UnlockedL1_GetSeed_Seed, &Call_SecurityAccess_UnlockedL1_GetSeed_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_SecurityAccess_UnlockedL1_DCM_E_PENDING:
      fct_error = TRUE;
      break;
    case RTE_E_SecurityAccess_UnlockedL1_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Confirmation; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Confirmation(0U, 0U, 0U, 0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_ServiceRequestNotification_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)TSC_Dcm_Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication(0U, Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication_RequestData, 0U, 0U, 0U, &Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication_ErrorCode); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_ServiceRequestNotification_E_NOT_OK:
      fct_error = TRUE;
      break;
    case RTE_E_ServiceRequestNotification_E_REQUEST_NOT_ACCEPTED:
      fct_error = TRUE;
      break;
  }

  {
    FncPtrType Dcm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Dcm_FctPtr = (FncPtrType)Rte_SwitchAck_DcmEcuReset_DcmEcuReset; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Dcm_Rte_SwitchAck_DcmEcuReset_DcmEcuReset(); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_NO_DATA:
      fct_error = TRUE;
      break;
    case RTE_E_TRANSMIT_ACK:
      fct_error = TRUE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
  }

 /* PRQA S 3226, 1863 L1 */ /* MD_Rte_Os, MD_Rte_Os */
  TSC_Dcm_SchM_Enter_Dcm_DCM_EXCLUSIVE_AREA_0();
  TSC_Dcm_SchM_Exit_Dcm_DCM_EXCLUSIVE_AREA_0();
/* PRQA L:L1 */

  Dcm_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetActiveProtocol
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetActiveProtocol> of PortPrototype <DCMServices>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dcm_GetActiveProtocol(Dcm_ProtocolType *ActiveProtocol)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DCMServices_E_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetActiveProtocol_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Dcm_CODE) Dcm_GetActiveProtocol(P2VAR(Dcm_ProtocolType, AUTOMATIC, RTE_DCM_APPL_VAR) ActiveProtocol) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dcm_GetActiveProtocol (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetRequestKind
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetRequestKind> of PortPrototype <DCMServices>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dcm_GetRequestKind(uint16 TesterSourceAddress, Dcm_RequestKindType *RequestKind)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DCMServices_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetRequestKind_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Dcm_CODE) Dcm_GetRequestKind(uint16 TesterSourceAddress, P2VAR(Dcm_RequestKindType, AUTOMATIC, RTE_DCM_APPL_VAR) RequestKind) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dcm_GetRequestKind (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetSecurityLevel
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSecurityLevel> of PortPrototype <DCMServices>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dcm_GetSecurityLevel(Dcm_SecLevelType *SecLevel)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DCMServices_E_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetSecurityLevel_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Dcm_CODE) Dcm_GetSecurityLevel(P2VAR(Dcm_SecLevelType, AUTOMATIC, RTE_DCM_APPL_VAR) SecLevel) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dcm_GetSecurityLevel (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetSesCtrlType
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSesCtrlType> of PortPrototype <DCMServices>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dcm_GetSesCtrlType(Dcm_SesCtrlType *SesCtrlType)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DCMServices_E_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetSesCtrlType_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Dcm_CODE) Dcm_GetSesCtrlType(P2VAR(Dcm_SesCtrlType, AUTOMATIC, RTE_DCM_APPL_VAR) SesCtrlType) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dcm_GetSesCtrlType (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ResetToDefaultSession
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ResetToDefaultSession> of PortPrototype <DCMServices>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dcm_ResetToDefaultSession(void)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DCMServices_E_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ResetToDefaultSession_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Dcm_CODE) Dcm_ResetToDefaultSession(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dcm_ResetToDefaultSession (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SetActiveDiagnostic
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetActiveDiagnostic> of PortPrototype <DCMServices>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Dcm_SetActiveDiagnostic(boolean active)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DCMServices_E_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SetActiveDiagnostic_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Dcm_CODE) Dcm_SetActiveDiagnostic(boolean active) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Dcm_SetActiveDiagnostic (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define Dcm_STOP_SEC_CODE
#include "Dcm_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void Dcm_TestDefines(void)
{
  /* Enumeration Data Types */

  Dcm_CommunicationModeType Test_Dcm_CommunicationModeType_V_1 = DCM_ENABLE_RX_TX_NORM;
  Dcm_CommunicationModeType Test_Dcm_CommunicationModeType_V_2 = DCM_ENABLE_RX_DISABLE_TX_NORM;
  Dcm_CommunicationModeType Test_Dcm_CommunicationModeType_V_3 = DCM_DISABLE_RX_ENABLE_TX_NORM;
  Dcm_CommunicationModeType Test_Dcm_CommunicationModeType_V_4 = DCM_DISABLE_RX_TX_NORMAL;
  Dcm_CommunicationModeType Test_Dcm_CommunicationModeType_V_5 = DCM_ENABLE_RX_TX_NM;
  Dcm_CommunicationModeType Test_Dcm_CommunicationModeType_V_6 = DCM_ENABLE_RX_DISABLE_TX_NM;
  Dcm_CommunicationModeType Test_Dcm_CommunicationModeType_V_7 = DCM_DISABLE_RX_ENABLE_TX_NM;
  Dcm_CommunicationModeType Test_Dcm_CommunicationModeType_V_8 = DCM_DISABLE_RX_TX_NM;
  Dcm_CommunicationModeType Test_Dcm_CommunicationModeType_V_9 = DCM_ENABLE_RX_TX_NORM_NM;
  Dcm_CommunicationModeType Test_Dcm_CommunicationModeType_V_10 = DCM_ENABLE_RX_DISABLE_TX_NORM_NM;
  Dcm_CommunicationModeType Test_Dcm_CommunicationModeType_V_11 = DCM_DISABLE_RX_ENABLE_TX_NORM_NM;
  Dcm_CommunicationModeType Test_Dcm_CommunicationModeType_V_12 = DCM_DISABLE_RX_TX_NORM_NM;

  Dcm_ConfirmationStatusType Test_Dcm_ConfirmationStatusType_V_1 = DCM_RES_POS_OK;
  Dcm_ConfirmationStatusType Test_Dcm_ConfirmationStatusType_V_2 = DCM_RES_POS_NOT_OK;
  Dcm_ConfirmationStatusType Test_Dcm_ConfirmationStatusType_V_3 = DCM_RES_NEG_OK;
  Dcm_ConfirmationStatusType Test_Dcm_ConfirmationStatusType_V_4 = DCM_RES_NEG_NOT_OK;

  Dcm_DiagnosticSessionControlType Test_Dcm_DiagnosticSessionControlType_V_1 = DCM_ENUM_DEFAULT_SESSION;
  Dcm_DiagnosticSessionControlType Test_Dcm_DiagnosticSessionControlType_V_2 = DCM_ENUM_PROGRAMMING_SESSION;
  Dcm_DiagnosticSessionControlType Test_Dcm_DiagnosticSessionControlType_V_3 = DCM_ENUM_EXTENDED_SESSION;
  Dcm_DiagnosticSessionControlType Test_Dcm_DiagnosticSessionControlType_V_4 = DCM_ENUM_distantVehicleAccessSession_DVAS;

  Dcm_EcuResetType Test_Dcm_EcuResetType_V_1 = DCM_ENUM_NONE;
  Dcm_EcuResetType Test_Dcm_EcuResetType_V_2 = DCM_ENUM_HARD;
  Dcm_EcuResetType Test_Dcm_EcuResetType_V_3 = DCM_ENUM_KEYONOFF;
  Dcm_EcuResetType Test_Dcm_EcuResetType_V_4 = DCM_ENUM_SOFT;
  Dcm_EcuResetType Test_Dcm_EcuResetType_V_5 = DCM_ENUM_JUMPTOBOOTLOADER;
  Dcm_EcuResetType Test_Dcm_EcuResetType_V_6 = DCM_ENUM_JUMPTOSYSSUPPLIERBOOTLOADER;
  Dcm_EcuResetType Test_Dcm_EcuResetType_V_7 = DCM_ENUM_EXECUTE;

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_42 = DCM_E_VMSCNC_0;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_43 = DCM_E_VMSCNC_1;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_44 = DCM_E_VMSCNC_2;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_45 = DCM_E_VMSCNC_3;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_46 = DCM_E_VMSCNC_4;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_47 = DCM_E_VMSCNC_5;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_48 = DCM_E_VMSCNC_6;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_49 = DCM_E_VMSCNC_7;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_50 = DCM_E_VMSCNC_8;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_51 = DCM_E_VMSCNC_9;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_52 = DCM_E_VMSCNC_A;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_53 = DCM_E_VMSCNC_B;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_54 = DCM_E_VMSCNC_C;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_55 = DCM_E_VMSCNC_D;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_56 = DCM_E_VMSCNC_E;

  Dcm_OpStatusType Test_Dcm_OpStatusType_V_1 = DCM_INITIAL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_2 = DCM_PENDING;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_3 = DCM_CANCEL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_4 = DCM_FORCE_RCRRP_OK;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_5 = DCM_FORCE_RCRRP_NOT_OK;

  Dcm_ProtocolType Test_Dcm_ProtocolType_V_1 = DCM_OBD_ON_CAN;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_2 = DCM_OBD_ON_FLEXRAY;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_3 = DCM_OBD_ON_IP;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_4 = DCM_UDS_ON_CAN;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_5 = DCM_UDS_ON_FLEXRAY;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_6 = DCM_UDS_ON_IP;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_7 = DCM_NO_ACTIVE_PROTOCOL;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_8 = DCM_SUPPLIER_1;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_9 = DCM_SUPPLIER_2;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_10 = DCM_SUPPLIER_3;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_11 = DCM_SUPPLIER_4;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_12 = DCM_SUPPLIER_5;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_13 = DCM_SUPPLIER_6;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_14 = DCM_SUPPLIER_7;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_15 = DCM_SUPPLIER_8;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_16 = DCM_SUPPLIER_9;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_17 = DCM_SUPPLIER_10;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_18 = DCM_SUPPLIER_11;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_19 = DCM_SUPPLIER_12;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_20 = DCM_SUPPLIER_13;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_21 = DCM_SUPPLIER_14;
  Dcm_ProtocolType Test_Dcm_ProtocolType_V_22 = DCM_SUPPLIER_15;

  Dcm_RequestKindType Test_Dcm_RequestKindType_V_1 = DCM_REQ_KIND_NONE;
  Dcm_RequestKindType Test_Dcm_RequestKindType_V_2 = DCM_REQ_KIND_EXTERNAL;
  Dcm_RequestKindType Test_Dcm_RequestKindType_V_3 = DCM_REQ_KIND_ROE;

  Dcm_SecLevelType Test_Dcm_SecLevelType_V_1 = DCM_SEC_LEV_LOCKED;
  Dcm_SecLevelType Test_Dcm_SecLevelType_V_2 = DCM_SEC_LEV_L1;

  Dcm_SesCtrlType Test_Dcm_SesCtrlType_V_1 = DCM_DEFAULT_SESSION;
  Dcm_SesCtrlType Test_Dcm_SesCtrlType_V_2 = DCM_PROGRAMMING_SESSION;
  Dcm_SesCtrlType Test_Dcm_SesCtrlType_V_3 = DCM_EXTENDED_DIAGNOSTIC_SESSION;
  Dcm_SesCtrlType Test_Dcm_SesCtrlType_V_4 = DCM_distantVehicleAccessSession_DVAS;

  /* Modes */

  Dcm_DiagnosticSessionControlType Test_DcmDiagnosticSessionControl_MV_1 = RTE_MODE_DcmDiagnosticSessionControl_DEFAULT_SESSION;
  Dcm_DiagnosticSessionControlType Test_DcmDiagnosticSessionControl_MV_2 = RTE_MODE_DcmDiagnosticSessionControl_PROGRAMMING_SESSION;
  Dcm_DiagnosticSessionControlType Test_DcmDiagnosticSessionControl_MV_3 = RTE_MODE_DcmDiagnosticSessionControl_EXTENDED_SESSION;
  Dcm_DiagnosticSessionControlType Test_DcmDiagnosticSessionControl_MV_4 = RTE_MODE_DcmDiagnosticSessionControl_distantVehicleAccessSession_DVAS;
  Dcm_DiagnosticSessionControlType Test_DcmDiagnosticSessionControl_TV = RTE_TRANSITION_DcmDiagnosticSessionControl;

  Dcm_EcuResetType Test_DcmEcuReset_MV_1 = RTE_MODE_DcmEcuReset_NONE;
  Dcm_EcuResetType Test_DcmEcuReset_MV_2 = RTE_MODE_DcmEcuReset_HARD;
  Dcm_EcuResetType Test_DcmEcuReset_MV_3 = RTE_MODE_DcmEcuReset_KEYONOFF;
  Dcm_EcuResetType Test_DcmEcuReset_MV_4 = RTE_MODE_DcmEcuReset_SOFT;
  Dcm_EcuResetType Test_DcmEcuReset_MV_5 = RTE_MODE_DcmEcuReset_JUMPTOBOOTLOADER;
  Dcm_EcuResetType Test_DcmEcuReset_MV_6 = RTE_MODE_DcmEcuReset_JUMPTOSYSSUPPLIERBOOTLOADER;
  Dcm_EcuResetType Test_DcmEcuReset_MV_7 = RTE_MODE_DcmEcuReset_EXECUTE;
  Dcm_EcuResetType Test_DcmEcuReset_TV = RTE_TRANSITION_DcmEcuReset;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_Os:
     Reason:     This justification is used as summary justification for all deviations caused by the MICROSAR OS
                 which is for testing of the RTE. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
