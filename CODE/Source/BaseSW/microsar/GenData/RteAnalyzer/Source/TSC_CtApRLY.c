/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApRLY.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApRLY.h"
#include "TSC_CtApRLY.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApRLY_Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(boolean *data)
{
  return Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(data);
}

Std_ReturnType TSC_CtApRLY_Rte_Read_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode *data)
{
  return Rte_Read_PpInputVoltageMode_DeInputVoltageMode(data);
}

Std_ReturnType TSC_CtApRLY_Rte_Read_PpInt_OBC_ACRange_OBC_ACRange(OBC_ACRange *data)
{
  return Rte_Read_PpInt_OBC_ACRange_OBC_ACRange(data);
}

Std_ReturnType TSC_CtApRLY_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
{
  return Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(data);
}

Std_ReturnType TSC_CtApRLY_Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt *data)
{
  return Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(data);
}

Std_ReturnType TSC_CtApRLY_Rte_Read_PpInt_OBC_Status_OBC_Status(OBC_Status *data)
{
  return Rte_Read_PpInt_OBC_Status_OBC_Status(data);
}

Std_ReturnType TSC_CtApRLY_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(data);
}

Std_ReturnType TSC_CtApRLY_Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(PFC_Vdclink_V *data)
{
  return Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(data);
}

Std_ReturnType TSC_CtApRLY_Rte_Read_PpStopConditions_DeStopConditions(boolean *data)
{
  return Rte_Read_PpStopConditions_DeStopConditions(data);
}




Std_ReturnType TSC_CtApRLY_Rte_Write_PpOutputPrechargeRelays_DeOutputPrechargeRelays(boolean data)
{
  return Rte_Write_PpOutputPrechargeRelays_DeOutputPrechargeRelays(data);
}

Std_ReturnType TSC_CtApRLY_Rte_Write_PpOutputRelayMono_DeOutputRelayMono(boolean data)
{
  return Rte_Write_PpOutputRelayMono_DeOutputRelayMono(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApRLY_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}
Std_ReturnType TSC_CtApRLY_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
{
  return Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(frameNum, byteNum, data);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtMaxPrechargeVDCLinkMono  TSC_CtApRLY_Rte_CData_CalMaxPrechargeVDCLinkMono(void)
{
  return (IdtMaxPrechargeVDCLinkMono ) Rte_CData_CalMaxPrechargeVDCLinkMono();
}
IdtMaxPrechargeVDCLinkTriphasic  TSC_CtApRLY_Rte_CData_CalMaxPrechargeVDCLinkTriphasic(void)
{
  return (IdtMaxPrechargeVDCLinkTriphasic ) Rte_CData_CalMaxPrechargeVDCLinkTriphasic();
}
IdtMinPrechargeVDCLinkMono  TSC_CtApRLY_Rte_CData_CalMinPrechargeVDCLinkMono(void)
{
  return (IdtMinPrechargeVDCLinkMono ) Rte_CData_CalMinPrechargeVDCLinkMono();
}
IdtMinPrechargeVDCLinkTriphasic  TSC_CtApRLY_Rte_CData_CalMinPrechargeVDCLinkTriphasic(void)
{
  return (IdtMinPrechargeVDCLinkTriphasic ) Rte_CData_CalMinPrechargeVDCLinkTriphasic();
}

     /* CtApRLY */
      /* CtApRLY */



