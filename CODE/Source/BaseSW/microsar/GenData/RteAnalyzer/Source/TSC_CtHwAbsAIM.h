/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtHwAbsAIM.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(IdtAmbientTemperaturePhysicalValue data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempAC1Raw(IdtMsrTempRaw data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempAC2Raw(IdtMsrTempRaw data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempAC3Raw(IdtMsrTempRaw data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempACNRaw(IdtMsrTempRaw data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempDC1Raw(IdtMsrTempRaw data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempDC2Raw(IdtMsrTempRaw data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(IdtTempSyncRectPhysicalValue data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpElockFeedbackCurrent_DeElockFeedbackCurrent(IdtElockFeedbackCurrent data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpElockFeedbackLock_DeElockFeedbackLock(IdtElockFeedbackLock data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpElockFeedbackUnlock_DeElockFeedbackUnlock(IdtElockFeedbackUnlock data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(IdtExtOpPlugLockRaw data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(IdtPlugLedFeedbackPhysicalValue data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Write_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(IdtProximityDetectPhysicalValue data);

/** Client server interfaces */
Std_ReturnType TSC_CtHwAbsAIM_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data);

/** Service interfaces */
Std_ReturnType TSC_CtHwAbsAIM_Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_ReadBlock(dtRef_VOID DstPtr);
Std_ReturnType TSC_CtHwAbsAIM_Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr);

/** SW-C local Calibration Parameters */
IdtVoltageExternalADCReference  TSC_CtHwAbsAIM_Rte_CData_CalVoltageExternalADCReferenceWave1(void);
IdtVoltageExternalADCReference  TSC_CtHwAbsAIM_Rte_CData_CalVoltageExternalADCReferenceWave2(void);
IdtDebounceADCReference  TSC_CtHwAbsAIM_Rte_CData_CalDebounceADCReference(void);
IdtArrayInitAIMVoltRef * TSC_CtHwAbsAIM_Rte_CData_AIMVoltRefNvBlockNeed_DefaultValue(void);

/** Per Instance Memories */
Rte_DT_IdtArrayInitAIMVoltRef_0 *TSC_CtHwAbsAIM_Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock(void);



