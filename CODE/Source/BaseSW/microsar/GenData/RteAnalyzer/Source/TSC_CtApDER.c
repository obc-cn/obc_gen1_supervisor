/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApDER.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApDER.h"
#include "TSC_CtApDER.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDER_Rte_Read_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(IdtAmbientTemperaturePhysicalValue *data)
{
  return Rte_Read_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(data);
}

Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(DCHV_ADC_NTC_MOD_5 *data)
{
  return Rte_Read_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(data);
}

Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(DCHV_ADC_NTC_MOD_6 *data)
{
  return Rte_Read_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(data);
}

Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(DCLV_T_PP_A *data)
{
  return Rte_Read_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(data);
}

Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(DCLV_T_PP_B *data)
{
  return Rte_Read_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(data);
}

Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(DCLV_T_SW_BUCK_A *data)
{
  return Rte_Read_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(data);
}

Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(DCLV_T_SW_BUCK_B *data)
{
  return Rte_Read_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(data);
}

Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(PFC_Temp_M1_C *data)
{
  return Rte_Read_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(data);
}

Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(PFC_Temp_M4_C *data)
{
  return Rte_Read_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(data);
}

Std_ReturnType TSC_CtApDER_Rte_Read_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(IdtTempSyncRectPhysicalValue *data)
{
  return Rte_Read_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(data);
}




Std_ReturnType TSC_CtApDER_Rte_Write_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError(boolean data)
{
  return Rte_Write_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError(data);
}

Std_ReturnType TSC_CtApDER_Rte_Write_PpImplausibilityTempBuck_DeImplausibilityTempBuck(boolean data)
{
  return Rte_Write_PpImplausibilityTempBuck_DeImplausibilityTempBuck(data);
}

Std_ReturnType TSC_CtApDER_Rte_Write_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(boolean data)
{
  return Rte_Write_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(data);
}

Std_ReturnType TSC_CtApDER_Rte_Write_PpInt_DCDC_Temperature_DCDC_Temperature(DCDC_Temperature data)
{
  return Rte_Write_PpInt_DCDC_Temperature_DCDC_Temperature(data);
}

Std_ReturnType TSC_CtApDER_Rte_Write_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(DCLV_Temp_Derating_Factor data)
{
  return Rte_Write_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(data);
}

Std_ReturnType TSC_CtApDER_Rte_Write_PpInt_OBC_OBCTemp_OBC_OBCTemp(OBC_OBCTemp data)
{
  return Rte_Write_PpInt_OBC_OBCTemp_OBC_OBCTemp(data);
}

Std_ReturnType TSC_CtApDER_Rte_Write_PpOBCDerating_DeOBCDerating(boolean data)
{
  return Rte_Write_PpOBCDerating_DeOBCDerating(data);
}

Std_ReturnType TSC_CtApDER_Rte_Write_PpPDERATING_OBC_DePDERATING_OBC(IdtPDERATING_OBC data)
{
  return Rte_Write_PpPDERATING_OBC_DePDERATING_OBC(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCDC_FinishLinear(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCDC_FinishLinear();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCDC_MaxDeviation(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCDC_MaxDeviation();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCDC_MinDeviation(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCDC_MinDeviation();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCDC_StartLinear(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCDC_StartLinear();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_AmbTemp(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVHighTempEndDerating_AmbTemp();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_Buck_A(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVHighTempEndDerating_Buck_A();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_Buck_B(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVHighTempEndDerating_Buck_B();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_PushPull_A(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVHighTempEndDerating_PushPull_A();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_PushPull_B(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVHighTempEndDerating_PushPull_B();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_SyncRect(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVHighTempEndDerating_SyncRect();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_AmbTemp(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVHighTempStartDerating_AmbTemp();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_Buck_A(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVHighTempStartDerating_Buck_A();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_Buck_B(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVHighTempStartDerating_Buck_B();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_PushPull_A(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVHighTempStartDerating_PushPull_A();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_PushPull_B(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVHighTempStartDerating_PushPull_B();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_SyncRect(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVHighTempStartDerating_SyncRect();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_AmbTemp(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVLowTempEndDerating_AmbTemp();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_Buck_A(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVLowTempEndDerating_Buck_A();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_Buck_B(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVLowTempEndDerating_Buck_B();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_PushPull_A(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVLowTempEndDerating_PushPull_A();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_PushPull_B(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVLowTempEndDerating_PushPull_B();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_SyncRect(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVLowTempEndDerating_SyncRect();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_AmbTemp(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVLowTempStartDerating_AmbTemp();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_Buck_A(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVLowTempStartDerating_Buck_A();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_Buck_B(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVLowTempStartDerating_Buck_B();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_PushPull_A(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVLowTempStartDerating_PushPull_A();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_PushPull_B(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVLowTempStartDerating_PushPull_B();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_SyncRect(void)
{
  return (IdtTempDerating ) Rte_CData_CalDCLVLowTempStartDerating_SyncRect();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_AmbTempOBC(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCHighTempEndDerating_AmbTempOBC();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_DCHV_M5(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCHighTempEndDerating_DCHV_M5();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_DCHV_M6(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCHighTempEndDerating_DCHV_M6();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_PFC_M1(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCHighTempEndDerating_PFC_M1();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_PFC_M4(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCHighTempEndDerating_PFC_M4();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_AmbTempOBC(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCHighTempStartDerating_AmbTempOBC();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_DCHV_M5(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCHighTempStartDerating_DCHV_M5();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_DCHV_M6(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCHighTempStartDerating_DCHV_M6();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_PFC_M1(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCHighTempStartDerating_PFC_M1();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_PFC_M4(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCHighTempStartDerating_PFC_M4();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_AmbTempOBC(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCLowTempEndDerating_AmbTempOBC();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_DCHV_M5(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCLowTempEndDerating_DCHV_M5();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_DCHV_M6(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCLowTempEndDerating_DCHV_M6();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_PFC_M1(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCLowTempEndDerating_PFC_M1();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_PFC_M4(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCLowTempEndDerating_PFC_M4();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_AmbTempOBC(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCLowTempStartDerating_AmbTempOBC();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_DCHV_M5(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCLowTempStartDerating_DCHV_M5();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_DCHV_M6(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCLowTempStartDerating_DCHV_M6();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_PFC_M1(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCLowTempStartDerating_PFC_M1();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_PFC_M4(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBCLowTempStartDerating_PFC_M4();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBC_FinishLinear(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBC_FinishLinear();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBC_MaxDeviation(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBC_MaxDeviation();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBC_MinDeviation(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBC_MinDeviation();
}
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBC_StartLinear(void)
{
  return (IdtTempDerating ) Rte_CData_CalOBC_StartLinear();
}

     /* CtApDER */
      /* CtApDER */



