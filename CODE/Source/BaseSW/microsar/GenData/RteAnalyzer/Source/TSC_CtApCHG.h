/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApCHG.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApCHG_Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(IdtELockSetPoint *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(boolean *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpFreqOutRange_DeFreqOutRange(boolean *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(BMS_HighestChargeCurrentAllow *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(BMS_HighestChargeVoltageAllow *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(BMS_MainConnectorState *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(BMS_OnBoardChargerEnable *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(BMS_QuickChargeConnectorState *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_SOC_BMS_SOC(BMS_SOC *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_Voltage_BMS_Voltage(BMS_Voltage *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(OBC_CP_connection_Status *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_OBC_Fault_OBC_Fault(OBC_Fault *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(PFC_VPH1_Freq_Hz *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(PFC_VPH2_Freq_Hz *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(PFC_VPH3_Freq_Hz *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(PFC_VPH23_Peak_V *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(PFC_VPH31_Peak_V *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(PFC_VPH23_RMS_V *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(PFC_VPH31_RMS_V *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpOBCDerating_DeOBCDerating(boolean *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue(boolean *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue(boolean *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpPlantModeState_DePlantModeState(boolean *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpTempFaults_DeTempDC1FaultSCG(boolean *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpTempFaults_DeTempDC1FaultSCP(boolean *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpTempFaults_DeTempDC2FaultSCG(boolean *data);
Std_ReturnType TSC_CtApCHG_Rte_Read_PpTempFaults_DeTempDC2FaultSCP(boolean *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApCHG_Rte_Write_PpDiagnosticNoACInput_DeDiagnosticNoACInput(boolean data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(IdtInputVoltageMode data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_OBC_ACRange_Delayed_OBC_ACRange(OBC_ACRange data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpControlPilotFreqError_DeControlPilotFreqError(boolean data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(IdtEVSEMaximumPowerLimit data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpElockFaultDetected_DeElockFaultDetected(boolean data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpForceElockCloseMode4_DeForceElockCloseMode4(boolean data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq(DCDC_OBCMainContactorReq data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq(DCDC_OBCQuickChargeContactorReq data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(EVSE_RTAB_STOP_CHARGE data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_OBC_ACRange_OBC_ACRange(OBC_ACRange data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_OBC_Status_OBC_Status(OBC_Status data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress(boolean data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(boolean data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpRequestHWStopOBC_DeRequestHWStopOBC(boolean data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue(boolean data);
Std_ReturnType TSC_CtApCHG_Rte_Write_PpStopConditions_DeStopConditions(boolean data);

/** Client server interfaces */
Std_ReturnType TSC_CtApCHG_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);
Std_ReturnType TSC_CtApCHG_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data);

/** Service interfaces */
Std_ReturnType TSC_CtApCHG_Rte_Call_PpComMUserNeed_EthUserRequest_RequestComMode(ComM_ModeType ComMode);

/** Calibration Component Calibration Parameters */
IdtInputVoltageThreshold  TSC_CtApCHG_Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold(void);

/** SW-C local Calibration Parameters */
IdtMaxDemmandVoltage  TSC_CtApCHG_Rte_CData_CalMaxDemmandVoltage(void);
IdtMaxDiscoveryVoltage  TSC_CtApCHG_Rte_CData_CalMaxDiscoveryVoltage(void);
IdtMaxPrechargeVoltage  TSC_CtApCHG_Rte_CData_CalMaxPrechargeVoltage(void);
IdtStopChargeDemmandTimer  TSC_CtApCHG_Rte_CData_CalStopChargeDemmandTimer(void);
IdtTimeConfirmNoAC  TSC_CtApCHG_Rte_CData_CalTimeConfirmNoAC(void);
IdtBulkSocConf  TSC_CtApCHG_Rte_CData_CalBulkSocConf(void);
IdtDemandMaxCurrent  TSC_CtApCHG_Rte_CData_CalDemandMaxCurrent(void);
IdtEnergyCapacity  TSC_CtApCHG_Rte_CData_CalEnergyCapacity(void);
IdtMaxDemmandCurrent  TSC_CtApCHG_Rte_CData_CalMaxDemmandCurrent(void);
IdtMaxDiscoveryCurrent  TSC_CtApCHG_Rte_CData_CalMaxDiscoveryCurrent(void);
IdtMaxInputVoltage110V  TSC_CtApCHG_Rte_CData_CalMaxInputVoltage110V(void);
IdtMaxInputVoltage220V  TSC_CtApCHG_Rte_CData_CalMaxInputVoltage220V(void);
IdtMaxPrechargeCurrent  TSC_CtApCHG_Rte_CData_CalMaxPrechargeCurrent(void);
IdtMinInputVoltage110V  TSC_CtApCHG_Rte_CData_CalMinInputVoltage110V(void);
IdtMinInputVoltage220V  TSC_CtApCHG_Rte_CData_CalMinInputVoltage220V(void);
IdtTargetPrechargeCurrent  TSC_CtApCHG_Rte_CData_CalTargetPrechargeCurrent(void);
IdtThresholdNoAC  TSC_CtApCHG_Rte_CData_CalThresholdNoAC(void);
IdtTimeElockFaultDetected  TSC_CtApCHG_Rte_CData_CalTimeElockFaultDetected(void);




