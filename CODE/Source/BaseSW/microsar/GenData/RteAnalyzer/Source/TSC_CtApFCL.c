/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApFCL.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApFCL.h"
#include "TSC_CtApFCL.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApFCL_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
{
  return Rte_Read_PpAppRCDECUState_DeAppRCDECUState(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
{
  return Rte_Read_PpBatteryVolt_DeBatteryVolt(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
{
  return Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpChLedRGBCtl_DeChLedBrCtl(IdtChLedCtl *data)
{
  return Rte_Read_PpChLedRGBCtl_DeChLedBrCtl(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpChLedRGBCtl_DeChLedGrCtl(IdtChLedCtl *data)
{
  return Rte_Read_PpChLedRGBCtl_DeChLedGrCtl(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpChLedRGBCtl_DeChLedRrCtl(IdtChLedCtl *data)
{
  return Rte_Read_PpChLedRGBCtl_DeChLedRrCtl(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data)
{
  return Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected *data)
{
  return Rte_Read_PpHWEditionDetected_DeHWEditionDetected(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
{
  return Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
{
  return Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue(boolean *data)
{
  return Rte_Read_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue *data)
{
  return Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue *data)
{
  return Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue *data)
{
  return Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
{
  return Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(data);
}




Std_ReturnType TSC_CtApFCL_Rte_Write_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue(boolean data)
{
  return Rte_Write_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpExtChLedRGBCtl_DeExtChLedBrCtl(IdtExtChLedRGBCtl data)
{
  return Rte_Write_PpExtChLedRGBCtl_DeExtChLedBrCtl(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpExtChLedRGBCtl_DeExtChLedGrCtl(IdtExtChLedRGBCtl data)
{
  return Rte_Write_PpExtChLedRGBCtl_DeExtChLedGrCtl(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpExtChLedRGBCtl_DeExtChLedRrCtl(IdtExtChLedRGBCtl data)
{
  return Rte_Write_PpExtChLedRGBCtl_DeExtChLedRrCtl(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedBlueFaultOC(boolean data)
{
  return Rte_Write_PpLedFaults_DeLedBlueFaultOC(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedBlueFaultSCG(boolean data)
{
  return Rte_Write_PpLedFaults_DeLedBlueFaultSCG(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedBlueFaultSCP(boolean data)
{
  return Rte_Write_PpLedFaults_DeLedBlueFaultSCP(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedGreenFaultOC(boolean data)
{
  return Rte_Write_PpLedFaults_DeLedGreenFaultOC(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedGreenFaultSCG(boolean data)
{
  return Rte_Write_PpLedFaults_DeLedGreenFaultSCG(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedGreenFaultSCP(boolean data)
{
  return Rte_Write_PpLedFaults_DeLedGreenFaultSCP(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedRedFaultOC(boolean data)
{
  return Rte_Write_PpLedFaults_DeLedRedFaultOC(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedRedFaultSCG(boolean data)
{
  return Rte_Write_PpLedFaults_DeLedRedFaultSCG(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedRedFaultSCP(boolean data)
{
  return Rte_Write_PpLedFaults_DeLedRedFaultSCP(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedMonitoringConditions_DeBlueLedMonitoringConditions(boolean data)
{
  return Rte_Write_PpLedMonitoringConditions_DeBlueLedMonitoringConditions(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedMonitoringConditions_DeGreenLedMonitoringConditions(boolean data)
{
  return Rte_Write_PpLedMonitoringConditions_DeGreenLedMonitoringConditions(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedMonitoringConditions_DeRedLedMonitoringConditions(boolean data)
{
  return Rte_Write_PpLedMonitoringConditions_DeRedLedMonitoringConditions(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApFCL_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApFCL_Rte_Read_PpPlgLedrCtrl_DePlgLedrCtrl(IdtPlgLedrCtrl *data)
{
  return Rte_Read_PpPlgLedrCtrl_DePlgLedrCtrl(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Read_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(IdtPlugLedFeedbackPhysicalValue *data)
{
  return Rte_Read_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(data);
}




Std_ReturnType TSC_CtApFCL_Rte_Write_PpExtPlgLedrCtl_DeExtPlgLedrCtl(IdtExtPlgLedrCtl data)
{
  return Rte_Write_PpExtPlgLedrCtl_DeExtPlgLedrCtl(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedMonitoringConditions_DePlugLedMonitoringConditions(boolean data)
{
  return Rte_Write_PpLedMonitoringConditions_DePlugLedMonitoringConditions(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpPlugLedFaults_DeLedPlugFaultOC(boolean data)
{
  return Rte_Write_PpPlugLedFaults_DeLedPlugFaultOC(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCG(boolean data)
{
  return Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCG(data);
}

Std_ReturnType TSC_CtApFCL_Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCP(boolean data)
{
  return Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCP(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





IdtBatteryVoltMaxTest  TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(void)
{
  return (IdtBatteryVoltMaxTest ) Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest();
}
IdtBatteryVoltMinTest  TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(void)
{
  return (IdtBatteryVoltMinTest ) Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest();
}
IdtVehStopMaxTest  TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(void)
{
  return (IdtVehStopMaxTest ) Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest();
}

IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(void)
{
  return (IdtAfts_DelayLed ) Rte_CData_CalAfts_DelayLedBlueOff();
}
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(void)
{
  return (IdtAfts_DelayLed ) Rte_CData_CalAfts_DelayLedBlueOn();
}
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(void)
{
  return (IdtAfts_DelayLed ) Rte_CData_CalAfts_DelayLedGreenOff();
}
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(void)
{
  return (IdtAfts_DelayLed ) Rte_CData_CalAfts_DelayLedGreenOn();
}
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(void)
{
  return (IdtAfts_DelayLed ) Rte_CData_CalAfts_DelayLedPlugOff();
}
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(void)
{
  return (IdtAfts_DelayLed ) Rte_CData_CalAfts_DelayLedPlugOn();
}
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(void)
{
  return (IdtAfts_DelayLed ) Rte_CData_CalAfts_DelayLedRedOff();
}
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(void)
{
  return (IdtAfts_DelayLed ) Rte_CData_CalAfts_DelayLedRedOn();
}
IdtLedThresholdSCG  TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(void)
{
  return (IdtLedThresholdSCG ) Rte_CData_CalLedBlueThresholdSCG();
}
IdtLedThresholdSCP  TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(void)
{
  return (IdtLedThresholdSCP ) Rte_CData_CalLedBlueThresholdSCP();
}
IdtLedThresholdSCG  TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(void)
{
  return (IdtLedThresholdSCG ) Rte_CData_CalLedGreenThresholdSCG();
}
IdtLedThresholdSCP  TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(void)
{
  return (IdtLedThresholdSCP ) Rte_CData_CalLedGreenThresholdSCP();
}
IdtLedThresholdSCG  TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(void)
{
  return (IdtLedThresholdSCG ) Rte_CData_CalLedRedThresholdSCG();
}
IdtLedThresholdSCP  TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(void)
{
  return (IdtLedThresholdSCP ) Rte_CData_CalLedRedThresholdSCP();
}
IdtMaxNormalPlugLed  TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(void)
{
  return (IdtMaxNormalPlugLed ) Rte_CData_CalMaxNormalPlugLed();
}
IdtMinNormalPlugLed  TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(void)
{
  return (IdtMinNormalPlugLed ) Rte_CData_CalMinNormalPlugLed();
}
IdtPlugLedMaxOCDetection  TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(void)
{
  return (IdtPlugLedMaxOCDetection ) Rte_CData_CalPlugLedMaxOCDetection();
}
IdtPlugLedMinOCDetection  TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(void)
{
  return (IdtPlugLedMinOCDetection ) Rte_CData_CalPlugLedMinOCDetection();
}
IdtPlugLedThresholdSCG  TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(void)
{
  return (IdtPlugLedThresholdSCG ) Rte_CData_CalPlugLedThresholdSCG();
}
IdtPlugLedThresholdSCP  TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(void)
{
  return (IdtPlugLedThresholdSCP ) Rte_CData_CalPlugLedThresholdSCP();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedBlueOCConfirmTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedBlueOCHealTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedBlueSCGConfirmTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedBlueSCGHealTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedBlueSCPConfirmTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedBlueSCPHealTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedGreenOCConfirmTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedGreenOCHealTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedGreenSCGConfirmTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedGreenSCGHealTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedGreenSCPConfirmTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedGreenSCPHealTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedPlugOCConfirmTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedPlugOCHealTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedPlugSCGConfirmTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedPlugSCGHealTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedPlugSCPConfirmTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedPlugSCPHealTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedRedOCConfirmTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedRedOCHealTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedRedSCGConfirmTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedRedSCGHealTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedRedSCPConfirmTime();
}
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(void)
{
  return (IdtLedFaultTime ) Rte_CData_CalLedRedSCPHealTime();
}
boolean  TSC_CtApFCL_Rte_CData_CalEnableLedBlue(void)
{
  return (boolean ) Rte_CData_CalEnableLedBlue();
}
boolean  TSC_CtApFCL_Rte_CData_CalEnableLedGreen(void)
{
  return (boolean ) Rte_CData_CalEnableLedGreen();
}
boolean  TSC_CtApFCL_Rte_CData_CalEnableLedPlug(void)
{
  return (boolean ) Rte_CData_CalEnableLedPlug();
}
boolean  TSC_CtApFCL_Rte_CData_CalEnableLedRed(void)
{
  return (boolean ) Rte_CData_CalEnableLedRed();
}

     /* CtApFCL */
      /* CtApFCL */

/** Per Instance Memories */
uint16 *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC(void)
{
  return Rte_Pim_PimLedBlueCounterOC();
}
uint16 *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG(void)
{
  return Rte_Pim_PimLedBlueCounterSCG();
}
uint16 *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP(void)
{
  return Rte_Pim_PimLedBlueCounterSCP();
}
uint16 *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC(void)
{
  return Rte_Pim_PimLedGreenCounterOC();
}
uint16 *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG(void)
{
  return Rte_Pim_PimLedGreenCounterSCG();
}
uint16 *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP(void)
{
  return Rte_Pim_PimLedGreenCounterSCP();
}
uint16 *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC(void)
{
  return Rte_Pim_PimLedPlugCounterOC();
}
uint16 *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG(void)
{
  return Rte_Pim_PimLedPlugCounterSCG();
}
uint16 *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP(void)
{
  return Rte_Pim_PimLedPlugCounterSCP();
}
uint16 *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC(void)
{
  return Rte_Pim_PimLedRedCounterOC();
}
uint16 *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG(void)
{
  return Rte_Pim_PimLedRedCounterSCG();
}
uint16 *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP(void)
{
  return Rte_Pim_PimLedRedCounterSCP();
}
boolean *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC(void)
{
  return Rte_Pim_PimLedBluePrefaultOC();
}
boolean *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG(void)
{
  return Rte_Pim_PimLedBluePrefaultSCG();
}
boolean *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP(void)
{
  return Rte_Pim_PimLedBluePrefaultSCP();
}
boolean *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC(void)
{
  return Rte_Pim_PimLedGreenPrefaultOC();
}
boolean *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG(void)
{
  return Rte_Pim_PimLedGreenPrefaultSCG();
}
boolean *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP(void)
{
  return Rte_Pim_PimLedGreenPrefaultSCP();
}
boolean *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC(void)
{
  return Rte_Pim_PimLedPlugPrefaultOC();
}
boolean *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG(void)
{
  return Rte_Pim_PimLedPlugPrefaultSCG();
}
boolean *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP(void)
{
  return Rte_Pim_PimLedPlugPrefaultSCP();
}
boolean *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC(void)
{
  return Rte_Pim_PimLedRedPrefaultOC();
}
boolean *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG(void)
{
  return Rte_Pim_PimLedRedPrefaultSCG();
}
boolean *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP(void)
{
  return Rte_Pim_PimLedRedPrefaultSCP();
}



