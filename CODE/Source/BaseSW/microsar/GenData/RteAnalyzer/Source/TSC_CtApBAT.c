/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApBAT.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApBAT.h"
#include "TSC_CtApBAT.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApBAT_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
{
  return Rte_Read_PpBatteryVolt_DeBatteryVolt(data);
}




Std_ReturnType TSC_CtApBAT_Rte_Write_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions(boolean data)
{
  return Rte_Write_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions(data);
}

Std_ReturnType TSC_CtApBAT_Rte_Write_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState data)
{
  return Rte_Write_PpBatteryVoltageState_DeBatteryVoltageState(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApBAT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtBatteryVoltageTime  TSC_CtApBAT_Rte_CData_CalBatteryVoltageOVHealTime(void)
{
  return (IdtBatteryVoltageTime ) Rte_CData_CalBatteryVoltageOVHealTime();
}
IdtBatteryVoltageThreshold  TSC_CtApBAT_Rte_CData_CalBatteryVoltageThresholdOV(void)
{
  return (IdtBatteryVoltageThreshold ) Rte_CData_CalBatteryVoltageThresholdOV();
}
IdtBatteryVoltageThreshold  TSC_CtApBAT_Rte_CData_CalBatteryVoltageThresholdUV(void)
{
  return (IdtBatteryVoltageThreshold ) Rte_CData_CalBatteryVoltageThresholdUV();
}
IdtBatteryVoltageTime  TSC_CtApBAT_Rte_CData_CalBatteryVoltageUVConfirmTime(void)
{
  return (IdtBatteryVoltageTime ) Rte_CData_CalBatteryVoltageUVConfirmTime();
}
IdtBatteryVoltageTime  TSC_CtApBAT_Rte_CData_CalBatteryVoltageUVHealTime(void)
{
  return (IdtBatteryVoltageTime ) Rte_CData_CalBatteryVoltageUVHealTime();
}
IdtBatteryVoltageTime  TSC_CtApBAT_Rte_CData_CarBatteryVoltageOVConfirmTime(void)
{
  return (IdtBatteryVoltageTime ) Rte_CData_CarBatteryVoltageOVConfirmTime();
}

     /* CtApBAT */
      /* CtApBAT */



