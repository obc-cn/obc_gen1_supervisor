/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApPSH.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApPSH_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data);
Std_ReturnType TSC_CtApPSH_Rte_Read_PpInputChargePushRaw_DeInputChargePushRaw(boolean *data);
Std_ReturnType TSC_CtApPSH_Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(BSI_LockedVehicle *data);
Std_ReturnType TSC_CtApPSH_Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState *data);
Std_ReturnType TSC_CtApPSH_Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApPSH_Rte_Write_PpOutputChargePushFil_DeOutputChargePushFil(boolean data);

/** Client server interfaces */
Std_ReturnType TSC_CtApPSH_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);

/** Service interfaces */
Std_ReturnType TSC_CtApPSH_Rte_Call_NvMService_AC3_SRBS_NvPSHBlockNeed_SetRamBlockStatus(boolean RamBlockStatus);

/** SW-C local Calibration Parameters */
IdtTimePushInhDelay  TSC_CtApPSH_Rte_CData_CalTimePushInhDelay(void);
IdtTimeDebChargePush  TSC_CtApPSH_Rte_CData_CalTimeDebChargePushHigh(void);
IdtTimeDebChargePush  TSC_CtApPSH_Rte_CData_CalTimeDebChargePushLow(void);
boolean  TSC_CtApPSH_Rte_CData_CalCtrlFlowChargePush(void);

/** Per Instance Memories */
Rte_DT_IdtPSHNvMArray_0 *TSC_CtApPSH_Rte_Pim_NvPSHBlockNeed_MirrorBlock(void);



