/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApMSC.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApMSC.h"
#include "TSC_CtApMSC.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_CtApMSC_Rte_Call_NvMService_AC3_SRBS_NvMSCBlockNeedPowerLatchFlag_WriteBlock(dtRef_const_VOID SrcPtr)
{
  return Rte_Call_NvMService_AC3_SRBS_NvMSCBlockNeedPowerLatchFlag_WriteBlock(SrcPtr);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
{
  return Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(data);
}




Std_ReturnType TSC_CtApMSC_Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempL(OBC_SocketTempL data)
{
  return Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempL(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempN(OBC_SocketTempN data)
{
  return Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempN(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApMSC_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}
Std_ReturnType TSC_CtApMSC_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
{
  return Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(frameNum, byteNum, data);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApMSC_Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data)
{
  return Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(IdtEVSEMaximumPowerLimit *data)
{
  return Rte_Read_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpForceElockCloseMode4_DeForceElockCloseMode4(boolean *data)
{
  return Rte_Read_PpForceElockCloseMode4_DeForceElockCloseMode4(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode *data)
{
  return Rte_Read_PpInputVoltageMode_DeInputVoltageMode(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data)
{
  return Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(BMS_MainConnectorState *data)
{
  return Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(BMS_OnBoardChargerEnable *data)
{
  return Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(BSI_LockedVehicle *data)
{
  return Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(DCLV_Applied_Derating_Factor *data)
{
  return Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_DCLV_Power_DCLV_Power(DCLV_Power *data)
{
  return Rte_Read_PpInt_DCLV_Power_DCLV_Power(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
{
  return Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt *data)
{
  return Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt *data)
{
  return Rte_Read_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(IdtMaxInputACCurrentEVSE *data)
{
  return Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Read_PpPDERATING_OBC_DePDERATING_OBC(IdtPDERATING_OBC *data)
{
  return Rte_Read_PpPDERATING_OBC_DePDERATING_OBC(data);
}




Std_ReturnType TSC_CtApMSC_Rte_Write_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(IdtELockSetPoint data)
{
  return Rte_Write_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Write_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(DCDC_OBCDCDCRTPowerLoad data)
{
  return Rte_Write_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(data);
}

Std_ReturnType TSC_CtApMSC_Rte_Write_PpInt_OBC_PowerMax_OBC_PowerMax(OBC_PowerMax data)
{
  return Rte_Write_PpInt_OBC_PowerMax_OBC_PowerMax(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(VCU_DiagMuxOnPwt *data)
{
  return Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





IdtInputVoltageThreshold  TSC_CtApMSC_Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold(void)
{
  return (IdtInputVoltageThreshold ) Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold();
}

IdtOBC_PowerMaxValue  TSC_CtApMSC_Rte_CData_CalOBC_PowerMaxValue(void)
{
  return (IdtOBC_PowerMaxValue ) Rte_CData_CalOBC_PowerMaxValue();
}
IdtTimeLockDelay  TSC_CtApMSC_Rte_CData_CalTimeLockDelay(void)
{
  return (IdtTimeLockDelay ) Rte_CData_CalTimeLockDelay();
}

     /* CtApMSC */
      /* CtApMSC */

/** Per Instance Memories */
IdtOBC_PowerAvailableControlPilot *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot(void)
{
  return Rte_Pim_PimOBC_PowerAvailableControlPilot();
}
IdtOBC_PowerAvailableHardware *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware(void)
{
  return Rte_Pim_PimOBC_PowerAvailableHardware();
}
uint8 *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag(void)
{
  return Rte_Pim_PimMSC_PowerLatchFlag();
}
IdtOBC_PowerMaxCalculated *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated(void)
{
  return Rte_Pim_PimOBC_PowerMaxCalculated();
}



