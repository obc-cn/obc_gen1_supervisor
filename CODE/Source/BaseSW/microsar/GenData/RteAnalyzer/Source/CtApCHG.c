/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApCHG.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApCHG
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApCHG>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * ComM_ModeType
 *   
 *
 * ComM_UserHandleType
 *   
 *
 * Dcm_NegativeResponseCodeType
 *   
 *
 * Dcm_OpStatusType
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CtApCHG.h"
#include "TSC_CtApCHG.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApCHG_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * BMS_DCRelayVoltage: Integer in interval [0...500]
 * BMS_HighestChargeCurrentAllow: Integer in interval [0...2047]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * BMS_HighestChargeVoltageAllow: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * BMS_OnBoardChargerEnable: Boolean
 * BMS_QuickChargeConnectorState: Boolean
 * BMS_SOC: Integer in interval [0...1023]
 * BMS_Voltage: Integer in interval [0...500]
 * ComM_UserHandleType: Integer in interval [0...65535]
 * EVSE_RTAB_STOP_CHARGE: Boolean
 * IdtBulkSocConf: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtDemandMaxCurrent: Integer in interval [0...255]
 *   Unit: [A], Factor: 1, Offset: 50
 * IdtEVSEMaximumPowerLimit: Integer in interval [0...65535]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtEnergyCapacity: Integer in interval [0...255]
 *   Unit: [kWh], Factor: 1, Offset: 0
 * IdtInputVoltageThreshold: Integer in interval [0...100]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtMaxDemmandCurrent: Integer in interval [0...255]
 *   Unit: [A], Factor: 1, Offset: 50
 * IdtMaxDemmandVoltage: Integer in interval [0...500]
 *   Unit: [V], Factor: 1, Offset: 200
 * IdtMaxDiscoveryCurrent: Integer in interval [0...200]
 *   Unit: [A], Factor: 1, Offset: 50
 * IdtMaxDiscoveryVoltage: Integer in interval [0...500]
 *   Unit: [V], Factor: 1, Offset: 200
 * IdtMaxInputVoltage110V: Integer in interval [0...100]
 *   Unit: [V], Factor: 1, Offset: 100
 * IdtMaxInputVoltage220V: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 100
 * IdtMaxPrechargeCurrent: Integer in interval [0...255]
 *   Unit: [A], Factor: 1, Offset: 50
 * IdtMaxPrechargeVoltage: Integer in interval [0...500]
 *   Unit: [V], Factor: 1, Offset: 200
 * IdtMinInputVoltage110V: Integer in interval [0...100]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtMinInputVoltage220V: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtStopChargeDemmandTimer: Integer in interval [0...600]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTargetPrechargeCurrent: Integer in interval [0...100]
 *   Unit: [A], Factor: 1, Offset: 0
 * IdtThresholdNoAC: Integer in interval [0...100]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtTimeConfirmNoAC: Integer in interval [0...300]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeElockFaultDetected: Integer in interval [0...60]
 *   Unit: [s], Factor: 1, Offset: 0
 * OBC_HighVoltConnectionAllowed: Boolean
 * OBC_OBCStartSt: Boolean
 * OBC_PlugVoltDetection: Boolean
 * PFC_VPH12_Peak_V: Integer in interval [0...1023]
 * PFC_VPH12_RMS_V: Integer in interval [0...1023]
 * PFC_VPH1_Freq_Hz: Integer in interval [0...1023]
 * PFC_VPH23_Peak_V: Integer in interval [0...1023]
 * PFC_VPH23_RMS_V: Integer in interval [0...1023]
 * PFC_VPH2_Freq_Hz: Integer in interval [0...1023]
 * PFC_VPH31_Peak_V: Integer in interval [0...1023]
 * PFC_VPH31_RMS_V: Integer in interval [0...1023]
 * PFC_VPH3_Freq_Hz: Integer in interval [0...1023]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BMS_MainConnectorState: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_contactors_opened (0U)
 *   Cx1_precharge (1U)
 *   Cx2_contactors_closed (2U)
 *   Cx3_Invalid (3U)
 * ComM_ModeType: Enumeration of integer in interval [0...3] with enumerators
 *   COMM_NO_COMMUNICATION (0U)
 *   COMM_SILENT_COMMUNICATION (1U)
 *   COMM_FULL_COMMUNICATION (2U)
 * DCDC_OBCMainContactorReq: Enumeration of integer in interval [0...255] with enumerators
 *   Cx0_Request_to_Open_the_Contactors (0U)
 *   Cx1_Request_to_Close_the_Contactors (1U)
 *   Cx2_No_request (2U)
 *   Cx3_Reserved (3U)
 * DCDC_OBCQuickChargeContactorReq: Enumeration of integer in interval [0...255] with enumerators
 *   Cx0_Request_to_Open_the_Contactors (0U)
 *   Cx1_Request_to_Close_the_Contactors (1U)
 *   Cx2_No_request (2U)
 * DCLV_DCLVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_STANDBY (0U)
 *   Cx1_RUN (1U)
 *   Cx2_ERROR (2U)
 *   Cx3_ALARM (3U)
 *   Cx4_SAFE (4U)
 *   Cx5_DERATED (5U)
 *   Cx6_SHUTDOWN (6U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtELockSetPoint: Enumeration of integer in interval [0...1] with enumerators
 *   ELOCK_SETPOINT_UNLOCKED (0U)
 *   ELOCK_SETPOINT_LOCKED (1U)
 * IdtInputVoltageMode: Enumeration of integer in interval [0...2] with enumerators
 *   IVM_NO_INPUT_DETECTED (0U)
 *   IVM_MONOPHASIC_INPUT (1U)
 *   IVM_TRIPHASIC_INPUT (2U)
 * IdtOutputELockSensor: Enumeration of integer in interval [0...2] with enumerators
 *   ELOCK_LOCKED (0U)
 *   ELOCK_UNLOCKED (1U)
 *   ELOCK_DRIVE_UNDEFINED (2U)
 * OBC_ACRange: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_No_AC_10V_ (0U)
 *   Cx1_110V_85_132V_ (1U)
 *   Cx2_Invalid (2U)
 *   Cx3_220V_170_265V_ (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * OBC_CP_connection_Status: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Invalid_value (0U)
 *   Cx1_CP_invalid_not_connected (1U)
 *   Cx2_CP_valid_full_connected (2U)
 *   Cx3_CP_invalid_half_connected (3U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_no_charging (0U)
 *   Cx1_slow_charging (1U)
 *   Cx2_China_fast_charging (2U)
 *   Cx3_Euro_fast_charging (3U)
 * OBC_Fault: Enumeration of integer in interval [0...255] with enumerators
 *   OBC_FAULT_NO_FAULT (0U)
 *   OBC_FAULT_LEVEL_1 (64U)
 *   OBC_FAULT_LEVEL_2 (128U)
 *   OBC_FAULT_LEVEL_3 (192U)
 * OBC_InputVoltageSt: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_220V_AC (0U)
 *   Cx1_220V_AC_connected (1U)
 *   Cx2_220V_AC_disconnected (2U)
 *   Cx3_Invalid_value (3U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * VCU_ModeEPSRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_OFF (0U)
 *   Cx1_Active_Drive (1U)
 *   Cx2_Discharge (2U)
 *   Cx3_PI_Charge (3U)
 *   Cx4_PI_Balance (4U)
 *   Cx5_PI_Discharge (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtMaxDemmandVoltage Rte_CData_CalMaxDemmandVoltage(void)
 *   IdtMaxDiscoveryVoltage Rte_CData_CalMaxDiscoveryVoltage(void)
 *   IdtMaxPrechargeVoltage Rte_CData_CalMaxPrechargeVoltage(void)
 *   IdtStopChargeDemmandTimer Rte_CData_CalStopChargeDemmandTimer(void)
 *   IdtTimeConfirmNoAC Rte_CData_CalTimeConfirmNoAC(void)
 *   IdtBulkSocConf Rte_CData_CalBulkSocConf(void)
 *   IdtDemandMaxCurrent Rte_CData_CalDemandMaxCurrent(void)
 *   IdtEnergyCapacity Rte_CData_CalEnergyCapacity(void)
 *   IdtMaxDemmandCurrent Rte_CData_CalMaxDemmandCurrent(void)
 *   IdtMaxDiscoveryCurrent Rte_CData_CalMaxDiscoveryCurrent(void)
 *   IdtMaxInputVoltage110V Rte_CData_CalMaxInputVoltage110V(void)
 *   IdtMaxInputVoltage220V Rte_CData_CalMaxInputVoltage220V(void)
 *   IdtMaxPrechargeCurrent Rte_CData_CalMaxPrechargeCurrent(void)
 *   IdtMinInputVoltage110V Rte_CData_CalMinInputVoltage110V(void)
 *   IdtMinInputVoltage220V Rte_CData_CalMinInputVoltage220V(void)
 *   IdtTargetPrechargeCurrent Rte_CData_CalTargetPrechargeCurrent(void)
 *   IdtThresholdNoAC Rte_CData_CalThresholdNoAC(void)
 *   IdtTimeElockFaultDetected Rte_CData_CalTimeElockFaultDetected(void)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtInputVoltageThreshold Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold(void)
 *
 *********************************************************************************************************************/


#define CtApCHG_START_SEC_CODE
#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_V2G_States_Debug_CHGDebugData>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_V2G_States_Debug_CHGDebugData_DCM_E_PENDING
 *   RTE_E_DataServices_V2G_States_Debug_CHGDebugData_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApCHG_CODE) DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

  IdtMaxDemmandVoltage CalMaxDemmandVoltage_data;
  IdtMaxDiscoveryVoltage CalMaxDiscoveryVoltage_data;
  IdtMaxPrechargeVoltage CalMaxPrechargeVoltage_data;
  IdtStopChargeDemmandTimer CalStopChargeDemmandTimer_data;
  IdtTimeConfirmNoAC CalTimeConfirmNoAC_data;
  IdtBulkSocConf CalBulkSocConf_data;
  IdtDemandMaxCurrent CalDemandMaxCurrent_data;
  IdtEnergyCapacity CalEnergyCapacity_data;
  IdtMaxDemmandCurrent CalMaxDemmandCurrent_data;
  IdtMaxDiscoveryCurrent CalMaxDiscoveryCurrent_data;
  IdtMaxInputVoltage110V CalMaxInputVoltage110V_data;
  IdtMaxInputVoltage220V CalMaxInputVoltage220V_data;
  IdtMaxPrechargeCurrent CalMaxPrechargeCurrent_data;
  IdtMinInputVoltage110V CalMinInputVoltage110V_data;
  IdtMinInputVoltage220V CalMinInputVoltage220V_data;
  IdtTargetPrechargeCurrent CalTargetPrechargeCurrent_data;
  IdtThresholdNoAC CalThresholdNoAC_data;
  IdtTimeElockFaultDetected CalTimeElockFaultDetected_data;

  IdtInputVoltageThreshold PiInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxDemmandVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalStopChargeDemmandTimer_data = TSC_CtApCHG_Rte_CData_CalStopChargeDemmandTimer(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfirmNoAC_data = TSC_CtApCHG_Rte_CData_CalTimeConfirmNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBulkSocConf_data = TSC_CtApCHG_Rte_CData_CalBulkSocConf(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDemandMaxCurrent_data = TSC_CtApCHG_Rte_CData_CalDemandMaxCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnergyCapacity_data = TSC_CtApCHG_Rte_CData_CalEnergyCapacity(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDemmandCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTargetPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalTargetPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdNoAC_data = TSC_CtApCHG_Rte_CData_CalThresholdNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeElockFaultDetected_data = TSC_CtApCHG_Rte_CData_CalTimeElockFaultDetected(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PiInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApCHG_Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApCHG_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  return RTE_E_DataServices_V2G_States_Debug_CHGDebugData_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_V2G_States_Debug_CHGDebugData_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_V2G_States_Debug_CHGDebugData>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_V2G_States_Debug_CHGDebugData_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_V2G_States_Debug_CHGDebugData_DCM_E_PENDING
 *   RTE_E_DataServices_V2G_States_Debug_CHGDebugData_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_V2G_States_Debug_CHGDebugData_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApCHG_CODE) DataServices_V2G_States_Debug_CHGDebugData_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_V2G_States_Debug_CHGDebugData_ReadData (returns application error)
 *********************************************************************************************************************/

  IdtMaxDemmandVoltage CalMaxDemmandVoltage_data;
  IdtMaxDiscoveryVoltage CalMaxDiscoveryVoltage_data;
  IdtMaxPrechargeVoltage CalMaxPrechargeVoltage_data;
  IdtStopChargeDemmandTimer CalStopChargeDemmandTimer_data;
  IdtTimeConfirmNoAC CalTimeConfirmNoAC_data;
  IdtBulkSocConf CalBulkSocConf_data;
  IdtDemandMaxCurrent CalDemandMaxCurrent_data;
  IdtEnergyCapacity CalEnergyCapacity_data;
  IdtMaxDemmandCurrent CalMaxDemmandCurrent_data;
  IdtMaxDiscoveryCurrent CalMaxDiscoveryCurrent_data;
  IdtMaxInputVoltage110V CalMaxInputVoltage110V_data;
  IdtMaxInputVoltage220V CalMaxInputVoltage220V_data;
  IdtMaxPrechargeCurrent CalMaxPrechargeCurrent_data;
  IdtMinInputVoltage110V CalMinInputVoltage110V_data;
  IdtMinInputVoltage220V CalMinInputVoltage220V_data;
  IdtTargetPrechargeCurrent CalTargetPrechargeCurrent_data;
  IdtThresholdNoAC CalThresholdNoAC_data;
  IdtTimeElockFaultDetected CalTimeElockFaultDetected_data;

  IdtInputVoltageThreshold PiInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxDemmandVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalStopChargeDemmandTimer_data = TSC_CtApCHG_Rte_CData_CalStopChargeDemmandTimer(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfirmNoAC_data = TSC_CtApCHG_Rte_CData_CalTimeConfirmNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBulkSocConf_data = TSC_CtApCHG_Rte_CData_CalBulkSocConf(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDemandMaxCurrent_data = TSC_CtApCHG_Rte_CData_CalDemandMaxCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnergyCapacity_data = TSC_CtApCHG_Rte_CData_CalEnergyCapacity(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDemmandCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTargetPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalTargetPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdNoAC_data = TSC_CtApCHG_Rte_CData_CalThresholdNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeElockFaultDetected_data = TSC_CtApCHG_Rte_CData_CalTimeElockFaultDetected(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PiInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApCHG_Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_DataServices_V2G_States_Debug_CHGDebugData_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApCHG_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCHG_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApCHG_CODE) RCtApCHG_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCHG_init
 *********************************************************************************************************************/

  IdtMaxDemmandVoltage CalMaxDemmandVoltage_data;
  IdtMaxDiscoveryVoltage CalMaxDiscoveryVoltage_data;
  IdtMaxPrechargeVoltage CalMaxPrechargeVoltage_data;
  IdtStopChargeDemmandTimer CalStopChargeDemmandTimer_data;
  IdtTimeConfirmNoAC CalTimeConfirmNoAC_data;
  IdtBulkSocConf CalBulkSocConf_data;
  IdtDemandMaxCurrent CalDemandMaxCurrent_data;
  IdtEnergyCapacity CalEnergyCapacity_data;
  IdtMaxDemmandCurrent CalMaxDemmandCurrent_data;
  IdtMaxDiscoveryCurrent CalMaxDiscoveryCurrent_data;
  IdtMaxInputVoltage110V CalMaxInputVoltage110V_data;
  IdtMaxInputVoltage220V CalMaxInputVoltage220V_data;
  IdtMaxPrechargeCurrent CalMaxPrechargeCurrent_data;
  IdtMinInputVoltage110V CalMinInputVoltage110V_data;
  IdtMinInputVoltage220V CalMinInputVoltage220V_data;
  IdtTargetPrechargeCurrent CalTargetPrechargeCurrent_data;
  IdtThresholdNoAC CalThresholdNoAC_data;
  IdtTimeElockFaultDetected CalTimeElockFaultDetected_data;

  IdtInputVoltageThreshold PiInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxDemmandVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalStopChargeDemmandTimer_data = TSC_CtApCHG_Rte_CData_CalStopChargeDemmandTimer(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfirmNoAC_data = TSC_CtApCHG_Rte_CData_CalTimeConfirmNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBulkSocConf_data = TSC_CtApCHG_Rte_CData_CalBulkSocConf(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDemandMaxCurrent_data = TSC_CtApCHG_Rte_CData_CalDemandMaxCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnergyCapacity_data = TSC_CtApCHG_Rte_CData_CalEnergyCapacity(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDemmandCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTargetPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalTargetPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdNoAC_data = TSC_CtApCHG_Rte_CData_CalThresholdNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeElockFaultDetected_data = TSC_CtApCHG_Rte_CData_CalTimeElockFaultDetected(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PiInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApCHG_Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApCHG_task10msA
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpDiagnosticNoACInput_DeDiagnosticNoACInput(boolean data)
 *   Std_ReturnType Rte_Write_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(IdtInputVoltageMode data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_ACRange_Delayed_OBC_ACRange(OBC_ACRange data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCHG_task10msA_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApCHG_CODE) RCtApCHG_task10msA(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCHG_task10msA
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtMaxDemmandVoltage CalMaxDemmandVoltage_data;
  IdtMaxDiscoveryVoltage CalMaxDiscoveryVoltage_data;
  IdtMaxPrechargeVoltage CalMaxPrechargeVoltage_data;
  IdtStopChargeDemmandTimer CalStopChargeDemmandTimer_data;
  IdtTimeConfirmNoAC CalTimeConfirmNoAC_data;
  IdtBulkSocConf CalBulkSocConf_data;
  IdtDemandMaxCurrent CalDemandMaxCurrent_data;
  IdtEnergyCapacity CalEnergyCapacity_data;
  IdtMaxDemmandCurrent CalMaxDemmandCurrent_data;
  IdtMaxDiscoveryCurrent CalMaxDiscoveryCurrent_data;
  IdtMaxInputVoltage110V CalMaxInputVoltage110V_data;
  IdtMaxInputVoltage220V CalMaxInputVoltage220V_data;
  IdtMaxPrechargeCurrent CalMaxPrechargeCurrent_data;
  IdtMinInputVoltage110V CalMinInputVoltage110V_data;
  IdtMinInputVoltage220V CalMinInputVoltage220V_data;
  IdtTargetPrechargeCurrent CalTargetPrechargeCurrent_data;
  IdtThresholdNoAC CalThresholdNoAC_data;
  IdtTimeElockFaultDetected CalTimeElockFaultDetected_data;

  IdtInputVoltageThreshold PiInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxDemmandVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalStopChargeDemmandTimer_data = TSC_CtApCHG_Rte_CData_CalStopChargeDemmandTimer(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfirmNoAC_data = TSC_CtApCHG_Rte_CData_CalTimeConfirmNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBulkSocConf_data = TSC_CtApCHG_Rte_CData_CalBulkSocConf(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDemandMaxCurrent_data = TSC_CtApCHG_Rte_CData_CalDemandMaxCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnergyCapacity_data = TSC_CtApCHG_Rte_CData_CalEnergyCapacity(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDemmandCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTargetPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalTargetPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdNoAC_data = TSC_CtApCHG_Rte_CData_CalThresholdNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeElockFaultDetected_data = TSC_CtApCHG_Rte_CData_CalTimeElockFaultDetected(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PiInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApCHG_Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApCHG_Rte_Write_PpDiagnosticNoACInput_DeDiagnosticNoACInput(Rte_InitValue_PpDiagnosticNoACInput_DeDiagnosticNoACInput); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(Rte_InitValue_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpInt_OBC_ACRange_Delayed_OBC_ACRange(Rte_InitValue_PpInt_OBC_ACRange_Delayed_OBC_ACRange); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpInt_OBC_Status_Delayed_OBC_Status(Rte_InitValue_PpInt_OBC_Status_Delayed_OBC_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApCHG_task10msB
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data)
 *   Std_ReturnType Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(IdtELockSetPoint *data)
 *   Std_ReturnType Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(boolean *data)
 *   Std_ReturnType Rte_Read_PpFreqOutRange_DeFreqOutRange(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(BMS_HighestChargeCurrentAllow *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(BMS_HighestChargeVoltageAllow *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(BMS_MainConnectorState *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(BMS_OnBoardChargerEnable *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(BMS_QuickChargeConnectorState *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_SOC_BMS_SOC(BMS_SOC *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_Voltage_BMS_Voltage(BMS_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(OBC_CP_connection_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Fault_OBC_Fault(OBC_Fault *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(PFC_VPH1_Freq_Hz *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(PFC_VPH2_Freq_Hz *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(PFC_VPH3_Freq_Hz *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(PFC_VPH23_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(PFC_VPH31_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(PFC_VPH23_RMS_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(PFC_VPH31_RMS_V *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
 *   Std_ReturnType Rte_Read_PpOBCDerating_DeOBCDerating(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data)
 *   Std_ReturnType Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpPlantModeState_DePlantModeState(boolean *data)
 *   Std_ReturnType Rte_Read_PpTempFaults_DeTempDC1FaultSCG(boolean *data)
 *   Std_ReturnType Rte_Read_PpTempFaults_DeTempDC1FaultSCP(boolean *data)
 *   Std_ReturnType Rte_Read_PpTempFaults_DeTempDC2FaultSCG(boolean *data)
 *   Std_ReturnType Rte_Read_PpTempFaults_DeTempDC2FaultSCP(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpControlPilotFreqError_DeControlPilotFreqError(boolean data)
 *   Std_ReturnType Rte_Write_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(IdtEVSEMaximumPowerLimit data)
 *   Std_ReturnType Rte_Write_PpElockFaultDetected_DeElockFaultDetected(boolean data)
 *   Std_ReturnType Rte_Write_PpForceElockCloseMode4_DeForceElockCloseMode4(boolean data)
 *   Std_ReturnType Rte_Write_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq(DCDC_OBCMainContactorReq data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq(DCDC_OBCQuickChargeContactorReq data)
 *   Std_ReturnType Rte_Write_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(EVSE_RTAB_STOP_CHARGE data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_ACRange_OBC_ACRange(OBC_ACRange data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_Status_OBC_Status(OBC_Status data)
 *   Std_ReturnType Rte_Write_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress(boolean data)
 *   Std_ReturnType Rte_Write_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpRequestHWStopOBC_DeRequestHWStopOBC(boolean data)
 *   Std_ReturnType Rte_Write_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpStopConditions_DeStopConditions(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_PpComMUserNeed_EthUserRequest_RequestComMode(ComM_ModeType ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_MODE_LIMITATION, RTE_E_ComM_UserRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCHG_task10msB_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApCHG_CODE) RCtApCHG_task10msB(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCHG_task10msB
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  boolean Read_PpActiveDischargeRequest_DeActiveDischargeRequest;
  IdtELockSetPoint Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode;
  boolean Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop;
  boolean Read_PpFreqOutRange_DeFreqOutRange;
  BMS_DCRelayVoltage Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage;
  BMS_HighestChargeCurrentAllow Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow;
  BMS_HighestChargeVoltageAllow Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow;
  BMS_MainConnectorState Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState;
  BMS_OnBoardChargerEnable Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable;
  BMS_QuickChargeConnectorState Read_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState;
  BMS_SOC Read_PpInt_BMS_SOC_BMS_SOC;
  BMS_Voltage Read_PpInt_BMS_Voltage_BMS_Voltage;
  DCLV_DCLVStatus Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus;
  OBC_CP_connection_Status Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status;
  OBC_ChargingConnectionConfirmati Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
  OBC_ChargingMode Read_PpInt_OBC_ChargingMode_OBC_ChargingMode;
  OBC_Fault Read_PpInt_OBC_Fault_OBC_Fault;
  OBC_HighVoltConnectionAllowed Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed;
  OBC_PlugVoltDetection Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection;
  PFC_VPH1_Freq_Hz Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz;
  PFC_VPH2_Freq_Hz Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz;
  PFC_VPH3_Freq_Hz Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz;
  PFC_VPH12_Peak_V Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V;
  PFC_VPH23_Peak_V Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V;
  PFC_VPH31_Peak_V Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V;
  PFC_VPH12_RMS_V Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V;
  PFC_VPH23_RMS_V Read_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V;
  PFC_VPH31_RMS_V Read_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V;
  VCU_ModeEPSRequest Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest;
  boolean Read_PpOBCDerating_DeOBCDerating;
  IdtOutputELockSensor Read_PpOutputELockSensor_DeOutputELockSensor;
  boolean Read_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue;
  boolean Read_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue;
  boolean Read_PpPlantModeState_DePlantModeState;
  boolean Read_PpTempFaults_DeTempDC1FaultSCG;
  boolean Read_PpTempFaults_DeTempDC1FaultSCP;
  boolean Read_PpTempFaults_DeTempDC2FaultSCG;
  boolean Read_PpTempFaults_DeTempDC2FaultSCP;

  IdtMaxDemmandVoltage CalMaxDemmandVoltage_data;
  IdtMaxDiscoveryVoltage CalMaxDiscoveryVoltage_data;
  IdtMaxPrechargeVoltage CalMaxPrechargeVoltage_data;
  IdtStopChargeDemmandTimer CalStopChargeDemmandTimer_data;
  IdtTimeConfirmNoAC CalTimeConfirmNoAC_data;
  IdtBulkSocConf CalBulkSocConf_data;
  IdtDemandMaxCurrent CalDemandMaxCurrent_data;
  IdtEnergyCapacity CalEnergyCapacity_data;
  IdtMaxDemmandCurrent CalMaxDemmandCurrent_data;
  IdtMaxDiscoveryCurrent CalMaxDiscoveryCurrent_data;
  IdtMaxInputVoltage110V CalMaxInputVoltage110V_data;
  IdtMaxInputVoltage220V CalMaxInputVoltage220V_data;
  IdtMaxPrechargeCurrent CalMaxPrechargeCurrent_data;
  IdtMinInputVoltage110V CalMinInputVoltage110V_data;
  IdtMinInputVoltage220V CalMinInputVoltage220V_data;
  IdtTargetPrechargeCurrent CalTargetPrechargeCurrent_data;
  IdtThresholdNoAC CalThresholdNoAC_data;
  IdtTimeElockFaultDetected CalTimeElockFaultDetected_data;

  IdtInputVoltageThreshold PiInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxDemmandVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalStopChargeDemmandTimer_data = TSC_CtApCHG_Rte_CData_CalStopChargeDemmandTimer(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfirmNoAC_data = TSC_CtApCHG_Rte_CData_CalTimeConfirmNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBulkSocConf_data = TSC_CtApCHG_Rte_CData_CalBulkSocConf(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDemandMaxCurrent_data = TSC_CtApCHG_Rte_CData_CalDemandMaxCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnergyCapacity_data = TSC_CtApCHG_Rte_CData_CalEnergyCapacity(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDemmandCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTargetPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalTargetPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdNoAC_data = TSC_CtApCHG_Rte_CData_CalThresholdNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeElockFaultDetected_data = TSC_CtApCHG_Rte_CData_CalTimeElockFaultDetected(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PiInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApCHG_Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApCHG_Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(&Read_PpActiveDischargeRequest_DeActiveDischargeRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(&Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(&Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpFreqOutRange_DeFreqOutRange(&Read_PpFreqOutRange_DeFreqOutRange); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(&Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(&Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(&Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(&Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(&Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(&Read_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_BMS_SOC_BMS_SOC(&Read_PpInt_BMS_SOC_BMS_SOC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_BMS_Voltage_BMS_Voltage(&Read_PpInt_BMS_Voltage_BMS_Voltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(&Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(&Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&Read_PpInt_OBC_ChargingMode_OBC_ChargingMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_OBC_Fault_OBC_Fault(&Read_PpInt_OBC_Fault_OBC_Fault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(&Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(&Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(&Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(&Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(&Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(&Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(&Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(&Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(&Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(&Read_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(&Read_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(&Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpOBCDerating_DeOBCDerating(&Read_PpOBCDerating_DeOBCDerating); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&Read_PpOutputELockSensor_DeOutputELockSensor); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue(&Read_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue(&Read_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpPlantModeState_DePlantModeState(&Read_PpPlantModeState_DePlantModeState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpTempFaults_DeTempDC1FaultSCG(&Read_PpTempFaults_DeTempDC1FaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpTempFaults_DeTempDC1FaultSCP(&Read_PpTempFaults_DeTempDC1FaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpTempFaults_DeTempDC2FaultSCG(&Read_PpTempFaults_DeTempDC2FaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Read_PpTempFaults_DeTempDC2FaultSCP(&Read_PpTempFaults_DeTempDC2FaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpControlPilotFreqError_DeControlPilotFreqError(Rte_InitValue_PpControlPilotFreqError_DeControlPilotFreqError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(Rte_InitValue_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpElockFaultDetected_DeElockFaultDetected(Rte_InitValue_PpElockFaultDetected_DeElockFaultDetected); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpForceElockCloseMode4_DeForceElockCloseMode4(Rte_InitValue_PpForceElockCloseMode4_DeForceElockCloseMode4); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpInputVoltageMode_DeInputVoltageMode(Rte_InitValue_PpInputVoltageMode_DeInputVoltageMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq(Rte_InitValue_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq(Rte_InitValue_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(Rte_InitValue_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpInt_OBC_ACRange_OBC_ACRange(Rte_InitValue_PpInt_OBC_ACRange_OBC_ACRange); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(Rte_InitValue_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(Rte_InitValue_PpInt_OBC_OBCStartSt_OBC_OBCStartSt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpInt_OBC_Status_OBC_Status(Rte_InitValue_PpInt_OBC_Status_OBC_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress(Rte_InitValue_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(Rte_InitValue_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpRequestHWStopOBC_DeRequestHWStopOBC(Rte_InitValue_PpRequestHWStopOBC_DeRequestHWStopOBC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue(Rte_InitValue_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Write_PpStopConditions_DeStopConditions(Rte_InitValue_PpStopConditions_DeStopConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U, 0U, 0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApCHG_Rte_Call_PpComMUserNeed_EthUserRequest_RequestComMode(0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_ComM_UserRequest_E_MODE_LIMITATION:
      fct_error = TRUE;
      break;
    case RTE_E_ComM_UserRequest_E_NOT_OK:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_V2G_States_Debug_CHGInternalState>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_V2G_States_Debug_CHGInternalState_DCM_E_PENDING
 *   RTE_E_DataServices_V2G_States_Debug_CHGInternalState_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApCHG_CODE) RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

  IdtMaxDemmandVoltage CalMaxDemmandVoltage_data;
  IdtMaxDiscoveryVoltage CalMaxDiscoveryVoltage_data;
  IdtMaxPrechargeVoltage CalMaxPrechargeVoltage_data;
  IdtStopChargeDemmandTimer CalStopChargeDemmandTimer_data;
  IdtTimeConfirmNoAC CalTimeConfirmNoAC_data;
  IdtBulkSocConf CalBulkSocConf_data;
  IdtDemandMaxCurrent CalDemandMaxCurrent_data;
  IdtEnergyCapacity CalEnergyCapacity_data;
  IdtMaxDemmandCurrent CalMaxDemmandCurrent_data;
  IdtMaxDiscoveryCurrent CalMaxDiscoveryCurrent_data;
  IdtMaxInputVoltage110V CalMaxInputVoltage110V_data;
  IdtMaxInputVoltage220V CalMaxInputVoltage220V_data;
  IdtMaxPrechargeCurrent CalMaxPrechargeCurrent_data;
  IdtMinInputVoltage110V CalMinInputVoltage110V_data;
  IdtMinInputVoltage220V CalMinInputVoltage220V_data;
  IdtTargetPrechargeCurrent CalTargetPrechargeCurrent_data;
  IdtThresholdNoAC CalThresholdNoAC_data;
  IdtTimeElockFaultDetected CalTimeElockFaultDetected_data;

  IdtInputVoltageThreshold PiInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxDemmandVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalStopChargeDemmandTimer_data = TSC_CtApCHG_Rte_CData_CalStopChargeDemmandTimer(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfirmNoAC_data = TSC_CtApCHG_Rte_CData_CalTimeConfirmNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBulkSocConf_data = TSC_CtApCHG_Rte_CData_CalBulkSocConf(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDemandMaxCurrent_data = TSC_CtApCHG_Rte_CData_CalDemandMaxCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnergyCapacity_data = TSC_CtApCHG_Rte_CData_CalEnergyCapacity(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDemmandCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTargetPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalTargetPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdNoAC_data = TSC_CtApCHG_Rte_CData_CalThresholdNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeElockFaultDetected_data = TSC_CtApCHG_Rte_CData_CalTimeElockFaultDetected(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PiInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApCHG_Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_DataServices_V2G_States_Debug_CHGInternalState_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_V2G_States_Debug_CHGInternalState_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_V2G_States_Debug_CHGInternalState>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_V2G_States_Debug_CHGInternalState_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_V2G_States_Debug_CHGInternalState_DCM_E_PENDING
 *   RTE_E_DataServices_V2G_States_Debug_CHGInternalState_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_CHGInternalState_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApCHG_CODE) RDataServices_V2G_States_Debug_CHGInternalState_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_CHGInternalState_ReadData (returns application error)
 *********************************************************************************************************************/

  IdtMaxDemmandVoltage CalMaxDemmandVoltage_data;
  IdtMaxDiscoveryVoltage CalMaxDiscoveryVoltage_data;
  IdtMaxPrechargeVoltage CalMaxPrechargeVoltage_data;
  IdtStopChargeDemmandTimer CalStopChargeDemmandTimer_data;
  IdtTimeConfirmNoAC CalTimeConfirmNoAC_data;
  IdtBulkSocConf CalBulkSocConf_data;
  IdtDemandMaxCurrent CalDemandMaxCurrent_data;
  IdtEnergyCapacity CalEnergyCapacity_data;
  IdtMaxDemmandCurrent CalMaxDemmandCurrent_data;
  IdtMaxDiscoveryCurrent CalMaxDiscoveryCurrent_data;
  IdtMaxInputVoltage110V CalMaxInputVoltage110V_data;
  IdtMaxInputVoltage220V CalMaxInputVoltage220V_data;
  IdtMaxPrechargeCurrent CalMaxPrechargeCurrent_data;
  IdtMinInputVoltage110V CalMinInputVoltage110V_data;
  IdtMinInputVoltage220V CalMinInputVoltage220V_data;
  IdtTargetPrechargeCurrent CalTargetPrechargeCurrent_data;
  IdtThresholdNoAC CalThresholdNoAC_data;
  IdtTimeElockFaultDetected CalTimeElockFaultDetected_data;

  IdtInputVoltageThreshold PiInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxDemmandVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalStopChargeDemmandTimer_data = TSC_CtApCHG_Rte_CData_CalStopChargeDemmandTimer(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfirmNoAC_data = TSC_CtApCHG_Rte_CData_CalTimeConfirmNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBulkSocConf_data = TSC_CtApCHG_Rte_CData_CalBulkSocConf(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDemandMaxCurrent_data = TSC_CtApCHG_Rte_CData_CalDemandMaxCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnergyCapacity_data = TSC_CtApCHG_Rte_CData_CalEnergyCapacity(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDemmandCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTargetPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalTargetPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdNoAC_data = TSC_CtApCHG_Rte_CData_CalThresholdNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeElockFaultDetected_data = TSC_CtApCHG_Rte_CData_CalTimeElockFaultDetected(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PiInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApCHG_Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_DataServices_V2G_States_Debug_CHGInternalState_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_V2G_States_Debug_SCCStateMachine>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_DCM_E_PENDING
 *   RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApCHG_CODE) RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

  IdtMaxDemmandVoltage CalMaxDemmandVoltage_data;
  IdtMaxDiscoveryVoltage CalMaxDiscoveryVoltage_data;
  IdtMaxPrechargeVoltage CalMaxPrechargeVoltage_data;
  IdtStopChargeDemmandTimer CalStopChargeDemmandTimer_data;
  IdtTimeConfirmNoAC CalTimeConfirmNoAC_data;
  IdtBulkSocConf CalBulkSocConf_data;
  IdtDemandMaxCurrent CalDemandMaxCurrent_data;
  IdtEnergyCapacity CalEnergyCapacity_data;
  IdtMaxDemmandCurrent CalMaxDemmandCurrent_data;
  IdtMaxDiscoveryCurrent CalMaxDiscoveryCurrent_data;
  IdtMaxInputVoltage110V CalMaxInputVoltage110V_data;
  IdtMaxInputVoltage220V CalMaxInputVoltage220V_data;
  IdtMaxPrechargeCurrent CalMaxPrechargeCurrent_data;
  IdtMinInputVoltage110V CalMinInputVoltage110V_data;
  IdtMinInputVoltage220V CalMinInputVoltage220V_data;
  IdtTargetPrechargeCurrent CalTargetPrechargeCurrent_data;
  IdtThresholdNoAC CalThresholdNoAC_data;
  IdtTimeElockFaultDetected CalTimeElockFaultDetected_data;

  IdtInputVoltageThreshold PiInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxDemmandVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalStopChargeDemmandTimer_data = TSC_CtApCHG_Rte_CData_CalStopChargeDemmandTimer(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfirmNoAC_data = TSC_CtApCHG_Rte_CData_CalTimeConfirmNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBulkSocConf_data = TSC_CtApCHG_Rte_CData_CalBulkSocConf(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDemandMaxCurrent_data = TSC_CtApCHG_Rte_CData_CalDemandMaxCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnergyCapacity_data = TSC_CtApCHG_Rte_CData_CalEnergyCapacity(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDemmandCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTargetPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalTargetPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdNoAC_data = TSC_CtApCHG_Rte_CData_CalThresholdNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeElockFaultDetected_data = TSC_CtApCHG_Rte_CData_CalTimeElockFaultDetected(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PiInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApCHG_Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_V2G_States_Debug_SCCStateMachine_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_V2G_States_Debug_SCCStateMachine>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_V2G_States_Debug_SCCStateMachine_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_DCM_E_PENDING
 *   RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_SCCStateMachine_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApCHG_CODE) RDataServices_V2G_States_Debug_SCCStateMachine_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_SCCStateMachine_ReadData (returns application error)
 *********************************************************************************************************************/

  IdtMaxDemmandVoltage CalMaxDemmandVoltage_data;
  IdtMaxDiscoveryVoltage CalMaxDiscoveryVoltage_data;
  IdtMaxPrechargeVoltage CalMaxPrechargeVoltage_data;
  IdtStopChargeDemmandTimer CalStopChargeDemmandTimer_data;
  IdtTimeConfirmNoAC CalTimeConfirmNoAC_data;
  IdtBulkSocConf CalBulkSocConf_data;
  IdtDemandMaxCurrent CalDemandMaxCurrent_data;
  IdtEnergyCapacity CalEnergyCapacity_data;
  IdtMaxDemmandCurrent CalMaxDemmandCurrent_data;
  IdtMaxDiscoveryCurrent CalMaxDiscoveryCurrent_data;
  IdtMaxInputVoltage110V CalMaxInputVoltage110V_data;
  IdtMaxInputVoltage220V CalMaxInputVoltage220V_data;
  IdtMaxPrechargeCurrent CalMaxPrechargeCurrent_data;
  IdtMinInputVoltage110V CalMinInputVoltage110V_data;
  IdtMinInputVoltage220V CalMinInputVoltage220V_data;
  IdtTargetPrechargeCurrent CalTargetPrechargeCurrent_data;
  IdtThresholdNoAC CalThresholdNoAC_data;
  IdtTimeElockFaultDetected CalTimeElockFaultDetected_data;

  IdtInputVoltageThreshold PiInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxDemmandVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeVoltage_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalStopChargeDemmandTimer_data = TSC_CtApCHG_Rte_CData_CalStopChargeDemmandTimer(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeConfirmNoAC_data = TSC_CtApCHG_Rte_CData_CalTimeConfirmNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBulkSocConf_data = TSC_CtApCHG_Rte_CData_CalBulkSocConf(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDemandMaxCurrent_data = TSC_CtApCHG_Rte_CData_CalDemandMaxCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnergyCapacity_data = TSC_CtApCHG_Rte_CData_CalEnergyCapacity(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDemmandCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDemmandCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDiscoveryCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxDiscoveryCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMaxInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalMaxPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage110V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage110V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinInputVoltage220V_data = TSC_CtApCHG_Rte_CData_CalMinInputVoltage220V(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTargetPrechargeCurrent_data = TSC_CtApCHG_Rte_CData_CalTargetPrechargeCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalThresholdNoAC_data = TSC_CtApCHG_Rte_CData_CalThresholdNoAC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeElockFaultDetected_data = TSC_CtApCHG_Rte_CData_CalTimeElockFaultDetected(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PiInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApCHG_Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApCHG_STOP_SEC_CODE
#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApCHG_TestDefines(void)
{
  /* Enumeration Data Types */

  BMS_MainConnectorState Test_BMS_MainConnectorState_V_1 = Cx0_contactors_opened;
  BMS_MainConnectorState Test_BMS_MainConnectorState_V_2 = Cx1_precharge;
  BMS_MainConnectorState Test_BMS_MainConnectorState_V_3 = Cx2_contactors_closed;
  BMS_MainConnectorState Test_BMS_MainConnectorState_V_4 = Cx3_Invalid;

  ComM_ModeType Test_ComM_ModeType_V_1 = COMM_NO_COMMUNICATION;
  ComM_ModeType Test_ComM_ModeType_V_2 = COMM_SILENT_COMMUNICATION;
  ComM_ModeType Test_ComM_ModeType_V_3 = COMM_FULL_COMMUNICATION;

  DCDC_OBCMainContactorReq Test_DCDC_OBCMainContactorReq_V_1 = Cx0_Request_to_Open_the_Contactors;
  DCDC_OBCMainContactorReq Test_DCDC_OBCMainContactorReq_V_2 = Cx1_Request_to_Close_the_Contactors;
  DCDC_OBCMainContactorReq Test_DCDC_OBCMainContactorReq_V_3 = Cx2_No_request;
  DCDC_OBCMainContactorReq Test_DCDC_OBCMainContactorReq_V_4 = Cx3_Reserved;

  DCDC_OBCQuickChargeContactorReq Test_DCDC_OBCQuickChargeContactorReq_V_1 = Cx0_Request_to_Open_the_Contactors;
  DCDC_OBCQuickChargeContactorReq Test_DCDC_OBCQuickChargeContactorReq_V_2 = Cx1_Request_to_Close_the_Contactors;
  DCDC_OBCQuickChargeContactorReq Test_DCDC_OBCQuickChargeContactorReq_V_3 = Cx2_No_request;

  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_1 = Cx0_STANDBY;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_2 = Cx1_RUN;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_3 = Cx2_ERROR;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_4 = Cx3_ALARM;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_5 = Cx4_SAFE;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_6 = Cx5_DERATED;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_7 = Cx6_SHUTDOWN;

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_42 = DCM_E_VMSCNC_0;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_43 = DCM_E_VMSCNC_1;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_44 = DCM_E_VMSCNC_2;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_45 = DCM_E_VMSCNC_3;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_46 = DCM_E_VMSCNC_4;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_47 = DCM_E_VMSCNC_5;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_48 = DCM_E_VMSCNC_6;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_49 = DCM_E_VMSCNC_7;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_50 = DCM_E_VMSCNC_8;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_51 = DCM_E_VMSCNC_9;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_52 = DCM_E_VMSCNC_A;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_53 = DCM_E_VMSCNC_B;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_54 = DCM_E_VMSCNC_C;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_55 = DCM_E_VMSCNC_D;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_56 = DCM_E_VMSCNC_E;

  Dcm_OpStatusType Test_Dcm_OpStatusType_V_1 = DCM_INITIAL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_2 = DCM_PENDING;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_3 = DCM_CANCEL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_4 = DCM_FORCE_RCRRP_OK;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_5 = DCM_FORCE_RCRRP_NOT_OK;

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;

  IdtELockSetPoint Test_IdtELockSetPoint_V_1 = ELOCK_SETPOINT_UNLOCKED;
  IdtELockSetPoint Test_IdtELockSetPoint_V_2 = ELOCK_SETPOINT_LOCKED;

  IdtInputVoltageMode Test_IdtInputVoltageMode_V_1 = IVM_NO_INPUT_DETECTED;
  IdtInputVoltageMode Test_IdtInputVoltageMode_V_2 = IVM_MONOPHASIC_INPUT;
  IdtInputVoltageMode Test_IdtInputVoltageMode_V_3 = IVM_TRIPHASIC_INPUT;

  IdtOutputELockSensor Test_IdtOutputELockSensor_V_1 = ELOCK_LOCKED;
  IdtOutputELockSensor Test_IdtOutputELockSensor_V_2 = ELOCK_UNLOCKED;
  IdtOutputELockSensor Test_IdtOutputELockSensor_V_3 = ELOCK_DRIVE_UNDEFINED;

  OBC_ACRange Test_OBC_ACRange_V_1 = Cx0_No_AC_10V_;
  OBC_ACRange Test_OBC_ACRange_V_2 = Cx1_110V_85_132V_;
  OBC_ACRange Test_OBC_ACRange_V_3 = Cx2_Invalid;
  OBC_ACRange Test_OBC_ACRange_V_4 = Cx3_220V_170_265V_;
  OBC_ACRange Test_OBC_ACRange_V_5 = Cx4_Reserved;
  OBC_ACRange Test_OBC_ACRange_V_6 = Cx5_Reserved;
  OBC_ACRange Test_OBC_ACRange_V_7 = Cx6_Reserved;
  OBC_ACRange Test_OBC_ACRange_V_8 = Cx7_Reserved;

  OBC_CP_connection_Status Test_OBC_CP_connection_Status_V_1 = Cx0_Invalid_value;
  OBC_CP_connection_Status Test_OBC_CP_connection_Status_V_2 = Cx1_CP_invalid_not_connected;
  OBC_CP_connection_Status Test_OBC_CP_connection_Status_V_3 = Cx2_CP_valid_full_connected;
  OBC_CP_connection_Status Test_OBC_CP_connection_Status_V_4 = Cx3_CP_invalid_half_connected;

  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_1 = Cx0_invalid_value;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_2 = Cx1_not_connected;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_3 = Cx2_full_connected;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_4 = Cx3_CC_is_half_connected;

  OBC_ChargingMode Test_OBC_ChargingMode_V_1 = Cx0_no_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_2 = Cx1_slow_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_3 = Cx2_China_fast_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_4 = Cx3_Euro_fast_charging;

  OBC_Fault Test_OBC_Fault_V_1 = OBC_FAULT_NO_FAULT;
  OBC_Fault Test_OBC_Fault_V_2 = OBC_FAULT_LEVEL_1;
  OBC_Fault Test_OBC_Fault_V_3 = OBC_FAULT_LEVEL_2;
  OBC_Fault Test_OBC_Fault_V_4 = OBC_FAULT_LEVEL_3;

  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_1 = Cx0_No_220V_AC;
  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_2 = Cx1_220V_AC_connected;
  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_3 = Cx2_220V_AC_disconnected;
  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_4 = Cx3_Invalid_value;

  OBC_Status Test_OBC_Status_V_1 = Cx0_off_mode;
  OBC_Status Test_OBC_Status_V_2 = Cx1_Init_mode;
  OBC_Status Test_OBC_Status_V_3 = Cx2_standby_mode;
  OBC_Status Test_OBC_Status_V_4 = Cx3_conversion_working_;
  OBC_Status Test_OBC_Status_V_5 = Cx4_error_mode;
  OBC_Status Test_OBC_Status_V_6 = Cx5_degradation_mode;
  OBC_Status Test_OBC_Status_V_7 = Cx6_reserved;
  OBC_Status Test_OBC_Status_V_8 = Cx7_invalid;

  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_1 = Cx0_OFF;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_2 = Cx1_Active_Drive;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_3 = Cx2_Discharge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_4 = Cx3_PI_Charge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_5 = Cx4_PI_Balance;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_6 = Cx5_PI_Discharge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_7 = Cx6_Reserved;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_8 = Cx7_Reserved;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
