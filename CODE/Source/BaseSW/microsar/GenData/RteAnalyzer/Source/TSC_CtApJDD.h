/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApJDD.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApJDD_Rte_Read_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(boolean *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_PpBusOffCANFault_DeBusOffCANFault(boolean *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup(boolean *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(boolean *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(DATA_ACQ_JDD_BSI_2 *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD(VCU_CDEAccJDD *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD(VCU_CDEApcJDD *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct(VCU_CompteurRazGct *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_PpInt_VCU_CptTemporel_VCU_CptTemporel(VCU_CptTemporel *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev(VCU_EtatPrincipSev *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec(VCU_EtatReseauElec *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_PpInt_VCU_Kilometrage_VCU_Kilometrage(VCU_Kilometrage *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(VCU_PosShuntJDD *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_Pp_VCU_CompteurRazGct_VCU_CompteurRazGct(VCU_CompteurRazGct *data);
Std_ReturnType TSC_CtApJDD_Rte_Read_Pp_VCU_CptTemporel_VCU_CptTemporel(VCU_CptTemporel *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(NEW_JDD_OBC_DCDC_BYTE_0 data);
Std_ReturnType TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(NEW_JDD_OBC_DCDC_BYTE_1 data);
Std_ReturnType TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(NEW_JDD_OBC_DCDC_BYTE_2 data);
Std_ReturnType TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(NEW_JDD_OBC_DCDC_BYTE_3 data);
Std_ReturnType TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(NEW_JDD_OBC_DCDC_BYTE_4 data);
Std_ReturnType TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(NEW_JDD_OBC_DCDC_BYTE_5 data);
Std_ReturnType TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(NEW_JDD_OBC_DCDC_BYTE_6 data);
Std_ReturnType TSC_CtApJDD_Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(NEW_JDD_OBC_DCDC_BYTE_7 data);
Std_ReturnType TSC_CtApJDD_Rte_Write_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError(boolean data);

/** Client server interfaces */
Std_ReturnType TSC_CtApJDD_Rte_Call_PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx(void);

/** Service interfaces */
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x056216_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x056216_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x056317_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x056317_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0a0804_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0a0804_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0a084b_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0a084b_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0a9464_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0a9464_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0af864_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0af864_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0cf464_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x0cf464_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x108093_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x108093_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c413_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c413_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c512_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c512_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c613_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c613_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c713_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x10c713_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120a11_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120a11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120a12_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120a12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120b11_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120b11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120b12_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120b12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120c64_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120c64_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120c98_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120c98_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120d64_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120d64_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120d98_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x120d98_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d711_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d711_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d712_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d712_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d713_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d713_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d811_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d811_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d812_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d812_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d813_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d813_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d911_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d911_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d912_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d912_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d913_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12d913_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12da11_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12da11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12da12_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12da12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12da13_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12da13_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12db12_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12db12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12dc11_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12dc11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12dd12_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12dd12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12de11_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12de11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12df13_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12df13_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e012_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e012_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e111_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e111_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e213_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e213_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e319_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e319_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e712_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e712_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e811_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e811_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e912_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12e912_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12ea11_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12ea11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12f316_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12f316_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12f917_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x12f917_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x13e919_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x13e919_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x166c64_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x166c64_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179e11_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179e11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179e12_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179e12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179f11_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179f11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179f12_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x179f12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a0064_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a0064_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a7104_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a7104_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a714b_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a714b_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a7172_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0x1a7172_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xc07988_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xc07988_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xc08913_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xc08913_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd18787_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd18787_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd1a087_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd1a087_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd20781_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd20781_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd2a081_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd2a081_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd38782_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd38782_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd38783_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xd38783_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00081_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00081_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00087_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00087_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00214_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00214_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00362_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_CtApJDD_Rte_Call_EvtInfo_DTC_0xe00362_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_CtApJDD_Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_ReadBlock(dtRef_VOID DstPtr);
Std_ReturnType TSC_CtApJDD_Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_SetRamBlockStatus(boolean RamBlockStatus);
Std_ReturnType TSC_CtApJDD_Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr);

/** SW-C local Calibration Parameters */
boolean  TSC_CtApJDD_Rte_CData_CalEnableJDD(void);

/** Per Instance Memories */
Rte_DT_IdtArrayJDDNvMRamMirror_0 *TSC_CtApJDD_Rte_Pim_PimJDD_NvMRamMirror(void);



