/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApPLS.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApPLS.h"
#include "TSC_CtApPLS.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApPLS_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
{
  return Rte_Read_PpBatteryVolt_DeBatteryVolt(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data)
{
  return Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(IdtInputVoltageMode *data)
{
  return Rte_Read_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInt_BMS_Voltage_BMS_Voltage(BMS_Voltage *data)
{
  return Rte_Read_PpInt_BMS_Voltage_BMS_Voltage(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(DCDC_Status *data)
{
  return Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data)
{
  return Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
{
  return Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data)
{
  return Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data)
{
  return Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInt_DCLV_Input_Current_DCLV_Input_Current(DCLV_Input_Current *data)
{
  return Rte_Read_PpInt_DCLV_Input_Current_DCLV_Input_Current(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
{
  return Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(DCLV_Measured_Current *data)
{
  return Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(DCLV_Measured_Voltage *data)
{
  return Rte_Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
{
  return Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status *data)
{
  return Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(PFC_IPH1_RMS_0A1 *data)
{
  return Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
{
  return Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean *data)
{
  return Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(data);
}




Std_ReturnType TSC_CtApPLS_Rte_Write_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError(boolean data)
{
  return Rte_Write_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError(boolean data)
{
  return Rte_Write_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError(boolean data)
{
  return Rte_Write_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(IdtFaultLevel data)
{
  return Rte_Write_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(IdtFaultLevel data)
{
  return Rte_Write_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(IdtFaultLevel data)
{
  return Rte_Write_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault(boolean data)
{
  return Rte_Write_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault(boolean data)
{
  return Rte_Write_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault(boolean data)
{
  return Rte_Write_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault(boolean data)
{
  return Rte_Write_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault(boolean data)
{
  return Rte_Write_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(DCDC_InputVoltage data)
{
  return Rte_Write_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(DCDC_OutputCurrent data)
{
  return Rte_Write_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpInt_OBC_OutputCurrent_OBC_OutputCurrent(OBC_OutputCurrent data)
{
  return Rte_Write_PpInt_OBC_OutputCurrent_OBC_OutputCurrent(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(OBC_OutputVoltage data)
{
  return Rte_Write_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error(IdtFaultLevel data)
{
  return Rte_Write_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error(IdtFaultLevel data)
{
  return Rte_Write_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error(IdtFaultLevel data)
{
  return Rte_Write_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error(IdtFaultLevel data)
{
  return Rte_Write_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error(IdtFaultLevel data)
{
  return Rte_Write_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error(IdtFaultLevel data)
{
  return Rte_Write_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error(IdtFaultLevel data)
{
  return Rte_Write_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError(boolean data)
{
  return Rte_Write_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error(IdtFaultLevel data)
{
  return Rte_Write_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError(boolean data)
{
  return Rte_Write_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError(boolean data)
{
  return Rte_Write_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError(boolean data)
{
  return Rte_Write_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError(data);
}

Std_ReturnType TSC_CtApPLS_Rte_Write_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset(IdtVoltageCorrectionOffset data)
{
  return Rte_Write_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApPLS_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
{
  return Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(frameNum, byteNum, data);
}


     /* Service calls */
Std_ReturnType TSC_CtApPLS_Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID)
{
  return Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(CPID);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





IdtDCLVFaultTimeToRetryDefault  TSC_CtApPLS_Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(void)
{
  return (IdtDCLVFaultTimeToRetryDefault ) Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault();
}
IdtDCLVFaultTimeToRetryShortCircuit  TSC_CtApPLS_Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(void)
{
  return (IdtDCLVFaultTimeToRetryShortCircuit ) Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit();
}
IdtOBCFaultTimeToRetryDefault  TSC_CtApPLS_Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(void)
{
  return (IdtOBCFaultTimeToRetryDefault ) Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault();
}
IdtOBCFaultTimeToRetryOvertemp  TSC_CtApPLS_Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(void)
{
  return (IdtOBCFaultTimeToRetryOvertemp ) Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp();
}
IdtOBCFaultTimeToRetryVoltageError  TSC_CtApPLS_Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(void)
{
  return (IdtOBCFaultTimeToRetryVoltageError ) Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError();
}

IdtACTempTripTime  TSC_CtApPLS_Rte_CData_CalACTempTripTime(void)
{
  return (IdtACTempTripTime ) Rte_CData_CalACTempTripTime();
}
IdtACTempWarningTime  TSC_CtApPLS_Rte_CData_CalACTempWarningTime(void)
{
  return (IdtACTempWarningTime ) Rte_CData_CalACTempWarningTime();
}
IdtDCTempTripTime  TSC_CtApPLS_Rte_CData_CalDCTempTripTime(void)
{
  return (IdtDCTempTripTime ) Rte_CData_CalDCTempTripTime();
}
IdtDCTempWarningTime  TSC_CtApPLS_Rte_CData_CalDCTempWarningTime(void)
{
  return (IdtDCTempWarningTime ) Rte_CData_CalDCTempWarningTime();
}
IdtHVVTripTime  TSC_CtApPLS_Rte_CData_CalHVVTripTime(void)
{
  return (IdtHVVTripTime ) Rte_CData_CalHVVTripTime();
}
IdtHVVWarningTime  TSC_CtApPLS_Rte_CData_CalHVVWarningTime(void)
{
  return (IdtHVVWarningTime ) Rte_CData_CalHVVWarningTime();
}
IdtLVPTripTime  TSC_CtApPLS_Rte_CData_CalLVPTripTime(void)
{
  return (IdtLVPTripTime ) Rte_CData_CalLVPTripTime();
}
IdtLVVTripTime  TSC_CtApPLS_Rte_CData_CalLVVTripTime(void)
{
  return (IdtLVVTripTime ) Rte_CData_CalLVVTripTime();
}
IdtLVVWarningTime  TSC_CtApPLS_Rte_CData_CalLVVWarningTime(void)
{
  return (IdtLVVWarningTime ) Rte_CData_CalLVVWarningTime();
}
IdtOBCMaxEfficiency  TSC_CtApPLS_Rte_CData_CalOBCMaxEfficiency(void)
{
  return (IdtOBCMaxEfficiency ) Rte_CData_CalOBCMaxEfficiency();
}
IdtOBCOffsetAllowed  TSC_CtApPLS_Rte_CData_CalOBCOffsetAllowed(void)
{
  return (IdtOBCOffsetAllowed ) Rte_CData_CalOBCOffsetAllowed();
}
IdtOBCPTripTime  TSC_CtApPLS_Rte_CData_CalOBCPTripTime(void)
{
  return (IdtOBCPTripTime ) Rte_CData_CalOBCPTripTime();
}
IdtOffsetAllowed  TSC_CtApPLS_Rte_CData_CalOffsetAllowed(void)
{
  return (IdtOffsetAllowed ) Rte_CData_CalOffsetAllowed();
}
IdtACTempTripThreshold  TSC_CtApPLS_Rte_CData_CalACTempTripThreshold(void)
{
  return (IdtACTempTripThreshold ) Rte_CData_CalACTempTripThreshold();
}
IdtACTempWarningThreshold  TSC_CtApPLS_Rte_CData_CalACTempWarningThreshold(void)
{
  return (IdtACTempWarningThreshold ) Rte_CData_CalACTempWarningThreshold();
}
IdtDCTempTripThreshold  TSC_CtApPLS_Rte_CData_CalDCTempTripThreshold(void)
{
  return (IdtDCTempTripThreshold ) Rte_CData_CalDCTempTripThreshold();
}
IdtDCTempWarningThreshold  TSC_CtApPLS_Rte_CData_CalDCTempWarningThreshold(void)
{
  return (IdtDCTempWarningThreshold ) Rte_CData_CalDCTempWarningThreshold();
}
IdtHVVTripThreshold  TSC_CtApPLS_Rte_CData_CalHVVTripThreshold(void)
{
  return (IdtHVVTripThreshold ) Rte_CData_CalHVVTripThreshold();
}
IdtHVVWarningThreshold  TSC_CtApPLS_Rte_CData_CalHVVWarningThreshold(void)
{
  return (IdtHVVWarningThreshold ) Rte_CData_CalHVVWarningThreshold();
}
IdtLVVTripThreshold  TSC_CtApPLS_Rte_CData_CalLVVTripThreshold(void)
{
  return (IdtLVVTripThreshold ) Rte_CData_CalLVVTripThreshold();
}
IdtLVVWarningThreshold  TSC_CtApPLS_Rte_CData_CalLVVWarningThreshold(void)
{
  return (IdtLVVWarningThreshold ) Rte_CData_CalLVVWarningThreshold();
}
IdtMaxEfficiency  TSC_CtApPLS_Rte_CData_CalMaxEfficiency(void)
{
  return (IdtMaxEfficiency ) Rte_CData_CalMaxEfficiency();
}
IdtMinEfficiency  TSC_CtApPLS_Rte_CData_CalMinEfficiency(void)
{
  return (IdtMinEfficiency ) Rte_CData_CalMinEfficiency();
}
IdtOBCMinEfficiency  TSC_CtApPLS_Rte_CData_CalOBCMinEfficiency(void)
{
  return (IdtOBCMinEfficiency ) Rte_CData_CalOBCMinEfficiency();
}
IdtOBC_DelayEfficiencyDCLV  TSC_CtApPLS_Rte_CData_CalOBC_DelayEfficiencyDCLV(void)
{
  return (IdtOBC_DelayEfficiencyDCLV ) Rte_CData_CalOBC_DelayEfficiencyDCLV();
}
IdtOBC_DelayEfficiencyHVMono  TSC_CtApPLS_Rte_CData_CalOBC_DelayEfficiencyHVMono(void)
{
  return (IdtOBC_DelayEfficiencyHVMono ) Rte_CData_CalOBC_DelayEfficiencyHVMono();
}
IdtOBC_DelayEfficiencyHVTri  TSC_CtApPLS_Rte_CData_CalOBC_DelayEfficiencyHVTri(void)
{
  return (IdtOBC_DelayEfficiencyHVTri ) Rte_CData_CalOBC_DelayEfficiencyHVTri();
}

     /* CtApPLS */
      /* CtApPLS */



