/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApPXL.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApPXL.h"
#include "TSC_CtApPXL.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApPXL_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
{
  return Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(data);
}

Std_ReturnType TSC_CtApPXL_Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(IdtProximityDetectPhysicalValue *data)
{
  return Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(data);
}

Std_ReturnType TSC_CtApPXL_Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
{
  return Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(data);
}




Std_ReturnType TSC_CtApPXL_Rte_Write_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati data)
{
  return Rte_Write_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApPXL_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}


     /* Service calls */
Std_ReturnType TSC_CtApPXL_Rte_Call_NvMService_AC3_SRBS_PXLNvBlockNeed_ReadBlock(dtRef_VOID DstPtr)
{
  return Rte_Call_NvMService_AC3_SRBS_PXLNvBlockNeed_ReadBlock(DstPtr);
}
Std_ReturnType TSC_CtApPXL_Rte_Call_NvMService_AC3_SRBS_PXLNvBlockNeed_SetRamBlockStatus(boolean RamBlockStatus)
{
  return Rte_Call_NvMService_AC3_SRBS_PXLNvBlockNeed_SetRamBlockStatus(RamBlockStatus);
}
Std_ReturnType TSC_CtApPXL_Rte_Call_NvMService_AC3_SRBS_PXLNvBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr)
{
  return Rte_Call_NvMService_AC3_SRBS_PXLNvBlockNeed_WriteBlock(SrcPtr);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtProximityDetectVoltThreshold  TSC_CtApPXL_Rte_CData_CalProximityDetectMaxVoltage(void)
{
  return (IdtProximityDetectVoltThreshold ) Rte_CData_CalProximityDetectMaxVoltage();
}
IdtProximityDetectVoltThreshold  TSC_CtApPXL_Rte_CData_CalProximityDetectMinVoltage(void)
{
  return (IdtProximityDetectVoltThreshold ) Rte_CData_CalProximityDetectMinVoltage();
}
IdtProximityDetectVoltThreshold  TSC_CtApPXL_Rte_CData_CalProximityNotDetectMaxVoltage(void)
{
  return (IdtProximityDetectVoltThreshold ) Rte_CData_CalProximityNotDetectMaxVoltage();
}
IdtProximityDetectVoltThreshold  TSC_CtApPXL_Rte_CData_CalProximityNotDetectMinVoltage(void)
{
  return (IdtProximityDetectVoltThreshold ) Rte_CData_CalProximityNotDetectMinVoltage();
}
IdtDebounceProxDetect  TSC_CtApPXL_Rte_CData_CalDebounceProxDetectConnected(void)
{
  return (IdtDebounceProxDetect ) Rte_CData_CalDebounceProxDetectConnected();
}
IdtDebounceProxDetect  TSC_CtApPXL_Rte_CData_CalDebounceProxDetectNotConnected(void)
{
  return (IdtDebounceProxDetect ) Rte_CData_CalDebounceProxDetectNotConnected();
}
IdtDebounceProxDetect  TSC_CtApPXL_Rte_CData_CalDebounceProxInvalid(void)
{
  return (IdtDebounceProxDetect ) Rte_CData_CalDebounceProxInvalid();
}

     /* CtApPXL */
      /* CtApPXL */

/** Per Instance Memories */
uint8 *TSC_CtApPXL_Rte_Pim_PimPXL_NvMRamMirror(void)
{
  return Rte_Pim_PimPXL_NvMRamMirror();
}
boolean *TSC_CtApPXL_Rte_Pim_PimProximityFailure(void)
{
  return Rte_Pim_PimProximityFailure();
}



