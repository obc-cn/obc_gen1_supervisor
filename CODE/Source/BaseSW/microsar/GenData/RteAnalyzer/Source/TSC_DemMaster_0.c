/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DemMaster_0.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_DemMaster_0.h"
#include "TSC_DemMaster_0.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x056216_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x056216_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x056317_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x056317_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0a0804_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x0a0804_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0a084b_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x0a084b_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0a9464_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x0a9464_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0af864_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x0af864_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x0cf464_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x0cf464_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x108093_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x108093_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x10c413_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x10c413_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x10c512_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x10c512_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x10c613_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x10c613_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x10c713_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x10c713_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120a11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x120a11_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120a12_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x120a12_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120b11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x120b11_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120b12_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x120b12_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120c64_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x120c64_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120c98_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x120c98_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120d64_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x120d64_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x120d98_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x120d98_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d711_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12d711_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d712_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12d712_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d713_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12d713_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d811_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12d811_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d812_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12d812_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d813_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12d813_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d911_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12d911_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d912_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12d912_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12d913_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12d913_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12da11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12da11_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12da12_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12da12_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12da13_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12da13_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12db12_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12db12_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12dc11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12dc11_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12dd12_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12dd12_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12de11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12de11_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12df13_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12df13_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e012_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12e012_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e111_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12e111_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e213_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12e213_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e319_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12e319_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e712_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12e712_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e811_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12e811_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12e912_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12e912_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12ea11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12ea11_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12f316_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12f316_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x12f917_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x12f917_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x13e919_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x13e919_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x166c64_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x166c64_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x179e11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x179e11_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x179e12_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x179e12_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x179f11_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x179f11_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x179f12_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x179f12_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x1a0064_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x1a0064_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x1a7104_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x1a7104_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x1a714b_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x1a714b_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0x1a7172_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0x1a7172_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xc07988_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0xc07988_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xc08913_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0xc08913_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd18787_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0xd18787_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd1a087_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0xd1a087_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd20781_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0xd20781_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd2a081_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0xd2a081_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd38782_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0xd38782_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xd38783_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0xd38783_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xe00081_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0xe00081_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xe00087_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0xe00087_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xe00214_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0xe00214_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBFaultDetectCtr_DTC_0xe00362_GetFaultDetectionCounter(sint8 *FaultDetectionCounter)
{
  return Rte_Call_CBFaultDetectCtr_DTC_0xe00362_GetFaultDetectionCounter(FaultDetectionCounter);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D407_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D407_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D40C_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D40C_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D49C_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D49C_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D4CA_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D4CA_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D5CF_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D5CF_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D5D1_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D5D1_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D805_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D805_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D806_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D806_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D807_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D807_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D808_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D808_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D809_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D809_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D80C_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D80C_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D822_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D822_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D824_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D824_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D825_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D825_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D827_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D827_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D828_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D828_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D829_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D829_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82B_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D82B_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82C_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D82C_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82D_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D82D_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82E_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D82E_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D82F_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D82F_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D831_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D831_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83B_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D83B_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83C_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D83C_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83D_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D83D_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83E_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D83E_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D83F_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D83F_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D840_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D840_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D843_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D843_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D844_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D844_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D845_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D845_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D846_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D846_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84A_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D84A_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84B_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D84B_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84C_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D84C_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84D_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D84D_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84E_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D84E_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D84F_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D84F_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D850_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D850_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D851_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D851_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D852_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D852_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D853_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D853_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D854_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D854_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D855_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D855_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D8E9_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D8E9_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_D8EB_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D8EB_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE60_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE60_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE61_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE61_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE62_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE62_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE63_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE63_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE64_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE64_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE65_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE65_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE66_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE66_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE67_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE67_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE68_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE68_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE69_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE69_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6A_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE6A_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6B_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE6B_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6C_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE6C_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6D_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE6D_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6E_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE6E_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE6F_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE6F_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE70_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE70_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE71_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE71_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE72_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE72_ReadData(Data);
}
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_FE73_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE73_ReadData(Data);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* DemMaster_0 */
      /* DemMaster_0 */



